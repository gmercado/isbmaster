--users
insert into SBISASQL/ISBP82
(S82NOM, S82PWD, S82DESC, S82EMAIL, S82DISP, S82USUA)
VALUES
('admin',
'c90ca518f7afea5584fb0a31f9e1604e92f7290ff65614d2:f327f0ad39572a8e5ca692134ac8d4820535d82d2c75e68c',
'MIGUEL VEGA', 'mavega@grupobisa.com' ,'S', 'ISB Engine');

insert into SBISASQL.ISBP82
(S82NOM, S82PWD, S82DESC, S82EMAIL, S82DISP, S82USUA)
VALUES
  ('soap',
   '460388f5cbc5a7d89d90060e603cd559421db06f47857584:0487764a3e8945399a71f1b013afe1f87b725ed90fb38c4f',
   'SOAP ONLY', 'mavega@grupobisa.com' ,'S', 'ISB Engine');

insert into SBISASQL.ISBP82
(S82NOM, S82PWD, S82DESC, S82EMAIL, S82DISP, S82USUA)
VALUES
  ('rest',
   '2fc84f38528b300065d7e28f9592d3627f4b5e9f95a6afd6:2ac98a70a84d963a532a40f587d6b3aa45796f2c8ba8697e',
   'REST ONLY', 'mavega@grupobisa.com' ,'S', 'ISB Engine');

--roles
insert into SBISASQL/ISBP83
(S83NOM, S83DESC, S83DISP, S83USUA)
VALUES
('ALL', 'All permissions', 'S', 'ISB Engine');

insert into SBISASQL/ISBP83
(S83NOM, S83DESC, S83DISP, S83USUA)
VALUES
('SOAP ROLE', 'Only SOAP Services', 'S', 'ISB Engine');

insert into SBISASQL/ISBP83
(S83NOM, S83DESC, S83DISP, S83USUA)
VALUES
('REST ROLE', 'Only REST services', 'S', 'ISB Engine');

-- user-roles
insert into SBISASQL/ISBP85
(S85ISBP82, S85ISBP83, S85DISP, S85USUA)
VALUES
(
(select S82ID from sbisasql/ISBP82 where S82NOM='admin'),
(select S83ID from sbisasql/ISBP83 where S83NOM='ALL'),
'S', 'ISB Engine');

insert into SBISASQL/ISBP85
(S85ISBP82, S85ISBP83, S85DISP, S85USUA)
VALUES
(
(select S82ID from sbisasql/ISBP82 where S82NOM='soap'),
(select S83ID from sbisasql/ISBP83 where S83NOM='SOAP ROLE'),
'S', 'ISB Engine');

insert into SBISASQL/ISBP85
(S85ISBP82, S85ISBP83, S85DISP, S85USUA)
VALUES
(
(select S82ID from sbisasql/ISBP82 where S82NOM='rest'),
(select S83ID from sbisasql/ISBP83 where S83NOM='REST ROLE'),
'S', 'ISB Engine');


-- insertion of user for operations
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1076, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1077, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1078, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1079, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
--rest
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1080, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1081, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1082, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1083, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1084, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1085, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1086, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1087, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1088, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1089, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1090, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1091, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1092, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1093, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1094, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1095, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1096, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1097, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1098, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1099, 1009, 'S', '@ME', CURRENT_TIMESTAMP);

--SOAP only
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1076, 1012, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1077, 1012, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1078, 1012, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1079, 1012, 'S', '@ME', CURRENT_TIMESTAMP);

--REST Only
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1080, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1081, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1082, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1083, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1084, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1085, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1086, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1087, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1088, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1089, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1090, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1091, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1092, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1093, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1094, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1095, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1096, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1097, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1098, 1009, 'S', '@ME', CURRENT_TIMESTAMP);
insert into sbisasql.ISBP88 (S88ISBP87, S88ISBP83, S88disp, S88usua, S88feca) values (1099, 1009, 'S', '@ME', CURRENT_TIMESTAMP);