--users
insert into ISBP92
(S92NOM, S92PWD, S92DESC, S92EMAIL, S92DISP, S92USUA, s92feca)
VALUES
  ('admin',
   'c90ca518f7afea5584fb0a31f9e1604e92f7290ff65614d2:f327f0ad39572a8e5ca692134ac8d4820535d82d2c75e68c',
   'MIGUEL VEGA', 'mavega@grupobisa.com' ,'S', 'ISB Engine', now());

insert into ISBP92
(S92NOM, S92PWD, S92DESC, S92EMAIL, S92DISP, S92USUA, s92feca)
VALUES
  ('soap',
   '460388f5cbc5a7d89d90060e603cd559421db06f47857584:0487764a3e8945399a71f1b013afe1f87b725ed90fb38c4f',
   'SOAP ONLY', 'mavega@grupobisa.com' ,'S', 'ISB Engine', now());

insert into ISBP92
(S92NOM, S92PWD, S92DESC, S92EMAIL, S92DISP, S92USUA, s92feca)
VALUES
  ('rest',
   '2fc84f38528b300065d7e28f9592d3627f4b5e9f95a6afd6:2ac98a70a84d963a532a40f587d6b3aa45796f2c8ba8697e',
   'REST ONLY', 'mavega@grupobisa.com' ,'S', 'ISB Engine', now());

--roles
insert into ISBP93
(S93NOM, S93DESC, S93DISP, S93USUA, s93feca)
VALUES
  ('ALL', 'All permissions', 'S', 'ISB Engine', now());

insert into ISBP93
(S93NOM, S93DESC, S93DISP, S93USUA, s93feca)
VALUES
  ('SOAP ROLE', 'Only SOAP Services', 'S', 'ISB EnAccess denied for IP addressgine', now());

insert into ISBP93
(S93NOM, S93DESC, S93DISP, S93USUA, s93feca)
VALUES
  ('REST ROLE', 'Only REST services', 'S', 'ISB Engine', now());

-- user-roles
insert into ISBP95
(S95ISBP92, S95ISBP93, S95DISP, S95USUA, s95feca)
VALUES
  (
    (select S92ID from isbp92 where S92NOM='admin'),
  (select S93ID from isbp93 where S93NOM='ALL'),
  'S', 'ISB Engine', now());

insert into ISBP95
(S95ISBP92, S95ISBP93, S95DISP, S95USUA, s95feca)
VALUES
  (
    (select S92ID from isbp92 where S92NOM='soap'),
  (select S93ID from isbp93 where S93NOM='SOAP ROLE'),
  'S', 'ISB Engine', now());

insert into ISBP95
(S95ISBP92, S95ISBP93, S95DISP, S95USUA, s95feca)
VALUES
  (
    (select S92ID from isbp92 where S92NOM='rest'),
  (select S93ID from isbp93 where S93NOM='REST ROLE'),
  'S', 'ISB Engine', now());

/*
The following insert will work only after the server has been runned the first time
*/
-- insertion of user for operations
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (1, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (2, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (3, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (4, 1, 'S', '@ME', NOW());
--rest
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (5, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (6, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (7, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (8, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (9, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (10, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (11, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (12, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (13, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (14, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (15, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (16, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (17, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (18, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (19, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (20, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (21, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (22, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (23, 1, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (24, 1, 'S', '@ME', NOW());

--SOAP only
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (1, 2, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (2, 2, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (3, 2, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (4, 2, 'S', '@ME', NOW());

--REST Only
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (5, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (6, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (7, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (8, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (9, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (10, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (11, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (12, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (13, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (14, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (15, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (16, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (17, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (18, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (19, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (20, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (21, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (22, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (23, 3, 'S', '@ME', NOW());
insert into isbp98 (s98isbp97, s98isbp93, s98disp, s98usua, s98feca) values (24, 3, 'S', '@ME', NOW());