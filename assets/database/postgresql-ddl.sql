/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     12/3/2015 11:53:16 PM                        */
/*==============================================================*/

/*

drop index RELATIONSHIP_9_FK;

drop index ISBP91_PK;

drop table ISBP91;

drop index ISBP92_PK;

drop table ISBP92;

drop index ISBP93_PK;

drop table ISBP93;

drop index RELATIONSHIP_8_FK;

drop index ISBP94_PK;

drop table ISBP94;

drop index RELATIONSHIP_3_FK;

drop index RELATIONSHIP_2_FK;

drop index ISBP95_PK;

drop table ISBP95;

drop index ISBP96_PK;

drop table ISBP96;

drop index RELATIONSHIP_7_FK;

drop index ISBP97_PK;

drop table ISBP97;

drop index RELATIONSHIP_6_FK;

drop index RELATIONSHIP_5_FK;

drop index ISBP98_PK;

drop table ISBP98;
*/

/*==============================================================*/
/* Table: ISBP91                                                */
/*==============================================================*/
create table ISBP91 (
   S91IP                VARCHAR(20)          not null,
   S91ISBP92            INT4                 not null,
   S91DISP              CHAR(1)              null,
   S91USUA              VARCHAR(30)          null,
   S91FECA              DATE                 null,
   S91USUM              VARCHAR(30)          null,
   S91FECM              DATE                 null,
   constraint PK_ISBP91 primary key (S91IP, S91ISBP92)
);

comment on table ISBP91 is
'A table to determine allowed addresses';

/*==============================================================*/
/* Index: ISBP91_PK                                             */
/*==============================================================*/
create unique index ISBP91_PK on ISBP91 (
S91IP,
S91ISBP92
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_9_FK on ISBP91 (
S91ISBP92
);

/*==============================================================*/
/* Table: ISBP92                                                */
/*==============================================================*/
create table ISBP92 (
   S92ID                SERIAL               not null,
   S92NOM               VARCHAR(30)          not null,
   S92DESC              VARCHAR(200)         not null,
   S92PWD               VARCHAR(100)         not null,
   S92EMAIL             VARCHAR(100)         null,
   S92DISP              CHAR(1)              null default 'S'
      constraint CKC_S92DISP_ISBP92 check (S92DISP is null or (S92DISP in ('S','N'))),
   S92USUA              VARCHAR(30)          not null,
   S92FECA              DATE                 not null,
   S92USUM              VARCHAR(30)          null,
   S92FECM              DATE                 null,
   constraint PK_ISBP92 primary key (S92ID)
);

comment on table ISBP92 is
'Users table';

/*==============================================================*/
/* Index: ISBP92_PK                                             */
/*==============================================================*/
create unique index ISBP92_PK on ISBP92 (
S92ID
);

/*==============================================================*/
/* Table: ISBP93                                                */
/*==============================================================*/
create table ISBP93 (
   S93ID                SERIAL               not null,
   S93NOM               VARCHAR(40)          not null,
   S93DESC              VARCHAR(250)         null,
   S93DISP              CHAR(1)              not null,
   S93USUA              VARCHAR(30)          not null,
   S93FECA              DATE                 not null,
   S93USUM              VARCHAR(30)          null,
   S93FECM              DATE                 null,
   constraint PK_ISBP93 primary key (S93ID)
);

comment on table ISBP93 is
'Web services user roles';

/*==============================================================*/
/* Index: ISBP93_PK                                             */
/*==============================================================*/
create unique index ISBP93_PK on ISBP93 (
S93ID
);

/*==============================================================*/
/* Table: ISBP94                                                */
/*==============================================================*/
create table ISBP94 (
   S94ID                SERIAL               not null,
   S94ISBP97            INT4                 null,
   S94SOLIC             VARCHAR(254)         not null,
   S94RESP              VARCHAR(254)         null,
   S94FECREG            DATE                 not null,
   S94MILIS             INT8                 null,
   S94HTTPST            INT4                 not null,
   S94CODRES            VARCHAR(4)           null,
   S94IP                VARCHAR(15)          null,
   constraint PK_ISBP94 primary key (S94ID)
);

comment on table ISBP94 is
'Table where all IN/OUTmessages from/to services are invoked';

/*==============================================================*/
/* Index: ISBP94_PK                                             */
/*==============================================================*/
create unique index ISBP94_PK on ISBP94 (
S94ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_8_FK on ISBP94 (
S94ISBP97
);

/*==============================================================*/
/* Table: ISBP95                                                */
/*==============================================================*/
create table ISBP95 (
   S95ISBP93            INT4                 not null,
   S95ISBP92            INT4                 not null,
   S95DISP              CHAR(1)              not null
      constraint CKC_S95DISP_ISBP95 check (S95DISP in ('S','N')),
   S95USUA              VARCHAR(30)          not null,
   S95FECA              DATE                 not null,
   S95USUM              VARCHAR(30)          null,
   S95FECM              DATE                 null,
   constraint PK_ISBP95 primary key (S95ISBP93, S95ISBP92)
);

comment on table ISBP95 is
'Relationship between users and roles';

/*==============================================================*/
/* Index: ISBP95_PK                                             */
/*==============================================================*/
create unique index ISBP95_PK on ISBP95 (
S95ISBP93,
S95ISBP92
);

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_2_FK on ISBP95 (
S95ISBP92
);

/*==============================================================*/
/* Index: RELATIONSHIP_3_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_3_FK on ISBP95 (
S95ISBP93
);

/*==============================================================*/
/* Table: ISBP96                                                */
/*==============================================================*/
create table ISBP96 (
   S96ID                SERIAL               not null,
   S96NOM               VARCHAR(150)         not null,
   S96DESC              VARCHAR(255)         null,
   S96NS                VARCHAR(150)         not null,
   S96ENDPNT            VARCHAR(150)         null,
   S96TIPO              VARCHAR(4)           not null
      constraint CKC_S96TIPO_ISBP96 check (S96TIPO in ('SOAP','REST')),
   S96DISP              CHAR(1)              null default 'S'
      constraint CKC_S96DISP_ISBP96 check (S96DISP is null or (S96DISP in ('S','N'))),
   S96JCLA              VARCHAR(200)         null,
   S96USUA              VARCHAR(30)          not null,
   S96FECA              DATE                 not null,
   S96USUM             VARCHAR(30)          null,
   S96FECM              DATE                 null,
   constraint PK_ISBP96 primary key (S96ID)
);

comment on table ISBP96 is
'Web services definition table';

comment on column ISBP96.S96NS is
'Namespace';

comment on column ISBP96.S96ENDPNT is
'Endpoint';

comment on column ISBP96.S96JCLA is
'Java type full name';

/*==============================================================*/
/* Index: ISBP96_PK                                             */
/*==============================================================*/
create unique index ISBP96_PK on ISBP96 (
S96ID
);

/*==============================================================*/
/* Table: ISBP97                                                */
/*==============================================================*/
create table ISBP97 (
   S97ID                SERIAL               not null,
   S97ISBP96            INT4                 not null,
   S97NOM               VARCHAR(150)         not null,
   S97JOP               VARCHAR(150)         not null,
   S97DESC              VARCHAR(255)         null,
   S97DISP              CHAR(1)              not null default 'S'
      constraint CKC_S97DISP_ISBP97 check (S97DISP in ('S','N')),
   S97USUA              VARCHAR(30)          not null,
   S97FECA              DATE                 not null,
   S97USUM              VARCHAR(30)          null,
   S97FECM              DATE                 null,
   constraint PK_ISBP97 primary key (S97ID)
);

comment on table ISBP97 is
'Web service operations';

/*==============================================================*/
/* Index: ISBP97_PK                                             */
/*==============================================================*/
create unique index ISBP97_PK on ISBP97 (
S97ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_7_FK on ISBP97 (
S97ISBP96
);

/*==============================================================*/
/* Table: ISBP98                                                */
/*==============================================================*/
create table ISBP98 (
   S98ISBP97            INT4                 not null,
   S98ISBP93            INT4                 not null,
   S98DISP              CHAR(1)              not null
      constraint CKC_S98DISP_ISBP98 check (S98DISP in ('S','N')),
   S98USUA              VARCHAR(30)          not null,
   S98FECA              DATE                 not null,
   S98USUM              VARCHAR(30)          null,
   S98FECM              DATE                 null,
   constraint PK_ISBP98 primary key (S98ISBP97, S98ISBP93)
);

comment on table ISBP98 is
'Table for relationship between role and web service operations';

/*==============================================================*/
/* Index: ISBP98_PK                                             */
/*==============================================================*/
create unique index ISBP98_PK on ISBP98 (
S98ISBP97,
S98ISBP93
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_5_FK on ISBP98 (
S98ISBP93
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_6_FK on ISBP98 (
S98ISBP97
);

alter table ISBP91
   add constraint FK_ISBP91_RELATIONS_ISBP92 foreign key (S91ISBP92)
      references ISBP92 (S92ID)
      on delete restrict on update restrict;

alter table ISBP94
   add constraint FK_ISBP94_RELATIONS_ISBP97 foreign key (S94ISBP97)
      references ISBP97 (S97ID)
      on delete restrict on update restrict;

alter table ISBP95
   add constraint FK_ISBP95_RELATIONS_ISBP92 foreign key (S95ISBP92)
      references ISBP92 (S92ID)
      on delete restrict on update restrict;

alter table ISBP95
   add constraint FK_ISBP95_RELATIONS_ISBP93 foreign key (S95ISBP93)
      references ISBP93 (S93ID)
      on delete restrict on update restrict;

alter table ISBP97
   add constraint FK_ISBP97_RELATIONS_ISBP96 foreign key (S97ISBP96)
      references ISBP96 (S96ID)
      on delete restrict on update restrict;

alter table ISBP98
   add constraint FK_ISBP98_RELATIONS_ISBP93 foreign key (S98ISBP93)
      references ISBP93 (S93ID)
      on delete restrict on update restrict;

alter table ISBP98
   add constraint FK_ISBP98_RELATIONS_ISBP97 foreign key (S98ISBP97)
      references ISBP97 (S97ID)
      on delete restrict on update restrict;

