drop table dBISASQL/ICBOT100;
drop table dBISASQL/ICBOT101;
drop table dBISASQL/ICBOT102;
drop table dBISASQL/ICBOT103;
drop table dBISASQL/ICBOT104;
drop table dBISASQL/ICBOT110;
drop table dBISASQL/ICBOT111;
drop table dBISASQL/ICBOT112;

/*==============================================================*/
/* DBMS name:      CHAT BOT															        */
/* Created on:     02/09/2017 12:38:46 p.m.                     */
/*==============================================================*/
-- +++++++++++++++++++++  CLIENTES  +++++++++++++++++++++++++
CREATE TABLE dBISASQL/ICBOT100(
I100CODIGO NUMERIC(10) GENERATED ALWAYS AS IDENTITY
 (START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE
 NO CYCLE NO ORDER NO CACHE),
I100CODCLI VARCHAR(10) NOT NULL,
I100CODGEN VARCHAR(64) NOT NULL,
I100ESTCLI VARCHAR(3) NOT NULL,
I100USRCRE VARCHAR(100) NOT NULL,
I100FECCRE TIMESTAMP NOT NULL,
I100USRMOD VARCHAR(100),
I100FECMOD TIMESTAMP)

LABEL ON TABLE dBISASQL/ICBOT100 IS 'CHAT BOT - Archivo de Clientes'

LABEL ON COLUMN dBISASQL/ICBOT100(
I100CODIGO TEXT IS 'Id' ,
I100CODCLI TEXT IS 'Codigo Cliente' ,
I100CODGEN TEXT IS 'Codigo Generado' ,
I100ESTCLI TEXT IS 'Estado Cliente',
I100USRCRE TEXT IS 'Usuario creacion',
I100FECCRE TEXT IS 'Fecha creacion',
I100USRMOD TEXT IS 'Usuario modificacion',
I100FECMOD TEXT IS 'Fecha modificacion')

-- +++++++++++++++++++++  REDES SOCIALES  +++++++++++++++++++++++++
CREATE TABLE dBISASQL/ICBOT103(
I103CODIGO NUMERIC(10) GENERATED ALWAYS AS IDENTITY
 (START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE
 NO CYCLE NO ORDER NO CACHE),
I103TIPSOC NUMERIC(1) NOT NULL,
I103CODSOC VARCHAR(255) NOT NULL,
I103NOMSOC VARCHAR(500) NOT NULL,
I103ESTSOC VARCHAR(3) NOT NULL,
I103USRCRE VARCHAR(100) NOT NULL,
I103FECCRE TIMESTAMP NOT NULL,
I103USRMOD VARCHAR(100),
I103FECMOD TIMESTAMP)

LABEL ON TABLE dBISASQL/ICBOT103 IS 'CHAT BOT - Archivo de usuarios'

LABEL ON COLUMN dBISASQL/ICBOT103(
I103CODIGO TEXT IS 'Id' ,
I103TIPSOC TEXT IS 'Tipo red social' ,
I103CODSOC TEXT IS 'Codigo red social' ,
I103NOMSOC TEXT IS 'Nombre usuario' ,
I103ESTSOC TEXT IS 'Estado usuario',
I103USRCRE TEXT IS 'Usuario creacion',
I103FECCRE TEXT IS 'Fecha creacion',
I103USRMOD TEXT IS 'Usuario modificacion',
I103FECMOD TEXT IS 'Fecha modificacion')

-- +++++++++++++++++++++  REDES SOCIALES /Usuarios +++++++++++++++++++++++++
CREATE TABLE dBISASQL/ICBOT104(
I104CODIGO NUMERIC(10) GENERATED ALWAYS AS IDENTITY
 (START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE
 NO CYCLE NO ORDER NO CACHE),
I104I103US NUMERIC(10),
I104I100CL NUMERIC(10),
I104FECACT TIMESTAMP,
I104ESTSOC VARCHAR(3) NOT NULL,
I104I111TK NUMERIC(10),
I104USRCRE VARCHAR(100) NOT NULL,
I104FECCRE TIMESTAMP NOT NULL,
I104USRMOD VARCHAR(100),
I104FECMOD TIMESTAMP)

LABEL ON TABLE dBISASQL/ICBOT104 IS 'CHAT BOT - Archivo de usuarios conectados'

LABEL ON COLUMN dBISASQL/ICBOT104(
I104CODIGO TEXT IS 'Id' ,
I104I103US TEXT IS 'Cod usuario' ,
I104I100CL TEXT IS 'Cod cliente' ,
I104FECACT TEXT IS 'Fecha ultima accion' ,
I104ESTSOC TEXT IS 'Estado conexion',
I104I111TK TEXT IS 'Punto Session',
I104USRCRE TEXT IS 'Usuario creacion',
I104FECCRE TEXT IS 'Fecha creacion',
I104USRMOD TEXT IS 'Usuario modificacion',
I104FECMOD TEXT IS 'Fecha modificacion')

-- +++++++++++++++++++++  OPERACIONES  +++++++++++++++++++++++++
CREATE TABLE dBISASQL/ICBOT101(
I101CODIGO NUMERIC(10) GENERATED ALWAYS AS IDENTITY
 (START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE
 NO CYCLE NO ORDER NO CACHE),
I101I104UC NUMERIC(10) NOT NULL,
I101TRANID VARCHAR(40) NOT NULL,
I101TIPROC VARCHAR(3) NOT NULL,
I101PROCES VARCHAR(20),
I101ESTOPE VARCHAR(3) NOT NULL,
I101CDRESP NUMERIC(5),
I101MENSAJ VARCHAR(255),
I101CRIBUS CLOB,
I101RESBUS CLOB,
I101USRCRE VARCHAR(100) NOT NULL,
I101FECCRE TIMESTAMP NOT NULL,
I101USRMOD VARCHAR(100),
I101FECMOD TIMESTAMP)

LABEL ON TABLE dBISASQL/ICBOT101 IS 'CHAT BOT - Archivo de Operaciones'

LABEL ON COLUMN dBISASQL/ICBOT101(
I101CODIGO TEXT IS 'Id' ,
I101I104UC TEXT IS 'Codigo usuario ICBOT104' ,
I101TRANID TEXT IS 'Codigo Transaccion' ,
I101TIPROC TEXT IS 'Tipo Proceso',
I101PROCES TEXT IS 'Proceso' ,
I101ESTOPE TEXT IS 'Estado Operacion' ,
I101CDRESP TEXT IS 'Codigo respuesta' ,
I101MENSAJ TEXT IS 'Mensaje' ,
I101CRIBUS TEXT IS 'Peticion' ,
I101RESBUS TEXT IS 'Respuesta' ,
I101USRCRE TEXT IS 'Usuario creacion',
I101FECCRE TEXT IS 'Fecha creacion',
I101USRMOD TEXT IS 'Usuario modificacion',
I101FECMOD TEXT IS 'Fecha modificacion')

-- +++++++++++++++++++++  SOLICITUDES TOKEN +++++++++++++++++++++++++
CREATE TABLE dBISASQL/ICBOT102(
I102CODIGO NUMERIC(10) GENERATED ALWAYS AS IDENTITY
 (START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE
 NO CYCLE NO ORDER NO CACHE),
I102I101OP NUMERIC(10),
I102TIPROC NUMERIC(20) NOT NULL,
I102CDCONF VARCHAR(64),
I102ESTOPE VARCHAR(3),
I102FECSOL TIMESTAMP,
I102TMPVDA TIMESTAMP,
I102TUSADO CHAR(1),
I102TKNSMS VARCHAR(64),
I102USRCRE VARCHAR(100) NOT NULL,
I102FECCRE TIMESTAMP NOT NULL,
I102USRMOD VARCHAR(100),
I102FECMOD TIMESTAMP)

LABEL ON TABLE dBISASQL/ICBOT102 IS 'CHAT BOT - Archivo de Solicitudes'

LABEL ON COLUMN dBISASQL/ICBOT102(
I102CODIGO TEXT IS 'Id' ,
I102I101OP TEXT IS 'Codigo operacion ICBOT101' ,
I102TIPROC TEXT IS 'Tipo Proceso' ,
I102CDCONF TEXT IS 'Codigo confirmacion' ,
I102ESTOPE TEXT IS 'Estado Operacion' ,
I102FECSOL TEXT IS 'Fecha Solicitud' ,
I102TMPVDA TEXT IS 'Tiempo Vida' ,
I102TUSADO TEXT IS 'Token usado' ,
I102TKNSMS TEXT IS 'Token' ,
I102USRCRE TEXT IS 'Usuario creacion',
I102FECCRE TEXT IS 'Fecha creacion',
I102USRMOD TEXT IS 'Usuario modificacion',
I102FECMOD TEXT IS 'Fecha modificacion')

-- +++++++++++++++++++++  sesiones  +++++++++++++++++++++++++
CREATE TABLE dBISASQL/ICBOT110(
I110ID NUMERIC(10) GENERATED ALWAYS AS IDENTITY
 (START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE
 NO CYCLE NO ORDER NO CACHE),
I110CLI VARCHAR(10),
I110INI	TIMESTAMP,
I110FIN	TIMESTAMP,
I110IP	VARCHAR(15),
I110USRAGNT	VARCHAR(250),
I110LOGIN	VARCHAR(50),
I110EMP		VARCHAR(1),
I110SES		VARCHAR(255))

LABEL ON TABLE dBISASQL/ICBOT110 IS 'CHAT BOT - Sesion'

LABEL ON COLUMN dBISASQL/ICBOT110(
I110ID TEXT IS 'Codigo' ,
I110CLI TEXT IS 'Codigo de cliente' ,
I110INI TEXT IS 'Fecha desde' ,
I110FIN TEXT IS 'Fecha hasta' ,
I110IP TEXT IS 'IP terminal' ,
I110USRAGNT TEXT IS 'Agente cliente' ,
I110LOGIN TEXT IS 'Login' ,
I110EMP TEXT IS 'Si es empleado' ,
I110SES TEXT IS 'Codigo de session')

-- +++++++++++++++++++++  datos chatbot  +++++++++++++++++++++++++
CREATE TABLE dBISASQL/ICBOT111(
I111CODIGO NUMERIC(10) GENERATED ALWAYS AS IDENTITY
 (START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE
 NO CYCLE NO ORDER NO CACHE),
I111SESSID NUMERIC(10),
I111CHBTID BIGINT,
I111USERID BIGINT,
I111ALANOT VARCHAR(100),
I111BISAOT CLOB,
I111CHBTTK CLOB,
I111HOOKTK VARCHAR(100),
I111TKNVAL VARCHAR(2),
I111CHBVAL VARCHAR(2),
I111FECINI TIMESTAMP,
I111FECFIN TIMESTAMP)

LABEL ON TABLE dBISASQL/ICBOT111 IS 'CHAT BOT - Auth'

LABEL ON COLUMN dBISASQL/ICBOT111(
I111CODIGO TEXT IS 'Codigo' ,
I111SESSID TEXT IS 'Codigo de session' ,
I111CHBTID TEXT IS 'Codigo de chat' ,
I111USERID TEXT IS 'Usuario de chat' ,
I111ALANOT TEXT IS 'Codigo Alan' ,
I111BISAOT TEXT IS 'Codigo Bisa' ,
I111CHBTTK TEXT IS 'Token Chat' ,
I111TKNVAL TEXT IS 'Val Token' ,
I111CHBVAL TEXT IS 'Val Chat' ,
I111FECINI TEXT IS 'Fecha inicio',
I111FECFIN TEXT IS 'Fecha fin')

-- +++++++++++++++++++++  definir limites  +++++++++++++++++++++++++
CREATE TABLE dBISASQL/ICBOT112(
I112CODIGO NUMERIC(10) GENERATED ALWAYS AS IDENTITY
 (START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE
 NO CYCLE NO ORDER NO CACHE),
I112UEBISA VARCHAR(25) not null,I112NROCLI VARCHAR(10) not null,
I112SOCIAL NUMERIC(2) not null,I112NOTCEL VARCHAR(1),
 I112NOTEMA VARCHAR(1),I112ESTADO VARCHAR(3) default 'ACT',
 I112ESTCLA VARCHAR(3) default 'ACT',I112BLOQUE VARCHAR(3)
 default 'ACT', I112INTNTS INTEGER default 0, I112LIMDIA
 double default 0.0,I112TOTALD double default 0.0,
 I112LIMTR1 double default 0.0,I112TOTAL1 double
 default 0.0, I112TOTALT double default 0.0, I112USRALT
 VARCHAR(255) not null,I112FECALT TIMESTAMP not null,
 I112USRMOD VARCHAR(255),I112FECMOD TIMESTAMP)

LABEL ON TABLE dBISASQL/ICBOT112 IS 'Chat Bot - Limites'

LABEL ON COLUMN dBISASQL/ICBOT112(
I112CODIGO TEXT IS 'Codigo' ,
I112UEBISA TEXT IS 'Usuario ebisa' ,
I112NROCLI TEXT IS 'Numero Cliente' ,
I112SOCIAL TEXT IS 'Red social' ,
I112NOTCEL TEXT IS 'Notifica cel' ,
I112NOTEMA TEXT IS 'Notifica email' ,
I112ESTADO TEXT IS 'Estado' ,
I112BLOQUE TEXT IS 'BLoquea',I112INTNTS TEXT IS 'Intentos',
I112LIMDIA TEXT IS 'Limite diario',I112TOTALD TEXT IS 'Total',
I112LIMTR1 TEXT IS 'Limite tran1',I112TOTAL1 TEXT IS 'Total 1',
I112TOTALT TEXT IS 'Total final',
I112USRALT TEXT IS 'Usuario Alta',
I112FECALT TEXT IS 'Fecha Alta',
I112USRMOD TEXT IS 'Usuario Modificacion',
I112FECMOD TEXT IS 'Fecha Modificacion')

-- +++++++++++++++++++++  AGENCIAS  +++++++++++++++++++++++++
CREATE TABLE dBISASQL/GEN01(
G01ID   	NUMERIC(10) NOT NULL,
G01TIPAGE VARCHAR(10) NOT NULL,
G01DEPART VARCHAR(30) NOT NULL,
G01CIUDAD	VARCHAR(30) NOT NULL,
G01NOMBRE	VARCHAR(50) NOT NULL,
G01DIRECC	VARCHAR(255),
G01TELEFO	VARCHAR(255),
G01HORARI	VARCHAR(255),
G01LATITU	DOUBLE,G01LONGIT	DOUBLE,
G01DOLARE	VARCHAR(50),G01DEPOBI	VARCHAR(50),
G01CODIGO	NUMERIC(3),G01ESTADO	NUMERIC(3),
G01USRALT	VARCHAR(255) not null,
G01FECALT	TIMESTAMP not null,
G01USRMOD	VARCHAR(255),
G01FECMOD	TIMESTAMP)

LABEL ON TABLE dBISASQL/GEN01 IS 'Agencias'

LABEL ON COLUMN dBISASQL/GEN01(
G01ID TEXT IS 'Codigo' ,
G01TIPAGE TEXT IS 'Tipo agencia/cajero' ,
G01DEPART TEXT IS 'Departamento' ,
G01CIUDAD TEXT IS 'Ciudad' ,
G01NOMBRE TEXT IS 'Nombre' ,
G01DIRECC TEXT IS 'Direccion' ,
G01TELEFO TEXT IS 'Telefono' ,G01HORARI TEXT IS 'Horario' ,
G01LATITU TEXT IS 'Latitud',G01LONGIT TEXT IS 'Longitud',
G01DOLARE TEXT IS 'Dolares',G01DEPOBI TEXT IS 'Depobisa',
G01CODIGO TEXT IS 'Codigo AS400',G01ESTADO TEXT IS 'Estado',
G01USRALT TEXT IS 'Usuario Alta',
G01FECALT TEXT IS 'Fecha Alta',
G01USRMOD TEXT IS 'Usuario Modificacion',
G01FECMOD TEXT IS 'Fecha Modificacion')

-- +++++++++++++++++++++  LLAVES  +++++++++++++++++++++++++
ALTER TABLE dBISASQL/ICBOT100 ADD CONSTRAINT dBISASQL/pk_ICBOT100 PRIMARY KEY (I100CODIGO)

ALTER TABLE dBISASQL/ICBOT101 ADD CONSTRAINT dBISASQL/pk_ICBOT101 PRIMARY KEY (I101CODIGO)

ALTER TABLE dBISASQL/ICBOT102 ADD CONSTRAINT dBISASQL/pk_ICBOT102 PRIMARY KEY (I102CODIGO)

ALTER TABLE dBISASQL/ICBOT103 ADD CONSTRAINT dBISASQL/pk_ICBOT103 PRIMARY KEY (I103CODIGO)

ALTER TABLE dBISASQL/ICBOT104 ADD CONSTRAINT dBISASQL/pk_ICBOT104 PRIMARY KEY (I104CODIGO)

ALTER TABLE dBISASQL/ICBOT110 ADD CONSTRAINT dBISASQL/pk_ICBOT110 PRIMARY KEY (I110ID)

ALTER TABLE dBISASQL/ICBOT111 ADD CONSTRAINT dBISASQL/pk_ICBOT111 PRIMARY KEY (I111CODIGO)

ALTER TABLE dBISASQL/ICBOT112 ADD CONSTRAINT dBISASQL/pk_ICBOT112 PRIMARY KEY (I112CODIGO)

-- +++++++++++++++++++++  INDICES  +++++++++++++++++++++++++
CREATE INDEX dBISASQL/I_ICBOT100_CODGEN
ON dBISASQL/ICBOT100 (I100CODGEN ASC)

CREATE INDEX dBISASQL/I_ICBOT101_UC
ON dBISASQL/ICBOT101 (I101I104UC ASC)

CREATE INDEX dBISASQL/I_ICBOT102_OP
ON dBISASQL/ICBOT102 (I102I101OP ASC)

CREATE INDEX dBISASQL/I_ICBOT103_CODSOC
ON dBISASQL/ICBOT103 (I103CODSOC ASC)

CREATE INDEX dBISASQL/I_ICBOT104_US
ON dBISASQL/ICBOT104 (I104I103US ASC)

CREATE INDEX dBISASQL/I_ICBOT104_CL
ON dBISASQL/ICBOT104 (I104I100CL ASC)

CREATE INDEX dBISASQL/I_GEN01_TIP
ON dBISASQL/GEN01 (G01TIPAGE ASC)

CREATE INDEX dBISASQL/I_ICBOT111_SESSID
ON dBISASQL/ICBOT111 (I111SESSID ASC)

CREATE INDEX dBISASQL/I_ICBOT110_SES
ON dBISASQL/ICBOT110 (I110SES ASC)



