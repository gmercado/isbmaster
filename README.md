
** New Features

** Development changes
While testing, is necessary to set up VM options as follows:
-ea -javaagent:D:/openjpa-2.4.0.jar








** Warnings
i. Both SOAP and REST configuration servlets are Singleton, maybe will be necessary to instantiate'em and inject'em directly in
WebServiceModule, instead of using serve

** TODO
i. Namespaces are not applied to qualify WS operations.






** Changes
- New JDBC features. The following classes have implemented the required just throwing the UnsupportedOperationException
PerfStatement
PerfCallableStatement
PerfResultSet
CamioAmbienteDataSource
PerfConnection
PerfDataSource

- New API version. Changes in Apache HTTPClient ssl socket factory.
ConsumidorServiciosWeb#newSocket

- Wicket 7, New Features implemented. Renderer
indo/plumbing/components/MapChoiceRenderer#getObject
indo/plumbing/api/RolesBisa$StringIChoiceRenderer
indo/plumbing/utils/Monedas#choiceRenderer
indo/hallazgo/ui/params/ListaSubGrupoParametroFiltroPanel#

- Wicket 7, input types
indo/plumbing/components/Html5DateTextField, method removed

- Wicket 7, PropertyColumn
indo/plumbing/components/TimeStampPropertyColumn, method removed

- Wicket 7, AjaxFormValidatingBehavior
indo/logging/ui/LogAdmin$LevelDropDownPanel

- JDK 7+, Closeable
indo/logging/ui/SeguirElLogPanel, SOme closeable resource might be enchanced using the try-with-resources statement