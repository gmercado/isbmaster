package org.mvp.labs.openjpa;

import com.google.common.io.Resources;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import bus.database.jpa.BisaDictionary;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.StringUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;
import java.net.URL;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Charsets.US_ASCII;
import static com.google.common.io.Resources.getResource;

/**
 * @author Miguel Vega
 * @version $Id: SecurityServiceTest.java 0, 2015-09-22 11:22 PM mvega $
 * @see http://www.ibm.com/developerworks/data/library/techarticle/dm-1501unittest-sql-db2/index.html
 */
public class ManyToManyTest {

    static BasicDataSource memDS;

    private Injector injector;

    @Inject
    private EntityManagerFactory emf;

    final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @BeforeClass
    public static void init() throws Exception {
        memDS = new BasicDataSource();
        memDS.setUrl("jdbc:h2:mem:cerrarcamaras");
        memDS.setDriverClassName("org.h2.Driver");

        Connection connection = memDS.getConnection();
        URL sql = getResource(ManyToManyTest.class, "ddl.sql");
        Statement statement = connection.createStatement();
        statement.execute(Resources.toString(sql, US_ASCII));
        statement.close();
        connection.close();

        /*
        URL resource = getResource(ManyToManyTest.class, "data.xml");
        FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);
        IDatabaseTester databaseTester = new DataSourceDatabaseTester(memDS);
        databaseTester.setDataSet(build);
        IOperationListener defaultOperationListener = new DefaultOperationListener();
        IDatabaseConnection databaseConnection = databaseTester.getConnection();
        defaultOperationListener.connectionRetrieved(databaseConnection);
        databaseConnection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new H2DataTypeFactory());
        DatabaseOperation.CLEAN_INSERT.execute(databaseConnection, build);
        defaultOperationListener.operationSetUpFinished(databaseConnection);
        databaseConnection.close();
        */
    }

    @AfterClass
    public static void des() throws SQLException {
        memDS.close();
    }

    @Before
    public void setup(){
        injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                final EntityManagerFactory emf;
                String pu = "testPU";

                Map<String, Object> properties = new HashMap<String, Object>();
                properties.put("openjpa.Log", "log4j");
                properties.put("openjpa.ConnectionFactory", memDS);
                properties.put("openjpa.LockTimeout", 10000);
                properties.put("openjpa.jdbc.TransactionIsolation", "read-committed");
                properties.put("openjpa.BrokerImpl", "non-finalizing");

                Connection connection = null;
                try {
                    connection = memDS.getConnection();
                    String databaseProductName = connection.getMetaData().getDatabaseProductName();
                    LOGGER.info("La base a la que me conecto para el Persistence unit '{}' responde '{}'", pu, databaseProductName);
                    if (StringUtils.contains(databaseProductName, "DB2") && StringUtils.contains(databaseProductName, "AS")) {
                        properties.put("openjpa.jdbc.DBDictionary", BisaDictionary.class.getName());
                        LOGGER.info("Utilizando el dialecto BisaDictionary (AS/400 modificado) para la conexion {}",
                                getClass().getName());
                    }
                } catch (SQLException e) {
                    LOGGER.error("Error conectar la base de datos referida en el persistence unit " + pu, e);
                } finally {
                    try {
                        if (connection != null) {
                            connection.close();
                        }
                    } catch (SQLException e) {
                        LOGGER.warn("Error al cerrar la conexion a la base de datos", e);
                    }
                }

                properties.put("openjpa.jdbc.QuerySQLCache", "false");
                properties.put("openjpa.DataCache", "false");
                properties.put("openjpa.QueryCache", "false");
                properties.put("openjpa.RemoteCommitProvider", "sjvm");

                emf = Persistence.createEntityManagerFactory(pu, properties);

//                bind(DataSource.class).toProvider(() -> memDS).in(SINGLETON);
//                bind(EntityManagerFactory.class).toProvider(() -> emf).in(SINGLETON);
                bind(DataSource.class).toInstance(memDS);
                bind(EntityManagerFactory.class).toInstance(emf);
            }
        });

        injector.injectMembers(this);
    }

    @Test
    public void testCompanyWithClient1(){
        EntityManager em = emf.createEntityManager();

        // Start a transaction
        em.getTransaction().begin();
        // ------------

        CompanyEntity company = createTestCompany();
        ClientEntity client = createTestClient();
        em.persist(company);
        em.persist(client);
        em.flush();

        company.getClientCollection().add(client);

        em.merge(company);
        em.flush();

        // Commit the Transaction
        em.getTransaction().commit();

    }



    private CompanyEntity createTestCompany(){
        CompanyEntity c = new CompanyEntity();
        c.setName("JBay Solutions");
        c.setAddress("Av. 5 October, Lisbon");
        return c;
    }

    private ClientEntity createTestClient(){
        ClientEntity c = new ClientEntity();
        c.setName("Rui");
        c.setAddress("Home address");
        return c;
    }
}