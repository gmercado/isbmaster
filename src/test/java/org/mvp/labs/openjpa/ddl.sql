--http://blog.jbaysolutions.com/2012/12/17/jpa-2-relationships-many-to-many/

CREATE TABLE company (
 idcompany INT NOT NULL AUTO_INCREMENT,
 name VARCHAR(45) NOT NULL ,
 address VARCHAR(45) NOT NULL ,
 PRIMARY KEY (idcompany)
);
CREATE TABLE client (
 idclient INT NOT NULL AUTO_INCREMENT ,
 name VARCHAR(45) NOT NULL ,
 address VARCHAR(45) NULL ,
 PRIMARY KEY (idclient)
);

CREATE TABLE IF NOT EXISTS company_client (
 company_idcompany INT NOT NULL ,
 client_idclient INT NOT NULL ,
 PRIMARY KEY (company_idcompany, client_idclient) ,
);