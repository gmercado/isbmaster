package org.mvp.labs;

import com.google.common.base.Splitter;

import java.io.*;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: Runner.java; jul 15, 2016 04:15 PM mvega $
 */
public class Runner {
    public static void main(String[] args) throws IOException {

        String APP_NAME = "Aqua,";
        File file = new File("D:\\miguelvega\\downloads\\160715/allfiles.csv");

//        APP_NAME = "E-Bisa,";
//        file = new File("D:\\miguelvega\\downloads\\160715/allfiles.csv");

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

        PrintWriter writer = new PrintWriter(new FileOutputStream("D:\\miguelvega\\downloads\\160715/finals.csv"));

        final List<String> excludeStart = Arrays.asList(
                "/opt/Integracion/eBisa.ear/SvcStd75/Std_TempFile",
                "/opt/Integracion/eBisa.ear/SvcPerInf75/Bisa_TempAtach",
                "/opt/Integracion/eBisa.ear/SvcStd75/SvcPerInf75/Bisa_TempAtach",
                "/opt/Integracion/eBisa.ear/LogErrorService/ProactiveMessages",
                "/opt/Integracion/eBisa.ear/LogErrorService",
                "/opt/Integracion/eBisa.ear/Archivos");
        final List<String> excludeEnd = Arrays.asList();
        final List<String> excludeEquals = Arrays.asList();
        final List<String> excludeLength = Arrays.asList();

        String line = null;

        writer.println("Aplicacion,Directorio,Archivo,Fecha modificacion");

        while((line = reader.readLine())!=null){
            final Iterator<String> split = Splitter.on(",").split(line).iterator();

            final String fileName = split.next();
            final String filePerms = split.next();
            final String fileDate = split.next();

            if(excludeEquals.stream().filter(s -> fileName.equals(s)).count()>0)
                continue;

            if(excludeStart.stream().filter(s -> fileName.startsWith(s)).count()>0)
                continue;

            if(excludeEnd.stream().filter(s -> fileName.endsWith(s)).count()>0)
                continue;

            File tmp = new File(fileName);

            final String feed = APP_NAME + tmp.getParent().replace("\\", "/") + "," + tmp.getName() + "," + fileDate;
            writer.println(feed);
            System.out.println(feed);
        }

        reader.close();
    }
}
