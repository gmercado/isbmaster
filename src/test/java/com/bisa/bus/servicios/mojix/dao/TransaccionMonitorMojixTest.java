package com.bisa.bus.servicios.mojix.dao;

import bus.database.model.AmbienteActual;
import bus.env.api.MedioAmbiente;
import bus.monitor.MonitorModule;
import bus.monitor.api.*;
import bus.monitor.as400.AS400Factory;
import bus.plumbing.utils.Shutdowner;
import com.bisa.isb.api.jpa.DataSourceProvider;
import com.google.inject.AbstractModule;
import com.ibm.as400.access.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.math.BigDecimal;

import static bus.env.api.Variables.*;
import static org.mockito.Mockito.when;

/**
 * Created by josanchez on 21/07/2016.
 */
public class TransaccionMonitorMojixTest extends DataSourceProvider {

    @Before
    public void init() {
        initialize(new MonitorModule(), new AbstractModule() {
            @Override
            protected void configure() {
                bind(MedioAmbiente.class).toInstance(Mockito.mock(MedioAmbiente.class));
                bind(Shutdowner.class).toInstance(Mockito.mock(Shutdowner.class));
            }

        });

    }

    @Test
    public void testTransactionBuilding() throws IOException, SistemaCerradoException, ImposibleLeerRespuestaException, TransaccionEfectivaException {

        final AS400 as400 = new AS400();
        try {
            as400.setSystemName("bisades.grupobisa.net");
            as400.setUserId("EBANK411");
            as400.setPassword("bisaebank");
            //as400.setLocale(new Locale("es", "BO"));
            as400.setCcsid(284);

            //QSYSObjectPathName name = new QSYSObjectPathName(libreria, archivo, "FILE");
            //AS400FileRecordDescription description = new AS400FileRecordDescription(as400, name.getPath());
            //rf = description.retrieveRecordFormat()[0];

            MedioAmbiente mock = Mockito.mock(MedioAmbiente.class);

            when(mock.getValorDe(LIBRERIA_FORMATOS_MONITOR, LIBRERIA_FORMATOS_MONITOR_DEFAULT))
                    .thenReturn("SICBSBISA");
            AS400Factory factory = new AS400Factory(mock, Mockito.mock(Shutdowner.class)){
                public AS400 getAS400(int service) throws ConnectionPoolException {
                    return as400;
                }
            };
            when(mock.getValorDe(LIBRERIA_COLA_IN, LIBRERIA_COLA_IN_DEFAULT))
                    .thenReturn(LIBRERIA_COLA_IN_DEFAULT);
            when(mock.getValorDe(LIBRERIA_COLAS_MONITOR, LIBRERIA_COLAS_MONITOR_DEFAULT))
                    .thenReturn("SBNKPRD01");


            String bisaCliente = "1285";


            AmbienteActual actual = Mockito.mock(AmbienteActual.class);
            when(actual.isOffline()).thenReturn(false);

            TransaccionMonitorAuditable monitor =
                    new TransaccionMonitorAuditable(factory, actual, null, null, mock, null);



            monitor.setDatoCabeceraEnvio("WVER", 1);
            monitor.setDatoCabeceraEnvio("WCANAL", "5");


            monitor.setDatoCabeceraEnvio("WDEVICE", "MOX0123456");
            //monitor.setDatoCabeceraEnvio("WCANAL", "6");
////
            monitor.setDatoCabeceraEnvio("WNUMUSR", 5101);
            monitor.setDatoCabeceraEnvio("WAPRUSER", "EBANK411");
            monitor.setDatoCabeceraEnvio("WVERIF", "S");
            monitor.setDatoCabeceraEnvio("WTPODTA", 1);
//
//                /////////////////
            monitor.setDatoCabeceraEnvio("WCODTRN", "IC03");
            monitor.setCuerpo1EnvioFormat("PSR9011");
            monitor.setCuerpo1RecepcionFormat("PSR9114");

            monitor.setDatoCuerpo1("WLRGREG", new BigDecimal(200));
            monitor.setDatoCuerpo1("WCUENTA", new BigDecimal("0000454055"));
            monitor.setDatoCuerpo1("WTPOCTA", "I");
            //monitor.setDatoCuerpo1("WCODMON", new BigDecimal(""));
            monitor.setDatoCuerpo1("WPAG", new BigDecimal(0));
            monitor.setDatoCuerpo1("WCNT", new BigDecimal(0));
            monitor.setDatoCuerpo1("WFLG", "t");



            monitor.ejecutar();
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
    }




}