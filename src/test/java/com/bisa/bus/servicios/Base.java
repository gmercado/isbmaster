package com.bisa.bus.servicios;

import bus.cache.CacheModule;
import bus.database.jpa.BisaDictionary;
import bus.database.model.BasePrincipal;
import bus.database.model.BaseSinCambioAmbiente;
import com.bisa.bus.servicios.segip.dao.PersonaSegipDao;
import com.bisa.indicadoresbcb.dao.CotizacionBolsinDao;
import com.bisa.isb.api.jpa.SafeEntityManagerFactory;
import com.bisa.isb.ws.security.AuthSecurityService;
import com.bisa.isb.ws.security.AuthSecurityServiceImpl;
import com.bisa.isb.ws.security.dao.AuthWebServiceDao;
import com.google.inject.*;
import net.sf.ehcache.Ehcache;
import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Base {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private BasicDataSource memDS;

    protected Injector getInjector() {
        return Guice.createInjector(
                new AbstractModule() {
                    @Override
                    protected void configure() {

                        memDS = new BasicDataSource();
                        memDS.setUrl("jdbc:as400://bisads8.grupobisa.net/;prompt=false;naming=system;libraries=sbisasql sbnkprd01 sicbsbisa starjeta zpicbssms asid113001 sbisalib stablas;query storage limit=-1;hold statements=true");
                        memDS.setUsername("AQUA411S");
                        memDS.setPassword("aqua1aqua");
                        memDS.setDriverClassName("com.ibm.as400.access.AS400JDBCDriver");

                        bind(EntityManagerFactory.class).annotatedWith(BasePrincipal.class).toProvider(
                                new Provider<EntityManagerFactory>() {
                                    @Override
                                    public EntityManagerFactory get() {

                                        String persistencUnit = "principal";

                                        final EntityManagerFactory emf;

                                        Map<String, Object> properties = new HashMap<String, Object>();
//                properties.put("openjpa.Log", "log4j");
                                        properties.put("openjpa.Log", "DefaultLevel=WARN, Runtime=INFO, Tool=INFO, SQL=TRACE");
                                        properties.put("openjpa.ConnectionFactory", memDS);
                                        properties.put("openjpa.LockTimeout", 10000);
                                        properties.put("openjpa.jdbc.TransactionIsolation", "read-committed");
                                        properties.put("openjpa.BrokerImpl", "non-finalizing");

                                        Connection connection = null;
                                        try {
                                            connection = memDS.getConnection();
                                            String databaseProductName = connection.getMetaData().getDatabaseProductName();
                                            logger.info("La base a la que me conecto para el Persistence unit '{}' responde '{}'",
                                                    persistencUnit, databaseProductName);
                                            if (StringUtils.contains(databaseProductName, "DB2") && StringUtils.contains(databaseProductName, "AS")) {
                                                properties.put("openjpa.jdbc.DBDictionary", BisaDictionary.class.getName());
                                                logger.info("Utilizando el dialecto BisaDictionary (AS/400 modificado) para la conexion {}",
                                                        getClass().getName());
                                            }
                                        } catch (SQLException e) {
                                            logger.error("Error conectar la base de datos referida en el persistence unit " +
                                                    persistencUnit, e);
                                        } finally {
                                            try {
                                                if (connection != null) {
                                                    connection.close();
                                                }
                                            } catch (SQLException e) {
                                                logger.warn("Error al cerrar la conexion a la base de datos", e);
                                            }
                                        }

                                        properties.put("openjpa.jdbc.QuerySQLCache", "false");
                                        properties.put("openjpa.DataCache", "false");
                                        properties.put("openjpa.QueryCache", "false");
                                        properties.put("openjpa.RemoteCommitProvider", "sjvm");

                                        //http://stackoverflow.com/questions/2453671/nullable-date-column-merge-problem
//                properties.put("openjpa.DetachState", "loaded(DetachedStateField=true)");
//                properties.put("openjpa.DetachState", "fetch-groups");
                                        properties.put("openjpa.DetachState", "fetch-groups(DetachedStateField=true)");
                                        properties.put("openjpa.Log", "DefaultLevel=WARN, Runtime=INFO, Tool=INFO, SQL=TRACE");
//                properties.put("openjpa.DetachState", "fetch-groups(DetachedStateField=false)");
//                properties.put("openjpa.AutoDetach", "commit");

                                        emf = Persistence.createEntityManagerFactory(persistencUnit, properties);

                                        return emf;
                                    }
                                }
                        );

                        bind(DataSource.class).annotatedWith(BaseSinCambioAmbiente.class).toInstance(memDS);
                        bind(Configuration.class).toInstance(new BaseConfiguration());
                        bind(AuthSecurityService.class).to(AuthSecurityServiceImpl.class);
                        bind(AuthWebServiceDao.class);
                        bind(CotizacionBolsinDao.class);
                        bind(SafeEntityManagerFactory.class);
                        bind(Ehcache.class).toProvider(CacheModule.CacheProvider.class).in(Scopes.SINGLETON);
                        bind(PersonaSegipDao.class);
                    }
                }
        );
    }
}
