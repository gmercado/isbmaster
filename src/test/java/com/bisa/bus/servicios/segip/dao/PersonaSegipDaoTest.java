package com.bisa.bus.servicios.segip.dao;

import bus.interfaces.as400.dao.TipoAtt;
import bus.interfaces.as400.dao.ValorAttDao;
import bus.plumbing.utils.Convert;
import com.bisa.bus.servicios.Base;
import com.bisa.bus.servicios.segip.entities.Cliente;
import com.bisa.bus.servicios.segip.entities.ClienteNatural;
import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author rsalvatierra on 24/03/2016.
 */
public class PersonaSegipDaoTest extends Base {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    private PersonaSegipDao personaSegipDao;

    @Inject
    private ClienteDao clienteDao;

    @Inject
    private ClienteNaturalDao clienteNaturalDao;

    @Inject
    private ValorAttDao paramDao;


    @Before
    public void init() {
        Injector injector = getInjector();
        injector.injectMembers(this);
    }

    @Test
    public void testQuery() {
        int contar = personaSegipDao.contar();
        logger.info("Count = {}", contar);
        PersonaSegip s = new PersonaSegip();
        s.setId(1L);
        PersonaSegip s2 = new PersonaSegip();
        s.setId(2L);
        List<PersonaSegip> lista = personaSegipDao.getTodos(s, s2, null, 1, 2);
        for (PersonaSegip segip : lista) {
            logger.info("* {}", segip.getNombres());
            logger.info("* {}", segip.getId());
        }
       /* logger.info("Count = {}", lista.size());
        PersonaSegipHist sh=new PersonaSegipHist();
        sh.setId(10L);
        List<PersonaSegipHist> listaH = personaSegipHistDao.getTodos(sh,sh,null,1,10);
        for (PersonaSegipHist segip:listaH){
            logger.info("* {}", segip.getIdHist());
            logger.info("* {}", segip.getId());
            logger.info("* {}", segip.getNombres());
        }*/
    }

    @Test
    public void testQuery2() {
        Cliente cliente = clienteDao.getCliente("6766979");
        if (cliente != null) {
            System.out.println(cliente);
            System.out.println(paramDao.getValor(TipoAtt.ESTADO_CIVIL, cliente.getEstadoCivil()).getDescripcion());
            System.out.println(paramDao.getValor(TipoAtt.PROFESION, StringUtils.leftPad(cliente.getProfesion(), 3, '0')).getDescripcion());
            System.out.println(Convert.fromJulian(cliente.getFechaNacimiento().toString()));
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getId());
            if (clienteNatural != null) {
                System.out.println(clienteNatural);
            }
        }
    }

}