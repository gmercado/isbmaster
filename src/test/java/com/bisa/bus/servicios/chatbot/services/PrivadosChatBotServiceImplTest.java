package com.bisa.bus.servicios.chatbot.services;

import com.bisa.bus.servicios.Base;
import com.bisa.bus.servicios.chatbot.services.types.JsonRequestEstadoCuenta;
import com.bisa.bus.servicios.chatbot.services.types.JsonResponseComun;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PrivadosChatBotServiceImplTest extends Base {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    private PrivadosChatBotService privadosChatBotService;


    @Before
    public void init() {
        Injector injector = getInjector();
        injector.injectMembers(this);
    }

    @Test
    public void solicitaEstadoCuenta() {
        JsonRequestEstadoCuenta requestEstadoCuenta = new JsonRequestEstadoCuenta();
        requestEstadoCuenta.setCodCliente("3kpGn8eZDO3gTA6Fn5Vn4Ih1sz6sAEl/TDiOCPTlIbHzRKAhkKQjbQ==");
        requestEstadoCuenta.setCodCuenta("J9Xb4LmtRoYe3QNLB86iUg==");
        requestEstadoCuenta.setTipo("ca");
        JsonResponseComun responseComun = privadosChatBotService.solicitaEstadoCuenta(requestEstadoCuenta, "mojix");
        logger.info("responseComun -> {}", responseComun);
    }
}