package com.bisa.bus.servicios.sin.ciclos;

import bus.plumbing.compress.Compress7z;
import com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.tempuri.ISinFacCiclosService;
import com.bisa.bus.servicios.sin.ciclos.consumer.wsdl.Dummy;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.ClientImpl;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.cxf.service.model.*;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.ConnectionType;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import java.net.URL;
import java.util.List;

/**
 * @author by rsalvatierra on 31/01/2017.
 */
public class TestSin {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Before
    public void init() {
        System.setProperty("http.proxyHost", "navegar.grupobisa.net");
        System.setProperty("http.proxyPort", String.valueOf("8080"));
        System.setProperty("http.nonProxyHosts", "localhost");
    }

    @Test
    public void Test1() {
        logger.info("Test");
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.getInInterceptors().add(new LoggingInInterceptor());
        factory.getOutInterceptors().add(new LoggingOutInterceptor());
        factory.setAddress("http://localhost:51335/SinFacCiclosService.svc");
        ISinFacCiclosService port = factory.create(ISinFacCiclosService.class);
        Client client = ClientProxy.getClient(port);
        HTTPConduit http = (HTTPConduit) client.getConduit();
        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
        httpClientPolicy.setConnectionTimeout(36000);
        httpClientPolicy.setAllowChunking(false);
        httpClientPolicy.setConnection(ConnectionType.KEEP_ALIVE);
        http.setClient(httpClientPolicy);
        logger.info(port.ping());
    }

    @Test
    public void Test2() throws Exception {
        logger.info("Test");
        final QName SERVICE_NAME
                = new QName("http://tempuri.org/", "SinFacCiclosService");

        URL wsdlURL = Dummy.class.getResource("FacturacionCiclos.wsdl");
        System.out.println(wsdlURL);

        JaxWsDynamicClientFactory factory = JaxWsDynamicClientFactory.newInstance();
        Client client = factory.createClient(wsdlURL.toExternalForm(), SERVICE_NAME);
        ClientImpl clientImpl = (ClientImpl) client;
        Endpoint endpoint = clientImpl.getEndpoint();
        ServiceInfo serviceInfo = endpoint.getService().getServiceInfos().get(0);
        QName bindingName = new QName("http://tempuri.org/",
                "BasicHttpBinding_ISinFacCiclosService");
        BindingInfo binding = serviceInfo.getBinding(bindingName);
        //{
        QName opName = new QName("http://tempuri.org/", "ping");
        BindingOperationInfo boi = binding.getOperation(opName);
        BindingMessageInfo inputMessageInfo = boi.getInput();
        List<MessagePartInfo> parts = inputMessageInfo.getMessageParts();
        // only one part.
        MessagePartInfo partInfo = parts.get(0);
        Class<?> partClass = partInfo.getTypeClass();
        System.out.println(partClass.getCanonicalName()); // GetAgentDetails
        Object inputObject = partClass.newInstance();
        // Unfortunately, the slot inside of the part object is also called 'part'.
        // this is the descriptor for get/set part inside the GetAgentDetails class.
        /*PropertyDescriptor partPropertyDescriptor = new PropertyDescriptor("part", partClass);
        // This is the type of the class which really contains all the parameter information.
        Class<?> partPropType = partPropertyDescriptor.getPropertyType(); // AgentWSRequest
        System.out.println(partPropType.getCanonicalName());
        Object inputPartObject = partPropType.newInstance();
        partPropertyDescriptor.getWriteMethod().invoke(inputObject, inputPartObject);*/
        /*PropertyDescriptor numberPropertyDescriptor = new PropertyDescriptor("agentNumber", partPropType);
        numberPropertyDescriptor.getWriteMethod().invoke(inputPartObject, new Integer(314159));*/

        Object[] result = client.invoke(opName, inputObject);

        Class<?> resultClass = result[0].getClass();
        System.out.println(resultClass.getCanonicalName()); // GetAgentDetailsResponse
        logger.info("{}", result[0]);
        /*PropertyDescriptor resultDescriptor = new PropertyDescriptor("PingResponse", resultClass);
        Object wsResponse = resultDescriptor.getReadMethod().invoke(result[0]);
        Class<?> wsResponseClass = wsResponse.getClass();
        System.out.println(wsResponseClass.getCanonicalName());
        PropertyDescriptor agentNameDescriptor = new PropertyDescriptor("pingResult", wsResponseClass);
        String agentName = (String)agentNameDescriptor.getReadMethod().invoke(wsResponse);
        System.out.println("Agent name: " + agentName);*/

    }

    @Test
    public void Test3() {
        new Compress7z().compress("compress.7z", null);
    }
}
