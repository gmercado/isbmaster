package com.bisa.bus.servicios.asfi.infocred.consumer.stub;

import bus.env.api.Variables;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by gmercado on 07/02/2017.
 */
public class ImportarPorFtpTest {

    @Test
    public void init() {

        FTPClient ftpClient = new FTPClient();
        String url = Variables.INFOCRED_URL_FTP_DEFAULT;
        int port = Variables.INFOCRED_PUERTO_FTP_DEFAULT;
        String user = Variables.INFOCRED_USUARIO_FTP_DEFAULT;
        String pass = Variables.INFOCRED_PASSWORD_FTP_DEFAULT;

        try {
            ftpClient.connect(url, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            String a = ftpClient.printWorkingDirectory();
            System.out.println("======== " + a);

            for (String o : ftpClient.listNames()) {
                System.out.println(" == " + o);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Desconectando");
            try {
                if (ftpClient != null) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //@Test
    public void unZipIt() {

//        File temp = null;
//        InputStream is = null;
//        OutputStream outputStream = null;
//        try {
//            ZipFile zipFile = new ZipFile(new File("D:\\InfoCred_Docs\\InfoCred\\ARCHIVOS\\BIS 201609.zip"));
//
//            Enumeration<?> enu = zipFile.entries();
//            while (enu.hasMoreElements()) {
//                ZipEntry zipEntry = (ZipEntry) enu.nextElement();
//
//                String name = zipEntry.getName();
//                long size = zipEntry.getSize();
//                long compressedSize = zipEntry.getCompressedSize();
//                System.out.printf("name: %-20s | size: %6d | compressed size: %6d\n", name, size, compressedSize);
//
//                File file = new File(name);
//                if (name.endsWith("/")) {
//                    file.mkdirs();
//                    continue;
//                }
//
//                is = zipFile.getInputStream(zipEntry);
//                temp = File.createTempFile("BISA", ".txt");
//
//                outputStream = new FileOutputStream(temp);
//                int read = 0;
//                byte[] bytes = new byte[1024];
//                while ((read = is.read(bytes)) != -1) {
//                    outputStream.write(bytes, 0, read);
//                }
//            }
//            zipFile.close();
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        } finally {
//            if (is != null) {
//                try {
//                    is.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (outputStream != null) {
//                try {
//                    outputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
    }
}
