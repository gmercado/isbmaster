package com.bisa.bus.servicios.asfi.infocred.consumer.stub;

import org.apache.ws.security.WSPasswordCallback;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Punk
 * Date: 1/7/17
 * Time: 3:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClientCallback implements CallbackHandler {
    private String user;
    private String pass;
    public ClientCallback(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        for (Callback callback : callbacks) {
            WSPasswordCallback pc = (WSPasswordCallback) callback;
            if (user.equals(pc.getIdentifier())) {
                pc.setPassword(pass);
            }
        }
    }
}
