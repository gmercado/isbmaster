package com.bisa.bus.servicios.asfi.infocred.consumer.stub;

import bus.consumoweb.infocred.utilitarios.LogInterceptorHandler;
import bus.consumoweb.infocred.utilitarios.WSSecurityHeaderSOAPHandler;
import bus.env.api.Variables;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.junit.Before;
import org.junit.Test;

import javax.net.ssl.*;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.*;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;

/**
 * @author Miguel Vega
 * @version $Id: BICServiceTest.java; ene 30, 2017 02:52 PM miguelvega $
 */
public class BICServiceTest {
    final String HOST = "z1pxy02.grupobisa.net";
//    final String HOST = "127.0.0.1";

    final String PORT = "8080";
    private boolean trustAll = false;

    private SSLSocketFactory getFancyFactory() throws KeyManagementException, NoSuchAlgorithmException {
        final SSLContext context = SSLContext.getInstance("TLS");

        TrustManagerFactory trustStoreManager = null;
        KeyManagerFactory keyeManagerFactory = null;

        if(!trustAll) {
            //Use an existing truststore
            KeyStore trustStore = null;
            try {
                trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
                trustStore.load(new FileInputStream("C:/Program Files/Java/jre1.8.0_45/lib/security/cacerts"), "changeit".toCharArray());
                trustStoreManager = TrustManagerFactory.getInstance("SunX509");
                trustStoreManager.init(trustStore);
            } catch (Throwable e) {
                throw new IllegalStateException("Ha ocurrido un eror inesperado", e);
            }
        }

        context.init(
        keyeManagerFactory!=null?keyeManagerFactory.getKeyManagers():null,
                trustStoreManager!=null?trustStoreManager.getTrustManagers():new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());

        return context.getSocketFactory();
    }

    @Before
    public void init() {
        Authenticator.setDefault(new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("gmercado", "Gm3rc4d0q".toCharArray());
            }
        });
        System.setProperty("http.proxyHost", HOST);
        System.setProperty("http.proxyPort", PORT);
        System.setProperty("https.proxyHost", HOST);
        System.setProperty("https.proxyPort", PORT);

        //TODO need to research more about the following line, meanwhile, use it
        //System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
    }

    @Test
    public void testSimpleInquiry() throws IOException {

        IIndividualReportServiceContract in = null;
        try {

            JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
            factory.setServiceClass(IIndividualReportServiceContract.class);
            factory.setAddress(Variables.INFOCRED_END_POINT_DEFAULT);
            IIndividualReportServiceContract port = (IIndividualReportServiceContract) factory.create();

            Client client = ClientProxy.getClient(port);
//            HTTPConduit http = (HTTPConduit) client.getConduit();
//
//            http.getAuthorization().setUserName("BISWEBSERVICE");
//            http.getAuthorization().setPassword("B1s$S3rv1c3");
//            Endpoint cxfEndpoint = client.getEndpoint();
//            Map<String,Object> outProps = new HashMap<String,Object>();
//            ClientCallback c = new ClientCallback("BISWEBSERVICE", "B1s$S3rv1c3");
//            outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
//            outProps.put(WSHandlerConstants.USER, "BISWEBSERVICE");
//            outProps.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
//            outProps.put(WSHandlerConstants.PW_CALLBACK_REF, c);

//            WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
//            cxfEndpoint.getOutInterceptors().add(wssOut);

            BindingProvider bindingProvider = ((BindingProvider) port);

            List<Handler> handlerChain = new ArrayList<Handler>();
            handlerChain.add(new WSSecurityHeaderSOAPHandler("BISWEBSERVICE", "B1s$S3rv1c3"));
            bindingProvider.getBinding().setHandlerChain(handlerChain);

            client.getOutInterceptors().add(new LogInterceptorHandler());
//            Map ctx = ((BindingProvider)in).getRequestContext();
//            ctx.put("ws-security.username", "BISWEBSERVICE");
//            ctx.put("ws-security.password", "B1s$S3rv1c3");
//            ctx.put("ws-security.callback-handler", PasswordCallback.class.getName());

//
//            Client client = ClientProxy.getClient(in);
//            HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
//            AuthorizationPolicy authorizationPolicy = new AuthorizationPolicy();
//            authorizationPolicy.setUserName("BISWEBSERVICE");
//            authorizationPolicy.setPassword("B1s$S3rv1c3");
//            authorizationPolicy.setAuthorizationType("Basic");
//            httpConduit.setAuthorization(authorizationPolicy);
//
//            Endpoint cxfEndpoint = client.getEndpoint();
//            cxfEndpoint.getOutInterceptors();

//            Properties p = new Properties();
//            p.setProperty("security.username","BISWEBSERVICE");
//            p.setProperty("security.password","B1s$S3rv1c3");
//
//            Map<String, Object> ctx = ((BindingProvider)in).getRequestContext();
//            ctx.put("security.encryption.properties", p);


//            Client client = ClientProxy.getClient(in);
//            Endpoint cxfEndpoint = client.getEndpoint();
//            Map<String,Object> outProps = new HashMap<>();
//            outProps.put("action", "UsernameToken");
//            outProps.put("user","BISWEBSERVICE");
//            //outProps.put("ws-security.password","B1s$S3rv1c3");
//            ClientCallback c = new ClientCallback();
//            outProps.put("passwordCallbackRef",c);
//
//            WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
//            cxfEndpoint.getOutInterceptors().add(wssOut);

//            Properties p = new Properties();
//            p.setProperty("usernameHandler","BISWEBSERVICE");
//            p.setProperty("passwordHandler","B1s$S3rv1c3");
//            Map<String, Object> ctx = ((BindingProvider)in).getRequestContext();
//            ctx.put("security.encryption.properties", p);
//
//            Client client = ClientProxy.getClient(in);
//            HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
//
//            final HTTPClientPolicy httpClientPolicy = httpConduit.getClient();
//            httpClientPolicy.setAllowChunking(false);
//            httpClientPolicy.setAutoRedirect(true);
//
//            final BindingProvider bindingProvider = (BindingProvider) in;
//            final Map<String, Object> requestContext = bindingProvider.getRequestContext();
//
//            final Credentials credentials = new NTCredentials("admin", "admin", "", DOMAIN);
//            requestContext.put(Credentials.class.getName(), credentials);
//            requestContext.put(AsyncHTTPConduit.USE_ASYNC, Boolean.TRUE);

            Usuario usuario = new Usuario();
            usuario.setNombreCompleto("gmercado");
            usuario.setDocumento("677777");

            Titular titular = new Titular();
            titular.setDocumento("445968");
            titular.setTipoDocumento(1001);
            titular.setNombreCompleto(null);

            String response = port.getIndividualReport4(titular, usuario);
            System.out.println("XXXXXX === " + response);
            //TitularInfoCred t = XmlUtil.xmlToObject(response, TitularInfoCred.class);
            //System.out.println("====SC=====>>>>> " + t.getDatosPersonales().getScore());
            //String response = in.getTitularListByName(usuario, "JHEYSON SANCHEZ");
            //System.out.println(" === " + response);
        } catch (WebServiceException e) {
            System.out.println("ERROR 1");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("ERROR 2");
            e.printStackTrace();
        } finally {
            in = null;
        }
    }
}