package com.bisa.isb.commtools;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.junit.Test;

import javax.mail.PasswordAuthentication;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * @author Miguel Vega
 * @version $Id: OutgoingEmailTest.java; ene 29, 2016 12:40 PM mvega $
 */
public class OutgoingMessageTest {

    @Test
    public void testReflection() throws Exception {
        final Method method = HtmlEmail.class.getMethod("setSmtpPort", int.class);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>"+method.getName());
    }

    @Test
    public void testSendFromEmail() throws Throwable {
        Settings config = new Settings();
        config.addParam(Value.of(Settings.MessageType, OutgoingHtmlMessage.class));
        config.addParam(Value.of(Settings.Authentication, new PasswordAuthentication("miguelvega.name@gmail.com", "Mavp8584")));
//        config.addParam(Value.of(Settings.Authenticator, new DefaultAuthenticator("miguelvega.name@gmail.com", "****")));
        config.addParam(Value.of(Settings.SmtpPort, 587));
        config.addParam(Value.of(Settings.HostName, "smtp.gmail.com"));
        config.addParam(Value.of(Settings.StartTlsEnabled, true));

        OutgoingMessage msg = OutgoingEmailFactory.configure(config);
        msg
                .from(new Sender("Miguel Vega", "miguelvega.name@gmail.com"))
                .to(new Receiver("", "mikevegap@gmail.com"))
                .withMessage("Hello")
                .send();
    }

    @Test
    @SuppressWarnings({"unchecked"})
    public void testSendPlainMessage() throws EmailException {

        Settings settings = new Settings();
        settings.addParam(Value.of(Settings.MessageType, OutgoingHtmlMessage.class));
        settings.addParam(Value.of(Settings.Authenticator, new DefaultAuthenticator("nobody", "nada")));
        settings.addParam(Value.of(Settings.HostName, "smtpinterno2.grupobisa.net"));

        final OutgoingMessage<EmailException> outgoing = OutgoingEmailFactory.configure(settings);

        outgoing.
                from(new Sender("Miguel Vega", "mavega@grupobisa.com")).
                to(new Receiver("Destinatario Name", "mavega@grupobisa.com")).
                withMessage("<b>Hello</b> <i>world</i> @ " + new Date().toString()).
                send();
    }
}