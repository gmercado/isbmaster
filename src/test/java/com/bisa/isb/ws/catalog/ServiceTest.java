package com.bisa.isb.ws.catalog;

import com.bisa.isb.ws.services.CurrencyService;
import junit.framework.TestCase;
import org.junit.Test;

import javax.xml.namespace.QName;

/**
 * @author Miguel Vega
 * @version $Id: ServiceTest.java; sep 28, 2015 03:22 PM mvega $
 */
public class ServiceTest extends TestCase {
    public void testSimpleService(){
        Service service = Service.of(new QName("http://stackoverflow.com/questions", "currencyService"), CurrencyService.class, Service.Type.SOAP);
        System.out.println("......" + service.getName().getNamespaceURI());
    }
}