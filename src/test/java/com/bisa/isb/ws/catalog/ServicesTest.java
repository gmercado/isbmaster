package com.bisa.isb.ws.catalog;

import com.bisa.isb.ws.ServiceEndpoint;
import com.bisa.isb.ws.services.*;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

import static com.bisa.isb.ws.catalog.Services.discoverService;

/**
 * @author Miguel Vega
 * @version $Id: ServicesTest.java; sep 29, 2015 09:33 AM mvega $
 */
public class ServicesTest {

    final ServiceEndpoint soapEndpoint = new ServiceEndpoint() {
        @Override
        public String getPublishPath() {
            return "/soap/*";
        }
    };

    final ServiceEndpoint restEndpoint = new ServiceEndpoint() {
        @Override
        public String getPublishPath() {
            return "/rest/*";
        }
    };

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testNamespace(){
        String nsURI = Services.getDefaultNamespaceURI(CurrencyServiceImpl.class);
        logger.info("NS URI={}", nsURI);
    }

    @Test
    public void testDiscoverSOAPService(){
        Service service = discoverService(soapEndpoint, CurrencyServiceImpl.class);

        logger.info("Service EndPoint: {}", service.getEndpoint());
        logger.info("Service Name: {}", service.getName().getLocalPart());
        logger.info("Service URI: {}", service.getName().getPrefix());
        logger.info("Service NameSpace: {}", service.getName().getNamespaceURI());
    }

    @Test
    public void testDiscoverRESTService(){
        Service service = discoverService(restEndpoint, OrderServiceImpl.class);

        logger.info("1... Service EndPoint: {}", service.getEndpoint());
        logger.info("Service Name: {}", service.getName().getLocalPart());
        logger.info("Service URI: {}", service.getName().getPrefix());
        logger.info("Service NameSpace: {}", service.getName().getNamespaceURI());

        service = discoverService(restEndpoint, BooksService.class);

        logger.info("2... Service EndPoint: {}", service.getEndpoint());
        logger.info("Service Name: {}", service.getName().getLocalPart());
        logger.info("Service URI: {}", service.getName().getPrefix());
        logger.info("Service NameSpace: {}", service.getName().getNamespaceURI());
    }

    @Test
    public void testDiscoverMethods(){
        Service service;
        Set<Operation> operations;
/*
        logger.info("Evaluating a REST service  * * * * * * * * * * * * *");
        service = discoverService(restEndpoint, BookService.class);
        operations = Services.discoverServiceOperations2(service);
        operations.forEach(op-> logger.info("REST.1.Op name = {}, Method={}", op.getName(), op.getMethod().getName()));

//        if(true)return;

        logger.info("Evaluating a REST service without path annotations on methods * * * * * * * * * * * * *");
        service = discoverService(restEndpoint, OrderServiceImpl.class);
        operations = Services.discoverServiceOperations2(service);
        operations.forEach(op-> logger.info("REST.2.Op name = {}, Method={}", op.getName(), op.getMethod().getName()));

        logger.info("Evaluating a SOAP service * * * * * * * * * * * * *");
        service = discoverService(soapEndpoint, CurrencyServiceImpl.class);
        operations = Services.discoverServiceOperations2(service);
        final Service finalService1 = service;
        operations.forEach(op-> logger.info("SOAP.Op name = {}, Method={}, NS={}",
                new Object[]{op.getName(), op.getMethod().getName(), finalService1.getName().getNamespaceURI()}));
*/

        logger.info("Evaluating a SOAP HELLO service * * * * * * * * * * * * *");
        service = discoverService(soapEndpoint, HelloService.class);
        operations = Services.discoverServiceOperations2(service);
        final Service finalService = service;
        operations.forEach(op-> logger.info("SOAP.Op name = {}, Method={}, OP.REMARK={}, WS.NS={}, WS.name={}, WS.TIPO={}, WS.DESC={}",
                        new Object[]{op.getName(), op.getMethod().getName(), op.getRemark(),
                                finalService.getName().getNamespaceURI(),
                                finalService.getName().getLocalPart(), finalService.getType(), finalService.getRemark()}
        ));

    }
}