package com.bisa.isb.ws.soap;

import com.bisa.isb.api.jpa.DataSourceProvider;
import com.bisa.isb.catalog.Catalog;
import com.bisa.isb.ws.ServiceEndpoint;
import com.bisa.isb.ws.catalog.Service;
import com.bisa.isb.ws.catalog.ServiceCatalog;
import com.bisa.isb.ws.catalog.Services;
import com.bisa.isb.ws.security.AuthSecurityService;
import com.bisa.isb.ws.security.AuthSecurityServiceTest;
import com.bisa.isb.ws.security.dao.WSPayloadDao;
import com.bisa.isb.ws.security.entities.*;
import com.bisa.isb.ws.services.CurrencyServiceImpl;
import com.bisa.isb.ws.services.HelloServiceImpl;
import com.bisa.isb.ws.support.AuthenticationException;
import com.bisa.isb.ws.support.AuthorizationException;
import com.bisa.isb.ws.support.HttpBasicAuthInterceptor;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import bus.cache.CacheModule;
import org.apache.http.auth.Credentials;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: BasicAuthorizationInterceptorTest.java 0, 2015-09-29 12:18 AM mvega $
 */
public class HttpBasicAuthInterceptor4RESTTest extends DataSourceProvider {

    final Logger logger = LoggerFactory.getLogger(getClass());
    @Inject
    private AuthSecurityService authSecurityService;

    @Inject
    private WSPayloadDao wsPayloadDao;

    @Before
    public void init() {

        super.initialize(
        new AbstractModule() {
                    @Override
                    protected void configure() {

                        bind(WSPayloadDao.class);

                        bind(AuthSecurityService.class).toProvider(() -> {
                            return new AuthSecurityService() {
                                @Override
                                public AuthUser applySecuredPassword(AuthUser authUser, String password) {
                                    return null;
                                }

                                @Override
                                public AuthUser getUserByName(Credentials credentials) throws AuthenticationException {
                                    if (credentials.getPassword().equals("admin") && credentials.getUserPrincipal().getName().equals("admin")) {
                                        AuthUser usr = new AuthUser();
                                        usr.setId(8584L);
                                        usr.setUserName(credentials.getUserPrincipal().getName());

                                        //load roles

                                        AuthWebService aws = service(HelloServiceImpl.class);
                                        AuthWebService currws = service(CurrencyServiceImpl.class);

//                                        List<AuthWebServiceOperation> operations = role.getOperations();
                                        usr.getRoles().add(newRole("hello", op("sayHello", aws), op("sayGoodbye", aws)));
                                        usr.getRoles().add(newRole("list curr", op("getCurrencies", currws)));

                                        return usr;
                                    }
                                    throw new AuthenticationException("You have provided invalid credentials");
                                }

                                @Override
                                public void populateCatalog(Catalog<Service> serviceCatalog, Service.Type type) {

                                }

                                @Override
                                public List<AuthWebService> getAuthServices() {
                                    return null;
                                }

                                @Override
                                public List<AuthUserIP> getAllIPAddresses(AuthUser authUser) {
                                    return null;
                                }

                                private AuthRole newRole(String name, AuthWebServiceOperation... ops) {
                                    AuthRole role = new AuthRole();
                                    role.setName(name);
                                    role.setEnabled("S");

                                    Arrays.stream(ops).forEach(op -> role.getOperations().add(op));

                                    return role;
                                }

                                private AuthWebServiceOperation op(String opname, AuthWebService aws) {
                                    AuthWebServiceOperation op = new AuthWebServiceOperation();
                                    op.setName(opname);
                                    op.setJavaMethodName(opname);
                                    op.setEnabled("S");

                                    op.setWebService(aws);

                                    return op;
                                }

                                private AuthWebService service(Class endpoint) {

                                    QName qname = Services.getServiceWeakReference(endpoint).getLeft();

                                    AuthWebService ws = new AuthWebService();
                                    ws.setName(qname.getLocalPart());
                                    ws.setEnabled("S");
                                    ws.setNameSpace(qname.getNamespaceURI());
                                    ws.setJavaTypeName(endpoint.getName());
                                    return ws;
                                }
                            };
                        });
                    }
                }
                , new CacheModule()
        );

        injector.injectMembers(this);
    }

    @Test
    public void testAuthorization() throws AuthenticationException, NoSuchMethodException, AuthorizationException {

        ServiceEndpoint soap = new ServiceEndpoint() {
            @Override
            public String getPublishPath() {
                return "/soap/*";
            }
        };

        Catalog<Service> serviceCatalog = new ServiceCatalog();
        serviceCatalog.add(Services.discoverService(soap, HelloServiceImpl.class));
        serviceCatalog.add(Services.discoverService(soap, CurrencyServiceImpl.class));

        HttpBasicAuthInterceptor interceptor = new HttpBasicAuthInterceptor(authSecurityService, serviceCatalog,
                wsPayloadDao);

        AuthUser authUser = authSecurityService.getUserByName(new AuthSecurityServiceTest.Creds("admin", "admin"));

        Method method = HelloServiceImpl.class.getDeclaredMethod("sayHello", String.class);
        //AuthWebServiceOperation wsOp = interceptor.getWSOperation(authUser, HelloServiceImpl.class, method);

        //logger.info("Operation: {}", wsOp);
    }
}