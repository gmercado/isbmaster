package com.bisa.isb.ws.soap;

import com.bisa.isb.api.net.Subnet;
import com.google.common.net.InetAddresses;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author Miguel Vega
 * @version $Id: WSDLAuthenticationInterceptorTest.java; oct 06, 2015 09:14 AM mvega $
 */
public class WSDLAuthenticationInterceptorTest {
    @Test
    public void testInetAddress() throws UnknownHostException {
        Subnet subnet = Subnet.forString("172.21.0.0/24");

        InetAddress toTest = InetAddresses.forString("172.21.0.1");

        boolean inNet = subnet.isInNet(toTest);

        System.out.println("..."+subnet.toString()+", OK?"+inNet);
    }
}