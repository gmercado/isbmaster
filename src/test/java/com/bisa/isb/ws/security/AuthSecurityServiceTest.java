package com.bisa.isb.ws.security;

import bus.cache.CacheModule;
import bus.database.jpa.BisaDictionary;
import bus.database.model.BasePrincipal;
import bus.database.model.BaseSinCambioAmbiente;
import com.bisa.isb.api.jpa.SafeEntityManagerFactory;
import com.bisa.isb.api.net.Subnet;
import com.bisa.isb.api.security.PasswordHash;
import com.bisa.isb.catalog.Catalog;
import com.bisa.isb.ws.ServiceEndpoint;
import com.bisa.isb.ws.catalog.Service;
import com.bisa.isb.ws.catalog.ServiceCatalog;
import com.bisa.isb.ws.catalog.Services;
import com.bisa.isb.ws.security.dao.AuthWebServiceDao;
import com.bisa.isb.ws.security.entities.*;
import com.bisa.isb.ws.services.HelloServiceImpl;
import com.bisa.isb.ws.support.AuthenticationException;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.io.Resources;
import com.google.common.net.InetAddresses;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.StringUtils;
import org.apache.http.auth.Credentials;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.DefaultOperationListener;
import org.dbunit.IDatabaseTester;
import org.dbunit.IOperationListener;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.h2.H2DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;
import java.net.InetAddress;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.spec.InvalidKeySpecException;
import java.sql.*;
import java.util.*;

import static com.google.common.base.Charsets.US_ASCII;
import static com.google.common.io.Resources.getResource;
import static org.junit.Assert.assertTrue;

/**
 * @author Miguel Vega
 * @version $Id: SecurityServiceTest.java 0, 2015-09-22 11:22 PM mvega $
 * @see http://www.ibm.com/developerworks/data/library/techarticle/dm-1501unittest-sql-db2/index.html
 */
public class AuthSecurityServiceTest {

    static BasicDataSource memDS;

    private Injector injector;

    @Inject
    @BasePrincipal
    private EntityManagerFactory emf;

    @Inject
    private AuthSecurityService authSecurityService;

    @Inject
    private AuthWebServiceDao webServiceDao;

    final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @BeforeClass
    public static void init() throws Exception {
        memDS = new BasicDataSource();
        memDS.setUrl("jdbc:h2:mem:cerrarcamaras");
        memDS.setDriverClassName("org.h2.Driver");

        Connection connection = memDS.getConnection();
        URL sql = getResource(AuthSecurityService.class, "auth-ddl.sql");
        Statement statement = connection.createStatement();
        statement.execute(Resources.toString(sql, US_ASCII));
        statement.close();
        connection.close();

        URL resource = getResource(AuthSecurityService.class, "auth-data.xml");
        FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);
        IDatabaseTester databaseTester = new DataSourceDatabaseTester(memDS);
        databaseTester.setDataSet(build);
        IOperationListener defaultOperationListener = new DefaultOperationListener();
        IDatabaseConnection databaseConnection = databaseTester.getConnection();
        defaultOperationListener.connectionRetrieved(databaseConnection);
        databaseConnection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new H2DataTypeFactory());
        DatabaseOperation.CLEAN_INSERT.execute(databaseConnection, build);
        defaultOperationListener.operationSetUpFinished(databaseConnection);
        databaseConnection.close();
    }

    @AfterClass
    public static void des() throws SQLException {
        memDS.close();
    }

    @Before
    public void setup() {
        injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                final EntityManagerFactory emf;
                String pu = "auxiliar";

                Map<String, Object> properties = new HashMap<String, Object>();
//                properties.put("openjpa.Log", "log4j");
                properties.put("openjpa.Log", "DefaultLevel=WARN, Runtime=INFO, Tool=INFO, SQL=TRACE");
                properties.put("openjpa.ConnectionFactory", memDS);
                properties.put("openjpa.LockTimeout", 10000);
                properties.put("openjpa.jdbc.TransactionIsolation", "read-committed");
                properties.put("openjpa.BrokerImpl", "non-finalizing");

                Connection connection = null;
                try {
                    connection = memDS.getConnection();
                    String databaseProductName = connection.getMetaData().getDatabaseProductName();
                    LOGGER.info("La base a la que me conecto para el Persistence unit '{}' responde '{}'", pu, databaseProductName);
                    if (StringUtils.contains(databaseProductName, "DB2") && StringUtils.contains(databaseProductName, "AS")) {
                        properties.put("openjpa.jdbc.DBDictionary", BisaDictionary.class.getName());
                        LOGGER.info("Utilizando el dialecto BisaDictionary (AS/400 modificado) para la conexion {}",
                                getClass().getName());
                    }
                } catch (SQLException e) {
                    LOGGER.error("Error conectar la base de datos referida en el persistence unit " + pu, e);
                } finally {
                    try {
                        if (connection != null) {
                            connection.close();
                        }
                    } catch (SQLException e) {
                        LOGGER.warn("Error al cerrar la conexion a la base de datos", e);
                    }
                }

                properties.put("openjpa.jdbc.QuerySQLCache", "false");
                properties.put("openjpa.DataCache", "false");
                properties.put("openjpa.QueryCache", "false");
                properties.put("openjpa.RemoteCommitProvider", "sjvm");

                //http://stackoverflow.com/questions/2453671/nullable-date-column-merge-problem
//                properties.put("openjpa.DetachState", "loaded(DetachedStateField=true)");
//                properties.put("openjpa.DetachState", "fetch-groups");
                properties.put("openjpa.DetachState", "fetch-groups(DetachedStateField=true)");
                properties.put("openjpa.Log", "DefaultLevel=WARN, Runtime=INFO, Tool=INFO, SQL=TRACE");
//                properties.put("openjpa.DetachState", "fetch-groups(DetachedStateField=false)");
//                properties.put("openjpa.AutoDetach", "commit");

                emf = Persistence.createEntityManagerFactory(pu, properties);

//                bind(DataSource.class).toProvider(() -> memDS).in(SINGLETON);
//                bind(EntityManagerFactory.class).toProvider(() -> emf).in(SINGLETON);
                bind(DataSource.class).annotatedWith(BaseSinCambioAmbiente.class).toInstance(memDS);
                bind(EntityManagerFactory.class).annotatedWith(BasePrincipal.class).toInstance(emf);
                bind(Configuration.class).toInstance(new BaseConfiguration());
                bind(AuthSecurityService.class).to(AuthSecurityServiceImpl.class);
                bind(AuthWebServiceDao.class);
                bind(SafeEntityManagerFactory.class);
            }
        }, new CacheModule());

        injector.injectMembers(this);
    }

    @Test
    public void testSimpleQuery() throws SQLException {
        PreparedStatement ps = memDS.getConnection().prepareStatement("select * from ISBP82");
        ResultSet resultSet = ps.executeQuery();

        ResultSetMetaData metaData = resultSet.getMetaData();

        while (resultSet.next()) {
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                System.out.print(metaData.getColumnName(i) + "=" + resultSet.getObject(i) + ", ");
            }
            System.out.println();
        }
    }

    @Test
    public void testFetchAllUsers() throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {

        com.bisa.isb.ws.security.entities.AuthUser a;

        a = (AuthUser) Class.forName("com.bisa.isb.ws.security.entities.AuthUser").newInstance();

        TypedQuery<AuthUser> query = emf.createEntityManager().
                createQuery("select a from AuthUser a WHERE  a.userName = :userName AND a.password=:pwd", AuthUser.class);
        query.setParameter("userName", "admin");
        query.setParameter("pwd", "a43d192abfba8dec4ca9c514d84fb27de28598a0e9e15837:e4f5db17b6bebe0f8efbf5709e27ac3301ab052c180a399f");

        query = emf.createEntityManager().
                createQuery("select a from AuthUser a", AuthUser.class);

        List<AuthUser> resultList = query.getResultList();
        for (AuthUser authUser : resultList) {
            System.out.println("AUTH=" + authUser.getUserName() + ", CREATION DATE=" + authUser.getFechaCreacion());
            List<AuthRole> roles = authUser.getRoles();
            roles.forEach(role -> {
                        List<AuthWebServiceOperation> operations = role.getOperations();
                        System.out.println("\tROLE=" + role.getName() + " has " + operations.size() + " operations");
                        operations.forEach(
                                op -> System.out.println("\t\t\tOperation=" + op.getName() +
                                        ", WSID=" + op.getWebService().getId()
                                        + ", WSNAME=" + op.getWebService().getName()
                                        + ", TYPE=" + op.getWebService().getType())
                        );
                    }
            );
        }
    }

    @Test
    public void testPopulateServices() {
        Catalog<Service> serviceCatalog = new ServiceCatalog();

        //
//        serviceCatalog.add(Services.discoverService(soapEndpoint, CurrencyServiceImpl.class));

        serviceCatalog.add(Services.discoverService(soapEndpoint, HelloServiceImpl.class));

//        serviceCatalog.add(Services.discoverService(restEndpoint, BookService.class));

        authSecurityService.populateCatalog(serviceCatalog, Service.Type.SOAP);

        //fetch all from database

//        List<AuthWebService> services = authSecurityService.getAuthServices();
        List<AuthWebService> services = webServiceDao.getEntityManagerFactory().createEntityManager().
                createQuery("select a from AuthWebService a", AuthWebService.class).getResultList();

        services.forEach(authWebService -> {
            LOGGER.info("---> SERVICE...{} :: {}", authWebService.getDescription(), authWebService);
            List<AuthWebServiceOperation> ops = authWebService.getOperations();
            ops.forEach(op -> {
                LOGGER.info("\tOP.name={}, op.desc={}", op.getName(), op.getDesc());
            });
        });
    }

    @Test(expected = NoSuchElementException.class)
//    @Test
    public void testRetrieveIPAddresses() {
        AuthUser authUser = new AuthUser();
        authUser.setId(1L);

        List<AuthUserIP> ips = authSecurityService.getAllIPAddresses(authUser);

        ips.forEach(ip -> LOGGER.info("IP ADDRESS IS: {}", ip.getIp()));

        final InetAddress toTest = InetAddresses.forString("192.168.1.1");

        AuthUserIP authUserIP = ips.stream().filter(ip -> Subnet.forString(ip.getIp()).isInNet(toTest)).findFirst().get();

        LOGGER.info("USER AUTH FOUND: " + authUserIP);

        Assert.assertEquals("192.168.1.0/24", authUserIP.getIp());

        //this will throw an exception
        ips.stream().filter(ip -> Subnet.forString(ip.getIp()).isInNet(InetAddresses.forString("127.0.0.1"))).
                findFirst().get();
    }


    @Test
    public void testSImpleListWithIllegalArgExceptionOnFilter() {
        List<String> list = ImmutableList.of("a", "b", "c");
        list.stream().filter(x -> isSame("d", x)).findFirst().get();
    }

    private boolean isSame(String a, String b) {
        if (a.equals(b))
            return true;
        throw new IllegalArgumentException("Not same");
    }


    @Test
    public void testSecureService() throws InvalidKeySpecException, NoSuchAlgorithmException {
        LOGGER.info("SECURE = {}", PasswordHash.createHash("admin"));
    }

    @Test
    public void testFetchUser() {
        try {
            AuthUser authUser = authSecurityService.getUserByName(new Creds("admin", "admin"));
            assertTrue(1 == authUser.getId());
            LOGGER.info("Uer {} has {} roles", authUser.getUserName(), authUser.getRoles().size());
        } catch (AuthenticationException x) {
            assertTrue(false);
        }

        try {
            authSecurityService.getUserByName(new Creds("inexistente", "any"));
            assertTrue(false);
        } catch (AuthenticationException x) {
            assertTrue(x.getMessage().indexOf("is not present in database") > 0);
        }

        try {
            authSecurityService.getUserByName(new Creds("admin", "adminadmin"));
            assertTrue(false);
        } catch (AuthenticationException x) {
            assertTrue(x.getMessage().indexOf("Invalid password") == 0);
        }
    }

    @Test
    public void testPredicate() {
        Set<String> set = ImmutableSet.of("one", "two", "three", "four", "five");

        List<String> list = ImmutableList.of("one", "four", "five");

        set.forEach(val -> {
            list.stream().filter(l -> l.equals(val)).findFirst().orElse("");
        });
    }

    public static class Creds implements Credentials {

        final String name;
        final String pwd;

        public Creds(String name, String pwd) {
            this.name = name;
            this.pwd = pwd;
        }

        @Override
        public Principal getUserPrincipal() {
            return new Principal() {
                @Override
                public String getName() {
                    return name;
                }
            };
        }

        @Override
        public String getPassword() {
            return pwd;
        }
    }

    final ServiceEndpoint soapEndpoint = new ServiceEndpoint() {
        @Override
        public String getPublishPath() {
            return "/soap/*";
        }
    };

    final ServiceEndpoint restEndpoint = new ServiceEndpoint() {
        @Override
        public String getPublishPath() {
            return "/rest/*";
        }
    };
}