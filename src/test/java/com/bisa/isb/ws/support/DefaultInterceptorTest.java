package com.bisa.isb.ws.support;

import com.bisa.isb.api.net.Subnet;
import com.google.common.net.InetAddresses;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Miguel Vega, Org
 * @version $Id: DefaultInterceptorTest.java; Jan 13, 2016. 11:52 PM mvega $
 * @source $URL$
 */
public class DefaultInterceptorTest {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testInnetAddress(){
        InetAddress inetAddress = InetAddresses.forString("192.168.0.1");

        logger.info("Address: {}", inetAddress);

        assertTrue(Subnet.forString("192.168.0.0/24").isInNet(inetAddress));
        assertTrue(Subnet.forString("192.168.0.1/32").isInNet(inetAddress));
        assertTrue(Subnet.forString("192.168.0.1").isInNet(inetAddress));
        assertFalse(Subnet.forString("192.168.1.1").isInNet(inetAddress));
    }
}