package com.bisa.isb.api;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Miguel Vega
 * @version $Id: AttemptExecutorTest.java; oct 06, 2015 11:18 AM mvega $
 */
public class AttemptExecutorTest {

    @Test
    public void testNoError() {
        Integer result = AttemptExecutor.<Integer, RuntimeException>createExecutor(RuntimeException.class).
                execute(new Attempt<Integer>("a simple integer test") {
                    @Override
                    public Integer doTry() throws Throwable {
                        return 8584;
                    }
                });

        assertEquals(new Integer(8584), result);
    }

    @Test
    public void testTryCounts() throws IOException {

        final int expectedTryNum = 3;

        AttemptExecutor<Integer, IOException> executor = AttemptExecutor.createExecutor(500l, IOException.class);

        Integer counts = executor.execute(new Attempt<Integer>("a simple test") {

            int tryNum = 0;

            @Override
            public Integer doTry() throws Throwable {
                if (++tryNum == expectedTryNum) {
                    return tryNum;
                }
                throw new IllegalStateException("An avoidable exception, just for test");
            }
        });

        assertEquals(expectedTryNum, counts.intValue());
    }

    @Test
    public void testCatchManageableException() {
        AttemptExecutor<Void, IllegalArgumentException> executor = AttemptExecutor.<Void, IllegalArgumentException>createExecutor(IllegalArgumentException.class);

        boolean exceptionCaught = false;

        try {
            executor.execute(new Attempt<Void>("A exception caught") {
                @Override
                public Void doTry() throws Throwable {
                    throw new IllegalArgumentException("Expected exception threw");
                }
            });
        } catch (IllegalArgumentException e) {
            exceptionCaught = true;
        }

        assertTrue(exceptionCaught);
    }

    @Test
    public void testDaaIntegrityException() throws Exception {
        AttemptExecutor<Void, Exception> executor = AttemptExecutor.<Void, Exception>createExecutor(3, 500l, Exception.class);

        executor.execute(new Attempt<Void>("poipoipoi") {
            @Override
            public Void doTry() throws Throwable {
                throw new IllegalStateException();
            }
        });
    }

    @Test
    public void testUncheckedException() {

        AttemptExecutor<Object, Exception> executor = AttemptExecutor.createExecutor(3, 500);

        executor.executeUnchecked(new Attempt<Object>("qwerty") {
            @Override
            public Object doTry() throws Throwable {
                throw new IOException("");
            }
        }, "Oh my !!!");
    }

}