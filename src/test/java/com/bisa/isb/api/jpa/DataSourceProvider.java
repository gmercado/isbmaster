package com.bisa.isb.api.jpa;

import com.bisa.isb.ws.security.AuthSecurityService;
import com.bisa.isb.ws.security.AuthSecurityServiceImpl;
import com.bisa.isb.ws.security.dao.AuthWebServiceDao;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.io.Resources;
import com.google.inject.*;
import bus.cache.CacheModule;
import bus.database.jpa.BisaDictionary;
import bus.database.model.BasePrincipal;
import bus.database.model.BaseSinCambioAmbiente;
import net.sf.ehcache.Ehcache;
import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.StringUtils;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.DefaultOperationListener;
import org.dbunit.IDatabaseTester;
import org.dbunit.IOperationListener;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.h2.H2DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Charsets.US_ASCII;
import static com.google.common.io.Resources.getResource;

/**
 * @author Miguel Vega
 * @version $Id: DataSourceBuilder.java; nov 06, 2015 03:53 PM mvega $
 */
public class DataSourceProvider {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected Injector injector;

    static BasicDataSource memDS;

    @Inject
    @BasePrincipal
    protected EntityManagerFactory entityManagerFactory;

    static {
        if(Strings.isNullOrEmpty(System.getProperty("as400")))
            withMmemoryDataSource();
        else
            withAS400DataSource();
    }

    protected static void withMmemoryDataSource(){
        try {
            memDS = new BasicDataSource();
            memDS.setUrl("jdbc:h2:mem:cerrarcamaras");
            memDS.setDriverClassName("org.h2.Driver");

            Connection connection = memDS.getConnection();
            URL sql = getResource(AuthSecurityService.class, "auth-ddl.sql");
            Statement statement = connection.createStatement();
            statement.execute(Resources.toString(sql, US_ASCII));
            statement.close();
            connection.close();

            URL resource = getResource(AuthSecurityService.class, "auth-data.xml");
            FlatXmlDataSet build = new FlatXmlDataSetBuilder().build(resource);
            IDatabaseTester databaseTester = new DataSourceDatabaseTester(memDS);
            databaseTester.setDataSet(build);
            IOperationListener defaultOperationListener = new DefaultOperationListener();
            IDatabaseConnection databaseConnection = databaseTester.getConnection();
            defaultOperationListener.connectionRetrieved(databaseConnection);
            databaseConnection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new H2DataTypeFactory());
            DatabaseOperation.CLEAN_INSERT.execute(databaseConnection, build);
            defaultOperationListener.operationSetUpFinished(databaseConnection);
            databaseConnection.close();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        } finally {
        }
    }

    protected static void withAS400DataSource(){
        memDS = new BasicDataSource();
        memDS.setUrl("jdbc:as400://bisades.grupobisa.net/;prompt=false;naming=system;libraries=sbisasql sbnkprd01 sicbsbisa starjeta zpicbssms asid113001 sbisalib stablas;query storage limit=-1;hold statements=true");
        memDS.setUsername("mavega411");
        memDS.setPassword("mavega0");
        memDS.setDriverClassName("com.ibm.as400.access.AS400JDBCDriver");
    }


    public void initialize(Module... abstractModules){
        this.initialize("auxiliar", abstractModules);
    }

    public void initialize(final String persistencUnit, Module... abstractModules){
        List<Module> modules = Lists.newArrayList();
        modules.addAll(Arrays.asList(abstractModules));
        modules.add(
                new AbstractModule() {
                    @Override
                    protected void configure() {
                        final EntityManagerFactory emf;

                        Map<String, Object> properties = new HashMap<String, Object>();
//                properties.put("openjpa.Log", "log4j");
                        properties.put("openjpa.Log", "DefaultLevel=WARN, Runtime=INFO, Tool=INFO, SQL=TRACE");
                        properties.put("openjpa.ConnectionFactory", memDS);
                        properties.put("openjpa.LockTimeout", 10000);
                        properties.put("openjpa.jdbc.TransactionIsolation", "read-committed");
                        properties.put("openjpa.BrokerImpl", "non-finalizing");

                        Connection connection = null;
                        try {
                            connection = memDS.getConnection();
                            String databaseProductName = connection.getMetaData().getDatabaseProductName();
                            logger.info("La base a la que me conecto para el Persistence unit '{}' responde '{}'",
                                    persistencUnit, databaseProductName);
                            if (StringUtils.contains(databaseProductName, "DB2") && StringUtils.contains(databaseProductName, "AS")) {
                                properties.put("openjpa.jdbc.DBDictionary", BisaDictionary.class.getName());
                                logger.info("Utilizando el dialecto BisaDictionary (AS/400 modificado) para la conexion {}",
                                        getClass().getName());
                            }
                        } catch (SQLException e) {
                            logger.error("Error conectar la base de datos referida en el persistence unit " +
                                    persistencUnit, e);
                        } finally {
                            try {
                                if (connection != null) {
                                    connection.close();
                                }
                            } catch (SQLException e) {
                                logger.warn("Error al cerrar la conexion a la base de datos", e);
                            }
                        }

                        properties.put("openjpa.jdbc.QuerySQLCache", "false");
                        properties.put("openjpa.DataCache", "false");
                        properties.put("openjpa.QueryCache", "false");
                        properties.put("openjpa.RemoteCommitProvider", "sjvm");

                        //http://stackoverflow.com/questions/2453671/nullable-date-column-merge-problem
//                properties.put("openjpa.DetachState", "loaded(DetachedStateField=true)");
//                properties.put("openjpa.DetachState", "fetch-groups");
                        properties.put("openjpa.DetachState", "fetch-groups(DetachedStateField=true)");
                        properties.put("openjpa.Log", "DefaultLevel=WARN, Runtime=INFO, Tool=INFO, SQL=TRACE");
//                properties.put("openjpa.DetachState", "fetch-groups(DetachedStateField=false)");
//                properties.put("openjpa.AutoDetach", "commit");

                        emf = Persistence.createEntityManagerFactory(persistencUnit, properties);

//                bind(DataSource.class).toProvider(() -> memDS).in(SINGLETON);
//                bind(EntityManagerFactory.class).toProvider(() -> emf).in(SINGLETON);
                        bind(DataSource.class).annotatedWith(BaseSinCambioAmbiente.class).toInstance(memDS);
                        bind(EntityManagerFactory.class).annotatedWith(BasePrincipal.class).toInstance(emf);
                        bind(Configuration.class).toInstance(new BaseConfiguration());
                        bind(AuthSecurityService.class).to(AuthSecurityServiceImpl.class);
                        bind(AuthWebServiceDao.class);
                        bind(SafeEntityManagerFactory.class);
                        bind(Ehcache.class).toProvider(CacheModule.CacheProvider.class).in(Scopes.SINGLETON);
                    }
                }
        );
        injector = Guice.createInjector(modules);

        injector.injectMembers(this);
    }
}
