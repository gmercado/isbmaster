package com.bisa.isb.api.security;

import com.google.common.base.Splitter;
import org.junit.Assert;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Iterator;

/**
 * @author Miguel Vega
 * @version $Id: PasswordHashTest.java 0, 2015-09-20 06:21 PM mvega $
 */
public class PasswordHashTest {

    @Test
    public void testSimpleSalt() throws InvalidKeySpecException, NoSuchAlgorithmException {
        String pwd = "admin";
        final String hash = PasswordHash.createHash(pwd);
        System.out.println("HASH=" + hash);

        System.out.println("SOAP admin=" + PasswordHash.createHash("admin"));
        //System.out.println("SOAP=" + PasswordHash.createHash("soap"));
        System.out.println("REST=" + PasswordHash.createHash("rest"));

        Iterator<String> results = Splitter.on(":").trimResults().omitEmptyStrings().split(hash).iterator();

        String salt = results.next();
        String hpwd = results.next();

        System.out.println("SALT="+salt);
        System.out.println("HASH="+hpwd);

        //verify

        Assert.assertTrue(PasswordHash.validatePassword(pwd, hash));
    }

    /**
     * Tests the basic functionality of the PasswordHash class
     */
    @Test
    public void testHash() {
        try {
            // Print out 10 hashes
            for (int i = 0; i < 10; i++)
                System.out.println(PasswordHash.createHash("p\r\nassw0Rd!"));

            // Test password validation
            boolean failure = false;
            System.out.println("Running tests...");
            for (int i = 0; i < 100; i++) {
                String password = "" + i;
                String hash = PasswordHash.createHash(password);
                String secondHash = PasswordHash.createHash(password);
                if (hash.equals(secondHash)) {
                    System.out.println("FAILURE: TWO HASHES ARE EQUAL!");
                    failure = true;
                }
                String wrongPassword = "" + (i + 1);
                if (PasswordHash.validatePassword(wrongPassword, hash)) {
                    System.out.println("FAILURE: WRONG PASSWORD ACCEPTED!");
                    failure = true;
                }
                if (!PasswordHash.validatePassword(password, hash)) {
                    System.out.println("FAILURE: GOOD PASSWORD NOT ACCEPTED!");
                    failure = true;
                }
            }
            if (failure)
                System.out.println("TESTS FAILED!");
            else
                System.out.println("TESTS PASSED!");
        } catch (Exception ex) {
            System.out.println("ERROR: " + ex);
        }
    }
}