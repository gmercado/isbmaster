package com.bisa.isb.api.tramites;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Miguel Vega
 * @version $Id: CliElemPK.java; nov 11, 2015 11:37 AM mvega $
 */
@Embeddable
public class CliElemPK {
    @Column(name = "T51NIDCLIP")
    private Integer clientId;

    @Column(name = "T51NIDELEM")
    private Integer elementId;

    public Integer getElementId() {
        return elementId;
    }

    public void setElementId(Integer elementId) {
        this.elementId = elementId;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }
}
