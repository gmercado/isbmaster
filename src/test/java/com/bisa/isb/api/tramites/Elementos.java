package com.bisa.isb.api.tramites;

import javax.persistence.*;

/**
 * @author Miguel Vega
 * @version $Id: Elementos.java; nov 10, 2015 04:10 PM mvega $
 */
@Entity
@Table(name = "TIP56")
public class Elementos {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "T56NIDELEM")
    private Integer id;

    @Column(name = "T56VREF")
    private String reference;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
