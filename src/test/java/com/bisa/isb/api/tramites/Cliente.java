package com.bisa.isb.api.tramites;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: Elementos.java; nov 10, 2015 04:10 PM mvega $
 */
@Entity
@Table(name = "TIP50")
public class Cliente implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "T50NIDCP")
    private Integer id;

    @Column(name = "T50VNRODOC")
    private String numDoc;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true) //FetchType.LAZY
    @JoinTable(
            name = "TIP51",
            joinColumns = { @JoinColumn(name="T51NIDCLIP", referencedColumnName="T50NIDCP") },
            inverseJoinColumns={ @JoinColumn(name="T51NIDELEM", referencedColumnName="T56NIDELEM", unique=true) }
    )
//    @Transient
    private List<Elementos> elements;

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Elementos> getElements() {
        return elements;
    }

    public void setElements(List<Elementos> elements) {
        this.elements = elements;
    }

}
