package com.bisa.isb.api.tramites;

import com.bisa.isb.api.jpa.DataSourceProvider;
import com.bisa.isb.api.jpa.SimpleDao;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: OperationsTest.java; nov 10, 2015 04:47 PM mvega $
 */
public class OperationsTest extends DataSourceProvider{

    private SimpleDao<Cliente, Integer> simpleDao;

    @Before
    public void init(){

        withAS400DataSource();

        super.initialize("tramitesPU");

        this.simpleDao = new SimpleDao<Cliente, Integer>(entityManagerFactory){};
    }

    @Test
    public void testSimpleQuery(){

//        PCEnhancer.main(new String[]{"D:\\miguelvega\\java\\bisa\\aqua2\\src\\test\\java\\com\\bisa\\isb\\api\\tramites\\Cliente.java"});

        Cliente cli = simpleDao.findById(1);

        logger.info("CLIENT...{}", cli.getId());

        List<Elementos> elements = cli.getElements();

        elements.forEach(el -> logger.info("Element is>{}", el.getReference()));
    }

    @Test
    public void testInsert(){
        Cliente c = new Cliente();
        c.setNumDoc("6105078");

        Elementos e = new Elementos();
        e.setId(1);
        c.setElements(ImmutableList.of(e));

        Cliente persist = simpleDao.merge(c);

        logger.info("New entity persisted...{}-{}", persist.getNumDoc(), persist.getId());
    }
}
