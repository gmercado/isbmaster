package com.bisa.isb.api.tramites;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Miguel Vega
 * @version $Id: RelClienteElem.java; nov 10, 2015 04:30 PM mvega $
 */
@Entity
@Table(name = "TIP51")
public class RelClienteElem {
    @EmbeddedId
    private CliElemPK id;

    public CliElemPK getId() {
        return id;
    }

    public void setId(CliElemPK id) {
        this.id = id;
    }
}
