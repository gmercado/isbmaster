package com.bisa.isb;

import com.google.common.base.Splitter;
import org.junit.Test;

import java.io.StringReader;
import java.util.Iterator;
import java.util.Scanner;

/**
 * @author Miguel Vega
 * @version $Id: PopulateSequence.java; oct 07, 2015 03:04 PM mvega $
 */
public class PopulateSequence {
    @Test
    public void testPopulateSequenceFromTAB720(){
        String source = "" +
                "1234824020,6912\n" +
                "1813284011,6911\n" +
                "452212307 ,6918\n" +
                "452210291 ,6918\n" +
                "1101404028,6911\n" +
                "452212307 ,6918\n" +
                "1778824015,6912\n" +
                "452210291 ,6918\n" +
                "1201103763,6912\n" +
                "0604470455,6912\n" +
                "1201103763,6912\n" +
                "0483640013,6912\n" +
                "4366934010,6913\n" +
                "20560029  ,6913\n" +
                "4378840011,6911\n" +
                "0821674018,6911\n" +
                "3345214011,6912\n" +
                "47960011  ,6912\n" +
                "195960020 ,6911\n" +
                "894890011 ,6912\n" +
                "1614110011,6911\n" +
                "0821674018,6911\n" +
                "2266024013,6911\n" +
                "0392374019,6915\n" +
                "3255984011,6912\n" +
                "26540038  ,6911\n" +
                "26542138  ,6911\n" +
                "1481754018,6912\n" +
                "1725200010,6913\n" +
                "894890011 ,6912\n" +
                "0190232018,6913";

        String date = "2015279";

        final String select = "SELECT T20BANCO, T20TPOCTA, T20CUENTA, T20FECHA, T20NUMDIA,\n" +
                "T20CODTRN, T20CANAL, T20HORA, T20CAJA, T20NUMSEQ, T20USR,\n" +
                "T20TPOCRG, T20CARGO FROM tap820\n" +
                "WHERE T20FECHA=:date AND T20CUENTA=:cuenta AND T20CAJA=:caja";

        Scanner scanner = new Scanner(new StringReader(source));
        while(scanner.hasNextLine()){
            Iterator<String> split = Splitter.on(",").trimResults().split(scanner.nextLine()).iterator();
            System.out.println(select.replace(":date", date).replace(":cuenta", split.next()).replace(":caja", split.next()));
            System.out.println();
        }


    }


    @Test
    public void testCreateUpdatesForSequence(){
        String recs = "" +
                "88880683967, 268610\n" +
                "88880683970, 332879\n" +
                "88880683971, 19754 \n" +
                "88880683972, 19755 \n" +
                "88880683973, 332880\n" +
                "88880683974, 19756 \n" +
                "88880683978, 268613\n" +
                "88880683979, 19758 \n" +
                "88880683981, 268614\n" +
                "88880683982, 268615\n" +
                "88880683983, 268616\n" +
                "88880683985, 268617\n" +
                "88880683988, 78055 \n" +
                "88880683989, 78056 \n" +
                "88880683991, 332881\n" +
                "88880683997, 332882\n" +
                "88880684006, 268626\n" +
                "88880684011, 268629\n" +
                "88880684012, 332888\n" +
                "88880684013, 268630\n" +
                "88880684016, 332889\n" +
                "88880684021, 332892\n" +
                "88880684027, 332897\n" +
                "88880684030, 31972 \n" +
                "88880684032, 268636\n" +
                "88880684036, 332901\n" +
                "88880684039, 332904\n" +
                "88880684040, 268637\n" +
                "88880684042, 78062 \n" +
                "88880684048, 268642\n" +
                "88880684050, 78063";

        final String update = "UPDATE ZBNKPRD01/ACHP01 SET ACHNUMSEQ = :seq\n" +
                "WHERE ACHNUMORD = :orden";

        Scanner scanner = new Scanner(new StringReader(recs));
        while(scanner.hasNextLine()){
            Iterator<String> split = Splitter.on(",").trimResults().split(scanner.nextLine()).iterator();
            System.out.println(update.replace(":orden", split.next()).replace(":seq", split.next()));
        }

    }
}
