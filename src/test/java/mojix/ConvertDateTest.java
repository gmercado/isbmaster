package mojix;

import com.bisa.isb.api.jpa.DataSourceProvider;
import org.joda.time.DateTime;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

/**
 * Created by josanchez on 13/07/2016.
 */
public class ConvertDateTest extends DataSourceProvider {

    @Test
    public void decimalFormat() {

        BigDecimal dt = new BigDecimal(1);
        logger.info("value 1: {}", dt.movePointLeft(2));

        dt = new BigDecimal(0);
        logger.info("value 2: {}", dt.movePointLeft(2));

        dt = new BigDecimal(100);
        logger.info("value 3: {}", dt.movePointLeft(2));

        dt = new BigDecimal(1000);
        logger.info("value 4: {}", dt.movePointLeft(2));

        dt = new BigDecimal(1152653);
        logger.info("value 5: {}", dt.movePointLeft(2));

        double a = 3400.51;

        dt = new BigDecimal(3400.51);

        String monto = "3400.51";

        monto = monto.replace(".", "");

        logger.info("value sin punto 6: {}", monto);

        DateTime now = DateTime.now();

        SimpleDateFormat dff = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        dff.format(now.toDate());
        System.out.println("DATE FORMAT:" + dff.format(now.toDate()));


    }
}
