package utilitarios;

import bus.consumoweb.infocred.objetos.TitularInfoCred;
import bus.consumoweb.infocred.utilitarios.XmlUtil;
import org.junit.Test;

import javax.xml.bind.JAXBException;

/**
 * Created by gmercado on 08/05/2017.
 */
public class XmlUtilTest {

    @Test
    public void ini() {
        try {
            TitularInfoCred datosXml = XmlUtil.xmlToObject(xml, TitularInfoCred.class);
            System.out.println(datosXml);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }




    String xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n" +
            "<titular>\n" +
            "    <cabecera Titulo=\"INFORME CONFIDENCIAL\">\n" +
            "        <fechaHora Titulo=\"Fecha Emisión\">04/05/2017 13:14:30</fechaHora>\n" +
            "        <reporteNumero Titulo=\"Nro. de Reporte\">80174233217321026</reporteNumero>\n" +
            "        <usuario Titulo=\"Emitido por\" TipoDocumento=\"DESCONOCIDO\" NumeroDocumento=\"999888777\">[USUARIO INICIAL]\n" +
            "        </usuario>\n" +
            "        <entidad Titulo=\"Entidad\" TipoDocumento=\"NIT\" NumeroDocumento=\"1020149020\">BANCO BISA SA</entidad>\n" +
            "        <nroSolicitud Titulo=\"Nro. Solicitud\">80174233</nroSolicitud>\n" +
            "        <tipoDeCambio Titulo=\"Tipo de Cambio\" Fecha=\"03/10/2013\" MonedaOrigen=\"Bs\" MonedaDestino=\"USD\" Compra=\"6,86\"\n" +
            "                      Venta=\"6,96\"/>\n" +
            "    </cabecera>\n" +
            "    <datosPersonales Titulo=\"DATOS PERSONALES\">\n" +
            "        <datosGenerales>\n" +
            "            <datosGenerales>\n" +
            "                <nombreCompleto Titulo=\"Nombre Completo\">TITULAR DESCONOCIDO PRUEBA</nombreCompleto>\n" +
            "                <tipoDocumento Titulo=\"Tipo Documento\">CI</tipoDocumento>\n" +
            "                <nroDocumento Titulo=\"Nro. Documento\">7689511</nroDocumento>\n" +
            "                <ext Titulo=\"Ext.\"></ext>\n" +
            "                <estadoCivil Titulo=\"Estado Civil\">SOLTERO(A)</estadoCivil>\n" +
            "                <tipoPersona Titulo=\"Tipo Persona\">NATURAL</tipoPersona>\n" +
            "                <fechaNacFechaDef Titulo=\"Fecha Nac./Fecha Def.\">28/02/1976</fechaNacFechaDef>\n" +
            "            </datosGenerales>\n" +
            "        </datosGenerales>\n" +
            "        <datosConyuge/>\n" +
            "        <domicilio>\n" +
            "            <domicilio>\n" +
            "                <direccion Titulo=\"Direccion\">CONDOMINIO LOS SAUCES C-5 #54 (LA FLORIDA)</direccion>\n" +
            "                <localidad Titulo=\"Localidad\">NUESTRA SEÑORA DE LA PAZ</localidad>\n" +
            "                <provincia Titulo=\"Provincia\">MURILLO</provincia>\n" +
            "                <departamento Titulo=\"Departamento\">LA PAZ</departamento>\n" +
            "                <fechaDeclaracion Titulo=\"Fecha Declaracion\">10/09/2012</fechaDeclaracion>\n" +
            "            </domicilio>\n" +
            "            <domicilio>\n" +
            "                <direccion Titulo=\"Direccion\">CALLE ADOLFO CONZALES No. 2360 ZONA ALTO SOPOCACHI</direccion>\n" +
            "                <localidad Titulo=\"Localidad\">NUESTRA SEÑORA DE LA PAZ</localidad>\n" +
            "                <provincia Titulo=\"Provincia\">MURILLO</provincia>\n" +
            "                <departamento Titulo=\"Departamento\">LA PAZ</departamento>\n" +
            "                <fechaDeclaracion Titulo=\"Fecha Declaracion\">17/08/2009</fechaDeclaracion>\n" +
            "            </domicilio>\n" +
            "            <domicilio>\n" +
            "                <direccion Titulo=\"Direccion\">CALLE 2 ZONA PAMPAHASI</direccion>\n" +
            "                <localidad Titulo=\"Localidad\">NUESTRA SEÑORA DE LA PAZ</localidad>\n" +
            "                <provincia Titulo=\"Provincia\">MURILLO</provincia>\n" +
            "                <departamento Titulo=\"Departamento\">LA PAZ</departamento>\n" +
            "                <fechaDeclaracion Titulo=\"Fecha Declaracion\">03/01/2006</fechaDeclaracion>\n" +
            "            </domicilio>\n" +
            "        </domicilio>\n" +
            "        <score>\n" +
            "            <score>\n" +
            "                <score Titulo=\"Score\">924</score>\n" +
            "                <descripcion Titulo=\"Descripcion\">RIESGO BAJO</descripcion>\n" +
            "            </score>\n" +
            "        </score>\n" +
            "    </datosPersonales>\n" +
            "    <datosFinancieros>\n" +
            "        <sISTEMAFINANCIERO Titulo=\"SISTEMA FINANCIERO\">\n" +
            "            <sISTEMAFINANCIERO Titulo=\"DEUDA EN EL SISTEMA FINANCIERO SALDOS EXPRESADOS EN BOLIVIANOS\">\n" +
            "                <fila numero=\"1\">\n" +
            "                    <sISTEMA titulo=\"SISTEMA\" tipo=\"char\">R</sISTEMA>\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"varchar\">BANCO AGRICOLA DE BOLIVIA SA</eNTIDAD>\n" +
            "                    <fECHAACTUALIZACION titulo=\"FECHA ACTUALIZACION\" tipo=\"char\">31/08/2016</fECHAACTUALIZACION>\n" +
            "                    <fECHAPROCESAMIENTO titulo=\"FECHA PROCESAMIENTO\" tipo=\"char\">27/09/2016</fECHAPROCESAMIENTO>\n" +
            "                    <tIPOCREDITO titulo=\"TIPO CREDITO\" tipo=\"char\">N0 - CONSUMO</tIPOCREDITO>\n" +
            "                    <tIPOOBLIGADO titulo=\"TIPO OBLIGADO\" tipo=\"char\">1A - DEUDOR</tIPOOBLIGADO>\n" +
            "                    <fECHAINICIO titulo=\"FECHA INICIO\" tipo=\"smalldatetime\">13/02/1998 00:00:00</fECHAINICIO>\n" +
            "                    <fECHAULTIMACUOTA titulo=\"FECHA ULTIMA CUOTA\" tipo=\"smalldatetime\">20/09/2000 00:00:00\n" +
            "                    </fECHAULTIMACUOTA>\n" +
            "                    <mONEDA titulo=\"MONEDA\" tipo=\"char\">CMV</mONEDA>\n" +
            "                    <mONTOORIGINAL titulo=\"MONTO ORIGINAL\" tipo=\"money\">6,800.00</mONTOORIGINAL>\n" +
            "                    <vALORCUOTA titulo=\"VALOR CUOTA\" tipo=\"money\">0.00</vALORCUOTA>\n" +
            "                    <cALIFICACION titulo=\"CALIFICACION\" tipo=\"char\">F</cALIFICACION>\n" +
            "                    <eSTADO titulo=\"ESTADO\" tipo=\"char\">EJECUCION</eSTADO>\n" +
            "                    <sALDO titulo=\"SALDO\" tipo=\"money\">37,044.00</sALDO>\n" +
            "                    <cONTINGENTE titulo=\"CONTINGENTE\" tipo=\"money\">0.00</cONTINGENTE>\n" +
            "                    <dIASMORA titulo=\"DIAS MORA\" tipo=\"int\">5704</dIASMORA>\n" +
            "                    <hISTORICO titulo=\"HISTORICO\" tipo=\"char\">555555555333333333</hISTORICO>\n" +
            "                    <pERIODODEPAGO titulo=\"PERIODO DE PAGO\" tipo=\"char\">A VENCIMIENTO</pERIODODEPAGO>\n" +
            "                    <tIPCAN titulo=\"TIP CAN\" tipo=\"char\"></tIPCAN>\n" +
            "                    <tIPINT titulo=\"TIP INT\" tipo=\"char\">FV</tIPINT>\n" +
            "                    <tIPOPE titulo=\"TIP OPE\" tipo=\"char\">01</tIPOPE>\n" +
            "                    <tIPING titulo=\"TIP ING\" tipo=\"char\"></tIPING>\n" +
            "                    <cAEDEC titulo=\"CAEDEC\" tipo=\"char\">C - 11100</cAEDEC>\n" +
            "                    <aCTECONO titulo=\"ACTECONO\" tipo=\"char\">C - 11100</aCTECONO>\n" +
            "                </fila>\n" +
            "            </sISTEMAFINANCIERO>\n" +
            "            <tOTALSISTEMAFINANCIERO Titulo=\"TOTAL DEUDA EN EL SISTEMA FINANCIERO EXPRESADO EN BOLIVIANOS\">\n" +
            "                <fila numero=\"1\">\n" +
            "                    <oRDEN titulo=\"ORDEN\" tipo=\"int\">1</oRDEN>\n" +
            "                    <dESCRIPCION titulo=\"DESCRIPCION\" tipo=\"char\">TOTAL DEUDA DIRECTA</dESCRIPCION>\n" +
            "                    <sALDO titulo=\"SALDO\" tipo=\"money\">37,044.00</sALDO>\n" +
            "                </fila>\n" +
            "                <fila numero=\"2\">\n" +
            "                    <oRDEN titulo=\"ORDEN\" tipo=\"int\">2</oRDEN>\n" +
            "                    <dESCRIPCION titulo=\"DESCRIPCION\" tipo=\"char\">TOTAL DEUDA INDIRECTA</dESCRIPCION>\n" +
            "                    <sALDO titulo=\"SALDO\" tipo=\"money\">0.00</sALDO>\n" +
            "                </fila>\n" +
            "                <fila numero=\"3\">\n" +
            "                    <oRDEN titulo=\"ORDEN\" tipo=\"int\">3</oRDEN>\n" +
            "                    <dESCRIPCION titulo=\"DESCRIPCION\" tipo=\"char\">TOTAL CONTINGENTE DIRECTO</dESCRIPCION>\n" +
            "                    <sALDO titulo=\"SALDO\" tipo=\"money\"></sALDO>\n" +
            "                </fila>\n" +
            "                <fila numero=\"4\">\n" +
            "                    <oRDEN titulo=\"ORDEN\" tipo=\"int\">4</oRDEN>\n" +
            "                    <dESCRIPCION titulo=\"DESCRIPCION\" tipo=\"char\">TOTAL CONTINGENTE INDIRECTO</dESCRIPCION>\n" +
            "                    <sALDO titulo=\"SALDO\" tipo=\"money\"></sALDO>\n" +
            "                </fila>\n" +
            "            </tOTALSISTEMAFINANCIERO>\n" +
            "            <dESCRIPCIONCAEDECACTECONO Titulo=\"\">\n" +
            "                <fila numero=\"1\">\n" +
            "                    <cAEDECACTECO titulo=\"CAEDEC ACTECO\" tipo=\"char\">C</cAEDECACTECO>\n" +
            "                    <cODIGODESTINOCREDITOACTIVIDADECONOMICA titulo=\"CODIGO DESTINO CREDITO ACTIVIDAD ECONOMICA\"\n" +
            "                                                            tipo=\"char\">11100\n" +
            "                    </cODIGODESTINOCREDITOACTIVIDADECONOMICA>\n" +
            "                    <dESCRIPCIONCAEDECACTECO titulo=\"DESCRIPCION CAEDEC ACTECO\" tipo=\"char\">EXTRACCION DE PETROLEO CRUDO\n" +
            "                        Y GAS NATURAL\n" +
            "                    </dESCRIPCIONCAEDECACTECO>\n" +
            "                    <dESCRIPCIONDESTINOCREDITOACTIVIDADECONOMICA\n" +
            "                            titulo=\"DESCRIPCION DESTINO CREDITO ACTIVIDAD ECONOMICA\" tipo=\"char\">EXTRACCION DE PETROLEO\n" +
            "                        CRUDO Y GAS NATURAL\n" +
            "                    </dESCRIPCIONDESTINOCREDITOACTIVIDADECONOMICA>\n" +
            "                    <dESCRIPCIONREPORTECAEDECACTECO titulo=\"DESCRIPCION REPORTE CAEDEC ACTECO\" tipo=\"char\">C - 11100:\n" +
            "                        EXTRACCION DE PETROLEO CRUDO Y GAS NATURAL - EXTRACCION DE PETROLEO CRUDO Y GAS NATURAL\n" +
            "                    </dESCRIPCIONREPORTECAEDECACTECO>\n" +
            "                </fila>\n" +
            "            </dESCRIPCIONCAEDECACTECONO>\n" +
            "            <dESCRIPCIONTIPCAN Titulo=\"\"/>\n" +
            "            <dESCRIPCIONTIPINT Titulo=\"\">\n" +
            "                <fila numero=\"1\">\n" +
            "                    <cODIGOTIPOINTERES titulo=\"CODIGO TIPO INTERES\" tipo=\"char\">FV</cODIGOTIPOINTERES>\n" +
            "                    <dESCRIPCIONTIPOINTERES titulo=\"DESCRIPCION TIPO INTERES\" tipo=\"char\">TASA FIJA CON PAGO AL\n" +
            "                        VENCIMIENTO\n" +
            "                    </dESCRIPCIONTIPOINTERES>\n" +
            "                    <dESCRIPCIONREPORTETIPINT titulo=\"DESCRIPCION REPORTE TIPINT\" tipo=\"char\">FV - TASA FIJA CON PAGO AL\n" +
            "                        VENCIMIENTO\n" +
            "                    </dESCRIPCIONREPORTETIPINT>\n" +
            "                </fila>\n" +
            "            </dESCRIPCIONTIPINT>\n" +
            "            <dESCRIPCIONTIPOPE Titulo=\"\">\n" +
            "                <fila numero=\"1\">\n" +
            "                    <cODIGOTIPOOPERACION titulo=\"CODIGO TIPO OPERACION\" tipo=\"char\">01</cODIGOTIPOOPERACION>\n" +
            "                    <dESCRIPCIONTIPOOPERACION titulo=\"DESCRIPCION TIPO OPERACION\" tipo=\"char\">PRESTAMO\n" +
            "                    </dESCRIPCIONTIPOOPERACION>\n" +
            "                    <dESCRIPCIONREPORTETIPOPE titulo=\"DESCRIPCION REPORTE TIPOPE\" tipo=\"char\">01 - PRESTAMO\n" +
            "                    </dESCRIPCIONREPORTETIPOPE>\n" +
            "                </fila>\n" +
            "            </dESCRIPCIONTIPOPE>\n" +
            "            <dESCRIPCIONTIPING Titulo=\"\"/>\n" +
            "        </sISTEMAFINANCIERO>\n" +
            "        <cUENTASCORRIENTES Titulo=\"REHABILITACION Y CLAUSURA DE CUENTAS CORRIENTES\">\n" +
            "            <cUENTASCORRIENTES Titulo=\"\">\n" +
            "                <fila numero=\"1\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\">BANCO NACIONAL DE BOLIVIA SA</eNTIDAD>\n" +
            "                    <eSTADO titulo=\"ESTADO\" tipo=\"char\">CLAUSURADA</eSTADO>\n" +
            "                    <fECHA titulo=\"FECHA\" tipo=\"smalldatetime\">06/04/2016 00:00:00</fECHA>\n" +
            "                    <nROCIRCULAR titulo=\"NRO CIRCULAR\" tipo=\"char\"></nROCIRCULAR>\n" +
            "                    <cIUDAD titulo=\"CIUDAD\" tipo=\"char\"></cIUDAD>\n" +
            "                </fila>\n" +
            "                <fila numero=\"2\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\">CUENTA REHABILITADA</eNTIDAD>\n" +
            "                    <eSTADO titulo=\"ESTADO\" tipo=\"char\">REHABILITADA</eSTADO>\n" +
            "                    <fECHA titulo=\"FECHA\" tipo=\"smalldatetime\">15/04/2016 00:00:00</fECHA>\n" +
            "                    <nROCIRCULAR titulo=\"NRO CIRCULAR\" tipo=\"char\"></nROCIRCULAR>\n" +
            "                    <cIUDAD titulo=\"CIUDAD\" tipo=\"char\"></cIUDAD>\n" +
            "                </fila>\n" +
            "                <fila numero=\"3\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\">BANCO NACIONAL DE BOLIVIA SA</eNTIDAD>\n" +
            "                    <eSTADO titulo=\"ESTADO\" tipo=\"char\">CLAUSURADA</eSTADO>\n" +
            "                    <fECHA titulo=\"FECHA\" tipo=\"smalldatetime\">23/07/2016 00:00:00</fECHA>\n" +
            "                    <nROCIRCULAR titulo=\"NRO CIRCULAR\" tipo=\"char\"></nROCIRCULAR>\n" +
            "                    <cIUDAD titulo=\"CIUDAD\" tipo=\"char\"></cIUDAD>\n" +
            "                </fila>\n" +
            "                <fila numero=\"4\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\">CUENTA REHABILITADA</eNTIDAD>\n" +
            "                    <eSTADO titulo=\"ESTADO\" tipo=\"char\">REHABILITADA</eSTADO>\n" +
            "                    <fECHA titulo=\"FECHA\" tipo=\"smalldatetime\">05/08/2016 00:00:00</fECHA>\n" +
            "                    <nROCIRCULAR titulo=\"NRO CIRCULAR\" tipo=\"char\"></nROCIRCULAR>\n" +
            "                    <cIUDAD titulo=\"CIUDAD\" tipo=\"char\"></cIUDAD>\n" +
            "                </fila>\n" +
            "            </cUENTASCORRIENTES>\n" +
            "        </cUENTASCORRIENTES>\n" +
            "        <aFP Titulo=\"DEUDAS EN MORA POR APORTES A LAS AFPS\">\n" +
            "            <dEUDASAFPS Titulo=\"DEUDA DIRECTA\">\n" +
            "                <fila numero=\"1\">\n" +
            "                    <eNTIDADACREEDORA titulo=\"ENTIDAD ACREEDORA\" tipo=\"char\">AFP PREVISION</eNTIDADACREEDORA>\n" +
            "                    <mONTO titulo=\"MONTO\" tipo=\"money\">1,252.80</mONTO>\n" +
            "                    <pERIODOSENMORA titulo=\"PERIODOS EN MORA\" tipo=\"smalldatetime\">10/06/2011 00:00:00</pERIODOSENMORA>\n" +
            "                    <fECHAACTUALIZACION titulo=\"FECHA ACTUALIZACION\" tipo=\"smalldatetime\">10/06/2011 00:00:00\n" +
            "                    </fECHAACTUALIZACION>\n" +
            "                </fila>\n" +
            "            </dEUDASAFPS>\n" +
            "        </aFP>\n" +
            "        <cASASCOMERCIALES Titulo=\"CASAS COMERCIALES\">\n" +
            "            <cACOMER Titulo=\"CASAS COMERCIALES\">\n" +
            "                <fila numero=\"1\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\">NUEVATEL</eNTIDAD>\n" +
            "                    <fECHAINGRESO titulo=\"FECHA INGRESO\" tipo=\"smalldatetime\">01/04/2009 00:00:00</fECHAINGRESO>\n" +
            "                    <mONTO titulo=\"MONTO\" tipo=\"money\">4,019.40</mONTO>\n" +
            "                    <tIPOOBLIGADO titulo=\"TIPO OBLIGADO\" tipo=\"char\">DIRECTA</tIPOOBLIGADO>\n" +
            "                    <eSTADODEUDA titulo=\"ESTADO DEUDA\" tipo=\"char\">CASTIGADA</eSTADODEUDA>\n" +
            "                    <rEFERENCIA titulo=\"REFERENCIA\" tipo=\"char\">2517947 / 200806</rEFERENCIA>\n" +
            "                    <fECHAACTUALIZACION titulo=\"FECHA ACTUALIZACION\" tipo=\"smalldatetime\">29/02/2012 00:00:00\n" +
            "                    </fECHAACTUALIZACION>\n" +
            "                    <fECHAINICIOOPERACION titulo=\"FECHA INICIO OPERACION\" tipo=\"smalldatetime\"></fECHAINICIOOPERACION>\n" +
            "                </fila>\n" +
            "                <fila numero=\"2\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\">NUEVATEL</eNTIDAD>\n" +
            "                    <fECHAINGRESO titulo=\"FECHA INGRESO\" tipo=\"smalldatetime\">02/03/2009 00:00:00</fECHAINGRESO>\n" +
            "                    <mONTO titulo=\"MONTO\" tipo=\"money\">11,764.62</mONTO>\n" +
            "                    <tIPOOBLIGADO titulo=\"TIPO OBLIGADO\" tipo=\"char\">DIRECTA</tIPOOBLIGADO>\n" +
            "                    <eSTADODEUDA titulo=\"ESTADO DEUDA\" tipo=\"char\">CASTIGADA</eSTADODEUDA>\n" +
            "                    <rEFERENCIA titulo=\"REFERENCIA\" tipo=\"char\">2517947 / 200805</rEFERENCIA>\n" +
            "                    <fECHAACTUALIZACION titulo=\"FECHA ACTUALIZACION\" tipo=\"smalldatetime\">29/02/2012 00:00:00\n" +
            "                    </fECHAACTUALIZACION>\n" +
            "                    <fECHAINICIOOPERACION titulo=\"FECHA INICIO OPERACION\" tipo=\"smalldatetime\"></fECHAINICIOOPERACION>\n" +
            "                </fila>\n" +
            "                <fila numero=\"3\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\">NUEVATEL</eNTIDAD>\n" +
            "                    <fECHAINGRESO titulo=\"FECHA INGRESO\" tipo=\"smalldatetime\">02/02/2009 00:00:00</fECHAINGRESO>\n" +
            "                    <mONTO titulo=\"MONTO\" tipo=\"money\">4,675.94</mONTO>\n" +
            "                    <tIPOOBLIGADO titulo=\"TIPO OBLIGADO\" tipo=\"char\">DIRECTA</tIPOOBLIGADO>\n" +
            "                    <eSTADODEUDA titulo=\"ESTADO DEUDA\" tipo=\"char\">CASTIGADA</eSTADODEUDA>\n" +
            "                    <rEFERENCIA titulo=\"REFERENCIA\" tipo=\"char\">2517947 / 200804</rEFERENCIA>\n" +
            "                    <fECHAACTUALIZACION titulo=\"FECHA ACTUALIZACION\" tipo=\"smalldatetime\">29/02/2012 00:00:00\n" +
            "                    </fECHAACTUALIZACION>\n" +
            "                    <fECHAINICIOOPERACION titulo=\"FECHA INICIO OPERACION\" tipo=\"smalldatetime\"></fECHAINICIOOPERACION>\n" +
            "                </fila>\n" +
            "                <fila numero=\"4\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\">NUEVATEL</eNTIDAD>\n" +
            "                    <fECHAINGRESO titulo=\"FECHA INGRESO\" tipo=\"smalldatetime\">30/06/2009 00:00:00</fECHAINGRESO>\n" +
            "                    <mONTO titulo=\"MONTO\" tipo=\"money\">88.67</mONTO>\n" +
            "                    <tIPOOBLIGADO titulo=\"TIPO OBLIGADO\" tipo=\"char\">DIRECTA</tIPOOBLIGADO>\n" +
            "                    <eSTADODEUDA titulo=\"ESTADO DEUDA\" tipo=\"char\">CASTIGADA</eSTADODEUDA>\n" +
            "                    <rEFERENCIA titulo=\"REFERENCIA\" tipo=\"char\">2420568 / 200809</rEFERENCIA>\n" +
            "                    <fECHAACTUALIZACION titulo=\"FECHA ACTUALIZACION\" tipo=\"smalldatetime\">29/02/2012 00:00:00\n" +
            "                    </fECHAACTUALIZACION>\n" +
            "                    <fECHAINICIOOPERACION titulo=\"FECHA INICIO OPERACION\" tipo=\"smalldatetime\"></fECHAINICIOOPERACION>\n" +
            "                </fila>\n" +
            "                <fila numero=\"5\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\">NUEVATEL</eNTIDAD>\n" +
            "                    <fECHAINGRESO titulo=\"FECHA INGRESO\" tipo=\"smalldatetime\">30/06/2009 00:00:00</fECHAINGRESO>\n" +
            "                    <mONTO titulo=\"MONTO\" tipo=\"money\">190.00</mONTO>\n" +
            "                    <tIPOOBLIGADO titulo=\"TIPO OBLIGADO\" tipo=\"char\">DIRECTA</tIPOOBLIGADO>\n" +
            "                    <eSTADODEUDA titulo=\"ESTADO DEUDA\" tipo=\"char\">CASTIGADA</eSTADODEUDA>\n" +
            "                    <rEFERENCIA titulo=\"REFERENCIA\" tipo=\"char\">2670039 / 200809</rEFERENCIA>\n" +
            "                    <fECHAACTUALIZACION titulo=\"FECHA ACTUALIZACION\" tipo=\"smalldatetime\">29/02/2012 00:00:00\n" +
            "                    </fECHAACTUALIZACION>\n" +
            "                    <fECHAINICIOOPERACION titulo=\"FECHA INICIO OPERACION\" tipo=\"smalldatetime\"></fECHAINICIOOPERACION>\n" +
            "                </fila>\n" +
            "                <fila numero=\"6\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\">NUEVATEL</eNTIDAD>\n" +
            "                    <fECHAINGRESO titulo=\"FECHA INGRESO\" tipo=\"smalldatetime\">02/05/2009 00:00:00</fECHAINGRESO>\n" +
            "                    <mONTO titulo=\"MONTO\" tipo=\"money\">190.00</mONTO>\n" +
            "                    <tIPOOBLIGADO titulo=\"TIPO OBLIGADO\" tipo=\"char\">DIRECTA</tIPOOBLIGADO>\n" +
            "                    <eSTADODEUDA titulo=\"ESTADO DEUDA\" tipo=\"char\">CASTIGADA</eSTADODEUDA>\n" +
            "                    <rEFERENCIA titulo=\"REFERENCIA\" tipo=\"char\">2670039 / 200807</rEFERENCIA>\n" +
            "                    <fECHAACTUALIZACION titulo=\"FECHA ACTUALIZACION\" tipo=\"smalldatetime\">29/02/2012 00:00:00\n" +
            "                    </fECHAACTUALIZACION>\n" +
            "                    <fECHAINICIOOPERACION titulo=\"FECHA INICIO OPERACION\" tipo=\"smalldatetime\"></fECHAINICIOOPERACION>\n" +
            "                </fila>\n" +
            "                <fila numero=\"7\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\">NUEVATEL</eNTIDAD>\n" +
            "                    <fECHAINGRESO titulo=\"FECHA INGRESO\" tipo=\"smalldatetime\">02/05/2009 00:00:00</fECHAINGRESO>\n" +
            "                    <mONTO titulo=\"MONTO\" tipo=\"money\">190.00</mONTO>\n" +
            "                    <tIPOOBLIGADO titulo=\"TIPO OBLIGADO\" tipo=\"char\">DIRECTA</tIPOOBLIGADO>\n" +
            "                    <eSTADODEUDA titulo=\"ESTADO DEUDA\" tipo=\"char\">CASTIGADA</eSTADODEUDA>\n" +
            "                    <rEFERENCIA titulo=\"REFERENCIA\" tipo=\"char\">2420568 / 200807</rEFERENCIA>\n" +
            "                    <fECHAACTUALIZACION titulo=\"FECHA ACTUALIZACION\" tipo=\"smalldatetime\">29/02/2012 00:00:00\n" +
            "                    </fECHAACTUALIZACION>\n" +
            "                    <fECHAINICIOOPERACION titulo=\"FECHA INICIO OPERACION\" tipo=\"smalldatetime\"></fECHAINICIOOPERACION>\n" +
            "                </fila>\n" +
            "                <fila numero=\"8\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\">NUEVATEL</eNTIDAD>\n" +
            "                    <fECHAINGRESO titulo=\"FECHA INGRESO\" tipo=\"smalldatetime\">02/06/2009 00:00:00</fECHAINGRESO>\n" +
            "                    <mONTO titulo=\"MONTO\" tipo=\"money\">190.00</mONTO>\n" +
            "                    <tIPOOBLIGADO titulo=\"TIPO OBLIGADO\" tipo=\"char\">DIRECTA</tIPOOBLIGADO>\n" +
            "                    <eSTADODEUDA titulo=\"ESTADO DEUDA\" tipo=\"char\">CASTIGADA</eSTADODEUDA>\n" +
            "                    <rEFERENCIA titulo=\"REFERENCIA\" tipo=\"char\">2670039 / 200808</rEFERENCIA>\n" +
            "                    <fECHAACTUALIZACION titulo=\"FECHA ACTUALIZACION\" tipo=\"smalldatetime\">29/02/2012 00:00:00\n" +
            "                    </fECHAACTUALIZACION>\n" +
            "                    <fECHAINICIOOPERACION titulo=\"FECHA INICIO OPERACION\" tipo=\"smalldatetime\"></fECHAINICIOOPERACION>\n" +
            "                </fila>\n" +
            "                <fila numero=\"9\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\">NUEVATEL</eNTIDAD>\n" +
            "                    <fECHAINGRESO titulo=\"FECHA INGRESO\" tipo=\"smalldatetime\">02/06/2009 00:00:00</fECHAINGRESO>\n" +
            "                    <mONTO titulo=\"MONTO\" tipo=\"money\">190.00</mONTO>\n" +
            "                    <tIPOOBLIGADO titulo=\"TIPO OBLIGADO\" tipo=\"char\">DIRECTA</tIPOOBLIGADO>\n" +
            "                    <eSTADODEUDA titulo=\"ESTADO DEUDA\" tipo=\"char\">CASTIGADA</eSTADODEUDA>\n" +
            "                    <rEFERENCIA titulo=\"REFERENCIA\" tipo=\"char\">2420568 / 200808</rEFERENCIA>\n" +
            "                    <fECHAACTUALIZACION titulo=\"FECHA ACTUALIZACION\" tipo=\"smalldatetime\">29/02/2012 00:00:00\n" +
            "                    </fECHAACTUALIZACION>\n" +
            "                    <fECHAINICIOOPERACION titulo=\"FECHA INICIO OPERACION\" tipo=\"smalldatetime\"></fECHAINICIOOPERACION>\n" +
            "                </fila>\n" +
            "            </cACOMER>\n" +
            "        </cASASCOMERCIALES>\n" +
            "        <rECTIFICACIONES Titulo=\"RECTIFICACIONES\">\n" +
            "            <rECTIFICACION Titulo=\"\">\n" +
            "                <fila numero=\"1\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\"></eNTIDAD>\n" +
            "                    <fECHARESPALDO titulo=\"FECHA RESPALDO\" tipo=\"char\"></fECHARESPALDO>\n" +
            "                    <rESPALDO titulo=\"RESPALDO\" tipo=\"char\"></rESPALDO>\n" +
            "                    <nUMEROOPERACION titulo=\"NUMERO OPERACION\" tipo=\"char\"></nUMEROOPERACION>\n" +
            "                    <fECHADESDE titulo=\"FECHA DESDE\" tipo=\"char\"></fECHADESDE>\n" +
            "                    <fECHAHASTA titulo=\"FECHA HASTA\" tipo=\"char\"></fECHAHASTA>\n" +
            "                    <mOTIVO titulo=\"MOTIVO\" tipo=\"char\"></mOTIVO>\n" +
            "                    <dETALLENOTARECTIFICATORIA titulo=\"DETALLE NOTA RECTIFICATORIA\" tipo=\"char\">SEGUNDA PRUEBA DE\n" +
            "                        RECTIFICACION DE ENTIDAD\n" +
            "                    </dETALLENOTARECTIFICATORIA>\n" +
            "                </fila>\n" +
            "                <fila numero=\"2\">\n" +
            "                    <eNTIDAD titulo=\"ENTIDAD\" tipo=\"char\"></eNTIDAD>\n" +
            "                    <fECHARESPALDO titulo=\"FECHA RESPALDO\" tipo=\"char\"></fECHARESPALDO>\n" +
            "                    <rESPALDO titulo=\"RESPALDO\" tipo=\"char\"></rESPALDO>\n" +
            "                    <nUMEROOPERACION titulo=\"NUMERO OPERACION\" tipo=\"char\"></nUMEROOPERACION>\n" +
            "                    <fECHADESDE titulo=\"FECHA DESDE\" tipo=\"char\"></fECHADESDE>\n" +
            "                    <fECHAHASTA titulo=\"FECHA HASTA\" tipo=\"char\"></fECHAHASTA>\n" +
            "                    <mOTIVO titulo=\"MOTIVO\" tipo=\"char\"></mOTIVO>\n" +
            "                    <dETALLENOTARECTIFICATORIA titulo=\"DETALLE NOTA RECTIFICATORIA\" tipo=\"char\">PRUEBA DE RECTIFICACION\n" +
            "                        DE ENTIDAD\n" +
            "                    </dETALLENOTARECTIFICATORIA>\n" +
            "                </fila>\n" +
            "            </rECTIFICACION>\n" +
            "        </rECTIFICACIONES>\n" +
            "        <formatoHistorial Titulo=\"HISTORIAL DE DEUDA\">\n" +
            "            <vwSaldosPersonaDeudaDirecta Titulo=\"HISTORIAL DE DEUDA DIRECTA\">\n" +
            "                <fila numero=\"1\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">DIC</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">5</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">0.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"2\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">NOV</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">37,044.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"3\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">OCT</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">37,098.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"4\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">SEP</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">37,098.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"5\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">AGO</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">37,098.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"6\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">JUL</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">37,098.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"7\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">JUN</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">41,903.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"8\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">MAY</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">42,170.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"9\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">ABR</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">42,328.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"10\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">MAR</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">42,459.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"11\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">FEB</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">42,766.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"12\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">ENE</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">43,039.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "            </vwSaldosPersonaDeudaDirecta>\n" +
            "            <vwSaldosPersonaDeudaIndirecta Titulo=\"HISTORIAL DE DEUDA INDIRECTA\">\n" +
            "                <fila numero=\"1\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">DIC</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">5</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">0.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"2\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">NOV</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">5</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">0.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"3\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">OCT</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">5</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">0.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"4\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">SEP</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">20,465.45</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"5\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">AGO</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">20,465.45</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"6\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">JUL</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">20,465.45</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"7\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">JUN</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">20,465.45</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"8\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">MAY</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">3</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">20,465.45</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"9\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">ABR</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">5</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">0.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"10\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">MAR</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">5</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">0.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"11\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">FEB</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">5</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">0.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "                <fila numero=\"12\">\n" +
            "                    <aBREVIATURA titulo=\"MES\" tipo=\"nvarchar\">ENE</aBREVIATURA>\n" +
            "                    <eSTADO1 titulo=\"ESTADO 2014\" tipo=\"nvarchar\">5</eSTADO1>\n" +
            "                    <sALDO1 titulo=\" SALDO 2014\" tipo=\"money\">0.00</sALDO1>\n" +
            "                    <eSTADO2 titulo=\"ESTADO 2013\" tipo=\"nvarchar\">5</eSTADO2>\n" +
            "                    <sALDO2 titulo=\" SALDO 2013\" tipo=\"money\">0.00</sALDO2>\n" +
            "                    <eSTADO3 titulo=\"ESTADO 2012\" tipo=\"nvarchar\">5</eSTADO3>\n" +
            "                    <sALDO3 titulo=\" SALDO 2012\" tipo=\"money\">0.00</sALDO3>\n" +
            "                    <eSTADO4 titulo=\"ESTADO 2011\" tipo=\"nvarchar\">5</eSTADO4>\n" +
            "                    <sALDO4 titulo=\" SALDO 2011\" tipo=\"money\">0.00</sALDO4>\n" +
            "                    <eSTADO5 titulo=\"ESTADO 2010\" tipo=\"nvarchar\">5</eSTADO5>\n" +
            "                    <sALDO5 titulo=\" SALDO 2010\" tipo=\"money\">0.00</sALDO5>\n" +
            "                </fila>\n" +
            "            </vwSaldosPersonaDeudaIndirecta>\n" +
            "        </formatoHistorial>\n" +
            "    </datosFinancieros>\n" +
            "    <cONSULTASREALIZADAS>\n" +
            "        <cONSULTASREALIZADAS>\n" +
            "            <iNSTITUCION Titulo=\"INSTITUCION\">BANCO BISA SA</iNSTITUCION>\n" +
            "            <fECHA Titulo=\"FECHA\">04/05/2017 13:14:29</fECHA>\n" +
            "            <cONSULTA Titulo=\"CONSULTA\">Infocred+</cONSULTA>\n" +
            "        </cONSULTASREALIZADAS>\n" +
            "    </cONSULTASREALIZADAS>\n" +
            "    <notas titulo=\"NOTAS\">\n" +
            "        <fECHAACTUALIZACION titulo=\"FECHA ACTUALIZACION\">DATO QUE INDICA LA FECHA DE LA INFORMACION REPORTADA.\n" +
            "        </fECHAACTUALIZACION>\n" +
            "        <fECHAPROCESAMIENTO titulo=\"FECHA PROCESAMIENTO\">DATO QUE INDICA LA FECHA DE PROCESAMIENTO (CARGA) DE LA\n" +
            "            INFORMACION.\n" +
            "        </fECHAPROCESAMIENTO>\n" +
            "        <sISTEMA titulo=\"SISTEMA\">(R): ENTIDAD REGULADA POR LA ASFI. (N): ENTIDAD NO REGULADA POR LA ASFI.</sISTEMA>\n" +
            "        <hISTORICO titulo=\"HISTORICO\">MUESTRA EL COMPORTAMIENTO HISTORICO DE LOS ULTIMOS 18 MESES, EL HISTORICO SE DEBE\n" +
            "            LEER DE IZQUIERDA A DERECHA DONDE EL PRIMER REGISTRO MUESTRA EL MES ACTUAL, EL SEGUNDO MUESTRA EL MES\n" +
            "            ANTERIOR Y ASI SUCESIVAMENTE. LOS NUMEROS QUE TIENE EL HISTORICO SE INTERPRETA DE LA SIGUIENTE MANERA: UNO\n" +
            "            (1) EL TITULAR ESTUVO VIGENTE ESE MES, DOS (2) EL TITULAR ESTUVO VENCIDO ESE MES, TRES (3) EL TITULAR ESTUVO\n" +
            "            EN EJECUCION ESE MES, CUATRO (4) EL TITULAR ESTUVO CON DEUDA CASTIGADA ESE MES Y CINCO (5) NO SE TIENEN\n" +
            "            DATOS DE ESE MES.\n" +
            "        </hISTORICO>\n" +
            "        <cAEDEC titulo=\"CAEDEC\">CORRESPONDE AL CODIGO ASIGNADO POR LA ASFI PARA EL DESTINO DE CREDITO.</cAEDEC>\n" +
            "        <aCTECONO titulo=\"ACT. ECONO.\">CORRESPONDE AL CODIGO ASIGNADO PARA LA ACTIVIDAD ECONOMICA DEL TITULAR.\n" +
            "        </aCTECONO>\n" +
            "        <rEHABILITACIONYCLAUSURADECUENTASCORRIENTES titulo=\"REHABILITACION Y CLAUSURA DE CUENTAS CORRIENTES\">EN LA\n" +
            "            INFORMACION DE REHABILITACION DE CUENTAS , LA FUENTE(ASFI) NO CONSIGNA LA ENTIDAD.\n" +
            "        </rEHABILITACIONYCLAUSURADECUENTASCORRIENTES>\n" +
            "        <rECTIFICACIONES titulo=\"RECTIFICACIONES\">DETALLE DE RECTIFICACIONES POR ERROR DE LA ENTIDAD REPORTANTE.\n" +
            "        </rECTIFICACIONES>\n" +
            "        <mONTO titulo=\"MONTO\">TODOS LOS MONTOS, CON EXCEPCIÓN DE MONTO ORIGINAL, ESTAN EXPRESADOS EN BOLIVIANOS A LA\n" +
            "            FECHA DE CORTE DE MES.\n" +
            "        </mONTO>\n" +
            "        <mONTOORIGINAL titulo=\"MONTO ORIGINAL\">EXPRESADOS EN LA MONEDA ORIGINAL DEL CREDITO (MN) MONEDA NACIONAL, (ME)\n" +
            "            MONEDA EXTRANJERA, (CMV) MONEDA NACIONAL CON MANTENIMIENTO DE VALOR Y (UFV) MONEDA NACIONAL UNIDAD FOMENTO A\n" +
            "            LA VIVIENDA.\n" +
            "        </mONTOORIGINAL>\n" +
            "        <nOTAS titulo=\"NOTAS\">TÓMESE EN CUENTA QUE LA INFORMACIÓN DE BANCA COMUNAL DE INSTITUCIONES FINANCIERAS DE\n" +
            "            DESARROLLO (IFDS) QUE CUENTAN CON LICENCIA DE FUNCIONAMIENTO DE LA AUTORIDAD DE SUPERVISIÓN DEL SISTEMA\n" +
            "            FINANCIERO (ASFI) ES PROVISTA POR LA CENTRAL DE INFORMACIÓN CREDITICIA (CIC) EN EL MARCO DE LA NORMATIVA\n" +
            "            VIGENTE: LIBRO 3, TITULO II, CAPITULO II, SECCIÓN 3, ARTICULO 12° Y LIBRO 3, TITULO II, CAPITULO II, SECCIÓN\n" +
            "            3, ARTICULO 12°, INCISO O).\n" +
            "        </nOTAS>\n" +
            "        <aCLARACION titulo=\"ACLARACION\">ESTE REPORTE ESTA SUJETO AL SECRETO BANCARIO CONFORME CON LA LEY 393 VIGENTE A\n" +
            "            PARTIR DEL 20 DE NOVIEMBRE DE 2013.\n" +
            "        </aCLARACION>\n" +
            "    </notas>\n" +
            "</titular>";
}
