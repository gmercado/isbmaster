package utilitarios;

import bus.consumoweb.infocred.utilitarios.DomUtil;
import com.google.common.base.Strings;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by gmercado on 05/05/2017.
 */
public class DomUtilTest {
    File ruta = new File("D:\\InfoCred_Docs\\UltimasAlertas\\19_NuevaConsultaAjena_217324033_108889_20174620713.xls");
    @Test
    public void init() {
        try {
            List<Map<Integer,String>> cuerpo = obtenerCuerpo(ruta.getPath(), 9);
            for (Map<Integer, String> datos : cuerpo) {
                for (Integer llave : datos.keySet()) {
                    System.out.println(" ===>> " + datos.get(llave));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String obtenerCabeceras(String path) throws ParserConfigurationException, IOException, SAXException {
        return DomUtil.obtenerCabeceras(path);
    }

    public List<Map<Integer,String>> obtenerCuerpo(String path, int inicioCuerpo) throws ParserConfigurationException, IOException, SAXException {
        return DomUtil.obtenerCuerpo(path, inicioCuerpo);
    }
}
