package bus.plumbing.utils;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author by rsalvatierra on 06/06/2017.
 */
public class NumberToLetterTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void convertNumberToLetter() throws Exception {
        String texto = NumberToLetter.convertNumberToLetter("1638260,28");
        logger.info(texto);
        assert texto != null;
    }

}