package bus.consumoweb.clavemovil;

import bus.consumoweb.aqua.ConsumidorServiciosWebAqua;
import bus.consumoweb.dao.ConsumoWebDao;
import bus.env.api.MedioAmbiente;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static bus.env.api.Variables.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * @author by rsalvatierra on 26/06/2017.
 */
public class ClaveMovilDaoTest {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void claveMovil() throws Exception {
        final ConsumoWebDao consumoWebDao = Mockito.mock(ConsumoWebDao.class);
        final MedioAmbiente medioAmbiente = Mockito.mock(MedioAmbiente.class);

        when(medioAmbiente.getValorIntDe(eq(AQUA_PORT), eq(AQUA_PORT_DEFAULT))).thenReturn(AQUA_PORT_DEFAULT);
        when(medioAmbiente.getValorDe(eq(AQUA_HOST), eq(AQUA_HOST_DEFAULT))).thenReturn(AQUA_HOST_DEFAULT);
        when(medioAmbiente.getValorDe(eq(AQUA_USERNAME), eq(AQUA_USERNAME_DEFAULT))).thenReturn(AQUA_USERNAME_DEFAULT);
        when(medioAmbiente.getValorDe(eq(AQUA_PASSWORD), eq(AQUA_PASSWORD_DEFAULT))).thenReturn(AQUA_PASSWORD_DEFAULT);
        when(medioAmbiente.getValorDe(eq(AQUA_PATH), eq(AQUA_PATH_DEFAULT))).thenReturn(AQUA_PATH_DEFAULT);

        final ExecutorService executorService = Executors.newSingleThreadExecutor();
        ConsumidorServiciosWebAqua consumidorServiciosWeb =
                new ConsumidorServiciosWebAqua(medioAmbiente, executorService, consumoWebDao);
        ClaveMovilDao claveMovilDao = new ClaveMovilDao(consumidorServiciosWeb, medioAmbiente);

        logger.info(claveMovilDao.claveMovil("59177784000").getEstado());
        executorService.shutdown();

    }

}