package bus.ws.support;

import com.bisa.isb.ws.support.AuthenticationException;
import com.bisa.isb.ws.support.HttpAuthentication;
import com.bisa.isb.ws.support.UnsupportedFeatureException;
import com.google.common.base.Splitter;
import org.apache.http.auth.Credentials;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

/**
 * @author Miguel Vega
 * @version $Id: HttpAuthenticationTest.java; sep 18, 2015 09:39 AM mvega $
 */
public class HttpAuthenticationTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testCheckSingleHeaderValue() {

        final Iterator<String> values = Splitter.on(" ").trimResults().omitEmptyStrings().split("Basic aHR0cHdhdGNoOmY= ").iterator();
        values.forEachRemaining(s -> logger.info("SPPLITTED = {}", s));

        //Something like> Authorization: Basic aHR0cHdhdGNoOmY=
        String authHeader = "Basic aHR0cHdhdGNoOmY=";

        try {
            HttpAuthentication<Credentials> auth = HttpAuthentication.forAuthorizationHeader(authHeader);

            Credentials cred = auth.authenticate(credentials -> credentials);

            logger.info("User:" + cred.getUserPrincipal().getName() + ", Password=" + cred.getPassword());

        } catch (UnsupportedFeatureException | AuthenticationException e) {
            throw new IllegalStateException("This should never have to happen");
        }
    }
}