package bus.ws.support;

import java.math.BigDecimal;

/**
 * @author Miguel Vega
 * @version $Id: AritmethicOp.java; sep 17, 2015 05:29 PM mvega $
 */
public abstract class ArithmeticOp {
    public abstract BigDecimal evaluate(BigDecimal... input);
}
