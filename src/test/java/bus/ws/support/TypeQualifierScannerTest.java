package bus.ws.support;

import org.junit.Test;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jws.WebService;
import java.util.Set;

/**
 * @author Miguel Vega
 * @version $Id: TypeQualifierScannerTest.java; sep 16, 2015 09:45 AM mvega $
 */
public class TypeQualifierScannerTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testSearchnnotatedClasses() {
        Reflections reflections = new Reflections("");
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(WebService.class);
        annotated.forEach(x -> logger.info("Class found is {}", x));
    }

    @Test
    public void testSearchSubclasses() {
        Reflections reflections = new Reflections("bus.ws.support");
        Set<Class<? extends ArithmeticOp>> subTypesOf = reflections.getSubTypesOf(ArithmeticOp.class);
        subTypesOf.forEach(x -> logger.info("Operation found is {}", x));
    }


}