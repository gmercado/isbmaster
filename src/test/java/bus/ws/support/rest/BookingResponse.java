package bus.ws.support.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Miguel Vega
 * @version $Id: GetBookingResult.java; nov 12, 2015 12:34 PM mvega $
 */
public class BookingResponse {
    public GetBookingResult getBookingResult() {
        return bookingResult;
    }

    public void setBookingResult(GetBookingResult bookingResult) {
        this.bookingResult = bookingResult;
    }

    @JsonProperty("GetBookingResult")
    private GetBookingResult bookingResult;
}
