package bus.ws.support.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Miguel Vega
 * @version $Id: BOAResponse.java; nov 05, 2015 11:54 AM mvega $
 */

public class BOAResponse {
    public String getTokenResult() {
        return tokenResult;
    }

    public void setTokenResult(String tokenResult) {
        this.tokenResult = tokenResult;
    }

    @JsonProperty("GetTokenResult")
    private String tokenResult;
}
