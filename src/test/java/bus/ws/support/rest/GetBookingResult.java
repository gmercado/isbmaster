package bus.ws.support.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Miguel Vega
 * @version $Id: GetBookingResponse.java; nov 12, 2015 12:38 PM mvega $
 */

public class GetBookingResult {

    @JsonProperty("reserva")
    private Reserva reserva;

    public Reserva getReserva() {
        return reserva;
    }

    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }
}
