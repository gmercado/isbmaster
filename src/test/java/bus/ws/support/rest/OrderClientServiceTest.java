package bus.ws.support.rest;

import com.bisa.isb.ws.services.model.Order;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.transport.http.HTTPConduit;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: OrderClientServiceTest.java; sep 22, 2015 05:44 PM mvega $
 */
public class OrderClientServiceTest {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Before
    public void setup() throws Exception {
//        applyTrsutStore();
        //avoid spring dependencies
        System.setProperty("org.apache.cxf.useSpringClassHelpers", "false");

        System.setProperty("javax.net.ssl.trustStore", "C:/Java/jdk1.8.0_25/jre/lib/security/cacerts");
        System.setProperty("javax.net.ssl.trustStorePassword ", "changeit");
        System.setProperty("javax.net.ssl.trustStore", "C:/Java/jdk1.8.0_25/jre/lib/security/cacerts");
        System.setProperty("javax.net.ssl.keyPassword", "changeit");

        logger.info("-------------------------->{}", System.getProperty("javax.net.ssl.trustStore"));
        logger.info("-------------------------->{}", System.getProperty("javax.net.ssl.keyPassword"));
    }

    @Test
    public void testSSLConsume() throws Exception {

        List<Object> providers = new ArrayList<Object>();

        providers.add(new JacksonJaxbJsonProvider());
        WebClient client = WebClient.create("https://127.0.0.1:8443/rest", providers);

        HTTPConduit conduit = WebClient.getConfig(client).getHttpConduit();
        conduit.getClient().setReceiveTimeout(1000000);
        conduit.getClient().setConnectionTimeout(1000000);
        //conduit.getTlsClientParameters()
        if (true) {

            TLSClientParameters params =
                    conduit.getTlsClientParameters();

            if (params == null) {
                params = new TLSClientParameters();
                conduit.setTlsClientParameters(params);
            }

            params.setTrustManagers(new TrustManager[]{new FakeTrustManager()});

            params.setDisableCNCheck(true);
        }

        client = client.accept("application/json").type("application/json").path("/order").query("id", "1");

        Order order = client.get(Order.class);

        System.out.println("Order:" + order.getCustomerName());

    }

    private void applyTrsutStore() throws Exception {
        TrustManagerFactory tmf = TrustManagerFactory
                .getInstance(TrustManagerFactory.getDefaultAlgorithm());
// Using null here initialises the TMF with the default trust store.
        tmf.init((KeyStore) null);

// Get hold of the default trust manager
        X509TrustManager defaultTm = null;
        for (TrustManager tm : tmf.getTrustManagers()) {
            if (tm instanceof X509TrustManager) {
                defaultTm = (X509TrustManager) tm;
                break;
            }
        }

        FileInputStream myKeys = new FileInputStream("C:/Java/jdk1.8.0_25/jre/lib/security/cacerts");

// Do the same with your trust store this time
// Adapt how you load the keystore to your needs
        KeyStore myTrustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        myTrustStore.load(myKeys, "changeit".toCharArray());

        myKeys.close();

        tmf = TrustManagerFactory
                .getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(myTrustStore);

// Get hold of the default trust manager
        X509TrustManager myTm = null;
        for (TrustManager tm : tmf.getTrustManagers()) {
            if (tm instanceof X509TrustManager) {
                myTm = (X509TrustManager) tm;
                break;
            }
        }

// Wrap it in your own class.
        final X509TrustManager finalDefaultTm = defaultTm;
        final X509TrustManager finalMyTm = myTm;
        X509TrustManager customTm = new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                // If you're planning to use client-cert auth,
                // merge results from "defaultTm" and "myTm".
                return finalDefaultTm.getAcceptedIssuers();
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
                try {
                    finalMyTm.checkServerTrusted(chain, authType);
                } catch (CertificateException e) {
                    // This will throw another CertificateException if this fails too.
                    finalDefaultTm.checkServerTrusted(chain, authType);
                }
            }

            @Override
            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
                // If you're planning to use client-cert auth,
                // do the same as checking the server.
                finalDefaultTm.checkClientTrusted(chain, authType);
            }
        };


        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, new TrustManager[]{customTm}, null);

// You don't have to set this as the default context,
// it depends on the library you're using.
        SSLContext.setDefault(sslContext);
    }

}
