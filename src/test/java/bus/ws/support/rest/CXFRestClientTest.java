package bus.ws.support.rest;

import com.bisa.isb.ws.services.model.Book;
import org.apache.cxf.jaxrs.client.WebClient;
import org.junit.Before;
import org.junit.Test;

import javax.net.ssl.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * @author Miguel Vega
 * @version $Id: CXFRestClientTest.java; sep 21, 2015 10:49 AM mvega $
 * @see http://www.informit.com/guides/content.aspx?g=java&seqNum=599
 */
public class CXFRestClientTest {

    @Before
    public void inti() throws NoSuchAlgorithmException, KeyManagementException {
        //avoid spring dependencies
        System.setProperty("org.apache.cxf.useSpringClassHelpers", "false");

        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    context.getSocketFactory());
        } catch (Exception e) { // should never happen
            e.printStackTrace();
        }

        /*
        //trust any certfica
        TrustManager tm = new X509TrustManager() {
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        };

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, new TrustManager[] { tm }, null);
        */
    }

    @Test
    public void testTempratureService() {

        String bookId = "001";

        // Create a WebClient pointing to the base URL of the RESTful web service
        WebClient client = WebClient.create("https://localhost:8443/rest/bookshelf/books");

        // Set the path from which we wish to get the object, request XML, and use JAXB
        // to convert the response to a Book object
//        Book book = client.path( "book/" + bookId ).accept( "text/xml" ).get( Book.class );
        Book book = client.path("/" + bookId).accept("text/xml").get(Book.class);

        System.out.println("---->" + book.getBookName());
    }

    @Test
    public void testBookshelfService() {
        Client client = ClientBuilder.newBuilder().newClient();
        WebTarget target = client.target("https://localhost:8443/rest/bookshelf/books");
//        target = target.path("service").queryParam("a", "avalue");
        target = target.path("001");

        Invocation.Builder builder = target.request();
        Response response = builder.get();
        Book book = builder.get(Book.class);

        System.out.println("......" + book.getBookName());
    }
}