package bus.ws.support.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author Miguel Vega
 * @version $Id: GetBookingTest.java; nov 12, 2015 04:28 PM mvega $
 */
public class GetBookingTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testPureJSON() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(readFile());

        JsonNode tipoReserva = root.path("GetBookingResult").path("reserva").path("hora_creacion").path("#text");

        logger.info("Tipo Reserva...{}", tipoReserva.asText());

        String tipReserva = root.at("/GetBookingResult/reserva/responsable/tipo_reserva").toString();

        logger.info("Tipo Reserva 2...{}", tipReserva);
    }

    @Test
    public void testJSON2Pojo() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        //By default all fields without explicit view definition are included, disable this
//        mapper.configure(SerializationConfig.Feature.DEFAULT_VIEW_INCLUSION, false);

        String json = readFile();

        GetBookingResult gbr = mapper.readValue(json, GetBookingResult.class);

        logger.info("GetBookingResult....{}", gbr);
    }

    private String readFile(){
        try {
            return new String(Files.readAllBytes(
                    Paths.get((ClassLoader.getSystemResource("boa/ReservaV2respuesta.json").toURI()))));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
        }
    }
}
