package bus.ws.support.rest;

/**
 * @author Miguel Vega
 * @version $Id: FakeTrsutStore.java; sep 22, 2015 08:30 AM mvega $
 */
public class FakeTrustManager implements javax.net.ssl.X509TrustManager {
    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
        return null;
    }
    public void checkClientTrusted(java.security.cert.X509Certificate[] certs,
                                   String authType) {
    }
    public void checkServerTrusted(java.security.cert.X509Certificate[] certs,
                                   String authType) {
    }
}
