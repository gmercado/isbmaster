package bus.ws.support.rest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Miguel Vega
 * @version $Id: GetTokenObj.java; nov 05, 2015 10:03 AM mvega $
 */
@XmlRootElement
public class GetTokenObj {
    @XmlElement
    private String credentials;
    @XmlElement
    private String ipAddress;
    @XmlElement
    private String lang;

    @XmlElement
    private boolean xmlOrJson = false;

    @XmlElement
    private Locator locator;
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Locator getLocator() {
        return locator;
    }

    public void setLocator(Locator locator) {
        this.locator = locator;
    }

    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    public boolean isXmlOrJson() {
        return xmlOrJson;
    }

    public void setXmlOrJson(boolean xmlOrJson) {
        this.xmlOrJson = xmlOrJson;
    }

    public static class Locator{
        @XmlElement
        private String pnr;
        @XmlElement
        private String identifierPnr;

        public String getPnr() {
            return pnr;
        }

        public void setPnr(String pnr) {
            this.pnr = pnr;
        }

        public String getIdentifierPnr() {
            return identifierPnr;
        }

        public void setIdentifierPnr(String identifierPnr) {
            this.identifierPnr = identifierPnr;
        }
    }
}
