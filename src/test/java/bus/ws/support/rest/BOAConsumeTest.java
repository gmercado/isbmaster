package bus.ws.support.rest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.google.common.collect.ImmutableList;
import org.apache.cxf.jaxrs.client.WebClient;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

//import org.codehaus.jackson.map.ObjectMapper;

/**
 * @author Miguel Vega
 * @version $Id: BOAConsumeTest.java; nov 05, 2015 10:14 AM mvega $
 */
public class BOAConsumeTest {

    @Before
    public void init() throws NoSuchAlgorithmException, KeyManagementException {
        //avoid spring dependencies
        System.setProperty("org.apache.cxf.useSpringClassHelpers", "false");

        String host = "127.0.0.1";
        String port = "3241";

        System.setProperty("http.proxyHost", host);
        System.setProperty("http.proxyPort", port);
        System.setProperty("https.proxyHost", host);
        System.setProperty("https.proxyPort", port);
    }

    @Test
    public void simpleCOnsumeBOA() {

        GetTokenObj gto = new GetTokenObj();
        gto.setCredentials("{27298685-77a2-48ea-bf37-8607efd36401}{6747e485-9f0c-442b-92c7-83657239f50c}");
        gto.setIpAddress("186.121.212.154");
        gto.setLang("ES");

        GetTokenObj.Locator locator = new GetTokenObj.Locator();
        locator.setIdentifierPnr("PRUEBA");
        locator.setPnr("J5NJ9");
        gto.setLocator(locator);

        WebClient wc = WebClient.create("http://skbpruebas.cloudapp.net/ServicesVPN/Token.svc/GetToken",
                ImmutableList.of(new JacksonJaxbJsonProvider()));

        Response post = wc.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).post(gto);

        if(post.getStatus()!=200){
            throw new IllegalStateException("Excpected a 200 HTTP status code");
        }

        String jsonString = post.readEntity(String.class);
        System.out.println("RESPOSNE is: "+ jsonString);

        //convert result into Object
        // Create a JaxBContext
        try {
            BOAResponse boaResponse = new ObjectMapper().readValue(jsonString, BOAResponse.class);

            System.out.println("----------------------|"+boaResponse.getTokenResult());
        }catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testReadReserva2JSONResponse() throws URISyntaxException, IOException {

        String jsonString = null;

        jsonString = new String(Files.readAllBytes(
                Paths.get((ClassLoader.getSystemResource("boa/ReservaV2respuesta.json").toURI()))));

//        System.out.println("RESPONSE is: "+ jsonString);

        //convert result into Object
        // Create a JaxBContext
        try {
            BookingResponse resBooking = new ObjectMapper().readValue(jsonString, BookingResponse.class);

            System.out.println("----------------------|" + resBooking.getBookingResult().getReserva());
        }catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
