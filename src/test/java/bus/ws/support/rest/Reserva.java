package bus.ws.support.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Miguel Vega
 * @version $Id: Reserva.java; nov 12, 2015 12:43 PM mvega $
 */
public class Reserva {

    @JsonProperty("fecha_creacion")
    private String fechaCreacion;

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
}
