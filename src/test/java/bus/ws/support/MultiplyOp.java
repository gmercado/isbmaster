package bus.ws.support;

import com.bisa.isb.api.AKA;

import java.math.BigDecimal;

import static java.util.Arrays.stream;

/**
 * @author Miguel Vega
 * @version $Id: SumOp.java; sep 17, 2015 05:30 PM mvega $
 */
@AKA("Multiply")
public class MultiplyOp extends ArithmeticOp implements Parseable<BigDecimal> {

    @Override
    public BigDecimal evaluate(BigDecimal... input) {
        BigDecimal result = BigDecimal.ONE;
        stream(input).forEach(number -> result.multiply(number));
        return result;
    }

    @Override
    public String function(BigDecimal bigDecimal) {
        return bigDecimal.toPlainString();
    }
}
