package bus.ws.support;

import java.math.BigDecimal;

import static java.util.Arrays.stream;

/**
 * @author Miguel Vega
 * @version $Id: SumOp.java; sep 17, 2015 05:30 PM mvega $
 */
public class SumOp extends ArithmeticOp {
    @Override
    public BigDecimal evaluate(BigDecimal... input) {
        BigDecimal result = BigDecimal.ZERO;
        stream(input).forEach(number -> result.add(number));
        return result;
    }
}
