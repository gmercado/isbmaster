package bus.ws.support;

import com.bisa.isb.api.AKA;

/**
 * @author Miguel Vega
 * @version $Id: Operable.java; sep 17, 2015 05:28 PM mvega $
 */
@AKA("Dummy name")
public interface Parseable<U> {
    String function(U u);
}
