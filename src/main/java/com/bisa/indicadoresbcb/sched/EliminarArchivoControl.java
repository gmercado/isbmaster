package com.bisa.indicadoresbcb.sched;

import bus.plumbing.wicket.BisaWebApplication;
import com.bisa.indicadoresbcb.dao.CotizacionBolsinDao;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.Application;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by ccalle on 06/03/2017.
 */
public class EliminarArchivoControl implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(EliminarArchivoControl.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Iniciando tarea de limpieza");
        eliminarArchivosControl();
        LOGGER.info("Tarea finalizada tarea de de limpieza");
    }
    public void eliminarArchivosControl(){
        File fileToDelete = FileUtils.getFile(CotizacionBolsinDao.BCB_COTIZACIONES_WARNING_FILE);
        if(FileUtils.deleteQuietly(fileToDelete)){
            LOGGER.info("Se ha eliminado el archivo de control registro de cotizaciones, {}",CotizacionBolsinDao
                    .BCB_COTIZACIONES_WARNING_FILE);
        }
        fileToDelete = FileUtils.getFile(CotizacionBolsinDao.BCB_COTIZACIONES_HORA_TOPE_FILE);
        if(FileUtils.deleteQuietly(fileToDelete)){
            LOGGER.info("Se ha eliminado el archivo de control tope registro de cotizaciones, {}",CotizacionBolsinDao
                    .BCB_COTIZACIONES_HORA_TOPE_FILE);
        }

    }
}
