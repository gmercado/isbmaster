package com.bisa.indicadoresbcb.sched;

import com.bisa.indicadoresbcb.dao.CotizacionBolsinDao;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Esto normalmente corre los días jueves por la madrugada
 * se solicita tasa tre con la fecha actual
 * Created by ccalle on 07/04/2017.
 */
public class PreRegTasaTreDiaDosJob implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(PreRegTasaTreDiaDosJob.class);

    private final CotizacionBolsinDao cotizacionBolsinDao;

    @Inject
    public PreRegTasaTreDiaDosJob(CotizacionBolsinDao cotizacionBolsinDao) {
        this.cotizacionBolsinDao = cotizacionBolsinDao;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        intentarRegistro();
    }
    protected void intentarRegistro(){
        Date fechaActual=cotizacionBolsinDao.getFechaActualTap001();
        LOGGER.info("Intentado registro para la fecha: {}",fechaActual);
        if(fechaActual!=null){
            try{
                cotizacionBolsinDao.intentarRegistrarTRE(fechaActual);
            } catch (Exception e) {
                LOGGER.error("Error al intentar registro de tasa tre",e);
            }
        }else{
            LOGGER.error("Error, no se pudo obtener la fecha actual de la Tap001");
        }

    }

}