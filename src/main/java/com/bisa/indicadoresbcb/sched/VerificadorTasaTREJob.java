package com.bisa.indicadoresbcb.sched;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.mail.api.MailerFactory;
import com.bisa.indicadoresbcb.dao.CotizacionBolsinDao;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;
import java.util.Date;

/**
 * Created by ccalle on 07/04/2017.
 */
public class VerificadorTasaTREJob implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(VerificadorTasaTREJob.class);
    private final MedioAmbiente medioAmbiente;
    private final MailerFactory mailerFactory;
    private final CotizacionBolsinDao cotizacionBolsinDao;

    @Inject
    public VerificadorTasaTREJob(MedioAmbiente medioAmbiente, MailerFactory mailerFactory,CotizacionBolsinDao cotizacionBolsinDao) {
        this.medioAmbiente = medioAmbiente;
        this.mailerFactory = mailerFactory;
        this.cotizacionBolsinDao = cotizacionBolsinDao;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.debug("Iniciando tarea");
        Date fechaActual=cotizacionBolsinDao.getFechaActualTap001();
        cotizacionBolsinDao.verificarRegistroTRE(fechaActual);
        LOGGER.debug("Tarea finalizada tarea");
    }


}