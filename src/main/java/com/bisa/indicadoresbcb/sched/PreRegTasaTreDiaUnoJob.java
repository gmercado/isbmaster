package com.bisa.indicadoresbcb.sched;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.mail.api.MailerFactory;
import com.bisa.indicadoresbcb.dao.CotizacionBolsinDao;
import com.google.inject.Inject;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;
import java.util.Date;

/**
 * Esto normalmente corre los días miercoles por la tarde/noche
 * se solicita tasa tre con la fecha actual + 1
 * Created by ccalle on 07/04/2017.
 */
public class PreRegTasaTreDiaUnoJob implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(PreRegTasaTreDiaUnoJob.class);

    private final CotizacionBolsinDao cotizacionBolsinDao;

    @Inject
    public PreRegTasaTreDiaUnoJob(CotizacionBolsinDao cotizacionBolsinDao) {
        this.cotizacionBolsinDao = cotizacionBolsinDao;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        intentarRegistro();
    }
    protected void intentarRegistro(){
        Date fechaActual=cotizacionBolsinDao.getFechaActualTap001();
        DateTime dateTime = new DateTime(fechaActual);
        dateTime=dateTime.plusDays(1);
        Date siguenteDia=dateTime.toDate();
        LOGGER.info("Intentado registro para la fecha(manana): {}",siguenteDia);
        if(fechaActual!=null){
            try{
                cotizacionBolsinDao.intentarRegistrarTRE(siguenteDia);
            } catch (Exception e) {
                LOGGER.error("Error al intentar registro de tasa tre",e);
            }
        }else{
            LOGGER.error("Error, no se pudo obtener la fecha actual de la Tap001");
        }

    }

}
