package com.bisa.indicadoresbcb.sched;

import bus.env.api.MedioAmbiente;
import bus.mail.api.MailerFactory;
import bus.plumbing.wicket.BisaWebApplication;
import com.bisa.indicadoresbcb.dao.CotizacionBolsinDao;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.Application;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ccalle on 02/02/2017.
 */
public class TipoDeCambioJob implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(TipoDeCambioJob.class);
    private final MedioAmbiente medioAmbiente;
    private final MailerFactory mailerFactory;
    private final CotizacionBolsinDao cotizacionBolsinDao;

    @Inject
    public TipoDeCambioJob(MedioAmbiente medioAmbiente, MailerFactory mailerFactory,CotizacionBolsinDao cotizacionBolsinDao) {
        this.medioAmbiente = medioAmbiente;
        this.mailerFactory = mailerFactory;
        this.cotizacionBolsinDao = cotizacionBolsinDao;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.debug("Iniciando tarea");
        intentoDeRegistro();
        LOGGER.debug("Tarea finalizada tarea");
    }
    protected void intentoDeRegistro(){
        Date fechaActual=cotizacionBolsinDao.getFechaActualTap001();
        LOGGER.info("fechaActual: {}",fechaActual);
        if(fechaActual!=null){ 
            Date siguienteDiaHabil=cotizacionBolsinDao.siguienteDiaHabil(fechaActual);
            try{
                cotizacionBolsinDao.intentarRegistrar(siguienteDiaHabil);
            } catch (Exception e) {
                LOGGER.error("Error al intentar registro de cotizaciones",e);
            }
        }else{
            LOGGER.error("Error, no se pudo obtener la fecha actual de la Tap001");
        }

    }

}
