package com.bisa.indicadoresbcb.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by ccalle on 06/02/2017.
 */
@Entity
@Table(name = "GLP701")
public class CotizacionBolsin implements Serializable {
    @Id
    @Column(name = "GCNRSEC", columnDefinition = "NUMERIC(6)")
    private Long id;

    @Column(name = "GCFEEF", columnDefinition = "DECIMAL(8)")
    private String fechaEfectiva;

    @Column(name = "GCHOEF", columnDefinition = "DECIMAL(8)")
    private String horaEfectiva;

    @Column(name = "GCEURO", columnDefinition = "DECIMAL(7,5)")
    private Float euroCotizacion;

    @Column(name = "GCUSDC", columnDefinition = "DECIMAL(7,5)")
    private Float dolarCompraCotizacion;

    @Column(name = "GCUSDV", columnDefinition = "DECIMAL(7,5)")
    private Float dolarVentaCotizacion;

    @Column(name = "GCUFV", columnDefinition = "DECIMAL(7,5)")
    private Float ufvCotizacion;

    @Column(name = "GCUFV", columnDefinition = "DECIMAL(7,5)")
    private Float yenCotizacion;

    @Column(name = "GCLIBRA", columnDefinition = "DECIMAL(7,5)")
    private Float libraCotizacion;

    @Column(name = "GCFRANCO", columnDefinition = "DECIMAL(7,5)")
    private Float francoCotizacion;

    @Column(name = "GCREAL", columnDefinition = "DECIMAL(7,5)")
    private Float realCotizacion;

    @Column(name = "GCYUAN", columnDefinition = "DECIMAL(7,5)")
    private Float yuanCotizacion;

    @Column(name = "GC$ARG", columnDefinition = "DECIMAL(7,5)")
    private Float argentinoPesoCotizacion;

    @Column(name = "GCCOT1", columnDefinition = "DECIMAL(7,5)")
    private Float futura1Cotizacion;
    @Column(name = "GCCOT2", columnDefinition = "DECIMAL(7,5)")
    private Float futura2Cotizacion;
    @Column(name = "GCCOT3", columnDefinition = "DECIMAL(7,5)")
    private Float futura3Cotizacion;

    @Column(name = "GCCOT4", columnDefinition = "DECIMAL(7,5)")
    private Float futura4Cotizacion;


    @Column(name = "GCINCEUR", columnDefinition = "DECIMAL(7,5)")
    private Float euroCompraIncremento;
    @Column(name = "GCINVEUR", columnDefinition = "DECIMAL(7,5)")
    private Float euroVentaIncremento;
    @Column(name = "GCINCREC", columnDefinition = "DECIMAL(7,5)")
    private Float euroCompraRemesasIncremento;
    @Column(name = "GCINCREV", columnDefinition = "DECIMAL(7,5)")
    private Float euroVentaRemesasIncremento;
    @Column(name = "GCINCUSD", columnDefinition = "DECIMAL(7,5)")
    private Float dolarCompraIncremento;
    @Column(name = "GCINVUSD", columnDefinition = "DECIMAL(7,5)")
    private Float dolarVentaIncremento;
    @Column(name = "GCINCRSC", columnDefinition = "DECIMAL(7,5)")
    private Float dolarCompraRemesasIncremento;
    @Column(name = "GCINCREM", columnDefinition = "DECIMAL(7,5)")
    private Float dolarVentaRemesasIncremento;
    @Column(name = "GCINCJPY", columnDefinition = "DECIMAL(7,5)")
    private Float yenCompraIncremento;
    @Column(name = "GCINVJPY", columnDefinition = "DECIMAL(7,5)")
    private Float yenVentaIncremento;
    @Column(name = "GCINCFSS", columnDefinition = "DECIMAL(7,5)")
    private Float francoCompraIncremento;
    @Column(name = "GCINVFSS", columnDefinition = "DECIMAL(7,5)")
    private Float francoVentaIncremento;
    @Column(name = "GCINCITL", columnDefinition = "DECIMAL(7,5)")
    private Float libraCompraIncremento;
    @Column(name = "GCINVITL", columnDefinition = "DECIMAL(7,5)")
    private Float libraVentaIncremento;
    @Column(name = "GCINCBRL", columnDefinition = "DECIMAL(7,5)")
    private Float brlCompraIncremento;
    @Column(name = "GCINVBRL", columnDefinition = "DECIMAL(7,5)")
    private Float brlVentaIncremento;
    @Column(name = "GCINCRRC", columnDefinition = "DECIMAL(7,5)")
    private Float brlCompraRemesasIncremento;
    @Column(name = "GCINCRRV", columnDefinition = "DECIMAL(7,5)")
    private Float brlVentaRemesasIncremento;
    @Column(name = "GCINCCNY", columnDefinition = "DECIMAL(7,5)")
    private Float cnyCompraIncremento;
    @Column(name = "GCINVCNY", columnDefinition = "DECIMAL(7,5)")
    private Float cnyVentaIncremento;
    @Column(name = "GCINCRNC", columnDefinition = "DECIMAL(7,5)")
    private Float cnyCompraRemesasIncremento;
    @Column(name = "GCINCRNV", columnDefinition = "DECIMAL(7,5)")
    private Float cnyVentaRemesasIncremento;
    @Column(name = "GCINCARS", columnDefinition = "DECIMAL(7,5)")
    private Float arsCompraIncremento;
    @Column(name = "GCINVARS", columnDefinition = "DECIMAL(7,5)")
    private Float arsVentaIncremento;
    @Column(name = "GCINCR$C", columnDefinition = "DECIMAL(7,5)")
    private Float arsCompraRemesasIncremento;
    @Column(name = "GCINCR$V", columnDefinition = "DECIMAL(7,5)")
    private Float arsVentaRemesasIncremento;

    @Column(name = "GCESTA", columnDefinition = "CHAR(1)")
    private String estado;

    @Column(name = "GCDISA", columnDefinition = "CHAR(10)")
    private String origenRegistro;

    @Column(name = "GCUSRA", columnDefinition = "CHAR(10)")
    private String usuarioRegistro;

    @Column(name = "GCFECA", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;

    public CotizacionBolsin() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFechaEfectiva() {
        return fechaEfectiva;
    }

    public void setFechaEfectiva(String fechaEfectiva) {
        this.fechaEfectiva = fechaEfectiva;
    }

    public String getHoraEfectiva() {
        return horaEfectiva;
    }

    public void setHoraEfectiva(String horaEfectiva) {
        this.horaEfectiva = horaEfectiva;
    }

    public Float getEuroCotizacion() {
        return euroCotizacion;
    }

    public void setEuroCotizacion(Float euroCotizacion) {
        this.euroCotizacion = euroCotizacion;
    }

    public Float getDolarCompraCotizacion() {
        return dolarCompraCotizacion;
    }

    public void setDolarCompraCotizacion(Float dolarCompraCotizacion) {
        this.dolarCompraCotizacion = dolarCompraCotizacion;
    }

    public Float getDolarVentaCotizacion() {
        return dolarVentaCotizacion;
    }

    public void setDolarVentaCotizacion(Float dolarVentaCotizacion) {
        this.dolarVentaCotizacion = dolarVentaCotizacion;
    }

    public Float getUfvCotizacion() {
        return ufvCotizacion;
    }

    public void setUfvCotizacion(Float ufvCotizacion) {
        this.ufvCotizacion = ufvCotizacion;
    }

    public Float getYenCotizacion() {
        return yenCotizacion;
    }

    public void setYenCotizacion(Float yenCotizacion) {
        this.yenCotizacion = yenCotizacion;
    }

    public Float getLibraCotizacion() {
        return libraCotizacion;
    }

    public void setLibraCotizacion(Float libraCotizacion) {
        this.libraCotizacion = libraCotizacion;
    }

    public Float getFrancoCotizacion() {
        return francoCotizacion;
    }

    public void setFrancoCotizacion(Float francoCotizacion) {
        this.francoCotizacion = francoCotizacion;
    }

    public Float getRealCotizacion() {
        return realCotizacion;
    }

    public void setRealCotizacion(Float realCotizacion) {
        this.realCotizacion = realCotizacion;
    }

    public Float getYuanCotizacion() {
        return yuanCotizacion;
    }

    public void setYuanCotizacion(Float yuanCotizacion) {
        this.yuanCotizacion = yuanCotizacion;
    }

    public Float getArgentinoPesoCotizacion() {
        return argentinoPesoCotizacion;
    }

    public void setArgentinoPesoCotizacion(Float argentinoPesoCotizacion) {
        this.argentinoPesoCotizacion = argentinoPesoCotizacion;
    }

    public Float getFutura1Cotizacion() {
        return futura1Cotizacion;
    }

    public void setFutura1Cotizacion(Float futura1Cotizacion) {
        this.futura1Cotizacion = futura1Cotizacion;
    }

    public Float getFutura2Cotizacion() {
        return futura2Cotizacion;
    }

    public void setFutura2Cotizacion(Float futura2Cotizacion) {
        this.futura2Cotizacion = futura2Cotizacion;
    }

    public Float getFutura3Cotizacion() {
        return futura3Cotizacion;
    }

    public void setFutura3Cotizacion(Float futura3Cotizacion) {
        this.futura3Cotizacion = futura3Cotizacion;
    }

    public Float getFutura4Cotizacion() {
        return futura4Cotizacion;
    }

    public void setFutura4Cotizacion(Float futura4Cotizacion) {
        this.futura4Cotizacion = futura4Cotizacion;
    }

    public Float getEuroCompraIncremento() {
        return euroCompraIncremento;
    }

    public void setEuroCompraIncremento(Float euroCompraIncremento) {
        this.euroCompraIncremento = euroCompraIncremento;
    }

    public Float getEuroVentaIncremento() {
        return euroVentaIncremento;
    }

    public void setEuroVentaIncremento(Float euroVentaIncremento) {
        this.euroVentaIncremento = euroVentaIncremento;
    }

    public Float getEuroCompraRemesasIncremento() {
        return euroCompraRemesasIncremento;
    }

    public void setEuroCompraRemesasIncremento(Float euroCompraRemesasIncremento) {
        this.euroCompraRemesasIncremento = euroCompraRemesasIncremento;
    }

    public Float getEuroVentaRemesasIncremento() {
        return euroVentaRemesasIncremento;
    }

    public void setEuroVentaRemesasIncremento(Float euroVentaRemesasIncremento) {
        this.euroVentaRemesasIncremento = euroVentaRemesasIncremento;
    }

    public Float getDolarCompraIncremento() {
        return dolarCompraIncremento;
    }

    public void setDolarCompraIncremento(Float dolarCompraIncremento) {
        this.dolarCompraIncremento = dolarCompraIncremento;
    }

    public Float getDolarVentaIncremento() {
        return dolarVentaIncremento;
    }

    public void setDolarVentaIncremento(Float dolarVentaIncremento) {
        this.dolarVentaIncremento = dolarVentaIncremento;
    }

    public Float getDolarCompraRemesasIncremento() {
        return dolarCompraRemesasIncremento;
    }

    public void setDolarCompraRemesasIncremento(Float dolarCompraRemesasIncremento) {
        this.dolarCompraRemesasIncremento = dolarCompraRemesasIncremento;
    }

    public Float getDolarVentaRemesasIncremento() {
        return dolarVentaRemesasIncremento;
    }

    public void setDolarVentaRemesasIncremento(Float dolarVentaRemesasIncremento) {
        this.dolarVentaRemesasIncremento = dolarVentaRemesasIncremento;
    }

    public Float getYenCompraIncremento() {
        return yenCompraIncremento;
    }

    public void setYenCompraIncremento(Float yenCompraIncremento) {
        this.yenCompraIncremento = yenCompraIncremento;
    }

    public Float getYenVentaIncremento() {
        return yenVentaIncremento;
    }

    public void setYenVentaIncremento(Float yenVentaIncremento) {
        this.yenVentaIncremento = yenVentaIncremento;
    }

    public Float getFrancoCompraIncremento() {
        return francoCompraIncremento;
    }

    public void setFrancoCompraIncremento(Float francoCompraIncremento) {
        this.francoCompraIncremento = francoCompraIncremento;
    }

    public Float getFrancoVentaIncremento() {
        return francoVentaIncremento;
    }

    public void setFrancoVentaIncremento(Float francoVentaIncremento) {
        this.francoVentaIncremento = francoVentaIncremento;
    }

    public Float getLibraCompraIncremento() {
        return libraCompraIncremento;
    }

    public void setLibraCompraIncremento(Float libraCompraIncremento) {
        this.libraCompraIncremento = libraCompraIncremento;
    }

    public Float getLibraVentaIncremento() {
        return libraVentaIncremento;
    }

    public void setLibraVentaIncremento(Float libraVentaIncremento) {
        this.libraVentaIncremento = libraVentaIncremento;
    }

    public Float getBrlCompraIncremento() {
        return brlCompraIncremento;
    }

    public void setBrlCompraIncremento(Float brlCompraIncremento) {
        this.brlCompraIncremento = brlCompraIncremento;
    }

    public Float getBrlVentaIncremento() {
        return brlVentaIncremento;
    }

    public void setBrlVentaIncremento(Float brlVentaIncremento) {
        this.brlVentaIncremento = brlVentaIncremento;
    }

    public Float getBrlCompraRemesasIncremento() {
        return brlCompraRemesasIncremento;
    }

    public void setBrlCompraRemesasIncremento(Float brlCompraRemesasIncremento) {
        this.brlCompraRemesasIncremento = brlCompraRemesasIncremento;
    }

    public Float getBrlVentaRemesasIncremento() {
        return brlVentaRemesasIncremento;
    }

    public void setBrlVentaRemesasIncremento(Float brlVentaRemesasIncremento) {
        this.brlVentaRemesasIncremento = brlVentaRemesasIncremento;
    }

    public Float getCnyCompraIncremento() {
        return cnyCompraIncremento;
    }

    public void setCnyCompraIncremento(Float cnyCompraIncremento) {
        this.cnyCompraIncremento = cnyCompraIncremento;
    }

    public Float getCnyVentaIncremento() {
        return cnyVentaIncremento;
    }

    public void setCnyVentaIncremento(Float cnyVentaIncremento) {
        this.cnyVentaIncremento = cnyVentaIncremento;
    }

    public Float getCnyCompraRemesasIncremento() {
        return cnyCompraRemesasIncremento;
    }

    public void setCnyCompraRemesasIncremento(Float cnyCompraRemesasIncremento) {
        this.cnyCompraRemesasIncremento = cnyCompraRemesasIncremento;
    }

    public Float getCnyVentaRemesasIncremento() {
        return cnyVentaRemesasIncremento;
    }

    public void setCnyVentaRemesasIncremento(Float cnyVentaRemesasIncremento) {
        this.cnyVentaRemesasIncremento = cnyVentaRemesasIncremento;
    }

    public Float getArsCompraIncremento() {
        return arsCompraIncremento;
    }

    public void setArsCompraIncremento(Float arsCompraIncremento) {
        this.arsCompraIncremento = arsCompraIncremento;
    }

    public Float getArsVentaIncremento() {
        return arsVentaIncremento;
    }

    public void setArsVentaIncremento(Float arsVentaIncremento) {
        this.arsVentaIncremento = arsVentaIncremento;
    }

    public Float getArsCompraRemesasIncremento() {
        return arsCompraRemesasIncremento;
    }

    public void setArsCompraRemesasIncremento(Float arsCompraRemesasIncremento) {
        this.arsCompraRemesasIncremento = arsCompraRemesasIncremento;
    }

    public Float getArsVentaRemesasIncremento() {
        return arsVentaRemesasIncremento;
    }

    public void setArsVentaRemesasIncremento(Float arsVentaRemesasIncremento) {
        this.arsVentaRemesasIncremento = arsVentaRemesasIncremento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getOrigenRegistro() {
        return origenRegistro;
    }

    public void setOrigenRegistro(String origenRegistro) {
        this.origenRegistro = origenRegistro;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
}


