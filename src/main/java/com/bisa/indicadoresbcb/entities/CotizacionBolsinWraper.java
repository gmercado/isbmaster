package com.bisa.indicadoresbcb.entities;

import javax.persistence.Column;
import java.io.Serializable;
import java.security.InvalidParameterException;

/**
 * Created by ccalle on 01/03/2017.
 */
public class CotizacionBolsinWraper implements Serializable{

    private Float euroCotizacion;
    private Float dolarCompraCotizacion;
    private Float dolarVentaCotizacion;
    private Float ufvCotizacion;
    private Float yenCotizacion;
    private Float libraCotizacion;
    private Float francoCotizacion;
    private Float realCotizacion;
    private Float yuanCotizacion;
    private Float argentinoPesoCotizacion;
    private Float futura1Cotizacion;
    private Float futura2Cotizacion;
    private Float futura3Cotizacion;
    private Float futura4Cotizacion;

    public CotizacionBolsinWraper() {
    }
    public void verificarNulos() throws InvalidParameterException{
        if(euroCotizacion==null) throw new InvalidParameterException("No se encontro la cotizacion de euros");
        if(dolarCompraCotizacion==null) throw new InvalidParameterException("No se encontro la cotizacion de dolar " +
                "compra");
        if(dolarVentaCotizacion==null) throw new InvalidParameterException("No se encontro la cotizacion de dolar " +
                "venta");
        if(ufvCotizacion==null) throw new InvalidParameterException("No se encontro la cotizacion de UFV");
        if(yenCotizacion==null)  throw new InvalidParameterException("No se encontro la cotizacion de YEN");
        if(libraCotizacion==null) throw new InvalidParameterException("No se encontro la cotizacion de LIBRAS");
        if(francoCotizacion==null) throw new InvalidParameterException("No se encontro la cotizacion de FRANCOS");
        if(realCotizacion==null) throw new InvalidParameterException("No se encontro la cotizacion de REALES");;
        if(yuanCotizacion==null) throw new InvalidParameterException("No se encontro la cotizacion de YUAN");;
        if(argentinoPesoCotizacion==null) throw new InvalidParameterException("No se encontro la cotizacion de PESOS " +
                "ARGENTINOS");
    }
    public Float getEuroCotizacion() {
        return euroCotizacion;
    }

    public void setEuroCotizacion(Float euroCotizacion) {
        this.euroCotizacion = euroCotizacion;
    }

    public Float getDolarCompraCotizacion() {
        return dolarCompraCotizacion;
    }

    public void setDolarCompraCotizacion(Float dolarCompraCotizacion) {
        this.dolarCompraCotizacion = dolarCompraCotizacion;
    }

    public Float getDolarVentaCotizacion() {
        return dolarVentaCotizacion;
    }

    public void setDolarVentaCotizacion(Float dolarVentaCotizacion) {
        this.dolarVentaCotizacion = dolarVentaCotizacion;
    }

    public Float getUfvCotizacion() {
        return ufvCotizacion;
    }

    public void setUfvCotizacion(Float ufvCotizacion) {
        this.ufvCotizacion = ufvCotizacion;
    }

    public Float getYenCotizacion() {
        return yenCotizacion;
    }

    public void setYenCotizacion(Float yenCotizacion) {
        this.yenCotizacion = yenCotizacion;
    }

    public Float getLibraCotizacion() {
        return libraCotizacion;
    }

    public void setLibraCotizacion(Float libraCotizacion) {
        this.libraCotizacion = libraCotizacion;
    }

    public Float getFrancoCotizacion() {
        return francoCotizacion;
    }

    public void setFrancoCotizacion(Float francoCotizacion) {
        this.francoCotizacion = francoCotizacion;
    }

    public Float getRealCotizacion() {
        return realCotizacion;
    }

    public void setRealCotizacion(Float realCotizacion) {
        this.realCotizacion = realCotizacion;
    }

    public Float getYuanCotizacion() {
        return yuanCotizacion;
    }

    public void setYuanCotizacion(Float yuanCotizacion) {
        this.yuanCotizacion = yuanCotizacion;
    }

    public Float getArgentinoPesoCotizacion() {
        return argentinoPesoCotizacion;
    }

    public void setArgentinoPesoCotizacion(Float argentinoPesoCotizacion) {
        this.argentinoPesoCotizacion = argentinoPesoCotizacion;
    }

    public Float getFutura1Cotizacion() {
        return futura1Cotizacion;
    }

    public void setFutura1Cotizacion(Float futura1Cotizacion) {
        this.futura1Cotizacion = futura1Cotizacion;
    }

    public Float getFutura2Cotizacion() {
        return futura2Cotizacion;
    }

    public void setFutura2Cotizacion(Float futura2Cotizacion) {
        this.futura2Cotizacion = futura2Cotizacion;
    }

    public Float getFutura3Cotizacion() {
        return futura3Cotizacion;
    }

    public void setFutura3Cotizacion(Float futura3Cotizacion) {
        this.futura3Cotizacion = futura3Cotizacion;
    }

    public Float getFutura4Cotizacion() {
        return futura4Cotizacion;
    }

    public void setFutura4Cotizacion(Float futura4Cotizacion) {
        this.futura4Cotizacion = futura4Cotizacion;
    }
}
