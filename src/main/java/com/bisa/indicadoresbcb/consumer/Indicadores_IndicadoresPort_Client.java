
package com.bisa.indicadoresbcb.consumer;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;

/**
 * This class was generated by Apache CXF 3.1.9
 * 2017-01-17T11:15:40.231-04:00
 * Generated source version: 3.1.9
 * 
 */
public final class Indicadores_IndicadoresPort_Client {

    private static final QName SERVICE_NAME = new QName("http://ws.bcb.gob.bo", "indicadoresService");

    private Indicadores_IndicadoresPort_Client() {
    }

    public static void main(String args[]) throws java.lang.Exception {
        URL wsdlURL = IndicadoresService.WSDL_LOCATION;
        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
            File wsdlFile = new File(args[0]);
            try {
                if (wsdlFile.exists()) {
                    wsdlURL = wsdlFile.toURI().toURL();
                } else {
                    wsdlURL = new URL(args[0]);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
      
        IndicadoresService ss = new IndicadoresService(wsdlURL, SERVICE_NAME);
        Indicadores port = ss.getIndicadoresPort();  
        
        {
        System.out.println("Invoking obtenerIndicador...");
        java.lang.Integer _obtenerIndicador_codIndicador = null;
        java.lang.Integer _obtenerIndicador_codMoneda = null;
        java.lang.String _obtenerIndicador_fecha = "";
        java.util.List<Indicador> _obtenerIndicador__return = port.obtenerIndicador(_obtenerIndicador_codIndicador, _obtenerIndicador_codMoneda, _obtenerIndicador_fecha);
        System.out.println("obtenerIndicador.result=" + _obtenerIndicador__return);


        }
        {
        System.out.println("Invoking obtenerIndicadorMonedas...");
        java.lang.Integer _obtenerIndicadorMonedas_codIndicador = null;
        java.lang.String _obtenerIndicadorMonedas_fecha = "";
        java.util.List<Indicador> _obtenerIndicadorMonedas__return = port.obtenerIndicadorMonedas(_obtenerIndicadorMonedas_codIndicador, _obtenerIndicadorMonedas_fecha);
        System.out.println("obtenerIndicadorMonedas.result=" + _obtenerIndicadorMonedas__return);


        }
        {
        System.out.println("Invoking obtenerIndicadorXML...");
        java.lang.Integer _obtenerIndicadorXML_codIndicador = null;
        java.lang.Integer _obtenerIndicadorXML_codMoneda = null;
        java.lang.String _obtenerIndicadorXML_fecha = "";
        java.lang.String _obtenerIndicadorXML__return = port.obtenerIndicadorXML(_obtenerIndicadorXML_codIndicador, _obtenerIndicadorXML_codMoneda, _obtenerIndicadorXML_fecha);
        System.out.println("obtenerIndicadorXML.result=" + _obtenerIndicadorXML__return);


        }

        System.exit(0);
    }

}
