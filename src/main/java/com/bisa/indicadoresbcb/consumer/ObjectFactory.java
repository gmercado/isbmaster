
package com.bisa.indicadoresbcb.consumer;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the bus.consumoweb.indicadoresbcb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ObtenerIndicador_QNAME = new QName("http://ws.bcb.gob.bo", "obtenerIndicador");
    private final static QName _ObtenerIndicadorMonedas_QNAME = new QName("http://ws.bcb.gob.bo", "obtenerIndicadorMonedas");
    private final static QName _ObtenerIndicadorMonedasResponse_QNAME = new QName("http://ws.bcb.gob.bo", "obtenerIndicadorMonedasResponse");
    private final static QName _ObtenerIndicadorResponse_QNAME = new QName("http://ws.bcb.gob.bo", "obtenerIndicadorResponse");
    private final static QName _ObtenerIndicadorXML_QNAME = new QName("http://ws.bcb.gob.bo", "obtenerIndicadorXML");
    private final static QName _ObtenerIndicadorXMLResponse_QNAME = new QName("http://ws.bcb.gob.bo", "obtenerIndicadorXMLResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: bus.consumoweb.indicadoresbcb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObtenerIndicador }
     * 
     */
    public ObtenerIndicador createObtenerIndicador() {
        return new ObtenerIndicador();
    }

    /**
     * Create an instance of {@link ObtenerIndicadorMonedas }
     * 
     */
    public ObtenerIndicadorMonedas createObtenerIndicadorMonedas() {
        return new ObtenerIndicadorMonedas();
    }

    /**
     * Create an instance of {@link ObtenerIndicadorMonedasResponse }
     * 
     */
    public ObtenerIndicadorMonedasResponse createObtenerIndicadorMonedasResponse() {
        return new ObtenerIndicadorMonedasResponse();
    }

    /**
     * Create an instance of {@link ObtenerIndicadorResponse }
     * 
     */
    public ObtenerIndicadorResponse createObtenerIndicadorResponse() {
        return new ObtenerIndicadorResponse();
    }

    /**
     * Create an instance of {@link ObtenerIndicadorXML }
     * 
     */
    public ObtenerIndicadorXML createObtenerIndicadorXML() {
        return new ObtenerIndicadorXML();
    }

    /**
     * Create an instance of {@link ObtenerIndicadorXMLResponse }
     * 
     */
    public ObtenerIndicadorXMLResponse createObtenerIndicadorXMLResponse() {
        return new ObtenerIndicadorXMLResponse();
    }

    /**
     * Create an instance of {@link Indicador }
     * 
     */
    public Indicador createIndicador() {
        return new Indicador();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerIndicador }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.bcb.gob.bo", name = "obtenerIndicador")
    public JAXBElement<ObtenerIndicador> createObtenerIndicador(ObtenerIndicador value) {
        return new JAXBElement<ObtenerIndicador>(_ObtenerIndicador_QNAME, ObtenerIndicador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerIndicadorMonedas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.bcb.gob.bo", name = "obtenerIndicadorMonedas")
    public JAXBElement<ObtenerIndicadorMonedas> createObtenerIndicadorMonedas(ObtenerIndicadorMonedas value) {
        return new JAXBElement<ObtenerIndicadorMonedas>(_ObtenerIndicadorMonedas_QNAME, ObtenerIndicadorMonedas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerIndicadorMonedasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.bcb.gob.bo", name = "obtenerIndicadorMonedasResponse")
    public JAXBElement<ObtenerIndicadorMonedasResponse> createObtenerIndicadorMonedasResponse(ObtenerIndicadorMonedasResponse value) {
        return new JAXBElement<ObtenerIndicadorMonedasResponse>(_ObtenerIndicadorMonedasResponse_QNAME, ObtenerIndicadorMonedasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerIndicadorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.bcb.gob.bo", name = "obtenerIndicadorResponse")
    public JAXBElement<ObtenerIndicadorResponse> createObtenerIndicadorResponse(ObtenerIndicadorResponse value) {
        return new JAXBElement<ObtenerIndicadorResponse>(_ObtenerIndicadorResponse_QNAME, ObtenerIndicadorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerIndicadorXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.bcb.gob.bo", name = "obtenerIndicadorXML")
    public JAXBElement<ObtenerIndicadorXML> createObtenerIndicadorXML(ObtenerIndicadorXML value) {
        return new JAXBElement<ObtenerIndicadorXML>(_ObtenerIndicadorXML_QNAME, ObtenerIndicadorXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerIndicadorXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.bcb.gob.bo", name = "obtenerIndicadorXMLResponse")
    public JAXBElement<ObtenerIndicadorXMLResponse> createObtenerIndicadorXMLResponse(ObtenerIndicadorXMLResponse value) {
        return new JAXBElement<ObtenerIndicadorXMLResponse>(_ObtenerIndicadorXMLResponse_QNAME, ObtenerIndicadorXMLResponse.class, null, value);
    }

}
