package com.bisa.indicadoresbcb.consumer;

import java.util.List;

/**
 * Created by ccalle on 17/01/2017.
 */
public class BCBCliente {
  public static void main(String[] argv) {

      IndicadoresService service = new IndicadoresService();
      Indicadores client = service.getIndicadoresPort();
      List<Indicador> indicadores = client.obtenerIndicador(2, 69, "04/01/2017");
      for (Indicador indicador : indicadores) {
          System.out.println(" ==============ssss= >>> " +indicador.getCodDato()+"--"+ indicador.getDato());
      }
  }
}
