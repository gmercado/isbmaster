package com.bisa.indicadoresbcb;

import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.indicadoresbcb.dao.CotizacionBolsinDao;
import com.bisa.indicadoresbcb.sched.*;

/**
 * Created by ccalle on 02/02/2017.
 */
public class IndicadoresBCBModule extends AbstractModuleBisa {
    @Override
    protected void configure() {
        bind(CotizacionBolsinDao.class);
/*
Seconds         *
Minutes         0/5 every 5 minutes
Hours           10-23
Day of month    *
Month           *
Day of week     *
Year            *
         */
        bindSched("TipoDeCambioJob", "BCB", "0 0/15 16-23 ? * MON-FRI", TipoDeCambioJob.class);
        bindSched("EliminarArchivoControl", "BCB", "0 0 16 ? * MON-FRI", EliminarArchivoControl.class);
        bindSched("PreRegTasaTreDiaUnoJob", "BCB", "0 0/30 15-23 ? * WED", PreRegTasaTreDiaUnoJob.class);
        bindSched("PreRegTasaTreDiaDosJob", "BCB", "0 0/30 0-5 ? * THU", PreRegTasaTreDiaDosJob.class);
        bindSched("VerificadorTasaTREJob", "BCB", "0 0 9-23 ? * THU", VerificadorTasaTREJob.class);
        bindUI("com/bisa/indicadoresbcb/ui");
    }

}
