package com.bisa.indicadoresbcb.dao;

import bus.consumoweb.entities.ConsumoWeb;
import bus.consumoweb.yellowpepper.dao.YellowPepperServicioDao;
import bus.database.dao.Dao;
import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.mail.api.MailerFactory;
import bus.mail.model.Mailer;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.indicadoresbcb.consumer.Indicador;
import com.bisa.indicadoresbcb.consumer.Indicadores;
import com.bisa.indicadoresbcb.consumer.IndicadoresService;
import com.bisa.indicadoresbcb.entities.CotizacionBolsin;
import com.bisa.indicadoresbcb.entities.CotizacionBolsinWraper;
import com.bisa.indicadoresbcb.tipos.*;
import com.bisa.isb.ws.soap.client.LogSOAP;
import com.bisa.isb.ws.soap.client.LogSOAPHandler;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;
import org.apache.openjpa.persistence.OpenJPAQuery;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.xml.ws.BindingProvider;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.InvalidParameterException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by ccalle on 13/02/2017.
 */
public class CotizacionBolsinDao extends DaoImpl<CotizacionBolsin, Long> implements LogSOAP {

    private static final Logger LOGGER = LoggerFactory.getLogger(CotizacionBolsinDao.class);
    private final MedioAmbiente medioAmbiente;
    private final MailerFactory mailerFactory;
    private final YellowPepperServicioDao yellowPepperServicioDao;
    public static final String BCB_COTIZACIONES_WARNING_FILE="/etc/bcb_indicadores/bcb_cotizaciones_warning.txt";
    public static final String BCB_COTIZACIONES_HORA_TOPE_FILE="/etc/bcb_indicadores/bcb_cotizaciones_hora_tope.txt";
    public static final String BCB_COTIZACIONES_SISTEMA="bcbcot";
    public static final String BCB_COTIZACIONES_CLIENTE="ISBSVC";
    public static final String BCB_COTIZACIONES_SERVICIO="bcbcot";
    public static final String BCB_COTIZACIONES_VAR_HORA_ACTUAL="HORA_ACTUAL";
    public static final String BCB_COTIZACIONES_VAR_HORA_UMBRAL="HORA_UMBRAL";
    public static final String BCB_COTIZACIONES_VAR_HORA_TOPE="HORA_TOPE";
    public static final String BCB_COTIZACIONES_VAR_FECHA_CARGA="FECHA_CARGA";
    private final Dao<ConsumoWeb, Long> consumoWebDao;
    private final ExecutorService executor;


    protected DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm");
    @Inject
    protected CotizacionBolsinDao(@BasePrincipal EntityManagerFactory entityManagerFactory, MedioAmbiente
            medioAmbiente, MailerFactory mailerFactory, YellowPepperServicioDao yellowPepperServicioDao, Dao<ConsumoWeb, Long> consumoWebDao, @SQLFueraDeLinea ExecutorService executor) {
        super(entityManagerFactory);
        this.medioAmbiente=medioAmbiente;
        this.mailerFactory=mailerFactory;
        this.yellowPepperServicioDao=yellowPepperServicioDao;
        this.consumoWebDao=consumoWebDao;
        this.executor=executor;

    }
    public Date siguienteDiaHabil(Date fecha){
        DateTime dateTime = new DateTime(fecha);
        Date siguenteDia=null;
        boolean loop=true;
        while (loop){
            dateTime=dateTime.plusDays(1);
            siguenteDia=dateTime.toDate();
            if(esDiaLaborable(siguenteDia)){
                loop=false;
            }
        }
        return siguenteDia;
    }
    private boolean esDiaLaborable(Date fechaHoy){
        if(fechaHoy==null){
            LOGGER.error("Para determinar el Dia Laborable, el parametro Fecha no puede ser NULO.");
            return false;
        }
        if(esSabadoODomingo(fechaHoy)) return false;

        if(esDiaFeriado(fechaHoy)) return false;
        return true;
    }
    private boolean esSabadoODomingo(Date fecha){
        boolean result=false;
        //"u" Day number of week (1 = Monday, ..., 6 =Saturday, 7 = Sunday)
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("u");

        String formateado=simpleDateFormat.format(fecha);
        if(formateado.equals("6") || formateado.equals("7")){
            result=true;
        }
        return result;
    }

    private boolean esDiaFeriado(Date fechaHoy) {
        if(fechaHoy==null) return false;
        final String fecha = FormatosUtils.formatoFechaISO(fechaHoy);
        if(StringUtils.trimToNull(fecha)==null) return false;
        BigDecimal  result= doWithTransaction(new WithTransaction<BigDecimal  >() {
            @Override
            public BigDecimal  call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {

                @SuppressWarnings("unchecked") OpenJPAQuery<BigDecimal > sqlAgencia =
                        entityManager.createNativeQuery("select FFFECH from TAPFFERI where FFFECH = ?",
                                BigDecimal.class);
                BigDecimal  feriado=null;
                try {
                    feriado = sqlAgencia.setParameter(1, new BigDecimal(fecha)).getSingleResult();
                } catch (NoResultException e) {
                    return null;
                }
                return feriado;
            }
        });
        if (result!=null){
            return true;
        }
        return false;
    }


    /**
     * Verifica si existe una cotizacion tipo Bolsin en estado ingresado
     * @return Null si no existe, si no cadena con la fecha, hora y usuario de registro
     */
    public String cotizacionEstaIngresada() {

        String descripcion= doWithTransaction(new WithTransaction<String>() {
            @Override
            public String call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                @SuppressWarnings("unchecked") OpenJPAQuery<String> sqlAgencia =
                        entityManager.createNativeQuery("select ' Registrado en: ' || VARCHAR(GCFECA) ||', por el usuario: '||GCUSRA from GLP701 where GCESTA='I' and GCORIG='B'",
                                String.class);
                String descripcion=null;
                try {
                    descripcion =medioAmbiente.getValorDe(Variables.BCB_COTIZACIONES_PENDIENTES_DE_POSTEO, Variables.BCB_COTIZACIONES_PENDIENTES_DE_POSTEO_DEFAULT)+ sqlAgencia.getSingleResult();
                } catch (NoResultException e) {
                    return null;
                }
                return descripcion;
            }
        });
        if (descripcion!=null){
            return descripcion;
        }
        return null;
    }
    public Date getFechaActualTap001() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyD");
        Integer fechaJuliana= doWithTransaction(new WithTransaction<Integer>() {
            @Override
            public Integer call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                @SuppressWarnings("unchecked") OpenJPAQuery<Integer> sqlFechaJuliana =
                        entityManager.createNativeQuery("SELECT DSDT FROM TAP001 WHERE DSBK = 1",
                                String.class);
                Integer fechaJuliana=null;
                try {
                    fechaJuliana =sqlFechaJuliana.getSingleResult();
                } catch (NoResultException e) {
                    return null;
                }
                return fechaJuliana;
            }
        });
        if (fechaJuliana!=null){
            try {
               LOGGER.debug("Fecha actual de proceso {} julinana{}", sdf.parse(fechaJuliana.toString()),fechaJuliana);return
                        sdf.parse(fechaJuliana.toString());
            } catch (ParseException e) {
                LOGGER.error("Fecha de proceso no se pudo parsear >> {}", e);
            }
        }
        return null;
    }

    public void intentarRegistrar(Date parsedDate){
        try{
            String estadoIngreso=cotizacionEstaIngresada();

            if(estadoIngreso==null){
                try{
                    obtenerIndicadorTimeout(); //solo para provocar timeout
                }catch (Exception eTimeout){
                    LOGGER.error("Aqui siempre da timeout ",eTimeout);
                }
                CotizacionBolsinWraper cotizacionBolsinWraper=obtenerIndicadores(parsedDate);
                registrarCotizacion(parsedDate, cotizacionBolsinWraper);
            }else {
                LOGGER.info("La cotizacion ya estaba registrada: "+estadoIngreso);
            }
        } catch (Exception e) {
            LOGGER.info("No se pudo encontar las cotizaciones.",e);
            String horaUmbral = medioAmbiente.getValorDe(Variables.BCB_COTIZACIONES_UMBRAL_HORA_WARNING,
                    Variables.BCB_COTIZACIONES_UMBRAL_HORA_WARNING_DEFAULT);
            String horaUmbralTope = medioAmbiente.getValorDe(Variables.BCB_COTIZACIONES_UMBRAL_HORA_TOPE,
                    Variables.BCB_COTIZACIONES_UMBRAL_HORA_TOPE_DEFAULT);

            LocalTime timeUmbral = LocalTime.parse(horaUmbral);
            LocalTime timeUmbralTope = LocalTime.parse(horaUmbralTope);
            LocalTime timeNow = LocalTime.now();
            //verificar hora tope
            if(timeNow.isAfter(timeUmbralTope.minusMinutes(3))) {
                File fileUmbralTope = new File(BCB_COTIZACIONES_HORA_TOPE_FILE);
                boolean exists = fileUmbralTope.exists();
                if(!exists){
                    String msg=formatearMensaje(medioAmbiente.getValorDe(Variables.BCB_COTIZACIONES_NOTIFICACION_GRAVE_EMAIL,
                            Variables.BCB_COTIZACIONES_NOTIFICACION_GRAVE_EMAIL_DEFAULT),timeNow,timeUmbral,timeUmbralTope,
                            parsedDate);
                    String msgCorto=formatearMensaje(medioAmbiente.getValorDe(Variables.BCB_COTIZACIONES_NOTIFICACION_GRAVE_SMS,
                            Variables.BCB_COTIZACIONES_NOTIFICACION_GRAVE_SMS_DEFAULT),timeNow,timeUmbral,timeUmbralTope,
                            parsedDate);
                    //NOTIFICAR(alarma grave) SI ES QUE PASO LA HORA UMBRAL TOPE
                    notificarPorEmail("Alerta grave: Mantenimiento de cotizaciones BCB",msg);
                    notificarSMS("Alerta grave cotiz. BCB",msgCorto);
                    try {
                        fileUmbralTope.createNewFile();
                        FileWriter fw = new FileWriter(fileUmbralTope.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(msg);
                        bw.close();
                    }catch (IOException ex){
                        LOGGER.error("Error al crear archivo de notificacion",ex);
                    }
                }
            }else if(timeNow.isAfter(timeUmbral.minusMinutes(3))) { //verificar hora umbral
                File fileUmbral = new File(BCB_COTIZACIONES_WARNING_FILE);
                boolean exists = fileUmbral.exists();
                if(!exists){
                    //NOTIFICAR(alarmar) SI ES QUE PASO LA HORA UMBRAL WARNING
                    String msg=formatearMensaje(medioAmbiente.getValorDe(Variables.BCB_COTIZACIONES_NOTIFICACION_ALERTA_EMAIL,
                            Variables.BCB_COTIZACIONES_NOTIFICACION_ALERTA_EMAIL_DEFAULT),timeNow,timeUmbral,timeUmbralTope,
                            parsedDate);
                    String msgCorto=formatearMensaje(medioAmbiente.getValorDe(Variables.BCB_COTIZACIONES_NOTIFICACION_ALERTA_SMS,
                            Variables.BCB_COTIZACIONES_NOTIFICACION_ALERTA_SMS_DEFAULT),timeNow,timeUmbral,
                            timeUmbralTope,
                            parsedDate);
                    notificarPorEmail("Alerta: Mantenimiento de cotizaciones BCB",msg);
                    notificarSMS("Alerta cotizaciones BCB",msgCorto);
                    try {
                        fileUmbral.createNewFile();
                        FileWriter fw = new FileWriter(fileUmbral.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(msg);
                        bw.close();
                    }catch (IOException ex){
                        LOGGER.error("Error al crear archivo de notificacion",ex);
                    }
                }
            } else{
                LOGGER.error("No se obtuvo las cotizaciones correctamente, seguiremos intentando",e);
            }
        }
    }

    private String formatearMensaje(String plantilla,LocalTime timeNow,LocalTime timeUmbral,LocalTime timeUmbralTope,
                                    Date fechaCarga){
        String mensaje=plantilla.replaceAll(BCB_COTIZACIONES_VAR_HORA_ACTUAL,timeNow.format(dateTimeFormatter));
        if(timeUmbral!=null){
            mensaje=mensaje.replaceAll(BCB_COTIZACIONES_VAR_HORA_UMBRAL,timeUmbral.format(dateTimeFormatter));
        }
        if(timeUmbralTope!=null){
            mensaje=mensaje.replaceAll(BCB_COTIZACIONES_VAR_HORA_TOPE,timeUmbralTope.format(dateTimeFormatter));
        }
        mensaje=mensaje.replaceAll(BCB_COTIZACIONES_VAR_FECHA_CARGA,FormatosUtils.fechaFormateadaConYYYY(fechaCarga));
        return mensaje;
    }

    /**
     * Registra la cotizacion del BCB
     * @param fechaCarga
     * @param cotizacionBolsin
     * @return
     * @throws InvalidParameterException
     */
    public Integer registrarCotizacion(Date fechaCarga, final CotizacionBolsinWraper cotizacionBolsin) throws InvalidParameterException {
        if(fechaCarga==null) return null;
        final String fecha = FormatosUtils.formatoFechaISO(fechaCarga);
        final String hora= FormatosUtils.horaFormateadaConHHmmss(new Date());
        cotizacionBolsin.verificarNulos();
        if(StringUtils.trimToNull(fecha)==null) return null;
        return doWithTransaction(new WithTransaction<Integer>() {

            @Override
            public Integer call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {

                Query queryUpdateSecuencia = entityManager.createNativeQuery(
                        "update GLP703 set GCNRSEC=GCNRSEC+1 where GCBANK=1");
                Integer result = queryUpdateSecuencia.executeUpdate();
                if(result.intValue()==1){
                    Query queryInsert = entityManager.createNativeQuery(
                            "INSERT INTO GLP701( GCNRSEC, "+
                                    "  GCEURO, GCUSDC, GCUSDV,  GCUFV, GCYEN, GCLIBRA, "+
                                    "  GCFRANCO,  GCREAL,  GCYUAN, GC$ARG, "+
                                    "  GCCOT1,  GCCOT2,  GCCOT3,  GCCOT4, "+
                                    "  GCINCEUR, GCINVEUR, GCINCREC,  GCINCREV,  GCINCUSD,  GCINVUSD,  GCINCRSC, "+
                                    "  GCINCREM,  GCINCJPY,  GCINVJPY,  GCINCFSS,  GCINVFSS,  GCINCITL,  GCINVITL, "+
                                    "  GCINCBRL,  GCINVBRL,  GCINCRRC,  GCINCRRV,  GCINCCNY,  GCINVCNY,  GCINCRNC, "+
                                    "  GCINCRNV,  GCINCARS,  GCINVARS,  GCINCR$C,  GCINCR$V, "+
                                    "  GCESTA,  GCORIG,  GCDISA,  GCUSRA,  GCFECA "+
                                    ")  "+
                                    "select (select GCNRSEC from GLP703 where GCBANK=1) GCNRSEC,  "+
                                    "   GCEURO, GCUSDC, GCUSDV,  GCUFV, GCYEN, GCLIBRA,  "+
                                    "  GCFRANCO,  GCREAL,  GCYUAN, GC$ARG,  "+
                                    "  GCCOT1,  GCCOT2,  GCCOT3,  GCCOT4,  "+
                                    "  GCINCEUR, GCINVEUR, GCINCREC,  GCINCREV,  GCINCUSD,  GCINVUSD,  GCINCRSC,  "+
                                    "  GCINCREM,  GCINCJPY,  GCINVJPY,  GCINCFSS,  GCINVFSS,  GCINCITL,  GCINVITL,  "+
                                    "  GCINCBRL,  GCINVBRL,  GCINCRRC,  GCINCRRV,  GCINCCNY,  GCINVCNY,  GCINCRNC,  "+
                                    "  GCINCRNV,  GCINCARS,  GCINVARS,  GCINCR$C,  GCINCR$V,  "+
                                    "  'I' GCESTA, 'B' GCORIG, 'NONE' GCDISA,  'ISBSVC' GCUSRA,  current timestamp " +
                                    "GCFECA  "+
                                    "from GLP701 where GCESTA='P' order by GCFEEF desc  FETCH FIRST 1 ROWS ONLY  ");
                    result = queryInsert.executeUpdate();
                    if(result.intValue()==1){
                        Query queryUpdate = entityManager.createNativeQuery(
                                "update GLP701 " +
                                        "set GCFEEF=? , GCHOEF=? ," +
                                        " GCEURO  =? ,GCUSDC  =? , GCUSDV  =? , GCUFV   =? , GCYEN   =? , GCLIBRA =? " +
                                        ", GCFRANCO=? , GCREAL  =? ,  GCYUAN  =? , GC$ARG  =?  " +
                                        "where GCNRSEC=(select GCNRSEC from GLP703 where GCBANK=1)");

                        queryUpdate.setParameter(1,new BigInteger(fecha));//GCFEEF
                        queryUpdate.setParameter(2,new BigInteger(hora));//GCHOEF
                        queryUpdate.setParameter(3,cotizacionBolsin.getEuroCotizacion());//GCEURO
                        queryUpdate.setParameter(4,cotizacionBolsin.getDolarCompraCotizacion());//GCUSDC
                        queryUpdate.setParameter(5,cotizacionBolsin.getDolarVentaCotizacion());//GCUSDV
                        queryUpdate.setParameter(6,cotizacionBolsin.getUfvCotizacion());//GCUFV
                        queryUpdate.setParameter(7,cotizacionBolsin.getYenCotizacion());//GCYEN
                        queryUpdate.setParameter(8,cotizacionBolsin.getLibraCotizacion());//GCLIBRA
                        queryUpdate.setParameter(9,cotizacionBolsin.getFrancoCotizacion());//GCFRANCO
                        queryUpdate.setParameter(10,cotizacionBolsin.getRealCotizacion());//GCREAL
                        queryUpdate.setParameter(11,cotizacionBolsin.getYuanCotizacion());//GCYUAN
                        queryUpdate.setParameter(12,cotizacionBolsin.getArgentinoPesoCotizacion());//GC$ARG
                        result = queryUpdate.executeUpdate();
                        LOGGER.info("Se registro en GLP701 fecha[{}]. Las cotizaciones seran aplicadas a media noche.",
                                fecha);
                        LocalTime timeNow = LocalTime.now();
                        File fileUmbral = new File(BCB_COTIZACIONES_WARNING_FILE);
                        boolean exists = fileUmbral.exists();
                        String msg=formatearMensaje(medioAmbiente.getValorDe(Variables.BCB_COTIZACIONES_NOTIFICACION_DESALARMA_EMAIL,
                                Variables.BCB_COTIZACIONES_NOTIFICACION_DESALARMA_EMAIL_DEFAULT),timeNow,null,
                                null,
                                fechaCarga);
                        String msgCorto=formatearMensaje(medioAmbiente.getValorDe(Variables.BCB_COTIZACIONES_NOTIFICACION_DESALARMA_SMS,
                                Variables.BCB_COTIZACIONES_NOTIFICACION_DESALARMA_SMS_DEFAULT),timeNow,null,
                                null,
                                fechaCarga);;
                        if(exists){ //si existe archivo se ha enviado una notificacion con la alarma
                            //NOTIFICAR(desalarmar)
                            notificarPorEmail("Mantenimiento de cotizaciones BCB",msg);
                            notificarSMS("Desalarma cotiz. BCB",msgCorto);
                        }else{
                            String notificarSiempre = medioAmbiente.getValorDe(Variables
                                            .BCB_COTIZACIONES_NOTIFICAR_REGISTRO_SIEMPRE,
                                    Variables.BCB_COTIZACIONES_NOTIFICAR_REGISTRO_SIEMPRE_DEFAULT);
                            if(Boolean.valueOf(notificarSiempre)){
                                notificarPorEmail("Mantenimiento de cotizaciones BCB",msg);
                                notificarSMS("Desalarma cotiz. BCB",msgCorto);
                            }
                        }
                        if(result.intValue()==1){
                            LOGGER.info("Se registro en GLP701 fecha[{}]",fecha);
                            Query queryInsertHistorico = entityManager.createNativeQuery(
                                    "INSERT INTO GLP701H" +
                                            "( " +
                                            "  GCNRSEC,  GCFEEF,  GCHOEF,  GCEURO,  GCUSDC,  GCUSDV,  GCUFV,  GCYEN, " +
                                            "  GCLIBRA,  GCFRANCO,  GCREAL,  GCYUAN,  GC$ARG,  GCCOT1,  GCCOT2,  GCCOT3, " +
                                            "  GCCOT4,  GCINCEUR,  GCINVEUR,  GCINCREC,  GCINCREV,  GCINCUSD,  GCINVUSD, " +
                                            "  GCINCRSC,  GCINCREM,  GCINCJPY,  GCINVJPY,  GCINCFSS,  GCINVFSS,  GCINCITL, " +
                                            "  GCINVITL,  GCINCBRL,  GCINVBRL,  GCINCRRC,  GCINCRRV,  GCINCCNY,  GCINVCNY, " +
                                            "  GCINCRNC,  GCINCRNV,  GCINCARS,  GCINVARS,  GCINCR$C,  GCINCR$V,  GCESTA, " +
                                            "  GCORIG,  GCDISA,  GCUSRA,  GCFECA) " +
                                            "select GCNRSEC,  GCFEEF,  GCHOEF,  GCEURO,  GCUSDC,  GCUSDV,  GCUFV,  GCYEN,  GCLIBRA, " +
                                            "  GCFRANCO,  GCREAL,  GCYUAN,  GC$ARG,  GCCOT1,  GCCOT2,  GCCOT3,  GCCOT4,  GCINCEUR, " +
                                            "  GCINVEUR,  GCINCREC,  GCINCREV,  GCINCUSD,  GCINVUSD,  GCINCRSC,  GCINCREM,  GCINCJPY, " +
                                            "  GCINVJPY,  GCINCFSS,  GCINVFSS,  GCINCITL,  GCINVITL,  GCINCBRL,  GCINVBRL,  GCINCRRC, " +
                                            "  GCINCRRV,  GCINCCNY,  GCINVCNY,  GCINCRNC,  GCINCRNV,  GCINCARS,  GCINVARS,  GCINCR$C, " +
                                            "  GCINCR$V,  GCESTA,  GCORIG,  GCDISA,  GCUSRA,  GCFECA " +
                                            "  from GLP701  " +
                                            "where GCNRSEC=(select GCNRSEC from GLP703 where GCBANK=1)");
                            result = queryInsertHistorico.executeUpdate();
                            return result;
                        }else {
                            LOGGER.error("No se actualizo GLP701 con los tipos de cambio");
                            return null;
                        }
                    }else{
                        LOGGER.error("Error al registrar cotizaciones: no se registro nada en GLP701");
                        return null;
                    }

                }else {
                    LOGGER.error("Error al registrar cotizaciones: no se encontro secuencia en GLP703");
                    return null;
                }

            }
        });
    }

    /**
     * Una peticion solo para provocar timeout, no registra nada
     * Misteriosamente en la primera peticion siempre da timeout
     * @return
     */
    public void obtenerIndicadorTimeout(){
        Date fechaHoy=new Date();
        final String fecha = FormatosUtils.fechaFormateadaConYYYY(fechaHoy);

        //Limpiar configuracion de proxy
        String proxyHost=System.getProperty("http.proxyHost");
        String proxyPort=System.getProperty("http.proxyPort");
        System.clearProperty("http.proxyHost");
        System.clearProperty("http.proxyPort");

        IndicadoresService service = new IndicadoresService();

        Indicadores client = service.getIndicadoresPort();
        List<Indicador> indicadores = client.obtenerIndicador(IndicadorBCB.TIPO_DE_CAMBIO.getCodigo(), MonedaBCB.DOLAR_COMPRA.getCodigo(), fecha);
        LOGGER.debug(" ===============indicadores.size >>> " +indicadores.size());

        //REESTABLECER CONFIGURACION PROXY SI EXISTIA
        if(proxyHost!=null&&!proxyHost.isEmpty()){
            System.setProperty("http.proxyHost",proxyHost);
        }
        if(proxyPort!=null&&!proxyPort.isEmpty()){
            System.setProperty("http.proxyPort",proxyPort);
        }
    }

    /**
     * Obtiene indicadores tasa TRE del BCB
     * @param fechaHoy
     * @return
     */
    public TasaTreWrapper obtenerIndicadoresTre(Date fechaHoy){

        TasaTreWrapper tasaTreWrapper= new TasaTreWrapper();
        final String fecha = FormatosUtils.fechaFormateadaConYYYY(fechaHoy);

        //Limpiar configuracion de proxy
        String proxyHost=System.getProperty("http.proxyHost");
        String proxyPort=System.getProperty("http.proxyPort");
        System.clearProperty("http.proxyHost");
        System.clearProperty("http.proxyPort");


        IndicadoresService service = new IndicadoresService();

        Indicadores client = service.getIndicadoresPort();
        /* No funca con
        Client cxfClient = ClientProxy.getClient(client);
        //java.lang.ClassCastException: com.sun.xml.ws.client.sei.SEIStub cannot be cast to org.apache.cxf.frontend.ClientProxy
        cxfClient.getOutInterceptors().add(outgoingPayloadInterceptor);
        cxfClient.getInInterceptors().add(incomingPayloadInterceptor.setName("alguna cosa"));
        */

        /*Trace Service */
        BindingProvider bindingProvider = ((BindingProvider) client);
        List handlerChain = bindingProvider.getBinding().getHandlerChain();
        handlerChain.add(new LogSOAPHandler(this));
        bindingProvider.getBinding().setHandlerChain(handlerChain);

        final MonedaTreBCB[] monedas=MonedaTreBCB.values();


        for (MonedaTreBCB moneda: monedas) {

            LOGGER.debug(" ==============Moneda: "+moneda.getCodigo()+"-"+moneda.name());
            List<Indicador> indicadores = client.obtenerIndicador(IndicadorBCB.TRE.getCodigo(), moneda.getCodigo(),
                    fecha);
            IndicadorTreBCBWrapper indicadorBCBWrapper = new IndicadorTreBCBWrapper(IndicadorBCB.TRE);
            for (Indicador indicador : indicadores) {
                String campo=indicador.getCodDato();
                LOGGER.debug(" =============== >>> " +indicador.getCodDato()+"--"+ indicador.getDato());
                indicadorBCBWrapper.asignarValor(indicador);

            }
            if(indicadorBCBWrapper.getCodigoErrorBCB().getCodigo()== CodigoErrorBCB.OK.getCodigo()) {
                if(indicadorBCBWrapper.getMonedatreBCB().getCodigo()==MonedaTreBCB.NM.getCodigo()) {
                    tasaTreWrapper.setMn(indicadorBCBWrapper.getValor());
                }else if(indicadorBCBWrapper.getMonedatreBCB().getCodigo()== MonedaTreBCB.ME.getCodigo()) {
                    tasaTreWrapper.setMe(indicadorBCBWrapper.getValor());
                }else if(indicadorBCBWrapper.getMonedatreBCB().getCodigo()==MonedaTreBCB.UFV.getCodigo()) {
                    tasaTreWrapper.setMnUFV(indicadorBCBWrapper.getValor());
                }else if(indicadorBCBWrapper.getMonedatreBCB().getCodigo()==MonedaTreBCB.MV_DOL.getCodigo()) {
                    tasaTreWrapper.setMvDol(indicadorBCBWrapper.getValor());
                }
                tasaTreWrapper.setFechaVigenciaInicial(indicadorBCBWrapper.getFechaVigenciaInicio());                        tasaTreWrapper.setFechaVigenciaFinal(indicadorBCBWrapper.getFechaVigenciaFin());
            }
            LOGGER.debug(" ===============indicadorBCBWrapper >>> " +indicadorBCBWrapper);

        }
        //REESTABLECER CONFIGURACION PROXY SI EXISTIA
        if(proxyHost!=null&&!proxyHost.isEmpty()){
            System.setProperty("http.proxyHost",proxyHost);
        }
        if(proxyPort!=null&&!proxyPort.isEmpty()){
            System.setProperty("http.proxyPort",proxyPort);
        }

        return tasaTreWrapper;
    }

    /**
     * Verifica registro de la tasa TRE y notifica en caso de no encontrarlo
     */
    public void verificarRegistroTRE(Date fechaActual){
        LOGGER.info("fechaActual: {}",fechaActual);
        if(fechaActual!=null){
            Integer cantidad= verificarSiEstaIngresadaLNP011(fechaActual);
            if(!(cantidad!=null && cantidad.intValue()==4)){ //cuatro indicadores
                //reportar a responsable de carga
                String horaIni = medioAmbiente.getValorDe(Variables.BCB_TRE_CARGA_VERIFICACION_HORA_INICIO,
                        Variables.BCB_TRE_CARGA_VERIFICACION_HORA_INICIO_DEFAULT);
                String horaFin = medioAmbiente.getValorDe(Variables.BCB_TRE_CARGA_VERIFICACION_HORA_FIN,
                        Variables.BCB_TRE_CARGA_VERIFICACION_HORA_FIN_DEFAULT);
                LocalTime timeIni = LocalTime.parse(horaIni);
                LocalTime timeFin = LocalTime.parse(horaFin);
                LocalTime timeNow = LocalTime.now();
                //verificar hora tope
                if(timeNow.isAfter(timeIni.minusMinutes(3))
                        && timeNow.isBefore(timeFin.plusMinutes(3))) {
                    notificarNoCargaTRE(fechaActual,false);

                }
                //reportar a jefe de responsable de carga
                String horaIniEscalada = medioAmbiente.getValorDe(Variables.BCB_TRE_CARGA_VERIFICACION_ESCALADA_HORA_INICIO,
                        Variables.BCB_TRE_CARGA_VERIFICACION_ESCALADA_HORA_INICIO_DEFAULT);
                String horaFinEscalada = medioAmbiente.getValorDe(Variables.BCB_TRE_CARGA_VERIFICACION_ESCALADA_HORA_FIN,
                        Variables.BCB_TRE_CARGA_VERIFICACION_ESCALADA_HORA_FIN_DEFAULT);

                LocalTime timeIniEscalada = LocalTime.parse(horaIniEscalada);
                LocalTime timeFinEscalada = LocalTime.parse(horaFinEscalada);

                //verificar hora tope
                if(timeNow.isAfter(timeIniEscalada.minusMinutes(3))
                        && timeNow.isBefore(timeFinEscalada.plusMinutes(3))) {
                    notificarNoCargaTRE(fechaActual,true);
                }
            }else{
                LOGGER.info("La tasa TRE de esta semana ya esta registrada. Fecha consultada[{}]",fechaActual);
            }
        }else{
            LOGGER.error("Error, no se pudo obtener la fecha actual de la Tap001");
        }

    }

    /**
     * Obtiene indicadores de tipo de cambio del BCB
     * @param fechaHoy
     * @return
     */
    public CotizacionBolsinWraper obtenerIndicadores(Date fechaHoy){
        CotizacionBolsinWraper cotizacionBolsin= new CotizacionBolsinWraper();
        final String fecha = FormatosUtils.fechaFormateadaConYYYY(fechaHoy);

        //Limpiar configuracion de proxy
        String proxyHost=System.getProperty("http.proxyHost");
        String proxyPort=System.getProperty("http.proxyPort");
        System.clearProperty("http.proxyHost");
        System.clearProperty("http.proxyPort");


        IndicadoresService service = new IndicadoresService();

        Indicadores client = service.getIndicadoresPort();
        /* No funca con
        Client cxfClient = ClientProxy.getClient(client);
        //java.lang.ClassCastException: com.sun.xml.ws.client.sei.SEIStub cannot be cast to org.apache.cxf.frontend.ClientProxy
        cxfClient.getOutInterceptors().add(outgoingPayloadInterceptor);
        cxfClient.getInInterceptors().add(incomingPayloadInterceptor.setName("alguna cosa"));
        */

        /*Trace Service */
        BindingProvider bindingProvider = ((BindingProvider) client);
        List handlerChain = bindingProvider.getBinding().getHandlerChain();
        handlerChain.add(new LogSOAPHandler(this));
        bindingProvider.getBinding().setHandlerChain(handlerChain);

        final MonedaBCB[] monedas=MonedaBCB.values();

        for (MonedaBCB moneda: monedas) {

            LOGGER.debug(" ==============Moneda: "+moneda.getCodigo()+"-"+moneda.name());
            List<Indicador> indicadores = client.obtenerIndicador(IndicadorBCB.TIPO_DE_CAMBIO.getCodigo(), moneda.getCodigo(), fecha);
            IndicadorBCBWrapper indicadorBCBWrapper = new IndicadorBCBWrapper(IndicadorBCB.TIPO_DE_CAMBIO);
            for (Indicador indicador : indicadores) {
                String campo=indicador.getCodDato();
                LOGGER.debug(" =============== >>> " +indicador.getCodDato()+"--"+ indicador.getDato());
                indicadorBCBWrapper.asignarValor(indicador);

            }
            if(indicadorBCBWrapper.getCodigoErrorBCB().getCodigo()== CodigoErrorBCB.OK.getCodigo()) {
                if(indicadorBCBWrapper.getMonedaBCB().getCodigo()==MonedaBCB.EURO.getCodigo()) {
                    cotizacionBolsin.setEuroCotizacion(indicadorBCBWrapper.getValor());
                }else if(indicadorBCBWrapper.getMonedaBCB().getCodigo()==MonedaBCB.DOLAR_COMPRA.getCodigo()) {
                    cotizacionBolsin.setDolarCompraCotizacion(indicadorBCBWrapper.getValor());
                }else if(indicadorBCBWrapper.getMonedaBCB().getCodigo()==MonedaBCB.DOLAR_VENTA.getCodigo()) {
                    cotizacionBolsin.setDolarVentaCotizacion(indicadorBCBWrapper.getValor());
                }else if(indicadorBCBWrapper.getMonedaBCB().getCodigo()==MonedaBCB.UFV.getCodigo()) {
                    cotizacionBolsin.setUfvCotizacion(indicadorBCBWrapper.getValor());
                }else if(indicadorBCBWrapper.getMonedaBCB().getCodigo()==MonedaBCB.JAPON_YEN.getCodigo()) {
                    cotizacionBolsin.setYenCotizacion(indicadorBCBWrapper.getValor());
                }else if(indicadorBCBWrapper.getMonedaBCB().getCodigo()==MonedaBCB.INGLATERRA_LIBRA.getCodigo()) {
                    cotizacionBolsin.setLibraCotizacion(indicadorBCBWrapper.getValor());
                }else if(indicadorBCBWrapper.getMonedaBCB().getCodigo()==MonedaBCB.SUIZA_FRANCO.getCodigo()) {
                    cotizacionBolsin.setFrancoCotizacion(indicadorBCBWrapper.getValor());
                }else if(indicadorBCBWrapper.getMonedaBCB().getCodigo()==MonedaBCB.BRASIL_REAL.getCodigo()) {
                    cotizacionBolsin.setRealCotizacion(indicadorBCBWrapper.getValor());
                }else if(indicadorBCBWrapper.getMonedaBCB().getCodigo()==MonedaBCB.YUAN_RENMINBI_OFFSHORE.getCodigo()) {
                    cotizacionBolsin.setYuanCotizacion(indicadorBCBWrapper.getValor());
                }else if(indicadorBCBWrapper.getMonedaBCB().getCodigo()==MonedaBCB.ARGENTINA_PESO.getCodigo()) {
                    cotizacionBolsin.setArgentinoPesoCotizacion(indicadorBCBWrapper.getValor());
                }
            }
            LOGGER.debug(" ===============indicadorBCBWrapper >>> " +indicadorBCBWrapper);

        }
        //REESTABLECER CONFIGURACION PROXY SI EXISTIA
        if(proxyHost!=null&&!proxyHost.isEmpty()){
            System.setProperty("http.proxyHost",proxyHost);
        }
        if(proxyPort!=null&&!proxyPort.isEmpty()){
            System.setProperty("http.proxyPort",proxyPort);
        }
        System.clearProperty("http.proxyPort");
        return cotizacionBolsin;
    }

    /**
     * Verifica si la tasa tre ha sido registrada en la LNP011
     *
     * @return Null si no existe, si no 4 de los cuatro registros
     */
    public Integer verificarSiEstaIngresadaLNP011(Date fecha) {

        Integer cantidad= doWithTransaction(new WithTransaction<Integer>() {
            @Override
            public Integer call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                @SuppressWarnings("unchecked") OpenJPAQuery<Integer> sqlAgencia =
                        entityManager.createNativeQuery("select count(*) from LNP011 a where LRBK=1 and LRPRNR in(14,15,16,17) and TIMESTAMP_FORMAT(?,'DD/MM/YYYY') between  TIMESTAMP_FORMAT(CHAR(LREFFD ),'YYYYDDD') and (TIMESTAMP_FORMAT(CHAR(LREFFD ),'YYYYDDD')+ 6 DAYS)",
                                Integer.class);
                sqlAgencia.setParameter(1,sdf.format(fecha));
                Integer count=null;
                try {
                    count =sqlAgencia.getSingleResult();
                } catch (NoResultException e) {
                    return null;
                }
                return count;
            }
        });
        return cantidad;

    }
    private String formatearMensajeTRE(String plantilla,LocalTime timeNow){
        String mensaje=plantilla.replaceAll(BCB_COTIZACIONES_VAR_HORA_ACTUAL,timeNow.format(dateTimeFormatter));
        return mensaje;
    }
    public  void notificarNoCargaTRE(Date fecha,boolean aLosJefes){
        LocalTime timeNow = LocalTime.now();
        String msg=null;
        String msgCorto=null;
        String destinatarios =null;
        String destinatariosSms =null;
        int prioridad= medioAmbiente.getValorIntDe(Variables.BCB_TRE_SMS_PRIORIDAD,Variables.BCB_TRE_SMS_PRIORIDAD_DEFAULT);
        if(!aLosJefes){
            msg=formatearMensajeTRE(medioAmbiente.getValorDe(Variables.BCB_TRE_CARGA_VERIFICACION_EMAIL,
                    Variables.BCB_TRE_CARGA_VERIFICACION_EMAIL_DEFAULT),timeNow);
            msgCorto=formatearMensajeTRE(medioAmbiente.getValorDe(Variables.BCB_TRE_CARGA_VERIFICACION_SMS,
                    Variables.BCB_TRE_CARGA_VERIFICACION_SMS_DEFAULT),timeNow);
            destinatarios = medioAmbiente.getValorDe(Variables.BCB_TRE_CARGA_EMAIL_ENVIO,
                    Variables.BCB_TRE_CARGA_EMAIL_ENVIO_DEFAULT);
            destinatariosSms = medioAmbiente.getValorDe(Variables.BCB_TRE_CARGA_SMS_ENVIO,
                    Variables.BCB_TRE_CARGA_SMS_ENVIO_DEFAULT);
        }else{
            msg=formatearMensajeTRE(medioAmbiente.getValorDe(Variables.BCB_TRE_CARGA_VERIFICACION_ESCALADA_EMAIL,
                    Variables.BCB_TRE_CARGA_VERIFICACION_ESCALADA_EMAIL_DEFAULT),timeNow);
            msgCorto=formatearMensajeTRE(medioAmbiente.getValorDe(Variables.BCB_TRE_CARGA_VERIFICACION_ESCALADA_SMS,
                    Variables.BCB_TRE_CARGA_VERIFICACION_ESCALADA_SMS_DEFAULT),timeNow);
            destinatarios = medioAmbiente.getValorDe(Variables.BCB_TRE_CARGA_ESCALADA_EMAIL_ENVIO,
                    Variables.BCB_TRE_CARGA_ESCALADA_EMAIL_ENVIO_DEFAULT);
            destinatariosSms = medioAmbiente.getValorDe(Variables.BCB_TRE_CARGA_ESCALADA_SMS_ENVIO,
                    Variables.BCB_TRE_CARGA_ESCALADA_SMS_ENVIO_DEFAULT);
        }
        LOGGER.debug("msgCortos[{}]destinatariosSms[{}]. ",msgCorto,destinatariosSms );
        LOGGER.debug("msg[{}]destinatarios[{}]. ",msg,destinatarios );
        notificarSMS("AlertaTRE",msgCorto,destinatariosSms,prioridad);
        notificarPorEmail("",msg,destinatarios);
    }

    /**
     * Verifica si la tasa tre ha sido registrada en la ISBT81101
     * @return Null si no existe, si no 1
     */
    public Integer verificarSiEstaIngresadaISBT81101(Date fecha) {

        Integer cantidad= doWithTransaction(new WithTransaction<Integer>() {
            @Override
            public Integer call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                @SuppressWarnings("unchecked") OpenJPAQuery<Integer> sqlAgencia =
                        entityManager.createNativeQuery("select  count(*) from ISBT81101 a where TIMESTAMP_FORMAT(?,'DD/MM/YYYY') between TIMESTAMP_FORMAT(LNEFFDI,'DD/MM/YYYY') and TIMESTAMP_FORMAT(LNEFFDF,'DD/MM/YYYY')",
                                Integer.class);
                sqlAgencia.setParameter(1,sdf.format(fecha));
                Integer count=null;
                try {
                    count =sqlAgencia.getSingleResult();
                } catch (NoResultException e) {
                    return null;
                }
                return count;
            }
        });
        return cantidad;

    }
    /**
     * Registra la tasa TRE del BCB en la tabla de los miercoles    
     * @param tasaTreWrapper
     * @return
     * @throws InvalidParameterException
     */
    public Integer registrarTasaTRE(final TasaTreWrapper tasaTreWrapper) throws InvalidParameterException {

        tasaTreWrapper.verificarNulos();
        return doWithTransaction(new WithTransaction<Integer>() {
            @Override
            public Integer call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String strQuery=
                        "UPDATE ISBT81101       "+
                                "   SET LNTITCF = 'PFecha_Al ',  "+
                                "       LNTITMN = 'MN    ',      "+
                                "       LNTITMV = 'MVDOL ',      "+
                                "       LNTITUF = 'MN-UFV',      "+
                                "       LNTITME = 'ME    ',      "+
                                "       LNTITDI = 'VFecha_Del',  "+
                                "       LNTITDF = 'VFecha_Al ',  "+
                                "       LCAFFDI = '10/12/2010',  "+ //estos valores no son utilizados
                                "       LCAFFDF = '06/01/2010',   "+

                        "       LNRATEMN = ?, " +
                        "       LNRATEMV = ?, " +
                        "       LNRATEUF = ?, " +
                        "       LNRATEME = ?, " +
                        "       LNEFFDI = ?, " +
                        "       LNEFFDF = ? ";
                Query queryUpdate = entityManager.createNativeQuery(strQuery);
                LOGGER.info("TRE query {}",strQuery);
                LOGGER.info("TRE params: LNRATEMN= {}, LNRATEMV={}, LNRATEUF={}, LNRATEME={}, LNEFFDI={}, LNEFFDF={}",tasaTreWrapper.getMn(),tasaTreWrapper.getMvDol(),tasaTreWrapper.getMnUFV(),tasaTreWrapper.getMe(),sdf.format(tasaTreWrapper.getFechaVigenciaInicial()),sdf.format(tasaTreWrapper.getFechaVigenciaFinal()));
                queryUpdate.setParameter(1,tasaTreWrapper.getMn());
                queryUpdate.setParameter(2,tasaTreWrapper.getMvDol());
                queryUpdate.setParameter(3,tasaTreWrapper.getMnUFV());
                queryUpdate.setParameter(4,tasaTreWrapper.getMe());
                queryUpdate.setParameter(5,sdf.format(tasaTreWrapper.getFechaVigenciaInicial()));
                queryUpdate.setParameter(6,sdf.format(tasaTreWrapper.getFechaVigenciaFinal()));
                Integer result = queryUpdate.executeUpdate();
                LOGGER.info("Se registro en ISBT81101 fechas[{}]-[{}]. Rows affected:{} " +
                                " se aplicara el jueves en la madrugada.",
                        tasaTreWrapper.getFechaVigenciaInicial(),tasaTreWrapper.getFechaVigenciaFinal(),result);
                return result;
            }
        });
    }
    public void motrarISBT81101() {

        String resultado= doWithTransaction(new WithTransaction<String>() {
            @Override
            public String call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                @SuppressWarnings("unchecked") OpenJPAQuery<String> sqlFechaJuliana =
                        entityManager.createNativeQuery("select concat('LNRATEMN:',concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(LNRATEMN,', LNRATEMV:'), LNRATEMV),', LNRATEUF:'), LNRATEUF),', LNRATEME:'), LNRATEME),', LNEFFDI'), LNEFFDI),', LNEFFDF'), LNEFFDF)) from ISBT81101",
                                String.class);
                String resultado=null;
                try {
                    resultado =sqlFechaJuliana.getSingleResult();
                } catch (NoResultException e) {
                    return null;
                }
                return resultado;
            }
        });
        if (resultado!=null){
            LOGGER.info("Contenido ISBT81101: {} ", resultado);
        }else{
            LOGGER.error("No se pudo obtener ISBT81101");
        }

    }
    /**
     * Intentar registrar TRE en la ISBT81101
     * @param parsedDate
     */
    public void intentarRegistrarTRE(Date parsedDate){
        try{
            LOGGER.info("TRE Antes de intentar registrar");
            motrarISBT81101();
            Integer estadoIngreso= verificarSiEstaIngresadaISBT81101(parsedDate);
            if(estadoIngreso==null || estadoIngreso.intValue()==0){
                try{
                    obtenerIndicadorTimeout(); //solo para provocar timeout
                }catch (Exception eTimeout){
                    //LOGGER.error("Aqui siempre da timeout ",eTimeout);
                    LOGGER.warn("Aqui siempre da timeout ");
                }
                TasaTreWrapper tasaTreWrapper=obtenerIndicadoresTre(parsedDate);
                registrarTasaTRE(tasaTreWrapper);
                LOGGER.info("TRE despues de registrar");
                motrarISBT81101();
            }else {
                LOGGER.info("La tasa TRE para la fecha {} ya estaba registrada({}) ",parsedDate,estadoIngreso);
            }
        } catch (Throwable e) {
            LOGGER.error("No se obtuvo la tasa TRE correctamente, seguiremos intentando(mientras dure este job)",e);
        }
    }

    public void notificarPorEmail(String asunto,String mensaje){

        String destinatarios = medioAmbiente.getValorDe(Variables.BCB_COTIZACIONES_EMAIL_ENVIO,
                Variables.BCB_COTIZACIONES_EMAIL_ENVIO_DEFAULT);
        String[] arraysMail =  StringUtils.split(destinatarios, ",;");
        Mailer mail =   mailerFactory.getMailer(arraysMail);

        StringBuilder sb = null;

        if(mensaje!=null && mensaje.length()>0){
            sb = new StringBuilder();
            sb.append("\n");
            sb.append(mensaje);
            sb.append("\n");
            sb.append("Atentamente,\nSistema Aqua-ISB\n");
            try {
                mail.mailAndForget(asunto, sb.toString());
            } catch (Exception e) {

                LOGGER.error("Error: No se puede eviar el correo electronico con el siguiente detalle: \n" + sb.toString() + "\n", e);
            }
            return;
        }

        LOGGER.debug("CORREO A ENVIARSE: \n" + sb.toString());


    }
    public void notificarSMS(String titulo,String mensaje,String destinatarios,int prioridad){
        String cliente="0"; //no aplica
        String[] arraysDestinatarios =  StringUtils.split(destinatarios, ",;");
        for (String destinatario:arraysDestinatarios) {
            try{
                yellowPepperServicioDao.sendNotificacionYellowPepper(destinatario.trim(),titulo,mensaje,cliente,
                        BCB_COTIZACIONES_CLIENTE, BCB_COTIZACIONES_SISTEMA, BCB_COTIZACIONES_SERVICIO, prioridad);
            }catch (Exception ex){
                LOGGER.error("error al notificar SMS",ex);
            }
        }
    }

    public void notificarPorEmail(String asunto,String mensaje,String destinatarios){
        String[] arraysMail =  StringUtils.split(destinatarios, ",;");
        Mailer mail =   mailerFactory.getMailer(arraysMail);

        StringBuilder sb = null;

        if(mensaje!=null && mensaje.length()>0){
            sb = new StringBuilder();
            sb.append("\n");
            sb.append(mensaje);
            sb.append("\n");
            sb.append("Atentamente,\nSistema Aqua-ISB\n");
            try {
                mail.mailAndForget(asunto, sb.toString());
            } catch (Exception e) {

                LOGGER.error("Error: No se puede eviar el correo electronico con el siguiente detalle: \n" + sb.toString() + "\n", e);
            }
            return;
        }

        LOGGER.debug("CORREO A ENVIARSE: \n" + sb.toString());


    }
    public void notificarSMS(String titulo,String mensaje){
            String cliente="0"; //no aplica
        int prioridad= medioAmbiente.getValorIntDe(Variables.BCB_COTIZACIONES_SMS_PRIORIDAD,Variables.BCB_COTIZACIONES_SMS_PRIORIDAD_DEFAULT);
        String destinatarios = medioAmbiente.getValorDe(Variables.BCB_COTIZACIONES_SMS_ENVIO,
                Variables.BCB_COTIZACIONES_SMS_ENVIO_DEFAULT);
        String[] arraysDestinatarios =  StringUtils.split(destinatarios, ",;");
        for (String destinatario:arraysDestinatarios) {
            yellowPepperServicioDao.sendNotificacionYellowPepper(destinatario,titulo,mensaje,cliente,
                    BCB_COTIZACIONES_CLIENTE, BCB_COTIZACIONES_SISTEMA, BCB_COTIZACIONES_SERVICIO, prioridad);
        }
    }


    @Override
    public Future<ConsumoWeb> persistRequest(String request) {
        LOGGER.debug("Aqui guardar el request" + request);
            final ConsumoWeb consumoWeb = new ConsumoWeb();
            Future<ConsumoWeb> consumoWebFuture;
            consumoWebFuture = executor.submit(() -> {
                consumoWeb.setFecha(new Date());
                consumoWeb.setConsumidor(StringUtils.abbreviate(BCB_COTIZACIONES_SERVICIO, 10));
                if (request != null) {
                    consumoWeb.setPregunta(request);
                }
                consumoWebDao.persist(consumoWeb);
                LOGGER.debug("Registrando el consumo de WS.");
                return consumoWeb;
            });
            return consumoWebFuture;
    }

    @Override
    public void persistResponse(int httpStatus, String response,int millis,Future<ConsumoWeb> future) {
        LOGGER.debug("Aqui guardar el response httpstatus {} response {}" ,httpStatus, response);
        if (future != null) {
            final Future<ConsumoWeb> finalConsumoWebFuture = future;
            executor.submit(() -> {
                try {
                    final ConsumoWeb consumoWeb = finalConsumoWebFuture.get(10, TimeUnit.SECONDS);
                    consumoWeb.setStatus(httpStatus);
                    consumoWeb.setMilis(millis);
                    if (response != null) {
                        consumoWeb.setRespuesta(response);
                    }
                    consumoWebDao.merge(consumoWeb);
                    LOGGER.debug("Actualizando el consumo de WS.");
                } catch (InterruptedException e) {
                    LOGGER.error("Error: Actualizacion de consumo interrumpido.", e);
                } catch (TimeoutException e) {
                    LOGGER.error("Error: La persistencia de consumo web tarda mucho.", e);
                } catch (Exception e) {
                    LOGGER.error("Error: Ha ocurrido un error inesperado", e);
                }
            });
        }
    }
}
