package com.bisa.indicadoresbcb.util;

/**
 * Created by ccalle on 31/01/2017.
 */
public class EnumUtil {
    public static <T extends Enum<?>> T searchEnum(Class<T> enumeration,
                                                   String search) {
        for (T each : enumeration.getEnumConstants()) {
            if (each.name().compareToIgnoreCase(search) == 0) {
                return each;
            }
        }
        return null;
    }
    public static <T extends Enum<?>> T searchEnumField(Class<T> enumeration,
                                                   String search, String fieldName) {
        for (T each : enumeration.getEnumConstants()) {
            if (each.name().compareToIgnoreCase(search) == 0) {
                return each;
            }
        }
        return null;
    }
}
