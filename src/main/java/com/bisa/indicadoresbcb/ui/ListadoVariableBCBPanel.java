package com.bisa.indicadoresbcb.ui;

import bus.database.components.FiltroPanel;
import bus.env.api.Variables;
import bus.env.entities.Variable;
import bus.plumbing.api.RolesBisa;
import bus.users.api.Metadatas;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import java.util.Set;

/**
 * Created by ccalle on 04/04/2017.
 */
public class ListadoVariableBCBPanel extends FiltroPanel<Variable> {
    private static final long serialVersionUID = 1L;


    protected ListadoVariableBCBPanel(String id, final IModel<Variable> model1, IModel<Variable> model2) {
        super(id, model1, model2);
        Variable variable= new Variable();
        Set<String> roles = getSession().getMetaData(Metadatas.USER_META_DATA_KEY).getRoles();
        if(roles.contains(RolesBisa.ISB_ADMIN) ||(roles.contains(RolesBisa.BCB_COT)&&roles.contains(RolesBisa.BCB_TRE)) ){
            variable.setNombre(Variables.BCB_INDICADORES);
        }else if (roles.contains(RolesBisa.BCB_COT)) {
            variable.setNombre(Variables.BCB_COTIZACIONES);
        }else if (roles.contains(RolesBisa.BCB_TRE)) {
            variable.setNombre(Variables.BCB_TRE);
        }
        model1.setObject(variable);
        add(new TextField<String>("nombre", new PropertyModel<String>(model1, "nombre")));

    }

}
