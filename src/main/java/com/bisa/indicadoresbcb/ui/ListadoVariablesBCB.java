package com.bisa.indicadoresbcb.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.env.entities.Variable;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPanel;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxLink;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marcelo Morales @since 6/6/11
 * @author rsalvatierra modificado on 09/05/2016
 */
@AuthorizeInstantiation({RolesBisa.BCB_COT,RolesBisa.BCB_TRE, RolesBisa.ISB_ADMIN})
@Menu(value = "Par\u00e1metros Web Service BCB", subMenu = SubMenu.INDICADORES_BCB)
public class ListadoVariablesBCB extends Listado<Variable, Long> {

        private static final long serialVersionUID = -3502036478677387071L;

        private Panel panelModal;
        private WebMarkupContainer markupModal;

        @Override
        protected ListadoPanel<Variable, Long> newListadoPanel(String id) {
            return new ListadoPanelBandeja(id);
        }

        @Override
        protected void onInitialize() {
            super.onInitialize();

        }



        private class ListadoPanelBandeja extends ListadoPanel<Variable, Long> {
            private static final long serialVersionUID = 1L;

            public ListadoPanelBandeja(String id) {
                super(id);
            }

            @Override
            protected List<IColumn<Variable, String>> newColumns(ListadoDataProvider<Variable, Long> dataProvider) {
                List<IColumn<Variable, String>> lista = new ArrayList<IColumn<Variable, String>>();
                lista.add(new PropertyColumn<Variable, String>(Model.of("nombre"), "nombre", "nombre"));
                lista.add(new PropertyColumn<Variable, String>(Model.of("valor"), "valor", "valor"));

                lista.add(new AbstractColumn<Variable, String>(Model.of("Editar")) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public void populateItem(final Item<ICellPopulator<Variable>> cellItem, String componentId, IModel<Variable> rowModel) {
                        cellItem.add(new LinkPanel<Variable>(componentId, Model.of("Editar"), rowModel) {

                            private static final long serialVersionUID = 1L;

                            @Override
                            protected AbstractLink newLink(String linkId, final IModel<Variable> objectModel) {
                                return new IndicatingAjaxLink<Variable>(linkId, objectModel) {

                                    private static final long serialVersionUID = 1L;

                                    @Override
                                    protected void onInitialize() {
                                        super.onInitialize();
                                    }

                                    @Override
                                    public void onClick(AjaxRequestTarget target) {
                                        LOGGER.debug("Click en detalle");
                                        setResponsePage(EditarVariableBCB.class, new PageParameters().add("id", getModelObject().getId()));

                                    }
                                };
                            }
                        });
                    }
                });



                return lista;
            }

            @Override
            protected Class<? extends Dao<Variable, Long>> getProviderClazz() {
                return VariableBCBDao.class;
            }

            @Override
            protected Variable newObject1() {
                Variable variable = new Variable();

                return variable;
            }

            @Override
            protected Variable newObject2() {
                Variable variable = new Variable();

                return variable;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<Variable> newFiltroPanel(String id, IModel<Variable> model1, IModel<Variable> model2) {
                return new ListadoVariableBCBPanel(id, model1, model2);
            }

            @Override
            protected boolean isVisibleData() {
                Variable variable = getModel1().getObject();
                return !(StringUtils.isEmpty(variable.getNombre()) &&
                        variable.getId() == null
                );
            }

        }


    }
