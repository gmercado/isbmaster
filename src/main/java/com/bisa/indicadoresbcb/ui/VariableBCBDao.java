package com.bisa.indicadoresbcb.ui;

import bus.config.dao.CryptUtils;
import bus.database.dao.DaoImplCached;
import bus.database.model.BasePrincipal;
import bus.env.api.MedioAmbiente;
import bus.env.entities.Variable;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.timer.Timer;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by ccalle on 04/04/2017.
 */

public class VariableBCBDao extends DaoImplCached<Variable, Long> implements MedioAmbiente, Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(bus.env.dao.VariableDao.class);

    private final CryptUtils cryptUtils;

    private final Configuration configuration;

    @Inject
    public VariableBCBDao(@BasePrincipal EntityManagerFactory entityManagerFactory,
                       CryptUtils cryptUtils,
                       Configuration configuration,
                       Ehcache cache) {
        super(entityManagerFactory, cache);
        this.cryptUtils = cryptUtils;
        this.configuration = configuration;
    }

    @Override
    public Variable merge(Variable data) {
        Variable merged = super.merge(data);
        ehcache.remove(merged.getNombre());
        return merged;
    }

    @Override
    public Variable remove(Long id) {
        Variable removed = super.remove(id);
        ehcache.remove(removed.getNombre());
        return removed;
    }

    @Override
    protected Path<Long> countPath(Root<Variable> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<Variable> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.<String>get("nombre"));
        paths.add(p.<String>get("valor"));
        return paths;
    }

    @Override
    public boolean existe(final String variable) {

        Element element = ehcache.get(variable);
        if (element != null && !element.isExpired() && element.getObjectValue() != null) {
            return true;
        }

        try {
            return doWithTransaction((entityManager, t) -> {
                OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
                OpenJPACriteriaQuery<Variable> query = cb.createQuery(Variable.class);
                query.where(cb.equal(query.from(Variable.class).get("nombre"), variable));
                TypedQuery<Variable> typedQuery = entityManager.createQuery(query);

                List<Variable> resultList = typedQuery.getResultList();
                return resultList != null && resultList.size() > 0;
            });
        } catch (Exception e) {
            LOGGER.error("No pude encontrar una variable, asumo que no existe", e);
            return false;
        }
    }

    @Override
    public String valor(String nombre, Object... param) {
        return null;
    }

    @Override
    public List<Variable> getValorBulkDe(String nombreVariable) {
        return null;
    }

    @Override
    public String getValorDe(final String nombreVariable, String valorPorDefecto) {
        Element element = ehcache.get(nombreVariable);
        if (element != null && !element.isExpired() && element.getObjectValue() != null) {
            return (String) element.getObjectValue();
        }
        Variable variable;
        try {
            variable = doWithTransaction((entityManager, t) -> {
                OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
                OpenJPACriteriaQuery<Variable> query = cb.createQuery(Variable.class);
                query.where(cb.equal(query.from(Variable.class).get("nombre"), nombreVariable));

                TypedQuery<Variable> typedQuery = entityManager.createQuery(query);
                List<Variable> resultList = typedQuery.getResultList();
                if (resultList.isEmpty()) {
                    return null;
                }
                if (resultList.size() > 1) {
                    LOGGER.warn("Buscando una variable con mas de un registro, {}, deviolviendo el primero", resultList);
                }
                return resultList.get(0);
            });
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error al obtener el valor de la variable, asumiendo el valor por defecto", e);
            variable = null;
        }

        boolean putInCache = true;

        String valor;
        if (variable == null) {
            putInCache = false;
            valor = configuration.getString(nombreVariable, valorPorDefecto);
        } else {
            valor = variable.getValor();
        }

        if (cryptUtils.esCryptLegado(valor)) {
            valor = cryptUtils.dec(valor);
        } else if (cryptUtils.esOpenssl(valor)) {
            valor = cryptUtils.decssl(valor);
        }
        valor = Joiner.on("\n").join(Iterables.filter(Splitter.on("\n").split(valor), input -> input != null && !input.startsWith("// ")));

        if (putInCache) {
            element = new Element(nombreVariable, valor);
            element.setTimeToLive((int) (Timer.ONE_DAY / 1000L));
            ehcache.put(element);
        }

        return valor;
    }

    @Override
    public Long getValorLongDe(String nombreVariable, Long valorPorDefecto) {
        try {
            return Long.parseLong(getValorDe(nombreVariable, ""));
        } catch (NumberFormatException e) {
            LOGGER.warn("Utilizando el valor por defecto para la variable entera {}", nombreVariable);
            return valorPorDefecto;
        }
    }

    @Override
    public Integer getValorIntDe(String nombreVariable, Integer valorPorDefecto) {
        try {
            return Integer.parseInt(getValorDe(nombreVariable, ""));
        } catch (NumberFormatException e) {
            LOGGER.warn("Utilizando el valor por defecto para la variable entera {}", nombreVariable);
            return valorPorDefecto;
        }
    }

    @Override
    public BigDecimal getValorBigDecimalDe(String variable, BigDecimal valorPorDefecto) {
        try {
            return new BigDecimal(getValorDe(variable, ""));
        } catch (NumberFormatException e) {
            LOGGER.warn("Utilizando el valor por defecto para la variable decimal {}", variable);
            return valorPorDefecto;
        }
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<Variable> from, Variable example, Variable example2) {

        List<Predicate> list = new LinkedList<Predicate>();
        if (example != null) {
            list.add(cb.like(from.<String>get("nombre"), "%"+example.getNombre()+"%"));
        }
        return list.toArray(new Predicate[list.size()]);
    }
}