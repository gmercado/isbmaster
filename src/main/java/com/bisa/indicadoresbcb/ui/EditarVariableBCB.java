package com.bisa.indicadoresbcb.ui;

import bus.env.dao.VariableDao;
import bus.env.entities.Variable;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.users.api.Metadatas;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.validator.StringValidator;

import java.util.Set;

/**
 * @author Marcelo Morales Date: 9/5/11 Time: 9:02 AM
 * @author rsalvatierra modificado on 09/05/2016
 */
@Titulo("Edici\u00f3n de variable")
@AuthorizeInstantiation({RolesBisa.BCB_COT,RolesBisa.BCB_TRE, RolesBisa.ISB_ADMIN})
public class EditarVariableBCB extends BisaWebPage {

    /**
     * @param parameters necesito el "id" de la variable.
     */
    public EditarVariableBCB(PageParameters parameters) {
        super(parameters);
        IModel<Variable> model = new LoadableDetachableModel<Variable>() {
            @Override
            protected Variable load() {
                return getInstance(VariableDao.class).find(getPageParameters().get("id").toLong());
            }
        };

        Form<Variable> editar;
        add(editar = new Form<>("editar", new CompoundPropertyModel<>(model)));

        editar.add(new Label("nombre"));

        TextArea<String> valor;
        editar.add(valor = new TextArea<>("valor"));
        valor.setRequired(true);
        valor.add(StringValidator.maximumLength(1000));
        valor.setOutputMarkupId(true);

        editar.add(new IndicatingAjaxButton("editar", editar) {

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                try {
                    VariableDao variableDao = getInstance(VariableDao.class);
                    Variable v = (Variable) form.getModelObject();
                    variableDao.merge(v,getSession().getMetaData(Metadatas.USER_META_DATA_KEY).getUserlogon(),getIPAcceso(getRequest()));
                    LOGGER.info("El usuario {} ha actualizado el valor de la variable {}",
                            getSession().getMetaData(Metadatas.USER_META_DATA_KEY).getUserlogon(),
                            v.getNombre());
                    getSession().info("Operaci\u00f3n ejecutada satisfactoriamente");
                    setResponsePage(ListadoVariablesBCB.class);
                } catch (Exception e) {
                    LOGGER.error("Ha ocurrido un error inesperado al editar variable", e);
                    getSession().error("Ha ocurrido un error inesperado, comun\u00edquese con soporte t\u00e9cnico");
                    target.add(feedbackPanel);
                }
            }
        });

        editar.add(new Button("cancelar") {

            @Override
            public void onSubmit() {
                getSession().info("Operaci\u00f3n cancelada");
                setResponsePage(ListadoVariablesBCB.class);
            }
        }.setDefaultFormProcessing(false));
    }
}
