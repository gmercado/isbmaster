package com.bisa.indicadoresbcb.tipos;

import com.bisa.indicadoresbcb.consumer.Indicador;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ccalle on 31/01/2017.
 */
public class IndicadorBCBWrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(IndicadorBCBWrapper.class);
    private CodigoErrorBCB codigoErrorBCB;
    private IndicadorBCB indicadorBCB;
    private MonedaBCB monedaBCB;
    private Date fecha;
    private float valor;

    public IndicadorBCBWrapper(IndicadorBCB indicadorBCB) {
        this.indicadorBCB= indicadorBCB;
    }
    public void asignarValor(Indicador indicador){
        String campo=indicador.getCodDato();
        if(campo.contains("CodError")){
            codigoErrorBCB= convertirACodError(indicador.getDato());
        } else if(campo.contains("CodMoneda")){
            monedaBCB= convertirAMonedaBCB(indicador.getDato());
        }else if(campo.contains("Fecha")){
            //31/01/2017
            SimpleDateFormat format =new SimpleDateFormat("dd/MM/yyyy");
            try {
                fecha= format.parse(indicador.getDato());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else if(campo.contains("Valor")){
            try {
                valor= Double.valueOf(indicador.getDato()).floatValue();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }
    public static MonedaBCB convertirAMonedaBCB(String name) {
        int codigo=Integer.parseInt(name);
        for (MonedaBCB e : MonedaBCB.values()) {
            if (e.getCodigo()==codigo) {
                return e;
            }
        }
        return null;
    }

    public static CodigoErrorBCB convertirACodError(String name) {
        int codigo=Integer.parseInt(name);
        for (CodigoErrorBCB e : CodigoErrorBCB.values()) {
            if (e.getCodigo()==codigo) {
                return e;
            }
        }
        return null;
    }
    public CodigoErrorBCB getCodigoErrorBCB() {
        return codigoErrorBCB;
    }

    public void setCodigoErrorBCB(CodigoErrorBCB codigoErrorBCB) {
        this.codigoErrorBCB = codigoErrorBCB;
    }

    public IndicadorBCB getIndicadorBCB() {
        return indicadorBCB;
    }

    public void setIndicadorBCB(IndicadorBCB indicadorBCB) {
        this.indicadorBCB = indicadorBCB;
    }

    public MonedaBCB getMonedaBCB() {
        return monedaBCB;
    }

    public void setMonedaBCB(MonedaBCB monedaBCB) {
        this.monedaBCB = monedaBCB;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "IndicadorBCBWrapper{" +
                "codigoErrorBCB=" + codigoErrorBCB +
                ", indicadorBCB=" + indicadorBCB +
                ", monedaBCB=" + monedaBCB +
                ", fecha=" + fecha +
                ", valor=" + valor +
                '}';
    }
}
