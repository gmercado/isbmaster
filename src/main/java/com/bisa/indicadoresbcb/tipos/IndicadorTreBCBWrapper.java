package com.bisa.indicadoresbcb.tipos;

import com.bisa.indicadoresbcb.consumer.Indicador;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ccalle on 31/01/2017.
 */
public class IndicadorTreBCBWrapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(IndicadorTreBCBWrapper.class);
    private CodigoErrorBCB codigoErrorBCB;
    private IndicadorBCB indicadorBCB;
    private MonedaTreBCB monedatreBCB;
    private Date fechaVigenciaInicio;
    private Date fechaVigenciaFin;
    private float valor;

    public IndicadorTreBCBWrapper(IndicadorBCB indicadorBCB) {
        this.indicadorBCB= indicadorBCB;
    }
    public void asignarValor(Indicador indicador){
        String campo=indicador.getCodDato();
        if(campo.contains("CodError")){
            codigoErrorBCB= convertirACodError(indicador.getDato());
        } else if(campo.contains("CodMoneda")){
            monedatreBCB= convertirAMonedaBCB(indicador.getDato());
        }else if(campo.contains("FechaVigenciaInicio")){
            //31/01/2017
            SimpleDateFormat format =new SimpleDateFormat("dd/MM/yyyy");
            try {
                fechaVigenciaInicio= format.parse(indicador.getDato());
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }else if(campo.contains("FechaVigenciaFin")){
            //31/01/2017
            SimpleDateFormat format =new SimpleDateFormat("dd/MM/yyyy");
            try {
                fechaVigenciaFin= format.parse(indicador.getDato());
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }else if(campo.contains("Valor")){
            try {
                //redondeando a 2 decimales segun normativa 393 ASFI
                LOGGER.debug("campo [{}], valor [{}]",campo,indicador.getDato());
                DecimalFormat df = new DecimalFormat("#.00");
                df.setRoundingMode(RoundingMode.HALF_UP);
                valor=df.parse(indicador.getDato()).floatValue();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }
    public static MonedaTreBCB convertirAMonedaBCB(String name) {
        int codigo=Integer.parseInt(name);
        for (MonedaTreBCB e : MonedaTreBCB.values()) {
            if (e.getCodigo()==codigo) {
                return e;
            }
        }
        return null;
    }

    public static CodigoErrorBCB convertirACodError(String name) {
        int codigo=Integer.parseInt(name);
        for (CodigoErrorBCB e : CodigoErrorBCB.values()) {
            if (e.getCodigo()==codigo) {
                return e;
            }
        }
        return null;
    }
    public CodigoErrorBCB getCodigoErrorBCB() {
        return codigoErrorBCB;
    }

    public void setCodigoErrorBCB(CodigoErrorBCB codigoErrorBCB) {
        this.codigoErrorBCB = codigoErrorBCB;
    }

    public IndicadorBCB getIndicadorBCB() {
        return indicadorBCB;
    }

    public void setIndicadorBCB(IndicadorBCB indicadorBCB) {
        this.indicadorBCB = indicadorBCB;
    }

    public MonedaTreBCB getMonedatreBCB() {
        return monedatreBCB;
    }

    public void setMonedatreBCB(MonedaTreBCB monedatreBCB) {
        this.monedatreBCB = monedatreBCB;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public Date getFechaVigenciaInicio() {
        return fechaVigenciaInicio;
    }

    public void setFechaVigenciaInicio(Date fechaVigenciaInicio) {
        this.fechaVigenciaInicio = fechaVigenciaInicio;
    }

    public Date getFechaVigenciaFin() {
        return fechaVigenciaFin;
    }

    public void setFechaVigenciaFin(Date fechaVigenciaFin) {
        this.fechaVigenciaFin = fechaVigenciaFin;
    }

    @Override
    public String toString() {
        return "IndicadorBCBWrapper{" +
                "codigoErrorBCB=" + codigoErrorBCB +
                ", indicadorBCB=" + indicadorBCB +
                ", monedaBCB=" + monedatreBCB +
                ", fechaVigenciaInicio=" + fechaVigenciaInicio +
                ", fechaVigenciaFin=" + fechaVigenciaFin +
                ", valor=" + valor +
                '}';
    }
}
