package com.bisa.indicadoresbcb.tipos;

/**
 * Created by ccalle on 31/01/2017.
 */
public enum CodigoErrorBCB {
    OK (0),//     Consulta con exito
    FALTA_ALGUN_PARAMETRO (1001),//  Alguno de los parametros esta vacio
    INDICADOR_NO_VALIDO (1002),//  Codigo de Tipo de Indicador no valido
    MONEDA_NO_VALIDA (1003),//  Codigo de Moneda no valido
    FECHA_NO_VALIDA (1004),//  Fecha invalida
    NO_EXISTE_VALOR (2001),//  No existe valor para los parametros dados
    ERROR_INTERNO_BCB (2002);//  Error interno en el BCB
    private final int codigo;


    CodigoErrorBCB(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo() {
        return codigo;
    }



}
