package com.bisa.indicadoresbcb.tipos;

/**
 * Created by ccalle on 31/01/2017.
 */
public enum MonedaTreBCB {
    NM(69),
    MV_DOL(75),
    UFV(76),
    ME(34);

    private final int codigo;


    MonedaTreBCB(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo() {
        return codigo;
    }
}
