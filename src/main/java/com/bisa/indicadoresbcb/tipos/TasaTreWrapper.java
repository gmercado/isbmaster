package com.bisa.indicadoresbcb.tipos;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.Date;

/**
 * Tasa de Interes de Referencia *
 * Created by ccalle on 23/03/2017.
 */
public class TasaTreWrapper implements Serializable {
    //private String fechaCalculoInicial; no usado
    //private String fechaCalculoFinal; no usado
    private Float mn; //'Tasa MN'
    private Float mvDol; //'Tasa MVDOL'
    private Float mnUFV; //'Index Rate UFV'
    private Float me; //'Tasa ME'
    private Date fechaVigenciaInicial;
    private Date fechaVigenciaFinal;

    public TasaTreWrapper() {
    }
    public void verificarNulos() throws InvalidParameterException {
        if(mn==null) throw new InvalidParameterException("No se encontro el indicador MN");
        if(mvDol==null) throw new InvalidParameterException("No se encontro el indicador MVDOL ");
        if(mnUFV==null) throw new InvalidParameterException("No se encontro el indicador MN UFV");
        if(me==null) throw new InvalidParameterException("No se encontro el indicador ME");
        if(fechaVigenciaInicial==null)  throw new InvalidParameterException("No se encontro la fecha de vigencia inicial");
        if(fechaVigenciaFinal==null)  throw new InvalidParameterException("No se encontro la fecha de vigencia final");
    }
    public Float getMn() {
        return mn;
    }

    public void setMn(Float mn) {
        this.mn = mn;
    }

    public Float getMvDol() {
        return mvDol;
    }

    public void setMvDol(Float mvDol) {
        this.mvDol = mvDol;
    }

    public Float getMnUFV() {
        return mnUFV;
    }

    public void setMnUFV(Float mnUFV) {
        this.mnUFV = mnUFV;
    }

    public Float getMe() {
        return me;
    }

    public void setMe(Float me) {
        this.me = me;
    }

    public Date getFechaVigenciaInicial() {
        return fechaVigenciaInicial;
    }

    public void setFechaVigenciaInicial(Date fechaVigenciaInicial) {
        this.fechaVigenciaInicial = fechaVigenciaInicial;
    }

    public Date getFechaVigenciaFinal() {
        return fechaVigenciaFinal;
    }

    public void setFechaVigenciaFinal(Date fechaVigenciaFinal) {
        this.fechaVigenciaFinal = fechaVigenciaFinal;
    }
}