package com.bisa.indicadoresbcb.tipos;

/**
 * Created by ccalle on 31/01/2017.
 */
public enum MonedaBCB {
    EURO(53),
    DOLAR_COMPRA(34),
    DOLAR_VENTA(35),
    UFV(76),
    JAPON_YEN(20),
    INGLATERRA_LIBRA(33),
    SUIZA_FRANCO(31),
    BRASIL_REAL(6),
    YUAN_RENMINBI_OFFSHORE(26),
    ARGENTINA_PESO(1);
    /*
    Varios estan en el servicio del BCB, pero no en el AS400
    Especificaciones técnicas del Servicio Web de Indicadores del BCB v.3.2
     */
    private final int codigo;


    MonedaBCB(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo() {
        return codigo;
    }
}
