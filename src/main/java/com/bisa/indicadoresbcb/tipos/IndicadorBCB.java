package com.bisa.indicadoresbcb.tipos;

/**
 * Created by ccalle on 31/01/2017.
 */
public enum IndicadorBCB {
    TIPO_DE_CAMBIO(1),
    TRE(2), //TASA DE INTERES DE REFERENCIA
    UFV(3); //TASA ACTIVA DE PARIDAD REFERENCIAL
    private final int codigo;


    IndicadorBCB(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo() {
        return codigo;
    }
}
