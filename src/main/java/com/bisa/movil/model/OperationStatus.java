package com.bisa.movil.model;

public enum OperationStatus {

    NO_USADO, //0
    EN_ESPERA, // 1
    APROBADA, // 2
    RECHAZADA_USUARIO, // 3
    RECHAZADA_SISTEMA // 4

}
