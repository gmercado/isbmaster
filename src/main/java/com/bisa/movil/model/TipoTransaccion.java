package com.bisa.movil.model;

public enum TipoTransaccion {

    /**
     * Transferencia cuentas propias.
     */
    TRACUE("Transferencia a Cuenta propia", true),

    /**
     * Transferencia a terceros.
     */
    TRATER("Transferencia a Cuenta Bisa", true),

    /**
     * Otros bancos
     */
    TRAOB("Transferencia a Cuenta otro banco", true),

    /**
     * Solicitud de clave móvil
     */
    SOLCLA("Solicitud de clave móvil", true),

    /**
     * Pago de préstamo
     */
    PAGPRE("Pago de Préstamo", false),

    /**
     * Pago de tarjeta de crédito
     */
    PAGTC("Pago de Tarjeta de Crédito", true),

    /**
     * Giro Movil
     */
    GIRMOV("Giro Movil", true),

    /**
     * Adelanto de tarjeta de crédito.
     */
    ADETC("Adelanto Tarjeta de Crédito", true),

    /**
     * Aprobaciones de cheques de gerencia
     */
    CHEGER("Cheque de Gerencia", true),

    /**
     * Aprobaciones de giros al interior
     */
    GIRO("Giro al Interior", true),

    /**
     * Aprobaciones de lotes de pago
     */
    LOTE("Lote de Pagos", true),

    /**
     * Aprobaciones de Beneficiarios
     */
    BENEF("Beneficiario", true),

    /**
     * Aprobaciones de Transferencias al Exterior
     */
    TRANSEXT("Transferencia al Exterior", true),

    /**
     * Aprobaciones de Boletas On-Line
     */
    BOLETAS("Boleta On-Line", true),

    /**
     * Pago de servicios On-Line.
     */
    PAGSERV("Pago de Servicios", true),

    /**
     * Pago de colegios On-Line
     */
    PAGCOL("Pago de Pensiones", true),

    /**
     * Pago de colegios On-Line
     */
    PAGCOB("Cobranzas", true);

    private final String string;

    private final boolean mostrarOperacion;

    private TipoTransaccion(final String descripcion, boolean mostrarOperacion) {
        string = descripcion;
        this.mostrarOperacion = mostrarOperacion;
    }


    public String getDescripcion() {
        return string;
    }

    public boolean isMostrarOperacion() {
        return mostrarOperacion;
    }
}
