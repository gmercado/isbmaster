package com.bisa.movil.model;

public enum TipoAuthorization {

    /**
     * Aceptación.
     */
    A,

    /**
     * Rechazo.
     */
    R
}
