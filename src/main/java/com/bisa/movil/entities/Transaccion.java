package com.bisa.movil.entities;

import bus.interfaces.ebisa.tipos.TransaccionEstado;
import bus.monitor.as400.FormatosUtils;
import bus.monitor.as400.MonedasUtils;
import com.bisa.movil.model.TipoTransaccion;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "EBP01")
public class Transaccion implements Serializable {

    private static final long serialVersionUID = 5725289960146290341L;

    public static final String APLICACION = "ebisamovil";

    public static final String PAGO_SERVICIOS_FG = "pagoservicios";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "E01ID")
    private Long id;

    @Column(name = "E01NROCLI", precision = 10, scale = 0)
    private BigDecimal cliente;

    @Column(name = "E01CODTRN", length = 4, nullable = false)
    private String codigoTransaccion;

    @Column(name = "E01IMPORTE", precision = 15, scale = 2, nullable = false)
    private BigDecimal importe;

    @Column(name = "E01CODMON", nullable = false)
    private Short moneda;

    @Column(name = "E01FECPROC")
    private Long fechaProcesamiento;

    @Column(name = "E01POSTEO", length = 1)
    private String posteo;

    @Column(name = "E01NUMCAJA", precision = 4, scale = 0)
    private BigDecimal numeroCaja;

    @Column(name = "E01NUMSEC", precision = 7, scale = 0)
    private BigDecimal numeroSecuencia;

    @Column(name = "E01CTAORI", precision = 16, scale = 0)
    private BigDecimal cuentaOrigen;

    @Column(name = "E01CTADEST", length = 100)
    private String cuentaDestinoDescripcion;

    @Column(name = "E01NROOPE")
    private Long numeroOperacion;

    @Column(name = "E01ESTADO", length = 100)
    private String estado;

    @Column(name = "E01FECING")
    private Date fechaIngreso;

    @Column(name = "E01NOTAS", length = 30)
    private String notas;

    @Column(name = "E01CODERR", length = 3)
    private String codigoError;

    @Column(name = "E01DESTCTA", length = 20)
    private String cuentaDestino;

    @Column(name = "E01DESTBNK", precision = 4, scale = 2)
    private BigDecimal bancoDestino;

    @Column(name = "E01DESTSUC", length = 10)
    private String sucursalDestino;

    @Column(name = "E01DESTTPO", length = 30)
    private String tipoDestino;

    @Column(name = "E01DESTTIT", length = 50)
    private String titularDestino;

    @Column(name = "E01DESTMON")
    private Short monedaDestino;

    @Column(name = "E01CTAORIM")
    private Short monedaCuentaOrigen;

    @Column(name = "E01CTAORIT", length = 1)
    private String tipoCuentaOrigen;

    @Column(name = "E01HORPROC")
    private Integer horaProcesamiento;

    @Column(name = "E01USUARIO", length = 25)
    private String usuario;

    @Column(name = "E01NOMUSR", length = 25)
    private String nombreUsuario;

    @Column(name = "E01CODBEN", length = 20)
    private String codigoBeneficiario;

    @Column(name = "E01CODEST", length = 4)
    @Enumerated(EnumType.STRING)
    private TransaccionEstado codigoEstado;

    @Column(name = "E01SENDCOR")
    private Character enviarCorreo;

    @Column(name = "E01SENDSMS")
    private Character enviarSMS;

    @Column(name = "E01CORREO", length = 50)
    private String correo;

    @Column(name = "E01CELULAR")
    private String celular;

    @Version
    @Column(name = "E01VER")
    private long version;

    @Column(name = "E01CREAPP")
    private String aplicacionCreadora;

    @Column(name = "E01UPDAPP")
    private String aplicacionModificadora;

    @Column(name = "E01TIPOPE", length = 10)
    @Enumerated(EnumType.STRING)
    private TipoTransaccion tipoOperacion;

    @Column(name = "E01CODVAL", length = 5)
    private String errorEbisa;

    @Column(name = "E01COT", precision = 7, scale = 5)
    private BigDecimal cotizacion;

    @Column(name = "E01COM", precision = 10, scale = 5)
    private BigDecimal comision;

    @Column(name = "E01PORCEN")
    private BigDecimal porcentaje;

    @Column(name = "E01UPDTS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tiempoActualizacion;

    @Column(name = "E01CODADI", length = 15)
    private String codigoAdicional;

    @Column(name = "E01IMPDEB", precision = 15, scale = 2)
    private BigDecimal importeDebitado;

    @Column(name = "E01SALDO", precision = 15, scale = 2)
    private BigDecimal saldoLuegoTransaccion;

    @Column(name = "E01FORENT", length = 50)
    private String formaEntrega;

    @Column(name = "E01NITCI", length = 15)
    private String nitciFactura;

    @Column(name = "E01NOMFAC", length = 40)
    private String nombreFactura;

    @Column(name = "E01RESFAC", length = 10)
    private String resultadoFacturacion;

    @Column(name = "E01MENAQUA", length = 500)
    private String mensajeAqua;

    @Column(name = "E01DOCBEN", length = 20)
    private String documentoBeneficiario;

    @Column(name = "E01TDOCBEN", length = 3)
    private String tipoDocumentoBeneficiario;

    @Column(name = "E01NROLOTE")
    private BigDecimal nroLote;

    @Column(name = "E01TRANSEXT")
    private Long transExteriorId;

    @Column(name = "E01NROBOL")
    private BigDecimal numeroBoleta;

    @Column(name = "E01COMFOR")
    private BigDecimal comisionFormularios;

    @Column(name = "E01VALGAR")
    private BigDecimal valorGarantia;

    @Column(name = "E01TICKET", length = 10)
    private String ticket;

    @Column(name = "E01COEM", length = 10)
    private String pagoServicioCodigoEmpresa;

    @Column(name = "E01NOEM", length = 10)
    private String pagoServicioNombreEmpresa;

    @Column(name = "E01COSER", length = 10)
    private String pagoServicioCodigoServicio;

    @Column(name = "E01DESER", length = 10)
    private String pagoServicioDescripcionServicio;

    @Column(name = "E01COTIPO", length = 10)
    private String pagoServicioCodigoTipoBusqueda;

    @Column(name = "E01VALBUS", length = 30)
    private String pagoServicioValorBusqueda;

    @Column(name = "E01VALBUS2", length = 30)
    private String pagoServicioValorBusqueda2;

    @Column(name = "E01DESBUS", length = 30)
    private String pagoServicioDescripcionBusqueda;

    @Column(name = "E01PSNITCI", length = 15)
    private String pagoServicioNitciFactura;

    @Column(name = "E01PSNOMFAC", length = 40)
    private String pagoServicioNombreFactura;

    @Column(name = "E01PSCNEM")
    private Long pagoServicioCiudadNemetec;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "E01PSDIR", length = 100)
    private String pagoServicioDireccion;

    @Column(name = "E01PSALIAS", length = 30)
    private String pagoServiciosAlias;

    @Column(name = "E01PCNOMAL", length = 40)
    private String pagoColegiosNombreAlumno;

    @Column(name = "E01PCPER", length = 20)
    private String pagoColegiosPeriodo;

    @Transient
    private String mensajeNotificacion;


    public Transaccion() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCliente() {
        return cliente;
    }

    public void setCliente(BigDecimal cliente) {
        this.cliente = cliente;
    }

    public String getCodigoTransaccion() {
        return codigoTransaccion;
    }

    public void setCodigoTransaccion(String codigoTransaccion) {
        this.codigoTransaccion = codigoTransaccion;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public Short getMoneda() {
        return moneda;
    }

    public void setMoneda(Short moneda) {
        this.moneda = moneda;
    }

    public Long getFechaProcesamiento() {
        return fechaProcesamiento;
    }

    public void setFechaProcesamiento(Long fechaProcesamiento) {
        this.fechaProcesamiento = fechaProcesamiento;
    }

    public void setFechaProcesamiento(Date fechaProcesamiento) {
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        String fechaStr = df.format(fechaProcesamiento);
        this.fechaProcesamiento = Long.parseLong(fechaStr);
    }

    public String getPosteo() {
        return posteo;
    }

    public void setPosteo(String posteo) {
        this.posteo = posteo;
    }

    public BigDecimal getNumeroCaja() {
        return numeroCaja;
    }

    public void setNumeroCaja(BigDecimal numeroCaja) {
        this.numeroCaja = numeroCaja;
    }

    public BigDecimal getNumeroSecuencia() {
        return numeroSecuencia;
    }

    public void setNumeroSecuencia(BigDecimal numeroSecuencia) {
        this.numeroSecuencia = numeroSecuencia;
    }

    public BigDecimal getCuentaOrigen() {
        return cuentaOrigen;
    }

    public void setCuentaOrigen(BigDecimal cuentaOrigen) {
        this.cuentaOrigen = cuentaOrigen;
    }

    public String getCuentaDestinoDescripcion() {
        return cuentaDestinoDescripcion;
    }

    public void setCuentaDestinoDescripcion(String cuentaDestinoDescripcion) {
        this.cuentaDestinoDescripcion = cuentaDestinoDescripcion;
    }

    public Long getNumeroOperacion() {
        return numeroOperacion;
    }

    public void setNumeroOperacion(Long numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public String getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(String codigoError) {
        this.codigoError = codigoError;
    }

    public String getCuentaDestino() {
        return cuentaDestino;
    }

    public void setCuentaDestino(String cuentaDestino) {
        this.cuentaDestino = cuentaDestino;
    }

    public BigDecimal getBancoDestino() {
        return bancoDestino;
    }

    public void setBancoDestino(BigDecimal bancoDestino) {
        this.bancoDestino = bancoDestino;
    }

    public String getSucursalDestino() {
        return sucursalDestino;
    }

    public void setSucursalDestino(String sucursalDestino) {
        this.sucursalDestino = sucursalDestino;
    }

    public String getTipoDestino() {
        return tipoDestino;
    }

    public void setTipoDestino(String tipoDestino) {
        this.tipoDestino = tipoDestino;
    }

    public String getTitularDestino() {
        return titularDestino;
    }

    public void setTitularDestino(String titularDestino) {
        this.titularDestino = titularDestino;
    }

    public Short getMonedaDestino() {
        return monedaDestino;
    }

    public void setMonedaDestino(Short monedaDestino) {
        this.monedaDestino = monedaDestino;
    }

    public Short getMonedaCuentaOrigen() {
        return monedaCuentaOrigen;
    }

    public void setMonedaCuentaOrigen(Short monedaCuentaOrigen) {
        this.monedaCuentaOrigen = monedaCuentaOrigen;
    }

    public String getTipoCuentaOrigen() {
        return tipoCuentaOrigen;
    }

    public void setTipoCuentaOrigen(String tipoCuentaOrigen) {
        this.tipoCuentaOrigen = tipoCuentaOrigen;
    }

    public Integer getHoraProcesamiento() {
        return horaProcesamiento;
    }

    public void setHoraProcesamiento(Integer horaProcesamiento) {
        this.horaProcesamiento = horaProcesamiento;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCodigoBeneficiario() {
        return codigoBeneficiario;
    }

    public void setCodigoBeneficiario(String codigoBeneficiario) {
        this.codigoBeneficiario = codigoBeneficiario;
    }

    public TransaccionEstado getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(TransaccionEstado codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Character getEnviarCorreo() {
        return enviarCorreo;
    }

    public void setEnviarCorreo(Character enviarCorreo) {
        this.enviarCorreo = enviarCorreo;
    }

    public Character getEnviarSMS() {
        return enviarSMS;
    }

    public void setEnviarSMS(Character enviarSMS) {
        this.enviarSMS = enviarSMS;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getAplicacionCreadora() {
        return aplicacionCreadora;
    }

    public void setAplicacionCreadora(String aplicacionCreadora) {
        this.aplicacionCreadora = aplicacionCreadora;
    }

    public String getAplicacionModificadora() {
        return aplicacionModificadora;
    }

    public void setAplicacionModificadora(String aplicacionModificadora) {
        this.aplicacionModificadora = aplicacionModificadora;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Transaccion))
            return false;

        Transaccion that = (Transaccion) o;

        if (id != null ? !id.equals(that.id) : that.id != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public TipoTransaccion getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(TipoTransaccion tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getErrorEbisa() {
        return errorEbisa;
    }

    public void setErrorEbisa(String errorEbisa) {
        this.errorEbisa = errorEbisa;
    }

    public BigDecimal getCotizacion() {
        return cotizacion;
    }

    public void setCotizacion(BigDecimal cotizacion) {
        this.cotizacion = cotizacion;
    }

    public BigDecimal getComision() {
        return comision;
    }

    public void setComision(BigDecimal comision) {
        this.comision = comision;
    }

    public Date getTiempoActualizacion() {
        return tiempoActualizacion;
    }

    public void setTiempoActualizacion(Date tiempoActualizacion) {
        this.tiempoActualizacion = tiempoActualizacion;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public BigDecimal getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigDecimal porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getCodigoAdicional() {
        return codigoAdicional;
    }

    public void setCodigoAdicional(String codigoAdicional) {
        this.codigoAdicional = codigoAdicional;
    }

    public BigDecimal getImporteDebitado() {
        return importeDebitado;
    }

    public void setImporteDebitado(BigDecimal importeDebitado) {
        this.importeDebitado = importeDebitado;
    }

    public BigDecimal getSaldoLuegoTransaccion() {
        return saldoLuegoTransaccion;
    }

    public void setSaldoLuegoTransaccion(BigDecimal saldoLuegoTransaccion) {
        this.saldoLuegoTransaccion = saldoLuegoTransaccion;
    }

    public String getMensajeNotificacion() {        return mensajeNotificacion;    }
    public void setMensajeNotificacion(String mensajeNotificacion) {        this.mensajeNotificacion = mensajeNotificacion;    }

    @Override
    public String toString() {
        return "Transaccion{" +
                "id=" + id +
                ", cliente=" + cliente +
                ", codigoTransaccion=" + codigoTransaccion +
                ", importe=" + importe +
                ", moneda=" + moneda +
                ", fechaProcesamiento=" + fechaProcesamiento +
                ", posteo=" + posteo +
                ", numeroCaja=" + numeroCaja +
                ", numeroSecuencia=" + numeroSecuencia +
                ", cuentaOrigen=" + cuentaOrigen +
                ", cuentaDestinoDescripcion=" + cuentaDestinoDescripcion +
                ", numeroOperacion=" + numeroOperacion +
                ", estado=" + estado +
                ", fechaIngreso=" + fechaIngreso +
                ", notas=" + notas +
                ", codigoError=" + codigoError +
                ", cuentaDestino=" + cuentaDestino +
                ", bancoDestino=" + bancoDestino +
                ", sucursalDestino=" + sucursalDestino +
                ", tipoDestino=" + tipoDestino +
                ", titularDestino=" + titularDestino +
                ", monedaDestino=" + monedaDestino +
                ", monedaCuentaOrigen=" + monedaCuentaOrigen +
                ", tipoCuentaOrigen=" + tipoCuentaOrigen +
                ", horaProcesamiento=" + horaProcesamiento +
                ", usuario=" + usuario +
                ", nombreUsuario=" + nombreUsuario +
                ", codigoBeneficiario=" + codigoBeneficiario +
                ", codigoEstado=" + codigoEstado +
                ", enviarCorreo=" + enviarCorreo +
                ", enviarSMS=" + enviarSMS +
                ", correo=" + correo +
                ", celular=" + celular +
                ", version=" + version +
                ", aplicacionCreadora=" + aplicacionCreadora +
                ", aplicacionModificadora=" + aplicacionModificadora +
                ", tipoOperacion=" + tipoOperacion +
                ", errorEbisa=" + errorEbisa +
                ", cotizacion=" + cotizacion +
                ", comision=" + comision +
                ", porcentaje=" + porcentaje +
                ", tiempoActualizacion=" + tiempoActualizacion +
                ", codigoAdicional=" + codigoAdicional +
                ", importeDebitado=" + importeDebitado +
                ", saldoLuegoTransaccion=" + saldoLuegoTransaccion +
                ", formaEntrega=" + formaEntrega +
                ", nitciFactura=" + nitciFactura +
                ", nombreFactura=" + nombreFactura +
                ", resultadoFacturacion=" + resultadoFacturacion +
                ", mensajeAqua=" + mensajeAqua +
                ", documentoBeneficiario=" + documentoBeneficiario +
                ", tipoDocumentoBeneficiario=" + tipoDocumentoBeneficiario +
                ", nroLote=" + nroLote +
                ", transExteriorId=" + transExteriorId +
                ", numeroBoleta=" + numeroBoleta +
                ", comisionFormularios=" + comisionFormularios +
                ", valorGarantia=" + valorGarantia +
                ", ticket=" + ticket +
                ", pagoServicioCodigoEmpresa=" + pagoServicioCodigoEmpresa +
                ", pagoServicioCodigoServicio=" + pagoServicioCodigoServicio +
                ", pagoServicioCodigoTipoBusqueda=" + pagoServicioCodigoTipoBusqueda +
                ", pagoServicioValorBusqueda=" + pagoServicioValorBusqueda +
                ", pagoServicioNitciFactura=" + pagoServicioNitciFactura +
                ", pagoServicioNombreFactura=" + pagoServicioNombreFactura +
                ", pagoServiciosAlias=" + pagoServiciosAlias +
                '}';
    }

    public String getDescripcionCompletaOrigen(String titularCuentaOrigen) {
        final StringBuilder sb = new StringBuilder(100);

        sb.append("<b>").
                append(FormatosUtils.getDescripcionCuenta(this.getTipoCuentaOrigen().charAt(0))).
                append(" Nro. ");
        FormatosUtils.appendNumeroCuenta(this.getCuentaOrigen().toPlainString(), sb);
        sb.append("  ").
                append(" en ").
                append(MonedasUtils.getDescripcionHumanaByBisa(this.getMonedaCuentaOrigen())).
                append("</b> a nombre de ").append(titularCuentaOrigen);

        return sb.toString();
    }

    public String getDescripcionCompletaDestino() {
        final StringBuilder sb = new StringBuilder(100);

        // Pantalla para Cuenta Bisa - PANTALLA DE PROCESADA E HISTORICO

        sb.append("<b>").
                append(FormatosUtils.getDescripcionCuenta(this.getTipoDestino().charAt(0))).
                append(" Nro. ");
        FormatosUtils.appendNumeroCuenta(this.getCuentaDestino(), sb);
        sb.append("  ").
                append(" en ").
                append(MonedasUtils.getDescripcionHumanaByBisa(this.getMonedaDestino())).
                append("</b>");

        if (this.getTitularDestino() != null) {
            sb.append(" a nombre de ").
                    append(this.getTitularDestino());
        }

        return sb.toString();
    }

    public String getFormaEntrega() {
        return formaEntrega;
    }

    public void setFormaEntrega(String formaEntrega) {
        this.formaEntrega = formaEntrega;
    }

    public String getNitciFactura() {
        return nitciFactura;
    }

    public void setNitciFactura(String nitciFactura) {
        this.nitciFactura = nitciFactura;
    }

    public String getNombreFactura() {
        return nombreFactura;
    }

    public void setNombreFactura(String nombreFactura) {
        this.nombreFactura = nombreFactura;
    }

    public String getResultadoFacturacion() {
        return resultadoFacturacion;
    }

    public void setResultadoFacturacion(String resultadoFacturacion) {
        this.resultadoFacturacion = resultadoFacturacion;
    }

    public String getMensajeAqua() {
        return mensajeAqua;
    }

    public void setMensajeAqua(String mensajeAqua) {
        this.mensajeAqua = mensajeAqua;
    }

    public String getDocumentoBeneficiario() {
        return documentoBeneficiario;
    }

    public void setDocumentoBeneficiario(String documentoBeneficiario) {
        this.documentoBeneficiario = documentoBeneficiario;
    }

    public String getTipoDocumentoBeneficiario() {
        return tipoDocumentoBeneficiario;
    }

    public void setTipoDocumentoBeneficiario(String tipoDocumentoBeneficiario) {
        this.tipoDocumentoBeneficiario = tipoDocumentoBeneficiario;
    }

    public BigDecimal getNroLote() {
        return nroLote;
    }

    public void setNroLote(BigDecimal nroLote) {
        this.nroLote = nroLote;
    }

    public Long getTransExteriorId() {
        return transExteriorId;
    }

    public void setTransExteriorId(Long transExteriorId) {
        this.transExteriorId = transExteriorId;
    }

    public BigDecimal getNumeroBoleta() {
        return numeroBoleta;
    }

    public void setNumeroBoleta(BigDecimal numeroBoleta) {
        this.numeroBoleta = numeroBoleta;
    }

    public BigDecimal getComisionFormularios() {
        return comisionFormularios;
    }

    public void setComisionFormularios(BigDecimal comisionFormularios) {
        this.comisionFormularios = comisionFormularios;
    }

    public BigDecimal getValorGarantia() {
        return valorGarantia;
    }

    public void setValorGarantia(BigDecimal valorGarantia) {
        this.valorGarantia = valorGarantia;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getPagoServicioCodigoEmpresa() {
        return pagoServicioCodigoEmpresa;
    }

    public void setPagoServicioCodigoEmpresa(String pagoServicioCodigoEmpresa) {
        this.pagoServicioCodigoEmpresa = pagoServicioCodigoEmpresa;
    }

    public String getPagoServicioCodigoServicio() {
        return pagoServicioCodigoServicio;
    }

    public void setPagoServicioCodigoServicio(String pagoServicioCodigoServicio) {
        this.pagoServicioCodigoServicio = pagoServicioCodigoServicio;
    }

    public String getPagoServicioCodigoTipoBusqueda() {
        return pagoServicioCodigoTipoBusqueda;
    }

    public void setPagoServicioCodigoTipoBusqueda(String pagoServicioCodigoTipoBusqueda) {
        this.pagoServicioCodigoTipoBusqueda = pagoServicioCodigoTipoBusqueda;
    }

    public String getPagoServicioValorBusqueda() {
        return pagoServicioValorBusqueda;
    }

    public void setPagoServicioValorBusqueda(String pagoServicioValorBusqueda) {
        this.pagoServicioValorBusqueda = pagoServicioValorBusqueda;
    }

    public String getPagoServicioNitciFactura() {
        return pagoServicioNitciFactura;
    }

    public void setPagoServicioNitciFactura(String pagoServicioNitciFactura) {
        this.pagoServicioNitciFactura = pagoServicioNitciFactura;
    }

    public String getPagoServicioNombreFactura() {
        return pagoServicioNombreFactura;
    }

    public void setPagoServicioNombreFactura(String pagoServicioNombreFactura) {
        this.pagoServicioNombreFactura = pagoServicioNombreFactura;
    }

    public Long getPagoServicioCiudadNemetec() {
        return pagoServicioCiudadNemetec;
    }

    public void setPagoServicioCiudadNemetec(Long pagoServicioCiudadNemetec) {
        this.pagoServicioCiudadNemetec = pagoServicioCiudadNemetec;
    }

    public String getPagoServicioDireccion() {
        return pagoServicioDireccion;
    }

    public void setPagoServicioDireccion(String pagoServicioDireccion) {
        this.pagoServicioDireccion = pagoServicioDireccion;
    }

    public String getPagoServicioDescripcionBusqueda() {
        return pagoServicioDescripcionBusqueda;
    }

    public void setPagoServicioDescripcionBusqueda(String pagoServicioDescripcionBusqueda) {
        this.pagoServicioDescripcionBusqueda = pagoServicioDescripcionBusqueda;
    }

    public String getPagoServicioNombreEmpresa() {
        return pagoServicioNombreEmpresa;
    }

    public void setPagoServicioNombreEmpresa(String pagoServicioNombreEmpresa) {
        this.pagoServicioNombreEmpresa = pagoServicioNombreEmpresa;
    }

    public String getPagoServicioDescripcionServicio() {
        return pagoServicioDescripcionServicio;
    }

    public void setPagoServicioDescripcionServicio(String pagoServicioDescripcionServicio) {
        this.pagoServicioDescripcionServicio = pagoServicioDescripcionServicio;
    }

    public String getPagoColegiosNombreAlumno() {
        return pagoColegiosNombreAlumno;
    }

    public void setPagoColegiosNombreAlumno(String pagoColegiosNombreAlumno) {
        this.pagoColegiosNombreAlumno = pagoColegiosNombreAlumno;
    }

    public String getPagoColegiosPeriodo() {
        return pagoColegiosPeriodo;
    }

    public void setPagoColegiosPeriodo(String pagoColegiosPeriodo) {
        this.pagoColegiosPeriodo = pagoColegiosPeriodo;
    }

    public String getPagoServiciosAlias() {
        return pagoServiciosAlias;
    }

    public void setPagoServiciosAlias(String pagoServiciosAlias) {
        this.pagoServiciosAlias = pagoServiciosAlias;
    }

    public String getPagoServicioValorBusqueda2() {
        return pagoServicioValorBusqueda2;
    }

    public void setPagoServicioValorBusqueda2(String pagoServicioValorBusqueda2) {
        this.pagoServicioValorBusqueda2 = pagoServicioValorBusqueda2;
    }
}
