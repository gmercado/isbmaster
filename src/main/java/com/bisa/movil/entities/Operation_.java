package com.bisa.movil.entities;

import com.bisa.movil.model.OperationStatus;

import javax.persistence.metamodel.SingularAttribute;
import java.math.BigDecimal;
import java.util.Date;

@javax.persistence.metamodel.StaticMetamodel
        (value=com.bisa.movil.entities.Operation.class)
@javax.annotation.Generated
        (value="org.apache.openjpa.persistence.meta.AnnotationProcessor6",date="Tue Feb 06 12:31:21 BOT 2018")
public class Operation_ {
    public static volatile SingularAttribute<Operation,String> aplicacionCreadora;
    public static volatile SingularAttribute<Operation,String> aplicacionModificadora;
    public static volatile SingularAttribute<Operation,Date> creationDate;
    public static volatile SingularAttribute<Operation,Long> customerID;
    public static volatile SingularAttribute<Operation,Boolean> deleted;
    public static volatile SingularAttribute<Operation,String> detailRQ;
    public static volatile SingularAttribute<Operation,String> detailRS;
    public static volatile SingularAttribute<Operation,String> falseService;
    public static volatile SingularAttribute<Operation,String> immediateService;
    public static volatile SingularAttribute<Operation,BigDecimal> monto;
    public static volatile SingularAttribute<Operation,String> object;
    public static volatile SingularAttribute<Operation,Long> operationId;
    public static volatile SingularAttribute<Operation,BigDecimal> percentage;
    public static volatile SingularAttribute<Operation,OperationStatus> state;
    public static volatile SingularAttribute<Operation,String> trueService;
    public static volatile SingularAttribute<Operation,Long> userID;
}
