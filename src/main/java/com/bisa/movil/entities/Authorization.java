package com.bisa.movil.entities;

import com.bisa.movil.model.TipoAuthorization;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "tbl_Authorization")
public class Authorization implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "AuthorizationID")
    private Long authorizationID;

    @Column(name = "UserID")
    private Long userId;

    @Column(name = "Percentage")
    private Integer percentage;

    @Column(name = "OperationID")
    private Long operationId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DateTransact", nullable = false)
    private Date date;

    @Column(name = "CreationApp", length = 50)
    private String aplicacionCreadora;

    @Column(name = "UpdateApp", length = 50)
    private String aplicacionModificadora;

    @Column(name = "AuthType")
    @Enumerated(EnumType.STRING)
    private TipoAuthorization tipo;

    @Column(name = "Synchronized")
    private Boolean sincronizado;

    public Authorization() {
    }

    public Long getAuthorizationID() {
        return authorizationID;
    }

    public void setAuthorizationID(Long authorizationID) {
        this.authorizationID = authorizationID;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public Long getOperationId() {
        return operationId;
    }

    public void setOperationId(Long operationId) {
        this.operationId = operationId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAplicacionCreadora() {
        return aplicacionCreadora;
    }

    public void setAplicacionCreadora(String aplicacionCreadora) {
        this.aplicacionCreadora = aplicacionCreadora;
    }

    public String getAplicacionModificadora() {
        return aplicacionModificadora;
    }

    public void setAplicacionModificadora(String aplicacionModificadora) {
        this.aplicacionModificadora = aplicacionModificadora;
    }

    public TipoAuthorization getTipo() {
        return tipo;
    }

    public void setTipo(TipoAuthorization tipo) {
        this.tipo = tipo;
    }

    public Boolean getSincronizado() {
        return sincronizado;
    }

    public void setSincronizado(Boolean sincronizado) {
        this.sincronizado = sincronizado;
    }
}

