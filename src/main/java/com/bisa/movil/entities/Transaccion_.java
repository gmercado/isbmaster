package com.bisa.movil.entities;


import bus.interfaces.ebisa.tipos.TransaccionEstado;
import com.bisa.movil.model.TipoTransaccion;

import javax.persistence.metamodel.SingularAttribute;
import java.math.BigDecimal;
import java.util.Date;

@javax.persistence.metamodel.StaticMetamodel
        (value=com.bisa.movil.entities.Transaccion.class)
@javax.annotation.Generated
        (value="org.apache.openjpa.persistence.meta.AnnotationProcessor6",date="Tue Feb 06 12:31:21 BOT 2018")
public class Transaccion_ {
    public static volatile SingularAttribute<Transaccion,String> aplicacionCreadora;
    public static volatile SingularAttribute<Transaccion,String> aplicacionModificadora;
    public static volatile SingularAttribute<Transaccion,BigDecimal> bancoDestino;
    public static volatile SingularAttribute<Transaccion,String> celular;
    public static volatile SingularAttribute<Transaccion,BigDecimal> cliente;
    public static volatile SingularAttribute<Transaccion,String> codigoAdicional;
    public static volatile SingularAttribute<Transaccion,String> codigoBeneficiario;
    public static volatile SingularAttribute<Transaccion,String> codigoError;
    public static volatile SingularAttribute<Transaccion,TransaccionEstado> codigoEstado;
    public static volatile SingularAttribute<Transaccion,String> codigoTransaccion;
    public static volatile SingularAttribute<Transaccion,BigDecimal> comision;
    public static volatile SingularAttribute<Transaccion,BigDecimal> comisionFormularios;
    public static volatile SingularAttribute<Transaccion,String> correo;
    public static volatile SingularAttribute<Transaccion,BigDecimal> cotizacion;
    public static volatile SingularAttribute<Transaccion,String> cuentaDestino;
    public static volatile SingularAttribute<Transaccion,String> cuentaDestinoDescripcion;
    public static volatile SingularAttribute<Transaccion,BigDecimal> cuentaOrigen;
    public static volatile SingularAttribute<Transaccion,String> documentoBeneficiario;
    public static volatile SingularAttribute<Transaccion,Character> enviarCorreo;
    public static volatile SingularAttribute<Transaccion,Character> enviarSMS;
    public static volatile SingularAttribute<Transaccion,String> errorEbisa;
    public static volatile SingularAttribute<Transaccion,String> estado;
    public static volatile SingularAttribute<Transaccion,Date> fechaIngreso;
    public static volatile SingularAttribute<Transaccion,Long> fechaProcesamiento;
    public static volatile SingularAttribute<Transaccion,String> formaEntrega;
    public static volatile SingularAttribute<Transaccion,Integer> horaProcesamiento;
    public static volatile SingularAttribute<Transaccion,Long> id;
    public static volatile SingularAttribute<Transaccion,BigDecimal> importe;
    public static volatile SingularAttribute<Transaccion,BigDecimal> importeDebitado;
    public static volatile SingularAttribute<Transaccion,String> mensajeAqua;
    public static volatile SingularAttribute<Transaccion,Short> moneda;
    public static volatile SingularAttribute<Transaccion,Short> monedaCuentaOrigen;
    public static volatile SingularAttribute<Transaccion,Short> monedaDestino;
    public static volatile SingularAttribute<Transaccion,String> nitciFactura;
    public static volatile SingularAttribute<Transaccion,String> nombreFactura;
    public static volatile SingularAttribute<Transaccion,String> nombreUsuario;
    public static volatile SingularAttribute<Transaccion,String> notas;
    public static volatile SingularAttribute<Transaccion,BigDecimal> nroLote;
    public static volatile SingularAttribute<Transaccion,BigDecimal> numeroBoleta;
    public static volatile SingularAttribute<Transaccion,BigDecimal> numeroCaja;
    public static volatile SingularAttribute<Transaccion,Long> numeroOperacion;
    public static volatile SingularAttribute<Transaccion,BigDecimal> numeroSecuencia;
    public static volatile SingularAttribute<Transaccion,String> pagoColegiosNombreAlumno;
    public static volatile SingularAttribute<Transaccion,String> pagoColegiosPeriodo;
    public static volatile SingularAttribute<Transaccion,Long> pagoServicioCiudadNemetec;
    public static volatile SingularAttribute<Transaccion,String> pagoServicioCodigoEmpresa;
    public static volatile SingularAttribute<Transaccion,String> pagoServicioCodigoServicio;
    public static volatile SingularAttribute<Transaccion,String> pagoServicioCodigoTipoBusqueda;
    public static volatile SingularAttribute<Transaccion,String> pagoServicioDescripcionBusqueda;
    public static volatile SingularAttribute<Transaccion,String> pagoServicioDescripcionServicio;
    public static volatile SingularAttribute<Transaccion,String> pagoServicioDireccion;
    public static volatile SingularAttribute<Transaccion,String> pagoServicioNitciFactura;
    public static volatile SingularAttribute<Transaccion,String> pagoServicioNombreEmpresa;
    public static volatile SingularAttribute<Transaccion,String> pagoServicioNombreFactura;
    public static volatile SingularAttribute<Transaccion,String> pagoServicioValorBusqueda;
    public static volatile SingularAttribute<Transaccion,String> pagoServicioValorBusqueda2;
    public static volatile SingularAttribute<Transaccion,String> pagoServiciosAlias;
    public static volatile SingularAttribute<Transaccion,BigDecimal> porcentaje;
    public static volatile SingularAttribute<Transaccion,String> posteo;
    public static volatile SingularAttribute<Transaccion,String> resultadoFacturacion;
    public static volatile SingularAttribute<Transaccion,BigDecimal> saldoLuegoTransaccion;
    public static volatile SingularAttribute<Transaccion,String> sucursalDestino;
    public static volatile SingularAttribute<Transaccion,String> ticket;
    public static volatile SingularAttribute<Transaccion,Date> tiempoActualizacion;
    public static volatile SingularAttribute<Transaccion,String> tipoCuentaOrigen;
    public static volatile SingularAttribute<Transaccion,String> tipoDestino;
    public static volatile SingularAttribute<Transaccion,String> tipoDocumentoBeneficiario;
    public static volatile SingularAttribute<Transaccion,TipoTransaccion> tipoOperacion;
    public static volatile SingularAttribute<Transaccion,String> titularDestino;
    public static volatile SingularAttribute<Transaccion,Long> transExteriorId;
    public static volatile SingularAttribute<Transaccion,String> usuario;
    public static volatile SingularAttribute<Transaccion,BigDecimal> valorGarantia;
    public static volatile SingularAttribute<Transaccion,Long> version;
}