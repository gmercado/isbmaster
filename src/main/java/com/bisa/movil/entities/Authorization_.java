package com.bisa.movil.entities;

import com.bisa.movil.entities.Authorization;
import com.bisa.movil.model.TipoAuthorization;

import javax.persistence.metamodel.SingularAttribute;
import java.util.Date;

@javax.persistence.metamodel.StaticMetamodel
        (value=com.bisa.movil.entities.Authorization.class)
@javax.annotation.Generated
        (value="org.apache.openjpa.persistence.meta.AnnotationProcessor6",date="Tue Feb 06 12:31:21 BOT 2018")
public class Authorization_ {
    public static volatile SingularAttribute<Authorization,String> aplicacionCreadora;
    public static volatile SingularAttribute<Authorization,String> aplicacionModificadora;
    public static volatile SingularAttribute<Authorization,Long> authorizationID;
    public static volatile SingularAttribute<Authorization,Date> date;
    public static volatile SingularAttribute<Authorization,Long> operationId;
    public static volatile SingularAttribute<Authorization,Integer> percentage;
    public static volatile SingularAttribute<Authorization,Boolean> sincronizado;
    public static volatile SingularAttribute<Authorization,TipoAuthorization> tipo;
    public static volatile SingularAttribute<Authorization,Long> userId;
}
