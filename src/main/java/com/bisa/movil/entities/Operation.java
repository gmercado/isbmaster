package com.bisa.movil.entities;

import com.bisa.movil.model.OperationStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "tbl_Operation")
public class Operation  implements Serializable {
    private static final long serialVersionUID = 1541254L;
    @Id
    @Column(name = "OperationID")
    private Long operationId;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "StateID")
    private OperationStatus state;

    @Column(name = "DetailRQ")
    private String detailRQ;

    @Column(name = "DetailRS")
    private String detailRS;

    @Column(name = "Percentage")
    private BigDecimal percentage;

    @Column(name = "CreationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;


    @Column(name = "UserID")
    private Long userID;


    @Column(name = "CustomerID")
    private Long customerID;

    @Column(name = "Deleted")
    private Boolean deleted;

    @Column(name = "TrueService")
    private String trueService;

    @Column(name = "FalseService")
    private String falseService;

    @Column(name = "ImmediateService")
    private String immediateService;

    @Column(name = "Object")
    private String object;

    private BigDecimal monto;

    @Column(name = "CreationApp", length = 50)
    private String aplicacionCreadora;

    @Column(name = "UpdateApp", length = 50)
    private String aplicacionModificadora;

    public Operation() {
    }

    public Long getOperationId() {
        return operationId;
    }

    public void setOperationId(Long operationId) {
        this.operationId = operationId;
    }

    public OperationStatus getState() {
        return state;
    }

    public void setState(OperationStatus state) {
        this.state = state;
    }

    public String getDetailRQ() {
        return detailRQ;
    }

    public void setDetailRQ(String detailRQ) {
        this.detailRQ = detailRQ;
    }

    public String getDetailRS() {
        return detailRS;
    }

    public void setDetailRS(String detailRS) {
        this.detailRS = detailRS;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userId) {
        this.userID = userId;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerId) {
        this.customerID = customerId;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getTrueService() {
        return trueService;
    }

    public void setTrueService(String trueService) {
        this.trueService = trueService;
    }

    public String getFalseService() {
        return falseService;
    }

    public void setFalseService(String falseService) {
        this.falseService = falseService;
    }

    public String getImmediateService() {
        return immediateService;
    }

    public void setImmediateService(String immediateService) {
        this.immediateService = immediateService;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getAplicacionCreadora() {
        return aplicacionCreadora;
    }

    public void setAplicacionCreadora(String aplicacionCreadora) {
        this.aplicacionCreadora = aplicacionCreadora;
    }

    public String getAplicacionModificadora() {
        return aplicacionModificadora;
    }

    public void setAplicacionModificadora(String aplicacionModificadora) {
        this.aplicacionModificadora = aplicacionModificadora;
    }
}
