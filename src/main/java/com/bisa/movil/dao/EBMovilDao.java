package com.bisa.movil.dao;

import bus.database.dao.DaoImplBase;
import bus.database.model.BaseAuxiliar;
import bus.database.model.BasePrincipal;
import bus.interfaces.ebisa.tipos.TransaccionEstado;
import com.bisa.movil.entities.Authorization;
import com.bisa.movil.entities.Authorization_;
import com.bisa.movil.entities.Operation;
import com.bisa.movil.entities.Transaccion;
import com.bisa.movil.model.OperationStatus;
import com.bisa.movil.model.TipoAuthorization;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class EBMovilDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(EBMovilDao.class);

    private final TransaccionDao transaccionDao;

    private final DaoImplBase<Void> daoSQLServer;

    private static final BigDecimal CIEN = new BigDecimal("100");

    public static final String TRANSACCION_APLICACION = "ebisamovil";


    @Inject
    public EBMovilDao(@BasePrincipal EntityManagerFactory entityManagerFactoryAS400, TransaccionDao transaccionDao, @BaseAuxiliar EntityManagerFactory entityManagerFactory) {
        this.transaccionDao = new TransaccionDao(entityManagerFactoryAS400);
        this.daoSQLServer = new DaoImplBase<Void>(entityManagerFactory);
    }

    public void sincronizarAuthorizationsConTransacciones() {
        daoSQLServer.doWithTransaction(new DaoImplBase.WithTransaction<Object>() {

            @Override
            public Object call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                final OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();

                // 1. Buscar en la tabla tbl_autorization registros con “Syncronized=false” (registros insertados por el e-BISA)

                final TypedQuery<Authorization> query;
                {
                    final OpenJPACriteriaQuery<Authorization> cq = cb.createQuery(Authorization.class);
                    final Root<Authorization> from = cq.from(Authorization.class);
                    cq.where(cb.equal(from.get(Authorization_.sincronizado), false));
                    query = entityManager.createQuery(cq);
                    query.setMaxResults(200); // de doscientos en doscientos, para no tardar demasiado si nos atrasamos.
                }

                final List<Authorization> authorizationList = query.getResultList();

                LOGGER.debug("Se han encontrado las autorizaciones {} para sincronizar", authorizationList);

                for (Authorization authorization : authorizationList) {
                    // 1.2. Si la autorizacion se inserto hace mas de 15 segundos
                    // Para dar tiempo a que se actualicen todos los registros, se dio casos donde coinsidió la insersión
                    // con el proceso de sincronización, se espera 15 segundo por que puede haber una diferencia de relojes
                    if(authorization.getDate().compareTo(DateUtils.addSeconds(new Date(), -15))>=0) {
                        LOGGER.debug("posiblemente coinside proceso asincrono con insersion");
                        continue;
                    }

                    // 2. If (nueva autorizacion sincronizada=false y existe autorizacion de ebisamovil anterior)

                    final TypedQuery<Long> countQuery;
                    {
                        final OpenJPACriteriaQuery<Long> cq = cb.createQuery(Long.class);
                        final Root<Authorization> from = cq.from(Authorization.class);
                        cq.select(cb.count(from.get(Authorization_.authorizationID))).
                                where(
                                        cb.equal(from.get(Authorization_.operationId), authorization.getOperationId()),
                                        cb.lessThanOrEqualTo(from.get(Authorization_.date), authorization.getDate()),
                                        cb.equal(from.get(Authorization_.aplicacionCreadora), TRANSACCION_APLICACION),
                                        cb.notEqual(from.get(Authorization_.authorizationID), authorization.getAuthorizationID()));
                        countQuery = entityManager.createQuery(cq);
                    }

                    Long singleResult;
                    try {
                        singleResult = countQuery.getSingleResult();
                    } catch (NoResultException e) {
                        singleResult = 0L;
                    }

                    // por false: Si no existen registros, actualizar el campo Syncronized=true.
                    //    Finaliza sincronización del registro.

                    if (singleResult == 0L) {
                        authorization.setSincronizado(true);
                        authorization.setAplicacionModificadora(TRANSACCION_APLICACION);
                        LOGGER.debug("No es necesario sincronizar ebisa-ebisamovil AutID={} OpID={}",
                                new Object[]{authorization.getAuthorizationID(), authorization.getOperationId()});

                        entityManager.merge(authorization);
                        continue;
                    }

                    // 2.2 Si existen registros, actualizar los siguientes campos en la EBP01:
                    //-        E01FECPROC (fechaProcesamiento)
                    //-        E01HORPROC (horaProcesamiento)
                    //-        E01POSTEO  (posteo)
                    //-        E01ESTADO  (estado)
                    //-        E01CODERR  (codigoError)
                    //-        E01PORCEN  (porcentaje)
                    //-        E01NUMCAJA (numeroCaja)
                    //-        E01NUMSEC  (numeroSecuencia)
                    //-        E01UPDTS=Fecha/hora actualización
                    //-        E01UPDAPP=ebisamovil

                    //Los datos E01NUMCAJA Y E01NUMSEC se deben buscan según cada operación:
                    //·        Transferencias a cuentas propias se encuentra en el DetailRS de tbl_operation, tags: WNUMUSR y WNUMSEQ
                    //·        Transferencia a terceros y otros bancos los llena por defecto el e-BISA.

                    LOGGER.debug("Hay registros de ebisamovil para {}", authorization);

                    LOGGER.info("Sincronizando ebisa-ebisamovil AutID={} OpID={}",
                            authorization.getAuthorizationID(), authorization.getOperationId());


                    final Operation op = entityManager.find(Operation.class, authorization.getOperationId());

                    final String immediateService = op.getImmediateService();

                    Transaccion transaccion = null;
                    boolean guardarLuego = true;

                    try {
                        final long id = Long.parseLong(StringUtils.trimToEmpty(immediateService));
                        transaccion = transaccionDao.find(id);

                        transaccion.setTiempoActualizacion(new Date());
                        transaccion.setAplicacionModificadora(Transaccion.APLICACION);

                        final OperationStatus state = op.getState();

                        switch (state) {
                            case EN_ESPERA:
                                authorization.setTipo(TipoAuthorization.A);
                                transaccion.setCodigoEstado(TransaccionEstado.PEN);
                                transaccion.setEstado("Pendiente (" + op.getPercentage().toPlainString() + "%)");
                                transaccion.setPorcentaje(op.getPercentage());
                                break;

                            case APROBADA:
                                authorization.setTipo(TipoAuthorization.A);
                                transaccion.setCodigoEstado(TransaccionEstado.PRO);
                                transaccion.setPosteo("P");
                                transaccion.setEstado("Procesada");
                                transaccion.setPorcentaje(CIEN);
                                //TODO que es OperacionHistorica? ¿Cómo se usa?
                                /*OperacionHistorica operacionHistorica = obtenerOperacionHistorica(transaccion);
                                if (operacionHistorica != null) {
                                    operacionHistorica.actualizarTransaccionAprobada(transaccion, op);
                                }*/
                                break;

                            case RECHAZADA_USUARIO:
                                transaccion.setCodigoEstado(TransaccionEstado.REU);
                                transaccion.setFechaProcesamiento(new Date());
                                transaccion.setPosteo("R");
                                transaccion.setEstado("Rechazada(S)");

                                final TypedQuery<Long> countSiguientes;
                            {
                                final OpenJPACriteriaQuery<Long> cq = cb.createQuery(Long.class);
                                final Root<Authorization> from = cq.from(Authorization.class);
                                cq.select(cb.count(from.get(Authorization_.authorizationID))).where(
                                        cb.equal(from.get(Authorization_.operationId), authorization.getOperationId()),
                                        cb.greaterThanOrEqualTo(from.get(Authorization_.date), authorization.getDate()),
                                        cb.greaterThan(from.get(Authorization_.authorizationID), authorization.getAuthorizationID()));
                                countSiguientes = entityManager.createQuery(cq);
                            }

                            Long hayPosteriores;
                            try {
                                hayPosteriores = countSiguientes.getSingleResult();
                            } catch (NoResultException e) {
                                hayPosteriores = 0L;
                            }

                            if (hayPosteriores > 0) {
                                authorization.setTipo(TipoAuthorization.A);
                            } else {
                                authorization.setTipo(TipoAuthorization.R);
                            }

                            break;

                            case RECHAZADA_SISTEMA:
                                authorization.setTipo(TipoAuthorization.A);
                                transaccion.setCodigoEstado(TransaccionEstado.RES);
                                transaccion.setFechaProcesamiento(new Date());
                                transaccion.setPosteo("R");
                                transaccion.setEstado("Rechazada(S)");
                                //TODO que es OperacionHistorica? ¿Cómo se usa?
                                /*
                                OperacionHistorica opHist = obtenerOperacionHistorica(transaccion);
                                if (opHist != null) {
                                    opHist.actualizarTransaccionRechazada(transaccion, op);
                                }
                                */
                                break;
                        }


                        authorization.setAplicacionModificadora(Transaccion.APLICACION);
                        authorization.setSincronizado(true);

                    } catch (Exception e) {
                        LOGGER.error("Durante el proceso de sincronizacion, immediateService='" + immediateService +
                                "' de la operacion " + op.getOperationId(), e);
                        guardarLuego = false;
                    }

                    if (!guardarLuego) {
                        //  Ya ha salido en el log: no hacemos nada....
                        continue;
                    }

                    if (transaccion == null) {
                        //  Ya ha salido en el log: no hacemos nada....
                        continue;
                    }

                    if (LOGGER.isDebugEnabled()) {
                        // because the object allocation (Object[]) is guarded on debug....
                        //noinspection ObjectAllocationInLoop
                        LOGGER.debug("Ahora voy a guardar la transaccion (EBP01) {} y la autorizacion (tbl_authorization) {} operacion (tbl_operation) {}",
                                new Object[]{transaccion.getId(), authorization.getAuthorizationID(), op.getOperationId()});
                    }


                    transaccionDao.merge(transaccion);
                    entityManager.merge(authorization);
                }

                return null;
            }
        });
    }


}
