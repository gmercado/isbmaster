package com.bisa.movil.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.movil.entities.Transaccion;
import com.bisa.movil.entities.Transaccion_;
import com.google.inject.Inject;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author Marcelo Morales
 *         Created: 12/8/11 2:58 PM
 */
public class TransaccionDao extends DaoImpl<Transaccion, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransaccionDao.class);

    protected TransaccionDao() {
        super(null);
    }

    @Inject
    public TransaccionDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }


    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<Transaccion> from, Transaccion example, Transaccion example2) {
        if (example == null) {
            return null;
        }

        if (example.getId() != null) {
            return new Predicate[]{cb.equal(from.get(Transaccion_.id), example.getId())};
        }

        Predicate qbe = cb.qbe(from, example, Transaccion_.tiempoActualizacion);

        if (example.getTiempoActualizacion() != null && example2 != null && example2.getTiempoActualizacion() != null) {
            return new Predicate[]{
                    cb.between(from.get(Transaccion_.tiempoActualizacion), example.getTiempoActualizacion(), example2.getTiempoActualizacion()),
                    qbe};
        }

        return new Predicate[] {qbe};
    }

    @Override
    public Transaccion find(final Long id) {
        LOGGER.debug("Buscando {} con id {}", Transaccion.class, id);
        return doWithTransaction((entityManager, t) -> entityManager.find(Transaccion.class, id));
    }

}