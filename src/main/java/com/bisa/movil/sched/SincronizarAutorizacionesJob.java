package com.bisa.movil.sched;

import bus.env.api.MedioAmbiente;
import bus.mail.api.MailerFactory;
import com.bisa.indicadoresbcb.sched.VerificadorTasaTREJob;
import com.bisa.monitoreo.dao.JobsDao;
import com.bisa.movil.dao.EBMovilDao;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SincronizarAutorizacionesJob  implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(VerificadorTasaTREJob.class);
    private final MedioAmbiente medioAmbiente;
    private final MailerFactory mailerFactory;
    private final EBMovilDao eBMovilDao;

    @Inject
    public SincronizarAutorizacionesJob(MedioAmbiente medioAmbiente, MailerFactory mailerFactory,EBMovilDao eBMovilDao) {
        this.medioAmbiente = medioAmbiente;
        this.mailerFactory = mailerFactory;
        this.eBMovilDao = eBMovilDao;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.debug("Iniciando tarea");
        eBMovilDao.sincronizarAuthorizationsConTransacciones();

        LOGGER.debug("Tarea finalizada tarea");
    }


}
