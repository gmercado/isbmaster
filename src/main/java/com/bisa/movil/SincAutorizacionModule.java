package com.bisa.movil;

import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.indicadoresbcb.dao.CotizacionBolsinDao;
import com.bisa.monitoreo.sched.JobsMonitorJob;
import com.bisa.movil.sched.SincronizarAutorizacionesJob;

public class SincAutorizacionModule extends AbstractModuleBisa {
    @Override
    protected void configure() {
        bind(CotizacionBolsinDao.class);
/*
Seconds         0
Minutes         10
Hours           0/1
Day of month    1/1
Month           *
Day of week     *
Year            *
 	0 0 0/1 1/1 * ? *
         */
        bindSched("SincronizarAutorizacionesJob", "JOB", "10 * * * * ?", SincronizarAutorizacionesJob.class);

    }
}
