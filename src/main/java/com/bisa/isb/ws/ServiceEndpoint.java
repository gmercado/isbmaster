package com.bisa.isb.ws;

import com.bisa.isb.ws.support.Annotations;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;

/**
 * @author Miguel Vega
 * @version $Id: ServiceEndpoint.java 0, 2015-09-29 12:26 AM mvega $
 */
public abstract class ServiceEndpoint extends CXFNonSpringServlet {
    /**
     * Use the annotation in the class to determine the endpoint
     *
     * @return path
     */
    public String getPublishPath() {
        return Annotations.get(this.getClass(), Publish.class).value();
    }
}
