package com.bisa.isb.ws.rest;

import com.google.inject.BindingAnnotation;

import javax.interceptor.InterceptorBinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Useful to annotate anything related to a REST WS
 * @author Miguel Vega
 * @version $Id: Testeable.java 0, 2015-11-25 11:33 PM mvega $
 */
@InterceptorBinding
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
@Retention(value = RetentionPolicy.RUNTIME)
@BindingAnnotation
public @interface REST {
}
