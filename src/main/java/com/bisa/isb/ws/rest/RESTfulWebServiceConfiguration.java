package com.bisa.isb.ws.rest;

import com.bisa.isb.catalog.Catalog;
import com.bisa.isb.ws.Publish;
import com.bisa.isb.ws.ServiceEndpoint;
import com.bisa.isb.ws.catalog.Service;
import com.bisa.isb.ws.catalog.Services;
import com.bisa.isb.ws.security.AuthSecurityService;
import com.bisa.isb.ws.support.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.google.common.base.Optional;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.apache.cxf.jaxrs.provider.JAXBElementProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.ws.rs.Path;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.apache.cxf.common.util.SystemPropertyAction.getPropertyOrNull;


/**
 * @author Miguel Vega
 * @version $Id: RESTfulServiceConfiguration.java; sep 21, 2015 09:33 AM mvega $
 * @see 'http://cxf.apache.org/docs/jaxrs-services-configuration.html'
 */
@Publish("/rest/*")
@Singleton
public class RESTfulWebServiceConfiguration extends ServiceEndpoint {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    private AuthSecurityService authSecurityService;

    @Inject
    private Catalog<Service> serviceCatalog;

    @Inject
    private JAXRSServerFactoryBean factory;

    @REST
    @Inject
    private Map<Class, Object> wsInstances;

    static {
        //force not to use SpringAopClassHelper
        System.setProperty("org.apache.cxf.useSpringClassHelpers", "false");
        //String propertyOrNull = getPropertyOrNull("org.apache.cxf.useSpringClassHelpers");
        //System.out.println("..." + propertyOrNull);
    }

    @Override
    public void init(ServletConfig sc) throws ServletException {
        super.init(sc);
        logger.debug("RESTfulService org.apache.cxf.common.util.ClassHelper Spring dependency allowed?: {}",
                getPropertyOrNull("org.apache.cxf.useSpringClassHelpers"));
    }

    @Override
    protected void loadBus(ServletConfig sc) {
        super.loadBus(sc);

        final Bus cxfServiceBus = super.getBus();
        factory.setBus(cxfServiceBus);

        Set<Class<?>> loadedRestServices = Sets.newConcurrentHashSet();

        for (final Class<?> restService : wsInstances.keySet()) {
            try {

                final Object wsInstance = wsInstances.get(restService);

                Path path = Optional.fromNullable(Annotations.get(restService, Path.class)).or(
                        Annotations.get(wsInstance.getClass(), Path.class)
                );

                Service service = Services.discoverService(this, restService);
                serviceCatalog.add(service);
                //Class<?> finalServiceType = Optional.<Class<?>>fromNullable(serviceEndpoint).or(restService);
                //factory.setResourceClasses(restService);
                factory.setResourceProvider(new SingletonResourceProvider(wsInstance));
                //factory.setAddress(path.value());
                logger.debug(" * * * * * * * * * * * * RESTful Service {} has been created properly. This will be served with the following URI: {}",
                        service.getUniqueName(), service.getName().getPrefix());

                loadedRestServices.add(Optional.<Class<?>>fromNullable(wsInstance.getClass()).or(restService));

            } catch (org.apache.cxf.service.factory.ServiceConstructionException x) {
                logger.error("X( X( X( X( X( X( Unable to publish RESTful service for endpoint: " + restService.getName(), x);
            }
        }

        List providers = factory.getProviders();
        logger.info("Number of default providers found...................................................................." + providers.size());
        providers.forEach(o -> logger.info("An existing provider has been found...{}", o.getClass()));

        //for now only two publishers
        providers.add(new JAXBElementProvider());
        //providers.add(new org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider());
        providers.add(new JacksonJaxbJsonProvider());

        factory.create();

        //persist service catalog
        authSecurityService.populateCatalog(serviceCatalog, Service.Type.REST);

    }
}