package com.bisa.isb.ws.rest;

import com.bisa.isb.catalog.Catalog;
import com.bisa.isb.ws.catalog.Service;
import com.bisa.isb.ws.security.AuthSecurityService;
import com.bisa.isb.ws.security.dao.WSPayloadDao;
import com.bisa.isb.ws.security.entities.AuthUser;
import com.bisa.isb.ws.support.AuthorizationException;
import com.bisa.isb.ws.support.HttpBasicAuthInterceptor;
import com.google.inject.Inject;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;

/**
 * @author Miguel Vega
 * @version $Id: BasicAuthenticationInterceptor.java; sep 29, 2015 11:15 AM mvega $
 */
public class HttpBasicAuthInterceptor4REST extends HttpBasicAuthInterceptor {

    @Inject
    public HttpBasicAuthInterceptor4REST(AuthSecurityService authSecurityService, Catalog<Service> serviceCatalog,
                                         WSPayloadDao wsPayloadDao) {
        super(authSecurityService, serviceCatalog, wsPayloadDao);
    }

    /**
     * Overrides the default FAULT exception handling and returns a simply HTTP comprehensive response.
     * @param authUser
     * @param message
     * @param httpServletResponse
     * @param x
     */
    @Override
    public void resourceUnauthorized(AuthUser authUser, Message message, HttpServletResponse httpServletResponse,
                                     AuthorizationException x) throws Fault {

        final int scForbidden = SC_FORBIDDEN;

        //super.resourceUnauthorized(authUser, message, x);
        completeCXFInterceptorChainOnError(message, scForbidden);

        httpServletResponse.setStatus(scForbidden);
        try {
            httpServletResponse.sendError(scForbidden);

            Fault fault = new Fault("incorrect username or password", Logger.getGlobal());
            fault.setStatusCode(scForbidden);
            throw fault;

        } catch (IOException e) {
            throw new IllegalStateException("Unexpected error has occurred while sending back error to client");
        }
    }
}