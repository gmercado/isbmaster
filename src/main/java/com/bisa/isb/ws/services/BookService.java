package com.bisa.isb.ws.services;

import bus.database.model.BasePrincipal;
import com.bisa.isb.ws.catalog.Remark;
import com.bisa.isb.ws.services.dao.BookShelfDAO;
import com.bisa.isb.ws.services.model.Book;
import com.google.inject.Inject;

import javax.sql.DataSource;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import java.sql.SQLException;

/*
 * TODO: Book Service class - Get, update, add and delete a specific book
 */
@Remark("Una simple descripcion que se guardara en base de datos")
@Path("/bookshelf/books/{id}")
@Produces({"application/xml", "application/json"})
@Consumes({"application/xml", "application/json"})
public class BookService {

    private BookShelfDAO bookShelfDAO;

    public BookService(BookShelfDAO bookShelfDAO) {
        this.bookShelfDAO = bookShelfDAO;
    }
    public BookService() {
        this.bookShelfDAO = new BookShelfDAO();
    }

    @Inject
    @BasePrincipal
    DataSource dataSource;

    public BookShelfDAO getBookShelfDAO() {
        return bookShelfDAO;
    }

    @GET
    public Book getBook(@PathParam("id") String bookId) {
        System.out.println("Get Book with Id: " + bookId);
        Book book = getBookShelfDAO().getBook(bookId);
        if (book == null) {
            ResponseBuilder builder = Response.status(Status.BAD_REQUEST);
            builder.entity("Book Not Found");
            throw new WebApplicationException(builder.build());
        }

        try {
            book.setAuthor(dataSource.getConnection().getClientInfo().toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return book;
    }

    @PUT
    public Response updateBook(@PathParam("id") String bookId, Book bookToUpdateWith) {
        System.out.println("Update Book with Id : " + bookId);
        Book book = getBookShelfDAO().getBook(bookId);
        if (book == null) {
            return Response.status(Status.BAD_REQUEST).build();
        }
        getBookShelfDAO().updateBook(bookToUpdateWith);
        return Response.ok().build();
    }

    //POST - unused, doesn't make sense to add book with a specific id, so this method is present on /books

    @DELETE
    public Response deleteBook(@PathParam("id") String bookId) {
        System.out.println("Delete Book with Id : " + bookId);
        Book book = getBookShelfDAO().getBook(bookId);
        if (book == null) {
            return Response.status(Status.BAD_REQUEST).build();
        }
        getBookShelfDAO().deleteBook(bookId);
        return Response.ok().build();
    }
}
