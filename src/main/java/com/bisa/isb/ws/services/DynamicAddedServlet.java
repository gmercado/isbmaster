package com.bisa.isb.ws.services;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Miguel Vega
 * @version $Id: DynamicAddedServlet.java; mar 17, 2015 03:01 AM mvega $
 */
public class DynamicAddedServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext sc = getServletContext();

        if(checkAuth(req, resp)){
            return;
        }

        PrintWriter out = resp.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title> Servlet Added </title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h2 style=\"margin: 50px;\">The Servlet Version is 3.0 or later so<br />"
                + "&nbsp;the Servlet is added.<br /><br />"
                + "&nbsp;&nbsp;Servlet Version: "+sc.getMajorVersion()+"."+sc.getMinorVersion()+"</h2>");
        out.println("</body>");
        out.println("</html>");
        out.close();
    }


    private boolean checkAuth(HttpServletRequest request, HttpServletResponse response){

/*
        String authHeader = request.getHeader("Authorization");
        if (authHeader != null) {
            StringTokenizer st = new StringTokenizer(authHeader);
            if (st.hasMoreTokens()) {
                String basic = st.nextToken();

                if (basic.equalsIgnoreCase("Basic")) {
                    try {
                        String credentials = new String(Base64.decodeBase64(st.nextToken()), "UTF-8");
//                        LOG.debug("Credentials: " + credentials);
                        int p = credentials.indexOf(":");
                        if (p != -1) {
                            String _username = credentials.substring(0, p).trim();
                            String _password = credentials.substring(p + 1).trim();

                            if (!username.equals(_username) || !password.equals(_password)) {
                                unauthorized(response, "Bad credentials");
                            }

                            filterChain.doFilter(servletRequest, servletResponse);
                        } else {
                            unauthorized(response, "Invalid authentication token");
                        }
                    } catch (UnsupportedEncodingException e) {
                        throw new Error("Couldn't retrieve authentication", e);
                    }
                }
            }
        } else {
            unauthorized(response);
        }*/
        return false;
    }
}
