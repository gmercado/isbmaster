package com.bisa.isb.ws.services;

/**
 * @author Miguel Vega
 * @version $Id: HelloServlet.java; sep 15, 2015 03:22 PM mvega $
 */

import com.bisa.isb.ws.support.HttpAuthentication;
import com.bisa.isb.ws.support.UnsupportedFeatureException;
import org.apache.commons.codec.binary.Base64;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet(
        value = "/hello",
        initParams = {
                @WebInitParam(name = "foo", value = "Hello "),
                @WebInitParam(name = "bar", value = " World!")
        })
public class HelloServlet extends HttpServlet {

    Map validUsers = new HashMap();

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        // ie this user has no password
        validUsers.put("james:", "authorized");

        validUsers.put("jswan:mypassword", "authorized");
    }

    // This method checks the user information sent in the Authorization
    // header against the database of users maintained in the users Hashtable.
    protected boolean allowUser(String auth) throws IOException {

        if (auth == null) {
            return false;  // no auth
        }
        if (!auth.toUpperCase().startsWith("BASIC ")) {
            return false;  // we only do BASIC
        }
        // Get encoded user and password, comes after "BASIC "
        String userpassEncoded = auth.substring(6);
        // Decode it, using any base 64 decoder
        String userpassDecoded = new String(Base64.decodeBase64(userpassEncoded));

        // Check our user list to see if that user and password are "allowed"
        return "authorized".equals(validUsers.get(userpassDecoded));
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res)
            throws IOException, ServletException {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        // Get Authorization header
        String auth = req.getHeader("Authorization");

        //new implementation
        try {
            HttpAuthentication authentication = HttpAuthentication.forAuthorizationHeader(auth);

            //until here the authentication has been accepted
            // Allowed, so show him the secret stuff
            out.println("Top-secret stuff");
        } catch (UnsupportedFeatureException e) {
            e.printStackTrace();
            // Not allowed, so report he's unauthorized
            res.setHeader("WWW-Authenticate", "BASIC realm=\"jswan test\"");
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            // Could offer to add him to the allowed user list
        }
    }
}