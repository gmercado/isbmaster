package com.bisa.isb.ws.services;

/**
 * @author Miguel Vega
 * @version $Id: SomeException.java 0, 2015-09-15 01:02 AM mvega $
 */
import javax.xml.ws.WebFault;

@WebFault(name="exception")
public class SomeException extends Exception {

    public String contactInfo = "Sacrosanct Blood.";
    private static final long serialVersionUID = 1L;

    private SomeException() {
    }

    public SomeException(String message, Throwable cause) {
        super(message, cause);
    }

    public SomeException(String message) {
        super(message);
    }
}