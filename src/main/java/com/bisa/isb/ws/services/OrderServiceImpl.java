package com.bisa.isb.ws.services;

import com.bisa.isb.ws.services.model.Order;
import com.bisa.isb.ws.services.model.OrderList;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: SimpleServiceImpl.java; sep 22, 2015 12:48 PM mvega $
 */

public class OrderServiceImpl implements OrderService {

    List<Order> list = new ArrayList();

    public OrderServiceImpl() {
        Order order = new Order();
        order.setItemName("Veggie Pizza");
        order.setQuantity(9);
        order.setCustomerName("OptumInsight");
        list.add(order);

        order = new Order();
        order.setItemName("Green Salad");
        order.setQuantity(2);
        order.setCustomerName("OptumInsight");
        list.add(order);
    }

    @Override
    public Order getOrderXml(int id) {
        return getOrder(id);
    }

    @Override
    public Order getOrderJson(String strId) {
        int id = Integer.valueOf(strId);
        return getOrder(id);
    }

    @Override
    public OrderList getAllOrders() {
        OrderList fullList = new OrderList();
        for(Order order : list) {
            fullList.getOrders().add(order);
        }
        return fullList;
    }

    // Common method returning an Order POJO
    public Order getOrder(int id) {
        if ((id > 0) && (id <= list.size())) {
            return list.get(id - 1);
        }
        else
            return null;
    }
}