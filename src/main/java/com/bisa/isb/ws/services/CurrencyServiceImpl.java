package com.bisa.isb.ws.services;

import com.bisa.isb.ws.security.AuthSecurityService;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: CurrencyServiceImpl.java; sep 14, 2015 11:01 AM mvega $
 */
@WebService(targetNamespace = CurrencyService.NAMESPACE)
public class CurrencyServiceImpl implements CurrencyService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

//    @Resource
//    private WebServiceContext context;
//
//    @Inject
//    private AuthSecurityService authWebService;

    static {
        System.setProperty("http.proxyHost", "127.0.0.1");
        System.setProperty("http.proxyPort", "3242");
        System.setProperty("https.proxyHost", "127.0.0.1");
        System.setProperty("https.proxyPort", "3242");
    }

    @Override
    public BigDecimal convert(BigDecimal value, String source, String target) {

        return MonetaryUnit.valueOf(source).convert(MonetaryUnit.valueOf(target), value);

        /*
        ExchangeRateProvider xchange = MonetaryConversions.getExchangeRateProvider();
        ExchangeRate exchangeRate = xchange.getExchangeRate(source, target);
        NumberValue factor = exchangeRate.getFactor();

        CurrencyUnit scurr = exchangeRate.getBaseCurrency();
        CurrencyUnit sdest = exchangeRate.getCurrency();

        CurrencyConversion conversion = MonetaryConversions.getConversion(sdest);

        MonetaryAmount sourceAmount = Money.of(value, source);

        MonetaryAmount inDest = sourceAmount.with(conversion);

        return new BigDecimal(inDest.getNumber().doubleValueExact());
        */
    }

    @Override
    public List<MonetaryUnit> getCurrencies() {
        return ImmutableList.of(MonetaryUnit.BOB, MonetaryUnit.EUR, MonetaryUnit.USD);
    }
}
