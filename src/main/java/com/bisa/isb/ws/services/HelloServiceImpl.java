package com.bisa.isb.ws.services;

import com.bisa.isb.api.Texts;

import javax.jws.WebService;

/**
 * @author Miguel Vega
 * @version $Id: HelloServiceImpl.java; sep 15, 2015 03:27 PM mvega $
 */
@WebService(endpointInterface= "com.bisa.isb.ws.services.HelloService", serviceName = "hello")
public class HelloServiceImpl implements HelloService{

    @Override
    public String sayHello(String name) {
        return Texts.build("Hello ", name, "!!! :)");
    }

    @Override
    public String sayGoodbye(String name) {
        return Texts.build("Bye ", name, "!!! :(");
    }
}