package com.bisa.isb.ws.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: CurrencyService.java; sep 14, 2015 10:50 AM mvega $
 */
@WebService(
        targetNamespace = CurrencyService.NAMESPACE,
        name = "CurrencyService"
)
public interface CurrencyService {

    String NAMESPACE = "http://www.bisa.com/ebanking/services/soap";
    int SCALE = 10;

    @WebMethod(action = "convertAction", operationName = "convertOperation", exclude = false)
    @WebResult(name = "conversionResult", targetNamespace = NAMESPACE)
    BigDecimal convert(@WebParam(name = "amount") BigDecimal value, @WebParam(name = "sourceCurrency") String source,
                       @WebParam(name = "destCurrency") String target);

    @WebMethod(operationName = "getCurrencies")
    @WebResult(name = "currencies")
    List<MonetaryUnit> getCurrencies();

    enum MonetaryUnit {
        USD(BigDecimal.valueOf(1).divide(BigDecimal.valueOf(6.96), SCALE, RoundingMode.HALF_UP)) {
        }, EUR(BigDecimal.valueOf(1).divide(BigDecimal.valueOf(9.10), SCALE, RoundingMode.HALF_UP)) {
        }, BOB(BigDecimal.ONE) {
        };

        final BigDecimal baseAmount;

        MonetaryUnit(BigDecimal amount) {
            this.baseAmount = amount;
        }

        BigDecimal convert(MonetaryUnit dest, BigDecimal value) {
            return getFactor(dest).multiply(value);
        }

        public BigDecimal getFactor(MonetaryUnit dest) {
            return dest.baseAmount.divide(baseAmount, 2, RoundingMode.FLOOR);
        }
    }
}
