package com.bisa.isb.ws.services.model;

/**
 * @author Miguel Vega
 * @version $Id: OrderList.java; sep 22, 2015 12:47 PM mvega $
 * @see http://stackoverflow.com/questions/4144296/marshalling-a-list-of-objects-with-jaxb
 */

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "OrderList")
@XmlSeeAlso({Order.class})
public class OrderList {

    List<Order> orders;

    /*@XmlElementWrapper(name="orders")
    @XmlElement(name = "order")*/
    @XmlElementRef()
    public List<Order> getOrders() {
        if (orders == null) {
            orders = new ArrayList();
        }
        return this.orders;
    }
}