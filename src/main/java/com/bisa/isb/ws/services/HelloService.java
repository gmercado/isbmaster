package com.bisa.isb.ws.services;

/**
 * @author Miguel Vega
 * @version $Id: HelloService.java; sep 15, 2015 03:21 PM mvega $
 */

import com.bisa.isb.ws.catalog.Remark;

import javax.jws.WebMethod;
import javax.jws.WebService;

//@WebService(targetNamespace = "http://hello/within")
@WebService
@Remark("A simple service to handle greetings")
public interface HelloService {
    @WebMethod
    @Remark("Hello method to salutate thee.")
    String sayHello(String name);

    @WebMethod
    @Remark("Goodbye....!!!")
    String sayGoodbye(String name);
}