package com.bisa.isb.ws.services;

/**
 * @author Miguel Vega
 * @version $Id: SimpleService.java; sep 22, 2015 12:48 PM mvega $
 */

import com.bisa.isb.ws.services.model.Order;
import com.bisa.isb.ws.services.model.OrderList;

import javax.ws.rs.*;

@Path("/order/")
public interface OrderService {
    @GET
    @Produces("application/xml")
    @Path("{orderId}")
    Order getOrderXml(@PathParam ("orderId") int id);

    @GET
    @Produces("application/json")
    @Path("/")
    Order getOrderJson(@QueryParam("id") @DefaultValue("-1") String strId);

    @GET
    @Produces("application/xml")
    @Path("all")
    OrderList getAllOrders();
}