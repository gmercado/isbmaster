package com.bisa.isb.ws.services.model;

/**
 * @author Miguel Vega
 * @version $Id: oRDER.java; sep 22, 2015 12:47 PM mvega $
 */
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "order")
public class Order {

    private String itemName;
    private int quantity;
    private String customerName;

    @XmlElement
    public String getItemName() { return itemName; }

    public void setItemName(String itemName) { this.itemName = itemName; }

    @XmlElement
    public int getQuantity() { return quantity; }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @XmlElement
    public String getCustomerName() { return customerName; }

    public void setCustomerName(String customerName) { this.customerName = customerName; }
}