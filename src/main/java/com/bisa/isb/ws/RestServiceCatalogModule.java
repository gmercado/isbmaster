package com.bisa.isb.ws;

import com.bisa.isb.ws.rest.REST;
import com.bisa.isb.ws.support.Annotations;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import java.util.Map;
import java.util.Set;

import static com.bisa.isb.ws.support.Annotations.get;

/**
 * @author Miguel Vega
 * @version $Id: RestServiceCatalogModule.java; jul 15, 2016 10:33 AM mvega $
 * @see <a href="http://stackoverflow.com/questions/347248/how-can-i-get-a-list-of-all-the-implementations-of-an-interface-programmatically">Get implementations</a>
 */
public class RestServiceCatalogModule extends AbstractModule {

    final Logger logger = LoggerFactory.getLogger(getClass());

    final ServletContext servletContext;

    public RestServiceCatalogModule(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    protected void configure() {
        //get all the services annotated with REST
        Reflections reflections = new Reflections();
        //@Path
        final Set<Class<?>> typesAnnotatedWithPath = reflections.getTypesAnnotatedWith(Path.class);

        final Set<Class> visitedTypes = Sets.newHashSet();

        //the objects within the following colelctions are used to allow injection throwugh CXF
        final Map<Class, Object> wsInstances = Maps.newHashMap();

        typesAnnotatedWithPath.forEach(pathType -> {
            Path path = get(pathType, Path.class);
            if (visitedTypes.contains(pathType))
                return;

            //what happens if the service is a REST one
            Class implType = null;

            if (pathType.isInterface()) {
                //get the implementation
                //TODO, can one REST interface have more than one implementation, we'll assume that's true
                final Set<Class<?>> implementations = (Set<Class<?>>) reflections.getSubTypesOf(pathType);
                if (implementations.isEmpty()) {
                    logger.warn("An interface has been declared [{}], but no implementation ha sbeen found. This REST type will be skipped",
                            pathType.getName());
                }
                implType = implementations.iterator().next();
                bind(pathType).to(implType);
            } else {
                bind(pathType);
                implType = pathType;
            }

            path = Annotations.get(pathType, Path.class);

            //work with implType object instead
            visitedTypes.add(implType);

            try {
                Object instance = implType.newInstance();
                requestInjection(instance);

                wsInstances.put(implType, instance);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });

        bind(new TypeLiteral<Map<Class, Object>>() {
        }).annotatedWith(REST.class).
                toProvider(() -> wsInstances).asEagerSingleton();
    }
}

