package com.bisa.isb.ws;

import javax.inject.Qualifier;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * URL pattern for publishing
 *
 * <p>Example usage:
 *
 * <pre>
 *   @Publishing("/urlPattern")
 *   public class AnyServlet {
 *     ...
 *   }</pre>

 * @author Miguel Vega
 * @version $Id: ServicePath.java; sep 21, 2015 02:03 PM mvega $
 */
@Qualifier
@Documented
@Retention(RUNTIME)
public @interface Publish {

    /** The name. */
    String value();
}

