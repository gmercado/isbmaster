package com.bisa.isb.ws;

import com.bisa.isb.catalog.Catalog;
import com.bisa.isb.ws.catalog.Service;
import com.bisa.isb.ws.catalog.ServiceCatalog;
import com.bisa.isb.ws.soap.SOAP;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jws.WebService;
import javax.servlet.ServletContext;
import java.util.Map;
import java.util.Set;

import static com.bisa.isb.ws.support.Annotations.get;

/**
 * @author Miguel Vega
 * @version $Id: ServiceCatalogModule.java 0, 2015-11-25 11:31 PM mvega $
 */
public class SOAPServiceCatalogModule extends AbstractModule {

    final Logger logger = LoggerFactory.getLogger(getClass());

    final ServletContext servletContext;

    public SOAPServiceCatalogModule(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    protected void configure() {

        //prepare all services
        //load all SOAP services
        final Set<Class> visitedTypes = Sets.newHashSet();
        //configure all classpath
        Reflections reflections = new Reflections("");
        Set<Class<?>> webservices = reflections.getTypesAnnotatedWith(WebService.class);

        //the objects within the following colelctions are used to allow injection throwugh CXF
        final Map<Class, Object> availableInstances = Maps.newHashMap();

        webservices.forEach(qualifiedType -> {
            WebService annotation = get(qualifiedType, WebService.class);
            if (visitedTypes.contains(qualifiedType) || qualifiedType.isInterface())
                return;

            //if not present yet, need to retrieve the
            logger.debug("About to check annotation while loading web services: " + annotation);

//            Service service = Services.discoverService(serviceEndpoint, qualifiedType);//getServiceFromJavaType(qualifiedType);

            visitedTypes.add(qualifiedType);

            bind(qualifiedType);

            try {
                Object instance = qualifiedType.newInstance();
                requestInjection(instance);

                availableInstances.put(qualifiedType, instance);
            } catch (InstantiationException e) {
                throw new IllegalStateException(e);
            } catch (IllegalAccessException e) {
                throw new IllegalStateException(e);
            }
        });

        bind(new TypeLiteral<Map<Class, Object>>() {
        }).annotatedWith(SOAP.class).
                toProvider(() -> availableInstances).asEagerSingleton();


        //todo, it's possible that an injection using names will be necessary for Catalog
        bind(new TypeLiteral<Catalog<Service>>() {
        }).toInstance(new ServiceCatalog());
    }

}
