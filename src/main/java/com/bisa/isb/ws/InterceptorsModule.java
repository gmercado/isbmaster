package com.bisa.isb.ws;

import com.bisa.isb.ws.rest.HttpBasicAuthInterceptor4REST;
import com.bisa.isb.ws.soap.WSDLAuthenticationInterceptor;
import com.bisa.isb.ws.support.HttpBasicAuthInterceptor;
import com.bisa.isb.ws.support.RequestPayloadInterceptor;
import com.bisa.isb.ws.support.ResponsePayloadInterceptor;
import com.bisa.isb.ws.support.ResponseRestServiceInterceptor;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Scopes;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;

/**
 * @author Miguel Vega
 * @version $Id: InterceptorModule.java; oct 06, 2015 03:44 PM mvega $
 */
public class InterceptorsModule extends AbstractModule{

    @Override
    protected void configure() {
        //auth interceptors
        bind(WSDLAuthenticationInterceptor.class);
        bind(HttpBasicAuthInterceptor.class);
        bind(HttpBasicAuthInterceptor4REST.class);

        //exchange interceptors
        bind(RequestPayloadInterceptor.class);
        bind(ResponsePayloadInterceptor.class);

        //prepare both, REST and SOAP factory Beans
        bind(JaxWsServerFactoryBean.class).toProvider(new SOAPFactoryBeanProvider()).in(Scopes.SINGLETON);
        bind(JAXRSServerFactoryBean.class).toProvider(new RESTFactoryBeanProvider()).in(Scopes.SINGLETON);

    }

    private class RESTFactoryBeanProvider implements Provider<JAXRSServerFactoryBean>{

        @Inject
        private HttpBasicAuthInterceptor4REST authIntecept;

        @Inject
        private RequestPayloadInterceptor requestPayloadInterceptor;

        @Inject
        private ResponsePayloadInterceptor responsePayloadInterceptor;

        @Inject
        private ResponseRestServiceInterceptor responseRestServiceInterceptor;

        @Override
        public JAXRSServerFactoryBean get() {
            JAXRSServerFactoryBean factory = new JAXRSServerFactoryBean();

            factory.getInInterceptors().add(authIntecept);

            factory.getInInterceptors().add(requestPayloadInterceptor);
            factory.getInFaultInterceptors().add(requestPayloadInterceptor);

            factory.getOutInterceptors().add(responsePayloadInterceptor);
            factory.getOutInterceptors().add(responseRestServiceInterceptor);
            factory.getOutFaultInterceptors().add(responsePayloadInterceptor);
            factory.getOutFaultInterceptors().add(responseRestServiceInterceptor);

            return factory;
        }
    }

    private class SOAPFactoryBeanProvider implements Provider<JaxWsServerFactoryBean>{

        @Inject
        private WSDLAuthenticationInterceptor wsdlAuthenticationInterceptor;

        @Inject
        private HttpBasicAuthInterceptor httpBasicAuthInterceptor;

        @Inject
        private RequestPayloadInterceptor requestPayloadInterceptor;

        @Inject
        private ResponsePayloadInterceptor responsePayloadInterceptor;

        @Override
        public JaxWsServerFactoryBean get() {
            JaxWsServerFactoryBean factory; //= new ServerFactoryBean();
            //javax.xml.ws.WebServiceException:
            // Attributes portName, serviceName and endpointInterface are not allowed in the @WebService annotation of an SEI.
            factory = new JaxWsServerFactoryBean();

            factory.getInInterceptors().add(wsdlAuthenticationInterceptor);
            factory.getInInterceptors().add(httpBasicAuthInterceptor);

            factory.getInInterceptors().add(requestPayloadInterceptor);
            factory.getInFaultInterceptors().add(requestPayloadInterceptor);

            factory.getOutInterceptors().add(responsePayloadInterceptor);
            factory.getOutFaultInterceptors().add(responsePayloadInterceptor);
            return factory;
        }
    }
}
