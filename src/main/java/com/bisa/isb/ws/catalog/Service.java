package com.bisa.isb.ws.catalog;

import javax.xml.namespace.QName;
import java.io.Serializable;

/**
 * @author Miguel Vega
 * @version $Id: Service.java; sep 28, 2015 02:26 PM mvega $
 */
public class Service implements Serializable{
    protected final Class<?> endpoint;

    protected final QName name;

    protected final Type type;

    protected String remark;

    Service(QName name, Class<?> endpoint, Type type) {
        this.endpoint = endpoint;
        this.name = name;
        this.type = type;
    }

    public static Service of(QName qName, Class<?> endpoint, Type type){
        return new Service(qName, endpoint, type);
    }

    public Class<?> getEndpoint() {
        return endpoint;
    }

    /**
     * This name is the uniqe name in service catalog. Combination of namespaceURI & localPart make the key.
     * {@link QName#getLocalPart()}}, the name of the service in Catalog
     * {@link QName#getNamespaceURI()}, the namespace where service belongs to
     * {@link QName#getPrefix()}, the service context URI
     * @return
     */
    public QName getName() {
        return name;
    }

    public Type getType(){
        return type;
    }

    public final String getUniqueName(){
        String nsURI = name.getNamespaceURI();
        return nsURI.concat(nsURI.endsWith("/")?"":"/").concat(name.getLocalPart());
    }

    public enum Type{
        SOAP, REST
    }

    public String getRemark() {
        return remark;
    }

    public Service setRemark(String remark) {
        this.remark = remark;
        return this;
    }
}
