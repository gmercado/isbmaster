package com.bisa.isb.ws.catalog;

import com.bisa.isb.api.Triplet;
import com.bisa.isb.ws.ServiceEndpoint;
import com.bisa.isb.ws.support.Annotations;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.*;
import javax.xml.namespace.QName;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import static com.bisa.isb.ws.support.Annotations.*;
import static com.google.common.base.Strings.isNullOrEmpty;
import static java.util.Arrays.stream;
import static java.util.Optional.ofNullable;

/**
 * @author Miguel Vega
 * @version $Id: Services.java 0, 2015-09-28 11:03 PM mvega $
 */
public class Services {

    static final Logger logger = LoggerFactory.getLogger(Services.class);

    /**
     * Returns a QNAme with data only about the Service Name and the Service NameSpace URI. Service context URI is not
     * present in prefix value.
     *
     * @param endpointType
     * @return a triplet with data about service: {NAME, TYPE, DESCRIPTION}
     */
    public static Triplet<QName, Service.Type, String> getServiceWeakReference(Class<?> endpointType) {

        WebService annotation = get(endpointType, WebService.class);
        //This will be invoked only when Service implementing class does not declare the serviceName value

        String wsNS = annotation.targetNamespace();
        String wsName = annotation.name();
        String restURI = "";
        String description = get(endpointType, Remark.class).value();

        if (!isNullOrEmpty(wsName) && !isNullOrEmpty(wsNS))
            return Triplet.of(new QName(wsNS, wsName), Service.Type.SOAP, description);

        //maybe a REST
        Path path = getNullable(endpointType, Path.class);
        if (path != null) {
            wsNS = getDefaultNamespaceURI(endpointType);
            wsName = endpointType.getSimpleName();
            return Triplet.of(new QName(wsNS, wsName, path.value()), Service.Type.REST, description);
        }

        // fetch interface service
        Class[] interfaces = endpointType.getInterfaces();

        Service.Type type = null;

        //iterate through all interfaces looking for WebService annotation
        for (Class wsInterface : interfaces) {

            WebService wsAnnotation = getNullable(wsInterface, WebService.class);

            if (wsAnnotation == null) {
                //maybe a REST
                path = getNullable(wsInterface, Path.class);
                if (path == null)
                    //interface found is not WebService neither Path annotated
                    continue;
                else {
                    //REST service endpoint found
                    wsNS = getDefaultNamespaceURI(endpointType);
                    wsName = wsInterface.getSimpleName();
                    restURI = path.value();
                    type = Service.Type.REST;
                    String tmpDesc = get(wsInterface, Remark.class).value();
                    description = !isNullOrEmpty(description) && tmpDesc==null?description:tmpDesc;
                    break;
                }
            }

            type = Service.Type.SOAP;
            wsName = isNullOrEmpty(wsAnnotation.name()) ? wsInterface.getSimpleName() : wsAnnotation.name();
//            description = get(endpointType, Remark.class).value();
            String tmpDesc = get(wsInterface, Remark.class).value();
            description = !isNullOrEmpty(description) && tmpDesc==null?description:tmpDesc;

            if (!isNullOrEmpty(wsNS))//if NamespaceURI has not been determined yet
                wsNS = isNullOrEmpty(wsAnnotation.targetNamespace()) ? getDefaultNamespaceURI(wsInterface) : wsAnnotation.targetNamespace();
            break;
        }

        wsNS = isNullOrEmpty(wsNS)?null:wsNS;
        wsName = isNullOrEmpty(wsName)?null:wsName;

        type = type!=null?type:Service.Type.SOAP;

        Triplet triplet = Triplet.of(new QName(ofNullable(wsNS).orElse(getDefaultNamespaceURI(endpointType)),
                ofNullable(wsName).orElse(endpointType.getSimpleName()), restURI), type, description);
        return triplet;
    }

    /**
     * Retrieves the a service implementation about the one configured in Java classes
     *
     * @param serviceEndpoint
     * @param endpointType
     * @return
     */
    public static Service discoverService(ServiceEndpoint serviceEndpoint, Class endpointType) {
//        QName qName = getSOAPServiceQName(endpointType);
        Triplet<QName, Service.Type, String> triplet = getServiceWeakReference(endpointType);

        final QName qName = triplet.getLeft();//getServiceWeakReference(endpointType);
        final Service.Type type = triplet.getMiddle();
        final String descr = triplet.getRight();

        final String URI = serviceEndpoint.getPublishPath().replace("/*", "/");

//        return Service.of(new QName(wsNS, wsName, URI.concat(wsName)), endpointType);
        return Service.of(
                new QName(
                        qName.getNamespaceURI(),
                        qName.getLocalPart(),
                        URI.concat(isNullOrEmpty(qName.getPrefix()) ? qName.getLocalPart() : qName.getPrefix()).replace("//", "/")
                ),
                endpointType, type).setRemark(descr);
    }

    /**
     * Creates simple Namespace URI for a Java type, using the package location
     *
     * @param type
     * @return
     */
    public static String getDefaultNamespaceURI(Class type) {
        String name = type.getPackage().getName();

        ArrayList<String> vals = Lists.newArrayList(Splitter.on(".").trimResults().split(name));
        Collections.reverse(vals);

        //vals.stream().map(Object::toString).collect(Collectors.joining("."));
        StringBuilder sb = new StringBuilder("http://");
        for (String val : vals) {
            sb.append(val).append(".");
        }
        sb.append("/");

        return sb.toString().replace("./", "/");
    }

    /**
     * Fetches all operations from a web service.
     * @param service
     * @return empty set if class in parameter is not a valid service.
     */
    public static Set<Operation> discoverServiceOperations2(Service service) {
        //Fetch the class which has the annotation which determines class as a service
        Class endpoint = service.getEndpoint();

        if(!isAnnotatedWith(endpoint, WebService.class)){
            //try search some classes annotated with @WebService
            endpoint = stream(endpoint.getInterfaces())
                    .filter(wsi -> isAnnotatedWith(wsi, WebService.class))
                    .findFirst().orElse(null);
        }else{
            //check if there is an interface annotated with WebService. Interfaces are mandatory
            endpoint = stream(endpoint.getInterfaces())
                    .filter(wsi -> isAnnotatedWith(wsi, WebService.class))
                    .findFirst().orElse(endpoint);
        }

        //Verify if class is either or not NULL
        if(endpoint==null){
            //It was not a SOAP web srevice, look up for REST
            endpoint = service.getEndpoint();
            if(!isAnnotatedWith(endpoint, Path.class)){
                endpoint = stream(endpoint.getInterfaces())
                        .filter(wsi -> isAnnotatedWith(wsi, Path.class))
                        .findFirst().orElse(null);

            }
        }

        if (endpoint==null)
            return Collections.emptySet();

        Method[] methods = endpoint.getDeclaredMethods();

        return stream(methods).filter(method -> {
            if(!isAnnotatedWith(method, WebMethod.class)){
                //
                if(isAnnotatedWith(method, Path.class) || isAnnotatedWith(method, GET.class) ||
                        isAnnotatedWith(method, POST.class) || isAnnotatedWith(method, PUT.class) ||
                        isAnnotatedWith(method, DELETE.class)){
                    return true;
                }
                return false;
            }
            return true;
        }).map(method -> {
            String opName;

            if (isAnnotatedWith(method, WebMethod.class)) {
                WebMethod webMethod = get(method, WebMethod.class);
                opName = isNullOrEmpty(webMethod.operationName()) ? method.getName() : webMethod.operationName();
            } else {
                Path path = get(method, Path.class);
                opName = isNullOrEmpty(path.value()) ? "/" : path.value();
            }

            String value = get(method, Remark.class).value();
            return new Operation(opName, method, value);
        }).collect(Collectors.toSet());
    }

    /**
     * Fetched all operation from a Java web service.
     * <p>If SOAP, the {@link WebMethod} is used: name is {@link WebMethod#operationName()} if not null, else java method name</p>
     * <p>If REST, the {@link Path} is used: name is {@link Path#value()}</p>
     *
     * @param service
     * @return
     * @deprecated replaced by version 2 of this method
     */
    @Deprecated
    public static Set<Operation> discoverServiceOperations(Service service) {

        Class<?> endpoint = service.getEndpoint();

        Method[] methods = endpoint.getDeclaredMethods();

        return stream(methods).
                filter(candidate -> {
                    //check SOAP first
                    Method method = deepSearchMethodAnnotatedWith(candidate, WebMethod.class);
                    if (method == null) {
                        //check REST
                        method = deepSearchMethodAnnotatedWith(candidate, Path.class);
                    }
                    return method != null;
                }).
                map(method -> {
                    //check SOAP first
                    Method opMethod = deepSearchMethodAnnotatedWith(method, WebMethod.class);
                    String opName;

                    if (opMethod == null) {
                        opMethod = deepSearchMethodAnnotatedWith(method, Path.class);
                        opName = get(opMethod, Path.class).value();
                    } else {
                        WebMethod annotation = get(opMethod, WebMethod.class);
                        opName = isNullOrEmpty(annotation.operationName()) ? method.getName() : annotation.operationName();
                    }

                    Remark remark = get(opMethod, Remark.class);
                    return new Operation(opName, method, remark.value());
                }).collect(Collectors.toSet());
    }

    /**
     * This will search method in declaring class and interfaces of declaring class if method does not exist in the
     * declaring class.
     * @param annotation
     * @param method
     * @param <T>
     * @return
     */
    private static <T extends Annotation> Method deepSearchMethodAnnotatedWith(Method method, Class<T> annotation) {
        T nullable = Annotations.getNullable(method, annotation);
        if (nullable != null)
            return method;

        Class<?>[] interfaces = method.getDeclaringClass().getInterfaces();
        try {
            return stream(interfaces).filter(inter -> {
                Method m = getSimilar(inter, method);
                if (m == null) return false;
                T nullable1 = getNullable(m, annotation);
                return nullable1 != null;
            }).map(clazz -> getSimilar(clazz, method)).findFirst().get();
        } catch (NoSuchElementException x) {
            //possibly there is a method in declaring class but not in imterface
            return null;
        }
    }

    /**
     * Look for method which look like the one given as parameter in class or interfaces
     * @param clazz
     * @param method
     * @return
     */
    private static Method getSimilar(Class clazz, Method method) {
        try {
            Method mm = clazz.getMethod(method.getName(), method.getParameterTypes());
            return mm;
        } catch (NoSuchMethodException e) {
            return null;
        }
    }
}
