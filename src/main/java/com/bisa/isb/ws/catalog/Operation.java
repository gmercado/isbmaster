package com.bisa.isb.ws.catalog;

import java.lang.reflect.Method;

/**
 * @author Miguel Vega
 * @version $Id: Operation.java; sep 30, 2015 07:04 PM mvega $
 */
public class Operation {
    private final String name;
    private final Method method;
    private final String remark;

    public Operation(String name, Method method, String remark){
        this.name = name;
        this.method = method;
        this.remark = remark;
    }

    public String getName() {
        return name;
    }

    public Method getMethod() {
        return method;
    }

    public String getRemark() {
        return remark;
    }
}
