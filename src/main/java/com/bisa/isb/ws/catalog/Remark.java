package com.bisa.isb.ws.catalog;

import java.lang.annotation.*;

/**
 * Annotation used to annotate the {@link com.bisa.isb.ws.security.entities.AuthWebService} and
 * {@link com.bisa.isb.ws.security.entities.AuthWebServiceOperation} objects when populating database
 * @author Miguel Vega
 * @version $Id: Description.java; sep 30, 2015 07:06 PM mvega $
 */

@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface Remark {
    String value();
}
