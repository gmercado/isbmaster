package com.bisa.isb.ws.catalog;

import com.bisa.isb.catalog.Catalog;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.inject.Singleton;

import javax.xml.namespace.QName;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This class will help to specify and organize all services published from this application.
 *
 * @author Miguel Vega
 * @version $Id: ServiceCatalog.java; sep 28, 2015 01:39 PM mvega $
 */
@Singleton
public class ServiceCatalog implements Catalog<Service> {

    final static BiMap<QName, Service> services = HashBiMap.create();

    @Override
    public void add(Service service) throws UnsupportedOperationException {
        services.put(service.getName(), service);
    }

    @Override
    public void remove(Service service) throws UnsupportedOperationException {
        QName s = services.inverse().get(service);
        services.remove(s);
    }

    @Override
    public void replace(QName id, Service service) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Method not supported");
    }

    @Override
    public List<Service> find(final QName id) {
        return services.entrySet().stream().
                filter(
                        entry -> entry.getKey().getNamespaceURI().equals(id.getNamespaceURI()) &&
                                entry.getKey().getLocalPart().equals(id.getLocalPart())
                ).
                map(Map.Entry::getValue).collect(Collectors.toList());
        //collect(Collectors.collectingAndThen(item -> item.getValue()));
    }

    @Override
    public Set<Service> fetchAll() {
        return services.values();
    }
}
