package com.bisa.isb.ws;

import com.bisa.isb.api.jpa.SafeEntityManagerFactory;
import com.bisa.isb.ws.aop.WSOperationInterceptor;
import com.bisa.isb.ws.rest.RESTfulWebServiceConfiguration;
import com.bisa.isb.ws.security.AuthSecurityService;
import com.bisa.isb.ws.security.AuthSecurityServiceImpl;
import com.bisa.isb.ws.soap.SOAPWebServiceConfiguration;
import com.google.inject.matcher.Matchers;
import com.google.inject.servlet.ServletModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jws.WebMethod;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.bisa.isb.ws.support.Annotations.get;

/**
 * @author Miguel Vega
 * @version $Id: WebServiceModule.java; sep 14, 2015 09:58 AM mvega $
 */
public class WebServiceModule extends ServletModule {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    protected void configureServlets() {

        System.setProperty("org.apache.cxf.useSpringClassHelpers", "false");

        //bind services
        //bind(AuthSecurityService.class).toProvider(() -> authSecurityService);

        bind(AuthSecurityService.class).to(AuthSecurityServiceImpl.class);

        //TODO, Not being used properly because Interceptors are not created on injection
        bind(SafeEntityManagerFactory.class);

        //configure others
        configureAOP();

        configureWebServices();
    }

    /**
     * Use CXF to configure services dinamically
     */
    private void configureWebServices() {
        // Dynamic add servlet for servlet 3.x or later

        install(new InterceptorsModule());

        install(new SOAPServiceCatalogModule(getServletContext()));
        install(new RestServiceCatalogModule(getServletContext()));

        Class<SOAPWebServiceConfiguration> soapWSEndpointType = SOAPWebServiceConfiguration.class;
        String path = get(soapWSEndpointType, Publish.class).value();
        serve(path).with(soapWSEndpointType);

        // RESTful services
        Class<RESTfulWebServiceConfiguration> restWSEndpointType = RESTfulWebServiceConfiguration.class;
        path = get(restWSEndpointType, Publish.class).value();
        serve(path).with(restWSEndpointType);
    }

    private void configureAOP() {
        WSOperationInterceptor wsoi = new WSOperationInterceptor();
        //
        requestInjection(wsoi);
        bindInterceptor(Matchers.any(), Matchers.annotatedWith(WebMethod.class), wsoi);
    }

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File("d:/InfoLaptop.txt"))));

        PrintWriter writer = new PrintWriter(new File("d:/InfoLaptop.txt.new"));

        String fileData = readLargeFile("d:/InfoLaptop.txt");

        //remove bold tags
        //Pattern patt = Pattern.compile("<b>([^<]*)</b>");
        Pattern patt = Pattern.compile("([^<]*): ");
        Matcher m = patt.matcher(fileData);
        StringBuffer sb = new StringBuffer(fileData.length());

        while (m.find()) {
            String text = m.group(1);
            System.out.println("..." + text);
            // ... possibly process 'text' ...
            m.appendReplacement(sb, Matcher.quoteReplacement(text));
        }
        m.appendTail(sb);
        //return sb.toString();
/*

        String line;
        while((line = reader.readLine())!=null){
            writer.println(line.replace(": ", ":\n\t"));
        }

        reader.close();
        writer.close();*/
    }

    /**
     * Read a large file in chunks with fixed size buffer
     *
     * @param fileName
     * @return
     * @throws IOException
     * @see http://howtodoinjava.com/2013/05/01/3-ways-to-read-files-using-java-nio/
     */
    static String readLargeFile(String fileName) throws IOException {
        RandomAccessFile aFile = new RandomAccessFile(fileName, "r");

        FileChannel inChannel = aFile.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        while (inChannel.read(buffer) > 0) {
            buffer.flip();
            for (int i = 0; i < buffer.limit(); i++) {
                System.out.print((char) buffer.get());
            }
            buffer.clear(); // do something with the data and clear/compact it.
        }
        inChannel.close();
        aFile.close();

        return buffer.toString();
    }
}
