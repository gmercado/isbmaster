## The following specifications must match the requirements for the new Web Service publishing engine

# Activities

1. Modulo de publicacion web services.
2. Crear dos servicios web para publicarlos de manera sencilla
3. El wsdl de estos debe poder generarse de manera controlada (No prioritario)
5. Crear un interceptor de consumo de servicios web.
   Los metodos de los web services pueden estar anotados con security si estos ser[an controlados por rol o usuario.
4. El consumo de los servicio web deben estar interceptados para evitar que un usuario no registrado consuma un servicio web.