package com.bisa.isb.ws.aop;

import com.google.inject.Inject;
import com.google.inject.Injector;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Apply security while intercepting web service operation methods.
 * @author Miguel Vega
 * @version $Id: WSOperationInterceptor.java; sep 14, 2015 10:22 AM mvega $
 */
public class WSOperationInterceptor implements MethodInterceptor{
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    private Injector injector;

    //todo, need the principalSecurity management.

    /**
     * All about the control must be defined here.
     * @param invocation
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        logger.info("*****************************************************************************************");
        logger.info("Methid is ...{}", invocation.getMethod().getName());
        logger.info("*****************************************************************************************");
        return invocation.proceed();
    }
}
