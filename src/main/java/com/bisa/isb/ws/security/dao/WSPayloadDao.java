package com.bisa.isb.ws.security.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.isb.ws.security.entities.AuthWebServiceOperation;
import com.bisa.isb.ws.security.entities.WSPayload;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.*;

/**
 * @author Miguel Vega
 * @version $Id: WSPayloadService.java 0, 2015-11-03 10:14 PM mvega $
 */
public class WSPayloadDao extends DaoImpl<WSPayload, Long> implements Serializable {

    public static final String TRANSPORTED_PAYLOAD = "cxf.intercepted.payload";
    public static final String TRANSPORTED_PAYLOAD_ID = "cxf.intercepted.payload.PK";
    private static final Logger LOGGER = LoggerFactory.getLogger(WSPayloadDao.class);

    @Inject
    public WSPayloadDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Long> countPath(Root<WSPayload> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<WSPayload> p) {
        Path<String> ipAddress = p.get("ipAddress");
        return Collections.singletonList(ipAddress);
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<WSPayload> from, WSPayload example, WSPayload example2) {

        List<Predicate> predicates = new LinkedList<>();

        Date fecha = example.getDateCreated();
        String ip = StringUtils.trimToNull(example.getIpAddress());
        AuthWebServiceOperation wsOperation = example.getWsOperation();
        if (fecha != null && example2.getDateCreated() != null) {
            Date inicio = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH);
            Date inicio2 = DateUtils.truncate(example2.getDateCreated(), Calendar.DAY_OF_MONTH);
            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
            LOGGER.info("   FECHAS ENTRE [{}] A [{}]", inicio, inicio2);
            predicates.add(cb.between(from.<Date>get("dateCreated"), inicio, inicio2));
        }

        if (StringUtils.trimToNull(ip) != null) {
            LOGGER.info(">>>>> ipAddress=" + ip);
            predicates.add(cb.equal(from.get("ipAddress"), StringUtils.trimToEmpty(ip)));
        }

        if (wsOperation != null) {
            LOGGER.info(">>>>> wsOperation=" + wsOperation.toString());
            predicates.add(cb.equal(from.get("wsOperation"), wsOperation));
        }
        return predicates.toArray(new Predicate[predicates.size()]);
    }

    /**
     * <p>Insertas a new record in ISBP84 with adata about request payload. MILIS contains the milliseconds read the right moment of
     * insertion. After this insertion, need to update the following fields:</p>
     * <p>
     * <ul>
     * <li>S84HTTPST</li>
     * <li>S84CODRES</li>
     * <li><b>S84MILIS</b>, use the current value in thsi column ad substract it from the CURRENT_TIME_MILLIS to calculate the elapsed time</li>
     * </ul>
     * </p>
     *
     * @param httpServletRequest
     * @param webServiceOperation
     */
    public WSPayload persist(HttpServletRequest httpServletRequest, AuthWebServiceOperation webServiceOperation) {

        String payloadMsg = (String) httpServletRequest.getAttribute(WSPayloadDao.TRANSPORTED_PAYLOAD);

        WSPayload payload = new WSPayload();

        payload.setIpAddress(httpServletRequest.getRemoteAddr());
        payload.setDateCreated(new Date());
        payload.setElapsedTime(System.currentTimeMillis());
        payload.setHttpStatusCode(0);
        payload.setWsOperation(webServiceOperation);
        payload.setRequestPayload(payloadMsg);
//        payload.setWsType(WSPayload.WSType.SERVER);
        payload.setReturnCode("0");

        return merge(payload);
    }
}
