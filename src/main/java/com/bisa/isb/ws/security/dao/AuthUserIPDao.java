package com.bisa.isb.ws.security.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.isb.ws.security.api.UserIpService;
import com.bisa.isb.ws.security.entities.AuthUser;
import com.bisa.isb.ws.security.entities.AuthUserIP;
import com.bisa.isb.ws.security.entities.EnumEnabled;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * @author Roger Chura @version 1.0
 * @author rsalvatierra modificado on 15/04/2016
 */
public class AuthUserIPDao extends DaoImpl<AuthUserIP, Long> implements UserIpService, Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthUserIPDao.class);

    @Inject
    public AuthUserIPDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Long> countPath(Root<AuthUserIP> from) {
        return from.get("idUser");
    }


    public List<AuthUserIP> getAllIPAddresses(AuthUser authUser) {
        return doWithTransaction((entityManager, t) -> {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<AuthUserIP> q = cb.createQuery(AuthUserIP.class);
            Root<AuthUserIP> p = q.from(AuthUserIP.class);

            q.where(cb.equal(p.get("idUser"), authUser.getId()));
            TypedQuery<AuthUserIP> query = entityManager.createQuery(q);
            return query.getResultList();
        });
    }

    @Override
    public boolean matchList(List<String> lista1, List<String> lista2) {
        Collection<String> different1 = new HashSet<>(lista1);
        Collection<String> different2 = new HashSet<>(lista2);

        different1.removeAll(lista2);
        different2.removeAll(lista1);

        LOGGER.debug("Lista DIFERENTE 1 ->" + different1.toString());
        LOGGER.debug("Lista DIFERENTE 2 ->" + different2.toString());

        return !(different1.isEmpty() && different2.isEmpty());
    }

    @Override
    public boolean newUserIp(AuthUser user, List<String> ips, String usuario) {
        if (user == null) {
            LOGGER.error("Error: No se puede identificar el Usuario. No se puede registrar la relacion Usuario-Ip.");
            return false;
        }
        if (ips == null) {
            LOGGER.error("Error: No se puede obtener la lista de ips. No se puede registrar la relacion Usuario-Ip.");
            return false;
        }

        int contador = 0;
        for (String ip : ips) {
            AuthUserIP userIP = new AuthUserIP();
            try {
                userIP.setIdUser(user.getId());
                userIP.setIp(ip);
                userIP.setEnabled(EnumEnabled.S.name());
                userIP.setFechaCreacion(new Date());
                userIP.setUsuarioCreador(usuario);
                userIP.setFechaModificacion(new Date());
                userIP.setUsuarioModificador(usuario);
                merge(userIP);
                contador++;
            } catch (Exception e) {
                LOGGER.error("Error <Exception>: No se puede insertar el registro <AuthUserIP> con ID [" + user.toString() + "].", e);
                return false;
            }
        }
        LOGGER.debug("Se insertaron {} registro(s) de tipo <AuthUserIP>.", contador);
        return true;
    }

    @Override
    public boolean deleteUserIp(AuthUser usuario) {
        // Eliminacion de IPs
        if (usuario.getIps() != null && usuario.getIps().size() > 0) {
            List<AuthUserIP> ips = getAllIPAddresses(usuario);
            for (AuthUserIP ip : ips) {
                remove(ip.getId());
            }
        }
        return true;
    }

}
