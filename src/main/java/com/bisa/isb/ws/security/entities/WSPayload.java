package com.bisa.isb.ws.security.entities;

import bus.plumbing.utils.FormatosUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The payload entity contains request and response payloads
 *
 * @author Miguel Vega
 * @author rsalvatierra modificado on 21/04/2016
 * @version $Id: WSUser.java 0, 2015-09-22 10:05 PM mvega $
 */
@Entity
@Table(name = "ISBP84")
public class WSPayload implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S84ID")
    private Long id;

    @Column(name = "S84SOLIC", nullable = false)
    private String requestPayload;

    @Column(name = "S84RESP", nullable = false)
    private String responsePayload;

    @Column(name = "S84FECREG", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;

    @Column(name = "S84MILIS", nullable = false)
    private Long elapsedTime;

    @Column(name = "S84IP")
    private String ipAddress;

    @Column(name = "S84CODRES")
    private String returnCode;

    @Column(name = "S84HTTPST")
    private Integer httpStatusCode;

    @ManyToOne(optional = true, fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "S84ISBP87")
    private AuthWebServiceOperation wsOperation;

    public Integer getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(Integer httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequestPayload() {
        return requestPayload;
    }

    public void setRequestPayload(String requestPayload) {
        this.requestPayload = requestPayload;
    }

    public String getResponsePayload() {
        return responsePayload;
    }

    public void setResponsePayload(String responsePayload) {
        this.responsePayload = responsePayload;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(Long elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WSPayload that = (WSPayload) o;

        if (getId() != that.getId()) return false;
        if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
        if (httpStatusCode != null ? !httpStatusCode.equals(that.httpStatusCode) : that.httpStatusCode != null)
            return false;
        if (ipAddress != null ? !ipAddress.equals(that.ipAddress) : that.ipAddress != null) return false;
        if (requestPayload != null ? !requestPayload.equals(that.requestPayload) : that.requestPayload != null)
            return false;
        if (responsePayload != null ? !responsePayload.equals(that.responsePayload) : that.responsePayload != null)
            return false;
        if (returnCode != null ? !returnCode.equals(that.returnCode) : that.returnCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().intValue() : 0;
        result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
        result = 31 * result + (httpStatusCode != null ? httpStatusCode.hashCode() : 0);
        result = 31 * result + (ipAddress != null ? ipAddress.hashCode() : 0);
        result = 31 * result + (requestPayload != null ? requestPayload.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WSPayload{" +
                "id=" + id +
                ", requestPayload='" + requestPayload + '\'' +
                ", responsePayload='" + responsePayload + '\'' +
                ", dateCreated=" + dateCreated +
                ", elapsedTime=" + elapsedTime +
                ", ipAddress='" + ipAddress + '\'' +
                ", returnCode='" + returnCode + '\'' +
                ", httpStatusCode=" + httpStatusCode +
                ", wsOperation=" + wsOperation +
                '}';
    }

    public AuthWebServiceOperation getWsOperation() {
        return wsOperation;
    }

    public void setWsOperation(AuthWebServiceOperation wsOperation) {
        this.wsOperation = wsOperation;
    }

    public String getFormatoDateCreated() {
        return FormatosUtils.formatoFechaHora(dateCreated);
    }

    public String getTextoRequest() {
        return "ver solicitud";
    }

    public String getTextoResponse() {
        return "ver respuesta";
    }

    public String getTextoServicio() {
        StringBuffer sb = new StringBuffer("");
        sb.append(getWsOperation().getWebService().getName());
        sb.append("::");
        sb.append(getWsOperation().getName());
        return sb.toString();
    }
}
