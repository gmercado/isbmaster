package com.bisa.isb.ws.security.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;

/**
 * @author Roger Chura
 * @version 1.0
 */
@Entity
@Table(name = "ISBP88")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S88FECA")),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S88USUA")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S88FECM")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S88USUM"))})
public class AuthRoleOperation extends EntityBase {

    @EmbeddedId
    private PKRoleOper id;

    @Column(name = "S88DISP")
    private String enabled;

    public PKRoleOper getId() {
        return id;
    }

    public void setId(PKRoleOper id) {
        this.id = id;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthRoleOperation that = (AuthRoleOperation) o;
        return !(id != null ? !id.equals(that.id) : that.id != null);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }


}
