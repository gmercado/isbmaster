package com.bisa.isb.ws.security.ui;

import bus.database.components.FiltroPanel;
import bus.plumbing.components.Html5DateTextField;
import com.bisa.isb.ws.security.api.WSOperationService;
import com.bisa.isb.ws.security.entities.AuthWebServiceOperation;
import com.bisa.isb.ws.security.entities.WSPayload;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import java.util.List;

/**
 * @author rchura on 16-12-15.
 * @author rsalvatierra modificado on 21/04/2016
 */
public class HistorialWebServices extends FiltroPanel<WSPayload> {

    @Inject
    WSOperationService wsOperationService;

    public HistorialWebServices(String id, IModel<WSPayload> model1, IModel<WSPayload> model2, ModalWindow inpanel, ModalWindow outpanel) {
        super(id, model1, model2);

        add(new Html5DateTextField("fechaDesde", new PropertyModel<>(model1, "dateCreated")).setOutputMarkupId(true));
        add(new Html5DateTextField("fechaHasta", new PropertyModel<>(model2, "dateCreated")).setOutputMarkupId(true));
        add(new TextField<>("ip", new PropertyModel<>(model1, "ipAddress")));
        add(new DropDownChoice<>("operacion", new PropertyModel<>(model1, "wsOperation"),
                new AbstractReadOnlyModel<List<AuthWebServiceOperation>>() {
                    @Override
                    public List<AuthWebServiceOperation> getObject() {
                        return wsOperationService.getOperations();
                    }
                }, new IChoiceRenderer<AuthWebServiceOperation>() {
            @Override
            public Object getDisplayValue(AuthWebServiceOperation authWebServiceOperation) {
                return authWebServiceOperation.getDescOperacionWS();
            }

            @Override
            public String getIdValue(AuthWebServiceOperation authWebServiceOperation, int i) {
                return Integer.toString(i);
            }

            @Override
            public AuthWebServiceOperation getObject(String s, IModel<? extends List<? extends AuthWebServiceOperation>> iModel) {
                if (StringUtils.trimToNull(s) != null) return iModel.getObject().get(Integer.parseInt(s));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));

        add(inpanel);
        add(outpanel);
    }
}