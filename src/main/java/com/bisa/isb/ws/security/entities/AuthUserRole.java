package com.bisa.isb.ws.security.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;

/**
 * @author Roger Chura
 * @version 1.0
 */
@Entity
@Table(name = "ISBP85")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S85FECA")),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S85USUA")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S85FECM")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S85USUM"))})
public class AuthUserRole extends EntityBase {

    @EmbeddedId
    private PKUserRole id;

    @Column(name = "S85DISP")
    private String enabled;


    public PKUserRole getId() {
        return id;
    }

    public void setId(PKUserRole id) {
        this.id = id;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AuthUserRole{");
        sb.append("id=").append(id);
        sb.append(", enabled='").append(enabled).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
