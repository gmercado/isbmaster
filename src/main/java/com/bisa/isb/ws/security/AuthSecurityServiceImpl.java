package com.bisa.isb.ws.security;

import bus.database.model.BasePrincipal;
import bus.database.model.BaseSinCambioAmbiente;
import bus.plumbing.utils.Par;
import com.bisa.isb.api.security.PasswordHash;
import com.bisa.isb.catalog.Catalog;
import com.bisa.isb.ws.catalog.Operation;
import com.bisa.isb.ws.catalog.Remark;
import com.bisa.isb.ws.catalog.Service;
import com.bisa.isb.ws.catalog.Services;
import com.bisa.isb.ws.security.dao.AuthUserIPDao;
import com.bisa.isb.ws.security.entities.AuthUser;
import com.bisa.isb.ws.security.entities.AuthUserIP;
import com.bisa.isb.ws.security.entities.AuthWebService;
import com.bisa.isb.ws.security.entities.AuthWebServiceOperation;
import com.bisa.isb.ws.support.AuthenticationException;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.http.auth.Credentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.bisa.isb.ws.support.Annotations.get;
import static com.google.common.base.Strings.isNullOrEmpty;
import static java.util.Optional.ofNullable;

/**
 * @author Miguel Vega
 * @author rsalvatierra modificado on 21/04/2016
 * @version $Id: SecurityServiceImpl.java 0, 2015-09-22 10:43 PM mvega $
 */
public class AuthSecurityServiceImpl implements AuthSecurityService {

    public static final String ISB_ENGINE = "ISB engine";
    public static final String HABILITADO = "S";
    final Logger logger = LoggerFactory.getLogger(getClass());

    private final EntityManagerFactory entityManagerFactory;

    private final AuthUserIPDao userIPDao;

    @Inject
    public AuthSecurityServiceImpl(@BaseSinCambioAmbiente DataSource dataSource,
                                   @BasePrincipal EntityManagerFactory entityManagerFactory,
                                   AuthUserIPDao userIPDao) {
        super();
        this.entityManagerFactory = entityManagerFactory;
        this.userIPDao = userIPDao;
    }

    @Override
    public AuthUser applySecuredPassword(AuthUser authUser, String password) {
        try {
            authUser.setPassword(PasswordHash.createHash(password));
            return authUser;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new IllegalStateException("Ha ocurrido un error inesperado", e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param credentials
     * @return
     */
    @Override
    public AuthUser getUserByName(Credentials credentials) throws AuthenticationException {

        TypedQuery<AuthUser> query = entityManagerFactory.createEntityManager().
                createQuery("select a from AuthUser a WHERE  a.userName = :userName", AuthUser.class);
        query.setParameter("userName", credentials.getUserPrincipal().getName());
        try {
            AuthUser authUser = query.getSingleResult();

            String hashed = authUser.getPassword();


            if (!PasswordHash.validatePassword(credentials.getPassword(), hashed)) {
                throw new AuthenticationException("Invalid password. User '" + credentials.getUserPrincipal().getName() +
                        "' and password  ***** do not match.");
            }

            return authUser;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new IllegalStateException("Ha ocurrido un error inesperado", e);
        } catch (NoResultException x) {
            throw new AuthenticationException("User '" + credentials.getUserPrincipal().getName() + "' is not present in database");
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param serviceCatalog
     */
    @Override
    @Transactional
    public void populateCatalog(Catalog<Service> serviceCatalog, Service.Type type) {
        Set<Service> services = serviceCatalog.fetchAll();

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        //fetch existing services
        List<AuthWebService> dbServices = entityManager.createQuery("select ws from AuthWebService ws where ws.type=:tipo", AuthWebService.class)
                .setParameter("tipo", type)
                .getResultList();
        //compare two lists to differentiate new and existng services
        Set<Service> newServices = Sets.newConcurrentHashSet();
        Set<Par<Service, AuthWebService>> existingServices = Sets.newConcurrentHashSet();

        services.forEach(servC -> {
            if (servC.getType().equals(type)) {
                AuthWebService authWebService = dbServices.stream().filter(
                        dbServ -> StringUtils.trimToNull(dbServ.getJavaTypeName()).equals(StringUtils.trimToNull(servC.getEndpoint().getName())))
                        .findFirst()
                        .orElse(null);
                if (authWebService == null) {
                    //service does not exist
                    newServices.add(servC);
                } else {
                    //service already exists. Updata some data
                    existingServices.add(new Par<>(servC, authWebService));
                }
            }
        });

        //insert new services
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            newServices.forEach(service -> {
                AuthWebService ws = new AuthWebService();
                ws.setJavaTypeName(service.getEndpoint().getName());
                ws.setName(service.getName().getLocalPart());
                ws.setNameSpace(service.getName().getNamespaceURI());
                ws.setEnabled(HABILITADO);
                ws.setEndpoint(service.getName().getPrefix());
                ws.setType(service.getType());
                ws.setDescription(isNullOrEmpty(service.getRemark()) ? "" : service.getRemark());
                ws.setFechaCreacion(new Date());
                ws.setFechaModificacion(new Date());
                ws.setUsuarioCreador(ISB_ENGINE);
                ws.setUsuarioModificador(ISB_ENGINE);
                Set<Operation> operations = Services.discoverServiceOperations2(service);
                operations.forEach(op -> {
                    AuthWebServiceOperation wsop = new AuthWebServiceOperation();
                    wsop.setName(op.getName());
                    wsop.setEnabled(HABILITADO);
                    wsop.setDesc(ofNullable(get(op.getMethod(), Remark.class).value()).orElse(""));
                    wsop.setJavaMethodName(op.getMethod().getName());
                    wsop.setFechaCreacion(new Date());
                    wsop.setUsuarioCreador(ISB_ENGINE);
                    wsop.setFechaModificacion(new Date());
                    wsop.setUsuarioModificador(ISB_ENGINE);
                    wsop.setWebService(ws);
                    ws.getOperations().add(wsop);
                });
                entityManager.merge(ws);
            });
            //Verificar existentes
            existingServices.forEach(serviceAuthWebServicePar -> {
                Service service = serviceAuthWebServicePar.getCar();
                AuthWebService ws = serviceAuthWebServicePar.getCdr();
                Set<Operation> operations = Services.discoverServiceOperations2(service);
                operations.forEach(operation -> {
                    AuthWebServiceOperation wsop = ws.getOperations().stream().filter(authWebServiceOperation ->
                            StringUtils.trimToNull(authWebServiceOperation.getName()).equals(StringUtils.trimToNull(operation.getName()))
                                    && StringUtils.trimToNull(authWebServiceOperation.getJavaMethodName()).equals(StringUtils.trimToNull(operation.getMethod().getName())))
                            .findFirst()
                            .orElse(null);
                    if (wsop == null) {
                        //operation does not exist
                        wsop = new AuthWebServiceOperation();
                        wsop.setName(operation.getName());
                        wsop.setEnabled(HABILITADO);
                        wsop.setDesc(ofNullable(get(operation.getMethod(), Remark.class).value()).orElse(""));
                        wsop.setJavaMethodName(operation.getMethod().getName());
                        wsop.setFechaCreacion(new Date());
                        wsop.setUsuarioCreador(ISB_ENGINE);
                        wsop.setFechaModificacion(new Date());
                        wsop.setUsuarioModificador(ISB_ENGINE);
                        wsop.setWsId(ws.getId());
                        //wsop.setWebService(ws);
                        entityManager.merge(wsop);
                    }
                });
            });
            transaction.commit();
        } catch (Throwable t) {
            logger.error("An unexpected error has occurred", t);
            transaction.rollback();
        }
    }

    @Override
    public List<AuthWebService> getAuthServices() {
        TypedQuery<AuthWebService> query = entityManagerFactory.createEntityManager().createQuery("select a from AuthWebService a", AuthWebService.class);
        return query.getResultList();
    }

    @Override
    public List<AuthUserIP> getAllIPAddresses(AuthUser authUser) {
        List<AuthUserIP> lista = userIPDao.getAllIPAddresses(authUser);
        for (AuthUserIP p : lista) {
            logger.info("BASE DE DATOS -> " + p.toString());
        }
        return lista;
    }
}
