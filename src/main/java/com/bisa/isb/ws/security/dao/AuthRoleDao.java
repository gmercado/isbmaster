package com.bisa.isb.ws.security.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.isb.ws.security.api.RoleService;
import com.bisa.isb.ws.security.entities.*;
import com.google.inject.Inject;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author Roger Chura @version 1.0
 * @author rsalvatierra modificado on 14/04/2016
 */
public class AuthRoleDao extends DaoImpl<AuthRole, Integer> implements RoleService, Serializable {

    @Inject
    public AuthRoleDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Integer> countPath(Root<AuthRole> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<AuthRole> p) {
        Path<String> userlogon = p.get("name");
        return Collections.singletonList(userlogon);
    }

    @Override
    public AuthRole getRole(int id) {
        return find(id);
    }

    @Override
    public List<AuthRole> getRoles() {
        AuthRole authRole = new AuthRole();
        authRole.setEnabled(EnumEnabled.S.name());
        return getTodos(authRole, null, 0, 0);
    }

    @Override
    public AuthRole newRole(AuthRole role, List<AuthWebServiceOperation> operaciones, String usuario) {
        if (role == null) return null;
        return doWithTransaction((entityManager, t) -> {
            AuthRole newRole = role;
            newRole.setEnabled(EnumEnabled.S.name());
            newRole.setFechaCreacion(new Date());
            newRole.setUsuarioCreador(usuario);
            newRole.setFechaModificacion(new Date());
            newRole.setUsuarioModificador(usuario);
            newRole.setOperations(null);
            newRole.setUsuarios(null);
            newRole = persist(newRole);
            for (AuthWebServiceOperation x : operaciones) {
                AuthRoleOperation rolOp = new AuthRoleOperation();
                PKRoleOper pkRolOP = new PKRoleOper();
                pkRolOP.setIdRole(newRole.getId());
                pkRolOP.setIdOper(x.getId());
                rolOp.setId(pkRolOP);
                rolOp.setEnabled(EnumEnabled.S.name());
                rolOp.setFechaCreacion(new Date());
                rolOp.setUsuarioCreador(usuario);
                rolOp.setFechaModificacion(new Date());
                rolOp.setUsuarioModificador(usuario);
                entityManager.persist(rolOp);
            }
            return entityManager.find(AuthRole.class, newRole.getId());
        });
    }

    @Override
    public void updateRole(AuthRole role, String usuario) {
        role.setUsuarioModificador(usuario);
        role.setFechaModificacion(new Date());
        merge(role);
    }

    @Override
    public void deleteRole(AuthRole role) {
        remove(role.getId());
    }
}
