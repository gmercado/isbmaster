package com.bisa.isb.ws.security.entities;

import bus.database.model.EntityBase;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author Miguel Vega
 * @version $Id: WSUser.java 0, 2015-09-22 10:05 PM mvega $
 */
@Entity
@Table(name = "ISBP82")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S82FECA", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S82USUA")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S82FECM")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S82USUM"))
})
public class AuthUser extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S82ID")
    private Long id;

    @Column(name = "S82NOM", nullable = false)
    private String userName;

    @Column(name = "S82DESC", nullable = false)
    private String desc;

    @Column(name = "S82PWD", nullable = false)
    private String password;

    @Column(name = "S82EMAIL", nullable = false)
    private String email;

    @Column(name = "S82DISP")
    private String enabled;

    @Transient
    private EnumEnabled tipoEnabled;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true) //FetchType.LAZY
    @JoinTable(
            name = "ISBP85",
            joinColumns = {@JoinColumn(name = "S85ISBP82", referencedColumnName = "S82ID")},
            inverseJoinColumns = {@JoinColumn(name = "S85ISBP83", referencedColumnName = "S83ID", unique = true)}
    )
    private List<AuthRole> roles;

    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name = "S81IP")
    @JoinTable(name = "ISBP81", joinColumns = @JoinColumn(name = "S81ISBP82"))
    private List<String> ips;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<AuthRole> getRoles() {
        roles = Optional.fromNullable(roles).or(Lists.newLinkedList());
        return roles;
    }

    public void setRoles(List<AuthRole> roles) {
        this.roles = roles;
    }

    public List<String> getIps() {
        return ips;
    }

    public void setIps(List<String> ips) {
        this.ips = ips;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String mail) {
        this.email = mail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthUser that = (AuthUser) o;

        return Objects.equals(id, that.id) && !(userName != null ? !userName.equals(that.userName) : that.userName != null) && !(password != null ? !password.equals(that.password) : that.password != null);

    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public EnumEnabled getTipoEnabled() {
        return tipoEnabled;
    }

    public void setTipoEnabled(EnumEnabled tipoEnabled) {
        this.tipoEnabled = tipoEnabled;
    }

    @Override
    public String toString() {
        return "AuthUser{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", desc='" + desc + '\'' +
                ", email='" + email + '\'' +
                ", enabled='" + enabled + '\'' +
                '}';
    }

    public String getTextPwd() {
        return "password";
    }
}
