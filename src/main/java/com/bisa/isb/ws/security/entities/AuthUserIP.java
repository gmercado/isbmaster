package com.bisa.isb.ws.security.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;

/**
 * @author Roger Chura
 * @version 1.0
 */
@Entity
@Table(name = "ISBP81")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S81FECA")),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S81USUA")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S81FECM")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S81USUM"))})
public class AuthUserIP extends EntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S81ID")
    private Long id;

    @Column(name = "S81ISBP82")
    private Long idUser;

    @Column(name = "S81IP")
    private String ip;

    @Column(name = "S81DISP")
    private String enabled;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        return "AuthUserIP{" +
                "id=" + id +
                ", idUser=" + idUser +
                ", ip='" + ip + '\'' +
                ", enabled='" + enabled + '\'' +
                '}';
    }

}
