package com.bisa.isb.ws.security.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;

/**
 * @author Miguel Vega
 * @version $Id: AuthWSOperation.java; sep 23, 2015 05:03 PM mvega $
 */
@Entity
@Table(name = "ISBP87")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S87FECA")),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S87USUA")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S87FECM")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S87USUM"))})
public class AuthWebServiceOperation extends EntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S87ID")
    private Integer id;

    @Column(name = "S87ISBP86")
    private Integer wsId;

    @Column(name = "S87NOM")
    private String name;

    @Column(name = "S87DESC")
    private String desc;

    @Column(name = "S87DISP")
    private String enabled;

    @Column(name = "S87JOP")
    private String javaMethodName;

    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL) //LAZY
    @JoinColumn(name = "S87ISBP86", updatable = true, insertable = true, nullable = true)
    private AuthWebService webService;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWsId() {
        return wsId;
    }

    public void setWsId(Integer wsId) {
        this.wsId = wsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String descOperation) {
        this.desc = descOperation;
    }

//    @Override
//    public int hashCode() {
//        return super.hashCode();
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        return super.equals(obj);
//    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthWebServiceOperation that = (AuthWebServiceOperation) o;

        if (!id.equals(that.id)) return false;
        if (!wsId.equals(that.wsId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + wsId.hashCode();
        return result;
    }


    public AuthWebService getWebService() {
        return webService;
    }

    public void setWebService(AuthWebService webService) {
        this.webService = webService;
    }

    public String getJavaMethodName() {
        return javaMethodName;
    }

    public void setJavaMethodName(String javaMethodName) {
        this.javaMethodName = javaMethodName;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AuthWebServiceOperation{");
        sb.append("id=").append(id);
        sb.append(", wsId=").append(wsId);
        sb.append(", name='").append(name).append('\'');
        sb.append(", desc='").append(desc).append('\'');
        sb.append(", enabled='").append(enabled).append('\'');
        sb.append(", javaMethodName='").append(javaMethodName).append('\'');
        sb.append(", webService=").append(webService);
        sb.append('}');
        return sb.toString();
    }

    public String getDescOperacionWS(){
        String operacion = ""+getName()+" ["+getJavaMethodName()+" - "+getDesc()+"] ";
        if(getWebService()!=null) return operacion+
                "{"+getWebService().getType().name()+" = "+getWebService().getName()+" - "+getWebService().getDescription()+" ("+getWebService().getEndpoint()+")}";
        else return operacion;
    }
}
