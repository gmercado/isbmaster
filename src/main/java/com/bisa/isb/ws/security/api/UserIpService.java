package com.bisa.isb.ws.security.api;

import com.bisa.isb.ws.security.entities.AuthUser;

import java.util.List;

/**
 * @author rsalvatierra on 14/04/2016.
 */
public interface UserIpService {

    boolean matchList(List<String> lista1, List<String> lista2);

    boolean newUserIp(AuthUser user, List<String> ips, String usuario);

    boolean deleteUserIp(AuthUser user);
}
