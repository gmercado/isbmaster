package com.bisa.isb.ws.security;

import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.isb.ws.security.api.*;
import com.bisa.isb.ws.security.dao.*;

/**
 * @author Marcelo Morales @since 4/30/12
 * @author rsalvatierra modificado on 14/04/2016
 */
public class WSSecurityModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(UserService.class).to(AuthUserDao.class);
        bind(RoleService.class).to(AuthRoleDao.class);
        bind(UserRoleService.class).to(AuthUserRoleDao.class);
        bind(UserIpService.class).to(AuthUserIPDao.class);
        bind(RoleOperationService.class).to(AuthRoleOperationDao.class);
        bind(WebServService.class).to(AuthWebServiceDao.class);
        bind(WSOperationService.class).to(AuthWSOperationDao.class);
        bindUI("com/bisa/isb/ws/security/ui");
    }
}
