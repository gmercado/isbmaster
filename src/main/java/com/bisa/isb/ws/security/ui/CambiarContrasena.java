/*
 *  Copyright 2009 Banco Bisa S.A.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.bisa.isb.ws.security.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.AbstractValidator;
import bus.users.api.Metadatas;
import com.bisa.isb.api.security.PasswordHash;
import com.bisa.isb.ws.security.api.UserService;
import com.bisa.isb.ws.security.entities.AuthUser;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.validation.EqualPasswordInputValidator;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.io.IClusterable;
import org.apache.wicket.validation.IErrorMessageSource;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidationError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;

/**
 * @author rchura on 22-12-15.
 * @author rsalvatierra modificado on 14/04/2016
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Titulo("Cambio de contrase\u00f1a")
public class CambiarContrasena extends BisaWebPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(CambiarContrasena.class);
    @Inject
    UserService userServices;
    private String actual;
    private String nueva1;
    private String nueva2;

    public CambiarContrasena(PageParameters parameters) {
        super(parameters);

        final AuthUser user = userServices.getUser(getPageParameters().get("id").toInteger());
        if (user == null) getSession().error("No se puede obtener el registro del usuario.");

        Form<?> cambiarcontrasena;
        add(cambiarcontrasena = new Form<Void>("cambiarcontrasena"));

        PasswordTextField actualtx;
        cambiarcontrasena.add(actualtx = new PasswordTextField("actual", new PropertyModel<>(this, "actual")));
        actualtx.add(new AbstractValidator<String>() {
            @Override
            protected void onValidate(IValidatable<String> validatable) {
                try {
                    if (user != null) {
                        if (!PasswordHash.validatePassword(validatable.getValue(), user.getPassword())) {
                            validatable.error(messageSource -> "Contrase\u00f1a original no coincide");
                        }
                    }
                } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                    LOGGER.error("Hubo un error : {}", e);
                }
            }
        });

        PasswordTextField nuevo1tx;
        cambiarcontrasena.add(nuevo1tx = new PasswordTextField("nueva1", new PropertyModel<>(this, "nueva1")));
        nuevo1tx.add(new AbstractValidator<String>() {
            @Override
            protected void onValidate(IValidatable<String> validatable) {
                if (StringUtils.length(validatable.getValue()) < 10) {
                    validatable.error(new MyValidationError());
                }
            }
        });

        PasswordTextField nuevo2tx;
        cambiarcontrasena.add(nuevo2tx = new PasswordTextField("nueva2", new PropertyModel<>(this, "nueva2")));

        cambiarcontrasena.add(new EqualPasswordInputValidator(nuevo1tx, nuevo2tx));

        cambiarcontrasena.add(new Button("guardar", Model.of("Cambiar")) {
            @Override
            public void onSubmit() {
                try {
                    String usuario = getSession().getMetaData(Metadatas.USER_META_DATA_KEY).getUserlogon();
                    if (getNueva1().equalsIgnoreCase(getNueva2())) {
                        if (user != null) {
                            user.setPassword(PasswordHash.createHash(getNueva1()));
                            user.setFechaModificacion(new Date());
                            userServices.updateUser(user, usuario);
                            getSession().info("Contrase\u00f1a cambiada satisfactoriamente");
                            setResponsePage(ListadoUsers.class);
                        }
                    } else {
                        getSession().error("No se puede cambiar la contrase\u00f1a.");
                    }

                } catch (Exception e) {
                    LOGGER.error("Error al cambiar contrase\u00f1a de un usuario", e);
                    getSession().error("Ha ocurrido un error al cambiar la contrase\u00f1a, comun\u00edquese con personal de Banco Bisa");
                }
            }
        });

        cambiarcontrasena.add(new Button("cancelar", Model.of("Cancelar")) {
            @Override
            public void onSubmit() {
                setResponsePage(ListadoUsers.class);
                getSession().info("Operaci\u00f3n cancelada, no se ha modificado la contrase\u00f1a del usuario");
            }
        }.setDefaultFormProcessing(false));
    }

    public String getActual() {
        return actual;
    }

    public void setActual(String actual) {
        this.actual = actual;
    }

    public String getNueva1() {
        return nueva1;
    }

    public void setNueva1(String nueva1) {
        this.nueva1 = nueva1;
    }

    public String getNueva2() {
        return nueva2;
    }

    public void setNueva2(String nueva2) {
        this.nueva2 = nueva2;
    }

    private static class MyValidationError implements IValidationError, IClusterable, Serializable {

        @Override
        public String getErrorMessage(IErrorMessageSource messageSource) {
            return "La contrase\u00f1a debe tener por lo menos 10 caracteres.";
        }
    }
}
