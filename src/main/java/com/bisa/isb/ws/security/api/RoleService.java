package com.bisa.isb.ws.security.api;

import com.bisa.isb.ws.security.entities.AuthRole;
import com.bisa.isb.ws.security.entities.AuthWebServiceOperation;

import java.util.List;

/**
 * @author rsalvatierra on 14/04/2016.
 */
public interface RoleService {

    AuthRole getRole(int id);

    List<AuthRole> getRoles();

    AuthRole newRole(AuthRole role, List<AuthWebServiceOperation> operaciones, String usuario);

    void updateRole(AuthRole data, String usuario);

    void deleteRole(AuthRole data);
}
