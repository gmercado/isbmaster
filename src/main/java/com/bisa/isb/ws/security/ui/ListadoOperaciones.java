package com.bisa.isb.ws.security.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import com.bisa.isb.ws.security.api.WSOperationService;
import com.bisa.isb.ws.security.api.WebServService;
import com.bisa.isb.ws.security.entities.AuthWebServiceOperation;
import com.google.inject.Inject;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.core.util.lang.PropertyResolver;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.stream.Collectors;

/**
 * @author rchura on 23-09-15.
 * @author rsalvatierra modificado on 14/04/2016
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Titulo("Operaciones Servicio Web")
public class ListadoOperaciones extends BisaWebPage {

    @Inject
    WSOperationService wsOperationService;

    @Inject
    WebServService wsServService;

    public ListadoOperaciones(PageParameters parameters) {
        super(parameters);
        LinkedList<IColumn<AuthWebServiceOperation, String>> iColumns = new LinkedList<>();
        iColumns.add(new PropertyColumn<>(Model.of("Nombre operacion"), "name", "name"));
        iColumns.add(new PropertyColumn<>(Model.of("Descripci\u00f3n"), "desc", "desc"));
        iColumns.add(new PropertyColumn<>(Model.of("Disponible"), "enabled", "enabled"));
        iColumns.add(new PropertyColumn<>(Model.of("M\u00e9todo"), "javaMethodName", "javaMethodName"));

        SortableDataProvider<AuthWebServiceOperation, String> sortableDataProvider = new ColasDataProvider();
        add(new AjaxFallbackDefaultDataTable<>("listadoOperaciones", iColumns, sortableDataProvider, 5));

        //Form
        Form<?> form = new Form<Void>("ope");
        add(form);
        //botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(ListadoServicios.class);
            }
        });
    }

    private class ColasDataProvider extends SortableDataProvider<AuthWebServiceOperation, String> {

        private final LinkedList<AuthWebServiceOperation> colas;

        {
            colas = new LinkedList<>();
            colas.addAll(wsOperationService.getOperations(wsServService.get(getPageParameters().get("id").toInt())).stream().collect(Collectors.toList()));
        }

        @Override
        public Iterator<? extends AuthWebServiceOperation> iterator(long first, long count) {
            SortParam sort = getSort();
            if (sort != null) {
                Collections.sort(colas, (o1, o2) -> {
                    Integer value1 = ((Number) PropertyResolver.getValue(getSort().getProperty(), o1)).intValue();
                    Integer value2 = ((Number) PropertyResolver.getValue(getSort().getProperty(), o2)).intValue();
                    if (getSort().isAscending()) {
                        return value1.compareTo(value2);
                    } else {
                        return value2.compareTo(value1);
                    }
                });
            }
            return colas.subList((int) first, (int) (first + count)).iterator();
        }

        @Override
        public long size() {
            return colas.size();
        }

        @Override
        public IModel<AuthWebServiceOperation> model(AuthWebServiceOperation object) {
            return Model.of(object);
        }
    }
}
