package com.bisa.isb.ws.security.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.isb.ws.security.api.UserRoleService;
import com.bisa.isb.ws.security.entities.*;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * @author Roger Chura @version 1.0
 * @author rsalvatierra modificado on 14/04/2016
 */
public class AuthUserRoleDao extends DaoImpl<AuthUserRole, PKUserRole> implements UserRoleService, Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthUserRoleDao.class);

    @Inject
    public AuthUserRoleDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<PKUserRole> countPath(Root<AuthUserRole> from) {
        return from.get("id");
    }

    @Override
    public boolean matchList(List<AuthRole> lista1, List<AuthRole> lista2) {
        Collection<AuthRole> different1 = new HashSet<>(lista1);
        Collection<AuthRole> different2 = new HashSet<>(lista2);

        different1.removeAll(lista2);
        different2.removeAll(lista1);

        LOGGER.debug("Lista DIFERENTE 1 ->" + different1.toString());
        LOGGER.debug("Lista DIFERENTE 2 ->" + different2.toString());

        return !(different1.isEmpty() && different2.isEmpty());
    }

    @Override
    public boolean newUserRole(AuthUser user, List<AuthRole> roles, String usuario) {
        if (user == null) {
            LOGGER.error("Error: No se puede identificar el Usuario. No se puede registrar la relacion Usuario-Rol.");
            return false;
        }
        if (roles == null) {
            LOGGER.error("Error: No se puede obtener la lista de Roles. No se puede registrar la relacion Usuario-Rol.");
            return false;
        }

        int contador = 0;
        for (AuthRole rol : roles) {
            AuthUserRole userRole = new AuthUserRole();
            PKUserRole pkUserRole = new PKUserRole();
            try {
                pkUserRole.setIdUser(user.getId());
                pkUserRole.setIdRol(rol.getId());
                userRole.setId(pkUserRole);
                userRole.setEnabled(EnumEnabled.S.name());
                userRole.setFechaCreacion(new Date());
                userRole.setUsuarioCreador(usuario);
                userRole.setFechaModificacion(new Date());
                userRole.setUsuarioModificador(usuario);
                merge(userRole);
                contador++;
            } catch (Exception e) {
                LOGGER.error("Error <Exception>: No se puede insertar el registro <AuthUserRole> con ID [" + pkUserRole.toString() + "].", e);
                return false;
            }
        }
        LOGGER.debug("Se insertaron {} registro(s) de tipo <AuthUserRole>.", contador);
        return true;
    }

    @Override
    public boolean deleteUserRole(AuthUser user, List<AuthRole> roles) {
        if (user == null) {
            LOGGER.error("Error: No se puede identificar el Usuario. No se puede eliminar la relacion Usuario-Rol.");
            return false;
        }
        if (roles == null) {
            LOGGER.error("Error: No se puede obtener la lista de Roles. No se puede eliminar la relacion Usuario-Rol.");
            return false;
        }

        int contador = 0;
        for (AuthRole rol : roles) {
            PKUserRole pkUserRol = new PKUserRole();
            AuthUserRole userRole;
            try {
                pkUserRol.setIdUser(user.getId());
                pkUserRol.setIdRol(rol.getId());
                userRole = find(pkUserRol);
                if (userRole != null) {
                    LOGGER.debug("Eliminando el registro Rol-Operacion." + userRole.toString());
                    remove(userRole.getId());
                    contador++;
                }
            } catch (Exception e) {
                LOGGER.error("Error <Exception>: No se puede eliminar el registro <AuthUserRole> con ID [" + pkUserRol.toString() + "].", e);
                return false;
            }
        }
        LOGGER.debug("Se eliminaron {} registro(s) de tipo <AuthUserRole>.", contador);
        return true;
    }

    @Override
    public boolean deleteUserRole(AuthRole role, List<AuthUser> users) {
        if (role == null) {
            LOGGER.error("Error: No se puede identificar el rol. No se puede eliminar la relacion Usuario-Rol.");
            return false;
        }
        if (users == null) {
            LOGGER.error("Error: No se puede obtener la lista de usuarios. No se puede eliminar la relacion Usuario-Rol.");
            return false;
        }

        int contador = 0;
        for (AuthUser user : users) {
            PKUserRole pkUserRol = new PKUserRole();
            AuthUserRole userRole;
            try {
                pkUserRol.setIdUser(user.getId());
                pkUserRol.setIdRol(role.getId());
                userRole = find(pkUserRol);
                if (userRole != null) {
                    LOGGER.debug("Eliminando el registro Rol-Operacion." + userRole.toString());
                    remove(userRole.getId());
                    contador++;
                }
            } catch (Exception e) {
                LOGGER.error("Error <Exception>: No se puede eliminar el registro <AuthUserRole> con ID [" + pkUserRol.toString() + "].", e);
                return false;
            }
        }
        LOGGER.debug("Se eliminaron {} registro(s) de tipo <AuthUserRole>.", contador);
        return true;
    }
}
