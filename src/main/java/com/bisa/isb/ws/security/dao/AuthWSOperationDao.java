package com.bisa.isb.ws.security.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.isb.ws.security.api.WSOperationService;
import com.bisa.isb.ws.security.entities.AuthWebService;
import com.bisa.isb.ws.security.entities.AuthWebServiceOperation;
import com.bisa.isb.ws.security.entities.EnumEnabled;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Roger Chura @version 1.0
 * @author rsalvatierra modificadon on 15/04/2016
 */
public class AuthWSOperationDao extends DaoImpl<AuthWebServiceOperation, Integer> implements WSOperationService, Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthWSOperationDao.class);

    @Inject
    public AuthWSOperationDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Integer> countPath(Root<AuthWebServiceOperation> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<AuthWebServiceOperation> p) {
        Path<String> userlogon = p.get("name");
        return Collections.singletonList(userlogon);
    }

    @Override
    public List<AuthWebServiceOperation> getOperations() {
        AuthWebServiceOperation wsoper = new AuthWebServiceOperation();
        wsoper.setEnabled(EnumEnabled.S.name());  // Solo operaciones activas
        return getTodos(wsoper, null, 0, 0);
    }

    @Override
    public List<AuthWebServiceOperation> getOperations(AuthWebService id) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<AuthWebServiceOperation> q = cb.createQuery(AuthWebServiceOperation.class);
                        Root<AuthWebServiceOperation> p = q.from(AuthWebServiceOperation.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("webService"), id));

                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<AuthWebServiceOperation> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta: {}", e);
            return null;
        }
    }
}
