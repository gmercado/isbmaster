package com.bisa.isb.ws.security.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by rchura on 24-09-15.
 */
@Embeddable
public class PKRoleOper implements Serializable {

    @Column(name = "S88ISBP83")
    private Integer idRole;

    @Column(name = "S88ISBP87")
    private Integer idOper;

    public PKRoleOper() {
    }

    public Integer getIdRole() {
        return idRole;
    }

    public void setIdRole(Integer idRole) {
        this.idRole = idRole;
    }

    public Integer getIdOper() {
        return idOper;
    }

    public void setIdOper(Integer idOper) {
        this.idOper = idOper;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PKRoleOper that = (PKRoleOper) o;

        if (idRole != null ? !idRole.equals(that.idRole) : that.idRole != null) return false;
        if (idOper != null ? !idOper.equals(that.idOper) : that.idOper != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idRole.intValue();
        result = 31 * result + (idRole != null ? idRole.hashCode() : 0);
        result = 31 * result + (idOper != null ? idOper.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PKRoleOper{");
        sb.append("idRole=").append(idRole);
        sb.append(", idOper=").append(idOper);
        sb.append('}');
        return sb.toString();
    }
}
