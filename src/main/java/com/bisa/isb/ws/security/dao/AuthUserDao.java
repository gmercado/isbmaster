package com.bisa.isb.ws.security.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.isb.api.security.PasswordHash;
import com.bisa.isb.ws.security.api.UserService;
import com.bisa.isb.ws.security.entities.*;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;

/**
 * @author Roger Chura @version 1.0
 * @author rsalvatierra modificado on 14/04/2016
 */
public class AuthUserDao extends DaoImpl<AuthUser, Integer> implements UserService, Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthUserDao.class);

    @Inject
    public AuthUserDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Integer> countPath(Root<AuthUser> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<AuthUser> p) {
        Path<String> userlogon = p.get("userName");
        return Collections.singletonList(userlogon);
    }

    @Override
    public AuthUser getUser(int id) {
        return find(id);
    }

    @Override
    public AuthUser newUser(AuthUser nuevoUsuario, Set<AuthRole> roles, String username, String pwdDefault) {
        if (nuevoUsuario == null) return null;
        return doWithTransaction((entityManager, t) -> {
            // Crea un nuevo registro de Usuario y los Roles asociados
            // Para que la transaccion sea atomica
            LOGGER.debug("Usuario a registrar  -> " + nuevoUsuario.toString());
            AuthUser user = nuevoUsuario;
            user.setUserName(user.getUserName().toLowerCase());
            user.setEnabled(EnumEnabled.S.name());
            try {
                user.setPassword(PasswordHash.createHash(pwdDefault));
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                LOGGER.error("Hubo un error {}", e);
            }
            user.setFechaCreacion(new Date());
            user.setUsuarioCreador(username);
            user.setFechaModificacion(new Date());
            user.setUsuarioModificador(username);
            user = persist(user);

            LOGGER.debug("Roles a registrar  -> " + roles.toString());
            List<AuthRole> listaRoles = new LinkedList<>();
            listaRoles.addAll(roles);
            for (AuthRole x : listaRoles) {

                AuthUserRole userRole = new AuthUserRole();
                PKUserRole pkUserRole = new PKUserRole();

                pkUserRole.setIdUser(user.getId());
                pkUserRole.setIdRol(x.getId());
                userRole.setId(pkUserRole);
                userRole.setEnabled(EnumEnabled.S.name());
                userRole.setFechaCreacion(new Date());
                userRole.setUsuarioCreador(username);
                userRole.setFechaModificacion(new Date());
                userRole.setUsuarioModificador(username);
                entityManager.persist(userRole);
            }
            return entityManager.find(AuthUser.class, user.getId());
        });
    }

    @Override
    public void updateUser(AuthUser user, String usuario) {
        user.setFechaModificacion(new Date());
        user.setUsuarioModificador(usuario);
        merge(user);
    }

    @Override
    public void deleteUser(AuthUser usuario) {
        remove(usuario.getId().intValue());
    }
}
