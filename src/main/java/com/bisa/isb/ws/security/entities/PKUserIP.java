package com.bisa.isb.ws.security.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by rchura on 24-09-15.
 */
@Embeddable
public class PKUserIP implements Serializable {

    @Column(name = "S81ISBP82")
    private Long idUser;

    @Column(name = "S81IP")
    private String ip;

    public PKUserIP() {
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PKUserIP that = (PKUserIP) o;

        if (idUser != that.idUser) return false;
        if (ip != null ? !ip.equals(that.ip) : that.ip != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idUser.intValue();
        result = 31 * result + (idUser != null ? idUser.hashCode() : 0);
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PKUserIP{");
        sb.append("idUser=").append(idUser);
        sb.append(", ip='").append(ip).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
