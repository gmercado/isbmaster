package com.bisa.isb.ws.security.api;

import com.bisa.isb.ws.security.entities.AuthRole;
import com.bisa.isb.ws.security.entities.AuthUser;

import java.util.List;

/**
 * @author rsalvatierra on 14/04/2016.
 */
public interface UserRoleService {

    boolean matchList(List<AuthRole> lista1, List<AuthRole> lista2);

    boolean newUserRole(AuthUser user, List<AuthRole> roles, String usuario);

    boolean deleteUserRole(AuthUser user, List<AuthRole> roles);

    boolean deleteUserRole(AuthRole rol, List<AuthUser> users);
}
