package com.bisa.isb.ws.security.api;

import com.bisa.isb.ws.security.entities.AuthRole;
import com.bisa.isb.ws.security.entities.AuthWebServiceOperation;

import java.util.List;

/**
 * @author rsalvatierra on 15/04/2016.
 */
public interface RoleOperationService {

    boolean matchList(List<AuthWebServiceOperation> lista1, List<AuthWebServiceOperation> lista2);

    boolean newRoleOperation(AuthRole role, List<AuthWebServiceOperation> operations, String usuario);

    boolean deleteRoleOperation(AuthRole role, List<AuthWebServiceOperation> operations);
}
