package com.bisa.isb.ws.security.ui;

import bus.database.components.Listado;
import bus.database.components.ListadoDataProvider;
import bus.database.components.ListadoPanel;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.AjaxCheckBoxColumn;
import bus.plumbing.components.IndicatingAjaxToolButton;
import bus.plumbing.components.LinkPropertyColumn;
import bus.plumbing.components.ToolButton;
import com.bisa.isb.ws.security.api.UserIpService;
import com.bisa.isb.ws.security.api.UserRoleService;
import com.bisa.isb.ws.security.api.UserService;
import com.bisa.isb.ws.security.dao.AuthUserDao;
import com.bisa.isb.ws.security.entities.AuthUser;
import com.bisa.isb.ws.security.entities.EnumEnabled;
import com.google.inject.Inject;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author rchura on 23-09-15.
 * @author rsalvatierra modificado on 14/04/2016
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Menu(value = "Usuarios Servicios Web", subMenu = SubMenu.SERVICIO_WEB)
public class ListadoUsers extends Listado<AuthUser, Integer> {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ListadoUsers.class);
    private final IModel<HashSet<AuthUser>> seleccionados;
    @Inject
    UserService userService;
    @Inject
    UserIpService userIpService;
    @Inject
    UserRoleService userRoleService;

    public ListadoUsers() {
        super();
        seleccionados = new Model<>(new HashSet<>());
    }

    @Override
    protected ListadoPanel<AuthUser, Integer> newListadoPanel(String id) {
        return new ListadoPanel<AuthUser, Integer>(id) {
            @Override
            protected List<IColumn<AuthUser, String>> newColumns(ListadoDataProvider<AuthUser, Integer> dataProvider) {
                List<IColumn<AuthUser, String>> columns = new LinkedList<>();
                columns.add(new AjaxCheckBoxColumn<>(seleccionados, dataProvider));
                columns.add(new LinkPropertyColumn<AuthUser>(Model.of("Nombre Usuario"), "userName", "userName") {
                    @Override
                    protected void onClick(AuthUser object) {
                        setResponsePage(EditAuthUser.class, new PageParameters().add("id", object.getId()));
                    }
                });
                columns.add(new PropertyColumn<>(Model.of("Descripci\u00f3n"), "desc", "desc"));
                columns.add(new PropertyColumn<>(Model.of("Disponible"), "enabled", "enabled"));
                columns.add(new PropertyColumn<>(Model.of("e-Mail"), "email"));
                columns.add(new LinkPropertyColumn<AuthUser>(Model.of("Acci\u00f3n"), "textPwd") {
                    @Override
                    protected void onClick(AuthUser object) {
                        setResponsePage(CambiarContrasena.class, new PageParameters().add("id", object.getId()));
                    }
                });
                return columns;
            }

            @Override
            protected Class<? extends Dao<AuthUser, Integer>> getProviderClazz() {
                return AuthUserDao.class;
            }

            @Override
            protected void poblarBotones(RepeatingView repeatingView, String idButton, Form<Void> listform) {
                WebMarkupContainer wmk;
                repeatingView.add(wmk = new WebMarkupContainer(repeatingView.newChildId()));
                wmk.add(new ToolButton(idButton) {

                    @Override
                    public void onSubmit() {
                        setResponsePage(NewAuthUser.class);
                    }
                }.setClassAttribute("btn btn-info")
                        .setIconAttribute("icon-file icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Crear nuevo usuario")));

                repeatingView.add(wmk = new WebMarkupContainer(repeatingView.newChildId()));
                wmk.add(new IndicatingAjaxToolButton(idButton, listform) {

                    @Override
                    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                        target.add(feedbackPanel);
                        target.add(table);
                        Set<AuthUser> usuariosSeleccionados = seleccionados.getObject();
                        if (usuariosSeleccionados == null || usuariosSeleccionados.isEmpty()) {
                            getSession().warn("Nada por hacer");
                            return;
                        }
                        int eliminados = 0;
                        try {
                            for (AuthUser usuario : usuariosSeleccionados) {
                                LOGGER.debug("Eliminando el Usuario: " + usuario.toString());
                                if (EnumEnabled.S.name().equalsIgnoreCase(usuario.getEnabled())) {
                                    LOGGER.error("El usuario [{}] no puede ser eliminado, se encuentra con estado Activo.", usuario.getUserName());
                                    continue;
                                }
                                userRoleService.deleteUserRole(usuario, usuario.getRoles());
                                userIpService.deleteUserIp(usuario);
                                userService.deleteUser(usuario);
                                eliminados++;
                            }
                        } catch (Exception e) {
                            LOGGER.error("Ha ocurrido un error al eliminar un usuario seleccionado.", e);
                        }

                        if (usuariosSeleccionados.size() != eliminados) {
                            getSession().error("Algunos (" + (usuariosSeleccionados.size() - eliminados) + " de " +
                                    (usuariosSeleccionados.size()) +
                                    ") registros de usuarios no pudieron eliminarse. Consulte a soporte t\u00e9cnico.");
                        } else {
                            getSession().info("Operaci\u00f3n realizada correctamente");
                        }
                        seleccionados.setObject(new HashSet<>());
                    }

                    @Override
                    protected void onError(AjaxRequestTarget target, Form<?> form) {
                        target.add(feedbackPanel);
                    }
                }.setClassAttribute("btn btn-danger")
                        .setIconAttribute("icon-remove icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Eliminar usuarios seleccionados")));
            }

        };
    }
}
