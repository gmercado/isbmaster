package com.bisa.isb.ws.security.entities;

import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.IModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rchura on 22-12-15.
 */
public enum EnumEnabled {

    S("Activo"),
    N("Inactivo");

    private String descripcion;

    EnumEnabled(String descripcion) {
        this.descripcion = descripcion;
    }

    // Obtiene un objeto EstadosBoa a partir de un valor entero
    public static EnumEnabled getValor(String valor) {
        List<EnumEnabled> estados = new ArrayList<>(Arrays.asList((EnumEnabled.values())));
        for (EnumEnabled es : estados) {
            if (valor.equalsIgnoreCase(es.name())) {
                return es;
            }
        }
        return null;
    }

    // Obtiene un objeto EstadosBoa a partir de un valor entero
    public static String getDescripcion(String valor) {
        List<EnumEnabled> estados = new ArrayList<>(Arrays.asList((EnumEnabled.values())));
        for (EnumEnabled es : estados) {
            if (valor.equalsIgnoreCase(es.name())) {
                return es.getDescripcion();
            }
        }
        return "<Desconocido>";
    }

    public static List<EnumEnabled> getListEnumEnabled() {
        return new ArrayList<>(Arrays.asList((EnumEnabled.values())));
    }

    public static IChoiceRenderer<EnumEnabled> getChoiceEnumEnabled() {
        return new IChoiceRenderer<EnumEnabled>() {
            @Override
            public Object getDisplayValue(EnumEnabled object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(EnumEnabled object, int index) {
                return Integer.toString(index);
            }

            @Override
            public EnumEnabled getObject(String id, IModel<? extends List<? extends EnumEnabled>> choices) {
                return null;
            }
        };
    }

    public String getDescripcion() {
        return descripcion;
    }

}

