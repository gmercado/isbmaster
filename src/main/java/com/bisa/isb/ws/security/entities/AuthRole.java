package com.bisa.isb.ws.security.entities;

import bus.database.model.EntityBase;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * This entity is related to the SBP93
 *
 * @author Miguel Vega
 * @version $Id: WSAuthRole.java 0, 2015-09-22 10:06 PM mvega $
 */
@Entity
@Table(name = "ISBP83")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S83FECA")),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S83USUA")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S83FECM")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S83USUM"))})
public class AuthRole extends EntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S83ID")
    private Integer id;

    @Column(name = "S83NOM")
    private String name;

    @Column(name = "S83DESC")
    private String desc;

    @Column(name = "S83DISP")
    private String enabled;

    @Transient
    private EnumEnabled tipoEnabled;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(
            name = "ISBP88",
            joinColumns = {@JoinColumn(name = "S88ISBP83", referencedColumnName = "S83ID")},
            inverseJoinColumns = {@JoinColumn(name = "S88ISBP87", referencedColumnName = "S87ID")}
    )
    private List<AuthWebServiceOperation> operations;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(
            name = "ISBP85",
            joinColumns = {@JoinColumn(name = "S85ISBP83", referencedColumnName = "S83ID")},
            inverseJoinColumns = {@JoinColumn(name = "S85ISBP82", referencedColumnName = "S82ID")}
    )
    private List<AuthUser> usuarios;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<AuthWebServiceOperation> getOperations() {
        operations = Optional.fromNullable(operations).or(Lists.newLinkedList());
        return operations;
    }

    public void setOperations(List<AuthWebServiceOperation> operations) {
        this.operations = operations;
    }

    public List<AuthUser> getUsuarios() {
        usuarios = Optional.fromNullable(usuarios).or(Lists.newLinkedList());
        return usuarios;
    }

    public void setUsuarios(List<AuthUser> usuarios) {
        this.usuarios = usuarios;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String descRol) {
        this.desc = descRol;
    }

    public EnumEnabled getTipoEnabled() {
        return tipoEnabled;
    }

    public void setTipoEnabled(EnumEnabled tipoEnabled) {
        this.tipoEnabled = tipoEnabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthRole that = (AuthRole) o;
        return Objects.equals(id, that.id) && !(name != null ? !name.equals(that.name) : that.name != null);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0); // + (authUser != null ? authUser.hashCode() : 0);
        return result;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "AuthRole{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", enabled='" + enabled + '\'' +
                '}';
    }

    public String getNameDesc() {
        return name + " - " + desc;
    }
}
