package com.bisa.isb.ws.security.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.AjaxLinkPropertyColumn;
import bus.plumbing.components.TextPlainCodeModalWindow;
import bus.plumbing.components.XMLCodeModalWindow;
import com.bisa.isb.ws.catalog.Service;
import com.bisa.isb.ws.security.dao.WSPayloadDao;
import com.bisa.isb.ws.security.entities.WSPayload;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.LinkedList;
import java.util.List;

/**
 * @author rsalvatierra on 21/04/2016.
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Menu(value = "Historial de Servicios Web", subMenu = SubMenu.SERVICIO_WEB)
public class HistorialListado extends Listado<WSPayload, Long> {

    @Override
    protected ListadoPanel<WSPayload, Long> newListadoPanel(String id) {
        ModalWindow inpanel = new ModalWindow("inpanel");
        ModalWindow outpanel = new ModalWindow("outpanel");

        inpanel.setTitle("Petici\u00f3n del Servicio");
        inpanel.setWidthUnit("em");
        inpanel.setHeightUnit("em");
        inpanel.setMaskType(ModalWindow.MaskType.SEMI_TRANSPARENT);
        inpanel.setCssClassName(ModalWindow.CSS_CLASS_BLUE);

        outpanel.setTitle("Respuesta del Servicio");
        outpanel.setWidthUnit("em");
        outpanel.setHeightUnit("em");
        outpanel.setMaskType(ModalWindow.MaskType.SEMI_TRANSPARENT);
        outpanel.setCssClassName(ModalWindow.CSS_CLASS_BLUE);

        return new ListadoPanel<WSPayload, Long>(id) {
            @Override
            protected List<IColumn<WSPayload, String>> newColumns(ListadoDataProvider<WSPayload, Long> dataProvider) {
                List<IColumn<WSPayload, String>> columns = new LinkedList<>();
                columns.add(new PropertyColumn<>(Model.of("ID"), "id", "id"));
                columns.add(new PropertyColumn<>(Model.of("Servicio"), "textoServicio"));
                columns.add(new PropertyColumn<>(Model.of("Fecha Registro"), "dateCreated", "formatoDateCreated"));
                columns.add(new AjaxLinkPropertyColumn<WSPayload>(Model.of("Request"), "textoRequest") {
                    @Override
                    protected void onClick(AjaxRequestTarget target, WSPayload object) {
                        if (Service.Type.REST.equals(object.getWsOperation().getWebService().getType())) {
                            inpanel.setContent(new TextPlainCodeModalWindow(inpanel.getContentId(), Model.of(object.getRequestPayload())));
                            inpanel.show(target);
                        } else {
                            inpanel.setContent(new XMLCodeModalWindow(inpanel.getContentId(), Model.of(object.getRequestPayload())));
                            inpanel.show(target);
                        }
                    }
                });
                columns.add(new AjaxLinkPropertyColumn<WSPayload>(Model.of("Response"), "textoResponse") {
                    @Override
                    protected void onClick(AjaxRequestTarget target, WSPayload object) {
                        if (Service.Type.REST.equals(object.getWsOperation().getWebService().getType())) {
                            outpanel.setContent(new TextPlainCodeModalWindow(outpanel.getContentId(), Model.of(object.getResponsePayload())));
                            outpanel.show(target);
                        } else {
                            outpanel.setContent(new XMLCodeModalWindow(outpanel.getContentId(), Model.of(object.getResponsePayload())));
                            outpanel.show(target);
                        }

                    }
                });
                columns.add(new PropertyColumn<>(Model.of("C\u00f3digo HTTP"), "httpStatusCode", "httpStatusCode"));
                columns.add(new PropertyColumn<>(Model.of("Duraci\u00f3n"), "elapsedTime", "elapsedTime"));
                columns.add(new PropertyColumn<>(Model.of("IP Origen"), "ipAddress", "ipAddress"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<WSPayload, Long>> getProviderClazz() {
                return WSPayloadDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<WSPayload> newFiltroPanel(String id, IModel<WSPayload> model1, IModel<WSPayload> model2) {
                return new HistorialWebServices(id, model1, model2, inpanel, outpanel);
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("id", false);
            }


            @Override
            protected WSPayload newObject2() {
                return new WSPayload();
            }

            @Override
            protected WSPayload newObject1() {
                return new WSPayload();
            }
        };

    }
}
