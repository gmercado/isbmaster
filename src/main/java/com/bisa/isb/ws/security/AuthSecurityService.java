package com.bisa.isb.ws.security;

import com.bisa.isb.catalog.Catalog;
import com.bisa.isb.ws.catalog.Service;
import com.bisa.isb.ws.security.entities.AuthUser;
import com.bisa.isb.ws.security.entities.AuthUserIP;
import com.bisa.isb.ws.security.entities.AuthWebService;
import com.bisa.isb.ws.support.AuthenticationException;
import org.apache.http.auth.Credentials;

import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: AuthDao.java 0, 2015-09-22 10:15 PM mvega $
 */
public interface AuthSecurityService {

    /**
     * @param authUser
     * @param password
     * @return
     */
    AuthUser applySecuredPassword(AuthUser authUser, String password);

    /**
     * Retrieves an AuthUser if this exists in database
     *
     * @param credentials
     * @return
     */
    AuthUser getUserByName(Credentials credentials) throws AuthenticationException;

    /**
     * Populates service catalog. Insert new services, update existing services and disable services (todo. if possible).
     * Use the NameSpaceURI and Name attributes to match records.
     *
     * @param serviceCatalog
     */
    void populateCatalog(Catalog<Service> serviceCatalog,Service.Type type);

    List<AuthWebService> getAuthServices();

    /**
     * Fetches all valid IP addresses in database for given {@param authUser}
     *
     * @param authUser
     * @return
     */
    List<AuthUserIP> getAllIPAddresses(AuthUser authUser);
}