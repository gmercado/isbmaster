package com.bisa.isb.ws.security.ui;

import bus.database.components.Listado;
import bus.database.components.ListadoDataProvider;
import bus.database.components.ListadoPanel;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.AjaxCheckBoxColumn;
import bus.plumbing.components.IndicatingAjaxToolButton;
import bus.plumbing.components.LinkPropertyColumn;
import bus.plumbing.components.ToolButton;
import com.bisa.isb.ws.security.api.RoleOperationService;
import com.bisa.isb.ws.security.api.RoleService;
import com.bisa.isb.ws.security.api.UserRoleService;
import com.bisa.isb.ws.security.dao.AuthRoleDao;
import com.bisa.isb.ws.security.entities.AuthRole;
import com.google.inject.Inject;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author rchura on 23-09-15.
 * @author rsalvatierra modificado on 15/04/2016
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Menu(value = "Roles Servicios Web", subMenu = SubMenu.SERVICIO_WEB)
public class ListadoRoles extends Listado<AuthRole, Integer> {

    private final IModel<HashSet<AuthRole>> seleccionados;
    @Inject
    RoleService roleService;
    @Inject
    UserRoleService userRoleService;
    @Inject
    RoleOperationService roleOperationService;

    public ListadoRoles() {
        super();
        seleccionados = new Model<>(new HashSet<>());
    }

    @Override
    protected ListadoPanel<AuthRole, Integer> newListadoPanel(String id) {
        return new ListadoPanel<AuthRole, Integer>(id) {
            @Override
            protected List<IColumn<AuthRole, String>> newColumns(ListadoDataProvider<AuthRole, Integer> dataProvider) {
                List<IColumn<AuthRole, String>> columns = new LinkedList<>();
                columns.add(new AjaxCheckBoxColumn<>(seleccionados, dataProvider));
                columns.add(new LinkPropertyColumn<AuthRole>(Model.of("Nombre Rol"), "name", "name") {
                    @Override
                    protected void onClick(AuthRole object) {
                        setResponsePage(EditAuthRole.class, new PageParameters().add("id", object.getId()));
                    }
                });
                columns.add(new PropertyColumn<>(Model.of("Descripci\u00f3n"), "desc", "desc"));
                columns.add(new PropertyColumn<>(Model.of("Disponible"), "enabled", "enabled"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<AuthRole, Integer>> getProviderClazz() {
                return AuthRoleDao.class;
            }

            @Override
            protected void poblarBotones(RepeatingView repeatingView, String idButton, Form<Void> listform) {
                WebMarkupContainer wmk;
                repeatingView.add(wmk = new WebMarkupContainer(repeatingView.newChildId()));
                wmk.add(new ToolButton(idButton) {
                    @Override
                    public void onSubmit() {
                        setResponsePage(NewAuthRole.class);
                    }
                }.setClassAttribute("btn btn-info")
                        .setIconAttribute("icon-file icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Crear nuevo rol")));


                repeatingView.add(wmk = new WebMarkupContainer(repeatingView.newChildId()));
                wmk.add(new IndicatingAjaxToolButton(idButton, listform) {
                    @Override
                    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                        target.add(feedbackPanel);
                        target.add(table);
                        Set<AuthRole> usuariosSeleccionados = seleccionados.getObject();
                        if (usuariosSeleccionados == null || usuariosSeleccionados.isEmpty()) {
                            getSession().warn("Nada por hacer");
                            return;
                        }
                        LOGGER.debug("REGISTRO SELECCIONADOS:" + seleccionados.toString());
                        int eliminados = 0;
                        int errados = 0;
                        try {
                            for (AuthRole rol : usuariosSeleccionados) {
                                LOGGER.debug("REGISTRO A ELIMINAR:" + rol.toString());
                                roleOperationService.deleteRoleOperation(rol, rol.getOperations());
                                userRoleService.deleteUserRole(rol, rol.getUsuarios());
                                roleService.deleteRole(rol);
                                eliminados++;
                            }
                        } catch (Exception e) {
                            LOGGER.error("Ha ocurrido un error al eliminar a un usuario ", e);
                            errados++;
                        }
                        if (errados != 0) {
                            getSession().error("Algunos (" + errados + " de " +
                                    (eliminados + errados) +
                                    ") registros de roles no pudieron eliminarse. Consule a soporte t\u00eacnico.");
                        } else {
                            getSession().info("Operaci\u00f3n realizada correctamente");
                        }
                        seleccionados.setObject(new HashSet<>());
                    }

                    @Override
                    protected void onError(AjaxRequestTarget target, Form<?> form) {
                        target.add(feedbackPanel);
                    }
                }.setClassAttribute("btn btn-danger")
                        .setIconAttribute("icon-remove icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Eliminar roles seleccionados")));
            }
        };
    }
}
