package com.bisa.isb.ws.security.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.isb.ws.security.api.WebServService;
import com.bisa.isb.ws.security.entities.AuthWebService;
import com.google.inject.Inject;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Collections;

/**
 * @author Roger Chura
 * @version 1.0
 */
public class AuthWebServiceDao extends DaoImpl<AuthWebService, Integer> implements WebServService, Serializable {

    @Inject
    public AuthWebServiceDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Integer> countPath(Root<AuthWebService> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<AuthWebService> p) {
        Path<String> userlogon = p.get("name");
        return Collections.singletonList(userlogon);
    }

    @Override
    public AuthWebService get(int id) {
        return find(id);
    }
}
