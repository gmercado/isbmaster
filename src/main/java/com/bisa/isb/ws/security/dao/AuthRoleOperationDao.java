package com.bisa.isb.ws.security.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.isb.ws.security.api.RoleOperationService;
import com.bisa.isb.ws.security.entities.*;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * @author Roger Chura @version 1.0
 * @author rsalvatierra modificado on 15/04/2016
 */
public class AuthRoleOperationDao extends DaoImpl<AuthRoleOperation, PKRoleOper> implements RoleOperationService, Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthRoleOperationDao.class);

    @Inject
    public AuthRoleOperationDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<PKRoleOper> countPath(Root<AuthRoleOperation> from) {
        return from.get("id.idRole");
    }

    @Override
    public boolean matchList(List<AuthWebServiceOperation> lista1, List<AuthWebServiceOperation> lista2) {
        Collection<AuthWebServiceOperation> different1 = new HashSet<>(lista1);
        Collection<AuthWebServiceOperation> different2 = new HashSet<>(lista2);

        different1.removeAll(lista2);
        different2.removeAll(lista1);

        LOGGER.debug("Lista DIFERENTE 1 ->" + different1.toString());
        LOGGER.debug("Lista DIFERENTE 2 ->" + different2.toString());

        return !(different1.isEmpty() && different2.isEmpty());
    }

    @Override
    public boolean newRoleOperation(AuthRole rol, List<AuthWebServiceOperation> operaciones, String usuario) {
        if (rol == null) {
            LOGGER.error("Error: No se puede identificar el Rol. No se puede registrar la relacion Rol-Operaciones.");
            return false;
        }
        if (operaciones == null) {
            LOGGER.error("Error: No se puede obtener la lista de Operaciones. No se puede registrar la relacion Rol-Operaciones.");
            return false;
        }

        int contador = 0;
        for (AuthWebServiceOperation wsOperation : operaciones) {
            AuthRoleOperation rolOp = new AuthRoleOperation();
            PKRoleOper pkRolOP = new PKRoleOper();
            try {
                pkRolOP.setIdRole(rol.getId());
                pkRolOP.setIdOper(wsOperation.getId());
                rolOp.setId(pkRolOP);
                rolOp.setEnabled(EnumEnabled.S.name());
                rolOp.setFechaCreacion(new Date());
                rolOp.setUsuarioCreador(usuario);
                rolOp.setFechaModificacion(new Date());
                rolOp.setUsuarioModificador(usuario);
                merge(rolOp);
                contador++;
            } catch (Exception e) {
                LOGGER.error("Error <Exception>: No se puede insertar el registro <AuthRoleOperation> con ID [" + pkRolOP.toString() + "].", e);
                return false;
            }
        }
        LOGGER.debug("Se insertaron {} registro(s) de tipo <AuthRoleOperation>.", contador);
        return true;
    }

    @Override
    public boolean deleteRoleOperation(AuthRole rol, List<AuthWebServiceOperation> operaciones) {

        if (rol == null) {
            LOGGER.error("Error: No se puede identificar el Rol. No se puede eliminar la relacion Rol-Operaciones.");
            return false;
        }
        if (operaciones == null) {
            LOGGER.error("Error: No se puede obtener la lista de Operaciones. No se puede eliminar la relacion Rol-Operaciones.");
            return false;
        }

        int contador = 0;
        for (AuthWebServiceOperation wsOperation : operaciones) {
            PKRoleOper pkRolOP = new PKRoleOper();
            try {
                pkRolOP.setIdRole(rol.getId());
                pkRolOP.setIdOper(wsOperation.getId());
                AuthRoleOperation rolOp = find(pkRolOP);
                if (rolOp != null) {
                    remove(rolOp.getId());
                    contador++;
                }
            } catch (Exception e) {
                LOGGER.error("Error <Exception>: No se puede eliminar el registro <AuthRoleOperation> con ID [" + pkRolOP.toString() + "].", e);
                return false;
            }
        }
        LOGGER.debug("Se eliminaron {} registro(s) de tipo <AuthRoleOperation>.", contador);
        return true;
    }
}
