/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


package com.bisa.isb.ws.security.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.users.api.Metadatas;
import com.bisa.isb.ws.security.api.RoleService;
import com.bisa.isb.ws.security.api.WSOperationService;
import com.bisa.isb.ws.security.entities.AuthRole;
import com.bisa.isb.ws.security.entities.AuthWebServiceOperation;
import com.google.inject.Inject;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.validator.StringValidator;

import java.util.List;

/**
 * @author Roger Chura @version 1.0
 * @author rsalvatierra modificado on 15/04/2016
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Titulo("Crear nuevo rol para Servicio Web")
public class NewAuthRole extends BisaWebPage {

    @Inject
    RoleService roleService;
    @Inject
    WSOperationService wsOperationService;
    private IModel<AuthRole> userModel;

    /**
     * @param parameters puede contener un id del cual copiar.
     */
    public NewAuthRole(PageParameters parameters) {
        super(parameters);
        AuthRole authRole = new AuthRole();
        userModel = Model.of(authRole);
        Form<AuthRole> nuevoRol;
        add(nuevoRol = new Form<>("newForm", new CompoundPropertyModel<>(userModel)));

        nuevoRol.add(new RequiredTextField<String>("name").
                add(StringValidator.maximumLength(30)).
                setOutputMarkupId(true));

        nuevoRol.add(new RequiredTextField<String>("desc").
                add(StringValidator.maximumLength(50)).
                setOutputMarkupId(true));

        CheckBoxMultipleChoice<AuthWebServiceOperation> cbmc = new CheckBoxMultipleChoice<>("operations",
                new AbstractReadOnlyModel<List<AuthWebServiceOperation>>() {
                    @Override
                    public List<AuthWebServiceOperation> getObject() {
                        return wsOperationService.getOperations();
                    }
                }, new IChoiceRenderer<AuthWebServiceOperation>() {
            @Override
            public Object getDisplayValue(AuthWebServiceOperation authWebServiceOperation) {
                return authWebServiceOperation.getDescOperacionWS();
            }

            @Override
            public String getIdValue(AuthWebServiceOperation authWebServiceOperation, int i) {
                return Integer.toString(i);
            }

            @Override
            public AuthWebServiceOperation getObject(String s, IModel<? extends List<? extends AuthWebServiceOperation>> iModel) {
                return null;
            }
        });

        cbmc.setRequired(true).setOutputMarkupId(true);
        nuevoRol.add(cbmc);

        nuevoRol.add(new Button("ok", Model.of("Guardar")) {

            @Override
            public void onSubmit() {
                try {
                    String usuario = getSession().getMetaData(Metadatas.USER_META_DATA_KEY).getUserlogon();
                    LOGGER.debug("INICIA {}" + getUserModel());
                    AuthRole modelObject = (AuthRole) getForm().getModelObject();
                    LOGGER.debug("OPERACIONES SELECCIONADAS  ->> " + modelObject.getOperations().size());
                    LOGGER.debug("OPERACIONES SELECCIONADAS  ->> " + modelObject.getOperations());
                    AuthRole newRole = roleService.newRole(modelObject, modelObject.getOperations(), usuario);
                    if (newRole != null) {
                        getSession().info("Rol creado satisfactoriamente");
                    } else {
                        getSession().info("No se creo el Rol ");
                    }
                    setResponsePage(ListadoRoles.class);
                    LOGGER.debug("FINALIZA");
                } catch (Exception e) {
                    getSession().error("Ha ocurrido un error inesperado, comun\u00edquese con soporte t\u00e9cnico");
                    LOGGER.error("Ha ocurrido un error inesperado al crear el registro de nuevo rol", e);
                }
            }
        });

        nuevoRol.add(new Button("cancelar", Model.of("Cancelar")) {
            @Override
            public void onSubmit() {
                setResponsePage(ListadoRoles.class);
                getSession().info("Operaci\u00f3n cancelada, no se ha creado un nuevo rol");
            }
        }.setDefaultFormProcessing(false));

    }

    public IModel<AuthRole> getUserModel() {
        return userModel;
    }

}
