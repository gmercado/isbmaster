package com.bisa.isb.ws.security.api;

import com.bisa.isb.ws.security.entities.AuthRole;
import com.bisa.isb.ws.security.entities.AuthUser;

import java.util.Set;

/**
 * @author rsalvatierra on 14/04/2016.
 */
public interface UserService {

    AuthUser getUser(int id);

    AuthUser newUser(AuthUser data, Set<AuthRole> rol, String username, String pwdDefault);

    void updateUser(AuthUser data, String usuario);

    void deleteUser(AuthUser data);

}
