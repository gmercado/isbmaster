package com.bisa.isb.ws.security.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.EMailValidator;
import bus.plumbing.components.IPAddressValidator;
import bus.users.api.Metadatas;
import com.bisa.isb.ws.security.api.RoleService;
import com.bisa.isb.ws.security.api.UserIpService;
import com.bisa.isb.ws.security.api.UserRoleService;
import com.bisa.isb.ws.security.api.UserService;
import com.bisa.isb.ws.security.entities.AuthRole;
import com.bisa.isb.ws.security.entities.AuthUser;
import com.bisa.isb.ws.security.entities.EnumEnabled;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.*;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.validator.StringValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author rchura on 22-12-15.
 * @author rsalvatierra modificado on 14/04/2016
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Titulo("Editar usuario de Servicio Web")
public class EditAuthUser extends BisaWebPage {

    protected static final Logger LOGGER = LoggerFactory.getLogger(EditAuthUser.class);
    @Inject
    UserService userService;
    @Inject
    UserRoleService userRoleService;
    @Inject
    UserIpService userIpService;
    @Inject
    RoleService roleService;
    private IModel<AuthUser> userModel;

    public EditAuthUser(PageParameters parameters) {
        super(parameters);

        AuthUser user = userService.getUser(getPageParameters().get("id").toInteger());
        user.setTipoEnabled(EnumEnabled.getValor(user.getEnabled()));
        userModel = Model.of(user);
        Form<?> formEdit = new Form<>("editUser", new CompoundPropertyModel<>(userModel));
        add(formEdit);
        formEdit.add(new Label("userName"));
        formEdit.add(new Label("desc"));

        formEdit.add(new RequiredTextField<String>("email", new PropertyModel<>(userModel, "email")).
                add(new EMailValidator()).
                add(StringValidator.maximumLength(50)).
                setOutputMarkupId(true));

        final DropDownChoice<EnumEnabled> dropDownChoice = new DropDownChoice<>("enabled", new PropertyModel<>(userModel, "tipoEnabled"),
                EnumEnabled.getListEnumEnabled(), EnumEnabled.getChoiceEnumEnabled());
        formEdit.add(dropDownChoice);

        CheckBoxMultipleChoice<AuthRole> cbmc = new CheckBoxMultipleChoice<>("roles", new PropertyModel<>(userModel, "roles"), new AbstractReadOnlyModel<List<AuthRole>>() {
            @Override
            public List<AuthRole> getObject() {
                return roleService.getRoles();
            }
        });
        cbmc.setChoiceRenderer(new ChoiceRenderer<>("nameDesc", "id"));
        cbmc.setRequired(true).setOutputMarkupId(true);

        formEdit.add(cbmc);

        final ListView<String> listView = new ListView<String>("ips", new PropertyModel<>(userModel, "ips")) {
            @Override
            protected void populateItem(final ListItem<String> item) {
                PropertyModel<String> propertyModel =
                        new PropertyModel<>(userModel, "ips[" + item.getIndex() + "]");
                RequiredTextField<String> iptf = new RequiredTextField<>("ip", propertyModel);
                iptf.setType(String.class);
                iptf.add(new IPAddressValidator());
                item.add(iptf);
                item.add(removeLink("eliminar", item));
            }
        };
        listView.setReuseItems(true);

        formEdit.add(listView);

        formEdit.add(new Link<ListView<String>>("mas", new Model<>(listView)) {
            @Override
            public void onClick() {
                ListView<String> lv = getModel().getObject();
                lv.modelChanging();
                lv.getList().add("[digite un nuevo IP]");
                lv.modelChanged();
                lv.removeAll();
            }
        });

        formEdit.add(new Button("guardar", Model.of("Guardar")) {

            @Override
            public void onSubmit() {
                try {
                    String usuario = getSession().getMetaData(Metadatas.USER_META_DATA_KEY).getUserlogon();
                    AuthUser userOrigin = userService.getUser(getPageParameters().get("id").toInteger());
                    AuthUser user = userModel.getObject();
                    List<AuthRole> roles = user.getRoles();
                    List<String> ips = user.getIps();
                    if (roles == null || roles.isEmpty()) {
                        getSession().error("Debe seleccionar al menos un Rol para el usuario.");
                        return;
                    }
                    if (ips == null || ips.isEmpty()) {
                        getSession().error("Debe ingresar al menos un IP para el usuario.");
                        return;
                    }
                    boolean actualizar = false;
                    //Roles
                    if (userRoleService.matchList(userOrigin.getRoles(), roles)) {
                        // Elimnando la relacion inicial
                        userRoleService.deleteUserRole(userOrigin, userOrigin.getRoles());
                        // Registrando el nuevo conjunto seleccionado
                        userRoleService.newUserRole(userOrigin, roles, usuario);
                        actualizar = true;
                    }
                    //IPS
                    if (userIpService.matchList(userOrigin.getIps(), ips)) {
                        // Elimnando la relacion inicial
                        userIpService.deleteUserIp(userOrigin);
                        // Registrando el nuevo conjunto seleccionado
                        userIpService.newUserIp(userOrigin, ips, usuario);
                        actualizar = true;
                    }
                    //Estado
                    String input = dropDownChoice.getInput(); //Obtiene el indice del valor seleccionado en pantalla.
                    LOGGER.debug("dropDownChoice.getInput()-->" + input);
                    if (input == null) {
                        input = dropDownChoice.getValue(); // Obtiene el valor enviado o el valor inicializado en la pantalla (indice de la lista).
                    }
                    final List<EnumEnabled> strings = EnumEnabled.getListEnumEnabled();
                    if (StringUtils.trimToNull(input) != null) {
                        int index = Integer.parseInt(input);
                        input = index >= 0 ? strings.get(index).name() : strings.get(0).name();
                        LOGGER.debug("index>=0?strings.get(index).name()-->" + input);
                    } else {
                        input = strings.get(0).name();
                        LOGGER.debug("strings.get(0).name()-->" + input);
                    }
                    if (!(userOrigin.getEnabled().equalsIgnoreCase(input))) {
                        actualizar = true;
                    }
                    //Email
                    if (!(userOrigin.getEmail().equalsIgnoreCase(user.getEmail()))) {
                        actualizar = true;
                    }
                    //Actualizar
                    if (actualizar) {
                        userOrigin.setEnabled(input);
                        userOrigin.setEmail(user.getEmail());
                        userService.updateUser(userOrigin, usuario);
                        getSession().info("Usuario actualizado satisfactoriamente.");
                    } else {
                        getSession().warn("No se actualiz\u00f3 el usuario.");
                    }
                    setResponsePage(ListadoUsers.class);
                } catch (Exception e) {
                    getSession().error("Ha ocurrido un error inesperado, comun\u00edquese con soporte t\u00e9cnico.");
                    LOGGER.error("Ha ocurrido un error inesperado al actualizar el registro del usuario.", e);
                }
            }
        });

        formEdit.add(new Button("cancelar", Model.of("Cancelar")) {
            @Override
            public void onSubmit() {
                setResponsePage(ListadoUsers.class);
                getSession().info("Operaci\u00f3n cancelada, no se ha modificado el registro del usuario.");
            }
        }.setDefaultFormProcessing(false));
    }
}





