package com.bisa.isb.ws.security.api;

import com.bisa.isb.ws.security.entities.AuthWebService;

/**
 * @author rsalvatierra on 19/04/2016.
 */
public interface WebServService {

    AuthWebService get(int id);
}
