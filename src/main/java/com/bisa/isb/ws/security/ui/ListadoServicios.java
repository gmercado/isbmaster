package com.bisa.isb.ws.security.ui;

import bus.database.components.Listado;
import bus.database.components.ListadoDataProvider;
import bus.database.components.ListadoPanel;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;
import com.bisa.isb.ws.security.dao.AuthWebServiceDao;
import com.bisa.isb.ws.security.entities.AuthWebService;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * @author rchura on 23-09-15.
 * @author rsalvatierra modificado on 14/04/2016
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Menu(value = "Servicios Web", subMenu = SubMenu.SERVICIO_WEB)
public class ListadoServicios extends Listado<AuthWebService, Integer> {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ListadoServicios.class);

    public ListadoServicios() {
        super();
    }

    @Override
    protected ListadoPanel<AuthWebService, Integer> newListadoPanel(String id) {
        return new ListadoPanel<AuthWebService, Integer>(id) {
            @Override
            protected List<IColumn<AuthWebService, String>> newColumns(ListadoDataProvider<AuthWebService, Integer> dataProvider) {
                List<IColumn<AuthWebService, String>> columns = new LinkedList<>();
                columns.add(new LinkPropertyColumn<AuthWebService>(Model.of("Nombre Servicio"), "name", "name") {
                    @Override
                    protected void onClick(AuthWebService object) {
                        setResponsePage(ListadoOperaciones.class, new PageParameters().add("id", object.getId()));
                    }
                });
                columns.add(new PropertyColumn<>(Model.of("Descripci\u00f3n"), "description", "description"));
                columns.add(new PropertyColumn<>(Model.of("Tipo"), "type.name"));
                columns.add(new PropertyColumn<>(Model.of("Disponible"), "enabled", "enabled"));
                columns.add(new PropertyColumn<>(Model.of("NameSpace"), "nameSpace", "nameSpace"));
                columns.add(new PropertyColumn<>(Model.of("EndPoint"), "endpoint"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<AuthWebService, Integer>> getProviderClazz() {
                return AuthWebServiceDao.class;
            }

        };
    }
}
