package com.bisa.isb.ws.security.entities;

import bus.database.model.EntityBase;
import com.bisa.isb.ws.catalog.Service;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: AuthWebService.java 0, 2015-09-22 10:07 PM mvega $
 */
@Entity
@Table(name = "ISBP86")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S86FECA", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S86USUA")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S86FECM", nullable = false)),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S86USUM"))}
)
public class AuthWebService extends EntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S86ID")
    private Integer id;

    @Column(name = "S86NS")
    private String nameSpace;

    @Column(name = "S86TIPO")
    @Enumerated(EnumType.STRING)
    private Service.Type type;

    @Column(name = "S86DISP")
    private String enabled;

    @Column(name = "S86NOM")
    private String name;

    @Column(name = "S86ENDPNT")
    private String endpoint;

    @Column(name = "S86JCLA")
    private String javaTypeName;

    @Column(name = "S86DESC")
    private String description;

    /*@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(
            name = "ISBP87",
            joinColumns = {@JoinColumn(name = "S87ISBP86", referencedColumnName = "S86ID")}
    )*/
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "webService", cascade = {CascadeType.ALL})
    private List<AuthWebServiceOperation> operations;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public void setNameSpace(String namespace) {
        this.nameSpace = namespace;
    }

    public Service.Type getType() {
        return type;
    }

    public void setType(Service.Type type) {
        this.type = type;
    }


    public List<AuthWebServiceOperation> getOperations() {
        operations = Optional.fromNullable(operations).or(Lists.newLinkedList());
        return operations;
    }

    public void setOperations(List<AuthWebServiceOperation> operations) {
        this.operations = operations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getJavaTypeName() {
        return javaTypeName;
    }

    public void setJavaTypeName(String javaTypeName) {
        this.javaTypeName = javaTypeName;
    }


    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthWebService that = (AuthWebService) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (nameSpace != null ? !nameSpace.equals(that.nameSpace) : that.nameSpace != null) return false;
        if (endpoint != null ? !endpoint.equals(that.endpoint) : that.endpoint != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (nameSpace != null ? nameSpace.hashCode() : 0);
        result = 31 * result + (endpoint != null ? endpoint.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AuthWebService{");
        sb.append("id=").append(id);
        sb.append(", nameSpace='").append(nameSpace).append('\'');
        sb.append(", type=").append(type);
        sb.append(", enabled='").append(enabled).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", endpoint='").append(endpoint).append('\'');
        sb.append(", javaTypeName='").append(javaTypeName).append('\'');
        //sb.append(", operations=").append(operations);
        sb.append('}');
        return sb.toString();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
