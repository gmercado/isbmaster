package com.bisa.isb.ws.security.ui;

import bus.env.api.MedioAmbiente;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.EMailValidator;
import bus.users.api.Metadatas;
import com.bisa.isb.ws.security.api.RoleService;
import com.bisa.isb.ws.security.api.UserService;
import com.bisa.isb.ws.security.entities.AuthRole;
import com.bisa.isb.ws.security.entities.AuthUser;
import com.google.inject.Inject;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.model.*;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.validator.StringValidator;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static bus.env.api.Variables.WEBSERVICE_USERPWD;
import static bus.env.api.Variables.WEBSERVICE_USERPWD_DEFAULT;

/**
 * @author Roger Chura @version 1.0
 * @author rsalvatierra modificado on 14/04/2016
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Titulo("Crear nuevo usuario Servicio Web")
public class NewAuthUser extends BisaWebPage {

    @Inject
    UserService userService;

    @Inject
    RoleService roleService;

    @Inject
    MedioAmbiente medioAmbiente;

    private IModel<AuthUser> userModel;

    private Set<AuthRole> roles;

    /**
     * @param parameters puede contener un id del cual copiar.
     */
    public NewAuthUser(PageParameters parameters) {
        super(parameters);

        userModel = Model.of(new AuthUser());
        Form<AuthUser> nuevoUsuario;
        add(nuevoUsuario = new Form<>("newForm", new CompoundPropertyModel<>(userModel)));

        nuevoUsuario.add(new RequiredTextField<String>("userName").
                add(StringValidator.maximumLength(30)).
                setOutputMarkupId(true));

        nuevoUsuario.add(new RequiredTextField<String>("desc").
                add(StringValidator.maximumLength(50)).
                setOutputMarkupId(true));

        nuevoUsuario.add(new RequiredTextField<String>("email").
                add(new EMailValidator()).
                add(StringValidator.maximumLength(50)).
                setOutputMarkupId(true));

        final IModel<List<AuthRole>> choisesAuthRole = new AbstractReadOnlyModel<List<AuthRole>>() {
            @Override
            public List<AuthRole> getObject() {
                return roleService.getRoles();
            }
        };

        CheckBoxMultipleChoice<AuthRole> cbmc = new CheckBoxMultipleChoice<>("roles",
                new PropertyModel<>(this, "roles"), choisesAuthRole, new IChoiceRenderer<AuthRole>() {

            @Override
            public Object getDisplayValue(AuthRole authRole) {
                return authRole.getNameDesc();
            }

            @Override
            public String getIdValue(AuthRole authRole, int i) {
                return Integer.toString(i);
            }

            @Override
            public AuthRole getObject(String s, IModel<? extends List<? extends AuthRole>> iModel) {
                return null;
            }
        });

        cbmc.setRequired(true).setOutputMarkupId(true);
        nuevoUsuario.add(cbmc);

        nuevoUsuario.add(new Button("ok", Model.of("Guardar")) {
            @Override
            public void onSubmit() {
                try {
                    LOGGER.debug("Inicio de registro de usuario {}", getUserModel());
                    String usuario = getSession().getMetaData(Metadatas.USER_META_DATA_KEY).getUserlogon();
                    String pwd = medioAmbiente.getValorDe(WEBSERVICE_USERPWD, WEBSERVICE_USERPWD_DEFAULT);
                    AuthUser user = userService.newUser((AuthUser) getForm().getModelObject(), roles, usuario, pwd);
                    if (user != null) {
                        getSession().info("Usuario creado satisfactoriamente");
                        setResponsePage(ListadoUsers.class);
                    } else {
                        throw new Exception();
                    }
                    LOGGER.debug("Finaliza registro de usuario");
                } catch (Exception e) {
                    getSession().error("Ha ocurrido un error inesperado, comun�quese con soporte t�cnico");
                    LOGGER.error("Ha ocurrido un error inesperado al crear el registro de nuevo usuario", e);
                }
            }
        });

        nuevoUsuario.add(new Button("cancelar", Model.of("Cancelar")) {
            @Override
            public void onSubmit() {
                setResponsePage(ListadoUsers.class);
                getSession().info("Operaci\u00f3n cancelada, no se ha creado un nuevo usuario de servicio web");
            }
        }.setDefaultFormProcessing(false));

    }

    public IModel<AuthUser> getUserModel() {
        return userModel;
    }

    public Set<AuthRole> getRoles() {
        if (roles == null) {
            roles = new LinkedHashSet<>();
        }
        return roles;
    }

    public void setRoles(Set<AuthRole> roles) {
        this.roles = roles;
    }
}
