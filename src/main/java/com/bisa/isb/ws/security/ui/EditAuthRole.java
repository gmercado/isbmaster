package com.bisa.isb.ws.security.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.users.api.Metadatas;
import com.bisa.isb.ws.security.api.RoleOperationService;
import com.bisa.isb.ws.security.api.RoleService;
import com.bisa.isb.ws.security.api.WSOperationService;
import com.bisa.isb.ws.security.entities.AuthRole;
import com.bisa.isb.ws.security.entities.AuthWebServiceOperation;
import com.bisa.isb.ws.security.entities.EnumEnabled;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.model.*;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author rchura on 22-12-15.
 * @author rsalvatierra modificado on 15/04/2016
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Titulo("Editar rol para Servicio Web")
public class EditAuthRole extends BisaWebPage {

    protected static final Logger LOGGER = LoggerFactory.getLogger(EditAuthRole.class);
    @Inject
    RoleService roleService;
    @Inject
    WSOperationService wsOperationService;
    @Inject
    RoleOperationService roleOperationService;

    private IModel<AuthRole> userModel;

    public EditAuthRole(PageParameters parameters) {
        super(parameters);
        AuthRole role = roleService.getRole(getPageParameters().get("id").toInteger());
        role.setTipoEnabled(EnumEnabled.getValor(role.getEnabled()));
        userModel = Model.of(role);
        Form<?> formEdit = new Form<>("editRol", new CompoundPropertyModel<>(userModel));
        add(formEdit);
        formEdit.add(new Label("name"));
        formEdit.add(new Label("desc"));

        final DropDownChoice<EnumEnabled> dropDownChoice = new DropDownChoice<>("enabled", new PropertyModel<>(userModel, "tipoEnabled"),
                EnumEnabled.getListEnumEnabled(), EnumEnabled.getChoiceEnumEnabled());
        formEdit.add(dropDownChoice);

        CheckBoxMultipleChoice<AuthWebServiceOperation> cbmc = new CheckBoxMultipleChoice<>("operaciones",
                new PropertyModel<>(userModel, "operations"), new AbstractReadOnlyModel<List<AuthWebServiceOperation>>() {
            @Override
            public List<AuthWebServiceOperation> getObject() {
                return wsOperationService.getOperations();
            }
        });
        cbmc.setChoiceRenderer(new ChoiceRenderer<>("descOperacionWS", "id"));
        cbmc.setRequired(true).setOutputMarkupId(true);
        formEdit.add(cbmc);

        formEdit.add(new Button("guardar", Model.of("Guardar")) {

            @Override
            public void onSubmit() {
                try {
                    String usuario = getSession().getMetaData(Metadatas.USER_META_DATA_KEY).getUserlogon();
                    AuthRole rolOrigin = roleService.getRole(getPageParameters().get("id").toInteger());
                    AuthRole rolModel = userModel.getObject();
                    List<AuthWebServiceOperation> operaciones = rolModel.getOperations();
                    if (operaciones == null || operaciones.isEmpty()) {
                        getSession().error("Debe seleccionar al menos una Operación del Servicio Web.");
                        return;
                    }
                    boolean actualizar = false;

                    LOGGER.debug("operaciones SELECCIONADAS-->" + operaciones.toString());
                    LOGGER.debug("BASE DATOS -->" + rolOrigin.getOperations());

                    if (roleOperationService.matchList(rolOrigin.getOperations(), operaciones)) {
                        // Elimnando la relacion inicial
                        roleOperationService.deleteRoleOperation(rolOrigin, rolOrigin.getOperations());
                        // Registrando el nuevo conjunto seleccionado
                        roleOperationService.newRoleOperation(rolOrigin, operaciones, usuario);
                        actualizar = true;
                    }
                    String input = dropDownChoice.getInput(); //Obtiene el indice del valor seleccionado en pantalla.
                    if (input == null) {
                        input = dropDownChoice.getValue(); // Obtiene el valor enviado o el valor inicializado en la pantalla (indice de la lista).
                    }
                    final List<EnumEnabled> strings = EnumEnabled.getListEnumEnabled();
                    if (StringUtils.trimToNull(input) != null) {
                        int index = Integer.parseInt(input);
                        input = index >= 0 ? strings.get(index).name() : strings.get(0).name();
                        LOGGER.debug("index>=0?strings.get(index).name()-->" + input);
                    } else {
                        input = strings.get(0).name();
                        LOGGER.debug("strings.get(0).name()-->" + input);
                    }
                    if (!(rolOrigin.getEnabled().equalsIgnoreCase(input))) {
                        actualizar = true;
                    }
                    if (actualizar) {
                        rolOrigin.setEnabled(input);
                        roleService.updateRole(rolOrigin, usuario);
                        getSession().info("Rol actualizado satisfactoriamente.");
                    } else {
                        getSession().warn("No se actualiz\u00f3 el Rol.");
                    }
                    setResponsePage(ListadoRoles.class);
                } catch (Exception e) {
                    getSession().error("Ha ocurrido un error inesperado, comun\u00edquese con soporte t\u00e9cnico");
                    LOGGER.error("Ha ocurrido un error inesperado al actualizar el registro de nuevo rol", e);
                }
            }
        });

        formEdit.add(new Button("cancelar", Model.of("Cancelar")) {
            @Override
            public void onSubmit() {
                setResponsePage(ListadoRoles.class);
                getSession().info("Operaci\u00f3n cancelada, no se ha modificado el registro de rol.");
            }
        }.setDefaultFormProcessing(false));
    }
}





