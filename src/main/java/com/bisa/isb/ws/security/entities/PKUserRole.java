package com.bisa.isb.ws.security.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by rchura on 24-09-15.
 */
@Embeddable
public class PKUserRole implements Serializable {

    @Column(name = "S85ISBP82")
    private Long idUser;

    @Column(name = "S85ISBP83")
    private Integer idRol;

    public PKUserRole() {
    }

    public PKUserRole(Long idUser, Integer idRol) {
        this.idUser = idUser;
        this.idRol = idRol;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PKUserRole that = (PKUserRole) o;

        if (idUser != that.idUser) return false;
        if (idRol != null ? !idRol.equals(that.idRol) : that.idRol != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idUser.intValue();
        result = 31 * result + (idUser != null ? idUser.hashCode() : 0);
        result = 31 * result + (idRol != null ? idRol.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PKUserRole{");
        sb.append("idUser=").append(idUser);
        sb.append(", idRol=").append(idRol);
        sb.append('}');
        return sb.toString();
    }
}
