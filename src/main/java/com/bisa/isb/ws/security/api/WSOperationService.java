package com.bisa.isb.ws.security.api;

import com.bisa.isb.ws.security.entities.AuthWebService;
import com.bisa.isb.ws.security.entities.AuthWebServiceOperation;

import java.util.List;

/**
 * @author rsalvatierra on 15/04/2016.
 */
public interface WSOperationService {
    List<AuthWebServiceOperation> getOperations();

    List<AuthWebServiceOperation> getOperations(AuthWebService id);
}
