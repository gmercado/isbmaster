package com.bisa.isb.ws.support;

import com.bisa.isb.api.GenericException;

/**
 * @author Miguel Vega
 * @version $Id: AuthenticationExeption.java 0, 2015-09-17 11:37 PM mvega $
 */
public class AuthorizationException extends GenericException {

    public AuthorizationException(String message, Throwable cause, Object... params) {
        super(message, cause, params);
    }

    public AuthorizationException(String message, Object... params) {
        super(message, params);
    }
}
