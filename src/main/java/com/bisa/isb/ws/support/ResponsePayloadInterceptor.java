package com.bisa.isb.ws.support;

import com.bisa.isb.ws.security.AuthSecurityService;
import com.bisa.isb.ws.security.dao.WSPayloadDao;
import com.bisa.isb.ws.security.entities.WSPayload;
import com.google.inject.Inject;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.LoggingMessage;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Miguel Vega
 * @version $Id: MessageExchangeInterceptor.java; oct 06, 2015 03:37 PM mvega $
 */
public class ResponsePayloadInterceptor extends LoggingOutInterceptor {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final AuthSecurityService authSecurityService;

    private final WSPayloadDao wsPayloadDao;

    private final AtomicReference<Message> messageWrapper = new AtomicReference();

    @Inject
    public ResponsePayloadInterceptor(AuthSecurityService authSecurityService, WSPayloadDao wsPayloadDao) {
        super();

        this.authSecurityService = authSecurityService;
        this.wsPayloadDao = wsPayloadDao;
    }

    /**
     * Receive the message and fetch the source handlers to persist metadata about payload
     *
     * @param message
     * @throws Fault
     */
    @Override
    public void handleMessage(Message message) throws Fault {
        messageWrapper.set(message);
        super.handleMessage(message);
    }

    @Override
    protected String formatLoggingMessage(LoggingMessage loggingMessage) {

        String inSOAP = loggingMessage.getPayload().toString();

        Message message = messageWrapper.get();

        logger.debug(" OUT < ****************************************");
        logger.debug(inSOAP);
        logger.debug(" OUT > ****************************************");

        final HttpServletResponse httpResponse = (HttpServletResponse) message.get(AbstractHTTPDestination.HTTP_RESPONSE);

        WSPayload wsPayload = (WSPayload) message.getExchange().get(WSPayloadDao.TRANSPORTED_PAYLOAD_ID);

        if (wsPayload != null) {
            wsPayload.setHttpStatusCode(httpResponse.getStatus());
            wsPayload.setResponsePayload(loggingMessage.getPayload().toString());

            AtomicLong now = new AtomicLong(System.currentTimeMillis());
            wsPayload.setElapsedTime(now.addAndGet(-1 * wsPayload.getElapsedTime()));

            wsPayloadDao.merge(wsPayload);
        }
        return super.formatLoggingMessage(loggingMessage);
    }
}
