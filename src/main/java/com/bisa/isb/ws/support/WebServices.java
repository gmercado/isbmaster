package com.bisa.isb.ws.support;

import org.apache.cxf.message.Message;
import org.apache.cxf.service.invoker.MethodDispatcher;
import org.apache.cxf.service.model.BindingOperationInfo;

import java.lang.reflect.Method;

/**
 * @author Miguel Vega
 * @version $Id: JAXWebServices.java; oct 30, 2015 04:13 PM mvega $
 */
public class WebServices {
    public static Method getBoundMethod(Message m) throws IllegalStateException {
        BindingOperationInfo bop = m.getExchange().get(BindingOperationInfo.class);
        if (bop != null) {
            MethodDispatcher md = (MethodDispatcher)
                    m.getExchange().get(org.apache.cxf.service.Service.class).get(MethodDispatcher.class.getName());
            return md.getMethod(bop);
        }
        Method method = (Method) m.get("org.apache.cxf.resource.method");
        if (method != null) {
            return method;
        }

        throw new IllegalStateException("Method is not available : Unauthorized");
    }
}
