package com.bisa.isb.ws.support;

import com.bisa.isb.api.AKA;
import com.google.common.base.Splitter;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.auth.Credentials;

import java.security.Principal;
import java.util.Iterator;

/**
 * All infomation about header AUTHORIZATION must have been validated before this class starts evaluating.
 * <br/>
 * Handle authorization like the following snippet:
 * <p>
 * GET /securefiles/ HTTP/1.1
 * Host: www.httpwatch.com
 * Authorization: Basic aHR0cHdhdGNoOmY=
 * </p>
 *
 * @author Miguel Vega
 * @version $Id: BasicHttpAuthentication.java; sep 17, 2015 03:47 PM mvega $
 */
@AKA("BASIC")
public class BasicHttpAuthentication<E> extends HttpAuthentication<E> {

    BasicHttpAuthentication(String httpAuthHeader) {
        super(httpAuthHeader);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Uses the HTTP heaer AUthorization to determine access for request.
     *
     * @return
     * @throws AuthenticationException
     */
    @Override
    public E authenticate(CredentialsEvaluator<E> credentialsEvaluator) throws AuthenticationException {
        // Get encoded user and password, comes after "BASIC "
        String userpassEncoded = httpAuthHeader.substring(6);
        // Decode it, using any base 64 decoder
        String userpassDecoded = new String(Base64.decodeBase64(userpassEncoded));
        // Check our user list to see if that user and password are "allowed"

        Iterator<String> splitted = Splitter.on(":").split(userpassDecoded).iterator();

        final String name = splitted.next();
        final String password = splitted.next();

        Credentials credentials = new Credentials() {
            @Override
            public Principal getUserPrincipal() {
                return () -> name;
            }

            @Override
            public String getPassword() {
                return password;
            }
        };

        return credentialsEvaluator.evaluate(credentials);
    }
}
