package com.bisa.isb.ws.support;

import com.bisa.isb.api.AKA;
import com.bisa.isb.api.Texts;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import org.apache.commons.collections.keyvalue.UnmodifiableMapEntry;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.bisa.isb.api.Texts.build;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

/**
 * @author Miguel Vega
 * @version $Id: HttpAuthentication.java 0, 2015-09-17 10:17 PM mvega $
 */
public abstract class HttpAuthentication <E> implements Authentication<CredentialsEvaluator<E>, E> {

    final static Map<String, Class<? extends Authentication>> availables = Maps.newConcurrentMap();

    final protected Logger logger = LoggerFactory.getLogger(getClass());

    final protected String httpAuthHeader;

    static {
        startScan();
    }


    HttpAuthentication(String httpAuthHeader) {
        this.httpAuthHeader = httpAuthHeader;
    }

    private static void startScan() {
        //discover all implementors
        Reflections reflections = new Reflections(Authentication.class.getPackage().getName());
        Set<Class<? extends Authentication>> subTypesOf = reflections.getSubTypesOf(Authentication.class);

        final Logger logger = LoggerFactory.getLogger("Authentication.Scanner");

        subTypesOf.forEach(auth -> {
            AKA aka = Annotations.get(auth, AKA.class);
            if (aka == null || Texts.isNullOrEmpty(aka.value())) {
                logger.warn("Authentication '{}' has been found, but has been skipped due of AKA annotation missing", auth.getName());
                return;
            }

            logger.info("Authentication supported for HTTP type: {} in class {}", aka.value(), auth);
            availables.put(aka.value().trim().toLowerCase(), auth);
        });

    }

    /**
     * Searchs http headers and returns the authorization handler which matches the required.
     *
     * @param headers
     * @param <S>
     * @return
     * @throws UnsupportedFeatureException is thrown when the requested feature is not implemented yet in current API. Nothing to do for the moment.
     * @throws IllegalArgumentException is thrown when the HTTP security header is not present in Http request.
     */
    public static <S extends HttpAuthentication> S lookUpInHttpHeaders(Map<String, List<String>> headers) throws UnsupportedFeatureException, IllegalArgumentException {

        //final String authorizationHeaderValue = headers.keySet().stream().filter(AUTHORIZATION::equals).findFirst().orElse("NOT FOUND");
        /*
        final String authorizationHeaderValue = headers.keySet().stream().filter(httpHeader -> {
            return AUTHORIZATION.equals(httpHeader);
        }).findFirst().orElse("NOT FOUND");
        */

        Map.Entry<String, List<String>> authHeaderEntry = headers.entrySet().stream().
                filter(entry -> AUTHORIZATION.equals(entry.getKey())).findFirst().
                orElse(new
                        UnmodifiableMapEntry("", ImmutableList.of("")));

        return HttpAuthentication.forAuthorizationHeader(authHeaderEntry.getValue().stream().findFirst().get());

        /*
        Object value = authHeaderEntry.getValue();
        if(List.class.isAssignableFrom(value.getClass())){
            List<String> lvalue = (List<String>) value;
            return HttpAuthentication.forAuthorizationHeader(lvalue.stream().findFirst().get());
        }else if(String.class.isAssignableFrom(value.getClass())){
            //is string
            return HttpAuthentication.forAuthorizationHeader((String) value);
        }else
            throw new IllegalStateException(
                    "Error interno, no deberia suceder, casos extrmos de HEADER y tipos. Se esperaba un List o String en vez de: "+
                            value.getClass());
        */
    }

    /**
     * Once the 'Authorization' value has been fetched from headers, it can be passed straight to this parameter.
     * @param authHeaderValue
     * @param <S>
     * @return
     * @throws AuthenticationException
     */
    public static <S extends HttpAuthentication> S forAuthorizationHeader(String authHeaderValue) throws UnsupportedFeatureException {

        if(Texts.isNullOrEmpty(authHeaderValue)){
            //need to have this header
            return (S) new HttpAuthentication<Void>(null){

                @Override
                public Void authenticate(CredentialsEvaluator input) throws AuthenticationException {
                    throw new AuthenticationException("Http header 'Authorization' was not provided in request");
                }
            };
        }

        //authHeader looks like
        //Basic aHR0cHdhdGNoOmY=
        //without the 'Authorization' part


        final Iterator<String> values = Splitter.on(" ").trimResults().omitEmptyStrings().split(authHeaderValue).iterator();
        if (!values.hasNext()) {
            throw new IllegalArgumentException(
                    build(
                            "Unable to process request. HTTP authentication is required, need to provide the ",
                            AUTHORIZATION,
                            " HTTP header to gain access")
            );
        }

        //
        try {
            Class<? extends Authentication> authHandlerClass = availables.get(values.next().toLowerCase());
            if (authHandlerClass == null)
                throw new AuthenticationException(
                        build(
                                "HTTP authentication found '",
                                AUTHORIZATION,
                                "' is not supported yet. Please contact the administrator.")
                );

            Constructor<? extends Authentication> constructor = authHandlerClass.getDeclaredConstructor(String.class);
            constructor.setAccessible(true);

            return (S) constructor.newInstance(authHeaderValue);
//            return (S) authHandlerClass.newInstance();
        } catch (Throwable e) {
            throw new IllegalStateException("FATAL. An unexpected error has occurred while discovering HTTP SECURITY. "+
                    e.getMessage(), e);
        }
    }

    /**
     * Use {@link HttpAuthentication#authenticate()} method instead.
     * No need to implement it, because {@link HttpAuthentication} factory class already passed input as constructor parameter in this class.
     * <br/>Use the {@link HttpAuthentication#httpAuthHeader} field.
     * @param input
     * @return
     * @throws AuthenticationException
     */
    /*
    @Override
    public final Status authenticate(String input) throws AuthenticationException {
        return authenticate(null);
    }*/

    /**
     * Method to evaluate HTTP authentication.
     *
     * @return
     */
    //public abstract Status authenticate();

    public enum Status {
        GRANTED, DENIED
    }
}
