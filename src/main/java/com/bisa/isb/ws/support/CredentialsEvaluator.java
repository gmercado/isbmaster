package com.bisa.isb.ws.support;

import org.apache.http.auth.Credentials;

/**
 * @author Miguel Vega
 * @version $Id: CredentialSupplier.java 0, 2015-09-17 11:07 PM mvega $
 */
public interface CredentialsEvaluator<E>{
    /**
     *
     * @param credentials
     * @return true if credentials are valid
     */
    E evaluate(Credentials credentials) throws AuthenticationException;
}
