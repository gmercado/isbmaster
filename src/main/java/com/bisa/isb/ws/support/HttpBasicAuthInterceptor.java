package com.bisa.isb.ws.support;

import com.bisa.isb.catalog.Catalog;
import com.bisa.isb.ws.catalog.Service;
import com.bisa.isb.ws.catalog.Services;
import com.bisa.isb.ws.security.AuthSecurityService;
import com.bisa.isb.ws.security.dao.WSPayloadDao;
import com.bisa.isb.ws.security.entities.*;
import com.bisa.isb.ws.soap.Faults;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import org.apache.cxf.binding.soap.SoapFault;
import org.apache.cxf.helpers.CastUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.security.AccessDeniedException;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.http.AbstractHTTPDestination;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

/**
 * @author Miguel Vega
 * @version $Id: HttpBasicAuthInterceptor.java; sep 29, 2015 01:08 PM mvega $
 */
public class HttpBasicAuthInterceptor extends DefaultInterceptor<Message> {

    private final Catalog<Service> serviceCatalog;

    private final WSPayloadDao wsPayloadDao;

    /**
     * @param phase determines the phase for implementation
     */
    //@Inject
    public HttpBasicAuthInterceptor(String phase, AuthSecurityService authSecurityService, Catalog<Service> serviceCatalog,
                                    WSPayloadDao wsPayloadDao) {
        super(phase, authSecurityService);
        this.serviceCatalog = serviceCatalog;
        this.wsPayloadDao = wsPayloadDao;
    }

    /**
     * Using {@link Phase#PRE_INVOKE} phase by default
     */
    @Inject
    public HttpBasicAuthInterceptor(AuthSecurityService authSecurityService, Catalog<Service> serviceCatalog,
                                    WSPayloadDao wsPayloadDao) {
        this(Phase.PRE_INVOKE, authSecurityService, serviceCatalog, wsPayloadDao);
    }


    @Override
    public void handleMessage(Message message) throws Fault {
        final Map<String, List<String>> headers = CastUtils.cast((Map) message.get(Message.PROTOCOL_HEADERS));

        final HttpServletRequest httpRequest = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);
        final HttpServletResponse httpResponse = (HttpServletResponse) message.get(AbstractHTTPDestination.HTTP_RESPONSE);

        HttpAuthentication<AuthUser> authentication = null;
        try {
            authentication = HttpAuthentication.lookUpInHttpHeaders(headers);
        } catch (IllegalArgumentException e) {
            logger.error("An unexpected error has occurred.", e);
            //authorization header not provided
        } catch (UnsupportedFeatureException e) {
            logger.warn("BASIC AUTH not found");
            requestWWWAuthentication(httpResponse);
            return;
        }
        AuthUser authUser = null;
        try {
            //authentication
            if (authentication != null) {
                authUser = authentication.authenticate(credentials -> authSecurityService.getUserByName(credentials));
            }
            //authorization
            AuthWebServiceOperation wsOperation = checkAuthorization(authUser, message);
            if (authUser != null) {
                logger.debug("Consumption successful for user {}, from address {}",
                        authUser.getUserName(), httpRequest.getRemoteAddr());
            }
            //store payload
            WSPayload payload = wsPayloadDao.persist(httpRequest, wsOperation);
            //append the ID to the request as a new parameter
            message.getExchange().put(WSPayloadDao.TRANSPORTED_PAYLOAD_ID, payload);
        } catch (AuthenticationException x) {
            logger.warn("Authorization needed while consuming service");
            completeCXFInterceptorChainOnError(message, SC_UNAUTHORIZED);
            //Just in case of using any broser or similar client
            requestWWWAuthentication(httpResponse);
        } catch (AuthorizationException x) {
            logger.warn(x.getMessage());
            resourceUnauthorized(authUser, message, httpResponse, x);
        } catch (Throwable x) {
            logger.error("ha ocurrido un error inesperado", x);
        }
    }

    /**
     * Helpful method to unleash the exception handling. This might be necessary for REST implementations.
     * By default a FAULT exception is thrown.
     * When using with RESTFul services, this produces an HTTP 500 response code (internal server error)
     *
     * @param authUser
     * @param message
     * @param x
     */
    public void resourceUnauthorized(AuthUser authUser, Message message,
                                     HttpServletResponse httpServletResponse,
                                     AuthorizationException x) throws Fault {
        logger.warn("Authorization required for user '{}' while attempting to consume service operation '{}'",
                authUser.getUserName(), getBoundMethod(message));

//            prepareErrorResponse(message, SC_UNAUTHORIZED);
        SoapFault of = new Faults().of(SC_FORBIDDEN + "");
        of.setStatusCode(SC_FORBIDDEN);
        throw of;
    }

    /**
     * Triggers the basic Authentication to the client if this didn't provide an Authentication yet
     *
     * @param httpServletResponse
     */
    protected void requestWWWAuthentication(HttpServletResponse httpServletResponse) {
        try {
            httpServletResponse.setHeader("WWW-Authenticate", "BASIC realm=\"BISA services\"");
            httpServletResponse.sendError(SC_UNAUTHORIZED);
        } catch (IOException e) {
            logger.warn("Unexpected error!", e);
        }
    }

    /**
     * Verifies id the use provided has access granted to the requested service.
     *
     * @param authUser Auth User
     * @param message  Message
     * @return the Object fetched from database if possible.
     * @throws AccessDeniedException
     */
    protected AuthWebServiceOperation checkAuthorization(AuthUser authUser, Message message) throws AuthorizationException {
        //authentication successful
        final Method method = getBoundMethod(message);
        Class<?> serviceInvoked = method.getDeclaringClass();
        logger.debug("Service invoked...{}", serviceInvoked.getName());
        logger.debug("Method is ...{}", method.getName());
        //todo, seek for web service operation permissions, no need to return, just examine
        return getWSOperation(authUser, serviceInvoked, method);
    }

    protected AuthWebServiceOperation getWSOperation(AuthUser authUser, Class serviceInvoked, Method method) throws AuthorizationException {
        Set<AuthWebServiceOperation> operations = Sets.newConcurrentHashSet();
        List<AuthRole> roles = authUser.getRoles();
        roles.forEach(role -> operations.addAll(role.getOperations()));
        //check in all ws operations
        QName serviceQName = Services.getServiceWeakReference(serviceInvoked).getLeft();
        final AtomicReference<Service> service = new AtomicReference<>(null);
        try {
            service.set(serviceCatalog.find(serviceQName).iterator().next());
        } catch (NoSuchElementException x) {
            throw new AuthorizationException("None of following roles '{}' are granted to consume '{}#{}'",
                    authUser.getRoles().stream().map(AuthRole::getName).collect(Collectors.joining(",")),
                    serviceInvoked.getName(),
                    method.getName());
        }
        try {
            return operations.stream().filter(wsop -> {
                AuthWebService webService = wsop.getWebService();
                QName name = service.get().getName();
                String javaMethod = method.getName();
                return webService.getNameSpace().equals(name.getNamespaceURI())
                        && wsop.getJavaMethodName().equals(javaMethod)
                        && webService.getName().equals(name.getLocalPart());
            }).findFirst().get();
        } catch (NoSuchElementException x) {
            throw new AuthorizationException("Requested operation '{}#{}' is not granted to user '{}' or in the following roles: '{}'",
                    service.get().getUniqueName(),
                    method.getName(),
                    authUser.getUserName(),
                    authUser.getRoles().stream().map(AuthRole::getName).collect(Collectors.joining(",")));
        }
    }

    protected Method getBoundMethod(Message m) throws IllegalStateException {
        return WebServices.getBoundMethod(m);
    }
}