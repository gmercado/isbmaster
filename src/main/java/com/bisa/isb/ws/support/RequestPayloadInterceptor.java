package com.bisa.isb.ws.support;

import com.bisa.isb.ws.security.dao.WSPayloadDao;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingMessage;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Miguel Vega
 * @version $Id: MessageExchangeInterceptor.java; oct 06, 2015 03:37 PM mvega $
 */
public class RequestPayloadInterceptor extends LoggingInInterceptor {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final AtomicReference<Message> messageWrapper = new AtomicReference<>();

    @Inject
    public RequestPayloadInterceptor() {
        super(Phase.RECEIVE);
    }

    /**
     * Receive the message and fetch the source handlers to persist metadata about payload
     *
     * @param message content message
     * @throws Fault by default
     */
    @Override
    public void handleMessage(Message message) throws Fault {
        messageWrapper.set(message);
        super.handleMessage(message);
    }

    @Override
    protected String formatLoggingMessage(LoggingMessage loggingMessage) {

        Message message = messageWrapper.get();
        String method = (String) message.get(Message.HTTP_REQUEST_METHOD);
        String query = (String) message.get(Message.QUERY_STRING);
        if (!"GET".equals(method) || !Strings.isNullOrEmpty(query)) {
            String inSOAP = loggingMessage.getPayload().toString();

            HttpServletRequest request = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);
            request.setAttribute(WSPayloadDao.TRANSPORTED_PAYLOAD, inSOAP);

            logger.debug(" IN < ****************************************");
            logger.debug(inSOAP);
            logger.debug(" IN > ****************************************");
        }

        return super.formatLoggingMessage(loggingMessage);
    }
}
