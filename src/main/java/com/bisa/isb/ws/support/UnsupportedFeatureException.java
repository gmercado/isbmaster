package com.bisa.isb.ws.support;

import com.bisa.isb.api.GenericException;

/**
 * @author Miguel Vega
 * @version $Id: UnsupportedFeatureException.java; sep 18, 2015 09:09 AM mvega $
 */
public class UnsupportedFeatureException extends GenericException {
    public UnsupportedFeatureException(Throwable cause, String message, Object... params) {
        super(message, cause, params);
    }

    public UnsupportedFeatureException(String message, Object... params) {
        super(message, params);
    }
}
