package com.bisa.isb.ws.support;

import com.bisa.isb.api.GenericException;

/**
 * @author Miguel Vega
 * @version $Id: AuthenticationExeption.java 0, 2015-09-17 11:37 PM mvega $
 */
public class AuthenticationException extends GenericException {

    public AuthenticationException(Throwable cause, String message, Object... params) {
        super(message, cause, params);
    }

    public AuthenticationException(String message, Object... params) {
        super(message, params);
    }
}
