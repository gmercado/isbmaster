package com.bisa.isb.ws.support;

/**
 * A very simple interafce to allow discovery in objects
 * @author Miguel Vega
 * @version $Id: Visitor.java; sep 16, 2015 12:58 PM mvega $
 */
public interface Visitor<U> {
    void visit(U u);
}
