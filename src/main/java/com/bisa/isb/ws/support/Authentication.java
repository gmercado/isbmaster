package com.bisa.isb.ws.support;

/**
 * A very simple class to allow authentication be handled by Web Service publishers.
 * By now this class only can handle teh following authentications:
 * <ul>
 * <li>BASIC</li>
 * </ul>
 *
 * @author Miguel Vega
 * @version $Id: Authentication.java; sep 17, 2015 03:45 PM mvega $
 */
public interface Authentication<S, T> {

    T authenticate(S input) throws AuthenticationException;
}