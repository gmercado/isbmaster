package com.bisa.isb.ws.support;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author Miguel Vega
 * @version $Id: Types.java; sep 16, 2015 12:57 PM mvega $
 */
public class Annotations<T extends Annotation> {

    final Annotation annotation;

    Annotations(T annotation) {
        this.annotation = annotation;
    }

    public static <S extends Annotation> Annotations<S> of(S s) {
        return new Annotations<S>(s);
    }

    public Annotation tryFetch(Class type) {
        return (type.getAnnotation(type) != null) ? type.getAnnotation(type) : null;
    }

    /**
     * Simply makes a query on type to return the annotation on it
     *
     * @param source
     * @param annotation
     * @param <T>
     * @return
     */
    public static <T extends Annotation> T getNullable(Class source, Class<T> annotation) {
        return (T) source.getAnnotation(annotation);
    }

    public static <T extends Annotation> T getNullable(Method method, Class<T> annotation) {
        return (T) method.getAnnotation(annotation);
    }

    /**
     * Verifies if a Class or Method is annotated with given annotation type
     *
     * @param element
     * @param annotation
     * @return
     */
    public static boolean isAnnotatedWith(AnnotatedElement element, Class<? extends Annotation> annotation) {
        return element.getAnnotation(annotation) != null;
    }

    /**
     * Useful when need an annotation to perform any operation if exists or not
     *
     * @param source
     * @param required
     * @param <T>
     * @return returns annotation requested if this does not exists ,a proxied instance will be created instead.
     */
    public static <T extends Annotation> T get(AnnotatedElement source, Class<T> required) {
        Annotation annotation = source.getAnnotation(required);
        if (annotation == null) {
            return (T) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                    new Class[]{required}, (proxy, method, args) -> {
                        //returns null because of an unknown implementation
                        return null;
                    });
        }
        return (T) annotation;
    }
}
