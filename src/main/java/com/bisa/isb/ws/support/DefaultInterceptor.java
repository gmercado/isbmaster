package com.bisa.isb.ws.support;

import com.bisa.isb.api.net.Subnet;
import com.bisa.isb.ws.security.AuthSecurityService;
import com.bisa.isb.ws.security.entities.AuthUser;
import com.bisa.isb.ws.security.entities.AuthUserIP;
import com.google.common.net.InetAddresses;
import org.apache.commons.lang.StringUtils;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.interceptor.security.AccessDeniedException;
import org.apache.cxf.message.Exchange;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.transport.Conduit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.util.List;
import java.util.NoSuchElementException;

import static com.bisa.isb.api.Texts.build;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

/**
 * @author Miguel Vega
 * @version $Id: Interceptors.java; oct 06, 2015 02:14 PM mvega $
 * @see http://cxf.apache.org/docs/interceptors.html
 */
public abstract class DefaultInterceptor<U extends Message> extends AbstractPhaseInterceptor<U>{

    final protected Logger logger = LoggerFactory.getLogger(DefaultInterceptor.class);

    final protected AuthSecurityService authSecurityService;

    public DefaultInterceptor(String interceptorId, String phase, AuthSecurityService authSecurityService) {
        super(interceptorId, phase);
        this.authSecurityService = authSecurityService;
    }
    public DefaultInterceptor(String phase, AuthSecurityService authSecurityService) {
        this(null, phase, authSecurityService);
    }

    /**
     * Checks if the request is coming from a valid network address
     * @param authUser
     * @param httpRequest
     * @param httpResponse
     * @throws AccessDeniedException
     */
    public void checkRemoteAddress(AuthUser authUser, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws AccessDeniedException {
        String ipString =  httpRequest.getRemoteAddr();
        ipString = StringUtils.trimToEmpty(StringUtils.remove(ipString, "/"));
        logger.info("IP httpRequest Remote: [{}], new IP strip [{}].", httpRequest.getRemoteAddr(), ipString);
        final InetAddress remoteAddress = InetAddresses.forString(ipString);
        //final InetAddress remoteAddress = InetAddresses.forString(httpRequest.getRemoteAddr());

        List<AuthUserIP> ips = authSecurityService.getAllIPAddresses(authUser);
        //todo, connections from localhost are always allowed, also are the reverse proxied addresses
        if(remoteAddress.isAnyLocalAddress() || remoteAddress.isLoopbackAddress()){
            logger.info("A new request has come from ");
            return;
        }

        try {
            //fetch the valid ip from database
            boolean found = false;
            //List<AuthUserIP> ips = authSecurityService.getAllIPAddresses(authUser);
            for(AuthUserIP ip: ips){
                logger.debug("IP de comparacion en la BD [{}]",ip.getIp());
                if(ipString.equalsIgnoreCase(StringUtils.trimToEmpty(ip.getIp()))){
                    found = true;
                }
            }
            if(!found)
            {
                throw new AccessDeniedException(build("Access denied for IP address: [", ipString, "] while attempting to consume a service."));
            }
            //ips.stream().filter(ip -> Subnet.forString(ip.getIp()).isInNet(remoteAddress)).findFirst().get();
            //ips.stream().filter(ip -> Subnet.forString(ip.getId().getIp()).isInNet(remoteAddress)).findFirst().get();

        } catch (NoSuchElementException | IllegalArgumentException e) {
            throw new AccessDeniedException(build("Access denied for IP address: ", remoteAddress.toString(), " while attempting to consume a service."));
        }
    }

    /**
     * Prepares the HTTP response to reject request
     * @param httpResponse
     */
    protected void unauthorize(HttpServletResponse httpResponse) {
        try {
            // Not allowed, so report he's unauthorized
            httpResponse.setHeader("WWW-Authenticate", "BASIC realm=\"BISA SOAP services\"");
            httpResponse.sendError(SC_UNAUTHORIZED);
            // Could offer to add him to the allowed user list
        } catch (IOException ioe) {
            throw new IllegalStateException("An unexpected error has occurred. While evaluating authorization.", ioe);
        }
    }

    /**
     * To continue the intercepting of messages without producing a stream closed error.
     * @param message
     * @param httpResponseCode
     */
    protected void completeCXFInterceptorChainOnError(final Message message, int httpResponseCode){
        Message outMessage = getOutMessage(message);
        outMessage.put(Message.RESPONSE_CODE, httpResponseCode);
        message.getInterceptorChain().abort();
        try {
            getConduit(message).prepare(outMessage);
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
        }
    }

    protected Message getOutMessage(Message inMessage) {
        Exchange exchange = inMessage.getExchange();
        Message outMessage = exchange.getOutMessage();
        if (outMessage == null) {
            Endpoint endpoint = exchange.get(Endpoint.class);
            outMessage = endpoint.getBinding().createMessage();
            exchange.setOutMessage(outMessage);
        }
        outMessage.putAll(inMessage);
        return outMessage;
    }

    protected Conduit getConduit(Message inMessage) throws IOException {
        Exchange exchange = inMessage.getExchange();
        /*
        EndpointReferenceType target = exchange.get(EndpointReferenceType.class);
        Conduit conduit = exchange.getDestination().getBackChannel(inMessage, null, target);
        */
        Conduit conduit = exchange.getDestination().getBackChannel(inMessage);
        exchange.setConduit(conduit);
        return conduit;
    }

    private void close(Message outMessage) throws IOException {
        OutputStream os = outMessage.getContent(OutputStream.class);
        os.flush();
        os.close();
    }
}
