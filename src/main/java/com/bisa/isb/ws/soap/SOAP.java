package com.bisa.isb.ws.soap;

import com.google.inject.BindingAnnotation;

import javax.interceptor.InterceptorBinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Useful to nnotate anything related to SOAP WS
 * @author Miguel Vega
 * @version $Id: Testeable.java 0, 2015-11-25 11:33 PM mvega $
 */
@InterceptorBinding
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
@Retention(value = RetentionPolicy.RUNTIME)
@BindingAnnotation
public @interface SOAP {
}
