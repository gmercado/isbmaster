package com.bisa.isb.ws.soap;

import com.bisa.isb.catalog.Catalog;
import com.bisa.isb.ws.Publish;
import com.bisa.isb.ws.ServiceEndpoint;
import com.bisa.isb.ws.catalog.Service;
import com.bisa.isb.ws.catalog.Services;
import com.bisa.isb.ws.security.AuthSecurityService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.cxf.Bus;
import org.apache.cxf.interceptor.Interceptor;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;
import org.apache.cxf.jaxws.binding.soap.JaxWsSoapBindingConfiguration;
import org.apache.cxf.jaxws.support.JaxWsServiceFactoryBean;
import org.apache.cxf.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import java.util.List;
import java.util.Map;

/**
 * The main entry point for the SOAP web services declared in project.
 *
 * @author Miguel Vega
 * @version $Id: DefaultCXFServlet.java; sep 14, 2015 03:52 PM mvega $
 * @see TODO, need toimplement web service references to enhance customization: http://docs.oracle.com/cd/E17802_01/webservices/webservices/docs/2.0/tutorial/doc/JAXWS3.html
 */
@Publish("/soap/*")
@Singleton
public class SOAPWebServiceConfiguration extends ServiceEndpoint {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    private AuthSecurityService authSecurityService;

    @Inject
    private Catalog<Service> serviceCatalog;

    @Inject
    private JaxWsServerFactoryBean ifactory;

    @Inject
    @SOAP
    private Map<Class, Object> soapServiceInstances;

    @Override
    protected void loadBus(ServletConfig sc) {
        super.loadBus(sc);

        final Bus cxfServiceBus = super.getBus();

        /*
        final Set<Class> visitedTypes = Sets.newHashSet();
        //configure all classpath
        Reflections reflections = new Reflections("");
        Set<Class<?>> webservices = reflections.getTypesAnnotatedWith(WebService.class);
        webservices.forEach(qualifiedType -> {
            WebService annotation = get(qualifiedType, WebService.class);
            if (visitedTypes.contains(qualifiedType) || qualifiedType.isInterface())
                return;

            visitedTypes.add(qualifiedType);

            //if not present yet, need to retrieve the
            logger.debug("About to check annotation while loading web services: " + annotation);

            Service service = Services.discoverService(this, qualifiedType);//getServiceFromJavaType(qualifiedType);
        */


        soapServiceInstances.keySet().forEach(qualifiedType -> {

            Service service = Services.discoverService(this, qualifiedType);//getServiceFromJavaType(qualifiedType);

            serviceCatalog.add(service);

            JaxWsServerFactoryBean factory = new JaxWsServerFactoryBean();

            List<Interceptor<? extends Message>> inInterceptors = ifactory.getInInterceptors();
            factory.getInInterceptors().addAll(inInterceptors);
            factory.getInFaultInterceptors().addAll(ifactory.getInFaultInterceptors());
            factory.getOutInterceptors().addAll(ifactory.getOutInterceptors());
            factory.getOutFaultInterceptors().addAll(ifactory.getOutFaultInterceptors());

            factory.setBus(cxfServiceBus);

            //factory.setServiceClass(qualifiedType);
            factory.setServiceBean(soapServiceInstances.get(qualifiedType));

            factory.setBindingConfig(new JaxWsSoapBindingConfiguration(new JaxWsServiceFactoryBean()));


            String uri = "/" + service.getName().getLocalPart();
            factory.setAddress(uri);
            factory.create();

            logger.debug(" * * * * * * * * SOAP Service {} has been created correctly to be served with the following URI: {}",
                    service.getUniqueName(), service.getName().getPrefix());
        });

        //persist service catalog
        authSecurityService.populateCatalog(serviceCatalog, Service.Type.SOAP);
    }
}