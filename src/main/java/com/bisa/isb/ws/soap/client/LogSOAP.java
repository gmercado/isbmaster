package com.bisa.isb.ws.soap.client;

import bus.consumoweb.entities.ConsumoWeb;

import java.util.concurrent.Future;

/**
 * Created by ccalle on 15/03/2017.
 */
public interface LogSOAP {
   public Future<ConsumoWeb> persistRequest(String request);
   public void persistResponse(int httpStatus, String response,int millis,Future<ConsumoWeb> future);

}
