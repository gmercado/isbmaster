package com.bisa.isb.ws.soap;

import org.apache.cxf.binding.soap.SoapFault;

import javax.xml.namespace.QName;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * @author Miguel Vega
 * @version $Id: Faults.java; sep 24, 2015 04:34 PM mvega $
 */
public class Faults {
    final ResourceBundle resourceBundle;

    public Faults() {
        final Reader reader = new StringReader("" +
                "401=Request requires HTTP authentication.\n" +
                "403=Unauthorized, the server understood the request but refused to fulfill it.\n" +
                "8584=Autenticacion requerida\n" +
                "1000=No tiene suficientes permisos para utilizar esta operacion");
        try {
            resourceBundle = new PropertyResourceBundle(reader);
        } catch (IOException e) {
            throw new IllegalStateException("An unexcpecte=ed error has occurred");
        }
    }

    public SoapFault of(String code){
        return new SoapFault(new org.apache.cxf.common.i18n.Message(
                code,
                resourceBundle),
                QName.valueOf(code)
                //                        new QName("http://www.bisa.com/soap/ebisa/fault", "8584")
        );
    }

    public SoapFault causedBy(String code, Throwable cause){
        return new SoapFault(new org.apache.cxf.common.i18n.Message(
                code,
                resourceBundle),
                cause,
                QName.valueOf(code)
                //                        new QName("http://www.bisa.com/soap/ebisa/fault", "8584")
        );
    }
}
