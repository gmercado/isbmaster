package com.bisa.isb.ws.soap;

import com.bisa.isb.ws.security.AuthSecurityService;
import com.bisa.isb.ws.security.entities.AuthUser;
import com.bisa.isb.ws.support.AuthenticationException;
import com.bisa.isb.ws.support.HttpAuthentication;
import com.bisa.isb.ws.support.UnsupportedFeatureException;
import com.google.inject.Inject;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.helpers.CastUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.security.AccessDeniedException;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

/**
 * @author Miguel Vega
 * @version $Id: WSDLAUthenticationInterceptor.java; sep 24, 2015 04:29 PM mvega $
 */
public class WSDLAuthenticationInterceptor extends DefaultSoapInterceptor {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    public WSDLAuthenticationInterceptor(AuthSecurityService authSecurityService) {
        super(Phase.RECEIVE, authSecurityService);
    }

    @Override
    public void handleMessage(SoapMessage message) throws Fault {

        final Map<String, List<String>> headers = CastUtils.cast((Map) message.get(Message.PROTOCOL_HEADERS));

        final HttpServletRequest httpRequest = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);
        final HttpServletResponse httpResponse = (HttpServletResponse) message.get(AbstractHTTPDestination.HTTP_RESPONSE);

        HttpAuthentication<AuthUser> authentication = null;
        try {
            authentication = HttpAuthentication.lookUpInHttpHeaders(headers);
        } catch (IllegalArgumentException e) {
            logger.error("An unexpected error has occurred.", e);
            //no se proveyo del header de seguridad
        } catch (UnsupportedFeatureException e) {
            unauthorize(httpResponse);
            return;
        }

        try {
            AuthUser authUser = authentication.authenticate(credentials -> authSecurityService.getUserByName(credentials));

            logger.info("Consumption successful for user {}, from address {}",
                    authUser.getUserName(), httpRequest.getRemoteAddr());

            //verify if the user has access to current service
            checkRemoteAddress(authUser, httpRequest, httpResponse);
        } catch (AuthenticationException x) {
            completeCXFInterceptorChainOnError(message, SC_UNAUTHORIZED);
            unauthorize(httpResponse);
        } catch(AccessDeniedException x){
            logger.warn(x.getMessage());
            //invalid incoming IP address
            completeCXFInterceptorChainOnError(message, SC_FORBIDDEN);
            try {
                httpResponse.sendError(SC_FORBIDDEN);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}