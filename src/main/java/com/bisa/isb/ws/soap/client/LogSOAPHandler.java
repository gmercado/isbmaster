package com.bisa.isb.ws.soap.client;

import bus.consumoweb.consumer.ClienteServiciosWeb;
import bus.consumoweb.entities.ConsumoWeb;
import bus.plumbing.utils.Watch;
import bus.plumbing.utils.Watches;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.util.Set;
import java.util.concurrent.Future;

import static bus.plumbing.utils.Watches.CONSUMO_WS;

/**
 * see {@link com.bisa.isb.ws.security.WebServiceHandler}
 */
public class LogSOAPHandler implements SOAPHandler<SOAPMessageContext> {

    private static final Logger LOGGER = LoggerFactory.getLogger(com.bisa.isb.ws.security.WebServiceHandler.class);
    private LogSOAP logSOAP;
    private Watch watch;
    private Future<ConsumoWeb> consumoWebFuture;
    public LogSOAPHandler(LogSOAP logSOAP) {
        this.logSOAP=logSOAP;
    }

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        logToSystemOut(context);
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        logToSystemOut(context);
        return true;
    }

    @Override
    public void close(MessageContext context) {

    }

    private void logToSystemOut(SOAPMessageContext smc) {
        Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        SOAPMessage message = smc.getMessage();
        try {
            message.writeTo(baos);
            LOGGER.debug(">> {}", baos.toString());
            if (outboundProperty) {
                LOGGER.debug("\nOutgoing message:");
                watch = new Watch(CONSUMO_WS);
                consumoWebFuture=logSOAP.persistRequest(baos.toString());
            } else {
                LOGGER.debug("\nIncoming message HTTP_RESPONSE_CODE:"+smc.get(MessageContext.HTTP_RESPONSE_CODE));
                LOGGER.debug("\nIncoming message:");
                final int milis = (int) watch.stop();
                logSOAP.persistResponse((Integer)smc.get(MessageContext.HTTP_RESPONSE_CODE),baos
                        .toString(),milis,consumoWebFuture);
            }
        } catch (Exception e) {
            LOGGER.error("Exception in handler:", e);
        }




    }
}
