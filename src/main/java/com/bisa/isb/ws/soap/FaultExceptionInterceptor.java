package com.bisa.isb.ws.soap;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Document;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import java.io.ByteArrayInputStream;

/**
 * @author Miguel Vega
 * @version $Id: FaultExceptionInterceptor.java; sep 17, 2015 12:18 PM mvega $
 */
public class FaultExceptionInterceptor extends AbstractPhaseInterceptor<SoapMessage>{

    public FaultExceptionInterceptor(String phase) {
        super(Phase.WRITE);
    }

    @Override
    public void handleMessage(SoapMessage message) throws Fault {
        Exception e = message.getContent(Exception.class);
        if (e instanceof Fault) {
            Fault fault = (Fault) e;
            fault.setMessage("This is a critical fault");
            fault.setFaultCode(new QName("ns", "CriticalFault"));
            try {
                DocumentBuilder builder = getBuilder();
                Document doc = builder.parse(new ByteArrayInputStream("<detail><critical>critical details</critical></detail>".getBytes()));
                fault.setDetail(doc.getDocumentElement());
            } catch (Exception e1) {
                System.out.println("Cannot build detail element: " + e.getMessage());
            }
        }
    }

    //todo, need to be completed
    DocumentBuilder getBuilder(){
        return null;
    }
}
