package com.bisa.isb.ws.soap;

/**
 * @author Miguel Vega
 * @version $Id: DefaultSOAPInterceptor.java; oct 06, 2015 03:08 PM mvega $
 */

import com.bisa.isb.ws.security.AuthSecurityService;
import com.bisa.isb.ws.support.DefaultInterceptor;
import org.apache.cxf.binding.soap.SoapFault;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.SoapInterceptor;
import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageUtils;
import org.apache.cxf.staxutils.StaxUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.net.URI;
import java.util.Collections;
import java.util.Set;

public abstract class DefaultSoapInterceptor extends DefaultInterceptor<SoapMessage>implements SoapInterceptor {

    public DefaultSoapInterceptor(String p, AuthSecurityService authSecurityService) {
        super(p, authSecurityService);
    }
    public DefaultSoapInterceptor(String i, String p, AuthSecurityService authSecurityService) {
        super(i, p, authSecurityService);
    }


    public Set<URI> getRoles() {
        return Collections.emptySet();
    }

    public Set<QName> getUnderstoodHeaders() {
        return Collections.emptySet();
    }

    protected String getFaultCodePrefix(XMLStreamWriter writer, QName faultCode) throws XMLStreamException {
        String codeNs = faultCode.getNamespaceURI();
        String prefix = null;
        if (codeNs.length() > 0) {
            prefix = StaxUtils.getUniquePrefix(writer, codeNs, true);
        }
        return prefix;
    }

    protected void prepareStackTrace(SoapMessage message, SoapFault fault) throws Exception {
        boolean config = MessageUtils.getContextualBoolean(message, Message.FAULT_STACKTRACE_ENABLED, false);
        if (config && fault.getCause() != null) {
            StringBuilder sb = new StringBuilder();
            Throwable throwable = fault.getCause();
            sb.append("Caused by: ").append(throwable.getClass().getCanonicalName())
                    .append(": " + throwable.getMessage() + "\n").append(Message.EXCEPTION_CAUSE_SUFFIX);
            while (throwable != null) {
                for (StackTraceElement ste : throwable.getStackTrace()) {
                    sb.append(ste.getClassName() + "!" + ste.getMethodName() + "!" + ste.getFileName() + "!"
                            + ste.getLineNumber() + Message.EXCEPTION_CAUSE_SUFFIX);
                }
                throwable = throwable.getCause();
                if (throwable != null) {
                    sb.append("Caused by: " +  throwable.getClass().getCanonicalName()
                            + " : " + throwable.getMessage() + Message.EXCEPTION_CAUSE_SUFFIX);
                }
            }
            Element detail = fault.getDetail();
            String soapNamespace = message.getVersion().getNamespace();
            if (detail == null) {
                Document doc = DOMUtils.newDocument();
                Element stackTrace = doc.createElementNS(
                        Fault.STACKTRACE_NAMESPACE, Fault.STACKTRACE);
                stackTrace.setTextContent(sb.toString());
                detail = doc.createElementNS(
                        soapNamespace, "detail");
                fault.setDetail(detail);
                detail.appendChild(stackTrace);
            } else {
                Element stackTrace =
                        detail.getOwnerDocument().createElementNS(Fault.STACKTRACE_NAMESPACE,
                                Fault.STACKTRACE);
                stackTrace.setTextContent(sb.toString());
                detail.appendChild(stackTrace);
            }
        }
    }

    static String getFaultMessage(SoapMessage message, SoapFault fault) {
        if (message.get("forced.faultstring") != null) {
            return (String) message.get("forced.faultstring");
        }
        boolean config = MessageUtils.getContextualBoolean(message, Message.EXCEPTION_MESSAGE_CAUSE_ENABLED, false);
        if (fault.getMessage() != null) {
            if (config && fault.getCause() != null
                    && fault.getCause().getMessage() != null && !fault.getMessage().equals(fault.getCause().getMessage())) {
                return fault.getMessage() + " Caused by: " + fault.getCause().getMessage();
            } else {
                return fault.getMessage();
            }
        } else if (config && fault.getCause() != null) {
            if (fault.getCause().getMessage() != null) {
                return fault.getCause().getMessage();
            } else {
                return fault.getCause().toString();
            }
        } else {
            return "Fault occurred while processing.";
        }
    }
}