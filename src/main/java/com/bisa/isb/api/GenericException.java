package com.bisa.isb.api;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Miguel Vega
 * @version $Id: AuthenticationExeption.java 0, 2015-09-17 11:37 PM mvega $
 */
public class GenericException extends Throwable{

    /*
    public EBankException(Throwable cause, String message, Object... params) {
        super(message, cause);
    }

    public EBankException(String message) {
        super(message);
    }

    //todo, must implement this to replace {} with oaram values
    private static String parse(String message, Object... params){
        return message;
    }

    public EBankException(String message, Throwable throwable, Object... values) {
        super(replace(message, values), throwable);
    }
    */

    public GenericException(String message, Throwable cause, Object... params) {
        super(replace(message, params), cause);
    }

    public GenericException(String message, Object... values) {
        super(replace(message, values));
    }

    static String replace(String text, Object... values) {

        if (values.length<1) return text;

        Pattern patt = Pattern.compile("[{][}]");

        Matcher m = patt.matcher(text);
        StringBuffer sb = new StringBuffer(text.length());

        int ith = 0;

        while (m.find()) {
            String group = m.group(0);

            // ... possibly process 'text' ...
            if(ith<values.length)
                group = String.valueOf(values[ith++]);
            else
                group = "{}";
            m.appendReplacement(sb, Matcher.quoteReplacement(group));
        }
        m.appendTail(sb);
        return sb.toString();
    }
}
