package com.bisa.isb.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class allows to perform executions avoiding some expected error. WIll try to perform a routine by a certain
 * number of times
 * is thrown.
 *
 * @author Miguel Vega
 * @version $Id: ContingencyAttempt.java 0, 8/22/14 9:24 PM, @miguel $
 */
public class AttemptExecutor<T, U extends Throwable> {

    final Logger LOGGER;

    final int attemptCount;

    final long timeUntilNextAttempt;

    final Class<U> manageableType;

    AttemptExecutor(int attemptCount, long timeUntilNextAttempt, Class<U> manageableType) {
        this.attemptCount = attemptCount;
        this.LOGGER = LoggerFactory.getLogger(getClass());
        this.timeUntilNextAttempt = timeUntilNextAttempt;
        this.manageableType = manageableType;
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(int attemptCount) {
        return new AttemptExecutor<X, Y>(attemptCount, 10000l, null);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(long timeUntilNextAttempt) {
        return new AttemptExecutor<X, Y>(10, timeUntilNextAttempt, null);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(int attemptCount, long timeUntilNextAttempt) {
        return new AttemptExecutor<X, Y>(attemptCount, timeUntilNextAttempt, null);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor() {
        return createExecutor(null);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(Class<Y> manageableType) {
        return new AttemptExecutor<X, Y>(10, 10000l, manageableType);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(
            long timeUntilNextAttempt, Class<Y> type) {
        return new AttemptExecutor<X, Y>(10, timeUntilNextAttempt, type);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(
            int attemptCount,
            Class<Y> manageableType) {
        return new AttemptExecutor<X, Y>(attemptCount, 10000l, manageableType);
    }

    public static <X, Y extends Throwable> AttemptExecutor<X, Y> createExecutor(
            int attemptCount,
            long timeUntilNextAttempt,
            Class<Y> type) {
        return new AttemptExecutor<X, Y>(attemptCount, timeUntilNextAttempt, type);
    }

    public T execute(final Attempt<T> attempt) throws U {
        return this.execute(attempt, "Unable to perform {}. A unrecoverable state has been reached, contact immediately with administrator.");
    }

    public T execute(final Attempt<T> attempt, final String messageOnFailure) throws U {

        final AtomicInteger count = new AtomicInteger(0);

        Throwable caught = null;

        do {
            try {

                count.set(count.incrementAndGet());

                T call = attempt.doTry();

                return call;

            } catch (Throwable throwable) {

                if (manageableType != null && throwable.getClass().isAssignableFrom(manageableType)) {
                    throw (U) throwable;
                }

                LOGGER.warn(" X X X X X X X X Contingency attempt has failed while executing: {}", attempt.getAttemptDescription());

                if (count.get() == attemptCount) {
                    break;
                }

                caught = throwable;

                try {
                    Thread.sleep(timeUntilNextAttempt);
                } catch (InterruptedException ignored) {
                }
            }
        } while (true);
        return null;
    }

    public T executeUnchecked(final Attempt<T> attempt) {
        return executeUnchecked(attempt, "Unable to perform {}. A unrecoverable state has been reached, contact immediately with administrator.");
    }

    public T executeUnchecked(final Attempt<T> attempt, final String messageOnFailure) {

        final AtomicInteger count = new AtomicInteger(0);

        Throwable caught = null;

        do {
            try {

                count.set(count.incrementAndGet());

                T call = attempt.doTry();

                return call;

            } catch (Throwable throwable) {

                LOGGER.warn(" X X X X X X X X Contingency attempt has failed while executing: " + attempt.getAttemptDescription() + ", will try up to " + attemptCount + " current try is " + count.get(), throwable);

                if (count.get() == attemptCount) {
                    break;
                }

                caught = throwable;

                try {
                    Thread.sleep(timeUntilNextAttempt);
                } catch (InterruptedException e) {
                }
            }
        } while (true);
        return null;
    }
}