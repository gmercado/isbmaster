package com.bisa.isb.api;

import com.google.common.base.Optional;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class to allow easy usage of String objects
 *
 * @author Miguel Vega
 * @version $Id: Texts.java; sep 17, 2015 03:57 PM mvega $
 */
public class Texts {
    @SafeVarargs
    public static <T> String build(T... texts) {
        StringBuilder builder = new StringBuilder();

        for (T text : texts) {
            builder.append(text);
        }

        return builder.toString();
    }

    public static boolean isNullOrEmpty(String input) {
        return input == null || input.trim().isEmpty();
    }

    /**
     * Produces a new String instance from all objects provided as an array
     *
     * @param values
     * @return
     */
    public static String asText(Object... values) {
        final StringBuilder stringBuffer = new StringBuilder();
        for (Object value : values) {
            Object or = Optional.fromNullable(value).or("");
            stringBuffer.append(or.toString());
        }
        return stringBuffer.toString();
    }

    /**
     * Search pattern is source text, where text is a REGEX. This only works for the first match.
     *
     * @param source
     * @param text
     * @return true if pattern is found in source text, false if not
     */
    public static Optional<String> stringBefore(String source, String text) {
        Pattern pattern = Pattern.compile(text);
        Matcher matcher = pattern.matcher(source);
        //only the first match will be reported
        if (matcher.find()) {
            return Optional.of(source.substring(0, matcher.start()));
        }
        return Optional.absent();
    }

    /**
     * Search pattern is source text, where text is a REGEX. This only works for the first match.
     *
     * @param source
     * @param text
     * @return true if pattern is found in source text, false if not
     */
    public static Optional<String> stringAfter(String source, String text) {
        Pattern pattern = Pattern.compile(text);
        Matcher matcher = pattern.matcher(source);
        //only the first match will be reported
        if (matcher.find()) {
            return Optional.of(source.substring(matcher.end()));
        }
        return Optional.absent();
    }

    /**
     * Retrieves the String value of the given object, if object is NULL, then return s an empty String.
     *
     * @param obj
     * @return
     */
    public static String toText(Object obj) {
        if (obj != null)
            return String.valueOf(obj);
        return "";
    }

    /**
     * Retrieves the value of given object, if object is
     *
     * @param obj
     * @return
     */
    public static String fromNullable(Object obj) {
        if (obj != null)
            return String.valueOf(obj);
        return null;
    }
}
