package com.bisa.isb.api.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.lang.reflect.ParameterizedType;

/**
 * @author Miguel Vega
 * @version $Id: JpaDao.java; nov 06, 2015 11:39 AM mvega $
 */
public abstract class SimpleDao<E, K> implements Dao<E, K> {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    protected Class<E> entityClass;

    /*
    @PersistenceContext
    protected EntityManager entityManager;
    */

    /**
     * If a EntityManager is injected via PersistentCOntex, no need to use the EntityManagerFactory, but we're
     * using GUICe, so need to use the factory to close each EntityManager instance
     */
    protected EntityManagerFactory entityManagerFactory;

    /**
     * While injecting is a must in this application, a new constructor has been enabled.
     * @param entityManagerFactory
     */
    public SimpleDao(EntityManagerFactory entityManagerFactory) {
        this();
        this.entityManagerFactory = entityManagerFactory;
    }

    public SimpleDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class) genericSuperclass.getActualTypeArguments()[0];
    }

    public E merge(E entity) {
        try {
            return executeAnyTx(entityManager -> entityManager.merge(entity));
        }catch(Throwable t){
            LOGGER.error("No pude guardar la data en la base de datos.", t);
            throw t;
        }
    }

    public void remove(E entity) {
        executeAnyTx(entityManager -> {
            entityManager.remove(entity);
            return null;
        });
    }

    public E findById(K id) {
        return executeAnyReadOnly(entityManager -> entityManager.find(entityClass, id));
    }

    protected <V, T extends Throwable> V executeAnyReadOnly(TransactionPerformer<V, T> callable)throws T{
        final EntityManager em = entityManagerFactory.createEntityManager();
        try {
            return callable.perform(em);
        } finally{
            em.close();
        }
    }

    protected <V, T extends Throwable> V executeAnyTx(TransactionPerformer<V, T> callable)throws T{
        final EntityManager em = entityManagerFactory.createEntityManager();
        final EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            V perform = callable.perform(em);
            tx.commit();
            return perform;
        } finally{
            if(tx.isActive())tx.rollback();
            em.close();
        }
    }
}