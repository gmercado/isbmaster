package com.bisa.isb.api.jpa;

import com.google.inject.Inject;
import bus.database.model.BasePrincipal;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceException;
import java.lang.annotation.Annotation;

/**
 * @author Miguel Vega
 * @version $Id: SafeEntityManagerFactory.java; oct 06, 2015 11:11 AM mvega $
 * @todo need to make the EntityManagerFactory dynamic
 */
public class SafeEntityManagerFactory {

    private final EntityManagerFactory entityManagerFactory;

    @Inject
    public SafeEntityManagerFactory(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super();
        this.entityManagerFactory = entityManagerFactory;
    }

    /**
     *
     * @param qualifiedEMF
     * @param transaction
     * @param <T>
     * @return
     * @throws javax.persistence.PersistenceException if any proble occurs while executing transaction
     */
    public <T> T doItWith(Class<? extends Annotation> qualifiedEMF, Transaction<EntityManager, T> transaction) throws PersistenceException{
        //EntityManagerFactory entityManagerFactory = injector.getBinding(Key.get(EntityManagerFactory.class, qualifiedEMF)).getProvider().get();
        final EntityManager em = entityManagerFactory.createEntityManager();
        try {
            return transaction.doTransaction(em);
        }catch(Throwable t){
            throw new javax.persistence.PersistenceException("An unexpected error has occurred while executing transaction.", t);
        }finally {
            em.close();
        }
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }
}
