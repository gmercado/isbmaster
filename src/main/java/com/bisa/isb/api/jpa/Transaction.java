package com.bisa.isb.api.jpa;

/**
 * @author Miguel Vega
 * @version $Id: Transaction.java; oct 06, 2015 11:20 AM mvega $
 */
public interface Transaction<U, V> {
    /**
     *
     * @param u
     * @return
     */
    V doTransaction(U u);
}
