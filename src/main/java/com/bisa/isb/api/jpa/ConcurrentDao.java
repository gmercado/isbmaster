package com.bisa.isb.api.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.lang.reflect.ParameterizedType;

/**
 * TODO, pending
 * A thread safe Dao implementation. Use Future to perform tasks a concurrent TransactionPerformed is required.
 * @author Miguel Vega
 * @version $Id: ConcurrentDao.java; nov 06, 2015 03:41 PM mvega $
 */
public class ConcurrentDao<E, K> implements Dao<E, K>{

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    protected Class<E> entityClass;

    /**
     * If a EntityManager is injected via PersistentCOntex, no need to use the EntityManagerFactory, but we're
     * using GUICe, so need to use the factory to close each EntityManager instance
     */
    protected EntityManagerFactory entityManagerFactory;

    /**
     * While injecting is a must in this application, a new constructor has been enabled.
     * @param entityManagerFactory
     */
    public ConcurrentDao(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class) genericSuperclass.getActualTypeArguments()[1];
    }

    @Override
    public E merge(E entity) {
        throw new UnsupportedOperationException("Not supportred yet");
    }

    @Override
    public void remove(E entity) {

    }

    @Override
    public E findById(K id) {
        return null;
    }

    protected <V, T extends Throwable> V executeAny(TransactionPerformer<V, T> callable)throws T{
        final EntityManager em = entityManagerFactory.createEntityManager();
        try {
            return callable.perform(em);
        } finally{
            em.close();
        }
    }
}
