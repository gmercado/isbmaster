package com.bisa.isb.api.jpa;

import javax.persistence.EntityManager;

/**
 * Interface to execute a code snippet with a JPA transaction.
 * @author Miguel Vega
 * @version $Id: ClauseExecutor.java; nov 06, 2015 02:33 PM mvega $
 */
public interface TransactionPerformer<U, T extends Throwable>{
    U perform(EntityManager entityManager) throws T;
}
