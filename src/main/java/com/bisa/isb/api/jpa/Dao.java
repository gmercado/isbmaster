package com.bisa.isb.api.jpa;

/**
 * E type determines the Entity class.<br/>
 * K, determines the PK type.
 * @author Miguel Vega
 * @version $Id: Dao.java; nov 06, 2015 11:36 AM mvega $
 */
public interface Dao<E, K> {
    E merge(E entity);
    void remove(E entity);
    E findById(K id);
}