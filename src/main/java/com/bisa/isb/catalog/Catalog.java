package com.bisa.isb.catalog;

import javax.xml.namespace.QName;
import java.util.List;
import java.util.Set;

/**
 * @author Miguel Vega
 * @version $Id: Catalog.java; sep 28, 2015 01:43 PM mvega $
 */
public interface Catalog<T> {
    /**
     * Adds the specified entry to this catalog. In some cases the catalog will be backed onto a
     * server, which may not allow for additions.
     * <p>
     * An IService may belong to more than one Catalog.
     * </p>
     *
     * @param t the item to add to the catalog
     * @throws UnsupportedOperationException
     */
    void add(T t) throws UnsupportedOperationException;

    /**
     * Removes the specified entry to this catalog. In some cases the catalog will be backed onto a
     * server, which may not allow for deletions.
     *
     * @param t
     * @throws UnsupportedOperationException
     */
    void remove(T t) throws UnsupportedOperationException;

    /**
     * Replaces the specified entry in this catalog. In some cases the catalog will be backed onto a
     * server, which may not allow for deletions.
     *
     * @param id
     * @param t
     * @throws UnsupportedOperationException
     */
    void replace(QName id, T t)
            throws UnsupportedOperationException;

    /**
     * Find resources matching this id directly from this Catalog.
     *
     * @param id      used to match resolves
     * @ param monitor used to show the progress of the find.
     * @return List (possibly empty) of resolves (objects implementing the
     * Resolve interface)
     */
    List<T> find(QName id);

    /**
     * Fetches all items in service catalog
     * @return
     */
    Set<T> fetchAll();

    /**
     * Find Service matching this id directly from this Catalog.  This method is guaranteed to be non-blocking.
     *
     * @param query   a URI used to match resolves
     * @param monitor monitor used to watch progress
     * @return a List (possibly empty) of matching services (objects of type
     * Service).
     */
//    List findService(URI query, ProgressListener monitor);

    /**
     * Performs a search on this catalog based on the specified inputs.
     * <p>
     * The pattern uses the following conventions:
     * <ul>
     * <li>
     * <li> use " " to surround a phase
     * <li> use + to represent 'AND'
     * <li> use - to represent 'OR'
     * <li> use ! to represent 'NOT'
     * <li> use ( ) to designate scope
     * </ul>
     * The bbox provided shall be in Lat - Long, or null if the search is not to be contained within
     * a specified area.
     * </p>
     *
     * @param pattern Search pattern (see above)
     * @param bbox    The bbox in Lat-Long (ESPG 4269), or null
     * @param monitor for progress, or null if monitoring is not desired
     * @return List containg objects of type Resolve.
     */
    /*List search(String pattern, Envelope bbox, ProgressListener monitor)
            throws IOException;*/
}
