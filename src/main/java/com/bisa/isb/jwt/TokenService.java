package com.bisa.isb.jwt;

import bus.env.api.MedioAmbiente;
import com.bisa.isb.commtools.CommonService;
import com.google.inject.Inject;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.impl.TextCodec;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.util.UUID;

import static bus.env.api.Variables.*;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * @author by rsalvatierra on 31/10/2017.
 */
@Path("/a/t")
@Produces({"application/json;charset=UTF-8"})
public class TokenService extends CommonService {

    private static final String APPLICATION = "application";

    @Inject
    private MedioAmbiente medioAmbiente;

    /**
     * Obtener token
     *
     * @param httpHeaders headers
     * @param aplication  aplicacion
     * @param user        usuario
     * @return String
     */
    @GET
    @Path("/getToken/{application}/{user}")
    @Consumes(APPLICATION_JSON)
    public String getToken(@Context HttpHeaders httpHeaders,
                           @PathParam("application") final String aplication,
                           @PathParam("user") final String user) {
        if (user == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            String id = UUID.randomUUID().toString().replace("-", "");
            DateTime currentTime = new DateTime();
            return Jwts.builder()
                    .setId(id)
                    .setSubject(user)
                    .claim(APPLICATION, aplication)
                    //.compressWith(CompressionCodecs.DEFLATE)
                    .signWith(SignatureAlgorithm.HS512,
                            getKey())
                    .setIssuedAt(currentTime.toDate())
                    .setNotBefore(currentTime.toDate())
                    .setExpiration(currentTime.plusMinutes(getMinutes()).toDate())
                    .compact();
        }
    }

    /**
     * Validar token
     *
     * @param httpHeaders headers
     * @param aplication  aplicacion
     * @param token       token
     * @return String
     */
    @GET
    @Path("/validateToken/{application}/{token}")
    @Consumes(APPLICATION_JSON)
    public String validateToken(@Context HttpHeaders httpHeaders,
                                @PathParam("application") final String aplication,
                                @PathParam("token") final String token) {
        String result;
        if (token == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            try {
                Jwts.parser()
                        .require(APPLICATION, aplication)
                        .setSigningKey(getKey())
                        .parseClaimsJws(token);
                result = "OK";
            } catch (Exception e) {
                result = "NOK";

            }
        }
        return result;
    }

    private byte[] getKey() {
        return TextCodec.BASE64.decode(medioAmbiente.getValorDe(LLAVE_TOKEN_WEB, LLAVE_TOKEN_WEB_DEFAULT));
    }

    private int getMinutes() {
        return medioAmbiente.getValorIntDe(TIEMPO_VIGENCIA_TOKEN_WEB, TIEMPO_VIGENCIA_TOKEN_WEB_DEFAULT);
    }
}
