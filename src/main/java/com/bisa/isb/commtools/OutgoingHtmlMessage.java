package com.bisa.isb.commtools;


import com.google.common.base.Preconditions;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.PasswordAuthentication;
import java.nio.charset.Charset;
import java.util.NoSuchElementException;

import static java.util.Arrays.asList;

/**
 * @author Miguel Vega
 * @version $Id: EmailSender.java; ene 14, 2016 03:43 PM mvega $
 */
public class OutgoingHtmlMessage implements OutgoingMessage<EmailException> {
    final HtmlEmail email;

    Sender sender;

    final static Logger logger = LoggerFactory.getLogger(OutgoingHtmlMessage.class);

    private Charset charset = null;

    OutgoingHtmlMessage(Settings settings) {
        super();
        this.email = new HtmlEmail();//new HtmlEmail(); MultiPartEmail

        try {
            Value<PasswordAuthentication> val = (Value<PasswordAuthentication>) settings.values.stream().
                    filter(value -> PasswordAuthentication.class.isAssignableFrom(value.param.type)).findFirst().get();
            email.setAuthentication(val.v.getUserName(), val.v.getPassword());
        }catch(NoSuchElementException x){

        }

        settings.configureSettings(email);
    }

    public OutgoingHtmlMessage from(Sender sender){
        this.sender = sender;
        return this;
    }

    public OutgoingHtmlMessage withSubject(String subject){
        email.setSubject(subject);
        return this;
    }

    /**
     * Specify the direct recipients
     * todo, need to specify the charset while adding a destination
     * @param receivers
     * @return
     */
    public OutgoingHtmlMessage to(Receiver... receivers){
        Preconditions.checkArgument(receivers !=null && receivers.length>0);
        for (Receiver receiver : receivers) {
            try {
                email.addTo(receiver.getTo(), receiver.getName());
            } catch (EmailException e) {
                //this was controlled while creating the recipient
            }
        }
        return this;
    }

    /**
     * Specify the carbon copy recipients
     * todo, need to specify the charset while adding a destination
     * @param receivers
     * @return
     */
    public OutgoingHtmlMessage toCC(Receiver... receivers){
        Preconditions.checkArgument(receivers !=null && receivers.length>0);

        asList(receivers).stream().forEach(receiver -> {
            try {
                email.addCc(receiver.getTo(), receiver.getName());
            } catch (EmailException e) {
                logger.warn("Attempting to send email to an invalid address of CC");
            }
        });

        return this;
    }

    /**
     * Specify the blind carbon copy recipients
     * todo, need to specify the charset while adding a destination
     * @param receivers
     * @return
     */
    public OutgoingHtmlMessage toBCC(Receiver... receivers){
        Preconditions.checkArgument(receivers !=null && receivers.length>0);

        asList(receivers).stream().forEach(receiver -> {
            try {
                email.addBcc(receiver.getTo(), receiver.getName());
            } catch (EmailException e) {
                logger.warn("Attempting to send email to an invalid address of CC");
            }
        });

        return this;
    }

    public OutgoingHtmlMessage withMessage(String message){
        try {
            email.setHtmlMsg(message);
            //email.setMsg(message);
        } catch (EmailException e) {
            throw new IllegalArgumentException("The message provided had a problem", e);
        }
        return this;
    }

    public void send() throws EmailException {
        email.send();
    }

    public void attach(){
        // Create the attachment
        EmailAttachment attachment = new EmailAttachment();
        attachment.setPath("mypictures/john.jpg");
        attachment.setDisposition(EmailAttachment.ATTACHMENT);
        attachment.setDescription("Picture of John");
        attachment.setName("John");
    }

    public Charset getCharset() {
        return charset;
    }

    public void setCharset(Charset charset) {
        this.charset = charset;
    }
}
