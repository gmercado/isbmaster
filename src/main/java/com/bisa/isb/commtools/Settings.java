package com.bisa.isb.commtools;

import com.bisa.isb.commtools.Value.Param;
import com.google.common.collect.Lists;
import org.apache.commons.mail.DefaultAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.PasswordAuthentication;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * @author Miguel Vega
 * @version $Id: Settings.java; ene 14, 2016 05:08 PM mvega $
 */
public class Settings {

    final List<Value<?>> values = Lists.newLinkedList();

    public static Param<Class<? extends OutgoingMessage>> MessageType = new Param(OutgoingMessage.class, "messaging", null, false);

    public static Param<Boolean> Tls = new Param<Boolean>(false, "email", "Tls");
    public static Param<Boolean> StartTlsEnabled = new Param(false, "email", "Tls");
    public static Param<String> HostName = new Param("smtp.local.domain", "email", "HostName");
    public static Param<Integer> SmtpPort = new Param(587, "email", "SmtpPort");
//    public static Param<Object> Authenticator = new Param(javax.mail.Authenticator.class, new DefaultAuthenticator("nobody", "nobody"), "email", "Authenticator");
    public static Param<javax.mail.Authenticator> Authenticator = new Param(javax.mail.Authenticator.class, new DefaultAuthenticator("nobody", "nobody"), "email", "Authenticator");
    public static Param<PasswordAuthentication> Authentication = new Param(PasswordAuthentication.class, new PasswordAuthentication("any", "nobody"), "email", "Authentication");

    private static Logger logger = LoggerFactory.getLogger(Settings.class);

    /**
     * Adds a new parameter to configure the Settings while sending a message via email or SMS.
     * @param value
     * @param <U>
     * @return
     */
    public <U> Settings addParam(Value<U> value){
        values.add(value);
        return this;
    }

    /**
     * This method parses all values within and applies to the target object using reflection.
     * Initially uses the setters. If they don[t exist, use the field to make a straight placing.
     * @param target
     */
    protected void configureSettings(final Object target){
        for (Value<?> value : values) {

            if(!value.param.typeProperty)continue;

            if(PasswordAuthentication.class.isAssignableFrom(value.param.getType())){
                continue;
            }

            final String setter = "set"+value.param.name;
//            final String setter = value.param.name;
            try {
                final Object v = value.v;

                final Method method = target.getClass().getMethod(setter, v.getClass());
                //Integer.class!=int.class
                method.invoke(target, v);
//                BeanUtilsBean2.getInstance().setProperty(target, value.param.name, v);
            } catch (NoSuchMethodException e) {
                logger.warn("Method '{}' with param type '{}' does not exist for object of type '{}'", setter,
                        value.v.getClass(),
                        target.getClass());

                /*final Method[] methods = target.getClass().getMethods();
                for (Method method : methods) {
                    logger.info(">>>>>>" + method.getName() + ", params.." + Arrays.asList(method.getParameterTypes()));
                }*/

            } catch (InvocationTargetException e) {
                logger.warn("An error has ocurred while applying settings", e);
            } catch (IllegalAccessException e) {
                logger.warn("An error has ocurred while applying settings", e);
            }
        }
    }
}
