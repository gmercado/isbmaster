package com.bisa.isb.commtools;

/**
 * @author Miguel Vega
 * @version $Id: OutgoingEmail.java; ene 29, 2016 12:44 PM mvega $
 */
public interface OutgoingMessage <X extends Throwable>{

    OutgoingMessage<X> from(Sender sender);

    OutgoingMessage<X> to(Receiver... sender);
    /**
     * This is wher the user defines the text to be send as the email body.
     * @param message
     * @return
     */
    OutgoingMessage<X> withMessage(String message);

    void send() throws X;
}