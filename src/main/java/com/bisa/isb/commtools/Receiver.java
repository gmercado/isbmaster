package com.bisa.isb.commtools;

/**
 * @author Miguel Vega
 * @version $Id: Sender.java; ene 14, 2016 03:57 PM mvega $
 */
public class Receiver {

    private final String name;
    private final String to;

    public Receiver(String name, String to) {
        this.name = name;
        this.to = to;
    }

    public String getName() {
        return name;
    }

    public String getTo() {
        return to;
    }
}
