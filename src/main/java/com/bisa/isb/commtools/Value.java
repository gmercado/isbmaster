package com.bisa.isb.commtools;

/**
 * @author Miguel Vega
 * @version $Id: Value.java; Jan 31, 2016. 7:10 PM mvega $
 * @source $URL$
 */
public class Value <V>{
    final V v;
    final Param<V> param;
    /**
     * Determines if the value must be used to inject via setter method into the target object while applying
     * values in the {@link Settings#configureSettings(Object)} method.
     * Otherwise might be used for another process.
     */

    Value(Param<V> param, V v) {
        this.v = v;
        this.param = param;
    }

    public static <V> Value<V> of(Param<V> param, V v){
        return new Value<V>(param, v);
    }

    public static class Param <T>{
        final boolean typeProperty;
        final Class<T> type;
        final T sample;
        final String name, group;

        Param(Class<T> type, T notNullSample, String group, String name) {
            this(type, notNullSample, group, name, true);
        }

        Param(Class<T> type, T notNullSample, String group, String name, boolean typeProperty) {
            this.group = group;
            this.name = name;

            this.sample = notNullSample;
            this.type = type;

            this.typeProperty = typeProperty;
        }

        /**
         * TODO fails when recovering the type while creating the setter method parameter class.
         * TODO Use explicit type parameter in other constructor if variable is a subclas or implementation of interface
         * @param notNullSample
         * @param group
         * @param name
         */
        Param(T notNullSample, String group, String name) {
            this(notNullSample, group, name, true);
        }

        Param(T notNullSample, String group, String name, boolean typeProperty) {
            assert notNullSample!=null;

            this.group = group;
            this.name = name;

            this.typeProperty = typeProperty;

            this.sample = notNullSample;
            this.type = (Class<T>) notNullSample.getClass();
            /*this.type = (Class<T>) ((Param) getClass()
                    .getGenericSuperclass()).getActualTypeArguments()[0];*/
        }

        public Class<T> getType() {
            return type;
        }

        public T getSample() {
            return sample;
        }
    }
}
