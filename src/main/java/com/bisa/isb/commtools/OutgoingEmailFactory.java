package com.bisa.isb.commtools;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.NoSuchElementException;

/**
 * @author Miguel Vega
 * @version $Id: OutgoingEmailFactory.java; ene 29, 2016 02:08 PM mvega $
 */
public class OutgoingEmailFactory {
    //factory body
    public static OutgoingMessage configure(Settings settings){
        Class<OutgoingMessage> om = null;
        try {
            final Value<Class<OutgoingMessage>> first = (Value<Class<OutgoingMessage>>) settings.values.stream().
                    filter(value -> {
                        final Object v = value.v;
                        if(!(v instanceof Class))return false;
                        return OutgoingMessage.class.isAssignableFrom((Class<?>) v);
                    }).
                    findFirst().get();

            om = first.v;

            final Constructor<OutgoingMessage> constructor = om.getDeclaredConstructor(Settings.class);
            constructor.setAccessible(true);
            return constructor.newInstance(settings);
        }catch(NoSuchElementException x){
            throw new IllegalStateException("A parameter of type OutgoingMessage was expected in settings, but not found.");
        } catch (InvocationTargetException e) {
            throw new IllegalStateException("Unable to create an outgoing instance", e);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException("Unable to create an outgoing instance for type '"+om+"'. Constructor with parameter of type Settings not found", e);
        } catch (InstantiationException e) {
            throw new IllegalStateException("Unable to create an outgoing instance. Constructor failed", e);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("Unable to create an outgoing instance. Access not allowed.", e);
        }
    }
}
