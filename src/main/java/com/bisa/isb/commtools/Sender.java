package com.bisa.isb.commtools;

/**
 * @author Miguel Vega
 * @version $Id: Sender.java; ene 14, 2016 03:57 PM mvega $
 */
public class Sender {
    private final String name;
    private final String from;

    public Sender(String name, String from) {
        this.name = name;
        this.from = from;
    }

    public String getName() {
        return name;
    }

    public String getFrom() {
        return from;
    }
}
