package com.bisa.isb.commtools;

import java.nio.charset.Charset;

/**
 * @author Miguel Vega
 * @version $Id: EncodedString.java; ene 29, 2016 01:53 PM mvega $
 */
public class EncodedString {
    final String text;
    final Charset charset;

    public EncodedString(String text) {
        this(text, Charset.defaultCharset());
    }

    public EncodedString(String text, Charset charset) {
        this.text = text;
        this.charset = charset;
    }

    public String getText() {
        return text;
    }

    public Charset getCharset() {
        return charset;
    }
}
