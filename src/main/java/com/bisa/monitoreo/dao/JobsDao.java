package com.bisa.monitoreo.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.monitoreo.entities.JobInfo;
import com.bisa.monitoreo.entities.JobInfoTotal;
import com.bisa.monitoreo.entities.JobMsg;
import com.google.inject.Inject;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.security.InvalidParameterException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by ccalle on 21/11/2017.
 */
public class JobsDao extends DaoImpl<JobInfo, Long> {
    private static final Logger LOGGER = LoggerFactory.getLogger(JobsDao.class);
    private static final String QUERY_JOBS_POR_IP="insert into ISBP70 (I70VJOBN, I70VUSR, I70VIP,I70VJST) "+
            "SELECT A.JOB_NAME, A.AUTHORIZATION_NAME, B.REMOTE_ADDRESS,A.JOB_STATUS "+
            "FROM "+
            "( select job_name, AUTHORIZATION_NAME, JOB_STATUS "+
            "FROM TABLE(QSYS2.ACTIVE_JOB_INFO(JOB_NAME_FILTER => 'QZDASOINIT')) AJ "+
            "where AUTHORIZATION_NAME in(%s)  "+
            "AND JOB_STATUS NOT IN ('END', 'EOJ', 'PSRW') "+
            ")A, "+
            "( SELECT REMOTE_ADDRESS,AUTHORIZATION_NAME,JOB_NAME FROM QSYS2.NETSTAT_JOB_INFO  "+
            "WHERE REMOTE_ADDRESS IN (%s ) "+
            ")B WHERE "+
            "A.JOB_NAME=B.JOB_NAME";
    private static final String QUERY_HISTORICO="insert into ISBP70H (I70DFECHA,I70VJOBN, I70VUSR, I70VIP,I70VJST) " +
            "select I70DFECHA,I70VJOBN, I70VUSR, I70VIP,I70VJST from ISBP70";

    private static final String QUERY_DELETE="delete from ISBP70 ";
    private final MedioAmbiente medioAmbiente;
    @Inject
    protected JobsDao(@BasePrincipal EntityManagerFactory entityManagerFactory, MedioAmbiente
            medioAmbiente) {
        super(entityManagerFactory);
        this.medioAmbiente=medioAmbiente;
    }
    public Integer registrarJobStatusHistorico() throws InvalidParameterException {

        return doWithTransaction(new WithTransaction<Integer>() {
            @Override
            public Integer call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {


                Query queryUpdate = entityManager.createNativeQuery(QUERY_HISTORICO);
                LOGGER.info("Query {}",QUERY_HISTORICO);
                Integer result = queryUpdate.executeUpdate();
                LOGGER.info("Se registro en ISBP70h . Rows affected:{}", result);

                Query queryDelete = entityManager.createNativeQuery(QUERY_DELETE);
                LOGGER.info("Query {}",QUERY_DELETE);
                result = queryDelete.executeUpdate();
                LOGGER.info("Se ELIMINO ISBP70 . Rows affected:{}", result);

                return result;
            }
        });
    }
    public Integer registrarJobStatus() throws InvalidParameterException {

        return doWithTransaction(new WithTransaction<Integer>() {
            @Override
            public Integer call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                String usuarios = medioAmbiente.getValorDe(Variables.JOBS_MONITOR_USERS, Variables.JOBS_MONITOR_USERS_DEFAULT);
                String addressIP = medioAmbiente.getValorDe(Variables.JOBS_MONITOR_IP, Variables.JOBS_MONITOR_IP_DEFAULT);


                Query queryUpdate = entityManager.createNativeQuery(String.format(QUERY_JOBS_POR_IP,usuarios,addressIP));
                LOGGER.info("Query {}",QUERY_JOBS_POR_IP);

                Integer result = queryUpdate.executeUpdate();
                LOGGER.info("Se registro en ISBP70 . Rows affected:{}", result);
                return result;
            }
        });
    }

    public List<JobInfo> getReciente() {
        LOGGER.debug("Obteniendo job info reciente");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<JobInfo> q = cb.createQuery(JobInfo.class);
                        Root<JobInfo> p = q.from(JobInfo.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<JobInfo> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta: {}", e);
        }
        return null;
    }
    @SuppressWarnings({"unchecked"})
    public List<JobInfoTotal> getTotalPorIP() {
        LOGGER.debug("Obteniendo total por ip");
        return doWithTransaction(
                (entityManager, t) -> {
                    String sql = "SELECT I70VUSR as user,I70VIP as addressIP, count(*) as total from ISBP70 group by I70VUSR,I70VIP";

                    Query q = entityManager.createNativeQuery(sql, JobInfoTotal.class);;
                    List<JobInfoTotal> jobInfoTotal = null;
                    try {
                        jobInfoTotal = (List<JobInfoTotal>) q.getResultList();
                    } catch (NonUniqueResultException e) {
                        LOGGER.error("Error al obtener el total de jobs por ip ", e);
                        return null;
                    } catch (Exception e) {
                        LOGGER.error("Error al obtener el total de jobs por ip ", e);
                        return null;
                    }
                    return jobInfoTotal;
                });
    }
    @SuppressWarnings({"unchecked"})
    public List<JobMsg> getJobMessages(String jobName) {
        LOGGER.debug("Obteniendo total por ip");
        return doWithTransaction(
                (entityManager, t) -> {
                    String sql = "SELECT VARCHAR_FORMAT(MESSAGE_TIMESTAMP, 'YYYY-MM-DD HH24:MI:SS') as fecha, MESSAGE_TEXT as mensaje " +
                            "FROM TABLE(QSYS2.JOBLOG_INFO('"+jobName+"')) A  " +
                            "ORDER BY MESSAGE_TIMESTAMP ASC";

                    Query q = entityManager.createNativeQuery(sql, JobMsg.class);;
                    List<JobMsg> jobMsg = null;
                    try {
                        jobMsg = (List<JobMsg>) q.getResultList();
                    } catch (NonUniqueResultException e) {
                        LOGGER.error("Error al obtener mensajes del job ", e);
                        return null;
                    } catch (Exception e) {
                        LOGGER.error("Error al obtener mensajes del job ", e);
                        return null;
                    }
                    return jobMsg;
                });
    }
}
