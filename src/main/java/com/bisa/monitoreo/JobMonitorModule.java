package com.bisa.monitoreo;

import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.indicadoresbcb.dao.CotizacionBolsinDao;
import com.bisa.monitoreo.sched.JobsMonitorJob;

/**
 * Created by ccalle on 21/11/2017.
 */
public class JobMonitorModule extends AbstractModuleBisa {
    @Override
    protected void configure() {
        bind(CotizacionBolsinDao.class);
/*
Seconds         0
Minutes         10
Hours           0/1
Day of month    1/1
Month           *
Day of week     *
Year            *
 	0 0 0/1 1/1 * ? *
         */
        bindSched("JobsMonitorJob", "JOB", "0 0 0/1 1/1 * ?", JobsMonitorJob.class);

        bindUI("com/bisa/monitoreo/ui");
    }

}