package com.bisa.monitoreo.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPanel;
import com.bisa.monitoreo.dao.JobsDao;
import com.bisa.monitoreo.entities.JobInfo;
import com.bisa.monitoreo.entities.JobInfoTotal;
import com.bisa.monitoreo.entities.JobMsg;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by ccalle on 21/11/2017.
 */
@AuthorizeInstantiation({RolesBisa.JOB_MONITOR_POR_IP, RolesBisa.ISB_ADMIN})
@Menu(value = "Jobs por IP y usuario", subMenu = SubMenu.JOBS_IP)
public class JobsMonitor extends BisaWebPage {
    private static final long serialVersionUID = -1374736908731954160L;
    private static final Logger LOGGER = LoggerFactory.getLogger(JobsMonitor.class);
    public JobsMonitor(final PageParameters parameters) {
        super(parameters);


        add(new ListView("listview_total", getInstance(JobsDao.class).getTotalPorIP()) {
            protected void populateItem(ListItem item) {
                JobInfoTotal elemento= (JobInfoTotal) item.getModelObject();
                item.add(new Label("total_usuario", elemento.getUser()));
                item.add(new Label("total_ip", elemento.getAddressIP()));
                item.add(new Label("total_cantidad", elemento.getTotal()));

            }
        });
        add(new ListView("listview_detalle", getInstance(JobsDao.class).getReciente()) {
            protected void populateItem(ListItem item) {
                JobInfo elemento= (JobInfo) item.getModelObject();
                item.add(new Label("detalle_fecha", elemento.getFecha()));
                WebMarkupContainer span = new WebMarkupContainer("detalle_jobname");


                item.add(new LinkPanel<JobInfo>("detalle_jobname", Model.of(elemento.getJobName()), item.getModel()) {

                    private static final long serialVersionUID = 1L;

                    @Override
                    protected AbstractLink newLink(String linkId, IModel<JobInfo> objectModel) {
                        return new AjaxLink<JobInfo>(linkId, objectModel) {

                            private static final long serialVersionUID = 1L;

                            @Override
                            public void onClick(AjaxRequestTarget target) {
                                LOGGER.info("getModelObject().getJobName()");

                                List<JobMsg> lstMsg= getInstance(JobsDao.class).getJobMessages(objectModel.getObject().getJobName());
                                StringBuffer tablaMsg= new StringBuffer();
                                for (JobMsg msg : lstMsg) {
                                    tablaMsg.append("<tr><td class=\"text-success\">").append(msg.getFecha()).append("</td><td class=\"text-success\">").append(msg.getMensaje()).append("</td></tr>");
                                }

                                String titulo="Mensajes del job: "+objectModel.getObject().getJobName();
                                target.appendJavaScript("$('#myModalLabel').html('"+titulo+"');");
                                target.appendJavaScript("$('#job_msg tbody').html('"+tablaMsg.toString()+"');");

                                target.appendJavaScript("$('#myModalExtra').modal('show');return false;");
                            }
                        };
                    }

                });


                item.add(new Label("detalle_user", elemento.getUsuario()));
                item.add(new Label("detalle_ip", elemento.getAddressIP()));
                item.add(new Label("detalle_jobstatus", elemento.getJobStatus()));

            }
        });
    }
}
