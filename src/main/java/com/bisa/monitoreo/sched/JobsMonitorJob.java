package com.bisa.monitoreo.sched;

import bus.env.api.MedioAmbiente;
import bus.mail.api.MailerFactory;
import com.bisa.indicadoresbcb.dao.CotizacionBolsinDao;
import com.bisa.indicadoresbcb.sched.VerificadorTasaTREJob;
import com.bisa.monitoreo.dao.JobsDao;
import com.bisa.monitoreo.entities.JobInfo;
import com.bisa.monitoreo.entities.JobInfoTotal;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * Created by ccalle on 21/11/2017.
 */
public class JobsMonitorJob implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(VerificadorTasaTREJob.class);
    private final MedioAmbiente medioAmbiente;
    private final MailerFactory mailerFactory;
    private final JobsDao jobsDao;

    @Inject
    public JobsMonitorJob(MedioAmbiente medioAmbiente, MailerFactory mailerFactory,JobsDao jobsDao) {
        this.medioAmbiente = medioAmbiente;
        this.mailerFactory = mailerFactory;
        this.jobsDao = jobsDao;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.debug("Iniciando tarea");
        jobsDao.registrarJobStatusHistorico();
        jobsDao.registrarJobStatus();
        LOGGER.debug("Tarea finalizada tarea");
    }


}