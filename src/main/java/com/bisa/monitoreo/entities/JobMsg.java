package com.bisa.monitoreo.entities;

import java.io.Serializable;

/**
 * Created by ccalle on 22/11/2017.
 */
public class JobMsg implements Serializable {

    private String fecha;

    private String mensaje;

    public JobMsg() {
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
