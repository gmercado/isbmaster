package com.bisa.monitoreo.entities;

import java.io.Serializable;

/**
 * Created by ccalle on 21/11/2017.
 */
public class JobInfoTotal implements Serializable {

    private String user;

    private String addressIP;

    private long total;

    public JobInfoTotal() {
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getAddressIP() {
        return addressIP;
    }

    public void setAddressIP(String addressIP) {
        this.addressIP = addressIP;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }
}
