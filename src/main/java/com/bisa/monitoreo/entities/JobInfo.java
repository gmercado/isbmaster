package com.bisa.monitoreo.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by ccalle on 21/11/2017.
 */
@Entity
@Table(name = "ISBP70")
public class JobInfo implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I70NID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "I70DFECHA")
    private Date fecha;

    @Column(name = "I70VJOBN")
    private String jobName;

    @Column(name = "I70VUSR")
    private String usuario;
    @Column(name = "I70VIP")
    private String addressIP;
    @Column(name = "I70VJST")
    private String jobStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getAddressIP() {
        return addressIP;
    }

    public void setAddressIP(String addressIP) {
        this.addressIP = addressIP;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }
}
