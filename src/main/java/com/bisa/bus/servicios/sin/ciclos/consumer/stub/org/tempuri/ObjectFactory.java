package com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.tempuri;

import com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.datacontract.Respuesta;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the org.tempuri package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PingResponsePingResult_QNAME = new QName("http://tempuri.org/", "pingResult");
    private final static QName _PruebaConexionUsuario_QNAME = new QName("http://tempuri.org/", "usuario");
    private final static QName _PruebaConexionResponsePruebaConexionResult_QNAME = new QName("http://tempuri.org/", "pruebaConexionResult");
    private final static QName _IniciarCicloCodigoCaracteristica_QNAME = new QName("http://tempuri.org/", "codigoCaracteristica");
    private final static QName _IniciarCicloTipoAutorizacion_QNAME = new QName("http://tempuri.org/", "tipoAutorizacion");
    private final static QName _IniciarCicloResponseIniciarCicloResult_QNAME = new QName("http://tempuri.org/", "iniciarCicloResult");
    private final static QName _ConfirmarInicioETicket_QNAME = new QName("http://tempuri.org/", "eTicket");
    private final static QName _ConfirmarInicioResponseConfirmarInicioResult_QNAME = new QName("http://tempuri.org/", "confirmarInicioResult");
    private final static QName _FinalizarCicloArchivo_QNAME = new QName("http://tempuri.org/", "archivo");
    private final static QName _FinalizarCicloResponseFinalizarCicloResult_QNAME = new QName("http://tempuri.org/", "finalizarCicloResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Ping }
     */
    public Ping createPing() {
        return new Ping();
    }

    /**
     * Create an instance of {@link PingResponse }
     */
    public PingResponse createPingResponse() {
        return new PingResponse();
    }

    /**
     * Create an instance of {@link PruebaConexion }
     */
    public PruebaConexion createPruebaConexion() {
        return new PruebaConexion();
    }

    /**
     * Create an instance of {@link PruebaConexionResponse }
     */
    public PruebaConexionResponse createPruebaConexionResponse() {
        return new PruebaConexionResponse();
    }

    /**
     * Create an instance of {@link IniciarCiclo }
     */
    public IniciarCiclo createIniciarCiclo() {
        return new IniciarCiclo();
    }

    /**
     * Create an instance of {@link IniciarCicloResponse }
     */
    public IniciarCicloResponse createIniciarCicloResponse() {
        return new IniciarCicloResponse();
    }

    /**
     * Create an instance of {@link ConfirmarInicio }
     */
    public ConfirmarInicio createConfirmarInicio() {
        return new ConfirmarInicio();
    }

    /**
     * Create an instance of {@link ConfirmarInicioResponse }
     */
    public ConfirmarInicioResponse createConfirmarInicioResponse() {
        return new ConfirmarInicioResponse();
    }

    /**
     * Create an instance of {@link FinalizarCiclo }
     */
    public FinalizarCiclo createFinalizarCiclo() {
        return new FinalizarCiclo();
    }

    /**
     * Create an instance of {@link FinalizarCicloResponse }
     */
    public FinalizarCicloResponse createFinalizarCicloResponse() {
        return new FinalizarCicloResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pingResult", scope = PingResponse.class)
    public JAXBElement<String> createPingResponsePingResult(String value) {
        return new JAXBElement<String>(_PingResponsePingResult_QNAME, String.class, PingResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "usuario", scope = PruebaConexion.class)
    public JAXBElement<String> createPruebaConexionUsuario(String value) {
        return new JAXBElement<String>(_PruebaConexionUsuario_QNAME, String.class, PruebaConexion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pruebaConexionResult", scope = PruebaConexionResponse.class)
    public JAXBElement<String> createPruebaConexionResponsePruebaConexionResult(String value) {
        return new JAXBElement<String>(_PruebaConexionResponsePruebaConexionResult_QNAME, String.class, PruebaConexionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "codigoCaracteristica", scope = IniciarCiclo.class)
    public JAXBElement<String> createIniciarCicloCodigoCaracteristica(String value) {
        return new JAXBElement<String>(_IniciarCicloCodigoCaracteristica_QNAME, String.class, IniciarCiclo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "tipoAutorizacion", scope = IniciarCiclo.class)
    public JAXBElement<String> createIniciarCicloTipoAutorizacion(String value) {
        return new JAXBElement<String>(_IniciarCicloTipoAutorizacion_QNAME, String.class, IniciarCiclo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "usuario", scope = IniciarCiclo.class)
    public JAXBElement<String> createIniciarCicloUsuario(String value) {
        return new JAXBElement<String>(_PruebaConexionUsuario_QNAME, String.class, IniciarCiclo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Respuesta }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "iniciarCicloResult", scope = IniciarCicloResponse.class)
    public JAXBElement<Respuesta> createIniciarCicloResponseIniciarCicloResult(Respuesta value) {
        return new JAXBElement<Respuesta>(_IniciarCicloResponseIniciarCicloResult_QNAME, Respuesta.class, IniciarCicloResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "eTicket", scope = ConfirmarInicio.class)
    public JAXBElement<String> createConfirmarInicioETicket(String value) {
        return new JAXBElement<String>(_ConfirmarInicioETicket_QNAME, String.class, ConfirmarInicio.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "usuario", scope = ConfirmarInicio.class)
    public JAXBElement<String> createConfirmarInicioUsuario(String value) {
        return new JAXBElement<String>(_PruebaConexionUsuario_QNAME, String.class, ConfirmarInicio.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Respuesta }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "confirmarInicioResult", scope = ConfirmarInicioResponse.class)
    public JAXBElement<Respuesta> createConfirmarInicioResponseConfirmarInicioResult(Respuesta value) {
        return new JAXBElement<Respuesta>(_ConfirmarInicioResponseConfirmarInicioResult_QNAME, Respuesta.class, ConfirmarInicioResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "archivo", scope = FinalizarCiclo.class)
    public JAXBElement<byte[]> createFinalizarCicloArchivo(byte[] value) {
        return new JAXBElement<>(_FinalizarCicloArchivo_QNAME, byte[].class, FinalizarCiclo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "eTicket", scope = FinalizarCiclo.class)
    public JAXBElement<String> createFinalizarCicloETicket(String value) {
        return new JAXBElement<String>(_ConfirmarInicioETicket_QNAME, String.class, FinalizarCiclo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "usuario", scope = FinalizarCiclo.class)
    public JAXBElement<String> createFinalizarCicloUsuario(String value) {
        return new JAXBElement<String>(_PruebaConexionUsuario_QNAME, String.class, FinalizarCiclo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Respuesta }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "finalizarCicloResult", scope = FinalizarCicloResponse.class)
    public JAXBElement<Respuesta> createFinalizarCicloResponseFinalizarCicloResult(Respuesta value) {
        return new JAXBElement<Respuesta>(_FinalizarCicloResponseFinalizarCicloResult_QNAME, Respuesta.class, FinalizarCicloResponse.class, value);
    }

}
