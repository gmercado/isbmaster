package com.bisa.bus.servicios.sin.ciclos.model;

/**
 * @author by rsalvatierra on 06/01/2017.
 */
public enum EstadoFactura {

    ACTIVO("A"),
    DEPURADO("D");

    private String descripcion;

    EstadoFactura(String descripcion) {
        this.descripcion = descripcion;
    }

    public static EstadoFactura get(String codigo) {
        for (EstadoFactura valor : EstadoFactura.values()) {
            if (valor.getDescripcion().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
