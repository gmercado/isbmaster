package com.bisa.bus.servicios.sin.ciclos.api;

import bus.env.api.MedioAmbiente;
import bus.plumbing.compress.Compress7z;
import bus.plumbing.compress.Item;
import bus.plumbing.file.ArchivoBase;
import bus.plumbing.file.ArchivoProfile;
import bus.plumbing.utils.Convert;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.Monedas;
import com.bisa.bus.servicios.sin.ciclos.dao.FacturaDao;
import com.bisa.bus.servicios.sin.ciclos.entities.DosificacionAutomatica;
import com.bisa.bus.servicios.sin.ciclos.entities.Factura;
import com.bisa.bus.servicios.sin.ciclos.model.CierreDosificacion;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import static bus.env.api.Variables.*;


/**
 * @author by rsalvatierra on 07/03/2017.
 */
public class ArchivoCierre {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArchivoCierre.class);
    private final MedioAmbiente medioAmbiente;
    private final ArchivoBase archivoBase;
    private final FacturaDao facturaDao;
    private long nroFilas;
    private long valorFilasEfectivas;
    private BigDecimal montoTotal;
    private String separador;

    @Inject
    public ArchivoCierre(MedioAmbiente medioAmbiente, ArchivoBase archivoBase, FacturaDao facturaDao) {
        this.medioAmbiente = medioAmbiente;
        this.archivoBase = archivoBase;
        this.facturaDao = facturaDao;
    }

    public CierreDosificacion generaArchivo(DosificacionAutomatica dosificacionAutomatica) {
        File archivoTexto, archivoCompreso;
        ArchivoProfile archivoProfile;
        String nombreArchivoTexto, nombreArchivoCompreso;
        CierreDosificacion cierreDosificacion = new CierreDosificacion();
        String nombreArchivo = medioAmbiente.getValorDe(SIN_NOMBRE_ARCHIVO, SIN_NOMBRE_ARCHIVO_DEFAULT);
        separador = medioAmbiente.getValorDe(SIN_SEPARADOR_ARCHIVO, SIN_SEPARADOR_ARCHIVO_DEFAULT);
        nombreArchivoTexto = "" + FormatosUtils.fechaFormateadaConYYMMDD(new Date()) + nombreArchivo + ".txt";
        nombreArchivoCompreso = "" + FormatosUtils.fechaFormateadaConYYMMDD(new Date()) + nombreArchivo + ".7z";
        try {
            List<Factura> facturaList = facturaDao.getFacturasProcesadasByNumAut(dosificacionAutomatica.getNumeroAutorizacion().toString());
            if (facturaList != null && facturaList.size() > 0) {
                //>Generar Archivo Texto
                archivoTexto = archivoBase.crearArchivoSimple(nombreArchivoTexto);
                if (!archivoTexto.exists()) {
                    if (!archivoTexto.createNewFile()) {
                        LOGGER.warn("verificar archivoTexto de cierre ciclo SIN. ");
                    }
                }
                archivoProfile = poblarArchivo(archivoTexto, facturaList);
                if (archivoProfile != null) {
                    cierreDosificacion.setArchivoTexto(archivoProfile);
                    cierreDosificacion.setNumeroFacturas(nroFilas);
                    cierreDosificacion.setMontoTotal(montoTotal);
                }
                //>Generar Archivo Compreso
                archivoCompreso = archivoBase.crearArchivoSimple(nombreArchivoCompreso);
                if (!archivoCompreso.exists()) {
                    if (!archivoCompreso.createNewFile()) {
                        LOGGER.warn("verificar archivoCompreso de cierre ciclo SIN. ");
                    }
                }
                new Compress7z().compress(archivoCompreso, getItems(archivoTexto, nombreArchivoTexto));
                cierreDosificacion.setArchivoCompreso(archivoCompreso);
                cierreDosificacion.setArchivo(Convert.FileToBytes2(archivoCompreso));
            }
        } catch (IOException e) {
            LOGGER.error("Error: Ocurrio un error al generar el archivoTexto de cierre ciclo. ", e);
        }
        return cierreDosificacion;
    }

    private ArchivoProfile poblarArchivo(File archivo, List<Factura> facturaList) {
        if (archivo == null) return null;
        if (facturaList == null || facturaList.size() <= 0) return null;
        FileWriter archivoWrite;
        FileReader filer = null;
        try {
            archivoWrite = new FileWriter(archivo.getAbsoluteFile());
            BufferedWriter bufferWriter = new BufferedWriter(archivoWrite);
            armarBloque(facturaList, bufferWriter);
            bufferWriter.close();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("DETALLE DEL ARCHIVO");
                filer = new FileReader(archivo);
                int valor = filer.read();
                while (valor != -1) {
                    LOGGER.debug("", valor);
                    valor = filer.read();
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error: Ocurrio un error inesperado al armar el archivo", e);
        } finally {
            if (filer != null) {
                try {
                    filer.close();
                } catch (IOException e) {
                    LOGGER.error("Error: Ocurrio un error inesperado al cerrar el archivo", e);
                }
            }
        }
        return new ArchivoProfile(archivo, valorFilasEfectivas, nroFilas);
    }

    private void armarBloque(List<Factura> facturaList, BufferedWriter bufferWriter) throws IOException {
        long valorFilas;
        nroFilas = 0L;
        valorFilasEfectivas = 0L;
        montoTotal = BigDecimal.ZERO;
        // Detalle del archivo
        for (Factura factura : facturaList) {
            // Generando registros de factura
            if (StringUtils.trimToNull(factura.getTipoMoneda()) != null) {
                valorFilas = poblarLineaArchivo(bufferWriter, factura, nroFilas, separador);
                if (valorFilas != 0L) {
                    valorFilasEfectivas = valorFilasEfectivas + valorFilas;
                    nroFilas++;
                    // Sumatoria de totales
                    montoTotal = montoTotal.add(factura.getMontoCambiado());
                }
            }
        }
    }

    private long poblarLineaArchivo(BufferedWriter bw, Factura factura, long fila, String separador) throws IOException {
        if (factura == null) return 0L;
        StringBuilder sb = new StringBuilder();
        if (fila > 0L) {
            bw.write("\n");
        }
        sb.append(factura.getGrupo());
        sb.append(separador);
        sb.append(factura.getNumeroAutorizacion());
        sb.append(separador);
        sb.append(factura.getId().getNumeroFactura());
        sb.append(separador);
        sb.append(StringUtils.trimToEmpty(factura.getCodigoControl()));
        sb.append(separador);
        sb.append(factura.getNumeroDocumento());
        sb.append(separador);
        sb.append(StringUtils.trimToEmpty(factura.getBeneficiario().replace("#", "\u00d1")));
        sb.append(separador);
        sb.append(FormatosUtils.fechaYYYYMMDD(factura.getFecha()));
        sb.append(separador);
        sb.append(factura.getMontoCambiado());
        sb.append(separador);
        sb.append(factura.getImporteICE());
        sb.append(separador);
        sb.append(factura.getImporteExento());
        sb.append(separador);
        sb.append(factura.getVentasGravadas());
        sb.append(separador);
        sb.append(factura.getDescuentos());
        sb.append(separador);
        sb.append(StringUtils.trimToEmpty(factura.getPlaca()));
        sb.append(separador);
        sb.append(StringUtils.trimToEmpty(factura.getPaisPlaca()));
        sb.append(separador);
        sb.append(StringUtils.trimToEmpty(factura.getTipoEnvase()));
        sb.append(separador);
        sb.append(StringUtils.trimToEmpty(factura.getTipProducto()));
        sb.append(separador);
        sb.append(StringUtils.trimToEmpty(factura.getAutorizacionVenta()));
        sb.append(separador);
        sb.append(factura.getTipoCambio().setScale(2, RoundingMode.HALF_UP));
        sb.append(separador);
        sb.append(Monedas.getDescripcionByBisaCorta(StringUtils.trimToEmpty(factura.getTipoMoneda())));
        bw.write(sb.toString());

        long valorFila = 0L;
        char[] charArray = sb.toString().toCharArray();
        for (char car : charArray) {
            valorFila += car;
        }

        return valorFila;
    }

    private Item[] getItems(File archivoTexto, String nombreArchivoTexto) {
        Item[] items = new Item[1];
        items[0] = new Item(nombreArchivoTexto, "");
        items[0].setContent(Convert.FileToBytes2(archivoTexto));
        return items;
    }
}
