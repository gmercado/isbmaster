package com.bisa.bus.servicios.sin.ciclos.api;

/**
 * @author by rsalvatierra on 17/02/2017.
 */
public interface ConfiguracionSIN {
    int REQ_TIPO_PERSONA = 1;
    int REQ_NUM_DOCUMENTO = 2;
    int REQ_INFO_ADICIONAL = 3;
    int ERROR_INFO_ADICIONAL = 4;
    int SIN_FACTURAS = 5;
    int SIN_ERROR = 6;
    int REQ_BROWSER = 7;
    int REQ_IP = 8;
    int REQ_TERMINAL = 9;
    int REQ_NUM_AUTORIZACION = 10;
    int REQ_NUM_FACTURA = 11;
    int REQ_IDENTIFICADOR = 12;
    int REQ_RESPUESTA = 13;
    int REQ_EVENTO = 14;
}
