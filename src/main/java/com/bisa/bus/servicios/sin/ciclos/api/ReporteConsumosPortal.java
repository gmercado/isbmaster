package com.bisa.bus.servicios.sin.ciclos.api;

import bus.plumbing.utils.Convert;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.sin.ciclos.entities.BitacoraFactura;
import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author by rsalvatierra on 21/03/2017.
 */
public class ReporteConsumosPortal {
    public static final String NOMBRE_REPORTE = "consultasFacturaPortal";
    public static final String EXTENSION_REPORTE = ".csv";
    private static final String separador = ",";

    public static byte[] get(List<BitacoraFactura> consumos) {
        File file;
        Writer bw;
        try {
            file = File.createTempFile(NOMBRE_REPORTE, EXTENSION_REPORTE);

            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.ISO_8859_1.name()));
            bw.append("FECHA").append(separador);
            bw.append("ACCION").append(separador);
            bw.append("TIPO_PERSONA").append(separador);
            bw.append("NRO_CLIENTE").append(separador);
            bw.append("NRO_DOCUMENTO").append(separador);
            bw.append("BENEFICIARIO").append(separador);
            bw.append("NRO_FACTURA").append(separador);
            bw.append("NRO_AUTORIZACION").append(separador);
            bw.append("RESPUESTA").append(separador);

            for (BitacoraFactura orden : consumos) {
                bw.append('\n');
                bw.append(StringUtils.trimToEmpty(orden.getFechaConsumo())).append(separador);
                bw.append(StringUtils.trimToEmpty(orden.getTipoEventoResp())).append(separador);
                bw.append(StringUtils.trimToEmpty(orden.getTipoPersonaResp())).append(separador);
                bw.append(StringUtils.trimToEmpty((orden.getNumCliente() != null) ? orden.getNumCliente() : "")).append(separador);
                bw.append(StringUtils.trimToEmpty(orden.getNumeroDocumento())).append(separador);
                bw.append(FormatosUtils.convertirCaracteresEspeciales(StringUtils.trimToEmpty((orden.getNomBeneficiario() != null) ? orden.getNomBeneficiario() : ""))).append(separador);
                bw.append(StringUtils.trimToEmpty((orden.getNumeroFactura() != null) ? orden.getNumeroFactura().toString() : "")).append(separador);
                bw.append(StringUtils.trimToEmpty((orden.getNumeroAutorizacion() != null) ? orden.getNumeroAutorizacion().toString() : "")).append(separador);
                bw.append(FormatosUtils.convertirCaracteresEspeciales(StringUtils.trimToEmpty(orden.getRespuesta()))).append(separador);
            }
            bw.flush();
            bw.close();
        } catch (IOException e) {
            return null;
        }
        return Convert.FileToBytes(file);
    }
}
