package com.bisa.bus.servicios.sin.ciclos.model;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @author by rsalvatierra on 15/02/2017.
 */
public class SolicitudDosificacion implements Serializable {
    Long codigoActividadEconomica;

    String codigoCaracteristica;

    BigInteger nit;

    BigInteger nitEmisor;

    Integer paralela;

    Integer sucursal;

    String tipoAutorizacion;

    String usuario;

    String eTicket;

    public SolicitudDosificacion() {
    }

    public Long getCodigoActividadEconomica() {
        return codigoActividadEconomica;
    }

    public void setCodigoActividadEconomica(Long codigoActividadEconomica) {
        this.codigoActividadEconomica = codigoActividadEconomica;
    }

    public String getCodigoCaracteristica() {
        return codigoCaracteristica;
    }

    public void setCodigoCaracteristica(String codigoCaracteristica) {
        this.codigoCaracteristica = codigoCaracteristica;
    }

    public BigInteger getNit() {
        return nit;
    }

    public void setNit(BigInteger nit) {
        this.nit = nit;
    }

    public BigInteger getNitEmisor() {
        return nitEmisor;
    }

    public void setNitEmisor(BigInteger nitEmisor) {
        this.nitEmisor = nitEmisor;
    }

    public Integer getParalela() {
        return paralela;
    }

    public void setParalela(Integer paralela) {
        this.paralela = paralela;
    }

    public Integer getSucursal() {
        return sucursal;
    }

    public void setSucursal(Integer sucursal) {
        this.sucursal = sucursal;
    }

    public String getTipoAutorizacion() {
        return tipoAutorizacion;
    }

    public void setTipoAutorizacion(String tipoAutorizacion) {
        this.tipoAutorizacion = tipoAutorizacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String geteTicket() {
        return eTicket;
    }

    public void seteTicket(String eTicket) {
        this.eTicket = eTicket;
    }
}
