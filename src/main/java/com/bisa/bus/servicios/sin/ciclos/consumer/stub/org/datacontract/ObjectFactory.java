package com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.datacontract;

import com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.microsoft.ArrayOfstring;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the org.datacontract.schemas._2004._07.slnfacturacionciclos package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Respuesta_QNAME = new QName("http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", "Respuesta");
    private final static QName _RespuestaCodigo_QNAME = new QName("http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", "Codigo");
    private final static QName _RespuestaETicket_QNAME = new QName("http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", "ETicket");
    private final static QName _RespuestaError_QNAME = new QName("http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", "Error");
    private final static QName _RespuestaLeyenda_QNAME = new QName("http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", "Leyenda");
    private final static QName _RespuestaListaMensajes_QNAME = new QName("http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", "ListaMensajes");
    private final static QName _RespuestaLlave_QNAME = new QName("http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", "Llave");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.slnfacturacionciclos
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Respuesta }
     */
    public Respuesta createRespuesta() {
        return new Respuesta();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Respuesta }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", name = "Respuesta")
    public JAXBElement<Respuesta> createRespuesta(Respuesta value) {
        return new JAXBElement<Respuesta>(_Respuesta_QNAME, Respuesta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", name = "Codigo", scope = Respuesta.class)
    public JAXBElement<String> createRespuestaCodigo(String value) {
        return new JAXBElement<String>(_RespuestaCodigo_QNAME, String.class, Respuesta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", name = "ETicket", scope = Respuesta.class)
    public JAXBElement<String> createRespuestaETicket(String value) {
        return new JAXBElement<String>(_RespuestaETicket_QNAME, String.class, Respuesta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", name = "Error", scope = Respuesta.class)
    public JAXBElement<String> createRespuestaError(String value) {
        return new JAXBElement<String>(_RespuestaError_QNAME, String.class, Respuesta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", name = "Leyenda", scope = Respuesta.class)
    public JAXBElement<String> createRespuestaLeyenda(String value) {
        return new JAXBElement<String>(_RespuestaLeyenda_QNAME, String.class, Respuesta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", name = "ListaMensajes", scope = Respuesta.class)
    public JAXBElement<ArrayOfstring> createRespuestaListaMensajes(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_RespuestaListaMensajes_QNAME, ArrayOfstring.class, Respuesta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", name = "Llave", scope = Respuesta.class)
    public JAXBElement<String> createRespuestaLlave(String value) {
        return new JAXBElement<String>(_RespuestaLlave_QNAME, String.class, Respuesta.class, value);
    }

}
