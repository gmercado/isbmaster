package com.bisa.bus.servicios.sin.ciclos.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author rsalvatierra on 06/01/2017.
 */
@Embeddable
public class PKDosificacion implements Serializable {
    @Column(name = "FFCBNK", columnDefinition = "DECIMAL(3)")
    private BigDecimal codBanco;

    @Column(name = "FFCSUC", columnDefinition = "DECIMAL(4)")
    private BigDecimal codSucursal;

    @Column(name = "FFCSEC", columnDefinition = "DECIMAL(2)")
    private BigDecimal codSector;

    @Column(name = "FFTIPO", columnDefinition = "DECIMAL(1)")
    private int tipo;

    @Column(name = "FFNFOR", columnDefinition = "DECIMAL(15)")
    private String numeroAutorizacion;

    public PKDosificacion() {
    }

    public BigDecimal getCodBanco() {
        return codBanco;
    }

    public void setCodBanco(BigDecimal codBanco) {
        this.codBanco = codBanco;
    }

    public BigDecimal getCodSucursal() {
        return codSucursal;
    }

    public void setCodSucursal(BigDecimal codSucursal) {
        this.codSucursal = codSucursal;
    }

    public BigDecimal getCodSector() {
        return codSector;
    }

    public void setCodSector(BigDecimal codSector) {
        this.codSector = codSector;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PKDosificacion that = (PKDosificacion) o;

        return tipo == that.tipo && codBanco.equals(that.codBanco) && codSucursal.equals(that.codSucursal) && codSector.equals(that.codSector) && numeroAutorizacion.equals(that.numeroAutorizacion);

    }

    @Override
    public int hashCode() {
        int result = codBanco.hashCode();
        result = 31 * result + codSucursal.hashCode();
        result = 31 * result + codSector.hashCode();
        result = 31 * result + tipo;
        result = 31 * result + numeroAutorizacion.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "PKDosificacion{" +
                "codBanco=" + codBanco +
                ", codSucursal=" + codSucursal +
                ", codSector=" + codSector +
                ", tipo=" + tipo +
                ", numeroAutorizacion='" + numeroAutorizacion + '\'' +
                '}';
    }
}
