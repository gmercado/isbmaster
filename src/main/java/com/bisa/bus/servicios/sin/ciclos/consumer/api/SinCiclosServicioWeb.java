package com.bisa.bus.servicios.sin.ciclos.consumer.api;

import bus.consumoweb.consumer.ClienteServiciosWeb;
import com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.datacontract.Respuesta;
import com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.tempuri.ISinFacCiclosService;
import com.bisa.bus.servicios.sin.ciclos.model.CierreDosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.SolicitudDosificacion;
import com.google.inject.Inject;
import org.apache.cxf.jaxrs.ext.multipart.InputStreamDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author by rsalvatierra on 13/02/2017.
 */
public class SinCiclosServicioWeb {
    protected final Logger LOGGER = LoggerFactory.getLogger(SinCiclosServicioWeb.class);
    private final ClienteServiciosWeb clienteServicio;
    private ISinFacCiclosService client;

    @Inject
    public SinCiclosServicioWeb(@ConsumidorSinCiclos ClienteServiciosWeb clienteServicio) {
        this.clienteServicio = clienteServicio;
    }

    public void createService(boolean mtom) {
        //Instancia
        client = clienteServicio.setJaxWS(ISinFacCiclosService.class);
        clienteServicio.addClient(client, mtom);
    }

    public String ping() {
        String respuesta = null;
        try {
            //Ejecuta
            createService(false);
            respuesta = client.ping();
        } catch (Exception e) {
            LOGGER.error("Hubo un error al comunicarse con el servicio local del SIN (ping)", e);
        }
        return respuesta;
    }

    public String pruebaConexion(String usuario) throws Exception {
        String respuesta;
        try {
            //Ejecuta
            createService(false);
            respuesta = client.pruebaConexion(usuario);
        } catch (Exception e) {
            LOGGER.error("Hubo un error al comunicarse con el servicio local del SIN (pruebaConexion)", e);
            throw new Exception("Hubo un error al comunicarse con el servicio local del SIN (pruebaConexion)");
        }
        return respuesta;
    }

    public Respuesta iniciarCiclo(SolicitudDosificacion solicitud) throws Exception {
        Respuesta respuesta;
        try {
            Long codigoActividadEconomica = solicitud.getCodigoActividadEconomica();
            String codigoCaracteristica = solicitud.getCodigoCaracteristica();
            BigInteger nit = solicitud.getNit();
            BigInteger nitEmisor = solicitud.getNitEmisor();
            Integer paralela = solicitud.getParalela();
            Integer sucursal = solicitud.getSucursal();
            String tipoAutorizacion = solicitud.getTipoAutorizacion();
            String usuario = solicitud.getUsuario();
            //Ejecuta
            createService(false);
            respuesta = client.iniciarCiclo(codigoActividadEconomica, codigoCaracteristica, nit, nitEmisor, paralela, sucursal, tipoAutorizacion, usuario);
        } catch (Exception e) {
            LOGGER.error("Hubo un error al comunicarse con el servicio local del SIN (iniciarCiclo)", e);
            throw new Exception("Hubo un error al comunicarse con el servicio local del SIN (iniciarCiclo)");
        }
        return respuesta;
    }

    public Respuesta confirmarInicio(SolicitudDosificacion solicitud) throws Exception {
        Respuesta respuesta;
        try {
            String eTicket = solicitud.geteTicket();
            BigInteger nit = solicitud.getNit();
            String usuario = solicitud.getUsuario();
            //Ejecuta
            createService(false);
            respuesta = client.confirmarInicio(eTicket, nit, usuario);
        } catch (Exception e) {
            LOGGER.error("Hubo un error al comunicarse con el servicio local del SIN (confirmarInicio)", e);
            throw new Exception("Hubo un error al comunicarse con el servicio local del SIN (confirmarInicio)");
        }
        return respuesta;
    }

    public Respuesta finalizarCiclo(CierreDosificacion cierre) {
        Respuesta respuesta = null;
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(cierre.getArchivo());
            DataSource dataSource = new InputStreamDataSource(bis, "application/octet-stream");
            DataHandler dataHandler = new DataHandler(dataSource);
            //byte[] archivo = cierre.getArchivo();
            String eTicket = cierre.geteTicket();
            BigDecimal montoTotal = cierre.getMontoTotal();
            BigInteger nit = cierre.getNit();
            Long numeroFacturas = cierre.getNumeroFacturas();
            String usuario = cierre.getUsuario();
            //Ejecuta
            createService(true);
            respuesta = client.finalizarCiclo(dataHandler, eTicket, montoTotal, nit, numeroFacturas, usuario);
        } catch (Exception e) {
            LOGGER.error("Hubo un error al comunicarse con el servicio local del SIN (finalizarCiclo)", e);
        }
        return respuesta;
    }

}
