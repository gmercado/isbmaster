package com.bisa.bus.servicios.sin.ciclos.sched;

import bus.env.api.MedioAmbiente;
import com.bisa.bus.servicios.sin.ciclos.api.FacturacionCiclosProcess;
import com.bisa.bus.servicios.sin.ciclos.dao.DosificacionDao;
import com.bisa.bus.servicios.sin.ciclos.entities.Dosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.EstadoDosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.FormaDosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.ResultadoDosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.TipoCodigoError;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static bus.env.api.Variables.*;

/**
 * @author by rsalvatierra on 20/02/2017.
 */
public class CargaDosificacion implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(CargaDosificacion.class);

    private final MedioAmbiente medioAmbiente;
    private final FacturacionCiclosProcess facturacionCiclosProcess;
    private final DosificacionDao dosificacionDao;

    @Inject
    public CargaDosificacion(MedioAmbiente medioAmbiente, FacturacionCiclosProcess facturacionCiclosProcess, DosificacionDao dosificacionDao) {
        this.medioAmbiente = medioAmbiente;
        this.facturacionCiclosProcess = facturacionCiclosProcess;
        this.dosificacionDao = dosificacionDao;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Inicio carga dosificacion >>");
        int reintentos = medioAmbiente.getValorIntDe(BISA_SIN_REINTENTOS_JOB, BISA_SIN_REINTENTOS_JOB_DEFAULT);
        String usuario = medioAmbiente.getValorDe(BISA_SIN_USUARIO_JOB, BISA_SIN_USUARIO_JOB_DEFAULT);
        String parametro = medioAmbiente.getValorDe(BISA_SIN_CARACTERISTICAS_JOB, BISA_SIN_CARACTERISTICAS_JOB_DEFAULT);
        String[] caracteristicas = parametro.split(",");
        List<Dosificacion> dosificacionList = dosificacionDao.getDosificaciones(FormaDosificacion.CICLOS, EstadoDosificacion.PENDIENTE);
        for (String caracteristica : caracteristicas) {
            boolean insiste = true;
            int contador = 0;
            LOGGER.info(">> intento consumo {} ", contador);
            if (dosificacionList.size() == 0) {
                while (insiste) {
                    try {
                        ResultadoDosificacion resultadoDosificacion = facturacionCiclosProcess.iniciarCiclo(caracteristica, usuario);
                        if (TipoCodigoError.OK.equals(resultadoDosificacion.getCodError())) {
                            LOGGER.info("INICIAR CICLO{}", resultadoDosificacion.getMensaje());
                            ResultadoDosificacion resultadoDosificacion2 = facturacionCiclosProcess.confirmarInicio(resultadoDosificacion.getDosificacionAutomatica(), usuario);
                            if (TipoCodigoError.OK.equals(resultadoDosificacion2.getCodError())) {
                                LOGGER.info("CONFIRMAR CICLO{}", resultadoDosificacion2.getMensaje());
                                insiste = false;
                            } else {
                                LOGGER.warn("Error en CargaDosificacion >> CONFIRMAR CICLO{}", resultadoDosificacion2.getMensaje());
                            }
                        } else {
                            LOGGER.warn("Error en CargaDosificacion >>  INICIAR CICLO{}", resultadoDosificacion.getMensaje());
                        }
                    } catch (Exception ex) {
                        LOGGER.error("Error en comunicacion >>>", ex);
                    }
                    if (contador >= reintentos) {
                        insiste = false;
                    }
                    contador++;
                    if (insiste) {
                        //Se duerme hilo para continuar siguiente re intento
                        try {
                            Thread.sleep(1000 * 60 * 3);
                        } catch (InterruptedException e) {
                            LOGGER.error("Hubo un error CargaDosificacion >> ", e);
                        }
                    }
                }
            } else {
                LOGGER.warn("aun se tienen dosificaciones pendientes no cerradas >> {}", dosificacionList.size());
            }
        }
        LOGGER.info("FIN carga dosificacion >>");
    }
}
