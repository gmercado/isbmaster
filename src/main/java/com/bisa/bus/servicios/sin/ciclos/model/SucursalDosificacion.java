package com.bisa.bus.servicios.sin.ciclos.model;

/**
 * @author by rsalvatierra on 10/01/2017.
 */
public enum SucursalDosificacion {
    NACIONAL("0");

    private String codigo;

    SucursalDosificacion(String codigo) {
        this.codigo = codigo;
    }

    public static SucursalDosificacion get(String codigo) {
        for (SucursalDosificacion valor : SucursalDosificacion.values()) {
            if (valor.getCodigo().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }

    public String getCodigo() {
        return codigo;
    }
}
