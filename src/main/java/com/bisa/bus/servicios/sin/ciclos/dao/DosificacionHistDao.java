package com.bisa.bus.servicios.sin.ciclos.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.sin.ciclos.api.DosificacionHist;
import com.bisa.bus.servicios.sin.ciclos.entities.DosificacionHistorico;
import com.bisa.bus.servicios.sin.ciclos.entities.PKDosificacionHistorico;
import com.bisa.bus.servicios.sin.ciclos.model.Historico;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by rsalvatierra on 23/12/2016.
 */
public class DosificacionHistDao extends DaoImpl<DosificacionHistorico, PKDosificacionHistorico> implements DosificacionHist {

    private static final Logger LOGGER = LoggerFactory.getLogger(DosificacionHistDao.class);

    @Inject
    protected DosificacionHistDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<PKDosificacionHistorico> countPath(Root<DosificacionHistorico> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<DosificacionHistorico> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.get("id.numeroAutorizacion"));
        return paths;
    }

    @Override
    public List<Historico> getHistorico2(String pNumeroAutorizacion) {
        LOGGER.debug("Obteniendo historico para {}", pNumeroAutorizacion);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {

                        String sql = "select ffnfor as numeroAutorizacion,ffnfac as numeroFactura,ffcact as estado,fffein as fechaIngreso,ffflie as fechaLimiteEmision,ffllav as llave,ffleyen as leyenda,fvuse as codUsuario,fvfecaud as fecha" +
                                " from FAP03301" +
                                " where ffnfor=?" +
                                " union\n" +
                                " select ffnfor,ffnfac,ffcact,fffein,ffflie,ffllav,ffleyen,fvuse,fvfecaud" +
                                " from FAP03301h" +
                                " where ffnfor=?" +
                                " order by 9 desc";
                        Query q = entityManager.createNativeQuery(sql, Historico.class)
                                .setParameter(1, new BigDecimal(pNumeroAutorizacion))
                                .setParameter(2, new BigDecimal(pNumeroAutorizacion));
                        return (List<Historico>) q.getResultList();
                    });
        } catch (Exception e) {
            LOGGER.error("Error en la consulta: {}", e);
            return null;
        }
    }

}
