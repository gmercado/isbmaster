package com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.tempuri;

import com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.datacontract.Respuesta;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Clase Java para anonymous complex type.
 * <p>
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="finalizarCicloResult" type="{http://schemas.datacontract.org/2004/07/slnFacturacionCiclos}Respuesta" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "finalizarCicloResult"
})
@XmlRootElement(name = "finalizarCicloResponse")
public class FinalizarCicloResponse {

    @XmlElementRef(name = "finalizarCicloResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<Respuesta> finalizarCicloResult;

    /**
     * Obtiene el valor de la propiedad finalizarCicloResult.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link Respuesta }{@code >}
     */
    public JAXBElement<Respuesta> getFinalizarCicloResult() {
        return finalizarCicloResult;
    }

    /**
     * Define el valor de la propiedad finalizarCicloResult.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link Respuesta }{@code >}
     */
    public void setFinalizarCicloResult(JAXBElement<Respuesta> value) {
        this.finalizarCicloResult = value;
    }

}
