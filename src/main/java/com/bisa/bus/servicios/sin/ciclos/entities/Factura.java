package com.bisa.bus.servicios.sin.ciclos.entities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 23/12/2016.
 */
@Entity
@Table(name = "FAP03201")
public class Factura implements Serializable {

    @EmbeddedId
    private PKFactura id;

    @Column(name = "FMALFA", columnDefinition = "CHAR(10)")
    private String codAlfanumerico;

    @Column(name = "FMCLIE", columnDefinition = "CHAR(10)")
    private String numCliente;

    @Column(name = "FMTDOC", columnDefinition = "CHAR(3)")
    private String tipoDocumento;

    @Column(name = "FMNDOC", columnDefinition = "DECIMAL(20)")
    private BigDecimal numeroDocumento;

    @Column(name = "FMFECP", columnDefinition = "DECIMAL(8)")
    private BigDecimal fecha;

    @Column(name = "FMFANU", columnDefinition = "DECIMAL(8)")
    private BigDecimal fechaAnulacion;

    @Column(name = "FMTOIM", columnDefinition = "DECIMAL(11,2)")
    private BigDecimal montoTotal;

    @Column(name = "FMSUPR", columnDefinition = "CHAR(10)")
    private String codUsuario;

    @Column(name = "FMMONE", columnDefinition = "DECIMAL(3)")
    private BigDecimal codMoneda;

    @Column(name = "FMCONT", columnDefinition = "CHAR(1)")
    private String aplicaContable;

    @Column(name = "FMPROC", columnDefinition = "DECIMAL(1)")
    private BigDecimal facturaProcesada;

    @Column(name = "FMIMPR", columnDefinition = "DECIMAL(1)")
    private BigDecimal facturaImpresa;

    @Column(name = "FMESTA", columnDefinition = "DECIMAL(1)")
    private BigDecimal estado;

    @Column(name = "FMNFOR", columnDefinition = "DECIMAL(15)")
    private Long numeroAutorizacion;

    @Column(name = "FMBENE", columnDefinition = "CHAR(50)")
    private String beneficiario;

    @Column(name = "FMCNIV", columnDefinition = "CHAR(1)")
    private String codNivel;

    @Column(name = "FMNASI", columnDefinition = "DECIMAL(5)")
    private BigDecimal numeroAsiento;

    @Column(name = "FMFLIE", columnDefinition = "DECIMAL(8)")
    private BigDecimal fechaLimite;

    @Column(name = "FMCCON", columnDefinition = "CHAR(14)")
    private String codigoControl;

    @Column(name = "FMGRUPO", columnDefinition = "DECIMAL(2)")
    private BigDecimal grupo;

    @Column(name = "FMIMPICE", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal importeICE;

    @Column(name = "FMIMPEXT", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal importeExento;

    @Column(name = "FMVENGRA", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal ventasGravadas;

    @Column(name = "FMDESBON", columnDefinition = "DECIMAL(10,2)")
    private BigDecimal descuentos;

    @Column(name = "FMPLACA", columnDefinition = "CHAR(10)")
    private String placa;

    @Column(name = "FMPPLACA", columnDefinition = "CHAR(15)")
    private String paisPlaca;

    @Column(name = "FMTIPEMV", columnDefinition = "CHAR(2)")
    private String tipoEnvase;

    @Column(name = "FMTIPPRO", columnDefinition = "CHAR(5)")
    private String tipProducto;

    @Column(name = "FMAUTVEN", columnDefinition = "CHAR(15)")
    private String AutorizacionVenta;

    @Column(name = "FMTIPCAM", columnDefinition = "DECIMAL(11,7)")
    private BigDecimal tipoCambio;

    @Column(name = "FMTIPMON", columnDefinition = "CHAR(10)")
    private String tipoMoneda;

    @Column(name = "FMTIPCAMO", columnDefinition = "DECIMAL(11,7)")
    private BigDecimal tipoCambioContable;

    @Column(name = "FMTIPTRA", columnDefinition = "CHAR(3)")
    private String tipoTransaccion;

    @Column(name = "FMDIFPER", columnDefinition = "DECIMAL(15,2)")
    private BigDecimal diferenciaPerdida;

    @Column(name = "FMDIFGAN", columnDefinition = "DECIMAL(15,2)")
    private BigDecimal diferenciaGanacia;

    @Column(name = "FMDIFCAM", columnDefinition = "DECIMAL(11,7)")
    private BigDecimal diferenciaCambio;

    @Column(name = "FMMONCAM", columnDefinition = "DECIMAL(15,2)")
    private BigDecimal montoCambiado;

    @Column(name = "FMTIPCAOO", columnDefinition = "DECIMAL(11,7)")
    private BigDecimal tipoCambioOficial;

    @Column(name = "FMESTFACT", columnDefinition = "CHAR(1)")
    private String estadoFactura;

    public Factura() {
    }

    public PKFactura getId() {
        return id;
    }

    public void setId(PKFactura id) {
        this.id = id;
    }

    public String getCodAlfanumerico() {
        return codAlfanumerico;
    }

    public void setCodAlfanumerico(String codAlfanumerico) {
        this.codAlfanumerico = codAlfanumerico;
    }

    public String getNumCliente() {
        return numCliente;
    }

    public void setNumCliente(String numCliente) {
        this.numCliente = numCliente;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public BigDecimal getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(BigDecimal numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public BigDecimal getFecha() {
        return fecha;
    }

    public void setFecha(BigDecimal fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getFechaAnulacion() {
        return fechaAnulacion;
    }

    public void setFechaAnulacion(BigDecimal fechaAnulacion) {
        this.fechaAnulacion = fechaAnulacion;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(String supr) {
        this.codUsuario = supr;
    }

    public BigDecimal getCodMoneda() {
        return codMoneda;
    }

    public void setCodMoneda(BigDecimal moneda) {
        this.codMoneda = moneda;
    }

    public String getAplicaContable() {
        return aplicaContable;
    }

    public void setAplicaContable(String contabilizada) {
        this.aplicaContable = contabilizada;
    }

    public BigDecimal getFacturaProcesada() {
        return facturaProcesada;
    }

    public void setFacturaProcesada(BigDecimal facturaProcesada) {
        this.facturaProcesada = facturaProcesada;
    }

    public BigDecimal getFacturaImpresa() {
        return facturaImpresa;
    }

    public void setFacturaImpresa(BigDecimal facturaImpresa) {
        this.facturaImpresa = facturaImpresa;
    }

    public BigDecimal getEstado() {
        return estado;
    }

    public void setEstado(BigDecimal estado) {
        this.estado = estado;
    }

    public Long getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(Long numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public String getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(String beneficiario) {
        this.beneficiario = beneficiario;
    }

    public String getCodNivel() {
        return codNivel;
    }

    public void setCodNivel(String cniv) {
        this.codNivel = cniv;
    }

    public BigDecimal getNumeroAsiento() {
        return numeroAsiento;
    }

    public void setNumeroAsiento(BigDecimal nasi) {
        this.numeroAsiento = nasi;
    }

    public BigDecimal getFechaLimite() {
        return fechaLimite;
    }

    public void setFechaLimite(BigDecimal fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    public String getCodigoControl() {
        return codigoControl;
    }

    public void setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
    }

    public BigDecimal getGrupo() {
        return grupo;
    }

    public void setGrupo(BigDecimal grupo) {
        this.grupo = grupo;
    }

    public BigDecimal getImporteICE() {
        return importeICE;
    }

    public void setImporteICE(BigDecimal importeICE) {
        this.importeICE = importeICE;
    }

    public BigDecimal getImporteExento() {
        return importeExento;
    }

    public void setImporteExento(BigDecimal importeExento) {
        this.importeExento = importeExento;
    }

    public BigDecimal getVentasGravadas() {
        return ventasGravadas;
    }

    public void setVentasGravadas(BigDecimal ventasGravadas) {
        this.ventasGravadas = ventasGravadas;
    }

    public BigDecimal getDescuentos() {
        return descuentos;
    }

    public void setDescuentos(BigDecimal descuentos) {
        this.descuentos = descuentos;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getPaisPlaca() {
        return paisPlaca;
    }

    public void setPaisPlaca(String paisPlaca) {
        this.paisPlaca = paisPlaca;
    }

    public String getTipoEnvase() {
        return tipoEnvase;
    }

    public void setTipoEnvase(String tipoEnvase) {
        this.tipoEnvase = tipoEnvase;
    }

    public String getTipProducto() {
        return tipProducto;
    }

    public void setTipProducto(String tipProducto) {
        this.tipProducto = tipProducto;
    }

    public String getAutorizacionVenta() {
        return AutorizacionVenta;
    }

    public void setAutorizacionVenta(String autorizacionVenta) {
        AutorizacionVenta = autorizacionVenta;
    }

    public BigDecimal getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(BigDecimal tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public String getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public BigDecimal getTipoCambioContable() {
        return tipoCambioContable;
    }

    public void setTipoCambioContable(BigDecimal tipoCambioOficial) {
        this.tipoCambioContable = tipoCambioOficial;
    }

    public String getTipoTransaccion() {
        return tipoTransaccion;
    }

    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    public BigDecimal getDiferenciaPerdida() {
        return diferenciaPerdida;
    }

    public void setDiferenciaPerdida(BigDecimal diferenciaPerdida) {
        this.diferenciaPerdida = diferenciaPerdida;
    }

    public BigDecimal getDiferenciaGanacia() {
        return diferenciaGanacia;
    }

    public void setDiferenciaGanacia(BigDecimal diferenciaGanacia) {
        this.diferenciaGanacia = diferenciaGanacia;
    }

    public BigDecimal getDiferenciaCambio() {
        return this.diferenciaCambio;
    }

    public void setDiferenciaCambio(BigDecimal diferenciaCambio) {
        this.diferenciaCambio = diferenciaCambio;
    }

    public BigDecimal getDifCambio() {
        return tipoCambioContable.subtract(tipoCambio);
    }

    public BigDecimal getMontoCambiado() {
        return montoCambiado;
    }

    public void setMontoCambiado(BigDecimal montoCambiado) {
        this.montoCambiado = montoCambiado;
    }

    public BigDecimal getTipoCambioOficial() {
        return tipoCambioOficial;
    }

    public void setTipoCambioOficial(BigDecimal tipoCambioOficial) {
        this.tipoCambioOficial = tipoCambioOficial;
    }

    public String getEstadoFactura() {
        return estadoFactura;
    }

    public void setEstadoFactura(String estadoFactura) {
        this.estadoFactura = estadoFactura;
    }
}
