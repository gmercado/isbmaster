package com.bisa.bus.servicios.sin.ciclos.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 28/12/2016.
 */
@Embeddable
public class PKFactura implements Serializable {

    @Column(name = "FMCBNK", columnDefinition = "DECIMAL(3)")
    private BigDecimal codBanco;

    @Column(name = "FMCSUC", columnDefinition = "DECIMAL(4)")
    private BigDecimal codSucursal;

    @Column(name = "FMCSEC", columnDefinition = "DECIMAL(2)")
    private BigDecimal codSector;

    @Column(name = "FMNFAC", columnDefinition = "DECIMAL(9)")
    private Long numeroFactura;

    public PKFactura() {
    }

    public BigDecimal getCodBanco() {
        return codBanco;
    }

    public void setCodBanco(BigDecimal codBanco) {
        this.codBanco = codBanco;
    }

    public BigDecimal getCodSucursal() {
        return codSucursal;
    }

    public void setCodSucursal(BigDecimal codSucursal) {
        this.codSucursal = codSucursal;
    }

    public BigDecimal getCodSector() {
        return codSector;
    }

    public void setCodSector(BigDecimal codSecuencia) {
        this.codSector = codSecuencia;
    }

    public Long getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(Long numFactura) {
        this.numeroFactura = numFactura;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PKFactura pkFactura = (PKFactura) o;

        return codBanco.equals(pkFactura.codBanco) && codSucursal.equals(pkFactura.codSucursal) && codSector.equals(pkFactura.codSector) && numeroFactura.equals(pkFactura.numeroFactura);

    }

    @Override
    public int hashCode() {
        int result = codBanco.hashCode();
        result = 31 * result + codSucursal.hashCode();
        result = 31 * result + codSector.hashCode();
        result = 31 * result + numeroFactura.hashCode();
        return result;
    }
}
