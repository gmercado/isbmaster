package com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import java.math.BigDecimal;
import java.math.BigInteger;


/**
 * <p>Clase Java para anonymous complex type.
 * <p>
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="archivo" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="eTicket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="montoTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="nit" type="{http://www.w3.org/2001/XMLSchema}unsignedLong" minOccurs="0"/&gt;
 *         &lt;element name="numeroFacturas" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/&gt;
 *         &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "archivo",
        "eTicket",
        "montoTotal",
        "nit",
        "numeroFacturas",
        "usuario"
})
@XmlRootElement(name = "finalizarCiclo")
public class FinalizarCiclo {

    @XmlElementRef(name = "archivo", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> archivo;
    @XmlElementRef(name = "eTicket", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eTicket;
    protected BigDecimal montoTotal;
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger nit;
    @XmlSchemaType(name = "unsignedInt")
    protected Long numeroFacturas;
    @XmlElementRef(name = "usuario", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> usuario;

    /**
     * Obtiene el valor de la propiedad archivo.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     */
    public JAXBElement<byte[]> getArchivo() {
        return archivo;
    }

    /**
     * Define el valor de la propiedad archivo.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     */
    public void setArchivo(JAXBElement<byte[]> value) {
        this.archivo = value;
    }

    /**
     * Obtiene el valor de la propiedad eTicket.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public JAXBElement<String> getETicket() {
        return eTicket;
    }

    /**
     * Define el valor de la propiedad eTicket.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public void setETicket(JAXBElement<String> value) {
        this.eTicket = value;
    }

    /**
     * Obtiene el valor de la propiedad montoTotal.
     *
     * @return possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    /**
     * Define el valor de la propiedad montoTotal.
     *
     * @param value allowed object is
     *              {@link BigDecimal }
     */
    public void setMontoTotal(BigDecimal value) {
        this.montoTotal = value;
    }

    /**
     * Obtiene el valor de la propiedad nit.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getNit() {
        return nit;
    }

    /**
     * Define el valor de la propiedad nit.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setNit(BigInteger value) {
        this.nit = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroFacturas.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getNumeroFacturas() {
        return numeroFacturas;
    }

    /**
     * Define el valor de la propiedad numeroFacturas.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setNumeroFacturas(Long value) {
        this.numeroFacturas = value;
    }

    /**
     * Obtiene el valor de la propiedad usuario.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public JAXBElement<String> getUsuario() {
        return usuario;
    }

    /**
     * Define el valor de la propiedad usuario.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public void setUsuario(JAXBElement<String> value) {
        this.usuario = value;
    }

}
