package com.bisa.bus.servicios.sin.ciclos.model;

/**
 * @author by rsalvatierra on 15/02/2017.
 */
public enum EstadoAutomatico {

    INICIO("I"),
    CONFIRMACION("C"),
    CERRADO("F");

    private String descripcion;

    EstadoAutomatico(String descripcion) {
        this.descripcion = descripcion;
    }

    public static EstadoAutomatico get(String codigo) {
        for (EstadoAutomatico valor : EstadoAutomatico.values()) {
            if (valor.getDescripcion().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
