package com.bisa.bus.servicios.sin.ciclos.model;

/**
 * @author by rsalvatierra on 10/01/2017.
 */
public enum TipoDosificacion {
    ND(0, "No Definida"),
    CVME(5, "Compra/Venta Moneda Extranjera");

    private int codigo;
    private String descripcion;

    TipoDosificacion(int codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static TipoDosificacion get(int codigo) {
        for (TipoDosificacion valor : TipoDosificacion.values()) {
            if (valor.getCodigo() == codigo) {
                return valor;
            }
        }
        return null;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
