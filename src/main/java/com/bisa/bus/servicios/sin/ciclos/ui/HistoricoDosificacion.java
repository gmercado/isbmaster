package com.bisa.bus.servicios.sin.ciclos.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.sin.ciclos.api.DosificacionHist;
import com.bisa.bus.servicios.sin.ciclos.entities.PKDosificacionHistorico;
import com.bisa.bus.servicios.sin.ciclos.model.Historico;
import com.google.inject.Inject;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.core.util.lang.PropertyResolver;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.stream.Collectors;

/**
 * @author rsalvatierra on 23/03/2016.
 */
@AuthorizeInstantiation({RolesBisa.SIN_DOSIFICACION})
@Titulo("Hist\u00f3rico Dosificaci\u00f3n")
public class HistoricoDosificacion extends BisaWebPage {

    @Inject
    DosificacionHist dosificacionHist;
    private PKDosificacionHistorico pkDosificacion;

    public HistoricoDosificacion(PageParameters parameters) {
        super(parameters);
        pkDosificacion = new PKDosificacionHistorico();
        pkDosificacion.setCodBanco(new BigDecimal(getPageParameters().get("codBanco").toString()));
        pkDosificacion.setCodSucursal(new BigDecimal(getPageParameters().get("codSucursal").toString()));
        pkDosificacion.setCodSector(new BigDecimal(getPageParameters().get("codSector").toString()));
        pkDosificacion.setTipo(getPageParameters().get("tipo").toInt());
        pkDosificacion.setNumeroAutorizacion(getPageParameters().get("numeroAutorizacion").toString());
        LinkedList<IColumn<Historico, String>> iColumns = new LinkedList<>();
        iColumns.add(new PropertyColumn<Historico, String>(Model.of("Fecha"), "fecha") {
            @Override
            public void populateItem(Item<ICellPopulator<Historico>> item, String componentId, IModel<Historico> rowModel) {
                Historico hist = rowModel.getObject();
                item.add(new Label(componentId, hist.getFecha()).add(new AttributeAppender("style", Model.of("text-align:center"))));
            }
        });
        iColumns.add(new PropertyColumn<>(Model.of("Usuario"), "codUsuario"));
        iColumns.add(new PropertyColumn<>(Model.of("Num. Factura"), "numeroFactura"));
        iColumns.add(new PropertyColumn<>(Model.of("Estado"), "estadoDesc"));
        iColumns.add(new PropertyColumn<>(Model.of("Fecha Ingreso"), "fechaDesdeInicio"));
        iColumns.add(new PropertyColumn<>(Model.of("Fecha Limite Emision"), "fechaHastaFin"));
        iColumns.add(new PropertyColumn<>(Model.of("Llave SIN"), "llave"));
        iColumns.add(new PropertyColumn<>(Model.of("Leyenda"), "leyenda"));

        SortableDataProvider<Historico, String> sortableDataProvider = new ColasDataProvider();
        add(new AjaxFallbackDefaultDataTable<>("datosHistoricos", iColumns, sortableDataProvider, 5));

        IModel<PKDosificacionHistorico> objectModel = new CompoundPropertyModel<>(pkDosificacion);
        //Form
        Form<PKDosificacionHistorico> form = new Form<>("historico", objectModel);
        add(form);
        //botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(new EditarDosificacion(getPageParameters()));
            }
        });
    }

    private class ColasDataProvider extends SortableDataProvider<Historico, String> {

        private final LinkedList<Historico> colas;

        {
            colas = new LinkedList<>();
            colas.addAll(dosificacionHist.getHistorico2(pkDosificacion.getNumeroAutorizacion()).stream().collect(Collectors.toList()));
        }

        @Override
        public Iterator<? extends Historico> iterator(long first, long count) {
            SortParam sort = getSort();
            if (sort != null) {
                Collections.sort(colas, (o1, o2) -> {
                    Integer value1 = ((Number) PropertyResolver.getValue(getSort().getProperty(), o1)).intValue();
                    Integer value2 = ((Number) PropertyResolver.getValue(getSort().getProperty(), o2)).intValue();
                    if (getSort().isAscending()) {
                        return value1.compareTo(value2);
                    } else {
                        return value2.compareTo(value1);
                    }
                });
            }
            return colas.subList((int) first, (int) (first + count)).iterator();
        }

        @Override
        public long size() {
            return colas.size();
        }

        @Override
        public IModel<Historico> model(Historico object) {
            return Model.of(object);
        }
    }
}
