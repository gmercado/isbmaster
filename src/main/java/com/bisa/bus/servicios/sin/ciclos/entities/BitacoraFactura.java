package com.bisa.bus.servicios.sin.ciclos.entities;

import bus.database.model.EntityBase;
import bus.env.api.MedioAmbiente;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.wicket.BisaWebApplication;
import com.bisa.bus.servicios.segip.entities.Cliente;
import com.bisa.bus.servicios.sin.ciclos.model.TipoEvento;
import com.bisa.bus.servicios.sin.ciclos.model.TipoPersona;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static bus.env.api.Variables.*;

/**
 * @author by rsalvatierra on 16/02/2017.
 */
@Entity
@Table(name = "FAP043")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "F43FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "F43USRCRE", nullable = false, columnDefinition = "CHAR(255) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "F43FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "F43USRMOD", columnDefinition = "CHAR(255)"))
})
public class BitacoraFactura extends EntityBase implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F43CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "F43DESCIP", columnDefinition = "VARCHAR(100)")
    private String ip;

    @Column(name = "F43TERMIN", columnDefinition = "VARCHAR(100)")
    private String terminal;

    @Column(name = "F43BROWSE", columnDefinition = "VARCHAR(100)")
    private String browser;

    @Column(name = "F43EVENTO", columnDefinition = "NUMERIC(1)")
    private int evento;

    @Column(name = "F43RESPUE", columnDefinition = "VARCHAR(100)")
    private String respuesta;

    @Column(name = "F43TIPPER", columnDefinition = "NUMERIC(1)")
    private int tipoPersona;

    @Column(name = "F43NUMDOC", columnDefinition = "VARCHAR(40)")
    private String numeroDocumento;

    @Column(name = "F43NUMAUT", columnDefinition = "NUMERIC(15)")
    private Long numeroAutorizacion;

    @Column(name = "F43NUMFAC", columnDefinition = "NUMERIC(10)")
    private Long numeroFactura;

    @Column(name = "F43NUMCLI", columnDefinition = "VARCHAR(10)")
    private String numeroCliente;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "F43FECHAH", columnDefinition = "TIMESTAMP")
    private Date fecha;

    @Transient
    private TipoPersona tipoPersonas;

    @Transient
    private TipoEvento tipoEvento;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "F43NUMFAC", referencedColumnName = "FMNFAC", insertable = false, updatable = false),
            @JoinColumn(name = "F43NUMAUT", referencedColumnName = "FMNFOR", insertable = false, updatable = false),
    })
    private Factura factura;

    @ManyToOne
    @JoinColumn(name = "F43NUMCLI", referencedColumnName = "CUNBR", insertable = false, updatable = false)
    private Cliente cliente;

    public BitacoraFactura() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public int getEvento() {
        return evento;
    }

    public void setEvento(int evento) {
        this.evento = evento;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public int getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(int tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public Long getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(Long numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public Long getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(Long numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public TipoPersona getTipoPersonas() {
        return tipoPersonas;
    }

    public void setTipoPersonas(TipoPersona tipoPersonas) {
        this.tipoPersonas = tipoPersonas;
    }

    public TipoEvento getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(TipoEvento tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public String getDesc() {
        return getBrowser() + "_" + getTerminal() + "_" + getIp();
    }

    public String getFechaConsumo() {
        return FormatosUtils.formatoFechaHora(getFecha());
    }

    public String getNumeroCliente() {
        return numeroCliente;
    }

    public void setNumeroCliente(String numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getNomBeneficiario() {
        if (getFactura() != null) {
            return remove1(getFactura().getBeneficiario());
        } else if (getCliente() != null) {
            return getCliente().getNombreCompleto();
        }
        return "";
    }

    public String getNumCliente() {
        if (getFactura() != null) {
            return getFactura().getNumCliente();
        } else if (getCliente() != null) {
            return getCliente().getId();
        }
        return getNumeroCliente();
    }

    public String getTipoEventoResp() {
        String tipo = "";
        TipoEvento tipoR = TipoEvento.get(getEvento());
        if (tipoR != null) {
            tipo = tipoR.toString();
        }
        return tipo;
    }

    public String getTipoPersonaResp() {
        String tipo = "";
        TipoPersona tipoR = TipoPersona.get(getTipoPersona());
        if (tipoR != null) {
            tipo = tipoR.toString();
        }
        return tipo;
    }


    public String remove1(String input) {
        if (input != null) {
            MedioAmbiente medioAmbiente = BisaWebApplication.get().getInstance(MedioAmbiente.class);
            // Cadena de caracteres original a sustituir.
            String original = medioAmbiente.getValorDe(SIN_CARACTERES_ESPECIALES, SIN_CARACTERES_ESPECIALES_DEFAULT);
            // Cadena de caracteres ASCII que reemplazarán los originales.
            String ascii = medioAmbiente.getValorDe(SIN_CARACTERES_ESPECIALES_REEMPLAZO, SIN_CARACTERES_ESPECIALES_REEMPLAZO_DEFAULT);
            String output = input;
            for (int i = 0; i < original.length(); i++) {
                // Reemplazamos los caracteres especiales.
                output = output.replace(original.charAt(i), ascii.charAt(i));
            }
            return output;
        }
        return null;
    }
}
