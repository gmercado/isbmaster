package com.bisa.bus.servicios.sin.ciclos.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author by rsalvatierra on 15/02/2017.
 */
@Entity
@Table(name = "FAP042")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "F42FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "F42USRCRE", nullable = false, columnDefinition = "CHAR(255) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "F42FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "F42USRMOD", columnDefinition = "CHAR(255)"))
})
public class DosificacionAutomatica extends EntityBase implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F42CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @NotNull
    @Column(name = "F42CODCAR", nullable = false, columnDefinition = "VARCHAR(10)")
    private String codigoCaracteristica;

    @Column(name = "F42TICSIN", columnDefinition = "VARCHAR(100)")
    private String eTicket;

    @Column(name = "F42RANDES", columnDefinition = "NUMERIC(10)")
    private Long rangoDesde;

    @Column(name = "F42NUMAUT", columnDefinition = "NUMERIC(15)")
    private Long numeroAutorizacion;

    @Column(name = "F42KEYCOD", columnDefinition = "VARCHAR(100)")
    private String llave;

    @Column(name = "F42FECFIN", columnDefinition = "NUMERIC(8)")
    private Long fechaFin;

    @Column(name = "F42LEYDAT", columnDefinition = "VARCHAR(200)")
    private String leyenda;

    @Column(name = "F42NUMFAC", columnDefinition = "NUMERIC(8)")
    private BigInteger cantidadFacturas;

    @Column(name = "F42MONTOT", columnDefinition = "DECIMAL(14,5)")
    private BigDecimal montoTotalFacturas;

    @Basic(fetch = FetchType.EAGER)
    @Column(name = "F42FILENV", columnDefinition = "BLOB")
    @Lob
    private byte[] archivo;

    @Column(name = "F42MENREC")
    private String mensajes;

    @Column(name = "F42ESTADO", columnDefinition = "VARCHAR(1)")
    private String estado;

    public DosificacionAutomatica() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoCaracteristica() {
        return codigoCaracteristica;
    }

    public void setCodigoCaracteristica(String codigoCaracteristica) {
        this.codigoCaracteristica = codigoCaracteristica;
    }

    public String geteTicket() {
        return eTicket;
    }

    public void seteTicket(String eTicket) {
        this.eTicket = eTicket;
    }

    public Long getRangoDesde() {
        return rangoDesde;
    }

    public void setRangoDesde(Long rangoDesde) {
        this.rangoDesde = rangoDesde;
    }

    public Long getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(Long numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }

    public Long getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Long fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getLeyenda() {
        return leyenda;
    }

    public void setLeyenda(String leyenda) {
        this.leyenda = leyenda;
    }

    public BigInteger getCantidadFacturas() {
        return cantidadFacturas;
    }

    public void setCantidadFacturas(BigInteger cantidadFacturas) {
        this.cantidadFacturas = cantidadFacturas;
    }

    public BigDecimal getMontoTotalFacturas() {
        return montoTotalFacturas;
    }

    public void setMontoTotalFacturas(BigDecimal montoTotalFacturas) {
        this.montoTotalFacturas = montoTotalFacturas;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public String getMensajes() {
        return mensajes;
    }

    public void setMensajes(String mensajes) {
        this.mensajes = mensajes;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
