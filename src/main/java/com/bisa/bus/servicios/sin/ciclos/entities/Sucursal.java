package com.bisa.bus.servicios.sin.ciclos.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author by rsalvatierra on 29/12/2016.
 */
@Entity
@Table(name = "CFP89001")
public class Sucursal implements Serializable {

    @Column(name = "CFSSUC", columnDefinition = "NUMERIC(3)")
    private int codSucursal;

    @Column(name = "CFSSSU", columnDefinition = "CHAR(20)")
    private String sucursal;

    @Id
    @Column(name = "CFSAGE", columnDefinition = "NUMERIC(3)")
    private int codAgencia;

    @Column(name = "CFSSAG", columnDefinition = "CHAR(50)")
    private String agencia;

    public Sucursal() {
    }

    public int getCodSucursal() {
        return codSucursal;
    }

    public void setCodSucursal(int codSucursal) {
        this.codSucursal = codSucursal;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public int getCodAgencia() {
        return codAgencia;
    }

    public void setCodAgencia(int codAgencia) {
        this.codAgencia = codAgencia;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }
}
