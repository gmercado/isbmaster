package com.bisa.bus.servicios.sin.ciclos.model;

/**
 * @author by rsalvatierra on 06/01/2017.
 */
public enum EstadoDosificacion {

    ACTIVO("1"),
    INACTIVO("9"),
    PENDIENTE("2");

    private String descripcion;

    EstadoDosificacion(String descripcion) {
        this.descripcion = descripcion;
    }

    public static EstadoDosificacion get(String codigo) {
        for (EstadoDosificacion valor : EstadoDosificacion.values()) {
            if (valor.getDescripcion().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
