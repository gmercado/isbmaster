package com.bisa.bus.servicios.sin.ciclos.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 28/12/2016.
 */
public class DetalleFactura implements Serializable {
    private BigDecimal cantidad;
    private String concepto;
    private BigDecimal tipoCambioOficial;
    private BigDecimal tipoCambio;
    private BigDecimal diferencia;
    private BigDecimal subtotal;

    public DetalleFactura() {
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public BigDecimal getTipoCambioOficial() {
        return tipoCambioOficial;
    }

    public void setTipoCambioOficial(BigDecimal tipoCambioOficial) {
        this.tipoCambioOficial = tipoCambioOficial;
    }

    public BigDecimal getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(BigDecimal tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public BigDecimal getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(BigDecimal diferencia) {
        this.diferencia = diferencia;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }
}
