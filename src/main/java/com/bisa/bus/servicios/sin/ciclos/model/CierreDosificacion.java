package com.bisa.bus.servicios.sin.ciclos.model;

import bus.plumbing.file.ArchivoProfile;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author by rsalvatierra on 15/02/2017.
 */
public class CierreDosificacion {
    byte[] archivo;
    String eTicket;
    BigDecimal montoTotal;
    BigInteger nit;
    Long numeroFacturas;
    String usuario;
    ArchivoProfile archivoTexto;
    File archivoCompreso;

    public CierreDosificacion() {
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public String geteTicket() {
        return eTicket;
    }

    public void seteTicket(String eTicket) {
        this.eTicket = eTicket;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public BigInteger getNit() {
        return nit;
    }

    public void setNit(BigInteger nit) {
        this.nit = nit;
    }

    public Long getNumeroFacturas() {
        return numeroFacturas;
    }

    public void setNumeroFacturas(Long numeroFacturas) {
        this.numeroFacturas = numeroFacturas;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public ArchivoProfile getArchivoTexto() {
        return archivoTexto;
    }

    public void setArchivoTexto(ArchivoProfile archivoTexto) {
        this.archivoTexto = archivoTexto;
    }

    public File getArchivoCompreso() {
        return archivoCompreso;
    }

    public void setArchivoCompreso(File archivoCompreso) {
        this.archivoCompreso = archivoCompreso;
    }
}
