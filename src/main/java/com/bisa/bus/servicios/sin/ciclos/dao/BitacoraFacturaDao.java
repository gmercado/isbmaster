package com.bisa.bus.servicios.sin.ciclos.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.segip.entities.Cliente;
import com.bisa.bus.servicios.sin.ciclos.entities.BitacoraFactura;
import com.bisa.bus.servicios.sin.ciclos.model.RequestQuery;
import com.bisa.bus.servicios.sin.ciclos.model.RequestQueryLog;
import com.bisa.bus.servicios.sin.ciclos.model.TipoEvento;
import com.bisa.bus.servicios.sin.ciclos.model.TipoPersona;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;

/**
 * @author by rsalvatierra on 17/02/2017.
 */
public class BitacoraFacturaDao extends DaoImpl<BitacoraFactura, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BitacoraFacturaDao.class);

    @Inject
    protected BitacoraFacturaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Long> countPath(Root<BitacoraFactura> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<BitacoraFactura> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.get("numeroDocumento"));
        return paths;
    }

    public Long registrarBitacora(RequestQuery query, Cliente cliente, String respuesta, String usuario) {
        LOGGER.debug("query {}, resepuesta {},usuario {}", query, respuesta, usuario);
        BitacoraFactura bitacoraFactura = new BitacoraFactura();
        bitacoraFactura.setNumeroDocumento(query.getNumeroDocumento() != null ? query.getNumeroDocumento().toPlainString() : "");
        bitacoraFactura.setTipoPersona(query.getTipoPersona());
        bitacoraFactura.setTerminal(query.getTerminal());
        bitacoraFactura.setIp(query.getIp());
        bitacoraFactura.setBrowser(query.getBrowser());
        bitacoraFactura.setFecha(new Date());
        bitacoraFactura.setUsuarioCreador(usuario);
        bitacoraFactura.setFechaCreacion(new Date());
        bitacoraFactura.setEvento(TipoEvento.CONSULTAR.getCodigo());
        bitacoraFactura.setRespuesta(respuesta);
        if (cliente != null) {
            bitacoraFactura.setNumeroCliente(cliente.getId());
        }
        bitacoraFactura = persist(bitacoraFactura);
        return bitacoraFactura.getId();
    }

    public void registrarBitacora(RequestQueryLog query, String usuario) {
        LOGGER.debug("query {}, usuario {}", query, usuario);
        BitacoraFactura bitacoraFactura = new BitacoraFactura();
        bitacoraFactura.setNumeroDocumento(query.getNumeroDocumento() != null ? query.getNumeroDocumento().toPlainString() : "");
        bitacoraFactura.setTipoPersona(query.getTipoPersona());
        bitacoraFactura.setTerminal(query.getTerminal());
        bitacoraFactura.setIp(query.getIp());
        bitacoraFactura.setBrowser(query.getBrowser());
        bitacoraFactura.setFecha(new Date());
        bitacoraFactura.setUsuarioCreador(usuario);
        bitacoraFactura.setFechaCreacion(new Date());
        bitacoraFactura.setEvento(query.getEvento());
        bitacoraFactura.setRespuesta(query.getRespuesta());
        bitacoraFactura.setNumeroAutorizacion(query.getNumeroAutorizacion());
        bitacoraFactura.setNumeroFactura(query.getNumeroFactura());
        persist(bitacoraFactura);
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<BitacoraFactura> from, BitacoraFactura example, BitacoraFactura example2) {

        List<Predicate> predicates = new LinkedList<>();
        Date fecha = example.getFecha();
        String numeroDocumento = StringUtils.trimToNull(example.getNumeroDocumento());
        Long numeroFactura = example.getNumeroFactura();
        TipoPersona tipoPersona = example.getTipoPersonas();
        TipoEvento tipoEvento = example.getTipoEvento();
        if (fecha != null && example2.getFecha() != null) {
            Date inicio = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH);
            Date inicio2 = DateUtils.truncate(example2.getFecha(), Calendar.DAY_OF_MONTH);
            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
            LOGGER.debug("   FECHAS ENTRE [{}] A [{}]", inicio, inicio2);
            predicates.add(cb.between(from.get("fecha"), inicio, inicio2));
        }

        if (numeroDocumento != null) {
            final String s = StringUtils.trimToEmpty(numeroDocumento);
            LOGGER.debug(">>>>> numeroDocumento=" + s);
            predicates.add(cb.equal(from.get("numeroDocumento"), s));
        }
        if (tipoPersona != null) {
            LOGGER.debug(">>>>> tipoPersona =" + tipoPersona);
            predicates.add(cb.equal(from.get("tipoPersona"), tipoPersona.getCodigo()));
        }
        if (tipoEvento != null) {
            LOGGER.debug(">>>>> tipoEvento =" + tipoEvento);
            predicates.add(cb.equal(from.get("evento"), tipoEvento.getCodigo()));
        }
        if (numeroFactura != null) {
            LOGGER.debug(">>>>> numeroFactura =" + numeroFactura);
            predicates.add(cb.equal(from.get("numeroFactura"), numeroFactura));
        }
        return predicates.toArray(new Predicate[predicates.size()]);
    }

    public List<BitacoraFactura> getConsumos(BitacoraFactura model1, BitacoraFactura model2) {
        LOGGER.debug("Obteniendo consumos Facturas:{},{}", model1, model2);
        try {
            Date desde = model1.getFecha();
            Date hasta = model2.getFecha();
            String numeroDocumento = StringUtils.trimToNull(model1.getNumeroDocumento());
            Long numeroFactura = model1.getNumeroFactura();
            TipoPersona tipoPersona = model1.getTipoPersonas();
            TipoEvento tipoEvento = model1.getTipoEvento();
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<BitacoraFactura> q = cb.createQuery(BitacoraFactura.class);
                        Root<BitacoraFactura> p = q.from(BitacoraFactura.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        if (StringUtils.trimToNull(numeroDocumento) != null) {
                            predicatesAnd.add(cb.equal(p.get("numeroDocumento"), StringUtils.trimToEmpty(numeroDocumento)));
                        }

                        if (desde != null && hasta != null) {
                            Date inicio = DateUtils.truncate(desde, Calendar.DAY_OF_MONTH);
                            Date inicio2 = DateUtils.truncate(hasta, Calendar.DAY_OF_MONTH);
                            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
                            predicatesAnd.add(cb.between(p.get("fecha"), inicio, inicio2));
                        }

                        if (numeroFactura != null) {
                            predicatesAnd.add(cb.equal(p.get("numeroFactura"), numeroFactura));
                        }
                        if (tipoPersona != null) {
                            predicatesAnd.add(cb.equal(p.get("tipoPersona"), tipoPersona.getCodigo()));
                        }
                        if (tipoEvento != null) {
                            predicatesAnd.add(cb.equal(p.get("evento"), tipoEvento.getCodigo()));
                        }

                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<BitacoraFactura> query = entityManager.createQuery(q);
                        q.orderBy(cb.desc(p.get("id")));
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta: {}", e);
        }
        return null;
    }
}
