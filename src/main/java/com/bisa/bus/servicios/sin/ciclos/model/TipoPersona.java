package com.bisa.bus.servicios.sin.ciclos.model;

/**
 * @author by rsalvatierra on 29/12/2016.
 */
public enum TipoPersona {
    NATURAL(1, "Natural"),
    JURIDICO(2, "Juridico");

    private int codigo;
    private String descripcion;

    TipoPersona(int codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static TipoPersona get(int codigo) {
        for (TipoPersona valor : TipoPersona.values()) {
            if (codigo == valor.getCodigo()) {
                return valor;
            }
        }
        return null;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
