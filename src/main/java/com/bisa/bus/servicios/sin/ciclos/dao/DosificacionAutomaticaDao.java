package com.bisa.bus.servicios.sin.ciclos.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.sin.ciclos.entities.DosificacionAutomatica;
import com.bisa.bus.servicios.sin.ciclos.model.EstadoAutomatico;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * @author by rsalvatierra on 15/02/2017.
 */
public class DosificacionAutomaticaDao extends DaoImpl<DosificacionAutomatica, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DosificacionAutomaticaDao.class);

    @Inject
    protected DosificacionAutomaticaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Long> countPath(Root<DosificacionAutomatica> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<DosificacionAutomatica> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.get("codigoCaracteristica"));
        return paths;
    }

    /**
     * Obtener Dosificación Automatica
     *
     * @return DosificacionAutomatica
     */
    public DosificacionAutomatica getDosificacion(String codigoCaracteristica, EstadoAutomatico pEstado) {
        LOGGER.debug("Obteniendo dosificacion iniciada {}", pEstado);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<DosificacionAutomatica> q = cb.createQuery(DosificacionAutomatica.class);
                        Root<DosificacionAutomatica> p = q.from(DosificacionAutomatica.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("codigoCaracteristica"), StringUtils.trimToEmpty(codigoCaracteristica)));
                        predicatesAnd.add(cb.equal(p.get("estado"), pEstado.getDescripcion()));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));

                        TypedQuery<DosificacionAutomatica> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() > 0) {
                            return query.getResultList().get(0);
                        }
                        return null;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public DosificacionAutomatica getDosificacion(Long pNumeroAutorizacion) {
        LOGGER.debug("Obteniendo dosificacion iniciada {}", pNumeroAutorizacion);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<DosificacionAutomatica> q = cb.createQuery(DosificacionAutomatica.class);
                        Root<DosificacionAutomatica> p = q.from(DosificacionAutomatica.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("numeroAutorizacion"), pNumeroAutorizacion));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));

                        TypedQuery<DosificacionAutomatica> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() > 0) {
                            return query.getResultList().get(0);
                        }
                        return null;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }


    public DosificacionAutomatica getDosificacion(String pEticket, String codigoCaracteristica) {
        LOGGER.debug("Obteniendo dosificacion  {} {}", pEticket, codigoCaracteristica);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<DosificacionAutomatica> q = cb.createQuery(DosificacionAutomatica.class);
                        Root<DosificacionAutomatica> p = q.from(DosificacionAutomatica.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("eTicket"), StringUtils.trimToEmpty(pEticket)));
                        predicatesAnd.add(cb.equal(p.get("codigoCaracteristica"), StringUtils.trimToEmpty(codigoCaracteristica)));

                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));

                        TypedQuery<DosificacionAutomatica> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() > 0) {
                            return query.getResultList().get(0);
                        }
                        return null;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }
}

