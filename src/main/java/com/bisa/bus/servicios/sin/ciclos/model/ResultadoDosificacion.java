package com.bisa.bus.servicios.sin.ciclos.model;

import com.bisa.bus.servicios.sin.ciclos.entities.DosificacionAutomatica;

import java.io.Serializable;

/**
 * @author by rsalvatierra on 15/02/2017.
 */
public class ResultadoDosificacion implements Serializable {

    DosificacionAutomatica dosificacionAutomatica;
    TipoCodigoError codError;
    String mensaje;

    public DosificacionAutomatica getDosificacionAutomatica() {
        return dosificacionAutomatica;
    }

    public void setDosificacionAutomatica(DosificacionAutomatica dosificacionAutomatica) {
        this.dosificacionAutomatica = dosificacionAutomatica;
    }

    public TipoCodigoError getCodError() {
        return codError;
    }

    public void setCodError(TipoCodigoError codError) {
        this.codError = codError;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
