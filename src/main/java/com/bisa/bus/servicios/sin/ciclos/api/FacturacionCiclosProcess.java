package com.bisa.bus.servicios.sin.ciclos.api;

import com.bisa.bus.servicios.sin.ciclos.entities.DosificacionAutomatica;
import com.bisa.bus.servicios.sin.ciclos.model.*;

/**
 * @author by rsalvatierra on 20/12/2016.
 */
public interface FacturacionCiclosProcess {
    String probarConexion() throws Exception;

    ResultadoDosificacion iniciarCiclo(String codigoCaracteristica, String usuario);

    ResultadoDosificacion confirmarInicio(DosificacionAutomatica dosificacionAutomatica, String usuario);

    ResultadoDosificacion cierreCiclo(DosificacionAutomatica dosificacionAutomatica, String usuario);

    FacturasResult obtenerFacturas(RequestQuery query, String usuario);

    LogFacturasResult registrarLogFacturas(RequestQueryLog query, String usuario);
}
