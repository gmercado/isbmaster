package com.bisa.bus.servicios.sin.ciclos.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by rsalvatierra on 28/12/2016.
 */
public class CabeceraFactura implements Serializable {

    private BigDecimal nitBanco;
    private String razonSocial;
    private String casaMatriz;
    private BigDecimal numeroFactura;
    private String numeroAutorizacion;
    private String lugar;
    private String agencia;
    private String fecha;
    private BigDecimal documento;
    private String beneficiario;
    private BigDecimal montoTotal;
    private String detalleMonto;
    private String codigoControl;
    private String fechaLimiteEmision;
    private String leyenda;
    private List<DetalleFactura> detalleFacturas;

    public CabeceraFactura() {
    }

    public BigDecimal getNitBanco() {
        return nitBanco;
    }

    public void setNitBanco(BigDecimal nitBanco) {
        this.nitBanco = nitBanco;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getCasaMatriz() {
        return casaMatriz;
    }

    public void setCasaMatriz(String casaMatriz) {
        this.casaMatriz = casaMatriz;
    }

    public BigDecimal getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(BigDecimal numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getDocumento() {
        return documento;
    }

    public void setDocumento(BigDecimal documento) {
        this.documento = documento;
    }

    public String getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(String beneficiario) {
        this.beneficiario = beneficiario;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public String getCodigoControl() {
        return codigoControl;
    }

    public void setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
    }

    public String getFechaLimiteEmision() {
        return fechaLimiteEmision;
    }

    public void setFechaLimiteEmision(String fechaLimiteEmision) {
        this.fechaLimiteEmision = fechaLimiteEmision;
    }

    public String getDetalleMonto() {
        return detalleMonto;
    }

    public void setDetalleMonto(String detalleMonto) {
        this.detalleMonto = detalleMonto;
    }

    public List<DetalleFactura> getDetalleFacturas() {
        return detalleFacturas;
    }

    public void setDetalleFacturas(List<DetalleFactura> detalleFacturas) {
        this.detalleFacturas = detalleFacturas;
    }

    public void add(DetalleFactura detalleFactura) {
        if (detalleFacturas == null) {
            detalleFacturas = new ArrayList<>();
        }
        detalleFacturas.add(detalleFactura);
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getLeyenda() {
        return leyenda;
    }

    public void setLeyenda(String leyenda) {
        this.leyenda = leyenda;
    }
}
