package com.bisa.bus.servicios.sin.ciclos.ws;

import com.bisa.bus.servicios.sin.ciclos.api.FacturacionCiclosProcess;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author by rsalvatierra on 20/12/2016.
 */
public class WebServiceFacturacionCiclosEndPoint implements WebServiceFacturacionCiclos {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebServiceFacturacionCiclosEndPoint.class);

    @Inject
    private FacturacionCiclosProcess facturacionCiclosProcess;

    @Override
    public String probarConexion(String request) {
        try {
            LOGGER.debug("ingreso -> {}", request);
            return facturacionCiclosProcess.probarConexion();
        } catch (Exception e) {
            LOGGER.error("Hubo un error:", e);
        }
        return null;
    }
}
