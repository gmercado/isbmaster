package com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.datacontract;

import com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.microsoft.ArrayOfstring;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Clase Java para Respuesta complex type.
 * <p>
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;complexType name="Respuesta"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Confirmacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="ETicket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Error" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Leyenda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ListaMensajes" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/&gt;
 *         &lt;element name="Llave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumeroAutorizacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="RangoDesde" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Respuesta", propOrder = {
        "codigo",
        "confirmacion",
        "eTicket",
        "error",
        "leyenda",
        "listaMensajes",
        "llave",
        "numeroAutorizacion",
        "rangoDesde"
})
public class Respuesta {

    @XmlElementRef(name = "Codigo", namespace = "http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codigo;
    @XmlElement(name = "Confirmacion")
    protected Long confirmacion;
    @XmlElementRef(name = "ETicket", namespace = "http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eTicket;
    @XmlElementRef(name = "Error", namespace = "http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", type = JAXBElement.class, required = false)
    protected JAXBElement<String> error;
    @XmlElementRef(name = "Leyenda", namespace = "http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", type = JAXBElement.class, required = false)
    protected JAXBElement<String> leyenda;
    @XmlElementRef(name = "ListaMensajes", namespace = "http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfstring> listaMensajes;
    @XmlElementRef(name = "Llave", namespace = "http://schemas.datacontract.org/2004/07/slnFacturacionCiclos", type = JAXBElement.class, required = false)
    protected JAXBElement<String> llave;
    @XmlElement(name = "NumeroAutorizacion")
    protected Long numeroAutorizacion;
    @XmlElement(name = "RangoDesde")
    protected Long rangoDesde;

    /**
     * Obtiene el valor de la propiedad codigo.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public JAXBElement<String> getCodigo() {
        return codigo;
    }

    /**
     * Define el valor de la propiedad codigo.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public void setCodigo(JAXBElement<String> value) {
        this.codigo = value;
    }

    /**
     * Obtiene el valor de la propiedad confirmacion.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getConfirmacion() {
        return confirmacion;
    }

    /**
     * Define el valor de la propiedad confirmacion.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setConfirmacion(Long value) {
        this.confirmacion = value;
    }

    /**
     * Obtiene el valor de la propiedad eTicket.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public JAXBElement<String> getETicket() {
        return eTicket;
    }

    /**
     * Define el valor de la propiedad eTicket.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public void setETicket(JAXBElement<String> value) {
        this.eTicket = value;
    }

    /**
     * Obtiene el valor de la propiedad error.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public JAXBElement<String> getError() {
        return error;
    }

    /**
     * Define el valor de la propiedad error.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public void setError(JAXBElement<String> value) {
        this.error = value;
    }

    /**
     * Obtiene el valor de la propiedad leyenda.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public JAXBElement<String> getLeyenda() {
        return leyenda;
    }

    /**
     * Define el valor de la propiedad leyenda.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public void setLeyenda(JAXBElement<String> value) {
        this.leyenda = value;
    }

    /**
     * Obtiene el valor de la propiedad listaMensajes.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     */
    public JAXBElement<ArrayOfstring> getListaMensajes() {
        return listaMensajes;
    }

    /**
     * Define el valor de la propiedad listaMensajes.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     */
    public void setListaMensajes(JAXBElement<ArrayOfstring> value) {
        this.listaMensajes = value;
    }

    /**
     * Obtiene el valor de la propiedad llave.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public JAXBElement<String> getLlave() {
        return llave;
    }

    /**
     * Define el valor de la propiedad llave.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public void setLlave(JAXBElement<String> value) {
        this.llave = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroAutorizacion.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    /**
     * Define el valor de la propiedad numeroAutorizacion.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setNumeroAutorizacion(Long value) {
        this.numeroAutorizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad rangoDesde.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getRangoDesde() {
        return rangoDesde;
    }

    /**
     * Define el valor de la propiedad rangoDesde.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setRangoDesde(Long value) {
        this.rangoDesde = value;
    }

}
