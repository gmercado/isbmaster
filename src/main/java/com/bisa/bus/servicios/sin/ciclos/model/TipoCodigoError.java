package com.bisa.bus.servicios.sin.ciclos.model;

/**
 * @author by rsalvatierra on 15/02/2017.
 */
public enum TipoCodigoError {
    OK("000", "Correcto"),
    EXISTE("101", "Existe registro"),
    NO_EXISTE("102", "No existe registro"),
    NO_REALIZO_CONSULTA("103", "No se realizo la busqueda"),
    SERV_NO_HAB("104", "El servicio del SIN no esta habilitado para su consumo"),
    SIN_INICIO("105", "Debe tener inicio para confirmar"),
    ERR_INESPERADO("999", "Error inesperado");

    private String codigo;
    private String descripcion;

    TipoCodigoError(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static TipoCodigoError get(String codigo) {
        for (TipoCodigoError valor : TipoCodigoError.values()) {
            if (valor.getCodigo().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
