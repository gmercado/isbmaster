package com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import java.math.BigInteger;


/**
 * <p>Clase Java para anonymous complex type.
 * <p>
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codigoActividadEconomica" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="codigoCaracteristica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nit" type="{http://www.w3.org/2001/XMLSchema}unsignedLong" minOccurs="0"/&gt;
 *         &lt;element name="nitEmisor" type="{http://www.w3.org/2001/XMLSchema}unsignedLong" minOccurs="0"/&gt;
 *         &lt;element name="paralela" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="tipoAutorizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "codigoActividadEconomica",
        "codigoCaracteristica",
        "nit",
        "nitEmisor",
        "paralela",
        "sucursal",
        "tipoAutorizacion",
        "usuario"
})
@XmlRootElement(name = "iniciarCiclo")
public class IniciarCiclo {

    protected Long codigoActividadEconomica;
    @XmlElementRef(name = "codigoCaracteristica", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codigoCaracteristica;
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger nit;
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger nitEmisor;
    protected Integer paralela;
    protected Integer sucursal;
    @XmlElementRef(name = "tipoAutorizacion", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipoAutorizacion;
    @XmlElementRef(name = "usuario", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> usuario;

    /**
     * Obtiene el valor de la propiedad codigoActividadEconomica.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getCodigoActividadEconomica() {
        return codigoActividadEconomica;
    }

    /**
     * Define el valor de la propiedad codigoActividadEconomica.
     *
     * @param value allowed object is
     *              {@link Long }
     */
    public void setCodigoActividadEconomica(Long value) {
        this.codigoActividadEconomica = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCaracteristica.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public JAXBElement<String> getCodigoCaracteristica() {
        return codigoCaracteristica;
    }

    /**
     * Define el valor de la propiedad codigoCaracteristica.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public void setCodigoCaracteristica(JAXBElement<String> value) {
        this.codigoCaracteristica = value;
    }

    /**
     * Obtiene el valor de la propiedad nit.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getNit() {
        return nit;
    }

    /**
     * Define el valor de la propiedad nit.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setNit(BigInteger value) {
        this.nit = value;
    }

    /**
     * Obtiene el valor de la propiedad nitEmisor.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getNitEmisor() {
        return nitEmisor;
    }

    /**
     * Define el valor de la propiedad nitEmisor.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setNitEmisor(BigInteger value) {
        this.nitEmisor = value;
    }

    /**
     * Obtiene el valor de la propiedad paralela.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getParalela() {
        return paralela;
    }

    /**
     * Define el valor de la propiedad paralela.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setParalela(Integer value) {
        this.paralela = value;
    }

    /**
     * Obtiene el valor de la propiedad sucursal.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getSucursal() {
        return sucursal;
    }

    /**
     * Define el valor de la propiedad sucursal.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setSucursal(Integer value) {
        this.sucursal = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoAutorizacion.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public JAXBElement<String> getTipoAutorizacion() {
        return tipoAutorizacion;
    }

    /**
     * Define el valor de la propiedad tipoAutorizacion.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public void setTipoAutorizacion(JAXBElement<String> value) {
        this.tipoAutorizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad usuario.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public JAXBElement<String> getUsuario() {
        return usuario;
    }

    /**
     * Define el valor de la propiedad usuario.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public void setUsuario(JAXBElement<String> value) {
        this.usuario = value;
    }

}
