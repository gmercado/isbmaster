package com.bisa.bus.servicios.sin.ciclos.model;

import java.io.Serializable;

/**
 * @author by rsalvatierra on 28/12/2016.
 */
public class LogFacturasResult implements Serializable {
    private int codRespuesta;
    private String mensaje;

    public LogFacturasResult() {
    }

    public int getCodRespuesta() {
        return codRespuesta;
    }

    public void setCodRespuesta(int codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
