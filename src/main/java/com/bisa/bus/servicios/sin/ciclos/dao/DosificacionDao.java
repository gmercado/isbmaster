package com.bisa.bus.servicios.sin.ciclos.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.sin.ciclos.entities.Dosificacion;
import com.bisa.bus.servicios.sin.ciclos.entities.PKDosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.EstadoDosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.FormaDosificacion;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * @author by rsalvatierra on 23/12/2016.
 */
public class DosificacionDao extends DaoImpl<Dosificacion, PKDosificacion> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DosificacionDao.class);

    @Inject
    protected DosificacionDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    /**
     * Obtener Dosificaciones
     *
     * @return List<Dosificacion> dosificaciones
     */
    public HashMap<String, Dosificacion> getDosificaciones() {
        LOGGER.debug("Obteniendo lista dosificaciones");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        HashMap<String, Dosificacion> map = new HashMap<>();
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Dosificacion> q = cb.createQuery(Dosificacion.class);
                        Root<Dosificacion> p = q.from(Dosificacion.class);
                        TypedQuery<Dosificacion> query = entityManager.createQuery(q);
                        List<Dosificacion> dosificaciones = query.getResultList();
                        for (Dosificacion dosificacion : dosificaciones) {
                            map.put(dosificacion.getId().getNumeroAutorizacion(), dosificacion);
                        }
                        return map;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    /**
     * Obtener Dosificación
     *
     * @return Dosificacion
     */
    public List<Dosificacion> getDosificaciones(FormaDosificacion pFormaDosificacion, EstadoDosificacion pEstadoDosificacion) {
        LOGGER.debug("Obteniendo lista dosificaciones {}", pFormaDosificacion);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Dosificacion> q = cb.createQuery(Dosificacion.class);
                        Root<Dosificacion> p = q.from(Dosificacion.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("estado"), pEstadoDosificacion.getDescripcion()));
                        predicatesAnd.add(cb.equal(p.get("tipoProceso"), pFormaDosificacion.getCodigo()));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));

                        TypedQuery<Dosificacion> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    @Override
    protected Path<PKDosificacion> countPath(Root<Dosificacion> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<Dosificacion> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.get("id.numeroAutorizacion"));
        return paths;
    }
}
