package com.bisa.bus.servicios.sin.ciclos.model;

/**
 * @author by rsalvatierra on 17/02/2017.
 */
public enum TipoEvento {
    CONSULTAR(1, "Consultar PDF"),
    DESCARGAR(2, "Descargar PDF"),
    IMPRIMIR(3, "Imprimir PDF");

    private int codigo;
    private String descripcion;

    TipoEvento(int codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static TipoEvento get(int codigo) {
        for (TipoEvento valor : TipoEvento.values()) {
            if (valor.getCodigo() == codigo) {
                return valor;
            }
        }
        return null;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
