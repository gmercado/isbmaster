package com.bisa.bus.servicios.sin.ciclos.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.sin.ciclos.api.FacturacionCiclosProcess;
import com.bisa.bus.servicios.sin.ciclos.entities.Dosificacion;
import com.bisa.bus.servicios.sin.ciclos.entities.DosificacionAutomatica;
import com.bisa.bus.servicios.sin.ciclos.model.ResultadoDosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.TipoCodigoError;
import com.bisa.bus.servicios.sin.ciclos.model.TipoDosificacion;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static bus.users.api.Metadatas.USER_META_DATA_KEY;

/**
 * @author by rsalvatierra on 16/02/2017.
 */
@Titulo("Solicitar Dosificaci\u00F3n SIN")
@AuthorizeInstantiation({RolesBisa.SIN_DOSIFICACION})
public class SolicitarDosificacion extends BisaWebPage {

    private final String usuario;
    private final IModel<Dosificacion> variableIModel;
    @Inject
    FacturacionCiclosProcess facturacionCiclosProcess;
    private DosificacionAutomatica dosificacionAutomatica;

    public SolicitarDosificacion() {
        super();
        usuario = getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase();
        variableIModel = new CompoundPropertyModel<>(Model.of(new Dosificacion()));
        Form<Dosificacion> crear;
        add(crear = new Form<>("nueva", variableIModel));

        crear.add(new DropDownChoice<>("id.tipo", new PropertyModel<>(variableIModel, "tipoDosificacion"),
                new LoadableDetachableModel<List<? extends TipoDosificacion>>() {
                    @Override
                    protected List<? extends TipoDosificacion> load() {
                        return new ArrayList<>(Collections.singletonList((TipoDosificacion.CVME)));
                    }
                }, new IChoiceRenderer<TipoDosificacion>() {
            @Override
            public Object getDisplayValue(TipoDosificacion object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(TipoDosificacion object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoDosificacion getObject(String id, IModel<? extends List<? extends TipoDosificacion>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }));


        crear.add(new IndicatingAjaxButton("probar") {
            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                Dosificacion dosificacion = variableIModel.getObject();
                if (dosificacion != null && dosificacion.getTipoDosificacion() == null) {
                    getSession().warn("El Tipo es requerido.");
                    target.add(feedbackPanel);
                    return;
                }
                String resultado;
                try {
                    resultado = facturacionCiclosProcess.probarConexion();
                    getSession().info("Pruebas Conexion >> " + resultado);
                    target.add(feedbackPanel);
                } catch (Exception e) {
                    LOGGER.error("Error inesperado al probar conexion", e);
                    error(e.getMessage());
                    target.add(feedbackPanel);
                }
            }
        });

        crear.add(new IndicatingAjaxButton("iniciar") {
            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                Dosificacion dosificacion = variableIModel.getObject();
                if (dosificacion != null && dosificacion.getTipoDosificacion() == null) {
                    getSession().warn("El Tipo es requerido.");
                    target.add(feedbackPanel);
                    return;
                }
                try {
                    ResultadoDosificacion resultadoInicio = facturacionCiclosProcess.iniciarCiclo("CSCF", usuario);
                    if (resultadoInicio != null) {
                        if (TipoCodigoError.OK.equals(resultadoInicio.getCodError())) {
                            dosificacionAutomatica = resultadoInicio.getDosificacionAutomatica();
                            getSession().info("Inicio enviado exitosamente");
                            target.add(feedbackPanel);
                        } else {
                            getSession().error(resultadoInicio.getMensaje());
                            target.add(feedbackPanel);
                        }
                    }
                } catch (Exception e) {
                    LOGGER.error("Error inesperado al iniciar ciclo dosificacion", e);
                    error(e.getMessage());
                    target.add(feedbackPanel);
                }
            }
        });

        crear.add(new IndicatingAjaxButton("confirmar") {
            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                Dosificacion dosificacion = variableIModel.getObject();
                if (dosificacion != null && dosificacion.getTipoDosificacion() == null) {
                    getSession().warn("El Tipo es requerido.");
                    target.add(feedbackPanel);
                    return;
                }
                try {
                    if (dosificacionAutomatica != null) {
                        ResultadoDosificacion resultadoInicio = facturacionCiclosProcess.confirmarInicio(dosificacionAutomatica, usuario);
                        if (resultadoInicio != null) {
                            if (TipoCodigoError.OK.equals(resultadoInicio.getCodError())) {
                                getSession().info("Inicio confirmado exitosamente");
                                target.add(feedbackPanel);
                            } else {
                                getSession().error(resultadoInicio.getMensaje());
                                target.add(feedbackPanel);
                            }
                        }
                    }
                } catch (Exception e) {
                    LOGGER.error("Error inesperado al iniciar ciclo dosificacion", e);
                    error(e.getMessage());
                    target.add(feedbackPanel);
                }
            }
        });

        crear.add(new Button("cancelar") {
            private static final long serialVersionUID = 62944255371806955L;

            @Override
            public void onSubmit() {
                setResponsePage(ListaDosificaciones.class);
                getSession().info("Operaci\u00f3n cancelada");
            }
        }.setDefaultFormProcessing(false));
    }
}
