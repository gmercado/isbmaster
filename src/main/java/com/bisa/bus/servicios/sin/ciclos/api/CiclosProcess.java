package com.bisa.bus.servicios.sin.ciclos.api;

import bus.env.api.MedioAmbiente;
import bus.plumbing.utils.Convert;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.NumberToLetter;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.segip.dao.ClienteDao;
import com.bisa.bus.servicios.segip.entities.Cliente;
import com.bisa.bus.servicios.sin.ciclos.consumer.api.SinCiclosServicioWeb;
import com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.datacontract.Respuesta;
import com.bisa.bus.servicios.sin.ciclos.dao.*;
import com.bisa.bus.servicios.sin.ciclos.entities.*;
import com.bisa.bus.servicios.sin.ciclos.model.*;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static bus.env.api.Variables.*;
import static com.bisa.bus.servicios.sin.ciclos.api.ConfiguracionSIN.*;

/**
 * @author rsalvatierra on 20/12/2016.
 */
public class CiclosProcess implements FacturacionCiclosProcess {

    private static final Logger LOG = LoggerFactory.getLogger(CiclosProcess.class);

    private final FacturaDao facturaDao;
    private final SucursalDao sucursalDao;
    private final DosificacionDao dosificacionDao;
    private final DosificacionHistDao dosificacionHistDao;
    private final DosificacionAutomaticaDao dosificacionAutomaticaDao;
    private final ClienteDao clienteDao;
    private final BitacoraFacturaDao bitacoraFacturaDao;
    private final MedioAmbiente medioAmbiente;
    private final SinCiclosServicioWeb wsSinCiclos;
    private final ArchivoCierre archivoCierre;

    @Inject
    public CiclosProcess(FacturaDao facturaDao, SucursalDao sucursalDao, DosificacionDao dosificacionDao, DosificacionHistDao dosificacionHistDao, DosificacionAutomaticaDao dosificacionAutomaticaDao, ClienteDao clienteDao, BitacoraFacturaDao bitacoraFacturaDao, MedioAmbiente medioAmbiente, SinCiclosServicioWeb wsSinCiclos, ArchivoCierre archivoCierre) {
        this.facturaDao = facturaDao;
        this.sucursalDao = sucursalDao;
        this.dosificacionDao = dosificacionDao;
        this.dosificacionHistDao = dosificacionHistDao;
        this.dosificacionAutomaticaDao = dosificacionAutomaticaDao;
        this.clienteDao = clienteDao;
        this.bitacoraFacturaDao = bitacoraFacturaDao;
        this.medioAmbiente = medioAmbiente;
        this.wsSinCiclos = wsSinCiclos;
        this.archivoCierre = archivoCierre;
    }

    /*
    public String ping() {
        LOG.info("INICIO >> ping");
        String resultado = wsSinCiclos.ping();
        LOG.info("resultado >> {}", resultado);
        LOG.info("FIN >> fin");
        return resultado;
    }*/

    @Override
    public String probarConexion() throws Exception {
        LOG.info("INICIO >> probarConexion");
        String resultado = wsSinCiclos.pruebaConexion(getUsuarioSIN());
        LOG.info("resultado >> {}", resultado);
        LOG.info("FIN >> probarConexion");
        return resultado;
    }

    @Override
    public ResultadoDosificacion iniciarCiclo(String codigoCaracteristica, String usuario) {
        LOG.info("INICIO >> iniciarCiclo");
        ResultadoDosificacion resultadoDosificacion = new ResultadoDosificacion();
        try {
            List<Dosificacion> dosificacionList = dosificacionDao.getDosificaciones(FormaDosificacion.CICLOS, EstadoDosificacion.PENDIENTE);
            if (dosificacionList != null && !dosificacionList.isEmpty()) {
                resultadoDosificacion = getError(TipoCodigoError.EXISTE, SIN_MENSAJE_EXISTE_DOSIF_PENDIENTE);
            } else {
                if (existeDosificacion(codigoCaracteristica, EstadoAutomatico.INICIO)) {
                    resultadoDosificacion = getError(TipoCodigoError.EXISTE, SIN_MENSAJE_EXISTE);
                } else {
                    DosificacionAutomatica dosificacion = consultarServicioSIN(codigoCaracteristica, usuario);
                    if (dosificacion == null) {
                        resultadoDosificacion = getError(TipoCodigoError.SERV_NO_HAB, SIN_MENSAJE_ERROR_SERVICIO);
                    } else {
                        resultadoDosificacion.setCodError(TipoCodigoError.OK);
                        resultadoDosificacion.setMensaje(BISA_SIN_MENSAJE_SIN_ERROR);
                        resultadoDosificacion.setDosificacionAutomatica(dosificacion);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Hubo un error en registro de dosificacion ", e);
            resultadoDosificacion = getError(TipoCodigoError.ERR_INESPERADO, SIN_MENSAJE_ERROR_INESPERADO);
        }
        LOG.info("FIN >> iniciarCiclo");
        return resultadoDosificacion;
    }

    @Override
    public ResultadoDosificacion confirmarInicio(DosificacionAutomatica dosificacionAutomatica, String usuario) {
        LOG.info("INICIO >> confirmarInicio");
        ResultadoDosificacion resultadoDosificacion = new ResultadoDosificacion();
        try {
            List<Dosificacion> dosificacionList = dosificacionDao.getDosificaciones(FormaDosificacion.CICLOS, EstadoDosificacion.PENDIENTE);
            if (dosificacionList != null && !dosificacionList.isEmpty()) {
                resultadoDosificacion = getError(TipoCodigoError.EXISTE, SIN_MENSAJE_EXISTE_DOSIF_PENDIENTE);
            } else {
                if (!existeDosificacion(dosificacionAutomatica.getCodigoCaracteristica(), EstadoAutomatico.INICIO)) {
                    resultadoDosificacion = getError(TipoCodigoError.NO_EXISTE, SIN_MENSAJE_EXISTE);
                } else if (StringUtils.trimToNull(dosificacionAutomatica.geteTicket()) == null) {
                    resultadoDosificacion = getError(TipoCodigoError.SIN_INICIO, SIN_MENSAJE_SIN_INICIO);
                } else {
                    DosificacionAutomatica dosificacion = consultarServicioSIN(dosificacionAutomatica, usuario);
                    if (dosificacion == null) {
                        resultadoDosificacion = getError(TipoCodigoError.SERV_NO_HAB, SIN_MENSAJE_ERROR_SERVICIO);
                    } else {
                        //Cargar dosificacion
                        cargarDosificacion(dosificacionAutomatica, usuario);
                        resultadoDosificacion.setCodError(TipoCodigoError.OK);
                        resultadoDosificacion.setMensaje(BISA_SIN_MENSAJE_SIN_ERROR);
                        resultadoDosificacion.setDosificacionAutomatica(dosificacion);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Hubo un error en la confirmacion del registro de dosificacion ", e);
            resultadoDosificacion = getError(TipoCodigoError.ERR_INESPERADO, SIN_MENSAJE_ERROR_INESPERADO);
        }
        LOG.info("FIN >> confirmarInicio");
        return resultadoDosificacion;
    }

    @Override
    public ResultadoDosificacion cierreCiclo(DosificacionAutomatica dosificacionAutomatica, String usuario) {
        LOG.info("INICIO >> cierreCiclo");
        ResultadoDosificacion resultadoDosificacion = new ResultadoDosificacion();
        try {
            if (!existeDosificacion(dosificacionAutomatica.getCodigoCaracteristica(), EstadoAutomatico.CONFIRMACION)) {
                resultadoDosificacion = getError(TipoCodigoError.NO_EXISTE, SIN_MENSAJE_EXISTE);
            } else if (StringUtils.trimToNull(dosificacionAutomatica.geteTicket()) == null) {
                resultadoDosificacion = getError(TipoCodigoError.SIN_INICIO, SIN_MENSAJE_SIN_INICIO);
            } else {
                DosificacionAutomatica dosificacion = consultarServicioSINCierre(dosificacionAutomatica, usuario);
                if (dosificacion == null) {
                    resultadoDosificacion = getError(TipoCodigoError.SERV_NO_HAB, SIN_MENSAJE_ERROR_SERVICIO);
                } else {
                    cerrarDosificacion(dosificacion, usuario);
                    resultadoDosificacion.setCodError(TipoCodigoError.OK);
                    resultadoDosificacion.setMensaje(BISA_SIN_MENSAJE_SIN_ERROR);
                    resultadoDosificacion.setDosificacionAutomatica(dosificacion);
                }
            }
        } catch (Exception e) {
            LOG.error("Hubo un error en el cierre del ciclo de la dosificacion ", e);
            resultadoDosificacion = getError(TipoCodigoError.ERR_INESPERADO, SIN_MENSAJE_ERROR_INESPERADO);
        }
        LOG.info("FIN >> cierreCiclo");
        return resultadoDosificacion;
    }

    @Override
    public FacturasResult obtenerFacturas(RequestQuery query, String usuario) {
        boolean queryByNumCliente = false;
        Cliente cliente = null;
        List<Factura> listaFacturas;
        FacturasResult result = new FacturasResult();
        try {
            TipoPersona tipoPersona = TipoPersona.get(query.getTipoPersona());
            if (tipoPersona == null) {
                throw new Exception(getMensaje(REQ_TIPO_PERSONA));
            }
            if (query.getNumeroDocumento() == null) {
                throw new Exception(getMensaje(REQ_NUM_DOCUMENTO));
            }
            if (query.getInfoAdicional() == null) {
                throw new Exception(getMensaje(REQ_INFO_ADICIONAL));
            }
            if (query.getBrowser() == null) {
                throw new Exception(getMensaje(REQ_BROWSER));
            }
            if (query.getIp() == null) {
                throw new Exception(getMensaje(REQ_IP));
            }
            if (query.getTerminal() == null) {
                throw new Exception(getMensaje(REQ_TERMINAL));
            }
            query.setInfoAdicional(StringUtils.trimToEmpty(query.getInfoAdicional()));
            //Validar informacion adicional
            if (TipoPersona.NATURAL.equals(tipoPersona)) {
                cliente = clienteDao.getCliente(query.getNumeroDocumento().toPlainString());
                //Cliente
                if (cliente != null) {
                    //Persona Natural
                    Date fechaNacimientoBD = Convert.fromJulian(cliente.getFechaNacimiento().toString());
                    Date fechaNacimiento = FormatosUtils.getFechaByString(query.getInfoAdicional());
                    if (fechaNacimiento == null || !fechaNacimiento.equals(fechaNacimientoBD)) {
                        throw new Exception(getMensaje(ERROR_INFO_ADICIONAL));
                    }
                }
            } else {
                //Persona Juridica
                if (!BigDecimal.ZERO.equals(query.getNumeroDocumento())) {
                    cliente = clienteDao.getClienteJuridico(query.getNumeroDocumento().toPlainString());
                }
                if (cliente != null) {
                    BigDecimal numClienteConsulta = new BigDecimal(query.getInfoAdicional());
                    BigDecimal numCliente = new BigDecimal(cliente.getId());
                    if (!numCliente.equals(numClienteConsulta)) {
                        throw new Exception(getMensaje(ERROR_INFO_ADICIONAL));
                    }
                } else {
                    cliente = clienteDao.getClienteById(query.getInfoAdicional());
                    if (cliente != null) {
                        queryByNumCliente = true;
                        if (StringUtils.trimToNull(cliente.getNit()) != null) {
                            String nitCOnsulta = query.getNumeroDocumento().toPlainString();
                            String nit = cliente.getNit();
                            if (!nit.equals(nitCOnsulta)) {
                                throw new Exception(getMensaje(ERROR_INFO_ADICIONAL));
                            }
                        }
                    }
                }
            }
            //Obtener Sucursales
            HashMap<BigDecimal, Sucursal> listaSucursales = sucursalDao.getSucursales();
            HashMap<String, Dosificacion> listaDosificaciones = dosificacionDao.getDosificaciones();
            //Obtener Facturas
            if (queryByNumCliente) {
                listaFacturas = facturaDao.getFacturasByNumCliente(cliente.getId());
            } else {
                listaFacturas = facturaDao.getFacturas(query.getNumeroDocumento(), tipoPersona);
            }
            if (listaFacturas != null && !listaFacturas.isEmpty()) {
                for (Factura factura : listaFacturas) {
                    //Cabecera
                    CabeceraFactura cabeceraFactura = getCabecera(factura, listaSucursales, listaDosificaciones);
                    //Detalle
                    cabeceraFactura.add(getDetalleFactura(factura));
                    result.add(cabeceraFactura);
                }
            }
            if (result.getFacturas() != null && !result.getFacturas().isEmpty()) {
                result.setCodRespuesta(0);
                result.setMensaje(getMensaje(SIN_ERROR));
            } else {
                result.setCodRespuesta(1);
                result.setMensaje(getMensaje(SIN_FACTURAS));
            }
        } catch (Exception e) {
            LOG.error("Hubo un error:", e);
            result.setCodRespuesta(999);
            result.setMensaje("Error:" + e.getMessage());
        } finally {
            try {
                result.setIdentificador(bitacoraFacturaDao.registrarBitacora(query, cliente, result.getMensaje(), usuario));
            } catch (Exception ex) {
                LOG.error("Hubo un error al registrar la bitacora de facturas", ex);
            }
        }
        return result;
    }

    @Override
    public LogFacturasResult registrarLogFacturas(RequestQueryLog query, String usuario) {
        LogFacturasResult result = new LogFacturasResult();
        try {
            TipoPersona tipoPersona = TipoPersona.get(query.getTipoPersona());
            if (tipoPersona == null) {
                throw new Exception(getMensaje(REQ_TIPO_PERSONA));
            }
            if (query.getNumeroDocumento() == null) {
                throw new Exception(getMensaje(REQ_NUM_DOCUMENTO));
            }
            if (query.getBrowser() == null) {
                throw new Exception(getMensaje(REQ_BROWSER));
            }
            if (query.getIp() == null) {
                throw new Exception(getMensaje(REQ_IP));
            }
            if (query.getTerminal() == null) {
                throw new Exception(getMensaje(REQ_TERMINAL));
            }
            if (query.getNumeroAutorizacion() == null) {
                throw new Exception(getMensaje(REQ_NUM_AUTORIZACION));
            }
            if (query.getNumeroFactura() == null) {
                throw new Exception(getMensaje(REQ_NUM_FACTURA));
            }
            if (query.getIdentificador() == null) {
                throw new Exception(getMensaje(REQ_IDENTIFICADOR));
            }
            /*if (query.getRespuesta() == null) {
                throw new Exception(getMensaje(REQ_RESPUESTA));
            }*/
            TipoEvento evento = TipoEvento.get(query.getEvento());
            if (evento == null) {
                throw new Exception(getMensaje(REQ_EVENTO));
            }
            bitacoraFacturaDao.registrarBitacora(query, usuario);
            result.setCodRespuesta(0);
            result.setMensaje(getMensaje(SIN_ERROR));
        } catch (Exception e) {
            LOG.error("Hubo un error:", e);
            result.setCodRespuesta(999);
            result.setMensaje("Error:" + e.getMessage());
        }
        return result;
    }

    private String getRazonSocial() {
        return medioAmbiente.getValorDe(BISA_RAZON_SOCIAL, BISA_RAZON_SOCIAL_DEFAULT);
    }

    private String getCasaMatriz() {
        return medioAmbiente.getValorDe(BISA_CASA_MATRIZ, BISA_CASA_MATRIZ_DEFAULT);
    }

    private BigDecimal getNIT() {
        return medioAmbiente.getValorBigDecimalDe(BISA_NIT, BISA_NIT_DEFAULT);
    }

    private String getLugarNacional() {
        return medioAmbiente.getValorDe(BISA_NACIONAL, BISA_NACIONAL_DEFAULT);
    }

    private String getLeyendaDefault() {
        return medioAmbiente.getValorDe(BISA_LEYENDA, BISA_LEYENDA_DEFAULT);
    }

    private String getUsuarioSIN() {
        return medioAmbiente.getValorDe(BISA_SIN_USUARIO, BISA_SIN_USUARIO_DEFAULT);
    }

    private Integer getSucursal() {
        return medioAmbiente.getValorIntDe(BISA_CODIGO_SUCURSAL_DOSIFICACION, BISA_CODIGO_SUCURSAL_DOSIFICACION_DEFAULT);
    }

    private Integer getParalela() {
        return medioAmbiente.getValorIntDe(BISA_CODIGO_DOSIFICACION_PARALELA, BISA_CODIGO_DOSIFICACION_PARALELA_DEFAULT);
    }

    private Long getCodigoActividadEconomica() {
        return medioAmbiente.getValorLongDe(BISA_CODIGO_ACTIVIDAD_ECONOMICA, BISA_CODIGO_ACTIVIDAD_ECONOMICA_DEFAULT);
    }

    private String getMensaje(int tipo) {
        String mensaje;
        switch (tipo) {
            case REQ_TIPO_PERSONA:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_TIPO_PERSONA, BISA_SIN_MENSAJE_TIPO_PERSONA_DEFAULT);
                break;
            case REQ_NUM_DOCUMENTO:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_NUMERO_DOCUMENTO, BISA_SIN_MENSAJE_NUMERO_DOCUMENTO_DEFAULT);
                break;
            case REQ_INFO_ADICIONAL:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_INFO_ADICIONAL, BISA_SIN_MENSAJE_INFO_ADICIONAL_DEFAULT);
                break;
            case ERROR_INFO_ADICIONAL:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_ERROR_INFO_ADICIONAL, BISA_SIN_MENSAJE_ERROR_INFO_ADICIONAL_DEFAULT);
                break;
            case SIN_FACTURAS:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_SIN_FACTURAS, BISA_SIN_MENSAJE_SIN_FACTURAS_DEFAULT);
                break;
            case SIN_ERROR:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_SIN_ERROR, BISA_SIN_MENSAJE_SIN_ERROR_DEFAULT);
                break;
            case REQ_BROWSER:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_BROWSER, BISA_SIN_MENSAJE_BROWSER_DEFAULT);
                break;
            case REQ_IP:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_IP, BISA_SIN_MENSAJE_IP_DEFAULT);
                break;
            case REQ_TERMINAL:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_TERMINAL, BISA_SIN_MENSAJE_TERMINAL_DEFAULT);
                break;
            case REQ_NUM_AUTORIZACION:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_NUM_AUTORIZACION, BISA_SIN_MENSAJE_NUM_AUTORIZACION_DEFAULT);
                break;
            case REQ_NUM_FACTURA:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_NUM_FACTURA, BISA_SIN_MENSAJE_NUM_FACTURA_DEFAULT);
                break;
            case REQ_IDENTIFICADOR:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_IDENTIFICADOR, BISA_SIN_MENSAJE_IDENTIFICADOR_DEFAULT);
                break;
            case REQ_RESPUESTA:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_RESPUESTA, BISA_SIN_MENSAJE_RESPUESTA_DEFAULT);
                break;
            case REQ_EVENTO:
                mensaje = medioAmbiente.getValorDe(BISA_SIN_MENSAJE_EVENTO, BISA_SIN_MENSAJE_EVENTO_DEFAULT);
                break;
            default:
                mensaje = "";
                break;
        }
        return mensaje;
    }

    private String getMensajesArray(List<String> errorList, EstadoAutomatico estado) {
        StringBuilder mensajes = new StringBuilder("");
        if (errorList != null) {
            for (String item : errorList) {
                LOG.info("error SIN >> {}", item);
                mensajes.append(estado.getDescripcion()).append("-").append(item).append("\n");
            }
        }
        return mensajes.toString();
    }

    private DosificacionAutomatica consultarServicioSIN(String codigoCaracteristica, String usuario) throws Exception {
        //Enviar solicitud dosificacion
        SolicitudDosificacion solicitudDosificacion = new SolicitudDosificacion();
        solicitudDosificacion.setCodigoActividadEconomica(getCodigoActividadEconomica());
        solicitudDosificacion.setCodigoCaracteristica(codigoCaracteristica);
        solicitudDosificacion.setNit(getNIT().toBigInteger());
        solicitudDosificacion.setNitEmisor(BigInteger.ZERO);
        solicitudDosificacion.setParalela(getParalela());
        solicitudDosificacion.setSucursal(getSucursal());
        solicitudDosificacion.setTipoAutorizacion(null);
        solicitudDosificacion.setUsuario(getUsuarioSIN());
        Respuesta respuesta = wsSinCiclos.iniciarCiclo(solicitudDosificacion);
        if (respuesta != null && respuesta.getETicket() != null) {
            String eTicket = respuesta.getETicket().getValue();
            //Registrar solicitud de dosificacion
            DosificacionAutomatica dosificacion = dosificacionAutomaticaDao.getDosificacion(eTicket, codigoCaracteristica);
            if (dosificacion == null) {
                dosificacion = new DosificacionAutomatica();
            }
            dosificacion.seteTicket(eTicket);
            dosificacion.setCodigoCaracteristica(codigoCaracteristica);
            dosificacion.setEstado(EstadoAutomatico.INICIO.getDescripcion());
            dosificacion.setFechaCreacion(new Date());
            dosificacion.setUsuarioCreador(usuario);
            if (respuesta.getListaMensajes() != null && respuesta.getListaMensajes().getValue() != null) {
                dosificacion.setMensajes(getMensajesArray(respuesta.getListaMensajes().getValue().getString(), EstadoAutomatico.INICIO));
            }
            return dosificacionAutomaticaDao.merge(dosificacion);
        }
        return null;
    }

    private ResultadoDosificacion getError(TipoCodigoError error, String codMensaje) {
        ResultadoDosificacion resultadoDosificacion = new ResultadoDosificacion();
        resultadoDosificacion.setCodError(error);
        resultadoDosificacion.setMensaje(medioAmbiente.getValorDe(codMensaje, ""));
        return resultadoDosificacion;
    }

    private boolean existeDosificacion(String codigoCaracteristica, EstadoAutomatico estado) {
        return ((dosificacionAutomaticaDao.getDosificacion(codigoCaracteristica, estado)) != null);
    }

    private DosificacionAutomatica consultarServicioSIN(DosificacionAutomatica dosificacionAutomatica, String usuario) throws Exception {
        SolicitudDosificacion solicitudDosificacion = new SolicitudDosificacion();
        solicitudDosificacion.seteTicket(dosificacionAutomatica.geteTicket());
        solicitudDosificacion.setNit(getNIT().toBigInteger());
        solicitudDosificacion.setUsuario(getUsuarioSIN());
        Respuesta respuesta = wsSinCiclos.confirmarInicio(solicitudDosificacion);
        if (respuesta != null) {
            if (respuesta.getListaMensajes() != null && respuesta.getListaMensajes().getValue() != null) {
                String mensajes = dosificacionAutomatica.getMensajes() + "\n";
                mensajes += getMensajesArray(respuesta.getListaMensajes().getValue().getString(), EstadoAutomatico.CONFIRMACION);
                dosificacionAutomatica.setMensajes(mensajes);
            }
            if (respuesta.getNumeroAutorizacion() != null) {
                dosificacionAutomatica.setRangoDesde(respuesta.getRangoDesde());
                dosificacionAutomatica.setNumeroAutorizacion(respuesta.getNumeroAutorizacion());
                if (respuesta.getLlave() != null) {
                    dosificacionAutomatica.setLlave(respuesta.getLlave().getValue());
                }
                if (respuesta.getLeyenda() != null) {
                    dosificacionAutomatica.setLeyenda(respuesta.getLeyenda().getValue());
                }
                dosificacionAutomatica.setFechaFin(respuesta.getConfirmacion());
                dosificacionAutomatica.setEstado(EstadoAutomatico.CONFIRMACION.getDescripcion());
            }
            dosificacionAutomatica.setUsuarioModificador(usuario);
            dosificacionAutomatica.setFechaModificacion(new Date());
            return dosificacionAutomaticaDao.merge(dosificacionAutomatica);
        }
        return null;
    }

    private DosificacionAutomatica consultarServicioSINCierre(DosificacionAutomatica dosificacionAutomatica, String usuario) throws Exception {
        CierreDosificacion cierreDosificacion = archivoCierre.generaArchivo(dosificacionAutomatica);
        cierreDosificacion.seteTicket(dosificacionAutomatica.geteTicket());
        cierreDosificacion.setNit(getNIT().toBigInteger());
        cierreDosificacion.setUsuario(getUsuarioSIN());
        Respuesta respuesta = wsSinCiclos.finalizarCiclo(cierreDosificacion);
        if (respuesta != null && respuesta.getListaMensajes() != null && respuesta.getListaMensajes().getValue() != null) {
            String mensajes = dosificacionAutomatica.getMensajes() + "\n";
            mensajes += getMensajesArray(respuesta.getListaMensajes().getValue().getString(), EstadoAutomatico.CERRADO);
            dosificacionAutomatica.setMensajes(mensajes);
        }
        if (respuesta != null && respuesta.getNumeroAutorizacion() != null) {
            dosificacionAutomatica.setCantidadFacturas(BigInteger.valueOf(cierreDosificacion.getNumeroFacturas()));
            dosificacionAutomatica.setMontoTotalFacturas(cierreDosificacion.getMontoTotal());
            dosificacionAutomatica.setArchivo(cierreDosificacion.getArchivo());
            dosificacionAutomatica.setEstado(EstadoAutomatico.CERRADO.getDescripcion());
        }
        dosificacionAutomatica.setUsuarioModificador(usuario);
        dosificacionAutomatica.setFechaModificacion(new Date());
        return dosificacionAutomaticaDao.merge(dosificacionAutomatica);
    }


    private CabeceraFactura getCabecera(Factura factura, HashMap<BigDecimal, Sucursal> listaSucursales, HashMap<String, Dosificacion> listaDosificaciones) {
        CabeceraFactura cabeceraFactura = new CabeceraFactura();
        cabeceraFactura.setNitBanco(getNIT());
        cabeceraFactura.setRazonSocial(getRazonSocial());
        cabeceraFactura.setCasaMatriz(getCasaMatriz());
        cabeceraFactura.setNumeroFactura(new BigDecimal(factura.getId().getNumeroFactura()));
        cabeceraFactura.setNumeroAutorizacion(String.valueOf(factura.getNumeroAutorizacion()));
        if (listaSucursales != null) {
            Sucursal sucursal = listaSucursales.get(factura.getId().getCodSucursal());
            if (sucursal != null) {
                cabeceraFactura.setLugar(StringUtils.trimToEmpty(sucursal.getSucursal()));
                cabeceraFactura.setAgencia(StringUtils.trimToEmpty(remove1(sucursal.getAgencia())));
                //En caso de Sucursal Nacional
                if (SucursalDosificacion.NACIONAL.name().equals(cabeceraFactura.getLugar())) {
                    cabeceraFactura.setLugar(getLugarNacional());
                }
            }
        }
        if (factura.getFecha() != null) {
            cabeceraFactura.setFecha(FormatosUtils.fechaLiteral(factura.getFecha()));
        }
        cabeceraFactura.setDocumento(factura.getNumeroDocumento());
        cabeceraFactura.setBeneficiario(StringUtils.trimToEmpty(remove1(factura.getBeneficiario())));
        cabeceraFactura.setMontoTotal(factura.getMontoCambiado());
        cabeceraFactura.setDetalleMonto(NumberToLetter.convertNumberToLetter(factura.getMontoCambiado().toString()));
        cabeceraFactura.setCodigoControl(StringUtils.trimToEmpty(factura.getCodigoControl()));
        if (listaDosificaciones != null && listaDosificaciones.size() > 0 && factura.getNumeroAutorizacion() != null) {
            Dosificacion dosificacion = listaDosificaciones.get(String.valueOf(factura.getNumeroAutorizacion()));
            if (dosificacion != null) {
                cabeceraFactura.setLeyenda(StringUtils.trimToEmpty(dosificacion.getLeyenda()));
            }
            if (cabeceraFactura.getLeyenda() == null) {
                cabeceraFactura.setLeyenda(getLeyendaDefault());
            }
        }
        if (factura.getFecha() != null) {
            cabeceraFactura.setFechaLimiteEmision(FormatosUtils.fechaYYYYMMDD(factura.getFechaLimite()));
        }
        return cabeceraFactura;
    }

    private DetalleFactura getDetalleFactura(Factura factura) {
        DetalleFactura detalleFactura = new DetalleFactura();
        detalleFactura.setCantidad(factura.getMontoTotal());
        if (factura.getTipoTransaccion() != null) {
            TipoTransaccion tipoTransaccion = TipoTransaccion.get(factura.getTipoTransaccion());
            if (tipoTransaccion != null) {
                detalleFactura.setConcepto(tipoTransaccion.getDescripcion());
            }
        }
        detalleFactura.setTipoCambioOficial(factura.getTipoCambioContable());
        detalleFactura.setTipoCambio(factura.getTipoCambio());
        detalleFactura.setDiferencia(factura.getDiferenciaCambio());
        detalleFactura.setSubtotal(factura.getMontoCambiado());
        return detalleFactura;
    }

    private void cargarDosificacion(DosificacionAutomatica dosificacionAutomatica, String usuario) throws Exception {
        PKDosificacion pkDosificacion = new PKDosificacion();
        Dosificacion dosificacion = new Dosificacion();
        dosificacion.setId(pkDosificacion);
        dosificacion.getId().setCodBanco(new BigDecimal(BancoDosificacion.BISA.getCodigo()));
        dosificacion.getId().setCodSucursal(new BigDecimal(SucursalDosificacion.NACIONAL.getCodigo()));
        dosificacion.getId().setCodSector(new BigDecimal(SectorDosificacion.TODOS.getCodigo()));
        dosificacion.getId().setTipo(TipoDosificacion.CVME.getCodigo());
        dosificacion.setEstado(EstadoDosificacion.PENDIENTE.getDescripcion());
        if (dosificacionAutomatica.getNumeroAutorizacion() != null) {
            dosificacion.getId().setNumeroAutorizacion(dosificacionAutomatica.getNumeroAutorizacion().toString());
        }
        //Verificar que no exista repetido
        Dosificacion dos1 = dosificacionDao.find(dosificacion.getId());
        if (dos1 != null) {
            throw new Exception("La dosificaci\u00f3n ya existe, verifique.");
        }
        int cantidad = medioAmbiente.getValorIntDe(SIN_DOSIFICACIONES_PENDIENTES, SIN_DOSIFICACIONES_PENDIENTES_DEFAULT);
        //Verificar que solo sea un pendiente
        List<Dosificacion> listaDosificaciones = dosificacionDao.getDosificaciones(FormaDosificacion.CICLOS, EstadoDosificacion.PENDIENTE);
        if (listaDosificaciones != null && listaDosificaciones.size() == cantidad) {
            throw new Exception("Existen otras dosificaciones manuales pendientes, verifique.");
        }
        //Desde
        dosificacion.setFechaDesde(new Date());
        if (dosificacion.getFechaDesde() != null) {
            dosificacion.setFechaInicio(new BigDecimal(FormatosUtils.fechaYYYYMMDD(dosificacion.getFechaDesde())));
        } else {
            dosificacion.setFechaInicio(BigDecimal.ZERO);
        }
        //Hasta
        dosificacion.setFechaFin(new BigDecimal(dosificacionAutomatica.getFechaFin()));
        dosificacion.setFechaRegistro(new Date());
        dosificacion.setTipoProceso(FormaDosificacion.CICLOS.getCodigo());
        dosificacion.setCodAlfanumerico(" ");
        dosificacion.setUsuario(usuario);
        dosificacion.setIp("localhost");
        dosificacion.setTerminal(" ");
        //datoso adicionales del servicio
        dosificacion.setLlave(dosificacionAutomatica.getLlave());
        if (dosificacionAutomatica.getRangoDesde() != null) {
            dosificacion.setNumeroFacturaDesde(dosificacionAutomatica.getRangoDesde().toString());
        }
        dosificacion.setNumeroFacturaHasta("999999999");
        dosificacion.setNumeroControl("90000000");
        dosificacion.setNumFactura(dosificacion.getNumeroFacturaDesde());
        dosificacion.setLeyenda(dosificacionAutomatica.getLeyenda());
        dosificacionDao.persist(dosificacion);
    }

    private void cerrarDosificacion(DosificacionAutomatica dosificacionAutomatica, String usuario) throws Exception {
        TransferObject to = new TransferObject();
        to.initializate();

        PKDosificacion pkDosificacion = new PKDosificacion();
        pkDosificacion.setCodBanco(new BigDecimal(BancoDosificacion.BISA.getCodigo()));
        pkDosificacion.setCodSucursal(new BigDecimal(SucursalDosificacion.NACIONAL.getCodigo()));
        pkDosificacion.setCodSector(new BigDecimal(SectorDosificacion.TODOS.getCodigo()));
        pkDosificacion.setTipo(TipoDosificacion.CVME.getCodigo());
        pkDosificacion.setNumeroAutorizacion(dosificacionAutomatica.getNumeroAutorizacion().toString());
        Dosificacion dosificacion = dosificacionDao.find(pkDosificacion);
        DosificacionHistorico dh = to.convert(dosificacion, DosificacionHistorico.class);
        dosificacionHistDao.persist(dh);
        dosificacion.setEstado(EstadoDosificacion.INACTIVO.getDescripcion());
        dosificacion.setFechaRegistro(new Date());
        dosificacion.setUsuario(usuario);
        dosificacion.setIp("localhost");
        dosificacion.setTerminal(" ");
        dosificacionDao.merge(dosificacion);
    }
    /*
    private void generaEmail(ArchivoProfile archivoProfile) {
        // Generacion del mail
        String mails = medioAmbiente.getValorDe(SIN_EMAIL_ENVIO, SIN_EMAIL_ENVIO_DEFAULT);
        String[] arraysMail = StringUtils.split(mails, ",;");

        String subject = medioAmbiente.getValorDe(SIN_SUBJECT_MAIL, SIN_SUBJECT_MAIL_DEFAULT) +
                " " + FormatosUtils.fechaFormateadaConYYYYMMDD(new Date());
        StringBuilder sb = new StringBuilder();

        Mailer mail = mailerFactory.getMailer(arraysMail);

        if (archivoProfile != null) {
            sb.append("\n\n");
            sb.append("Se\u00f1ores,\n\n");
            sb.append("Adjuntamos a la presente, el archivo correspondiente a las factura a la fecha: ");
            sb.append(FormatosUtils.fechaFormateadaConYYYY(new Date()));
            sb.append(" bajo el siguiente detalle:\n\n");
            sb.append("\tNombre del archivo                      Registros\n");
            sb.append("\t------------------------                ---------\n\t");

            sb.append(StringUtils.rightPad(archivoProfile.getArchivo().getName(), 36, " "));
            sb.append("\t");
            sb.append(archivoProfile.getNroFilas());
            sb.append("\n\t");

            sb.append("\n\nAtte,");
            sb.append("\nSistema Aqua-ISB\n");
            sb.append("Banco Bisa S.A.\n");

            LOG.debug("MAIL GENERADO:\n" + sb.toString());
            mail.mailAndForgetTempFileExample(subject, sb.toString(), archivoProfile.getArchivo(), null);

        } else {
            LOG.warn("No se genera archivo de facturas");

            sb.append("\n\n");
            sb.append("Se\u00f1ores,\n\n");
            sb.append("Enviamos el correo correspondiente a la fecha ");
            sb.append(FormatosUtils.fechaFormateadaConYYYY(new Date()));
            sb.append(" indicando que no hubieron facturas.");

            sb.append("\n\nAtte,");
            sb.append("\nSistema Aqua-ISB\n");
            sb.append("Banco Bisa S.A.\n");

            LOG.debug("MAIL GENERADO:\n" + sb.toString());
            mail.mailAndForget(subject, sb.toString());
        }
    }*/

    private String remove1(String input) {
        if (input != null) {
            // Cadena de caracteres original a sustituir.
            String original = medioAmbiente.getValorDe(SIN_CARACTERES_ESPECIALES, SIN_CARACTERES_ESPECIALES_DEFAULT);
            // Cadena de caracteres ASCII que reemplazarán los originales.
            String ascii = medioAmbiente.getValorDe(SIN_CARACTERES_ESPECIALES_REEMPLAZO, SIN_CARACTERES_ESPECIALES_REEMPLAZO_DEFAULT);
            String output = input;
            for (int i = 0; i < original.length(); i++) {
                // Reemplazamos los caracteres especiales.
                output = output.replace(original.charAt(i), ascii.charAt(i));
            }
            return output;
        }
        return null;
    }
}
