package com.bisa.bus.servicios.sin.ciclos.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.sin.ciclos.api.ReporteConsumosPortal;
import com.bisa.bus.servicios.sin.ciclos.dao.BitacoraFacturaDao;
import com.bisa.bus.servicios.sin.ciclos.entities.BitacoraFactura;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler;
import org.apache.wicket.util.resource.AbstractResourceStreamWriter;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * @author by rsalvatierra on 21/02/2017.
 */
@AuthorizeInstantiation({RolesBisa.SIN_ADM})
@Menu(value = "Reporte - Consultas Facturas del Portal", subMenu = SubMenu.SIN)
public class ListadoConsumosFacturasPortal extends Listado<BitacoraFactura, Long> {

    @Override
    protected ListadoPanel<BitacoraFactura, Long> newListadoPanel(String id) {

        return new ListadoPanel<BitacoraFactura, Long>(id) {
            @Override
            protected List<IColumn<BitacoraFactura, String>> newColumns(ListadoDataProvider<BitacoraFactura, Long> dataProvider) {
                List<IColumn<BitacoraFactura, String>> columns = new LinkedList<>();
                columns.add(new PropertyColumn<>(Model.of("ID"), "id", "id"));
                columns.add(new PropertyColumn<>(Model.of("Fecha Hora"), "fechaConsumo", "fechaConsumo"));
                columns.add(new PropertyColumn<>(Model.of("Acci\u00f3n"), "tipoEventoResp"));
                columns.add(new PropertyColumn<>(Model.of("Tipo Persona"), "tipoPersonaResp"));
                columns.add(new PropertyColumn<>(Model.of("Nro. Cliente"), "numCliente"));
                columns.add(new PropertyColumn<>(Model.of("Nro. Documento"), "numeroDocumento"));
                columns.add(new PropertyColumn<>(Model.of("Beneficiario"), "nomBeneficiario"));
                columns.add(new PropertyColumn<>(Model.of("Nro. Factura"), "numeroFactura"));
                columns.add(new PropertyColumn<>(Model.of("Nro. Autorizaci\u00f3n"), "numeroAutorizacion"));
                columns.add(new PropertyColumn<>(Model.of("Respuesta"), "respuesta"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<BitacoraFactura, Long>> getProviderClazz() {
                return BitacoraFacturaDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<BitacoraFactura> newFiltroPanel(String id, IModel<BitacoraFactura> model1, IModel<BitacoraFactura> model2) {
                ConsumosFacturasPortalFiltro filtro = new ConsumosFacturasPortalFiltro(id, model1, model2);
                filtro.add(new Link("saveExcel") {
                    @Override
                    public void onClick() {
                        if (getModel1() == null || getModel1().getObject() == null) {
                            getSession().error("Hubo un error al exportar el archivo.");
                            return;
                        }
                        if (getModel1().getObject().getFecha() == null || getModel2().getObject().getFecha() == null) {
                            getSession().error("Las fechas son requeridas.");
                            return;
                        }
                        AbstractResourceStreamWriter resourceStreamWriter = new AbstractResourceStreamWriter() {
                            @Override
                            public void write(final OutputStream output) throws IOException {
                                BitacoraFacturaDao bitacoraFacturaDao = getInstance(BitacoraFacturaDao.class);
                                List<BitacoraFactura> consumos = bitacoraFacturaDao.getConsumos(getModel1().getObject(), getModel2().getObject());
                                byte[] reporte = ReporteConsumosPortal.get(consumos);
                                if (reporte != null) {
                                    output.write(reporte);
                                    output.flush();
                                    output.close();
                                }
                            }
                        };

                        getRequestCycle().scheduleRequestHandlerAfterCurrent(new ResourceStreamRequestHandler(resourceStreamWriter)
                                .setFileName(ReporteConsumosPortal.NOMBRE_REPORTE + ReporteConsumosPortal.EXTENSION_REPORTE));
                    }
                });
                return filtro;
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("fecha", false);
            }


            @Override
            protected BitacoraFactura newObject2() {
                return new BitacoraFactura();
            }

            @Override
            protected BitacoraFactura newObject1() {
                return new BitacoraFactura();
            }

            @Override
            protected boolean isVisibleData() {
                BitacoraFactura bitacora = getModel1().getObject();
                BitacoraFactura bitacora2 = getModel2().getObject();
                return !((bitacora.getFecha() == null || bitacora2.getFecha() == null));
            }
        };
    }

}
