package com.bisa.bus.servicios.sin.ciclos.entities;

import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.sin.ciclos.model.EstadoDosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.FormaDosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.TipoDosificacion;
import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author by rsalvatierra on 23/12/2016.
 */
@Entity
@Table(name = "FAP03301H")
public class DosificacionHistorico implements Serializable {

    @EmbeddedId
    private PKDosificacionHistorico id;

    @Column(name = "FFALFA", columnDefinition = "CHAR(10)")
    private String codAlfanumerico;

    @Column(name = "FFNFAC", columnDefinition = "DECIMAL(9)")
    private String numFactura;

    @Column(name = "FFFEIN", columnDefinition = "DECIMAL(8)")
    private BigDecimal fechaInicio;

    @Column(name = "FFNFAD", columnDefinition = "DECIMAL(9)")
    private String numeroFacturaDesde;

    @Column(name = "FFNFAH", columnDefinition = "DECIMAL(9)")
    private String numeroFacturaHasta;

    @Column(name = "FFNCON", columnDefinition = "DECIMAL(9)")
    private String numeroControl;

    @Column(name = "FFFLIE", columnDefinition = "DECIMAL(8)")
    private BigDecimal fechaFin;//Limite de emision

    @Column(name = "FFLLAV", columnDefinition = "CHAR(256)")
    private String llave;

    @Column(name = "FFLEYEN", columnDefinition = "CHAR(400)")
    private String leyenda;

    @Column(name = "FFDOSIF", columnDefinition = "CHAR(1)")
    private String tipoProceso;

    @Column(name = "FVUSE", columnDefinition = "CHAR(10)")
    private String usuario;

    @Column(name = "FVJOBNAME", columnDefinition = "CHAR(6)")
    private String terminal;

    @Column(name = "FFCACT", columnDefinition = "CHAR(1)")
    private String estado;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FVFECAUD", nullable = false)
    private Date fechaRegistro;

    @Column(name = "FVIPADD", columnDefinition = "CHAR(15)")
    private String ip;

    @Transient
    private EstadoDosificacion estadoDosificacion;

    @Transient
    private FormaDosificacion formaDosificacion;

    @Transient
    private TipoDosificacion tipoDosificacion;

    @Transient
    private Date fechaDesde;

    @Transient
    private Date fechaHasta;

    public DosificacionHistorico() {
    }

    public DosificacionHistorico(PKDosificacionHistorico id, String numFactura, BigDecimal fechaInicio, String numeroFacturaDesde, String numeroFacturaHasta, String numeroControl, BigDecimal fechaFin, String llave, String leyenda, String tipoProceso) {
        this.id = id;
        this.codAlfanumerico = " ";
        this.numFactura = numFactura;
        this.fechaInicio = fechaInicio;
        this.numeroFacturaDesde = numeroFacturaDesde;
        this.numeroFacturaHasta = numeroFacturaHasta;
        this.numeroControl = numeroControl;
        this.fechaFin = fechaFin;
        this.llave = llave;
        this.leyenda = leyenda;
        this.tipoProceso = tipoProceso;
    }

    public PKDosificacionHistorico getId() {
        return id;
    }

    public void setId(PKDosificacionHistorico id) {
        this.id = id;
    }

    public String getCodAlfanumerico() {
        return codAlfanumerico;
    }

    public void setCodAlfanumerico(String codAlfanumerico) {
        this.codAlfanumerico = codAlfanumerico;
    }

    public String getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(String numFactura) {
        this.numFactura = numFactura;
    }

    public BigDecimal getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(BigDecimal fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getNumeroFacturaDesde() {
        return numeroFacturaDesde;
    }

    public void setNumeroFacturaDesde(String numeroFacturaDesde) {
        this.numeroFacturaDesde = numeroFacturaDesde;
    }

    public String getNumeroFacturaHasta() {
        return numeroFacturaHasta;
    }

    public void setNumeroFacturaHasta(String numeroFacturaHasta) {
        this.numeroFacturaHasta = numeroFacturaHasta;
    }

    public String getNumeroControl() {
        return numeroControl;
    }

    public void setNumeroControl(String contador) {
        this.numeroControl = contador;
    }

    public BigDecimal getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(BigDecimal fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }


    public String getLeyenda() {
        return leyenda;
    }

    public void setLeyenda(String leyenda) {
        this.leyenda = leyenda;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getFechaHastaFin() {
        return FormatosUtils.dateYYYYMMDD(getFechaFin());
    }

    public Date getFechaDesdeInicio() {
        return FormatosUtils.dateYYYYMMDD(getFechaInicio());
    }

    public String getEstadoTexto() {
        EstadoDosificacion estadoDosificacion = getEstadoDosificacion();
        if (estadoDosificacion != null) {
            return estadoDosificacion.name();
        }
        return "";
    }

    public EstadoDosificacion getEstadoDosificacion() {
        if (estadoDosificacion != null) {
            return estadoDosificacion;
        } else {
            if (getId() != null && getEstado() != null) {
                return EstadoDosificacion.get(getEstado());
            }
        }
        return null;
    }

    public void setEstadoDosificacion(EstadoDosificacion estadoDosificacion) {
        this.estadoDosificacion = estadoDosificacion;
    }

    public String getTipoProceso() {
        return tipoProceso;
    }

    public void setTipoProceso(String tipoProceso) {
        this.tipoProceso = tipoProceso;
    }

    public String getTipo() {
        TipoDosificacion tipoDosificacion = getTipoDosificacion();
        if (tipoDosificacion != null) {
            return tipoDosificacion.name();
        }
        return "";
    }

    public TipoDosificacion getTipoDosificacion() {
        if (tipoDosificacion != null) {
            return tipoDosificacion;
        } else {
            if (getId() != null) {
                return TipoDosificacion.get(getId().getTipo());
            }
        }
        return null;
    }

    public void setTipoDosificacion(TipoDosificacion tipoDosificacion) {
        this.tipoDosificacion = tipoDosificacion;
    }

    public String getForma() {
        FormaDosificacion formaDosificacion = getFormaDosificacion();
        if (formaDosificacion != null) {
            return formaDosificacion.name();
        }
        return "";
    }

    public FormaDosificacion getFormaDosificacion() {
        if (formaDosificacion != null) {
            return formaDosificacion;
        } else {
            if (getId() != null) {
                return FormaDosificacion.get(StringUtils.trimToNull(getTipoProceso()));
            }
        }
        return null;
    }

    public void setFormaDosificacion(FormaDosificacion formaDosificacion) {
        this.formaDosificacion = formaDosificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Dosificacion{" +
                "id=" + id +
                ", codAlfanumerico='" + codAlfanumerico + '\'' +
                ", numFactura='" + numFactura + '\'' +
                ", fechaInicio=" + fechaInicio +
                ", numeroFacturaDesde='" + numeroFacturaDesde + '\'' +
                ", numeroFacturaHasta='" + numeroFacturaHasta + '\'' +
                ", numeroControl='" + numeroControl + '\'' +
                ", fechaFin=" + fechaFin +
                ", llave='" + llave + '\'' +
                ", leyenda='" + leyenda + '\'' +
                ", tipoProceso='" + tipoProceso + '\'' +
                ", estadoDosificacion=" + estadoDosificacion +
                ", formaDosificacion=" + formaDosificacion +
                ", tipoDosificacion=" + tipoDosificacion +
                ", fechaDesde=" + fechaDesde +
                ", fechaHasta=" + fechaHasta +
                '}';
    }
}
