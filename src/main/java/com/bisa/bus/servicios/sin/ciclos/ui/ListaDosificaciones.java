package com.bisa.bus.servicios.sin.ciclos.ui;

import bus.database.components.Listado;
import bus.database.components.ListadoDataProvider;
import bus.database.components.ListadoPanel;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.AjaxCheckBoxColumn;
import bus.plumbing.components.IndicatingAjaxToolButton;
import bus.plumbing.components.LinkPropertyColumn;
import bus.plumbing.components.ToolButton;
import com.bisa.bus.servicios.sin.ciclos.api.FacturacionCiclosProcess;
import com.bisa.bus.servicios.sin.ciclos.dao.DosificacionAutomaticaDao;
import com.bisa.bus.servicios.sin.ciclos.dao.DosificacionDao;
import com.bisa.bus.servicios.sin.ciclos.entities.Dosificacion;
import com.bisa.bus.servicios.sin.ciclos.entities.DosificacionAutomatica;
import com.bisa.bus.servicios.sin.ciclos.entities.PKDosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.EstadoDosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.FormaDosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.ResultadoDosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.TipoCodigoError;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static bus.users.api.Metadatas.USER_META_DATA_KEY;

/**
 * @author by rsalvatierra on 06/01/2017.
 */
@AuthorizeInstantiation({RolesBisa.SIN_DOSIFICACION})
@Menu(value = "Dosificaciones", subMenu = SubMenu.SIN)
public class ListaDosificaciones extends Listado<Dosificacion, PKDosificacion> {

    private final IModel<HashSet<Dosificacion>> seleccionados;
    @Inject
    FacturacionCiclosProcess facturacionCiclosProcess;

    public ListaDosificaciones() {
        super();
        this.seleccionados = new Model<>(new HashSet<>());
    }

    @Override
    protected ListadoPanel<Dosificacion, PKDosificacion> newListadoPanel(String id) {
        return new ListadoPanel<Dosificacion, PKDosificacion>(id) {
            @Override
            protected List<IColumn<Dosificacion, String>> newColumns(ListadoDataProvider<Dosificacion, PKDosificacion> dataProvider) {
                ArrayList<IColumn<Dosificacion, String>> iColumns = new ArrayList<>();
                iColumns.add(new AjaxCheckBoxColumn<>(seleccionados, dataProvider));
                iColumns.add(new LinkPropertyColumn<Dosificacion>(Model.of("N\u00FAmero Autorizaci\u00F3n"), "id.numeroAutorizacion") {
                    @Override
                    protected void onClick(Dosificacion object) {
                        setResponsePage(EditarDosificacion.class, new PageParameters()
                                .add("codBanco", object.getId().getCodBanco())
                                .add("codSucursal", object.getId().getCodSucursal())
                                .add("codSector", object.getId().getCodSector())
                                .add("tipo", object.getId().getTipo())
                                .add("numeroAutorizacion", object.getId().getNumeroAutorizacion()));
                    }
                });
                iColumns.add(new PropertyColumn<>(Model.of("Tipo"), "tipo"));
                iColumns.add(new PropertyColumn<>(Model.of("Fecha Ingreso"), "fechaDesdeInicio"));
                iColumns.add(new PropertyColumn<>(Model.of("Fecha L\u00EDmite Emisi\u00F3n"), "fechaHastaFin"));
                iColumns.add(new PropertyColumn<Dosificacion, String>(Model.of("Factura Actual"), "numFactura") {
                    @Override
                    public void populateItem(Item<ICellPopulator<Dosificacion>> item, String componentId, IModel<Dosificacion> rowModel) {
                        Dosificacion dosificacion = rowModel.getObject();
                        item.add(new Label(componentId, dosificacion.getNumFactura()).add(new AttributeAppender("style", Model.of("text-align:right"))));
                    }
                });
                iColumns.add(new PropertyColumn<Dosificacion, String>(Model.of("N\u00FAmero Control"), "numeroControl") {
                    @Override
                    public void populateItem(Item<ICellPopulator<Dosificacion>> item, String componentId, IModel<Dosificacion> rowModel) {
                        Dosificacion dosificacion = rowModel.getObject();
                        item.add(new Label(componentId, dosificacion.getNumeroControl()).add(new AttributeAppender("style", Model.of("text-align:right"))));
                    }
                });
                iColumns.add(new PropertyColumn<>(Model.of("Modo"), "forma"));
                iColumns.add(new PropertyColumn<>(Model.of("Usuario Ult. Act."), "usuario"));
                iColumns.add(new PropertyColumn<>(Model.of("Fecha Ult. Act."), "fechaConsumo"));
                iColumns.add(new PropertyColumn<Dosificacion, String>(Model.of("Estado"), "estadoTexto") {
                    @Override
                    public void populateItem(Item<ICellPopulator<Dosificacion>> item, String componentId, final IModel<Dosificacion> rowModel) {
                        item.add(new Label(componentId, getDataModel(rowModel)).
                                add(new AttributeModifier("class", new AbstractReadOnlyModel<String>() {
                                    @Override
                                    public String getObject() {
                                        String est = rowModel.getObject().getEstado();
                                        EstadoDosificacion estado = EstadoDosificacion.get(est);

                                        if (EstadoDosificacion.ACTIVO.equals(estado)) {
                                            return "btn btn-small disabled btn-success";
                                        } else if (EstadoDosificacion.PENDIENTE.equals(estado)) {
                                            return "btn btn-small disabled btn-info";
                                        } else if (EstadoDosificacion.INACTIVO.equals(estado)) {
                                            return "btn btn-small disabled btn-danger";
                                        }
                                        return "mutted";
                                    }
                                })));
                    }
                });
                return iColumns;
            }

            @Override
            protected void poblarBotones(RepeatingView rv, String idButton, Form<Void> filtroForm) {
                WebMarkupContainer wmk;
                rv.add(wmk = new WebMarkupContainer(rv.newChildId()));
                wmk.add(new ToolButton(idButton) {
                    @Override
                    public void onSubmit() {
                        setResponsePage(NuevaDosificacion.class);
                    }
                }.setClassAttribute("btn btn-info")
                        .setIconAttribute("icon-file icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Crear dosificaci\u00F3n")));
                rv.add(wmk = new WebMarkupContainer(rv.newChildId()));
                wmk.add(new ToolButton(idButton) {
                    @Override
                    public void onSubmit() {
                        setResponsePage(SolicitarDosificacion.class);
                    }
                }.setClassAttribute("btn btn-success")
                        .setIconAttribute("icon-file icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Solicitar dosificaci\u00F3n SIN")))
                        .setVisible(false);

                rv.add(wmk = new WebMarkupContainer(rv.newChildId()));
                wmk.add(new IndicatingAjaxToolButton(idButton, filtroForm) {

                    @Override
                    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                        target.add(feedbackPanel);
                        target.add(table);
                        Set<Dosificacion> dosificacionSet = seleccionados.getObject();
                        if (dosificacionSet == null || dosificacionSet.isEmpty()) {
                            getSession().warn("Nada por hacer");
                            return;
                        }
                        if (dosificacionSet.size() != 1) {
                            getSession().warn("Solo se puede realizar el cierre de un registro a la vez");
                            return;
                        }
                        int errados = 0;
                        String mensaje = "";
                        String usuario = getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase();
                        for (Dosificacion dosificacion : dosificacionSet) {
                            try {
                                if (!FormaDosificacion.CICLOS.getCodigo().equals(StringUtils.trimToEmpty(dosificacion.getTipoProceso()))) {
                                    mensaje = "La dosificaci\u00f3n debe tener la modalidad por CICLOS para proceder con el cierre.";
                                    errados++;
                                    break;
                                }

                                if (!EstadoDosificacion.ACTIVO.getDescripcion().equals(StringUtils.trimToEmpty(dosificacion.getEstado()))) {
                                    mensaje = "La dosificaci\u00f3n debe estar ACTIVA para proceder con el cierre.";
                                    errados++;
                                    break;
                                }

                                DosificacionAutomatica dosificacionAutomatica = getInstance(DosificacionAutomaticaDao.class).getDosificacion(Long.parseLong(dosificacion.getId().getNumeroAutorizacion()));
                                if (dosificacionAutomatica == null) {
                                    mensaje = "Error al procesar el cierre";
                                    errados++;
                                    break;
                                }
                                ResultadoDosificacion resultadoDosificacion = facturacionCiclosProcess.cierreCiclo(dosificacionAutomatica, usuario);
                                if (!TipoCodigoError.OK.equals(resultadoDosificacion.getCodError())) {
                                    mensaje = resultadoDosificacion.getMensaje();
                                    errados++;
                                }
                            } catch (Exception e) {
                                LOGGER.error("Ha ocurrido un error al procesar el cierre " + dosificacion.getId().getNumeroAutorizacion(), e);
                                mensaje = "Error al procesar el cierre";
                                errados++;
                            }
                        }
                        if (errados > 0) {
                            getSession().error(mensaje + ". Consulte a soporte t\u00e9cnico.");
                        } else {
                            getSession().info("Operaci\u00f3n realizada correctamente");
                        }
                        seleccionados.setObject(new HashSet<>());
                    }

                    @Override
                    protected void onError(AjaxRequestTarget target, Form<?> form) {
                        target.add(feedbackPanel);
                    }
                }.setClassAttribute("btn btn-danger")
                        .setIconAttribute("icon-remove icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Cierre Ciclo")))
                        .setVisible(false);
            }

            @Override
            protected Class<? extends Dao<Dosificacion, PKDosificacion>> getProviderClazz() {
                return DosificacionDao.class;
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("fechaInicio", false);
            }
        };
    }
}
