package com.bisa.bus.servicios.sin.ciclos.model;

import bus.plumbing.utils.FormatosUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author rsalvatierra on 11/04/2016.
 */
public class Historico implements Serializable {

    private BigDecimal numeroAutorizacion;
    private BigDecimal numeroFactura;
    private String estado;
    private BigDecimal fechaIngreso;
    private BigDecimal fechaLimiteEmision;
    private String llave;
    private String leyenda;
    private String codUsuario;
    private Date fecha;

    public Historico() {
    }

    public BigDecimal getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(BigDecimal numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public BigDecimal getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(BigDecimal numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public BigDecimal getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(BigDecimal fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public BigDecimal getFechaLimiteEmision() {
        return fechaLimiteEmision;
    }

    public void setFechaLimiteEmision(BigDecimal fechaLimiteEmision) {
        this.fechaLimiteEmision = fechaLimiteEmision;
    }

    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }

    public String getLeyenda() {
        return leyenda;
    }

    public void setLeyenda(String leyenda) {
        this.leyenda = leyenda;
    }

    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaHastaFin() {
        return FormatosUtils.dateYYYYMMDD(getFechaLimiteEmision());
    }

    public Date getFechaDesdeInicio() {
        return FormatosUtils.dateYYYYMMDD(getFechaIngreso());
    }

    public String getEstadoDesc() {
        EstadoDosificacion estadoDosificacion = getEstadoDosificacion();
        if (estadoDosificacion != null) {
            return estadoDosificacion.name();
        }
        return "";
    }

    public EstadoDosificacion getEstadoDosificacion() {
        if (getEstado() != null) {
            return EstadoDosificacion.get(getEstado());
        }
        return null;
    }
}
