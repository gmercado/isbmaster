package com.bisa.bus.servicios.sin.ciclos.rest;

import com.bisa.bus.servicios.sin.ciclos.api.FacturacionCiclosProcess;
import com.bisa.bus.servicios.sin.ciclos.model.FacturasResult;
import com.bisa.bus.servicios.sin.ciclos.model.LogFacturasResult;
import com.bisa.bus.servicios.sin.ciclos.model.RequestQuery;
import com.bisa.bus.servicios.sin.ciclos.model.RequestQueryLog;
import com.bisa.isb.ws.catalog.Remark;
import com.google.common.base.Splitter;
import com.google.inject.Inject;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Iterator;

/**
 * @author by rsalvatierra on 28/12/2016.
 */
@Path("/sin/ciclos")
@Produces({"application/json;charset=UTF-8"})
@Remark(value = "Servicio para consulta de facturas desde el portal")
public class ConsultaFacturasService {

    public static final String APPLICATION_JSON = "application/json";
    public static final String AUTHORIZATION = "Authorization";
    public static final String ERROR_CATEGORY_NOT_FOUND_ERROR = "<error>Category Not Found</error>";
    public static final String USER_NAME = "USRNAME";
    public static final String BASIC = "BASIC";
    public static final String SEPARADOR_BASIC = ":";

    @Inject
    FacturacionCiclosProcess facturacionCiclosProcess;

    protected Response getBuild(Response.Status status) {
        Response.ResponseBuilder builder = Response.status(status);
        builder.type(APPLICATION_JSON);
        builder.entity(ERROR_CATEGORY_NOT_FOUND_ERROR);
        return builder.build();
    }

    protected String getUser(HttpHeaders httpHeaders) {
        String authorization = httpHeaders.getHeaderString(AUTHORIZATION);
        HashMap<String, String> credenciales = getUsernameHeaders(authorization);
        return credenciales.get(USER_NAME);
    }

    public HashMap<String, String> getUsernameHeaders(String keyHeader) {
        HashMap<String, String> authorizationMap = new HashMap<>();
        if (keyHeader != null && keyHeader.toUpperCase().startsWith(BASIC)) {
            String userpassEncoded = keyHeader.substring(6);
            String userpassDecoded = new String(org.apache.commons.codec.binary.Base64.decodeBase64(userpassEncoded));
            Iterator<String> splitted = Splitter.on(SEPARADOR_BASIC).split(userpassDecoded).iterator();
            final String name = splitted.next();
            authorizationMap.put(USER_NAME, name);
        }
        return authorizationMap;
    }

    /**
     * El Servicio permite obtener las facturas emitidas del Cliente
     *
     * @param httpHeaders headers
     * @param clientBasic datos basicos
     * @return FacturasResult
     */
    @POST
    @Path("/consultaFacturas")
    @Remark(value = "Metodo para consultar facturas")
    public FacturasResult getfacturas(@Context HttpHeaders httpHeaders, RequestQuery clientBasic) {
        if (clientBasic == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return facturacionCiclosProcess.obtenerFacturas(clientBasic, getUser(httpHeaders));
        }
    }

    /**
     * El Servicio permite registrar el log de eventos de las facturas emitidas del Cliente
     *
     * @param httpHeaders headers
     * @param clientBasic datos basicos
     * @return LogFacturasResult
     */
    @PUT
    @Path("/registroLogFacturas")
    @Remark(value = "Metodo para registrar log de facturas")
    public LogFacturasResult setLogfacturas(@Context HttpHeaders httpHeaders, RequestQueryLog clientBasic) {
        if (clientBasic == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return facturacionCiclosProcess.registrarLogFacturas(clientBasic, getUser(httpHeaders));
        }
    }
}
