package com.bisa.bus.servicios.sin.ciclos.model;

/**
 * @author by rsalvatierra on 28/12/2016.
 */
public enum TipoTransaccion {
    VME("Venta Moneda Extranjera"),
    CME("Compra Moneda Extranjera");

    private String descripcion;

    TipoTransaccion(String descripcion) {
        this.descripcion = descripcion;
    }

    public static TipoTransaccion get(String codigo) {
        for (TipoTransaccion valor : TipoTransaccion.values()) {
            if (codigo.equals(valor.name())) {
                return valor;
            }
        }
        return null;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
