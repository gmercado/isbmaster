package com.bisa.bus.servicios.sin.ciclos.consumer.api;

import bus.consumoweb.consumer.ClienteServiciosWeb;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import com.bisa.bus.servicios.sin.ciclos.consumer.stub.org.tempuri.ISinFacCiclosService;
import com.google.inject.Inject;
import org.apache.cxf.transports.http.configuration.ConnectionType;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

import java.net.URL;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.*;

/**
 * @author by rsalvatierra on 13/02/2017.
 */
public class ConsumidorServiciosSinCiclos extends ClienteServiciosWeb {

    private static final String SIN_CICLOS = "sin.ciclos";
    private static final String WSDL = "/com/bisa/bus/servicios/sin/ciclos/consumer/api/wsdl/FacturacionCiclos.wsdl";

    @Inject
    public ConsumidorServiciosSinCiclos(@SQLFueraDeLinea ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente) {
        super(executor, consumoWebDao, medioAmbiente);
    }

    public String getUsuario() {
        return "";//parametros.getValorDe(SEGIP_USUARIO, SEGIP_USUARIO_DEFAULT);
    }

    public String getClave() {
        return "";//cryptUtils.dec(parametros.getValorDe(SEGIP_CLAVE, SEGIP_CLAVE_DEFAULT));
    }

    @Override
    protected URL newURL() {
        return ConsumidorServiciosSinCiclos.class.getResource(WSDL);
    }

    @Override
    protected Class getResult() {
        return ConsumidorServiciosSinCiclos.class;
    }

    @Override
    protected String getPort() {
        return medioAmbiente.getValorDe(SIN_PORT_NAME, SIN_PORT_NAME_DEFAULT);
    }

    @Override
    protected String getEndPoint() {
        return medioAmbiente.getValorDe(SIN_URL, SIN_URL_DEFAULT);
    }

    @Override
    protected String getOperationName() {
        return "";//medioAmbiente.getValorDe(SEGIP_OPERATION_NAME, SEGIP_OPERATION_NAME_DEFAULT);
    }

    @Override
    protected String getServiceName() {
        return "";//medioAmbiente.getValorDe(SEGIP_SERVICE_NAME, SEGIP_SERVICE_NAME_DEFAULT);
    }

    @Override
    protected void setProxy() {
        String host = medioAmbiente.getValorDe(PROXY_HOST, PROXY_HOST_DEFAULT);
        int port = medioAmbiente.getValorIntDe(PROXY_PORT, PROXY_PORT_DEFAULT);
        if (host != null && port > 0) {
            System.setProperty(PROXY_HOST, host);
            System.setProperty(PROXY_PORT, String.valueOf(port));
        }
        System.setProperty("http.proxyHost", "navegar.grupobisa.net");
        System.setProperty("http.proxyPort", String.valueOf("8080"));
        System.setProperty("http.nonProxyHosts", "localhost");

    }

    @Override
    protected HTTPClientPolicy getHttpClientPolicy() {
        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
        httpClientPolicy.setConnectionTimeout(600000);
        httpClientPolicy.setAllowChunking(false);
        httpClientPolicy.setConnection(ConnectionType.KEEP_ALIVE);
        return httpClientPolicy;
    }

    @Override
    protected String getId() {
        return SIN_CICLOS;
    }

    @Override
    protected String getNameSpace() {
        return "";//medioAmbiente.getValorDe(SEGIP_NAMESPACE, SEGIP_NAMESPACE_DEFAULT);
    }

    @Override
    public Class getService() {
        return ISinFacCiclosService.class;
    }

}
