package com.bisa.bus.servicios.sin.ciclos.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.sin.ciclos.entities.Factura;
import com.bisa.bus.servicios.sin.ciclos.model.EstadoFactura;
import com.bisa.bus.servicios.sin.ciclos.model.TipoPersona;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 * @author by rsalvatierra on 23/12/2016.
 */
public class FacturaDao extends DaoImpl<Factura, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FacturaDao.class);

    @Inject
    protected FacturaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    /**
     * Obtener Datos Facturas
     *
     * @param pNumeroDocumento Numero Documento
     * @return List<Factura> facturas cliente
     */
    public List<Factura> getFacturas(final BigDecimal pNumeroDocumento, final TipoPersona pTipoPersona) {
        LOGGER.debug("Obteniendo facturas cliente :{}.", pNumeroDocumento);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

                        CriteriaQuery<Factura> q = cb.createQuery(Factura.class);
                        Root<Factura> p = q.from(Factura.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("numeroDocumento"), pNumeroDocumento));
                        if (TipoPersona.JURIDICO.equals(pTipoPersona)) {
                            predicatesAnd.add(cb.equal(p.get("tipoDocumento"), "NIT"));
                        } else {
                            predicatesAnd.add(cb.notEqual(p.get("tipoDocumento"), "NIT"));
                        }
                        /*predicatesAnd.add(cb.equal(p.get("numCliente"), ""));
                        if (TipoPersona.NATURAL.equals(pTipoPersona)) {
                            predicatesAnd.add(cb.equal(p.get("tipoDocumento"), "CI"));
                        } else {
                            predicatesAnd.add(cb.equal(p.get("tipoDocumento"), "NIT"));
                        }*/
                        predicatesAnd.add(cb.equal(p.get("estadoFactura"), EstadoFactura.ACTIVO.getDescripcion()));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        q.orderBy(cb.asc(p.get("fecha")), cb.asc(p.get("id").get("numeroFactura")));
                        TypedQuery<Factura> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    /**
     * Obtener Datos Facturas
     *
     * @param pNumeroCliente Numero Cliente
     * @return List<Factura> facturas cliente
     */
    public List<Factura> getFacturasByNumCliente(final String pNumeroCliente/*, final BigDecimal pNumeroDocumento*/) {
        LOGGER.debug("Obteniendo facturas cliente :{}.", pNumeroCliente);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

                        CriteriaQuery<Factura> q = cb.createQuery(Factura.class);
                        Root<Factura> p = q.from(Factura.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("numCliente"), pNumeroCliente));
                        //predicatesAnd.add(cb.equal(p.get("numeroDocumento"), pNumeroDocumento));
                        predicatesAnd.add(cb.equal(p.get("estadoFactura"), EstadoFactura.ACTIVO.getDescripcion()));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        q.orderBy(cb.asc(p.get("fecha")), cb.asc(p.get("id").get("numeroFactura")));
                        TypedQuery<Factura> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public List<Factura> getFacturasProcesadasByNumAut(final String numeroAutorizacion) {
        LOGGER.debug("Obteniendo facturas procesadas para el numero de autorizacion : {}.",
                numeroAutorizacion);
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<Factura> q = cb.createQuery(Factura.class);
                    Root<Factura> p = q.from(Factura.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("numeroAutorizacion"), numeroAutorizacion));
                    predicatesAnd.add(cb.equal(p.get("estadoFactura"), EstadoFactura.ACTIVO.getDescripcion()));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    q.orderBy(cb.asc(p.get("fecha")), cb.asc(p.get("id").get("numeroFactura")));
                    TypedQuery<Factura> query = entityManager.createQuery(q);
                    return query.getResultList();
                }
        );
    }
}
