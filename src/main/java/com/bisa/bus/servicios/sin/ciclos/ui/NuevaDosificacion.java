package com.bisa.bus.servicios.sin.ciclos.ui;

import bus.env.api.MedioAmbiente;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.utils.FechaOperator;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.GuiceLoader;
import com.bisa.bus.servicios.sin.ciclos.dao.DosificacionDao;
import com.bisa.bus.servicios.sin.ciclos.entities.Dosificacion;
import com.bisa.bus.servicios.sin.ciclos.model.*;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.model.*;
import org.apache.wicket.validation.validator.PatternValidator;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static bus.env.api.Variables.*;
import static bus.users.api.Metadatas.USER_META_DATA_KEY;

/**
 * @author by rsalvatierra on 06/01/2017.
 */
@Titulo("Nueva Dosificaci\u00F3n")
@AuthorizeInstantiation({RolesBisa.SIN_DOSIFICACION})
public class NuevaDosificacion extends BisaWebPage {

    private final IModel<Dosificacion> variableIModel;

    public NuevaDosificacion() {
        super();
        final String datePattern = "dd/MM/yyyy";
        variableIModel = new CompoundPropertyModel<>(Model.of(new Dosificacion()));
        Form<Dosificacion> crear;
        add(crear = new Form<>("nueva", variableIModel));
        MedioAmbiente medioAmbiente = GuiceLoader.get(MedioAmbiente.class);

        variableIModel.getObject().setNumeroFacturaDesde(medioAmbiente.getValorDe(BISA_SIN_FACTURA_DESDE, BISA_SIN_FACTURA_DESDE_DEFAULT));
        variableIModel.getObject().setNumeroFacturaHasta(medioAmbiente.getValorDe(BISA_SIN_FACTURA_HASTA, BISA_SIN_FACTURA_HASTA_DEFAULT));
        variableIModel.getObject().setNumFactura(medioAmbiente.getValorDe(BISA_SIN_FACTURA_ACTUAL, BISA_SIN_FACTURA_ACTUAL_DEFAULT));

        crear.add(new DropDownChoice<>("id.tipo", new PropertyModel<>(variableIModel, "tipoDosificacion"),
                new LoadableDetachableModel<List<? extends TipoDosificacion>>() {
                    @Override
                    protected List<? extends TipoDosificacion> load() {
                        return new ArrayList<>(Collections.singletonList((TipoDosificacion.CVME)));
                    }
                }, new IChoiceRenderer<TipoDosificacion>() {
            @Override
            public Object getDisplayValue(TipoDosificacion object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(TipoDosificacion object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoDosificacion getObject(String id, IModel<? extends List<? extends TipoDosificacion>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }));

        crear.add(new DropDownChoice<>("tipoProceso", new PropertyModel<>(variableIModel, "formaDosificacion"),
                new LoadableDetachableModel<List<? extends FormaDosificacion>>() {
                    @Override
                    protected List<? extends FormaDosificacion> load() {
                        return new ArrayList<>(Collections.singletonList((FormaDosificacion.COMPUTARIZADA)));
                    }
                }, new IChoiceRenderer<FormaDosificacion>() {
            @Override
            public Object getDisplayValue(FormaDosificacion object) {
                return object.toString();
            }

            @Override
            public String getIdValue(FormaDosificacion object, int index) {
                return Integer.toString(index);
            }

            @Override
            public FormaDosificacion getObject(String id, IModel<? extends List<? extends FormaDosificacion>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }));

        TextField<String> alfa;
        crear.add(alfa = new TextField<>("codAlfanumerico"));
        alfa.setOutputMarkupId(true);

        TextField<String> factura;
        crear.add(factura = new TextField<>("numFactura"));
        factura.setOutputMarkupId(true);

        DateTextField fechaDesde = new DateTextField("fechaDesde", new PropertyModel<>(variableIModel, "fechaDesde"), datePattern);
        fechaDesde.add(new DatePicker());
        crear.add(fechaDesde);

        TextField<String> facturaDesde;
        crear.add(facturaDesde = new TextField<>("numeroFacturaDesde"));
        facturaDesde.setOutputMarkupId(true);

        TextField<String> facturaHasta;
        crear.add(facturaHasta = new TextField<>("numeroFacturaHasta"));
        facturaHasta.setOutputMarkupId(true);

        TextField<String> numeroAutorizacion;
        crear.add(numeroAutorizacion = new TextField<>("id.numeroAutorizacion"));
        numeroAutorizacion.add(new PatternValidator("[0-9]*"));
        numeroAutorizacion.setOutputMarkupId(true);

        TextField<String> numeroAutorizacionDD;
        crear.add(numeroAutorizacionDD = new TextField<>("ddNumeroAutorizacion"));
        numeroAutorizacionDD.add(new PatternValidator("[0-9]*"));
        numeroAutorizacionDD.setOutputMarkupId(true);

        TextField<String> nombre;
        crear.add(nombre = new TextField<>("numeroControl"));
        nombre.add(new PatternValidator("[0-9]*"));
        nombre.setOutputMarkupId(true);

        DateTextField fechaFin = new DateTextField("fechaHasta", new PropertyModel<>(variableIModel, "fechaHasta"), datePattern);
        fechaFin.add(new DatePicker());
        crear.add(fechaFin);

        DateTextField fechaFinDD = new DateTextField("ddFechaLimiteEmision", new PropertyModel<>(variableIModel, "ddFechaLimiteEmision"), datePattern);
        fechaFinDD.add(new DatePicker());
        crear.add(fechaFinDD);

        TextArea<String> llave;
        crear.add(llave = new TextArea<>("llave"));
        llave.setOutputMarkupId(true);

        TextArea<String> llaveDD;
        crear.add(llaveDD = new TextArea<>("ddLlaveSin"));
        llaveDD.setOutputMarkupId(true);

        TextArea<String> valor;
        crear.add(valor = new TextArea<>("leyenda"));
        valor.setOutputMarkupId(true);

        crear.add(new DropDownChoice<>("id.estado", new PropertyModel<>(variableIModel, "estadoDosificacion"),
                new LoadableDetachableModel<List<? extends EstadoDosificacion>>() {
                    @Override
                    protected List<? extends EstadoDosificacion> load() {
                        return new ArrayList<>(Collections.singletonList((EstadoDosificacion.PENDIENTE)));
                    }
                }, new IChoiceRenderer<EstadoDosificacion>() {
            @Override
            public Object getDisplayValue(EstadoDosificacion object) {
                return object.toString();
            }

            @Override
            public String getIdValue(EstadoDosificacion object, int index) {
                return Integer.toString(index);
            }

            @Override
            public EstadoDosificacion getObject(String id, IModel<? extends List<? extends EstadoDosificacion>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }));

        crear.add(new IndicatingAjaxButton("crear") {
            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                String[] cantidad_num_control = getInstance(MedioAmbiente.class).getValorDe(SIN_NUM_CONTROL_DIGITOS, SIN_NUM_CONTROL_DIGITOS_DEFAULT).split(":");
                String[] cantidad_num_autorizacion = getInstance(MedioAmbiente.class).getValorDe(SIN_NUM_AUTORIZACION_DIGITOS, SIN_NUM_AUTORIZACION_DIGITOS_DEFAULT).split(":");
                Dosificacion dosificacion = variableIModel.getObject();
                if (dosificacion.getTipoDosificacion() == null
                        || !TipoDosificacion.CVME.equals(dosificacion.getTipoDosificacion())) {
                    getSession().error("El Tipo es requerido.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getId() == null || dosificacion.getId().getNumeroAutorizacion() == null) {
                    getSession().error("El N\u00famero Autorizaci\u00f3n es requerido.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getId().getNumeroAutorizacion().length() < Integer.parseInt(cantidad_num_autorizacion[0]) || dosificacion.getId().getNumeroAutorizacion().length() > Integer.parseInt(cantidad_num_autorizacion[1])) {
                    getSession().error("El N\u00famero Autorizaci\u00f3n no es v\u00e1lido.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getDdNumeroAutorizacion() == null) {
                    getSession().error("El N\u00famero Autorizaci\u00f3n (Repetir) es requerido.");
                    target.add(feedbackPanel);
                    return;
                }
                if (!dosificacion.getId().getNumeroAutorizacion().equals(dosificacion.getDdNumeroAutorizacion())) {
                    getSession().error("El N\u00famero Autorizaci\u00f3n y su repetido deben ser iguales.");
                    target.add(feedbackPanel);
                    return;
                }

                if (dosificacion.getNumeroControl() == null) {
                    getSession().error("El N\u00famero Control es requerido.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getNumeroControl().length() < Integer.parseInt(cantidad_num_control[0]) || dosificacion.getNumeroControl().length() > Integer.parseInt(cantidad_num_control[1])) {
                    getSession().error("El N\u00famero Control no es v\u00e1lido.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getFechaDesde() == null) {
                    getSession().error("La Fecha Ingreso es requerida.");
                    target.add(feedbackPanel);
                    return;
                }
                Date fechaActual = FechaOperator.getZeroTimeDate(new Date());
                Date fechaDesde = dosificacion.getFechaDesde();
                if (fechaActual.after(fechaDesde)) {
                    getSession().error("La Fecha Ingreso no es v\u00e1lida.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getFechaHasta() == null) {
                    getSession().error("La Fecha L\u00edmite Emisi\u00f3n es requerida.");
                    target.add(feedbackPanel);
                    return;
                }
                Date fechaHasta = dosificacion.getFechaHasta();
                if (fechaActual.after(fechaHasta)) {
                    getSession().error("La Fecha L\u00edmite Emisi\u00f3n no es v\u00e1lida.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getDdFechaLimiteEmision() == null) {
                    getSession().error("La Fecha L\u00edmite Emisi\u00f3n (Repetir) es requerida.");
                    target.add(feedbackPanel);
                    return;
                }
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                if (!sdf.format(dosificacion.getFechaHasta()).equals(sdf.format(dosificacion.getDdFechaLimiteEmision()))) {
                    getSession().error("La Fecha L\u00edmite Emisi\u00f3n y su repetido deben ser iguales.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getFormaDosificacion() == null
                        || !FormaDosificacion.COMPUTARIZADA.equals(dosificacion.getFormaDosificacion())) {
                    getSession().error("La Forma es requerida.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getEstadoDosificacion() == null
                        || !EstadoDosificacion.PENDIENTE.equals(dosificacion.getEstadoDosificacion())) {
                    getSession().error("El Estado es requerido.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getLlave() == null) {
                    getSession().error("La Llave SIN es requerida.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getLlave().length() > 256) {
                    getSession().error("La Llave SIN no es v\u00e1lida.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getDdLlaveSin() == null) {
                    getSession().error("La Llave SIN (Repetir) es requerida.");
                    target.add(feedbackPanel);
                    return;
                }
                if (!dosificacion.getLlave().equals(dosificacion.getDdLlaveSin())) {
                    getSession().error("La Llave SIN y su repetido deben ser iguales.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getLeyenda() == null) {
                    getSession().error("La Leyenda es requerida.");
                    target.add(feedbackPanel);
                    return;
                }
                if (dosificacion.getLeyenda().length() > 400) {
                    getSession().error("La Leyenda no es v\u00e1lida.");
                    target.add(feedbackPanel);
                    return;
                }
                dosificacion.getId().setCodBanco(new BigDecimal(BancoDosificacion.BISA.getCodigo()));
                dosificacion.getId().setCodSucursal(new BigDecimal(SucursalDosificacion.NACIONAL.getCodigo()));
                dosificacion.getId().setCodSector(new BigDecimal(SectorDosificacion.TODOS.getCodigo()));
                dosificacion.getId().setTipo(dosificacion.getTipoDosificacion().getCodigo());
                dosificacion.setEstado(dosificacion.getEstadoDosificacion().getDescripcion());
                //Verificar que no exista repetido
                Dosificacion dos1 = getInstance(DosificacionDao.class).find(dosificacion.getId());
                if (dos1 != null) {
                    getSession().error("La dosificaci\u00f3n ya existe, verifique.");
                    target.add(feedbackPanel);
                    return;
                }
                int cantidad = getInstance(MedioAmbiente.class).getValorIntDe(SIN_DOSIFICACIONES_PENDIENTES, SIN_DOSIFICACIONES_PENDIENTES_DEFAULT);
                //Verificar que solo sea un pendiente
                List<Dosificacion> listaDosificaciones = getInstance(DosificacionDao.class).getDosificaciones(FormaDosificacion.COMPUTARIZADA, EstadoDosificacion.PENDIENTE);
                if (listaDosificaciones != null && listaDosificaciones.size() == cantidad) {
                    getSession().error("Existen otras dosificaciones manuales pendientes, verifique.");
                    target.add(feedbackPanel);
                    return;
                }
                try {
                    if (dosificacion.getFechaDesde() != null) {
                        dosificacion.setFechaInicio(new BigDecimal(FormatosUtils.fechaYYYYMMDD(dosificacion.getFechaDesde())));
                    } else {
                        dosificacion.setFechaInicio(BigDecimal.ZERO);
                    }
                    if (dosificacion.getFechaHasta() != null) {
                        dosificacion.setFechaFin(new BigDecimal(FormatosUtils.fechaYYYYMMDD(dosificacion.getFechaHasta())));
                    } else {
                        dosificacion.setFechaFin(BigDecimal.ZERO);
                    }
                    dosificacion.setFechaRegistro(new Date());
                    dosificacion.setTipoProceso(FormaDosificacion.COMPUTARIZADA.getCodigo());
                    dosificacion.setCodAlfanumerico(" ");
                    dosificacion.setUsuario(getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase());
                    dosificacion.setIp(getIPAcceso(getRequest()));
                    dosificacion.setTerminal(" ");
                    getInstance(DosificacionDao.class).persist(dosificacion);
                    info("Dosificaci\u00f3n creada");
                    setResponsePage(ListaDosificaciones.class);
                } catch (Exception e) {
                    LOGGER.error("Error inesperado al guardar dosificacion", e);
                    error(e.getMessage());
                    target.add(feedbackPanel);
                }
            }
        });

        crear.add(new Button("cancelar") {
            private static final long serialVersionUID = 62944255371806955L;

            @Override
            public void onSubmit() {
                setResponsePage(ListaDosificaciones.class);
                getSession().info("Operaci\u00f3n cancelada");
            }
        }.setDefaultFormProcessing(false));
    }
}
