package com.bisa.bus.servicios.sin.ciclos;

import bus.consumoweb.consumer.ClienteServiciosWeb;
import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.bus.servicios.sin.ciclos.api.CiclosProcess;
import com.bisa.bus.servicios.sin.ciclos.api.DosificacionHist;
import com.bisa.bus.servicios.sin.ciclos.api.FacturacionCiclosProcess;
import com.bisa.bus.servicios.sin.ciclos.consumer.api.ConsumidorServiciosSinCiclos;
import com.bisa.bus.servicios.sin.ciclos.consumer.api.ConsumidorSinCiclos;
import com.bisa.bus.servicios.sin.ciclos.dao.DosificacionHistDao;
import com.bisa.bus.servicios.sin.ciclos.sched.CargaDosificacion;

/**
 * @author by rsalvatierra on 19/12/2016.
 */
public class FacturacionCiclosModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(ClienteServiciosWeb.class).annotatedWith(ConsumidorSinCiclos.class).to(ConsumidorServiciosSinCiclos.class);
        bind(FacturacionCiclosProcess.class).to(CiclosProcess.class);
        bind(DosificacionHist.class).to(DosificacionHistDao.class);
        bindSched("CargaDosificacion", "SIN", "0 0 * * * ?", CargaDosificacion.class);
        bindUI("com/bisa/bus/servicios/sin/ciclos/ui");
    }
}
