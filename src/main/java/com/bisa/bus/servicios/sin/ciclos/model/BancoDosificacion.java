package com.bisa.bus.servicios.sin.ciclos.model;

/**
 * @author by rsalvatierra on 10/01/2017.
 */
public enum BancoDosificacion {
    BISA("1");

    private String codigo;

    BancoDosificacion(String codigo) {
        this.codigo = codigo;
    }

    public static BancoDosificacion get(String codigo) {
        for (BancoDosificacion valor : BancoDosificacion.values()) {
            if (valor.getCodigo().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }

    public String getCodigo() {
        return codigo;
    }
}
