package com.bisa.bus.servicios.sin.ciclos.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.sin.ciclos.entities.Sucursal;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * @author by rsalvatierra on 29/12/2016.
 */
public class SucursalDao extends DaoImpl<Sucursal, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SucursalDao.class);

    @Inject
    protected SucursalDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    /**
     * Obtener Sucursales
     *
     * @return List<Sucursal> sucursales
     */
    public HashMap<BigDecimal, Sucursal> getSucursales() {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        HashMap<BigDecimal, Sucursal> map = new HashMap<>();
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Sucursal> q = cb.createQuery(Sucursal.class);
                        Root<Sucursal> p = q.from(Sucursal.class);
                        TypedQuery<Sucursal> query = entityManager.createQuery(q);
                        List<Sucursal> sucursales = query.getResultList();
                        for (Sucursal sucursal : sucursales) {
                            map.put(new BigDecimal(sucursal.getCodAgencia()), sucursal);
                        }
                        return map;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }
}
