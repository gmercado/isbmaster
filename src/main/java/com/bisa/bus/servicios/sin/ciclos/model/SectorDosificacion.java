package com.bisa.bus.servicios.sin.ciclos.model;

/**
 * @author by rsalvatierra on 10/01/2017.
 */
public enum SectorDosificacion {
    TODOS("0");

    private String codigo;

    SectorDosificacion(String codigo) {
        this.codigo = codigo;
    }

    public static SectorDosificacion get(String codigo) {
        for (SectorDosificacion valor : SectorDosificacion.values()) {
            if (valor.getCodigo().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }

    public String getCodigo() {
        return codigo;
    }
}
