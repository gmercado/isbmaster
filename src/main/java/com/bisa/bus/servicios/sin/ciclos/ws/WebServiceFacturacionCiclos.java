package com.bisa.bus.servicios.sin.ciclos.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 * @author by rsalvatierra on 20/12/2016.
 */
@WebService(
        targetNamespace = WebServiceFacturacionCiclos.NAMESPACE,
        name = "WebServiceFacturacionCiclos")
interface WebServiceFacturacionCiclos {

    String NAMESPACE = "http://www.bisa.com/ebanking/services/soap";

    /**
     * *
     * Descripcion: Consulta Certificacion Datos PersonaSegip
     */
    @WebMethod(operationName = "test")
    @WebResult(name = "out", targetNamespace = NAMESPACE)
    String probarConexion(@WebParam(name = "in") String request);
}
