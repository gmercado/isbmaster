package com.bisa.bus.servicios.sin.ciclos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by rsalvatierra on 28/12/2016.
 */
public class FacturasResult implements Serializable {
    private List<CabeceraFactura> facturas;
    private Long identificador;
    private int codRespuesta;
    private String mensaje;

    public FacturasResult() {
    }

    public List<CabeceraFactura> getFacturas() {
        return facturas;
    }

    public void setFacturas(List<CabeceraFactura> facturas) {
        this.facturas = facturas;
    }

    public void add(CabeceraFactura cabeceraFactura) {
        if (facturas == null) {
            facturas = new ArrayList<>();
        }
        facturas.add(cabeceraFactura);
    }

    public int getCodRespuesta() {
        return codRespuesta;
    }

    public void setCodRespuesta(int codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Long getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Long identificador) {
        this.identificador = identificador;
    }

    @Override
    public String toString() {
        return "FacturasResult{" +
                "codRespuesta=" + codRespuesta +
                ", mensaje='" + mensaje + '\'' +
                '}';
    }
}
