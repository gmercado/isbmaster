package com.bisa.bus.servicios.sin.ciclos.ui;

import bus.database.components.FiltroPanel;
import com.bisa.bus.servicios.sin.ciclos.entities.BitacoraFactura;
import com.bisa.bus.servicios.sin.ciclos.model.TipoEvento;
import com.bisa.bus.servicios.sin.ciclos.model.TipoPersona;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author by rsalvatierra on 21/02/2017.
 */
class ConsumosFacturasPortalFiltro extends FiltroPanel<BitacoraFactura> {

    ConsumosFacturasPortalFiltro(String id, IModel<BitacoraFactura> model1, IModel<BitacoraFactura> model2) {
        super(id, model1, model2);
        final String datePattern = "dd/MM/yyyy";
        DateTextField fechaDesde = new DateTextField("fechaDesde", new PropertyModel<>(model1, "fecha"), datePattern);
        fechaDesde.setRequired(true);
        fechaDesde.add(new DatePicker());

        DateTextField fechaHasta = new DateTextField("fechaHasta", new PropertyModel<>(model2, "fecha"), datePattern);
        fechaHasta.setRequired(true);
        fechaHasta.add(new DatePicker());

        add(new TextField<>("numeroDocumento", new PropertyModel<>(model1, "numeroDocumento")));
        add(new TextField<>("numeroFactura", new PropertyModel<>(model1, "numeroFactura")));
        add(fechaDesde);
        add(fechaHasta);
        //Evento
        add(new DropDownChoice<>("evento", new PropertyModel<>(model1, "tipoEvento"),
                new LoadableDetachableModel<List<? extends TipoEvento>>() {
                    @Override
                    protected List<? extends TipoEvento> load() {
                        List<TipoEvento> lista = new ArrayList<>();
                        lista.add(TipoEvento.CONSULTAR);
                        lista.add(TipoEvento.DESCARGAR);
                        return lista;
                    }
                }, new IChoiceRenderer<TipoEvento>() {
            @Override
            public Object getDisplayValue(TipoEvento object) {
                return object.toString();
            }

            @Override
            public String getIdValue(TipoEvento object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoEvento getObject(String id, IModel<? extends List<? extends TipoEvento>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));

        add(new DropDownChoice<>("tipoPer", new PropertyModel<>(model1, "tipoPersonas"),
                new LoadableDetachableModel<List<? extends TipoPersona>>() {
                    @Override
                    protected List<? extends TipoPersona> load() {
                        return new ArrayList<>(Arrays.asList((TipoPersona.values())));
                    }
                }, new IChoiceRenderer<TipoPersona>() {
            @Override
            public Object getDisplayValue(TipoPersona object) {
                return object.toString();
            }

            @Override
            public String getIdValue(TipoPersona object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoPersona getObject(String id, IModel<? extends List<? extends TipoPersona>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));
    }
}
