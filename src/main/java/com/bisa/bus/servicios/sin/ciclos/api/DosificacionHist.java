package com.bisa.bus.servicios.sin.ciclos.api;

import com.bisa.bus.servicios.sin.ciclos.model.Historico;

import java.util.List;

/**
 * @author by rsalvatierra on 18/01/2017.
 */
public interface DosificacionHist {
    List<Historico> getHistorico2(String pNumeroAutorizacion);
}
