package com.bisa.bus.servicios.sin.ciclos.model;

/**
 * @author by rsalvatierra on 10/01/2017.
 */
public enum FormaDosificacion {
    CICLOS("A"),
    COMPUTARIZADA("M");

    private String codigo;

    FormaDosificacion(String codigo) {
        this.codigo = codigo;
    }

    public static FormaDosificacion get(String codigo) {
        for (FormaDosificacion valor : FormaDosificacion.values()) {
            if (valor.getCodigo().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }

    public String getCodigo() {
        return codigo;
    }
}
