package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestTransaccion implements Serializable, Request {

    private String transactionID;
    private String bulkTransactionID;

    public JsonRequestTransaccion() {
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getBulkTransactionID() {
        return bulkTransactionID;
    }

    public void setBulkTransactionID(String bulkTransactionID) {
        this.bulkTransactionID = bulkTransactionID;
    }

    @Override
    public String toString() {
        return "JsonRequestTransccion{" +
                "transactionID='" + transactionID + '\'' +
                "bulkTransactionID='" + bulkTransactionID + '\'' +
                '}';
    }
}
