package com.bisa.bus.servicios.mojix.api;

import bus.interfaces.as400.entities.Cliente;
import bus.interfaces.as400.entities.Cuenta;
import com.bisa.bus.servicios.mojix.entities.*;
import com.bisa.bus.servicios.mojix.model.Request;
import com.bisa.bus.servicios.mojix.model.Response;
import com.bisa.bus.servicios.mojix.services.types.*;
import com.bisa.bus.servicios.segip.entities.ClienteNatural;

import java.util.List;

/**
 * @author by rsalvatierra on 29/05/2017.
 */
public class ResponseOperacion {
    private boolean valido;
    private Cliente cliente;
    private ClienteJoven clienteJoven;
    private ClienteNatural clienteNatural;
    private Solicitud solicitud;
    private Operacion operacion;
    private Transferencia transferencia;
    private Cuenta cuenta;
    private Request request;
    private Response response;
    private BeneficiarioJoven beneficiarioJoven;
    private Giro giro;
    private JsonResultBisa resultBisa;
    private JsonResultClienteBisa resultClienteBisa;
    private JsonResultComun resultContactoBisa;
    private JsonResultCuentaBanco resultCuentaBanco;
    private JsonResultCuentaBisa resultCuentaBisa;
    private JsonResultBulkBisa resultBulkBisa;
    private JsonResultBulkConfirmar resultBulkConfirmar;
    private JsonResultInfoAgencias resultInfoAgencias;
    private JsonResultInfoTelefonicas resultInfoTelefonicas;
    private JsonResultInfoBancos resultInfoBancos;
    private List<Transferencia> transferencias;
    private int nroProcesados;
    private int nroNoProcesados;
    private List<JsonResultBulkBisa> procesados;
    private List<JsonResultBulkBisa> noProcesados;

    public ResponseOperacion(Request request) {
        setValido(false);
        setRequest(request);
    }

    public boolean isValido() {
        return valido;
    }

    public void setValido(boolean valido) {
        this.valido = valido;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ClienteJoven getClienteJoven() {
        return clienteJoven;
    }

    void setClienteJoven(ClienteJoven clienteJoven) {
        this.clienteJoven = clienteJoven;
    }

    public ClienteNatural getClienteNatural() {
        return clienteNatural;
    }

    void setClienteNatural(ClienteNatural clienteNatural) {
        this.clienteNatural = clienteNatural;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }


    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public JsonResultBisa getResultBisa() {
        return resultBisa;
    }

    void setResultBisa(JsonResultBisa resultBisa) {
        this.resultBisa = resultBisa;
        setResponse(resultBisa);
    }

    public JsonResultClienteBisa getResultClienteBisa() {
        return resultClienteBisa;
    }

    void setResultClienteBisa(JsonResultClienteBisa resultClienteBisa) {
        this.resultClienteBisa = resultClienteBisa;
        setResponse(resultClienteBisa);
    }

    public JsonResultComun getResultContactoBisa() {
        return resultContactoBisa;
    }

    void setResultContactoBisa(JsonResultComun resultContactoBisa) {
        this.resultContactoBisa = resultContactoBisa;
        setResponse(resultContactoBisa);
    }

    public JsonResultCuentaBanco getResultCuentaBanco() {
        return resultCuentaBanco;
    }

    void setResultCuentaBanco(JsonResultCuentaBanco resultCuentaBanco) {
        this.resultCuentaBanco = resultCuentaBanco;
        setResponse(resultCuentaBanco);
    }

    public JsonResultCuentaBisa getResultCuentaBisa() {
        return resultCuentaBisa;
    }

    void setResultCuentaBisa(JsonResultCuentaBisa resultCuentaBisa) {
        this.resultCuentaBisa = resultCuentaBisa;
        setResponse(resultCuentaBisa);
    }

    public JsonResultBulkBisa getResultBulkBisa() {
        return resultBulkBisa;
    }

    public void setResultBulkBisa(JsonResultBulkBisa resultBulkBisa) {
        this.resultBulkBisa = resultBulkBisa;
        setResponse(resultBulkBisa);
    }

    public JsonResultBulkConfirmar getResultBulkConfirmar() {
        return resultBulkConfirmar;
    }

    public void setResultBulkConfirmar(JsonResultBulkConfirmar resultBulkConfirmar) {
        this.resultBulkConfirmar = resultBulkConfirmar;
        setResponse(resultBulkConfirmar);
    }

    public Transferencia getTransferencia() {
        return transferencia;
    }

    public void setTransferencia(Transferencia transferencia) {
        this.transferencia = transferencia;
    }

    public int getNroProcesados() {
        return nroProcesados;
    }

    public void setNroProcesados(int nroProcesados) {
        this.nroProcesados = nroProcesados;
    }

    public int getNroNoProcesados() {
        return nroNoProcesados;
    }

    public void setNroNoProcesados(int nroNoProcesados) {
        this.nroNoProcesados = nroNoProcesados;
    }

    public List<JsonResultBulkBisa> getProcesados() {
        return procesados;
    }

    public void setProcesados(List<JsonResultBulkBisa> procesados) {
        this.procesados = procesados;
    }

    public List<JsonResultBulkBisa> getNoProcesados() {
        return noProcesados;
    }

    public void setNoProcesados(List<JsonResultBulkBisa> noProcesados) {
        this.noProcesados = noProcesados;
    }

    public List<Transferencia> getTransferencias() {
        return transferencias;
    }

    public void setTransferencias(List<Transferencia> transferencias) {
        this.transferencias = transferencias;
    }

    public JsonResultInfoAgencias getResultInfoAgencias() {
        return resultInfoAgencias;
    }

    public void setResultInfoAgencias(JsonResultInfoAgencias resultInfoAgencias) {
        this.resultInfoAgencias = resultInfoAgencias;
        setResponse(resultInfoAgencias);
    }

    public JsonResultInfoTelefonicas getResultInfoTelefonicas() {
        return resultInfoTelefonicas;
    }

    public void setResultInfoTelefonicas(JsonResultInfoTelefonicas resultInfoTelefonicas) {
        this.resultInfoTelefonicas = resultInfoTelefonicas;
        setResponse(resultInfoTelefonicas);
    }

    public JsonResultInfoBancos getResultInfoBancos() {
        return resultInfoBancos;
    }

    public void setResultInfoBancos(JsonResultInfoBancos resultInfoBancos) {
        this.resultInfoBancos = resultInfoBancos;
        setResponse(resultInfoBancos);
    }

    public BeneficiarioJoven getBeneficiarioJoven() {
        return beneficiarioJoven;
    }

    public void setBeneficiarioJoven(BeneficiarioJoven beneficiarioJoven) {
        this.beneficiarioJoven = beneficiarioJoven;
    }

    public Giro getGiro() {
        return giro;
    }

    public void setGiro(Giro giro) {
        this.giro = giro;
    }
}
