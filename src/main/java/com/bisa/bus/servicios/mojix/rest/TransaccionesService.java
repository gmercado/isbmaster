package com.bisa.bus.servicios.mojix.rest;

import com.bisa.bus.servicios.mojix.services.TransaccionesMojixService;
import com.bisa.bus.servicios.mojix.services.types.*;
import com.google.inject.Inject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * @author by josanchez on 11/07/2016.
 * @author rsalvatierra on 04/10/2016.
 */
@Path("/sjoven/Transacciones")
@Produces({"application/json;charset=UTF-8"})
public class TransaccionesService extends CommonService {

    private static final String INI_TRANSFERENCIA = "/iniTransferencia";
    private static final String CONF_TRANSFERENCIA = "/confTransferencia";
    private static final String INI_TRANSF_PROGRAMADA = "/iniTransfProgramada";
    private static final String CONF_TRANSF_PROGRAMADA = "/confTransfProgramada";
    private static final String TRANSFERENCIA_PROGRAMADA = "/TransferenciaProgramada";
    private static final String INI_BULK_TRANSFERENCIA = "/iniBulkTransferencia";
    private static final String CONF_BULK_TRANSFERENCIA = "/confBulkTransferencia";
    private static final String INI_TRANSFERENCIA_ACH = "/iniTransferenciaACH";
    private static final String CONF_TRANSFERENCIA_ACH = "/confTransferenciaACH";
    private static final String INI_TRANSF_PROGRAMADA_ACH = "/iniTransfProgramadaACH";
    private static final String CONF_TRANS_PROGRAMADA_ACH = "/confTransProgramadaACH";
    private static final String TRANSFERENCIA_PROGRAMADA_ACH = "/transferenciaProgramadaACH";
    private static final String INI_GIRO_MOVIL = "/iniGiroMovil";
    private static final String CONF_GIRO_MOVIL = "/confGiroMovil";

    @Inject
    private TransaccionesMojixService transaccionesMojixService;

    /**
     * Permite iniciar Transferencia entre cuentas banco BISA
     *
     * @param httpHeaders          headers
     * @param requestTransferencia datos basicos
     * @return JsonResultContactoBisa
     */
    @POST
    @Path(INI_TRANSFERENCIA)
    public JsonResultComun iniciarTransferencia(@Context HttpHeaders httpHeaders, JsonRequestTransferencia requestTransferencia) {
        if (requestTransferencia == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.iniciarTransferencia(requestTransferencia, getUser(httpHeaders));
        }
    }

    /**
     * Permite confirmar una transferencia en base al Token SMS enviado por el banco BISA
     *
     * @param httpHeaders      headers
     * @param requestConfirmar datos basicos
     * @return JsonResultContactoBisa
     */
    @POST
    @Path(CONF_TRANSFERENCIA)
    public JsonResultComun confirmarTransferencia(@Context HttpHeaders httpHeaders, JsonRequestConfirmar requestConfirmar) {
        if (requestConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.confirmarTransferencia(requestConfirmar, getUser(httpHeaders));
        }
    }

    /**
     * El servicio Permite Solicitar Una transferencia Programda
     *
     * @param httpHeaders             headers
     * @param requestTransfProgramada datos basicos
     * @return JsonResultContactoBisa
     */
    @POST
    @Path(INI_TRANSF_PROGRAMADA)
    public JsonResultComun iniciarTransferenciaProgramada(@Context HttpHeaders httpHeaders, JsonRequestTransfProgramada requestTransfProgramada) {
        if (requestTransfProgramada == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.iniciarTransferenciaProgramada(requestTransfProgramada, getUser(httpHeaders));
        }
    }

    /**
     * El servicio permite confirmar la Solicitud de transferencia programada
     *
     * @param httpHeaders      headers
     * @param requestConfirmar datos basicos
     * @return JsonResultContactoBisa
     */
    @POST
    @Path(CONF_TRANSF_PROGRAMADA)
    public JsonResultComun confirmarTransferenciaProgramada(@Context HttpHeaders httpHeaders, JsonRequestConfirmar requestConfirmar) {
        if (requestConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.confirmarTransferenciaProgramada(requestConfirmar, getUser(httpHeaders));
        }
    }

    /**
     * Permite Realizar una Transferencia Programada sin solicitar TokenSMS
     *
     * @param httpHeaders             headers
     * @param requestTransfProgramada datos basicos
     * @return JsonResultContactoBisa
     */
    @POST
    @Path(TRANSFERENCIA_PROGRAMADA)
    public JsonResultComun transferenciaProgramada(@Context HttpHeaders httpHeaders, JsonRequestTransfProgramada requestTransfProgramada) {
        if (requestTransfProgramada == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.transferenciaProgramada(requestTransfProgramada, getUser(httpHeaders));
        }
    }

    /**
     * Permite iniciar varias Trasnferencias entre cuentas banco BISA
     *
     * @param httpHeaders headers
     * @param requestBulk datos basicos
     * @return JsonResultBulkBisa
     */
    @POST
    @Path(INI_BULK_TRANSFERENCIA)
    public JsonResultBulkBisa iniciarBulkTransferencia(@Context HttpHeaders httpHeaders, JsonRequestBulk requestBulk) {
        if (requestBulk == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.iniciarBulkTransferencia(requestBulk, getUser(httpHeaders));
        }
    }

    /**
     * Permite Confirmar varias Trasnferencias entre cuentas banco BISA en base token SMS
     *
     * @param httpHeaders          headers
     * @param requestBulkConfirmar datos basicos
     * @return JsonResultBulkConfirmar
     */
    @POST
    @Path(CONF_BULK_TRANSFERENCIA)
    public JsonResultBulkConfirmar confirmarBulkTransferencia(@Context HttpHeaders httpHeaders, JsonRequestBulkConfirmar requestBulkConfirmar) {
        if (requestBulkConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.confirmarBulkTransferencia(requestBulkConfirmar, getUser(httpHeaders));
        }
    }

    /**
     * Permite iniciar Transferencia entre cuentas de distintos Bancos (ACH)
     *
     * @param httpHeaders             headers
     * @param requestTransferenciaACH datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(INI_TRANSFERENCIA_ACH)
    public JsonResultComun iniciarTransferenciaACH(@Context HttpHeaders httpHeaders, JsonRequestTransferenciaACH requestTransferenciaACH) {
        if (requestTransferenciaACH == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.iniciarTransferenciaACH(requestTransferenciaACH, getUser(httpHeaders));
        }
    }

    /**
     * Permite confirmar una transferencia ACH en base al Token SMS enviado por el banco BISA
     *
     * @param httpHeaders      headers
     * @param requestConfirmar datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(CONF_TRANSFERENCIA_ACH)
    public JsonResultComun confirmarTransferenciaACH(@Context HttpHeaders httpHeaders, JsonRequestConfirmar requestConfirmar) {
        if (requestConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.confirmarTransferenciaACH(requestConfirmar, getUser(httpHeaders));
        }
    }

    /**
     * El servicio Permite Solicitar Una transferencia Programada entre otros bancos
     *
     * @param httpHeaders             headers
     * @param requestTransfProgramada datos basicos
     * @return JsonResultContactoBisa
     */
    @POST
    @Path(INI_TRANSF_PROGRAMADA_ACH)
    public JsonResultComun iniciarTransferenciaProgramadaACH(@Context HttpHeaders httpHeaders, JsonRequestTransfProgramadaACH requestTransfProgramada) {
        if (requestTransfProgramada == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.iniciarTransferenciaProgramadaACH(requestTransfProgramada, getUser(httpHeaders));
        }
    }

    /**
     * El servicio permite confirmar la Solicitud de transferencia programada entre otros bancos
     *
     * @param httpHeaders      headers
     * @param requestConfirmar datos basicos
     * @return JsonResultContactoBisa
     */
    @POST
    @Path(CONF_TRANS_PROGRAMADA_ACH)
    public JsonResultComun confirmarTransferenciaProgramadaACH(@Context HttpHeaders httpHeaders, JsonRequestConfirmar requestConfirmar) {
        if (requestConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.confirmarTransferenciaProgramadaACH(requestConfirmar, getUser(httpHeaders));
        }
    }

    /**
     * Permite Realizar una Transferencia Programada ACH sin solicitar TokenSMS
     *
     * @param httpHeaders             headers
     * @param requestTransfProgramada datos basicos
     * @return JsonResultContactoBisa
     */
    @POST
    @Path(TRANSFERENCIA_PROGRAMADA_ACH)
    public JsonResultComun transferenciaProgramadaACH(@Context HttpHeaders httpHeaders, JsonRequestTransfProgramadaACH requestTransfProgramada) {
        if (requestTransfProgramada == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.transferenciaProgramadaACH(requestTransfProgramada, getUser(httpHeaders));
        }
    }

    /**
     * Permite iniciar Transferencia de Giro Movil
     *
     * @param httpHeaders      headers
     * @param requestGiroMovil datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(INI_GIRO_MOVIL)
    public JsonResultComun iniciarGiroMovil(@Context HttpHeaders httpHeaders, JsonRequestGiroMovil requestGiroMovil) {
        if (requestGiroMovil == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.iniciarGiroMovil(requestGiroMovil, getUser(httpHeaders));
        }
    }

    /**
     * Permite confirmar una transferencia Giro Movil en base al Token SMS enviado por el banco BISA
     *
     * @param httpHeaders      headers
     * @param requestConfirmar datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(CONF_GIRO_MOVIL)
    public JsonResultComun confirmarGiroMovil(@Context HttpHeaders httpHeaders, JsonRequestConfirmar requestConfirmar) {
        if (requestConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesMojixService.confirmarGiroMovil(requestConfirmar, getUser(httpHeaders));
        }
    }


}