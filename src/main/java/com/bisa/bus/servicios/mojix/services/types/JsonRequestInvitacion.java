package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.ContactoMojix;
import com.bisa.bus.servicios.mojix.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author by josanchez on 22/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestInvitacion implements Serializable, Request {

    private String bisaClienteID;
    private String tipo;
    private String nombre;
    private String fecha;
    private BigDecimal monto;
    private String moneda;
    private String motivo;
    private String transactionID;
    private List<ContactoMojix> contactos;

    public JsonRequestInvitacion() {
    }


    public String getBisaClienteID() {
        return bisaClienteID;
    }

    public void setBisaClienteID(String bisaClienteID) {
        this.bisaClienteID = bisaClienteID;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public List<ContactoMojix> getContactos() {
        return contactos;
    }

    public void setContactos(List<ContactoMojix> contactos) {
        this.contactos = contactos;
    }

    @Override
    public String toString() {
        return "JsonRequestInvitacion{" +
                "bisaClienteID='" + bisaClienteID + '\'' +
                ", tipo='" + tipo + '\'' +
                ", nombre='" + nombre + '\'' +
                ", fecha='" + fecha + '\'' +
                ", monto=" + monto +
                ", moneda='" + moneda + '\'' +
                ", motivo='" + motivo + '\'' +
                ", transactionID='" + transactionID + '\'' +
                ", contactos=" + contactos +
                '}';
    }
}
