package com.bisa.bus.servicios.mojix.model;

import bus.interfaces.as400.entities.BancoDestino;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EntidadFinancieraMojix implements Serializable {
    private Long codigo;
    private String nombreBanco;

    public EntidadFinancieraMojix(BancoDestino bancoDestino) {
        setCodigo(bancoDestino.getId());
        setNombreBanco(StringUtils.trimToEmpty(bancoDestino.getNombreCorto()));
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    @Override
    public String toString() {
        return "EntidadFinancieraMojix{" +
                "codigo='" + codigo + '\'' +
                ", nombreBanco='" + nombreBanco + '\'' +
                '}';
    }
}
