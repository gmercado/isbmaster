package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 11/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestBasic implements Serializable, Request {

    private String CI;
    private String fechaNac;
    private String transactionID;

    public JsonRequestBasic() {
    }

    public JsonRequestBasic(String CI, String fechaNac, String transactionID) {
        setCI(CI);
        setFechaNac(fechaNac);
        setTransactionID(transactionID);
    }

    public String getCI() {
        return CI;
    }

    public void setCI(String CI) {
        this.CI = CI;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    @Override
    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    @Override
    public String toString() {
        return "JsonRequestBasic{" +
                "CI='" + CI + '\'' +
                ", fechaNac='" + fechaNac + '\'' +
                ", transactionID='" + transactionID + '\'' +
                '}';
    }
}


