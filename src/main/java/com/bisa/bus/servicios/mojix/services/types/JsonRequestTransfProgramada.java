package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Geografica;
import com.bisa.bus.servicios.mojix.model.Request;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by josanchez on 22/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
public class JsonRequestTransfProgramada extends JsonRequestTransferencia implements Serializable, Request {

    private String periodicidad;
    private String tipo;

    public JsonRequestTransfProgramada() {
    }

    public JsonRequestTransfProgramada(String bisaClienteID, String cuentaOrigen, String cuentaDestino, String moneda, BigDecimal monto, String motivo, String transactionID, String periodicidad, Geografica geoProperties) {
        setBisaClienteID(bisaClienteID);
        setCuentaOrigen(cuentaOrigen);
        setCuentaDestino(cuentaDestino);
        setMoneda(moneda);
        setMonto(monto);
        setMotivo(motivo);
        setTransactionID(transactionID);
        setPeriodicidad(periodicidad);
        setGeoProperties(geoProperties);
    }

    public String getPeriodicidad() {
        return periodicidad;
    }

    public void setPeriodicidad(String periodicidad) {
        this.periodicidad = periodicidad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "JsonRequestTransfProgramada{" +
                "bisaClienteID='" + getBisaClienteID() + '\'' +
                ", cuentaOrigen='" + getCuentaOrigen() + '\'' +
                ", cuentaDestino='" + getCuentaDestino() + '\'' +
                ", moneda='" + getMoneda() + '\'' +
                ", monto=" + getMonto() +
                ", motivo='" + getMotivo() + '\'' +
                ", transactionID='" + getTransactionID() + '\'' +
                ", periodicidad='" + getPeriodicidad() + '\'' +
                ", geoProperties=" + getGeoProperties() +
                '}';
    }

}
