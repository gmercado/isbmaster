package com.bisa.bus.servicios.mojix.services;

import com.bisa.bus.servicios.mojix.services.types.*;

/**
 * @author by rsalvatierra on 31/05/2017.
 */
public interface ClienteMojixService {

    JsonResultBisa iniciarCliente(JsonRequestBasic requestBasic, String usuario);

    JsonResultClienteBisa confirmarCliente(JsonRequestConfirmar request, String usuario);

    JsonResultComun iniciarContacto(JsonRequestContacto request, String usuario);

    JsonResultComun confirmarContacto(JsonRequestConfirmar request, String usuario);

    JsonResultCuentaBanco iniCuentaBancoBisa(JsonRequestCuentaBanco request, String usuario);

    JsonResultComun confirmarCuentaBancoBisa(JsonRequestConfirmarCuenta request, String usuario);

    JsonResultClienteBisa adicionaCliente(JsonRequestBasic requestBasic, String usuario);

    JsonResultComun confirmarCuentaOtroBanco(JsonRequestConfirmar requestConfirmar, String usuario);

    JsonResultComun iniCuentaOtroBanco(JsonRequestCuentaOtroBanco requestCuentaOtroBanco, String usuario);

    JsonResultComun iniCuentaGiroMovil(JsonRequestCuentaGiroMovil requestCuentaGiroMovil, String usuario);

    JsonResultComun confirmarCuentaGiroMovil(JsonRequestConfirmar requestConfirmar, String usuario);

    JsonResultComun enviarSMS(JsonRequestInvitacion requestInvitacion, String usuario);

    JsonResultComun enviarClaveMovil(JsonRequestClaveMovil requestClaveMovil, String usuario);
}
