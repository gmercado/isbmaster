package com.bisa.bus.servicios.mojix.model;

/**
 * @author by rsalvatierra on 07/07/2017.
 */
public enum EstadoRecarga {
    INA,
    ACT
}
