package com.bisa.bus.servicios.mojix.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.bisa.bus.servicios.mojix.entities.Solicitud;
import com.bisa.bus.servicios.mojix.model.EstadoSolicitud;
import com.bisa.bus.servicios.mojix.model.TipoSolicitud;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author by josanchez on 13/07/2016.
 * @author rsalvatierra modificado on 20/10/2016
 */
public class SolicitudDao extends DaoImpl<Solicitud, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SolicitudDao.class);

    @Inject
    public SolicitudDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public List<Solicitud> getByOperacion(Operacion operacion) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Solicitud> q = cb.createQuery(Solicitud.class);
                        Root<Solicitud> p = q.from(Solicitud.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("operacion"), operacion));
                        predicatesAnd.add(cb.equal(p.get("estadoSolicitud"), EstadoSolicitud.PEN));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Solicitud> query = entityManager.createQuery(q);
                        q.orderBy(cb.asc(p.get("id")));
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public Solicitud getByConfirmacion(Operacion operacion, final String codigoConfirmacion) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Solicitud> q = cb.createQuery(Solicitud.class);
                        Root<Solicitud> p = q.from(Solicitud.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("operacion"), operacion));
                        predicatesAnd.add(cb.equal(p.get("codigoConfirmacion"), StringUtils.trimToEmpty(codigoConfirmacion)));
                        predicatesAnd.add(cb.equal(p.get("estadoSolicitud"), EstadoSolicitud.PEN));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Solicitud> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public Solicitud getByConsultaTrans(Operacion operacion) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Solicitud> q = cb.createQuery(Solicitud.class);
                        Root<Solicitud> p = q.from(Solicitud.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("operacion"), operacion));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        q.orderBy(cb.asc(p.get("id")));
                        TypedQuery<Solicitud> query = entityManager.createQuery(q);
                        List<Solicitud> solicitudList = query.getResultList();
                        return (solicitudList.isEmpty() ? null : solicitudList.get(0));
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public Solicitud newSolicitud(TipoSolicitud tipoProcesoMojix) {
        Solicitud solicitud = new Solicitud();
        solicitud.setTipoProceso(tipoProcesoMojix.getTipo());
        solicitud.setEstadoSolicitud(EstadoSolicitud.PEN);
        return solicitud;
    }

    public Solicitud registrarOperacion(Solicitud operaciones, String usuario) {
        operaciones.setUsuarioCreador(usuario);
        operaciones.setFechaCreacion(new Date());
        return persist(operaciones);
    }

    public Solicitud actualizar(Solicitud operaciones, String usuario) {
        operaciones.setUsuarioModificador(usuario);
        operaciones.setFechaModificacion(new Date());
        return merge(operaciones);
    }
}
