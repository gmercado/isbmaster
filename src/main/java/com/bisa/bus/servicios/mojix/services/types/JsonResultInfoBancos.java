package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.CodigosConfirmacion;
import com.bisa.bus.servicios.mojix.model.EntidadFinancieraMojix;
import com.bisa.bus.servicios.mojix.model.Response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonResultInfoBancos implements Serializable, Response {

    private String statusCode;
    private String mensaje;
    private List<EntidadFinancieraMojix> bancos;

    public JsonResultInfoBancos(String statusCode, CodigosConfirmacion condConf, String mensaje) {
        setStatusCode(statusCode);
        setMensaje(mensaje);
    }

    public JsonResultInfoBancos(String statusCode, String mensaje, List<EntidadFinancieraMojix> bancos) {
        setStatusCode(statusCode);
        setMensaje(mensaje);
        setBancos(bancos);
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public void setConfCode(String confCode) {

    }

    public List<EntidadFinancieraMojix> getBancos() {
        return bancos;
    }

    public void setBancos(List<EntidadFinancieraMojix> bancos) {
        this.bancos = bancos;
    }

    @Override
    public String toString() {
        return "JsonResultInfoBancos{" +
                "statusCode='" + statusCode + '\'' +
                ", mensaje='" + mensaje + '\'' +
                ", bancos=" + bancos +
                '}';
    }
}
