package com.bisa.bus.servicios.mojix.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author by rsalvatierra on 18/04/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClienteBisa implements Serializable {

    private String bisaClienteID;
    private String nombre;
    private String paterno;
    private String materno;
    private String telefono;
    private String genero;

    public ClienteBisa() {
    }

    public ClienteBisa(String bisaClienteID, String nombre, String paterno, String materno, String telefono, String genero) {
        this.bisaClienteID = bisaClienteID;
        this.nombre = nombre;
        this.paterno = paterno;
        this.materno = materno;
        this.telefono = telefono;
        this.genero = genero;
    }

    public String getBisaClienteID() {
        return bisaClienteID;
    }

    public void setBisaClienteID(String bisaClienteID) {
        this.bisaClienteID = bisaClienteID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }


    @Override
    public String toString() {
        return "ClienteBisa{" +
                "bisaClienteID='" + bisaClienteID + '\'' +
                ", nombre='" + nombre + '\'' +
                ", paterno='" + paterno + '\'' +
                ", materno='" + materno + '\'' +
                ", telefono='" + telefono + '\'' +
                ", genero='" + genero + '\'' +
                '}';
    }


    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
