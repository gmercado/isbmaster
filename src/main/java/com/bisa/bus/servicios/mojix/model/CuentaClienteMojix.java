package com.bisa.bus.servicios.mojix.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author by rsalvatierra on 18/04/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CuentaClienteMojix implements Serializable {
    private String numeroCuenta;
    private String saldo;
    private String primaria;
    private MonedaMojix moneda;

    public CuentaClienteMojix(String numeroCuenta, String saldo, String primaria, MonedaMojix moneda) {
        setNumeroCuenta(numeroCuenta);
        setSaldo(saldo);
        setPrimaria(primaria);
        setMoneda(moneda);
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getPrimaria() {
        return primaria;
    }

    public void setPrimaria(String primaria) {
        this.primaria = primaria;
    }

    public MonedaMojix getMoneda() {
        return moneda;
    }

    public void setMoneda(MonedaMojix moneda) {
        this.moneda = moneda;
    }

    @Override
    public String toString() {
        return "CuentaClienteMojix{" +
                "numeroCuenta='" + numeroCuenta + '\'' +
                ", saldo='" + saldo + '\'' +
                ", primaria='" + primaria + '\'' +
                ", moneda=" + moneda +
                '}';
    }
}
