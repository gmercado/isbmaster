package com.bisa.bus.servicios.mojix.model;

import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 06/12/2016.
 */
public interface Transfer {

    String getCuentaOrigen();

    String getCuentaDestino();

    String getMoneda();

    BigDecimal getMonto();

    String getMotivo();

    String getPeriodicidad();

    Geografica getGeoProperties();
}
