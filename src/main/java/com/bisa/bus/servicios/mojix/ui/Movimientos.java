package com.bisa.bus.servicios.mojix.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.mojix.dao.OperacionDao;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.LinkedList;
import java.util.List;

/**
 * @author by rsalvatierra on 05/07/2017.
 */
@AuthorizeInstantiation({RolesBisa.JOVEN_MOVIMIENTOS})
@Menu(value = "Consulta de Movimientos", subMenu = SubMenu.JOVEN)
public class Movimientos extends Listado<Operacion, Long> {

    @Override
    protected ListadoPanel<Operacion, Long> newListadoPanel(String id) {

        return new ListadoPanel<Operacion, Long>(id) {
            @Override
            protected List<IColumn<Operacion, String>> newColumns(ListadoDataProvider<Operacion, Long> dataProvider) {
                List<IColumn<Operacion, String>> columns = new LinkedList<>();
                columns.add(new PropertyColumn<>(Model.of("ID"), "id", "id"));
                columns.add(new PropertyColumn<>(Model.of("Fecha Hora"), "fechaConsumo", "fechaConsumo"));
                columns.add(new PropertyColumn<>(Model.of("Cod. Cliente"), "cliente.codigoCliente"));
                columns.add(new PropertyColumn<>(Model.of("Transacci\u00f3n"), "transaccionID"));
                columns.add(new PropertyColumn<>(Model.of("Tipo Transacci\u00f3n"), "tipoOperacion.descripcion"));
                columns.add(new PropertyColumn<>(Model.of("Movimiento"), "proceso.descripcion"));
                columns.add(new PropertyColumn<>(Model.of("Estado"), "estadoOperacion.descripcion"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<Operacion, Long>> getProviderClazz() {
                return OperacionDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<Operacion> newFiltroPanel(String id, IModel<Operacion> model1, IModel<Operacion> model2) {
                FiltroMovimientos filtro = new FiltroMovimientos(id, model1, model2);
                /*filtro.add(new Link("saveExcel") {
                    @Override
                    public void onClick() {
                        if (getModel1() == null || getModel1().getObject() == null) {
                            getSession().error("Hubo un error al exportar el archivo.");
                            return;
                        }
                        if (getModel1().getObject().getFecha() == null || getModel2().getObject().getFecha() == null) {
                            getSession().error("Las fechas son requeridas.");
                            return;
                        }
                        AbstractResourceStreamWriter resourceStreamWriter = new AbstractResourceStreamWriter() {
                            @Override
                            public void write(final OutputStream output) throws IOException {
                                BitacoraFacturaDao bitacoraFacturaDao = getInstance(BitacoraFacturaDao.class);
                                List<BitacoraFactura> consumos = bitacoraFacturaDao.getConsumos(getModel1().getObject(), getModel2().getObject());
                                byte[] reporte = ReporteConsumosPortal.get(consumos);
                                if (reporte != null) {
                                    output.write(reporte);
                                    output.flush();
                                    output.close();
                                }
                            }
                        };

                        getRequestCycle().scheduleRequestHandlerAfterCurrent(new ResourceStreamRequestHandler(resourceStreamWriter)
                                .setFileName(ReporteConsumosPortal.NOMBRE_REPORTE + ReporteConsumosPortal.EXTENSION_REPORTE));
                    }
                });*/
                return filtro;
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("fechaCreacion", false);
            }


            @Override
            protected Operacion newObject2() {
                return new Operacion();
            }

            @Override
            protected Operacion newObject1() {
                return new Operacion();
            }

            @Override
            protected boolean isVisibleData() {
                Operacion operacion1 = getModel1().getObject();
                Operacion operacion2 = getModel2().getObject();
                return !((operacion1.getFechaCreacion() == null || operacion2.getFechaCreacion() == null));
            }
        };
    }

}

