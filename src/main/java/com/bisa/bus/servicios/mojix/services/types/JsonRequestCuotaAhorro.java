package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestCuotaAhorro implements Serializable, Request {
    private String bisaClienteID;
    private String cuenta;
    private BigDecimal monto;
    private String moneda;
    private String transactionID;

    public JsonRequestCuotaAhorro() {
    }

    public String getBisaClienteID() {
        return bisaClienteID;
    }

    public void setBisaClienteID(String bisaClienteID) {
        this.bisaClienteID = bisaClienteID;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    @Override
    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    @Override
    public String toString() {
        return "JsonRequestCuotaAhorro{" +
                "bisaClienteID='" + bisaClienteID + '\'' +
                ", cuenta='" + cuenta + '\'' +
                ", monto=" + monto +
                ", moneda='" + moneda + '\'' +
                ", transactionID='" + transactionID + '\'' +
                '}';
    }
}
