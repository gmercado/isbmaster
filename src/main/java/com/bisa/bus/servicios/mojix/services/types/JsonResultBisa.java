package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.CodigosConfirmacion;
import com.bisa.bus.servicios.mojix.model.Response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 11/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonResultBisa implements Serializable, Response {
    public static final String ZERO = "0";
    private String statusCode;
    private String count;
    private String confCode;
    private String mensaje;

    public JsonResultBisa(String statusCode, CodigosConfirmacion confCode, String mensaje) {
        setStatusCode(statusCode);
        setCount(ZERO);
        setConfCode(confCode.getCodigo());
        setMensaje(mensaje);
    }

    public JsonResultBisa(String statusCode, String count, String mensaje) {
        setStatusCode(statusCode);
        setCount(count);
        setConfCode(null);
        setMensaje(mensaje);
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getConfCode() {
        return confCode;
    }

    public void setConfCode(String confCode) {
        this.confCode = confCode;
    }

    @Override
    public String toString() {
        return "JsonResultBisa{" +
                "statusCode='" + getStatusCode() + '\'' +
                ", count='" + getCount() + '\'' +
                ", confCode='" + getConfCode() + '\'' +
                ", mensajee='" + getMensaje() + '\'' +
                '}';
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}

