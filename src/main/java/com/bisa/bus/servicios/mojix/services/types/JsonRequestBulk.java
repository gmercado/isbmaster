package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.CuentaTransferencia;
import com.bisa.bus.servicios.mojix.model.Geografica;
import com.bisa.bus.servicios.mojix.model.Request;
import com.bisa.bus.servicios.mojix.model.TransferBulk;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

/**
 * @author by josanchez on 16/08/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestBulk implements Serializable, Request, TransferBulk {

    private String bisaClienteID;
    private String cuentaOrigen;
    private String motivo;
    private String bulkTransactionID;
    private Geografica geoProperties;
    private List<CuentaTransferencia> cuentas;

    public JsonRequestBulk() {
    }

    public JsonRequestBulk(String bisaClienteID, String cuentaOrigen, String motivo, String bulkTransactionID, Geografica geoProperties, List<CuentaTransferencia> cuentas) {
        setBisaClienteID(bisaClienteID);
        setCuentaOrigen(cuentaOrigen);
        setMotivo(motivo);
        setBulkTransactionID(bulkTransactionID);
        setGeoProperties(geoProperties);
        setCuentas(cuentas);
    }

    public JsonRequestBulk(String bisaClienteID) {
        this.bisaClienteID = bisaClienteID;
    }

    public String getBisaClienteID() {
        return bisaClienteID;
    }

    public void setBisaClienteID(String bisaClienteID) {
        this.bisaClienteID = bisaClienteID;
    }

    public String getCuentaOrigen() {
        return cuentaOrigen;
    }

    public void setCuentaOrigen(String cuentaOrigen) {
        this.cuentaOrigen = cuentaOrigen;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getBulkTransactionID() {
        return bulkTransactionID;
    }

    public void setBulkTransactionID(String bulkTransactionID) {
        this.bulkTransactionID = bulkTransactionID;
    }

    public Geografica getGeoProperties() {
        return geoProperties;
    }

    public void setGeoProperties(Geografica geoProperties) {
        this.geoProperties = geoProperties;
    }

    public List<CuentaTransferencia> getCuentas() {
        return cuentas;
    }

    public void setCuentas(List<CuentaTransferencia> cuentas) {
        this.cuentas = cuentas;
    }

    @Override
    public String getTransactionID() {
        return getBulkTransactionID();
    }

    @Override
    public String toString() {
        return "JsonRequestBulk{" +
                "bisaClienteID='" + bisaClienteID + '\'' +
                ", cuentaOrigen='" + cuentaOrigen + '\'' +
                ", motivo='" + motivo + '\'' +
                ", bulkTransactionID='" + bulkTransactionID + '\'' +
                ", geoProperties=" + geoProperties +
                ", cuentas=" + cuentas +
                '}';
    }
}
