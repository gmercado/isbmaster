package com.bisa.bus.servicios.mojix.entities;

import bus.consumoweb.pagoservicios.model.Servicio;
import bus.database.model.EntityBase;
import com.bisa.bus.servicios.mojix.model.EstadoRecarga;
import com.bisa.bus.servicios.mojix.model.TipoEmpresaServicio;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 07/07/2017.
 */
@Entity
@Table(name = "SJOV07")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S07FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S07USRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S07FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S07USRMOD"))
})
public class Recarga extends EntityBase implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S07CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "S07CODOPE")
    private Operacion operacion;
    @Column(name = "S07NROCUE", columnDefinition = "VARCHAR(10)")
    private String cuenta;
    @Column(name = "S07MONEDA", columnDefinition = "VARCHAR(2)")
    private Short moneda;
    @Column(name = "S07MONTOS", columnDefinition = "VARCHAR(2)")
    private BigDecimal importe;
    @Column(name = "S07EMPTEL", columnDefinition = "VARCHAR(2)")
    private String empresa;
    @Column(name = "S07NOMBRE", columnDefinition = "VARCHAR(20)")
    private String nombreCompletoFactura;
    @Column(name = "S07NRODOC", columnDefinition = "VARCHAR(20)")
    private String docIdentidadFactura;
    @Column(name = "S07CIUDAF", columnDefinition = "VARCHAR(3)")
    private String ciudadFactura;
    @Column(name = "S07AGENCI", columnDefinition = "NUMERIC(3)")
    private int agenciaFactura;
    @Column(name = "S07NROTEL", columnDefinition = "NUMERIC(8)")
    private Long celularBeneficiario;
    @Enumerated(EnumType.STRING)
    @Column(name = "S07ESTADO", columnDefinition = "VARCHAR(2)")
    private EstadoRecarga estadoRecarga;
    @Column(name = "S07NUMEIP", columnDefinition = "VARCHAR(15)")
    private String ip;
    @Column(name = "S07CODPAI", columnDefinition = "VARCHAR(3)")
    private String pais;
    @Column(name = "S07CIUDAD", columnDefinition = "VARCHAR(3)")
    private String ciudad;
    @Column(name = "S07LATITU", columnDefinition = "numeric(10,6)")
    private BigDecimal latitud;
    @Column(name = "S07LONGIT", columnDefinition = "numeric(10,6)")
    private BigDecimal longitud;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public Short getMoneda() {
        return moneda;
    }

    public void setMoneda(Short moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getNombreCompletoFactura() {
        return nombreCompletoFactura;
    }

    public void setNombreCompletoFactura(String nombreCompletoFactura) {
        this.nombreCompletoFactura = nombreCompletoFactura;
    }

    public String getDocIdentidadFactura() {
        return docIdentidadFactura;
    }

    public void setDocIdentidadFactura(String docIdentidadFactura) {
        this.docIdentidadFactura = docIdentidadFactura;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public BigDecimal getLatitud() {
        return latitud;
    }

    public void setLatitud(BigDecimal latitud) {
        this.latitud = latitud;
    }

    public BigDecimal getLongitud() {
        return longitud;
    }

    public void setLongitud(BigDecimal longitud) {
        this.longitud = longitud;
    }

    public Long getCelularBeneficiario() {
        return celularBeneficiario;
    }

    public void setCelularBeneficiario(Long celularBeneficiario) {
        this.celularBeneficiario = celularBeneficiario;
    }

    public EstadoRecarga getEstadoRecarga() {
        return estadoRecarga;
    }

    public void setEstadoRecarga(EstadoRecarga estadoRecarga) {
        this.estadoRecarga = estadoRecarga;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCiudadFactura() {
        return ciudadFactura;
    }

    public void setCiudadFactura(String ciudadFactura) {
        this.ciudadFactura = ciudadFactura;
    }

    public int getAgenciaFactura() {
        return agenciaFactura;
    }

    public void setAgenciaFactura(int agenciaFactura) {
        this.agenciaFactura = agenciaFactura;
    }

    public Servicio getServicio() {
        TipoEmpresaServicio tipoEmpresaServicio = TipoEmpresaServicio.get(getEmpresa());
        Servicio servicio = new Servicio();
        if (tipoEmpresaServicio != null) {
            servicio.setEmpresaCodigo(tipoEmpresaServicio.name());
            servicio.setCodigo(tipoEmpresaServicio.getDescripcion());
            servicio.setTipo(tipoEmpresaServicio.getCodigo());
        }
        return servicio;
    }
}
