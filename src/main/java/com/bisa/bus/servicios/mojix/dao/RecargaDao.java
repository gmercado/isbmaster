package com.bisa.bus.servicios.mojix.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.bisa.bus.servicios.mojix.entities.Recarga;
import com.bisa.bus.servicios.mojix.model.EstadoRecarga;
import com.bisa.bus.servicios.mojix.model.Geografica;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

/**
 * @author by rsalvatierra on 07/07/2017.
 */
public class RecargaDao extends DaoImpl<Recarga, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecargaDao.class);

    @Inject
    protected RecargaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Long> countPath(Root<Recarga> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<Recarga> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.get("celularBeneficiario"));
        return paths;
    }

    public Recarga newRecarga(Geografica lugar) {
        Recarga recarga = new Recarga();
        if (lugar != null) {
            recarga.setPais(lugar.getPais());
            recarga.setCiudad(lugar.getCiudad());
            recarga.setLatitud(new BigDecimal(lugar.getLatitud()));
            recarga.setLongitud(new BigDecimal(lugar.getLongitud()));
            recarga.setIp(lugar.getIP());
        }
        recarga.setEstadoRecarga(EstadoRecarga.INA);
        return recarga;
    }

    public Recarga registrar(Recarga recarga, String usuario) {
        recarga.setUsuarioCreador(usuario);
        recarga.setFechaCreacion(new Date());
        return persist(recarga);
    }

    public Recarga actualizar(Recarga recarga, String usuario) {
        recarga.setUsuarioModificador(usuario);
        recarga.setFechaModificacion(new Date());
        return merge(recarga);
    }

    /**
     * Retorna Recarga a partir de la operacion
     *
     * @param pOperacion Codigo operacion
     * @return Recarga
     */
    public Recarga getByOperacion(Operacion pOperacion) {
        LOGGER.debug("Obteniendo registro por operacion:{}.", pOperacion);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Recarga> q = cb.createQuery(Recarga.class);
                        Root<Recarga> p = q.from(Recarga.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("operacion"), pOperacion));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Recarga> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }
}
