package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestInfo implements Serializable, Request {

    private String bisaClienteID;
    private String transactionID;

    public JsonRequestInfo(String bisaClienteID) {
        this.bisaClienteID = bisaClienteID;
    }

    public JsonRequestInfo() {
    }

    public String getBisaClienteID() {
        return bisaClienteID;
    }

    public void setBisaClienteID(String bisaClienteID) {
        this.bisaClienteID = bisaClienteID;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    @Override
    public String toString() {
        return "JsonRequestInfo{" +
                "bisaClienteID='" + bisaClienteID + '\'' +
                '}';
    }
}
