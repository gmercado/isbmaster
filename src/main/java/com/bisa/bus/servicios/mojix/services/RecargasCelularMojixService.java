package com.bisa.bus.servicios.mojix.services;

import com.bisa.bus.servicios.mojix.services.types.JsonRequestConfirmar;
import com.bisa.bus.servicios.mojix.services.types.JsonRequestRecarga;
import com.bisa.bus.servicios.mojix.services.types.JsonResultComun;

/**
 * @author by rsalvatierra on 22/06/2017.
 */
public interface RecargasCelularMojixService {
    JsonResultComun iniciarRecarga(JsonRequestRecarga requestRecarga, String usuario);

    JsonResultComun confirmarRecarga(JsonRequestConfirmar requestConfirmar, String usuario);
}
