package com.bisa.bus.servicios.mojix.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author by rsalvatierra on 18/04/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CuentaBancoBisa implements Serializable {
    private String numeroCuenta;
    private MonedaMojix moneda;

    public CuentaBancoBisa(String numeroCuenta, MonedaMojix moneda) {
        setNumeroCuenta(numeroCuenta);
        setMoneda(moneda);
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public MonedaMojix getMoneda() {
        return moneda;
    }

    public void setMoneda(MonedaMojix moneda) {
        this.moneda = moneda;
    }

    @Override
    public String toString() {
        return "CuentaBancoBisa{" +
                "numeroCuenta='" + numeroCuenta + '\'' +
                ", moneda=" + moneda +
                '}';
    }
}
