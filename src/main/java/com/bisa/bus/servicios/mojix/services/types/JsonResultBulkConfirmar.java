package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.CodigosConfirmacion;
import com.bisa.bus.servicios.mojix.model.Response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonResultBulkConfirmar implements Serializable, Response {

    private String statusCode;
    private String mensaje;
    private String bulkTransactionID;
    private String bulkConfCode;
    private Integer totalProcesados;
    private List<JsonResultBulkBisa> procesados;
    private Integer totalNoProcesados;
    private List<JsonResultBulkBisa> noProcesados;

    public JsonResultBulkConfirmar(String statusCode, String mensaje) {
        setStatusCode(statusCode);
        setBulkConfCode(null);
        setMensaje(mensaje);
    }

    public JsonResultBulkConfirmar(String statusCode, CodigosConfirmacion confCode, String mensaje) {
        setStatusCode(statusCode);
        setBulkConfCode(confCode.getCodigo());
        setMensaje(mensaje);
    }

    public JsonResultBulkConfirmar(String statusCode, String mensaje, String bulkTransactionID, String bulkConfCode,
                                   Integer totalProcesados, List<JsonResultBulkBisa> procesados,
                                   Integer totalNoProcesados, List<JsonResultBulkBisa> noProcesados) {
        setStatusCode(statusCode);
        setMensaje(mensaje);
        setBulkTransactionID(bulkTransactionID);
        setBulkConfCode(bulkConfCode);
        setTotalProcesados(totalProcesados);
        setProcesados(procesados);
        setTotalNoProcesados(totalNoProcesados);
        setNoProcesados(noProcesados);
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public void setConfCode(String confCode) {
        setBulkConfCode(confCode);
    }

    public String getBulkTransactionID() {
        return bulkTransactionID;
    }

    public void setBulkTransactionID(String bulkTransactionID) {
        this.bulkTransactionID = bulkTransactionID;
    }

    public String getBulkConfCode() {
        return bulkConfCode;
    }

    public void setBulkConfCode(String bulkConfCode) {
        this.bulkConfCode = bulkConfCode;
    }

    public Integer getTotalProcesados() {
        return totalProcesados;
    }

    public void setTotalProcesados(Integer totalProcesados) {
        this.totalProcesados = totalProcesados;
    }

    public List<JsonResultBulkBisa> getProcesados() {
        if (procesados == null) {
            procesados = new ArrayList<>();
        }
        return procesados;
    }

    public void setProcesados(List<JsonResultBulkBisa> procesados) {
        this.procesados = procesados;
    }

    public Integer getTotalNoProcesados() {
        return totalNoProcesados;
    }

    public void setTotalNoProcesados(Integer totalNoProcesados) {
        this.totalNoProcesados = totalNoProcesados;
    }

    public List<JsonResultBulkBisa> getNoProcesados() {
        if (noProcesados == null) {
            noProcesados = new ArrayList<>();
        }
        return noProcesados;
    }

    public void setNoProcesados(List<JsonResultBulkBisa> noProcesados) {
        this.noProcesados = noProcesados;
    }


    @Override
    public String toString() {
        return "JsonResultBulkConfirmar{" +
                "statusCode='" + getStatusCode() + '\'' +
                ", mensaje='" + getMensaje() + '\'' +
                ", bulkTransactionID='" + getBulkTransactionID() + '\'' +
                ", bulkConfCode='" + getBulkConfCode() + '\'' +
                ", totalProcesados=" + getTotalProcesados() +
                ", procesados=" + getProcesados() +
                ", totalNoProcesados=" + getTotalNoProcesados() +
                ", noProcesados=" + getNoProcesados() +
                '}';
    }
}
