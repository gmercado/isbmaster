package com.bisa.bus.servicios.mojix.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.bisa.bus.servicios.mojix.entities.Transferencia;
import com.bisa.bus.servicios.mojix.model.EstadoTransferencia;
import com.bisa.bus.servicios.mojix.model.Geografica;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author by josanchez on 16/08/2016.
 * @author rsalvatierra modificado on 20/10/2016
 */
public class TransferenciaDao extends DaoImpl<Transferencia, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransferenciaDao.class);

    @Inject
    protected TransferenciaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public Transferencia getTransferByOperacion(Operacion operacion) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Transferencia> q = cb.createQuery(Transferencia.class);
                        Root<Transferencia> p = q.from(Transferencia.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("operacion"), operacion));
                        predicatesAnd.add(cb.equal(p.get("estadoTransferencia"), EstadoTransferencia.PENDIENTE.getCodigo()));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Transferencia> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }


    public List<Transferencia> getTranfersByOperacion(Operacion operacion) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Transferencia> q = cb.createQuery(Transferencia.class);
                        Root<Transferencia> p = q.from(Transferencia.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("operacion"), operacion));
                        predicatesAnd.add(cb.equal(p.get("estadoTransferencia"), EstadoTransferencia.PENDIENTE.getCodigo()));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Transferencia> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public Transferencia newTransferencia(Geografica lugar) {
        Transferencia transferencia = new Transferencia();
        if (lugar != null) {
            transferencia.setPais(lugar.getPais());
            transferencia.setCiudad(lugar.getCiudad());
            transferencia.setLatitud(new BigDecimal(lugar.getLatitud()));
            transferencia.setLongitud(new BigDecimal(lugar.getLongitud()));
            transferencia.setIp(lugar.getIP());
        }
        transferencia.setEstadoTransferencia(EstadoTransferencia.PENDIENTE.getCodigo());
        return transferencia;
    }

    public Transferencia registrar(Transferencia transferencia, String usuario) {
        transferencia.setUsuarioCreador(usuario);
        transferencia.setFechaCreacion(new Date());
        return persist(transferencia);
    }

    public Transferencia actualizar(Transferencia transferencia, String usuario) {
        transferencia.setUsuarioModificador(usuario);
        transferencia.setFechaModificacion(new Date());
        return merge(transferencia);
    }
}
