package com.bisa.bus.servicios.mojix.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.mojix.entities.ClienteJoven;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.OpenJPAQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.LinkedList;

/**
 * @author by josanchez on 14/07/2016.
 * @author rsalvatierra modificado on 20/10/2016
 */
public class ClienteJovenDao extends DaoImpl<ClienteJoven, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClienteJovenDao.class);
    private static final String CLIENTE_ACTIVO = "AC";
    private static final String CONTRATO_SEGMENTO_JOVEN = "SEGM";
    private static final String ESTADO_ACTIVO = "A";

    @Inject
    protected ClienteJovenDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    /**
     * Retorna Cliente Segmento Joven
     *
     * @param pCodigoCliente Codigo cliente
     * @return ClienteJoven
     */
    public ClienteJoven getByCodigoCliente(String pCodigoCliente) {
        LOGGER.debug("Obteniendo registro por Codigo Cliente:{}.", pCodigoCliente);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<ClienteJoven> q = cb.createQuery(ClienteJoven.class);
                        Root<ClienteJoven> p = q.from(ClienteJoven.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("codigoCliente"), StringUtils.trimToEmpty(pCodigoCliente)));
                        predicatesAnd.add(cb.equal(p.get("estadoCliente"), StringUtils.trimToEmpty(CLIENTE_ACTIVO)));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<ClienteJoven> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    /**
     * Retorna Cliente Segmento Joven
     *
     * @param pCodigoClienteEncriptado Codigo cliente Cifrado
     * @return ClienteJoven
     */
    public ClienteJoven getByCodigoCifrado(String pCodigoClienteEncriptado) {
        LOGGER.debug("Obteniendo registro por Codigo Cliente Cifrado:{}.", pCodigoClienteEncriptado);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<ClienteJoven> q = cb.createQuery(ClienteJoven.class);
                        Root<ClienteJoven> p = q.from(ClienteJoven.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("codigoClienteBisa"), StringUtils.trimToEmpty(pCodigoClienteEncriptado)));
                        predicatesAnd.add(cb.equal(p.get("estadoCliente"), StringUtils.trimToEmpty(CLIENTE_ACTIVO)));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<ClienteJoven> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    /**
     * Registrar Cliente Segmento Joven
     *
     * @param cliente Datos cliente
     * @param usuario Usuario
     * @return ClienteJoven
     */
    public ClienteJoven registrar(ClienteJoven cliente, String usuario) {
        cliente.setFechaCreacion(new Date());
        cliente.setUsuarioCreador(usuario);
        cliente.setEstadoCliente(CLIENTE_ACTIVO);
        return persist(cliente);
    }


    /**
     * Verificar si tiene contrato para Bisa NEO
     *
     * @param numeroCliente codigo de cliente
     * @return String
     */
    public Integer existeContrato(final String numeroCliente) {
        if (StringUtils.trimToNull(numeroCliente) == null) {
            LOGGER.error("Error: Debe especificar un numero de cliente.");
            return null;
        }
        return doWithTransaction((entityManager, t) -> {

            String cliente = StringUtils.leftPad(StringUtils.trimToEmpty(numeroCliente), 10, '0');

            String sql = "select c.Q510codigo from AQP510 c " +
                    "where c.q510nrocli = ? " +
                    "and c.Q510tipser = ? " +
                    "and c.q510estado = ? ";

            OpenJPAQuery<Integer> consultaCuenta =
                    entityManager.createNativeQuery(sql, Integer.class);

            consultaCuenta.setParameter(1, cliente);
            consultaCuenta.setParameter(2, CONTRATO_SEGMENTO_JOVEN);
            consultaCuenta.setParameter(3, ESTADO_ACTIVO);

            try {
                return consultaCuenta.getSingleResult();
            } catch (NoResultException e) {
                LOGGER.error("AS400 <NoResultException>: No se encontraron registro de cuentas para el cliente [" + cliente + "] en <getCuentaCliente>. ", e);
            } catch (Exception e) {
                LOGGER.error("AS400 <Exception>: Error al ejecutar la consulta en <getCuentaCliente> para el cliente [" + cliente + "]. ", e);
            }
            return null;
        });
    }
}
