package com.bisa.bus.servicios.mojix.model;

import bus.interfaces.as400.entities.Cliente;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.bisa.bus.servicios.mojix.entities.Solicitud;

import java.io.Serializable;

/**
 * @author by josanchez on 24/08/2016.
 */
public class EnvioDetalle implements Serializable {

    private String tipoServicio;

    private int tipoRespuesta;

    private Solicitud operaciones;

    private Cliente cliente;

    private Operacion operacion;

    private String generarClavePar;

    public EnvioDetalle(String tipoServicio, int tipoRespuesta, Solicitud operaciones, Cliente cliente, Operacion operacion, String generarClavePar) {
        setTipoServicio(tipoServicio);
        setTipoRespuesta(tipoRespuesta);
        setOperaciones(operaciones);
        setCliente(cliente);
        setOperacion(operacion);
        setGenerarClavePar(generarClavePar);
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public int getTipoRespuesta() {
        return tipoRespuesta;
    }

    public void setTipoRespuesta(int tipoRespuesta) {
        this.tipoRespuesta = tipoRespuesta;
    }

    public Solicitud getOperaciones() {
        return operaciones;
    }

    public void setOperaciones(Solicitud operaciones) {
        this.operaciones = operaciones;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }

    public String getGenerarClavePar() {
        return generarClavePar;
    }

    public void setGenerarClavePar(String generarClavePar) {
        this.generarClavePar = generarClavePar;
    }
}
