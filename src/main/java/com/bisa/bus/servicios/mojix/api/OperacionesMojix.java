package com.bisa.bus.servicios.mojix.api;

import bus.consumoweb.clavemovil.ClaveMovilDao;
import bus.consumoweb.clavemovil.RetornarClaveMovil;
import bus.consumoweb.facturacion.model.FacturaSolicitud;
import bus.consumoweb.giromovil.GiroMovilDao;
import bus.consumoweb.giromovil.model.EmitirGiroResponse;
import bus.consumoweb.giromovil.model.GiroMovil;
import bus.consumoweb.pagoservicios.PagoServiciosDao;
import bus.consumoweb.pagoservicios.model.*;
import bus.consumoweb.yellowpepper.dao.YellowPepperServicioDao;
import bus.interfaces.as400.dao.AgenciaDao;
import bus.interfaces.as400.dao.BancoDestinoDao;
import bus.interfaces.as400.dao.CuentaDao;
import bus.interfaces.as400.entities.Agencia;
import bus.interfaces.as400.entities.BancoDestino;
import bus.interfaces.as400.entities.Cliente;
import bus.interfaces.as400.entities.Cuenta;
import bus.interfaces.ebisa.dao.BeneficiarioDao;
import bus.interfaces.ebisa.dao.EstadoLoteHistoricoDao;
import bus.interfaces.ebisa.dao.PrevencionLavado;
import bus.interfaces.ebisa.dao.TipoPagoLoteHistoricoDao;
import bus.interfaces.ebisa.entities.Beneficiario;
import bus.interfaces.ebisa.entities.EstadoLoteHistorico;
import bus.interfaces.ebisa.tipos.TipoCuentaBeneficiario;
import bus.interfaces.ebisa.tipos.TipoCuentaOtrosBancos;
import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.plumbing.utils.TelefonoConverter;
import com.bisa.bus.servicios.mojix.entities.BeneficiarioJoven;
import com.bisa.bus.servicios.mojix.entities.Giro;
import com.bisa.bus.servicios.mojix.entities.Recarga;
import com.bisa.bus.servicios.mojix.model.AgenciasMojix;
import com.bisa.bus.servicios.mojix.model.EmpresaTelefonicaMojix;
import com.bisa.bus.servicios.mojix.model.EntidadFinancieraMojix;
import com.bisa.bus.servicios.mojix.model.TipoBeneficiario;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * @author by rsalvatierra on 28/06/2017.
 */
class OperacionesMojix {

    private static final Logger LOGGER = LoggerFactory.getLogger(OperacionesMojix.class);
    private static final String PREPAGO = "PREPAGO";
    private static final String TIGO = "TIGO";
    private static final String ENTEL = "ENTEL";
    private static final String VIVA = "VIVA";
    private static final String SERVICIO_SJ = "SJ";
    private static final String APLICATION_SMS = "mojix";
    private static final int PRIORIDAD_DEFAULT = 1;

    private final PagoServiciosDao pagoServiciosDao;
    private final AgenciaDao agenciaDao;
    private final BancoDestinoDao bancoDestinoDao;
    private final BeneficiarioDao beneficiarioDao;
    private final EstadoLoteHistoricoDao estadoLoteHistoricoDao;
    private final TipoPagoLoteHistoricoDao tipoPagoLoteHistoricoDao;
    private final ClaveMovilDao claveMovilDao;
    private final GiroMovilDao giroMovilDao;
    private final YellowPepperServicioDao yellowPepperServicioDao;
    private final PrevencionLavado prevencionLavadoDao;
    private final CuentaDao cuentaDao;

    @Inject
    public OperacionesMojix(PagoServiciosDao pagoServiciosDao, AgenciaDao agenciaDao, BancoDestinoDao bancoDestinoDao, BeneficiarioDao beneficiarioDao, EstadoLoteHistoricoDao estadoLoteHistoricoDao, TipoPagoLoteHistoricoDao tipoPagoLoteHistoricoDao, ClaveMovilDao claveMovilDao, GiroMovilDao giroMovilDao, YellowPepperServicioDao yellowPepperServicioDao, PrevencionLavado prevencionLavadoDao, CuentaDao cuentaDao) {
        this.pagoServiciosDao = pagoServiciosDao;
        this.agenciaDao = agenciaDao;
        this.bancoDestinoDao = bancoDestinoDao;
        this.beneficiarioDao = beneficiarioDao;
        this.estadoLoteHistoricoDao = estadoLoteHistoricoDao;
        this.tipoPagoLoteHistoricoDao = tipoPagoLoteHistoricoDao;
        this.claveMovilDao = claveMovilDao;
        this.giroMovilDao = giroMovilDao;
        this.yellowPepperServicioDao = yellowPepperServicioDao;
        this.prevencionLavadoDao = prevencionLavadoDao;
        this.cuentaDao = cuentaDao;
    }

    List<EmpresaTelefonicaMojix> getEmpresasTelefonicas() throws ExecutionException, TimeoutException, JAXBException, IOException {
        List<EmpresaTelefonicaMojix> empresaTelefonicaMojixList = null;
        ListadoDeServiciosResponse serviciosResponse = pagoServiciosDao.consultarServicios();
        if (serviciosResponse != null && !serviciosResponse.getServicio().isEmpty()) {
            empresaTelefonicaMojixList = new ArrayList<>();
            for (Servicio servicio : serviciosResponse.getServicio()) {
                if (PREPAGO.equals(servicio.getCodigo())
                        && (TIGO.equals(servicio.getEmpresaCodigo())
                        || ENTEL.equals(servicio.getEmpresaCodigo())
                        || VIVA.equals(servicio.getEmpresaCodigo()))) {
                    LOGGER.debug("servicio -> {}", servicio);
                    empresaTelefonicaMojixList.add(new EmpresaTelefonicaMojix(servicio));
                }
            }
        }
        return empresaTelefonicaMojixList;
    }

    List<AgenciasMojix> getAgencias() {
        List<AgenciasMojix> agenciasMojixList = null;
        List<Agencia> agenciaList = agenciaDao.getAgenciasActivas();
        if (!agenciaList.isEmpty()) {
            agenciasMojixList = new ArrayList<>();
            for (Agencia agencia : agenciaList) {
                agenciasMojixList.add(new AgenciasMojix(agencia));
            }
        }
        return agenciasMojixList;
    }

    List<EntidadFinancieraMojix> getBancos() {
        List<EntidadFinancieraMojix> entidadFinancieraMojixList = null;
        List<BancoDestino> bancoDestinoList = bancoDestinoDao.getBancosActivos();
        if (!bancoDestinoList.isEmpty()) {
            entidadFinancieraMojixList = new ArrayList<>();
            for (BancoDestino bancoDestino : bancoDestinoList) {
                entidadFinancieraMojixList.add(new EntidadFinancieraMojix(bancoDestino));
            }
        }
        return entidadFinancieraMojixList;
    }

    BancoDestino getBancoDestino(Long codigo) {
        return bancoDestinoDao.find(codigo);
    }

    Beneficiario setBeneficiario(BeneficiarioJoven beneficiarioJoven, String codCliente, String usuario) {

        Beneficiario beneficiario = beneficiarioDao.getByCodigo(new BigDecimal(codCliente), beneficiarioJoven.getNumeroCuenta());
        if (beneficiario == null) {
            beneficiario = beneficiarioDao.getByCuenta(new BigDecimal(codCliente), beneficiarioJoven.getNumeroCuenta(), beneficiarioJoven.getBancoBeneficiario());
            if (beneficiario == null) {
                //Registrar nuevo beneficiario
                beneficiario = new Beneficiario();
                beneficiario.setBancoBeneficiario(beneficiarioJoven.getBancoBeneficiario());
                beneficiario.setCodigoBeneficiario(beneficiarioJoven.getNumeroCuenta());
                beneficiario.setCodigoCliente(new BigDecimal(codCliente));
                beneficiario.setCuentaBeneficiario(beneficiarioJoven.getNumeroCuenta());
                beneficiario.setMoneda(beneficiarioJoven.getMonedaCuenta());
                beneficiario.setEstado(estadoLoteHistoricoDao.find(EstadoLoteHistorico.B03));
                beneficiario.setNroDocumentoIdentidad(beneficiarioJoven.getNumeroDocumento());
                beneficiario.setTipoDocumentoIdentidad(beneficiarioJoven.getTipoDocumento());
                beneficiario.setTipoPersona('N');
                if (TipoBeneficiario.CUENTA_BANCO.equals(beneficiarioJoven.getTipoBeneficiario())) {
                    beneficiario.setTipoPago(tipoPagoLoteHistoricoDao.find(1));
                } else if (TipoBeneficiario.CUENTA_OTRO_BANCO.equals(beneficiarioJoven.getTipoBeneficiario())) {
                    beneficiario.setTipoPago(tipoPagoLoteHistoricoDao.find(4));
                }
                Cuenta cuenta = cuentaDao.getByNumero(Long.parseLong(beneficiarioJoven.getNumeroCuenta()));
                if ("A".equals(cuenta.getTipoCta())) {
                    beneficiario.setTipoCuentaBeneficiario(TipoCuentaBeneficiario.CAJA_AHORRO);
                } else {
                    beneficiario.setTipoCuentaBeneficiario(TipoCuentaBeneficiario.CUENTA_CORRIENTE);
                }
                beneficiario.setNombreCorto(beneficiarioJoven.getPaterno() + " " + beneficiarioJoven.getNombre());
                beneficiario.setNombreCompleto(beneficiarioJoven.getPaterno() + " " + beneficiarioJoven.getMaterno() + " " + beneficiarioJoven.getNombre());
                beneficiario.setNombres(beneficiarioJoven.getNombre());
                beneficiario.setPaterno(beneficiarioJoven.getPaterno());
                beneficiario.setMaterno(beneficiarioJoven.getMaterno());
                beneficiario.setTipoCuentaOtrosBancos(TipoCuentaOtrosBancos.C);
                beneficiario.setSucursal(Integer.parseInt(beneficiarioJoven.getDepartamentoCuenta()));
                return beneficiarioDao.registrar(beneficiario, usuario);
            }
        }
        return beneficiario;
    }

    String enviarClaveMovil(String nroTelefono) throws
            ExecutionException, TimeoutException, IOException, SAXException, ImposibleLeerRespuestaException {
        RetornarClaveMovil result = claveMovilDao.claveMovil(TelefonoConverter.getCodigoInternacional(nroTelefono));
        LOGGER.debug(" clave movil -> {}", result);
        return result != null ? result.getEstado() : null;
    }

    void enviarSMS(String nroTelefono, String nroDocumento, String titulo, String mensaje, String usuario) {
        yellowPepperServicioDao.sendNotificacionYellowPepper(StringUtils.trimToEmpty(nroTelefono),
                titulo,
                StringUtils.trimToEmpty(mensaje),
                StringUtils.trimToEmpty(nroDocumento),
                StringUtils.trimToEmpty(usuario),
                APLICATION_SMS,
                SERVICIO_SJ,
                PRIORIDAD_DEFAULT);
    }

    String procesarGiroMovil(Giro giro, Cliente cliente, Cuenta cuentaOrigen, String canal) {
        GiroMovil giroMovil = new GiroMovil();
        //Beneficiario
        giroMovil.setCanal(canal);
        giroMovil.setDocIdentidadBeneficiario(giro.getDocIdentidadBeneficiario());
        giroMovil.setTipoIdentidadBeneficiario(giro.getTipoIdentidadBeneficiario());
        giroMovil.setNombreCompletoBeneficiario(giro.getNombreCompletoBeneficiario());
        giroMovil.setCelularBeneficiario(TelefonoConverter.getCodigoInternacional(giro.getCelularBeneficiario()));
        //Remitente
        giroMovil.setRemitente(cliente.getNombreCompleto());
        giroMovil.setCelularRemitente(TelefonoConverter.getCodigoInternacional(cliente.getTelefono()));
        //Datos de la transaccion
        giroMovil.setCuenta(cuentaOrigen.getNumeroCuenta());
        giroMovil.setTipoCuenta(cuentaOrigen.getTipoCta());
        giroMovil.setMonedaCuenta(cuentaOrigen.getCodigoMoneda());
        giroMovil.setImporte(giro.getImporte());
        giroMovil.setMoneda(giro.getMoneda());
        giroMovil.setMotivo(giro.getMotivo());
        EmitirGiroResponse emitirGiroResponse = giroMovilDao.procesarGiroMovil(giroMovil);
        LOGGER.debug("resultado giro movil {}", emitirGiroResponse);
        return emitirGiroResponse != null ? emitirGiroResponse.getEstado() : null;
    }

    String compraCredito(Recarga recarga, Cliente cliente, Cuenta cuentaOrigen) throws ExecutionException, TimeoutException, JAXBException, IOException {
        Servicio servicio = recarga.getServicio();
        Busqueda busqueda = new Busqueda();
        busqueda.setTipo(servicio.getTipo());
        busqueda.setCi(cliente.getDocumento());
        busqueda.setImporte(recarga.getImporte());
        busqueda.setMoneda(recarga.getMoneda());
        busqueda.setValor(String.valueOf(recarga.getCelularBeneficiario()));
        busqueda.setValor2(cliente.getDocumento());
        busqueda.setNit(recarga.getDocIdentidadFactura());
        busqueda.setRazonSocial(recarga.getNombreCompletoFactura());
        BigDecimal nroCliente = new BigDecimal(cliente.getCodCliente());
        ElementoBusqueda elementoBusqueda = new ElementoBusqueda();
        FavoritoPagoServicios fav = new FavoritoPagoServicios();
        ConsultaCuentasResponse consultaCuentasResponse = pagoServiciosDao.consultarCuentas(servicio, busqueda, elementoBusqueda, nroCliente, fav);
        LOGGER.info("consulta de cuenta -> {}", consultaCuentasResponse);
        if (consultaCuentasResponse != null) {
            ConsultaCuentasDeServiciosResponse consultaCuentasDeServiciosResponse = consultaCuentasResponse.getResponse();
            if (consultaCuentasDeServiciosResponse.getCuenta() != null && consultaCuentasDeServiciosResponse.getCuenta().size() == 1) {
                PagoDeudaServicio pagoDeudaServicio = new PagoDeudaServicio();
                pagoDeudaServicio.setBusqueda(busqueda);
                pagoDeudaServicio.setServicio(servicio);
                pagoDeudaServicio.setCuenta(consultaCuentasDeServiciosResponse.getCuenta().get(0));
                pagoDeudaServicio.setCuentaDebito(recarga.getCuenta());
                pagoDeudaServicio.setMonedaDebito(String.valueOf(cuentaOrigen.getCodigoMoneda()));
                pagoDeudaServicio.setMonedaServicio(String.valueOf(recarga.getMoneda()));
                pagoDeudaServicio.setTipoCuentaDebito(cuentaOrigen.getTipoCta());
                pagoDeudaServicio.setFactura(consultaCuentasDeServiciosResponse.getCuenta().get(0).getDeuda().getFactura().get(0));
                pagoDeudaServicio.setTipoEntrega(TipoEntrega.AGENCIA);
                //pagoDeudaServicio.setCiudad();
                //pagoDeudaServicio.setDepartamento();
                //pagoDeudaServicio.setDireccion();
                pagoDeudaServicio.setAgencia(String.valueOf(recarga.getAgenciaFactura()));
                FacturaSolicitud facturaSolicitud = new FacturaSolicitud();
                facturaSolicitud.setNombreFactura(recarga.getNombreCompletoFactura());
                facturaSolicitud.setNitciFactura(recarga.getDocIdentidadFactura());
                pagoDeudaServicio.setFacturaSolicitud(facturaSolicitud);
                pagoDeudaServicio.setNroCliente(cliente.getCodCliente());
                pagoDeudaServicio.setIdUsuario(cliente.getDocumento());
                pagoDeudaServicio.setFullName(cliente.getNombreCompleto());
                pagoDeudaServicio.setUserName(cliente.getDocumento());
                PagoDeudaDeServiciosResponse pagoDeudaDeServiciosResponse = pagoServiciosDao.pagarServicioConIntermediario(pagoDeudaServicio);
                LOGGER.info("recarga -> {}", pagoDeudaDeServiciosResponse);
            }
        }

        return null;
    }

    boolean validarPCC01(BigDecimal importe, String moneda) {
        return prevencionLavadoDao.necesitaFormularioPCC01(importe, Short.parseShort(moneda));
    }

}
