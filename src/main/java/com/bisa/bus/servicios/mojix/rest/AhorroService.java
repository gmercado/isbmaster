package com.bisa.bus.servicios.mojix.rest;

import com.bisa.bus.servicios.mojix.services.CuentaAhorroMojixService;
import com.bisa.bus.servicios.mojix.services.types.JsonRequestAhorro;
import com.bisa.bus.servicios.mojix.services.types.JsonRequestConfirmar;
import com.bisa.bus.servicios.mojix.services.types.JsonRequestCuotaAhorro;
import com.bisa.bus.servicios.mojix.services.types.JsonResultComun;
import com.google.inject.Inject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * @author by rsalvatierra on 22/06/2017.
 */
@Path("/sjoven/Ahorro")
@Produces({"application/json;charset=UTF-8"})
public class AhorroService extends CommonService {

    private static final String INI_AHORRO = "/iniAhorro";
    private static final String CONF_AHORRO = "/confAhorro";
    private static final String INI_CUOTA_AHORRO = "/iniCuotaAhorro";
    private static final String CONF_CUOTA_AHORRO = "/confCuotaAhorro";
    private static final String CUOTA_AHORRO = "/cuotaAhorro";

    @Inject
    private CuentaAhorroMojixService cuentaAhorroMojixService;

    /**
     * Permite solicitar token SMS para creacion de un ahorro
     *
     * @param httpHeaders   headers
     * @param requestAhorro datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(INI_AHORRO)
    public JsonResultComun iniciarCuentaAhorro(@Context HttpHeaders httpHeaders, JsonRequestAhorro requestAhorro) {
        if (requestAhorro == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return cuentaAhorroMojixService.crearCuentaAhorro(requestAhorro, getUser(httpHeaders));
        }
    }

    /**
     * Permite confirmar el token SMS para la creacion de un ahorro
     *
     * @param httpHeaders      headers
     * @param requestConfirmar datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(CONF_AHORRO)
    public JsonResultComun confirmarCuentaAhorro(@Context HttpHeaders httpHeaders, JsonRequestConfirmar requestConfirmar) {
        if (requestConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return cuentaAhorroMojixService.confirmarCuentaAhorro(requestConfirmar, getUser(httpHeaders));
        }
    }

    /**
     * Permite solicitar token SMS para pagar cuota ahorro
     *
     * @param httpHeaders        headers
     * @param requestCuotaAhorro datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(INI_CUOTA_AHORRO)
    public JsonResultComun iniciarCuotaAhorro(@Context HttpHeaders httpHeaders, JsonRequestCuotaAhorro requestCuotaAhorro) {
        if (requestCuotaAhorro == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return cuentaAhorroMojixService.iniciarCuotaAhorro(requestCuotaAhorro, getUser(httpHeaders));
        }
    }

    /**
     * Permite confirmar el token SMS para la creacion de un ahorro
     *
     * @param httpHeaders      headers
     * @param requestConfirmar datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(CONF_CUOTA_AHORRO)
    public JsonResultComun confirmarCuotaAhorro(@Context HttpHeaders httpHeaders, JsonRequestConfirmar requestConfirmar) {
        if (requestConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return cuentaAhorroMojixService.confirmarCuotaAhorro(requestConfirmar, getUser(httpHeaders));
        }
    }

    /**
     * Permite para la creacion de un ahorro sin solicitar token SMS
     *
     * @param httpHeaders        headers
     * @param requestCuotaAhorro datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(CUOTA_AHORRO)
    public JsonResultComun cuotaAhorro(@Context HttpHeaders httpHeaders, JsonRequestCuotaAhorro requestCuotaAhorro) {
        if (requestCuotaAhorro == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return cuentaAhorroMojixService.cuotaAhorro(requestCuotaAhorro, getUser(httpHeaders));
        }
    }
}
