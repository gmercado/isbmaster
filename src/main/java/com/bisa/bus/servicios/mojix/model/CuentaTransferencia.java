package com.bisa.bus.servicios.mojix.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by josanchez on 16/08/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CuentaTransferencia implements Serializable {

    private String transactionID;
    private String cuentaDestino;
    private String moneda;
    private BigDecimal monto;

    public CuentaTransferencia() {
    }

    public CuentaTransferencia(String transactionID, String cuentaDestino, String moneda, BigDecimal monto) {
        this.transactionID = transactionID;
        this.cuentaDestino = cuentaDestino;
        this.moneda = moneda;
        this.monto = monto;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getCuentaDestino() {
        return cuentaDestino;
    }

    public void setCuentaDestino(String cuentaDestino) {
        this.cuentaDestino = cuentaDestino;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    @Override
    public String toString() {
        return "CuentaTransferencia{" +
                "transactionID='" + transactionID + '\'' +
                ", cuentaDestino='" + cuentaDestino + '\'' +
                ", moneda='" + moneda + '\'' +
                ", monto=" + monto +
                '}';
    }
}
