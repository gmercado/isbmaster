package com.bisa.bus.servicios.mojix.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.mojix.entities.BeneficiarioJoven;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.LinkedList;

/**
 * @author by rsalvatierra on 03/07/2017.
 */
public class BeneficiarioJovenDao extends DaoImpl<BeneficiarioJoven, Long> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BeneficiarioJovenDao.class);

    @Inject
    protected BeneficiarioJovenDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    /**
     * Retorna Beneficiario a partir de la operacion
     *
     * @param pOperacion Codigo operacion
     * @return BeneficiarioJoven
     */
    public BeneficiarioJoven getByOperacion(Operacion pOperacion) {
        LOGGER.debug("Obteniendo registro por operacion:{}.", pOperacion);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<BeneficiarioJoven> q = cb.createQuery(BeneficiarioJoven.class);
                        Root<BeneficiarioJoven> p = q.from(BeneficiarioJoven.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("operacion"), pOperacion));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<BeneficiarioJoven> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public BeneficiarioJoven registrarBeneficiario(BeneficiarioJoven beneficiarioJoven, String usuario) {
        beneficiarioJoven.setUsuarioCreador(usuario);
        beneficiarioJoven.setFechaCreacion(new Date());
        return persist(beneficiarioJoven);
    }

    public BeneficiarioJoven actualizarBeneficiario(BeneficiarioJoven beneficiarioJoven, String usuario) {
        beneficiarioJoven.setUsuarioModificador(usuario);
        beneficiarioJoven.setFechaModificacion(new Date());
        return merge(beneficiarioJoven);
    }
}
