package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author by rsalvatierra on 18/11/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestConfirmarCuenta extends JsonRequestConfirmar implements Serializable, Request {

    private boolean eBisa;

    public JsonRequestConfirmarCuenta() {
    }

    public JsonRequestConfirmarCuenta(String confCode, String tokenSMS, String transactionID) {
        setConfCode(confCode);
        setTokenSMS(tokenSMS);
        setTransactionID(transactionID);
    }

    public boolean iseBisa() {
        return eBisa;
    }

    public void seteBisa(boolean eBisa) {
        this.eBisa = eBisa;
    }

    @Override
    public String toString() {
        return "JsonRequestConfirmar{" +
                "confCode='" + getConfCode() + '\'' +
                ", tokenSMS='" + getTokenSMS() + '\'' +
                ", transactionID='" + getTransactionID() + '\'' +
                '}';
    }

}
