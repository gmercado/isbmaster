package com.bisa.bus.servicios.mojix.services;

import com.bisa.bus.servicios.mojix.api.ProcesosMojix;
import com.bisa.bus.servicios.mojix.api.ResponseOperacion;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.bisa.bus.servicios.mojix.entities.Solicitud;
import com.bisa.bus.servicios.mojix.model.TipoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoSolicitud;
import com.bisa.bus.servicios.mojix.services.types.*;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.bisa.bus.servicios.mojix.model.TipoProceso.*;

/**
 * @author by rsalvatierra on 31/05/2017.
 */
public class ClienteMojixServiceImpl implements ClienteMojixService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClienteMojixServiceImpl.class);
    private final ProcesosMojix procesosMojix;

    @Inject
    public ClienteMojixServiceImpl(ProcesosMojix procesosMojix) {
        this.procesosMojix = procesosMojix;
    }

    @Override
    public JsonResultBisa iniciarCliente(JsonRequestBasic requestBasic, String usuario) {
        LOGGER.debug("Inicio <iniciarCliente>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateIniCliente(requestBasic);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.CLI, iniciarCliente, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CLIENTE, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta(responseOperacion.getResponse(), responseOperacion.isValido(), operacion, solicitud, usuario);
        //Enviar SMS
        procesosMojix.enviarSMS(responseOperacion.getCliente(), solicitud, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <iniciarCliente>");
        return responseOperacion.getResultBisa();
    }

    @Override
    public JsonResultClienteBisa confirmarCliente(JsonRequestConfirmar request, String usuario) {
        LOGGER.debug("Inicio <confirmarCliente>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConfIniCliente(request);
        //Confirmar Operacion original
        Operacion operacion = procesosMojix.setConfirmarOperacion(request, responseOperacion.isValido(), usuario);
        //Confirmar Solicitud orignal
        procesosMojix.setConfirmarSolicitud(responseOperacion.getSolicitud(), responseOperacion.isValido(), usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONFCLIENTE, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta2(responseOperacion.getResponse(), responseOperacion.isValido(), solicitud, usuario);
        LOGGER.debug("Fin <confirmarCliente>");
        return responseOperacion.getResultClienteBisa();
    }

    @Override
    public JsonResultComun iniciarContacto(JsonRequestContacto request, String usuario) {
        LOGGER.debug("Inicio <iniciarContacto>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateIniContacto(request);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.CLI, iniciarContacto, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONTACTO, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta(responseOperacion.getResponse(), responseOperacion.isValido(), operacion, solicitud, usuario);
        //Enviar SMS
        procesosMojix.enviarSMS(responseOperacion.getCliente(), solicitud, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <iniciarContacto>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun confirmarContacto(JsonRequestConfirmar request, String usuario) {
        LOGGER.debug("Inicio <confirmarContacto>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConfirmar(request);
        //Confirmar Operacion
        Operacion operacion = procesosMojix.setConfirmarOperacion(request, responseOperacion.isValido(), usuario);
        //Confirmar Solicitud
        procesosMojix.setConfirmarSolicitud(responseOperacion.getSolicitud(), responseOperacion.isValido(), usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONFCONTACTO, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta2(responseOperacion.getResponse(), responseOperacion.isValido(), solicitud, usuario);
        LOGGER.debug("Fin <confirmarContacto>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultCuentaBanco iniCuentaBancoBisa(JsonRequestCuentaBanco request, String usuario) {
        LOGGER.debug("Inicio <iniciarCuenta>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateIniCuenta(request);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.CLI, iniciarCuenta, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CUENTA, usuario);
        //Registrar Beneficiario
        procesosMojix.setBeneficiario(responseOperacion.getBeneficiarioJoven(), operacion, responseOperacion.isValido(), usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta(responseOperacion.getResponse(), responseOperacion.isValido(), operacion, solicitud, usuario);
        //Enviar SMS
        procesosMojix.enviarSMS(responseOperacion.getCliente(), solicitud, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <iniciarCuenta>");
        return responseOperacion.getResultCuentaBanco();
    }

    @Override
    public JsonResultComun confirmarCuentaBancoBisa(JsonRequestConfirmarCuenta request, String usuario) {
        LOGGER.debug("Inicio <confirmarCuenta>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConfirmar(request);
        //Confirmar Operacion
        Operacion operacion = procesosMojix.setConfirmarOperacion(responseOperacion.getRequest(), responseOperacion.isValido(), usuario);
        //Confirmar Solicitud
        procesosMojix.setConfirmarSolicitud(responseOperacion.getSolicitud(), responseOperacion.isValido(), usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONFCUENTA, usuario);
        //Actualizar Beneficiario
        procesosMojix.actualizarBeneficiario(operacion, request.iseBisa(), responseOperacion.isValido(), usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta2(responseOperacion.getResponse(), responseOperacion.isValido(), solicitud, usuario);
        LOGGER.debug("Fin <confirmarCuenta>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultClienteBisa adicionaCliente(JsonRequestBasic requestBasic, String usuario) {
        LOGGER.debug("Inicio <adicionaCliente>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateAddCliente(requestBasic, usuario);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.CLI, adicionarCliente, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_ADDCLIENTE, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuestaInOne(responseOperacion.isValido(), responseOperacion.getResponse(), operacion, solicitud, usuario);
        LOGGER.debug("Fin <adicionaCliente>");
        return responseOperacion.getResultClienteBisa();
    }

    @Override
    public JsonResultComun iniCuentaOtroBanco(JsonRequestCuentaOtroBanco requestCuentaOtroBanco, String usuario) {
        LOGGER.debug("Inicio <iniCuentaOtroBanco>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateIniCuentaOtroBanco(requestCuentaOtroBanco);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.CLI, iniCuentaOtroBanco, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CUENTAOTROBANCO, usuario);
        //Registrar Beneficiario
        procesosMojix.setBeneficiario(responseOperacion.getBeneficiarioJoven(), operacion, responseOperacion.isValido(), usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta(responseOperacion.getResponse(), responseOperacion.isValido(), operacion, solicitud, usuario);
        //Enviar SMS
        procesosMojix.enviarSMS(responseOperacion.getCliente(), solicitud, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <iniCuentaOtroBanco>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun confirmarCuentaOtroBanco(JsonRequestConfirmar requestConfirmar, String usuario) {
        LOGGER.debug("Inicio <confirmarCuentaOtroBanco>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConfirmar(requestConfirmar);
        //Confirmar Operacion
        Operacion operacion = procesosMojix.setConfirmarOperacion(requestConfirmar, responseOperacion.isValido(), usuario);
        //Confirmar Solicitud
        procesosMojix.setConfirmarSolicitud(responseOperacion.getSolicitud(), responseOperacion.isValido(), usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONFCUENTAOTROBANCO, usuario);
        //Actualizar Beneficiario
        procesosMojix.actualizarBeneficiario(operacion, responseOperacion.isValido(), usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta2(responseOperacion.getResponse(), responseOperacion.isValido(), solicitud, usuario);
        LOGGER.debug("Fin <confirmarCuentaOtroBanco>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun iniCuentaGiroMovil(JsonRequestCuentaGiroMovil requestCuentaGiroMovil, String usuario) {
        LOGGER.debug("Inicio <iniCuentaGiroMovil>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateIniCuentaGiroMovil(requestCuentaGiroMovil);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.CLI, iniCuentaGiroMovil, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CUENTAGIROMOVIL, usuario);
        //Registrar Beneficiario
        procesosMojix.setBeneficiario(responseOperacion.getBeneficiarioJoven(), operacion, responseOperacion.isValido(), usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta(responseOperacion.getResponse(), responseOperacion.isValido(), operacion, solicitud, usuario);
        //Enviar SMS
        procesosMojix.enviarSMS(responseOperacion.getCliente(), solicitud, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <iniCuentaGiroMovil>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun confirmarCuentaGiroMovil(JsonRequestConfirmar requestConfirmar, String usuario) {
        LOGGER.debug("Inicio <confirmarCuentaGiroMovil>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConfirmar(requestConfirmar);
        //Confirmar Operacion
        Operacion operacion = procesosMojix.setConfirmarOperacion(requestConfirmar, responseOperacion.isValido(), usuario);
        //Confirmar Solicitud
        procesosMojix.setConfirmarSolicitud(responseOperacion.getSolicitud(), responseOperacion.isValido(), usuario);
        //Actualizar Beneficiario
        procesosMojix.actualizarBeneficiario(operacion, false, responseOperacion.isValido(), usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONFCUENTAGIROMOVIL, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta2(responseOperacion.getResponse(), responseOperacion.isValido(), solicitud, usuario);
        LOGGER.debug("Fin <confirmarCuentaGiroMovil>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun enviarSMS(JsonRequestInvitacion requestInvitacion, String usuario) {
        LOGGER.debug("Inicio <enviarSMS>");
        requestInvitacion.setTransactionID(String.valueOf(procesosMojix.getTime()));
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateEnviarSMS(requestInvitacion);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.CLI, enviarSMS, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_ENVIARSMS, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuestaInOne(responseOperacion.isValido(), responseOperacion.getResponse(), operacion, solicitud, usuario);
        //Enviar Varios SMS
        procesosMojix.enviarLoteSMS(requestInvitacion.getContactos(), responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <enviarSMS>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun enviarClaveMovil(JsonRequestClaveMovil requestClaveMovil, String usuario) {
        LOGGER.debug("Inicio <enviarClaveMovil>");
        requestClaveMovil.setTransactionID(String.valueOf(procesosMojix.getTime()));
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateEnviarClaveMovil(requestClaveMovil);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.CLI, enviarClaveMovil, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_ENVIARCLAVEMOVIL, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuestaInOne(responseOperacion.isValido(), responseOperacion.getResponse(), operacion, solicitud, usuario);
        LOGGER.debug("Fin <enviarClaveMovil>");
        return responseOperacion.getResultContactoBisa();
    }
}
