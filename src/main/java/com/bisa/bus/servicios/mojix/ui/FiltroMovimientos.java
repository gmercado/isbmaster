package com.bisa.bus.servicios.mojix.ui;

import bus.database.components.FiltroPanel;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.bisa.bus.servicios.mojix.model.EstadoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoProceso;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author by rsalvatierra on 05/07/2017.
 */
class FiltroMovimientos extends FiltroPanel<Operacion> {

    FiltroMovimientos(String id, IModel<Operacion> model1, IModel<Operacion> model2) {
        super(id, model1, model2);
        final String datePattern = "dd/MM/yyyy";
        DateTextField fechaDesde = new DateTextField("fechaDesde", new PropertyModel<>(model1, "fechaCreacion"), datePattern);
        fechaDesde.setRequired(true);
        fechaDesde.add(new DatePicker());

        DateTextField fechaHasta = new DateTextField("fechaHasta", new PropertyModel<>(model2, "fechaCreacion"), datePattern);
        fechaHasta.setRequired(true);
        fechaHasta.add(new DatePicker());

        add(new TextField<>("cliente.codigoCliente", new PropertyModel<>(model1, "cliente.codigoCliente")));
        add(new TextField<>("transaccionID", new PropertyModel<>(model1, "transaccionID")));
        add(fechaDesde);
        add(fechaHasta);
        //Tipo Proceso
        add(new DropDownChoice<>("proceso", new PropertyModel<>(model1, "proceso"),
                new LoadableDetachableModel<List<? extends TipoProceso>>() {
                    @Override
                    protected List<? extends TipoProceso> load() {
                        return new ArrayList<>(Arrays.asList((TipoProceso.values())));
                    }
                }, new IChoiceRenderer<TipoProceso>() {
            @Override
            public Object getDisplayValue(TipoProceso object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(TipoProceso object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoProceso getObject(String id, IModel<? extends List<? extends TipoProceso>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));
        //Tipo Operacion
        add(new DropDownChoice<>("tipoOperacion", new PropertyModel<>(model1, "tipoOperacion"),
                new LoadableDetachableModel<List<? extends TipoOperacion>>() {
                    @Override
                    protected List<? extends TipoOperacion> load() {
                        return new ArrayList<>(Arrays.asList((TipoOperacion.values())));
                    }
                }, new IChoiceRenderer<TipoOperacion>() {
            @Override
            public Object getDisplayValue(TipoOperacion object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(TipoOperacion object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoOperacion getObject(String id, IModel<? extends List<? extends TipoOperacion>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));
        //Estado
        add(new DropDownChoice<>("estadoOperacion", new PropertyModel<>(model1, "estadoOperacion"),
                new LoadableDetachableModel<List<? extends EstadoOperacion>>() {
                    @Override
                    protected List<? extends EstadoOperacion> load() {
                        return new ArrayList<>(Arrays.asList((EstadoOperacion.values())));
                    }
                }, new IChoiceRenderer<EstadoOperacion>() {
            @Override
            public Object getDisplayValue(EstadoOperacion object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(EstadoOperacion object, int index) {
                return Integer.toString(index);
            }

            @Override
            public EstadoOperacion getObject(String id, IModel<? extends List<? extends EstadoOperacion>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));
    }
}

