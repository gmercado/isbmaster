package com.bisa.bus.servicios.mojix.model;

/**
 * @author by josanchez on 12/07/2016.
 */
public enum TipoSolicitud {

    TRANS_CLIENTE(1, "IniCliente"),
    TRANS_CONFCLIENTE(2, "ConfCliente"),
    TRANS_CONTACTO(3, "IniContacto"),
    TRANS_CONFCONTACTO(4, "ConfContacto"),
    TRANS_ADDCLIENTE(5, "AddCliente"),

    TRANS_CUENTA(10, "IniCuenta"),
    TRANS_CONFCUENTA(11, "ConfCuenta"),
    TRANS_CUENTAOTROBANCO(12, "IniCuentaOtroBanco"),
    TRANS_CONFCUENTAOTROBANCO(13, "ConfContacto"),
    TRANS_CUENTAGIROMOVIL(14, "IniCuentaGiroMovil"),
    TRANS_CONFCUENTAGIROMOVIL(15, "ConfContacto"),

    TRANS_CONSULTA(20, "Consulta"),
    TRANS_CONSULTACUENTA(21, "ConCuenta"),
    TRANS_CONSULTATELEFONICAS(22, "ConTelefonicas"),
    TRANS_CONSULTAAGENCIAS(23, "ConAgencias"),
    TRANS_CONSULTABANCOS(24, "ConBancos"),

    TRANS_ENVIARSMS(30, "EnviarSMS"),
    TRANS_ENVIARCLAVEMOVIL(31, "EnviarClaveMovil"),

    TRANS_GIROMOVIL(40, "GiroMovil"),
    TRANS_CONFGIROMOVIL(41, "ConfGiroMovil"),

    TRANS_TRANSFERENCIA(50, "IniTransferencia"),
    TRANS_CONFTRANSFERENCIA(51, "ConfTransferencia"),
    TRANS_PROGTRANSFERENCIA(52, "IniTransferenciaPRG"),
    TRANS_CONFTRANSFERENCIAPROGRAMADA(53, "ConfTransferenciaPRG"),
    TRANS_TRANSFERENCIAPROGRAMADA(54, "TransferenciaPRG"),
    TRANS_TRANSFERENCIABULK(55, "IniTransferenciaBULK"),
    TRANS_CONFTRANSFERENCIABULK(56, "ConfTransferenciaBULK"),

    TRANS_TRANSFERENCIAACH(60, "IniTransferencia"),
    TRANS_CONFTRANSFERENCIAACH(61, "ConfTransferencia"),
    TRANS_PROGTRANSFERENCIAACH(62, "IniTransferenciaPRG"),
    TRANS_CONFTRANSFERENCIAPROGRAMADAACH(63, "ConfTransferenciaPRG"),
    TRANS_TRANSFERENCIAPROGRAMADAACH(64, "TransferenciaPRG"),

    TRANS_RECARGAMOVIL(70, "RecargaMovil"),
    TRANS_CONFRECARGAMOVIL(71, "ConfRecargaMovil");

    private int tipo;
    private String transaccion;

    TipoSolicitud(int tipo, String transaccion) {
        this.setTipo(tipo);
        this.setTransaccion(transaccion);
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(String transaccion) {
        this.transaccion = transaccion;
    }
}

