package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestContacto implements Serializable, Request {
    private String bisaClienteID;
    private String nroContactos;
    private String transactionID;

    public JsonRequestContacto() {
    }

    public JsonRequestContacto(String bisaClienteID, String nroContactos, String transactionID) {
        setBisaClienteID(bisaClienteID);
        setNroContactos(nroContactos);
        setTransactionID(transactionID);
    }

    public String getBisaClienteID() {
        return bisaClienteID;
    }

    public void setBisaClienteID(String bisaClienteID) {
        this.bisaClienteID = bisaClienteID;
    }

    public String getNroContactos() {
        return nroContactos;
    }

    public void setNroContactos(String nroContactos) {
        this.nroContactos = nroContactos;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    @Override
    public String toString() {
        return "JsonRequestContacto{" +
                "bisaClienteID='" + bisaClienteID + '\'' +
                ", nroContactos='" + nroContactos + '\'' +
                ", transactionID='" + transactionID + '\'' +
                '}';
    }
}
