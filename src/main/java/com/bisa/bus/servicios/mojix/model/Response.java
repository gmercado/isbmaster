package com.bisa.bus.servicios.mojix.model;

/**
 * @author by rsalvatierra on 02/12/2016.
 */
public interface Response {

    String getStatusCode();

    String getMensaje();

    void setConfCode(String confCode);

    String toString();
}
