package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.CodigosConfirmacion;
import com.bisa.bus.servicios.mojix.model.EmpresaTelefonicaMojix;
import com.bisa.bus.servicios.mojix.model.Response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonResultInfoTelefonicas implements Serializable, Response {

    private String statusCode;
    private String mensaje;
    private List<EmpresaTelefonicaMojix> empresas;

    public JsonResultInfoTelefonicas(String statusCode, CodigosConfirmacion codConf, String mensaje) {
        setStatusCode(statusCode);
        setMensaje(mensaje);
    }

    public JsonResultInfoTelefonicas(String statusCode, String mensaje, List<EmpresaTelefonicaMojix> empresas) {
        setStatusCode(statusCode);
        setMensaje(mensaje);
        setEmpresas(empresas);
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public void setConfCode(String confCode) {

    }

    public List<EmpresaTelefonicaMojix> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<EmpresaTelefonicaMojix> empresas) {
        this.empresas = empresas;
    }

    @Override
    public String toString() {
        return "JsonResultInfoTelefonicas{" +
                "statusCode='" + statusCode + '\'' +
                ", mensaje='" + mensaje + '\'' +
                ", empresas=" + empresas +
                '}';
    }
}
