package com.bisa.bus.servicios.mojix.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author by josanchez on 16/08/2016.
 * @author rsalvatierra modificado on 20/10/2016
 */
@Entity
@Table(name = "SJOV03")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S03FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S03USRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S03FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S03USRMOD"))
})
public class Transferencia extends EntityBase implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "S03MONTOS", columnDefinition = "numeric(10,2)")
    public BigDecimal monto;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S03CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "S03CODOPE")
    private Operacion operacion;
    @Column(name = "S03ESTADO", columnDefinition = "VARCHAR(2)")
    private String estadoTransferencia;
    @Column(name = "S03CTAORI", columnDefinition = "VARCHAR(10)")
    private String cuentaOrigen;
    @Column(name = "S03CTADES", columnDefinition = "VARCHAR(10)")
    private String cuentaDestino;
    @Column(name = "S03MONEDA", columnDefinition = "NUMERIC(2)")
    private Short moneda;
    @Column(name = "S03MOTIVO", columnDefinition = "VARCHAR(100)")
    private String motivo;

    @Column(name = "S03NUMCAJ", columnDefinition = "NUMERIC(10)")
    private Long nroCaja;

    @Column(name = "S03NUMSEQ", columnDefinition = "NUMERIC(10)")
    private Long nroSecuencia;

    @Column(name = "S03NROCON", columnDefinition = "VARCHAR(40)")
    private String numeroRef;

    @Column(name = "S03PERIOD", columnDefinition = "varchar(15)")
    private String periodicidad;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "S03EJECCI", columnDefinition = "TIMESTAMP")
    private Date ejecucion;

    @Column(name = "S03NUMEIP", columnDefinition = "VARCHAR(15)")
    private String ip;

    @Column(name = "S03CODPAI", columnDefinition = "VARCHAR(3)")
    private String pais;

    @Column(name = "S03CIUDAD", columnDefinition = "VARCHAR(3)")
    private String ciudad;

    @Column(name = "S03LATITU", columnDefinition = "numeric(10,6)")
    private BigDecimal latitud;

    @Column(name = "S03LONGIT", columnDefinition = "numeric(10,6)")
    private BigDecimal longitud;


    public Transferencia() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }

    public String getEstadoTransferencia() {
        return estadoTransferencia;
    }

    public void setEstadoTransferencia(String estadoTransferencia) {
        this.estadoTransferencia = estadoTransferencia;
    }

    public String getCuentaOrigen() {
        return cuentaOrigen;
    }

    public void setCuentaOrigen(String cuentaOrigen) {
        this.cuentaOrigen = cuentaOrigen;
    }

    public String getCuentaDestino() {
        return cuentaDestino;
    }

    public void setCuentaDestino(String cuentaDestino) {
        this.cuentaDestino = cuentaDestino;
    }

    public Short getMoneda() {
        return moneda;
    }

    public void setMoneda(Short moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Long getNroCaja() {
        return nroCaja;
    }

    public void setNroCaja(Long nroCaja) {
        this.nroCaja = nroCaja;
    }

    public Long getNroSecuencia() {
        return nroSecuencia;
    }

    public void setNroSecuencia(Long nroSecuencia) {
        this.nroSecuencia = nroSecuencia;
    }

    public String getNumeroRef() {
        return numeroRef;
    }

    public void setNumeroRef(String numeroContacto) {
        this.numeroRef = numeroContacto;
    }

    public String getPeriodicidad() {
        return periodicidad;
    }

    public void setPeriodicidad(String periodicidad) {
        this.periodicidad = periodicidad;
    }

    public Date getEjecucion() {
        return ejecucion;
    }

    public void setEjecucion(Date ejecucion) {
        this.ejecucion = ejecucion;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public BigDecimal getLatitud() {
        return latitud;
    }

    public void setLatitud(BigDecimal latitud) {
        this.latitud = latitud;
    }

    public BigDecimal getLongitud() {
        return longitud;
    }

    public void setLongitud(BigDecimal longitud) {
        this.longitud = longitud;
    }

    @Override
    public String toString() {
        return "Transferencia{" +
                "monto=" + monto +
                ", id=" + id +
                ", operacion=" + operacion +
                ", estadoTransferencia='" + estadoTransferencia + '\'' +
                ", cuentaOrigen='" + cuentaOrigen + '\'' +
                ", cuentaDestino='" + cuentaDestino + '\'' +
                ", moneda=" + moneda +
                ", motivo='" + motivo + '\'' +
                ", nroCaja=" + nroCaja +
                ", nroSecuencia=" + nroSecuencia +
                ", numeroRef='" + numeroRef + '\'' +
                ", periodicidad='" + periodicidad + '\'' +
                ", ejecucion=" + ejecucion +
                ", ip='" + ip + '\'' +
                ", pais='" + pais + '\'' +
                ", ciudad='" + ciudad + '\'' +
                ", latitud=" + latitud +
                ", longitud=" + longitud +
                '}';
    }
}
