package com.bisa.bus.servicios.mojix.model;

/**
 * @author by rsalvatierra on 06/12/2016.
 */
public enum EstadoTransferencia {
    PENDIENTE("PE", "PENDIENTE"),
    CONFIRMADA("PR", "CONFIRMADO"),
    AGENDADA("AG", "AGENDADO"),
    AGENDADA_PROCESADA("AP", "AGENDADO_PROCESADO"),
    BULK_PROCESADA("BP", "BULK_PROCESADO"),
    ANULADA("AN", "ANULADO"),
    ERROR("ER", "ERROR");

    private String codigo;
    private String descripcion;

    EstadoTransferencia(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
