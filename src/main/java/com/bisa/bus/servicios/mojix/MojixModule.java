package com.bisa.bus.servicios.mojix;

import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.bus.servicios.mojix.sched.SchedOperacionesMojix;
import com.bisa.bus.servicios.mojix.services.*;

/**
 * @author by josanchez on 13/07/2016.
 */
public class MojixModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(ClienteMojixService.class).to(ClienteMojixServiceImpl.class);
        bind(ConsultasMojixService.class).to(ConsultasMojixServiceImpl.class);
        bind(TransaccionesMojixService.class).to(TransaccionesMojixServiceImpl.class);
        bind(RecargasCelularMojixService.class).to(RecargasCelularMojixServiceImpl.class);
        bind(CuentaAhorroMojixService.class).to(CuentaAhorroMojixServiceImpl.class);
        bindSched("CaducarTokens", "BancaJoven", "0 * * * * ?", SchedOperacionesMojix.class);
        bindUI("com/bisa/bus/servicios/mojix/ui");
    }
}
