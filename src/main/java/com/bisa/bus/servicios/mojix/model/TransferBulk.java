package com.bisa.bus.servicios.mojix.model;

import java.util.List;

/**
 * @author by rsalvatierra on 02/06/2017.
 */
public interface TransferBulk {
    String getCuentaOrigen();

    List<CuentaTransferencia> getCuentas();

    String getMotivo();

    Geografica getGeoProperties();
}
