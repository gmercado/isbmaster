package com.bisa.bus.servicios.mojix.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author by josanchez on 14/07/2016.
 * @author rsalvatierra modificado on 20/10/2016
 */
@Entity
@Table(name = "SJOV04")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S04FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S04USRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S04FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S04USRMOD"))
})
public class Masivo extends EntityBase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S02CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "S04CODBLK")
    private Operacion operacionBulk;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "S04CODOPE")
    private Operacion operacion;

    public Masivo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Operacion getOperacionBulk() {
        return operacionBulk;
    }

    public void setOperacionBulk(Operacion operacionBulk) {
        this.operacionBulk = operacionBulk;
    }

    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }

    @Override
    public String toString() {
        return "ClienteEnmascaradoMojix{" +
                "id=" + getId() +
                ", codigoBulk=" + getOperacionBulk() +
                ", codigoOperacion=" + getOperacion() +
                '}';
    }
}
