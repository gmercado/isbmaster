package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestCuentaBanco implements Serializable, Request {
    private String bisaClienteID;
    private String nroCuenta;
    private String telefono;
    private String transactionID;

    public JsonRequestCuentaBanco() {
    }

    public JsonRequestCuentaBanco(String bisaClienteID, String nroCuenta, String telefono, String transactionID) {
        setBisaClienteID(bisaClienteID);
        setNroCuenta(nroCuenta);
        setTelefono(telefono);
        setTransactionID(transactionID);
    }

    public String getBisaClienteID() {
        return bisaClienteID;
    }

    public void setBisaClienteID(String bisaClienteID) {
        this.bisaClienteID = bisaClienteID;
    }

    public String getNroCuenta() {
        return nroCuenta;
    }

    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    @Override
    public String toString() {
        return "JsonRequestCuentaBanco{" +
                "bisaClienteID='" + bisaClienteID + '\'' +
                ", nroCuenta='" + nroCuenta + '\'' +
                ", telefono='" + telefono + '\'' +
                ", transactionID='" + transactionID + '\'' +
                '}';
    }
}
