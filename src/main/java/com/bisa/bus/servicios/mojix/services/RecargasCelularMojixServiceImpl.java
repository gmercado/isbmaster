package com.bisa.bus.servicios.mojix.services;

import com.bisa.bus.servicios.mojix.api.ProcesosMojix;
import com.bisa.bus.servicios.mojix.api.ResponseOperacion;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.bisa.bus.servicios.mojix.entities.Solicitud;
import com.bisa.bus.servicios.mojix.model.TipoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoSolicitud;
import com.bisa.bus.servicios.mojix.services.types.JsonRequestConfirmar;
import com.bisa.bus.servicios.mojix.services.types.JsonRequestRecarga;
import com.bisa.bus.servicios.mojix.services.types.JsonResultComun;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.bisa.bus.servicios.mojix.model.TipoProceso.recargaMovil;

/**
 * @author by rsalvatierra on 22/06/2017.
 */
public class RecargasCelularMojixServiceImpl implements RecargasCelularMojixService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecargasCelularMojixServiceImpl.class);
    private final ProcesosMojix procesosMojix;

    @Inject
    public RecargasCelularMojixServiceImpl(ProcesosMojix procesosMojix) {
        this.procesosMojix = procesosMojix;
    }

    @Override
    public JsonResultComun iniciarRecarga(JsonRequestRecarga requestRecarga, String usuario) {
        LOGGER.debug("Inicio <iniciarRecarga>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateIniRecargaMovil(requestRecarga);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.RCM, recargaMovil, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_RECARGAMOVIL, usuario);
        //Registrar Giro Movil
        procesosMojix.setRecarga(requestRecarga, operacion, responseOperacion.isValido(), usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta(responseOperacion.getResponse(), responseOperacion.isValido(), operacion, solicitud, usuario);
        //Enviar SMS
        procesosMojix.enviarSMS(responseOperacion.getCliente(), solicitud, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <iniciarRecarga>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun confirmarRecarga(JsonRequestConfirmar requestConfirmar, String usuario) {
        LOGGER.debug("Inicio <confirmarRecarga>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConfIniRecargaMovil(requestConfirmar);
        //Confirmar Operacion
        Operacion operacion = procesosMojix.setConfirmarOperacion(responseOperacion.getRequest(), responseOperacion.isValido(), usuario);
        //Confirmar Solicitud
        procesosMojix.setConfirmarSolicitud(responseOperacion.getSolicitud(), responseOperacion.isValido(), usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONFRECARGAMOVIL, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta2(responseOperacion.getResponse(), responseOperacion.isValido(), solicitud, usuario);
        //Actualizar giro movil
        //procesosMojix.actualizarTransferencia(responseOperacion.getTransferencia(), responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <confirmarRecarga>");
        return responseOperacion.getResultContactoBisa();
    }
}
