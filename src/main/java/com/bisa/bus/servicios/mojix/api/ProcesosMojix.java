package com.bisa.bus.servicios.mojix.api;

import bus.env.api.MedioAmbiente;
import bus.interfaces.as400.dao.ClienteCuentaDetalleDao;
import bus.interfaces.as400.dao.ClienteDao;
import bus.interfaces.as400.dao.CuentaDao;
import bus.interfaces.as400.entities.Cliente;
import bus.interfaces.as400.entities.ClienteCuentaDetalle;
import bus.interfaces.as400.entities.Cuenta;
import bus.interfaces.token.GeneradorPin;
import bus.interfaces.token.ParPines;
import bus.interfaces.token.TresClavesUtils;
import bus.monitor.api.IRespuestasMonitor;
import bus.plumbing.utils.Convert;
import bus.plumbing.utils.Monedas;
import com.bisa.bus.servicios.mojix.dao.*;
import com.bisa.bus.servicios.mojix.entities.*;
import com.bisa.bus.servicios.mojix.exception.OperacionesMojixException;
import com.bisa.bus.servicios.mojix.model.*;
import com.bisa.bus.servicios.mojix.services.types.*;
import com.bisa.bus.servicios.segip.dao.ClienteNaturalDao;
import com.bisa.bus.servicios.segip.entities.ClienteNatural;
import com.google.common.base.Optional;
import com.google.inject.Inject;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static bus.env.api.Variables.*;
import static bus.monitor.api.ParametersMonitor.*;
import static com.bisa.bus.servicios.mojix.api.ValidaCampos.*;
import static com.bisa.bus.servicios.mojix.model.CodigosConfirmacion.ERROR;
import static com.bisa.bus.servicios.mojix.model.MensajesMojix.*;
import static com.bisa.bus.servicios.mojix.model.MessageKeyword.*;
import static java.lang.Integer.parseInt;

/**
 * @author by josanchez on 13/07/2016.
 * @author rsalvatierra modificado on 20/10/2016
 */
public class ProcesosMojix {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcesosMojix.class);

    private static final String SHA1 = "SHA1";
    private static final String UTF_8 = "UTF-8";
    private static final String TITULO_SMS = "MOJIX_NOTIFICACION_TOKEN";
    private static final String USED = "T";
    private static final String NUSED = "F";
    private static final String PROCESADA = "P";
    private static final String ENABLE = "enable";
    private static final Long SALT_5L = 5L;
    private static final String DEFAULT_FONOS_PERMITIDOS = "*";
    private static final long BANCO_BISA = 1009L;

    private final MedioAmbiente medioAmbiente;
    private final OperacionDao operacionDao;
    private final SolicitudDao solicitudDao;
    private final ClienteNaturalDao clienteNaturalDao;
    private final ClienteJovenDao clienteJovenDao;
    private final ClienteDao clienteDao;
    private final ClienteCuentaDetalleDao clienteCuentaDetalleDao;
    private final TransaccionMonitorMojix transaccionMonitorMojix;
    private final TransferenciaDao transferenciaDao;
    private final RecargaDao recargaDao;
    private final BeneficiarioJovenDao beneficiarioJovenDao;
    private final CuentaDao cuentaDao;
    private final GiroDao giroDao;
    private final OperacionesMojix operacionesMojix;

    @Inject
    public ProcesosMojix(MedioAmbiente medioAmbiente, TransaccionMonitorMojix transaccionMonitorMojix,
                         ClienteJovenDao clienteJovenDao, OperacionDao operacionDao, SolicitudDao solicitudDao,
                         ClienteNaturalDao clienteNaturalDao, ClienteDao clienteDao, ClienteCuentaDetalleDao clienteCuentaDetalleDao,
                         TransferenciaDao transferenciaDao, RecargaDao recargaDao, BeneficiarioJovenDao beneficiarioJovenDao,
                         CuentaDao cuentaDao, GiroDao giroDao, OperacionesMojix operacionesMojix) {
        this.medioAmbiente = medioAmbiente;
        this.transaccionMonitorMojix = transaccionMonitorMojix;
        this.operacionDao = operacionDao;
        this.solicitudDao = solicitudDao;
        this.clienteNaturalDao = clienteNaturalDao;
        this.clienteJovenDao = clienteJovenDao;
        this.clienteDao = clienteDao;
        this.clienteCuentaDetalleDao = clienteCuentaDetalleDao;
        this.transferenciaDao = transferenciaDao;
        this.recargaDao = recargaDao;
        this.beneficiarioJovenDao = beneficiarioJovenDao;
        this.cuentaDao = cuentaDao;
        this.giroDao = giroDao;
        this.operacionesMojix = operacionesMojix;
    }

    private static String replaceKeywords(String msg, Map<String, Object> params) {
        for (String key : params.keySet()) {
            msg = msg.replace("$" + key, toText(params.get(key)));
        }
        msg = msg.replace("$SECONDS_UNTIL_NEXT_SEND", "" + TimeUnit.MILLISECONDS.toSeconds(Optional.fromNullable((Long) params.get("MILLIS_REMAINING_UNTIL_NEXT_ATTEMPT")).or(0L)));
        msg = msg.replace("$MINUTES_UNTIL_NEXT_SEND", "" + TimeUnit.MILLISECONDS.toSeconds(Optional.fromNullable((Long) params.get("MILLIS_REMAINING_UNTIL_NEXT_ATTEMPT")).or(0L)) / 60);
        return msg;
    }

    private static String toText(Object object) {
        if (object == null)
            return "";
        if (BigDecimal.class.isAssignableFrom(object.getClass()))
            return ((BigDecimal) object).toPlainString();

        return String.valueOf(object);
    }

    private String encriptarID(String password, Long salt) {
        try {
            MessageDigest md = MessageDigest.getInstance(SHA1);
            md.update(password.getBytes(UTF_8));
            md.update(new BigInteger(salt.toString()).toByteArray());
            return String.valueOf(Hex.encodeHex(md.digest()));
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
    }

    private boolean cuentaSinFondo(String errorMonitor) {
        String[] codigos = StringUtils.splitPreserveAllTokens(medioAmbiente.getValorDe(MONITOR_CODIGO_SINFONDO, MONITOR_CODIGO_SINFONDO_DEFUALT), ":", 0);
        if (codigos != null) {
            for (String s : codigos) {
                if (s.equalsIgnoreCase(errorMonitor)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean errorPCC01(String errorMonitor) {
        String[] codigos = StringUtils.splitPreserveAllTokens(medioAmbiente.getValorDe(MONITOR_CODIGO_PCC01, MONITOR_CODIGO_PCC01_DEFUALT), ":", 0);
        if (codigos != null) {
            for (String s : codigos) {
                if (s.equalsIgnoreCase(errorMonitor)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void enviarSMS(Cliente cliente, Solicitud solicitud, boolean isValido, String usuario) {
        if (isValido && cliente != null) {
            enviarSMS(cliente, getMsgSMS(solicitud), usuario);
        }
    }

    public void enviarLoteSMS(List<ContactoMojix> contactos, boolean isValido, String usuario) {
        if (isValido && contactos != null) {
            String mensaje = medioAmbiente.getValorDe(MOJIX_MENSAJE_EVENTO, MOJIX_MENSAJE_EVENTO_DEFAULT);
            for (ContactoMojix contactoMojix : contactos) {
                operacionesMojix.enviarSMS(contactoMojix.getNroCelular(), "", TITULO_SMS, mensaje, usuario);
            }
        }
    }

    private void enviarSMS(Cliente cliente, String mensaje, String usuario) {
        String fonosPermitidos = medioAmbiente.getValorDe(MOJIX_TELEFONOS_PERMITIDOS, MOJIX_TELEFONOS_PERMITIDOS_DEFAULT);
        //Verificar telefonos permitidos
        if (DEFAULT_FONOS_PERMITIDOS.equals(StringUtils.trim(fonosPermitidos))) {
            LOGGER.debug("{} es *... permitiendo acceso", MOJIX_TELEFONOS_PERMITIDOS);
        } else if (!fonosPermitidos.contains(cliente.getTelefono())) {
            LOGGER.error("Fono {} no esta contenido en la variable {}. Coloca asterisco en esta variable para desactivar la verificacion.", cliente.getTelefono(), MOJIX_TELEFONOS_PERMITIDOS);
            return;
        }
        operacionesMojix.enviarSMS(cliente.getTelefono(), cliente.getDocumento(), TITULO_SMS, mensaje, usuario);
    }

    private String getMsgSMS(Solicitud solicitud) {
        final SimpleDateFormat dff = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        final Map<String, Object> params = new HashMap<>();
        params.put(TOKENSMS.name(), solicitud.getPin());
        params.put(TOKENTIEMPOVIDA.name(), dff.format(solicitud.getTiempoVida()));
        String msg = medioAmbiente.getValorDe(MOJIX_NOTIFICACION_TOKEN, MOJIX_NOTIFICACION_TOKEN_DEFAULT);
        return replaceKeywords(msg, params);
    }

    private <T> T getResponse(MensajesMojix mensajeMojix, Map<String, Object> params, Class<T> clazz) {
        String codigo = null;
        String mensaje = null;
        if (mensajeMojix != null) {
            codigo = mensajeMojix.getCodigo();
            mensaje = getMensaje(mensajeMojix, params);
        }
        try {
            return clazz.getConstructor(String.class, CodigosConfirmacion.class, String.class).newInstance(codigo, ERROR, mensaje);
        } catch (Exception e) {
            LOGGER.error("Hubo un error al cargas los mensajes ->", e);
        }
        return null;
    }

    private <T> T getResponse(MensajesMojix mensajeMojix, Map<String, Object> params, Class<T> clazz, String transactionID) {
        String codigo = null;
        String mensaje = null;
        if (mensajeMojix != null) {
            codigo = mensajeMojix.getCodigo();
            mensaje = getMensaje(mensajeMojix, params);
        }
        try {
            return clazz.getConstructor(String.class, CodigosConfirmacion.class, String.class, String.class).newInstance(codigo, ERROR, mensaje, transactionID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ClienteJoven getCliente(Cliente cliente, String usuario) {
        ClienteJoven clienteJoven = null;
        if (cliente != null) {
            clienteJoven = clienteJovenDao.getByCodigoCliente(cliente.getCodCliente());
            //Generar encriptado en SJC02 por CODCLI (PK)
            if (clienteJoven == null) {
                clienteJoven = new ClienteJoven();
                clienteJoven.setCodigoCliente(cliente.getCodCliente());
                clienteJoven.setCodigoClienteBisa(encriptarID(cliente.getCodCliente(), SALT_5L));
                clienteJoven = clienteJovenDao.registrar(clienteJoven, usuario);
            }
        }
        return clienteJoven;
    }

    private CuentaBancoBisa getCuentaBisa(Cuenta cuenta) {
        if (cuenta != null && cuenta.getCodigoMoneda() != null) {
            String descripcion = Monedas.getDescripcionByBisa(cuenta.getCodigoMoneda());
            return new CuentaBancoBisa(String.valueOf(cuenta.getNumeroCuenta()), new MonedaMojix(String.valueOf(cuenta.getCodigoMoneda()), descripcion));
        } else {
            return null;
        }
    }

    private String getCodigoConf(Long id) {
        try {
            MessageDigest md = MessageDigest.getInstance(SHA1);
            md.update(String.valueOf(id).getBytes(UTF_8));
            md.update(new BigInteger(SALT_5L.toString()).toByteArray());
            return String.valueOf(Hex.encodeHex(md.digest()));
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
    }

    private int getTamanioTransactionid() {
        return medioAmbiente.getValorIntDe(MOJIX_TAMANIO_TRANSACTION_ID, MOJIX_TAMANIO_TRANSACTION_ID_DEFAULT);
    }

    private int getTamanioMotivo() {
        return medioAmbiente.getValorIntDe(MOJIX_TAMANIO_MOTIVO, MOJIX_TAMANIO_MOTIVO_DEFAULT);
    }

    private String getCanal() {
        return medioAmbiente.getValorDe(MOJIX_CANAL, MOJIX_CANAL_DEFAULT);
    }

    private String getOK() {
        return "0";
    }

    private String getMensajeOk() {
        return medioAmbiente.getValorDe(MOJIX_MENSAJE_REGISTRO_EXITOSO, MOJIX_MENSAJE_REGISTRO_EXITOSO_DEFAULT);
    }

    private String getMensaje(MensajesMojix mensajeMojix, Map<String, Object> params) {
        String mensaje;
        if (mensajeMojix.isKeyword() && params != null) {
            mensaje = ProcesosMojix.replaceKeywords(medioAmbiente.getValorDe(mensajeMojix.getMensaje(), mensajeMojix.getDescripcion()), params);
        } else {
            mensaje = medioAmbiente.getValorDe(mensajeMojix.getMensaje(), mensajeMojix.getDescripcion());
        }
        return mensaje;
    }

    private boolean isValidTiempoSolicitud() {
        String validaTiempo = medioAmbiente.getValorDe(MOJIX_VALIDA_TIEMPO_SOLICITUD_TOKEN_SMS, MOJIX_VALIDA_TIEMPO_SOLICITUD_TOKEN_SMS_DEFAULT);
        return ENABLE.equals(validaTiempo);
    }

    private int getTiempoEntreClaves() {
        return medioAmbiente.getValorIntDe(TIEMPO_ENTRE_SOLICITUD_CLAVES, TIEMPO_ENTRE_SOLICITUD_CLAVES_DEFAULT);
    }

    private void generarClave(Solicitud solicitud) throws NoSuchAlgorithmException {
        DateTime now = DateTime.now();
        String horasEntrePedidos = medioAmbiente.getValorDe(HORAS_ENTRE_PEDIDOS_QUE_ANULAN, HORAS_ENTRE_PEDIDOS_QUE_ANULAN_DEFAULT);
        int cantidadDigitos = medioAmbiente.getValorIntDe(CANTIDAD_DE_DIGITOS, CANTIDAD_DE_DIGITOS_DEFAULT);
        ParPines nuevoPin = TresClavesUtils.actualizarClaveMovil(solicitud, cantidadDigitos, horasEntrePedidos);
        solicitud.setTokenUsado(NUSED);
        solicitud.setTiempoVida(now.plusHours(getTiempoVida()).toDate());
        solicitud.setPin(nuevoPin.getPin());
    }

    private int getTiempoVida() {
        return parseInt(medioAmbiente.getValorDe(MOJIX_MENSAJE_TIEMPO_DE_VIDA_TOKEN_HORAS, MOJIX_MENSAJE_TIEMPO_DE_VIDA_TOKEN_HORAS_DEFAULT));
    }

    /**
     * validateIniCliente Validar el servicio iniciarCliente
     *
     * @param requestBasic datos solicitud
     * @return JsonResultBisa validacion
     */
    public ResponseOperacion validateIniCliente(JsonRequestBasic requestBasic) {
        String transactionID;
        String fechaNac;
        String documentoCI;
        BigDecimal fechaJuliana;
        int cantidad;
        ResponseOperacion responseOperacion = new ResponseOperacion(requestBasic);
        try {
            //Datos de entrada
            transactionID = requestBasic.getTransactionID();
            fechaNac = requestBasic.getFechaNac();
            documentoCI = requestBasic.getCI();
            Map<String, Object> params = new HashMap<>();
            //Valor requerido
            if (StringUtils.trimToNull(transactionID) == null) {
                params.put(NOMBRECAMPO.name(), ValidaCampos.CODIGO_TRANSACCION);
                responseOperacion.setResultBisa(getResponse(ERROR_REQUERIDO, params, JsonResultBisa.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(fechaNac) == null) {
                params.put(NOMBRECAMPO.name(), FECHA_NACIMIENTO);
                responseOperacion.setResultBisa(getResponse(ERROR_REQUERIDO, params, JsonResultBisa.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(documentoCI) == null) {
                params.put(NOMBRECAMPO.name(), DOCUMENTO_CI);
                responseOperacion.setResultBisa(getResponse(ERROR_REQUERIDO, params, JsonResultBisa.class));
                return responseOperacion;
            }
            //Validar tamaño transaction ID
            if (transactionID.length() > getTamanioTransactionid()) {
                responseOperacion.setResultBisa(getResponse(ERROR_TAMANO, null, JsonResultBisa.class));
                return responseOperacion;
            }
            //Validar formato Fecha
            fechaJuliana = Convert.convertDateToJulian(fechaNac);
            if (fechaJuliana.compareTo(BigDecimal.ZERO) == 0) {
                responseOperacion.setResultBisa(getResponse(ERROR_FORMATO, null, JsonResultBisa.class));
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(transactionID);
            JsonResultBisa x = getExisteOperacion(transactionID, operacion, JsonResultBisa.class);
            if (x != null) {
                responseOperacion.setResultBisa(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Obtener cliente
            List<Cliente> clienteList = clienteDao.getListaClientesByDocumentoAndFecNacimiento(documentoCI, fechaJuliana);
            if (clienteList == null || clienteList.size() == 0) {
                params.put(DOCID.name(), documentoCI);
                params.put(FECNAC.name(), fechaNac);
                responseOperacion.setResultBisa(getResponse(ERROR_CLIENTE_NOEXISTE, params, JsonResultBisa.class));
                return responseOperacion;
            }
            cantidad = clienteList.size();
            if (cantidad == 1) {
                responseOperacion.setCliente(clienteList.get(0));
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <iniciarCliente>", e);
            responseOperacion.setResultBisa(getResponse(ERROR_PROCESO, null, JsonResultBisa.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultBisa(new JsonResultBisa(getOK(), String.valueOf(cantidad), getMensajeOk()));
        return responseOperacion;
    }

    /**
     * validateConfIniCliente Validar el servicio de confirmacion al iniciarCliente
     *
     * @param request datos solicitud
     * @return JsonResultClienteBisa validacion
     */
    public ResponseOperacion validateConfIniCliente(JsonRequestConfirmar request) {
        ClienteBisa clienteBisa = null;
        ResponseOperacion responseOperacion = new ResponseOperacion(request);
        try {
            Map<String, Object> params = new HashMap<>();
            //Validar datos requeridos
            JsonResultClienteBisa x = getExisteConfirmacion(request.getTransactionID(), request.getConfCode(), request.getTokenSMS(), JsonResultClienteBisa.class);
            if (x != null) {
                responseOperacion.setResultClienteBisa(x);
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(request.getTransactionID());
            x = getExisteOperacion(operacion, request.getTransactionID(), request.getConfCode(), request.getTokenSMS(), JsonResultClienteBisa.class);
            if (x != null) {
                responseOperacion.setResultClienteBisa(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Obtener solicitud origen
            Solicitud solicitud = solicitudDao.getByConfirmacion(operacion, request.getConfCode());
            x = getExisteSolicitud(solicitud, request.getConfCode(), request.getTokenSMS(), JsonResultClienteBisa.class);
            if (x != null) {
                responseOperacion.setResultClienteBisa(x);
                return responseOperacion;
            }
            responseOperacion.setSolicitud(solicitud);
            //Procesar solicitud
            if (operacion != null) {
                //Obtener Cliente
                Cliente cliente = clienteDao.getByCodigoCliente(operacion.getCliente().getCodigoCliente());
                if (cliente == null) {
                    params.put(CODCLIENTE.name(), operacion.getCliente().getCodigoCliente());
                    responseOperacion.setResultClienteBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultClienteBisa.class));
                    return responseOperacion;
                }
                ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
                if (clienteNatural == null) {
                    responseOperacion.setResultClienteBisa(getResponse(ERROR_SIN_DATA, null, JsonResultClienteBisa.class));
                    return responseOperacion;
                }
                responseOperacion.setCliente(cliente);
                responseOperacion.setClienteNatural(clienteNatural);
                //Cargar cliente
                clienteBisa = new ClienteBisa();
                clienteBisa.setBisaClienteID(StringUtils.trimToEmpty(operacion.getCliente().getCodigoClienteBisa()));
                clienteBisa.setTelefono(StringUtils.trimToEmpty(cliente.getTelefono()));
                clienteBisa.setMaterno(StringUtils.trimToEmpty(clienteNatural.getMaterno()));
                clienteBisa.setPaterno(StringUtils.trimToEmpty(clienteNatural.getPaterno()));
                clienteBisa.setNombre(StringUtils.trimToEmpty(clienteNatural.getNombre()));
                clienteBisa.setGenero(StringUtils.trimToEmpty(cliente.getSexo()));
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <confirmarCliente>", e);
            responseOperacion.setResultClienteBisa(getResponse(ERROR_PROCESO, null, JsonResultClienteBisa.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultClienteBisa(new JsonResultClienteBisa(getOK(), getMensajeOk(), clienteBisa));
        return responseOperacion;
    }

    /**
     * validateIniContacto Validar el servicio iniciarContacto
     *
     * @param requestBasic datos solicitud
     * @return JsonResultBisa validacion
     */
    public ResponseOperacion validateIniContacto(JsonRequestContacto requestBasic) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestBasic);
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido
            if (StringUtils.trimToNull(requestBasic.getTransactionID()) == null) {
                params.put(NOMBRECAMPO.name(), ValidaCampos.CODIGO_TRANSACCION);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getNroContactos()) == null) {
                params.put(NOMBRECAMPO.name(), NRO_CONTACTOS);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar tamaño transaction ID
            if (requestBasic.getTransactionID().length() > getTamanioTransactionid()) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_TAMANO, null, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(requestBasic.getTransactionID());
            JsonResultComun x = getExisteOperacion(requestBasic.getTransactionID(), operacion, JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Validar cliente

            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestBasic.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_SIN_DATA, null, JsonResultComun.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <iniciarContacto>", e);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultContactoBisa(new JsonResultComun(getOK(), getMensajeOk()));
        return responseOperacion;
    }

    /**
     * validateConfirmar Validar el servicio de confirmacion al iniciarContacto
     *
     * @param request datos solicitud
     * @return JsonResultContactoBisa validacion
     */
    public ResponseOperacion validateConfirmar(JsonRequestConfirmar request) {
        ResponseOperacion responseOperacion = new ResponseOperacion(request);
        try {
            //Validar datos requeridos
            JsonResultComun x = getExisteConfirmacion(request.getTransactionID(), request.getConfCode(), request.getTokenSMS(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(request.getTransactionID());
            x = getExisteOperacion(operacion, request.getTransactionID(), request.getConfCode(), request.getTokenSMS(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Obtener solicitud origen
            Solicitud solicitud = solicitudDao.getByConfirmacion(operacion, request.getConfCode());
            x = getExisteSolicitud(solicitud, request.getConfCode(), request.getTokenSMS(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            responseOperacion.setSolicitud(solicitud);
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <confirmar>", e);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultContactoBisa(new JsonResultComun(getOK(), getMensajeOk()));
        return responseOperacion;
    }

    /**
     * validateIniCuenta Validar el servicio iniciarCuenta
     *
     * @param requestBasic datos solicitud
     * @return JsonResultCuentaBanco validacion
     */
    public ResponseOperacion validateIniCuenta(JsonRequestCuentaBanco requestBasic) {
        ClienteBancoBisa clienteBancoBisa;
        ResponseOperacion responseOperacion = new ResponseOperacion(requestBasic);
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido
            if (StringUtils.trimToNull(requestBasic.getTransactionID()) == null) {
                params.put(NOMBRECAMPO.name(), ValidaCampos.CODIGO_TRANSACCION);
                responseOperacion.setResultCuentaBanco(getResponse(ERROR_REQUERIDO, params, JsonResultCuentaBanco.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultCuentaBanco(getResponse(ERROR_REQUERIDO, params, JsonResultCuentaBanco.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getNroCuenta()) == null) {
                params.put(NOMBRECAMPO.name(), NRO_CUENTA);
                responseOperacion.setResultCuentaBanco(getResponse(ERROR_REQUERIDO, params, JsonResultCuentaBanco.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getTelefono()) == null) {
                params.put(NOMBRECAMPO.name(), NRO_TELEFONO);
                responseOperacion.setResultCuentaBanco(getResponse(ERROR_REQUERIDO, params, JsonResultCuentaBanco.class));
                return responseOperacion;
            }
            //Validar tamaño transaction ID
            if (requestBasic.getTransactionID().length() > getTamanioTransactionid()) {
                responseOperacion.setResultCuentaBanco(getResponse(ERROR_TAMANO, null, JsonResultCuentaBanco.class));
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(requestBasic.getTransactionID());
            JsonResultCuentaBanco x = getExisteOperacion(requestBasic.getTransactionID(), operacion, JsonResultCuentaBanco.class);
            if (x != null) {
                responseOperacion.setResultCuentaBanco(x);
                return responseOperacion;
            }
            //Validar cliente

            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestBasic.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultCuentaBanco(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultCuentaBanco.class));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultCuentaBanco(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultCuentaBanco.class));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultCuentaBanco(getResponse(ERROR_SIN_DATA, null, JsonResultCuentaBanco.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);

            //Procesar Solicitud
            //Validar cuenta a agregar
            ClienteCuentaDetalle clienteCuentaDetalleAdicion = clienteCuentaDetalleDao.getByCuenta(Long.parseLong(requestBasic.getNroCuenta()));
            if (clienteCuentaDetalleAdicion == null) {
                params.put(CUENTAINVALIDA.name(), requestBasic.getNroCuenta());
                responseOperacion.setResultCuentaBanco(getResponse(ERROR_CUENTA_INEXISTENTE, params, JsonResultCuentaBanco.class));
                return responseOperacion;
            }
            Cuenta cuentaAdicion = cuentaDao.getByNumero(clienteCuentaDetalleAdicion.getCuenta());
            if (cuentaAdicion == null) {
                params.put(CUENTAINVALIDA.name(), requestBasic.getNroCuenta());
                responseOperacion.setResultCuentaBanco(getResponse(ERROR_CUENTA_INEXISTENTE, params, JsonResultCuentaBanco.class));
                return responseOperacion;
            }
            Cliente clienteAdicion = clienteDao.getByCodigoCliente(clienteCuentaDetalleAdicion.getCodigoCliente());
            if (clienteAdicion == null) {
                params.put(CUENTAINVALIDA.name(), requestBasic.getNroCuenta());
                responseOperacion.setResultCuentaBanco(getResponse(ERROR_CUENTA_INEXISTENTE, params, JsonResultCuentaBanco.class));
                return responseOperacion;
            }
            ClienteNatural clienteNaturalAdicion = clienteNaturalDao.getDatosCliente(clienteCuentaDetalleAdicion.getCodigoCliente());
            if (clienteNaturalAdicion == null) {
                params.put(CUENTAINVALIDA.name(), requestBasic.getNroCuenta());
                responseOperacion.setResultCuentaBanco(getResponse(ERROR_CUENTA_INEXISTENTE, params, JsonResultCuentaBanco.class));
                return responseOperacion;
            }

            clienteBancoBisa = new ClienteBancoBisa();
            clienteBancoBisa.setBisaClienteID(StringUtils.trimToEmpty(clienteJoven.getCodigoClienteBisa()));
            clienteBancoBisa.setTelefono(StringUtils.trimToEmpty(clienteAdicion.getTelefono()));
            clienteBancoBisa.setMaterno(StringUtils.trimToEmpty(clienteNaturalAdicion.getMaterno()));
            clienteBancoBisa.setPaterno(StringUtils.trimToEmpty(clienteNaturalAdicion.getPaterno()));
            clienteBancoBisa.setNombre(StringUtils.trimToEmpty(clienteNaturalAdicion.getNombre()));
            clienteBancoBisa.setCuenta(getCuentaBisa(cuentaAdicion));

            BeneficiarioJoven beneficiarioJoven = new BeneficiarioJoven();
            beneficiarioJoven.setTipoBeneficiario(TipoBeneficiario.CUENTA_BANCO);
            beneficiarioJoven.setBancoBeneficiario(operacionesMojix.getBancoDestino(BANCO_BISA));
            beneficiarioJoven.setNumeroCuenta(String.valueOf(cuentaAdicion.getNumeroCuenta()));
            beneficiarioJoven.setMonedaCuenta(cuentaAdicion.getCodigoMoneda());
            beneficiarioJoven.setDepartamentoCuenta(cuentaAdicion.getAgencia().toString());
            beneficiarioJoven.setNumeroTelefono(StringUtils.trimToEmpty(clienteAdicion.getTelefono()));
            beneficiarioJoven.setNumeroDocumento(StringUtils.trimToEmpty(clienteAdicion.getDocumento()));
            beneficiarioJoven.setTipoDocumento(StringUtils.trimToEmpty(clienteAdicion.getTipoDoc()));
            beneficiarioJoven.setNombre(StringUtils.trimToEmpty(clienteNaturalAdicion.getNombre()));
            beneficiarioJoven.setPaterno(StringUtils.trimToEmpty(clienteNaturalAdicion.getPaterno()));
            beneficiarioJoven.setMaterno(StringUtils.trimToEmpty(clienteNaturalAdicion.getMaterno()));
            responseOperacion.setBeneficiarioJoven(beneficiarioJoven);
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <iniciarCuenta>", e);
            responseOperacion.setResultCuentaBanco(getResponse(ERROR_PROCESO, null, JsonResultCuentaBanco.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultCuentaBanco(new JsonResultCuentaBanco(getOK(), getMensajeOk(), clienteBancoBisa));
        return responseOperacion;
    }

    /**
     * validateAddCliente Validar el servicio adicionarCliente
     *
     * @param requestBasic datos solicitud
     * @return JsonResultClienteBisa validacion
     */
    public ResponseOperacion validateAddCliente(JsonRequestBasic requestBasic, String usuario) {
        String transactionID;
        String fechaNac;
        String documentoCI;
        BigDecimal fechaJuliana;
        ClienteBisa clienteBisa;
        ResponseOperacion responseOperacion = new ResponseOperacion(requestBasic);
        try {
            //Datos de entrada
            transactionID = requestBasic.getTransactionID();
            fechaNac = requestBasic.getFechaNac();
            documentoCI = requestBasic.getCI();
            Map<String, Object> params = new HashMap<>();
            //Valor requerido
            if (StringUtils.trimToNull(transactionID) == null) {
                params.put(NOMBRECAMPO.name(), ValidaCampos.CODIGO_TRANSACCION);
                responseOperacion.setResultClienteBisa(getResponse(ERROR_REQUERIDO, params, JsonResultClienteBisa.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(fechaNac) == null) {
                params.put(NOMBRECAMPO.name(), FECHA_NACIMIENTO);
                responseOperacion.setResultClienteBisa(getResponse(ERROR_REQUERIDO, params, JsonResultClienteBisa.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(documentoCI) == null) {
                params.put(NOMBRECAMPO.name(), DOCUMENTO_CI);
                responseOperacion.setResultClienteBisa(getResponse(ERROR_REQUERIDO, params, JsonResultClienteBisa.class));
                return responseOperacion;
            }
            //Validar tamaño transaction ID
            if (transactionID.length() > getTamanioTransactionid()) {
                responseOperacion.setResultClienteBisa(getResponse(ERROR_TAMANO, null, JsonResultClienteBisa.class));
                return responseOperacion;
            }
            //Validar formato Fecha
            fechaJuliana = Convert.convertDateToJulian(fechaNac);
            if (fechaJuliana.compareTo(BigDecimal.ZERO) == 0) {
                responseOperacion.setResultClienteBisa(getResponse(ERROR_FORMATO, null, JsonResultClienteBisa.class));
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(transactionID);
            JsonResultClienteBisa x = getExisteOperacion(transactionID, operacion, JsonResultClienteBisa.class);
            if (x != null) {
                responseOperacion.setResultClienteBisa(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Procesar solicitud
            Cliente cliente = clienteDao.getClienteByDocumentoAndFecNacimiento(documentoCI, fechaJuliana);
            if (cliente == null) {
                params.put(DOCID.name(), documentoCI);
                params.put(FECNAC.name(), fechaNac);
                responseOperacion.setResultClienteBisa(getResponse(ERROR_CLIENTE_NOEXISTE, params, JsonResultClienteBisa.class));
                return responseOperacion;
            }
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                responseOperacion.setResultClienteBisa(getResponse(ERROR_SIN_DATA, null, JsonResultClienteBisa.class));
                return responseOperacion;
            }
            //Validar contrato
            if (!realizoContrato(StringUtils.trimToEmpty(cliente.getCodCliente()))) {
                responseOperacion.setResultClienteBisa(getResponse(ERROR_CONTRATO, null, JsonResultClienteBisa.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteNatural(clienteNatural);

            clienteBisa = new ClienteBisa();
            clienteBisa.setBisaClienteID(StringUtils.trimToEmpty(getCliente(cliente, usuario).getCodigoClienteBisa()));
            clienteBisa.setTelefono(StringUtils.trimToEmpty(cliente.getTelefono()));
            clienteBisa.setMaterno(StringUtils.trimToEmpty(clienteNatural.getMaterno()));
            clienteBisa.setPaterno(StringUtils.trimToEmpty(clienteNatural.getPaterno()));
            clienteBisa.setNombre(StringUtils.trimToEmpty(clienteNatural.getNombre()));
            clienteBisa.setGenero(StringUtils.trimToEmpty(cliente.getSexo()));
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <adicionarCliente>", e);
            responseOperacion.setResultClienteBisa(getResponse(ERROR_PROCESO, null, JsonResultClienteBisa.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultClienteBisa(new JsonResultClienteBisa(getOK(), getMensajeOk(), clienteBisa));
        return responseOperacion;
    }


    /**
     * validateConsultaCuenta Validar el servicio consulta de cuentas
     *
     * @param requestBasic datos solicitud
     * @return JsonResultCuentaBanco validacion
     */
    public ResponseOperacion validateConsultaCuenta(JsonRequestInfo requestBasic) {
        IRespuestasMonitor monitor;
        MonedaMojix moneda;
        CuentaClienteMojix cuentaBisa;
        List<CuentaClienteMojix> cuentas = new ArrayList<>();
        ResponseOperacion responseOperacion = new ResponseOperacion(requestBasic);
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido
            if (StringUtils.trimToNull(requestBasic.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultCuentaBisa(getResponse(ERROR_REQUERIDO, params, JsonResultCuentaBisa.class));
                return responseOperacion;
            }
            //Validar clienteBancoBisa
            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestBasic.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_SIN_DATA, null, JsonResultComun.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);
            //Procesar Solicitud
            //Obtener cuentas
            List<String> cuentasCliente = clienteCuentaDetalleDao.getByCliente(cliente.getCodCliente());
            //Obtener saldos
            monitor = transaccionMonitorMojix.consultaCuenta(cliente);
            if (monitor == null) {
                responseOperacion.setResultCuentaBisa(getResponse(ERROR_SIN_RESULTADO, null, JsonResultCuentaBisa.class));
                return responseOperacion;
            } else {
                while (monitor.next1()) {
                    String cuenta = getValue(monitor.getDatoRecibidoCuerpo1Decimal(MON_CUENTA));
                    String codMoneda = getValue(monitor.getDatoRecibidoCuerpo1Decimal(MON_CODMONEDA));
                    String saldo = getValue(monitor.getDatoRecibidoCuerpo1Decimal(MON_SALDO));
                    //Validar cuenta
                    if (cuenta != null) {
                        if (cuentasCliente.contains(cuenta)) {
                            LOGGER.debug("cuenta>>:" + cuenta);
                            moneda = new MonedaMojix(codMoneda, Monedas.getDescripcionByBisa(codMoneda));
                            cuentaBisa = new CuentaClienteMojix(cuenta, saldo != null ? saldo.replace(".", "") : null, cuentas.size() == 0 ? "SI" : "NO", moneda);
                            cuentas.add(cuentaBisa);
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <consultaCuenta>", e);
            responseOperacion.setResultCuentaBisa(getResponse(ERROR_PROCESO, null, JsonResultCuentaBisa.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultCuentaBisa(new JsonResultCuentaBisa(getOK(), getMensajeOk(), cuentas));
        return responseOperacion;
    }

    /**
     * validateIniTransferencia Validar el servicio transferencia
     *
     * @param requestBasic datos solicitud
     * @return JsonResultContactoBisa validacion
     */
    public ResponseOperacion validateIniTransferencia(JsonRequestTransferencia requestBasic) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestBasic);
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido
            if (StringUtils.trimToNull(requestBasic.getTransactionID()) == null) {
                params.put(NOMBRECAMPO.name(), ValidaCampos.CODIGO_TRANSACCION);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getCuentaOrigen()) == null) {
                params.put(NOMBRECAMPO.name(), CUENTA_ORIGEN);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getCuentaDestino()) == null) {
                params.put(NOMBRECAMPO.name(), CUENTA_DESTINO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getMoneda()) == null) {
                params.put(NOMBRECAMPO.name(), MONEDA);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (requestBasic.getMonto() == null) {
                params.put(NOMBRECAMPO.name(), MONTO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getMotivo()) == null) {
                params.put(NOMBRECAMPO.name(), MOTIVO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (requestBasic.getGeoProperties() == null) {
                params.put(NOMBRECAMPO.name(), LUGAR);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar tamaño transaction ID
            if (requestBasic.getTransactionID().length() > getTamanioTransactionid()) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_TAMANO, null, JsonResultComun.class));
                return responseOperacion;
            }
            // Monto mayor a Cero.
            if (!(requestBasic.getMonto().compareTo(BigDecimal.ZERO) > 0)) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_MONTO, null, JsonResultComun.class));
                return responseOperacion;
            }
            // Moneda valida
            if (!Monedas.esValida(requestBasic.getMoneda())) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_MONEDA, null, JsonResultComun.class));
                return responseOperacion;
            }
            //validar PCC01
            if (operacionesMojix.validarPCC01(requestBasic.getMonto().movePointLeft(2), requestBasic.getMoneda())) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_PCC01, null, JsonResultComun.class));
                return responseOperacion;
            }
            // Motivo es obligatorio
            if (requestBasic.getMotivo().length() < getTamanioMotivo()) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_MOTIVO, null, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(requestBasic.getTransactionID());
            JsonResultComun x = getExisteOperacion(requestBasic.getTransactionID(), operacion, JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Validar clienteBancoBisa
            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestBasic.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_SIN_DATA, null, JsonResultComun.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);
            //Validar cuenta origen
            x = getExisteCuenta(clienteJoven, requestBasic.getCuentaOrigen(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            //Validar cuenta destino
            ClienteCuentaDetalle cuentaDetalle = clienteCuentaDetalleDao.getByCuenta(Long.parseLong(requestBasic.getCuentaDestino()));
            if (cuentaDetalle == null) {
                params = new HashMap<>();
                params.put(CUENTAINVALIDA.name(), requestBasic.getCuentaDestino());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CUENTA_INEXISTENTE, params, JsonResultComun.class));
                return responseOperacion;
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <iniciarTransferencia>", e);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultContactoBisa(new JsonResultComun(getOK(), getMensajeOk()));
        return responseOperacion;
    }

    /**
     * validateConfIniTransferencia Validar el servicio de confirmacion al iniciarCuenta
     *
     * @param request datos solicitud
     * @return JsonResultContactoBisa validacion
     */
    public ResponseOperacion validateConfIniTransferencia(JsonRequestConfirmar request) {
        ResponseOperacion responseOperacion = new ResponseOperacion(request);
        try {
            //Validar datos requeridos
            JsonResultComun x = getExisteConfirmacion(request.getTransactionID(), request.getConfCode(), request.getTokenSMS(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(request.getTransactionID());
            x = getExisteOperacion(operacion, request.getTransactionID(), request.getConfCode(), request.getTokenSMS(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            //Obtener solicitud origen
            Solicitud solicitud = solicitudDao.getByConfirmacion(operacion, request.getConfCode());
            x = getExisteSolicitud(solicitud, request.getConfCode(), request.getTokenSMS(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            responseOperacion.setSolicitud(solicitud);
            if (procesarTransferencia(responseOperacion, transferenciaDao.getTransferByOperacion(operacion)))
                return responseOperacion;
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <confirmarTransferencia>", e);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultContactoBisa(new JsonResultComun(getOK(), getMensajeOk()));
        return responseOperacion;
    }

    /**
     * validateIniTransferenciaProgramada Validar el servicio iniciar transferencia programada
     *
     * @param requestBasic datos solicitud
     * @return ResponseOperacion validacion
     */
    public ResponseOperacion validateIniTransferenciaProgramada(JsonRequestTransfProgramada requestBasic) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestBasic);
        if (StringUtils.trimToNull(requestBasic.getPeriodicidad()) == null) {
            Map<String, Object> params = new HashMap<>();
            params.put(NOMBRECAMPO.name(), PERIODICIDAD);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion = validateIniTransferencia(requestBasic);
        return responseOperacion;
    }

    /**
     * validateConfIniTransferenciaProgramada Validar el servicio de confirmacion al iniciarTransferencia Programada
     *
     * @param request datos solicitud
     * @return ResponseOperacion validacion
     */
    public ResponseOperacion validateConfIniTransferenciaProgramada(JsonRequestConfirmar request) {
        ResponseOperacion responseOperacion = validateConfirmar(request);
        if (responseOperacion.isValido()) {
            Transferencia transferencia = transferenciaDao.getTransferByOperacion(responseOperacion.getOperacion());
            responseOperacion.setTransferencia(transferencia);
        }
        return responseOperacion;
    }

    /**
     * validateTransferenciaProgramada Validar el servicio transferencia programada
     *
     * @param requestBasic datos solicitud
     * @return ResponseOperacion validacion
     */
    public ResponseOperacion validateTransferenciaProgramada(JsonRequestTransfProgramada requestBasic) {
        ResponseOperacion responseOperacion = validateIniTransferenciaProgramada(requestBasic);
        if (responseOperacion.isValido()) {
            try {
                Transferencia transferencia = transferenciaDao.newTransferencia(requestBasic.getGeoProperties());
                transferencia.setCuentaOrigen(requestBasic.getCuentaOrigen());
                transferencia.setCuentaDestino(requestBasic.getCuentaDestino());
                transferencia.setMoneda(Short.parseShort(requestBasic.getMoneda()));
                transferencia.setMonto(requestBasic.getMonto().movePointLeft(2));
                transferencia.setMotivo(StringUtils.trimToNull(StringUtils.upperCase(requestBasic.getMotivo())));
                transferencia.setPeriodicidad(StringUtils.trimToNull(StringUtils.upperCase(requestBasic.getPeriodicidad())));
                responseOperacion.setTransferencia(transferencia);
                if (procesarTransferencia(responseOperacion, transferencia))
                    return responseOperacion;
            } catch (Exception e) {
                LOGGER.error("Hubo un error inesperado <validateTransferenciaProgramada>", e);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
                return responseOperacion;
            }
            responseOperacion.setValido(true);
            responseOperacion.setResultContactoBisa(new JsonResultComun(getOK(), getMensajeOk()));
        }
        return responseOperacion;
    }


    /**
     * validateIniTransferenciaBulk Validar el servicio transferencia bulk
     *
     * @param requestBasic datos solicitud
     * @return ResponseOperacion validacion
     */
    public ResponseOperacion validateIniTransferenciaBulk(JsonRequestBulk requestBasic) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestBasic);
        //Datos de entrada
        String transactionID = StringUtils.trimToNull(requestBasic.getTransactionID());
        try {
            Map<String, Object> params = new HashMap<>();
            //Valor requerido
            if (StringUtils.trimToNull(requestBasic.getTransactionID()) == null) {
                params.put(NOMBRECAMPO.name(), ValidaCampos.CODIGO_TRANSACCION);
                responseOperacion.setResultBulkBisa(getResponse(ERROR_REQUERIDO, params, JsonResultBulkBisa.class, transactionID));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultBulkBisa(getResponse(ERROR_REQUERIDO, params, JsonResultBulkBisa.class, transactionID));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getCuentaOrigen()) == null) {
                params.put(NOMBRECAMPO.name(), CUENTA_ORIGEN);
                responseOperacion.setResultBulkBisa(getResponse(ERROR_REQUERIDO, params, JsonResultBulkBisa.class, transactionID));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getMotivo()) == null) {
                params.put(NOMBRECAMPO.name(), MOTIVO);
                responseOperacion.setResultBulkBisa(getResponse(ERROR_REQUERIDO, params, JsonResultBulkBisa.class, transactionID));
                return responseOperacion;
            }
            if (requestBasic.getGeoProperties() == null) {
                params.put(NOMBRECAMPO.name(), LUGAR);
                responseOperacion.setResultBulkBisa(getResponse(ERROR_REQUERIDO, params, JsonResultBulkBisa.class, transactionID));
                return responseOperacion;
            }
            if (requestBasic.getCuentas() == null) {
                params.put(NOMBRECAMPO.name(), LIST_CUENTAS);
                responseOperacion.setResultBulkBisa(getResponse(ERROR_REQUERIDO, params, JsonResultBulkBisa.class, transactionID));
                return responseOperacion;
            }
            if (requestBasic.getCuentas().isEmpty()) {
                params.put(NOMBRECAMPO.name(), LIST_CUENTAS);
                responseOperacion.setResultBulkBisa(getResponse(ERROR_REQUERIDO, params, JsonResultBulkBisa.class, transactionID));
                return responseOperacion;
            }
            //Validar tamaño transaction ID
            if (requestBasic.getTransactionID().length() > getTamanioTransactionid()) {
                responseOperacion.setResultBulkBisa(getResponse(ERROR_TAMANO, null, JsonResultBulkBisa.class, transactionID));
                return responseOperacion;
            }
            // Motivo es obligatorio
            if (requestBasic.getMotivo().length() < getTamanioMotivo()) {
                responseOperacion.setResultBulkBisa(getResponse(ERROR_MOTIVO, null, JsonResultBulkBisa.class, transactionID));
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(requestBasic.getTransactionID());
            JsonResultBulkBisa x = getExisteOperacion(requestBasic.getTransactionID(), operacion, JsonResultBulkBisa.class);
            if (x != null) {
                responseOperacion.setResultBulkBisa(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Validar clienteBancoBisa
            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestBasic.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultBulkBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultBulkBisa.class, transactionID));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultBulkBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultBulkBisa.class, transactionID));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultBulkBisa(getResponse(ERROR_SIN_DATA, null, JsonResultBulkBisa.class, transactionID));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);
            //Validar cuenta origen
            x = getExisteCuenta(clienteJoven, requestBasic.getCuentaOrigen(), JsonResultBulkBisa.class);
            if (x != null) {
                responseOperacion.setResultBulkBisa(x);
                return responseOperacion;
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateIniTransferenciaBulk>", e);
            responseOperacion.setResultBulkBisa(getResponse(ERROR_PROCESO, null, JsonResultBulkBisa.class, transactionID));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultBulkBisa(new JsonResultBulkBisa(getOK(), getMensajeOk(), transactionID));
        return responseOperacion;
    }


    /**
     * validateConfTransferenciaBulk Validar el servicio de confirmacion de la Transferencia Bulk
     *
     * @param request datos solicitud
     * @return ResponseOperacion validacion
     */
    public ResponseOperacion validateConfTransferenciaBulk(JsonRequestBulkConfirmar request) {
        ResponseOperacion responseOperacion = new ResponseOperacion(request);
        Map<String, Object> params = new HashMap<>();
        int nroProcesados = 0;
        int nroNoProcesados = 0;
        List<JsonResultBulkBisa> procesados = new ArrayList<>();
        List<JsonResultBulkBisa> noProcesados = new ArrayList<>();
        try {
            //Validar datos requeridos
            JsonResultBulkConfirmar x = getExisteConfirmacion(request.getBulkTransactionID(), request.getBulkConfCode(), request.getTokenSMS(), JsonResultBulkConfirmar.class);
            if (x != null) {
                responseOperacion.setResultBulkConfirmar(x);
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(request.getBulkTransactionID());
            x = getExisteOperacion(operacion, request.getBulkTransactionID(), request.getBulkConfCode(), request.getTokenSMS(), JsonResultBulkConfirmar.class);
            if (x != null) {
                responseOperacion.setResultBulkConfirmar(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Obtener solicitud origen
            Solicitud solicitud = solicitudDao.getByConfirmacion(operacion, request.getBulkConfCode());
            x = getExisteSolicitud(solicitud, request.getBulkConfCode(), request.getTokenSMS(), JsonResultBulkConfirmar.class);
            if (x != null) {
                responseOperacion.setResultBulkConfirmar(x);
                return responseOperacion;
            }
            responseOperacion.setSolicitud(solicitud);
            //Validar cada transferencia
            List<Transferencia> transferencias = transferenciaDao.getTranfersByOperacion(responseOperacion.getOperacion());

            for (Transferencia transferencia : transferencias) {
                //Validar Transferencias
                if (StringUtils.trimToNull(transferencia.getCuentaDestino()) == null) {
                    params.put(NOMBRECAMPO.name(), CUENTA_DESTINO);
                    noProcesados.add(getResponse(ERROR_REQUERIDO, params, JsonResultBulkBisa.class, transferencia.getNumeroRef()));
                    nroNoProcesados++;
                    continue;
                }
                if (transferencia.getMoneda() == null) {
                    params.put(NOMBRECAMPO.name(), MONEDA);
                    noProcesados.add(getResponse(ERROR_REQUERIDO, params, JsonResultBulkBisa.class, transferencia.getNumeroRef()));
                    nroNoProcesados++;
                    continue;
                }
                if (transferencia.getMonto() == null) {
                    params.put(NOMBRECAMPO.name(), MONTO);
                    noProcesados.add(getResponse(ERROR_REQUERIDO, params, JsonResultBulkBisa.class, transferencia.getNumeroRef()));
                    nroNoProcesados++;
                    continue;
                }
                // Monto mayor a Cero.
                if (!(transferencia.getMonto().compareTo(BigDecimal.ZERO) > 0)) {
                    noProcesados.add(getResponse(ERROR_MONTO, null, JsonResultBulkBisa.class, transferencia.getNumeroRef()));
                    nroNoProcesados++;
                    continue;
                }
                // Moneda valida
                if (!Monedas.esValida(StringUtils.trimToNull(String.valueOf(transferencia.getMoneda())))) {
                    noProcesados.add(getResponse(ERROR_MONEDA, null, JsonResultBulkBisa.class, transferencia.getNumeroRef()));
                    nroNoProcesados++;
                    continue;
                }
                //Validar cuenta destino
                ClienteCuentaDetalle cuentaDetalle = clienteCuentaDetalleDao.getByCuenta(Long.parseLong(transferencia.getCuentaDestino()));
                if (cuentaDetalle == null) {
                    params = new HashMap<>();
                    params.put(CUENTAINVALIDA.name(), transferencia.getCuentaDestino());
                    noProcesados.add(getResponse(ERROR_CUENTA_INEXISTENTE, params, JsonResultBulkBisa.class, transferencia.getNumeroRef()));
                    nroNoProcesados++;
                    continue;
                }
                //validar PCC01
                if (operacionesMojix.validarPCC01(transferencia.getMonto(), transferencia.getMoneda().toString())) {
                    noProcesados.add(getResponse(ERROR_PCC01, null, JsonResultBulkBisa.class, transferencia.getNumeroRef()));
                    nroNoProcesados++;
                    continue;
                }

                //Procesar transferencia
                IRespuestasMonitor monitor;
                Cuenta cuentaOrigen = cuentaDao.getByNumero(Long.parseLong(transferencia.getCuentaOrigen()));
                Cuenta cuentaDestino = cuentaDao.getByNumero(Long.parseLong(transferencia.getCuentaDestino()));
                //Procesar solicitud
                monitor = transaccionMonitorMojix.procesarTransferencia(transferencia, cuentaOrigen, cuentaDestino);
                if (monitor == null) {
                    noProcesados.add(getResponse(ERROR_MONITOR, null, JsonResultBulkBisa.class, transferencia.getNumeroRef()));
                    nroNoProcesados++;
                    continue;
                }
                BigDecimal numeroCaja = monitor.getDatoRecibidoCabeceraDecimal(MON_USUARIO);
                BigDecimal numeroSecuencial = monitor.getDatoRecibidoCabeceraDecimal(MON_NUMSEQ);
                String codigoPosteo = monitor.getDatoRecibidoCabeceraString(MON_POST);
                String mensaje = monitor.getDatoRecibidoCabeceraString(MON_REJ1);
                if ((PROCESADA.equalsIgnoreCase(codigoPosteo) && numeroCaja != null && numeroSecuencial != null)) {
                    transferencia.setNroCaja(numeroCaja.longValue());
                    transferencia.setNroSecuencia(numeroSecuencial.longValue());
                    procesados.add(new JsonResultBulkBisa(transferencia.getNumeroRef(), getOK(), "", getMensajeOk()));
                    nroProcesados++;
                } else {
                    if (cuentaSinFondo(StringUtils.trimToEmpty(mensaje))) {
                        noProcesados.add(getResponse(ERROR_SALDO_INSUFICIENTE, null, JsonResultBulkBisa.class, transferencia.getNumeroRef()));
                        nroNoProcesados++;
                    } else if (errorPCC01(StringUtils.trimToEmpty(mensaje))) {
                        noProcesados.add(getResponse(ERROR_PCC01, null, JsonResultBulkBisa.class, transferencia.getNumeroRef()));
                        nroNoProcesados++;
                    } else {
                        noProcesados.add(getResponse(ERROR_NO_DEBITO, null, JsonResultBulkBisa.class, transferencia.getNumeroRef()));
                        nroNoProcesados++;
                    }
                }
            }
            responseOperacion.setTransferencias(transferencias);
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateConfTransferenciaBulk>", e);
            responseOperacion.setResultBulkConfirmar(getResponse(ERROR_PROCESO, null, JsonResultBulkConfirmar.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultBulkConfirmar(new JsonResultBulkConfirmar(getOK(), getMensajeOk(), "", null, nroProcesados, procesados, nroNoProcesados, noProcesados));
        return responseOperacion;
    }


    /**
     * validateIniCuentaOtroBanco Validar el servicio iniciarCuentaOtroBanco
     *
     * @param requestBasic datos solicitud
     * @return ResponseOperacion validacion
     */
    public ResponseOperacion validateIniCuentaOtroBanco(JsonRequestCuentaOtroBanco requestBasic) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestBasic);
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido
            if (StringUtils.trimToNull(requestBasic.getTransactionID()) == null) {
                params.put(NOMBRECAMPO.name(), ValidaCampos.CODIGO_TRANSACCION);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getTipoDocumento()) == null) {
                params.put(NOMBRECAMPO.name(), TIPO_DOCUMENTO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getNroDocumento()) == null) {
                params.put(NOMBRECAMPO.name(), NRO_DOCUMENTO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getNombre()) == null) {
                params.put(NOMBRECAMPO.name(), NOMBRE);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getPaterno()) == null) {
                params.put(NOMBRECAMPO.name(), PATERNO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getNroCuenta()) == null) {
                params.put(NOMBRECAMPO.name(), NRO_CUENTA);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getBanco()) == null) {
                params.put(NOMBRECAMPO.name(), BANCO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar tamaño transaction ID
            if (requestBasic.getTransactionID().length() > getTamanioTransactionid()) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_TAMANO, null, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(requestBasic.getTransactionID());
            JsonResultComun x = getExisteOperacion(requestBasic.getTransactionID(), operacion, JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Validar cliente

            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestBasic.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_SIN_DATA, null, JsonResultComun.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);

            BeneficiarioJoven beneficiarioJoven = new BeneficiarioJoven();
            beneficiarioJoven.setTipoBeneficiario(TipoBeneficiario.CUENTA_OTRO_BANCO);
            beneficiarioJoven.setBancoBeneficiario(operacionesMojix.getBancoDestino(Long.parseLong(requestBasic.getBanco())));
            beneficiarioJoven.setNumeroCuenta(requestBasic.getNroCuenta());
            beneficiarioJoven.setMonedaCuenta(Short.valueOf(requestBasic.getMoneda()));
            beneficiarioJoven.setDepartamentoCuenta(requestBasic.getDptoCuenta());
            beneficiarioJoven.setNumeroTelefono(StringUtils.trimToEmpty(requestBasic.getTelefono()));
            beneficiarioJoven.setNumeroDocumento(StringUtils.trimToEmpty(requestBasic.getNroDocumento()));
            beneficiarioJoven.setTipoDocumento(StringUtils.trimToEmpty(requestBasic.getTipoDocumento()));
            beneficiarioJoven.setNombre(StringUtils.trimToEmpty(requestBasic.getNombre()));
            beneficiarioJoven.setPaterno(StringUtils.trimToEmpty(requestBasic.getPaterno()));
            beneficiarioJoven.setMaterno(StringUtils.trimToEmpty(requestBasic.getMaterno()));
            beneficiarioJoven.setRepiteEbisa(String.valueOf(requestBasic.iseBisa()));
            responseOperacion.setBeneficiarioJoven(beneficiarioJoven);
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateIniCuentaOtroBanco>", e);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultContactoBisa(new JsonResultComun(getOK(), getMensajeOk()));
        return responseOperacion;
    }

    /**
     * validateIniCuentaGiroMovil Validar el servicio iniciarGiroMovil
     *
     * @param requestBasic datos solicitud
     * @return ResponseOperacion validacion
     */
    public ResponseOperacion validateIniCuentaGiroMovil(JsonRequestCuentaGiroMovil requestBasic) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestBasic);
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido
            if (StringUtils.trimToNull(requestBasic.getTransactionID()) == null) {
                params.put(NOMBRECAMPO.name(), ValidaCampos.CODIGO_TRANSACCION);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getTipoDocumento()) == null) {
                params.put(NOMBRECAMPO.name(), TIPO_DOCUMENTO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getNroDocumento()) == null) {
                params.put(NOMBRECAMPO.name(), NRO_DOCUMENTO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getNombre()) == null) {
                params.put(NOMBRECAMPO.name(), NOMBRE);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            /*if (StringUtils.trimToNull(requestBasic.getPaterno()) == null) {
                params.put(NOMBRECAMPO.name(), PATERNO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }*/
            if (StringUtils.trimToNull(requestBasic.getTelefono()) == null) {
                params.put(NOMBRECAMPO.name(), NRO_TELEFONO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar tamaño transaction ID
            if (requestBasic.getTransactionID().length() > getTamanioTransactionid()) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_TAMANO, null, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(requestBasic.getTransactionID());
            JsonResultComun x = getExisteOperacion(requestBasic.getTransactionID(), operacion, JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Validar cliente

            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestBasic.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_SIN_DATA, null, JsonResultComun.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);

            BeneficiarioJoven beneficiarioJoven = new BeneficiarioJoven();
            beneficiarioJoven.setTipoBeneficiario(TipoBeneficiario.CUENTA_OTRO_BANCO);
            beneficiarioJoven.setBancoBeneficiario(operacionesMojix.getBancoDestino(BANCO_BISA));
            beneficiarioJoven.setNumeroCuenta(requestBasic.getTelefono());
            beneficiarioJoven.setNumeroTelefono(StringUtils.trimToEmpty(requestBasic.getTelefono()));
            beneficiarioJoven.setNumeroDocumento(StringUtils.trimToEmpty(requestBasic.getNroDocumento()));
            beneficiarioJoven.setTipoDocumento(StringUtils.trimToEmpty(requestBasic.getTipoDocumento()));
            beneficiarioJoven.setNombre(StringUtils.trimToEmpty(requestBasic.getNombre()));
            beneficiarioJoven.setPaterno(StringUtils.trimToEmpty(requestBasic.getPaterno()));
            beneficiarioJoven.setMaterno(StringUtils.trimToEmpty(requestBasic.getMaterno()));
            beneficiarioJoven.setRepiteEbisa(requestBasic.iseBisa() ? "SI" : "NO");
            responseOperacion.setBeneficiarioJoven(beneficiarioJoven);
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateIniCuentaGiroMovil>", e);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultContactoBisa(new JsonResultComun(getOK(), getMensajeOk()));
        return responseOperacion;
    }

    /**
     * validateEnviarSMS Validar el servicio enviar SMS
     *
     * @param requestBasic datos solicitud
     * @return ResponseOperacion validacion
     */
    public ResponseOperacion validateEnviarSMS(JsonRequestInvitacion requestBasic) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestBasic);
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido
            if (StringUtils.trimToNull(requestBasic.getTransactionID()) == null) {
                params.put(NOMBRECAMPO.name(), ValidaCampos.CODIGO_TRANSACCION);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getTipo()) == null) {
                params.put(NOMBRECAMPO.name(), TIPO_EVENTO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getNombre()) == null) {
                params.put(NOMBRECAMPO.name(), NOMBRE);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (requestBasic.getContactos() == null || requestBasic.getContactos().isEmpty()) {
                params.put(NOMBRECAMPO.name(), NRO_CONTACTOS);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }

            //Validar tamaño transaction ID
            if (requestBasic.getTransactionID().length() > getTamanioTransactionid()) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_TAMANO, null, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(requestBasic.getTransactionID());
            JsonResultComun x = getExisteOperacion(requestBasic.getTransactionID(), operacion, JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Validar cliente

            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestBasic.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_SIN_DATA, null, JsonResultComun.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateEnviarSMS>", e);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultContactoBisa(new JsonResultComun(getOK(), getMensajeOk()));
        return responseOperacion;
    }

    /**
     * validateEnviarClaveMovil Validar el servicio enviar Clave Movil
     *
     * @param requestBasic datos solicitud
     * @return ResponseOperacion validacion
     */
    public ResponseOperacion validateEnviarClaveMovil(JsonRequestClaveMovil requestBasic) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestBasic);
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido
            if (StringUtils.trimToNull(requestBasic.getTransactionID()) == null) {
                params.put(NOMBRECAMPO.name(), ValidaCampos.CODIGO_TRANSACCION);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestBasic.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar tamaño transaction ID
            if (requestBasic.getTransactionID().length() > getTamanioTransactionid()) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_TAMANO, null, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(requestBasic.getTransactionID());
            JsonResultComun x = getExisteOperacion(requestBasic.getTransactionID(), operacion, JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Validar cliente

            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestBasic.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestBasic.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_SIN_DATA, null, JsonResultComun.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);
            //Enviar clave movil
            String result = operacionesMojix.enviarClaveMovil(cliente.getTelefono());
            if (!getOK().equals(result)) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLAVEMOVIL, null, JsonResultComun.class));
                return responseOperacion;
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateEnviarClaveMovil>", e);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultContactoBisa(new JsonResultComun(getOK(), getMensajeOk()));
        return responseOperacion;
    }

    private <T> T getExisteConfirmacion(String transactionID, String confCode, String token, Class<T> Clazz) {
        Map<String, Object> params = new HashMap<>();
        //Valor requerido
        if (StringUtils.trimToNull(transactionID) == null) {
            params.put(NOMBRECAMPO.name(), ValidaCampos.CODIGO_TRANSACCION);
            return getResponse(ERROR_REQUERIDO, params, Clazz);
        }
        if (StringUtils.trimToNull(confCode) == null) {
            params.put(NOMBRECAMPO.name(), CODIGO_CONFIRMACION);
            return getResponse(ERROR_REQUERIDO, params, Clazz);
        }
        if (StringUtils.trimToNull(token) == null) {
            params.put(NOMBRECAMPO.name(), TOKEN_SMS);
            return getResponse(ERROR_REQUERIDO, params, Clazz);
        }
        return null;
    }

    private <T> T getExisteOperacion(String transactionID, Operacion operacion, Class<T> Clazz) {
        Map<String, Object> params = new HashMap<>();
        if (operacion != null) {
            //Validar transaction ID ANULADO.
            if (!EstadoOperacion.ACT.equals(operacion.getEstadoOperacion())) {
                params.put(TRANSID.name(), transactionID);
                return getResponse(ERROR_ANULADA, params, Clazz);
            }
            List<Solicitud> solicitudes = solicitudDao.getByOperacion(operacion);
            if (solicitudes != null && solicitudes.size() > 0) {
                for (Solicitud solicitud : solicitudes) {
                    //Validar si el transactionId ya fue registrado
                    if (USED.equals(solicitud.getTokenUsado())) {
                        params.put(TRANSID.name(), transactionID);
                        return getResponse(ERROR_TOKEN_EXPIRADO, params, Clazz);
                    } else if (NUSED.equals(solicitud.getTokenUsado())) {
                        //Validar Tiempo Token excedido
                        if (solicitud.isUltimaSolicitudDeClaveAntesDelHumbralEstablecido(getTiempoEntreClaves())
                                && isValidTiempoSolicitud()) {
                            params.put(SOLICITUDTOKEN.name(), getTiempoEntreClaves());
                            return getResponse(ERROR_TIEMPO_SOLICITUD, params, Clazz);
                        }
                    }
                }
            }
        }
        return null;
    }

    private <T> T getExisteOperacion(Operacion operacion, String transactionID, String confCode, String token, Class<T> Clazz) {
        Map<String, Object> params = new HashMap<>();
        if (operacion != null) {
            params.put(TOKENSMS.name(), token);
            params.put(CODCONFIRMACION.name(), confCode);
            //Validar transaction ID ANULADO.
            if (!EstadoOperacion.ACT.equals(operacion.getEstadoOperacion())) {
                params.put(TRANSID.name(), transactionID);
                return getResponse(ERROR_ANULADA, params, Clazz);
            }
            //Validar existencia cliente
            if (operacion.getCliente() == null) {
                return getResponse(ERROR_CONF_INVALIDA, params, Clazz);
            }
        } else {
            return getResponse(ERROR_CONF_INVALIDA, params, Clazz);
        }
        return null;
    }

    private <T> T getExisteSolicitud(Solicitud solicitud, String confCode, String token, Class<T> Clazz) throws NoSuchAlgorithmException {
        Map<String, Object> params = new HashMap<>();
        if (solicitud == null) {
            params.put(TOKENSMS.name(), token);
            params.put(CODCONFIRMACION.name(), confCode);
            return getResponse(ERROR_TOKEN_CONF, params, Clazz);
        }
        //Validar el código enviado a tu celular
        GeneradorPin generadorPin = new GeneradorPin();
        if (!generadorPin.verificar(token, solicitud.getTokenSMS())) {
            return getResponse(ERROR_CODIGO_SMS, null, Clazz);
        }
        return null;
    }

    private <T> T getExisteCuenta(ClienteJoven clienteJoven, String nroCuenta, Class<T> Clazz) {
        Long lnroCuenta = Long.parseLong(nroCuenta);
        if (clienteJoven != null) {
            ClienteCuentaDetalle clienteCuentaDetalle = clienteCuentaDetalleDao.getByCuentaCliente(lnroCuenta, clienteJoven.getCodigoCliente());
            if (clienteCuentaDetalle == null) {
                Map<String, Object> params = new HashMap<>();
                params.put(CUENTAINVALIDA.name(), nroCuenta);
                return getResponse(ERROR_CUENTA_INEXISTENTE, params, Clazz);
            }
            Cuenta cuenta = cuentaDao.getByNumero(clienteCuentaDetalle.getCuenta());
            if (cuenta == null) {
                Map<String, Object> params = new HashMap<>();
                params.put(CUENTAINVALIDA.name(), nroCuenta);
                return getResponse(ERROR_CUENTA_INEXISTENTE, params, Clazz);
            }
        }
        return null;
    }

    private String getValue(BigDecimal data) {
        if (data != null) {
            return data.toPlainString();
        }
        return null;
    }

    public Operacion setOperacion(Request request, Cliente cliente, TipoOperacion tipoOperacion, TipoProceso tipoProceso, String usuario) {
        Operacion operacion = operacionDao.getByTransactionID(request.getTransactionID());
        if (operacion == null) {
            operacion = operacionDao.newOperacion(tipoOperacion, tipoProceso);
            if (cliente != null) {
                operacion.setCliente(getCliente(cliente, usuario));
            }
            operacion.setTransaccionID(request.getTransactionID());
            operacion = operacionDao.registrarTipoOperacion(operacion, usuario);
        }
        return operacion;
    }

    public Solicitud setSolicitud(Request requestBasic, Response response, Operacion operacion, TipoSolicitud tipoProceso, String usuario) {
        Solicitud solicitud = solicitudDao.newSolicitud(tipoProceso);
        solicitud.setOperacion(operacion);
        solicitud.setCriterioBusqueda(requestBasic.toString());
        solicitud.setCodigoRespuesta(response.getStatusCode());
        solicitud.setMensaje(response.getMensaje());
        return solicitudDao.registrarOperacion(solicitud, usuario);
    }

    public Transferencia setTransferencia(Transfer transfer, Operacion operacion, boolean isValido, String usuario) {
        if (isValido) {
            Transferencia transferencia = transferenciaDao.newTransferencia(transfer.getGeoProperties());
            transferencia.setOperacion(operacion);
            transferencia.setCuentaOrigen(transfer.getCuentaOrigen());
            transferencia.setCuentaDestino(transfer.getCuentaDestino());
            transferencia.setMoneda(Short.parseShort(transfer.getMoneda()));
            transferencia.setMonto(transfer.getMonto().movePointLeft(2));
            transferencia.setMotivo(StringUtils.trimToNull(StringUtils.upperCase(transfer.getMotivo())));
            transferencia.setPeriodicidad(StringUtils.trimToNull(StringUtils.upperCase(transfer.getPeriodicidad())));
            return transferenciaDao.registrar(transferencia, usuario);
        }
        return null;
    }

    public void setTransferenciaBulk(TransferBulk transfer, Operacion operacion, boolean isValido, String usuario) {
        if (isValido) {
            List<CuentaTransferencia> cuentaTransferenciaList = transfer.getCuentas();
            if (!cuentaTransferenciaList.isEmpty()) {
                for (CuentaTransferencia cuentaTransferencia : cuentaTransferenciaList) {
                    try {
                        Transferencia transferencia = transferenciaDao.newTransferencia(transfer.getGeoProperties());
                        transferencia.setOperacion(operacion);
                        transferencia.setCuentaOrigen(transfer.getCuentaOrigen());
                        transferencia.setCuentaDestino(cuentaTransferencia.getCuentaDestino());
                        if (StringUtils.trimToNull(cuentaTransferencia.getMoneda()) != null) {
                            transferencia.setMoneda(Short.parseShort(cuentaTransferencia.getMoneda()));
                        }
                        if (cuentaTransferencia.getMonto() != null) {
                            transferencia.setMonto(cuentaTransferencia.getMonto().movePointLeft(2));
                        }
                        transferencia.setMotivo(StringUtils.trimToNull(StringUtils.upperCase(transfer.getMotivo())));
                        transferencia.setNumeroRef(cuentaTransferencia.getTransactionID());
                        transferenciaDao.registrar(transferencia, usuario);
                    } catch (Exception e) {
                        LOGGER.error("Hubo un error al procesar transaferencias bulk " + operacion, e);
                    }
                }
            }
        }
    }

    public void actualizarRespuesta(Response response, boolean isValido, Operacion operacion, Solicitud solicitud, String usuario) {
        if (isValido) {
            //Generar Clave
            try {
                generarClave(solicitud);
            } catch (NoSuchAlgorithmException e) {
                LOGGER.error("Error al generar el token ", e);
            }
            //Generar Codigo Confirmacion
            solicitud.setCodigoConfirmacion(getCodigoConf(solicitud.getId()));
            response.setConfCode(solicitud.getCodigoConfirmacion());
            solicitud.setResultadoBusqueda(response.toString());
            //Actualizar estado
            solicitud.setEstadoSolicitud(EstadoSolicitud.PEN);
        } else {
            solicitud.setResultadoBusqueda(response.toString());
            solicitud.setEstadoSolicitud(EstadoSolicitud.ERR);
            operacion.setEstadoOperacion(EstadoOperacion.ERR);
            operacionDao.actualizarTipoOperacion(operacion, usuario);
        }
        solicitudDao.actualizar(solicitud, usuario);
    }

    public Operacion setConfirmarOperacion(Request request, boolean isValido, String usuario) {
        Operacion operacion = operacionDao.getByTransactionID(request.getTransactionID());
        if (operacion != null && isValido) {
            operacion.setEstadoOperacion(EstadoOperacion.PRC);
            operacionDao.actualizarTipoOperacion(operacion, usuario);
        }
        return operacion;
    }

    public void setConfirmarSolicitud(Solicitud solicitud, boolean isValido, String usuario) {
        if (solicitud != null) {
            if (isValido) {
                solicitud.setTokenUsado(USED);
                solicitud.setEstadoSolicitud(EstadoSolicitud.PRC);
            } else {
                solicitud.setEstadoSolicitud(EstadoSolicitud.REC);
            }
            solicitudDao.actualizar(solicitud, usuario);
        }
    }

    public void actualizarRespuesta2(Response response, boolean isValido, Solicitud solicitud, String usuario) {
        if (isValido) {
            //Generar Codigo Confirmacion
            solicitud.setCodigoConfirmacion(getCodigoConf(solicitud.getId()));
            response.setConfCode(solicitud.getCodigoConfirmacion());
            solicitud.setResultadoBusqueda(response.toString());
            //Actualizar estado
            solicitud.setEstadoSolicitud(EstadoSolicitud.PRC);
        } else {
            solicitud.setResultadoBusqueda(response.toString());
            solicitud.setEstadoSolicitud(EstadoSolicitud.ERR);
        }
        solicitudDao.actualizar(solicitud, usuario);
    }

    public void actualizarRespuesta3(Response response, boolean isValido, Solicitud solicitud, String usuario) {
        if (isValido) {
            solicitud.setResultadoBusqueda(response.toString());
            solicitud.setEstadoSolicitud(EstadoSolicitud.PRC);
        } else {
            solicitud.setResultadoBusqueda(response.toString());
            solicitud.setEstadoSolicitud(EstadoSolicitud.ERR);
        }
        solicitudDao.actualizar(solicitud, usuario);
    }

    public void actualizarRespuestaInOne(boolean isValido, Response response, Operacion operacion, Solicitud solicitud, String usuario) {
        if (isValido) {
            //Generar Codigo Confirmacion
            solicitud.setCodigoConfirmacion(getCodigoConf(solicitud.getId()));
            response.setConfCode(solicitud.getCodigoConfirmacion());
            solicitud.setResultadoBusqueda(response.toString());
            //Actualizar estado
            solicitud.setEstadoSolicitud(EstadoSolicitud.PRC);
            operacion.setEstadoOperacion(EstadoOperacion.PRC);
        } else {
            solicitud.setResultadoBusqueda(response.toString());
            solicitud.setEstadoSolicitud(EstadoSolicitud.ERR);
            operacion.setEstadoOperacion(EstadoOperacion.ERR);
        }
        operacionDao.actualizarTipoOperacion(operacion, usuario);
        solicitudDao.actualizar(solicitud, usuario);
    }

    public void actualizarTransferencia(Transferencia transferencia, boolean isValido, String usuario) {
        if (transferencia != null) {
            if (isValido) {
                transferencia.setEstadoTransferencia(EstadoTransferencia.CONFIRMADA.getCodigo());
            } else {
                transferencia.setEstadoTransferencia(EstadoTransferencia.ERROR.getCodigo());
            }
            transferenciaDao.actualizar(transferencia, usuario);
        }
    }

    public void actualizarTransferencia2(Transferencia transferencia, boolean isValido, String usuario) {
        if (transferencia != null) {
            if (isValido) {
                transferencia.setEstadoTransferencia(EstadoTransferencia.AGENDADA.getCodigo());
            } else {
                transferencia.setEstadoTransferencia(EstadoTransferencia.ERROR.getCodigo());
            }
            transferenciaDao.actualizar(transferencia, usuario);
        }
    }

    public void actualizarTransferencia3(Transferencia transferencia, Operacion operacion, boolean isValido, String usuario) {
        if (transferencia != null) {
            transferencia.setOperacion(operacion);
            if (isValido) {
                transferencia.setEjecucion(new Date());
                transferencia.setEstadoTransferencia(EstadoTransferencia.AGENDADA_PROCESADA.getCodigo());
            } else {
                transferencia.setEstadoTransferencia(EstadoTransferencia.ERROR.getCodigo());
            }
            transferenciaDao.registrar(transferencia, usuario);
        }
    }

    public void actualizarTransferenciaBulk(ResponseOperacion responseOperacion, boolean isValido, String usuario) {
        if (isValido) {
            if (responseOperacion.getTransferencias() != null) {
                for (Transferencia transferencia : responseOperacion.getTransferencias()) {
                    if (transferencia.getNroCaja() != null) {
                        transferencia.setEstadoTransferencia(EstadoTransferencia.BULK_PROCESADA.getCodigo());
                    } else {
                        transferencia.setEstadoTransferencia(EstadoTransferencia.ERROR.getCodigo());
                    }
                    transferenciaDao.actualizar(transferencia, usuario);
                }
            }
        } else {
            if (responseOperacion.getTransferencias() != null) {
                for (Transferencia transferencia : responseOperacion.getTransferencias()) {
                    transferencia.setEstadoTransferencia(EstadoTransferencia.ERROR.getCodigo());
                    transferenciaDao.actualizar(transferencia, usuario);
                }
            }
        }
    }

    public long getTime() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Instant instant = timestamp.toInstant();
        return instant.toEpochMilli();
    }

    private boolean procesarTransferencia(ResponseOperacion responseOperacion, Transferencia transferencia) throws OperacionesMojixException {
        IRespuestasMonitor monitor;
        Cuenta cuentaOrigen = cuentaDao.getByNumero(Long.parseLong(transferencia.getCuentaOrigen()));
        Cuenta cuentaDestino = cuentaDao.getByNumero(Long.parseLong(transferencia.getCuentaDestino()));
        responseOperacion.setTransferencia(transferencia);
        //Procesar solicitud
        monitor = transaccionMonitorMojix.procesarTransferencia(transferencia, cuentaOrigen, cuentaDestino);
        if (monitor == null) {
            responseOperacion.setResultContactoBisa(getResponse(ERROR_MONITOR, null, JsonResultComun.class));
            return true;
        }
        BigDecimal numeroCaja = monitor.getDatoRecibidoCabeceraDecimal(MON_USUARIO);
        BigDecimal numeroSecuencial = monitor.getDatoRecibidoCabeceraDecimal(MON_NUMSEQ);
        String codigoPosteo = monitor.getDatoRecibidoCabeceraString(MON_POST);
        String mensaje = monitor.getDatoRecibidoCabeceraString(MON_REJ1);
        if ((PROCESADA.equalsIgnoreCase(codigoPosteo) && numeroCaja != null && numeroSecuencial != null)) {
            transferencia.setNroCaja(numeroCaja.longValue());
            transferencia.setNroSecuencia(numeroSecuencial.longValue());
        } else {
            if (cuentaSinFondo(StringUtils.trimToEmpty(mensaje))) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_SALDO_INSUFICIENTE, null, JsonResultComun.class));
            } else if (errorPCC01(StringUtils.trimToEmpty(mensaje))) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_PCC01, null, JsonResultComun.class));
            } else {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_NO_DEBITO, null, JsonResultComun.class));
            }
            return true;
        }
        return false;
    }

    private boolean realizoContrato(String numeroCliente) {
        return clienteJovenDao.existeContrato(numeroCliente) != null;
    }

    public ResponseOperacion validateIniGiroMovil(JsonRequestGiroMovil requestGiroMovil) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestGiroMovil);
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido
            if (StringUtils.trimToNull(requestGiroMovil.getTransactionID()) == null) {
                params.put(NOMBRECAMPO.name(), ValidaCampos.CODIGO_TRANSACCION);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestGiroMovil.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestGiroMovil.getCuentaOrigen()) == null) {
                params.put(NOMBRECAMPO.name(), CUENTA_ORIGEN);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestGiroMovil.getTelefono()) == null) {
                params.put(NOMBRECAMPO.name(), NRO_TELEFONO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestGiroMovil.getNombreCompleto()) == null) {
                params.put(NOMBRECAMPO.name(), NOMBRE);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestGiroMovil.getNroDocumento()) == null) {
                params.put(NOMBRECAMPO.name(), NRO_DOCUMENTO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestGiroMovil.getMoneda()) == null) {
                params.put(NOMBRECAMPO.name(), MONEDA);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (requestGiroMovil.getMonto() == null) {
                params.put(NOMBRECAMPO.name(), MONTO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestGiroMovil.getMotivo()) == null) {
                params.put(NOMBRECAMPO.name(), MOTIVO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (requestGiroMovil.getGeoProperties() == null) {
                params.put(NOMBRECAMPO.name(), LUGAR);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar tamaño transaction ID
            if (requestGiroMovil.getTransactionID().length() > getTamanioTransactionid()) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_TAMANO, null, JsonResultComun.class));
                return responseOperacion;
            }
            // Monto mayor a Cero.
            if (!(requestGiroMovil.getMonto().compareTo(BigDecimal.ZERO) > 0)) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_MONTO, null, JsonResultComun.class));
                return responseOperacion;
            }
            // Moneda valida
            if (!Monedas.esValida(requestGiroMovil.getMoneda())) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_MONEDA, null, JsonResultComun.class));
                return responseOperacion;
            }
            // Motivo es obligatorio
            if (requestGiroMovil.getMotivo().length() < getTamanioMotivo()) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_MOTIVO, null, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(requestGiroMovil.getTransactionID());
            JsonResultComun x = getExisteOperacion(requestGiroMovil.getTransactionID(), operacion, JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Validar clienteBancoBisa
            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestGiroMovil.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestGiroMovil.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestGiroMovil.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestGiroMovil.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_SIN_DATA, null, JsonResultComun.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);
            //Validar cuenta origen
            x = getExisteCuenta(clienteJoven, requestGiroMovil.getCuentaOrigen(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            Giro giro = new Giro();
            giro.setCelularBeneficiario(Long.parseLong(requestGiroMovil.getTelefono()));
            giro.setCuenta(requestGiroMovil.getCuentaOrigen());
            giro.setNombreCompletoBeneficiario(requestGiroMovil.getNombreCompleto());
            giro.setDocIdentidadBeneficiario(requestGiroMovil.getNroDocumento());
            giro.setTipoIdentidadBeneficiario(requestGiroMovil.getTipoDocumento());
            giro.setImporte(requestGiroMovil.getMonto().movePointLeft(2));
            giro.setMoneda(Short.valueOf(requestGiroMovil.getMoneda()));
            giro.setMotivo(requestGiroMovil.getMotivo());
            responseOperacion.setGiro(giro);

        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateIniGiroMovil>", e);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultContactoBisa(new JsonResultComun(getOK(), getMensajeOk()));
        return responseOperacion;
    }

    public ResponseOperacion validateConfIniGiroMovil(JsonRequestConfirmar requestConfirmar) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestConfirmar);
        try {
            //Validar datos requeridos
            JsonResultComun x = getExisteConfirmacion(requestConfirmar.getTransactionID(), requestConfirmar.getConfCode(), requestConfirmar.getTokenSMS(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(requestConfirmar.getTransactionID());
            x = getExisteOperacion(operacion, requestConfirmar.getTransactionID(), requestConfirmar.getConfCode(), requestConfirmar.getTokenSMS(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            //Obtener solicitud origen
            Solicitud solicitud = solicitudDao.getByConfirmacion(operacion, requestConfirmar.getConfCode());
            x = getExisteSolicitud(solicitud, requestConfirmar.getConfCode(), requestConfirmar.getTokenSMS(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            responseOperacion.setSolicitud(solicitud);
            //Giro Movil
            Cliente cliente = clienteDao.getByCodigoCliente(operacion.getCliente().getCodigoCliente());
            Giro giro = giroDao.getByOperacion(operacion);
            Cuenta cuentaOrigen = cuentaDao.getByNumero(Long.parseLong(StringUtils.trimToEmpty(giro.getCuenta())));
            String result = operacionesMojix.procesarGiroMovil(giro, cliente, cuentaOrigen, getCanal());
            if (!getOK().equals(result)) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_GIROMOVIL, null, JsonResultComun.class));
                return responseOperacion;
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateConfIniGiroMovil>", e);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultContactoBisa(new JsonResultComun(getOK(), getMensajeOk()));
        return responseOperacion;
    }

    public ResponseOperacion validateIniRecargaMovil(JsonRequestRecarga requestRecarga) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestRecarga);
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido
            if (StringUtils.trimToNull(requestRecarga.getTransactionID()) == null) {
                params.put(NOMBRECAMPO.name(), ValidaCampos.CODIGO_TRANSACCION);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestRecarga.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestRecarga.getCiudad()) == null) {
                params.put(NOMBRECAMPO.name(), CIUDAD_FACTURA);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestRecarga.getTelefono()) == null) {
                params.put(NOMBRECAMPO.name(), NRO_TELEFONO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestRecarga.getNumeroFactura()) == null) {
                params.put(NOMBRECAMPO.name(), NRO_FACTURA);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestRecarga.getNombreFactura()) == null) {
                params.put(NOMBRECAMPO.name(), NOMBRE_FACTURA);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestRecarga.getMoneda()) == null) {
                params.put(NOMBRECAMPO.name(), MONEDA);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (requestRecarga.getMonto() == null) {
                params.put(NOMBRECAMPO.name(), MONTO);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (StringUtils.trimToNull(requestRecarga.getCodigoAgencia()) == null) {
                params.put(NOMBRECAMPO.name(), AGENCIA_FACTURA);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            if (requestRecarga.getGeoProperties() == null) {
                params.put(NOMBRECAMPO.name(), LUGAR);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar tamaño transaction ID
            if (requestRecarga.getTransactionID().length() > getTamanioTransactionid()) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_TAMANO, null, JsonResultComun.class));
                return responseOperacion;
            }
            // Monto mayor a Cero.
            if (!(requestRecarga.getMonto().compareTo(BigDecimal.ZERO) > 0)) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_MONTO, null, JsonResultComun.class));
                return responseOperacion;
            }
            // Moneda valida
            if (!Monedas.esValida(requestRecarga.getMoneda())) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_MONEDA, null, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(requestRecarga.getTransactionID());
            JsonResultComun x = getExisteOperacion(requestRecarga.getTransactionID(), operacion, JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            responseOperacion.setOperacion(operacion);
            //Validar clienteBancoBisa
            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestRecarga.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestRecarga.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestRecarga.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestRecarga.getBisaClienteID());
                responseOperacion.setResultContactoBisa(getResponse(ERROR_SIN_DATA, null, JsonResultComun.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);
            //Validar cuenta origen
            x = getExisteCuenta(clienteJoven, requestRecarga.getNumeroCuenta(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateIniRecargaMovil>", e);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultContactoBisa(new JsonResultComun(getOK(), getMensajeOk()));
        return responseOperacion;
    }

    public ResponseOperacion validateConfIniRecargaMovil(JsonRequestConfirmar requestConfirmar) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestConfirmar);
        try {
            //Validar datos requeridos
            JsonResultComun x = getExisteConfirmacion(requestConfirmar.getTransactionID(), requestConfirmar.getConfCode(), requestConfirmar.getTokenSMS(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(requestConfirmar.getTransactionID());
            x = getExisteOperacion(operacion, requestConfirmar.getTransactionID(), requestConfirmar.getConfCode(), requestConfirmar.getTokenSMS(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            //Obtener solicitud origen
            Solicitud solicitud = solicitudDao.getByConfirmacion(operacion, requestConfirmar.getConfCode());
            x = getExisteSolicitud(solicitud, requestConfirmar.getConfCode(), requestConfirmar.getTokenSMS(), JsonResultComun.class);
            if (x != null) {
                responseOperacion.setResultContactoBisa(x);
                return responseOperacion;
            }
            responseOperacion.setSolicitud(solicitud);
            //Recarga Movil
            Cliente cliente = clienteDao.getByCodigoCliente(operacion.getCliente().getCodigoCliente());
            Recarga recarga = recargaDao.getByOperacion(operacion);
            Cuenta cuentaOrigen = cuentaDao.getByNumero(Long.parseLong(StringUtils.trimToEmpty(recarga.getCuenta())));
            String result = operacionesMojix.compraCredito(recarga, cliente, cuentaOrigen);
            if (!getOK().equals(result)) {
                responseOperacion.setResultContactoBisa(getResponse(ERROR_RECARGA, null, JsonResultComun.class));
                return responseOperacion;
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateConfIniRecargaMovil>", e);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultContactoBisa(new JsonResultComun(getOK(), getMensajeOk()));
        return responseOperacion;
    }

    public ResponseOperacion validateConsultaAgencias(JsonRequestInfo requestInfo) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestInfo);
        List<AgenciasMojix> agenciasMojixList;
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido BisaClienteID
            if (StringUtils.trimToNull(requestInfo.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultInfoAgencias(getResponse(ERROR_REQUERIDO, params, JsonResultInfoAgencias.class));
                return responseOperacion;
            }
            //Validar clienteBancoBisa
            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestInfo.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestInfo.getBisaClienteID());
                responseOperacion.setResultInfoAgencias(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultInfoAgencias.class));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestInfo.getBisaClienteID());
                responseOperacion.setResultInfoAgencias(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultInfoAgencias.class));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestInfo.getBisaClienteID());
                responseOperacion.setResultInfoAgencias(getResponse(ERROR_SIN_DATA, null, JsonResultInfoAgencias.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);
            //Obtener agencias
            agenciasMojixList = operacionesMojix.getAgencias();

        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateConsultaAgencias>", e);
            responseOperacion.setResultInfoAgencias(getResponse(ERROR_PROCESO, null, JsonResultInfoAgencias.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultInfoAgencias(new JsonResultInfoAgencias(getOK(), getMensajeOk(), agenciasMojixList));
        return responseOperacion;
    }

    public ResponseOperacion validateConsultaTelefonicas(JsonRequestInfo requestInfo) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestInfo);
        List<EmpresaTelefonicaMojix> empresaTelefonicaMojixList;
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido BisaClienteID
            if (StringUtils.trimToNull(requestInfo.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultInfoTelefonicas(getResponse(ERROR_REQUERIDO, params, JsonResultInfoTelefonicas.class));
                return responseOperacion;
            }
            //Validar clienteBancoBisa
            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestInfo.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestInfo.getBisaClienteID());
                responseOperacion.setResultInfoTelefonicas(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultInfoTelefonicas.class));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestInfo.getBisaClienteID());
                responseOperacion.setResultInfoTelefonicas(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultInfoTelefonicas.class));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestInfo.getBisaClienteID());
                responseOperacion.setResultInfoTelefonicas(getResponse(ERROR_SIN_DATA, null, JsonResultInfoTelefonicas.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);
            //Obtener empresas telefonicas
            empresaTelefonicaMojixList = operacionesMojix.getEmpresasTelefonicas();

        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateConsultaTelefonicas>", e);
            responseOperacion.setResultInfoTelefonicas(getResponse(ERROR_PROCESO, null, JsonResultInfoTelefonicas.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultInfoTelefonicas(new JsonResultInfoTelefonicas(getOK(), getMensajeOk(), empresaTelefonicaMojixList));
        return responseOperacion;
    }

    public ResponseOperacion validateConsultaBancos(JsonRequestInfo requestInfo) {
        ResponseOperacion responseOperacion = new ResponseOperacion(requestInfo);
        List<EntidadFinancieraMojix> entidadFinancieraMojixList;
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido BisaClienteID
            if (StringUtils.trimToNull(requestInfo.getBisaClienteID()) == null) {
                params.put(NOMBRECAMPO.name(), BISA_CLIENTE_ID);
                responseOperacion.setResultInfoBancos(getResponse(ERROR_REQUERIDO, params, JsonResultInfoBancos.class));
                return responseOperacion;
            }
            //Validar clienteBancoBisa
            //Validar Cliente Segmento Joven
            ClienteJoven clienteJoven = clienteJovenDao.getByCodigoCifrado(requestInfo.getBisaClienteID());
            if (clienteJoven == null) {
                params.put(CODCLIENTE.name(), requestInfo.getBisaClienteID());
                responseOperacion.setResultInfoBancos(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultInfoBancos.class));
                return responseOperacion;
            }
            //Validar cliente en CORE
            Cliente cliente = clienteDao.getByCodigoCliente(clienteJoven.getCodigoCliente());
            if (cliente == null) {
                params.put(CODCLIENTE.name(), requestInfo.getBisaClienteID());
                responseOperacion.setResultInfoBancos(getResponse(ERROR_CLIENTE_CODIGOINVALID, params, JsonResultInfoBancos.class));
                return responseOperacion;
            }
            //Validar datos cliente
            ClienteNatural clienteNatural = clienteNaturalDao.getDatosCliente(cliente.getCodCliente());
            if (clienteNatural == null) {
                params.put(CODCLIENTE.name(), requestInfo.getBisaClienteID());
                responseOperacion.setResultInfoBancos(getResponse(ERROR_SIN_DATA, null, JsonResultInfoBancos.class));
                return responseOperacion;
            }
            responseOperacion.setCliente(cliente);
            responseOperacion.setClienteJoven(clienteJoven);
            responseOperacion.setClienteNatural(clienteNatural);
            //Obtener agencias
            entidadFinancieraMojixList = operacionesMojix.getBancos();

        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateConsultaAgencias>", e);
            responseOperacion.setResultInfoBancos(getResponse(ERROR_PROCESO, null, JsonResultInfoBancos.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultInfoBancos(new JsonResultInfoBancos(getOK(), getMensajeOk(), entidadFinancieraMojixList));
        return responseOperacion;
    }

    public ResponseOperacion validateConsultaTransaccion(JsonRequestTransaccion request) {
        ResponseOperacion responseOperacion = new ResponseOperacion(request);
        Solicitud solicitud;
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido BisaClienteID
            if (StringUtils.trimToNull(request.getTransactionID()) == null) {
                params.put(NOMBRECAMPO.name(), CODIGO_TRANSACCION);
                responseOperacion.setResultContactoBisa(getResponse(ERROR_REQUERIDO, params, JsonResultComun.class));
                return responseOperacion;
            }
            //Validar operacion y solicitud
            Operacion operacion = operacionDao.getByTransactionID(request.getTransactionID());
            responseOperacion.setOperacion(operacion);
            //Obtener solicitud origen
            solicitud = solicitudDao.getByConsultaTrans(operacion);
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateConsultaTransaccion>", e);
            responseOperacion.setResultContactoBisa(getResponse(ERROR_PROCESO, null, JsonResultComun.class));
            return responseOperacion;
        }
        responseOperacion.setValido(true);
        responseOperacion.setResultContactoBisa(new JsonResultComun(solicitud.getCodigoRespuesta(), solicitud.getCodigoConfirmacion(), solicitud.getMensaje()));
        return responseOperacion;
    }

    public void setBeneficiario(BeneficiarioJoven beneficiarioJoven, Operacion operacion, boolean valido, String usuario) {
        if (valido && beneficiarioJoven != null) {
            beneficiarioJoven.setOperacion(operacion);
            beneficiarioJoven.setPersona(operacion.getCliente());
            beneficiarioJoven.setEstadoBeneficiario(EstadoBeneficiario.INA);
            beneficiarioJovenDao.registrarBeneficiario(beneficiarioJoven, usuario);
        }
    }

    public void actualizarBeneficiario(Operacion operacion, boolean eBisa, boolean valido, String usuario) {
        if (valido && operacion != null) {
            BeneficiarioJoven beneficiarioJoven = beneficiarioJovenDao.getByOperacion(operacion);
            if (beneficiarioJoven != null) {
                ClienteJoven clienteJoven = clienteJovenDao.find(beneficiarioJoven.getPersona().getId());
                String codCliente = clienteJoven.getCodigoCliente();

                beneficiarioJoven.setEstadoBeneficiario(EstadoBeneficiario.ACT);
                beneficiarioJoven.setRepiteEbisa(eBisa ? "SI" : "NO");
                beneficiarioJoven = beneficiarioJovenDao.actualizarBeneficiario(beneficiarioJoven, usuario);
                if (eBisa) {
                    operacionesMojix.setBeneficiario(beneficiarioJoven, codCliente, usuario);
                }
            }

        }
    }

    public void actualizarBeneficiario(Operacion operacion, boolean valido, String usuario) {
        if (valido && operacion != null) {
            BeneficiarioJoven beneficiarioJoven = beneficiarioJovenDao.getByOperacion(operacion);
            if (beneficiarioJoven != null) {
                ClienteJoven clienteJoven = clienteJovenDao.find(beneficiarioJoven.getPersona().getId());
                String codCliente = clienteJoven.getCodigoCliente();

                beneficiarioJoven.setEstadoBeneficiario(EstadoBeneficiario.ACT);
                beneficiarioJoven = beneficiarioJovenDao.actualizarBeneficiario(beneficiarioJoven, usuario);
                if ("SI".equals(StringUtils.trimToEmpty(beneficiarioJoven.getRepiteEbisa()))) {
                    operacionesMojix.setBeneficiario(beneficiarioJoven, codCliente, usuario);
                }
            }
        }
    }

    public void setGiro(Giro giro, Operacion operacion, boolean valido, String usuario) {
        if (valido && giro != null) {
            giro.setOperacion(operacion);
            giro.setEstadoGiro(EstadoGiro.INA);
            giroDao.registrar(giro, usuario);
        }
    }

    public void setRecarga(JsonRequestRecarga requestRecarga, Operacion operacion, boolean valido, String usuario) {
        if (valido) {
            Recarga recarga = recargaDao.newRecarga(requestRecarga.getGeoProperties());
            recarga.setOperacion(operacion);
            recarga.setCuenta(requestRecarga.getNumeroCuenta());
            recarga.setMoneda(Short.parseShort(requestRecarga.getMoneda()));
            recarga.setImporte(requestRecarga.getMonto().movePointLeft(2));
            recarga.setCelularBeneficiario(Long.parseLong(requestRecarga.getTelefono()));
            recarga.setEmpresa(requestRecarga.getCodigoEmpresaTelefonica());
            //factura
            recarga.setNombreCompletoFactura(requestRecarga.getNombreFactura());
            recarga.setDocIdentidadFactura(requestRecarga.getNumeroFactura());
            recarga.setAgenciaFactura(Integer.parseInt(requestRecarga.getCodigoAgencia()));
            recarga.setCiudadFactura(requestRecarga.getCiudad());

            recargaDao.registrar(recarga, usuario);
        }
    }
}

