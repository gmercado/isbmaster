package com.bisa.bus.servicios.mojix.model;

/**
 * @author by josanchez on 14/07/2016.
 */
public enum EstadoSolicitud {

    PRC("PROCESADO"),
    ERR("ERROR"),
    ANU("ANULADO"),
    PEN("PENDIENTE"),
    REC("RECHAZADO");

    private String descripcion;

    EstadoSolicitud(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
