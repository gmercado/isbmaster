package com.bisa.bus.servicios.mojix.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 16/08/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactoMojix implements Serializable {

    private String nroCelular;

    public ContactoMojix() {
    }

    public ContactoMojix(String nroCelular) {
        setNroCelular(nroCelular);
    }

    public String getNroCelular() {
        return nroCelular;
    }

    public void setNroCelular(String nroCelular) {
        this.nroCelular = nroCelular;
    }

    @Override
    public String toString() {
        return "Contacto{" +
                "nroCelular='" + nroCelular + '\'' +
                '}';
    }
}
