package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Geografica;
import com.bisa.bus.servicios.mojix.model.Request;
import com.bisa.bus.servicios.mojix.model.Transfer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by josanchez on 22/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestTransferencia implements Serializable, Request, Transfer {

    private String bisaClienteID;
    private String cuentaOrigen;
    private String cuentaDestino;
    private String moneda;
    private BigDecimal monto;
    private String motivo;
    private String transactionID;
    private Geografica geoProperties;

    public JsonRequestTransferencia() {
    }

    public JsonRequestTransferencia(String bisaClienteID, String cuentaOrigen, String cuentaDestino, String moneda, BigDecimal monto, String motivo, String transactionID, Geografica geoProperties) {
        setBisaClienteID(bisaClienteID);
        setCuentaOrigen(cuentaOrigen);
        setCuentaDestino(cuentaDestino);
        setMoneda(moneda);
        setMonto(monto);
        setMotivo(motivo);
        setTransactionID(transactionID);
        setGeoProperties(geoProperties);
    }

    public String getBisaClienteID() {
        return bisaClienteID;
    }

    public void setBisaClienteID(String bisaClienteID) {
        this.bisaClienteID = bisaClienteID;
    }

    public String getCuentaOrigen() {
        return cuentaOrigen;
    }

    public void setCuentaOrigen(String cuentaOrigen) {
        this.cuentaOrigen = cuentaOrigen;
    }

    public String getCuentaDestino() {
        return cuentaDestino;
    }

    public void setCuentaDestino(String cuentaDestino) {
        this.cuentaDestino = cuentaDestino;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @Override
    public String getPeriodicidad() {
        return null;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public Geografica getGeoProperties() {
        return geoProperties;
    }

    public void setGeoProperties(Geografica geoProperties) {
        this.geoProperties = geoProperties;
    }

    @Override
    public String toString() {
        return "JsonRequestTransferencia{" +
                "bisaClienteID='" + bisaClienteID + '\'' +
                ", cuentaOrigen='" + cuentaOrigen + '\'' +
                ", cuentaDestino='" + cuentaDestino + '\'' +
                ", moneda='" + moneda + '\'' +
                ", monto=" + monto +
                ", motivo='" + motivo + '\'' +
                ", transactionID='" + transactionID + '\'' +
                ", geoProperties=" + geoProperties +
                '}';
    }
}
