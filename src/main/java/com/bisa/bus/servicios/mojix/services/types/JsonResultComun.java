package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.CodigosConfirmacion;
import com.bisa.bus.servicios.mojix.model.Response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonResultComun implements Serializable, Response {

    private String statusCode;
    private String confCode;
    private String mensaje;

    public JsonResultComun() {
    }

    public JsonResultComun(String statusCode, CodigosConfirmacion confCode, String mensaje) {
        setStatusCode(statusCode);
        setConfCode(confCode.getCodigo());
        setMensaje(mensaje);
    }

    public JsonResultComun(String statusCode, String confCode, String mensaje) {
        setStatusCode(statusCode);
        setConfCode(confCode);
        setMensaje(mensaje);
    }

    public JsonResultComun(String statusCode, String mensaje) {
        setStatusCode(statusCode);
        setConfCode(null);
        setMensaje(mensaje);
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getConfCode() {
        return confCode;
    }

    public void setConfCode(String confCode) {
        this.confCode = confCode;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "JsonResultContactoBisa{" +
                "statusCode='" + getStatusCode() + '\'' +
                ", confCode='" + getConfCode() + '\'' +
                ", mensaje='" + getMensaje() + '\'' +
                '}';
    }
}
