package com.bisa.bus.servicios.mojix.rest;

import com.bisa.bus.servicios.mojix.services.ClienteMojixService;
import com.bisa.bus.servicios.mojix.services.types.*;
import com.google.inject.Inject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * @author by josanchez on 11/07/2016.
 * @author by rsalvatierra on 11/10/2016
 */
@Path("/sjoven/Cliente")
@Produces({"application/json;charset=UTF-8"})
public class ClienteService extends CommonService {

    private static final String INI_CLIENTE = "/iniCliente";
    private static final String CONF_CLIENTE = "/confCliente";
    private static final String INI_CONTACTO = "/iniContacto";
    private static final String CONF_CONTACTO = "/confContacto";
    private static final String CLIENTE = "/cliente";
    private static final String INI_CUENTA_BANCO_BISA = "/iniCuentaBancoBisa";
    private static final String CONF_CUENTA_BANCO_BISA = "/confCuentaBancoBisa";
    private static final String INI_CUENTA_OTRO_BANCO = "/iniCuentaOtroBanco";
    private static final String CONF_CUENTA_OTRO_BANCO = "/confCuentaOtroBanco";
    private static final String INI_CUENTA_GIRO_MOBILE = "/iniCuentaGiroMobile";
    private static final String CONF_CUENTA_GIRO_MOBILE = "/confCuentaGiroMobile";
    private static final String ENVIAR_SMS = "/enviarSMS";
    private static final String CAJERO_SIN_TARJETA = "/cajeroSinTarjeta";

    @Inject
    private ClienteMojixService clienteMojixService;

    /**
     * El Servicio permite verificar si el Cliente existe en el BISA Core
     *
     * @param httpHeaders  headers
     * @param requestBasic datos basicos
     * @return JsonResultBisa
     */
    @POST
    @Path(INI_CLIENTE)
    public JsonResultBisa iniciarCliente(@Context HttpHeaders httpHeaders, JsonRequestBasic requestBasic) {
        if (requestBasic == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return clienteMojixService.iniciarCliente(requestBasic, getUser(httpHeaders));
        }
    }

    /**
     * Confirma el TokenSMS que permite verificar al cliente
     *
     * @param httpHeaders      headers
     * @param requestConfirmar datos basicos
     * @return JsonResultClienteBisa
     */
    @POST
    @Path(CONF_CLIENTE)
    public JsonResultClienteBisa confirmarCliente(@Context HttpHeaders httpHeaders, JsonRequestConfirmar requestConfirmar) {
        if (requestConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return clienteMojixService.confirmarCliente(requestConfirmar, getUser(httpHeaders));
        }
    }

    /**
     * Permite Adicionar Beneficiarios de la Aplicación
     *
     * @param httpHeaders     headers
     * @param requestContacto datos basicos
     * @return JsonResultContactoBisa
     */
    @POST
    @Path(INI_CONTACTO)
    public JsonResultComun iniciarContacto(@Context HttpHeaders httpHeaders, JsonRequestContacto requestContacto) {
        if (requestContacto == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return clienteMojixService.iniciarContacto(requestContacto, getUser(httpHeaders));
        }
    }

    /**
     * Permite confirmar el Token SMS para adicionar un contacto de la Aplicación
     *
     * @param httpHeaders      headers
     * @param requestConfirmar datos basicos
     * @return JsonResultContactoBisa
     */
    @POST
    @Path(CONF_CONTACTO)
    public JsonResultComun confirmarContacto(@Context HttpHeaders httpHeaders, JsonRequestConfirmar requestConfirmar) {
        if (requestConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return clienteMojixService.confirmarContacto(requestConfirmar, getUser(httpHeaders));
        }
    }

    /**
     * El servicio permite adicionar y verificar un cliente que se adiciona desde el BackOffice
     *
     * @param httpHeaders  headers
     * @param requestBasic datos basicos
     * @return JsonResultClienteBisa
     */
    @POST
    @Path(CLIENTE)
    public JsonResultClienteBisa adicionaCliente(@Context HttpHeaders httpHeaders, JsonRequestBasic requestBasic) {
        if (requestBasic == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return clienteMojixService.adicionaCliente(requestBasic, getUser(httpHeaders));
        }
    }

    /**
     * Permite solicitar Token SMS para adicionar una cuenta BISA a la aplicación
     *
     * @param httpHeaders        headers
     * @param requestCuentaBanco datos basicos
     * @return JsonResultCuentaBanco
     */
    @POST
    @Path(INI_CUENTA_BANCO_BISA)
    public JsonResultCuentaBanco iniCuentaBancoBisa(@Context HttpHeaders httpHeaders, JsonRequestCuentaBanco requestCuentaBanco) {
        if (requestCuentaBanco == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return clienteMojixService.iniCuentaBancoBisa(requestCuentaBanco, getUser(httpHeaders));
        }

    }

    /**
     * Permite confirmar el Token SMS para adicionar una cuenta BISA a la aplicación
     *
     * @param httpHeaders      headers
     * @param requestConfirmar datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(CONF_CUENTA_BANCO_BISA)
    public JsonResultComun confirmarCuentaBancoBisa(@Context HttpHeaders httpHeaders, JsonRequestConfirmarCuenta requestConfirmar) {
        if (requestConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return clienteMojixService.confirmarCuentaBancoBisa(requestConfirmar, getUser(httpHeaders));
        }
    }

    /**
     * Permite solicitar Token SMS para adicionar una cuenta de otro Banco a la aplicación
     *
     * @param httpHeaders            headers
     * @param requestCuentaOtroBanco datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(INI_CUENTA_OTRO_BANCO)
    public JsonResultComun iniCuentaOtroBanco(@Context HttpHeaders httpHeaders, JsonRequestCuentaOtroBanco requestCuentaOtroBanco) {
        if (requestCuentaOtroBanco == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return clienteMojixService.iniCuentaOtroBanco(requestCuentaOtroBanco, getUser(httpHeaders));
        }

    }

    /**
     * Permite confirmar el Token SMS para adicionar una cuenta de otro Banco a la aplicación
     *
     * @param httpHeaders      headers
     * @param requestConfirmar datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(CONF_CUENTA_OTRO_BANCO)
    public JsonResultComun confirmarCuentaOtroBanco(@Context HttpHeaders httpHeaders, JsonRequestConfirmar requestConfirmar) {
        if (requestConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return clienteMojixService.confirmarCuentaOtroBanco(requestConfirmar, getUser(httpHeaders));
        }
    }

    /**
     * Permite solicitar Token SMS para adicionar una cuenta para giro movil a la aplicación
     *
     * @param httpHeaders            headers
     * @param requestCuentaGiroMovil datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(INI_CUENTA_GIRO_MOBILE)
    public JsonResultComun iniCuentaGiroMovil(@Context HttpHeaders httpHeaders, JsonRequestCuentaGiroMovil requestCuentaGiroMovil) {
        if (requestCuentaGiroMovil == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return clienteMojixService.iniCuentaGiroMovil(requestCuentaGiroMovil, getUser(httpHeaders));
        }

    }

    /**
     * Permite confirmar el Token SMS para adicionar una cuenta de giro movil a la aplicación
     *
     * @param httpHeaders      headers
     * @param requestConfirmar datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(CONF_CUENTA_GIRO_MOBILE)
    public JsonResultComun confirmarCuentaGiroMovil(@Context HttpHeaders httpHeaders, JsonRequestConfirmar requestConfirmar) {
        if (requestConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return clienteMojixService.confirmarCuentaGiroMovil(requestConfirmar, getUser(httpHeaders));
        }
    }

    /**
     * El servicio permite enviar un mensaje SMS a un contacto que no tiene la aplicacion
     *
     * @param httpHeaders       headers
     * @param requestInvitacion datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(ENVIAR_SMS)
    public JsonResultComun enviarSMS(@Context HttpHeaders httpHeaders, JsonRequestInvitacion requestInvitacion) {
        if (requestInvitacion == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return clienteMojixService.enviarSMS(requestInvitacion, getUser(httpHeaders));
        }
    }

    /**
     * El servicio permite enviar clave movil para los servicios sin tarjeta (CAJERO/POS)
     *
     * @param httpHeaders       headers
     * @param requestClaveMovil datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(CAJERO_SIN_TARJETA)
    public JsonResultComun enviarClaveMovil(@Context HttpHeaders httpHeaders, JsonRequestClaveMovil requestClaveMovil) {
        if (requestClaveMovil == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return clienteMojixService.enviarClaveMovil(requestClaveMovil, getUser(httpHeaders));
        }
    }
}
