package com.bisa.bus.servicios.mojix.entities;

import bus.database.model.EntityBase;
import bus.interfaces.token.TresClaves;
import com.bisa.bus.servicios.mojix.model.EstadoSolicitud;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author by josanchez on 12/07/2016.
 * @author rsalvatierra modificado on 20/10/2016
 */
@Entity
@Table(name = "SJOV01")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S01FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S01USRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S01FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S01USRMOD"))
})
public class Solicitud extends EntityBase implements Serializable, TresClaves {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(Solicitud.class);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S01CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "S01CODOPE")
    private Operacion operacion;

    @Column(name = "S01TIPROC")
    private int tipoProceso;

    @Column(name = "S01CDRESP", columnDefinition = "VARCHAR(5)")
    private String codigoRespuesta;

    @Column(name = "S01CDCONF", columnDefinition = "VARCHAR(64)")
    private String codigoConfirmacion;

    @Enumerated(EnumType.STRING)
    @Column(name = "S01ESTOPE")
    private EstadoSolicitud estadoSolicitud;

    @Column(name = "S01CRIBUS", columnDefinition = "CLOB")
    private String criterioBusqueda;

    @Column(name = "S01RESBUS", columnDefinition = "CLOB")
    private String resultadoBusqueda;

    @Column(name = "S01MENSAJ", columnDefinition = "CHAR(255)")
    private String mensaje;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "S01FECSOL", columnDefinition = "TIMESTAMP")
    private Date fechaSolicitudToken;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "S01TMPVDA", columnDefinition = "TIMESTAMP")
    private Date tiempoVida;

    @Column(name = "S01TUSADO", columnDefinition = "CHAR(1)")
    private String tokenUsado;

    @Column(name = "S01TKNSMS", columnDefinition = "VARCHAR(64)")
    private String tokenSMS;

    @Transient
    private String pin;

    public Solicitud() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTipoProceso() {
        return tipoProceso;
    }

    public void setTipoProceso(int tipoProceso) {
        this.tipoProceso = tipoProceso;
    }

    public String getCriterioBusqueda() {
        return criterioBusqueda;
    }

    public void setCriterioBusqueda(String criterioBusqueda) {
        this.criterioBusqueda = criterioBusqueda;
    }

    public String getResultadoBusqueda() {
        return resultadoBusqueda;
    }

    public void setResultadoBusqueda(String resultadoBusqueda) {
        this.resultadoBusqueda = resultadoBusqueda;
    }

    public EstadoSolicitud getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(EstadoSolicitud estadoOperacion) {
        this.estadoSolicitud = estadoOperacion;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getCodigoRespuesta() {
        return codigoRespuesta;
    }

    public void setCodigoRespuesta(String codigoRespuesta) {
        this.codigoRespuesta = codigoRespuesta;
    }


    public String getCodigoConfirmacion() {
        return codigoConfirmacion;
    }

    public void setCodigoConfirmacion(String codigoConfirmacion) {
        this.codigoConfirmacion = codigoConfirmacion;
    }

    @Override
    public Date getFechaSolicitudToken() {
        return fechaSolicitudToken;
    }

    @Override
    public void setFechaSolicitudToken(Date fechaSolicitudToken) {
        this.fechaSolicitudToken = fechaSolicitudToken;
    }

    @Override
    public String getTokenSMS() {
        return tokenSMS;
    }

    @Override
    public void setTokenSMS(String tokenSMS) {
        this.tokenSMS = tokenSMS;
    }

    public String getTokenUsado() {
        return tokenUsado;
    }

    public void setTokenUsado(String tokenUsado) {
        this.tokenUsado = tokenUsado;
    }

    public Date getTiempoVida() {
        return tiempoVida;
    }

    public void setTiempoVida(Date tiempoVida) {
        this.tiempoVida = tiempoVida;
    }

    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }


    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public boolean isUltimaSolicitudDeClaveAntesDelHumbralEstablecido(int segundosEntreSolicitudClave) {
        if (getFechaSolicitudToken() == null) {
            return false;
        }
        DateTime ahora = new DateTime();
        DateTime dtFechaSolicitud = new DateTime(getFechaSolicitudToken());
        LOGGER.info("Ultima fecha generacion de clave + {} segundos -> {} ahora -> {}, pasaron {} segundos",
                segundosEntreSolicitudClave,
                new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:SSS").format(dtFechaSolicitud.plusSeconds(segundosEntreSolicitudClave).toDate()),
                new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:SSS").format(ahora.toDate()),
                ahora.minus(dtFechaSolicitud.toDate().getTime()).toDate().getTime() / 1000);
        return ahora.isBefore(dtFechaSolicitud.plusSeconds(segundosEntreSolicitudClave));
    }

    @Override
    public String toString() {
        return "OperacionesMojix{" +
                "id=" + id +
                ", tipoProceso=" + tipoProceso +
                ", criterioBusqueda='" + criterioBusqueda + '\'' +
                ", resultadoBusqueda='" + resultadoBusqueda + '\'' +
                ", estadoOperacion='" + estadoSolicitud + '\'' +
                ", mensaje='" + mensaje + '\'' +
                ", codigoRespuesta='" + codigoRespuesta + '\'' +
                ", codigoConfirmacion='" + codigoConfirmacion + '\'' +
                ", tipoOperacion=" + operacion +
                '}';
    }
}


