package com.bisa.bus.servicios.mojix.model;

/**
 * @author by rsalvatierra on 03/07/2017.
 */
public enum EstadoGiro {
    INA,
    ACT
}
