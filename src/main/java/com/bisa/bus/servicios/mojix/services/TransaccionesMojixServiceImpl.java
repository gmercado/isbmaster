package com.bisa.bus.servicios.mojix.services;

import com.bisa.bus.servicios.mojix.api.ProcesosMojix;
import com.bisa.bus.servicios.mojix.api.ResponseOperacion;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.bisa.bus.servicios.mojix.entities.Solicitud;
import com.bisa.bus.servicios.mojix.entities.Transferencia;
import com.bisa.bus.servicios.mojix.model.TipoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoSolicitud;
import com.bisa.bus.servicios.mojix.services.types.*;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.bisa.bus.servicios.mojix.model.TipoProceso.*;

/**
 * @author by josanchez on 14/07/2016.
 * @author rsalvatierra on 04/10/2016
 */
public class TransaccionesMojixServiceImpl implements TransaccionesMojixService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransaccionesMojixServiceImpl.class);
    private final ProcesosMojix procesosMojix;

    @Inject
    public TransaccionesMojixServiceImpl(ProcesosMojix procesosMojix) {
        this.procesosMojix = procesosMojix;
    }

    @Override
    public JsonResultComun iniciarTransferencia(JsonRequestTransferencia request, String usuario) {
        LOGGER.debug("Inicio <iniciarTransferencia>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateIniTransferencia(request);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.TRF, transferencia, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_TRANSFERENCIA, usuario);
        //Registrar Transferencia
        procesosMojix.setTransferencia(request, operacion, responseOperacion.isValido(), usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta(responseOperacion.getResponse(), responseOperacion.isValido(), operacion, solicitud, usuario);
        //Enviar SMS
        procesosMojix.enviarSMS(responseOperacion.getCliente(), solicitud, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <iniciarTransferencia>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun confirmarTransferencia(JsonRequestConfirmar request, String usuario) {
        LOGGER.debug("Inicio <confirmarTransferencia>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConfIniTransferencia(request);
        //Confirmar Operacion
        Operacion operacion = procesosMojix.setConfirmarOperacion(responseOperacion.getRequest(), responseOperacion.isValido(), usuario);
        //Confirmar Solicitud
        procesosMojix.setConfirmarSolicitud(responseOperacion.getSolicitud(), responseOperacion.isValido(), usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONFTRANSFERENCIA, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta2(responseOperacion.getResponse(), responseOperacion.isValido(), solicitud, usuario);
        //Actualizar transferencia
        procesosMojix.actualizarTransferencia(responseOperacion.getTransferencia(), responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <confirmarTransferencia>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun iniciarTransferenciaProgramada(JsonRequestTransfProgramada request, String usuario) {
        LOGGER.debug("Inicio <iniciarTransferenciaProgramada>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateIniTransferenciaProgramada(request);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.TRF, transferenciaPRG, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_PROGTRANSFERENCIA, usuario);
        //Registrar Transferencia
        procesosMojix.setTransferencia(request, operacion, responseOperacion.isValido(), usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta(responseOperacion.getResponse(), responseOperacion.isValido(), operacion, solicitud, usuario);
        //Enviar SMS
        procesosMojix.enviarSMS(responseOperacion.getCliente(), solicitud, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <iniciarTransferenciaProgramada>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun confirmarTransferenciaProgramada(JsonRequestConfirmar request, String usuario) {
        LOGGER.debug("Inicio <confirmarTransferenciaProgramada>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConfIniTransferenciaProgramada(request);
        //Confirmar Operacion
        Operacion operacion = procesosMojix.setConfirmarOperacion(responseOperacion.getRequest(), responseOperacion.isValido(), usuario);
        //Confirmar Solicitud
        procesosMojix.setConfirmarSolicitud(responseOperacion.getSolicitud(), responseOperacion.isValido(), usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONFTRANSFERENCIAPROGRAMADA, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta2(responseOperacion.getResponse(), responseOperacion.isValido(), solicitud, usuario);
        //Actualizar transferencia
        procesosMojix.actualizarTransferencia2(responseOperacion.getTransferencia(), responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <confirmarTransferenciaProgramada>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun transferenciaProgramada(JsonRequestTransfProgramada request, String usuario) {
        LOGGER.debug("Inicio <transferenciaProgramada>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateTransferenciaProgramada(request);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.TRF, execTransferenciaPRG, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_TRANSFERENCIAPROGRAMADA, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuestaInOne(responseOperacion.isValido(), responseOperacion.getResponse(), operacion, solicitud, usuario);
        //Actualizar transferencia
        procesosMojix.actualizarTransferencia3(responseOperacion.getTransferencia(), operacion, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <transferenciaProgramada>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultBulkBisa iniciarBulkTransferencia(JsonRequestBulk request, String usuario) {
        LOGGER.debug("Inicio <iniciarBulkTransferencia>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateIniTransferenciaBulk(request);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.TRF, transferenciaBULK, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_TRANSFERENCIABULK, usuario);
        //Registrar Transferencia Bulk
        procesosMojix.setTransferenciaBulk(request, operacion, responseOperacion.isValido(), usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta(responseOperacion.getResponse(), responseOperacion.isValido(), operacion, solicitud, usuario);
        //Enviar SMS
        procesosMojix.enviarSMS(responseOperacion.getCliente(), solicitud, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <iniciarBulkTransferencia>");
        return responseOperacion.getResultBulkBisa();
    }

    @Override
    public JsonResultBulkConfirmar confirmarBulkTransferencia(JsonRequestBulkConfirmar request, String usuario) {
        LOGGER.debug("Inicio <confirmarBulkTransferencia>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConfTransferenciaBulk(request);
        //Confirmar Operacion
        Operacion operacion = procesosMojix.setConfirmarOperacion(responseOperacion.getRequest(), responseOperacion.isValido(), usuario);
        //Confirmar Solicitud
        procesosMojix.setConfirmarSolicitud(responseOperacion.getSolicitud(), responseOperacion.isValido(), usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONFTRANSFERENCIAPROGRAMADA, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta2(responseOperacion.getResponse(), responseOperacion.isValido(), solicitud, usuario);
        //Actualizar transferencia
        procesosMojix.actualizarTransferenciaBulk(responseOperacion, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <confirmarBulkTransferencia>");
        return responseOperacion.getResultBulkConfirmar();
    }

    @Override
    public JsonResultComun iniciarTransferenciaACH(JsonRequestTransferenciaACH requestTransferenciaACH, String usuario) {
        LOGGER.debug("Inicio <iniciarTransferenciaACH>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateIniTransferencia(requestTransferenciaACH);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.ACH, transferenciaACH, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_TRANSFERENCIAACH, usuario);
        //Registrar Transferencia
        procesosMojix.setTransferencia(requestTransferenciaACH, operacion, responseOperacion.isValido(), usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta(responseOperacion.getResponse(), responseOperacion.isValido(), operacion, solicitud, usuario);
        //Enviar SMS
        procesosMojix.enviarSMS(responseOperacion.getCliente(), solicitud, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <iniciarTransferenciaACH>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun confirmarTransferenciaACH(JsonRequestConfirmar requestConfirmar, String usuario) {
        LOGGER.debug("Inicio <confirmarTransferenciaACH>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConfIniTransferencia(requestConfirmar);
        //Confirmar Operacion
        Operacion operacion = procesosMojix.setConfirmarOperacion(responseOperacion.getRequest(), responseOperacion.isValido(), usuario);
        //Confirmar Solicitud
        procesosMojix.setConfirmarSolicitud(responseOperacion.getSolicitud(), responseOperacion.isValido(), usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONFTRANSFERENCIAACH, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta2(responseOperacion.getResponse(), responseOperacion.isValido(), solicitud, usuario);
        //Actualizar transferencia
        procesosMojix.actualizarTransferencia(responseOperacion.getTransferencia(), responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <confirmarTransferenciaACH>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun iniciarTransferenciaProgramadaACH(JsonRequestTransfProgramadaACH requestTransfProgramada, String usuario) {
        LOGGER.debug("Inicio <iniciarTransferenciaProgramadaACH>");
        //Validar Operacion
        ResponseOperacion responseOperacion = new ResponseOperacion(requestTransfProgramada);//TODO: revisar procesosMojix.validateIniTransferenciaProgramada(requestTransfProgramada);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.ACH, transferenciaACHPRG, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_PROGTRANSFERENCIAACH, usuario);
        //Registrar Transferencia
        procesosMojix.setTransferencia(requestTransfProgramada, operacion, responseOperacion.isValido(), usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta(responseOperacion.getResponse(), responseOperacion.isValido(), operacion, solicitud, usuario);
        //Enviar SMS
        procesosMojix.enviarSMS(responseOperacion.getCliente(), solicitud, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <iniciarTransferenciaProgramadaACH>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun confirmarTransferenciaProgramadaACH(JsonRequestConfirmar requestConfirmar, String usuario) {
        LOGGER.debug("Inicio <confirmarTransferenciaProgramadaACH>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConfIniTransferenciaProgramada(requestConfirmar);
        //Confirmar Operacion
        Operacion operacion = procesosMojix.setConfirmarOperacion(responseOperacion.getRequest(), responseOperacion.isValido(), usuario);
        //Confirmar Solicitud
        procesosMojix.setConfirmarSolicitud(responseOperacion.getSolicitud(), responseOperacion.isValido(), usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONFTRANSFERENCIAPROGRAMADAACH, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta2(responseOperacion.getResponse(), responseOperacion.isValido(), solicitud, usuario);
        //Actualizar transferencia
        procesosMojix.actualizarTransferencia2(responseOperacion.getTransferencia(), responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <confirmarTransferenciaProgramadaACH>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun transferenciaProgramadaACH(JsonRequestTransfProgramadaACH requestTransfProgramada, String usuario) {
        LOGGER.debug("Inicio <transferenciaProgramadaACH>");
        //Validar Operacion
        ResponseOperacion responseOperacion = new ResponseOperacion(requestTransfProgramada);//TODO: revisar procesosMojix.validateTransferenciaProgramada(requestTransfProgramada);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.ACH, execTransferenciaACHPRG, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_TRANSFERENCIAPROGRAMADAACH, usuario);
        //Registrar Transferencia
        Transferencia transferencia = procesosMojix.setTransferencia(requestTransfProgramada, operacion, responseOperacion.isValido(), usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuestaInOne(responseOperacion.isValido(), responseOperacion.getResponse(), operacion, solicitud, usuario);
        //Actualizar transferencia
        procesosMojix.actualizarTransferencia3(transferencia, operacion, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <transferenciaProgramadaACH>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun iniciarGiroMovil(JsonRequestGiroMovil requestGiroMovil, String usuario) {
        LOGGER.debug("Inicio <iniciarGiroMovil>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateIniGiroMovil(requestGiroMovil);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.GRM, giroMovil, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_GIROMOVIL, usuario);
        //Registrar Giro Movil
        procesosMojix.setGiro(responseOperacion.getGiro(), operacion, responseOperacion.isValido(), usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta(responseOperacion.getResponse(), responseOperacion.isValido(), operacion, solicitud, usuario);
        //Enviar SMS
        procesosMojix.enviarSMS(responseOperacion.getCliente(), solicitud, responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <iniciarGiroMovil>");
        return responseOperacion.getResultContactoBisa();
    }

    @Override
    public JsonResultComun confirmarGiroMovil(JsonRequestConfirmar requestConfirmar, String usuario) {
        LOGGER.debug("Inicio <confirmarGiroMovil>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConfIniGiroMovil(requestConfirmar);
        //Confirmar Operacion
        Operacion operacion = procesosMojix.setConfirmarOperacion(responseOperacion.getRequest(), responseOperacion.isValido(), usuario);
        //Confirmar Solicitud
        procesosMojix.setConfirmarSolicitud(responseOperacion.getSolicitud(), responseOperacion.isValido(), usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONFGIROMOVIL, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta2(responseOperacion.getResponse(), responseOperacion.isValido(), solicitud, usuario);
        //Actualizar giro movil
        //procesosMojix.actualizarTransferencia(responseOperacion.getTransferencia(), responseOperacion.isValido(), usuario);
        LOGGER.debug("Fin <confirmarGiroMovil>");
        return responseOperacion.getResultContactoBisa();
    }

}
