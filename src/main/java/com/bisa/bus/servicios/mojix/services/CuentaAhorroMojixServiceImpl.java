package com.bisa.bus.servicios.mojix.services;

import com.bisa.bus.servicios.mojix.services.types.JsonRequestAhorro;
import com.bisa.bus.servicios.mojix.services.types.JsonRequestConfirmar;
import com.bisa.bus.servicios.mojix.services.types.JsonRequestCuotaAhorro;
import com.bisa.bus.servicios.mojix.services.types.JsonResultComun;

/**
 * @author by rsalvatierra on 22/06/2017.
 */
public class CuentaAhorroMojixServiceImpl implements CuentaAhorroMojixService {
    @Override
    public JsonResultComun crearCuentaAhorro(JsonRequestAhorro requestAhorro, String user) {
        return null;
    }

    @Override
    public JsonResultComun confirmarCuentaAhorro(JsonRequestConfirmar requestConfirmar, String user) {
        return null;
    }

    @Override
    public JsonResultComun iniciarCuotaAhorro(JsonRequestCuotaAhorro requestCuotaAhorro, String user) {
        return null;
    }

    @Override
    public JsonResultComun confirmarCuotaAhorro(JsonRequestConfirmar requestConfirmar, String user) {
        return null;
    }

    @Override
    public JsonResultComun cuotaAhorro(JsonRequestCuotaAhorro requestCuotaAhorro, String user) {
        return null;
    }
}
