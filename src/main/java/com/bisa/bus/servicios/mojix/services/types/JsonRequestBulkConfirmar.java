package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Geografica;
import com.bisa.bus.servicios.mojix.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by josanchez on 22/07/2016.
 *
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestBulkConfirmar implements Serializable, Request {

    private String bulkConfCode;
    private String tokenSMS;
    private String bulkTransactionID;
    private Geografica geoProperties;

    public JsonRequestBulkConfirmar() {
    }

    public JsonRequestBulkConfirmar(String bulkConfCode, String tokenSMS, String bulkTransactionID, Geografica geoProperties) {
        setBulkConfCode(bulkConfCode);
        setTokenSMS(tokenSMS);
        setBulkTransactionID(bulkTransactionID);
        setGeoProperties(geoProperties);
    }

    public String getBulkConfCode() {
        return bulkConfCode;
    }

    public void setBulkConfCode(String bulkConfCode) {
        this.bulkConfCode = bulkConfCode;
    }

    public String getTokenSMS() {
        return tokenSMS;
    }

    public void setTokenSMS(String tokenSMS) {
        this.tokenSMS = tokenSMS;
    }

    public String getBulkTransactionID() {
        return bulkTransactionID;
    }

    public void setBulkTransactionID(String bulkTransactionID) {
        this.bulkTransactionID = bulkTransactionID;
    }

    public Geografica getGeoProperties() {
        return geoProperties;
    }

    public void setGeoProperties(Geografica geoProperties) {
        this.geoProperties = geoProperties;
    }

    @Override
    public String getTransactionID() {
        return bulkTransactionID;
    }

    @Override
    public String toString() {
        return "JsonRequestBulkConfirmar{" +
                "bulkConfCode='" + bulkConfCode + '\'' +
                ", tokenSMS='" + tokenSMS + '\'' +
                ", bulkTransactionID='" + bulkTransactionID + '\'' +
                ", geoProperties=" + geoProperties +
                '}';
    }
}
