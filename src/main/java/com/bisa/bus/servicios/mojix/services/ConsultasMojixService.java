package com.bisa.bus.servicios.mojix.services;

import com.bisa.bus.servicios.mojix.services.types.*;

/**
 * @author by rsalvatierra on 31/05/2017.
 */
public interface ConsultasMojixService {

    <T> T consultaTransaccion(JsonRequestTransaccion request, String usuario);

    JsonResultInfoBancos consultaBanco(JsonRequestInfo requestBasic, String usuario);

    JsonResultCuentaBisa consultaCuenta(JsonRequestInfo requestBasic, String usuario);

    JsonResultInfoTelefonicas consultaTelefonicas(JsonRequestInfo requestInfo, String usuario);

    JsonResultInfoAgencias consultaAgencias(JsonRequestInfo requestInfo, String usuario);
}
