package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Geografica;
import com.bisa.bus.servicios.mojix.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by josanchez on 22/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestRecarga implements Serializable, Request {

    private String bisaClienteID;
    private String telefono;
    private String codigoEmpresaTelefonica;
    private String numeroCuenta;
    private BigDecimal monto;
    private String moneda;
    private String numeroFactura;
    private String nombreFactura;
    private String ciudad;
    private String codigoAgencia;
    private String transactionID;
    private Geografica geoProperties;

    public JsonRequestRecarga() {
    }


    public String getBisaClienteID() {
        return bisaClienteID;
    }

    public void setBisaClienteID(String bisaClienteID) {
        this.bisaClienteID = bisaClienteID;
    }


    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCodigoEmpresaTelefonica() {
        return codigoEmpresaTelefonica;
    }

    public void setCodigoEmpresaTelefonica(String codigoEmpresaTelefonica) {
        this.codigoEmpresaTelefonica = codigoEmpresaTelefonica;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public String getNombreFactura() {
        return nombreFactura;
    }

    public void setNombreFactura(String nombreFactura) {
        this.nombreFactura = nombreFactura;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCodigoAgencia() {
        return codigoAgencia;
    }

    public void setCodigoAgencia(String codigoAgencia) {
        this.codigoAgencia = codigoAgencia;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public Geografica getGeoProperties() {
        return geoProperties;
    }

    public void setGeoProperties(Geografica geoProperties) {
        this.geoProperties = geoProperties;
    }


    @Override
    public String toString() {
        return "JsonRequestRecarga{" +
                "bisaClienteID='" + bisaClienteID + '\'' +
                ", telefono='" + telefono + '\'' +
                ", codigoEmpresaTelefonica='" + codigoEmpresaTelefonica + '\'' +
                ", numeroCuenta='" + numeroCuenta + '\'' +
                ", monto=" + monto +
                ", moneda='" + moneda + '\'' +
                ", numeroFactura='" + numeroFactura + '\'' +
                ", nombreFactura='" + nombreFactura + '\'' +
                ", ciudad='" + ciudad + '\'' +
                ", codigoAgencia='" + codigoAgencia + '\'' +
                ", transactionID='" + transactionID + '\'' +
                ", geoProperties=" + geoProperties +
                '}';
    }
}
