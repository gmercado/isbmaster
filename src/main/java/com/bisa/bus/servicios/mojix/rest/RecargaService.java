package com.bisa.bus.servicios.mojix.rest;

import com.bisa.bus.servicios.mojix.services.RecargasCelularMojixService;
import com.bisa.bus.servicios.mojix.services.types.JsonRequestConfirmar;
import com.bisa.bus.servicios.mojix.services.types.JsonRequestRecarga;
import com.bisa.bus.servicios.mojix.services.types.JsonResultComun;
import com.google.inject.Inject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * @author by rsalvatierra on 22/06/2017.
 */
@Path("/sjoven/Recarga")
@Produces({"application/json;charset=UTF-8"})
public class RecargaService extends CommonService {

    private static final String INI_RECARGA_CREDITO = "/iniRecargaCredito";
    private static final String CONF_RECARGA_CREDITO = "/confRecargaCredito";

    @Inject
    private RecargasCelularMojixService recargasCelularMojixService;

    /**
     * Permite solicitar token SMS para iniciar el proceso de recarga de credito al celular
     *
     * @param httpHeaders    headers
     * @param requestRecarga datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(INI_RECARGA_CREDITO)
    public JsonResultComun iniciarRecargaCredito(@Context HttpHeaders httpHeaders, JsonRequestRecarga requestRecarga) {
        if (requestRecarga == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return recargasCelularMojixService.iniciarRecarga(requestRecarga, getUser(httpHeaders));
        }
    }

    /**
     * Permite confirmar la recarga de credito al celular
     *
     * @param httpHeaders      headers
     * @param requestConfirmar datos basicos
     * @return JsonResultComun
     */
    @POST
    @Path(CONF_RECARGA_CREDITO)
    public JsonResultComun confirmarRecargaCredito(@Context HttpHeaders httpHeaders, JsonRequestConfirmar requestConfirmar) {
        if (requestConfirmar == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return recargasCelularMojixService.confirmarRecarga(requestConfirmar, getUser(httpHeaders));
        }
    }
}
