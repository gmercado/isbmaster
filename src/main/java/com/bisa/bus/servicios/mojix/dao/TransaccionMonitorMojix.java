package com.bisa.bus.servicios.mojix.dao;

import bus.env.api.MedioAmbiente;
import bus.interfaces.as400.entities.Cliente;
import bus.interfaces.as400.entities.Cuenta;
import bus.monitor.api.*;
import bus.monitor.transaction.trxin.Reg021;
import bus.monitor.transaction.trxout.CabeceraOUT;
import com.bisa.bus.servicios.mojix.entities.Transferencia;
import com.bisa.bus.servicios.mojix.exception.OperacionesMojixException;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Random;

import static bus.env.api.Variables.*;
import static bus.monitor.api.FormatersMonitor.*;
import static bus.monitor.api.ParametersMonitor.*;

/**
 * @author by josanchez on 19/07/2016.
 * @author rsalvatierra modificado on 20/10/2016
 */
public class TransaccionMonitorMojix {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransaccionMonitorMojix.class);
    private static final String WCLSTRN = "1";

    private final TransaccionesMonitor transaccionesMonitor;
    private final MedioAmbiente medioAmbiente;

    @Inject
    public TransaccionMonitorMojix(TransaccionesMonitor transaccionesMonitor, MedioAmbiente medioAmbiente) {
        this.transaccionesMonitor = transaccionesMonitor;
        this.medioAmbiente = medioAmbiente;
    }

    @SuppressWarnings({"unchecked"})
    public IRespuestasMonitor consultaCuenta(Cliente cliente) throws SistemaCerradoException, ImposibleLeerRespuestaException,
            TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {
        WithMonitor withMonitor = obtenerMonitorConsultaCuenta(cliente);
        return (IRespuestasMonitor) transaccionesMonitor.ejecutarTransaccionMonitor(getVersion(), null, withMonitor);
    }

    @SuppressWarnings({"unchecked"})
    private WithMonitor obtenerMonitorConsultaCuenta(Cliente cliente) {
        return new WithMonitor() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {
                // Obtiene la transacción
                String transaccion = medioAmbiente.getValorDe(MOJIX_CONSULTA_CUENTA_TRANSACCION_MONITOR, MOJIX_CONSULTA_CUENTA_TRANSACCION_MONITOR_DEFAULT);
                //Cabecera
                monitor.setDatoCabeceraEnvio(MON_DEVICE, getDevice());
                monitor.setDatoCabeceraEnvio(MON_TRANSACCION, transaccion);
                monitor.setDatoCabeceraEnvio(MON_USUARIO, getUsuario());
                monitor.setCuerpo1EnvioFormat(PSR9011);
                monitor.setCuerpo1RecepcionFormat(PSR9114);
                //Cuerpo
                monitor.setDatoCuerpo1(MON_CUENTA, new BigDecimal(cliente.getCodCliente()));
                monitor.setDatoCuerpo1(MON_TPOCTA, "I");
                monitor.setDatoCuerpo1(MON_PAG, cliente.getAgencia());
                monitor.setDatoCuerpo1(MON_CNT, new BigDecimal(0));
            }

            @Override
            public Object procesar(IRespuestasMonitor monitor) throws IOException, SistemaCerradoException, TransaccionNoEjecutadaCorrectamenteException {
                return monitor;
            }
        };
    }

    @SuppressWarnings({"unchecked"})
    public IRespuestasMonitor procesarTransferencia(Transferencia transferencia, Cuenta origen, Cuenta destino) throws OperacionesMojixException {
        IRespuestasMonitor monitor;
        Reg021 reg021;
        try {
            reg021 = new Reg021();
            reg021.setGlobal();
            reg021.setWCLSTRN(WCLSTRN);
            reg021.setWCUENTA(transferencia.getCuentaOrigen());
            reg021.setWTPOCTA(origen.getTipoCta());
            reg021.setWCODMON(String.valueOf(origen.getCodigoMoneda()));
            reg021.setWCUENTA2(transferencia.getCuentaDestino());
            reg021.setWTPOCTA2(destino.getTipoCta());
            reg021.setWNOTA(transferencia.getMotivo());
            reg021.setWCODMON2(String.valueOf(destino.getCodigoMoneda()));
            if (origen.getCodigoMoneda().equals(transferencia.getMoneda())) {
                reg021.setWIMPORTE(String.valueOf(transferencia.getMonto()));
            } else if (destino.getCodigoMoneda().equals(transferencia.getMoneda())) {
                reg021.setWIMPORTE2(String.valueOf(transferencia.getMonto()));
            } else {
                LOGGER.error("Error Monitor: MONEDA {}", transferencia.getMoneda());
                throw new IllegalStateException("Combinacion de monedas no soportada");
            }
            monitor = transferenciaMismoBanco(reg021);
        } catch (SistemaCerradoException e) {
            LOGGER.error("Error Monitor: No se puede efectuar la consulta.", e);
            monitor = null;
        } catch (ImposibleLeerRespuestaException e) {
            LOGGER.error("Error Monitor: No se puede efectuar la consulta.", e);
            throw new OperacionesMojixException(medioAmbiente.getValorDe(MOJIX_MENSAJE_REGISTRO_ERRONEO, MOJIX_MENSAJE_REGISTRO_ERRONEO_DEFAULT));
        } catch (TransaccionEfectivaException e) {
            LOGGER.error("Error Monitor: No se puede efectuar la consulta. <TransaccionEfectivaException>", e);
            monitor = null;
        } catch (TransaccionNoEjecutadaCorrectamenteException e) {
            LOGGER.error("Error Monitor: No se puede efectuar la consulta. <TransaccionNoEjecutadaCorrectamenteException>", e);
            monitor = null;
        } catch (IOException e) {
            LOGGER.error("Error Monitor: No se puede efectuar la consulta. <IOException>", e);
            monitor = null;
        } catch (Exception e) {
            LOGGER.error("Error Monitor: No se puede efectuar la consulta. <Exception>", e);
            monitor = null;
        }
        return monitor;
    }

    @SuppressWarnings({"unchecked"})
    private IRespuestasMonitor transferenciaMismoBanco(Reg021 reg021) throws SistemaCerradoException, ImposibleLeerRespuestaException,
            TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {
        WithMonitor withMonitor = obtenerDatosTransferenciaMismoBanco(reg021);
        return (IRespuestasMonitor) transaccionesMonitor.ejecutarTransaccionMonitor(getVersion(), null, withMonitor);
    }

    @SuppressWarnings({"unchecked"})
    private WithMonitor obtenerDatosTransferenciaMismoBanco(Reg021 reg021) {
        return new WithMonitor() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {
                // Obtiene la transacción
                String transaccion = medioAmbiente.getValorDe(MOJIX_TRANSFERENCIA_OTROS_BANCOS_TRANSACCION_MONITOR, MOJIX_TRANSFERENCIA_OTROS_BANCOS_TRANSACCION_MONITOR_DEFAULT);
                //Informacion Cabecera
                monitor.setDatoCabeceraEnvio(MON_DEVICE, getDevice());
                monitor.setDatoCabeceraEnvio(MON_TRANSACCION, transaccion);
                monitor.setDatoCabeceraEnvio(MON_USUARIO, getUsuario());
                monitor.setDatoCabeceraEnvio(MON_APRUSUARIO, "EBANKING");
                monitor.setDatoCabeceraEnvio(MON_TPODTA, new BigDecimal(1));
                monitor.setDatoCabeceraEnvio(MON_REVE, "S");
                monitor.setCuerpo1EnvioFormat(PSR9021);
                monitor.setCuerpo1RecepcionFormat(PSR9120);
                ///-----------------------------------CUERPO_TRANSACCION_TRANSFERECIA_TERCEROS
                monitor.setDatoCuerpo1(MON_CLSTRN, new BigDecimal(reg021.getWCLSTRN()));
                monitor.setDatoCuerpo1(MON_CUENTA, new BigDecimal(reg021.getWCUENTA()));
                monitor.setDatoCuerpo1(MON_TPOCTA, reg021.getWTPOCTA());
                monitor.setDatoCuerpo1(MON_CODMONEDA, new BigDecimal(reg021.getWCODMON()));
                monitor.setDatoCuerpo1(MON_IMPORTE, new BigDecimal(reg021.getWIMPORTE()));
                monitor.setDatoCuerpo1(MON_CUENTA2, new BigDecimal(reg021.getWCUENTA2()));
                monitor.setDatoCuerpo1(MON_TPOCTA2, reg021.getWTPOCTA2());
                monitor.setDatoCuerpo1(MON_CODMONEDA2, new BigDecimal(reg021.getWCODMON2()));
                monitor.setDatoCuerpo1(MON_IMPORTE2, new BigDecimal(reg021.getWIMPORTE2()));
                monitor.setDatoCuerpo1(MON_NOTA, reg021.getWNOTA());
                //monitor.setDatoCuerpo1("WCODADI",new BigDecimal(reg021.getWCODADI()));
            }

            @Override
            public Object procesar(IRespuestasMonitor monitor) throws IOException, SistemaCerradoException, TransaccionNoEjecutadaCorrectamenteException {
                return monitor;
            }
        };
    }

    @SuppressWarnings({"unchecked"})
    public IRespuestasMonitor revertirTransferencia(CabeceraOUT out) throws SistemaCerradoException, ImposibleLeerRespuestaException,
            TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {
        WithMonitor withMonitor = obtenerMonitorReversa(out);
        return (IRespuestasMonitor) transaccionesMonitor.ejecutarTransaccionMonitor(getVersion(), null, withMonitor);
    }

    @SuppressWarnings({"unchecked"})
    private WithMonitor obtenerMonitorReversa(CabeceraOUT out) {
        return new WithMonitor() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {
                // Obtiene la transacción
                String transaccion = medioAmbiente.getValorDe(MOJIX_TRANSFERENCIA_OTROS_BANCOS_TRANSACCION_MONITOR, MOJIX_TRANSFERENCIA_OTROS_BANCOS_TRANSACCION_MONITOR_DEFAULT);
                LOGGER.debug("Generando plantilla para la transaccion ST.");
                monitor.setDatoCabeceraEnvio(MON_DEVICE, getDevice());
                //monitor.setDatoCabeceraEnvio("WCANAL", "5");
                monitor.setDatoCabeceraEnvio(MON_APRUSUARIO, "EBANKING");
                monitor.setDatoCabeceraEnvio(MON_TPODTA, 6);
                monitor.setDatoCabeceraEnvio(MON_REVE, "S");
                monitor.setDatoCabeceraEnvio(MON_TRANSACCION, transaccion);
                monitor.setDatoCabeceraEnvio(MON_USUARIO, new BigDecimal(out.getWNUMUSR()));
                monitor.setDatoCabeceraEnvio(MON_NUMSEQ, new BigDecimal(out.getWNUMSEQ()));
            }

            @Override
            public Object procesar(IRespuestasMonitor monitor) throws IOException, SistemaCerradoException, TransaccionNoEjecutadaCorrectamenteException {
                return monitor;
            }
        };
    }

    private int getVersion() {
        return medioAmbiente.getValorIntDe(MONITOR_VERSION, MONITOR_VERSION_DEFAULT);
    }

    private String getDevice() {
        return "MOX" + StringUtils.leftPad("" + new Random().nextInt(99999), 7, "0");
    }

    private int getUsuario() {
        return medioAmbiente.getValorIntDe(MOJIX_USUARIO_MONITOR, MOJIX_USUARIO_MONITOR_DEFAULT);
    }
}



