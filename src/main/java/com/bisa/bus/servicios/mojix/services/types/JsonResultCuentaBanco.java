package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.ClienteBancoBisa;
import com.bisa.bus.servicios.mojix.model.CodigosConfirmacion;
import com.bisa.bus.servicios.mojix.model.Response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonResultCuentaBanco implements Serializable, Response {

    private ClienteBancoBisa cliente;
    private String statusCode;
    private String mensaje;
    private String confCode;

    public JsonResultCuentaBanco(String statusCode, CodigosConfirmacion confCode, String mensaje) {
        setStatusCode(statusCode);
        setConfCode(confCode.getCodigo());
        setMensaje(mensaje);
    }

    public JsonResultCuentaBanco(String statusCode, String mensaje, ClienteBancoBisa cliente) {
        setStatusCode(statusCode);
        setMensaje(mensaje);
        setConfCode(null);
        setCliente(cliente);
    }

    public ClienteBancoBisa getCliente() {
        return cliente;
    }

    public void setCliente(ClienteBancoBisa cliente) {
        this.cliente = cliente;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "JsonResultCuentaBisa{" +
                "statusCode='" + getStatusCode() + '\'' +
                ", mensaje='" + getMensaje() + '\'' +
                ", cliente=" + getCliente() +
                ", confCode=" + getConfCode() +
                '}';
    }

    public String getConfCode() {
        return confCode;
    }

    public void setConfCode(String confCode) {
        this.confCode = confCode;
    }
}
