package com.bisa.bus.servicios.mojix.entities;

import bus.database.model.EntityBase;
import bus.interfaces.as400.entities.BancoDestino;
import com.bisa.bus.servicios.mojix.model.EstadoBeneficiario;
import com.bisa.bus.servicios.mojix.model.TipoBeneficiario;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author by rsalvatierra on 30/06/2017.
 */
@Entity
@Table(name = "SJOV05")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S05FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S05USRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S05FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S05USRMOD"))
})
public class BeneficiarioJoven extends EntityBase implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S05CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "S05CODOPE")
    private Operacion operacion;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "S05CODPER")
    private ClienteJoven persona;

    @Column(name = "S05TIPBEN")
    @Enumerated(EnumType.ORDINAL)
    private TipoBeneficiario tipoBeneficiario;

    @Column(name = "S05TIPDOC", columnDefinition = "VARCHAR(10)")
    private String tipoDocumento;

    @Column(name = "S05NRODOC", columnDefinition = "VARCHAR(30)")
    private String numeroDocumento;

    @Column(name = "S05NOMBRE", columnDefinition = "VARCHAR(100)")
    private String nombre;

    @Column(name = "S05PATERN", columnDefinition = "VARCHAR(100)")
    private String paterno;

    @Column(name = "S05MATERN", columnDefinition = "VARCHAR(100)")
    private String materno;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "S05CODBAN")
    private BancoDestino bancoBeneficiario;

    @Column(name = "S05NROCUE", columnDefinition = "VARCHAR(20)")
    private String numeroCuenta;

    @Column(name = "S05MONCUE", columnDefinition = "NUMERIC(1)")
    private Short monedaCuenta;

    @Column(name = "S05DPTCUE", columnDefinition = "VARCHAR(3)")
    private String departamentoCuenta;

    @Column(name = "S05NROTEL", columnDefinition = "NUMERIC(8)")
    private String numeroTelefono;

    @Column(name = "S05REPEBI", columnDefinition = "VARCHAR(2)")
    private String repiteEbisa;

    @Column(name = "S05ESTADO")
    @Enumerated(EnumType.STRING)
    private EstadoBeneficiario estadoBeneficiario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }

    public ClienteJoven getPersona() {
        return persona;
    }

    public void setPersona(ClienteJoven persona) {
        this.persona = persona;
    }

    public TipoBeneficiario getTipoBeneficiario() {
        return tipoBeneficiario;
    }

    public void setTipoBeneficiario(TipoBeneficiario tipoBeneficiario) {
        this.tipoBeneficiario = tipoBeneficiario;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public BancoDestino getBancoBeneficiario() {
        return bancoBeneficiario;
    }

    public void setBancoBeneficiario(BancoDestino bancoBeneficiario) {
        this.bancoBeneficiario = bancoBeneficiario;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public Short getMonedaCuenta() {
        return monedaCuenta;
    }

    public void setMonedaCuenta(Short monedaCuenta) {
        this.monedaCuenta = monedaCuenta;
    }

    public String getDepartamentoCuenta() {
        return departamentoCuenta;
    }

    public void setDepartamentoCuenta(String departamentoCuenta) {
        this.departamentoCuenta = departamentoCuenta;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getRepiteEbisa() {
        return repiteEbisa;
    }

    public void setRepiteEbisa(String repiteEbisa) {
        this.repiteEbisa = repiteEbisa;
    }

    public EstadoBeneficiario getEstadoBeneficiario() {
        return estadoBeneficiario;
    }

    public void setEstadoBeneficiario(EstadoBeneficiario estadoBeneficiario) {
        this.estadoBeneficiario = estadoBeneficiario;
    }
}
