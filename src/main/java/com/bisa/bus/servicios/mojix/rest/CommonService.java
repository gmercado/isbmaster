package com.bisa.bus.servicios.mojix.rest;

import bus.plumbing.utils.RestUtils;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.util.HashMap;

/**
 * @author by rsalvatierra on 11/10/2016.
 */
class CommonService {

    private static final String APPLICATION_JSON = "application/json";
    private static final String AUTHORIZATION = "Authorization";
    private static final String ERROR_CATEGORY_NOT_FOUND_ERROR = "<error>Category Not Found</error>";
    private static final String USER_NAME = "USRNAME";

    protected String getUser(HttpHeaders httpHeaders) {
        String authorization = httpHeaders.getHeaderString(AUTHORIZATION);
        HashMap<String, String> credenciales = RestUtils.getUsernameHeaders(authorization, USER_NAME);
        return credenciales.get(USER_NAME);
    }

    Response getBuild(Response.Status status) {
        Response.ResponseBuilder builder = Response.status(status);
        builder.type(APPLICATION_JSON);
        builder.entity(ERROR_CATEGORY_NOT_FOUND_ERROR);
        return builder.build();
    }
}
