package com.bisa.bus.servicios.mojix.model;

/**
 * @author by rsalvatierra on 30/06/2017.
 */
public enum EstadoBeneficiario {
    INA,
    ACT
}
