package com.bisa.bus.servicios.mojix.api;

/**
 * @author oby rsalvatierra on 27/06/2017.
 */
class ValidaCampos {
    static final String CODIGO_TRANSACCION = "C\u00F3digo Transacci\u00F3n";
    static final String FECHA_NACIMIENTO = "Fecha Nacimiento";
    static final String DOCUMENTO_CI = "Documento CI";
    static final String TOKEN_SMS = "Token SMS";
    static final String CODIGO_CONFIRMACION = "C\u00F3digo Confirmaci\u00F3n";
    static final String BISA_CLIENTE_ID = "BISA Cliente ID";
    static final String NRO_CONTACTOS = "Nro Contactos";
    static final String NRO_CUENTA = "Nro Cuenta";
    static final String NRO_TELEFONO = "Telefono";
    static final String CUENTA_ORIGEN = "Cuenta Origen";
    static final String CUENTA_DESTINO = "Cuenta Destino";
    static final String MONEDA = "Moneda";
    static final String MONTO = "Monto";
    static final String LUGAR = "Georeferenciacion";
    static final String MOTIVO = "Motivo";
    static final String LIST_CUENTAS = "Listas de Cuentas";
    static final String PERIODICIDAD = "Periodicidad";
    static final String TIPO_DOCUMENTO = "Tipo Documento";
    static final String NRO_DOCUMENTO = "Nro Documento";
    static final String NOMBRE = "Nombre";
    static final String PATERNO = "Paterno";
    static final String BANCO = "Banco";
    static final String TIPO_EVENTO = "Tipo";
    static final String NRO_FACTURA = "Nro Factura";
    static final String NOMBRE_FACTURA = "Nombre Factura";
    static final String CIUDAD_FACTURA = "Ciudad Factura";
    static final String AGENCIA_FACTURA = "Agencia Factura";
}
