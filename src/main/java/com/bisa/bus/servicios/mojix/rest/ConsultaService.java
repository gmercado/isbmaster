package com.bisa.bus.servicios.mojix.rest;

import bus.monitor.dao.LogWebDao;
import com.bisa.bus.servicios.mojix.services.ConsultasMojixService;
import com.bisa.bus.servicios.mojix.services.types.*;
import com.google.inject.Inject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.sql.SQLException;

/**
 * @author by josanchez on 18/07/2016.
 * @author by rsalvatierra on 11/10/2016
 */
@Path("/sjoven/Consulta")
@Produces({"application/json;charset=UTF-8"})
public class ConsultaService extends CommonService {

    private static final String CUENTAS = "/cuentas";
    private static final String BANCOS = "/bancos";
    private static final String CONSULTA_TRANS = "/consultaTrans";
    private static final String EMPRESAS_TELEFONICAS = "/empresasTelefonicas";
    private static final String AGENCIAS_BISA = "/agenciasBisa";


    @Inject
    private ConsultasMojixService consultasMojixService;

    @Inject
    private LogWebDao logWebDao;

    /**
     * Consulta de cuentas y saldos un cliente
     *
     * @param httpHeaders headers
     * @param requestInfo datos basicos
     * @return JsonResultCuentaBisa
     */
    @POST
    @Path(CUENTAS)
    public JsonResultCuentaBisa consultaCuenta(@Context HttpHeaders httpHeaders, JsonRequestInfo requestInfo) {
        if (requestInfo == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasMojixService.consultaCuenta(requestInfo, getUser(httpHeaders));
        }
    }

    /**
     * Consulta estado de una transaccion, utilizado en caso "time out”
     *
     * @param httpHeaders       headers
     * @param requestTransccion datos basicos
     * @return JsonResultBisa
     */
    @POST
    @Path(CONSULTA_TRANS)
    public <T> T consultaTransaccion(@Context HttpHeaders httpHeaders, JsonRequestTransaccion requestTransccion) {
        if (requestTransccion == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasMojixService.consultaTransaccion(requestTransccion, getUser(httpHeaders));
        }
    }

    /**
     * El Servicio permite consultar codigos y nombres de bancos para adicionar cuentas
     *
     * @param httpHeaders headers
     * @param requestInfo datos basicos
     * @return JsonResultInfoBancos
     */
    @POST
    @Path(BANCOS)
    public JsonResultInfoBancos consultaBancos(@Context HttpHeaders httpHeaders, JsonRequestInfo requestInfo) {
        if (requestInfo == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasMojixService.consultaBanco(requestInfo, getUser(httpHeaders));
        }
    }

    /**
     * El Servicio permite consultar codigos y nombres de telefonicas para recarga
     *
     * @param httpHeaders headers
     * @param requestInfo datos basicos
     * @return JsonResultInfoTelefonicas
     */
    @POST
    @Path(EMPRESAS_TELEFONICAS)
    public JsonResultInfoTelefonicas consultaEmpresasTelefonicas(@Context HttpHeaders httpHeaders, JsonRequestInfo requestInfo) {
        if (requestInfo == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasMojixService.consultaTelefonicas(requestInfo, getUser(httpHeaders));
        }
    }

    /**
     * El Servicio permite consultar codigos y nombres de las agencias del Banco Bisa
     *
     * @param httpHeaders headers
     * @param requestInfo datos basicos
     * @return JsonResultInfoAgencias
     */
    @POST
    @Path(AGENCIAS_BISA)
    public JsonResultInfoAgencias consultaAgenciasBisa(@Context HttpHeaders httpHeaders, JsonRequestInfo requestInfo) {
        if (requestInfo == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasMojixService.consultaAgencias(requestInfo, getUser(httpHeaders));
        }
    }
}