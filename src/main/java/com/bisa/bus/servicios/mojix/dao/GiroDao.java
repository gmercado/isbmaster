package com.bisa.bus.servicios.mojix.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.mojix.entities.Giro;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.LinkedList;

/**
 * @author by rsalvatierra on 03/07/2017.
 */
public class GiroDao extends DaoImpl<Giro, Long> {
    private static final Logger LOGGER = LoggerFactory.getLogger(GiroDao.class);

    @Inject
    protected GiroDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    /**
     * Retorna Giro a partir de la operacion
     *
     * @param pOperacion Codigo operacion
     * @return Giro
     */
    public Giro getByOperacion(Operacion pOperacion) {
        LOGGER.debug("Obteniendo registro por operacion:{}.", pOperacion);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Giro> q = cb.createQuery(Giro.class);
                        Root<Giro> p = q.from(Giro.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("operacion"), pOperacion));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Giro> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public Giro registrar(Giro giro, String usuario) {
        giro.setUsuarioCreador(usuario);
        giro.setFechaCreacion(new Date());
        return persist(giro);
    }

    public Giro actualizar(Giro giro, String usuario) {
        giro.setUsuarioModificador(usuario);
        giro.setFechaModificacion(new Date());
        return merge(giro);
    }

}
