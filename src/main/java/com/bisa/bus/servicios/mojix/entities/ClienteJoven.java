package com.bisa.bus.servicios.mojix.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author by josanchez on 14/07/2016.
 * @author rsalvatierra modificado on 20/10/2016
 */
@Entity
@Table(name = "SJOV02")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S02FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S02USRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S02FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S02USRMOD"))
})
public class ClienteJoven extends EntityBase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S02CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "S02CODCLI", columnDefinition = "VARCHAR(10)")
    private String codigoCliente;

    @Column(name = "S02CODGEN", columnDefinition = "VARCHAR(64)")
    private String codigoClienteBisa;

    @Column(name = "S02ESTCLI", columnDefinition = "VARCHAR(3)")
    private String estadoCliente;

    public ClienteJoven() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoClienteBisa() {
        return codigoClienteBisa;
    }

    public void setCodigoClienteBisa(String codigoClienteBisa) {
        this.codigoClienteBisa = codigoClienteBisa;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getEstadoCliente() {
        return estadoCliente;
    }

    public void setEstadoCliente(String estadoCliente) {
        this.estadoCliente = estadoCliente;
    }

    @Override
    public String toString() {
        return "ClienteEnmascaradoMojix{" +
                "id=" + getId() +
                ", codigoCliente=" + getCodigoCliente() +
                ", codigoClienteBisa=" + getCodigoClienteBisa() +
                ", estadoCliente='" + getEstadoCliente() + '\'' +
                '}';
    }
}
