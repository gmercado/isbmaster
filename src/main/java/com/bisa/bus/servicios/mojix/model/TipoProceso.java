package com.bisa.bus.servicios.mojix.model;

/**
 * @author by rsalvatierra on 05/07/2017.
 */
public enum TipoProceso {
    iniciarCliente("CONSULTA CLIENTE"),
    iniciarContacto("AGREGAR CONTACTO"),
    iniciarCuenta("AGREGAR CUENTA"),
    iniCuentaOtroBanco("AGREGAR CUENTA ACH"),
    iniCuentaGiroMovil("AGREGAR CUENTA GIRO"),
    adicionarCliente("AFILIACION"),
    enviarSMS("ENVIAR SMS"),
    enviarClaveMovil("CLAVE MOVIL"),
    consultaCuenta("CONSULTA CUENTA"),
    consultaBancos("CONSULTA BANCOS"),
    consultaAgencias("CONSULTA AGENCIAS"),
    consultaTelefonicas("CONSULTA TELEFONICAS"),
    recargaMovil("RECARGA MOVIL"),
    transferencia("TRANSFERENCIA"),
    transferenciaPRG("PROG. TRANSF"),
    execTransferenciaPRG("EJEC. PROG. TRANSF"),
    transferenciaBULK("TRANSFERENCIA BULK"),
    transferenciaACH("ACH"),
    transferenciaACHPRG("PROG. ACH"),
    execTransferenciaACHPRG("EJEC PROG. ACH"),
    giroMovil("GIRO MOVIL");

    private String descripcion;

    TipoProceso(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
