package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestCuentaGiroMovil implements Serializable, Request {
    private String bisaClienteID;
    private String tipoDocumento;
    private String nroDocumento;
    private String nombre;
    private String paterno;
    private String materno;
    private String telefono;
    private boolean eBisa;
    private String transactionID;

    public JsonRequestCuentaGiroMovil() {
    }

    public JsonRequestCuentaGiroMovil(String bisaClienteID, String telefono, String transactionID) {
        setBisaClienteID(bisaClienteID);
        setTelefono(telefono);
        setTransactionID(transactionID);
    }

    public String getBisaClienteID() {
        return bisaClienteID;
    }

    public void setBisaClienteID(String bisaClienteID) {
        this.bisaClienteID = bisaClienteID;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public boolean iseBisa() {
        return eBisa;
    }

    public void seteBisa(boolean eBisa) {
        this.eBisa = eBisa;
    }

    @Override
    public String toString() {
        return "JsonRequestCuentaOtroBanco{" +
                "bisaClienteID='" + bisaClienteID + '\'' +
                ", tipoDocumento='" + tipoDocumento + '\'' +
                ", nroDocumento='" + nroDocumento + '\'' +
                ", nombre='" + nombre + '\'' +
                ", paterno='" + paterno + '\'' +
                ", materno='" + materno + '\'' +
                ", telefono='" + telefono + '\'' +
                ", eBisa=" + eBisa +
                ", transactionID='" + transactionID + '\'' +
                '}';
    }
}
