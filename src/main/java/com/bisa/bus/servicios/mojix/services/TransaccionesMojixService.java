package com.bisa.bus.servicios.mojix.services;

import com.bisa.bus.servicios.mojix.services.types.*;

/**
 * @author by josanchez on 14/07/2016.
 */
public interface TransaccionesMojixService {

    JsonResultComun iniciarTransferencia(JsonRequestTransferencia request, String usuario);

    JsonResultComun confirmarTransferencia(JsonRequestConfirmar request, String usuario);

    JsonResultComun iniciarTransferenciaProgramada(JsonRequestTransfProgramada request, String usuario);

    JsonResultComun confirmarTransferenciaProgramada(JsonRequestConfirmar request, String usuario);

    JsonResultComun transferenciaProgramada(JsonRequestTransfProgramada request, String usuario);

    JsonResultBulkBisa iniciarBulkTransferencia(JsonRequestBulk request, String usuario);

    JsonResultBulkConfirmar confirmarBulkTransferencia(JsonRequestBulkConfirmar request, String usuario);

    JsonResultComun iniciarTransferenciaACH(JsonRequestTransferenciaACH requestTransferenciaACH, String usuario);

    JsonResultComun confirmarTransferenciaACH(JsonRequestConfirmar requestConfirmar, String usuario);

    JsonResultComun iniciarTransferenciaProgramadaACH(JsonRequestTransfProgramadaACH requestTransfProgramada, String usuario);

    JsonResultComun confirmarTransferenciaProgramadaACH(JsonRequestConfirmar requestConfirmar, String usuario);

    JsonResultComun transferenciaProgramadaACH(JsonRequestTransfProgramadaACH requestTransfProgramada, String usuario);

    JsonResultComun iniciarGiroMovil(JsonRequestGiroMovil requestGiroMovil, String usuario);

    JsonResultComun confirmarGiroMovil(JsonRequestConfirmar requestConfirmar, String usuario);
}
