package com.bisa.bus.servicios.mojix.model;

/**
 * @author by josanchez on 17/08/2016.
 */
public enum TipoOperacion {

    CLI("CLIENTE"),
    CON("CONSULTA"),
    TRF("TRANSFERENCIA"),
    ACH("TRANSFERENCIA ACH"),
    GRM("GIRO MOVIL"),
    RCM("RECARGA MOVIL");

    private String descripcion;

    TipoOperacion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
