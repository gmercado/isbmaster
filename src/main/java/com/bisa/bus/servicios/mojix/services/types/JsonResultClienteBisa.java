package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.ClienteBisa;
import com.bisa.bus.servicios.mojix.model.CodigosConfirmacion;
import com.bisa.bus.servicios.mojix.model.Response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonResultClienteBisa implements Serializable, Response {

    public ClienteBisa result;
    private String statusCode;
    private String confCode;
    private String mensaje;

    public JsonResultClienteBisa() {
    }

    public JsonResultClienteBisa(String statusCode, CodigosConfirmacion confCode, String mensaje) {
        setStatusCode(statusCode);
        setConfCode(confCode.getCodigo());
        setMensaje(mensaje);
    }

    public JsonResultClienteBisa(String statusCode, String mensaje, ClienteBisa result) {
        setStatusCode(statusCode);
        setConfCode(null);
        setMensaje(mensaje);
        setResult(result);
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getConfCode() {
        return confCode;
    }

    public void setConfCode(String confCode) {
        this.confCode = confCode;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public ClienteBisa getResult() {
        return result;
    }

    public void setResult(ClienteBisa result) {
        this.result = result;
    }


    @Override
    public String toString() {
        return "JsonResultClienteBisa{" +
                "statusCode='" + getStatusCode() + '\'' +
                ", confCode='" + getConfCode() + '\'' +
                ", mensaje='" + getMensaje() + '\'' +
                ", result=" + getResult() +
                '}';
    }
}
