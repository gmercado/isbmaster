package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Geografica;
import com.bisa.bus.servicios.mojix.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by josanchez on 22/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestGiroMovil implements Serializable, Request {

    private String bisaClienteID;
    private String cuentaOrigen;
    private String nombreCompleto;
    private String tipoDocumento;
    private String nroDocumento;
    private String telefono;
    private String moneda;
    private BigDecimal monto;
    private String motivo;
    private String transactionID;
    private Geografica geoProperties;

    public JsonRequestGiroMovil() {
    }

    public JsonRequestGiroMovil(String bisaClienteID, String cuentaOrigen, String moneda, BigDecimal monto, String motivo, String transactionID, Geografica geoProperties) {
        setBisaClienteID(bisaClienteID);
        setCuentaOrigen(cuentaOrigen);
        setMoneda(moneda);
        setMonto(monto);
        setMotivo(motivo);
        setTransactionID(transactionID);
        setGeoProperties(geoProperties);
    }

    public String getBisaClienteID() {
        return bisaClienteID;
    }

    public void setBisaClienteID(String bisaClienteID) {
        this.bisaClienteID = bisaClienteID;
    }

    public String getCuentaOrigen() {
        return cuentaOrigen;
    }

    public void setCuentaOrigen(String cuentaOrigen) {
        this.cuentaOrigen = cuentaOrigen;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public Geografica getGeoProperties() {
        return geoProperties;
    }

    public void setGeoProperties(Geografica geoProperties) {
        this.geoProperties = geoProperties;
    }

    @Override
    public String toString() {
        return "JsonRequestGiroMovil{" +
                "bisaClienteID='" + bisaClienteID + '\'' +
                ", cuentaOrigen='" + cuentaOrigen + '\'' +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                ", tipoDocumento='" + tipoDocumento + '\'' +
                ", nroDocumento='" + nroDocumento + '\'' +
                ", telefono='" + telefono + '\'' +
                ", moneda='" + moneda + '\'' +
                ", monto=" + monto +
                ", motivo='" + motivo + '\'' +
                ", transactionID='" + transactionID + '\'' +
                ", geoProperties=" + geoProperties +
                '}';
    }
}
