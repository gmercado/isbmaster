package com.bisa.bus.servicios.mojix.model;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 */
public class MonedaMojix implements Serializable {

    String codigo;
    String descripcion;

    public MonedaMojix(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public MonedaMojix() {
    }


    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "MonedaMojix{" +
                "codigo='" + codigo + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }


}
