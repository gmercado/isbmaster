package com.bisa.bus.servicios.mojix.services;

import com.bisa.bus.servicios.mojix.api.ProcesosMojix;
import com.bisa.bus.servicios.mojix.api.ResponseOperacion;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.bisa.bus.servicios.mojix.entities.Solicitud;
import com.bisa.bus.servicios.mojix.model.TipoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoSolicitud;
import com.bisa.bus.servicios.mojix.services.types.*;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.bisa.bus.servicios.mojix.model.TipoProceso.*;

/**
 * @author by rsalvatierra on 31/05/2017.
 */
public class ConsultasMojixServiceImpl implements ConsultasMojixService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultasMojixServiceImpl.class);
    private final ProcesosMojix procesosMojix;

    @Inject
    public ConsultasMojixServiceImpl(ProcesosMojix procesosMojix) {
        this.procesosMojix = procesosMojix;
    }

    @Override
    public JsonResultInfoBancos consultaBanco(JsonRequestInfo requestBasic, String usuario) {
        LOGGER.debug("Inicio <consultaBanco>");
        requestBasic.setTransactionID(String.valueOf(procesosMojix.getTime()));
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConsultaBancos(requestBasic);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.CON, consultaBancos, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONSULTABANCOS, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuestaInOne(responseOperacion.isValido(), responseOperacion.getResponse(), operacion, solicitud, usuario);
        LOGGER.debug("Fin <consultaBanco>");
        return responseOperacion.getResultInfoBancos();
    }

    @Override
    public JsonResultCuentaBisa consultaCuenta(JsonRequestInfo requestBasic, String usuario) {
        LOGGER.debug("Inicio <consultaCuenta>");
        requestBasic.setTransactionID(String.valueOf(procesosMojix.getTime()));
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConsultaCuenta(requestBasic);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.CON, consultaCuenta, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONSULTACUENTA, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuestaInOne(responseOperacion.isValido(), responseOperacion.getResponse(), operacion, solicitud, usuario);
        LOGGER.debug("Fin <consultaCuenta>");
        return responseOperacion.getResultCuentaBisa();
    }

    @Override
    public JsonResultInfoTelefonicas consultaTelefonicas(JsonRequestInfo requestInfo, String usuario) {
        LOGGER.debug("Inicio <consultaTelefonicas>");
        requestInfo.setTransactionID(String.valueOf(procesosMojix.getTime()));
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConsultaTelefonicas(requestInfo);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.CON, consultaTelefonicas, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONSULTATELEFONICAS, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuestaInOne(responseOperacion.isValido(), responseOperacion.getResponse(), operacion, solicitud, usuario);
        LOGGER.debug("Fin <consultaTelefonicas>");
        return responseOperacion.getResultInfoTelefonicas();
    }

    @Override
    public JsonResultInfoAgencias consultaAgencias(JsonRequestInfo requestInfo, String usuario) {
        LOGGER.debug("Inicio <consultaAgencias>");
        requestInfo.setTransactionID(String.valueOf(procesosMojix.getTime()));
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConsultaAgencias(requestInfo);
        //Registrar Operacion
        Operacion operacion = procesosMojix.setOperacion(responseOperacion.getRequest(), responseOperacion.getCliente(), TipoOperacion.CON, consultaAgencias, usuario);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), operacion, TipoSolicitud.TRANS_CONSULTAAGENCIAS, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuestaInOne(responseOperacion.isValido(), responseOperacion.getResponse(), operacion, solicitud, usuario);
        LOGGER.debug("Fin <consultaAgencias>");
        return responseOperacion.getResultInfoAgencias();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T consultaTransaccion(JsonRequestTransaccion request, String usuario) {
        LOGGER.debug("Inicio <consultaTransaccion>");
        //Validar Operacion
        ResponseOperacion responseOperacion = procesosMojix.validateConsultaTransaccion(request);
        //Registrar Solicitud
        Solicitud solicitud = procesosMojix.setSolicitud(responseOperacion.getRequest(), responseOperacion.getResponse(), responseOperacion.getOperacion(), TipoSolicitud.TRANS_CONSULTA, usuario);
        //Actualizar Codigo Confirmacion
        procesosMojix.actualizarRespuesta3(responseOperacion.getResponse(), responseOperacion.isValido(), solicitud, usuario);
        LOGGER.debug("Fin <consultaTransaccion>");
        return (T) responseOperacion.getResultContactoBisa();
    }
}
