package com.bisa.bus.servicios.mojix.model;

import bus.interfaces.as400.entities.Agencia;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AgenciasMojix implements Serializable {
    private Long codigoAgencia;
    private String ciudadAgencia;
    private String nombreAgencia;

    public AgenciasMojix(Agencia agencia) {
        setCodigoAgencia(agencia.getCfbrch());
        setNombreAgencia(StringUtils.trimToEmpty(agencia.getNombreAgenciaSinCiudad()));
        setCiudadAgencia(agencia.getCiudad());
    }

    public Long getCodigoAgencia() {
        return codigoAgencia;
    }

    public void setCodigoAgencia(Long codigoAgencia) {
        this.codigoAgencia = codigoAgencia;
    }

    public String getCiudadAgencia() {
        return ciudadAgencia;
    }

    public void setCiudadAgencia(String ciudadAgencia) {
        this.ciudadAgencia = ciudadAgencia;
    }

    public String getNombreAgencia() {
        return nombreAgencia;
    }

    public void setNombreAgencia(String nombreAgencia) {
        this.nombreAgencia = nombreAgencia;
    }

    @Override
    public String toString() {
        return "AgenciasMojix{" +
                "codigoAgencia='" + codigoAgencia + '\'' +
                ", ciudadAgencia='" + ciudadAgencia + '\'' +
                ", nombreAgencia='" + nombreAgencia + '\'' +
                '}';
    }
}
