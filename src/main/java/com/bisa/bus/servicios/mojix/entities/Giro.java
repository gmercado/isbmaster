package com.bisa.bus.servicios.mojix.entities;

import bus.database.model.EntityBase;
import com.bisa.bus.servicios.mojix.model.EstadoGiro;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 03/07/2017.
 */
@Entity
@Table(name = "SJOV06")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S06FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S06USRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S06FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S06USRMOD"))
})
public class Giro extends EntityBase implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S06CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "S06CODOPE")
    private Operacion operacion;
    @Column(name = "S06NROCUE", columnDefinition = "VARCHAR(2)")
    private String cuenta;
    @Column(name = "S06MONEDA", columnDefinition = "VARCHAR(2)")
    private Short moneda;
    @Column(name = "S06MONTOS", columnDefinition = "VARCHAR(2)")
    private BigDecimal importe;
    @Column(name = "S06MOTIVO", columnDefinition = "VARCHAR(2)")
    private String motivo;
    @Column(name = "S06NOMBRE", columnDefinition = "VARCHAR(2)")
    private String nombreCompletoBeneficiario;
    @Column(name = "S06NRODOC", columnDefinition = "VARCHAR(2)")
    private String docIdentidadBeneficiario;
    @Column(name = "S06TIPDOC", columnDefinition = "VARCHAR(2)")
    private String tipoIdentidadBeneficiario;
    @Column(name = "S06NROTEL", columnDefinition = "VARCHAR(2)")
    private Long celularBeneficiario;
    @Enumerated(EnumType.STRING)
    @Column(name = "S06ESTADO", columnDefinition = "VARCHAR(2)")
    private EstadoGiro estadoGiro;
    @Column(name = "S06NUMGIR", columnDefinition = "NUMERIC(10)")
    private Long nroGiro;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public Short getMoneda() {
        return moneda;
    }

    public void setMoneda(Short moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getNombreCompletoBeneficiario() {
        return nombreCompletoBeneficiario;
    }

    public void setNombreCompletoBeneficiario(String nombreCompletoBeneficiario) {
        this.nombreCompletoBeneficiario = nombreCompletoBeneficiario;
    }

    public String getDocIdentidadBeneficiario() {
        return docIdentidadBeneficiario;
    }

    public void setDocIdentidadBeneficiario(String docIdentidadBeneficiario) {
        this.docIdentidadBeneficiario = docIdentidadBeneficiario;
    }

    public String getTipoIdentidadBeneficiario() {
        return tipoIdentidadBeneficiario;
    }

    public void setTipoIdentidadBeneficiario(String tipoIdentidadBeneficiario) {
        this.tipoIdentidadBeneficiario = tipoIdentidadBeneficiario;
    }

    public Long getCelularBeneficiario() {
        return celularBeneficiario;
    }

    public void setCelularBeneficiario(Long celularBeneficiario) {
        this.celularBeneficiario = celularBeneficiario;
    }

    public EstadoGiro getEstadoGiro() {
        return estadoGiro;
    }

    public void setEstadoGiro(EstadoGiro estadoGiro) {
        this.estadoGiro = estadoGiro;
    }

    public Long getNroGiro() {
        return nroGiro;
    }

    public void setNroGiro(Long nroGiro) {
        this.nroGiro = nroGiro;
    }
}
