package com.bisa.bus.servicios.mojix.model;

/**
 * @author Miguel Vega
 * @version $Id: MessageKeyword.java; dic 21, 2015 02:06 PM mvega $
 */
public enum MessageKeyword {
    TOKENSMS,
    TOKENTIEMPOVIDA,
    CUENTAINVALIDA,
    DOCID,
    FECNAC,
    CODCLIENTE,
    TRANSID,
    ESTADOTRANS,
    CORIGEN,
    CDESTINO,
    SOLICITUDTOKEN,
    CODCONFIRMACION,
    NOMBRECAMPO
}