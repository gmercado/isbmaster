package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.CodigosConfirmacion;
import com.bisa.bus.servicios.mojix.model.Response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonResultBulkBisa implements Serializable, Response {

    private String transactionID;
    private String statusCode;
    private String confCode;
    private String mensaje;

    public JsonResultBulkBisa(String statusCode, String mensaje, String transactionID) {
        setStatusCode(statusCode);
        setConfCode(null);
        setMensaje(mensaje);
        setTransactionID(transactionID);
    }

    public JsonResultBulkBisa(String statusCode, CodigosConfirmacion confCode, String mensaje) {
        setStatusCode(statusCode);
        setConfCode(confCode.getCodigo());
        setMensaje(mensaje);
    }

    public JsonResultBulkBisa(String statusCode, CodigosConfirmacion confCode, String mensaje, String transactionID) {
        setTransactionID(transactionID);
        setStatusCode(statusCode);
        setConfCode(confCode.getCodigo());
        setMensaje(mensaje);
    }

    public JsonResultBulkBisa(String transactionID, String statusCode, String confCode, String mensaje) {
        setTransactionID(transactionID);
        setStatusCode(statusCode);
        setConfCode(confCode);
        setMensaje(mensaje);
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getConfCode() {
        return confCode;
    }

    public void setConfCode(String confCode) {
        this.confCode = confCode;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "JsonResultContactoBisa{" +
                "statusCode='" + getStatusCode() + '\'' +
                ", confCode='" + getConfCode() + '\'' +
                ", mensaje='" + getMensaje() + '\'' +
                ", transactionID='" + getTransactionID() + '\'' +
                '}';
    }

}

