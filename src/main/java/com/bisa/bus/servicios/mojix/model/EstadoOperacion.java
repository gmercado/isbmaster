package com.bisa.bus.servicios.mojix.model;

/**
 * @author by josanchez on 17/08/2016.
 */
public enum EstadoOperacion {

    ACT("ACTIVO"),
    PRC("CONFIRMADO"),
    ANU("ANULADO"),
    ERR("ERROR");

    private String descripcion;

    EstadoOperacion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
