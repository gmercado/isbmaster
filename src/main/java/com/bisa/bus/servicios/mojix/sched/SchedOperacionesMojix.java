package com.bisa.bus.servicios.mojix.sched;

import com.bisa.bus.servicios.mojix.dao.OperacionDao;
import com.bisa.bus.servicios.mojix.dao.SolicitudDao;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.bisa.bus.servicios.mojix.entities.Solicitud;
import com.bisa.bus.servicios.mojix.model.EstadoOperacion;
import com.bisa.bus.servicios.mojix.model.EstadoSolicitud;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * @author by josanchez on 18/07/2016.
 * @author by rsalvatierra on 18/10/2016.
 */
public class SchedOperacionesMojix implements Job {

    private static final String USUARIO = "SJUSRJOB";
    private static final Logger LOGGER = LoggerFactory.getLogger(SchedOperacionesMojix.class);
    private final OperacionDao operacionDao;
    private final SolicitudDao solicitudDao;

    @Inject
    public SchedOperacionesMojix(OperacionDao operacionDao, SolicitudDao solicitudDao) {
        this.operacionDao = operacionDao;
        this.solicitudDao = solicitudDao;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Date fecTiempoVida;
        Date fecActual = new Date();
        boolean entro;
        //Recuperar registros en estado ACTIVO.
        List<Operacion> operaciones = operacionDao.getNoVigentes();
        if (operaciones != null && operaciones.size() > 0) {
            for (Operacion operacion : operaciones) {
                entro = false;
                List<Solicitud> solicitudes = solicitudDao.getByOperacion(operacion);
                if (solicitudes != null && solicitudes.size() > 0) {
                    for (Solicitud solicitud : solicitudes) {
                        if (solicitud.getTiempoVida() != null) {
                            fecTiempoVida = solicitud.getTiempoVida();
                            LOGGER.debug("Tiempo de Vida {}, Fecha Actual {}", fecTiempoVida, fecActual);
                            if (fecActual.after(fecTiempoVida)) {
                                entro = true;
                                LOGGER.info("SchedOperacionesMojix  Actualizando >>> {}", solicitud.getId());
                                solicitud.setEstadoSolicitud(EstadoSolicitud.ANU);
                                solicitudDao.actualizar(solicitud, USUARIO);
                            }
                        }
                    }
                    if (entro) {
                        operacion.setEstadoOperacion(EstadoOperacion.ANU);
                        operacionDao.actualizarTipoOperacion(operacion, USUARIO);
                    }
                }
            }
        } else {
            LOGGER.debug("No existen Operaciones pendientes.");
        }
    }
}
