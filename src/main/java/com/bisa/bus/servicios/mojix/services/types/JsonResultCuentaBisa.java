package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.CodigosConfirmacion;
import com.bisa.bus.servicios.mojix.model.CuentaClienteMojix;
import com.bisa.bus.servicios.mojix.model.Response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonResultCuentaBisa implements Serializable, Response {

    public List<CuentaClienteMojix> cuentas;
    private String statusCode;
    private String mensaje;

    public JsonResultCuentaBisa(String statusCode, CodigosConfirmacion confCode, String mensaje) {
        setStatusCode(statusCode);
        setMensaje(mensaje);
    }

    public JsonResultCuentaBisa(String statusCode, String mensaje, List<CuentaClienteMojix> cuentas) {
        setStatusCode(statusCode);
        setMensaje(mensaje);
        setCuentas(cuentas);
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public void setConfCode(String confCode) {

    }

    public List<CuentaClienteMojix> getCuentas() {
        return cuentas;
    }

    public void setCuentas(List<CuentaClienteMojix> cuentas) {
        this.cuentas = cuentas;
    }

    @Override
    public String toString() {
        return "JsonResultCuentaBisa{" +
                "statusCode='" + getStatusCode() + '\'' +
                ", mensaje='" + getMensaje() + '\'' +
                ", cuentas=" + getCuentas() +
                '}';
    }
}
