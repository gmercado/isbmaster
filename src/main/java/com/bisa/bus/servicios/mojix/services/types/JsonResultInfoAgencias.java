package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.AgenciasMojix;
import com.bisa.bus.servicios.mojix.model.CodigosConfirmacion;
import com.bisa.bus.servicios.mojix.model.Response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

/**
 * @author by josanchez on 18/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonResultInfoAgencias implements Serializable, Response {

    private String statusCode;
    private String mensaje;
    private List<AgenciasMojix> agencias;

    public JsonResultInfoAgencias(String statusCode, CodigosConfirmacion codigosConfirmacion, String mensaje) {
        setStatusCode(statusCode);
        setMensaje(mensaje);
    }

    public JsonResultInfoAgencias(String statusCode, String mensaje, List<AgenciasMojix> agencias) {
        setStatusCode(statusCode);
        setMensaje(mensaje);
        setAgencias(agencias);
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public void setConfCode(String confCode) {
    }

    public List<AgenciasMojix> getAgencias() {
        return agencias;
    }

    public void setAgencias(List<AgenciasMojix> agencias) {
        this.agencias = agencias;
    }

    @Override
    public String toString() {
        return "JsonResultInfoAgencias{" +
                "statusCode='" + statusCode + '\'' +
                ", mensaje='" + mensaje + '\'' +
                ", agencias=" + agencias +
                '}';
    }
}
