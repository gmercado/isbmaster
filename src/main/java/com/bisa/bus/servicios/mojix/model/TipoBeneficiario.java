package com.bisa.bus.servicios.mojix.model;

/**
 * @author by rsalvatierra on 30/06/2017.
 */
public enum TipoBeneficiario {
    CUENTA_BANCO,
    CUENTA_OTRO_BANCO,
    CUENTA_GIRO
}
