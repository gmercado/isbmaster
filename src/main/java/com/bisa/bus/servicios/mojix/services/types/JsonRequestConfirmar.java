package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 * @author by rsalvatierra on 18/11/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestConfirmar implements Serializable, Request {

    private String confCode;
    private String tokenSMS;
    private String transactionID;

    public JsonRequestConfirmar() {
    }

    public JsonRequestConfirmar(String confCode, String tokenSMS, String transactionID) {
        setConfCode(confCode);
        setTokenSMS(tokenSMS);
        setTransactionID(transactionID);
    }

    public String getConfCode() {
        return confCode;
    }

    public void setConfCode(String confCode) {
        this.confCode = confCode;
    }

    public String getTokenSMS() {
        return tokenSMS;
    }

    public void setTokenSMS(String tokenSMS) {
        this.tokenSMS = tokenSMS;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    @Override
    public String toString() {
        return "JsonRequestConfirmar{" +
                "confCode='" + confCode + '\'' +
                ", tokenSMS='" + tokenSMS + '\'' +
                ", transactionID='" + transactionID + '\'' +
                '}';
    }

}
