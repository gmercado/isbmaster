package com.bisa.bus.servicios.mojix.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.bisa.bus.servicios.mojix.model.EstadoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoProceso;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;


/**
 * @author by josanchez on 16/08/2016.
 * @author rsalvatierra modificado on 20/10/2016
 */
public class OperacionDao extends DaoImpl<Operacion, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OperacionDao.class);

    @Inject
    protected OperacionDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Long> countPath(Root<Operacion> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<Operacion> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.get("transaccionID"));
        return paths;
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<Operacion> from, Operacion example, Operacion example2) {

        List<Predicate> predicates = new LinkedList<>();
        Date fecha = example.getFechaCreacion();
        String codigoCliente = (example.getCliente() != null) ? StringUtils.trimToNull(example.getCliente().getCodigoCliente()) : null;
        String transaccionID = example.getTransaccionID();
        TipoOperacion tipoOperacion = example.getTipoOperacion();
        TipoProceso tipoProceso = example.getProceso();
        EstadoOperacion estadoOperacion = example.getEstadoOperacion();
        if (fecha != null && example2.getFechaCreacion() != null) {
            Date inicio = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH);
            Date inicio2 = DateUtils.truncate(example2.getFechaCreacion(), Calendar.DAY_OF_MONTH);
            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
            LOGGER.debug("   FECHAS ENTRE [{}] A [{}]", inicio, inicio2);
            predicates.add(cb.between(from.get("fechaCreacion"), inicio, inicio2));
        }

        if (codigoCliente != null) {
            final String s = StringUtils.trimToEmpty(codigoCliente);
            LOGGER.debug(">>>>> codigoCliente=" + s);
            predicates.add(cb.equal(from.get("cliente").get("codigoCliente"), s));
        }
        if (tipoOperacion != null) {
            LOGGER.debug(">>>>> tipoOperacion =" + tipoOperacion);
            predicates.add(cb.equal(from.get("tipoOperacion"), tipoOperacion));
        }
        if (tipoProceso != null) {
            LOGGER.debug(">>>>> tipoProceso =" + tipoProceso);
            predicates.add(cb.equal(from.get("proceso"), tipoProceso));
        }
        if (estadoOperacion != null) {
            LOGGER.debug(">>>>> estadoOperacion =" + estadoOperacion);
            predicates.add(cb.equal(from.get("estadoOperacion"), estadoOperacion));
        }
        if (transaccionID != null) {
            LOGGER.debug(">>>>> transaccionID =" + transaccionID);
            predicates.add(cb.equal(from.get("transaccionID"), transaccionID));
        }
        return predicates.toArray(new Predicate[predicates.size()]);
    }

    public Operacion getByTransactionID(final String transactionID) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Operacion> q = cb.createQuery(Operacion.class);
                        Root<Operacion> p = q.from(Operacion.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("transaccionID"), StringUtils.trimToEmpty(transactionID)));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Operacion> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public Operacion newOperacion(TipoOperacion tipoOperacion, TipoProceso proceso) {
        Operacion operacion = new Operacion();
        operacion.setProceso(proceso);
        operacion.setTipoOperacion(tipoOperacion);
        operacion.setEstadoOperacion(EstadoOperacion.ACT);
        return operacion;
    }

    public Operacion registrarTipoOperacion(Operacion operacion, String usuario) {
        operacion.setUsuarioCreador(usuario);
        operacion.setFechaCreacion(new Date());
        return persist(operacion);
    }

    public Operacion actualizarTipoOperacion(Operacion operacion, String usuario) {
        operacion.setUsuarioModificador(usuario);
        operacion.setFechaModificacion(new Date());
        return merge(operacion);
    }

    public List<Operacion> getNoVigentes() {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Operacion> q = cb.createQuery(Operacion.class);
                        Root<Operacion> p = q.from(Operacion.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(p.get("estadoOperacion").in(EstadoOperacion.ACT));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Operacion> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }
}
