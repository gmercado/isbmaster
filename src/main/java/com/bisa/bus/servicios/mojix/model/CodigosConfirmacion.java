package com.bisa.bus.servicios.mojix.model;

/**
 * @author by rsalvatierra on 14/10/2016.
 */
public enum CodigosConfirmacion {
    ERROR("0", "error");

    private String codigo;
    private String descripcion;

    CodigosConfirmacion(String codigo, String error) {
        setCodigo(codigo);
        setDescripcion(error);
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
