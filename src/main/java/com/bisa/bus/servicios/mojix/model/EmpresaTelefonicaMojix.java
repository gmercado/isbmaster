package com.bisa.bus.servicios.mojix.model;

import bus.consumoweb.pagoservicios.model.Servicio;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 18/07/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmpresaTelefonicaMojix implements Serializable {
    private String codigoEmpresaTelefonica;
    private String nombreEmpresaTelefonica;

    public EmpresaTelefonicaMojix(Servicio servicio) {
        setCodigoEmpresaTelefonica(servicio.getCodigo() + "-" + servicio.getEmpresaCodigo());
        setNombreEmpresaTelefonica(servicio.getEmpresaNombre());
    }

    public String getCodigoEmpresaTelefonica() {
        return codigoEmpresaTelefonica;
    }

    public void setCodigoEmpresaTelefonica(String codigoEmpresaTelefonica) {
        this.codigoEmpresaTelefonica = codigoEmpresaTelefonica;
    }

    public String getNombreEmpresaTelefonica() {
        return nombreEmpresaTelefonica;
    }

    public void setNombreEmpresaTelefonica(String nombreEmpresaTelefonica) {
        this.nombreEmpresaTelefonica = nombreEmpresaTelefonica;
    }

    @Override
    public String toString() {
        return "EmpresaTelefonicaMojix{" +
                "codigoEmpresaTelefonica='" + codigoEmpresaTelefonica + '\'' +
                ", nombreEmpresaTelefonica='" + nombreEmpresaTelefonica + '\'' +
                '}';
    }
}
