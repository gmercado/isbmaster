package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Geografica;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author by josanchez on 22/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestConfirmarTransferencia extends JsonRequestConfirmar implements Serializable {

    private Geografica geoProperties;

    public JsonRequestConfirmarTransferencia() {
    }

    public JsonRequestConfirmarTransferencia(String confCode, String tokenSMS, String transactionID, Geografica geoProperties) {
        setConfCode(confCode);
        setTokenSMS(tokenSMS);
        setTransactionID(transactionID);
        setGeoProperties(geoProperties);
    }

    public Geografica getGeoProperties() {
        return geoProperties;
    }

    public void setGeoProperties(Geografica geoProperties) {
        this.geoProperties = geoProperties;
    }

    @Override
    public String toString() {
        return "JsonRequestConfirmarTransaferencia{" +
                "confCode='" + getConfCode() + '\'' +
                ", tokenSMS='" + getTokenSMS() + '\'' +
                ", transactionID='" + getTransactionID() + '\'' +
                ", geoProperties=" + getGeoProperties() +
                '}';
    }
}
