package com.bisa.bus.servicios.mojix.services.types;

import com.bisa.bus.servicios.mojix.model.Geografica;
import com.bisa.bus.servicios.mojix.model.Request;
import com.bisa.bus.servicios.mojix.model.Transfer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by josanchez on 22/07/2016.
 * @author rsalvatierra on 14/10/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestTransferenciaACH extends JsonRequestTransferencia implements Serializable, Request, Transfer {

    private String bancoDestino;

    public JsonRequestTransferenciaACH() {
    }

    public JsonRequestTransferenciaACH(String bisaClienteID, String cuentaOrigen, String cuentaDestino, String moneda, BigDecimal monto, String motivo, String transactionID, Geografica geoProperties) {
        setBisaClienteID(bisaClienteID);
        setCuentaOrigen(cuentaOrigen);
        setCuentaDestino(cuentaDestino);
        setMoneda(moneda);
        setMonto(monto);
        setMotivo(motivo);
        setTransactionID(transactionID);
        setGeoProperties(geoProperties);
    }

    public String getBancoDestino() {
        return bancoDestino;
    }

    public void setBancoDestino(String bancoDestino) {
        this.bancoDestino = bancoDestino;
    }

    @Override
    public String toString() {
        return "JsonRequestTransferenciaACH{" +
                "bancoDestino='" + bancoDestino + '\'' +
                '}';
    }
}
