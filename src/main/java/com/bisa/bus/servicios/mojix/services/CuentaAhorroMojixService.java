package com.bisa.bus.servicios.mojix.services;

import com.bisa.bus.servicios.mojix.services.types.JsonRequestAhorro;
import com.bisa.bus.servicios.mojix.services.types.JsonRequestConfirmar;
import com.bisa.bus.servicios.mojix.services.types.JsonRequestCuotaAhorro;
import com.bisa.bus.servicios.mojix.services.types.JsonResultComun;

/**
 * @author by rsalvatierra on 22/06/2017.
 */
public interface CuentaAhorroMojixService {
    JsonResultComun crearCuentaAhorro(JsonRequestAhorro requestAhorro, String usuario);

    JsonResultComun confirmarCuentaAhorro(JsonRequestConfirmar requestConfirmar, String usuario);

    JsonResultComun iniciarCuotaAhorro(JsonRequestCuotaAhorro requestCuotaAhorro, String usuario);

    JsonResultComun confirmarCuotaAhorro(JsonRequestConfirmar requestConfirmar, String usuario);

    JsonResultComun cuotaAhorro(JsonRequestCuotaAhorro requestCuotaAhorro, String usuario);
}
