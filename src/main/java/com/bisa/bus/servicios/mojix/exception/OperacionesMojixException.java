package com.bisa.bus.servicios.mojix.exception;

/**
 * @author by josanchez on 18/08/2016.
 */
public class OperacionesMojixException extends Exception {

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    public OperacionesMojixException(String message) {
        super(message);
    }
}
