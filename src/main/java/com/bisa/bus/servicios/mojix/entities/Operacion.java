package com.bisa.bus.servicios.mojix.entities;

import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.mojix.model.EstadoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoProceso;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author by josanchez on 16/08/2016.
 * @author rsalvatierra modificado on 20/10/2016
 */
@Entity
@Table(name = "SJOV10")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S10FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S10USRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S10FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S10USRMOD"))
})
public class Operacion extends EntityBase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S10CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "S10CLISEG")
    private ClienteJoven cliente;

    @Column(name = "S10TRANID", columnDefinition = "VARCHAR(40)")
    private String transaccionID;

    @Enumerated(EnumType.STRING)
    @Column(name = "S10TIPROC")
    private TipoOperacion tipoOperacion;

    @Enumerated(EnumType.STRING)
    @Column(name = "S10PROCES")
    private TipoProceso proceso;

    @Enumerated(EnumType.STRING)
    @Column(name = "S10ESTOPE")
    private EstadoOperacion estadoOperacion;

    public Operacion() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClienteJoven getCliente() {
        return cliente;
    }

    public void setCliente(ClienteJoven cliente) {
        this.cliente = cliente;
    }

    public String getTransaccionID() {
        return transaccionID;
    }

    public void setTransaccionID(String transaccionID) {
        this.transaccionID = transaccionID;
    }

    public TipoOperacion getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(TipoOperacion tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public TipoProceso getProceso() {
        return proceso;
    }

    public void setProceso(TipoProceso proceso) {
        this.proceso = proceso;
    }

    public EstadoOperacion getEstadoOperacion() {
        return estadoOperacion;
    }

    public void setEstadoOperacion(EstadoOperacion estadoOperacion) {
        this.estadoOperacion = estadoOperacion;
    }

    public String getFechaConsumo() {
        return FormatosUtils.formatoFechaHora(getFechaCreacion());
    }

    @Override
    public String toString() {
        return "TipoOperacion{" +
                "id=" + getId() +
                ", tipoTransferencia='" + getTipoOperacion() + '\'' +
                ", TransaccionID='" + getTransaccionID() + '\'' +
                ", estadoOperacion='" + getEstadoOperacion() + '\'' +
                '}';
    }
}
