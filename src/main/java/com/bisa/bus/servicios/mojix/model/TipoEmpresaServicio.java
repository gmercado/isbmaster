package com.bisa.bus.servicios.mojix.model;

import org.apache.commons.lang.StringUtils;

/**
 * @author by rsalvatierra on 12/07/2017.
 */
public enum TipoEmpresaServicio {
    ENTEL("TEL", "PREPAGO"),
    VIVA("TEL", "PREPAGO"),
    TIGO("CEL", "PREPAGO");

    private String codigo;
    private String descripcion;

    TipoEmpresaServicio(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static TipoEmpresaServicio get(String codigo) {
        for (TipoEmpresaServicio valor : TipoEmpresaServicio.values()) {
            String tipoServicio = valor.getDescripcion() + "-" + valor.name();
            if (tipoServicio.equals(StringUtils.trimToEmpty(codigo))) {
                return valor;
            }
        }
        return null;
    }

    public static TipoEmpresaServicio get2(String codigo) {
        for (TipoEmpresaServicio valor : TipoEmpresaServicio.values()) {
            String tipoServicio = valor.name() + "-" + valor.getDescripcion();
            if (tipoServicio.equals(StringUtils.trimToEmpty(codigo))) {
                return valor;
            }
        }
        return null;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
