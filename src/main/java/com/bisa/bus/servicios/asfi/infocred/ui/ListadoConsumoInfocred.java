package com.bisa.bus.servicios.asfi.infocred.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.asfi.infocred.api.BitacoraInfocredService;
import com.bisa.bus.servicios.asfi.infocred.api.ReporteConsumos;
import com.bisa.bus.servicios.asfi.infocred.dao.BitacoraInfocredDao;
import com.bisa.bus.servicios.asfi.infocred.entities.BitacoraInfocred;
import com.google.inject.Inject;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler;
import org.apache.wicket.util.resource.AbstractResourceStreamWriter;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@AuthorizeInstantiation({RolesBisa.INFOCRED_ADMIN})
@Menu(value = "Reporte Consumo Infocred", subMenu = SubMenu.INFOCRED)
public class ListadoConsumoInfocred extends Listado<BitacoraInfocred, Long> {

    @Inject
    BitacoraInfocredService bitacoraInfocredService;

    @Override
    protected ListadoPanel<BitacoraInfocred, Long> newListadoPanel(String id) {

        return new ListadoPanel<BitacoraInfocred, Long>(id) {
            @Override
            protected List<IColumn<BitacoraInfocred, String>> newColumns(ListadoDataProvider<BitacoraInfocred, Long> dataProvider) {
                List<IColumn<BitacoraInfocred, String>> columns = new LinkedList<>();
                columns.add(new PropertyColumn<>(Model.of("ID"), "id", "id"));
                columns.add(new PropertyColumn<>(Model.of("Fecha"), "fechaConsumo", "fechaConsumo"));
                columns.add(new PropertyColumn<>(Model.of("Agencia"), "oficina.descripcion", "oficina.descripcion"));
                columns.add(new PropertyColumn<>(Model.of("Usuario"), "codigoUsuario", "codigoUsuario"));
                columns.add(new PropertyColumn<>(Model.of("Respuesta"), "codError", "codError"));
                columns.add(new PropertyColumn<>(Model.of("Estado"), "estadoConsulta", "estadoConsulta"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<BitacoraInfocred, Long>> getProviderClazz() {
                return BitacoraInfocredDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<BitacoraInfocred> newFiltroPanel(String id, IModel<BitacoraInfocred> model1, IModel<BitacoraInfocred> model2) {
                ListadoConsumosInfocredFiltroPanel filtro = new ListadoConsumosInfocredFiltroPanel(id, model1, model2);
                filtro.add(new Link("saveExcel") {
                    @Override
                    public void onClick() {
                        if (getModel1() == null || getModel1().getObject() == null) {
                            getSession().error("Hubo un error al exportar el archivo.");
                            return;
                        }
                        AbstractResourceStreamWriter resourceStreamWriter = new AbstractResourceStreamWriter() {
                            @Override
                            public void write(final OutputStream output) throws IOException {
                                List<BitacoraInfocred> consumos = bitacoraInfocredService.getConsumos(getModel1().getObject(), getModel2().getObject());
                                if (consumos != null && consumos.size() > 0) {
                                    byte[] reporte = ReporteConsumos.get(consumos);
                                    if (reporte != null) {
                                        output.write(reporte);
                                        output.flush();
                                        output.close();
                                    }
                                }
                            }
                        };

                        getRequestCycle().scheduleRequestHandlerAfterCurrent(new ResourceStreamRequestHandler(resourceStreamWriter)
                                .setFileName(ReporteConsumos.NOMBRE_REPORTE + ".csv"));
                    }
                });
                return filtro;
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("fechaHoraConsulta", false);
            }


            @Override
            protected BitacoraInfocred newObject2() {
                return new BitacoraInfocred();
            }

            @Override
            protected BitacoraInfocred newObject1() {
                return new BitacoraInfocred();
            }

            @Override
            protected boolean isVisibleData() {
                BitacoraInfocred bitacora = getModel1().getObject();
                BitacoraInfocred bitacora2 = getModel2().getObject();
                return !((bitacora.getFechaHoraConsulta() == null || bitacora2.getFechaHoraConsulta() == null));
            }
        };
    }
}
