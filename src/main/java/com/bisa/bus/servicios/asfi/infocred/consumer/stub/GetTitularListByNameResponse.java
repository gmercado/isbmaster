
package com.bisa.bus.servicios.asfi.infocred.consumer.stub;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetTitularListByNameResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTitularListByNameResult"
})
@XmlRootElement(name = "GetTitularListByNameResponse")
public class GetTitularListByNameResponse {

    @XmlElement(name = "GetTitularListByNameResult")
    protected String getTitularListByNameResult;

    /**
     * Obtiene el valor de la propiedad getTitularListByNameResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetTitularListByNameResult() {
        return getTitularListByNameResult;
    }

    /**
     * Define el valor de la propiedad getTitularListByNameResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetTitularListByNameResult(String value) {
        this.getTitularListByNameResult = value;
    }

}
