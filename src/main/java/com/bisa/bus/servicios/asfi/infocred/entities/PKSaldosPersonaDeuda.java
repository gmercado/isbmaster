package com.bisa.bus.servicios.asfi.infocred.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@Embeddable
public class PKSaldosPersonaDeuda {

    @Column(name = "I27ID")
    private Long id;

    @Column(name = "I27FILA")
    private Long fila;

    @Column(name = "I27TIPO")
    private String tipo;

    @Column(name = "I27GESTION")
    private String gestion;

    public PKSaldosPersonaDeuda() {
    }

    public PKSaldosPersonaDeuda(Long id, Long fila, String tipo, String gestion) {
        this.id = id;
        this.fila = fila;
        this.tipo = tipo;
        this.gestion = gestion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PKSaldosPersonaDeuda that = (PKSaldosPersonaDeuda) o;

        if (!id.equals(that.id)) return false;
        if (!fila.equals(that.fila)) return false;
        if (!tipo.equals(that.tipo)) return false;
        return gestion.equals(that.gestion);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + fila.hashCode();
        result = 31 * result + tipo.hashCode();
        result = 31 * result + gestion.hashCode();
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFila() {
        return fila;
    }

    public void setFila(Long fila) {
        this.fila = fila;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getGestion() {
        return gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    @Override
    public String toString() {
        return "PKSaldosPersonaDeuda{" +
                "id=" + id +
                ", fila=" + fila +
                ", tipo='" + tipo + '\'' +
                ", gestion='" + gestion + '\'' +
                '}';
    }
}
