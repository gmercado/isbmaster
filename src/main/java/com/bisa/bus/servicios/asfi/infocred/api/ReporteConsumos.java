package com.bisa.bus.servicios.asfi.infocred.api;

import com.bisa.bus.servicios.asfi.infocred.entities.BitacoraInfocred;
import com.bisa.bus.servicios.segip.api.ConfiguracionSegip;
import org.apache.commons.lang.StringUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import static bus.plumbing.utils.Convert.FileToBytes;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class ReporteConsumos {
    public static final String NOMBRE_REPORTE = "consumos-INFOCRED";
    private static final String separador = ",";

    public static byte[] get(List<BitacoraInfocred> consumos) {
        File file;
        BufferedWriter bw;
        try {
            file = File.createTempFile(NOMBRE_REPORTE, ConfiguracionSegip.ARCHIVO_CSV);
            bw = new BufferedWriter(new FileWriter(file));
            bw.append("FECHA").append(separador);
            bw.append("AGENCIA").append(separador);
            bw.append("USUARIO").append(separador);
            bw.append("CANAL").append(separador);
            bw.append("RESPUESTA").append(separador);
            bw.append("ESTADO").append(separador);

            for (BitacoraInfocred orden : consumos) {
                bw.append('\n');
                bw.append(StringUtils.trimToEmpty(orden.getFechaConsumo())).append(separador);
                bw.append(StringUtils.trimToEmpty(orden.getOficina().getDescripcion())).append(separador);
                bw.append(StringUtils.trimToEmpty(orden.getCodigoUsuario())).append(separador);
                bw.append(StringUtils.trimToEmpty(orden.getCanalUsuario())).append(separador);
                bw.append(StringUtils.trimToEmpty(orden.getCodError())).append(separador);
                bw.append(StringUtils.trimToEmpty(orden.getEstadoConsulta())).append(separador);
            }
            bw.flush();
            bw.close();
        } catch (IOException e) {
            return null;
        }
        return FileToBytes(file);
    }

    private static String format(String cadena) {
        return cadena.replace(",", ";").replace("\n", "").replace("\r", "");
    }
}
