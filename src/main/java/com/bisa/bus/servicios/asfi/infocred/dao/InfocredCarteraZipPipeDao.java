package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.env.api.Variables;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.asfi.infocred.api.InfocredCarteraZipPipeService;
import com.bisa.bus.servicios.asfi.infocred.api.InfocredException;
import com.bisa.bus.servicios.asfi.infocred.entities.AlertaCabecera;
import com.bisa.bus.servicios.asfi.infocred.entities.InfocredCarteraZipPipe;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.openlogics.gears.jdbc.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class InfocredCarteraZipPipeDao extends DaoImpl<InfocredCarteraZipPipe, Long> implements InfocredCarteraZipPipeService {

    private static final Logger log = LoggerFactory.getLogger(InfocredCarteraZipPipeDao.class);

    @Inject
    protected InfocredCarteraZipPipeDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    public boolean guardarCartera(String nombreArchivo, List<String> list, String separador) {
        if (list == null) {
            throw new InfocredException("El archivo de importacion cartera PIPE no es correcto.");
        }
        org.openlogics.gears.jdbc.ObjectDataStore ds = null;
        AlertaCabecera alertaCabecera = null;
        try {

            ds = org.openlogics.gears.jdbc.DataStoreFactory.createObjectDataStore(getConnection());
            ds.setAutoClose(false);
            ds.setAutoCommit(false);

            alertaCabecera = new AlertaCabecera("CARTERA PROPIA", FormatosUtils.fechaHoraFormateadaConYY(new Date()), nombreArchivo);
            alertaCabecera = ds.add(alertaCabecera);
            ds.commit();

            int numeroRegistros = 0;
            for (int i = 0; i < list.size(); i++) {
                int numeroLinea = i + 1;
                String linea = list.get(i);
                if (StringUtils.isBlank(linea) || i == 0) {
                    log.debug("Linea {} en blanco", numeroLinea);
                    continue;
                }
                String[] attrs = Iterables.toArray(Splitter.on(separador).trimResults().split(linea), String.class);
                if (attrs.length != 35) {
                    throw new InfocredException("El número de columnas no es el correcto en la fila: " + numeroLinea);
                }
                InfocredCarteraZipPipe infocredCarteraZipPipe = new InfocredCarteraZipPipe(alertaCabecera.getId(), attrs);
                ds.add(infocredCarteraZipPipe);
                numeroRegistros++;
            }
            // Actualizamos con el numero de registro guardados
            ds.update(Query.of("UPDATE IFP030 SET I30NROREG = ?, I30FECALT = ?, I30USRALT = ? WHERE I30ID = ?",
                    numeroRegistros, new Date(), Variables.INFOCRED_USUARIO_ALTA_JOBS, alertaCabecera.getId()));

            return true;
        } catch (Exception e) {
            log.error("Error al tratar de guardar los registros de cartera ", e);
            // Actualizamos la fecha de proceso como error si existe algun problema en la carga
            try {
                ds.update(Query.of("UPDATE IFP030 SET I30PERPROC = ? WHERE I30ID = ?",
                        FormatosUtils.fechaHoraFormateadaConYY(new Date()) + "-ERROR", alertaCabecera!= null ? alertaCabecera.getId() : 0));
            } catch (SQLException e1) {
                log.error("Error al actualizar la cabecera de error de la cartera", e1);
            }
            throw new InfocredException(e instanceof InfocredException ? ((InfocredException)e).getValidationErrors() : e.getMessage());
        } finally {
            if (ds != null) {
                try {
                    ds.tryCommitAndClose();
                } catch (SQLException e) {
                    log.error("No se pudo hacer commit en cartera pipe");
                }
            }
        }
    }
}
