package com.bisa.bus.servicios.asfi.infocred.api;

import bus.consumoweb.enums.CodigoRespuesta;
import bus.consumoweb.enums.TipoConsumoRespuesta;
import bus.consumoweb.enums.TipoEstadoTitular;
import bus.consumoweb.enums.TipoServicio;
import bus.consumoweb.infocred.objetos.DatosTitular;
import bus.consumoweb.infocred.objetos.DatosTitularLista;
import bus.consumoweb.infocred.utilitarios.DomUtil;
import bus.consumoweb.infocred.utilitarios.XmlUtil;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.JasperReportUtil;
import com.bisa.bus.servicios.asfi.infocred.entities.InformeConfidencial;
import com.bisa.bus.servicios.asfi.infocred.entities.Titulares;
import com.bisa.bus.servicios.asfi.infocred.model.RespuestaConsulta;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.ws.WebServiceException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import static bus.env.api.Variables.NUMERO_DIAS_VALIDO_REGISTRO_INFOCRED;
import static bus.env.api.Variables.NUMERO_DIAS_VALIDO_REGISTRO_INFOCRED_DEFAULT;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class InfocredServiceImpl implements InfocredService, Serializable {

    private static final Logger log = LoggerFactory.getLogger(InfocredServiceImpl.class);

    private static String NOMBRE_REPORTE = "Infocred-";
    private Titulares titular;
    private RespuestaConsulta respuestaConsulta;
    private SolicitudConsultaTitularInfocredForm request;

    private MedioAmbiente medioAmbiente;
    private ClienteWebServiceInfocred cliente;
    private TitularInfocredService titularInfocredService;
    private BitacoraInfocredService bitacoraInfocredService;
    private SaldosPersonaDeudaService saldosPersonaDeudaService;
    private InformeConfidencialService informeConfidencialService;

    public InfocredServiceImpl() {
    }

    @Inject
    public InfocredServiceImpl(InformeConfidencialService informeConfidencialService, MedioAmbiente medioAmbiente,
                               ClienteWebServiceInfocred cliente, TitularInfocredService titularInfocredService,
                               BitacoraInfocredService bitacoraInfocredService, SaldosPersonaDeudaService saldosPersonaDeudaService) {
        this.cliente = cliente;
        this.medioAmbiente = medioAmbiente;
        this.titularInfocredService = titularInfocredService;
        this.bitacoraInfocredService = bitacoraInfocredService;
        this.saldosPersonaDeudaService = saldosPersonaDeudaService;
        this.informeConfidencialService = informeConfidencialService;
    }

    public List<Titulares> obtenerDatosTitularPorNombre(SolicitudConsultaTitularInfocredForm request) {
        List<Titulares> titulares;
        try {
            String response = cliente.consumo(request, ClienteWebServiceInfocred.MetodoWebservice.TITULAR_LIST_BY_NAME);
            log.debug("Ingresa a consultar datos de titulares respuesta {}", response);
            Document document = DomUtil.stringToDocument(response);
            document.getDocumentElement().normalize();
            NodeList nList = document.getElementsByTagName("titulares");
            if (nList != null && nList.getLength() > 0) {
                titulares = new ArrayList<>();
                DatosTitularLista dt = XmlUtil.xmlToObject(response, DatosTitularLista.class);
                for (DatosTitular datosTitular : dt.getTitulares()) {
                    Titulares titular = new Titulares(datosTitular);
                    titulares.add(titular);
                }
                return titulares;
            }
        } catch (Exception e) {
            log.error("Error al verificar conectividad con infocred ..... ", e);
        }
        return null;
    }

    @Override
    public RespuestaConsulta getInfoCrediticioTitular(SolicitudConsultaTitularInfocredForm request) {
        try {
            setRequest(request);
            //Por parametro validamos si es permitida la consulta al WS INFOCRED
            if (esHabilitadoConsumo()) {
                // Verificamos si existe el registro en base de datos
                log.debug("Esta forzando la consulta {} ", request.isForzarConsulta() ? "SI" : "NO");
                if (!request.isForzarConsulta()) {
                    respuestaConsulta = existeRegistroEnBase();
                    if (respuestaConsulta != null && (CodigoRespuesta.OK.equals(respuestaConsulta.getCodError())
                            || CodigoRespuesta.RESPUESTA_INFOCRED_DUPLICADO.equals(respuestaConsulta.getCodError()))) {
                        return respuestaConsulta;
                    }
                }
                // Antes de consumir el servicio verificamos si los titulares estan en Base de datos si es forzada
                if (request.isForzarConsulta() && Strings.isNullOrEmpty(request.getTitular().getNombreCompleto())) {
                    List<Titulares> titulares = titularInfocredService.buscarTitularesPorDocumento(request);
                    // si tenemos resultados en base de datos se debe cambiar el estado a eliminado
                    for (Titulares titular : titulares) {
                        titular.setEstado(TipoEstadoTitular.E.toString());
                        titularInfocredService.cabiarEstado(titular);
                    }
                }
                respuestaConsulta = consumirServicioINFOCRED();
                return respuestaConsulta;

            } else {
                log.info("Solo esta habilitado para Buscar en Base de datos, no podrá consumir WS");
                respuestaConsulta = existeRegistroEnBase();
                return respuestaConsulta;
            }
        } finally {
            bitacoraInfocredService.registrar(request, respuestaConsulta);
        }
    }

    public RespuestaConsulta existeRegistroEnBase() {
        Connection conn = null;
        try {
            List<Titulares> titulares = titularInfocredService.buscarTitularesPorDocumento(request);
            if (titulares.size() > 0) {
                titular = titulares.get(0);
                DatosTitularLista dtl = new DatosTitularLista();
                for (Titulares titular : titulares) {
                    DatosTitular dt = new DatosTitular(titular);
                    dtl.getTitulares().add(dt);
                }
                if (titulares.size() > 1) {
                    return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSULTA,
                            CodigoRespuesta.RESPUESTA_INFOCRED_DUPLICADO, XmlUtil.objectToXml(dtl));
                }

                // Verificamos si es un registro válido
                long diaDelMes = medioAmbiente.getValorLongDe(NUMERO_DIAS_VALIDO_REGISTRO_INFOCRED, NUMERO_DIAS_VALIDO_REGISTRO_INFOCRED_DEFAULT);
                Calendar fechaActualizacion = GregorianCalendar.getInstance();
                fechaActualizacion.set(Calendar.DAY_OF_MONTH, (int) diaDelMes);
                long diferencia = FormatosUtils.diferenciaDiasEntreFechas(fechaActualizacion.getTime(), titular.getFechaCreacion());

                if (diferencia >= diaDelMes) {
                    log.info("El registro no esta actualizado se procede a la actualizacion del titular {}", request.getTitular().getDocumento());
                    titular.setEstado(TipoEstadoTitular.E.toString());
                    titularInfocredService.cabiarEstado(titular);
                    titular = null;
                    return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSULTA,
                            CodigoRespuesta.ERR_CON_DATA_DESACTUALIZADA, CodigoRespuesta.ERR_CON_DATA_DESACTUALIZADA.getDescripcion(), null);
                }

                if (!TipoEstadoTitular.P.toString().equals(titular.getEstado())) {
                    return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSULTA,
                            CodigoRespuesta.CONSULTA_NO_INICIADA, CodigoRespuesta.CONSULTA_NO_INICIADA.getDescripcion(), null);
                }

                conn = informeConfidencialService.unWrapConnection();
                if (conn == null) {
                    return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSULTA,
                            CodigoRespuesta.ERR_CONEXION, CodigoRespuesta.ERR_CONEXION.getDescripcion(), null);
                }
                // Si este usuario fue consultado anteriormente, actualizar auditoria
                titular.setUsuarioModificador(request.getUsuario().getNombreCompleto());
                titular.setFechaModificacion(new Date());
                titularInfocredService.guardar(titular);

                // Obtener la libreria o esquema de base de datos para los QUERIES de JASPER
                String libreria = medioAmbiente.getValorDe(Variables.INFOCRED_LIBRERIA_REPORTES, Variables.INFOCRED_LIBRERIA_REPORTES_DEFAULT);
                log.info("Los reportes Jasper realizaran los queries sobre la libreria {}", libreria);
                // pasamos por parametros el id y la gestion mayor del historial de deudas del titular
                String gestion = saldosPersonaDeudaService.buscarGestionMayor(titular.getIdCabecera());
                Map<String, Object> param = new HashMap<>();
                param.put("libreria", libreria);
                param.put("idParam", titular.getIdCabecera());
                param.put("gestion", Integer.parseInt(gestion));
                // Obtener el byte[]
                byte[] stream = JasperReportUtil.generarReporteStream(param,
                        NOMBRE_REPORTE + FormatosUtils.getFechaByFormatoDDMMYYYYhhmmss(new Date()), conn);

                return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSULTA,
                        CodigoRespuesta.OK, CodigoRespuesta.OK.getDescripcion(), stream, titular.getIdCabecera());
            }
        } catch (Exception e) {
            log.error("Ocurrio un error al verificar titular en Base de datos {} ", e);
            return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSULTA,
                    CodigoRespuesta.ERR_INESPERADO, CodigoRespuesta.ERR_INESPERADO.getDescripcion(), null);
        } finally {
            if (conn != null) {
                try {
                    if (!conn.isClosed()) {
                        conn.close();
                    }
                } catch (SQLException ign) {}
            }
        }
        return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSULTA,
                CodigoRespuesta.SIN_RESULTADOS, CodigoRespuesta.SIN_RESULTADOS.getDescripcion(), null);
    }

    public boolean esHabilitadoConsumo() {
        String valor = medioAmbiente.getValorDe(Variables.ES_HABILITADO_CONSUMO_INFOCRED, Variables.ES_HABILITADO_CONSUMO_INFOCRED_DEFAULT);
        if (!Strings.isNullOrEmpty(valor) && "SI".equals(valor.trim().toUpperCase()))
            return true;
        return false;
    }

    public RespuestaConsulta consumirServicioINFOCRED() {
        log.info("Consulta generada por el usuario {}", request.getUsuario());
        String response = "";
        try {
            response = cliente.consumo(request, ClienteWebServiceInfocred.MetodoWebservice.INDIVIDUAL_REPORT);
            // Si no tenemos respuesta ocurrio un error en el consumo

            if (Strings.isNullOrEmpty(response)) {
                log.error("El webservice no retornó una respuesta válida");
                return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSUMO,
                        CodigoRespuesta.ERR_CON_INFOCRED, CodigoRespuesta.ERR_CON_INFOCRED.getDescripcion(), null);
            }
            Document document = DomUtil.stringToDocument(response);
            document.getDocumentElement().normalize();
            NodeList nList = document.getElementsByTagName("titulares");
            // Si tiene mas de un resultado significa que es CI duplicado, nos retorna la estructura de Titutlares
            if (nList.getLength() > 0) {
                // Si es primer consumo de CI duplicados guardamos todos
                DatosTitularLista dt = XmlUtil.xmlToObject(response, DatosTitularLista.class);
                for (DatosTitular datosTitular : dt.getTitulares()) {
                    Titulares titular = new Titulares(datosTitular);
                    titular.setEstado(TipoEstadoTitular.N.toString());
                    titularInfocredService.guardar(titular);
                }
                return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSUMO,
                        CodigoRespuesta.RESPUESTA_INFOCRED_DUPLICADO, response);
            }
            // Si no retorna resultados el servicio de infocred
            nList = document.getElementsByTagName("datosPersonales");
            if (nList.getLength() == 0) {
                return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSUMO,
                        CodigoRespuesta.SIN_RESULTADOS, CodigoRespuesta.SIN_RESULTADOS.getDescripcion(), null);
            }
            // Primero guarda los datos del titular consultado
            if (titular == null) {
                // verificamos si existe el titular en base
                List<Titulares> list = titularInfocredService.buscarTitularesPorDocumento(request);
                if (list.size() == 0) {
                    titular = new Titulares(request.getTitular().getDocumento(), String.valueOf(request.getTitular().getTipoDocumento()), request.getTitular().getNombreCompleto());
                    titular = titularInfocredService.guardar(titular);
                }
                if (list.size() == 1) {
                    titular = list.get(0);
                }
            }
            if (titular == null || titular.getId() == null)
                return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSUMO,
                        CodigoRespuesta.ERR_BASE_DE_DATOS_INFOCRED, CodigoRespuesta.ERR_BASE_DE_DATOS_INFOCRED.getDescripcion(), null);
            // Se persiste el resultado de la consulta
            InformeConfidencial ic = informeConfidencialService.registrarInformeCofidencial(response, request);

            if (ic == null || ic.getId() == null || ic.getId().compareTo(0L) == 0) {
                return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSUMO,
                        CodigoRespuesta.ERR_BASE_DE_DATOS_INFOCRED, CodigoRespuesta.ERR_BASE_DE_DATOS_INFOCRED.getDescripcion(), null);
            }
            // Si no hubo problemas se cambia el estado del titular y se asigna el id de la cabecera
            titular.setEstado(TipoEstadoTitular.P.toString());
            titular.setIdCabecera(ic.getId());
            titularInfocredService.cabiarEstado(titular);

            Connection conn = informeConfidencialService.unWrapConnection();
            if (conn == null) {
                return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSULTA,
                        CodigoRespuesta.ERR_CONEXION, CodigoRespuesta.ERR_CONEXION.getDescripcion(), null);
            }
            // Obtener la libreria o esquema de base de datos para los QUERIES de JASPER
            String libreria = medioAmbiente.getValorDe(Variables.INFOCRED_LIBRERIA_REPORTES, Variables.INFOCRED_LIBRERIA_REPORTES_DEFAULT);
            log.info("Los reportes Jasper realizaran los queries sobre la libreria {}", libreria);
            // pasamos por parametros el id y la gestion mayor del historial de deudas del titular
            String gestion = saldosPersonaDeudaService.buscarGestionMayor(titular.getIdCabecera());
            Map<String, Object> param = new HashMap<>();
            param.put("libreria", libreria);
            param.put("idParam", titular.getIdCabecera());
            param.put("gestion", Integer.parseInt(gestion));
            // Obtener el byte[]
            byte[] stream = JasperReportUtil.generarReporteStream(param,
                    NOMBRE_REPORTE + FormatosUtils.getFechaByFormatoDDMMYYYYhhmmss(new Date()), conn);

            return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSUMO,
                    CodigoRespuesta.OK, CodigoRespuesta.OK.getDescripcion(), stream, ic.getId());

        } catch (WebServiceException e) {
            log.error("Error en el consumo del WS obtener titular de infocred ", e);
            return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSUMO,
                    CodigoRespuesta.ERR_CON_INFOCRED, CodigoRespuesta.ERR_CON_INFOCRED.getDescripcion(), null);

        } catch (Exception e) {
            log.error("Ocurrio un error inesperado ", e);
            return new RespuestaConsulta(TipoServicio.INFOCRED, TipoConsumoRespuesta.CONSUMO,
                    CodigoRespuesta.ERR_INESPERADO, CodigoRespuesta.ERR_INESPERADO.getDescripcion(), null);
        }
    }

    public void setRequest(SolicitudConsultaTitularInfocredForm request) {
        this.request = request;
    }
}
