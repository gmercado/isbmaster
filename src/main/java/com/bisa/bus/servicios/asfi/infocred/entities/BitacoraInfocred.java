package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.consumoweb.enums.TipoConsumoRespuesta;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.segip.entities.Oficina;
import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@Entity
@Table(name = "IFP010")
public class BitacoraInfocred implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I10ID", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "I10NOMUSR", columnDefinition = "CHAR(255) default user", nullable = false)
    private String codigoUsuario;

    @Column(name = "I10OFIUSR")
    private String oficinaUsuario;

    @Column(name = "I10CANUSR")
    private String canalUsuario;

    @Column(name = "I10TIPUSR")
    private String tipoUsuario;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "I10FECCON")
    private Date fechaHoraConsulta;

    @Column(name = "I10CODERR")
    private String codError;

    @Column(name = "I10CODRES")
    private String tipoRespuesta;

    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "I10OFIUSR")
    private Oficina oficina;

//    @Transient
//    private TipoRespuesta tipoRespuestaEnum;

    public BitacoraInfocred() {
    }

    public BitacoraInfocred(String codigoUsuario, String oficinaUsuario, String canalUsuario,
                            String tipoUsuario, Date fechaHoraConsulta, String codError, String tipoRespuesta) {
        this.codigoUsuario = codigoUsuario;
        this.oficinaUsuario = oficinaUsuario;
        this.canalUsuario = canalUsuario;
        this.tipoUsuario = tipoUsuario;
        this.fechaHoraConsulta = fechaHoraConsulta;
        this.codError = codError;
        this.tipoRespuesta = tipoRespuesta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getOficinaUsuario() {
        return oficinaUsuario;
    }

    public void setOficinaUsuario(String oficinaUsuario) {
        this.oficinaUsuario = oficinaUsuario;
    }

    public String getCanalUsuario() {
        return canalUsuario;
    }

    public void setCanalUsuario(String canalUsuario) {
        this.canalUsuario = canalUsuario;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public Date getFechaHoraConsulta() {
        return fechaHoraConsulta;
    }

    public void setFechaHoraConsulta(Date fechaHoraConsulta) {
        this.fechaHoraConsulta = fechaHoraConsulta;
    }

    public String getCodError() {
        return codError;
    }

    public void setCodError(String codError) {
        this.codError = codError;
    }

    public String getTipoRespuesta() {
        return tipoRespuesta;
    }

    public void setTipoRespuesta(String tipoRespuesta) {
        this.tipoRespuesta = tipoRespuesta;
    }

    public String getFechaConsumo() {
        return FormatosUtils.formatoFechaHora(getFechaHoraConsulta());
    }

    public Oficina getOficina() {
        return oficina;
    }

    public void setOficina(Oficina oficina) {
        this.oficina = oficina;
    }

//    public TipoRespuesta getTipoRespuestaEnum() {
//        return tipoRespuestaEnum;
//    }
//
//    public void setTipoRespuestaEnum(TipoRespuesta tipoRespuestaEnum) {
//        this.tipoRespuestaEnum = tipoRespuestaEnum;
//    }

    public String getEstadoConsulta() {
        String tipo = "-";
        TipoConsumoRespuesta tipoR;
        if (StringUtils.isNotBlank(getTipoRespuesta())) {
            tipoR = TipoConsumoRespuesta.get(StringUtils.trimToEmpty(getTipoRespuesta()));
            if (tipoR != null) {
                tipo = tipoR.toString();
            }
        }
        return tipo;
    }
}
