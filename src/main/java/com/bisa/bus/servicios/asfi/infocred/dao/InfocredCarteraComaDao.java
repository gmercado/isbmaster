package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.env.api.Variables;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.asfi.infocred.api.InfocredCarteraComaService;
import com.bisa.bus.servicios.asfi.infocred.api.InfocredException;
import com.bisa.bus.servicios.asfi.infocred.entities.AlertaCabecera;
import com.bisa.bus.servicios.asfi.infocred.entities.InfocredCarteraComa;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.openlogics.gears.jdbc.DataStoreFactory;
import org.openlogics.gears.jdbc.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class InfocredCarteraComaDao extends DaoImpl<InfocredCarteraComa, Long> implements Serializable, InfocredCarteraComaService {

    private static final Logger log = LoggerFactory.getLogger(InfocredCarteraComaDao.class);
    private static String QUOTE = "\"";
    @Inject
    protected InfocredCarteraComaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    public boolean guardarCartera(String nombreArchivo, List<String> list, String separador) {
        if (list == null) {
            throw new InfocredException("El archivo de importacion carteraXLS no es correcto.");
        }
        AlertaCabecera alertaCabecera = null;
        org.openlogics.gears.jdbc.ObjectDataStore ds = null;
        try {
            ds = DataStoreFactory.createObjectDataStore(getConnection());
            ds.setAutoClose(false);
            ds.setAutoCommit(false);

            alertaCabecera = new AlertaCabecera("CARTERA CSV", FormatosUtils.fechaHoraFormateadaConYY(new Date()), nombreArchivo);
            alertaCabecera = ds.add(alertaCabecera);
            ds.commit();

            int numeroRegistros = 0;
            for (int i = 0; i < list.size(); i++) {
                int numeroLinea = i + 1;
                String linea = list.get(i);
                if (StringUtils.isBlank(linea) || i == 0) {
                    log.warn("Linea {} en blanco", numeroLinea);
                    continue;
                }
                String[] attrs = Iterables.toArray(Splitter.on(separador).trimResults().split(linea), String.class);
                // Validamos este archivo por el separador coma y tiene valores decimales con el mismo.
                List<String> attrsValidado = validaSeparadorCalificador(attrs);
                if (attrsValidado.size() != 18) {
                    throw new InfocredException("El número de columnas no es el correcto fila: " + numeroLinea);
                }
                InfocredCarteraComa infocredCarteraComa = new InfocredCarteraComa(alertaCabecera.getId(), attrsValidado);
                ds.add(infocredCarteraComa);
                numeroRegistros++;
            }
            // Actualizamos con el numero de registro guardados
            ds.update(Query.of("UPDATE IFP030 SET I30NROREG = ?, I30FECALT = ?, I30USRALT = ? WHERE I30ID = ?",
                    numeroRegistros, new Date(), Variables.INFOCRED_USUARIO_ALTA_JOBS, alertaCabecera.getId()));
            return true;
        } catch (Exception e) {
            log.error("Error al tratar de guardar los registros de cartera coma ", e);
            // Actualizamos la fecha de proceso como error si existe algun problema en la carga
            try {
                ds.update(Query.of("UPDATE IFP030 SET I30PERPROC = ? WHERE I30ID = ?",
                        FormatosUtils.fechaHoraFormateadaConYY(new Date()) + "-ERROR", alertaCabecera!= null ? alertaCabecera.getId() : 0));
            } catch (SQLException e1) {
                log.error("Error al actualizar la cabecera de error de la cartera", e1);
            }
            throw new InfocredException(e instanceof InfocredException ? ((InfocredException)e).getValidationErrors() : e.getMessage());
        } finally {
            if (ds != null) {
                try {
                    ds.tryCommitAndClose();
                } catch (SQLException e) {
                    log.error("No se pudo hacer commit en cartera coma");
                }
            }
        }
    }

    public static List validaSeparadorCalificador(String[] fields) {
        String quoteTemp = "";
        List<String> result = new ArrayList<>();
        for (int i=0; i<fields.length; i++) {
            if (fields[i].startsWith(QUOTE)) {
                quoteTemp = fields[i].replace(".", "");
                continue;
            }
            if (fields[i].endsWith(QUOTE)) {
                quoteTemp = quoteTemp + "." + fields[i];
                result.add(quoteTemp.replaceAll("^"+QUOTE, "").replaceAll(QUOTE+"$", ""));
                continue;
            }
            result.add(fields[i].replaceAll("^"+QUOTE, "").replaceAll(QUOTE+"$", ""));
        }
        return result;
    }
}
