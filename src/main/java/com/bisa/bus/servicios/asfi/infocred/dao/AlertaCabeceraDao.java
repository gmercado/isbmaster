package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.consumoweb.enums.TipoAlerta;
import bus.consumoweb.infocred.utilitarios.DomUtil;
import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.asfi.infocred.api.AlertaCabeceraService;
import com.bisa.bus.servicios.asfi.infocred.api.InfocredException;
import com.bisa.bus.servicios.asfi.infocred.entities.AlertaCabecera;
import com.bisa.bus.servicios.asfi.infocred.entities.AlertaCuerpo;
import com.bisa.bus.servicios.asfi.infocred.entities.AlertaResumen;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.io.File;
import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class AlertaCabeceraDao extends DaoImpl<AlertaCabecera, Long> implements Serializable, AlertaCabeceraService {

    @Inject
    protected AlertaCabeceraDao(@BasePrincipal EntityManagerFactory entityManagerFactory, MedioAmbiente medioAmbiente) {
        super(entityManagerFactory);
        this.medioAmbiente = medioAmbiente;
    }

    private static final Logger log = LoggerFactory.getLogger(AlertaCabeceraDao.class);

    private final MedioAmbiente medioAmbiente;

    @Override
    public AlertaCabecera guardarAlerta(File archivo, String nombreOriginal) {
        return doWithTransaction((entityManager, t) -> {
            String datos = null;

            Date fechaAlta = new Date();
            String usuarioAlta = Variables.INFOCRED_USUARIO_ALTA_JOBS;
            try {
                datos = DomUtil.obtenerCabeceras(archivo.getPath());
                // Armamos la cabecera
                AlertaCabecera alertaCabecera = new AlertaCabecera(datos, nombreOriginal, usuarioAlta, fechaAlta);
                if (alertaCabecera == null || Strings.isNullOrEmpty(alertaCabecera.getTitulo())) {
                    throw new IllegalArgumentException("Revisar la cabecera del archivo " + nombreOriginal);
                }
                alertaCabecera = persist(alertaCabecera);

                // Inicio de la data del xml, contando los <tr>
                int inicioCuerpo = 0;
                // posiciones con la data válida de los <td>
                String td_data = null;
                // Número de posicion de las columnas de base de datos a insertar la data por <td>
                String columnas = null;

                if (alertaCabecera.getTitulo().toUpperCase().equals(TipoAlerta.CONSULTA_AJENA.getValor())) {
                    inicioCuerpo = medioAmbiente.getValorIntDe(Variables.INFOCRED_ALERTA_INICIO_CUERPO_CONSULTA_AJENA,
                            Variables.INFOCRED_ALERTA_INICIO_CUERPO_CONSULTA_AJENA_DEFAULT);

                    td_data = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_DATOS_CONSULTA_AJENA,
                            Variables.INFOCRED_ALERTA_DATOS_CONSULTA_AJENA_DEFAULT);

                    columnas = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_CONSULTA_AJENA,
                            Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_CONSULTA_AJENA_DEFAULT);
                }

                if (alertaCabecera.getTitulo().toUpperCase().equals(TipoAlerta.CAMBIO_ESTADO.getValor())) {
                    inicioCuerpo = medioAmbiente.getValorIntDe(Variables.INFOCRED_ALERTA_INICIO_CUERPO_CAMBIO_ESTADO,
                            Variables.INFOCRED_ALERTA_INICIO_CUERPO_CAMBIO_ESTADO_DEFAULT);

                    td_data = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_DATOS_CAMBIO_ESTADO,
                            Variables.INFOCRED_ALERTA_DATOS_CAMBIO_ESTADO_DEFAULT);

                    columnas = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_CAMBIO_ESTADO,
                            Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_CAMBIO_ESTADO_DEFAULT);
                }

                if (alertaCabecera.getTitulo().toUpperCase().equals(TipoAlerta.NUEVA_DEUDA.getValor())) {
                    inicioCuerpo = medioAmbiente.getValorIntDe(Variables.INFOCRED_ALERTA_INICIO_CUERPO_NUEVA_DEUDA,
                            Variables.INFOCRED_ALERTA_INICIO_CUERPO_NUEVA_DEUDA_DEFAULT);

                    td_data = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_DATOS_NUEVA_DEUDA,
                            Variables.INFOCRED_ALERTA_DATOS_NUEVA_DEUDA_DEFAULT);

                    columnas = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_NUEVA_DEUDA,
                            Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_NUEVA_DEUDA_DEFAULT);
                }

                if (alertaCabecera.getTitulo().toUpperCase().equals(TipoAlerta.VARIACION_SALDO.getValor())) {
                    inicioCuerpo = medioAmbiente.getValorIntDe(Variables.INFOCRED_ALERTA_INICIO_CUERPO_VARIACION_SALDO,
                            Variables.INFOCRED_ALERTA_INICIO_CUERPO_VARIACION_SALDO_DEFAULT);

                    td_data = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_DATOS_VARIACION_SALDO,
                            Variables.INFOCRED_ALERTA_DATOS_VARIACION_SALDO_DEFAULT);

                    columnas = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_VARIACION_SALDO,
                            Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_VARIACION_SALDO_DEFAULT);
                }

                if (alertaCabecera.getTitulo().toUpperCase().equals(TipoAlerta.NUEVA_CONSULTA_AJENA.getValor())) {
                    inicioCuerpo = medioAmbiente.getValorIntDe(Variables.INFOCRED_ALERTA_INICIO_CUERPO_NUEVA_CONSULTA_AJENA,
                            Variables.INFOCRED_ALERTA_INICIO_CUERPO_NUEVA_CONSULTA_AJENA_DEFAULT);

                    td_data = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_DATOS_NUEVA_CONSULTA_AJENA,
                            Variables.INFOCRED_ALERTA_DATOS_NUEVA_CONSULTA_AJENA_DEFAULT);

                    columnas = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_NUEVA_CONSULTA_AJENA,
                            Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_NUEVA_CONSULTA_AJENA_DEFAULT);
                }

                if (alertaCabecera.getTitulo().toUpperCase().equals(TipoAlerta.RESUMEN_CONSULTA_AJENA.getValor())) {
                    String nombreAlerta = medioAmbiente.getValorDe(Variables.INFOCRED_ARCHIVO_ALERTA_7,
                            Variables.INFOCRED_ARCHIVO_ALERTA_7_DEFAULT);
                    if (nombreOriginal.contains(nombreAlerta)) {
                        inicioCuerpo = medioAmbiente.getValorIntDe(Variables.INFOCRED_ALERTA_INICIO_CUERPO_NUEVO_RESUMEN,
                                Variables.INFOCRED_ALERTA_INICIO_CUERPO_NUEVO_RESUMEN_DEFAULT);

                        td_data = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_DATOS_NUEVO_RESUMEN,
                                Variables.INFOCRED_ALERTA_DATOS_NUEVO_RESUMEN_DEFAULT);

                        columnas = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_NUEVO_RESUMEN,
                                Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_NUEVO_RESUMEN_DEFAULT);
                    } else {
                        inicioCuerpo = medioAmbiente.getValorIntDe(Variables.INFOCRED_ALERTA_INICIO_CUERPO_RESUMEN,
                                Variables.INFOCRED_ALERTA_INICIO_CUERPO_RESUMEN_DEFAULT);

                        td_data = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_DATOS_RESUMEN,
                                Variables.INFOCRED_ALERTA_DATOS_RESUMEN_DEFAULT);

                        columnas = medioAmbiente.getValorDe(Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_RESUMEN,
                                Variables.INFOCRED_ALERTA_COLUMNA_INSERTAR_RESUMEN_DEFAULT);
                    }
                    List<Map<Integer, String>> cuerpo = DomUtil.obtenerCuerpo(archivo.getPath(), inicioCuerpo);
                    List<AlertaResumen> list = listadoCuerpoResumen(alertaCabecera.getId(), cuerpo, td_data, columnas, usuarioAlta, fechaAlta);
                    alertaCabecera.getAlertaResumenes().addAll(list);
                    return merge(alertaCabecera);
                }

                if (inicioCuerpo == 0 || Strings.isNullOrEmpty(td_data) || Strings.isNullOrEmpty(columnas)) {
                    throw new InfocredException("Revisar el formato del archivo o la configuracion de los parámetros de: " + archivo.getName());
                }

                List<Map<Integer, String>> cuerpo = DomUtil.obtenerCuerpo(archivo.getPath(), inicioCuerpo);
                List<AlertaCuerpo> list = listadoCuerpo(alertaCabecera.getId(), cuerpo, td_data, columnas, usuarioAlta, fechaAlta);
                alertaCabecera.getAlertaCuerpos().addAll(list);
                return merge(alertaCabecera);
            } catch (SAXException e) {
                log.error("Ocurrio un error al procesar el xml {}", nombreOriginal, e);
            } catch (Exception e) {
                log.error("Ocurrio un error inesperado al guardar el archivo {}", nombreOriginal, e);
            } finally {
                // Eliminamos el fichero del directorio temporal
                if (archivo != null) {
                    archivo.delete();
                }
            }
            return null;
        });
    }

    public List<AlertaCuerpo> listadoCuerpo(Long idCabecera, List<Map<Integer, String>> cuerpo, String td_data, String columnas, String usuario, Date fecha) {
        try {
            List<AlertaCuerpo> list = new ArrayList<>();
            AlertaCuerpo ac = new AlertaCuerpo(idCabecera, usuario, fecha);
            List<String> inserta_en = Splitter.on(DomUtil.SEPARADOR).splitToList(columnas);

            int cont = 0;
            for (Map<Integer, String> datos : cuerpo) {
                for (Integer llave : datos.keySet()) {
                    if (!td_data.contains(String.valueOf(llave))) {
                        continue;
                    }
                    int columna = Integer.parseInt(inserta_en.get(cont));
                    cont++;
                    switch (columna) {
                        case 1:
                            ac.setTipoDi(datos.get(llave));
                            break;
                        case 2:
                            ac.setNumeroDi(datos.get(llave));
                            break;
                        case 3:
                            ac.setExtension(datos.get(llave));
                            break;
                        case 4:
                            ac.setNombreCompleto(datos.get(llave));
                            break;
                        case 5:
                            ac.setFechaNacimiento(FormatosUtils.deDDMMYYYYaFecha(datos.get(llave)));
                            break;
                        case 6:
                            ac.setEntidad(datos.get(llave));
                            break;
                        case 7:
                            ac.setFecha(FormatosUtils.deDDMMYYYYaFecha(datos.get(llave)
                                    .replace("p.m.","")
                                    .replace("a.m.","")));
                            break;
                        case 8:
                            ac.setTipoObligacion(datos.get(llave));
                            break;
                        case 9:
                            ac.setTipoCredito(datos.get(llave));
                            break;
                        case 10:
                            ac.setEstado(datos.get(llave));
                            break;
                        case 11:
                            ac.setMontoActual(FormatosUtils.stringToBigDecimal(datos.get(llave)));
                            break;
                        case 12:
                            ac.setEstadoAnterior(datos.get(llave));
                            break;
                        case 13:
                            ac.setMontoAnterior(FormatosUtils.stringToBigDecimal(datos.get(llave)));
                            break;
                        case 14:
                            ac.setDepartamento(datos.get(llave));
                            break;
                        case 15:
                            ac.setCalificacionInstitucion(datos.get(llave));
                            break;
                        case 16:
                            ac.setSaldoInstitucion(FormatosUtils.stringToBigDecimal(datos.get(llave)));
                            break;
                        case 17:
                            ac.setEstadoCliente(datos.get(llave));
                            break;
                    }

                    if (cont == inserta_en.size()) {
                        cont = 0;
                        list.add(ac);
                        ac = new AlertaCuerpo(idCabecera, usuario, fecha);
                    }
                }
            }

        return list;

        } catch (Exception e) {
            throw new IllegalArgumentException("Error al parsear la data de ALERTAS", e);
        }
    }

    public List<AlertaResumen> listadoCuerpoResumen(Long idCabecera, List<Map<Integer, String>> cuerpo, String td_data, String columnas, String usuario, Date fechaAlta) {
        try {
            List<AlertaResumen> list = new ArrayList<>();
            AlertaResumen ar = new AlertaResumen(idCabecera, usuario, fechaAlta);
            List<String> inserta_en = Splitter.on(DomUtil.SEPARADOR).splitToList(columnas);

            int cont = 0;
            for (Map<Integer, String> datos : cuerpo) {
                for (Integer llave : datos.keySet()) {
                    if (!td_data.contains(String.valueOf(llave))) {
                        continue;
                    }
                    int columna = Integer.parseInt(inserta_en.get(cont));
                    cont++;
                    switch (columna) {
                        case 1:
                            ar.setEntidad(datos.get(llave));
                            break;
                        case 2:
                            ar.setCantidad(Long.parseLong(datos.get(llave)));
                            break;
                        case 3:
                            ar.setDepartamento(datos.get(llave));
                            break;
                        case 4:
                            ar.setClientesConsultados(Long.parseLong(datos.get(llave)));
                            break;
                        case 5:
                            ar.setClientesCompartidos(Long.parseLong(datos.get(llave)));
                            break;
                        case 6:
                            ar.setClientesPropios(Long.parseLong(datos.get(llave)));
                            break;

                    }
                    if (cont == inserta_en.size()) {
                        cont = 0;
                        list.add(ar);
                        ar = new AlertaResumen(idCabecera, usuario, fechaAlta);
                    }
                }
            }
            return list;
        } catch (Exception e) {
            throw new IllegalArgumentException("Error al parsear la data de ALERTAS", e);
        }
    }

    @Override
    protected Path<Long> countPath(Root<AlertaCabecera> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<AlertaCabecera> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.get("nombreArchivo"));
        return paths;
    }

    public List<AlertaCabecera> buscarPorNombreArchivo(String nombre) {
        return doWithTransaction((entityManager, t) -> {
            String query = "SELECT a FROM AlertaCabecera a WHERE LOWER(a.nombreArchivo) like :nombre";
            Query q = entityManager.createQuery(query);
            q.setParameter("nombre", Strings.emptyToNull(nombre).toLowerCase().trim());
            return q.getResultList();
        });
    }

    public Connection unWrapConnection() {
        return getConnection();
    }
}