package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.consumoweb.enums.TipoEstadoTitular;
import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.asfi.infocred.api.TitularInfocredService;
import com.bisa.bus.servicios.asfi.infocred.entities.Titulares;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class TitularInfocredDao extends DaoImpl<Titulares, Long> implements Serializable, TitularInfocredService {
    private static final Logger log = LoggerFactory.getLogger(TitularInfocredDao.class);

    @Inject
    protected TitularInfocredDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public List<Titulares> buscarTitularesPorDocumento(SolicitudConsultaTitularInfocredForm request) {
        return doWithTransaction((entityManager, t) -> {
            String query = "SELECT a FROM Titulares a WHERE a.documento = :nroDoc AND a.tipoDocumento = :tipDoc AND a.estado <> :estado";
            if (!Strings.isNullOrEmpty(request.getTitular().getNombreCompleto())) {
                query = "SELECT a FROM Titulares a WHERE a.documento = :nroDoc AND a.tipoDocumento = :tipDoc " +
                        "AND a.nombreCompleto like :nomCom AND a.estado <> :estado ";
                Query q = entityManager.createQuery(query);
                q.setParameter("nroDoc", request.getTitular().getDocumento());
                q.setParameter("tipDoc", String.valueOf(request.getTitular().getTipoDocumento()));
                q.setParameter("nomCom", String.valueOf(request.getTitular().getNombreCompleto()));
                q.setParameter("estado", String.valueOf(TipoEstadoTitular.E));
                return q.getResultList();
            }
            Query q = entityManager.createQuery(query);
            q.setParameter("nroDoc", request.getTitular().getDocumento());
            q.setParameter("tipDoc", String.valueOf(request.getTitular().getTipoDocumento()));
            q.setParameter("estado", String.valueOf(TipoEstadoTitular.E));
            return q.getResultList();
        });
    }

    public Titulares guardar(Titulares titulares) {
        if (titulares.getId() != null && titulares.getId() > 0L) {
            return merge(titulares);
        }
        return persist(titulares);
    }

    public Titulares cabiarEstado(Titulares titulares) {
        Titulares modificar = find(titulares.getId());
        modificar.setEstado(titulares.getEstado());
        modificar.setIdCabecera(titulares.getIdCabecera());
        return merge(modificar);
    }

    @Override
    protected Path<Long> countPath(Root<Titulares> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<Titulares> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.<String>get("estado"));
        paths.add(p.<String>get("documento"));
        paths.add(p.<String>get("nombreCompleto"));
        return paths;
    }
}
