package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import com.bisa.bus.servicios.asfi.infocred.api.AlertaResumenService;
import com.bisa.bus.servicios.asfi.infocred.entities.AlertaResumen;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class AlertaResumenDao extends DaoImpl<AlertaResumen, Long> implements Serializable, AlertaResumenService {

    protected AlertaResumenDao(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }
}
