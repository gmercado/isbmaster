package com.bisa.bus.servicios.asfi.infocred.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.asfi.infocred.dao.AlertaCabeceraDao;
import com.bisa.bus.servicios.asfi.infocred.entities.AlertaCabecera;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.LinkedList;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@AuthorizeInstantiation({RolesBisa.INFOCRED_ADMIN})
@Menu(value = "Monitoreo archivos", subMenu = SubMenu.INFOCRED)
public class ArchivosCargaMonitoreo extends Listado<AlertaCabecera, Long> {

    @Override
    protected ListadoPanel<AlertaCabecera, Long> newListadoPanel(String id) {

        return new ListadoPanel<AlertaCabecera, Long>(id) {
            @Override
            protected List<IColumn<AlertaCabecera, String>> newColumns(ListadoDataProvider<AlertaCabecera, Long> dataProvider) {
                List<IColumn<AlertaCabecera, String>> columns = new LinkedList<>();
                columns.add(new PropertyColumn<>(Model.of("Id"), "id", "id"));
                columns.add(new PropertyColumn<>(Model.of("Titulo"), "titulo", "titulo"));
                columns.add(new PropertyColumn<>(Model.of("Nombre Archivo"), "nombreArchivo", "nombreArchivo"));
                columns.add(new PropertyColumn<>(Model.of("Periodo"), "cargoPeriodoProceso", "cargoPeriodoProceso"));
                columns.add(new PropertyColumn<>(Model.of("Solicitante"), "usuarioSolicitante", "usuarioSolicitante"));
                columns.add(new PropertyColumn<>(Model.of("Fecha Proceso"), "fechaProceso", "fechaProceso"));
                columns.add(new PropertyColumn<>(Model.of("Nro Registros"), "nroRegistros", "nroRegistros"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<AlertaCabecera, Long>> getProviderClazz() {
                return AlertaCabeceraDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.BUSQUEDA;
            }

            @Override
            protected FiltroPanel<AlertaCabecera> newFiltroPanel(String id, IModel<AlertaCabecera> model1, IModel<AlertaCabecera> model2) {
                return new ListadoCargaMonitoreo(id, model1, model2);
            }

            @Override
            protected AlertaCabecera newObject1() {
                return new AlertaCabecera();
            }

            @Override
            protected AlertaCabecera newObject2() {
                return new AlertaCabecera();
            }
        };
    }
}
