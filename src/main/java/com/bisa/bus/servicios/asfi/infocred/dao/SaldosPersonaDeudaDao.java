package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.asfi.infocred.api.SaldosPersonaDeudaService;
import com.bisa.bus.servicios.asfi.infocred.entities.SaldosPersonaDeuda;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.io.Serializable;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class SaldosPersonaDeudaDao extends DaoImpl<SaldosPersonaDeuda, Long> implements Serializable, SaldosPersonaDeudaService {

    private static final Logger log = LoggerFactory.getLogger(SaldosPersonaDeudaDao.class);
    @Inject
    protected SaldosPersonaDeudaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    public String buscarGestionMayor(Long id) {
        return doWithTransaction((entityManager, t) -> {
            String query = "SELECT MAX(a.id.gestion) FROM SaldosPersonaDeuda a WHERE a.id.id = :id";
            Query q = entityManager.createQuery(query);
            q.setParameter("id", id);
            return (String) q.getSingleResult();
        });
    }
}
