package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import com.bisa.bus.servicios.asfi.infocred.api.DomicilioService;
import com.bisa.bus.servicios.asfi.infocred.entities.Domicilio;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 * Created by gmercado on 18/11/2016.
 */
public class DomicilioDao extends DaoImpl<Domicilio, Long> implements Serializable, DomicilioService {
    protected DomicilioDao(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }
}
