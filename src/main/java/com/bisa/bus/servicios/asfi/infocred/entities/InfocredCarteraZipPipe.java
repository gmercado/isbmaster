package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.database.model.EntityBase;
import bus.env.api.Variables;
import bus.plumbing.utils.FormatosUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Gary Mercado
 * Yrag knup
 */
@Entity
@Table(name = "IFP042")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I42FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I42USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I42FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I42USRMOD", columnDefinition = "CHAR(100)"))
})
public class InfocredCarteraZipPipe extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I42ID", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "I42CABECER")
    private Long idCabecera;

    @Column(name = "I42NOMCOM")
    private String nombreCompleto;

    @Column(name = "I42DOCUMEN")
    private String documento;

    @Column(name = "I42NRODOC")
    private String numeroDocumento;

    @Column(name = "I42EXTEN")
    private String extension;

    @Column(name = "I42FECNAC")
    private Date fechaNacimiento;

    @Column(name = "I42SCORE")
    private String score;

    @Column(name = "I42DIRECC")
    private String direccion;

    @Column(name = "I42LOCAL")
    private String localidad;

    @Column(name = "I42CANTON")
    private String canton;

    @Column(name = "I42SECCION")
    private String seccion;

    @Column(name = "I42PROVINC")
    private String provincia;

    @Column(name = "I42DEPTO")
    private String departamento;

    @Column(name = "I42FECDEC")
    @Temporal(TemporalType.DATE)
    private Date fechaDeclaracion;

    @Column(name = "I42ENTIDAD")
    private String entidadSBEF;

    @Column(name = "I42SIGLA")
    private String siglaSBEF;

    @Column(name = "I42CREDITO")
    private String creditoSBEF;

    @Column(name = "I42TIPOBLI")
    private String tipoObligadoSBEF;

    @Column(name = "I42VIGENTE", scale = 15, precision = 2)
    private BigDecimal vigente;

    @Column(name = "I42VENCIDO", scale = 15, precision = 2)
    private BigDecimal vencido;

    @Column(name = "I42EJECU", scale = 15, precision = 2)
    private BigDecimal ejecucion;

    @Column(name = "I42CONTIN", scale = 15, precision = 2)
    private BigDecimal contingente;

    @Column(name = "I42CASTIG", scale = 15, precision = 2)
    private BigDecimal castigado;

    @Column(name = "I42OTRCRED")
    private String otrosCreditos;

    @Column(name = "I42EJESBEF")
    private String ejecucionSBEF;

    @Column(name = "I42ENTFR")
    private String entidadFr;

    @Column(name = "I42SIGLAFR")
    private String siglaFr;

    @Column(name = "I42TIPOBFR")
    private String tipoObligadoFr;

    @Column(name = "I42FRVIG", scale = 15, precision = 2)
    private BigDecimal frVigente;

    @Column(name = "I42FRVENC", scale = 15, precision = 2)
    private BigDecimal frVencido;

    @Column(name = "I42FREJEC", scale = 15, precision = 2)
    private BigDecimal frEjecucion;

    @Column(name = "I42FRCAST", scale = 15, precision = 2)
    private BigDecimal frCastigado;

    @Column(name = "I42FRCALI")
    private String frCalificacion;

    @Column(name = "I42HISTORI")
    private String historico;

    @Column(name = "I42FECHA")
    private Date fecha;

    @Column(name = "I42SBEFCAL")
    private String sbefCalificacion;

    public InfocredCarteraZipPipe() {
    }

    public InfocredCarteraZipPipe(Long idCabecera, String[] columna) {
        this.idCabecera = idCabecera;
        this.nombreCompleto = columna[0];
        this.documento = columna[1];
        this.numeroDocumento = columna[2];
        this.extension = columna[3];
        this.fechaNacimiento = FormatosUtils.deDDMMYYYYaFecha(columna[4]);
        this.score = columna[5];
        this.direccion = columna[6];
        this.localidad = columna[7];
        this.canton = columna[8];
        this.seccion = columna[9];
        this.provincia = columna[10];
        this.departamento = columna[11];
        this.fechaDeclaracion = FormatosUtils.deDDMMYYYYaFecha(columna[12]);
        this.entidadSBEF = columna[13];
        this.siglaSBEF = columna[14];
        this.creditoSBEF = columna[15];
        this.tipoObligadoSBEF = columna[16];
        this.vigente = FormatosUtils.stringToBigDecimal(columna[17]);
        this.vencido = FormatosUtils.stringToBigDecimal(columna[18]);
        this.ejecucion = FormatosUtils.stringToBigDecimal(columna[19]);
        this.contingente = FormatosUtils.stringToBigDecimal(columna[20]);
        this.castigado = FormatosUtils.stringToBigDecimal(columna[21]);
        this.otrosCreditos = columna[22];
        this.ejecucionSBEF = columna[23];
        this.entidadFr = columna[24];
        this.siglaFr = columna[25];
        this.tipoObligadoFr = columna[26];
        this.frVigente = FormatosUtils.stringToBigDecimal(columna[27]);
        this.frVencido = FormatosUtils.stringToBigDecimal(columna[28]);
        this.frEjecucion = FormatosUtils.stringToBigDecimal(columna[29]);
        this.frCastigado = FormatosUtils.stringToBigDecimal(columna[30]);
        this.frCalificacion = columna[31];
        this.historico = columna[32];
        this.fecha = FormatosUtils.deDDMMYYYYaFecha(columna[33]);
        this.sbefCalificacion = columna[34];
        super.usuarioCreador = Variables.INFOCRED_USUARIO_ALTA_JOBS;
        super.fechaCreacion = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCabecera() {
        return idCabecera;
    }

    public void setIdCabecera(Long idCabecera) {
        this.idCabecera = idCabecera;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Date getFechaDeclaracion() {
        return fechaDeclaracion;
    }

    public void setFechaDeclaracion(Date fechaDeclaracion) {
        this.fechaDeclaracion = fechaDeclaracion;
    }

    public String getEntidadSBEF() {
        return entidadSBEF;
    }

    public void setEntidadSBEF(String entidadSBEF) {
        this.entidadSBEF = entidadSBEF;
    }

    public String getSiglaSBEF() {
        return siglaSBEF;
    }

    public void setSiglaSBEF(String siglaSBEF) {
        this.siglaSBEF = siglaSBEF;
    }

    public String getCreditoSBEF() {
        return creditoSBEF;
    }

    public void setCreditoSBEF(String creditoSBEF) {
        this.creditoSBEF = creditoSBEF;
    }

    public String getTipoObligadoSBEF() {
        return tipoObligadoSBEF;
    }

    public void setTipoObligadoSBEF(String tipoObligadoSBEF) {
        this.tipoObligadoSBEF = tipoObligadoSBEF;
    }

    public BigDecimal getVigente() {
        return vigente;
    }

    public void setVigente(BigDecimal vigente) {
        this.vigente = vigente;
    }

    public BigDecimal getVencido() {
        return vencido;
    }

    public void setVencido(BigDecimal vencido) {
        this.vencido = vencido;
    }

    public BigDecimal getEjecucion() {
        return ejecucion;
    }

    public void setEjecucion(BigDecimal ejecucion) {
        this.ejecucion = ejecucion;
    }

    public BigDecimal getContingente() {
        return contingente;
    }

    public void setContingente(BigDecimal contingente) {
        this.contingente = contingente;
    }

    public BigDecimal getCastigado() {
        return castigado;
    }

    public void setCastigado(BigDecimal castigado) {
        this.castigado = castigado;
    }

    public String getOtrosCreditos() {
        return otrosCreditos;
    }

    public void setOtrosCreditos(String otrosCreditos) {
        this.otrosCreditos = otrosCreditos;
    }

    public String getEjecucionSBEF() {
        return ejecucionSBEF;
    }

    public void setEjecucionSBEF(String ejecucionSBEF) {
        this.ejecucionSBEF = ejecucionSBEF;
    }

    public String getEntidadFr() {
        return entidadFr;
    }

    public void setEntidadFr(String entidadFr) {
        this.entidadFr = entidadFr;
    }

    public String getSiglaFr() {
        return siglaFr;
    }

    public void setSiglaFr(String siglaFr) {
        this.siglaFr = siglaFr;
    }

    public String getTipoObligadoFr() {
        return tipoObligadoFr;
    }

    public void setTipoObligadoFr(String tipoObligadoFr) {
        this.tipoObligadoFr = tipoObligadoFr;
    }

    public BigDecimal getFrVigente() {
        return frVigente;
    }

    public void setFrVigente(BigDecimal frVigente) {
        this.frVigente = frVigente;
    }

    public BigDecimal getFrVencido() {
        return frVencido;
    }

    public void setFrVencido(BigDecimal frVencido) {
        this.frVencido = frVencido;
    }

    public BigDecimal getFrEjecucion() {
        return frEjecucion;
    }

    public void setFrEjecucion(BigDecimal frEjecucion) {
        this.frEjecucion = frEjecucion;
    }

    public BigDecimal getFrCastigado() {
        return frCastigado;
    }

    public void setFrCastigado(BigDecimal frCastigado) {
        this.frCastigado = frCastigado;
    }

    public String getFrCalificacion() {
        return frCalificacion;
    }

    public void setFrCalificacion(String frCalificacion) {
        this.frCalificacion = frCalificacion;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getSbefCalificacion() {
        return sbefCalificacion;
    }

    public void setSbefCalificacion(String sbefCalificacion) {
        this.sbefCalificacion = sbefCalificacion;
    }

    @Override
    public String toString() {
        return "InfocredCarteraZipPipe{" +
                "id=" + id +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                ", documento='" + documento + '\'' +
                ", numeroDocumento='" + numeroDocumento + '\'' +
                ", extension='" + extension + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", score='" + score + '\'' +
                ", direccion='" + direccion + '\'' +
                ", localidad='" + localidad + '\'' +
                ", canton='" + canton + '\'' +
                ", seccion='" + seccion + '\'' +
                ", provincia='" + provincia + '\'' +
                ", departamento='" + departamento + '\'' +
                ", fechaDeclaracion=" + fechaDeclaracion +
                ", entidadSBEF='" + entidadSBEF + '\'' +
                ", siglaSBEF='" + siglaSBEF + '\'' +
                ", creditoSBEF='" + creditoSBEF + '\'' +
                ", tipoObligadoSBEF='" + tipoObligadoSBEF + '\'' +
                ", vigente=" + vigente +
                ", vencido=" + vencido +
                ", ejecucion=" + ejecucion +
                ", contingente=" + contingente +
                ", castigado=" + castigado +
                ", otrosCreditos='" + otrosCreditos + '\'' +
                ", ejecucionSBEF='" + ejecucionSBEF + '\'' +
                ", entidadFr='" + entidadFr + '\'' +
                ", siglaFr='" + siglaFr + '\'' +
                ", tipoObligadoFr='" + tipoObligadoFr + '\'' +
                ", frVigente=" + frVigente +
                ", frVencido=" + frVencido +
                ", frEjecucion=" + frEjecucion +
                ", frCastigado=" + frCastigado +
                ", frCalificacion='" + frCalificacion + '\'' +
                ", historico='" + historico + '\'' +
                ", fecha=" + fecha +
                ", sbefCalificacion='" + sbefCalificacion + '\'' +
                '}';
    }
}
