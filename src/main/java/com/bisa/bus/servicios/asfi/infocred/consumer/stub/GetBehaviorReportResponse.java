
package com.bisa.bus.servicios.asfi.infocred.consumer.stub;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetBehaviorReportResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getBehaviorReportResult"
})
@XmlRootElement(name = "GetBehaviorReportResponse")
public class GetBehaviorReportResponse {

    @XmlElement(name = "GetBehaviorReportResult")
    protected String getBehaviorReportResult;

    /**
     * Obtiene el valor de la propiedad getBehaviorReportResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetBehaviorReportResult() {
        return getBehaviorReportResult;
    }

    /**
     * Define el valor de la propiedad getBehaviorReportResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetBehaviorReportResult(String value) {
        this.getBehaviorReportResult = value;
    }

}
