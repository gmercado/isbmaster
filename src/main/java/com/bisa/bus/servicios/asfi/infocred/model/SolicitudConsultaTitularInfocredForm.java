package com.bisa.bus.servicios.asfi.infocred.model;

import com.bisa.bus.servicios.asfi.infocred.consumer.stub.Titular;
import com.bisa.bus.servicios.asfi.infocred.consumer.stub.Usuario;

import java.io.Serializable;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class SolicitudConsultaTitularInfocredForm implements Serializable {
    private static final long serialVersionUID = 1L;

    private Usuario usuario;

    private Titular titular;

    private String codigoAgencia;

    private String canal;

    private boolean forzarConsulta;

    public SolicitudConsultaTitularInfocredForm() {
    }

    public SolicitudConsultaTitularInfocredForm(Usuario usuario, Titular titular, String codigoAgencia, String canal, boolean forzarConsulta) {
        this.usuario = usuario;
        this.titular = titular;
        this.canal = canal;
        this.codigoAgencia = codigoAgencia;
        this.forzarConsulta = forzarConsulta;
    }

    public boolean isForzarConsulta() {
        return forzarConsulta;
    }

    public void setForzarConsulta(boolean forzarConsulta) {
        this.forzarConsulta = forzarConsulta;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Titular getTitular() {
        return titular;
    }

    public void setTitular(Titular titular) {
        this.titular = titular;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getCodigoAgencia() {
        return codigoAgencia;
    }

    public void setCodigoAgencia(String codigoAgencia) {
        this.codigoAgencia = codigoAgencia;
    }

    @Override
    public String toString() {
        return "SolicitudConsultaTitularInfocredForm{" +
                "usuario=" + usuario +
                ", titular=" + titular +
                ", codigoAgencia='" + codigoAgencia + '\'' +
                ", forzarConsulta=" + forzarConsulta +
                '}';
    }
}
