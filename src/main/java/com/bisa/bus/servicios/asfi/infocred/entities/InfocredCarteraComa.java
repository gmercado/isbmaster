package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.database.model.EntityBase;
import bus.env.api.Variables;
import bus.plumbing.utils.FormatosUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Gary Mercado
 * Yrag knup
 */
@Entity
@Table(name = "IFP041")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I41FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I41USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I41FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I41USRMOD", columnDefinition = "CHAR(100)"))
})
public class InfocredCarteraComa extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I41ID", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "I41CABECER")
    private Long idCabecera;

    @Column(name = "I41TXTBOX")
    private String textbox6;

    @Column(name = "I41ENTIDAD")
    private String entidad;

    @Column(name = "I41NOMCOM")
    private String nombreCompleto;

    @Column(name = "I41FECNAC")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;

    @Column(name = "I41TIPDOC")
    private String tipoDocumento;

    @Column(name = "I41NRODOC")
    private String numeroDocumento;

    @Column(name = "I41EXTEN")
    private String extension;

    @Column(name = "I41TIPOBL")
    private String tipoObligado;

    @Column(name = "I41VIGCARD")
    private BigDecimal vigenteCarteraDirecta;

    @Column(name = "I41VENCARD")
    private BigDecimal vencidoCarteraDirecta;

    @Column(name = "I41EJECARD")
    private BigDecimal ejecucionCarteraDirecta;

    @Column(name = "I41CASCARD")
    private BigDecimal castigadoCarteraDirecta;

    @Column(name = "I41CONCARD")
    private BigDecimal contingenteCarteraDirecta;

    @Column(name = "I41VIGCARI")
    private BigDecimal vigenteCarteraIndirecta;

    @Column(name = "I41VENCARI")
    private BigDecimal vencidoCarteraIndirecta;

    @Column(name = "I41EJECARI")
    private BigDecimal ejecucionCarteraIndirecta;

    @Column(name = "I41CASCARI")
    private BigDecimal castigadoCarteraIndirecta;

    @Column(name = "I41CONCARI")
    private BigDecimal contingenteCarteraIndirecta;

    public InfocredCarteraComa() {
    }

    public InfocredCarteraComa(Long idCabecera, List<String> columna) {
        this.idCabecera = idCabecera;
        this.textbox6 = columna.get(0);
        this.entidad = columna.get(1);
        this.nombreCompleto = columna.get(2);
        this.fechaNacimiento = FormatosUtils.deDDMMYYYYaFecha(columna.get(3));
        this.tipoDocumento = columna.get(4);
        this.numeroDocumento = columna.get(5);
        this.extension = columna.get(6);
        this.tipoObligado = columna.get(7);
        this.vigenteCarteraDirecta = FormatosUtils.stringToBigDecimal(columna.get(8));
        this.vencidoCarteraDirecta = FormatosUtils.stringToBigDecimal(columna.get(9));
        this.ejecucionCarteraDirecta = FormatosUtils.stringToBigDecimal(columna.get(10));
        this.castigadoCarteraDirecta = FormatosUtils.stringToBigDecimal(columna.get(11));
        this.contingenteCarteraDirecta = FormatosUtils.stringToBigDecimal(columna.get(12));
        this.vigenteCarteraIndirecta = FormatosUtils.stringToBigDecimal(columna.get(13));
        this.vencidoCarteraIndirecta = FormatosUtils.stringToBigDecimal(columna.get(14));
        this.ejecucionCarteraIndirecta = FormatosUtils.stringToBigDecimal(columna.get(15));
        this.castigadoCarteraIndirecta = FormatosUtils.stringToBigDecimal(columna.get(16));
        this.contingenteCarteraIndirecta = FormatosUtils.stringToBigDecimal(columna.get(17));
        super.usuarioCreador = Variables.INFOCRED_USUARIO_ALTA_JOBS;
        super.fechaCreacion = new Date();
    }

    public Long getIdCabecera() {
        return idCabecera;
    }

    public void setIdCabecera(Long idCabecera) {
        this.idCabecera = idCabecera;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTextbox6() {
        return textbox6;
    }

    public void setTextbox6(String textbox6) {
        this.textbox6 = textbox6;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getTipoObligado() {
        return tipoObligado;
    }

    public void setTipoObligado(String tipoObligado) {
        this.tipoObligado = tipoObligado;
    }

    public BigDecimal getVigenteCarteraDirecta() {
        return vigenteCarteraDirecta;
    }

    public void setVigenteCarteraDirecta(BigDecimal vigenteCarteraDirecta) {
        this.vigenteCarteraDirecta = vigenteCarteraDirecta;
    }

    public BigDecimal getVencidoCarteraDirecta() {
        return vencidoCarteraDirecta;
    }

    public void setVencidoCarteraDirecta(BigDecimal vencidoCarteraDirecta) {
        this.vencidoCarteraDirecta = vencidoCarteraDirecta;
    }

    public BigDecimal getEjecucionCarteraDirecta() {
        return ejecucionCarteraDirecta;
    }

    public void setEjecucionCarteraDirecta(BigDecimal ejecucionCarteraDirecta) {
        this.ejecucionCarteraDirecta = ejecucionCarteraDirecta;
    }

    public BigDecimal getCastigadoCarteraDirecta() {
        return castigadoCarteraDirecta;
    }

    public void setCastigadoCarteraDirecta(BigDecimal castigadoCarteraDirecta) {
        this.castigadoCarteraDirecta = castigadoCarteraDirecta;
    }

    public BigDecimal getContingenteCarteraDirecta() {
        return contingenteCarteraDirecta;
    }

    public void setContingenteCarteraDirecta(BigDecimal contingenteCarteraDirecta) {
        this.contingenteCarteraDirecta = contingenteCarteraDirecta;
    }

    public BigDecimal getVigenteCarteraIndirecta() {
        return vigenteCarteraIndirecta;
    }

    public void setVigenteCarteraIndirecta(BigDecimal vigenteCarteraIndirecta) {
        this.vigenteCarteraIndirecta = vigenteCarteraIndirecta;
    }

    public BigDecimal getVencidoCarteraIndirecta() {
        return vencidoCarteraIndirecta;
    }

    public void setVencidoCarteraIndirecta(BigDecimal vencidoCarteraIndirecta) {
        this.vencidoCarteraIndirecta = vencidoCarteraIndirecta;
    }

    public BigDecimal getEjecucionCarteraIndirecta() {
        return ejecucionCarteraIndirecta;
    }

    public void setEjecucionCarteraIndirecta(BigDecimal ejecucionCarteraIndirecta) {
        this.ejecucionCarteraIndirecta = ejecucionCarteraIndirecta;
    }

    public BigDecimal getCastigadoCarteraIndirecta() {
        return castigadoCarteraIndirecta;
    }

    public void setCastigadoCarteraIndirecta(BigDecimal castigadoCarteraIndirecta) {
        this.castigadoCarteraIndirecta = castigadoCarteraIndirecta;
    }

    public BigDecimal getContingenteCarteraIndirecta() {
        return contingenteCarteraIndirecta;
    }

    public void setContingenteCarteraIndirecta(BigDecimal contingenteCarteraIndirecta) {
        this.contingenteCarteraIndirecta = contingenteCarteraIndirecta;
    }

    @Override
    public String toString() {
        return "InfocredCarteraComa{" +
                "id=" + id +
                ", idCabecera=" + idCabecera +
                ", textbox6='" + textbox6 + '\'' +
                ", entidad='" + entidad + '\'' +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", tipoDocumento='" + tipoDocumento + '\'' +
                ", numeroDocumento='" + numeroDocumento + '\'' +
                ", extension='" + extension + '\'' +
                ", tipoObligado='" + tipoObligado + '\'' +
                ", vigenteCarteraDirecta=" + vigenteCarteraDirecta +
                ", vencidoCarteraDirecta=" + vencidoCarteraDirecta +
                ", ejecucionCarteraDirecta=" + ejecucionCarteraDirecta +
                ", castigadoCarteraDirecta=" + castigadoCarteraDirecta +
                ", contingenteCarteraDirecta=" + contingenteCarteraDirecta +
                ", vigenteCarteraIndirecta=" + vigenteCarteraIndirecta +
                ", vencidoCarteraIndirecta=" + vencidoCarteraIndirecta +
                ", ejecucionCarteraIndirecta=" + ejecucionCarteraIndirecta +
                ", castigadoCarteraIndirecta=" + castigadoCarteraIndirecta +
                ", contingenteCarteraIndirecta=" + contingenteCarteraIndirecta +
                '}';
    }
}
