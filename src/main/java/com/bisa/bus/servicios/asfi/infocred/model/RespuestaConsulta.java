package com.bisa.bus.servicios.asfi.infocred.model;

import bus.consumoweb.enums.CodigoRespuesta;
import bus.consumoweb.enums.TipoConsumoRespuesta;
import bus.consumoweb.enums.TipoServicio;

import java.io.Serializable;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class RespuestaConsulta implements Serializable {

    private TipoServicio tipoServicio;
    private CodigoRespuesta codError;
    private String descError;
    private String response;
    private TipoConsumoRespuesta tipoRespuesta;
    private byte[] documentoPDF;
    private Long idConsulta;

    public RespuestaConsulta() {
    }

    public RespuestaConsulta(TipoServicio tipoServicio, TipoConsumoRespuesta tipoRespuesta,
                             CodigoRespuesta codError, String descError, byte[] documentoPDF) {
        this.tipoServicio = tipoServicio;
        this.codError = codError;
        this.descError = descError;
        this.tipoRespuesta = tipoRespuesta;
        this.documentoPDF = documentoPDF;
    }

    public RespuestaConsulta(TipoServicio tipoServicio, TipoConsumoRespuesta tipoRespuesta,
                             CodigoRespuesta codError, String descError, byte[] documentoPDF, Long idConsulta) {
        this.tipoServicio = tipoServicio;
        this.codError = codError;
        this.descError = descError;
        this.tipoRespuesta = tipoRespuesta;
        this.documentoPDF = documentoPDF;
        this.idConsulta = idConsulta;
    }

    public RespuestaConsulta(TipoServicio tipoServicio, TipoConsumoRespuesta tipoRespuesta,
                             CodigoRespuesta codError, String response) {
        this.tipoServicio = tipoServicio;
        this.codError = codError;
        this.tipoRespuesta = tipoRespuesta;
        this.response = response;
    }

    public TipoServicio getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(TipoServicio tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public CodigoRespuesta getCodError() {
        return codError;
    }

    public void setCodError(CodigoRespuesta codError) {
        this.codError = codError;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public TipoConsumoRespuesta getTipoRespuesta() {
        return tipoRespuesta;
    }

    public void setTipoRespuesta(TipoConsumoRespuesta tipoRespuesta) {
        this.tipoRespuesta = tipoRespuesta;
    }

    public byte[] getDocumentoPDF() {
        return documentoPDF;
    }

    public void setDocumentoPDF(byte[] documentoPDF) {
        this.documentoPDF = documentoPDF;
    }

    public Long getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(Long idConsulta) {
        this.idConsulta = idConsulta;
    }

    @Override
    public String toString() {
        return "RespuestaConsulta{" +
                "tipoServicio=" + tipoServicio +
                ", codError=" + codError +
                ", descError='" + descError + '\'' +
                ", tipoRespuesta=" + tipoRespuesta +
                ", idConsulta=" + idConsulta +
                ", documentoPDF='" + documentoPDF != null ? "VALIDO" : "NULL" + '\'' +
                '}';
    }
}
