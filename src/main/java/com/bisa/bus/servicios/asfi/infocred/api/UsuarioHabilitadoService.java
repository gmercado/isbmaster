package com.bisa.bus.servicios.asfi.infocred.api;

import com.bisa.bus.servicios.asfi.infocred.entities.UsuarioHabilitado;

import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public interface UsuarioHabilitadoService {
    List<UsuarioHabilitado> buscarPorLogin(String login);
}
