package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.asfi.infocred.api.CasasComercialesService;
import com.bisa.bus.servicios.asfi.infocred.entities.CasasComerciales;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 *
 * @author Gary Mercado
 * Yrag Knup
 */
public class CasasComercialesDao extends DaoImpl<CasasComerciales, Long> implements Serializable, CasasComercialesService {

    protected CasasComercialesDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }
}
