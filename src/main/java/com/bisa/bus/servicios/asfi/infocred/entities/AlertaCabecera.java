package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.database.model.EntityBase;
import bus.env.api.Variables;
import com.google.common.base.Splitter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Gary Mercado
 * Yrag knup
 */
@Entity
@Table(name = "IFP030")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I30FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I30USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I30FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I30USRMOD", columnDefinition = "CHAR(100)"))
})
public class AlertaCabecera extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I30ID", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "I30TITULO")
    private String titulo;

    @Column(name = "I30FUENTE")
    private String fuente;

    @Column(name = "I30ENTCLI")
    private String entidadCliente;

    @Column(name = "I30USUSOL")
    private String usuarioSolicitante;

    @Column(name = "I30CARSOL")
    private String cargoSolicitante;

    @Column(name = "I30PERPROC")
    private String cargoPeriodoProceso;

    @Column(name = "I30FECPROC")
    private String fechaProceso;

    @Column(name = "I30NOMARCH")
    private String nombreArchivo;

    @Column(name = "I30NROREG")
    private Integer nroRegistros;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "CUERPO", nullable = false, insertable = false, updatable = false)
    private List<AlertaCuerpo> alertaCuerpos = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "RESUMEN", nullable = false, insertable = false, updatable = false)
    private List<AlertaResumen> alertaResumenes = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "CARTERACOMA", nullable = false, insertable = false, updatable = false)
    private List<InfocredCarteraComa> carteraComas = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "CARTERAPIPE", nullable = false, insertable = false, updatable = false)
    private List<InfocredCarteraPipe> carteraPipes = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "CARTERATXTPIPE", nullable = false, insertable = false, updatable = false)
    private List<InfocredCarteraZipPipe> carteraTxtPipes = new ArrayList<>();

    public AlertaCabecera() {
    }

    public AlertaCabecera(String titulo, String fechaProceso, String nombreArchivo) {
        this.titulo = titulo;
        this.fechaProceso = fechaProceso;
        this.nombreArchivo = nombreArchivo;
        super.fechaCreacion = new Date();
        super.usuarioModificador = Variables.INFOCRED_USUARIO_ALTA_JOBS;
    }

    public AlertaCabecera(String cabecera, String nombreArchivo, String usuarioAlta, Date fechaAlta) {
        List<String> res = Splitter.on('|').omitEmptyStrings().splitToList(cabecera);
        int count = 0;

        if (res.size() > 10) {
            for (String str : res) {
                count++;
                switch (count) {
                    case 1:
                        this.titulo = str;
                        break;
                    case 3:
                        this.fuente = str;
                        break;
                    case 5:
                        this.entidadCliente = str;
                        break;
                    case 7:
                        this.usuarioSolicitante = str;
                        break;
                    case 10:
                        this.cargoPeriodoProceso = str;
                        break;
                    default:
                        this.fechaProceso = str;
                        break;
                }
            }
        } else {
            for (String str : res) {
                count++;
                switch (count) {
                    case 1:
                        this.titulo = str;
                        break;
                    case 3:
                        this.fuente = str;
                        break;
                    case 5:
                        this.entidadCliente = str;
                        break;
                    case 7:
                        this.usuarioSolicitante = str;
                        break;
                    default:
                        this.fechaProceso = str;
                        break;
                }
            }
        }
        this.nombreArchivo = nombreArchivo;
        super.usuarioCreador = usuarioAlta;
        super.fechaCreacion = fechaAlta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    public String getEntidadCliente() {
        return entidadCliente;
    }

    public void setEntidadCliente(String entidadCliente) {
        this.entidadCliente = entidadCliente;
    }

    public String getUsuarioSolicitante() {
        return usuarioSolicitante;
    }

    public void setUsuarioSolicitante(String usuarioSolicitante) {
        this.usuarioSolicitante = usuarioSolicitante;
    }

    public String getCargoSolicitante() {
        return cargoSolicitante;
    }

    public void setCargoSolicitante(String cargoSolicitante) {
        this.cargoSolicitante = cargoSolicitante;
    }

    public String getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(String fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getCargoPeriodoProceso() {
        return cargoPeriodoProceso;
    }

    public void setCargoPeriodoProceso(String cargoPeriodoProceso) {
        this.cargoPeriodoProceso = cargoPeriodoProceso;
    }

    public List<AlertaCuerpo> getAlertaCuerpos() {
        return alertaCuerpos;
    }

    public void setAlertaCuerpos(List<AlertaCuerpo> alertaCuerpos) {
        this.alertaCuerpos = alertaCuerpos;
    }

    public List<AlertaResumen> getAlertaResumenes() {
        return alertaResumenes;
    }

    public void setAlertaResumenes(List<AlertaResumen> alertaResumenes) {
        this.alertaResumenes = alertaResumenes;
    }

    public List<InfocredCarteraComa> getCarteraComas() {
        return carteraComas;
    }

    public void setCarteraComas(List<InfocredCarteraComa> carteraComas) {
        this.carteraComas = carteraComas;
    }

    public List<InfocredCarteraPipe> getCarteraPipes() {
        return carteraPipes;
    }

    public void setCarteraPipes(List<InfocredCarteraPipe> carteraPipes) {
        this.carteraPipes = carteraPipes;
    }

    public List<InfocredCarteraZipPipe> getCarteraTxtPipes() {
        return carteraTxtPipes;
    }

    public void setCarteraTxtPipes(List<InfocredCarteraZipPipe> carteraTxtPipes) {
        this.carteraTxtPipes = carteraTxtPipes;
    }

    public Integer getNroRegistros() {
        return nroRegistros;
    }

    public void setNroRegistros(Integer nroRegistros) {
        this.nroRegistros = nroRegistros;
    }

    @Override
    public String toString() {
        return "AlertaCabecera{" +
                "id=" + id +
                ", titulo='" + titulo + '\'' +
                ", fuente='" + fuente + '\'' +
                ", entidadCliente='" + entidadCliente + '\'' +
                ", usuarioSolicitante='" + usuarioSolicitante + '\'' +
                ", cargoSolicitante='" + cargoSolicitante + '\'' +
                ", cargoPeriodoProceso='" + cargoPeriodoProceso + '\'' +
                ", fechaProceso='" + fechaProceso + '\'' +
                ", nombreArchivo='" + nombreArchivo + '\'' +
                ", nroRegistros=" + nroRegistros +
                ", alertaCuerpos=" + alertaCuerpos +
                ", alertaResumenes=" + alertaResumenes +
                ", carteraComas=" + carteraComas +
                ", carteraPipes=" + carteraPipes +
                '}';
    }
}
