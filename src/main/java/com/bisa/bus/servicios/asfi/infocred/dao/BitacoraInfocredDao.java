package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.consumoweb.enums.TipoConsumoRespuesta;
import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.asfi.infocred.api.BitacoraInfocredService;
import com.bisa.bus.servicios.asfi.infocred.entities.BitacoraInfocred;
import com.bisa.bus.servicios.asfi.infocred.model.RespuestaConsulta;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;
import com.bisa.bus.servicios.segip.entities.Oficina;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.*;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class BitacoraInfocredDao extends DaoImpl<BitacoraInfocred, Long> implements Serializable, BitacoraInfocredService {

    private static final Logger log = LoggerFactory.getLogger(BitacoraInfocredDao.class);

    @Inject
    protected BitacoraInfocredDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    public void registrar(SolicitudConsultaTitularInfocredForm request, RespuestaConsulta response) {
        BitacoraInfocred bitacoraSegip = new BitacoraInfocred();
        bitacoraSegip.setFechaHoraConsulta(new Date());
        bitacoraSegip.setCodigoUsuario(request.getUsuario().getNombreCompleto());
        bitacoraSegip.setOficinaUsuario(request.getCodigoAgencia());
        bitacoraSegip.setCanalUsuario(request.getCanal());
        //bitacoraSegip.setCriterioBusqueda(request.toString());
        //bitacoraSegip.setTipoBuro(request.getTipoBuro().getDescripcion());

        if (response != null) {
            bitacoraSegip.setTipoRespuesta(response.getTipoRespuesta().getDescripcion());
            bitacoraSegip.setCodError(response.getCodError().getCodigo());
        }
        persist(bitacoraSegip);
    }

    @Override
    public List<BitacoraInfocred> getConsumos(BitacoraInfocred model1, BitacoraInfocred model2) {
        log.debug("Obteniendo consumos Segip:{},{}", model1, model2);
        try {
            String codigoUsuario = model1.getCodigoUsuario();
            //String tipoRespuesta = (model1.getTipoRespuestaEnum() != null) ? model1.getTipoRespuestaEnum().getDescripcion() : null;
            Date desde = model1.getFechaHoraConsulta();
            Date hasta = model2.getFechaHoraConsulta();
            //String tipoCanal = (model1.getTipoCanal() != null) ? model1.getTipoCanal().getNombreParte() : null;
            String tipoOficina = (model1.getOficina() != null) ? model1.getOficina().getCodigo() : null;
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<BitacoraInfocred> q = cb.createQuery(BitacoraInfocred.class);
                        Root<BitacoraInfocred> p = q.from(BitacoraInfocred.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        if (StringUtils.trimToNull(codigoUsuario) != null) {
                            predicatesAnd.add(cb.equal(p.get("codigoUsuario"), StringUtils.trimToEmpty(codigoUsuario)));
                        }

                        if (desde != null && hasta != null) {
                            Date inicio = DateUtils.truncate(desde, Calendar.DAY_OF_MONTH);
                            Date inicio2 = DateUtils.truncate(hasta, Calendar.DAY_OF_MONTH);
                            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
                            predicatesAnd.add(cb.between(p.get("fechaHoraConsulta"), inicio, inicio2));
                        }
                        predicatesAnd.add(cb.equal(p.get("tipoRespuesta"), StringUtils.trimToEmpty(TipoConsumoRespuesta.CONSUMO.getDescripcion())));

                        if (tipoOficina != null) {
                            predicatesAnd.add(cb.equal(p.get("oficinaUsuario"), tipoOficina));
                        }

                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<BitacoraInfocred> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            log.error("Error en la consulta: {}", e);
        }
        return null;
    }

    @Override
    protected Path<Long> countPath(Root<BitacoraInfocred> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<BitacoraInfocred> p) {
        Path<String> codigoUsuario = p.get("codigoUsuario");
        return Collections.singletonList(codigoUsuario);
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<BitacoraInfocred> from, BitacoraInfocred example, BitacoraInfocred example2) {
        List<Predicate> predicates = new LinkedList<>();
        Date fecha = example.getFechaHoraConsulta();
        String usuario = StringUtils.trimToNull(example.getCodigoUsuario());
        //TipoRespuesta tipoRespuesta = example.getTipoRespuestaEnum();
        //VariableSegip tipoCanal = example.getTipoCanal();
        Oficina oficina = example.getOficina();
        if (fecha != null && example2.getFechaHoraConsulta() != null) {
            Date inicio = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH);
            Date inicio2 = DateUtils.truncate(example2.getFechaHoraConsulta(), Calendar.DAY_OF_MONTH);
            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
            log.debug("   FECHAS ENTRE [{}] A [{}]", inicio, inicio2);
            predicates.add(cb.between(from.get("fechaHoraConsulta"), inicio, inicio2));
        }

        if (usuario != null) {
            final String s = StringUtils.trimToEmpty(usuario);
            log.debug(">>>>> usuario=" + s);
            predicates.add(cb.equal(from.get("codigoUsuario"), s));
        }
            predicates.add(cb.equal(from.get("tipoRespuesta"), TipoConsumoRespuesta.CONSUMO.getDescripcion()));
        if (oficina != null) {
            log.debug(">>>>> tipoOficina =" + oficina);
            predicates.add(cb.equal(from.get("oficinaUsuario"), oficina.getCodigo()));
        }
        return predicates.toArray(new Predicate[predicates.size()]);
    }
}
