package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import com.bisa.bus.servicios.asfi.infocred.api.CuentasCorrientesService;
import com.bisa.bus.servicios.asfi.infocred.entities.CuentasCorrientes;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 *
 * @author Gary Mercado
 * Yrag Knup
 */
public class CuentasCorrientesDao extends DaoImpl<CuentasCorrientes, Long> implements Serializable, CuentasCorrientesService {

    protected CuentasCorrientesDao(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }
}
