package com.bisa.bus.servicios.asfi.infocred.api;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public interface SaldosPersonaDeudaService {
    String buscarGestionMayor(Long id);
}
