package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import com.bisa.bus.servicios.asfi.infocred.api.RectificacionService;
import com.bisa.bus.servicios.asfi.infocred.entities.Rectificacion;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 * Created by gmercado on 18/11/2016.
 */
public class RectificacionDao extends DaoImpl<Rectificacion, Long> implements Serializable, RectificacionService {

    protected RectificacionDao(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }
}
