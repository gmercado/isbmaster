/*
 * Copyright 2016 Banco Bisa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.consumoweb.infocred.objetos.SubAfpFila;
import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 *
 * @author gmercado
 */
@Entity
@Table(name = "IFP025")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I25FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I25USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I25FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I25USRMOD", columnDefinition = "CHAR(100)"))
})
public class DeudasAFP extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private PKDeudasAFP id;

    @Column(name = "I25ENTACRE")
    private String entidadAcreedora;

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "I25MONTO")
    private BigDecimal monto;

    @Column(name = "I25PERMORA")
    @Temporal(TemporalType.DATE)
    private Date periodoMora;

    @Column(name = "I25FECACT")
    @Temporal(TemporalType.DATE)
    private Date fechaActualizacion;

    public DeudasAFP() {
    }

    public DeudasAFP(SubAfpFila afp, InformeConfidencial informeConfidencial) {
        this.id = new PKDeudasAFP(informeConfidencial.getId(), Long.valueOf(afp.getFila()));
        this.entidadAcreedora = afp.getEntidadAcreedora();
        this.monto = FormatosUtils.stringToBigDecimal(afp.getMonto());
        this.periodoMora = FormatosUtils.deDDMMYYYYaFecha(afp.getPeriodosMora());
        this.fechaActualizacion = FormatosUtils.deDDMMYYYYaFecha(afp.getFechaActualizacion());
        this.fechaCreacion = informeConfidencial.getFechaCreacion();
        this.usuarioCreador = informeConfidencial.getUsuarioCreador();
    }

    public PKDeudasAFP getId() {
        return id;
    }

    public void setId(PKDeudasAFP id) {
        this.id = id;
    }

    public String getEntidadAcreedora() {
        return entidadAcreedora;
    }

    public void setEntidadAcreedora(String entidadAcreedora) {
        this.entidadAcreedora = entidadAcreedora;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public Date getPeriodoMora() {
        return periodoMora;
    }

    public void setPeriodoMora(Date periodoMora) {
        this.periodoMora = periodoMora;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    @Override
    public String toString() {
        return "DeudasAFP{" +
                "id=" + id +
                ", entidadAcreedora='" + entidadAcreedora + '\'' +
                ", monto=" + monto +
                ", periodoMora=" + periodoMora +
                ", fechaActualizacion=" + fechaActualizacion +
                '}';
    }
}
