/*
 * Copyright 2016 Banco Bisa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.consumoweb.enums.TipoDescripcion;
import bus.consumoweb.infocred.objetos.DescripcionCaedecacteconoFila;
import bus.database.model.EntityBase;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 *
 * @author gmercado
 */
@Entity
@Table(name = "IFP020")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I20FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I20USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I20FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I20USRMOD", columnDefinition = "CHAR(100)"))
})
public class SistemaFinancieroDescripcion extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private PKSistemaFinancieroDescripcion id;

    @Size(max = 10)
    @Column(name = "I20TIPO")
    private String tipo;

    @Size(max = 2)
    @Column(name = "I20CODUNO")
    private String codigoDescUno;

    @Size(max = 10)
    @Column(name = "I20CODDOS")
    private String codigoDescDos;

    @Size(max = 200)
    @Column(name = "I20DESUNO")
    private String descripcionUno;

    @Size(max = 200)
    @Column(name = "I20DESDOS")
    private String descripcionDos;

    @Size(max = 200)
    @Column(name = "I20DESTRS")
    private String descripcionTres;

    public SistemaFinancieroDescripcion() {
    }

    public SistemaFinancieroDescripcion(PKSistemaFinancieroDescripcion id, String tipo, String codigoDescUno, String codigoDescDos,
                                        String descripcionUno, String descripcionDos, String descripcionTres) {
        this.tipo = tipo;
        this.codigoDescUno = codigoDescUno;
        this.codigoDescDos = codigoDescDos;
        this.descripcionUno = descripcionUno;
        this.descripcionDos = descripcionDos;
        this.descripcionTres = descripcionTres;
    }

    public SistemaFinancieroDescripcion(DescripcionCaedecacteconoFila descripcionCaedecactecono,
                                        InformeConfidencial informeConfidencial) {
        // TODO: Faltan los demas tipos
        this.id = new PKSistemaFinancieroDescripcion(informeConfidencial.getId(), Long.valueOf(descripcionCaedecactecono.getFila()));
        this.tipo = TipoDescripcion.DESCRIPCIONCAEDECACTECONO.toString();
        this.codigoDescUno = descripcionCaedecactecono.getCaedecacteco();
        this.codigoDescDos = descripcionCaedecactecono.getCoddestinocreditoactividadeconomica();
        this.descripcionUno = descripcionCaedecactecono.getDescaedecacteco();
        this.descripcionDos = descripcionCaedecactecono.getDesdestinocreditoactividadeconomica();
        this.descripcionTres = descripcionCaedecactecono.getDesreportecaedecacteco();
        this.fechaCreacion = informeConfidencial.getFechaCreacion();
        this.usuarioCreador = informeConfidencial.getUsuarioCreador();
    }

    public PKSistemaFinancieroDescripcion getId() {
        return id;
    }

    public void setId(PKSistemaFinancieroDescripcion id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigoDescUno() {
        return codigoDescUno;
    }

    public void setCodigoDescUno(String codigoDescUno) {
        this.codigoDescUno = codigoDescUno;
    }

    public String getCodigoDescDos() {
        return codigoDescDos;
    }

    public void setCodigoDescDos(String codigoDescDos) {
        this.codigoDescDos = codigoDescDos;
    }

    public String getDescripcionUno() {
        return descripcionUno;
    }

    public void setDescripcionUno(String descripcionUno) {
        this.descripcionUno = descripcionUno;
    }

    public String getDescripcionDos() {
        return descripcionDos;
    }

    public void setDescripcionDos(String descripcionDos) {
        this.descripcionDos = descripcionDos;
    }

    public String getDescripcionTres() {
        return descripcionTres;
    }

    public void setDescripcionTres(String descripcionTres) {
        this.descripcionTres = descripcionTres;
    }

    @Override
    public String toString() {
        return "SistemaFinancieroDescripcion{" +
                "id=" + id +
                ", tipo='" + tipo + '\'' +
                ", codigoDescUno='" + codigoDescUno + '\'' +
                ", codigoDescDos='" + codigoDescDos + '\'' +
                ", descripcionUno='" + descripcionUno + '\'' +
                ", descripcionDos='" + descripcionDos + '\'' +
                ", descripcionTres='" + descripcionTres + '\'' +
                '}';
    }
}
