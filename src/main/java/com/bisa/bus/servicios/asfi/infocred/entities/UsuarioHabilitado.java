package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.consumoweb.enums.TipoAceptacion;
import bus.consumoweb.enums.TipoEstadoUsuario;
import bus.database.model.EntityBase;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@Entity
@Table(name = "IFP005")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I05FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I05USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I05FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I05USRMOD", columnDefinition = "CHAR(100)"))
})
public class UsuarioHabilitado extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I05ID")
    private Long id;

    @Column(unique = true, name = "I05LOGIN")
    private String login;

    @Column(name = "I05NRODOC")
    private String numeroDocumento;

    @Column(name = "I05NOMCOM")
    private String nombreCompleto;

    @Column(name = "I05ESTADO")
    @Enumerated(EnumType.STRING)
    private TipoEstadoUsuario estado;

    @Column(name = "I05CONDIR")
    @Enumerated(EnumType.STRING)
    private TipoAceptacion consultaDirecta;

    public UsuarioHabilitado() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public TipoEstadoUsuario getEstado() {
        return estado;
    }

    public void setEstado(TipoEstadoUsuario estado) {
        this.estado = estado;
    }

    public TipoAceptacion getConsultaDirecta() {
        return consultaDirecta;
    }

    public void setConsultaDirecta(TipoAceptacion consultaDirecta) {
        this.consultaDirecta = consultaDirecta;
    }

    @Override
    public String toString() {
        return "UsuarioHabilitado{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", numeroDocumento='" + numeroDocumento + '\'' +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                ", estado=" + estado +
                ", consultaDirecta=" + consultaDirecta +
                '}';
    }
}
