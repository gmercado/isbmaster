/*
 * Copyright 2016 Banco Bisa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.consumoweb.infocred.objetos.SubSistemaFinancieroFila;
import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 *
 * @author Gary Mercado
 * Yrag Knup
 */
@Entity
@Table(name = "IFP003")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I03FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I03USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I03FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I03USRMOD", columnDefinition = "CHAR(100)"))
})
public class DatosFinancieros extends EntityBase implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private PKDatosFinancieros id;

    @Size(max = 1)
    @Column(name = "I03SISTEMA")
    private String sistema;

    @Size(max = 100)
    @Column(name = "I03ENTIDAD")
    private String entidad;

    @Column(name = "I03FECACT")
    @Temporal(TemporalType.DATE)
    private Date fechaActualizacion;

    @Column(name = "I03FECPROC")
    @Temporal(TemporalType.DATE)
    private Date fechaProcesamiento;

    @Size(max = 100)
    @Column(name = "I03TIPCRE")
    private String tipoCredito;

    @Size(max = 100)
    @Column(name = "I03TIPOBL")
    private String tipoObligado;

    @Column(name = "I03FECINI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;

    @Column(name = "I03FECULTC")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaUltimaCuota;

    @Size(max = 3)
    @Column(name = "I03MONEDA")
    private String moneda;

    @Column(name = "I03MONORI", scale = 15, precision = 2)
    private BigDecimal montoOriginal;

    @Column(name = "I03VALCUO", scale = 15, precision = 2)
    private BigDecimal valorCuota;

    @Size(max = 1)
    @Column(name = "I03CALIFI")
    private String calificacion;

    @Size(max = 50)
    @Column(name = "I03ESTADO")
    private String estado;

    @Column(name = "I03SALDO", scale = 15, precision = 2)
    private BigDecimal saldo;

    @Column(name = "I03CONTING", scale = 15, precision = 2)
    private BigDecimal contingente;

    @Column(name = "I03DIASMOR", scale = 15, precision = 2)
    private BigDecimal diasMora;

    @Size(max = 20)
    @Column(name = "I03HISTORI")
    private String historico;

    @Size(max = 100)
    @Column(name = "I03PERPAG")
    private String peridoPago;

    @Size(max = 1)
    @Column(name = "I03TIPCAN")
    private String tipCan;

    @Size(max = 2)
    @Column(name = "I03TIPINT")
    private String tipInt;

    @Size(max = 2)
    @Column(name = "I03TIPOPE")
    private String tipOpe;

    @Size(max = 1)
    @Column(name = "I03TIPING")
    private String tipIng;

    @Size(max = 10)
    @Column(name = "I03CAEDEC")
    private String caeDec;

    @Size(max = 10)
    @Column(name = "I03ACTCONO")
    private String actcono;

    public DatosFinancieros() {
    }

    public DatosFinancieros(String sistema, String entidad, Date fechaActualizacion, Date fechaProcesamiento, String tipoCredito,
                            String tipoObligado, Date fechaInicio, Date fechaUltimaCuota, String moneda, BigDecimal montoOriginal,
                            BigDecimal valorCuota, String calificacion, String estado, BigDecimal saldo, BigDecimal contingente,
                            BigDecimal diasMora, String historico, String peridoPago, String tipCan, String tipInt, String tipOpe,
                            String tipIng, String caeDec, String actcono) {
        this.sistema = sistema;
        this.entidad = entidad;
        this.fechaActualizacion = fechaActualizacion;
        this.fechaProcesamiento = fechaProcesamiento;
        this.tipoCredito = tipoCredito;
        this.tipoObligado = tipoObligado;
        this.fechaInicio = fechaInicio;
        this.fechaUltimaCuota = fechaUltimaCuota;
        this.moneda = moneda;
        this.montoOriginal = montoOriginal;
        this.valorCuota = valorCuota;
        this.calificacion = calificacion;
        this.estado = estado;
        this.saldo = saldo;
        this.contingente = contingente;
        this.diasMora = diasMora;
        this.historico = historico;
        this.peridoPago = peridoPago;
        this.tipCan = tipCan;
        this.tipInt = tipInt;
        this.tipOpe = tipOpe;
        this.tipIng = tipIng;
        this.caeDec = caeDec;
        this.actcono = actcono;
    }

    public DatosFinancieros(SubSistemaFinancieroFila sFinanciero, InformeConfidencial informeConfidencial) {
        this.id = new PKDatosFinancieros(informeConfidencial.getId(), Long.valueOf(sFinanciero.getFila()));
        this.sistema = sFinanciero.getSistema();
        this.entidad = sFinanciero.getEntidad();
        this.fechaActualizacion = FormatosUtils.deDDMMYYYYaFecha(sFinanciero.getFechaActualizacion());
        this.fechaProcesamiento = FormatosUtils.deDDMMYYYYaFecha(sFinanciero.getFechaProcesamiento());
        this.tipoCredito = sFinanciero.getTipoCredito();
        this.tipoObligado = sFinanciero.getTipoObligado();
        this.fechaInicio = FormatosUtils.deDDMMYYYYaFecha(sFinanciero.getFechaInicio());
        this.fechaUltimaCuota = FormatosUtils.deDDMMYYYYaFecha(sFinanciero.getFechaUltimaCuota());
        this.moneda = sFinanciero.getMoneda();
        this.montoOriginal = FormatosUtils.stringToBigDecimal(sFinanciero.getMontoOriginal());
        this.valorCuota = FormatosUtils.stringToBigDecimal(sFinanciero.getValorCuota());
        this.calificacion = sFinanciero.getCalificacion();
        this.estado = sFinanciero.getEstado();
        this.saldo = FormatosUtils.stringToBigDecimal(sFinanciero.getSaldo());
        this.contingente = FormatosUtils.stringToBigDecimal(sFinanciero.getContingente());
        this.diasMora = FormatosUtils.stringToBigDecimal(sFinanciero.getDiasMora());
        this.historico = sFinanciero.getHistorico();
        this.peridoPago = sFinanciero.getPeriodoDePago();
        this.tipCan = sFinanciero.getTipCan();
        this.tipInt = sFinanciero.getTipInt();
        this.tipOpe = sFinanciero.getTipOpe();
        this.tipIng = sFinanciero.getTipIng();
        this.caeDec = sFinanciero.getCaedec();
        this.actcono = sFinanciero.getActecono();
        this.fechaCreacion = informeConfidencial.getFechaCreacion();
        this.usuarioCreador = informeConfidencial.getUsuarioCreador();
    }

    public PKDatosFinancieros getId() {
        return id;
    }

    public void setId(PKDatosFinancieros id) {
        this.id = id;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Date getFechaProcesamiento() {
        return fechaProcesamiento;
    }

    public void setFechaProcesamiento(Date fechaProcesamiento) {
        this.fechaProcesamiento = fechaProcesamiento;
    }

    public String getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public String getTipoObligado() {
        return tipoObligado;
    }

    public void setTipoObligado(String tipoObligado) {
        this.tipoObligado = tipoObligado;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaUltimaCuota() {
        return fechaUltimaCuota;
    }

    public void setFechaUltimaCuota(Date fechaUltimaCuota) {
        this.fechaUltimaCuota = fechaUltimaCuota;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getMontoOriginal() {
        return montoOriginal;
    }

    public void setMontoOriginal(BigDecimal montoOriginal) {
        this.montoOriginal = montoOriginal;
    }

    public BigDecimal getValorCuota() {
        return valorCuota;
    }

    public void setValorCuota(BigDecimal valorCuota) {
        this.valorCuota = valorCuota;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public BigDecimal getContingente() {
        return contingente;
    }

    public void setContingente(BigDecimal contingente) {
        this.contingente = contingente;
    }

    public BigDecimal getDiasMora() {
        return diasMora;
    }

    public void setDiasMora(BigDecimal diasMora) {
        this.diasMora = diasMora;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public String getPeridoPago() {
        return peridoPago;
    }

    public void setPeridoPago(String peridoPago) {
        this.peridoPago = peridoPago;
    }

    public String getTipCan() {
        return tipCan;
    }

    public void setTipCan(String tipCan) {
        this.tipCan = tipCan;
    }

    public String getTipInt() {
        return tipInt;
    }

    public void setTipInt(String tipInt) {
        this.tipInt = tipInt;
    }

    public String getTipOpe() {
        return tipOpe;
    }

    public void setTipOpe(String tipOpe) {
        this.tipOpe = tipOpe;
    }

    public String getTipIng() {
        return tipIng;
    }

    public void setTipIng(String tipIng) {
        this.tipIng = tipIng;
    }

    public String getCaeDec() {
        return caeDec;
    }

    public void setCaeDec(String caeDec) {
        this.caeDec = caeDec;
    }

    public String getActcono() {
        return actcono;
    }

    public void setActcono(String actcono) {
        this.actcono = actcono;
    }

    @Override
    public String toString() {
        return "DatosFinancieros{" +
                "id=" + id +
                ", sistema='" + sistema + '\'' +
                ", entidad='" + entidad + '\'' +
                ", fechaActualizacion=" + fechaActualizacion +
                ", fechaProcesamiento=" + fechaProcesamiento +
                ", tipoCredito='" + tipoCredito + '\'' +
                ", tipoObligado='" + tipoObligado + '\'' +
                ", fechaInicio=" + fechaInicio +
                ", fechaUltimaCuota=" + fechaUltimaCuota +
                ", moneda='" + moneda + '\'' +
                ", montoOriginal=" + montoOriginal +
                ", valorCuota=" + valorCuota +
                ", calificacion='" + calificacion + '\'' +
                ", estado='" + estado + '\'' +
                ", saldo=" + saldo +
                ", contingente=" + contingente +
                ", diasMora=" + diasMora +
                ", historico='" + historico + '\'' +
                ", peridoPago='" + peridoPago + '\'' +
                ", tipCan='" + tipCan + '\'' +
                ", tipInt='" + tipInt + '\'' +
                ", tipOpe='" + tipOpe + '\'' +
                ", tipIng='" + tipIng + '\'' +
                ", caeDec='" + caeDec + '\'' +
                ", actcono='" + actcono + '\'' +
                '}';
    }
}
