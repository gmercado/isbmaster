package com.bisa.bus.servicios.asfi.infocred.api;

import com.bisa.bus.servicios.asfi.infocred.entities.InfocredCarteraComa;

import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public interface InfocredCarteraComaService {

    boolean guardarCartera(String nombreArchivo, List<String> list, String separador);
}
