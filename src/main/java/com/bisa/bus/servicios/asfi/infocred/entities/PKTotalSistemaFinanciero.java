package com.bisa.bus.servicios.asfi.infocred.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@Embeddable
public class PKTotalSistemaFinanciero implements Serializable {

    @Column(name = "I22ID")
    private Long id;

    @Column(name = "I22FILA")
    private Long fila;

    public PKTotalSistemaFinanciero() {
    }

    public PKTotalSistemaFinanciero(Long id, Long fila) {
        this.id = id;
        this.fila = fila;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PKTotalSistemaFinanciero that = (PKTotalSistemaFinanciero) o;

        if (!id.equals(that.id)) return false;
        return fila.equals(that.fila);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + fila.hashCode();
        return result;
    }
}
