package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import com.bisa.bus.servicios.asfi.infocred.api.SistemaFinancieroDescripcionService;
import com.bisa.bus.servicios.asfi.infocred.entities.SistemaFinancieroDescripcion;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 * Created by gmercado on 18/11/2016.
 */
public class SistemaFinancieroDescripcionDao extends DaoImpl<SistemaFinancieroDescripcion, Long> implements Serializable, SistemaFinancieroDescripcionService {
    protected SistemaFinancieroDescripcionDao(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }
}
