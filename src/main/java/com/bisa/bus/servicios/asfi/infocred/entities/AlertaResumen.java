package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Gary Mercado
 * Yrag knup
 */
@Entity
@Table(name = "IFP032")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I32FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I32USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I32FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I32USRMOD", columnDefinition = "CHAR(100)"))
})
public class AlertaResumen extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I32ID", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "I32CABECER")
    private Long idCabecera;

    @Column(name = "I32ENTIDAD")//1
    private String entidad;

    @Column(name = "I32CANT")//2
    private Long cantidad;

    @Column(name = "I32DEPTO")//3
    private String departamento;

    @Column(name = "I32CLICONS")//4
    private Long clientesConsultados;

    @Column(name = "I32CLICOMP")//5
    private Long clientesCompartidos;

    @Column(name = "I32CLIPROP")//6
    private Long clientesPropios;

    public AlertaResumen() {
    }

    public AlertaResumen(Long idCabecera, String usuario, Date fechaAlta) {
        this.idCabecera = idCabecera;
        super.usuarioCreador = usuario;
        super.fechaCreacion = fechaAlta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCabecera() {
        return idCabecera;
    }

    public void setIdCabecera(Long idCabecera) {
        this.idCabecera = idCabecera;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Long getClientesConsultados() {
        return clientesConsultados;
    }

    public void setClientesConsultados(Long clientesConsultados) {
        this.clientesConsultados = clientesConsultados;
    }

    public Long getClientesCompartidos() {
        return clientesCompartidos;
    }

    public void setClientesCompartidos(Long clientesCompartidos) {
        this.clientesCompartidos = clientesCompartidos;
    }

    public Long getClientesPropios() {
        return clientesPropios;
    }

    public void setClientesPropios(Long clientesPropios) {
        this.clientesPropios = clientesPropios;
    }

    @Override
    public String toString() {
        return "AlertaResumen{" +
                "id=" + id +
                ", idCabecera=" + idCabecera +
                ", entidad='" + entidad + '\'' +
                ", cantidad=" + cantidad +
                ", departamento='" + departamento + '\'' +
                ", clientesConsultados='" + clientesConsultados + '\'' +
                ", clientesCompartidos='" + clientesCompartidos + '\'' +
                ", clientesPropios='" + clientesPropios + '\'' +
                '}';
    }
}
