/*
 * Copyright 2016 Banco Bisa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.consumoweb.enums.TipoEstadoTitular;
import bus.consumoweb.infocred.objetos.DatosTitular;
import bus.database.model.EntityBase;
import bus.env.api.Variables;
import bus.plumbing.utils.FormatosUtils;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@Entity
@Table(name = "IFP004")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I04FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I04USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I04FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I04USRMOD", columnDefinition = "CHAR(100)"))
})
public class Titulares extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I04ID")
    private Long id;

    @Column(name = "I04DOCUM")
    private String documento;

    @Column(name = "I04EXTEN")
    private String extension;

    @Column(name = "I04TIPDOC")
    private String tipoDocumento;

    @Column(name = "I04NOMCOM")
    private String nombreCompleto;

    @Column(name = "I04FECNAC")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;

    @Column(name = "I04ESTADO")
    private String estado;

    @Column(name = "I04IDCABEC")
    private Long idCabecera;

    @Transient
    private boolean consumo;

    @Transient
    private boolean forzarConsulta;

    public Titulares() {
    }

    public Titulares(String documento, String tipoDocumento, String nombreCompleto) {
        this.documento = documento;
        this.tipoDocumento = tipoDocumento;
        this.nombreCompleto = nombreCompleto;
        this.estado = TipoEstadoTitular.N.toString();
        this.fechaCreacion = new Date();
        this.usuarioCreador = Variables.INFOCRED_USUARIO_ALTA_JOBS;
    }

    public Titulares(String documento, String extension, String tipoDocumento, String nombreCompleto, Date fechaNacimiento) {
        this.documento = documento;
        this.extension = extension;
        this.tipoDocumento = tipoDocumento;
        this.nombreCompleto = nombreCompleto;
        this.fechaNacimiento = fechaNacimiento;
    }

    public Titulares(DatosTitular datosTitular) {
        this.documento = datosTitular.getDocumentoIdentidad();
        this.extension = datosTitular.getExtension();
        this.tipoDocumento = datosTitular.getTipoDocumentoIdentidad();
        this.nombreCompleto = datosTitular.getNombreCompleto();
        this.fechaNacimiento = FormatosUtils.deDDMMYYYYaFecha(datosTitular.getFechaNacimiento()
                != null ? datosTitular.getFechaNacimiento().trim() : "");
        this.fechaCreacion = new Date();
        this.usuarioCreador = Variables.INFOCRED_USUARIO_ALTA_JOBS;
        this.setConsumo(true);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public boolean isForzarConsulta() {
        return forzarConsulta;
    }

    public void setForzarConsulta(boolean forzarConsulta) {
        this.forzarConsulta = forzarConsulta;
    }

    public String getFechaNacimientoFormato() {
        return FormatosUtils.fechaFormateadaConYYYY(fechaNacimiento);
    }

    public boolean isConsumo() {
        return consumo;
    }

    public void setConsumo(boolean consumo) {
        this.consumo = consumo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getIdCabecera() {
        return idCabecera;
    }

    public void setIdCabecera(Long idCabecera) {
        this.idCabecera = idCabecera;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Titulares)) {
            return false;
        }
        Titulares other = (Titulares) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Titulares{" +
                "id=" + id +
                ", documento='" + documento + '\'' +
                ", extension='" + extension + '\'' +
                ", tipoDocumento='" + tipoDocumento + '\'' +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", estado='" + estado + '\'' +
                ", idCabecera=" + idCabecera +
                ", consumo=" + consumo +
                ", forzarConsulta=" + forzarConsulta +
                '}';
    }
}
