package com.bisa.bus.servicios.asfi.infocred.ui;

import bus.consumoweb.enums.CodigoRespuesta;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.file.FileTemp;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.asfi.infocred.model.RespuestaConsulta;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;
import org.apache.commons.io.FileUtils;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.PopupSettings;
import org.apache.wicket.markup.html.link.ResourceLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.IRequestCycle;
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler;
import org.apache.wicket.request.resource.ContentDisposition;
import org.apache.wicket.request.resource.ResourceStreamResource;
import org.apache.wicket.util.resource.FileResourceStream;
import org.apache.wicket.util.resource.IResourceStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@AuthorizeInstantiation({RolesBisa.INFOCRED_ADMIN, RolesBisa.INFOCRED_USUARIO})
@Titulo("Respuesta INFOCRED")
public class RespuestaInfocred extends BisaWebPage {

    private RespuestaConsulta response;
    private SolicitudConsultaTitularInfocredForm request;

    private static final Logger log = LoggerFactory.getLogger(RespuestaInfocred.class);

    public RespuestaInfocred(SolicitudConsultaTitularInfocredForm request, RespuestaConsulta response) {
        super();
        this.request = request;
        this.response = response;
        inicio();
    }

    public void inicio() {
        //Model
        IModel<RespuestaConsulta> objectModel = new CompoundPropertyModel<>(response);
        //Form
        Form<RespuestaConsulta> form = new Form<>("respuestaForm", objectModel);
        add(form);
        //Cuerpo
        form.add(new Label("numeroDocumento", Model.of(request.getTitular().getDocumento())));
        form.add(new Label("nombreCompleto", Model.of(request.getTitular().getNombreCompleto())));
        form.add(new Label("tipoServicio"));
        form.add(new Label("tipoRespuesta"));
        form.add(new Label("codError", Model.of(response.getCodError().getCodigo())));
        form.add(new Label("descError"));
        //botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(new ConsultaInfocred());
            }
        });

        if (response.getCodError() != null && CodigoRespuesta.OK.getCodigo().equals(response.getCodError().getCodigo())) {
            String nombre = FormatosUtils.getFechaByFormatoDDMMYYYYhhmmss(new Date());
            File file = FileTemp.create(nombre, ".pdf", response.getDocumentoPDF());
            IResourceStream firs = new FileResourceStream(file);
            try {
                form.add(new ResourceLink("reporte", new ResourceStreamResource() {
                    public IResourceStream getResourceStream() {
                        getRequestCycle().scheduleRequestHandlerAfterCurrent(
                                new ResourceStreamRequestHandler(firs) {
                                    @Override
                                    public void respond(IRequestCycle requestCycle) {
                                        super.respond(requestCycle);
                                        //FileUtils.deleteQuietly(file);
                                    }
                                }.setFileName(nombre).setContentDisposition(ContentDisposition.ATTACHMENT));
                        return firs;
                    }
                }).setPopupSettings(new PopupSettings("Reporte", PopupSettings.RESIZABLE | PopupSettings.SCROLLBARS).setHeight(500).setWidth(700)));
            } catch (Exception e) {
                log.error("Ocurrio un error al exportar el PDF ", e);
            } finally {
                if (firs != null) {
                    try {
                        firs.close();
                    } catch (IOException ign) {}
                }
            }
        } else {
            form.add(new Button("reporte") {
                @Override
                public void onSubmit() {
                    getSession().error("No se puede generar el reporte ...");
                }
            }.setDefaultFormProcessing(false));
        }
    }
}
