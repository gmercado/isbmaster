/*
 * Copyright 2016 Banco Bisa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.consumoweb.infocred.objetos.SubRectificacionesFila;
import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author gmercado
 */
@Entity
@Table(name = "IFP026")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I26FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I26USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I26FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I26USRMOD", columnDefinition = "CHAR(100)"))
})
public class Rectificacion extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private PKRectificacion id;

    @Column(name = "I26ENTIDAD")
    private String entidad;

    @Column(name = "I26FECRESP")
    @Temporal(TemporalType.DATE)
    private Date fechaRespaldo;

    @Column(name = "I26RESPALD")
    private String respaldo;

    @Column(name = "I26NUMOPE")
    private String numeroOperacion;

    @Column(name = "I26FECDES")
    @Temporal(TemporalType.DATE)
    private Date fechaDesde;

    @Column(name = "I26FECHAST")
    @Temporal(TemporalType.DATE)
    private Date fechaHasta;

    @Column(name = "I26MOTIVO")
    private String motivo;

    @Column(name = "I26NOTREC")
    private String detalleNotaRectifiacadora;

    public Rectificacion() {
    }

    public Rectificacion(SubRectificacionesFila rectificacion, InformeConfidencial informeConfidencial) {
        this.id = new PKRectificacion(Long.valueOf(informeConfidencial.getId()), Long.valueOf(rectificacion.getFila()));
        this.entidad = rectificacion.getEntidad();
        this.fechaRespaldo = FormatosUtils.deDDMMYYYYaFecha(rectificacion.getFechaRespaldo());
        this.respaldo = rectificacion.getRespaldo();
        this.numeroOperacion = rectificacion.getNumeroOperacion();
        this.fechaDesde = FormatosUtils.deDDMMYYYYaFecha(rectificacion.getFechaDesde());
        this.fechaHasta = FormatosUtils.deDDMMYYYYaFecha(rectificacion.getFechaHasta());
        this.motivo = rectificacion.getMotivo();
        this.detalleNotaRectifiacadora = rectificacion.getDetalleNotaRectificatoria();
        this.fechaCreacion = informeConfidencial.getFechaCreacion();
        this.usuarioCreador = informeConfidencial.getUsuarioCreador();
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public Date getFechaRespaldo() {
        return fechaRespaldo;
    }

    public void setFechaRespaldo(Date fechaRespaldo) {
        this.fechaRespaldo = fechaRespaldo;
    }

    public String getRespaldo() {
        return respaldo;
    }

    public void setRespaldo(String respaldo) {
        this.respaldo = respaldo;
    }

    public String getNumeroOperacion() {
        return numeroOperacion;
    }

    public void setNumeroOperacion(String numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getDetalleNotaRectifiacadora() {
        return detalleNotaRectifiacadora;
    }

    public void setDetalleNotaRectifiacadora(String detalleNotaRectifiacadora) {
        this.detalleNotaRectifiacadora = detalleNotaRectifiacadora;
    }

    @Override
    public String toString() {
        return "Rectificacion{" +
                "id=" + id +
                ", entidad='" + entidad + '\'' +
                ", fechaRespaldo=" + fechaRespaldo +
                ", respaldo='" + respaldo + '\'' +
                ", numeroOperacion='" + numeroOperacion + '\'' +
                ", fechaDesde=" + fechaDesde +
                ", fechaHasta=" + fechaHasta +
                ", motivo='" + motivo + '\'' +
                ", detalleNotaRectifiacadora='" + detalleNotaRectifiacadora + '\'' +
                '}';
    }
}
