package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Gary Mercado
 * Yrag knup
 */
@Entity
@Table(name = "IFP031")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I31FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I31USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I31FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I31USRMOD", columnDefinition = "CHAR(100)"))
})
public class AlertaCuerpo extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I31ID", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "I31CABECER")
    private Long idCabecera;

    @Column(name = "I31TIPODI")//1
    private String tipoDi;

    @Column(name = "I31NRODI")//2
    private String numeroDi;

    @Column(name = "I31EXTEN")//3
    private String extension;

    @Column(name = "I31NOMCOMP")//4
    private String nombreCompleto;

    @Column(name = "I31FECNAC")//5
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;

    @Column(name = "I31ENTIDAD")//6
    private String entidad;

    @Column(name = "I31FECHA")//7
    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Column(name = "I31TIPOBL")//8
    private String tipoObligacion;

    @Column(name = "I31TIPCRED")//9
    private String tipoCredito;

    @Column(name = "I31ESTADO")//10
    private String estado;

    @Column(name = "I31MONTO")//11
    private BigDecimal montoActual;

    @Column(name = "I31ESTANT")//12
    private String estadoAnterior;

    @Column(name = "I31MONTANT")//13
    private BigDecimal montoAnterior;

    @Column(name = "I31DEPTO")//14
    private String departamento;

    @Column(name = "I31CALINST")//15
    private String calificacionInstitucion;

    @Column(name = "I31SALINST")//16
    private BigDecimal saldoInstitucion;

    @Column(name = "I31ESTCLI")//17
    private String estadoCliente;

    public AlertaCuerpo() {
    }

    public AlertaCuerpo(Long idCabecera, String usuario, Date fechaAlta) {
        this.idCabecera = idCabecera;
        super.usuarioCreador = usuario;
        super.fechaCreacion = fechaAlta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCabecera() {
        return idCabecera;
    }

    public void setIdCabecera(Long idCabecera) {
        this.idCabecera = idCabecera;
    }

    public String getTipoDi() {
        return tipoDi;
    }

    public void setTipoDi(String tipoDi) {
        this.tipoDi = tipoDi;
    }

    public String getNumeroDi() {
        return numeroDi;
    }

    public void setNumeroDi(String numeroDi) {
        this.numeroDi = numeroDi;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipoObligacion() {
        return tipoObligacion;
    }

    public void setTipoObligacion(String tipoObligacion) {
        this.tipoObligacion = tipoObligacion;
    }

    public String getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public BigDecimal getMontoActual() {
        return montoActual;
    }

    public void setMontoActual(BigDecimal montoActual) {
        this.montoActual = montoActual;
    }

    public String getEstadoAnterior() {
        return estadoAnterior;
    }

    public void setEstadoAnterior(String estadoAnterior) {
        this.estadoAnterior = estadoAnterior;
    }

    public BigDecimal getMontoAnterior() {
        return montoAnterior;
    }

    public void setMontoAnterior(BigDecimal montoAnterior) {
        this.montoAnterior = montoAnterior;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCalificacionInstitucion() {
        return calificacionInstitucion;
    }

    public void setCalificacionInstitucion(String calificacionInstitucion) {
        this.calificacionInstitucion = calificacionInstitucion;
    }

    public BigDecimal getSaldoInstitucion() {
        return saldoInstitucion;
    }

    public void setSaldoInstitucion(BigDecimal saldoInstitucion) {
        this.saldoInstitucion = saldoInstitucion;
    }

    public String getEstadoCliente() {
        return estadoCliente;
    }

    public void setEstadoCliente(String estadoCliente) {
        this.estadoCliente = estadoCliente;
    }

    @Override
    public String toString() {
        return "AlertaCuerpo{" +
                "id=" + id +
                ", idCabecera=" + idCabecera +
                ", tipoDi='" + tipoDi + '\'' +
                ", numeroDi='" + numeroDi + '\'' +
                ", extension='" + extension + '\'' +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", entidad='" + entidad + '\'' +
                ", fecha=" + fecha +
                ", tipoObligacion='" + tipoObligacion + '\'' +
                ", tipoCredito='" + tipoCredito + '\'' +
                ", estado='" + estado + '\'' +
                ", montoActual=" + montoActual +
                ", estadoAnterior='" + estadoAnterior + '\'' +
                ", montoAnterior=" + montoAnterior +
                ", departamento='" + departamento + '\'' +
                ", calificacionInstitucion='" + calificacionInstitucion + '\'' +
                ", saldoInstitucion=" + saldoInstitucion +
                ", estadoCliente='" + estadoCliente + '\'' +
                '}';
    }
}
