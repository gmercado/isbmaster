package com.bisa.bus.servicios.asfi.infocred.api;

import com.bisa.bus.servicios.asfi.infocred.entities.AlertaCabecera;
import java.io.File;
import java.sql.Connection;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public interface AlertaCabeceraService {

    AlertaCabecera guardarAlerta(File archivo, String nombreOriginal);

    List<AlertaCabecera> buscarPorNombreArchivo(String nombre);

    Connection unWrapConnection();
}
