
package com.bisa.bus.servicios.asfi.infocred.consumer.stub;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIndividualReport4Result" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getIndividualReport4Result"
})
@XmlRootElement(name = "GetIndividualReport4Response")
public class GetIndividualReport4Response {

    @XmlElement(name = "GetIndividualReport4Result")
    protected String getIndividualReport4Result;

    /**
     * Obtiene el valor de la propiedad getIndividualReport4Result.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetIndividualReport4Result() {
        return getIndividualReport4Result;
    }

    /**
     * Define el valor de la propiedad getIndividualReport4Result.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetIndividualReport4Result(String value) {
        this.getIndividualReport4Result = value;
    }

}
