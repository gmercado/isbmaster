package com.bisa.bus.servicios.asfi.infocred.ws;

import com.bisa.bus.servicios.asfi.infocred.api.InfocredService;
import com.bisa.bus.servicios.asfi.infocred.entities.Titulares;
import com.bisa.bus.servicios.asfi.infocred.model.RespuestaConsulta;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;
import com.google.inject.Inject;

import javax.jws.WebService;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@WebService(targetNamespace = "bisa.com")
public class WebServiceInfocredEndPoint implements WebServiceInfocred {

    @Inject
    InfocredService infocredService;

    @Override
       public List<Titulares> consultaDatosTitular(SolicitudConsultaTitularInfocredForm request) {
        return infocredService.obtenerDatosTitularPorNombre(request);
    }

    @Override
    public RespuestaConsulta consultarTitular(SolicitudConsultaTitularInfocredForm request) {
        return infocredService.getInfoCrediticioTitular(request);
    }
}
