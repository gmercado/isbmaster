package com.bisa.bus.servicios.asfi.infocred.api;

import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public interface InfocredCarteraZipPipeService {

    boolean guardarCartera(String nombreArchivo, List<String> list, String separador);
}
