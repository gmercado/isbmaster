/*
 * Copyright 2016 Banco Bisa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.consumoweb.enums.TipoDeuda;
import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@Entity
@Table(name = "IFP027")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I27FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I27USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I27FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I27USRMOD", columnDefinition = "CHAR(100)"))
})
public class SaldosPersonaDeuda extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private PKSaldosPersonaDeuda id;

    @Column(name = "I27ABREV")
    private String abreviatura;

    @Column(name = "I27ESTADO")
    private String estado;

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "I27SALDO", scale = 15, precision = 2)
    private BigDecimal saldo;

    public SaldosPersonaDeuda() {
    }

    public SaldosPersonaDeuda(PKSaldosPersonaDeuda id, String tipo, String abreviatura, String estado,
                              BigDecimal saldo, String estado2, BigDecimal saldo2, String estado3,
                              BigDecimal saldo3, String estado4, BigDecimal saldo4, String estado5, BigDecimal saldo5) {
        this.id = id;
        this.abreviatura = abreviatura;
        this.estado = estado;
        this.saldo = saldo;
    }

    public SaldosPersonaDeuda(InformeConfidencial informeConfidencial, TipoDeuda tipoDeuda,
                              String fila, String abreviatura, String gestion, String estado, String saldo) {
        this.id = new PKSaldosPersonaDeuda(informeConfidencial.getId(), Long.valueOf(fila), tipoDeuda.toString(), gestion);
        this.estado = estado;
        this.abreviatura = abreviatura;
        this.saldo = FormatosUtils.stringToBigDecimal(saldo);
        this.fechaCreacion = informeConfidencial.getFechaCreacion();
        this.usuarioCreador = informeConfidencial.getUsuarioCreador();
    }

    public PKSaldosPersonaDeuda getId() {
        return id;
    }

    public void setId(PKSaldosPersonaDeuda id) {
        this.id = id;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "SaldosPersonaDeuda{" +
                "id=" + id +
                ", abreviatura='" + abreviatura + '\'' +
                ", estado='" + estado + '\'' +
                ", saldo=" + saldo +
                '}';
    }
}
