package com.bisa.bus.servicios.asfi.infocred.ui;

import bus.database.components.FiltroPanel;
import com.bisa.bus.servicios.asfi.infocred.api.InfocredService;
import com.bisa.bus.servicios.asfi.infocred.api.UsuarioHabilitadoService;
import com.bisa.bus.servicios.asfi.infocred.consumer.stub.Titular;
import com.bisa.bus.servicios.asfi.infocred.consumer.stub.Usuario;
import com.bisa.bus.servicios.asfi.infocred.entities.BitacoraInfocred;
import com.bisa.bus.servicios.asfi.infocred.entities.UsuarioHabilitado;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;
import com.bisa.bus.servicios.segip.api.ParametrosSegipService;
import com.bisa.bus.servicios.segip.entities.Oficina;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.resource.PackageResourceReference;

import java.util.List;

import static bus.users.api.Metadatas.USER_META_DATA_KEY;
/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class ListadoConsumosInfocredFiltroPanel extends FiltroPanel<BitacoraInfocred> {

    @Inject
    ParametrosSegipService parametros;
    @Inject
    private InfocredService infocredService;
    @Inject
    private UsuarioHabilitadoService usuarioHabilitadoService;

    private Image redIcon;
    private Image yellowIcon;
    private Image greenIcon;

    protected ListadoConsumosInfocredFiltroPanel(String id, IModel<BitacoraInfocred> model1, IModel<BitacoraInfocred> model2) {
        super(id, model1, model2);

        final WebMarkupContainer cont = new WebMarkupContainer("cont");
        cont.setOutputMarkupPlaceholderTag(true);
        cont.setOutputMarkupId(true);

        // Iconos
        redIcon = new Image("red-icon", new PackageResourceReference(ListadoConsumosInfocredFiltroPanel.class, "imagenes/red-icon.png"));
        redIcon.setOutputMarkupPlaceholderTag(true);
        redIcon.setOutputMarkupId(true);
        redIcon.setVisible(false);
        cont.add(redIcon);

        greenIcon = new Image("green-icon", new PackageResourceReference(ListadoConsumosInfocredFiltroPanel.class, "imagenes/green-icon.png"));
        greenIcon.setOutputMarkupPlaceholderTag(true);
        greenIcon.setOutputMarkupId(true);
        greenIcon.setVisible(false);
        cont.add(greenIcon);

        yellowIcon = new Image("yellow-icon", new PackageResourceReference(ListadoConsumosInfocredFiltroPanel.class, "imagenes/yellow-icon.png"));
        yellowIcon.setOutputMarkupPlaceholderTag(true);
        yellowIcon.setOutputMarkupId(true);
        yellowIcon.setVisible(true);
        cont.add(yellowIcon);
        add(cont);

        String usuario = getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase();
        // Con esto verificamos si hay conexion con infocred, se hace una peticion de datos de titular(No tiene costo)
        add(new AjaxButton("verificarConsumo") {
            @Override
            public void onSubmit(AjaxRequestTarget target, Form form) {
                greenIcon.setVisible(true);
                List<UsuarioHabilitado> resultado = usuarioHabilitadoService.buscarPorLogin(usuario);
                if (resultado.size() == 0) {
                    getSession().info("Debes actualizar la informaci\u00F3 del usuario " + model1.getObject().getCodigoUsuario() + ", comunicate con el encargado.");
                    setResponsePage(new ListadoConsumoInfocred());
                    return;
                }
                UsuarioHabilitado usuarioHabilitado = resultado.get(0);
                Usuario u = new Usuario(usuarioHabilitado.getNumeroDocumento(), usuarioHabilitado.getNombreCompleto());
                Titular t = new Titular(0, "0", usuarioHabilitado.getNombreCompleto());
                SolicitudConsultaTitularInfocredForm request =
                        new SolicitudConsultaTitularInfocredForm(u, t, "0", "ISB",  true);
                if(infocredService.obtenerDatosTitularPorNombre(request) != null) {
                    redIcon.setVisible(false);
                    yellowIcon.setVisible(false);
                    greenIcon.setVisible(true);
                } else {
                    redIcon.setVisible(true);
                    yellowIcon.setVisible(false);
                    greenIcon.setVisible(false);
                }
                target.add(cont);
            }
        }.setDefaultFormProcessing(false));

        final String datePattern = "dd/MM/yyyy";
        DateTextField fechaDesde = new DateTextField("fechaDesde", new PropertyModel<>(model1, "fechaHoraConsulta"), datePattern);
        fechaDesde.setRequired(true);
        fechaDesde.add(new DatePicker());

        DateTextField fechaHasta = new DateTextField("fechaHasta", new PropertyModel<>(model2, "fechaHoraConsulta"), datePattern);
        fechaHasta.setRequired(true);
        fechaHasta.add(new DatePicker());

        add(new TextField<>("usuario", new PropertyModel<>(model1, "codigoUsuario")));
        add(fechaDesde);
        add(fechaHasta);

        add(new DropDownChoice<>("oficina", new PropertyModel<>(model1, "oficina"),
                new LoadableDetachableModel<List<? extends Oficina>>() {
                    @Override
                    protected List<? extends Oficina> load() {
                        return parametros.getOficinas();
                    }
                }, new IChoiceRenderer<Oficina>() {
            @Override
            public Object getDisplayValue(Oficina object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(Oficina object, int index) {
                return Integer.toString(index);
            }

            @Override
            public Oficina getObject(String id, IModel<? extends List<? extends Oficina>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));
    }
}
