package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import com.bisa.bus.servicios.asfi.infocred.api.DatosFinancierosService;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 * Created by gmercado on 18/11/2016.
 */
public class DatosFinancierosDao extends DaoImpl<DatosFinancierosDao, Long> implements Serializable, DatosFinancierosService {

    protected DatosFinancierosDao(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }
}
