/*
 * Copyright 2014 Banco Bisa S.A..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bisa.bus.servicios.asfi.infocred.ui;

import bus.env.api.MedioAmbiente;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static bus.env.api.Variables.*;


/**
 * @author Josue Montano
 */
public class ReSendMailWrapper {

    private static final Logger LOG = LoggerFactory.getLogger(ReSendMailWrapper.class);

    private final MedioAmbiente medioAmbiente;

    @Inject
    public ReSendMailWrapper(MedioAmbiente medioAmbiente) {
        this.medioAmbiente = medioAmbiente;
    }

    /**
     *
     *
     * @param subject    Asunto del email
     * @param message    Mensaje
     * @param correoList Lista de correos a los que se enviara el mensaje
     * @return
     */
    public String sendMessage(String subject, String message, List<String> correoList) {

        final String prop = System.getProperty("send.email");
        if (prop != null && !Boolean.parseBoolean(prop)) {
            LOG.info("Unable to send email, due to the proeprty {}", prop);
            return "* * * *  * * * * *  * * * * *  * * DUMMY Email sent!!!!";
        }
        SimpleEmail simpleEmail = new SimpleEmail();
        try {
            simpleEmail.setHostName(medioAmbiente.getValorDe(MAIL_HOST, MAIL_HOST_DEFAULT));
            simpleEmail.setFrom(medioAmbiente.getValorDe(MAIL_FROM, MAIL_FROM_DEFAULT), medioAmbiente.getValorDe(MAIL_FROM_NAME, MAIL_FROM_NAME_DEFAULT));
            simpleEmail.setSubject("[BISA] " + subject);

            for (String email : correoList) {
                simpleEmail.addTo(email);
            }
            simpleEmail.setMsg(message);
            return simpleEmail.send();
        } catch (EmailException e) {
            LOG.warn("Correo no enviado correctamente, el mesaje a enviar es: " + message, e);
        }
        return null;
    }

    /**
     * Envia correo con estructura HTML
     * @param subject
     * @param message
     * @param correoList
     * @return
     */
    public boolean sendHtmlMessage(String subject, String message, List<String> correoList) {
        return sendHtmlMessage(subject, message, null, correoList);
    }
    public boolean sendHtmlMessage(String subject, String message, String urlImage, List<String> correoList) {

        MimeMultipart multipart = new MimeMultipart();
        Properties properties = new Properties();
        properties.put("mail.smtp.host", medioAmbiente.getValorDe(MAIL_HOST, MAIL_HOST_DEFAULT));
        Session session = Session.getDefaultInstance(properties, null);
        session.setDebug(false);
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(medioAmbiente.getValorDe(MAIL_FROM, MAIL_FROM_DEFAULT), medioAmbiente.getValorDe(MAIL_FROM_NAME, MAIL_FROM_NAME_DEFAULT)));
            msg.setRecipients(Message.RecipientType.TO, correoList.get(0));
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            StringBuffer body = new StringBuffer(message);

            MimeBodyPart mbp = new MimeBodyPart();
            mbp.setContent(body.toString(), "text/html");
            multipart.addBodyPart(mbp);
            // Esto por si se quiere agregar imagen
            if (!Strings.isNullOrEmpty(urlImage)) {
                mbp = new MimeBodyPart();
                DataSource fds = new FileDataSource(urlImage);
                mbp.setDataHandler(new DataHandler(fds));
                mbp.setHeader("Content-ID", "<image>");
                mbp.setDisposition(MimeBodyPart.INLINE);
                // add image to the multipart
                multipart.addBodyPart(mbp);
                multipart.addBodyPart(mbp);
            }
            msg.setContent(multipart);
            Transport.send(msg);
            return true;
        } catch (MessagingException e) {
            LOG.error("Correo: :No se pudo enviar el correo", e);
        } catch (Exception e) {
            LOG.error("Correo: inesperado al momento de enviar correo", e);
        }
        return false;
    }
}