package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.database.model.EntityBase;
import bus.env.api.Variables;
import bus.plumbing.utils.FormatosUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Gary Mercado
 * Yrag knup
 */
@Entity
@Table(name = "IFP040")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I40FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I40USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I40FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I40USRMOD", columnDefinition = "CHAR(100)"))
})
public class InfocredCarteraPipe extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I40ID", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "I40CABECER")
    private Long idCabecera;

    @Column(name = "I40NOMCOM")
    private String nombreCompleto;

    @Column(name = "I40DOCUMEN")
    private String documento;

    @Column(name = "I40NRODOC")
    private String numeroDocumento;

    @Column(name = "I40EXTEN")
    private String extension;

    @Column(name = "I40FECNAC")
    private Date fechaNacimiento;

    @Column(name = "I40SCORE")
    private String score;

    @Column(name = "I40DIRECC")
    private String direccion;

    @Column(name = "I40LOCAL")
    private String localidad;

    @Column(name = "I40CANTON")
    private String canton;

    @Column(name = "I40SECCION")
    private String seccion;

    @Column(name = "I40PROVINC")
    private String provincia;

    @Column(name = "I40DEPTO")
    private String departamento;

    @Column(name = "I40FECDEC")
    @Temporal(TemporalType.DATE)
    private Date fechaDeclaracion;

    @Column(name = "I40ENTIDAD")
    private String entidadSBEF;

    @Column(name = "I40SIGLA")
    private String siglaSBEF;

    @Column(name = "I40CREDITO")
    private String creditoSBEF;

    @Column(name = "I40TIPOBLI")
    private String tipoObligadoSBEF;

    @Column(name = "I40VIGENTE", scale = 15, precision = 2)
    private BigDecimal vigente;

    @Column(name = "I40VENCIDO", scale = 15, precision = 2)
    private BigDecimal vencido;

    @Column(name = "I40EJECU", scale = 15, precision = 2)
    private BigDecimal ejecucion;

    @Column(name = "I40CONTIN", scale = 15, precision = 2)
    private BigDecimal contingente;

    @Column(name = "I40CASTIG", scale = 15, precision = 2)
    private BigDecimal castigado;

    @Column(name = "I40OTRCRED")
    private String otrosCreditos;

    @Column(name = "I40EJESBEF")
    private String ejecucionSBEF;

    @Column(name = "I40HISTORI")
    private String historico;

    @Column(name = "I40FECHA")
    private Date fecha;

    public InfocredCarteraPipe() {
    }

    public InfocredCarteraPipe(Long idCabecera, String[] columna) {
        this.idCabecera = idCabecera;
        this.nombreCompleto = columna[0];
        this.documento = columna[1];
        this.numeroDocumento = columna[2];
        this.extension = columna[3];
        this.fechaNacimiento = FormatosUtils.deDDMMYYYYaFecha(columna[4]);
        this.score = columna[5];
        this.direccion = columna[6];
        this.localidad = columna[7];
        this.canton = columna[8];
        this.seccion = columna[9];
        this.provincia = columna[10];
        this.departamento = columna[11];
        this.fechaDeclaracion = FormatosUtils.deDDMMYYYYaFecha(columna[12]);
        this.entidadSBEF = columna[13];
        this.siglaSBEF = columna[14];
        this.creditoSBEF = columna[15];
        this.tipoObligadoSBEF = columna[16];
        this.vigente = FormatosUtils.stringToBigDecimal(columna[17]);
        this.vencido = FormatosUtils.stringToBigDecimal(columna[18]);
        this.ejecucion = FormatosUtils.stringToBigDecimal(columna[19]);
        this.contingente = FormatosUtils.stringToBigDecimal(columna[20]);
        this.castigado = FormatosUtils.stringToBigDecimal(columna[21]);
        this.otrosCreditos = columna[22];
        this.ejecucionSBEF = columna[23];
        this.historico = columna[24];
        this.fecha = FormatosUtils.deDDMMYYYYaFecha(columna[25]);
        super.usuarioCreador = Variables.INFOCRED_USUARIO_ALTA_JOBS;
        super.fechaCreacion = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCabecera() {
        return idCabecera;
    }

    public void setIdCabecera(Long idCabecera) {
        this.idCabecera = idCabecera;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Date getFechaDeclaracion() {
        return fechaDeclaracion;
    }

    public void setFechaDeclaracion(Date fechaDeclaracion) {
        this.fechaDeclaracion = fechaDeclaracion;
    }

    public String getEntidadSBEF() {
        return entidadSBEF;
    }

    public void setEntidadSBEF(String entidadSBEF) {
        this.entidadSBEF = entidadSBEF;
    }

    public String getSiglaSBEF() {
        return siglaSBEF;
    }

    public void setSiglaSBEF(String siglaSBEF) {
        this.siglaSBEF = siglaSBEF;
    }

    public String getCreditoSBEF() {
        return creditoSBEF;
    }

    public void setCreditoSBEF(String creditoSBEF) {
        this.creditoSBEF = creditoSBEF;
    }

    public String getTipoObligadoSBEF() {
        return tipoObligadoSBEF;
    }

    public void setTipoObligadoSBEF(String tipoObligadoSBEF) {
        this.tipoObligadoSBEF = tipoObligadoSBEF;
    }

    public BigDecimal getVigente() {
        return vigente;
    }

    public void setVigente(BigDecimal vigente) {
        this.vigente = vigente;
    }

    public BigDecimal getVencido() {
        return vencido;
    }

    public void setVencido(BigDecimal vencido) {
        this.vencido = vencido;
    }

    public BigDecimal getEjecucion() {
        return ejecucion;
    }

    public void setEjecucion(BigDecimal ejecucion) {
        this.ejecucion = ejecucion;
    }

    public BigDecimal getContingente() {
        return contingente;
    }

    public void setContingente(BigDecimal contingente) {
        this.contingente = contingente;
    }

    public BigDecimal getCastigado() {
        return castigado;
    }

    public void setCastigado(BigDecimal castigado) {
        this.castigado = castigado;
    }

    public String getOtrosCreditos() {
        return otrosCreditos;
    }

    public void setOtrosCreditos(String otrosCreditos) {
        this.otrosCreditos = otrosCreditos;
    }

    public String getEjecucionSBEF() {
        return ejecucionSBEF;
    }

    public void setEjecucionSBEF(String ejecucionSBEF) {
        this.ejecucionSBEF = ejecucionSBEF;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "InfocredCarteraPipe{" +
                "id=" + id +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                ", documento='" + documento + '\'' +
                ", numeroDocumento='" + numeroDocumento + '\'' +
                ", extension='" + extension + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", score='" + score + '\'' +
                ", direccion='" + direccion + '\'' +
                ", localidad='" + localidad + '\'' +
                ", canton='" + canton + '\'' +
                ", seccion='" + seccion + '\'' +
                ", provincia='" + provincia + '\'' +
                ", departamento='" + departamento + '\'' +
                ", fechaDeclaracion=" + fechaDeclaracion +
                ", entidadSBEF='" + entidadSBEF + '\'' +
                ", siglaSBEF='" + siglaSBEF + '\'' +
                ", creditoSBEF='" + creditoSBEF + '\'' +
                ", tipoObligadoSBEF='" + tipoObligadoSBEF + '\'' +
                ", vigente=" + vigente +
                ", vencido=" + vencido +
                ", ejecucion=" + ejecucion +
                ", contingente=" + contingente +
                ", castigado=" + castigado +
                ", otrosCreditos='" + otrosCreditos + '\'' +
                ", ejecucionSBEF='" + ejecucionSBEF + '\'' +
                ", historico='" + historico + '\'' +
                ", fecha=" + fecha +
                '}';
    }
}