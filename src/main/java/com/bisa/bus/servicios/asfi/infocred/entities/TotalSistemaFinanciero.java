/*
 * Copyright 2016 Banco Bisa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@Entity
@Table(name = "IFP022")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I22FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I22USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I22FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I22USRMOD", columnDefinition = "CHAR(100)"))
})
public class TotalSistemaFinanciero extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private PKTotalSistemaFinanciero id;

    @Column(name = "I22ORDEN")
    private BigInteger orden;

    @Column(name = "I22DESCRIP")
    private String descripcion;

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "I22SALDO")
    private BigDecimal saldo;

    public TotalSistemaFinanciero() {
    }

    public TotalSistemaFinanciero(PKTotalSistemaFinanciero id, BigInteger orden, String descripcion, BigDecimal saldo) {
        this.id = id;
        this.orden = orden;
        this.descripcion = descripcion;
        this.saldo = saldo;
    }

    public TotalSistemaFinanciero(bus.consumoweb.infocred.objetos.TotalSistemaFinancieroFila totalSistemaFinanciero,
                                  InformeConfidencial informeConfidencial) {
        this.id = new PKTotalSistemaFinanciero(informeConfidencial.getId(), Long.valueOf(totalSistemaFinanciero.getFila()));
        this.orden = FormatosUtils.stringToBigInteger(totalSistemaFinanciero.getOrden());
        this.descripcion = totalSistemaFinanciero.getDescripcion();
        this.saldo = FormatosUtils.stringToBigDecimal(totalSistemaFinanciero.getSaldo());
        this.fechaCreacion = informeConfidencial.getFechaCreacion();
        this.usuarioCreador = informeConfidencial.getUsuarioCreador();
    }

    public PKTotalSistemaFinanciero getId() {
        return id;
    }

    public void setId(PKTotalSistemaFinanciero id) {
        this.id = id;
    }

    public BigInteger getOrden() {
        return orden;
    }

    public void setOrden(BigInteger orden) {
        this.orden = orden;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "TotalSistemaFinanciero{" +
                "id=" + id +
                ", orden=" + orden +
                ", descripcion='" + descripcion + '\'' +
                ", saldo=" + saldo +
                '}';
    }
}
