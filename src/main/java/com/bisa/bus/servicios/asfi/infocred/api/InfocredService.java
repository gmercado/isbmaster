package com.bisa.bus.servicios.asfi.infocred.api;

import com.bisa.bus.servicios.asfi.infocred.entities.Titulares;
import com.bisa.bus.servicios.asfi.infocred.model.RespuestaConsulta;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;

import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public interface InfocredService {

    List<Titulares> obtenerDatosTitularPorNombre(SolicitudConsultaTitularInfocredForm request);

    RespuestaConsulta getInfoCrediticioTitular(final SolicitudConsultaTitularInfocredForm request);
}
