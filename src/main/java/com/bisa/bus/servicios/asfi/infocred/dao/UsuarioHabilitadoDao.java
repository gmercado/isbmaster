package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.asfi.infocred.api.UsuarioHabilitadoService;
import com.bisa.bus.servicios.asfi.infocred.entities.UsuarioHabilitado;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.*;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class UsuarioHabilitadoDao extends DaoImpl<UsuarioHabilitado, Long> implements Serializable, UsuarioHabilitadoService {

    private static final Logger log = LoggerFactory.getLogger(UsuarioHabilitadoDao.class);

    @Inject
    protected UsuarioHabilitadoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public List<UsuarioHabilitado> buscarPorLogin(String login) {
        return doWithTransaction((entityManager, t) -> {
            log.info("Buscando al usuario {} ", login);
            String query = "SELECT a FROM UsuarioHabilitado a WHERE lower(a.login) = :login";
            Query q = entityManager.createQuery(query);
            q.setParameter("login", login.toLowerCase());
            return q.getResultList();
        });
    }

    @Override
    protected Path<Long> countPath(Root<UsuarioHabilitado> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<UsuarioHabilitado> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.<String>get("nombreCompleto"));
        return paths;
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<UsuarioHabilitado> from, UsuarioHabilitado example, UsuarioHabilitado example2) {
        List<Predicate> predicates = new LinkedList<>();
        String usuario = StringUtils.trimToNull(example.getLogin());
        if (usuario != null && example.getLogin() != null) {
            predicates.add(cb.equal(from.get("login"), usuario));
        }
        return predicates.toArray(new Predicate[predicates.size()]);
    }
}
