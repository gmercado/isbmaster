package com.bisa.bus.servicios.asfi.infocred.api;

import com.bisa.bus.servicios.asfi.infocred.entities.DatosPersonales;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;

import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public interface DatosPersonalesService {

    List<DatosPersonales> datosPersonalesPorDocumento(SolicitudConsultaTitularInfocredForm request);
}
