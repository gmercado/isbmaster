package com.bisa.bus.servicios.asfi.infocred.api;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class InfocredException extends RuntimeException {
    final String mensaje;

    public InfocredException(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getValidationErrors() {
        return mensaje;
    }
}
