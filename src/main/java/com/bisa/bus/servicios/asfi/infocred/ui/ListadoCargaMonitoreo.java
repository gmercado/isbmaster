package com.bisa.bus.servicios.asfi.infocred.ui;

import bus.database.components.FiltroPanel;
import bus.plumbing.components.AlertFeedbackPanel;
import bus.plumbing.components.ControlGroupBorder;
import com.bisa.bus.servicios.asfi.infocred.entities.AlertaCabecera;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.validation.validator.StringValidator;

/**
 * Created by gmercado on 18/01/2017.
 */
public class ListadoCargaMonitoreo extends FiltroPanel<AlertaCabecera> {

    public final AlertFeedbackPanel feedbackPanel = new AlertFeedbackPanel("feedback", false);

    protected ListadoCargaMonitoreo(String id, IModel<AlertaCabecera> model1, IModel<AlertaCabecera> model2) {
        super(id, model1, model2);

        RequiredTextField<String> titulo =
                new RequiredTextField<>("nombreArchivo", Model.of(model1.getObject().getTitulo()));
        titulo.add(StringValidator.minimumLength(4));
        titulo.add(StringValidator.maximumLength(50));
        titulo.setOutputMarkupId(true);
        //add(nombreCompleto);
        add(new ControlGroupBorder("nombreArchivo", titulo, feedbackPanel, Model.of("Nombre de archivo:")).add(titulo));
    }
}
