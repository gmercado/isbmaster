
package com.bisa.bus.servicios.asfi.infocred.consumer.stub;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIndividualReport2Result" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getIndividualReport2Result"
})
@XmlRootElement(name = "GetIndividualReport2Response")
public class GetIndividualReport2Response {

    @XmlElement(name = "GetIndividualReport2Result")
    protected String getIndividualReport2Result;

    /**
     * Obtiene el valor de la propiedad getIndividualReport2Result.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetIndividualReport2Result() {
        return getIndividualReport2Result;
    }

    /**
     * Define el valor de la propiedad getIndividualReport2Result.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetIndividualReport2Result(String value) {
        this.getIndividualReport2Result = value;
    }

}
