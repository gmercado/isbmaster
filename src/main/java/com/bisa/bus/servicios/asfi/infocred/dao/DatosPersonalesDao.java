package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.asfi.infocred.api.DatosPersonalesService;
import com.bisa.bus.servicios.asfi.infocred.entities.DatosPersonales;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class DatosPersonalesDao extends DaoImpl<DatosPersonales, Long> implements Serializable, DatosPersonalesService {

    private static final Logger log = LoggerFactory.getLogger(DatosPersonalesDao.class);

    @Inject
    protected DatosPersonalesDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    public List<DatosPersonales> datosPersonalesPorDocumento(SolicitudConsultaTitularInfocredForm request) {
        log.info("Obteniendo registro infocred por Numero Documento:{}", request.getTitular().getDocumento());
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<DatosPersonales> q = cb.createQuery(DatosPersonales.class);
                        Root<DatosPersonales> p = q.from(DatosPersonales.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("numeroDocumento"), StringUtils.trimToEmpty(request.getTitular().getDocumento())));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        q.orderBy(cb.desc(p.get("fechaCreacion")));
                        TypedQuery<DatosPersonales> query = entityManager.createQuery(q);
                        query.setMaxResults(1);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            log.error("Error en la consulta al obtener datos personales", e);
            return null;
        }
    }
}
