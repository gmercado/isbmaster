/*
 * Copyright 2016 Banco Bisa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 *
 * @author Gary Mercado
 * Yrag Knup
 */
@Entity
@Table(name = "IFP002")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I02FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I02USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I02FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I02USRMOD", columnDefinition = "CHAR(100)"))
})
public class DatosPersonales extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "I02ID", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Size(max = 50)
    @Column(name = "I02NOMCOMP")
    private String nombreCompleto;

    @Size(max = 20)
    @Column(name = "I02TIPDOC")
    private String tipoDocumento;

    @Size(max = 15)
    @Column(name = "I02NRODOC")
    private String numeroDocumento;

    @Size(max = 3)
    @Column(name = "I02EXTENS")
    private String extension;

    @Size(max = 4)
    @Column(name = "I02TIPPER")
    private String tipoPersona;

    @Size(max = 15)
    @Column(name = "I02ESTCIV")
    private String estadoCivil;

    @Column(name = "I02FECNAC")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;

//    @Column(name = "i02fecsgip")
//    @Temporal(TemporalType.DATE)
//    private Date i02fecsgip;

//    @Size(max = 3)
//    @Column(name = "i02datsgip")
//    private String i02datsgip;

    @Column(name = "I02SCORE")
    private BigInteger score;

    @Size(max = 50)
    @Column(name = "I02SCRDES")
    private String scoreDescripcion;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "DOMICILIO", nullable = false, insertable = false, updatable = false)
    private Set<Domicilio> domicilios = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "FINANCIERO", nullable = false, insertable = false, updatable = false)
    private Set<DatosFinancieros> datosFinancieros = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "TOTALFINANCIERO", nullable = false, insertable = false, updatable = false)
    private Set<TotalSistemaFinanciero> totalSistemaFinancieros = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "SISFINANCIERO", nullable = false, insertable = false, updatable = false)
    private Set<SistemaFinancieroDescripcion> sistemaFinancieroDescripciones = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "CUENTACORRIENTE", nullable = false, insertable = false, updatable = false)
    private Set<CuentasCorrientes> cuentasCorrientes = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "AFPS", nullable = false, insertable = false, updatable = false)
    private Set<DeudasAFP> deudasAFPs = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "CASASCOMERCIALES", nullable = false, insertable = false, updatable = false)
    private Set<CasasComerciales> casasComerciales = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "RECTIFICACIONES", nullable = false, insertable = false, updatable = false)
    private Set<Rectificacion> rectificaciones = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "SALDOSDEUDA", nullable = false, insertable = false, updatable = false)
    private Set<SaldosPersonaDeuda> saldosPersonaDeudas = new HashSet<>();

    public DatosPersonales() {
    }

    public DatosPersonales(String nombreCompleto, String tipoDocumento, String numeroDocumento, String extension,
                           String tipoPersona, String estadoCivil, Date fechaNacimiento, BigInteger score, String scoreDescripcion) {
        this.nombreCompleto = nombreCompleto;
        this.tipoDocumento = tipoDocumento;
        this.numeroDocumento = numeroDocumento;
        this.extension = extension;
        this.tipoPersona = tipoPersona;
        this.estadoCivil = estadoCivil;
        this.fechaNacimiento = fechaNacimiento;
        this.score = score;
        this.scoreDescripcion = scoreDescripcion;
    }

    public DatosPersonales(bus.consumoweb.infocred.objetos.DatosPersonales datosPersonales, InformeConfidencial informeConfidencial) {
        this.id = informeConfidencial.getId();
        this.nombreCompleto = datosPersonales.getDatosGenerales().getSubDatosGenerales().getNombreCompleto();
        this.tipoDocumento = datosPersonales.getDatosGenerales().getSubDatosGenerales().getTipoDocumento();
        this.numeroDocumento = datosPersonales.getDatosGenerales().getSubDatosGenerales().getNroDocumento();
        this.extension = datosPersonales.getDatosGenerales().getSubDatosGenerales().getExt();
        this.tipoPersona = datosPersonales.getDatosGenerales().getSubDatosGenerales().getTipoPersona();
        this.estadoCivil = datosPersonales.getDatosGenerales().getSubDatosGenerales().getEstadoCivil();
        this.fechaNacimiento = FormatosUtils.deDDMMYYYYaFecha(datosPersonales.getDatosGenerales().getSubDatosGenerales().getFechaNacFechaDef());
        this.fechaCreacion = informeConfidencial.getFechaCreacion();
        this.usuarioCreador = informeConfidencial.getUsuarioCreador();
        if (datosPersonales != null && datosPersonales.getScore() != null && datosPersonales.getScore().getSubScores() != null
                && datosPersonales.getScore().getSubScores().size() > 0) {
            this.score = new BigInteger(datosPersonales.getScore().getSubScores().get(0).getScore());
            this.scoreDescripcion = datosPersonales.getScore().getSubScores().get(0).getDescripcion();
        }

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public BigInteger getScore() {
        return score;
    }

    public void setScore(BigInteger score) {
        this.score = score;
    }

    public String getScoreDescripcion() {
        return scoreDescripcion;
    }

    public void setScoreDescripcion(String scoreDescripcion) {
        this.scoreDescripcion = scoreDescripcion;
    }

    public Set<Domicilio> getDomicilios() {
        return domicilios;
    }

    public void setDomicilios(Set<Domicilio> domicilios) {
        this.domicilios = domicilios;
    }

    public Set<DatosFinancieros> getDatosFinancieros() {
        return datosFinancieros;
    }

    public void setDatosFinancieros(Set<DatosFinancieros> datosFinancieros) {
        this.datosFinancieros = datosFinancieros;
    }

    public Set<TotalSistemaFinanciero> getTotalSistemaFinancieros() {
        return totalSistemaFinancieros;
    }

    public void setTotalSistemaFinancieros(Set<TotalSistemaFinanciero> totalSistemaFinancieros) {
        this.totalSistemaFinancieros = totalSistemaFinancieros;
    }

    public Set<SistemaFinancieroDescripcion> getSistemaFinancieroDescripciones() {
        return sistemaFinancieroDescripciones;
    }

    public void setSistemaFinancieroDescripciones(Set<SistemaFinancieroDescripcion> sistemaFinancieroDescripciones) {
        this.sistemaFinancieroDescripciones = sistemaFinancieroDescripciones;
    }

    public Set<CuentasCorrientes> getCuentasCorrientes() {
        return cuentasCorrientes;
    }

    public void setCuentasCorrientes(Set<CuentasCorrientes> cuentasCorrientes) {
        this.cuentasCorrientes = cuentasCorrientes;
    }

    public Set<SaldosPersonaDeuda> getSaldosPersonaDeudas() {
        return saldosPersonaDeudas;
    }

    public void setSaldosPersonaDeudas(Set<SaldosPersonaDeuda> saldosPersonaDeudas) {
        this.saldosPersonaDeudas = saldosPersonaDeudas;
    }

    public Set<DeudasAFP> getDeudasAFPs() {
        return deudasAFPs;
    }

    public void setDeudasAFPs(Set<DeudasAFP> deudasAFPs) {
        this.deudasAFPs = deudasAFPs;
    }

    public Set<CasasComerciales> getCasasComerciales() {
        return casasComerciales;
    }

    public void setCasasComerciales(Set<CasasComerciales> casasComerciales) {
        this.casasComerciales = casasComerciales;
    }

    public Set<Rectificacion> getRectificaciones() {
        return rectificaciones;
    }

    public void setRectificaciones(Set<Rectificacion> rectificaciones) {
        this.rectificaciones = rectificaciones;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatosPersonales)) {
            return false;
        }
        DatosPersonales other = (DatosPersonales) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DatosPersonales{" +
                "id=" + id +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                ", tipoDocumento='" + tipoDocumento + '\'' +
                ", numeroDocumento='" + numeroDocumento + '\'' +
                ", extension='" + extension + '\'' +
                ", tipoPersona='" + tipoPersona + '\'' +
                ", estadoCivil='" + estadoCivil + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", score=" + score +
                ", scoreDescripcion='" + scoreDescripcion + '\'' +
                '}';
    }
}
