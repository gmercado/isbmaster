package com.bisa.bus.servicios.asfi.infocred.api;

import com.bisa.bus.servicios.asfi.infocred.entities.DatosPersonales;
import com.bisa.bus.servicios.asfi.infocred.entities.InformeConfidencial;
import com.bisa.bus.servicios.asfi.infocred.entities.Titulares;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;

import java.sql.Connection;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public interface InformeConfidencialService {

    DatosPersonales obtenerRegistroDeBaseDatos(SolicitudConsultaTitularInfocredForm request);

    InformeConfidencial registrarInformeCofidencial(String response, SolicitudConsultaTitularInfocredForm request);

    Connection unWrapConnection();

    InformeConfidencial getInformeConfidencial(Long id);
}
