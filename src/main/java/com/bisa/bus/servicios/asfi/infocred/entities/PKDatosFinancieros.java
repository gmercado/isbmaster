package com.bisa.bus.servicios.asfi.infocred.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 *
 * @author Gary Mercado
 * Yrag Knup
 */
@Embeddable
public class PKDatosFinancieros implements Serializable {

    @Column(name = "I03ID")
    private Long id;

    @Column(name = "I03FILA")
    private Long fila;

    public PKDatosFinancieros() {
    }

    public PKDatosFinancieros(Long id, Long fila) {
        this.id = id;
        this.fila = fila;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PKDatosFinancieros that = (PKDatosFinancieros) o;

        if (!id.equals(that.id)) return false;
        return fila.equals(that.fila);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + fila.hashCode();
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFila() {
        return fila;
    }

    public void setFila(Long fila) {
        this.fila = fila;
    }
}
