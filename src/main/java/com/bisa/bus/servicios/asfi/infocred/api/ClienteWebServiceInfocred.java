package com.bisa.bus.servicios.asfi.infocred.api;

import bus.config.dao.CryptUtils;
import bus.consumoweb.dao.ConsumoWebDao;
import bus.consumoweb.entities.ConsumoWeb;
import bus.consumoweb.infocred.utilitarios.LogInterceptorHandler;
import bus.consumoweb.infocred.utilitarios.WSSecurityHeaderSOAPHandler;
import bus.database.dao.Dao;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.utils.Watch;
import com.bisa.bus.servicios.asfi.infocred.consumer.stub.IIndividualReportServiceContract;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.Handler;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

import static bus.plumbing.utils.Watches.CONSUMO_WS;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class ClienteWebServiceInfocred {

    protected final MedioAmbiente medioAmbiente;
    private final ConsumoWebDao consumoWebDao;
    private final CryptUtils cryptUtils;

    public enum MetodoWebservice { INDIVIDUAL_REPORT, TITULAR_LIST_BY_NAME;}

    @Inject
    public ClienteWebServiceInfocred(MedioAmbiente medioAmbiente, ConsumoWebDao consumoWebDao, CryptUtils cryptUtils) {
        this.consumoWebDao = consumoWebDao;
        this.medioAmbiente = medioAmbiente;
        this.cryptUtils = cryptUtils;
    }

    protected final Logger log = LoggerFactory.getLogger(ClienteWebServiceInfocred.class);

    // Consumo metodo IndividualReport4
    public String consumo(SolicitudConsultaTitularInfocredForm request, MetodoWebservice metodo) {

        String infocred_usuario = medioAmbiente.getValorDe(Variables.INFOCRED_USUARIO_WEBSERVICE, Variables.INFOCRED_USUARIO_WEBSERVICE_DEFAULT);
        String infocred_password = medioAmbiente.getValorDe(Variables.INFOCRED_PASSWORD_WEBSERVICE, Variables.INFOCRED_PASSWORD_WEBSERVICE_DEFAULT);
        // verifica si esta encriptado
        if (cryptUtils.esCryptLegado(infocred_password)) {
            infocred_password = cryptUtils.dec(infocred_password);
        } else if (cryptUtils.esOpenssl(infocred_password)) {
            infocred_password = cryptUtils.decssl(infocred_password);
        }

        Future<ConsumoWeb> consumoWebFuture = null;
        Watch watch = new Watch(CONSUMO_WS);

        String cadena = "";
        int responseCode = 0;
        Client client = null;
        HTTPConduit http = null;
        try {
            watch.start();
            consumoWebFuture = registrarConsumo(request.toString());
            setProxy();

            JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
            factory.setServiceClass(IIndividualReportServiceContract.class);
            factory.setAddress(medioAmbiente.getValorDe(Variables.INFOCRED_END_POINT, Variables.INFOCRED_END_POINT_DEFAULT));
            IIndividualReportServiceContract port = (IIndividualReportServiceContract) factory.create();

            BindingProvider bindingProvider = ((BindingProvider) port);
            List<Handler> handlerChain = new ArrayList<>();
            handlerChain.add(new WSSecurityHeaderSOAPHandler(infocred_usuario, infocred_password));
            bindingProvider.getBinding().setHandlerChain(handlerChain);

            client = ClientProxy.getClient(port);
            http = (HTTPConduit) client.getConduit();
            TLSClientParameters tlsClientParameters = new TLSClientParameters();
            tlsClientParameters.setSSLSocketFactory(getFancyFactory());
            http.setTlsClientParameters(tlsClientParameters);

            /** Trace **/
            client.getOutInterceptors().add(new LogInterceptorHandler());

            String respuesta = null;
            if (MetodoWebservice.INDIVIDUAL_REPORT.equals(metodo)) {
                respuesta = port.getIndividualReport4(request.getTitular(), request.getUsuario());
            } else if (MetodoWebservice.TITULAR_LIST_BY_NAME.equals(metodo)) {
                respuesta = port.getTitularListByName(request.getUsuario(), request.getTitular().getNombreCompleto());
            }
            cadena = (respuesta != null) ? respuesta.toString() : "";
            responseCode = 200;
            port = null;
        } catch (WebServiceException e) {
            responseCode = 400;
            throw e;
        } catch (Exception e) {
            responseCode = 400;
            log.error("Ocurrio un error inesperado ", e);
        } finally {
            final int milis = (int) watch.stop();
            actualizaConsumo(cadena, responseCode, milis, consumoWebFuture);
            if (http != null) {
                http.close();
            }
            if (client != null) {
                client.destroy();
            }
        }
        return cadena;
    }

    private void setProxy() {
        String host = medioAmbiente.getValorDe(Variables.INFOCRED_HOST_PROXY, Variables.INFOCRED_HOST_PROXY_DEFAULT);
        if (Strings.isNullOrEmpty(host)) {
            log.info("No se tiene configurado una salida por PROXY");
            return;
        }
        String port = medioAmbiente.getValorDe(Variables.INFOCRED_PORT_PROXY, Variables.INFOCRED_PORT_PROXY_DEFAULT);
        String usuario = medioAmbiente.getValorDe(Variables.INFOCRED_USUARIO_PROXY, Variables.INFOCRED_USUARIO_PROXY_DEFAULT);
        String password = medioAmbiente.getValorDe(Variables.INFOCRED_PASSWORD_PROXY, Variables.INFOCRED_PASSWORD_PROXY_DEFAULT);

        Authenticator.setDefault(new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usuario, password.toCharArray());
            }
        });
        System.setProperty("http.proxyHost", host);
        System.setProperty("http.proxyPort", port);
        System.setProperty("https.proxyHost", host);
        System.setProperty("https.proxyPort", port);
    }

    private SSLSocketFactory getFancyFactory() throws KeyManagementException, NoSuchAlgorithmException {
        final SSLContext context = SSLContext.getInstance("TLS");

        TrustManagerFactory trustStoreManager = null;
        KeyManagerFactory keyeManagerFactory = null;
        String bandera = medioAmbiente.getValorDe(Variables.INFOCRED_SSL, Variables.INFOCRED_SSL_DEFAULT);
        if(Boolean.parseBoolean(bandera)) {
            //Para el caso que se especifique el cacerts del certificado
            String ruta = medioAmbiente.getValorDe(Variables.INFOCRED_RUTA_CACERTS_SSL, Variables.INFOCRED_RUTA_CACERTS_SSL_DEFAULT);
            String pass = medioAmbiente.getValorDe(Variables.INFOCRED_CLAVE_CACERTS_SSL, Variables.INFOCRED_CLAVE_CACERTS_SSL_DEFAULT);
            // verifica si esta encriptado
            if (cryptUtils.esCryptLegado(pass)) {
                pass = cryptUtils.dec(pass);
            } else if (cryptUtils.esOpenssl(pass)) {
                pass = cryptUtils.decssl(pass);
            }

            KeyStore trustStore = null;
            FileInputStream fis = null;
            System.setProperty("javax.net.ssl.keyStorePassword", pass);
            try {
                fis = new FileInputStream(ruta);
                trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
                trustStore.load(fis, pass.toCharArray());
                trustStoreManager = TrustManagerFactory.getInstance("SunX509");
                trustStoreManager.init(trustStore);
            } catch (Throwable e) {
                throw new IllegalStateException("Ha ocurrido un eror inesperado", e);
            } finally {
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException ign) {}
                }
            }
        }

        context.init(
                keyeManagerFactory!=null?keyeManagerFactory.getKeyManagers():null,
                trustStoreManager!=null?trustStoreManager.getTrustManagers():new X509TrustManager[]{new X509TrustManager(){
                    public void checkClientTrusted(X509Certificate[] chain,
                                                   String authType) throws CertificateException {}
                    public void checkServerTrusted(X509Certificate[] chain,
                                                   String authType) throws CertificateException {}
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }}}, new SecureRandom());

        return context.getSocketFactory();
    }

    public Future<ConsumoWeb> registrarConsumo(String solicitud) {
        final ConsumoWeb consumoWeb = new ConsumoWeb();
        Future<ConsumoWeb> consumoWebFuture;
        consumoWebFuture = getExecutor().submit(() -> {
            consumoWeb.setFecha(new Date());
            consumoWeb.setConsumidor(StringUtils.abbreviate("Infocred", 10));
            if (solicitud != null) {
                consumoWeb.setPregunta(solicitud);
            }
            getConsumoWebDao().persist(consumoWeb);
            log.debug("Registrando el consumo de WS.");
            return consumoWeb;
        });
        return consumoWebFuture;
    }

    public void actualizaConsumo(String respuesta, int codHttp, Integer milis, Future<ConsumoWeb> future) {
        if (future != null) {
            final Future<ConsumoWeb> finalConsumoWebFuture = future;
            final Integer finalStatusCode = codHttp;
            getExecutor().submit(() -> {
                try {
                    final ConsumoWeb consumoWeb = finalConsumoWebFuture.get(10, TimeUnit.SECONDS);
                    consumoWeb.setStatus(finalStatusCode);
                    consumoWeb.setMilis(milis);
                    if (respuesta != null) {
                        consumoWeb.setRespuesta(respuesta);
                    }
                    getConsumoWebDao().merge(consumoWeb);
                    log.debug("Actualizando el consumo de WS.");
                } catch (InterruptedException e) {
                    log.error("Error: Actualizacion de consumo interrumpido.", e);
                } catch (TimeoutException e) {
                    log.error("Error: La persistencia de consumo web tarda mucho.", e);
                } catch (Exception e) {
                    log.error("Error: Ha ocurrido un error inesperado", e);
                }
            });
        }
    }

    public ExecutorService getExecutor() {
        return Executors.newFixedThreadPool(2);
    }

    public Dao<ConsumoWeb, Long> getConsumoWebDao() {
        return consumoWebDao;
    }
}