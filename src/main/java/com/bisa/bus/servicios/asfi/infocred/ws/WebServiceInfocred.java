package com.bisa.bus.servicios.asfi.infocred.ws;

import com.bisa.bus.servicios.asfi.infocred.entities.Titulares;
import com.bisa.bus.servicios.asfi.infocred.model.RespuestaConsulta;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@WebService(
        targetNamespace = WebServiceInfocred.NAMESPACE,
        name = "WebServiceInfocred")
public interface WebServiceInfocred {
    String NAMESPACE = "http://www.bisa.com/ebanking/services/soap";

    @WebMethod(operationName = "consultaDatosTitular", exclude = false)
    @WebResult(name = "ConsultaDatosTitularResult", targetNamespace = NAMESPACE)
    List<Titulares> consultaDatosTitular(@WebParam(name = "requestConsultaDatosTitular") SolicitudConsultaTitularInfocredForm request);

    @WebMethod(operationName = "consultaTitular", exclude = false)
    @WebResult(name = "ConsultaTitularResult", targetNamespace = NAMESPACE)
    RespuestaConsulta consultarTitular(@WebParam(name = "requestConsultaTitular") SolicitudConsultaTitularInfocredForm request);
}
