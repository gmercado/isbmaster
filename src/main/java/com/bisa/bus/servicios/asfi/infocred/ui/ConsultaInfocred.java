package com.bisa.bus.servicios.asfi.infocred.ui;

import bus.consumoweb.enums.CodigoRespuesta;
import bus.consumoweb.enums.TipoAceptacion;
import bus.consumoweb.enums.TipoEstadoUsuario;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.ControlGroupBorder;
import com.bisa.bus.servicios.asfi.infocred.api.InfocredService;
import com.bisa.bus.servicios.asfi.infocred.api.UsuarioHabilitadoService;
import com.bisa.bus.servicios.asfi.infocred.consumer.stub.Titular;
import com.bisa.bus.servicios.asfi.infocred.consumer.stub.Usuario;
import com.bisa.bus.servicios.asfi.infocred.entities.Titulares;
import com.bisa.bus.servicios.asfi.infocred.entities.UsuarioHabilitado;
import com.bisa.bus.servicios.asfi.infocred.model.RespuestaConsulta;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;
import com.google.common.base.Splitter;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.Component;
import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static bus.users.api.Metadatas.OFICINA_META_DATA_KEY;
import static bus.users.api.Metadatas.USER_META_DATA_KEY;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@AuthorizeInstantiation({RolesBisa.INFOCRED_ADMIN, RolesBisa.INFOCRED_USUARIO})
@Menu(value = "Consulta Infocred", subMenu = SubMenu.INFOCRED)
public class ConsultaInfocred extends BisaWebPage {

    private IModel<Titulares> objectModel;

    private String respuesta;
    private String usuarioLogin;
    private TipoDocumentos documentos;

    private static final Logger log = LoggerFactory.getLogger(ConsultaInfocred.class);

    @Inject
    private MedioAmbiente medioAmbiente;
    @Inject
    private InfocredService infocredService;
    @Inject
    private UsuarioHabilitadoService usuarioHabilitadoService;

    public ConsultaInfocred() {
        inicio(new Titulares());
    }

    public ConsultaInfocred(Titulares titular) {
        inicio(titular);
    }

    public void inicio(Titulares titular) {
        // Declarando componentes
        this.objectModel = new CompoundPropertyModel<>(titular);
        Form form = new Form("consultaTitular", objectModel);
        add(form);
        // Cargamos al usurio en sesion
        usuarioLogin = getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase();
        //Número Documento
        RequiredTextField<String> documento = new RequiredTextField<>("documento");
        documento.setOutputMarkupId(true);
        form.add(new ControlGroupBorder("documento", documento, feedbackPanel, Model.of("N\u00famero Documento:")).add(documento));

        DropDownChoice tipoDocumento = new DropDownChoice<>("tipoDocumento", new PropertyModel<>(this, "documentos"),
                new LoadableDetachableModel<List<? extends TipoDocumentos>>() {
                    @Override
                    protected List<? extends TipoDocumentos> load() {
                        return tipoDocumentos();
                    }
                }, new IChoiceRenderer<TipoDocumentos>() {
            @Override
            public Object getDisplayValue(TipoDocumentos object) {
                return object.getNombre();
            }

            @Override
            public String getIdValue(TipoDocumentos object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoDocumentos getObject(String id, IModel<? extends List<? extends TipoDocumentos>> choices) {
                if (org.apache.commons.lang.StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        });
        tipoDocumento.setRequired(true);
        tipoDocumento.setNullValid(false);
        tipoDocumento.setOutputMarkupId(false);
        form.add(new ControlGroupBorder("tipoDocumento", tipoDocumento, feedbackPanel, Model.of("Tipo Documento:")).add(tipoDocumento));

        final TextField<String> nombreCompleto =
                new TextField("nombreCompleto", new PropertyModel<>(this, "respuesta"));
        nombreCompleto.setOutputMarkupId(true);
        nombreCompleto.setEnabled(false);
        form.add(new ControlGroupBorder("nombreCompleto", nombreCompleto, feedbackPanel, Model.of("Nombre Completo:")).add(nombreCompleto));

        //Forzar consulta a Infocred sin revise base de datos local

        final Component forzarConsulta = new CheckBox("forzarConsulta", new PropertyModel<>(objectModel, "forzarConsulta"))
                .setLabel(Model.of("Forzar consulta")).setOutputMarkupId(true);
        form.add(new ControlGroupBorder("forzarConsulta", forzarConsulta, feedbackPanel, Model.of("Consulta directa:")).add(forzarConsulta));

        // Modal
        final ModalWindow modal1;
        form.add(modal1 = new ModalWindow("modal1"));
        modal1.setCookieName("modal-1");

        modal1.setPageCreator(new ModalWindow.PageCreator() {
            @Override
            public Page createPage() {
                return new ModalContent1Page(ConsultaInfocred.this.getPageReference(), modal1);
            }
        });

        modal1.setWindowClosedCallback(new ModalWindow.WindowClosedCallback() {
            @Override
            public void onClose(AjaxRequestTarget target) {
                forzarConsulta.setEnabled(false);
                target.add(forzarConsulta);
                target.add(nombreCompleto);
            }
        });

        //Boton consulta
        //form.add(new ButtonAuthorize("consulta", RolesBisa.INFOCRED_CONSUMO) {
        form.add(new AjaxButton("consulta") {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                log.info("Inicia la consulta del historial crediticio del Documento: {}, " +
                        "TipoDoc: {}," + "Forzar: {}", getObjetoModel().getDocumento(), documentos, getObjetoModel().isForzarConsulta());
                // Verificamos si el usuario fue registrado su login con el CI, esto por norma de INFOCRED
                List<UsuarioHabilitado> resultado = usuarioHabilitadoService.buscarPorLogin(usuarioLogin);
                if (resultado.size() == 0) {
                    getSession().info("Debes actualizar la informaci\u00F3 del usuario " + usuarioLogin + ", comunicate con el encargado.");
                    setResponsePage(new ConsultaInfocred());
                    return;
                }
                UsuarioHabilitado usuarioHabilitado = resultado.get(0);
                if (TipoEstadoUsuario.I.equals(usuarioHabilitado.getEstado())) {
                    getSession().info("El usuario esta inhabilitado para consultas a infocred, solicita la habilitaci\u00F3n");
                    setResponsePage(new ConsultaInfocred());
                    return;
                }
                if (getObjetoModel().isForzarConsulta() && TipoAceptacion.F.equals(usuarioHabilitado.getConsultaDirecta())) {
                    getSession().info("El usuario no tiene permiso para realizar consulta directa a Infocred");
                    setResponsePage(new ConsultaInfocred());
                    return;
                }
                log.info("El usuario {} con CI {} solicito consulta INFOCRED", usuarioLogin , usuarioHabilitado.getNumeroDocumento());
                Usuario u = new Usuario(usuarioHabilitado.getNumeroDocumento(), usuarioLogin);
                Titular t = new Titular(Integer.parseInt(documentos.getValor()), getObjetoModel().getDocumento(), getRespuesta());

                SolicitudConsultaTitularInfocredForm request = new SolicitudConsultaTitularInfocredForm(u, t,
                        getSession().getMetaData(OFICINA_META_DATA_KEY).toUpperCase(), "ISB", getObjetoModel().isForzarConsulta());
                RespuestaConsulta response = infocredService.getInfoCrediticioTitular(request);
                // si tenemos mas de un resultado se debe desplegar un dialogo
                if (CodigoRespuesta.RESPUESTA_INFOCRED_DUPLICADO.equals(response.getCodError())) {
                    setRespuesta(response.getResponse());
                    modal1.show(target);
                    return;
                }
                if (response != null) {
                    setResponsePage(new RespuestaInfocred(request, response));
                } else {
                    getSession().error("No se pudo consultar al titular");
                }
            }
        });

        form.add(new Button("limpiar") {
            @Override
            public void onSubmit() {
                if ((StringUtils.isNotBlank(getObjetoModel().getDocumento()) && StringUtils.isNotEmpty(getObjetoModel().getDocumento()))
                        || (StringUtils.isNotBlank(getObjetoModel().getExtension()) && StringUtils.isNotEmpty(getObjetoModel().getExtension()))) {
                    getObjetoModel().setDocumento(null);
                    getObjetoModel().setExtension(null);
                    getObjetoModel().setNombreCompleto(null);
                    setResponsePage(new ConsultaInfocred());
                }
            }
        }.setDefaultFormProcessing(false));
    }

    public List<TipoDocumentos> tipoDocumentos() {
        String tipoDocumentosVar = medioAmbiente.getValorDe(Variables.INFOCRED_LISTA_TIPO_DOCUMENTOS, Variables.INFOCRED_LISTA_TIPO_DOCUMENTOS_DEFAULT);
        List<TipoDocumentos> listaDocumentos = new ArrayList<>();
        List<String> documentos = Splitter.on("|").splitToList(tipoDocumentosVar);
        for (int i = 0; i < documentos.size(); i = i + 2) {
            int valor = i + 1;
            TipoDocumentos td = new TipoDocumentos(documentos.get(i), documentos.get(valor));
            listaDocumentos.add(td);
        }
        return listaDocumentos;
    }

    private Titulares getObjetoModel() {
        return this.objectModel.getObject();
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public class TipoDocumentos implements Serializable {
        private String nombre;
        private String valor;

        public TipoDocumentos(String nombre, String valor) {
            this.nombre = nombre;
            this.valor = valor;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getValor() {
            return valor;
        }

        public void setValor(String valor) {
            this.valor = valor;
        }
    }
}
