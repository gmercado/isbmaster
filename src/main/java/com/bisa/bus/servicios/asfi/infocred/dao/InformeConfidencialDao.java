package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.consumoweb.enums.*;
import bus.consumoweb.infocred.objetos.*;
import bus.consumoweb.infocred.utilitarios.XmlUtil;
import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.asfi.infocred.api.DatosPersonalesService;
import com.bisa.bus.servicios.asfi.infocred.api.InformeConfidencialService;
import com.bisa.bus.servicios.asfi.infocred.entities.*;
import com.bisa.bus.servicios.asfi.infocred.entities.CasasComerciales;
import com.bisa.bus.servicios.asfi.infocred.entities.CuentasCorrientes;
import com.bisa.bus.servicios.asfi.infocred.entities.DatosFinancieros;
import com.bisa.bus.servicios.asfi.infocred.entities.DatosPersonales;
import com.bisa.bus.servicios.asfi.infocred.entities.Domicilio;
import com.bisa.bus.servicios.asfi.infocred.entities.TotalSistemaFinanciero;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class InformeConfidencialDao extends DaoImpl<InformeConfidencial, Long> implements Serializable, InformeConfidencialService {

    private static final Logger log = LoggerFactory.getLogger(InformeConfidencialDao.class);

    private final DatosPersonalesService datosPersonalesService;

    @Inject
    protected InformeConfidencialDao(@BasePrincipal EntityManagerFactory entityManagerFactory, DatosPersonalesService datosPersonalesService) {
        super(entityManagerFactory);
        this.datosPersonalesService = datosPersonalesService;
    }

    public DatosPersonales obtenerRegistroDeBaseDatos(SolicitudConsultaTitularInfocredForm request) {
        try {
            List<DatosPersonales> estaEnBD = datosPersonalesService.datosPersonalesPorDocumento(request);
            if (estaEnBD != null && estaEnBD.size() > 0) {
                return estaEnBD.get(0);
            }
        } catch (Exception e) {
            log.error("Error al validar registro", e);
        }
        return null;
    }

    public InformeConfidencial registrarInformeCofidencial(String response, SolicitudConsultaTitularInfocredForm request) {
        log.debug("No existe en nuestra base, se procede a guardar ...");
        return doWithTransaction((entityManager, t) -> {
//        EntityManager entityManager = entityManagerFactory.createEntityManager();
//        entityManager.getTransaction().begin();
            try {
                InformeConfidencial informeConfidencial;
                DatosPersonales datosPersonales;
                Domicilio domicilio;

                DatosFinancieros datosFinancieros;
                TotalSistemaFinanciero totalSistemaFinanciero;
                SistemaFinancieroDescripcion sistemaFinancieroDescripcion;
                CuentasCorrientes cuentasCorrientes;
                DeudasAFP deudasAFP;
                CasasComerciales casasComerciales;
                Rectificacion rectificacion;
                SaldosPersonaDeuda saldosPersonaDeuda = null;

                //Marshall del xml de respuesta
                TitularInfoCred datosXml = XmlUtil.xmlToObject(response, TitularInfoCred.class);
                Cabecera cabecera = datosXml.getCabecera();

                if (datosXml.getConsultasRealizadas() != null) {
                    log.debug("Si existe respuesta el titular consultado ... {}", response);

                    informeConfidencial = new InformeConfidencial(cabecera, datosXml.getConsultasRealizadas().getSubConsultasRealizadas());
                    informeConfidencial.setFechaCreacion(new Date());
                    informeConfidencial.setUsuarioCreador(request.getUsuario().getNombreCompleto());
                    informeConfidencial = persist(informeConfidencial);

                    datosPersonales = new DatosPersonales(datosXml.getDatosPersonales(), informeConfidencial);

                    int fila = 1;
                    if (Utilitarios.validaObjetosNull(datosXml, Utilitarios.DOMICILIO)) {
                        for (SubDomicilio d : datosXml.getDatosPersonales().getDomicilio().getSubDomicilio()) {
                            domicilio = new Domicilio(d, informeConfidencial, fila);
                            datosPersonales.getDomicilios().add(domicilio);
                            fila++;
                        }
                    }

                    if (Utilitarios.validaObjetosNull(datosXml, Utilitarios.SIS_FINANCIERO)) {
                        for (SubSistemaFinancieroFila ssf : datosXml.getDatosFinancieros().getSistemaFinanciero().getSubSistemaFinanciero().getFilas()) {
                            datosFinancieros = new DatosFinancieros(ssf, informeConfidencial);
                            datosPersonales.getDatosFinancieros().add(datosFinancieros);
                        }
                    }

                    if (Utilitarios.validaObjetosNull(datosXml, Utilitarios.TOTAL_SIS_FINANCIERO)) {
                        for (TotalSistemaFinancieroFila tsf :
                                datosXml.getDatosFinancieros().getSistemaFinanciero().getTotalSistemaFinanciero().getFilas()) {
                            totalSistemaFinanciero = new TotalSistemaFinanciero(tsf, informeConfidencial);
                            datosPersonales.getTotalSistemaFinancieros().add(totalSistemaFinanciero);
                        }
                    }

                    if (Utilitarios.validaObjetosNull(datosXml, Utilitarios.DESCRIPCIONES)) {
                        for (DescripcionCaedecacteconoFila dc :
                                datosXml.getDatosFinancieros().getSistemaFinanciero().getDescripcionCaedecactecono().getFilas()) {
                            sistemaFinancieroDescripcion = new SistemaFinancieroDescripcion(dc, informeConfidencial);
                            datosPersonales.getSistemaFinancieroDescripciones().add(sistemaFinancieroDescripcion);
                        }
                    }

                    if (Utilitarios.validaObjetosNull(datosXml, Utilitarios.CUENTAS_CORRIENTES)) {
                        for (CuentasCorrientesFila dcf :
                                datosXml.getDatosFinancieros().getCuentasCorrientes().getSubCuentasCorrientes().getFilas()) {
                            cuentasCorrientes = new CuentasCorrientes(dcf, informeConfidencial);
                            datosPersonales.getCuentasCorrientes().add(cuentasCorrientes);
                        }
                    }

                    if (Utilitarios.validaObjetosNull(datosXml, Utilitarios.DEUDA_AFP)) {
                        for (SubAfpFila saf :
                                datosXml.getDatosFinancieros().getAfp().getSubAfp().getFilas()) {
                            deudasAFP = new DeudasAFP(saf, informeConfidencial);
                            datosPersonales.getDeudasAFPs().add(deudasAFP);
                        }
                    }

                    if (Utilitarios.validaObjetosNull(datosXml, Utilitarios.CASAS_COMERCIALES)) {
                        for (SubCasasComercialesFila sccf :
                                datosXml.getDatosFinancieros().getCasasComerciales().getSubCasasComerciales().getFilas()) {
                            casasComerciales = new CasasComerciales(sccf, informeConfidencial);
                            datosPersonales.getCasasComerciales().add(casasComerciales);
                        }
                    }

                    if (Utilitarios.validaObjetosNull(datosXml, Utilitarios.RECTIFICACIONES)) {
                        for (SubRectificacionesFila srf :
                                datosXml.getDatosFinancieros().getRectificaciones().getSubRectificaciones().getFilas()) {
                            rectificacion = new Rectificacion(srf, informeConfidencial);
                            datosPersonales.getRectificaciones().add(rectificacion);
                        }
                    }

                    if (Utilitarios.validaObjetosNull(datosXml, Utilitarios.PERSONA_DEUDAS_DIREC)) {
                        for (SaldosPersonaDeudaDirectaFila spddf :
                                datosXml.getDatosFinancieros().getFormatoHistorial().getSaldosPersonaDeudaDirecta().getFilas()) {
                            for (int i = 1; i <= 5; i++) {
                                switch (i) {
                                    case 1:
                                        saldosPersonaDeuda = new SaldosPersonaDeuda(informeConfidencial, TipoDeuda.DIRECTA,
                                                spddf.getFila(), spddf.getAbreviatura(), spddf.getEstado1().getGestion(), spddf.getEstado1().getEstado(), spddf.getSaldo1());
                                        break;
                                    case 2:
                                        saldosPersonaDeuda = new SaldosPersonaDeuda(informeConfidencial, TipoDeuda.DIRECTA,
                                                spddf.getFila(), spddf.getAbreviatura(), spddf.getEstado2().getGestion(), spddf.getEstado2().getEstado(), spddf.getSaldo2());
                                        break;
                                    case 3:
                                        saldosPersonaDeuda = new SaldosPersonaDeuda(informeConfidencial, TipoDeuda.DIRECTA,
                                                spddf.getFila(), spddf.getAbreviatura(), spddf.getEstado3().getGestion(), spddf.getEstado3().getEstado(), spddf.getSaldo3());
                                        break;
                                    case 4:
                                        saldosPersonaDeuda = new SaldosPersonaDeuda(informeConfidencial, TipoDeuda.DIRECTA,
                                                spddf.getFila(), spddf.getAbreviatura(), spddf.getEstado4().getGestion(), spddf.getEstado4().getEstado(), spddf.getSaldo4());
                                        break;
                                    case 5:
                                        saldosPersonaDeuda = new SaldosPersonaDeuda(informeConfidencial, TipoDeuda.DIRECTA,
                                                spddf.getFila(), spddf.getAbreviatura(), spddf.getEstado5().getGestion(), spddf.getEstado5().getEstado(), spddf.getSaldo5());
                                        break;
                                }
                                if (saldosPersonaDeuda != null) {
                                    datosPersonales.getSaldosPersonaDeudas().add(saldosPersonaDeuda);
                                }
                            }
                        }
                    }

                    if (Utilitarios.validaObjetosNull(datosXml, Utilitarios.PERSONA_DEUDAS_INDIREC)) {
                        for (SaldosPersonaDeudaIndirectaFila spdif :
                                datosXml.getDatosFinancieros().getFormatoHistorial().getSaldosPersonaDeudaIndirecta().getFilas()) {
                            for (int i = 1; i <= 5; i++) {
                                switch (i) {
                                    case 1:
                                        saldosPersonaDeuda = new SaldosPersonaDeuda(informeConfidencial, TipoDeuda.INDIRECTA,
                                                spdif.getFila(), spdif.getAbreviatura(), spdif.getEstado1().getGestion(), spdif.getEstado1().getEstado(), spdif.getSaldo1());
                                        break;
                                    case 2:
                                        saldosPersonaDeuda = new SaldosPersonaDeuda(informeConfidencial, TipoDeuda.INDIRECTA,
                                                spdif.getFila(), spdif.getAbreviatura(), spdif.getEstado2().getGestion(), spdif.getEstado2().getEstado(), spdif.getSaldo1());
                                        break;
                                    case 3:
                                        saldosPersonaDeuda = new SaldosPersonaDeuda(informeConfidencial, TipoDeuda.INDIRECTA,
                                                spdif.getFila(), spdif.getAbreviatura(), spdif.getEstado3().getGestion(), spdif.getEstado3().getEstado(), spdif.getSaldo1());
                                        break;
                                    case 4:
                                        saldosPersonaDeuda = new SaldosPersonaDeuda(informeConfidencial, TipoDeuda.INDIRECTA,
                                                spdif.getFila(), spdif.getAbreviatura(), spdif.getEstado4().getGestion(), spdif.getEstado4().getEstado(), spdif.getSaldo1());
                                        break;
                                    case 5:
                                        saldosPersonaDeuda = new SaldosPersonaDeuda(informeConfidencial, TipoDeuda.INDIRECTA,
                                                spdif.getFila(), spdif.getAbreviatura(), spdif.getEstado5().getGestion(), spdif.getEstado5().getEstado(), spdif.getSaldo1());
                                        break;
                                }
                                if (saldosPersonaDeuda != null) {
                                    datosPersonales.getSaldosPersonaDeudas().add(saldosPersonaDeuda);
                                }
                            }
                        }
                    }
                    entityManager.persist(datosPersonales);
                } else {
                    informeConfidencial = new InformeConfidencial(cabecera, null);
                }
                //entityManager.getTransaction().commit();
                return informeConfidencial;
            } catch (Exception e) {
                log.error("No se pudo persistir el nuevo registro se procede hacer rollback", e);
            }
            return null;
        });
    }

    public Connection unWrapConnection() {
        return getConnection();
    }

    @Override
    public InformeConfidencial getInformeConfidencial(Long id) {
        return find(id);
    }
}