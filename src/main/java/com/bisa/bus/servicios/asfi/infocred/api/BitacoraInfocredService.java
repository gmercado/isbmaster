package com.bisa.bus.servicios.asfi.infocred.api;

import com.bisa.bus.servicios.asfi.infocred.entities.BitacoraInfocred;
import com.bisa.bus.servicios.asfi.infocred.model.RespuestaConsulta;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;

import java.util.List;

/**
 * Created by gmercado on 17/02/2017.
 */
public interface BitacoraInfocredService {

    List<BitacoraInfocred> getConsumos(BitacoraInfocred model1, BitacoraInfocred model2);

    void registrar(SolicitudConsultaTitularInfocredForm request, RespuestaConsulta response);
}
