package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.consumoweb.infocred.objetos.TitularInfoCred;

/**
 * @Author Gary Mercado
 * Yrag Knup 07/12/2016.
 */
public class Utilitarios {

    public final static int DOMICILIO = 1;
    public final static int SIS_FINANCIERO = 2;
    public final static int TOTAL_SIS_FINANCIERO = 3;
    public final static int DESCRIPCIONES = 4;
    public final static int CUENTAS_CORRIENTES = 5;
    public final static int PERSONA_DEUDAS_DIREC = 6;
    public final static int PERSONA_DEUDAS_INDIREC = 7;
    public final static int DEUDA_AFP = 8;
    public final static int CASAS_COMERCIALES = 9;
    public final static int RECTIFICACIONES = 10;

    public static boolean validaObjetosNull (TitularInfoCred data, int objeto) {
        switch (objeto) {
            case DOMICILIO:
                if (data.getDatosPersonales() != null && data.getDatosPersonales().getDomicilio() != null
                        && data.getDatosPersonales().getDomicilio().getSubDomicilio() != null) {
                    return true;
                }
                return false;
            case SIS_FINANCIERO:
                if (data.getDatosFinancieros() != null && data.getDatosFinancieros().getSistemaFinanciero() != null
                        && data.getDatosFinancieros().getSistemaFinanciero().getSubSistemaFinanciero() != null
                        && data.getDatosFinancieros().getSistemaFinanciero().getSubSistemaFinanciero().getFilas() != null) {
                    return true;
                }
                return false;
            case TOTAL_SIS_FINANCIERO:
                if (data.getDatosFinancieros() != null && data.getDatosFinancieros().getSistemaFinanciero() != null &&
                        data.getDatosFinancieros().getSistemaFinanciero().getTotalSistemaFinanciero() != null &&
                        data.getDatosFinancieros().getSistemaFinanciero().getTotalSistemaFinanciero().getFilas() != null) {
                    return true;
                }
                return false;
            case DESCRIPCIONES:
                if (data.getDatosFinancieros() != null && data.getDatosFinancieros().getSistemaFinanciero() != null &&
                        data.getDatosFinancieros().getSistemaFinanciero().getDescripcionCaedecactecono() != null &&
                        data.getDatosFinancieros().getSistemaFinanciero().getDescripcionCaedecactecono().getFilas() != null) {
                    return true;
                }
                return false;
            case CUENTAS_CORRIENTES:
                if (data.getDatosFinancieros() != null &&
                        data.getDatosFinancieros().getCuentasCorrientes() != null &&
                        data.getDatosFinancieros().getCuentasCorrientes().getSubCuentasCorrientes() != null &&
                        data.getDatosFinancieros().getCuentasCorrientes().getSubCuentasCorrientes().getFilas() != null) {
                    return true;
                }
                return false;
            case PERSONA_DEUDAS_DIREC:
                if (data.getDatosFinancieros() != null && data.getDatosFinancieros().getFormatoHistorial() != null &&
                        data.getDatosFinancieros().getFormatoHistorial().getSaldosPersonaDeudaDirecta() != null &&
                        data.getDatosFinancieros().getFormatoHistorial().getSaldosPersonaDeudaDirecta().getFilas() != null) {
                    return true;
                }
                return false;
            case PERSONA_DEUDAS_INDIREC:
                if (data.getDatosFinancieros() != null && data.getDatosFinancieros().getFormatoHistorial() != null &&
                        data.getDatosFinancieros().getFormatoHistorial().getSaldosPersonaDeudaIndirecta() != null &&
                        data.getDatosFinancieros().getFormatoHistorial().getSaldosPersonaDeudaIndirecta().getFilas() != null) {
                    return true;
                }
                return false;

            case DEUDA_AFP:
                if (data.getDatosFinancieros() != null &&
                        data.getDatosFinancieros().getAfp() != null &&
                        data.getDatosFinancieros().getAfp().getSubAfp() != null &&
                        data.getDatosFinancieros().getAfp().getSubAfp().getFilas() != null) {
                    return true;
                }
                return false;

            case CASAS_COMERCIALES:
                if (data.getDatosFinancieros() != null &&
                        data.getDatosFinancieros().getCasasComerciales() != null &&
                        data.getDatosFinancieros().getCasasComerciales().getSubCasasComerciales() != null &&
                        data.getDatosFinancieros().getCasasComerciales().getSubCasasComerciales().getFilas() != null) {
                    return true;
                }
                return false;

            case RECTIFICACIONES:
                if (data.getDatosFinancieros() != null &&
                        data.getDatosFinancieros().getRectificaciones() != null &&
                        data.getDatosFinancieros().getRectificaciones().getSubRectificaciones() != null &&
                        data.getDatosFinancieros().getRectificaciones().getSubRectificaciones().getFilas() != null) {
                    return true;
                }
                return false;
        }
        return false;
    }
}
