/*
 * Copyright 2016 Banco Bisa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.consumoweb.infocred.objetos.Cabecera;
import bus.consumoweb.infocred.objetos.SubConsultasRealizadas;
import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 *
 * @author Gary Mercado
 * Yrag knup
 */
@Entity
@Table(name = "IFP001")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I01FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I01USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I01FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I01USRMOD", columnDefinition = "CHAR(100)"))
})
public class InformeConfidencial extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String GET_LAZY_DATA = "DatosPersonalesLazyData";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I01ID", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "I01NROREP")
    private BigInteger numeroReporte;

    @Column(name = "I01NROSOL")
    private BigInteger numeroSolicitud;

    @Size(max = 50)
    @Column(name = "I01USUARIO")
    private String usuario;

    @Size(max = 30)
    @Column(name = "I01ENTIDAD")
    private String entidad;

    @Size(max = 100)
    @Column(name = "I01TIPCAM")
    private String tipoDeCambio;

    @Column(name = "I01FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Size(max = 100)
    @Column(name = "I01INSTIT")
    private String institucion;

    @Size(max = 20)
    @Column(name = "I01CONSULT")
    private String consulta;

    public InformeConfidencial() {
    }

    public InformeConfidencial(BigInteger numeroReporte, BigInteger numeroSolicitud, String usuario, String entidad,
                               String tipoDeCambio, Date fecha, String institucion, String consulta) {
        this.numeroReporte = numeroReporte;
        this.numeroSolicitud = numeroSolicitud;
        this.usuario = usuario;
        this.entidad = entidad;
        this.tipoDeCambio = tipoDeCambio;
        this.fecha = fecha;
        this.institucion = institucion;
        this.consulta = consulta;
    }

    // Para la tabla IFP001 se unió 2 objetos (Cabecera, SubConsultasRealizadas)
    public InformeConfidencial(Cabecera cabecera, SubConsultasRealizadas subConsultasRealizadas) {
        this.numeroReporte = FormatosUtils.stringToBigInteger(cabecera.getReporteNumero());
        this.numeroSolicitud = FormatosUtils.stringToBigInteger(cabecera.getNroSolicitud());
        this.usuario = cabecera.getUsuario();
        this.entidad = cabecera.getEntidad();
        this.tipoDeCambio = cabecera.getTipoDeCambio().getDataConsolidada();
        this.institucion = cabecera.getEntidad();
        this.fecha = FormatosUtils.deDDMMYYYYaFecha(cabecera.getFechaHora());
        this.consulta = subConsultasRealizadas != null ? subConsultasRealizadas.getConsulta() : "Sin resultado";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getNumeroReporte() {
        return numeroReporte;
    }

    public void setNumeroReporte(BigInteger numeroReporte) {
        this.numeroReporte = numeroReporte;
    }

    public BigInteger getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(BigInteger numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getTipoDeCambio() {
        return tipoDeCambio;
    }

    public void setTipoDeCambio(String tipoDeCambio) {
        this.tipoDeCambio = tipoDeCambio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InformeConfidencial)) {
            return false;
        }
        InformeConfidencial other = (InformeConfidencial) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InformeConfidencial{" +
                "id=" + id +
                ", numeroReporte=" + numeroReporte +
                ", numeroSolicitud=" + numeroSolicitud +
                ", usuario='" + usuario + '\'' +
                ", entidad='" + entidad + '\'' +
                ", tipoDeCambio='" + tipoDeCambio + '\'' +
                ", fecha=" + fecha +
                ", institucion='" + institucion + '\'' +
                ", consulta='" + consulta + '\'' +
                '}';
    }
}
