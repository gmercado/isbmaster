package com.bisa.bus.servicios.asfi.infocred.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;
import com.bisa.bus.servicios.asfi.infocred.dao.UsuarioHabilitadoDao;
import com.bisa.bus.servicios.asfi.infocred.entities.UsuarioHabilitado;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.LinkedList;
import java.util.List;

@AuthorizeInstantiation({RolesBisa.INFOCRED_ADMIN})
@Menu(value = "Administraci\u00f3n usuarios Infocred", subMenu = SubMenu.INFOCRED)
public class ListadoUsuarioHabilitado extends Listado<UsuarioHabilitado, Long> {

    @Override
    protected ListadoPanel<UsuarioHabilitado, Long> newListadoPanel(String id) {

        return new ListadoPanel<UsuarioHabilitado, Long>(id) {
            @Override
            protected List<IColumn<UsuarioHabilitado, String>> newColumns(ListadoDataProvider<UsuarioHabilitado, Long> dataProvider) {
                List<IColumn<UsuarioHabilitado, String>> columns = new LinkedList<>();
                //columns.add(new PropertyColumn<>(Model.of("Login"), "login", "login"));
                columns.add(new LinkPropertyColumn<UsuarioHabilitado>(Model.of("Login"), "login", "login") {
                    @Override
                    protected void onClick(UsuarioHabilitado object) {
                        setResponsePage(new UsuarioHabilitadoEdicion(object));
                    }
                });
                columns.add(new PropertyColumn<>(Model.of("Nro Documento"), "numeroDocumento", "numeroDocumento"));
                columns.add(new PropertyColumn<>(Model.of("Nombre Completo"), "nombreCompleto", "nombreCompleto"));
                columns.add(new PropertyColumn<>(Model.of("Estado"), "estado", "estado"));
                columns.add(new PropertyColumn<>(Model.of("Consulta Directa"), "consultaDirecta", "consultaDirecta"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<UsuarioHabilitado, Long>> getProviderClazz() {
                return UsuarioHabilitadoDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<UsuarioHabilitado> newFiltroPanel(String id, IModel<UsuarioHabilitado> model1, IModel<UsuarioHabilitado> model2) {
                ListadoUsuarioHabilitadoFiltro filtro = new ListadoUsuarioHabilitadoFiltro(id, model1, model2);
                return filtro;
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("id", true);
            }


            @Override
            protected UsuarioHabilitado newObject2() {
                return new UsuarioHabilitado();
            }

            @Override
            protected UsuarioHabilitado newObject1() {
                return new UsuarioHabilitado();
            }

        };
    }
}
