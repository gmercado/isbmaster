package com.bisa.bus.servicios.asfi.infocred.ui;

import bus.database.components.FiltroPanel;
import com.bisa.bus.servicios.asfi.infocred.entities.UsuarioHabilitado;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class ListadoUsuarioHabilitadoFiltro extends FiltroPanel<UsuarioHabilitado> {

    protected ListadoUsuarioHabilitadoFiltro(String id, IModel<UsuarioHabilitado> model1, IModel<UsuarioHabilitado> model2) {
        super(id, model1, model2);

        add(new TextField<>("login", new PropertyModel<>(model1, "login")));

        add(new Button("habilitar") {
            @Override
            public void onSubmit() {
                setResponsePage(UsuarioHabilitadoNuevo.class);
            }
        });
    }
}
