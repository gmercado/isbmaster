package com.bisa.bus.servicios.asfi.infocred.api;

import com.bisa.bus.servicios.asfi.infocred.entities.Titulares;
import com.bisa.bus.servicios.asfi.infocred.model.SolicitudConsultaTitularInfocredForm;

import java.util.List;

/**
 * Created by gmercado on 25/11/2016.
 */
public interface TitularInfocredService {

    List<Titulares> buscarTitularesPorDocumento(SolicitudConsultaTitularInfocredForm request);
    Titulares guardar(Titulares titulares);
    Titulares cabiarEstado(Titulares titulares);
}
