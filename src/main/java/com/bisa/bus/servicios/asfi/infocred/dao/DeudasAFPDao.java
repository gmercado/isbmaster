package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import com.bisa.bus.servicios.asfi.infocred.api.DeudasAFPService;
import com.bisa.bus.servicios.asfi.infocred.entities.DeudasAFP;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 * Created by gmercado on 18/11/2016.
 */
public class DeudasAFPDao extends DaoImpl<DeudasAFP, Long> implements Serializable, DeudasAFPService {

    protected DeudasAFPDao(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }
}
