package com.bisa.bus.servicios.asfi.infocred.ui;

import bus.consumoweb.enums.TipoAceptacion;
import bus.consumoweb.enums.TipoEstadoUsuario;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.asfi.infocred.dao.UsuarioHabilitadoDao;
import com.bisa.bus.servicios.asfi.infocred.entities.UsuarioHabilitado;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.model.*;
import org.apache.wicket.validation.validator.StringValidator;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static bus.users.api.Metadatas.USER_META_DATA_KEY;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@Titulo("Habilitar usuario consumo Infocred")
@AuthorizeInstantiation({RolesBisa.INFOCRED_ADMIN})
public class UsuarioHabilitadoNuevo extends BisaWebPage {

    public UsuarioHabilitadoNuevo() {

        IModel<UsuarioHabilitado> userModel = Model.of(new UsuarioHabilitado());
        Form<UsuarioHabilitado> nuevoUsuario;
        add(nuevoUsuario = new Form<>("usuarioInfocred", new CompoundPropertyModel<>(userModel)));

        nuevoUsuario.add(new RequiredTextField<String>("login").
                add(StringValidator.maximumLength(25)).
                setOutputMarkupId(true));

        nuevoUsuario.add(new RequiredTextField<String>("numeroDocumento").
                add(StringValidator.maximumLength(20)).
                setOutputMarkupId(true));

        nuevoUsuario.add(new RequiredTextField<String>("nombreCompleto").
                add(StringValidator.maximumLength(100)).
                setOutputMarkupId(true));


        nuevoUsuario.add(new DropDownChoice<>("estadoUsuario", new PropertyModel<>(userModel, "estado"),
                new LoadableDetachableModel<List<? extends TipoEstadoUsuario>>() {
                    @Override
                    protected List<? extends TipoEstadoUsuario> load() {
                        return Arrays.asList(TipoEstadoUsuario.values());
                    }
                }, new IChoiceRenderer<TipoEstadoUsuario>() {
            @Override
            public Object getDisplayValue(TipoEstadoUsuario object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(TipoEstadoUsuario object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoEstadoUsuario getObject(String id, IModel<? extends List<? extends TipoEstadoUsuario>> choices) {
                if (org.apache.commons.lang.StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }));

        nuevoUsuario.add(new DropDownChoice<>("consultaDirecta", new PropertyModel<>(userModel, "consultaDirecta"),
                new LoadableDetachableModel<List<? extends TipoAceptacion>>() {
                    @Override
                    protected List<? extends TipoAceptacion> load() {

                        return Arrays.asList(TipoAceptacion.values());
                    }
                }, new IChoiceRenderer<TipoAceptacion>() {
            @Override
            public Object getDisplayValue(TipoAceptacion object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(TipoAceptacion object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoAceptacion getObject(String id, IModel<? extends List<? extends TipoAceptacion>> choices) {
                if (org.apache.commons.lang.StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }));

        nuevoUsuario.add(new Button("crear", Model.of("Crear")) {
            private static final long serialVersionUID = -7498462827371758458L;
            @Override
            public void onSubmit() {
                UsuarioHabilitadoDao usuarioHabilitadoDao = getInstance(UsuarioHabilitadoDao.class);
                UsuarioHabilitado modelObject = (UsuarioHabilitado) getForm().getModelObject();
                modelObject.setUsuarioCreador(getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase());
                modelObject.setFechaCreacion(new Date());
                usuarioHabilitadoDao.persist(modelObject);
                getSession().info("Usuario Infocred habilitado satisfactoriamente");
                setResponsePage(ListadoUsuarioHabilitado.class);
            }
        });

        nuevoUsuario.add(new Button("cancelar", Model.of("Cancelar")) {
            private static final long serialVersionUID = 1L;
            @Override
            public void onSubmit() {
                setResponsePage(ListadoUsuarioHabilitado.class);
                getSession().info("Operaci\u00f3n cancelada, no se habilit\u00f3 usuario para consultas Infocred.");
            }
        }.setDefaultFormProcessing(false));

    }
}
