package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import com.bisa.bus.servicios.asfi.infocred.api.AlertaCuerpoService;
import com.bisa.bus.servicios.asfi.infocred.entities.AlertaCuerpo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class AlertaCuerpoDao extends DaoImpl<AlertaCuerpo, Long> implements Serializable, AlertaCuerpoService {

    //@Inject
    protected AlertaCuerpoDao(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

}
