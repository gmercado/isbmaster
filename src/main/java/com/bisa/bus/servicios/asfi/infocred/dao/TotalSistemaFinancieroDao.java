package com.bisa.bus.servicios.asfi.infocred.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.asfi.infocred.api.TotalSistemaFinancieroService;
import com.bisa.bus.servicios.asfi.infocred.entities.TotalSistemaFinanciero;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 * Created by gmercado on 18/11/2016.
 */
public class TotalSistemaFinancieroDao extends DaoImpl<TotalSistemaFinanciero, Long> implements Serializable, TotalSistemaFinancieroService {

    protected TotalSistemaFinancieroDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }
}
