/*
 * Copyright 2016 Banco Bisa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.consumoweb.infocred.objetos.SubDomicilio;
import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@Entity
@Table(name = "IFP021")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I21FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I21USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I21FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I21USRMOD", columnDefinition = "CHAR(100)"))
})
public class Domicilio extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private PKDomicilio id;

    @Size(max = 20)
    @Column(name = "I21DEPTO")
    private String departamento;

    @Size(max = 20)
    @Column(name = "I21LOCAL")
    private String localidad;

    @Size(max = 20)
    @Column(name = "I21PROV")
    private String provincia;

    @Size(max = 100)
    @Column(name = "I21DIREC")
    private String direccion;

    @Column(name = "I21FECDEC")
    @Temporal(TemporalType.DATE)
    private Date fecDeclaracion;

    public Domicilio() {
    }

    public Domicilio(PKDomicilio id) {
        this.id = id;
    }

    public Domicilio(PKDomicilio id, String departamento,
                     String localidad, String provincia, String direccion, Date fecDeclaracion) {
        this.id = id;
        this.departamento = departamento;
        this.localidad = localidad;
        this.provincia = provincia;
        this.direccion = direccion;
        this.fecDeclaracion = fecDeclaracion;
    }

    public Domicilio(SubDomicilio domicilio, InformeConfidencial informeConfidencial, int fila) {
        this.id = new PKDomicilio(informeConfidencial.getId(), Long.valueOf(fila));
        this.departamento = domicilio.getDepartamento();
        this.localidad = domicilio.getLocalidad();
        this.provincia = domicilio.getLocalidad();
        this.direccion = domicilio.getDireccion();
        this.fecDeclaracion = FormatosUtils.deDDMMYYYYaFecha(domicilio.getFechaDeclaracion());
        this.fechaCreacion = informeConfidencial.getFechaCreacion();
        this.usuarioCreador = informeConfidencial.getUsuarioCreador();
    }

    public PKDomicilio getId() {
        return id;
    }

    public void setId(PKDomicilio id) {
        this.id = id;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFecDeclaracion() {
        return fecDeclaracion;
    }

    public void setFecDeclaracion(Date fecDeclaracion) {
        this.fecDeclaracion = fecDeclaracion;
    }

    @Override
    public String toString() {
        return "Domicilio{" +
                "id=" + id +
                ", departamento='" + departamento + '\'' +
                ", localidad='" + localidad + '\'' +
                ", provincia='" + provincia + '\'' +
                ", direccion='" + direccion + '\'' +
                ", fecDeclaracion=" + fecDeclaracion +
                '}';
    }
}
