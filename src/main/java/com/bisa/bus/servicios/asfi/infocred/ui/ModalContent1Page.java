package com.bisa.bus.servicios.asfi.infocred.ui;

import bus.consumoweb.infocred.objetos.DatosTitular;
import bus.consumoweb.infocred.objetos.DatosTitularLista;
import bus.consumoweb.infocred.utilitarios.XmlUtil;
import com.bisa.bus.servicios.asfi.infocred.entities.Titulares;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.PageReference;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.AbstractItem;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static bus.users.api.Metadatas.USER_META_DATA_KEY;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class ModalContent1Page extends WebPage {

    private List<Titulares> list = new ArrayList<>();
    private static final Logger log = LoggerFactory.getLogger(ModalContent1Page.class);

    public ModalContent1Page(final PageReference modalWindowPage, final ModalWindow window) {
        try {
            DatosTitularLista d = XmlUtil.xmlToObject(((ConsultaInfocred) modalWindowPage.getPage()).getRespuesta(), DatosTitularLista.class);
            ((ConsultaInfocred) modalWindowPage.getPage()).setRespuesta(null);
            int cont = 0;
            for (DatosTitular datosTitular : d.getTitulares()) {
                Titulares titulares = new Titulares(datosTitular);
                titulares.setUsuarioCreador(getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase());
                titulares.setId(Long.parseLong(String.valueOf(cont)));
                list.add(titulares);
                cont++;
            }

            Iterator<Titulares> titulares = list.iterator();
            RepeatingView repeating = new RepeatingView("repeating");
            add(repeating);
            int index = 0;
            while (titulares.hasNext()) {
                AbstractItem item = new AbstractItem(repeating.newChildId());
                repeating.add(item);
                Titulares titular = titulares.next();
                item.add(new AjaxLink<Long>("selecciona", Model.of(titular.getId())) {
                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        ((ConsultaInfocred) modalWindowPage.getPage()).setRespuesta(list.get(getModelObject().intValue()).getNombreCompleto());
                        window.close(target);
                    }
                });
                item.add(new Label("documento", titular.getDocumento()));
                item.add(new Label("extension", titular.getExtension()));
                item.add(new Label("nombreCompleto", titular.getNombreCompleto()));

                final int idx = index;
                item.add(AttributeModifier.replace("class", new AbstractReadOnlyModel<String>() {
                    private static final long serialVersionUID = 1L;
                    @Override
                    public String getObject() {
                        return (idx % 2 == 1) ? "even" : "odd";
                    }
                }));

                index++;
            }
        } catch (JAXBException e) {
            log.error("Error al tratar de procesar el xml de titulares");
        }

//        add(new AjaxLink<Void>("closeCancel") {
//            @Override
//            public void onClick(AjaxRequestTarget target) {
//                window.close(target);
//            }
//        });
    }
}