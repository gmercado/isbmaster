package com.bisa.bus.servicios.asfi.infocred;

import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.bus.servicios.asfi.infocred.api.*;
import com.bisa.bus.servicios.asfi.infocred.dao.*;
import com.bisa.bus.servicios.asfi.infocred.entities.BitacoraInfocred;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class InfocredModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        //bind(ParametrosSegipService.class).to(VariableSegipDao.class);
        bind(InformeConfidencialService.class).to(InformeConfidencialDao.class);
        bind(BitacoraInfocredService.class).to(BitacoraInfocredDao.class);
        bind(TitularInfocredService.class).to(TitularInfocredDao.class);
        bind(UsuarioHabilitadoService.class).to(UsuarioHabilitadoDao.class);
        bind(DatosPersonalesService.class).to(DatosPersonalesDao.class);
        bind(AlertaCabeceraService.class).to(AlertaCabeceraDao.class);
        bind(SaldosPersonaDeudaService.class).to(SaldosPersonaDeudaDao.class);
        bind(InfocredCarteraComaService.class).to(InfocredCarteraComaDao.class);
        bind(InfocredCarteraPipeService.class).to(InfocredCarteraPipeDao.class);
        bind(InfocredCarteraZipPipeService.class).to(InfocredCarteraZipPipeDao.class);
        bind(InfocredService.class).to(InfocredServiceImpl.class);
        bindUI("com/bisa/bus/servicios/asfi/infocred/ui");
    }
}
