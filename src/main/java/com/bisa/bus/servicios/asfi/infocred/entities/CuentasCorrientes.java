/*
 * Copyright 2016 Banco Bisa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.consumoweb.infocred.objetos.CuentasCorrientesFila;
import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@Entity
@Table(name = "IFP023")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I23FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I23USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I23FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I23USRMOD", columnDefinition = "CHAR(100)"))
})
public class CuentasCorrientes extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private PKCuentasCorrientes id;

    @Column(name = "I23ENTIDAD")
    private String entidad;

    @Column(name = "I23ESTADO")
    private String estado;

    @Column(name = "I23FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    public CuentasCorrientes() {
    }

    public CuentasCorrientes(PKCuentasCorrientes id, String entidad, String estado, Date fecha) {
        this.id = id;
        this.entidad = entidad;
        this.estado = estado;
        this.fecha = fecha;
    }

    public CuentasCorrientes(CuentasCorrientesFila cuentasCorrientes, InformeConfidencial informeConfidencial) {
        this.id = new PKCuentasCorrientes(informeConfidencial.getId(), Long.valueOf(cuentasCorrientes.getFila()));
        this.entidad = cuentasCorrientes.getEntidad();
        this.estado = cuentasCorrientes.getEstado();
        this.fecha = FormatosUtils.deDDMMYYYYaFecha(cuentasCorrientes.getFecha());
        this.fechaCreacion = informeConfidencial.getFechaCreacion();
        this.usuarioCreador = informeConfidencial.getUsuarioCreador();
    }

    public PKCuentasCorrientes getId() {
        return id;
    }

    public void setId(PKCuentasCorrientes id) {
        this.id = id;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "CuentasCorrientes{" +
                "id=" + id +
                ", entidad='" + entidad + '\'' +
                ", estado='" + estado + '\'' +
                ", fecha=" + fecha +
                '}';
    }
}
