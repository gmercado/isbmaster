/*
 * Copyright 2016 Banco Bisa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bisa.bus.servicios.asfi.infocred.entities;

import bus.consumoweb.infocred.objetos.SubCasasComercialesFila;
import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 *
 * @author Gary Mercado
 * Yrag Knup
 */
@Entity
@Table(name = "IFP024")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I24FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I24USRALT", nullable = false, columnDefinition = "CHAR(100) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I24FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I24USRMOD", columnDefinition = "CHAR(100)"))
})
public class CasasComerciales extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private PKCasasComerciales id;

    @Column(name = "I24ENTIDAD")
    private String entidad;

    @Column(name = "I24FECING")
    @Temporal(TemporalType.DATE)
    private Date fechaIngreso;

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "I24MONTO")
    private BigDecimal monto;

    @Column(name = "I24TIPOBL")
    private String tipoObligado;

    @Column(name = "I24ESTDEU")
    private String estadoDeuda;

    @Column(name = "I24REFER")
    private String referencia;

    @Column(name = "I24FECACT")
    @Temporal(TemporalType.DATE)
    private Date fechaActualizacion;

    public CasasComerciales() {
    }

    public CasasComerciales(SubCasasComercialesFila casasComerciales, InformeConfidencial informeConfidencial) {
        this.id = new PKCasasComerciales(informeConfidencial.getId(), Long.valueOf(casasComerciales.getFila()));
        this.entidad = casasComerciales.getEntidad();
        this.fechaIngreso = FormatosUtils.deDDMMYYYYaFecha(casasComerciales.getFechaIngreso());
        this.monto = FormatosUtils.stringToBigDecimal(casasComerciales.getMonto());
        this.tipoObligado = casasComerciales.getTipoObligado();
        this.estadoDeuda = casasComerciales.getEstadoDeuda();
        this.referencia = casasComerciales.getReferencia();
        this.fechaActualizacion = FormatosUtils.deDDMMYYYYaFecha(casasComerciales.getFechaActualizacon());
        this.fechaCreacion = informeConfidencial.getFechaCreacion();
        this.usuarioCreador = informeConfidencial.getUsuarioCreador();
    }

    public PKCasasComerciales getId() {
        return id;
    }

    public void setId(PKCasasComerciales id) {
        this.id = id;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getTipoObligado() {
        return tipoObligado;
    }

    public void setTipoObligado(String tipoObligado) {
        this.tipoObligado = tipoObligado;
    }

    public String getEstadoDeuda() {
        return estadoDeuda;
    }

    public void setEstadoDeuda(String estadoDeuda) {
        this.estadoDeuda = estadoDeuda;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    @Override
    public String toString() {
        return "CasasComerciales{" +
                "id=" + id +
                ", entidad='" + entidad + '\'' +
                ", fechaIngreso=" + fechaIngreso +
                ", monto=" + monto +
                ", tipoObligado='" + tipoObligado + '\'' +
                ", estadoDeuda='" + estadoDeuda + '\'' +
                ", referencia='" + referencia + '\'' +
                ", fechaActualizacion=" + fechaActualizacion +
                '}';
    }
}
