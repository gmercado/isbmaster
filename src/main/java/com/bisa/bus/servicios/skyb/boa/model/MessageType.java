package com.bisa.bus.servicios.skyb.boa.model;

/**
 * @author rchura on 20-11-15
 */
public enum MessageType {

    /**
     * An error message
     */
    ERROR,
    /**
     * A warning message
     */
    WARNING,
    /**
     * An information message
     */
    INFO,
    /**
     * Simple message
     */
    NONE,
    REINTENTAR,
    SINRESPUESTA
}
