package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by rchura on 09-11-15.
 */
@XmlRootElement
public class EntradaTicketPNR extends Entrada{

    /**
     * language
     * AA {A->Z sin Ñ).
     * Idioma en el que se devuelven los mensajes al consumidor. Los valores aceptados son los siguientes:
     * ES: Español,   EN: Inglés
     * Si se envía un valor diferente de los anteriores, el sistema utilizará el idioma por defecto del usuario.
     */
    @XmlElement(name = "language")
    @JsonProperty(value = "language")
    private String lenguaje;

    public EntradaTicketPNR(String credenciales, String ip, Boolean xmlOrJson, Localizador localizador, String lenguaje) {
        super(credenciales, ip, xmlOrJson, localizador);
        this.lenguaje = lenguaje;
    }

    public String getLenguaje() {
        return lenguaje;
    }

    public void setLenguaje(String lenguaje) {
        this.lenguaje = lenguaje;
    }
}
