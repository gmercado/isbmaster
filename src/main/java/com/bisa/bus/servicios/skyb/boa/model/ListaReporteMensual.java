package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by rchura on 09-12-15.
 */
public class ListaReporteMensual {

    @JsonProperty(value = "ResultMonthlyReconciliation")
    private List<ReporteMensual> reporteMensual;

    public List<ReporteMensual> getReporteMensual() {
        return reporteMensual;
    }

    public void setReporteMensual(List<ReporteMensual> reporteMensual) {
        this.reporteMensual = reporteMensual;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ListaReporteMensual{");
        sb.append("reporteMensual=").append(reporteMensual);
        sb.append('}');
        return sb.toString();
    }
}
