package com.bisa.bus.servicios.skyb.boa.api;

import bus.database.datasource.CambioAmbienteDataSource;
import bus.database.jpa.EntityManagerFactoryServicePrincipal;
import bus.database.model.AmbienteActual;
import bus.database.model.BasePrincipal;
import bus.database.model.BaseSinCambioAmbiente;
import bus.env.api.MedioAmbiente;
import bus.monitor.api.*;
import bus.plumbing.cypher.TripleDes;
import com.bisa.bus.servicios.skyb.boa.dao.PagosBoaDao;
import com.bisa.bus.servicios.skyb.boa.dao.TransaccionMonitorBoa;
import com.bisa.bus.servicios.skyb.boa.entities.EstadosBoa;
import com.bisa.bus.servicios.skyb.boa.entities.PagosBoa;
import com.bisa.bus.servicios.skyb.boa.entities.TipoProcesoPago;
import com.bisa.bus.servicios.skyb.boa.model.Autorizacion;
import com.bisa.bus.servicios.skyb.boa.model.Mensaje;
import com.bisa.bus.servicios.skyb.boa.model.MessageType;
import com.bisa.bus.servicios.skyb.boa.ws.PagoReservaRequest;
import com.bisa.bus.servicios.skyb.boa.ws.PagoReservaResponse;
import com.bisa.bus.servicios.skyb.boa.ws.TokenRequest;
import com.bisa.bus.servicios.skyb.boa.ws.TokenResponse;
import com.google.inject.Inject;
import com.google.inject.Key;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.EntityManagerFactoryImpl;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import static bus.env.api.Variables.*;

/**
 * Created by rchura on 23-11-15.
 */
public class ProcesosBoa {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcesosBoa.class);
    private static final String PROCESADA = "P";
    private static final String AUTORIZADO ="1";
    private static final String NO_AUTORIZADO ="0";

    private final TransaccionMonitorBoa monitorBoa;
    private final MedioAmbiente medioAmbiente;
    private final ServiciosBoa serviciosBoa;
    private final PagosBoaDao pagosBoaDao;

    @Inject
    public ProcesosBoa(TransaccionMonitorBoa monitorBoa, MedioAmbiente medioAmbiente, ServiciosBoa serviciosBoa,
                       PagosBoaDao pagosBoaDao) {
        this.monitorBoa = monitorBoa;
        this.medioAmbiente = medioAmbiente;
        this.serviciosBoa = serviciosBoa;
        this.pagosBoaDao = pagosBoaDao;
    }


    public PagoReservaResponse pagoNormal(PagoReservaRequest request){

        PagoReservaResponse response = null;
        if (request == null) {
            response = new PagoReservaResponse(new Autorizacion(), 0L, null);
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, "No se puede obtener los datos de Solicitud para efectuar el Pago.");
            response.setMensaje(mensaje);
            LOGGER.error("Error: No se puede obtener los datos de Solicitud para efectuar el Pago <PagoReservaRequest>.");
            return response;
        }
        // Verifica si se permite el pago
        String permitir = StringUtils.trimToEmpty(medioAmbiente.getValorDe(SKYB_BOA_PERMITIR_PAGO, SKYB_BOA_PERMITIR_PAGO_DEFAULT));
        if("false".equalsIgnoreCase(permitir)) {
            response = new PagoReservaResponse(new Autorizacion(), 0L, null);
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_SERVICIO_NO_DISPONIBLE, SKYB_BOA_MENSAJE_SERVICIO_NO_DISPONIBLE_DEFAULT));
            response.setMensaje(mensaje);
            LOGGER.warn("Alerta: El pago de reservas a BoA no está disponible. Por favor intenta más tarde. ");
            return response;
        }

        PagosBoa pago = null;
        if(request.getPagoID()!=null && request.getPagoID()>0L){
            //Obtiene el registro de pago por ID si existe
            pago = pagosBoaDao.find(request.getPagoID());
        }

        // Identifica el tipo de operacion de pago
        if (TipoProcesoPago.PAGOAUTORIZADO.getTipo().equalsIgnoreCase(request.getProceso())) {
            //PAGO DE UN REGISTRO PENDIENTE DE AUTORIZACION
            //Intenta obtener algún registro de pago mediante el localizador
            if(pago==null){
                try {
                    pago = pagosBoaDao.getPagoByLocalizadorEstado(request.getCodigoPnr(), request.getApellidoGrupo(), request.getCliente());
                } catch (Exception e) {
                    pago = null;
                }
            }
            // Registro de pago de reserva en DB con estado GENERADO
            pago = pagosBoaDao.getPagosBoa(request, pago);
            if (pago == null) {
                response = new PagoReservaResponse(new Autorizacion(), 0L, null);
                Mensaje mensaje = new Mensaje("", MessageType.ERROR, "No se puede efectuar el registro de Pago.");
                response.setMensaje(mensaje);
                LOGGER.error("Error: No se puede efectuar el registro de Pago segun la solicitud <PagoReservaRequest>.");
                return response;
            }

            // Verifica el estado del registro de pago que debe estar en autorización (Espera de aprobaciones)
            if (EstadosBoa.AUTORIZACION.getEstado()!= pago.getEstado()){
                response = new PagoReservaResponse(new Autorizacion(), 0L, null);
                String resp = "No se puede efectuar el Pago de la reserva. " +
                        "La Reserva con PNR["+request.getCodigoPnr()+"-"+request.getApellidoGrupo()+"] no fue registrado para ser aprobado. " +
                        "Estado actual ["+EstadosBoa.valorEnum(pago.getEstado()).getDescripcion() +"]";
                Mensaje mensaje = new Mensaje("", MessageType.ERROR, resp);
                response.setMensaje(mensaje);
                LOGGER.error("Error: "+resp);
                return response;
            }

            response = procesarPago(request, pago);
            if(response!=null){
                if(response.getPagoID()==null){
                    response.setPagoID(pago.getId());
                }
            }
            return response;

        }else{
            //PAGO DE UN REGISTRO NORMAL
            //Intenta obtener algún registro de pago mediante el localizador
            if(pago==null){
                try {
                    pago = pagosBoaDao.getPagoByLocalizadorEstado(request.getCodigoPnr(), request.getApellidoGrupo(), request.getCliente());
                } catch (Exception e) {
                    pago = null;
                }
            }
            // Registro de pago de reserva en DB con estado GENERADO
            pago = pagosBoaDao.getPagosBoa(request, pago);
            if (pago == null) {
                response = new PagoReservaResponse();
                Mensaje mensaje = new Mensaje("", MessageType.ERROR, "No se puede efectuar el registro de Pago.");
                response.setMensaje(mensaje);
                LOGGER.error("Error: No se puede efectuar el registro de Pago segun la solicitud <PagoReservaRequest>.");
                return response;
            }

            response = procesarPago(request, pago);
            if(response!=null){
                if(response.getPagoID()==null){
                    response.setPagoID(pago.getId());
                }
            }
            return response;

        }


    }

    /**
     * Eejecuta todo el proceso  de pago independientemente si es o no con doble autorizacion
     * @param request
     * @return
     */
    public PagoReservaResponse procesarPago(PagoReservaRequest request, PagosBoa pago) {

        PagoReservaResponse response = new PagoReservaResponse(new Autorizacion(), 0L, null);
        response.setPagoID(pago.getId());


        // Validamos que el estado de la reserva no este en ninguno de los siguientes estados
        if (EstadosBoa.ENPROCESO.getEstado() == pago.getEstado() ||
                EstadosBoa.PAGOAUTORIZADO.getEstado() == pago.getEstado() ||
                EstadosBoa.SINRESPUESTA.getEstado() == pago.getEstado()) {

            EstadosBoa estado = EstadosBoa.valorEnum(pago.getEstado());

            //response.setPagoID(pago.getId());
            String resp = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_NO_PROCESA_EMISION, SKYB_BOA_MENSAJE_NO_PROCESA_EMISION_DEFAULT);
            resp = MessageFormat.format(resp, "La reserva se encuentra en estado " + estado.getDescripcion());
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, resp);

            response.setMensaje(mensaje);
            LOGGER.error("El Pago de la Reserva con ID[{}], no puede ser procesada. La reserva se encuentra en estado [{}].", pago.getId(), estado.getDescripcion());
            return response;
        }

        // Validamos que el estado de la reserva no este en ninguno de los siguientes estados
        if (EstadosBoa.PAGOCOMPLETADO.getEstado() == pago.getEstado() ||
                EstadosBoa.ENPROCESO.getEstado() == pago.getEstado() ||
                EstadosBoa.SINRESPUESTA.getEstado() == pago.getEstado() ||
                EstadosBoa.ERRORPAGO.getEstado() == pago.getEstado() ||
                EstadosBoa.ERRORREVERSA.getEstado() == pago.getEstado() ||
                EstadosBoa.REVERSANDO.getEstado() == pago.getEstado() ||
                EstadosBoa.ERRORDEBITO.getEstado() == pago.getEstado() ||
                EstadosBoa.RECHAZADO.getEstado() == pago.getEstado() ||
                EstadosBoa.PAGOREVERTIDO.getEstado() == pago.getEstado()) {

            EstadosBoa estado = EstadosBoa.valorEnum(pago.getEstado());

            //response.setPagoID(pago.getId());
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, "El Pago de la Reserva no puede ser procesada. La reserva se encuentra en estado [" + estado.toString() + "].");
            response.setMensaje(mensaje);
            LOGGER.error("El Pago de la Reserva con ID[{}], no puede ser procesada. La reserva se encuentra en estado [{}].", pago.getId(), estado.getDescripcion());
            return response;
        }


        // Para el caso de doble autorizacion se registra la solicitud A ESPERA DE AUTORIZACION DE PAGO o RECHAZA DE PAGO
        if (TipoProcesoPago.AUTORIZAR.getTipo().equalsIgnoreCase(request.getProceso())) {
            // Actualiza el pago
            pago.setEstado(EstadosBoa.AUTORIZACION.getEstado());
            pago = pagosBoaDao.actualizar(pago, request.getUsuario());
            if(pago!=null){
                // Respuesta de Autorizacion
                //response.setPagoID(pago.getId());
                Mensaje mensaje = new Mensaje(AUTORIZADO, MessageType.INFO, "El Pago de la Reserva fue registrado para su posterior aprobaci\u00f3n."); //Codigo 1 necesario para e-Bisa
                response.setMensaje(mensaje);
                LOGGER.info("El Pago de la Reserva con ID[{}], fue registrado por el usuario [{}] a la espera de aprobaciones.", pago.getId(), request.getUsuario());
                return response;
            }else{
                Mensaje mensaje = new Mensaje("", MessageType.ERROR, "No se puede registrar el Pago de la Reserva para su aprobaci\u00f3n.");
                response.setMensaje(mensaje);
                LOGGER.error("El Pago de la Reserva con PNR[{}], NO fue registrado para su aprobacion.", request.getCodigoPnr());
                return response;
            }

        } else if (TipoProcesoPago.RECHAZAR.getTipo().equalsIgnoreCase(request.getProceso())) {
            // Actualiza el pago
            pago.setFechaRechazo(new Date());
            pago.setUserRechazo(request.getUsuario());
            pago.setEstado(EstadosBoa.RECHAZADO.getEstado());
            pagosBoaDao.actualizar(pago, request.getUsuario());

            // Respuesta de rechazo
            //response.setPagoID(pago.getId());
            Mensaje mensaje = new Mensaje("", MessageType.INFO, "El Pago de la Reserva fue rechazado satisfactoriamente.");
            response.setMensaje(mensaje);
            LOGGER.info("El Pago de la Reserva con ID[{}], fue rechazado por el usuario [{}].", pago.getId(), request.getUsuario());
            return response;

        } else if (TipoProcesoPago.PAGAR.getTipo().equalsIgnoreCase(request.getProceso()) ||
                TipoProcesoPago.PAGOAUTORIZADO.getTipo().equalsIgnoreCase(request.getProceso())) {
            // Proceso principal de pago
            String resp= "";
            pago = ejecutarDebito(pago, request.getUsuario());

            if(pago ==null){
                // No actualizamos de estado por si quisieramos intentar nuevamente, solo reportamos el error.
                resp = "Ocurri\u00f3 un error inesperado al efectuar el d\u00e9bito. Por favor comun\u00edquese con Bisa Responde.";
                Mensaje mensaje = new Mensaje("", MessageType.ERROR, resp);
                response.setMensaje(mensaje);
                LOGGER.error("Error: "+resp);
                return response;
            }

            if((PROCESADA.equalsIgnoreCase(pago.getPosteo()) && pago.getNroCaja()!=null && pago.getNroSecuencia()!=null)&&
                    (StringUtils.trimToNull(pago.getPosteoReversa())==null && pago.getSecuenciaReversa()==null)){
                //Cambio de estado a debitado
                pago.setEstado(EstadosBoa.DEBITADO.getEstado());
                pago.setFechaProceso(new Date());
                pago.setUserProceso(request.getUsuario());
                pagosBoaDao.actualizar(pago, request.getUsuario());

                TokenRequest tokenRequest = new TokenRequest(request.getCliente(), request.getUsuario(), request.getCodigoPnr(),
                        request.getApellidoGrupo(), request.getIpOrigen(), request.getSistema());

                TokenResponse tokenResponse = serviciosBoa.obtenerToken(tokenRequest);
                if(tokenResponse==null || tokenResponse.getToken()==null){
                    //Revertir debito
                    /*
                    pago.setEstado(EstadosBoa.REVERSANDO.getEstado());
                    pago = pagosBoaDao.actualizar(pago, request.getUser());

                    pago = ejecutarReversa(pago, request.getUser());
                    if(pago==null){
                        resp = "Ocurri\u00f3 un error inesperado al revertir el d\u00e9bito. Por favor comun\u00edquese con Bisa Responde.";
                        Mensaje mensaje = new Mensaje("", MessageType.ERROR, resp);
                        response.setMensaje(mensaje);
                        LOGGER.error("Error: "+resp);
                        return response;
                    }*/
                    //Responder mensaje de error

                    //pago.setEstado(EstadosBoa.ERRORPAGO.getEstado());
                    //pago = pagosBoaDao.actualizar(pago, request.getUser());

                    pago.setEstado(EstadosBoa.SINRESPUESTA.getEstado());
                    //pago.setEstadoRespuesta(StringUtils.substring(response.getAutorizacion().getEstado(),0,5));
                    //pago.setMensajeRespuesta(StringUtils.substring(response.getAutorizacion().getMensaje(),0,200));

                    pago = pagosBoaDao.actualizar(pago, request.getUsuario());
                    response.setPagoID(pago.getId());

                    //resp = "Error al obtener el Token para efectuar la Transacci\u00f3n de Pago.";
                    //resp = "No se obtiene respuesta de getToken pero se almacena Transacci\u00f3n de Pago para reintento.";
                    resp = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_PROCESO_EMISION,SKYB_BOA_MENSAJE_PROCESO_EMISION_DEFAULT);
                    Mensaje mensaje = new Mensaje("", MessageType.INFO, resp);
                    // Respuesta por defecto para esperar reintentos
                    response.setAutorizacion(new Autorizacion("1",""));
                    response.setMensaje(mensaje);
                    LOGGER.info("No se obtiene respuesta de getToken pero se almacena Transacción de Pago para reintento de reserva ID[{}]. "+resp, pago.getId());
                    return response;
                }
                // Mensaje de error en recuperación de Token de BOA
                if(tokenResponse.getToken().startsWith("ERROR:")){
                    pago.setEstado(EstadosBoa.REVERSANDO.getEstado());
                    pago = pagosBoaDao.actualizar(pago, request.getUsuario());
                    pago = ejecutarReversa(pago, request.getUsuario());
                    if(pago==null){
                        resp = "Ocurri\u00f3 un error inesperado al revertir el d\u00e9bito. Por favor comun\u00edquese con Bisa Responde.";
                        Mensaje mensaje = new Mensaje("", MessageType.ERROR, resp);
                        response.setMensaje(mensaje);
                        LOGGER.error("Error: "+resp);
                        return response;
                    }
                    pago.setEstado(EstadosBoa.ERRORPAGO.getEstado());
                    pago.setEstadoRespuesta(StringUtils.substring(response.getMensaje().getCodigo(), 0, 5));
                    String desc  = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_NO_PROCESA_TOKEN, SKYB_BOA_MENSAJE_NO_PROCESA_TOKEN_DEFAULT);
                    desc = MessageFormat.format(desc, tokenResponse.getToken());

                    pago.setMensajeRespuesta(desc);
                    pago = pagosBoaDao.actualizar(pago, request.getUsuario());
                    Mensaje mensaje = new Mensaje("", MessageType.ERROR, desc);
                    response.setMensaje(mensaje);
                    LOGGER.error("Error: "+resp);
                    return response;
                }
//                if(true){
//                    // MARCAMOS CON ESTADO SIN RESPUESTA
//                    pago.setEstado(EstadosBoa.SINRESPUESTA.getEstado());
//                    pago.setEstadoRespuesta("900");
//                    pago.setMensajeRespuesta("Esperando respuesta");
//                    pago = pagosBoaDao.actualizar(pago, request.getUser());
//                    Mensaje mensaje = new Mensaje("900", MessageType.SINRESPUESTA, "No se tiene respuesta de BoA al registrar el pago del boleto. Prueba");
//                    response.setMensaje(mensaje);
//                    LOGGER.error("No se tiene respuesta de BoA al registrar el pago del boleto.");
//                    Autorizacion autorizDefault = new Autorizacion("", "");
//                    response.setAutorizacion(autorizDefault);
//                    return response;
//                }

                String token = tokenResponse.getToken();
                String codigoAutorizacion =""+pago.getNroCaja().toPlainString()+pago.getNroSecuencia().toPlainString();
                // ENCRIPTAR EL CODIGO DE AUTORIZACION

                codigoAutorizacion = encriptar(codigoAutorizacion);
                //LOGGER.info("Cadena Encriptada [{}].", codigoAutorizacion);

                //String desencriptado = desEncriptar(codigoAutorizacion);
                //LOGGER.info("Cadena DESEncriptada [{}].", desencriptado);
                LOGGER.info("Cadena token[{}].", token);
                token = StringUtils.trim(StringUtils.substring(token,0,250));
                pago.setToken(token);
                pago.setEstado(EstadosBoa.ENPROCESO.getEstado());
                pago = pagosBoaDao.actualizar(pago, request.getUsuario());

                //ENVIAMOS LA AUTORIZACION DE PAGO

                //MANEJAR EXCEPCIONES TIME OUT DE HTTP, (ERROR DIFERENTES A 200  HABRIA QUE REVERTIR)

                // Proceso de autorizacion de reserva en BOA
                response = serviciosBoa.pagarReserva(request, token, codigoAutorizacion);
                response.setPagoID(pago.getId());

                if(response.getMensaje()!=null && MessageType.SINRESPUESTA.equals(response.getMensaje().getTipo())){
                    // MARCAMOS CON ESTADO SIN RESPUESTA
                    pago.setEstado(EstadosBoa.SINRESPUESTA.getEstado());
                    pago.setEstadoRespuesta(StringUtils.substring(response.getMensaje().getCodigo(), 0, 5));
                    pago.setMensajeRespuesta(StringUtils.substring(response.getMensaje().getMensaje(), 0, 200));
                    pago = pagosBoaDao.actualizar(pago, request.getUsuario());
                    response.setAutorizacion(new Autorizacion("1",""));
                    return response;
                }


                if(response.getMensaje()!=null && MessageType.ERROR.equals(response.getMensaje().getTipo())){
                    // En caso de Error, Revertir debito
                    pago.setEstado(EstadosBoa.REVERSANDO.getEstado());
                    pago = pagosBoaDao.actualizar(pago, request.getUsuario());

                    pago = ejecutarReversa(pago, request.getUsuario());
                    if(pago==null){
                        resp = "Ocurri\u00f3 un error inesperado al revertir el d\u00e9bito. Por favor comun\u00edquese con Bisa Responde.";
                        Mensaje mensaje = new Mensaje("", MessageType.ERROR, resp);
                        response.setMensaje(mensaje);
                        LOGGER.error("Error: "+resp);
                        return response;
                    }
                    /*
                    if(response.getMensaje()==null){
                        //Responder mensaje de error
                        pago.setEstado(EstadosBoa.ERRORPAGO.getEstado());
                        pago = pagosBoaDao.actualizar(pago, request.getUser());

                        resp = "Pago NO autoriado por BoA. No se puede solicitar la emisi\u00f3n del boleto.";
                        Mensaje mensaje = new Mensaje("", MessageType.ERROR, resp);
                        response.setMensaje(mensaje);
                        LOGGER.info("Error al efectuar el Pago de la Reserva con ID[{}]. "+resp, pago.getId());
                        //return response;
                    }
                    */

                    pago.setEstado(EstadosBoa.ERRORPAGO.getEstado());
                    pago.setEstadoRespuesta(StringUtils.substring(response.getMensaje().getCodigo(),0,5));
                    String desc = ""+StringUtils.substring(response.getMensaje().getMensaje(),0,200);
                    pago.setMensajeRespuesta(desc);
                    pago = pagosBoaDao.actualizar(pago, request.getUsuario());

                    return response;
                }



                if(AUTORIZADO.equalsIgnoreCase(StringUtils.trimToEmpty(response.getAutorizacion().getEstado()))){
                    // Pago registrado satisfactoriamente
                    // A la espera de emision de boletos.

                    pago.setEstado(EstadosBoa.PAGOAUTORIZADO.getEstado());
                    pago.setEstadoRespuesta(StringUtils.substring(response.getAutorizacion().getEstado(),0,5));
                    pago.setMensajeRespuesta(StringUtils.substring(response.getAutorizacion().getMensaje(),0,200));

                    pago = pagosBoaDao.actualizar(pago, request.getUsuario());
                    response.setPagoID(pago.getId());

                }

                return response;

            }else{
                //Error en el proceso de debito
                // Respuesta error en debito
                if(cuentaSinFondo(StringUtils.trimToEmpty(pago.getErrorMonitor()))){
                    resp = "Saldo insuficiente en la cuenta del cliente. Codigo Monitor ["+StringUtils.trimToEmpty(pago.getErrorMonitor())+"].";
                }else{
                    resp = "No se puede efectuar d\u00e9bito de la cuenta del cliente. Codigo Monitor ["+StringUtils.trimToEmpty(pago.getErrorMonitor())+"]. Intenta m\u00e3s tarde por favor.";
                }
                pago.setEstado(EstadosBoa.ERRORPAGO.getEstado());
                pago.setMensajeRespuesta(StringUtils.substring(resp,0,200));
                pago = pagosBoaDao.actualizar(pago, request.getUsuario());
                response.setPagoID(pago.getId());

                //response.setPagoID(pago.getId());
                Mensaje mensaje = new Mensaje("", MessageType.ERROR, resp);
                response.setMensaje(mensaje);
                LOGGER.info("Error al efectuar el Pago de la Reserva con ID[{}]. "+resp, pago.getId());
                return response;
            }

        } else {
            LOGGER.error("Error el tipo de proceso [{}] no esta definido para procesar el Pago de la Reserva.", request.getProceso());
            //response.setPagoID(pago.getId());
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, "El Pago de la Reserva no puede ser procesada. Tipo de proceso NO definido [" + request.getProceso() + "].");
            response.setMensaje(mensaje);
            return response;
        }

    }


    public PagosBoa ejecutarDebito(PagosBoa pago, String usuario){

        if (EstadosBoa.GENERADO.getEstado() == pago.getEstado() ||
                EstadosBoa.AUTORIZACION.getEstado() == pago.getEstado()){
            // Estos estados permiten el debito

            IRespuestasMonitor monitor = null;
            try {
                monitor = monitorBoa.procesarDebito(pago);
            } catch (SistemaCerradoException e) {
                LOGGER.error("Error Monitor: No se puede efectuar el Debito <SistemaCerradoException>. ID Pago BoA [{}], Localizador [{}].",
                        pago.getId(), pago.getNroPNR()+"-"+pago.getNombre(), e);
                monitor =null;
            } catch (ImposibleLeerRespuestaException e) {
                //No se tiene respuesta del monitor de transacciones
                pago.setEstado(EstadosBoa.ERRORDEBITO.getEstado());
                pago.setMensajeRespuesta(StringUtils.substring(""+e.getCodigo()+"-"+e.getMessage(),0,200));
                pagosBoaDao.actualizar(pago, usuario);
                LOGGER.error("Error Monitor: No se puede efectuar el Debito <ImposibleLeerRespuestaException>. ID Pago BoA [{}], Localizador [{}].",
                        pago.getId(), pago.getNroPNR()+"-"+pago.getNombre(), e);
                monitor =null;
            } catch (TransaccionEfectivaException e) {
                LOGGER.error("Error Monitor: No se puede efectuar el Debito <TransaccionEfectivaException>. ID Pago BoA [{}], Localizador [{}].",
                        pago.getId(), pago.getNroPNR()+"-"+pago.getNombre(), e);
                monitor =null;
            } catch (TransaccionNoEjecutadaCorrectamenteException e) {
                LOGGER.error("Error Monitor: No se puede efectuar el Debito <TransaccionNoEjecutadaCorrectamenteException>. ID Pago BoA [{}], Localizador [{}].",
                        pago.getId(), pago.getNroPNR()+"-"+pago.getNombre(), e);
                monitor =null;
            } catch (IOException e) {
                LOGGER.error("Error Monitor: No se puede efectuar el Debito <IOException>. ID Pago BoA [{}], Localizador [{}].",
                        pago.getId(), pago.getNroPNR()+"-"+pago.getNombre(), e);
                monitor =null;
            } catch (Exception e) {
                LOGGER.error("Error Monitor: No se puede efectuar el Debito <IOException>. ID Pago BoA [{}], Localizador [{}].",
                        pago.getId(), pago.getNroPNR()+"-"+pago.getNombre(), e);
                monitor =null;
            }

            if(monitor==null){
                // OJO Retornar error en debito
                return null;
            }
            //Caso normal: Transaccion por el Aporte Total
            // Actualizamos caja y secuencia
            pago.setNroCaja(monitor.getDatoRecibidoCabeceraDecimal("WNUMUSR"));
            pago.setNroSecuencia(monitor.getDatoRecibidoCabeceraDecimal("WNUMSEQ"));
            pago.setPosteo(monitor.getDatoRecibidoCabeceraString("WPOST"));
            pago.setErrorMonitor(monitor.getDatoRecibidoCabeceraString("WREJ1"));
            //aporte.setnrocWnumctl(monitor.getDatoRecibidoCabeceraDecimal("WNUMCTL"));
            pago.setFechaMonitor(monitor.getDatoRecibidoCabeceraDecimal("WFECPRO"));
            pago.setFechaSistema(monitor.getDatoRecibidoCabeceraDecimal("WFECSIS"));
            pago.setHoraSistema(monitor.getDatoRecibidoCabeceraDecimal("WHORA"));
            pago.setCodigoSeguridad(monitor.getDatoRecibidoCabeceraDecimal("WCODSEG"));
            //pago.setCodigoSeguridad(monitor.getDatoRecibidoCabeceraDecimal("WCODSEG"));

            if (monitor.next1()) {
                pago.setTotalMonitor(monitor.getDatoRecibidoCuerpo1Decimal("WIMPTOT"));
                pago.setCargoMonitor(monitor.getDatoRecibidoCuerpo1Decimal("WCARGO"));
                pago.setCotizacionMonitor(monitor.getDatoRecibidoCuerpo1Decimal("WCOTACT"));
                pago.setCompraVenta(monitor.getDatoRecibidoCuerpo1String("WCOMVEN"));
            }else{
                LOGGER.warn("No se puede obtener los datos del Cuerpo1 del Monitor.");
            }
        }else if(EstadosBoa.DEBITADO.getEstado() == pago.getEstado()){
            // El aporte puede estar debitado
            EstadosBoa estado = EstadosBoa.valorEnum(pago.getEstado());
            LOGGER.info("El Pago de la reserva ya fue debitado, NO se puede procesar el debito nuevamente. Cliente:["+pago.getNroCliente()+"], " +
                    "PNR:["+pago.getNroPNR()+"-"+pago.getNombre()+"], Caja:["+pago.getNroCaja()+"], Secuencia:["+pago.getNroSecuencia()+
                    "]. El Aporte se encuentra con estado ["+estado.getDescripcion()+"].");
        }

        return pago;
    }


    public PagosBoa ejecutarReversa(PagosBoa pago, String usuario){


        IRespuestasMonitor monitor = null;
        try {
            monitor = monitorBoa.revertirDebito(pago);
        } catch (SistemaCerradoException e) {
            LOGGER.error("Error Monitor: No se puede Revertir el Debito <SistemaCerradoException>. ID Pago BoA [{}], Localizador [{}].",
                    pago.getId(), pago.getNroPNR()+"-"+pago.getNombre(), e);
            monitor =null;
        } catch (ImposibleLeerRespuestaException e) {
            //No se tiene respuesta del monitor de transacciones
            pago.setEstado(EstadosBoa.ERRORREVERSA.getEstado());
            pago.setMensajeRespuesta(StringUtils.substring(""+e.getCodigo()+"-"+e.getMessage(),0,200));
            pagosBoaDao.actualizar(pago, usuario);
            LOGGER.error("Error Monitor: No se puede Revertir el Debito <ImposibleLeerRespuestaException>. ID Pago BoA [{}], Localizador [{}].",
                    pago.getId(), pago.getNroPNR()+"-"+pago.getNombre(), e);
            monitor =null;
        } catch (TransaccionEfectivaException e) {
            LOGGER.error("Error Monitor: No se puede Revertir el Debito <TransaccionEfectivaException>. ID Pago BoA [{}], Localizador [{}].",
                    pago.getId(), pago.getNroPNR()+"-"+pago.getNombre(), e);
            monitor =null;
        } catch (TransaccionNoEjecutadaCorrectamenteException e) {
            LOGGER.error("Error Monitor: No se puede Revertir el Debito <TransaccionNoEjecutadaCorrectamenteException>. ID Pago BoA [{}], Localizador [{}].",
                    pago.getId(), pago.getNroPNR()+"-"+pago.getNombre(), e);
            monitor =null;
        } catch (IOException e) {
            LOGGER.error("Error Monitor: No se puede Revertir el Debito <IOException>. ID Pago BoA [{}], Localizador [{}].",
                    pago.getId(), pago.getNroPNR()+"-"+pago.getNombre(), e);
            monitor =null;
        } catch (Exception e) {
            LOGGER.error("Error Monitor: No se puede Revertir el Debito <Exception>. ID Pago BoA [{}], Localizador [{}].",
                    pago.getId(), pago.getNroPNR()+"-"+pago.getNombre(), e);
            monitor =null;
        }

        if(monitor==null){
            // OJO Retornar error en reversa
            return null;
        }

        pago.setSecuenciaReversa(monitor.getDatoRecibidoCabeceraDecimal("WNUMSEQ"));
        pago.setPosteoReversa(monitor.getDatoRecibidoCabeceraString("WPOST"));
        pago.setErrorMonitor(monitor.getDatoRecibidoCabeceraString("WREJ1"));

        return pago;

    }

    public String getAmbiente(String req){
        String respuesta = "";
        //++++++++++++++++++++++++++++++++++++++++++++  DATASOURCE
//        LOGGER.info("Obteniendo URL para <EntityManagerFactoryImpl>...");
//        String url = principal!=null?principal.getUrl():"Error:EntityManagerFactory.";
//        LOGGER.info("URL Actual = "+url);

        //++++++++++++++++++++++++++++++++++++++++++++  LIBRERIA DATAQUEUE

        LOGGER.info("Obteniendo libreria de cola para el Monitor de Transacciones");

        String libreriaCola = "";
        if(monitorBoa.getTransaccionesMonitor() !=null){

            TransaccionesMonitor monitor =  monitorBoa.getTransaccionesMonitor();

            AmbienteActual ambiente = monitor.getAmbienteActual();

            if(ambiente.isOffline()){
                libreriaCola = "Ambiente CERRRADO.";
                LOGGER.info(libreriaCola);
            }else{
                if (ambiente.isAmbienteTemporal()) {
                    libreriaCola = medioAmbiente.getValorDe(LIBRERIA_COLAS_MONITOR_TEMPORAL, LIBRERIA_COLAS_MONITOR_TEMPORAL_DEFAULT);
                    LOGGER.info("Ambiente TEMPORAL [Libreria:{}].", libreriaCola);
                    libreriaCola = "Ambiente TEMPORAL [Libreria:"+libreriaCola+"].";
                } else {
                    libreriaCola = medioAmbiente.getValorDe(LIBRERIA_COLAS_MONITOR, LIBRERIA_COLAS_MONITOR_DEFAULT);
                    LOGGER.info("Ambiente PRODUCCION [Libreria:{}].", libreriaCola);
                    libreriaCola = "Ambiente PRODUCCION [Libreria:"+libreriaCola+"].";
                }
            }
        }

        return respuesta +=libreriaCola;
    }

    private boolean cuentaSinFondo(String errorMonitor){
        String[] codigos = StringUtils.splitPreserveAllTokens(medioAmbiente.getValorDe(MONITOR_CODIGO_SINFONDO, MONITOR_CODIGO_SINFONDO_DEFUALT), ":", 0);
        for(String s : codigos){
            if(s.equalsIgnoreCase(errorMonitor)){
                return true;
            }
        }
        return false;
    }


    private String encriptar(String cadena){

        String nombreAlgoritmo = medioAmbiente.getValorDe(SKYB_BOA_ALGORITMO_ENCRIPTACION, SKYB_BOA_ALGORITMO_ENCRIPTACION_DEFAULT);
        String llaveEncriptacion = medioAmbiente.getValorDe(SKYB_BOA_LLAVE_ENCRIPTACION, SKYB_BOA_LLAVE_ENCRIPTACION_DEFAULT);

        TripleDes tripleDes = new TripleDes(nombreAlgoritmo, llaveEncriptacion);
        return tripleDes.encrypt(cadena);
    }


    private String desEncriptar(String cadena){
        String nombreAlgoritmo = medioAmbiente.getValorDe(SKYB_BOA_ALGORITMO_ENCRIPTACION, SKYB_BOA_ALGORITMO_ENCRIPTACION_DEFAULT);
        String llaveEncriptacion = medioAmbiente.getValorDe(SKYB_BOA_LLAVE_ENCRIPTACION, SKYB_BOA_LLAVE_ENCRIPTACION_DEFAULT);

        TripleDes tripleDes = new TripleDes(nombreAlgoritmo, llaveEncriptacion);
        return tripleDes.decrypt(cadena);
    }

}
