package com.bisa.bus.servicios.skyb.boa.consumer;

import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import com.bisa.bus.servicios.skyb.boa.dao.PagosBoaDao;
import com.bisa.bus.servicios.skyb.boa.entities.EstadosBoa;
import com.bisa.bus.servicios.skyb.boa.entities.PagosBoa;
import com.bisa.bus.servicios.skyb.boa.model.*;
import com.bisa.bus.servicios.skyb.boa.ws.ConsultaReservaResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.*;

/**
 * @author rchura on 09-11-15
 */
public class ConsumirBooking extends ConsumidorServiciosBoa {

    private final PagosBoaDao pagosBoaDao;

    @Inject
    public ConsumirBooking(@SQLFueraDeLinea ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente, PagosBoaDao pagosBoaDao) {
        super(executor, consumoWebDao, medioAmbiente);
        this.pagosBoaDao = pagosBoaDao;
    }

    @Override
    protected String getUrl() {
        return medioAmbiente.getValorDe(SKYB_BOA_URL_RESERVA, SKYB_BOA_URL_RESERVA_DEFAULT);
    }

    @Override
    public String getNombreMetodo() {
        return medioAmbiente.getValorDe(SKYB_BOA_METODO_BOOKING, SKYB_BOA_METODO_BOOKING_DEFAULT);
    }

    protected String getEstadosPendientes() {
        return medioAmbiente.getValorDe(SKYB_BOA_ESTADOS_PENDIENTE, SKYB_BOA_ESTADOS_PENDIENTE_DEFAULT);
    }

    protected String getMensajePendiente() {
        return medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_PENDIENTE, SKYB_BOA_MENSAJE_PENDIENTE_DEFAULT);
    }

    protected String getMensaje(EstadosBoa estadosBoa) {
        String mensaje = "";
        switch (estadosBoa) {
            case RECHAZADO:
                mensaje = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_RECHAZADO, SKYB_BOA_MENSAJE_RECHAZADO_DEFAULT);
                break;
            case AUTORIZACION:
                mensaje = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_AUTORIZACION, SKYB_BOA_MENSAJE_AUTORIZACION_DEFAULT);
                break;
            case PAGOCOMPLETADO:
                mensaje = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_COMPLETO, SKYB_BOA_MENSAJE_COMPLETO_DEFAULT);
                break;
            case PAGOREVERTIDO:
                mensaje = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_REVERTIDO, SKYB_BOA_MENSAJE_REVERTIDO_DEFAULT);
                break;
        }
        return mensaje;
    }

    protected String getParametros(Localizador localizador) {
        if (localizador == null) {
            LOGGER.error("NO se puede obtener los datos del \'Localizador (pnr, identificador)\'");
            return null;
        }
        Map<String, String> map = getEntradaBase();
        EntradaBooking booking = new EntradaBooking(map.get("credencial"), map.get("ip"), Boolean.FALSE,
                localizador, map.get("lenguaje"));
        ObjectMapper mapper = new ObjectMapper();
        String solicitud = null;
        try {
            solicitud = mapper.writeValueAsString(booking);
            LOGGER.debug("SOLICITUD JSON ES <ConsumirBooking> ---->: " + solicitud);
        } catch (JsonProcessingException e) {
            LOGGER.error("ERROR: <JsonProcessingException> al ejecutar el metodo: getParametros().", e);
        }
        return solicitud;
    }

    public ConsultaReservaResponse getBooking(Localizador localizador) {
        String respuesta;
        ConsultaReservaResponse response = null;
        //Verificar SI existe en la BD local
        PagosBoa pagosBoa = pagosBoaDao.getPagoByLocalizador(localizador.getPnr(), localizador.getIdentificador());
        if (pagosBoa != null) {
            boolean pendiente = isPendiente(pagosBoa);
            if (pendiente) {
                response = new ConsultaReservaResponse();
                response.setMensaje(new Mensaje("", MessageType.INFO, getMensajePendiente()));
                return response;
            } else {
                EstadosBoa estadosBoa = EstadosBoa.valorEnum(pagosBoa.getEstado());
                String mensaje = getMensaje(estadosBoa);
                if (StringUtils.trimToNull(mensaje) != null) {
                    response = new ConsultaReservaResponse();
                    response.setMensaje(new Mensaje("", MessageType.INFO, mensaje));
                    return response;
                }
            }
        }

        //Post a BOA
        try {
            respuesta = (String) sendPost(getParametros(localizador), localizador.getDatosConsumidor());
        } catch (IOException e) {
            LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getBooking().", e);
            response = new ConsultaReservaResponse();
            response.setMensaje(new Mensaje("900", MessageType.ERROR, "No se puede obtener respuesta del Sistema de BoA. Vuelva a intentar nuevamente."));
            return response;
        }
        if (respuesta != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            ResultadoBooking resultadoBooking = null;
            Reserva reserva = null;
            try {
                resultadoBooking = objectMapper.readValue(respuesta, ResultadoBooking.class);
                LOGGER.debug("ResultadoBooking OBJETO ---> " + resultadoBooking.getRespuesta());
                reserva = getReserva(resultadoBooking.getRespuesta());
                LOGGER.debug("OBJETO RESERVA:--->" + reserva.toString());
            } catch (JsonMappingException e) {
                LOGGER.warn("ERROR: <JsonProcessingException> al ejecutar el metodo: getBooking().", e);
            } catch (JsonParseException e) {
                LOGGER.warn("ERROR: <JsonParseException> al ejecutar el metodo: getBooking().", e);
            } catch (IOException e) {
                LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getBooking().", e);
            }
            //Armando Respuesta
            String mensajeError = null;
            if (reserva != null) {
                if (reserva.getPasajeros() == null) {
                    mensajeError = "No se puede obtener la lista de pasajeros para de la reserva [PNR: " + localizador.getPnr() + "].";
                } else {
                    for (Pasajero psj : reserva.getPasajeros()) {
                        if (psj.getPago() == null) {
                            mensajeError = "No se puede obtener el importe y la moneda para los pasajeros de la reserva [PNR: " + localizador.getPnr() + "].";
                        }
                    }
                }
                if (reserva.getVuelos() == null) {
                    mensajeError = "No se puede obtener la lista de vuelos para de la reserva [PNR: " + localizador.getPnr() + "].";
                }
                response = new ConsultaReservaResponse();
                response.setCodigoPnr(reserva.getLocalizadorResiber());
                response.setFechaReserva(reserva.getFechaCreacion());
                if (reserva.getTiempoLimite() != null)
                    response.setFechaLimite(reserva.getTiempoLimite().getFechaLimite());
                if (reserva.getVuelos() != null) response.setListaVuelos(reserva.getVuelos());
                if (reserva.getPasajeros() != null) response.setListaPasajeros(reserva.getPasajeros());

                if (StringUtils.trimToNull(mensajeError) != null) {
                    response.setMensaje(new Mensaje("800", MessageType.ERROR, mensajeError));
                }
            } else {
                response = new ConsultaReservaResponse();
                if (resultadoBooking != null) {
                    Mensaje msg = getMensaje(resultadoBooking.getRespuesta());
                    String mensaje = medioAmbiente.getValorDe(SKYB_BOA_SERVICIO_MENSAJE_PREFIJO, SKYB_BOA_SERVICIO_MENSAJE_PREFIJO_DEFAULT);
                    mensaje = MessageFormat.format(mensaje, msg.getMensaje());
                    msg.setMensaje(mensaje);
                    response.setMensaje(msg);
                }
                response.setCodigoPnr(localizador.getPnr());
                response.setFechaReserva("");
                response.setFechaLimite("");
                response.setListaVuelos(new ArrayList<>());
                response.setListaPasajeros(new ArrayList<>());
            }
        }

        return response;
    }

    private boolean isPendiente(PagosBoa pagosBoa) {
        boolean pendiente = false;
        String[] estadoPendientes = getEstadosPendientes().split(",");
        for (String estado : estadoPendientes) {
            if (estado.equals(String.valueOf(pagosBoa.getEstado()))) {
                pendiente = true;
                break;
            }
        }
        return pendiente;
    }

    protected Reserva getReserva(String cadenaJson) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(cadenaJson);

        String responsable = root.at("/reserva/responsable").toString();
        LOGGER.debug("Texto nodo Responsable:--->" + responsable);

        String nuevoJson = "{\"responsable\":" + responsable + "}";
        LOGGER.debug("NUEVO TEXTO REARMADO:--->" + nuevoJson);

        GetResponsable getResponsable = mapper.readValue(nuevoJson, GetResponsable.class);
        LOGGER.debug("OBJETO--->" + getResponsable.getResponsable().toString());

        // DATOS INICIALES
        String fechaCreacion = StringUtils.remove(root.at("/reserva/fecha_creacion/#text").toString(), "\"");
        String horaCreacion = StringUtils.remove(root.at("/reserva/hora_creacion/#text").toString(), "\"");
        String localizadorResiber = StringUtils.remove(root.at("/reserva/localizador_resiber/#text").toString(), "\"");

        // LISTA DE VUELOS
        JsonNode nodoVuelo = root.at("/reserva/vuelos");
        String jsonVuelos = nodoVuelo.toString();
        List<Vuelo> listaVuelos = null;
        LOGGER.debug("JSON LISTA VUELOS:--->" + jsonVuelos);
        if (nodoVuelo != null && StringUtils.trimToNull(jsonVuelos) != null) {

            GetVuelos getVuelos;
            try {
                getVuelos = mapper.readValue(jsonVuelos, GetVuelos.class);
                listaVuelos = getVuelos.getListaVuelos();
            } catch (JsonParseException e) {

                LOGGER.warn("JsonParseException  VUELO NOOO ES UN ARREGLO");
                GetVuelo vuelo = mapper.readValue(jsonVuelos, GetVuelo.class);
                LOGGER.debug("OBJETO VUELO:--->" + vuelo.getVuelo().toString());
                if (vuelo.getVuelo() != null) {
                    listaVuelos = new ArrayList<>();
                    listaVuelos.add(vuelo.getVuelo());
                }
            } catch (JsonMappingException e) {
                LOGGER.warn("JsonMappingException  VUELO NOOO ES UN ARREGLO");
                GetVuelo vuelo = mapper.readValue(jsonVuelos, GetVuelo.class);
                LOGGER.debug("OBJETO VUELO:--->" + vuelo.getVuelo().toString());
                if (vuelo.getVuelo() != null) {
                    listaVuelos = new ArrayList<>();
                    listaVuelos.add(vuelo.getVuelo());
                }
            }

        }

        // LISTA DE PASAJEROS
        JsonNode nodoPasajero = root.at("/reserva/pasajeros");
        String jsonPasajero = nodoPasajero.toString();
        List<Pasajero> listaPasajeros = null;
        LOGGER.debug("JSON LISTA PASAJEROS:--->" + jsonPasajero);
        if (nodoPasajero != null && StringUtils.trimToNull(jsonPasajero) != null) {
            GetPasajeros getPasajeros;
            try {
                getPasajeros = mapper.readValue(jsonPasajero, GetPasajeros.class);
                listaPasajeros = getPasajeros.getListaPasajeros();
            } catch (JsonParseException e) {
                LOGGER.warn("JsonParseException  PASAJERO NOOO ES UN ARREGLO");
                GetPasajero pasajero = mapper.readValue(jsonPasajero, GetPasajero.class);
                LOGGER.debug("OBJETO PASAJERO:--->" + pasajero.getPasajero().toString());
                if (pasajero.getPasajero() != null) {
                    listaPasajeros = new ArrayList<>();
                    listaPasajeros.add(pasajero.getPasajero());
                }
            } catch (JsonMappingException e) {

                LOGGER.warn("JsonMappingException PASAJERO NOOO ES UN ARREGLO");
                GetPasajero pasajero = mapper.readValue(jsonPasajero, GetPasajero.class);
                LOGGER.debug("OBJETO PASAJERO:--->" + pasajero.getPasajero().toString());
                if (pasajero.getPasajero() != null) {
                    listaPasajeros = new ArrayList<>();
                    listaPasajeros.add(pasajero.getPasajero());
                }
            }
        }

        // LISTA DE TARIFAS, TICKETS Y TASAS
        JsonNode nodoTarifa = root.at("/reserva/elementosTkt/fns");
        String jsonTarifas = nodoTarifa.toString();
        List<Tarifa> listaTarifas = null;
        LOGGER.debug("LISTA TARIFA:--->" + jsonTarifas);
        if (nodoTarifa != null && StringUtils.trimToNull(jsonTarifas) != null) {

            GetTarifas getTarifas;
            try {
                getTarifas = mapper.readValue(jsonTarifas, GetTarifas.class);
                listaTarifas = getTarifas.getListaTarifas();
            } catch (JsonParseException e) {

                LOGGER.warn("JsonParseException  TARIFA NOOO ES UN ARREGLO");
                GetTarifa tarifa = mapper.readValue(jsonTarifas, GetTarifa.class);
                LOGGER.debug("OBJETO TARIFA:--->" + tarifa.getTarifa().toString());
                if (tarifa.getTarifa() != null) {
                    listaTarifas = new ArrayList<>();
                    listaTarifas.add(tarifa.getTarifa());
                }
            } catch (JsonMappingException e) {

                LOGGER.warn("JsonMappingException  TARIFA NOOO ES UN ARREGLO");
                GetTarifa tarifa = mapper.readValue(jsonTarifas, GetTarifa.class);
                LOGGER.debug("OBJETO TARIFA:--->" + tarifa.getTarifa().toString());
                if (tarifa.getTarifa() != null) {
                    listaTarifas = new ArrayList<>();
                    listaTarifas.add(tarifa.getTarifa());
                }
            }

        }

        // LISTA DE CALCULO DE TARIFA

        JsonNode nodoCalculo = root.at("/reserva/elementosTkt/fcs");
        String jsonCalculo = nodoCalculo.toString();
        List<CalculoTarifa> listaCalculos = null;
        LOGGER.debug("JSON LISTA CALCULO TARIFA:--->" + jsonCalculo);
        if (nodoCalculo != null && StringUtils.trimToNull(jsonCalculo) != null) {
            GetCalculoTarifas calculoTarifas;

            try {
                calculoTarifas = mapper.readValue(jsonCalculo, GetCalculoTarifas.class);
                listaCalculos = calculoTarifas.getListaCalculos();
            } catch (JsonParseException e) {

                LOGGER.warn("JsonParseException  TARIFA NOOO ES UN ARREGLO");
                GetCalculoTarifa calculoTarifa = mapper.readValue(jsonCalculo, GetCalculoTarifa.class);
                LOGGER.debug("OBJETO TARIFA:--->" + calculoTarifa.getCalculo().toString());
                if (calculoTarifa.getCalculo() != null) {
                    listaCalculos = new ArrayList<>();
                    listaCalculos.add(calculoTarifa.getCalculo());
                }
            } catch (JsonMappingException e) {

                LOGGER.warn("JsonMappingException  TARIFA NOOO ES UN ARREGLO");
                GetCalculoTarifa calculoTarifa = mapper.readValue(jsonCalculo, GetCalculoTarifa.class);
                LOGGER.debug("OBJETO TARIFA:--->" + calculoTarifa.getCalculo().toString());
                if (calculoTarifa.getCalculo() != null) {
                    listaCalculos = new ArrayList<>();
                    listaCalculos.add(calculoTarifa.getCalculo());
                }

            }

        }

        // TIEMPO LIMITE
        String tiempoLimite = root.at("/reserva/tl").toString();
        String jsonTiempoLimie = "{\"tl\":" + tiempoLimite + "}";
        LOGGER.debug("NUEVO TEXTO JSON:--->" + jsonTiempoLimie);

        GetTiempoLimite getTiempoLimite = null;
        if (StringUtils.trimToNull(jsonTiempoLimie) != null) {
            getTiempoLimite = mapper.readValue(jsonTiempoLimie, GetTiempoLimite.class);
            LOGGER.debug("OBJETO--->" + getTiempoLimite.getTiempoLimite().toString());
        }



        // CREANDO EL OBJETO RESERVA
        Reserva reserva = new Reserva();
        reserva.setFechaCreacion(fechaCreacion);
        reserva.setHoraCreacion(horaCreacion);
        reserva.setLocalizadorResiber(localizadorResiber);
        reserva.setVuelos(listaVuelos);
        reserva.setPasajeros(listaPasajeros);
        if (getTiempoLimite != null) reserva.setTiempoLimite(getTiempoLimite.getTiempoLimite());
        if (getResponsable != null) reserva.setResponsable(getResponsable.getResponsable());

        TicketItinerario ticketItinerario = new TicketItinerario();
        ticketItinerario.setTarifa(listaTarifas);
        ticketItinerario.setCalculoTarifa(listaCalculos);
        reserva.setElementosTkt(ticketItinerario);

        return reserva;
    }
}