package com.bisa.bus.servicios.skyb.boa.consumer;

import com.bisa.bus.servicios.skyb.boa.model.EntradaToken;
import com.bisa.bus.servicios.skyb.boa.model.Localizador;
import com.bisa.bus.servicios.skyb.boa.model.ResultadoToken;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.*;

/**
 * Created by rchura on 04-11-15.
 */
public class ConsumirToken extends ConsumidorServiciosBoa {

    @Inject
    public ConsumirToken(@SQLFueraDeLinea ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente) {
        super(executor, consumoWebDao, medioAmbiente);
    }

    @Override
    protected String getUrl() {
        String url =  medioAmbiente.getValorDe(SKYB_BOA_URL_TOKEN, SKYB_BOA_URL_TOKEN_DEFAULT);
        return url;
    }

    @Override
    public String getNombreMetodo() {
        return medioAmbiente.getValorDe(SKYB_BOA_METODO_TOKEN, SKYB_BOA_METODO_TOKEN_DEFAULT);

    }


    public String getParametros(Localizador localizador){

        if(localizador==null){
            LOGGER.error("NO se puede obtener los datos del \'Localizador (pnr, identificador)\'");
            return null;
        }

        Map<String, String> map = getEntradaBase();
        EntradaToken entradaToken = new EntradaToken(map.get("credencial"),map.get("ip"), Boolean.FALSE, localizador);

        ObjectMapper mapper = new ObjectMapper();
        String solicitud = null;
        try {
            solicitud = mapper.writeValueAsString(entradaToken);
            LOGGER.debug("SOLICITUD JSON ES <ConsumirToken> ---->: " + solicitud);
        } catch (JsonProcessingException e) {
            LOGGER.error("ERROR: <JsonProcessingException> al ejecutar el metodo: getParametros().", e);
            solicitud = null;
        }

        return solicitud;
    }

    public ResultadoToken getToken(Localizador localizador){

        String respuesta = null;
        try {
            respuesta = (String) sendPost(getParametros(localizador), localizador.getDatosConsumidor());

        } catch (IOException e) {
            LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getToken().", e);
            respuesta = null;
        }

        ResultadoToken resultadoToken = null;
        if(respuesta!=null){
            try {
                resultadoToken = new ObjectMapper().readValue(respuesta, ResultadoToken.class);
            }catch (JsonMappingException e) {
                LOGGER.warn("ERROR: <JsonMappingException> al ejecutar el metodo: getToken().", e);
                resultadoToken = null;
            } catch (JsonParseException e) {
                LOGGER.warn("ERROR: <JsonParseException> al ejecutar el metodo: getToken().", e);
                resultadoToken = null;
            } catch (IOException e) {
                LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getToken().", e);
                resultadoToken = null;
            }
        }

        return resultadoToken;
    }

}
