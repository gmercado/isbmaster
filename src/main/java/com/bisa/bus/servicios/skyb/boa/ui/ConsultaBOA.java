package com.bisa.bus.servicios.skyb.boa.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.ControlGroupBorder;
import com.bisa.bus.servicios.skyb.boa.api.ServicioBoa;
import com.bisa.bus.servicios.skyb.boa.model.ConsultaReservaForm;
import com.bisa.bus.servicios.skyb.boa.model.ConsultaReservaResponseForm;
import com.bisa.bus.servicios.skyb.boa.ws.ConsultaReservaRequest;
import com.bisa.bus.servicios.skyb.boa.ws.ConsultaReservaResponse;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.validation.validator.StringValidator;

/**
 * @author rsalvatierra on 26/09/2016.
 */
@AuthorizeInstantiation({RolesBisa.ISB_BOA})
@Menu(value = "Consulta a BoA", subMenu = SubMenu.SERVICIOS_BOA)
public class ConsultaBOA extends BisaWebPage {

    @Inject
    ServicioBoa servicioBoa;

    private IModel<ConsultaReservaForm> objectModel;

    public ConsultaBOA() {
        inicio(new ConsultaReservaForm());
    }

    public ConsultaBOA(ConsultaReservaForm solicitud) {
        inicio(solicitud);
    }

    private void inicio(ConsultaReservaForm solicitud) {
        RequiredTextField<String> pnr;
        TextField<String> grupo;
        this.objectModel = new CompoundPropertyModel<>(solicitud);
        //Form
        Form<ConsultaReservaForm> form = new Form<>("buscarReserva", objectModel);
        add(form);
        //Número Documento
        pnr = new RequiredTextField<>("codigoPnr");
        pnr.add(StringValidator.minimumLength(5));
        pnr.add(StringValidator.maximumLength(6));
        pnr.setOutputMarkupId(true);
        form.add(new ControlGroupBorder("codigoPnr", pnr, feedbackPanel, Model.of("C\u00F3digo PNR:")).add(pnr));
        //Complemento
        grupo = new TextField<>("apellidoGrupo");
        grupo.add(StringValidator.maximumLength(25));
        form.add(new ControlGroupBorder("apellidoGrupo", grupo, feedbackPanel, Model.of("Apellido/Grupo:")).add(grupo));

        //Boton consulta
        form.add(new Button("consulta") {
            @Override
            public void onSubmit() {
                //Obtender datos
                ConsultaReservaRequest request = new ConsultaReservaRequest();
                request.setCodigoPnr(getObjetoModel().getCodigoPnr());
                request.setApellidoGrupo(getObjetoModel().getApellidoGrupo());
                request.setSistema("");
                request.setIpOrigen("");
                request.setCliente("");
                request.setUsuario("");
                ConsultaReservaResponse response = servicioBoa.consultarReserva(request);
                if (response != null) {
                    ConsultaReservaResponseForm resp = new ConsultaReservaResponseForm(response);
                    setResponsePage(new RespuestaBOA(getObjetoModel(), resp));
                } else {
                    getSession().error("Error al consultar BOA");
                }
            }
        });
        form.add(new Button("limpiar") {
            @Override
            public void onSubmit() {
                if ((StringUtils.isNotBlank(getObjetoModel().getCodigoPnr()) && StringUtils.isNotEmpty(getObjetoModel().getCodigoPnr()))
                        || (StringUtils.isNotBlank(getObjetoModel().getApellidoGrupo()) && StringUtils.isNotEmpty(getObjetoModel().getApellidoGrupo()))) {
                    setResponsePage(new ConsultaBOA());
                } else {
                    form.clearInput();
                }
            }
        }.setDefaultFormProcessing(false));
    }

    private ConsultaReservaForm getObjetoModel() {
        return this.objectModel.getObject();
    }
}
