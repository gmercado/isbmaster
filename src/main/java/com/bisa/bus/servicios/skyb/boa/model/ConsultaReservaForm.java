package com.bisa.bus.servicios.skyb.boa.model;

import java.io.Serializable;

/**
 * @author rchura on 11-11-15
 */
public class ConsultaReservaForm implements Serializable {

    protected String codigoPnr;

    protected String apellidoGrupo;

    public ConsultaReservaForm() {
    }

    public ConsultaReservaForm(String codigoPnr, String apellidoGrupo) {
        this.codigoPnr = codigoPnr;
        this.apellidoGrupo = apellidoGrupo;
    }


    public String getCodigoPnr() {
        return codigoPnr;
    }

    public void setCodigoPnr(String codigoPnr) {
        this.codigoPnr = codigoPnr;
    }

    public String getApellidoGrupo() {
        return apellidoGrupo;
    }

    public void setApellidoGrupo(String apellidoGrupo) {
        this.apellidoGrupo = apellidoGrupo;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("RequestBoa{");
        sb.append(", codigoPnr='").append(codigoPnr).append('\'');
        sb.append(", apellidoGrupo='").append(apellidoGrupo).append('\'');
        sb.append('}');
        return sb.toString();
    }


}
