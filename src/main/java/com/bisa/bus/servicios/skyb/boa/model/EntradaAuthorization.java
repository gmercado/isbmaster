package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by rchura on 09-11-15.
 */
@XmlRootElement
public class EntradaAuthorization extends Entrada {

    /**
     * token
     * Cadena de caracteres alfanuméricos. De hasta 250 caracteres.
     * Identificador único de la transacción obtenida por la ejecución del método GetToken
     */
    @XmlElement(name = "token")
    @JsonProperty(value = "token")
    private String token;


    /**
     * language
     * AA {A->Z sin Ñ).	Idioma en el que se devuelven los mensajes al consumidor.
     * Los valores aceptados son los siguientes:
     * ES: Español,  EN: Inglés
     * Si se envía un valor diferente de los anteriores, el sistema utilizará el idioma por defecto del usuario.
     */
    @XmlElement(name = "language")
    @JsonProperty(value = "language")
    private String lenguaje;


    /**
     * AuthorizationCode
     * Cadena de caracteres
     * Este dato debe ser enviado cifrado con algoritmo 3DES y con la llave proporcionada para la Entidad Financiera
     */
    @XmlElement(name = "authorizationCode")
    @JsonProperty(value = "authorizationCode")
    private String codigoAutorizacion;


    /**
     * Endorsement
     * Cadena de caracteres
     * Texto que representa la razón social a la que se emitirá la factura por la emisión del boleto.
     * Por Ej.: “NIT;4212442;RAZONSOCIAL”.
     */
    @XmlElement(name = "endorsement")
    @JsonProperty(value = "endorsement")
    private String endosoFactura;


    public EntradaAuthorization(String credenciales, String ip, Boolean xmlOrJson, Localizador localizador,
                                String token, String lenguaje, String codigoAutorizacion, String endosoFactura) {
        super(credenciales, ip, xmlOrJson, localizador);
        this.token = token;
        this.lenguaje = lenguaje;
        this.codigoAutorizacion = codigoAutorizacion;
        this.endosoFactura = endosoFactura;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLenguaje() {
        return lenguaje;
    }

    public void setLenguaje(String lenguaje) {
        this.lenguaje = lenguaje;
    }

    public String getCodigoAutorizacion() {
        return codigoAutorizacion;
    }

    public void setCodigoAutorizacion(String codigoAutorizacion) {
        this.codigoAutorizacion = codigoAutorizacion;
    }

    public String getEndosoFactura() {
        return endosoFactura;
    }

    public void setEndosoFactura(String endosoFactura) {
        this.endosoFactura = endosoFactura;
    }
}
