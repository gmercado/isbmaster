package com.bisa.bus.servicios.skyb.boa.ws;

import com.bisa.bus.servicios.skyb.boa.api.ProcesosBoa;
import com.bisa.bus.servicios.skyb.boa.api.ServiciosBoa;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jws.WebService;

/**
 * @author Roger Chura
 * @version $Id: WebServiceBoaEndPoint.java; oct 30, 2015 10:50 PM $
 */
@WebService(targetNamespace = WebServiceBoa.NAMESPACE)
public class WebServiceBoaEndPoint implements WebServiceBoa {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebServiceBoaEndPoint.class);

    @Inject
    private ServiciosBoa serviciosBoa;

    @Inject
    private ProcesosBoa procesosBoa;

    @Override
    public ConsultaReservaResponse getReserva(ConsultaReservaRequest request) {
        ConsultaReservaResponse response;
        LOGGER.debug("SOLICITUD OBJETO---> " + request);
        if (serviciosBoa == null) {
            LOGGER.debug(" OBJETO <serviciosBoa> ES NULO---> " + request);
            response = new ConsultaReservaResponse();
        } else {
            LOGGER.debug(" OBJETO <serviciosBoa> EXISTE---> " + request);
            response = serviciosBoa.consultarReserva(request);
        }
        LOGGER.debug("RESPUESTA SERVICIO <getReserva> -->" + response);
        return response;
    }

    @Override
    public TokenResponse getToken(TokenRequest request) {
        TokenResponse response;
        if (serviciosBoa == null) {
            LOGGER.debug(" OBJETO <serviciosBoa> ES NULO---> " + request);
            response = new TokenResponse();
        } else {
            LOGGER.debug(" OBJETO <serviciosBoa> EXISTE---> " + request);
            response = serviciosBoa.obtenerToken(request);
        }
        return response;
    }

    @Override
    public PagoReservaResponse payReserva(PagoReservaRequest request) {
        PagoReservaResponse response;
        if (serviciosBoa == null) {
            LOGGER.debug(" OBJETO <serviciosBoa> ES NULO---> " + request);
            response = new PagoReservaResponse();
        } else {
            LOGGER.debug(" OBJETO <serviciosBoa> EXISTE---> " + request);
            response = procesosBoa.pagoNormal(request);
        }
        return response;
    }

    @Override
    public TicketPagoResponse getTickets(TicketPagoRequest request) {
        TicketPagoResponse response;
        if (serviciosBoa == null) {
            LOGGER.debug(" OBJETO <serviciosBoa> ES NULO---> " + request);
            response = new TicketPagoResponse();
        } else {
            LOGGER.debug(" OBJETO <serviciosBoa> EXISTE---> " + request);
            response = serviciosBoa.obtenerTicket(request);
        }
        return response;

    }

    @Override
    public BilleteMailResponse getInvoiceMail(BilleteMailRequest request) {
        BilleteMailResponse response;
        if (serviciosBoa == null) {
            LOGGER.debug(" OBJETO <serviciosBoa> ES NULO---> " + request);
            response = new BilleteMailResponse();
        } else {
            LOGGER.debug(" OBJETO <serviciosBoa> EXISTE---> " + request);
            response = serviciosBoa.enviarBilleteMail(request);
        }
        return response;
    }

    @Override
    public BilletePdfResponse getInvoicePdf(BilletePdfRequest request) {
        BilletePdfResponse response;
        if (serviciosBoa == null) {
            LOGGER.debug(" OBJETO <serviciosBoa> ES NULO---> " + request);
            response = new BilletePdfResponse();
        } else {
            LOGGER.debug(" OBJETO <serviciosBoa> EXISTE---> " + request);
            response = serviciosBoa.obtenerBilletePdf(request);
        }
        return response;
    }

    @Override
    public String getAmbiente(String request) {
        return procesosBoa.getAmbiente(request);
    }
}
