package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Created by rchura on 06-11-15.
 */
@JsonRootName(value = "posicion")
public class Posicion {

    /**
     * El par (pagina, numero) identifica de manera única el atributo en el PNR.
      */

    @JsonProperty(value ="numPagina")
    private Long pagina;

    @JsonProperty(value ="numLinea")
    private Long linea;

    public Long getPagina() {
        return pagina;
    }

    public void setPagina(Long pagina) {
        this.pagina = pagina;
    }

    public Long getLinea() {
        return linea;
    }

    public void setLinea(Long linea) {
        this.linea = linea;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Posicion{");
        sb.append("pagina=").append(pagina);
        sb.append(", linea=").append(linea);
        sb.append('}');
        return sb.toString();
    }
}
