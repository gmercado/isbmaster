package com.bisa.bus.servicios.skyb.boa.ws;

/**
 * Created by rchura on 20-11-15.
 */
public class TicketPagoRequest extends  RequestBoa {

    private Long pagoID;

    public TicketPagoRequest(){}

    public TicketPagoRequest(String cliente, String usuario, String codigoPnr, String apellidoGrupo, String ipOrigen, String sistema) {
        super(cliente, usuario, codigoPnr, apellidoGrupo, ipOrigen, sistema);
    }

    public Long getPagoID() {
        return pagoID;
    }

    public void setPagoID(Long pagoID) {
        this.pagoID = pagoID;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TicketPagoRequest{");
        sb.append(super.toString());
        sb.append("pagoID=").append(pagoID);
        sb.append('}');
        return sb.toString();
    }


}
