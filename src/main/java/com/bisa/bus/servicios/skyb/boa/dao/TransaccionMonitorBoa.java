package com.bisa.bus.servicios.skyb.boa.dao;

import com.bisa.bus.servicios.skyb.boa.entities.PagosBoa;
import com.google.inject.Inject;
import bus.env.api.MedioAmbiente;
import bus.monitor.api.*;
import bus.monitor.entities.TransaccionAudit;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Random;

import static bus.env.api.Variables.*;

/**
 * Created by rchura on 19-11-15.
 */
public class TransaccionMonitorBoa {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransaccionMonitorBoa.class);

    private final TransaccionesMonitor transaccionesMonitor;
    private final MedioAmbiente medioAmbiente;

    @Inject
    public TransaccionMonitorBoa(TransaccionesMonitor transaccionesMonitor, MedioAmbiente medioAmbiente) {
        this.transaccionesMonitor = transaccionesMonitor;
        this.medioAmbiente = medioAmbiente;
    }

    public IRespuestasMonitor procesarDebito(PagosBoa pagosBoa) throws SistemaCerradoException, ImposibleLeerRespuestaException,
            TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {

        //Obtine la version del monitor
        int version = medioAmbiente.getValorIntDe(MONITOR_VERSION, MONITOR_VERSION_DEFAULT);
        String grupo = null;

        WithMonitor withMonitor = obtenerMonitorDebito(pagosBoa);
        IRespuestasMonitor monitor = (IRespuestasMonitor) transaccionesMonitor.ejecutarTransaccionMonitor(version, grupo, withMonitor);
        return monitor;
    }

    public void revertirDebito(PagosBoa pagosBoa, IRespuestasMonitor monitor, TransaccionAudit transaccionAudit){

        // Obtiene el GRUPO
        String grupo = monitor.getDatoRecibidoCabeceraString("WFLG2");

        try {
            transaccionesMonitor.revertir(monitor.obtenerObjetoCompleto(transaccionAudit), monitor.getCabeceraRecepcion(), grupo);
        } catch (SistemaCerradoException e) {
            e.printStackTrace();
        } catch (ImposibleLeerRespuestaException e) {
            e.printStackTrace();
        } catch (TransaccionEfectivaException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public IRespuestasMonitor revertirDebito(PagosBoa pagosBoa) throws SistemaCerradoException, ImposibleLeerRespuestaException,
            TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {

        //Obtine la version del monitor
        int version = medioAmbiente.getValorIntDe(MONITOR_VERSION, MONITOR_VERSION_DEFAULT);
        String grupo = null;

        LOGGER.info("Ejecutando la reversion en el sistema de cajas. Secuencia [{}] y Nro. Caja [{}].", pagosBoa.getNroSecuencia(), pagosBoa.getNroCaja());
        WithMonitor withMonitor = obtenerMonitorReversa(pagosBoa);
        IRespuestasMonitor monitor = (IRespuestasMonitor) transaccionesMonitor.ejecutarTransaccionMonitor(version, grupo, withMonitor);

        return monitor;

    }




    public WithMonitor obtenerMonitorDebito(PagosBoa pagosBoa){

        return new WithMonitor() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {

                // Obtiene la transacción
                String transaccion = medioAmbiente.getValorDe(SKYB_BOA_TRANSACCION_MONITOR, SKYB_BOA_TRANSACCION_MONITOR_DEFAULT);

                String glosa = "Pago Reserva BoA PNR:"+ StringUtils.trimToEmpty(pagosBoa.getNroPNR());

                LOGGER.debug("Generando plantilla para la transaccion ST.");

                String deviceReal = "BOA" + StringUtils.leftPad("" + new Random().nextInt(99999), 7, "0");
                monitor.setDatoCabeceraEnvio("WDEVICE", deviceReal);
                //monitor.setDatoCabeceraEnvio("WCANAL", "5");

                monitor.setDatoCabeceraEnvio("WNUMUSR", 5101);
                monitor.setDatoCabeceraEnvio("WAPRUSER", "EBANKING");
                monitor.setDatoCabeceraEnvio("WVERIF", "S");
                monitor.setDatoCabeceraEnvio("WTPODTA", 1);

                /////////////////
                monitor.setDatoCabeceraEnvio("WCODTRN", transaccion);
                monitor.setCuerpo1EnvioFormat("PSR9021");
                monitor.setCuerpo1RecepcionFormat("PSR9120");
                monitor.setDatoCuerpo1("WCLSTRN", 4);
                monitor.setDatoCuerpo1("WIMPTOT", pagosBoa.getImporteTotal());
                monitor.setDatoCuerpo1("WTPOIMP", "2");
                monitor.setDatoCuerpo1("WCODMON", pagosBoa.getMonedaImporteTotal());
                monitor.setDatoCuerpo1("WCUENTA2", pagosBoa.getCuentaDebito());
                monitor.setDatoCuerpo1("WTPOCTA2", pagosBoa.getTipoCtaDebito());
                monitor.setDatoCuerpo1("WCODMON2", pagosBoa.getMonedaCtaDebito());
                monitor.setDatoCuerpo1("WNOTA", StringUtils.left(StringUtils.trimToEmpty(pagosBoa.getNota()), 30));
                // Para envio de la descripcion de la glosa que debe registrarse en la transaccion ST50(Afecta al extracto del cliente)
                monitor.setCuerpo2EnvioFormat("PSR9035");
                monitor.setDatoCuerpo2("WDESCTRN", StringUtils.left(glosa, 30));

            }

            @Override
            public Object procesar(IRespuestasMonitor monitor) throws IOException, SistemaCerradoException, TransaccionNoEjecutadaCorrectamenteException {

                //TODO: Se puede armar otro objeto como respuesta

                return monitor;
            }
        };
     }



    public WithMonitor obtenerMonitorReversa(PagosBoa pagosBoa){

        return new WithMonitor() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {

                // Obtiene la transacción
                String transaccion = medioAmbiente.getValorDe(SKYB_BOA_TRANSACCION_MONITOR, SKYB_BOA_TRANSACCION_MONITOR_DEFAULT);

                String glosa = "Pago Reserva BoA PNR:"+ StringUtils.trimToEmpty(pagosBoa.getNroPNR());

                LOGGER.debug("Generando plantilla para la transaccion ST.");

                String deviceReal = "BOA" + StringUtils.leftPad("" + new Random().nextInt(99999), 7, "0");
                monitor.setDatoCabeceraEnvio("WDEVICE", deviceReal);
                //monitor.setDatoCabeceraEnvio("WCANAL", "5");

                monitor.setDatoCabeceraEnvio("WNUMUSR", 5101);
                monitor.setDatoCabeceraEnvio("WAPRUSER", "EBANKING");

                ////////////////
                monitor.setDatoCabeceraEnvio("WTPODTA", 6);
                monitor.setDatoCabeceraEnvio("WREVE", "S");

                /////////////////
                monitor.setDatoCabeceraEnvio("WCODTRN", transaccion);
                monitor.setDatoCabeceraEnvio("WNUMUSR", pagosBoa.getNroCaja());
                monitor.setDatoCabeceraEnvio("WNUMSEQ", pagosBoa.getNroSecuencia());

            }

            @Override
            public Object procesar(IRespuestasMonitor monitor) throws IOException, SistemaCerradoException, TransaccionNoEjecutadaCorrectamenteException {

                //TODO: Se puede armar otro objeto como respuesta

                return monitor;
            }
        };
    }

    public TransaccionesMonitor getTransaccionesMonitor() {
        return transaccionesMonitor;
    }
}
