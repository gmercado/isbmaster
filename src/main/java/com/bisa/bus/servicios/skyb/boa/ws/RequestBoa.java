package com.bisa.bus.servicios.skyb.boa.ws;

/**
 * Created by rchura on 20-11-15.
 */
public abstract class RequestBoa {

    protected String cliente;

    protected String usuario;

    protected String codigoPnr;

    protected String apellidoGrupo;

    protected String ipOrigen;

    protected String sistema;

    protected RequestBoa() {
    }

    protected RequestBoa(String cliente, String usuario, String codigoPnr, String apellidoGrupo, String ipOrigen, String sistema) {
        this.cliente = cliente;
        this.usuario = usuario;
        this.codigoPnr = codigoPnr;
        this.apellidoGrupo = apellidoGrupo;
        this.ipOrigen = ipOrigen;
        this.sistema = sistema;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCodigoPnr() {
        return codigoPnr;
    }

    public void setCodigoPnr(String codigoPnr) {
        this.codigoPnr = codigoPnr;
    }

    public String getApellidoGrupo() {
        return apellidoGrupo;
    }

    public void setApellidoGrupo(String apellidoGrupo) {
        this.apellidoGrupo = apellidoGrupo;
    }

    public String getIpOrigen() {
        return ipOrigen;
    }

    public void setIpOrigen(String ipOrigen) {
        this.ipOrigen = ipOrigen;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("RequestBoa{");
        sb.append("cliente='").append(cliente).append('\'');
        sb.append(", usuario='").append(usuario).append('\'');
        sb.append(", codigoPnr='").append(codigoPnr).append('\'');
        sb.append(", apellidoGrupo='").append(apellidoGrupo).append('\'');
        sb.append(", ipOrigen='").append(ipOrigen).append('\'');
        sb.append(", sistema='").append(sistema).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
