package com.bisa.bus.servicios.skyb.boa.consumer;

import com.bisa.bus.servicios.skyb.boa.model.*;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.*;

/**
 * Created by rchura on 04-11-15.
 */
public class ConsumirConciliacionDiaria extends ConsumidorServiciosBoa {

    @Inject
    public ConsumirConciliacionDiaria(@SQLFueraDeLinea ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente) {
        super(executor, consumoWebDao, medioAmbiente);
    }

    @Override
    protected String getUrl() {
        String url =  medioAmbiente.getValorDe(SKYB_BOA_URL_CONCILIACION, SKYB_BOA_URL_CONCILIACION_DEFAULT);
        return url;
    }

    @Override
    public String getNombreMetodo() {
        return medioAmbiente.getValorDe(SKYB_BOA_METODO_CONCILIACION_DIARIA, SKYB_BOA_METODO_CONCILIACION_DIARIA_DEFAULT);

    }


    public String getParametros(String fecha){


        Map<String, String> map = getEntradaBase();
        EntradaConciliacionDiaria entradaConcilDiaria = new EntradaConciliacionDiaria(map.get("credencial"),map.get("ip"),
                Boolean.FALSE, null, map.get("lenguaje"), fecha);

        ObjectMapper mapper = new ObjectMapper();
        String solicitud = null;
        try {
            solicitud = mapper.writeValueAsString(entradaConcilDiaria);
            LOGGER.debug("SOLICITUD JSON ES <ConsumirConciliacionDiaria> ---->: " + solicitud);
        } catch (JsonProcessingException e) {
            LOGGER.error("ERROR: <JsonProcessingException> al ejecutar el metodo: getParametros().", e);
            solicitud = null;
        }

        return solicitud;
    }

    public ResultadoConciliacionDiaria getConcilicionDiaria(String fecha){
        ResultadoConciliacionDiaria resultadoConciliacion = null;
        Mensaje mensaje = null;
        String respuesta;
        try {
            respuesta = (String) sendPost(getParametros(fecha), null);
        } catch (IOException e) {
            LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getConcilicionDiaria().", e);
            respuesta = null;
        }

        if(respuesta!=null){
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                resultadoConciliacion = objectMapper.readValue(respuesta, ResultadoConciliacionDiaria.class);
                LOGGER.debug("ResultadoConciliacionDiaria OBJETO ---> " + resultadoConciliacion.toString());
            }catch (JsonMappingException e) {
                LOGGER.warn("ERROR: <JsonMappingException> al ejecutar el metodo: getToken().", e);
                mensaje = getMensaje(resultadoConciliacion.getRespuesta());
            } catch (JsonParseException e) {
                LOGGER.warn("ERROR: <JsonParseException> al ejecutar el metodo: getToken().", e);
                mensaje = getMensaje(resultadoConciliacion.getRespuesta());
            } catch (IOException e) {
                LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getToken().", e);
                mensaje = getMensaje(resultadoConciliacion.getRespuesta());
            }
        }

        if(resultadoConciliacion!=null &&  resultadoConciliacion.getRespuesta()!=null){
            resultadoConciliacion = getResultadoConciliacionDiaria(resultadoConciliacion.getRespuesta());
            LOGGER.debug("OBJETO FINAL ResultadoConciliacionDiaria:--->" + resultadoConciliacion.toString());
        }

        return resultadoConciliacion;
    }


    public ResultadoConciliacionDiaria getResultadoConciliacionDiaria(String cadenaJson){
        ListaReporteDiario listaReporteDiario=null;
        Mensaje mensaje = null;
        List<ReporteDiario> listaReporteSimple = null;
        String jsonDailyReconciliation=null;
        ObjectMapper mapper = new ObjectMapper();
        ResultadoConciliacionDiaria resultadoConciliacionDiaria = new ResultadoConciliacionDiaria();

        try {
            JsonNode root = mapper.readTree(cadenaJson);
            JsonNode nodoDailyReconciliacion = root.at("/ArrayOfResultDailyReconciliation");
            jsonDailyReconciliation= nodoDailyReconciliacion.toString();
            LOGGER.debug("jsonDailyReconciliation STRING ---> " + jsonDailyReconciliation);

            if(StringUtils.trimToNull(jsonDailyReconciliation)!=null){
                listaReporteDiario = mapper.readValue(jsonDailyReconciliation, ListaReporteDiario.class);
                LOGGER.debug("ListaReporteDiario OBJETO ---> " + listaReporteDiario.toString());
            }

        } catch (JsonMappingException e) {
            LOGGER.warn("Error JsonMappingException -->" + cadenaJson, e);
            mensaje = getMensaje(cadenaJson);
            listaReporteDiario=null;
        } catch (JsonParseException e) {
            LOGGER.warn("Error JsonParseException -->" + cadenaJson, e);
            mensaje = getMensaje(cadenaJson);
            listaReporteDiario=null;
        } catch (IOException e) {
            LOGGER.error("Error IOException -->" + cadenaJson, e);
            mensaje = getMensaje(cadenaJson);
            listaReporteDiario=null;
        }

        GetReporteDiario reporteDiario = null;
        if(listaReporteDiario==null){
            try {
                reporteDiario = mapper.readValue(jsonDailyReconciliation, GetReporteDiario.class);
                LOGGER.debug("GetReporteDiario OBJETO ---> " + reporteDiario.toString());
                if(reporteDiario!=null){
                    listaReporteSimple = new ArrayList<ReporteDiario>();
                    listaReporteSimple.add(reporteDiario.getReporteDiario());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        if(listaReporteDiario!=null) resultadoConciliacionDiaria.setReporteDiario(listaReporteDiario.getReporteDiario());
        if(listaReporteSimple!=null) resultadoConciliacionDiaria.setReporteDiario(listaReporteSimple);
        if(mensaje!=null) resultadoConciliacionDiaria.setMensaje(mensaje);
        resultadoConciliacionDiaria.setRespuesta(cadenaJson);

        return resultadoConciliacionDiaria;
    }


}
