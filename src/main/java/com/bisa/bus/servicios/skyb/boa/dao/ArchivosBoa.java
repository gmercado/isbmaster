package com.bisa.bus.servicios.skyb.boa.dao;

import bus.env.api.MedioAmbiente;
import bus.plumbing.file.ArchivoBase;
import bus.plumbing.file.ArchivoProfile;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.skyb.boa.entities.MonedasBoa;
import com.bisa.bus.servicios.skyb.boa.entities.PagosBoa;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static bus.env.api.Variables.SKYB_BOA_ARCHIVO_BASE;
import static bus.env.api.Variables.SKYB_BOA_ARCHIVO_BASE_DEFAULT;

/**
 * @author by rchura on 27-01-16.
 */
public class ArchivosBoa {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArchivosBoa.class);
    private static final String ENTIDAD = "BISA";

    private final ArchivoBase archivoBase;
    private final MedioAmbiente medioAmbiente;
    private final PagosBoaDao pagosBoaDao;

    @Inject
    public ArchivosBoa(ArchivoBase archivoBase, MedioAmbiente medioAmbiente, PagosBoaDao pagosBoaDao) {
        this.archivoBase = archivoBase;
        this.medioAmbiente = medioAmbiente;
        this.pagosBoaDao = pagosBoaDao;
    }


    public ArchivoProfile generaArchivoCobranza(Date fechaDesde, Date fechaHasta) {
        File archivo;
        ArchivoProfile archivoProfile = null;
        String nombreArchivo = medioAmbiente.getValorDe(SKYB_BOA_ARCHIVO_BASE, SKYB_BOA_ARCHIVO_BASE_DEFAULT);

        //nombreArchivo = MessageFormat.format(nombreArchivo, FormatosUtils.fechaFormateadaConYYYYMMDD(fechaDesde));
        //nombreArchivo = ""+FormatosUtils.fechaFormateadaConYYMMDD(fechaDesde)+nombreArchivo+".csv";
        nombreArchivo = "" + FormatosUtils.fechaFormateadaConYYMMDD(fechaDesde) + nombreArchivo + ".txt";
        try {
            List<PagosBoa> listaPagos = pagosBoaDao.getPagosProcesadosByFecha(fechaDesde, fechaHasta);
            if (listaPagos != null && listaPagos.size() > 0) {
                archivo = archivoBase.crearArchivoSimple(nombreArchivo);

                if (!archivo.exists()) {
                    if (!archivo.createNewFile()) {
                        LOGGER.warn("Verificar archivo de conciliacion BOA. ");
                    }
                }
                archivoProfile = poblarArchivo(archivo, listaPagos, fechaDesde, fechaHasta);
            }
        } catch (IOException e) {
            LOGGER.error("Error: Ocurrio un error al generar el archivo de recaudaciones. ", e);
            archivoProfile = null;
        }
        return archivoProfile;
    }

    private ArchivoProfile poblarArchivo(File archivo, List<PagosBoa> listaPagos, Date fechaDesde, Date fechaHasta) {

        if (archivo == null) return null;
        if (listaPagos == null || listaPagos.size() <= 0) return null;

        String fechaYYYY = FormatosUtils.fechaFormateadaConYYYY(fechaDesde);
        String fechaHoraDesde = FormatosUtils.fechaHoraFormateadaConYY(fechaDesde);
        String fechaHoraHasta = FormatosUtils.fechaHoraFormateadaConYY(fechaHasta);

        long nroFilas = 0L;
        long valorFilas = 0L;

        FileWriter archivoWrite;
        try {
            archivoWrite = new FileWriter(archivo.getAbsoluteFile());

            BufferedWriter bufferWriter = new BufferedWriter(archivoWrite);

            StringBuilder sb = new StringBuilder();
            //sb.append("Entity,IP,RequestDateTime,IssueDateTime,PNR,IdentifierPNR,Authorization,TotalAmount,Currency,State\n");
            sb.append("BANCO BISA S.A.                                            DETALLE DE COBRANZA PARA BOLIVIANA DE AVIACION                              ");
            sb.append(fechaYYYY);
            sb.append("\nSUCURSAL: NACIONAL                                              DEL ");
            sb.append(fechaHoraDesde);
            sb.append(" AL ");
            sb.append(fechaHoraHasta);
            sb.append("\nENTIDAD       IP        FECHA SOLICITUD      FECHA EMISION        COD. RESERVA  APELLIDO/GRUPO        CODIGO AUTORIZACION   IMPORTE PAGO    MONEDA");
            sb.append("\n--------------------------------------------------------------------------------------------------------------------------------------------------");
            bufferWriter.write(sb.toString());
            nroFilas = listaPagos.size();
            //BOLIVIANOS
            valorFilas += armarBloque(listaPagos, bufferWriter, MonedasBoa.BOB);
            //DOLARES
            valorFilas += armarBloque(listaPagos, bufferWriter, MonedasBoa.USD);
            bufferWriter.close();

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("DETALLE DEL ARCHIVO");
                FileReader filer = new FileReader(archivo);
                int valor = filer.read();
                while (valor != -1) {
                    LOGGER.debug("", valor);
                    valor = filer.read();
                }
            }

        } catch (IOException e) {
            LOGGER.error("BOA: Ocurrio un error inesperado", e);
        }
        return new ArchivoProfile(archivo, valorFilas, nroFilas);
    }

    private long armarBloque(List<PagosBoa> listaPagos, BufferedWriter bufferWriter, MonedasBoa moneda) throws IOException {
        long valorFilas;
        long nroFilas = 0L;
        long valorFilasEfectivas = 0L;
        BigDecimal totalRecaudado = BigDecimal.ZERO;
        String impTotalTexto;
        // Detalle del archivo
        for (PagosBoa pago : listaPagos) {
            // Generando registros de pago
            valorFilas = poblarLineaArchivo(bufferWriter, pago, moneda.getCodigo());
            if (valorFilas != 0L) {
                valorFilasEfectivas = valorFilasEfectivas + valorFilas;
                nroFilas++;
                // Sumatoria de totales
                totalRecaudado = totalRecaudado.add(pago.getImporteTotal());
            }
        }
        impTotalTexto = FormatosUtils.decimalFormateado(totalRecaudado);
        if (nroFilas > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("\n-------------------------------------------------------------------------------------------------------------------------------------------------");
            sb.append(StringUtils.rightPad("\nTOTAL REGISTROS", 68, " "));
            sb.append(StringUtils.rightPad(String.valueOf(nroFilas), 36, " "));
            sb.append(StringUtils.rightPad("TOTAL IMPORTE", 22, " "));
            sb.append(StringUtils.rightPad(impTotalTexto, 20, " "));
            sb.append("\n-------------------------------------------------------------------------------------------------------------------------------------------------");
            bufferWriter.write(sb.toString());
        }
        return valorFilasEfectivas;
    }

    /*
        private ArchivoProfile poblarArchivo(File archivo, List<PagosBoa> listaPagos, Date fechaDesde, Date fechaHasta){

            if(archivo==null) return null;
            if(listaPagos==null || listaPagos.size()<=0) return null;

            String fechaPago =  FormatosUtils.fechaFormateadaConYYYYMMDD(fechaDesde);
            String fechaYYYY =  FormatosUtils.fechaFormateadaConYYYY(fechaDesde);
            String fechaHoraDesde =  FormatosUtils.fechaHoraFormateadaConYY(fechaDesde);
            String fechaHoraHasta =  FormatosUtils.fechaHoraFormateadaConYY(fechaHasta);

            long nroFilas = 0L;
            long valorFilas = 0L;

            BigDecimal totalRecaudado = BigDecimal.ZERO;

            FileWriter archivoWrite = null;
            try {
                archivoWrite = new FileWriter(archivo.getAbsoluteFile());

                BufferedWriter bufferWriter = new BufferedWriter(archivoWrite);

                StringBuilder sb = new StringBuilder();
                sb.append("Entity,IP,RequestDateTime,IssueDateTime,PNR,IdentifierPNR,Authorization,TotalAmount,Currency,State\n");
                /*
                sb.append("BANCO BISA S.A.                                            DETALLE DE COBRANZA PARA BOLIVIANA DE AVIACION                              "+fechaYYYY);
                sb.append("\nSUCURSAL: NACIONAL                                              DEL "+fechaHoraDesde+" AL "+fechaHoraHasta);
                sb.append("\nENTIDAD       IP        FECHA SOLICITUD      FECHA EMISION        COD. RESERVA  APELLIDO/GRUPO        CODIGO AUTORIZACION   IMPORTE PAGO    MONEDA");
                sb.append("\n--------------------------------------------------------------------------------------------------------------------------------------------------\n");
                */
/*
            bufferWriter.write(sb.toString());
            // Detalle del archivo
            for(PagosBoa pago : listaPagos){
                // Generando registros de pago
                valorFilas += poblarLineaArchivo(bufferWriter, pago, nroFilas, ",");
                nroFilas++;
                // Sumatoria de totales
                //totalRecaudado = totalRecaudado.add(pago.getImporteTotal());
            }

            //String impTotalTexto = FormatosUtils.decimalFormateado(totalRecaudado);

            sb = new StringBuilder();
            /*
            sb.append("\n-------------------------------------------------------------------------------------------------------------------------------------------------");
            sb.append(StringUtils.rightPad("\nTOTAL REGISTROS", 68, " "));
            sb.append(StringUtils.rightPad(String.valueOf(nroFilas), 36, " "));
            sb.append(StringUtils.rightPad("TOTAL IMPORTE", 22, " "));
            sb.append(StringUtils.rightPad(impTotalTexto, 20, " "));
            */
/*
            bufferWriter.write(sb.toString());
            bufferWriter.close();

            if(LOGGER.isDebugEnabled()){
                LOGGER.debug("DETALLE DEL ARCHIVO");
                FileReader filer = new FileReader(archivo);
                int valor=filer.read();
                while(valor!=-1){
                    System.out.print((char)valor);
                    valor=filer.read();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        ArchivoProfile archivoProfile = new ArchivoProfile(archivo, valorFilas, nroFilas);

        return archivoProfile;
    }

    */
    private long poblarLineaArchivo(BufferedWriter bw, PagosBoa pago, int monedas) throws IOException {

        if (pago == null) return 0L;
        if (monedas == pago.getMonedaImporteTotal()) {
            String ip = pago.getIpOrigen();
            String fechaSolicitud = FormatosUtils.formatoFechaHora(pago.getFechaProceso()); // Solicitud Autorizacion
            String fechaEmision = FormatosUtils.formatoFechaHora(pago.getFechaEmision());   // Emision Tickets
            String pnr = pago.getNroPNR();
            String apellidoGrupo = StringUtils.upperCase(pago.getNombre());
            String codigoAutorizacion = "" + pago.getNroCaja().toPlainString() + pago.getNroSecuencia().toPlainString();

            String importePago = FormatosUtils.decimalFormateado(pago.getImporteTotal());
            //String moneda = Monedas.getDescripcionByBisaCorta(Short.valueOf(""+pago.getMonedaImporteTotal()));
            MonedasBoa moneda = MonedasBoa.valorEnum(pago.getMonedaImporteTotal());
            /*String estado = "0";
            if (pago.getEstado() == EstadosBoa.PAGOAUTORIZADO.getEstado() || pago.getEstado() == EstadosBoa.PAGOCOMPLETADO.getEstado()) {
                estado = "1";
            }*/
            bw.write("\n");
            StringBuilder sb = new StringBuilder();
            sb.append(StringUtils.rightPad(ENTIDAD, 8, " "));
            sb.append(StringUtils.rightPad(ip, 17, " "));
            sb.append(StringUtils.rightPad(fechaSolicitud, 21, " "));
            sb.append(StringUtils.rightPad(fechaEmision, 21, " "));
            sb.append(StringUtils.rightPad(pnr, 15, " "));
            sb.append(StringUtils.rightPad(apellidoGrupo, 21, " "));
            sb.append(StringUtils.rightPad(codigoAutorizacion, 22, " "));
            sb.append(StringUtils.rightPad(importePago, 17, " "));
            if (moneda != null) {
                sb.append(StringUtils.rightPad(moneda.name(), 10, " "));
            }
            bw.write(sb.toString());
            long valorFila = 0L;
            char[] charArray = sb.toString().toCharArray();
            for (char car : charArray) {
                valorFila += car;
            }
            return valorFila;
        } else return 0L;


    }
/*
    private long poblarLineaArchivo(BufferedWriter bw, PagosBoa pago, long fila, String separador) throws IOException{

        if(pago==null) return 0L;
        String ip = pago.getIpOrigen();
        String fechaSolicitud = FormatosUtils.formatoFechaHora(pago.getFechaProceso()); // Solicitud Autorizacion
        String fechaEmision = FormatosUtils.formatoFechaHora(pago.getFechaEmision());   // Emision Tickets
        String pnr = StringUtils.upperCase(pago.getNroPNR());
        String apellidoGrupo = StringUtils.upperCase(pago.getNombre());
        String codigoAutorizacion =""+pago.getNroCaja().toPlainString()+pago.getNroSecuencia().toPlainString();

        String importePago = FormatosUtils.decimalFormateado(pago.getImporteTotal());
        //String moneda = Monedas.getDescripcionByBisaCorta(Short.valueOf(""+pago.getMonedaImporteTotal()));
        MonedasBoa moneda= MonedasBoa.valorEnum(pago.getMonedaImporteTotal());
        String estado="0";
        if(pago.getEstado()==EstadosBoa.PAGOAUTORIZADO.getEstado() || pago.getEstado()==EstadosBoa.PAGOCOMPLETADO.getEstado()){
            estado="1";
        }

        StringBuilder sb = new StringBuilder();
        if (fila > 0L) {
            bw.write("\n");
        }
        sb.append(ENTIDAD);
        sb.append(separador);
        sb.append(ip);
        sb.append(separador);
        sb.append(fechaSolicitud);
        sb.append(separador);
        sb.append(fechaEmision);
        sb.append(separador);
        sb.append(pnr);
        sb.append(separador);
        sb.append(apellidoGrupo);
        sb.append(separador);
        sb.append(codigoAutorizacion);
        sb.append(separador);
        sb.append(importePago);
        sb.append(separador);
        sb.append(moneda.name());
        sb.append(separador);
        sb.append(estado);
        bw.write(sb.toString());

        long valorFila = 0L;
        char[] charArray = sb.toString().toCharArray();
        for (char car : charArray) {
            valorFila += car;
        }

        return valorFila;

    }
*/
}
