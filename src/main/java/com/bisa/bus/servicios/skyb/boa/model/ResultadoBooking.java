package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * @author rchura on 10-11-15
 */
@JsonRootName(value = "GetBookingResult")
public class ResultadoBooking {

    @JsonProperty(value = "GetBookingResult")
    private String respuesta;


    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    @Override
    public String toString() {
        return "ResultadoBooking{" + "respuesta='" + respuesta + '\'' + '}';
    }
}
