package com.bisa.bus.servicios.skyb.boa.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import com.bisa.bus.servicios.skyb.boa.dao.PagosBoaDao;
import com.bisa.bus.servicios.skyb.boa.entities.PagosBoa;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * @author by rchura on 17-12-15.
 */
@Titulo("Detalle de Pago - BoA")
public class DetallePagoBoa extends BisaWebPage {

    public DetallePagoBoa(PageParameters parameters) {
        super(parameters);
        Long id = parameters.get("id").toLong();

        IModel<PagosBoa> pagoModel = new LoadableDetachableModel<PagosBoa>() {
            @Override
            protected PagosBoa load() {
                PagosBoaDao dao = getInstance(PagosBoaDao.class);
                return dao.find(id);
            }
        };

        setDefaultModel(new CompoundPropertyModel<>(pagoModel));

        add(new Label("id"));
        add(new Label("nroOperacion"));
        add(new Label("nroCliente"));
        add(new Label("nroPNR"));
        add(new Label("nombre"));
        add(new Label("fechaReserva"));
        add(new Label("fechaLimite"));
        add(new Label("monedaDescripcion"));
        add(new Label("importeFormato"));
        add(new Label("fechaProcesoFormato"));
        add(new Label("userProceso"));
        add(new Label("estadoDescripcion"));
        add(new Label("cuentaDebitoFormato"));
        add(new Label("tipoCtaDebito"));
        add(new Label("monedaCtaDebitoFormato"));
        add(new Label("nroCaja"));
        add(new Label("nroSecuencia"));
        add(new Label("posteo"));
        add(new Label("secuenciaReversa"));
        add(new Label("errorMonitor"));
        add(new Label("mensajeRespuesta"));
        add(new Label("nroPasajeros"));
        add(new Label("cantVuelos"));
        add(new Label("tickets"));
        add(new Label("tipoDocumento"));
        add(new Label("nroDocumento"));
        add(new Label("razonSocial"));
        add(new Label("mails"));
        add(new Label("nota"));
        add(new Label("ipOrigen"));
        add(new Label("fechaCreacionFormato"));
        add(new Label("usuarioCreador"));
        add(new Label("fechaModificacionFormato"));
        add(new Label("usuarioModificador"));

        add(new Label("fechaRechazoFormato"));
        add(new Label("userRechazo"));
        add(new Label("sistema"));
    }
}
