package com.bisa.bus.servicios.skyb.boa.sched;

import bus.consumoweb.yellowpepper.dao.YellowPepperServicioDao;
import bus.env.api.MedioAmbiente;
import bus.plumbing.components.EMailValidator;
import bus.plumbing.cypher.TripleDes;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.skyb.boa.api.ProcesosBoa;
import com.bisa.bus.servicios.skyb.boa.api.ServiciosBoa;
import com.bisa.bus.servicios.skyb.boa.dao.PagosBoaDao;
import com.bisa.bus.servicios.skyb.boa.entities.EstadosBoa;
import com.bisa.bus.servicios.skyb.boa.entities.PagosBoa;
import com.bisa.bus.servicios.skyb.boa.model.Autorizacion;
import com.bisa.bus.servicios.skyb.boa.model.Mensaje;
import com.bisa.bus.servicios.skyb.boa.model.MessageType;
import com.bisa.bus.servicios.skyb.boa.model.TicketPNR;
import com.bisa.bus.servicios.skyb.boa.ws.*;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static bus.env.api.Variables.*;

/**
 * Created by rchura on 04-11-15.
 */
public class VerificacionPagosBoA implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(VerificacionPagosBoA.class);

    private final ServiciosBoa serviciosBoa;
    private final PagosBoaDao pagosBoaDao;
    private final YellowPepperServicioDao yellowPepperServicioDao;
    private final MedioAmbiente medioAmbiente;
    private final ProcesosBoa procesosBoa;
    private static final String AUTORIZADO ="1";
    private static final String NO_AUTORIZADO ="0";

    @Inject
    public VerificacionPagosBoA(ServiciosBoa serviciosBoa, PagosBoaDao pagosBoaDao, YellowPepperServicioDao yellowPepperServicioDao, MedioAmbiente medioAmbiente, ProcesosBoa procesosBoa) {
        this.serviciosBoa = serviciosBoa;
        this.pagosBoaDao = pagosBoaDao;
        this.yellowPepperServicioDao = yellowPepperServicioDao;
        this.medioAmbiente = medioAmbiente;
        this.procesosBoa = procesosBoa;
    }


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        LOGGER.debug("Inicio de trabajo asincrono verificacion pagos BoA");
        // Obtencion de pagos autorizados pendientes de completarse
        // SkybTech encola las autorizaciones, es necesario obtener los tickets para confirmar el pago
        int dias = medioAmbiente.getValorIntDe(SKYB_BOA_VERIFICACION_DIAS_ATRAS, SKYB_BOA_VERIFICACION_DIAS_ATRAS_DEFAULT);
        int minutos_reintento = medioAmbiente.getValorIntDe(SKYB_BOA_MINUTOS_REINTENTO_PAGO, SKYB_BOA_MINUTOS_REINTENTO_PAGO_DEFAULT);

        Date nuevaFecha = FormatosUtils.modificarDiasDate(new Date(), dias);
        List<PagosBoa> listaPagos =  pagosBoaDao.getPagoSinRespuestaDesdeFecha(nuevaFecha);

        if(listaPagos!=null && listaPagos.size()>0){
            LOGGER.info("Existen pagos de reserva Sin Respuesta de autorización de pago. Se encontraron {} registros.", listaPagos.size());
            for(PagosBoa pago: listaPagos) {
                // Verificar si vencio el tiempo en minutos para reintento de solicitud de emisión de boleto
                double diferencia = (new Date()).getTime() - pago.getFechaCreacion().getTime();
                diferencia = (diferencia/(60*1000));
                if(minutos_reintento > 0 && minutos_reintento < diferencia){
                    pago.setEstado(EstadosBoa.REVERSANDO.getEstado());
                    pago = pagosBoaDao.actualizar(pago, pago.getUsuarioCreador());
                    revertirPagoBoa(pago, pago.getUsuarioCreador());
                    pago.setEstado(EstadosBoa.ERRORPAGO.getEstado());
                    pago.setEstadoRespuesta("");
                    pago.setMensajeRespuesta(medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_SERVICIO_NO_DISPONIBLE, SKYB_BOA_MENSAJE_SERVICIO_NO_DISPONIBLE_DEFAULT));
                    pago = pagosBoaDao.actualizar(pago, pago.getUsuarioCreador());
                    continue;
                }
                // Verificar que los pagos de reserva llegaron al estad PAGO PROCESAO o PAGO AUTORIZADO sin respuesta en ambos casos
                if(StringUtils.isEmpty(pago.getToken())) {
                    TokenRequest tokenRequest = new TokenRequest(pago.getNroCliente(), pago.getUserProceso(), pago.getNroPNR(),
                            pago.getNombre(), pago.getIpOrigen(), pago.getSistema());

                    // Solicitar token a BOA para inicio de porceso de emision
                    LOGGER.info("Solicitud de token a BOA para inicio de porceso de emision de boleto");
                    TokenResponse tokenResponse = serviciosBoa.obtenerToken(tokenRequest);
                    if(tokenResponse==null || tokenResponse.getToken()==null){
                        pago.setEstado(EstadosBoa.SINRESPUESTA.getEstado());
                        pago = pagosBoaDao.actualizar(pago, pago.getUsuarioCreador());
                        LOGGER.info("No se obtiene respuesta de getToken pero se almacena Transacción de Pago para reintento de reserva ID[{}]. "+pago.getId());
                        continue;
                    }

                    String token = tokenResponse.getToken();
                    String codigoAutorizacion =""+pago.getNroCaja().toPlainString()+pago.getNroSecuencia().toPlainString();

                    // ENCRIPTAR EL CODIGO DE AUTORIZACION
                    LOGGER.debug("[Codigo de autorización: {}, Cadena Encriptada {}].", codigoAutorizacion, encriptar(codigoAutorizacion));
                    codigoAutorizacion = encriptar(codigoAutorizacion);

                    pago.setToken(token);
                    pago.setEstado(EstadosBoa.ENPROCESO.getEstado());
                    pago = pagosBoaDao.actualizar(pago, pago.getUsuarioCreador());

                    PagoReservaResponse response = new PagoReservaResponse(new Autorizacion(), 0L, null);

                    // Proceso de autorizacion de reserva en BOA
                    LOGGER.debug("Solicitud de autorizacion de emision de boleto a BOA.");
                    response = serviciosBoa.autorizarReserva(pago, token, codigoAutorizacion);
                    response.setPagoID(pago.getId());

                    if(response.getMensaje()!=null && MessageType.SINRESPUESTA.equals(response.getMensaje().getTipo())){
                        // MARCAMOS CON ESTADO SIN RESPUESTA
                        pago.setEstado(EstadosBoa.SINRESPUESTA.getEstado());
                        pago.setEstadoRespuesta(StringUtils.substring(response.getMensaje().getCodigo(),0,5));
                        pago.setMensajeRespuesta(StringUtils.substring(response.getMensaje().getMensaje(),0,200));
                        pago = pagosBoaDao.actualizar(pago, pago.getUsuarioCreador());
                        continue;
                    }


                    if(response.getMensaje()!=null && MessageType.ERROR.equals(response.getMensaje().getTipo())){
                        // En caso de Error, Revertir debito
                        pago.setEstado(EstadosBoa.REVERSANDO.getEstado());
                        pago = pagosBoaDao.actualizar(pago, pago.getUsuarioCreador());

                        revertirPagoBoa(pago, pago.getUsuarioCreador());


                        pago.setEstado(EstadosBoa.ERRORPAGO.getEstado());
                        pago.setEstadoRespuesta(StringUtils.substring(response.getMensaje().getCodigo(),0,5));
                        String desc = ""+StringUtils.substring(response.getMensaje().getMensaje(),0,200);
                        pago.setMensajeRespuesta(desc);
                        pago = pagosBoaDao.actualizar(pago, pago.getUsuarioCreador());
                        continue;

                    }

                    if(AUTORIZADO.equalsIgnoreCase(StringUtils.trimToEmpty(response.getAutorizacion().getEstado()))){
                        // Pago registrado satisfactoriamente
                        // A la espera de emision de boletos.

                        pago.setEstado(EstadosBoa.PAGOAUTORIZADO.getEstado());
                        pago.setEstadoRespuesta(StringUtils.substring(response.getAutorizacion().getEstado(),0,5));
                        pago.setMensajeRespuesta(StringUtils.substring(response.getAutorizacion().getMensaje(),0,200));

                        pago = pagosBoaDao.actualizar(pago, pago.getUsuarioCreador());
                        response.setPagoID(pago.getId());

                    }
                } else {
                    LOGGER.info("Inicia verificacion de AUTORIZACION DE PAGO sin respueta pago: {}", pago.getId());
                    PagoReservaResponse response = new PagoReservaResponse(new Autorizacion(), 0L, null);
                    String codigoAutorizacion =""+pago.getNroCaja().toPlainString()+pago.getNroSecuencia().toPlainString();
                    // ENCRIPTAR EL CODIGO DE AUTORIZACION
                    LOGGER.info("[Codigo de autorización: {}, Cadena Encriptada {}].", codigoAutorizacion, encriptar(codigoAutorizacion));
                    codigoAutorizacion = encriptar(codigoAutorizacion);

                    // Proceso de autorizacion de reserva en BOA
                    response = serviciosBoa.autorizarReserva(pago, StringUtils.trim(pago.getToken()), codigoAutorizacion);
                    response.setPagoID(pago.getId());
                    if( response.getMensaje()!=null && MessageType.SINRESPUESTA.equals(response.getMensaje().getTipo()) ){
                        // MARCAMOS CON ESTADO SIN RESPUESTA
                        pago.setEstado(EstadosBoa.SINRESPUESTA.getEstado());
                        pago.setEstadoRespuesta(StringUtils.substring(response.getMensaje().getCodigo(),0,5));
                        pago.setMensajeRespuesta(StringUtils.substring(response.getMensaje().getMensaje(),0,200));
                        pago = pagosBoaDao.actualizar(pago, pago.getUsuarioCreador());
                        continue;
                    }

                    if(response.getMensaje()!=null && MessageType.ERROR.equals(response.getMensaje().getTipo())){
                        // En caso de Error, Revertir debito
                        pago.setEstado(EstadosBoa.REVERSANDO.getEstado());
                        pago = pagosBoaDao.actualizar(pago, pago.getUsuarioCreador());

                        revertirPagoBoa(pago, pago.getUsuarioCreador());

                        pago.setEstado(EstadosBoa.ERRORPAGO.getEstado());
                        pago.setEstadoRespuesta(StringUtils.substring(response.getMensaje().getCodigo(), 0, 5));
                        String desc = ""+StringUtils.substring(response.getMensaje().getMensaje(),0,200);
                        pago.setMensajeRespuesta(desc);
                        pago = pagosBoaDao.actualizar(pago, pago.getUsuarioCreador());
                        continue;
                    }

                    if(AUTORIZADO.equalsIgnoreCase(StringUtils.trimToEmpty(response.getAutorizacion().getEstado()))){
                        // Pago registrado satisfactoriamente
                        // A la espera de emision de boletos.

                        pago.setEstado(EstadosBoa.PAGOAUTORIZADO.getEstado());
                        pago.setEstadoRespuesta(StringUtils.substring(response.getAutorizacion().getEstado(), 0, 5));
                        pago.setMensajeRespuesta(StringUtils.substring(response.getAutorizacion().getMensaje(), 0, 200));

                        pago = pagosBoaDao.actualizar(pago, pago.getUsuarioCreador());
                        response.setPagoID(pago.getId());
                    }
                }

            }

        }else{
            LOGGER.debug("No existen pagos de reserva BOA, Sin Respuesta de autorizacion de emision de boletos en BoA.");
        }

    }


    private TicketPagoResponse getTickets(TicketPagoRequest request) {
        TicketPagoResponse response = null;
        if(serviciosBoa==null){
            LOGGER.debug(" OBJETO <serviciosBoa> ES NULO---> "+request.toString());
            response = new TicketPagoResponse();
        }
        else{
            LOGGER.debug(" OBJETO <serviciosBoa> EXISTE---> "+request.toString());
            response = serviciosBoa.obtenerTicket(request);
        }
        return response;

    }


    private void revertirPagoBoa(PagosBoa pago, String usuario ){

        LOGGER.info("Efectuando la reversa del debito en la cuenta del cliente.");

        PagosBoa pagosBoa;
        String resp= "";

        pago.setEstado(EstadosBoa.REVERSANDO.getEstado());
        pago = pagosBoaDao.actualizar(pago, usuario);

        //Revertir debito
        pagosBoa =  procesosBoa.ejecutarReversa(pago, usuario);
        if(pagosBoa==null){
            resp = "Ocurri\u00f3 un error inesperado al revertir el d\u00e9bito. Por favor comun\u00edquese con Bisa Responde.";

            pagosBoa.setEstado(EstadosBoa.ERRORREVERSA.getEstado());
            pagosBoa.setEstadoRespuesta("902");
            pagosBoa.setMensajeRespuesta(StringUtils.substring(resp,0,200));

        }else{
            LOGGER.debug("La reversa del debito fue efectuada correctamente.");
            pagosBoa.setEstado(EstadosBoa.PAGOREVERTIDO.getEstado());
        }

        pagosBoa = pagosBoaDao.actualizar(pagosBoa, usuario);

        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            LOGGER.error("Error: Nos han interrumpido en plena espera.");
        }
    }

    private String encriptar(String cadena){

        String nombreAlgoritmo = medioAmbiente.getValorDe(SKYB_BOA_ALGORITMO_ENCRIPTACION, SKYB_BOA_ALGORITMO_ENCRIPTACION_DEFAULT);
        String llaveEncriptacion = medioAmbiente.getValorDe(SKYB_BOA_LLAVE_ENCRIPTACION, SKYB_BOA_LLAVE_ENCRIPTACION_DEFAULT);

        TripleDes tripleDes = new TripleDes(nombreAlgoritmo, llaveEncriptacion);
        return tripleDes.encrypt(cadena);
    }
}
