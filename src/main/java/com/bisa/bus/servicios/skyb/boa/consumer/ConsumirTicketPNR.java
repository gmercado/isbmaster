package com.bisa.bus.servicios.skyb.boa.consumer;

import com.bisa.bus.servicios.skyb.boa.model.*;
import com.bisa.bus.servicios.skyb.boa.ws.ConsultaReservaResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.*;


/**
 * Created by rchura on 09-11-15.
 */
public class ConsumirTicketPNR extends ConsumidorServiciosBoa{


    @Inject
    public ConsumirTicketPNR(@SQLFueraDeLinea ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente) {
        super(executor, consumoWebDao, medioAmbiente);
    }

    @Override
    protected String getUrl() {
        return medioAmbiente.getValorDe(SKYB_BOA_URL_RESERVA, SKYB_BOA_URL_RESERVA_DEFAULT);
    }

    @Override
    public String getNombreMetodo() {
        return medioAmbiente.getValorDe(SKYB_BOA_METODO_TICKETPNR, SKYB_BOA_METODO_TICKETPNR_DEFAULT);
    }


    protected String getParametros(Localizador localizador) {

        if(localizador==null){
            LOGGER.error("NO se puede obtener los datos del \'Localizador (pnr, identificador)\'");
            return null;
        }

        Map<String, String> map = getEntradaBase();
        EntradaTicketPNR entradaTicket = new EntradaTicketPNR(map.get("credencial"), map.get("ip"), Boolean.FALSE,
                localizador, map.get("lenguaje"));

        ObjectMapper mapper = new ObjectMapper();
        String solicitud = null;
        try {
            solicitud = mapper.writeValueAsString(entradaTicket);
            LOGGER.debug("SOLICITUD JSON ES <ConsumirTicketPNR> ---->: " + solicitud);
        } catch (JsonProcessingException e) {
            LOGGER.error("ERROR: <JsonProcessingException> al ejecutar el metodo: getParametros().", e);
            solicitud = null;
        }
        return solicitud;
    }

    public ResultadoTicketPNR getTicketPNR(Localizador localizador){
        ResultadoTicketPNR resultadoTicketPNR = null;
        String respuesta;
        try {
            respuesta = (String) sendPost(getParametros(localizador), localizador.getDatosConsumidor());
        } catch (IOException e) {
            LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getTicketPNR().", e);
            resultadoTicketPNR = new ResultadoTicketPNR();
            resultadoTicketPNR.setMensaje(new Mensaje("900",MessageType.REINTENTAR, "No se puede obtener respuesta del Sistema de BoA. "+e.getMessage()));
            return resultadoTicketPNR;

        } catch (Exception e){
            LOGGER.error("ERROR: <Exception> al ejecutar el metodo: getTicketPNR().", e);
            resultadoTicketPNR = new ResultadoTicketPNR();
            resultadoTicketPNR.setMensaje(new Mensaje("900",MessageType.REINTENTAR, "No se puede obtener respuesta del Sistema de BoA. "+e.getMessage()));
            return resultadoTicketPNR;

        }


        if(respuesta!=null){
            resultadoTicketPNR = getResultadoTicketPNR(respuesta);
        }

        return resultadoTicketPNR;
    }


    private ResultadoTicketPNR getResultadoTicketPNR(String cadenaJson){

        ObjectMapper objectMapper = new ObjectMapper();
        ResultadoTicketPNR resultadoTicketPNR = null;
        LOGGER.debug("JSON--->"+cadenaJson);

        try {
            resultadoTicketPNR = objectMapper.readValue(cadenaJson, ResultadoTicketPNR.class);
            LOGGER.debug("ResultadoTicketPNR OBJETO ---> " + resultadoTicketPNR.getRespuesta());
        }catch (JsonMappingException e) {
            LOGGER.warn("ERROR: <JsonProcessingException> al ejecutar el metodo: getResultadoTicketPNR().", e);
            resultadoTicketPNR = null;
        } catch (JsonParseException e) {
            LOGGER.warn("ERROR: <JsonParseException> al ejecutar el metodo: getResultadoTicketPNR().", e);
            resultadoTicketPNR = null;
        } catch (IOException e) {
            LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getResultadoTicketPNR().", e);
            resultadoTicketPNR = null;
        }


        if(resultadoTicketPNR!=null){
            GetTicketPNR getTicketPNR=null;
            Mensaje mensaje = null;
            try {
                getTicketPNR = objectMapper.readValue(resultadoTicketPNR.getRespuesta(), GetTicketPNR.class);
                LOGGER.debug("GetTicketPNR OBJETO ---> " + getTicketPNR.getTicketPNR().toString());
            } catch (JsonMappingException e) {
                LOGGER.warn("Error JsonMappingException -->"+resultadoTicketPNR.getRespuesta());
                // Intenta parsear de manera individual
                getTicketPNR = getTicketePNRsimple(resultadoTicketPNR.getRespuesta());
                if(getTicketPNR==null){
                    mensaje = getMensaje(resultadoTicketPNR.getRespuesta());
                    getTicketPNR=null;
                }
            } catch (JsonParseException e) {
                LOGGER.warn("Error JsonParseException -->"+resultadoTicketPNR.getRespuesta());
                getTicketPNR = getTicketePNRsimple(resultadoTicketPNR.getRespuesta());
                if(getTicketPNR==null){
                    mensaje = getMensaje(resultadoTicketPNR.getRespuesta());
                    getTicketPNR=null;
                }
            } catch (IOException e) {
                LOGGER.debug("Error IOException -->"+resultadoTicketPNR.getRespuesta());
                getTicketPNR = getTicketePNRsimple(resultadoTicketPNR.getRespuesta());
                if(getTicketPNR==null){
                    mensaje = getMensaje(resultadoTicketPNR.getRespuesta());
                    getTicketPNR=null;
                }
            }

            if(getTicketPNR!=null) resultadoTicketPNR.setTicketPNR(getTicketPNR.getTicketPNR());
            if(mensaje!=null) resultadoTicketPNR.setMensaje(mensaje);

            LOGGER.debug("OBJETO ResultadoTicketPNR:--->" + resultadoTicketPNR.toString());
        }

        return resultadoTicketPNR;
    }


    private GetTicketPNR getTicketePNRsimple(String cadenaJson){

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = null;
        GetTicketPNR getTicketPNR = null;
        try {
            root = mapper.readTree(cadenaJson);
        } catch (IOException e) {
            LOGGER.error("Error: No se puede mapear a objeto JSON la cadena ["+cadenaJson+"].", e);
            return null;
        }

        String pasajero = root.at("/ResultGetTicketPNR/pasajeros/string").toString();
        String ticket = root.at("/ResultGetTicketPNR/tkts/string").toString();

        List<String> lpasajero = new ArrayList<>();
        lpasajero.add(StringUtils.remove(pasajero, "\""));
        List<String> lticket = new ArrayList<>();
        lticket.add(StringUtils.remove(ticket, "\""));

        ListaTitular listaTitular = new ListaTitular();
        listaTitular.setTitular(lpasajero);
        ListaTicket listaTicket = new ListaTicket();
        listaTicket.setTicket(lticket);

        TicketPNR ticketPNR = new TicketPNR();
        ticketPNR.setTitulares(listaTitular);
        ticketPNR.setTickets(listaTicket);

        getTicketPNR = new GetTicketPNR();
        getTicketPNR.setTicketPNR(ticketPNR);

        return getTicketPNR;
    }


}
