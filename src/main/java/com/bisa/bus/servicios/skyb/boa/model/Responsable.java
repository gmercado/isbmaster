package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 12-11-15.
 */
public class Responsable {

    @JsonProperty(value = "tipo_reserva")
    private String tipoReserva;

    @JsonProperty(value = "cod_cia")
    private String cod_cia;

    @JsonProperty(value = "off_resp")
    private String off_resp;

    public String getTipoReserva() {
        return tipoReserva;
    }

    public void setTipoReserva(String tipoReserva) {
        this.tipoReserva = tipoReserva;
    }

    public String getCod_cia() {
        return cod_cia;
    }

    public void setCod_cia(String cod_cia) {
        this.cod_cia = cod_cia;
    }

    public String getOff_resp() {
        return off_resp;
    }

    public void setOff_resp(String off_resp) {
        this.off_resp = off_resp;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Responsable{");
        sb.append("tipoReserva='").append(tipoReserva).append('\'');
        sb.append(", cod_cia='").append(cod_cia).append('\'');
        sb.append(", off_resp='").append(off_resp).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
