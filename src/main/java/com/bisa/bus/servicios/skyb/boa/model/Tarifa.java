package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

import java.math.BigDecimal;

/**
 * Created by rchura on 06-11-15.
 */
//@JsonIgnoreProperties({"tasa", "codigo_tasa", "tipo_tasa", "moneda_tasa", "importe_tasa"})
@JsonIgnoreProperties({"Fntaxs","tasa", "codigo_tasa", "tipo_tasa", "moneda_tasa", "importe_tasa"})
public class Tarifa {

    /**
     * posicion
     * Elemento “posicion” definido en el apartado 10.2.1.2.1	Identifica de manera única la información del pasajero en el PNR.
     */
    @JsonProperty(value = "posicion")
    private Posicion posicion;

    /**
     * inf
     * Carácter: “Y”, “N”	Indica si el pasajero se trata de un INF (“Y”), o no (“N”).
     */
    @JsonProperty(value = "inf")
    private String infante;

    /**
     * importe_tarifa
     * Decimal. El delimitador de los decimales es el carácter “.”.
     * Importe de la tarifa original
     */
    @JsonProperty(value = "importe_tarifa")
    private BigDecimal importeTarifa;

    /**
     * moneda_tarifa
     * Cadena de caracteres 	Moneda de la tarifa original.
     */
    @JsonProperty(value = "moneda_tarifa")
    private String monedaTarifa;

    /**
     * importe_total
     * Decimal. El delimitador de los decimales es el carácter “.”.
     * Importe de la tarifa en la moneda solicitada.
     */
    @JsonProperty(value = "importe_total")
    private BigDecimal importeTotal;

    /**
     * moneda_total
     * Cadena de caracteres.	Moneda de la tarifa procesada.
     */
    @JsonProperty(value = "moneda_total")
    private String monedaTotal;

    /**
     * Fntaxs
     * Listado de elementos “tasa”, definido en el apartado 10.2.110.2.1	Conjunto de tasas aplicadas al itinerario.
     */
    //@JsonProperty(value = "Fntaxs")
    //@JsonProperty(value = "tasa")
    //@JsonIgnoreProperties({"Fntaxs","tasa", "codigo_tasa", "tipo_tasa", "moneda_tasa", "importe_tasa"})
    //@JsonIgnore(true)
    //private List<Tasa> tasas;


    @JsonProperty(value = "Fntaxs")
    private JsonNode tasasJson;


    /**
     * tipo_tarifa
     * Cadena de caracteres.	Tipo tarifa
     */
    @JsonProperty(value = "tipo_tarifa")
    private String tipoTarifa;

    /**
     * tipo_total
     * Cadena de caracteres.	Tipo de la tarifa procesada.
     */
    @JsonProperty(value = "tipo_total")
    private String tipoTotal;

    /**
     * num_pax
     * Numérico.
     * Número de pasajero al que van asociadas las tarifas y tasas aplicadas al itinerario.
     */
    @JsonProperty(value = "num_pax")
    private Long nroPasajero;


    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }

    public String getInfante() {
        return infante;
    }

    public void setInfante(String infante) {
        this.infante = infante;
    }

    public BigDecimal getImporteTarifa() {
        return importeTarifa;
    }

    public void setImporteTarifa(BigDecimal importeTarifa) {
        this.importeTarifa = importeTarifa;
    }

//    public List<Tasa> getTasas() {
//        return tasas;
//    }
//
//    public void setTasas(List<Tasa> tasas) {
//        this.tasas = tasas;
//    }

//    public String getTasasJson() {
//        return tasasJson;
//    }
//
//    public void setTasasJson(String tasasJson) {
//        this.tasasJson = tasasJson;
//    }


    public JsonNode getTasasJson() {
        return tasasJson;
    }

    public void setTasasJson(JsonNode tasasJson) {
        this.tasasJson = tasasJson;
    }

    public String getTipoTarifa() {
        return tipoTarifa;
    }

    public void setTipoTarifa(String tipoTarifa) {
        this.tipoTarifa = tipoTarifa;
    }

    public String getTipoTotal() {
        return tipoTotal;
    }

    public void setTipoTotal(String tipoTotal) {
        this.tipoTotal = tipoTotal;
    }

    public Long getNroPasajero() {
        return nroPasajero;
    }

    public void setNroPasajero(Long nroPasajero) {
        this.nroPasajero = nroPasajero;
    }

    public BigDecimal getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(BigDecimal importeTotal) {
        this.importeTotal = importeTotal;
    }


    public String getMonedaTarifa() {
        return monedaTarifa;
    }

    public void setMonedaTarifa(String monedaTarifa) {
        this.monedaTarifa = monedaTarifa;
    }

    public String getMonedaTotal() {
        return monedaTotal;
    }

    public void setMonedaTotal(String monedaTotal) {
        this.monedaTotal = monedaTotal;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Tarifa{");
        sb.append("posicion=").append(posicion);
        sb.append(", infante='").append(infante).append('\'');
        sb.append(", importeTarifa=").append(importeTarifa);
        sb.append(", monedaTarifa='").append(monedaTarifa).append('\'');
        sb.append(", importeTotal=").append(importeTotal);
        sb.append(", monedaTotal='").append(monedaTotal).append('\'');
        //sb.append(", tasas=").append(tasas);
        sb.append(", tasasJson='").append(tasasJson).append('\'');
        sb.append(", tipoTarifa='").append(tipoTarifa).append('\'');
        sb.append(", tipoTotal='").append(tipoTotal).append('\'');
        sb.append(", nroPasajero=").append(nroPasajero);
        sb.append('}');
        return sb.toString();
    }
}
