package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 17-11-15.
 */
public class GetTiempoLimite {

    @JsonProperty(value = "tl")
    private TiempoLimite tiempoLimite;

    public TiempoLimite getTiempoLimite() {
        return tiempoLimite;
    }

    public void setTiempoLimite(TiempoLimite tiempoLimite) {
        this.tiempoLimite = tiempoLimite;
    }
}
