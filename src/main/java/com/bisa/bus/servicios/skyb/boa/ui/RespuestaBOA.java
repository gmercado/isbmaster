package com.bisa.bus.servicios.skyb.boa.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.skyb.boa.model.ConsultaReservaForm;
import com.bisa.bus.servicios.skyb.boa.model.ConsultaReservaResponseForm;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 * @author rsalvatierra on 18/03/2016.
 */
@AuthorizeInstantiation({RolesBisa.ISB_BOA})
@Titulo("Respuesta BoA")
public class RespuestaBOA extends BisaWebPage {

    public RespuestaBOA(ConsultaReservaForm solicitud, ConsultaReservaResponseForm reservaResponse) {
        super();
        //Model
        IModel<ConsultaReservaResponseForm> objectModel = new CompoundPropertyModel<>(reservaResponse);
        //Form
        Form<ConsultaReservaResponseForm> form = new Form<>("reservaResponse", objectModel);
        add(form);
        //Cuerpo
        String tpasajeros = reservaResponse.getListaPasajeros() != null ? String.valueOf(reservaResponse.getListaPasajeros().size()) : "";
        String tvuelos = reservaResponse.getListaVuelos() != null ? String.valueOf(reservaResponse.getListaVuelos().size()) : "";

        form.add(new Label("pnr", Model.of(solicitud.getCodigoPnr())));
        form.add(new Label("grupo", Model.of(solicitud.getApellidoGrupo())));
        form.add(new Label("mensaje"));
        form.add(new Label("fechaLimite"));
        form.add(new Label("fechaReserva"));
        form.add(new Label("pasajeros", Model.of(tpasajeros)));
        form.add(new Label("vuelos", Model.of(tvuelos)));
        //botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(new ConsultaBOA(solicitud));
            }
        });
    }
}
