package com.bisa.bus.servicios.skyb.boa.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.interfaces.as400.dao.ClienteDao;
import bus.interfaces.as400.entities.Cliente;
import bus.interfaces.as400.entities.ClientePK;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.skyb.boa.entities.EstadosBoa;
import com.bisa.bus.servicios.skyb.boa.entities.PagosBoa;
import com.bisa.bus.servicios.skyb.boa.entities.TotalPagosBoa;
import com.bisa.bus.servicios.skyb.boa.model.Pasajero;
import com.bisa.bus.servicios.skyb.boa.model.Vuelo;
import com.bisa.bus.servicios.skyb.boa.ws.PagoReservaRequest;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author by rchura on 11-11-15
 */
public class PagosBoaDao extends DaoImpl<PagosBoa, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PagosBoaDao.class);
    private final ClienteDao clienteDao;

    @Inject
    protected PagosBoaDao(@BasePrincipal EntityManagerFactory entityManagerFactory, ClienteDao clienteDao) {
        super(entityManagerFactory);
        this.clienteDao = clienteDao;
    }

    @Override
    protected Path<Long> countPath(Root<PagosBoa> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<PagosBoa> p) {
        Path<String> cliente = p.get("nroCliente");
        Path<String> nroPNR = p.get("nroPNR");
        return Arrays.asList(cliente, nroPNR);
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<PagosBoa> from, PagosBoa example, PagosBoa example2) {
        List<Predicate> predicates = new LinkedList<>();
        Date fecha = example.getFechaCreacion();
        String cliente = StringUtils.trimToNull(example.getNroCliente());
        String nroPNR = StringUtils.trimToNull(example.getNroPNR());
        EstadosBoa estadosBoa = null;
        if (example.getEstadosBoa() != null) {
            estadosBoa = example.getEstadosBoa();
        }
        if (fecha != null && example2.getFechaCreacion() != null) {
            Date inicio = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH);
            Date inicio2 = DateUtils.truncate(example2.getFechaCreacion(), Calendar.DAY_OF_MONTH);
            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
            LOGGER.debug("   FECHAS ENTRE [{}] A [{}]", inicio, inicio2);
            predicates.add(cb.between(from.get("fechaCreacion"), inicio, inicio2));
        }
        if (cliente != null) {
            final String s = FormatosUtils.numeroClienteFormateado(StringUtils.trimToEmpty(cliente));
            LOGGER.debug(">>>>> nroCliente=" + s);
            predicates.add(cb.equal(from.get("nroCliente"), s));
        }
        if (nroPNR != null) {
            LOGGER.debug(">>>>> nroPNR=" + nroPNR);
            predicates.add(cb.equal(from.get("nroPNR"), nroPNR));
        }
        if (estadosBoa != null) {
            LOGGER.debug(">>>>> estado=" + estadosBoa.getEstado());
            predicates.add(cb.equal(from.get("estado"), estadosBoa.getEstado()));
        }
        return predicates.toArray(new Predicate[predicates.size()]);
    }

    public PagosBoa registrar(PagosBoa pago, String usuario) {
        pago.setUsuarioCreador(usuario);
        pago.setFechaCreacion(new Date());
        return persist(pago);
    }

    public PagosBoa actualizar(PagosBoa pago, String usuario) {
        pago.setUsuarioModificador(usuario);
        pago.setFechaModificacion(new Date());
        return merge(pago);
    }

    public PagosBoa getPagosBoa(PagoReservaRequest request, PagosBoa pago) {
        PagosBoa boa;
        if (pago == null) {
            // Creacion del nuevo registro
            boa = new PagosBoa();
            boa.setNroCliente(request.getCliente());
            boa.setNroPNR(StringUtils.trimToEmpty(request.getCodigoPnr()).toUpperCase());
            boa.setNombre(StringUtils.trimToEmpty(request.getApellidoGrupo()).toUpperCase());
            boa.setEstado(EstadosBoa.GENERADO.getEstado());
            boa.setIpOrigen(request.getIpOrigen()); //IP Browser del cliente
            boa.setSistema(request.getSistema());

            // Reserva: datos generales
            boa.setFechaReserva(request.getFechaReserva());
            boa.setFechaLimite(request.getFechaLimite());

            StringBuffer buffer = null;
            // Reserva: lista de pasajeros
            if (request.getListaPasajeros() != null) {
                buffer = new StringBuffer();
                for (Pasajero p : request.getListaPasajeros()) {
                    buffer.append(p.getNombreApellidos());
                    buffer.append(";");
                }
                boa.setPasajeros(buffer.toString());
                boa.setNroPasajeros(Long.valueOf("" + request.getListaPasajeros().size()));
            }
            // Reserva: lista de vuelos
            if (request.getListaVuelos() != null) {
                boa.setVuelos(getCadenaVuelos(request.getListaVuelos()));
                boa.setCantVuelos(Long.valueOf("" + request.getListaVuelos().size()));
            }

            LOGGER.debug("Objeto Pago BOA -->" + boa.toString());

        } else {
            boa = pago;
        }

        // Actualizacion del registro de pago con los datos restantes para el pago
        boa.setImporteTotal(request.getImporteTotal());
        boa.setMonedaImporteTotal(request.getImporteMoneda());

        boa.setCuentaDebito(request.getCuentaDebito());
        boa.setMonedaCtaDebito(request.getCuentaMoneda());
        boa.setTipoCtaDebito(request.getCuentaTipo());

        ClientePK pk = new ClientePK(new BigDecimal("1"), request.getCliente());
        Cliente cliente = clienteDao.find(pk);
        if (cliente != null) boa.setTitularCta(cliente.getNombreCompleto());

        boa.setNota(request.getNota());
        boa.setTipoDocumento(request.getTipoDoc());
        boa.setNroDocumento(request.getNumeroDoc());
        boa.setRazonSocial(request.getRazonSocial());
        boa.setMails(request.getEmail());

        boa.setNroOperacion(request.getNroOperacion());

        //
        PagosBoa resultadoPago;
        if (pago == null) {
            resultadoPago = registrar(boa, request.getUsuario());
        } else {
            resultadoPago = actualizar(boa, request.getUsuario());
        }

        return resultadoPago;
    }

    private String getCadenaVuelos(List<Vuelo> vuelos) {
        StringBuffer buffer = null;
        // Reserva: lista de vuelos
        if (vuelos != null && vuelos.size() > 0) {
            buffer = new StringBuffer();
            for (Vuelo v : vuelos) {
                buffer.append("[");
                buffer.append("lin=" + v.getLinea()).append(",");
                buffer.append("nro=" + v.getNroVuelo()).append(",");
                buffer.append("fcs=" + v.getFechaSalida()).append(",");
                buffer.append("hrs=" + v.getHoraSalida()).append(",");
                buffer.append("hrl=" + v.getHoraLlegada()).append(",");
                buffer.append("ori=" + v.getOrigen()).append(",");
                buffer.append("des=" + v.getDestino()).append(",");
                buffer.append("cla=" + v.getClase()).append(",");
                buffer.append("];");
            }
        }
        return buffer == null ? "" : StringUtils.substring(buffer.toString(), 0, buffer.toString().length() - 1);
    }

    public PagosBoa getPagoByLocalizadorEstado(String pnr, String apellidoGrupo, String nroCliente) {
        LOGGER.debug("Obteniendo registro Pago de reserva BoA por PNR:{}, Nombre:{} y Estado.", pnr, apellidoGrupo);
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<PagosBoa> q = cb.createQuery(PagosBoa.class);
                    Root<PagosBoa> p = q.from(PagosBoa.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("nroPNR"), StringUtils.trimToEmpty(pnr)));
                    predicatesAnd.add(cb.equal(p.get("nombre"), StringUtils.trimToEmpty(apellidoGrupo)));
                    predicatesAnd.add(cb.equal(p.get("nroCliente"), StringUtils.trimToEmpty(nroCliente)));
                    predicatesAnd.add(p.get("estado").in(EstadosBoa.GENERADO.getEstado(), EstadosBoa.DEBITADO.getEstado(),
                            EstadosBoa.AUTORIZACION.getEstado()));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<PagosBoa> query = entityManager.createQuery(q);
                    return query.getSingleResult();
                }
        );
    }

    public PagosBoa getPagoByLocalizador(String pnr, String apellidoGrupo) {
        LOGGER.debug("Obteniendo registro Pago de reserva BoA por PNR:{} y Nombre:{}.", pnr, apellidoGrupo);
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<PagosBoa> q = cb.createQuery(PagosBoa.class);
                    Root<PagosBoa> p = q.from(PagosBoa.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("nroPNR"), StringUtils.trimToEmpty(pnr).toUpperCase()));
                    predicatesAnd.add(cb.equal(p.get("nombre"), StringUtils.trimToEmpty(apellidoGrupo).toUpperCase()));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<PagosBoa> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                        return query.getResultList().get(0);
                    } else {
                        return null;
                    }
                }
        );
    }

    public List<PagosBoa> getPagoByEstado(EstadosBoa estado) {
        LOGGER.debug("Obteniendo registro Pago de reserva BoA con Estado:[{}].", estado.getEstado() + "-" + estado.getDescripcion());
        return doWithTransaction(
                (entityManager, t) -> {

                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<PagosBoa> q = cb.createQuery(PagosBoa.class);
                    Root<PagosBoa> p = q.from(PagosBoa.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(p.get("estado").in(estado.getEstado()));

                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<PagosBoa> query = entityManager.createQuery(q);
                    return query.getResultList();
                }
        );
    }


    public List<PagosBoa> getPagoByEstadoFecha(EstadosBoa estado, Date fecha, int diferencia) {

        Date nuevaFecha = FormatosUtils.modificarMinutosDate(fecha, diferencia);
        LOGGER.debug("Obteniendo registro Pago de reserva BoA con Estado:[{}], hasta la fecha [{}].", estado.getEstado() + "-" + estado.getDescripcion(), FormatosUtils.fechaHoraLargaFormateada(nuevaFecha));
        return doWithTransaction(
                (entityManager, t) -> {

                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<PagosBoa> q = cb.createQuery(PagosBoa.class);
                    Root<PagosBoa> p = q.from(PagosBoa.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(p.get("estado").in(estado.getEstado()));
                    predicatesAnd.add(cb.lessThan(p.get("fechaCreacion"), nuevaFecha));

                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<PagosBoa> query = entityManager.createQuery(q);
                    return query.getResultList();
                }
        );
    }


    public List<PagosBoa> getPagosProcesadosByFecha(Date fechaIni, Date fechaFin, int moneda) {
        LOGGER.debug("Obteniendo Pagos procesados de BoA para el rango de fechas: [{} y {}].",
                FormatosUtils.formatoFechaHora(fechaIni), FormatosUtils.formatoFechaHora(fechaFin));
        return doWithTransaction(
                (entityManager, t) -> {

                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<PagosBoa> q = cb.createQuery(PagosBoa.class);
                    Root<PagosBoa> p = q.from(PagosBoa.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.between(p.get("fechaProceso"), fechaIni, fechaFin));
                    predicatesAnd.add(p.get("estado").in(EstadosBoa.PAGOAUTORIZADO.getEstado(), EstadosBoa.PAGOCOMPLETADO.getEstado()));
                    predicatesAnd.add(cb.equal(p.get("monedaImporteTotal"), moneda));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<PagosBoa> query = entityManager.createQuery(q);
                    return query.getResultList();
                }
        );
    }

    List<PagosBoa> getPagosProcesadosByFecha(Date fechaIni, Date fechaFin) {
        LOGGER.debug("Obteniendo Pagos procesados de BoA para el rango de fechas: [{} y {}].",
                FormatosUtils.formatoFechaHora(fechaIni), FormatosUtils.formatoFechaHora(fechaFin));
        return doWithTransaction(
                (entityManager, t) -> {

                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<PagosBoa> q = cb.createQuery(PagosBoa.class);
                    Root<PagosBoa> p = q.from(PagosBoa.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.between(p.get("fechaProceso"), fechaIni, fechaFin));
                    predicatesAnd.add(p.get("estado").in(EstadosBoa.PAGOAUTORIZADO.getEstado(), EstadosBoa.PAGOCOMPLETADO.getEstado()));
                    //predicatesAnd.add(cb.equal(p.get("monedaImporteTotal"), moneda));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    q.orderBy(cb.asc(p.get("monedaImporteTotal")), cb.asc(p.get("fechaProceso")));
                    TypedQuery<PagosBoa> query = entityManager.createQuery(q);
                    return query.getResultList();
                }
        );
    }


    public List<PagosBoa> getPagoSinRespuestaDesdeFecha(Date fecha) {
        LOGGER.debug("Obteniendo registro Pagos Sin Respuesta desde la fecha [{}].", FormatosUtils.fechaHoraLargaFormateada(fecha));
        return doWithTransaction(
                (entityManager, t) -> {

                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<PagosBoa> q = cb.createQuery(PagosBoa.class);
                    Root<PagosBoa> p = q.from(PagosBoa.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(p.get("estado").in(EstadosBoa.SINRESPUESTA.getEstado()));
                    predicatesAnd.add(cb.greaterThan(p.get("fechaCreacion"), fecha));

                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<PagosBoa> query = entityManager.createQuery(q);
                    return query.getResultList();
                }
        );
    }


    public String getNroCelularCliente(String nrocliente) {
        LOGGER.debug("Obteniendo el celular del cliente [{}].", nrocliente);
        return doWithTransaction(
                (entityManager, t) -> {
                    String celular = null;
                    String sql = "select CUCLPH as cel from cup003 where cubk=1 and cunbr=?";
                    Query query = entityManager.createNativeQuery(sql);
                    query.setParameter(1, nrocliente);

                    try {
                        BigDecimal numero = (BigDecimal) query.getSingleResult();
                        celular = numero != null ? numero.toPlainString() : "";
                    } catch (NonUniqueResultException e) {
                        LOGGER.error("Error al obtener el Numero de Celular del Cliente " + nrocliente + ". Se encontro mas de un registro. " + e.getMessage());
                        return null;
                    } catch (Exception e) {
                        LOGGER.error("Error al obtener el Numero de Celular del Cliente " + nrocliente + ". " + e.getMessage(), e);
                        return null;
                    }
                    return celular;
                });
    }

    @SuppressWarnings({"unchecked"})
    public List<TotalPagosBoa> getTotalPagosByFecha(final Date fechadesde, final Date fechahasta) {
        LOGGER.debug("Obteniendo totales pagados para BoA entre las fechas [{} --- {}].", fechadesde, fechahasta);
        return doWithTransaction(
                (entityManager, t) -> {
                    String sql = "SELECT DATE(BOAFECPRO) as fecha, BOATTOTMON as moneda, COUNT(BOAID) as cantidad, sum(BOATTOTAL) as total, " +
                            "sum(BOAWIMPTOT) as totalMonitor, \'EBISA\' as origen " +
                            "from EBPAGOSBOA " +
                            " WHERE BOAFECPRO >= ?" +
                            " and BOAFECPRO <= ?" +
                            " and BOAESTADO in(4, 12)" +
                            " group by DATE(BOAFECPRO), BOATTOTMON";

                    Query q = entityManager.createNativeQuery(sql, TotalPagosBoa.class).setParameter(1, fechadesde).setParameter(2, fechahasta);
                    List<TotalPagosBoa> totalPagosBoa = null;
                    try {
                        totalPagosBoa = (List<TotalPagosBoa>) q.getResultList();
                    } catch (NonUniqueResultException e) {
                        LOGGER.error("Error al obtener el total de pagos entre las fecha  [" + fechadesde + " - " + fechahasta + "].", e);
                        return null;
                    } catch (Exception e) {
                        LOGGER.error("Error al obtener el total de pagos entre las fecha  [" + fechadesde + " - " + fechahasta + "].", e);
                        return null;
                    }
                    return totalPagosBoa;
                });
    }


}
