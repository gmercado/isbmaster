package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 06-11-15.
 */
public class Vuelo {

    /**
     * posicion
     * Elemento “posicion” definido en el apartado 10.2.1.2.1
     * Identifica de manera única la información del pasajero en el PNR.
     */
    //@XmlElement(required = true)
    @JsonProperty(value = "posicion")
    private Posicion posicion;

    /**
     * linea
     * CC (A->Z, excepto Ñ, y 0->9).
     * Código de la compañía del usuario.
     */
    //@XmlElement(required = true)
    @JsonProperty(value = "linea")
    private String linea;

    /**
     * num_vuelo
     * NNNNA: cuatro números (entre 0 y 9) y un carácter alfabético
     * comprendido entre la A y la Z, exceptuando la Ñ.
     * 1<=Longitud<=5.
     * Número de vuelo.
     */
    //@XmlElement
    @JsonProperty(value = "num_vuelo")
    private String nroVuelo;

    /**
     * fecha_salida
     * DDMMMYY	Fecha de salida del vuelo.
     */
    //@XmlElement
    @JsonProperty(value = "fecha_salida")
    private String fechaSalida;

    /**
     * origen
     * AAA (A->Z, excepto Ñ, excepto Ñ).
     * Código del aeropuerto origen.
     */
    //@XmlElement
    @JsonProperty(value = "origen")
    private String origen;

    /**
     * destino
     * AAA (A->Z, excepto Ñ, excepto Ñ).
     * Código del aeropuerto destino.
     */
    @JsonProperty(value = "destino")
    private String destino;

    /**
     * hora_salida
     * HHMM 	Hora de salida del vuelo.
     */
    //@XmlElement
    @JsonProperty(value = "hora_salida")
    private String horaSalida;

    /**
     * hora_llegada
     * HHMM 	Hora de llegada del vuelo.
     */
    //@XmlElement
    @JsonProperty(value = "hora_llegada")
    private String horaLlegada;

    /**
     * clase
     * A (A->Z, excepto Ñ, excepto Ñ).
     * Clase en la que se ha realizado la reserva del vuelo.
     */
    //@XmlElement
    @JsonProperty(value = "clase")
    private String clase;

    /**
     * estado
     * AA (A-> Z, excepto Ñ).
     * Estado en el que se encuentra el segmento.
     * Entre los posibles valores del código de estado se encuentran:
     – HK: Confirmado.
     – TK: Confirmado (después de pasar Schedule Change).
     – RR: Reconfirmado.
     – KK: Confirmado (forzado).
     – HL: En lista de espera (después de pasar Schedule Change).
     – TL: En lista de espera.
     – KL: Confirmada lista de espera.
     – HN: En petición.
     – SA: Sujeto a espacio.
     – HX: Cancelado.
     – NO: Cancelado.
     – UN: Cancelado.
     – UC: Cancelada lista de espera.
     Para una documentación más exhaustiva de los códigos de estado, veáse referencia AIRIMP.
     *
     */
    //@XmlElement
    @JsonProperty(value = "estado")
    private String estado;

    //adicionado en las pruebas de BoA
    @JsonProperty(value = "msc")
    private String msc;

    //adicionado en las pruebas de BoA
    @JsonProperty(value = "cia_operadora")
    private String ciaOperadora;

    //adicionado en las pruebas de BoA
    @JsonProperty(value = "vuelo_operador")
    private String vueloOperador;

    //adicionado en las pruebas de BoA
    @JsonProperty(value = "clase_operador")
    private String claseOperador;

    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getNroVuelo() {
        return nroVuelo;
    }

    public void setNroVuelo(String nroVuelo) {
        this.nroVuelo = nroVuelo;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(String horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMsc() {
        return msc;
    }

    public void setMsc(String msc) {
        this.msc = msc;
    }

    public String getCiaOperadora() {
        return ciaOperadora;
    }

    public void setCiaOperadora(String ciaOperadora) {
        this.ciaOperadora = ciaOperadora;
    }

    public String getVueloOperador() {
        return vueloOperador;
    }

    public void setVueloOperador(String vueloOperador) {
        this.vueloOperador = vueloOperador;
    }

    public String getClaseOperador() {
        return claseOperador;
    }

    public void setClaseOperador(String claseOperador) {
        this.claseOperador = claseOperador;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Vuelo{");
        sb.append("posicion=").append(posicion);
        sb.append(", linea='").append(linea).append('\'');
        sb.append(", nroVuelo='").append(nroVuelo).append('\'');
        sb.append(", fechaSalida='").append(fechaSalida).append('\'');
        sb.append(", origen='").append(origen).append('\'');
        sb.append(", destino='").append(destino).append('\'');
        sb.append(", horaSalida='").append(horaSalida).append('\'');
        sb.append(", horaLlegada='").append(horaLlegada).append('\'');
        sb.append(", clase='").append(clase).append('\'');
        sb.append(", estado='").append(estado).append('\'');
        sb.append(", msc='").append(msc).append('\'');
        sb.append(", ciaOperadora='").append(ciaOperadora).append('\'');
        sb.append(", vueloOperador='").append(vueloOperador).append('\'');
        sb.append(", claseOperador='").append(claseOperador).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
