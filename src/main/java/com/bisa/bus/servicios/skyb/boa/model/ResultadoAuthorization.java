package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Created by rchura on 09-11-15.
 */
@JsonRootName(value = "SetAuthorizationResult")
public class ResultadoAuthorization {

    @JsonProperty(value = "SetAuthorizationResult")
    private String respuesta;

    @JsonIgnore
    private Autorizacion autorizacion;


    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Autorizacion getAutorizacion() {
        return autorizacion;
    }

    public void setAutorizacion(Autorizacion autorizacion) {
        this.autorizacion = autorizacion;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ResultadoAuthorization{");
        sb.append("respuesta='").append(respuesta).append('\'');
        sb.append(", autorizacion=").append(autorizacion);
        sb.append('}');
        return sb.toString();
    }
}
