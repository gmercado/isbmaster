package com.bisa.bus.servicios.skyb.boa.ws;

import com.bisa.bus.servicios.skyb.boa.model.Mensaje;

/**
 * Created by rchura on 20-11-15.
 */
public class BilleteMailResponse {

    private Long pagoID;

    private Mensaje mensaje;


    public Long getPagoID() {
        return pagoID;
    }

    public void setPagoID(Long pagoID) {
        this.pagoID = pagoID;
    }

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BilleteMailResponse{");
        sb.append("pagoID=").append(pagoID);
        sb.append(", mensaje=").append(mensaje);
        sb.append('}');
        return sb.toString();
    }


}
