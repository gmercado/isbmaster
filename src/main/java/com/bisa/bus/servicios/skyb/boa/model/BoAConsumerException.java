package com.bisa.bus.servicios.skyb.boa.model;

/**
 * Created by rchura on 30-11-15.
 */
public class BoAConsumerException extends Exception{

    private String mensajeExcepcion;

    public BoAConsumerException() {
        super();
        this.mensajeExcepcion = null;
    }

    public BoAConsumerException(String mensajeExcepcion) {
        super(mensajeExcepcion);
        this.mensajeExcepcion = mensajeExcepcion;
    }


    public BoAConsumerException(String mensajeExcepcion, Throwable cause) {
        super(mensajeExcepcion, cause);
        this.mensajeExcepcion = mensajeExcepcion;
    }
}
