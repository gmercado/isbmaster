package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Para un objeto individual cuando el objeto JSON no es un vector de objetos
 * Created by rchura on 30-12-15.
 *
 */
public class GetReporteDiario {

    @JsonProperty(value = "ResultDailyReconciliation")
    private ReporteDiario reporteDiario;

    public ReporteDiario getReporteDiario() {
        return reporteDiario;
    }

    public void setReporteDiario(ReporteDiario reporteDiario) {
        this.reporteDiario = reporteDiario;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GetReporteDiario{");
        sb.append("reporteDiario=").append(reporteDiario);
        sb.append('}');
        return sb.toString();
    }
}
