/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.bisa.bus.servicios.skyb.boa.ui;

import bus.database.components.FiltroPanel;
import com.bisa.bus.servicios.skyb.boa.entities.EstadosBoa;
import com.bisa.bus.servicios.skyb.boa.entities.PagosBoa;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author by rchura on 16-12-15.
 */
public class ListadoPagosBoaFiltroPanel extends FiltroPanel<PagosBoa> {

    public ListadoPagosBoaFiltroPanel(String id, IModel<PagosBoa> model1, IModel<PagosBoa> model2) {
        super(id, model1, model2);
        final String datePattern = "dd/MM/yyyy";
        DateTextField fechaDesde = new DateTextField("fechaDesde", new PropertyModel<>(model1, "fechaCreacion"), datePattern);
        fechaDesde.setRequired(true);
        fechaDesde.add(new DatePicker());

        DateTextField fechaHasta = new DateTextField("fechaHasta", new PropertyModel<>(model2, "fechaCreacion"), datePattern);
        fechaHasta.setRequired(true);
        fechaHasta.add(new DatePicker());

        add(new TextField<>("cliente", new PropertyModel<>(model1, "nroCliente")));
        add(fechaDesde);
        add(fechaHasta);
        add(new TextField<>("codigoPNR", new PropertyModel<>(model1, "nroPNR")));
        add(new DropDownChoice<>("estado", new PropertyModel<>(model1, "estadosBoa"),
                new LoadableDetachableModel<List<? extends EstadosBoa>>() {
                    @Override
                    protected List<? extends EstadosBoa> load() {
                        return new ArrayList<>(Arrays.asList((EstadosBoa.values())));
                    }
                }, new IChoiceRenderer<EstadosBoa>() {
            @Override
            public Object getDisplayValue(EstadosBoa object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(EstadosBoa object, int index) {
                return Integer.toString(index);
            }

            @Override
            public EstadosBoa getObject(String id, IModel<? extends List<? extends EstadosBoa>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));
    }
}