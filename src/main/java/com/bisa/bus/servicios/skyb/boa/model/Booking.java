package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 12-11-15.
 */
public class Booking {

    @JsonProperty(value = "reserva")
    private Reserva reserva;

    public Reserva getReserva() {
        return reserva;
    }

    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }

}
