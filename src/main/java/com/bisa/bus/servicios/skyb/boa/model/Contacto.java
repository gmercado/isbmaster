package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Informacion del Contacto
 * Created by rchura on 06-11-15.
 *
 */
@JsonRootName(value = "ct")
public class Contacto {


    /**
     * Posicion
     * Elemento “posicion” definido en el apartado 10.2.1.2.1	Identifica de manera única la información del pasajero en el PNR.
      */
    @JsonProperty(value = "posicion")
    private Posicion posicion;

    /**
     * Texto
     * Cadena de caracteres alfanuméricos.	Texto donde se encuentra la información de contacto.
     */
    @JsonProperty(value = "texto")
    private String infContacto;

    /**
     * num_pax
     * Numérico.
     * Número de pasajero al que va asociada la información de contacto.
     * En caso de que este campo no venga informado, la información de contacto se aplicará a todos los pasajeros de la reserva.
     */
    @JsonProperty(value = "num_pax")
    private Long nroPasajero;


    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }

    public String getInfContacto() {
        return infContacto;
    }

    public void setInfContacto(String infContacto) {
        this.infContacto = infContacto;
    }

    public Long getNroPasajero() {
        return nroPasajero;
    }

    public void setNroPasajero(Long nroPasajero) {
        this.nroPasajero = nroPasajero;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Contacto{");
        sb.append("posicion=").append(posicion);
        sb.append(", infContacto='").append(infContacto).append('\'');
        sb.append(", nroPasajero=").append(nroPasajero);
        sb.append('}');
        return sb.toString();
    }
}
