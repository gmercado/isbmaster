package com.bisa.bus.servicios.skyb.boa.entities;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by rchura on 31-12-15.
 */
public class ReembolsoPagosBoa {

    private EmpresaBoa empresa;

    private ServicioBoa servicio;

    private Date fecha;

    private Short moneda;

    private BigDecimal importeBisa;

    private BigDecimal importeBoa;

    private long cantidad;

    private String nota;

    private String canal;

    private BigDecimal cuentaReembolso;

    private Short monedaCuentaReembolso;

    private String tipoCuentaReembolso;

    private BigDecimal importeDiferencia;

    public EmpresaBoa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(EmpresaBoa empresa) {
        this.empresa = empresa;
    }

    public ServicioBoa getServicio() {
        return servicio;
    }

    public void setServicio(ServicioBoa servicio) {
        this.servicio = servicio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Short getMoneda() {
        return moneda;
    }

    public void setMoneda(Short moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getImporteBisa() {
        return importeBisa;
    }

    public void setImporteBisa(BigDecimal importeBisa) {
        this.importeBisa = importeBisa;
    }

    public BigDecimal getImporteBoa() {
        return importeBoa;
    }

    public void setImporteBoa(BigDecimal importeBoa) {
        this.importeBoa = importeBoa;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public BigDecimal getCuentaReembolso() {
        return cuentaReembolso;
    }

    public void setCuentaReembolso(BigDecimal cuentaReembolso) {
        this.cuentaReembolso = cuentaReembolso;
    }

    public Short getMonedaCuentaReembolso() {
        return monedaCuentaReembolso;
    }

    public void setMonedaCuentaReembolso(Short monedaCuentaReembolso) {
        this.monedaCuentaReembolso = monedaCuentaReembolso;
    }

    public String getTipoCuentaReembolso() {
        return tipoCuentaReembolso;
    }

    public void setTipoCuentaReembolso(String tipoCuentaReembolso) {
        this.tipoCuentaReembolso = tipoCuentaReembolso;
    }

    public BigDecimal getImporteDiferencia() {
        return importeDiferencia;
    }

    public void setImporteDiferencia(BigDecimal importeDiferencia) {
        this.importeDiferencia = importeDiferencia;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ReembolsoPagosBoa{");
        sb.append("empresa=").append(empresa);
        sb.append(", servicio=").append(servicio);
        sb.append(", fecha=").append(fecha);
        sb.append(", moneda=").append(moneda);
        sb.append(", importeBisa=").append(importeBisa);
        sb.append(", importeBoa=").append(importeBoa);
        sb.append(", cantidad=").append(cantidad);
        sb.append(", nota='").append(nota).append('\'');
        sb.append(", canal='").append(canal).append('\'');
        sb.append(", cuentaReembolso=").append(cuentaReembolso);
        sb.append(", monedaCuentaReembolso=").append(monedaCuentaReembolso);
        sb.append(", tipoCuentaReembolso='").append(tipoCuentaReembolso).append('\'');
        sb.append(", importeDiferencia=").append(importeDiferencia);
        sb.append('}');
        return sb.toString();
    }
}
