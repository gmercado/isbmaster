package com.bisa.bus.servicios.skyb.boa.entities;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rchura on 23-11-15.
 */
public enum MonedasBoa {

    BOB(0, "Bs."),
    USD(2, "$us.");

    private int codigo;
    private String abreviatura;

    MonedasBoa(int codigo, String abreviatura) {
        this.codigo = codigo;
        this.abreviatura = abreviatura;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getAbreviatura() {
        return abreviatura;
    }


    // Obtiene un objeto EstadosBoa a partir de un valor entero
    public static MonedasBoa valorEnum(int valor){
        List<MonedasBoa> monedas = new ArrayList<MonedasBoa>(Arrays.asList((MonedasBoa.values())));
        for(MonedasBoa mboa : monedas){
            if(valor==mboa.getCodigo()){
                return mboa;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MonedasBoa{");
        sb.append("codigo=").append(codigo);
        sb.append(", abreviatura='").append(abreviatura).append('\'');
        sb.append('}');
        return sb.toString();
    }


}
