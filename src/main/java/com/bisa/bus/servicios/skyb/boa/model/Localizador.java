package com.bisa.bus.servicios.skyb.boa.model;

import com.bisa.bus.servicios.skyb.boa.consumer.DatosConsumidor;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author rchura on 09-11-15
 */
public class Localizador {
    /**
     * pnr
     * Entre cinco y diez caracteres alfanuméricos   (A->Z, excepto Ñ y  0->9).
     * Código del localizador (PNR) que se va a visualizar.
     */
    @JsonProperty(value = "pnr")
    private String pnr;

    /**
     * identifierPnr
     * Cadena de al menos dos caracteres alfabéticos (A->Z, excepto Ñ) y los Caracteres especiales “ “ y “/”.
     * Primer apellido de alguno de los pasajeros con asiento incluidos en la reserva,
     * en el caso de ser una reserva de grupo, este campo corresponderá al nombre del mismo.
     */
    @JsonProperty(value = "identifierPnr")
    private String identificador;

    @JsonIgnore
    private DatosConsumidor datosConsumidor;

    public Localizador() {
    }

    public Localizador(String pnr, String identificador) {
        setPnr(pnr);
        setIdentificador(identificador);
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public DatosConsumidor getDatosConsumidor() {
        return datosConsumidor;
    }

    public void setDatosConsumidor(DatosConsumidor datosConsumidor) {
        this.datosConsumidor = datosConsumidor;
    }

    @Override
    public String toString() {
        return "Localizador{" + "pnr='" + pnr + '\'' + ", identificador='" + identificador + '\'' + '}';
    }
}
