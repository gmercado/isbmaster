package com.bisa.bus.servicios.skyb.boa.ws;

import com.bisa.isb.ws.catalog.Remark;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 * @author Roger Chura
 * @version $Id: WebServiceBoa.java; oct 30, 2015 10:50 PM $
 */
@WebService(
        targetNamespace = WebServiceBoa.NAMESPACE,
        name = "WebServiceBoa"
)
@Remark("Servicio Web para operaciones con BoA")
public interface WebServiceBoa {

    String NAMESPACE = "http://www.bisa.com/ebanking/services/soap";

    @WebMethod(operationName = "obtenerReserva", exclude = false)
    @WebResult(name = "reservaResult", targetNamespace = NAMESPACE)
    ConsultaReservaResponse getReserva(@WebParam(name = "requestReserva") ConsultaReservaRequest request);

    @WebMethod(operationName = "obtenerToken", exclude = false)
    @WebResult(name = "tokenResult", targetNamespace = NAMESPACE)
    TokenResponse getToken(@WebParam(name = "requestToken") TokenRequest request);

    @WebMethod(operationName = "pagarReserva", exclude = false)
    @WebResult(name = "pagoResult", targetNamespace = NAMESPACE)
    PagoReservaResponse payReserva(@WebParam(name = "requestPago") PagoReservaRequest request);

    @WebMethod(operationName = "obtenerTickets", exclude = false)
    @WebResult(name = "ticketResult", targetNamespace = NAMESPACE)
    TicketPagoResponse getTickets(@WebParam(name = "requestTicket") TicketPagoRequest request);

    @WebMethod(operationName = "enviarBilleteMail", exclude = false)
    @WebResult(name = "billeteMailResult", targetNamespace = NAMESPACE)
    BilleteMailResponse getInvoiceMail(@WebParam(name = "requestBilleteMail") BilleteMailRequest request);

    @WebMethod(operationName = "obtenerBillete", exclude = false)
    @WebResult(name = "billeteResult", targetNamespace = NAMESPACE)
    BilletePdfResponse getInvoicePdf(@WebParam(name = "requestBillete") BilletePdfRequest request);

    // Prueba de ambiente
    @WebMethod(operationName = "ambiente", exclude = false)
    @WebResult(name = "ambienteResult", targetNamespace = NAMESPACE)
    String getAmbiente(@WebParam(name = "requestAmbiente") String request);
}
