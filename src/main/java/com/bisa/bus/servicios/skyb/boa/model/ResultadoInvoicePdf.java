package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.io.DataInputStream;

/**
 * Created by rchura on 10-11-15.
 */

@JsonRootName(value = "resultGetInvoicePNRPDF")
public class ResultadoInvoicePdf {

    /**
     * PDF
     * Stream
     * Documento PDF que contiene todos los billetes asociados a la reserva.
     */
    @JsonProperty(value = "PDF")
    private DataInputStream pdf;


    public DataInputStream getPdf() {
        return pdf;
    }

    public void setPdf(DataInputStream pdf) {
        this.pdf = pdf;
    }
}
