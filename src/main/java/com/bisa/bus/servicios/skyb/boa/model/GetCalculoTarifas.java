package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by rchura on 17-11-15.
 */
public class GetCalculoTarifas {

    @JsonProperty(value ="calculo_tarifa")
    private List<CalculoTarifa> listaCalculos;

    public List<CalculoTarifa> getListaCalculos() {
        return listaCalculos;
    }

    public void setListaCalculos(List<CalculoTarifa> listaCalculos) {
        this.listaCalculos = listaCalculos;
    }
}
