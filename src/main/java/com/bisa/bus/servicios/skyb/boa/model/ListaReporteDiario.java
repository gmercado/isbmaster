package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by rchura on 09-12-15.
 */
public class ListaReporteDiario {

    @JsonProperty(value = "ResultDailyReconciliation")
    private List<ReporteDiario> reporteDiario;


    public List<ReporteDiario> getReporteDiario() {
        return reporteDiario;
    }

    public void setReporteDiario(List<ReporteDiario> reporteDiario) {
        this.reporteDiario = reporteDiario;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ListaReporteDiario{");
        sb.append("reporteDiario=").append(reporteDiario);
        sb.append('}');
        return sb.toString();
    }
}
