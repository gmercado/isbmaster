package com.bisa.bus.servicios.skyb.boa.ws;

/**
 * Created by rchura on 20-11-15.
 */
public class BilleteMailRequest  extends  RequestBoa{

    private Long pagoID;

    private String mails;

    public BilleteMailRequest(){}

    public BilleteMailRequest(String cliente, String usuario, String codigoPnr, String apellidoGrupo, String ipOrigen, String sistema) {
        super(cliente, usuario, codigoPnr, apellidoGrupo, ipOrigen, sistema);
    }

    public Long getPagoID() {
        return pagoID;
    }

    public void setPagoID(Long pagoID) {
        this.pagoID = pagoID;
    }

    public String getMails() {
        return mails;
    }

    public void setMails(String mails) {
        this.mails = mails;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BilleteMailRequest{");
        sb.append(super.toString());
        sb.append("pagoID=").append(pagoID);
        sb.append(", mails='").append(mails).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
