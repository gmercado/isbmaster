package com.bisa.bus.servicios.skyb.boa.api;

import bus.env.api.MedioAmbiente;
import com.bisa.bus.servicios.skyb.boa.consumer.*;
import com.bisa.bus.servicios.skyb.boa.entities.PagosBoa;
import com.bisa.bus.servicios.skyb.boa.model.*;
import com.bisa.bus.servicios.skyb.boa.ws.*;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.text.MessageFormat;
import java.util.Base64;
import java.util.List;
import static bus.env.api.Variables.*;

/**
 * @author rchura on 19-11-15
 */
public class ServiciosBoa implements ServicioBoa {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiciosBoa.class);
    private static final String AUTORIZADO ="1";
    private static final String NO_AUTORIZADO ="0";
    private static final String MENSAJE_CUANTA_CON_AUTORIZACION = "La reserva ya cuenta con una autorizacion";

    private final ConsumirToken consumirToken;
    private final ConsumirBooking consumirBooking;
    private final ConsumirAuthorization authorization;
    private final ConsumirTicketPNR consumirTicketPNR;
    private final ConsumirInvoiceEmail consumirInvoiceEmail;
    private final ConsumirInvoicePdf consumirInvoicePdf;
    private final ConsumirConciliacionDiaria conciliacionDiaria;
    private final ConsumirConciliacionMensual conciliacionMensual;
    private final MedioAmbiente medioAmbiente;

    @Inject
    public ServiciosBoa(ConsumirToken consumirToken, ConsumirBooking consumirBooking, ConsumirAuthorization authorization,
                        ConsumirTicketPNR consumirTicketPNR, ConsumirInvoiceEmail consumirInvoiceEmail, ConsumirInvoicePdf consumirInvoicePdf,
                        ConsumirConciliacionDiaria conciliacionDiaria, ConsumirConciliacionMensual conciliacionMensual, MedioAmbiente medioAmbiente) {
        this.consumirToken = consumirToken;
        this.consumirBooking = consumirBooking;
        this.authorization = authorization;
        this.consumirTicketPNR = consumirTicketPNR;
        this.consumirInvoiceEmail = consumirInvoiceEmail;
        this.consumirInvoicePdf = consumirInvoicePdf;
        this.conciliacionDiaria = conciliacionDiaria;
        this.conciliacionMensual = conciliacionMensual;
        this.medioAmbiente = medioAmbiente;
    }

    public ConsultaReservaResponse consultarReserva(ConsultaReservaRequest request) {
        ConsultaReservaResponse response;
        if (request == null) {
            LOGGER.error("Error: No se puede obtener los datos de Consulta para la Reserva <ConsultaReservaRequest>.");
            return null;
        }
        LOGGER.info("consultarReserva>> Iniciando consulta de reserva [PNR:{}, Nombre:{}] ", request.getCodigoPnr(), request.getApellidoGrupo());
        Localizador localizador = new Localizador(request.getCodigoPnr(), request.getApellidoGrupo());
        localizador.setDatosConsumidor(new DatosConsumidor(request.getCodigoPnr(), null));
        //Invocar metodo
        response = consumirBooking.getBooking(localizador);
        if (response == null) {
            LOGGER.error("Error: No se puede obtener los datos de la Reserva <ConsultaReservaRequest>.");
            response = new ConsultaReservaResponse();
            response.setMensaje(new Mensaje("", MessageType.ERROR, "No se puede obtener los datos de la Reserva."));
            return response;
        }
        if (response.getMensaje() == null) {
            response.setMensaje(new Mensaje("", MessageType.NONE, ""));
        }
        LOGGER.info("consultarReserva>> Finalizando consulta de reserva [PNR:{}, Nombre:{}] ", request.getCodigoPnr(), request.getApellidoGrupo());
        return response;
    }

    public TokenResponse obtenerToken(TokenRequest request) {
        TokenResponse response;
        if (request == null) {
            LOGGER.error("Error: No se puede obtener los datos de Solicitud para obtener el Token <TokenRequest>.");
            return null;
        }
        LOGGER.info("Iniciando solicitud de token para la reserva [PNR:{}, Nombre:{}] ", request.getCodigoPnr(), request.getApellidoGrupo());
        Localizador localizador = new Localizador(request.getCodigoPnr(), request.getApellidoGrupo());
        DatosConsumidor datosConsumidor = new DatosConsumidor(request.getCodigoPnr(), request.getPagoID());
        localizador.setDatosConsumidor(datosConsumidor);
        ResultadoToken token = consumirToken.getToken(localizador);
        if (token == null) {
            LOGGER.error("Error: No se puede obtener los datos del Token.");
            return null;
        }
        response = new TokenResponse();
        response.setToken(token.getToken());

        return response;
    }

    public PagoReservaResponse pagarReserva(PagoReservaRequest request, String token, String codigoAutorizacion) {

        PagoReservaResponse response = new PagoReservaResponse();
        if (request == null) {
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, "No se puede obtener los datos de Solicitud para efectuar el Pago.");
            response.setMensaje(mensaje);
            LOGGER.error("Error: No se puede obtener los datos de Solicitud para efectuar el Pago <PagoReservaRequest>.");
            return response;
        }

        LOGGER.info("Iniciando pago para la reserva [PNR:{}, Nombre:{}] ", request.getCodigoPnr(), request.getApellidoGrupo());
        Localizador localizador = new Localizador(request.getCodigoPnr(), request.getApellidoGrupo());
        DatosConsumidor datosConsumidor = new DatosConsumidor(request.getCodigoPnr(), request.getPagoID());
        localizador.setDatosConsumidor(datosConsumidor);
        //String endosoFactura = ""+request.getTipoDoc()+" "+request.getNumeroDoc();
        String endosoFactura = "NIT; " + request.getNumeroDoc() + "; " + request.getRazonSocial();
        String resp;
        Autorizacion autorizDefault = new Autorizacion("", "");

        ResultadoAuthorization resultadoAuthorization = authorization.setAuthorization(localizador, token, codigoAutorizacion, endosoFactura);
        if (resultadoAuthorization == null) {
            resp = "No se puede obtener el resultado del Pago.";
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, resp);
            response.setMensaje(mensaje);
            response.setAutorizacion(autorizDefault);
            LOGGER.error("Error Pago BoA con PNR[{}]. " + resp, localizador.getPnr() + "-" + localizador.getIdentificador());
            return response;
        }

        if (resultadoAuthorization.getAutorizacion() == null) {
            //ERROR no existe respuesta del pago
            resp = "No se tiene respuesta de BoA para el proceso de autorización del pago.";
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, resp);
            response.setMensaje(mensaje);
            response.setAutorizacion(autorizDefault);
            LOGGER.error("Error Pago BoA con PNR[{}]. " + resp, localizador.getPnr() + "-" + localizador.getIdentificador());
            return response;
        }

        response.setAutorizacion(resultadoAuthorization.getAutorizacion());
        Autorizacion autorizacion = resultadoAuthorization.getAutorizacion();

        if(false && ConsumidorServiciosBoa.SINRESPUESTA.equalsIgnoreCase(StringUtils.trimToEmpty(autorizacion.getEstado()))){
            // No tenemos respuesta de la autorizacion intentamos verificar mediane la obtención de los tickets.
            // Esperamos 2 segundo
            try {
                Thread.sleep(2000L);
            } catch (InterruptedException e) {
                LOGGER.error("Error: Nos han interrumpido en plena espera.");
            }

            ResultadoTicketPNR ticketPNR = consumirTicketPNR.getTicketPNR(localizador);
            if (ticketPNR == null) {
                // Para no revertir el débito
                Mensaje mensaje = new Mensaje("900", MessageType.SINRESPUESTA, "No se tiene respuesta de BoA al registrar el pago del boleto.");
                response.setMensaje(mensaje);
                LOGGER.error("No se tiene respuesta de BoA al registrar el pago del boleto.");
                // Adicionar estado por defecto para realizar reintentos de pago
                autorizDefault.setEstado("1");
                response.setAutorizacion(autorizDefault);
                return response;
            }

            MessageType tipoMensaje = ticketPNR.getMensaje() != null ? ticketPNR.getMensaje().getTipo() : null;
            if (tipoMensaje != null && MessageType.SINRESPUESTA.equals(tipoMensaje)) {
                response.setMensaje(ticketPNR.getMensaje());
                LOGGER.error("No se tiene respuesta de BoA al registrar el pago del boleto.");
                response.setAutorizacion(autorizDefault);
                return response;
            }

            // Verificamos si existen tickets emitidos.
            if (ticketPNR.getTicketPNR() != null) {
                TicketPNR ticket = ticketPNR.getTicketPNR();
                List<String> listaTk = ticket.getTickets() != null ? ticket.getTickets().getTicket() : null;
                if (listaTk != null && listaTk.size() > 0) {
                    autorizacion.setEstado(AUTORIZADO);
                } else {
                    autorizacion.setEstado(NO_AUTORIZADO);
                }
            }

        }

        //0: Determina que no se almaceno el código de autorización, y por ende no se solicitó la emisión del boleto.
        //1: Se realizó el almacenaje del código de autorización, y se solicitó la emisión del boleto.
        // Posibles respuestas
        //a. Estado = 1; Mensaje = "Se estableció correctamente la autorizacion."
        //b. Estado = 0; Mensaje = "El token no corresponde a la transacción."
        //c. Estado = 1; Mensaje = "La reserva ya cuenta con una autorizacion." Estado = 1 si el código de autorización que enviaste es el mismo que ya se encuentra registrado en la BD.
        //d. Estado = 0; Mensaje = "La reserva ya cuenta con una autorizacion." Estado = 0 si el código de autorización que enviaste es el distinto al que ya se encuentra registrado en la BD.

        if (AUTORIZADO.equalsIgnoreCase(StringUtils.trimToEmpty(autorizacion.getEstado()))) {
            LOGGER.info("Pago autorizado, se solicitó a BoA la emisi\u00f3n del boleto. {} ", autorizacion.getMensaje());
            //resp = "Pago autorizado, se solicitó a BoA la emisi\u00f3n del boleto. " + autorizacion.getMensaje();
            resp = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_COMPLETADO_PROCESO_EMISION, SKYB_BOA_MENSAJE_COMPLETADO_PROCESO_EMISION_DEFAULT);
            resp = MessageFormat.format(resp, autorizacion.getMensaje());
            Mensaje mensaje = new Mensaje(AUTORIZADO, MessageType.INFO, resp);
            response.setMensaje(mensaje);
            LOGGER.info("Pago BoA con PNR[{}]. "+resp, localizador.getPnr()+"-"+localizador.getIdentificador());
        }else if (ConsumidorServiciosBoa.SINRESPUESTA.equalsIgnoreCase(StringUtils.trimToEmpty(autorizacion.getEstado()))) {
            // Verificar si autorizacion no tiene respuesta marcar a estado sin respueta y proceder a reintento
            resp = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_PROCESO_EMISION, SKYB_BOA_MENSAJE_PROCESO_EMISION_DEFAULT);
            Mensaje mensaje = new Mensaje(autorizacion.getEstado(), MessageType.SINRESPUESTA, resp);
            response.setMensaje(mensaje);
            LOGGER.info("Pago BoA sin respuesta con PNR[{}]. " + resp, localizador.getPnr() + "-" + localizador.getIdentificador());
        }else{
            //resp = "Pago NO autoriado por BoA. No se puede solicitar la emisi\u00f3n del boleto. " + autorizacion.getMensaje();
            resp = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_NO_PROCESA_EMISION, SKYB_BOA_MENSAJE_NO_PROCESA_EMISION);
            resp = MessageFormat.format(resp, autorizacion.getMensaje());
            Mensaje mensaje = new Mensaje("" + autorizacion.getEstado(), MessageType.ERROR, resp);
            response.setMensaje(mensaje);
            LOGGER.info("Error Pago BoA con PNR[{}]. " + resp, localizador.getPnr() + "-" + localizador.getIdentificador());
        }

        if (response.getAutorizacion() == null) {
            response.setAutorizacion(autorizDefault);
        }

        LOGGER.debug("Finalizando pago para la reserva [PNR:{}, Nombre:{}] ", request.getCodigoPnr(), request.getApellidoGrupo());
        return response;
    }

    // Solicitud de autorización de emisión de boleto
    public PagoReservaResponse autorizarReserva(PagosBoa request, String token, String codigoAutorizacion){
        LOGGER.info("Inicio de proceso de autorizacion de emision de boleto, trabajo desatendito de verificacion de pago.");
        PagoReservaResponse response = new PagoReservaResponse();
        if(request==null){
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, "No se puede obtener los datos de Solicitud para efectuar el Pago.");
            response.setMensaje(mensaje);
            LOGGER.error("Error: No se puede obtener los datos de Solicitud para efectuar el Pago <PagoReservaRequest>.");
            return response;
        }

        LOGGER.info("Iniciando pago para la reserva [PNR:{}, Nombre:{}] ", request.getNroPNR(), request.getNombre());
        Localizador localizador = new Localizador(request.getNroPNR(), request.getNombre());
        DatosConsumidor datosConsumidor = new DatosConsumidor(request.getNroPNR(), request.getId());
        localizador.setDatosConsumidor(datosConsumidor);
        //String endosoFactura = ""+request.getTipoDoc()+" "+request.getNumeroDoc();
        String endosoFactura = "NIT; "+request.getNroDocumento()+"; "+request.getRazonSocial();
        String resp ="";
        Autorizacion autorizDefault = new Autorizacion("", "");

        ResultadoAuthorization resultadoAuthorization =  authorization.setAuthorization(localizador, token, codigoAutorizacion, endosoFactura);
        if (resultadoAuthorization==null){
            resp = "No se puede obtener el resultado del Pago.";
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, resp);
            response.setMensaje(mensaje);
            response.setAutorizacion(autorizDefault);
            LOGGER.error("Error Pago BoA con PNR[{}]. "+resp, localizador.getPnr()+"-"+localizador.getIdentificador());
            return response;
        }

        if(resultadoAuthorization.getAutorizacion()==null){
            // ERROR no existe respuesta del pago
            resp = "No se tiene respuesta de BoA para el proceso de autorización del pago.";
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, resp);
            response.setMensaje(mensaje);
            response.setAutorizacion(autorizDefault);
            LOGGER.error("Error Pago BoA con PNR[{}]. "+resp, localizador.getPnr()+"-"+localizador.getIdentificador());
            return response;
        }

        response.setAutorizacion(resultadoAuthorization.getAutorizacion());
        Autorizacion autorizacion = resultadoAuthorization.getAutorizacion();

        //0: Determina que no se almaceno el código de autorización, y por ende no se solicitó la emisión del boleto.
        //1: Se realizó el almacenaje del código de autorización, y se solicitó la emisión del boleto.
        // Posibles respuestas
        //a. Estado = 1; Mensaje = "Se estableció correctamente la autorizacion."
        //b. Estado = 0; Mensaje = "El token no corresponde a la transacción."
        //c. Estado = 1; Mensaje = "La reserva ya cuenta con una autorizacion." Estado = 1 si el código de autorización que enviaste es el mismo que ya se encuentra registrado en la BD.
        //d. Estado = 0; Mensaje = "La reserva ya cuenta con una autorizacion." Estado = 0 si el código de autorización que enviaste es el distinto al que ya se encuentra registrado en la BD.

        if(AUTORIZADO.equalsIgnoreCase(StringUtils.trimToEmpty(autorizacion.getEstado()))){
            resp = "Pago autorizado, se solicitó a BoA la emisi\u00f3n del boleto. "+autorizacion.getMensaje();
            resp = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_COMPLETADO_PROCESO_EMISION, SKYB_BOA_MENSAJE_COMPLETADO_PROCESO_EMISION_DEFAULT);
            resp = MessageFormat.format(resp, autorizacion.getMensaje());
            Mensaje mensaje = new Mensaje(AUTORIZADO, MessageType.INFO, resp);
            response.setMensaje(mensaje);
            LOGGER.info("Pago BoA con PNR[{}]. "+resp, localizador.getPnr()+"-"+localizador.getIdentificador());
        } else if (ConsumidorServiciosBoa.SINRESPUESTA.equalsIgnoreCase(StringUtils.trimToEmpty(autorizacion.getEstado()))) {
            // Verificar si autorizacion no tiene respuesta marcar a estado sin respueta y proceder a reintento
            resp = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_PROCESO_EMISION, SKYB_BOA_MENSAJE_PROCESO_EMISION_DEFAULT);
            Mensaje mensaje = new Mensaje(autorizacion.getEstado(), MessageType.SINRESPUESTA, resp);
            response.setMensaje(mensaje);
            LOGGER.info("Pago BoA sin respuesta con PNR[{}]. " + resp, localizador.getPnr() + "-" + localizador.getIdentificador());
        } else {
            resp = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_NO_PROCESA_EMISION, SKYB_BOA_MENSAJE_NO_PROCESA_EMISION);
            resp = MessageFormat.format(resp, autorizacion.getMensaje());
            Mensaje mensaje = new Mensaje("" + autorizacion.getEstado(), MessageType.ERROR, resp);
            response.setMensaje(mensaje);
            LOGGER.info("Error Pago BoA con PNR[{}]. " + resp, localizador.getPnr() + "-" + localizador.getIdentificador());
        }

        if(response.getAutorizacion()==null){
            response.setAutorizacion(autorizDefault);
        }

        LOGGER.debug("Finalizando pago para la reserva [PNR:{}, Nombre:{}] ", request.getNroPNR(), request.getNombre());
        return response;
    }

    public TicketPagoResponse obtenerTicket(TicketPagoRequest request) {

        TicketPagoResponse response = new TicketPagoResponse();
        if (request == null) {
            Mensaje mensaje = new Mensaje("", MessageType.REINTENTAR, "No se puede obtener los datos de Solicitud para obtener los Tickets del pago.");
            response.setMensaje(mensaje);
            LOGGER.error("Error: No se puede obtener los datos de Solicitud para obtener los Tickets del pago <TicketPagoRequest>.");
            return response;
        }

        LOGGER.info("Iniciando obtencion de tickets para la reserva [PNR:{}, Nombre:{}] ", request.getCodigoPnr(), request.getApellidoGrupo());
        Localizador localizador = new Localizador(request.getCodigoPnr(), request.getApellidoGrupo());
        DatosConsumidor datosConsumidor = new DatosConsumidor(request.getCodigoPnr(), request.getPagoID());
        localizador.setDatosConsumidor(datosConsumidor);

        ResultadoTicketPNR ticketPNR = consumirTicketPNR.getTicketPNR(localizador);
        if (ticketPNR == null) {
            Mensaje mensaje = new Mensaje("900", MessageType.REINTENTAR, "No se puede obtener los Tickets del Pago.");
            response.setMensaje(mensaje);
            LOGGER.error("Error: No se puede obtener los Tickets del Pago.");
            return response;
        }

        if (ticketPNR.getMensaje() != null) {
            response.setMensaje(ticketPNR.getMensaje());
        } else {
            Mensaje mensaje = new Mensaje("", MessageType.NONE, "");
            response.setMensaje(mensaje);
        }
        if (ticketPNR.getTicketPNR() != null) response.setTicketPNR(ticketPNR.getTicketPNR());

        response.setPagoID(request.getPagoID());

        LOGGER.debug("Finalizando obtencion de tickets para la reserva [PNR:{}, Nombre:{}] ", request.getCodigoPnr(), request.getApellidoGrupo());
        return response;
    }

    public BilleteMailResponse enviarBilleteMail(BilleteMailRequest request) {

        BilleteMailResponse response = new BilleteMailResponse();
        if (request == null) {
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, "No se puede obtener los datos de Solicitud para enviar el Billete por mail.");
            response.setMensaje(mensaje);
            LOGGER.error("Error: No se puede obtener los datos de Solicitud para enviar el Billete por mail <BilleteMailRequest>.");
            return response;
        }
        LOGGER.info("Iniciando envio de billetes por mail para la reserva [PNR:{}, Nombre:{} y Mail:{}].", request.getCodigoPnr(), request.getApellidoGrupo(), request.getMails());
        Localizador localizador = new Localizador(request.getCodigoPnr(), request.getApellidoGrupo());
        DatosConsumidor datosConsumidor = new DatosConsumidor(request.getCodigoPnr(), request.getPagoID());
        localizador.setDatosConsumidor(datosConsumidor);

        ResultadoInvoiceMail invoiceMail = consumirInvoiceEmail.getInvoicePNREmail(localizador, request.getMails());

        if (invoiceMail == null) {
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, "No se puede enviar el Billete por mail.");
            response.setMensaje(mensaje);
            LOGGER.error("Error: No se puede enviar el Billete por mail.");
            return response;
        }

        if (invoiceMail.getMensaje() != null) {
            response.setMensaje(invoiceMail.getMensaje());
        } else {
            Mensaje mensaje = new Mensaje("", MessageType.NONE, "");
            response.setMensaje(mensaje);
        }

        response.setPagoID(request.getPagoID());

        LOGGER.debug("Finalizando envio de billetes por mail para la reserva [PNR:{}, Nombre:{} y Mail:{}].", request.getCodigoPnr(), request.getApellidoGrupo(), request.getMails());
        return response;
    }

    public BilletePdfResponse obtenerBilletePdf(BilletePdfRequest request) {

        BilletePdfResponse response = new BilletePdfResponse();
        if (request == null) {
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, "No se puede obtener los datos de Solicitud para obtener el Billete en PDF.");
            response.setMensaje(mensaje);
            LOGGER.error("Error: No se puede obtener los datos de Solicitud para obtener el Billete en PDF <BilletePdfRequest>.");
            return response;
        }
        LOGGER.info("Iniciando impresion de billetes PDF para la reserva [PNR:{}, Nombre:{}] ", request.getCodigoPnr(), request.getApellidoGrupo());
        Localizador localizador = new Localizador(request.getCodigoPnr(), request.getApellidoGrupo());
        DatosConsumidor datosConsumidor = new DatosConsumidor(request.getCodigoPnr(), request.getPagoID());
        localizador.setDatosConsumidor(datosConsumidor);

        ByteArrayOutputStream outputStream = consumirInvoicePdf.getInvoicePDF(localizador);

        if (outputStream != null) {

            // Codificamos los bytes en Base64 para enviar como texto. El consumidor debe hacer el proceso inverso.
            Base64.Encoder encoder = Base64.getEncoder();
            String stringEncoded = encoder.encodeToString(outputStream.toByteArray()); //a.getBytes(StandardCharsets.UTF_8) );
            response.setPdfArray(stringEncoded);

            Mensaje mensaje = new Mensaje("0", MessageType.NONE, "Billete en formato PDF obtenido correctamente desde el servicio de BoA.");
            response.setMensaje(mensaje);

        } else {

            Mensaje mensaje = new Mensaje("900", MessageType.ERROR, "No se puede obtener el Billete en formato PDF desde el servicio de BoA.");
            response.setMensaje(mensaje);
            LOGGER.error("Error: No se puede obtener el Billete en formato PDF desde el servicio de BoA.");
        }

        response.setPagoID(request.getPagoID());

        LOGGER.debug("Finalizando impresion de billetes PDF para la reserva [PNR:{}, Nombre:{}] ", request.getCodigoPnr(), request.getApellidoGrupo());
        return response;

    }

    public ResultadoConciliacionDiaria obtenerReporteDiario(String fecha) {

        ResultadoConciliacionDiaria response = new ResultadoConciliacionDiaria();
        if (fecha == null) {
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, "No se puede obtener el parametro fecha para la Conciliacion Diaria.");
            response.setMensaje(mensaje);
            LOGGER.error("Error: No se puede obtener el parametro fecha para la Conciliacion Diaria <fecha>.");
            return response;
        }

        LOGGER.info("Obteniendo conciliacion diaria de BoA para la fecha: " + fecha);
        response = conciliacionDiaria.getConcilicionDiaria(fecha);
        if (response == null) {
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, "No se pueden obtener los datos de la Conciliacion Diaria.");
            response = new ResultadoConciliacionDiaria();
            response.setMensaje(mensaje);
            LOGGER.error("Error: No se pueden obtener los datos de la Conciliacion Diaria.");
            return response;
        }
        if (response.getMensaje() == null) {
            Mensaje mensaje = new Mensaje("", MessageType.NONE, "");
            response.setMensaje(mensaje);
        }
        return response;
    }

    @SuppressWarnings("unused")
    public ResultadoConciliacionMensual obtenerReporteMensaual(Integer mes, Integer anio) {

        ResultadoConciliacionMensual response = new ResultadoConciliacionMensual();
        if (mes == null || anio == null) {
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, "No se puede obtener parametros de mes y año para la Conciliacion Mensual.");
            response.setMensaje(mensaje);
            LOGGER.error("Error: No se puede obtener parametros de mes y año para la Conciliacion Mensual <mes, anio>.");
            return response;
        }

        LOGGER.info("Obteniendo conciliacion mensual de BoA para el mes:{}, anio:{}. ", mes, anio);
        response = conciliacionMensual.getConcilicionMensual(mes, anio);
        if (response == null) {
            Mensaje mensaje = new Mensaje("", MessageType.ERROR, "No se pueden obtener los datos de la Conciliacion Mensual.");
            response = new ResultadoConciliacionMensual();
            response.setMensaje(mensaje);
            LOGGER.error("Error: No se pueden obtener los datos de la Conciliacion Mensual.");
            return response;
        }

        if (response.getMensaje() == null) {
            Mensaje mensaje = new Mensaje("", MessageType.NONE, "");
            response.setMensaje(mensaje);
        }
        return response;
    }
}
