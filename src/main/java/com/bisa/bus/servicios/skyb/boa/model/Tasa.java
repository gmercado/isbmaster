package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.math.BigDecimal;

/**
 * Created by rchura on 06-11-15.
 */
@JsonRootName(value = "tasa")
public class Tasa {

    /**
     * codigo_tasa
     * Cadena de caracteres.	Código de la tasa.
     */
    @JsonProperty(value = "codigo_tasa")
    private String codigoTasa;

    /**
     * importe_tasa
     * Decimal. El delimitador de los decimales es el carácter “.”.
     * Importe de la tasa.
     */
    @JsonProperty(value = "importe_tasa")
    private BigDecimal importeTasa;

    /**
     * moneda_tasa
     * Cadena de caracteres.  Moneda de la tasa.
     */
    @JsonProperty(value = "moneda_tasa")
    private String monedaTasa;

    /**
     * tipo_tasa
     * Cadena de caracteres. 	Tipo de tasa.
     */
    @JsonProperty(value = "tipo_tasa")
    private String tipoTasa;

    public String getCodigoTasa() {
        return codigoTasa;
    }

    public void setCodigoTasa(String codigoTasa) {
        this.codigoTasa = codigoTasa;
    }

    public BigDecimal getImporteTasa() {
        return importeTasa;
    }

    public void setImporteTasa(BigDecimal importeTasa) {
        this.importeTasa = importeTasa;
    }

    public String getMonedaTasa() {
        return monedaTasa;
    }

    public void setMonedaTasa(String monedaTasa) {
        this.monedaTasa = monedaTasa;
    }

    public String getTipoTasa() {
        return tipoTasa;
    }

    public void setTipoTasa(String tipoTasa) {
        this.tipoTasa = tipoTasa;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Tasa{");
        sb.append("codigoTasa='").append(codigoTasa).append('\'');
        sb.append(", importeTasa=").append(importeTasa);
        sb.append(", monedaTasa='").append(monedaTasa).append('\'');
        sb.append(", tipoTasa='").append(tipoTasa).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
