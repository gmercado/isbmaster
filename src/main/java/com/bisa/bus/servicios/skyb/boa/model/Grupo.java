package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 05-11-15.
 */
public class Grupo {

    /**
     * Cadena de caracteres.	Nombre del grupo.
     */
    @JsonProperty(value ="Nombre")
    private String nombre;

    /**
     * Entero.	Define el número de pasajeros que pertenece al grupo.
     */
    @JsonProperty(value ="num_pasajeros")
    private Long nroPasajeros;

    /**
     * Identifica de manera única la información del grupo en el PNR.
     */
    @JsonProperty(value ="Posicion")
    private Posicion posicion;


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getNroPasajeros() {
        return nroPasajeros;
    }

    public void setNroPasajeros(Long nroPasajeros) {
        this.nroPasajeros = nroPasajeros;
    }

    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }
}
