package com.bisa.bus.servicios.skyb.boa.consumer;

import com.bisa.bus.servicios.skyb.boa.model.EntradaInvoiceEmail;
import com.bisa.bus.servicios.skyb.boa.model.Localizador;
import com.bisa.bus.servicios.skyb.boa.model.Mensaje;
import com.bisa.bus.servicios.skyb.boa.model.ResultadoInvoiceMail;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.*;


/**
 * Created by rchura on 09-11-15.
 */
public class ConsumirInvoiceEmail extends ConsumidorServiciosBoa {

    @Inject
    public ConsumirInvoiceEmail(@SQLFueraDeLinea ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente) {
        super(executor, consumoWebDao, medioAmbiente);
    }


    @Override
    protected String getUrl() {
        return medioAmbiente.getValorDe(SKYB_BOA_URL_RESERVA, SKYB_BOA_URL_RESERVA_DEFAULT);
    }

    @Override
    public String getNombreMetodo() {
        return medioAmbiente.getValorDe(SKYB_BOA_METODO_INVOICEMAIL, SKYB_BOA_METODO_INVOICEMAIL_DEFAULT);
    }

    protected String getParametros(Localizador localizador, String mail) {

        if(localizador==null){
            LOGGER.error("NO se puede obtener los datos del \'Localizador (pnr, identificador)\'");
            return null;
        }

        Map<String, String> map = getEntradaBase();
        EntradaInvoiceEmail invoiceMail = new EntradaInvoiceEmail(map.get("credencial"), map.get("ip"), Boolean.FALSE,
                localizador, map.get("lenguaje"), mail);

        ObjectMapper mapper = new ObjectMapper();
        String solicitud = null;
        try {
            solicitud = mapper.writeValueAsString(invoiceMail);
            LOGGER.debug("SOLICITUD JSON ES <ConsumirInvoiceEmail> ---->: " + solicitud);
        } catch (JsonProcessingException e) {
            LOGGER.error("ERROR: <JsonProcessingException> al ejecutar el metodo: getParametros().", e);
            solicitud = null;
        }
        return solicitud;
    }

    public ResultadoInvoiceMail getInvoicePNREmail(Localizador localizador, String mail){
        String respuesta = null;
        try {
            respuesta = (String) sendPost(getParametros(localizador, mail), localizador.getDatosConsumidor());
        } catch (IOException e) {
            LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getInvoicePNREmail().", e);
            respuesta = null;
        }

        ResultadoInvoiceMail resultadoInvoiceMail = null;
        if(respuesta!=null){
            resultadoInvoiceMail = getInvoiceMail(respuesta);
        }
        return resultadoInvoiceMail;
    }


    private ResultadoInvoiceMail getInvoiceMail(String cadenaJson){

        ObjectMapper objectMapper = new ObjectMapper();
        ResultadoInvoiceMail resultadoInvoiceMail = null;
        Mensaje mensaje = null;
        LOGGER.debug("JSON--->"+cadenaJson);

        try {
            resultadoInvoiceMail = objectMapper.readValue(cadenaJson, ResultadoInvoiceMail.class);
            LOGGER.info("ResultadoInvoiceMail OBJETO ---> " + resultadoInvoiceMail.getRespuesta());
        }catch (JsonMappingException e) {
            LOGGER.warn("ERROR: <JsonMappingException> al ejecutar el metodo: getInvoiceMail().", e);
            resultadoInvoiceMail = null;
        } catch (JsonParseException e) {
            LOGGER.warn("ERROR: <JsonParseException> al ejecutar el metodo: getInvoiceMail().", e);
            resultadoInvoiceMail = null;
        } catch (IOException e) {
            LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getInvoiceMail().", e);
            resultadoInvoiceMail = null;
        }

        if(resultadoInvoiceMail!=null){
            if(StringUtils.trimToNull(resultadoInvoiceMail.getRespuesta())!=null){
                mensaje = getMensaje(resultadoInvoiceMail.getRespuesta());
            }

            if(mensaje!=null) resultadoInvoiceMail.setMensaje(mensaje);
        }
        return resultadoInvoiceMail;
    }

}
