package com.bisa.bus.servicios.skyb.boa.entities;

/**
 * Created by rchura on 23-11-15.
 */
public enum EmpresaBoa {

    BOA("BoA", "Boliviana de Aviaci\u00f3n");


    private String codigo;
    private String descripcion;

    EmpresaBoa(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
