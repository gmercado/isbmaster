package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by rchura on 18-11-15.
 */
public class TicketPNR {

    private static final Logger LOGGER = LoggerFactory.getLogger(TicketPNR.class);

    /**
     * pax
     * Listado de elementos Pax, donde cada Pax es una cadena de caracteres alfanumérica.
     * Lista los apellidos y nombres de los titulares que son parte de la reserva.
     */
    //@JsonProperty(value = "pax")
    @JsonProperty(value = "pasajeros")
    private ListaTitular titulares;

    /**
     * tickets
     * Listado de elementos Tkt, donde cada Tkt es una cadena NNNNNNNNNNNNN (13 dígitos).
     * Lista de los números de  billete que corresponden a cada pasajero.
     * Los números de billete están en el mismo orden en el que se encuentran los pasajeros.
     */
    //@JsonProperty(value = "tickets")
    @JsonProperty(value = "tkts")
    private ListaTicket tickets;


    public ListaTitular getTitulares() {
        return titulares;
    }

    public void setTitulares(ListaTitular titulares) {
        this.titulares = titulares;
    }

    public ListaTicket getTickets() {
        return tickets;
    }

    public void setTickets(ListaTicket tickets) {
        this.tickets = tickets;
    }


    public String getTitularesTickets(){
        if(titulares==null || tickets==null){
            return null;
        }

        if(titulares.getTitular().size()<=0 || tickets.getTicket().size()<=0){
            return null;
        }

        StringBuffer sb = new StringBuffer();
        int cantidad=0;

        for(String titular : titulares.getTitular()) {
            sb.append("[");
            sb.append(titular);
            sb.append(":");
            try {
                sb.append(tickets.getTicket().get(cantidad++));
            }catch(Exception e) {
                LOGGER.error("Error: No se puede determinar el Numero de CantidadTicket para el Titular:["+titular+"] de la lista enviada por Boa,", e);
                sb.append("Indeterminado");
            }
            sb.append("] ");
        }

        return sb.toString();
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TicketPNR{");
        sb.append("titulares=").append(titulares.getTitular());
        sb.append(", tickets=").append(tickets.getTicket());
        sb.append('}');
        return sb.toString();
    }

}
