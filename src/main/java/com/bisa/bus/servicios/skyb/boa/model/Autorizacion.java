package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 09-11-15.
 */
public class Autorizacion {

    /**
     * State
     * Entero
     * Resultado de los procesos de validación y almacenaje del código de autorización.
     * Los valores posibles son:
     * 0: Determina que no se almaceno el código de autorización, y por ende no se solicitó la emisión del boleto.
     * 1: Se realizó el almacenaje del código de autorización, y se solicitó la emisión del boleto.
     */
    @JsonProperty(value = "Estado")
    private String estado;

    /**
     * Message
     * Cadena de caracteres.
     * Primer apellido de alguno de los pasajeros con asiento incluidos en la reserva,
     * en el caso de ser una reserva de grupo, este campo corresponderá al nombre del mismo.
     */
    @JsonProperty(value = "Mensaje")
    private String mensaje;

    public Autorizacion() {
    }

    public Autorizacion(String estado, String mensaje) {
        this.estado = estado;
        this.mensaje = mensaje;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Autorizacion{");
        sb.append("estado='").append(estado).append('\'');
        sb.append(", mensaje='").append(mensaje).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
