package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Created by rchura on 06-11-15.
 */
public class Pago {

    /**
     * importe
     * Decimal. El delimitador de los decimales es el carácter “.”.
     * Importe total del itinerario
     */
    @JsonProperty(value = "importe")
    private BigDecimal importe;

    /**
     * moneda
     * Cadena de caracteres alfanuméricos.
     * Moneda en la que se ha realizado la reserva.
     */
    @JsonProperty(value = "moneda")
    private String moneda;


    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Pago{");
        sb.append("importe=").append(importe);
        sb.append(", moneda='").append(moneda).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
