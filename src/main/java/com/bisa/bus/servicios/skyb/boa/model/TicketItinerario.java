package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by rchura on 06-11-15.
 */
public class TicketItinerario {

    /**
     * calculo_tarifa
     * Listado de elementos “calculo_tarifa”, definido en el apartado 10.2.1.10.1
     * Itinerario junto con sus importes asociados.
     */
    @JsonProperty(value = "calculo_tarifa")
    private List<CalculoTarifa> calculoTarifa;

    /**
     * tarifa
     * Listado de elementos “tarifa”, definido en el apartado 10.2.1.10.2
     * Conjunto de tarifas y tasas asociados al itinerario
     */
    @JsonProperty(value = "tarifa")
    private List<Tarifa> tarifa;

    /**
     * forma_pago
     * Listado de elementos “pagos”, definido en el apartado 10.2.1.10.3
     * Conjunto de formas de pago aplicadas sobre la reserva al emitir esta.
     */

    @JsonProperty(value = "forma_pago")
    private List<FormaPago> formaPago;


    public List<CalculoTarifa> getCalculoTarifa() {
        return calculoTarifa;
    }

    public void setCalculoTarifa(List<CalculoTarifa> calculoTarifa) {
        this.calculoTarifa = calculoTarifa;
    }

    public List<Tarifa> getTarifa() {
        return tarifa;
    }

    public void setTarifa(List<Tarifa> tarifa) {
        this.tarifa = tarifa;
    }

    public List<FormaPago> getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(List<FormaPago> formaPago) {
        this.formaPago = formaPago;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TicketItinerario{");
        sb.append("calculoTarifa=").append(calculoTarifa);
        sb.append(", tarifa=").append(tarifa);
        sb.append(", formaPago=").append(formaPago);
        sb.append('}');
        return sb.toString();
    }
}
