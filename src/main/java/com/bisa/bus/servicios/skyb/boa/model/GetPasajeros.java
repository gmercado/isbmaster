package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by rchura on 16-11-15.
 */
public class GetPasajeros {

    @JsonProperty("pasajeroDR")
    private List<Pasajero> listaPasajeros;

    public List<Pasajero> getListaPasajeros() {
        return listaPasajeros;
    }

    public void setListaPasajeros(List<Pasajero> listaPasajeros) {
        this.listaPasajeros = listaPasajeros;
    }
}
