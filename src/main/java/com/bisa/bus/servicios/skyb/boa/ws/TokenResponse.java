package com.bisa.bus.servicios.skyb.boa.ws;

/**
 * @author Roger Chura
 * @version $Id: WebServiceBoa.java; oct 30, 2015 10:50 PM $
 */

public class TokenResponse {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TokenResponse{");
        sb.append("token='").append(token).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
