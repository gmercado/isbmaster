package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;

/**
 * Created by rchura on 10-11-15.
 */
@JsonRootName(value = "DailyReconciliationResult")
public class ResultadoConciliacionDiaria {


    @JsonProperty(value = "DailyReconciliationResult")
    private String respuesta;

    @JsonIgnore
    private Mensaje mensaje;

    @JsonIgnore
    private List<ReporteDiario> reporteDiario;

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    public List<ReporteDiario> getReporteDiario() {
        return reporteDiario;
    }

    public void setReporteDiario(List<ReporteDiario> reporteDiario) {
        this.reporteDiario = reporteDiario;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ResultadoConciliacionDiaria{");
        sb.append("respuesta='").append(respuesta).append('\'');
        sb.append(", mensaje=").append(mensaje);
        sb.append(", reporteDiario=").append(reporteDiario);
        sb.append('}');
        return sb.toString();
    }
}
