package com.bisa.bus.servicios.skyb.boa.consumer;

import bus.consumoweb.consumer.ConsumidorServiciosWeb;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.env.api.MedioAmbiente;
import bus.plumbing.utils.Watch;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.wicket.util.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

import static bus.plumbing.utils.Watches.CONSUMO_WS;
import static bus.env.api.Variables.*;

/**
 * Created by rchura on 04-11-15.
 */
public abstract class SendHTTPRequest extends ConsumidorServiciosWeb {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private static final String USER_AGENT = "Mozilla/5.0";

    public SendHTTPRequest(ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente) {
        super(executor, consumoWebDao, medioAmbiente);
    }

    protected abstract String getUrl();

    protected abstract Proxy getProxy();

    public Object sendGet(String cadenaParametros) throws IOException {

        LOGGER.debug("Iniciando ejecucion HTTP GET request");
        String respuesta = null;
        String cadenaurl = ""+getUrl()+"?"+cadenaParametros;
        LOGGER.debug("URL + Parametros =>" + cadenaurl);

        URL url = new URL(cadenaurl);
        Proxy proxy = getProxy();
        HttpsURLConnection con = null;
        if(proxy==null){
            LOGGER.debug("Estableciendo conexion sin Proxy.");
            con = (HttpsURLConnection) url.openConnection();
        }else{
            LOGGER.debug("Estableciendo conexion con Proxy.");
            con = (HttpsURLConnection) url.openConnection(proxy);
        }

        if(con==null){
            LOGGER.error("NO se puede establecer la conexion a la URL:"+cadenaurl);
            return null;
        }

        LOGGER.debug("Estableciendo metodo y parametros");
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);

        LOGGER.debug("Obteniendo la respuesta de la conexion");
        int responseCode = con.getResponseCode();
        String inputLine;
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        if(responseCode!=200){
            LOGGER.error("Ocurrio un error al obtener la respuesta,  HTTP Response Code="+responseCode+ "Error Message="+response.toString());
            respuesta = null;
        }else{
            LOGGER.debug("Ejecucion correcta de la conexion, HTTP Response Code="+responseCode);
            respuesta = response.toString();
        }
        LOGGER.debug("Respuesta de la solicitud =" + respuesta+"; HTTP Response Code="+responseCode);
        return respuesta;
    }


    public Object sendPost(String cadenaParametros, DatosConsumidor datosConsumidor) throws IOException {

        Watch watch = new Watch(CONSUMO_WS);
        watch.start();

        LOGGER.debug("Iniciando ejecucion HTTP POST request");
        Future<ConsumoWeb> consumoWebFuture = null;
        String respuesta = null;
        String urlServer = getUrl()+getNombreMetodo();
        URL url = new URL(urlServer);
        Proxy proxy = getProxy();
        HttpURLConnection con = null;
        int responseCode = -1;
        String urlParameters = ""+cadenaParametros;

        int timeOut = medioAmbiente.getValorIntDe(WC_TIMEOUT, WC_TIMEOUT_DEFAULT);

        try{

            consumoWebFuture = registrarConsumo(urlParameters);

            if(proxy==null){
                LOGGER.debug("Estableciendo conexion sin Proxy.");
                con = (HttpURLConnection) url.openConnection();
            }else{
                LOGGER.debug("Estableciendo conexion con Proxy.");
                con = (HttpURLConnection) url.openConnection(proxy);
            }
            if(con==null){
                LOGGER.error("NO se puede establecer la conexion a la URL:"+urlServer);
                return null;
            }

            LOGGER.debug("Estableciendo metodo y parametros");
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.addRequestProperty("Content-Type", newContentType());
            LOGGER.debug("TIMEOUT->"+con.getConnectTimeout()+  "READTIMEOUT->"+con.getReadTimeout());
            con.setConnectTimeout(timeOut);
            con.setReadTimeout(timeOut);
            LOGGER.debug("TIMEOUT->"+con.getConnectTimeout()+  "READTIMEOUT->"+con.getReadTimeout());

            //con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            if(StringUtils.trimToNull(urlParameters)==null){
                LOGGER.error("No se tiene definido los parametros para la URL:"+urlServer);
                return null;
            }

            LOGGER.debug("Escribiendo los parametros en la URL, Send post request = " + urlParameters);
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            LOGGER.debug("Obteniendo la respuesta de la conexion.");
            BufferedReader input = null;
            try {
                responseCode = con.getResponseCode();
                String inputLine;
                input = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuffer response = new StringBuffer();
                while ((inputLine = input.readLine()) != null) {
                    response.append(inputLine);
                }
                input.close();

                if(responseCode!=200){
                    LOGGER.error("Ocurrio un error al obtener la respuesta,  HTTP Response Code="+responseCode+ "Error Message="+response.toString());
                    respuesta = "HTTP Response Code="+responseCode+ "Error Message="+response.toString();
                    throw new IOException(respuesta);
                }else{
                    LOGGER.debug("Ejecucion correcta de la conexion, HTTP Response Code=" + responseCode);
                    respuesta = response.toString();
                }
                LOGGER.debug("Respuesta de la solicitud =" + respuesta+"; HTTP Response Code="+responseCode);

                return respuesta;

            }catch (IOException e){
                LOGGER.error("ERROR: <IOException> al ejecutar el metodo: sendPost().", e);
                respuesta = e.getMessage();
                throw e;
            }catch (Exception e){
                LOGGER.error("ERROR: <Exception> al ejecutar el metodo: sendPost().", e);
                respuesta = e.getMessage();
                throw (IOException)e;
            }finally {
                IOUtils.closeQuietly(input);
            }

        }finally {
            if (con != null) {
                try {
                    con.disconnect();
                    LOGGER.debug("Cerrando la conexion.");
                } catch (Exception e) {
                    LOGGER.error("Error al cerrar", e);
                } finally {
                    final int milis = (int) watch.stop();
                    actualizaConsumo(respuesta, responseCode, milis, datosConsumidor, consumoWebFuture);
                }
            }else {
                final int milis = (int) watch.stop();
                actualizaConsumo(respuesta, responseCode, milis, datosConsumidor, consumoWebFuture);
            }
        }
    }


    public Map<String, String> getRespuesta( String respuesta){

        if(StringUtils.trimToNull(respuesta)==null){
            return null;
        }
        LOGGER.debug("CADENA INICIAL =" + respuesta);
        Map<String, String> mapa = new HashMap<String, String>();
        String cadena = StringUtils.replaceEachRepeatedly(respuesta, new String[]{"{", "}", "\""}, new String[]{";", ";", ""});
        LOGGER.debug("NUEVA CADENA ="+ cadena);
        String[] split = StringUtils.split(cadena, ";");
        for(String c : split){
            String[] s = StringUtils.split(c, ":");
            mapa.put(StringUtils.trimToEmpty(s[0]), StringUtils.trimToEmpty(s[1]));
        }
        LOGGER.debug("CADENA FINAL ="+ mapa);

        return mapa;
    }


    public Object sendPostPDF(String cadenaParametros, DatosConsumidor datosConsumidor) throws IOException {

        Watch watch = new Watch(CONSUMO_WS);
        watch.start();

        LOGGER.debug("Iniciando ejecucion HTTP POST request(PDF)");
        Future<ConsumoWeb> consumoWebFuture = null;
        String respuesta = null;
        String urlServer = getUrl()+getNombreMetodo();
        URL url = new URL(urlServer);
        Proxy proxy = getProxy();
        HttpURLConnection con = null;
        int responseCode = -1;
        String urlParameters = ""+cadenaParametros;
        try {

            consumoWebFuture = registrarConsumo(urlParameters);

            if(proxy==null){
                LOGGER.debug("Estableciendo conexion sin Proxy.");
                con = (HttpURLConnection) url.openConnection();
            }else{
                LOGGER.debug("Estableciendo conexion con Proxy.");
                con = (HttpURLConnection) url.openConnection(proxy);
            }
            if(con==null){
                LOGGER.error("NO se puede establecer la conexion a la URL:"+urlServer);
                return null;
            }

            LOGGER.debug("Estableciendo metodo y parametros");
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.addRequestProperty("Content-Type", newContentType());

            if(StringUtils.trimToNull(urlParameters)==null){
                LOGGER.error("No se tiene definido los parametros para la URL:"+urlServer);
                return null;
            }

            LOGGER.debug("Escribiendo los parametros en la URL, Send post request = " + urlParameters);
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            LOGGER.debug("Obteniendo la respuesta de la conexion.");

            ByteArrayOutputStream outputStream = null;
            InputStream inputStream = null;
            try {
                responseCode = con.getResponseCode();

                if(responseCode!=200){
                    LOGGER.error("Ocurrio un error al obtener la respuesta,  HTTP Response Code="+responseCode);
                    respuesta = "HTTP Response Code="+responseCode;
                    throw new IOException(respuesta);
                }else{

                    inputStream = con.getInputStream();
                    outputStream = new ByteArrayOutputStream();

                    int data;
                    while( (data = inputStream.read()) >= 0 ) {
                        outputStream.write(data);
                    }
                    inputStream.close();

                    // Codificamos los bytes en Base64 para guardar como texto.
                    Base64.Encoder encoder = Base64.getEncoder();
                    respuesta = encoder.encodeToString(outputStream.toByteArray());

                    LOGGER.debug("Ejecucion correcta de la conexion, HTTP Response Code=" + responseCode);


                }
                LOGGER.debug("Respuesta de la solicitud =" + respuesta+"; HTTP Response Code="+responseCode);
                return outputStream;

            }catch (IOException e){
                LOGGER.error("ERROR: <IOException> al ejecutar el metodo: sendPostPDF().", e);
                respuesta = e.getMessage();
                throw e;
            }catch (Exception e){
                LOGGER.error("ERROR: <Exception> al ejecutar el metodo: sendPostPDF().", e);
                respuesta = e.getMessage();
                throw (IOException)e;
            }finally {
                IOUtils.closeQuietly(inputStream);
            }

        }finally {
            if (con != null) {
                try {
                    con.disconnect();
                    LOGGER.debug("Cerrando la conexion.");
                } catch (Exception e) {
                    LOGGER.error("Error al cerrar", e);
                }
            }
            final int milis = (int) watch.stop();
            actualizaConsumo(respuesta, responseCode, milis, datosConsumidor, consumoWebFuture);
        }

    }



    // Para un caso de tipo REST es necesario identificar el nombre del método a consumir
    // En otro caso devolver cadena vacia.
    protected abstract String getNombreMetodo();

    protected abstract String getParametros();


    //++++++++++++++++ Metodos para guardar el consmo del servicio

    public Future<ConsumoWeb> registrarConsumo(String solicitud){

        final ConsumoWeb consumoWeb = new ConsumoWeb();
        Future<ConsumoWeb> consumoWebFuture = null;

        consumoWebFuture = getExecutor().submit(new Callable<ConsumoWeb>() {
            @Override
            public ConsumoWeb call() throws Exception {
                consumoWeb.setFecha(new Date());
                consumoWeb.setConsumidor(StringUtils.abbreviate(getConsumidorNombre(), 10));
                consumoWeb.setPregunta(solicitud);
                getConsumoWebDao().persist(consumoWeb);
                LOGGER.debug("Registrando el consumo de WS.");
                return consumoWeb;
            }
        });

        return consumoWebFuture;
    }


    public void actualizaConsumo(String respuesta, int codHttp, Integer milis, DatosConsumidor datosConsumidor, Future<ConsumoWeb> future){

        if (future != null) {
            final Future<ConsumoWeb> finalConsumoWebFuture = future;
            final Integer finalStatusCode = codHttp;
            getExecutor().submit(new Runnable() {
                @Override
                public void run() {
                    try {

                        final ConsumoWeb consumoWeb = finalConsumoWebFuture.get(10, TimeUnit.SECONDS);
                        consumoWeb.setStatus(finalStatusCode);
                        consumoWeb.setMilis(milis);
                        consumoWeb.setRespuesta(respuesta);
                        if(datosConsumidor!=null){
                            consumoWeb.setIdResp("" + datosConsumidor.getStringReferencia());
                            consumoWeb.setIdRelacionado(datosConsumidor.getIdReferencia());
                        }
                        getConsumoWebDao().merge(consumoWeb);
                        LOGGER.debug("Actualizando el consumo de WS.");
                    } catch (InterruptedException e) {
                        LOGGER.error("Error: Actualizacion de consumo interrumpido.", e);
                    } catch (TimeoutException e) {
                        LOGGER.error("Error: La persistencia de consumo web tarda mucho.", e);
                    } catch (Exception e) {
                        LOGGER.error("Error: Ha ocurrido un error inesperado", e);
                    }
                }
            });

        }
    }

}
