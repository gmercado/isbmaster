package com.bisa.bus.servicios.skyb.boa.entities;

import org.apache.commons.lang.StringUtils;

/**
 * Created by rchura on 23-11-15.
 */
public enum TipoProcesoPago {

    CREAR("CRE", "Crear"),
    PAGAR("PAG", "Pagar"),
    RECHAZAR("REC", "Rechazar"),
    AUTORIZAR("AUT", "Para Autorizar"),
    PAGOAUTORIZADO("PGA", "Pago Autorizado");

    private String tipo;
    private String descripcion;

    private TipoProcesoPago(String codigo, String descripcion) {
        this.tipo = codigo;
        this.descripcion = descripcion;
    }

    public String getTipo() {        return tipo;    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return StringUtils.trimToEmpty(getDescripcion());
    }
}
