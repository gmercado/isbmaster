package com.bisa.bus.servicios.skyb.boa;

import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.bus.servicios.skyb.boa.api.ServicioBoa;
import com.bisa.bus.servicios.skyb.boa.api.ServiciosBoa;
import com.bisa.bus.servicios.skyb.boa.sched.ConciliacionDiariaPagosBoA;
import com.bisa.bus.servicios.skyb.boa.sched.ConfirmacionPagosBoA;
import com.bisa.bus.servicios.skyb.boa.sched.VerificacionPagosBoA;

/**
 * @author Marcelo Morales
 * @since 4/30/12
 */
public class BoAModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(ServicioBoa.class).to(ServiciosBoa.class);
        bindSched("EmisionBoletos", "BOA", "0 * * * * ?", ConfirmacionPagosBoA.class);
        bindSched("ConciliacionPagos", "BOA", "0 * * * * ?", ConciliacionDiariaPagosBoA.class);
        bindSched("VerificacionPagos", "BOA", "0 * * * * ?", VerificacionPagosBoA.class);

        bindUI("com/bisa/bus/servicios/skyb/boa/ui");
    }
}
