package com.bisa.bus.servicios.skyb.boa.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rchura on 23-11-15.
 */
public enum ServicioBoa {

    PAGORESERVA_BS(0,"Pago de reserva en Bolivianos."),
    PAGORESERVA_SUS(2, "Pago de reserva en Dolares.");


    private int codigo;
    private String descripcion;

    ServicioBoa(int codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    // Obtiene un objeto EstadosBoa a partir de un valor entero
    public static ServicioBoa valorEnum(int valor){
        List<ServicioBoa> servicio = new ArrayList<ServicioBoa>(Arrays.asList((ServicioBoa.values())));
        for(ServicioBoa serv : servicio){
            if(valor==serv.getCodigo()){
                return serv;
            }
        }
        return null;
    }

}
