package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 06-11-15.
 */
public class Endoso {

    /**
     * Posicion
     * Elemento “posicion” definido en el apartado 10.2.1.2.1
     * Identifica de manera única la información del pasajero en el PNR.
     */
    @JsonProperty(value = "posicion")
    private Posicion posicion;

    /**
     * texto
     * Cadena de caracteres alfanuméricos.
     * Texto informativo (restricciones e información complementaria) que pasa del PNR
     * al campo “Restricciones y Endosos” del billete.
     */
    @JsonProperty(value = "texto")
    private String textoEndoso;


    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }

    public String getTextoEndoso() {
        return textoEndoso;
    }

    public void setTextoEndoso(String textoEndoso) {
        this.textoEndoso = textoEndoso;
    }
}
