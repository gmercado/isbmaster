package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by rchura on 09-12-15.
 */
public class ListaTitular {

    @JsonProperty(value = "string")
    private List<String> titular;


    public List<String> getTitular() {
        return titular;
    }

    public void setTitular(List<String> titular) {
        this.titular = titular;
    }

}


