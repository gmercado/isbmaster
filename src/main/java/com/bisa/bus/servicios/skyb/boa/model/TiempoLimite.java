package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Define el Tiempo Limite de la reserva
 * Created by rchura on 06-11-15.
 */
public class TiempoLimite {

    /**
     * Posicion
     * Elemento “posicion” definido en el apartado 10.2.1.2.1
     * Identifica de manera única la información del pasajero en el PNR.
     */
    @JsonProperty(value = "posicion")
    private Posicion posicion;

    /**
     * fecha_limite
     * DDMMMYY	Fecha límite para la emisión del billete.
     */
    @JsonProperty(value = "fecha_limite")
    private String fechaLimite;

    /**
     * Oficina
     * Cadena de caracteres.
     * Código de la oficina que ha impuesto el tiempo límite para emisión de la reserva.
     */
    @JsonProperty(value = "oficina")
    private String oficina;

    /**
     * num_pax
     * Numérico.	Número de pasajero al que va asociado el tiempo límite.
     * En caso de que este campo no venga informado, la información del tiempo límite se aplicará a todos los pasajeros de la reserva.
     */
    @JsonProperty(value = "num_pax")
    private Long nroPasajero;


    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }

    public String getFechaLimite() {
        return fechaLimite;
    }

    public void setFechaLimite(String fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    public String getOficina() {
        return oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    public Long getNroPasajero() {
        return nroPasajero;
    }

    public void setNroPasajero(Long nroPasajero) {
        this.nroPasajero = nroPasajero;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TiempoLimite{");
        sb.append("posicion=").append(posicion);
        sb.append(", fechaLimite='").append(fechaLimite).append('\'');
        sb.append(", oficina='").append(oficina).append('\'');
        sb.append(", nroPasajero=").append(nroPasajero);
        sb.append('}');
        return sb.toString();
    }
}
