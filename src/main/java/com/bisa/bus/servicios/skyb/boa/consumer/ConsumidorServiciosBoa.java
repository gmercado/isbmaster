package com.bisa.bus.servicios.skyb.boa.consumer;

import com.bisa.bus.servicios.skyb.boa.model.*;
import com.ctc.wstx.util.StringUtil;

import com.google.inject.Inject;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHost;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.*;

/**
 * Created by rchura on 30-10-15.
 */
public class ConsumidorServiciosBoa extends SendHTTPRequest {

    public static final String SINRESPUESTA = "SINRESPUESTA";
    private static final Map<String, String> mapa = new HashMap<>();

    @Inject
    public ConsumidorServiciosBoa(@SQLFueraDeLinea ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente) {
        super(executor, consumoWebDao, medioAmbiente);

        String credencial = medioAmbiente.getValorDe(SKYB_BOA_CREDENTIALS, SKYB_BOA_CREDENTIALS_DEFAULT);
        String lenguaje = medioAmbiente.getValorDe(SKYB_BOA_LENGUAJE, SKYB_BOA_LENGUAJE_DEFAULT);
        String ip = medioAmbiente.getValorDe(SKYB_BOA_IP, SKYB_BOA_IP_DEFAULT);
        mapa.put("credencial", credencial);
        mapa.put("lenguaje", lenguaje);
        mapa.put("ip", ip);
    }

    protected Map<String, String> getEntradaBase() {
        return mapa;
    }


    @Override
    protected String getUrl() {
        return null;
    }

    @Override
    protected Proxy getProxy() {
    return null;
    }


    @Override
    public String getNombreMetodo() {
        return "";
    }

    @Override
    protected String getParametros() {
        return null;
    }

    @Override
    protected String getHeaderWithIdName() {
        return "Id";
    }

    @Override
    protected String newContentType(Map<String, String> m) {
        return "application/json";

    }

    @Override
    protected String newContentType() {
        return "application/json";
    }

    @Override
    protected String getConsumidorNombre() {
        return "skybBoA";
    }

    @Override
    protected HttpHost newProxyHost() {
        return null;
    }


    @Override
    protected String newPath(String template, Map<String, String> m) {
        return null;
    }

    @Override
    protected String newBasicAuthentication() {
        return null;
    }

    @Override
    protected HttpHost newHost() {
        return null;
    }

    @Override
    protected String getSoapAction() {
        return null;
    }


    protected Mensaje getMensaje(String mensajeTexto){
        String texto = StringUtils.trimToNull(mensajeTexto);
        if(texto==null) return null;
        Mensaje mensaje = null;

        String[] split = texto.split(":");
        if(split!=null && split.length==2){
            String tipo = StringUtils.upperCase(split[0]);
            LOGGER.debug("ARMANDO EL TIPO DE MENSAJE EN BASE A --> "+tipo);
            mensaje = new Mensaje();
            if(StringUtils.contains(tipo, "ERROR")){
                mensaje.setTipo(MessageType.ERROR);
                mensaje.setMensaje(mensajeTexto); //split[1]
            }else if(StringUtils.contains(tipo, "INFO")){
                mensaje.setTipo(MessageType.INFO);
                mensaje.setMensaje(mensajeTexto);
            }else if(StringUtils.contains(tipo, "WARN")){
                mensaje.setTipo(MessageType.WARNING);
                mensaje.setMensaje(mensajeTexto);
            }else{
                if(StringUtils.trimToNull(tipo)!=null){
                    mensaje.setTipo(MessageType.ERROR);
                    mensaje.setMensaje(mensajeTexto);
                }
            }
            mensaje.setCodigo(" ");
        }else if(split!=null && split.length==1){
            mensaje = new Mensaje();
            mensaje.setTipo(MessageType.ERROR);
            mensaje.setMensaje(mensajeTexto);
            mensaje.setCodigo(" ");
        }

        return mensaje;
    }



}
