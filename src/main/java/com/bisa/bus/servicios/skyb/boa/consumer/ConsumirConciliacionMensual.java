package com.bisa.bus.servicios.skyb.boa.consumer;

import com.bisa.bus.servicios.skyb.boa.model.*;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.*;

/**
 * Created by rchura on 04-11-15.
 */
public class ConsumirConciliacionMensual extends ConsumidorServiciosBoa {

    @Inject
    public ConsumirConciliacionMensual(@SQLFueraDeLinea ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente) {
        super(executor, consumoWebDao, medioAmbiente);
    }

    @Override
    protected String getUrl() {
        String url =  medioAmbiente.getValorDe(SKYB_BOA_URL_CONCILIACION, SKYB_BOA_URL_CONCILIACION_DEFAULT);
        return url;
    }

    @Override
    public String getNombreMetodo() {
        return medioAmbiente.getValorDe(SKYB_BOA_METODO_CONCILIACION_MENSUAL, SKYB_BOA_METODO_CONCILIACION_MNESUAL_DEFAULT);

    }


    public String getParametros(Integer mes, Integer anio){


        Map<String, String> map = getEntradaBase();
        EntradaConciliacionMensual entradaConcilMensual = new EntradaConciliacionMensual(map.get("credencial"),map.get("ip"),
                Boolean.FALSE, null, map.get("lenguaje"), mes, anio);

        ObjectMapper mapper = new ObjectMapper();
        String solicitud = null;
        try {
            solicitud = mapper.writeValueAsString(entradaConcilMensual);
            LOGGER.debug("SOLICITUD JSON ES <ConsumirConciliacionMensual> ---->: " + solicitud);
        } catch (JsonProcessingException e) {
            LOGGER.error("ERROR: <JsonProcessingException> al ejecutar el metodo: getParametros().", e);
            solicitud = null;
        }

        return solicitud;
    }

    public ResultadoConciliacionMensual getConcilicionMensual(Integer mes, Integer anio){
        ResultadoConciliacionMensual resultadoConciliacion = null;
        Mensaje mensaje = null;
        String respuesta;
        try {
            respuesta = (String) sendPost(getParametros(mes, anio), null);
        } catch (IOException e) {
            LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getConcilicionMensual().", e);
            respuesta = null;
        }


        if(respuesta!=null){
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                resultadoConciliacion = objectMapper.readValue(respuesta, ResultadoConciliacionMensual.class);
                LOGGER.debug("ResultadoConciliacionMensual OBJETO ---> " + resultadoConciliacion.toString());
            }catch (JsonMappingException e) {
                LOGGER.warn("ERROR: <JsonMappingException> al ejecutar el metodo: getToken().", e);
                mensaje = getMensaje(resultadoConciliacion.getRespuesta());
            } catch (JsonParseException e) {
                LOGGER.warn("ERROR: <JsonParseException> al ejecutar el metodo: getToken().", e);
                mensaje = getMensaje(resultadoConciliacion.getRespuesta());
            } catch (IOException e) {
                LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getToken().", e);
                mensaje = getMensaje(resultadoConciliacion.getRespuesta());
            }
        }

        if(resultadoConciliacion!=null &&  resultadoConciliacion.getRespuesta()!=null){
            resultadoConciliacion = getResultadoConciliacionMensual(resultadoConciliacion.getRespuesta());
        }
        LOGGER.debug("OBJETO FINAL ResultadoConciliacionMensual:--->" + resultadoConciliacion.toString());


        return resultadoConciliacion;
    }


    public ResultadoConciliacionMensual getResultadoConciliacionMensual(String cadenaJson){
        ListaReporteMensual listaReporteMensual =null;
        Mensaje mensaje = null;
        List<ReporteMensual> listaReporteSimple = null;
        String jsonMonthlyReconciliation=null;

        ObjectMapper mapper = new ObjectMapper();
        ResultadoConciliacionMensual resultadoConciliacionMensual = new ResultadoConciliacionMensual();

        try {
            JsonNode root = mapper.readTree(cadenaJson);
            JsonNode nodoMonthlyReconciliation = root.at("/ArrayOfResultMonthlyReconciliation");
            jsonMonthlyReconciliation= nodoMonthlyReconciliation.toString();
            LOGGER.debug("jsonMonthlyReconciliation STRING ---> " + jsonMonthlyReconciliation);

            if(StringUtils.trimToNull(jsonMonthlyReconciliation)!=null){
                listaReporteMensual  = mapper.readValue(jsonMonthlyReconciliation, ListaReporteMensual.class);
                LOGGER.debug("ListaReporteMensual OBJETO ---> " + listaReporteMensual .toString());
            }

        } catch (JsonMappingException e) {
            LOGGER.warn("Error JsonMappingException -->" + cadenaJson, e);
            mensaje = getMensaje(cadenaJson);
            listaReporteMensual =null;
        } catch (JsonParseException e) {
            LOGGER.warn("Error JsonParseException -->" + cadenaJson, e);
            mensaje = getMensaje(cadenaJson);
            listaReporteMensual =null;
        } catch (IOException e) {
            LOGGER.error("Error IOException -->" + cadenaJson, e);
            mensaje = getMensaje(cadenaJson);
            listaReporteMensual =null;
        }

        GetReporteMensual reporteMensual = null;
        if(listaReporteMensual ==null){
            try {
                reporteMensual = mapper.readValue(jsonMonthlyReconciliation, GetReporteMensual.class);
                LOGGER.debug("GetReporteMensual OBJETO ---> " + reporteMensual.toString());
                if(reporteMensual!=null){
                    listaReporteSimple = new ArrayList<ReporteMensual>();
                    listaReporteSimple.add(reporteMensual.getReporteMensual());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        if(listaReporteMensual !=null) resultadoConciliacionMensual.setReporteMensual(listaReporteMensual .getReporteMensual());
        if(listaReporteSimple!=null) resultadoConciliacionMensual.setReporteMensual(listaReporteSimple);
        if(mensaje!=null) resultadoConciliacionMensual.setMensaje(mensaje);
        resultadoConciliacionMensual.setRespuesta(cadenaJson);

        return resultadoConciliacionMensual;
    }


}
