package com.bisa.bus.servicios.skyb.boa.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;
import com.bisa.bus.servicios.skyb.boa.dao.PagosBoaDao;
import com.bisa.bus.servicios.skyb.boa.entities.PagosBoa;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.LinkedList;
import java.util.List;

/**
 * @author by rchura on 16-12-15.
 */
@AuthorizeInstantiation({RolesBisa.ISB_ADMIN, RolesBisa.ISB_BOA})
@Menu(value = "Consulta de Pagos BoA", subMenu = SubMenu.SERVICIOS_BOA)
public class ListadoPagosBoa extends Listado<PagosBoa, Long> {

    public ListadoPagosBoa() {
        super();
    }

    @Override
    protected ListadoPanel<PagosBoa, Long> newListadoPanel(String id) {

        return new ListadoPanel<PagosBoa, Long>(id) {
            @Override
            protected List<IColumn<PagosBoa, String>> newColumns(ListadoDataProvider<PagosBoa, Long> dataProvider) {
                List<IColumn<PagosBoa, String>> columns = new LinkedList<>();
                columns.add(new LinkPropertyColumn<PagosBoa>(Model.of("ID"), "id", "id") {
                    @Override
                    protected void onClick(PagosBoa object) {
                        setResponsePage(DetallePagoBoa.class, new PageParameters().add("id", object.getId()));
                    }
                });
                columns.add(new PropertyColumn<>(Model.of("Nro.Cliente"), "nroCliente", "nroCliente"));
                columns.add(new PropertyColumn<>(Model.of("C\u00f3digo PNR"), "nroPNR", "nroPNR"));
                columns.add(new PropertyColumn<>(Model.of("Apellido/Grupo"), "nombre", "nombre"));
                columns.add(new PropertyColumn<PagosBoa, String>(Model.of("Moneda"), "monedaImporteTotal", "monedaDescripcion") {
                    @Override
                    public void populateItem(Item<ICellPopulator<PagosBoa>> item, String componentId, IModel<PagosBoa> rowModel) {
                        PagosBoa pago = rowModel.getObject();
                        item.add(new Label(componentId, pago.getMonedaDescripcion()).add(new AttributeAppender("style", Model.of("text-align:right"))));
                    }
                });
                columns.add(new PropertyColumn<PagosBoa, String>(Model.of("Importe"), "importeTotal", "importeFormato") {
                    @Override
                    public void populateItem(Item<ICellPopulator<PagosBoa>> item, String componentId, IModel<PagosBoa> rowModel) {
                        PagosBoa pago = rowModel.getObject();
                        item.add(new Label(componentId, pago.getImporteFormato()).add(new AttributeAppender("style", Model.of("text-align:right"))));
                    }
                });
                columns.add(new PropertyColumn<PagosBoa, String>(Model.of("Estado"), "estado", "estadoDescripcion") {
                    @Override
                    public void populateItem(Item<ICellPopulator<PagosBoa>> item, String componentId, IModel<PagosBoa> rowModel) {
                        PagosBoa pago = rowModel.getObject();
                        item.add(new Label(componentId, pago.getEstadoDescripcion()).add(new AttributeAppender("style", Model.of("text-align:center"))));
                    }
                });

                columns.add(new PropertyColumn<>(Model.of("Fecha Pago"), "fechaProceso", "fechaProcesoFormato"));
                columns.add(new PropertyColumn<>(Model.of("T.Vuelos"), "cantVuelos", "cantVuelos"));
                columns.add(new PropertyColumn<>(Model.of("T.Pasajeros"), "nroPasajeros", "nroPasajeros"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<PagosBoa, Long>> getProviderClazz() {
                return PagosBoaDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<PagosBoa> newFiltroPanel(String id, IModel<PagosBoa> model1, IModel<PagosBoa> model2) {
                return new ListadoPagosBoaFiltroPanel(id, model1, model2);

            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("fechaCreacion", false);
            }

            @Override
            protected PagosBoa newObject2() {
                return new PagosBoa();
            }

            @Override
            protected PagosBoa newObject1() {
                return new PagosBoa();
            }

            @Override
            protected boolean isVisibleData() {
                PagosBoa bitacora = getModel1().getObject();
                PagosBoa bitacora2 = getModel2().getObject();
                return !((bitacora.getFechaCreacion() == null || bitacora2.getFechaCreacion() == null));
            }
        };
    }

}
