package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Created by rchura on 06-11-15.
 */
@JsonRootName(value = "int")
public class INT {

    /**
     * Posicion
     * Elemento “posicion” definido en el apartado 10.2.1.2.1
     * Identifica de manera única la información del pasajero en el PNR.
     */
    @JsonProperty(value = "posicion")
    private Posicion posicion;

    /**
     * Texto
     * Cadena de caracteres alfanuméricos.	Texto que contiene la información relacionada con el INT.
     */
    @JsonProperty(value = "texto")
    private String texto;

    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
