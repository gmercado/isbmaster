package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by rchura on 09-11-15.
 */
@XmlRootElement
public class EntradaConciliacionDiaria extends Entrada{

    /**
     * language		X	AA {A->Z sin Ñ).
     * Idioma en el que se devuelven los mensajes al consumidor. Los valores aceptados son los siguientes:
     * ES: Español,      EN: Inglés
     * Si se envía un valor diferente de los anteriores, el sistema utilizará el idioma por defecto del usuario.
     */
    @XmlElement(name = "language")
    @JsonProperty(value = "language")
    private String lenguaje;

    //date (Cadena de texto en formato YYYYMMDD. P.Ej. 20151231)
    @XmlElement(name = "date")
    @JsonProperty(value = "date")
    private String date;

    public EntradaConciliacionDiaria() {
    }

    public EntradaConciliacionDiaria(String credenciales, String ip, Boolean xmlOrJson, Localizador localizador, String lenguaje, String date) {
        super(credenciales, ip, xmlOrJson, localizador);
        this.lenguaje = lenguaje;
        this.date = date;
    }

    public String getLenguaje() {
        return lenguaje;
    }

    public void setLenguaje(String lenguaje) {
        this.lenguaje = lenguaje;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("EntradaConciliacionDiaria{");
        sb.append("lenguaje='").append(lenguaje).append('\'');
        sb.append(", date='").append(date).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
