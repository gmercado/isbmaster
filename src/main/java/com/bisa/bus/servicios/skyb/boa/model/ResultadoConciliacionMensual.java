package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;

/**
 * Created by rchura on 10-11-15.
 */
@JsonRootName(value = "MonthlyReconciliationResult")
public class ResultadoConciliacionMensual {


    @JsonProperty(value = "MonthlyReconciliationResult")
    private String respuesta;

    @JsonIgnore
    private Mensaje mensaje;

    @JsonIgnore
    private List<ReporteMensual> reporteMensual;

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    public List<ReporteMensual> getReporteMensual() {
        return reporteMensual;
    }

    public void setReporteMensual(List<ReporteMensual> reporteMensual) {
        this.reporteMensual = reporteMensual;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ResultadoConciliacionMensual{");
        sb.append("respuesta='").append(respuesta).append('\'');
        sb.append(", mensaje=").append(mensaje);
        sb.append(", reporteMensual=").append(reporteMensual);
        sb.append('}');
        return sb.toString();
    }
}
