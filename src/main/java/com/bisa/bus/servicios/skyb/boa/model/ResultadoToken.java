package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Created by rchura on 10-11-15.
 */
@JsonRootName(value = "GetTokenResult")
public class ResultadoToken {

    /**
     * Token
     * Cadena de caracteres alfanuméricos. De hasta 250 caracteres.
     * Identificador único para las transacciones de emisión de boletos.
     */
    @JsonProperty(value = "GetTokenResult")
    private String token;


    @JsonIgnoreProperties(value = "Token", ignoreUnknown = true)
    private String tokenResult;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenResult() {
        return tokenResult;
    }

    public void setTokenResult(String tokenResult) {
        this.tokenResult = tokenResult;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ResultadoToken{");
        sb.append("token='").append(token).append('\'');
        sb.append(", tokenResult='").append(tokenResult).append('\'');
        sb.append('}');
        return sb.toString();
    }


    //
//    @Override
//    public String toString() {
//        final StringBuffer sb = new StringBuffer("ResultadoToken{");
//        sb.append("token='").append(token).append('\'');
//        sb.append('}');
//        return sb.toString();
//    }
}
