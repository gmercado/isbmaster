package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 06-11-15.
 */
public class TourCode {

    /**
     * Posicion
     * Elemento “posicion” definido en el apartado 10.2.1.2.1
     * Identifica de manera única la información del pasajero en el PNR.
     */
    @JsonProperty(value = "posicion")
    private Posicion posicion;


    /**
     * Texto
     * Cadena de caracteres.	Texto informativo del tour code asociado a la reserva.
     *
     */
    @JsonProperty(value = "texto")
    private String textoTourCode;


    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }

    public String getTextoTourCode() {
        return textoTourCode;
    }

    public void setTextoTourCode(String textoTourCode) {
        this.textoTourCode = textoTourCode;
    }
}
