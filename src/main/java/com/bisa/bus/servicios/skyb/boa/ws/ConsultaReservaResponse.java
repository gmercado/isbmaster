package com.bisa.bus.servicios.skyb.boa.ws;

import com.bisa.bus.servicios.skyb.boa.model.Mensaje;
import com.bisa.bus.servicios.skyb.boa.model.Pasajero;
import com.bisa.bus.servicios.skyb.boa.model.Vuelo;

import java.util.List;

/**
 * Created by rchura on 11-11-15.
 */

public class ConsultaReservaResponse {


    protected String codigoPnr;


    private List<Vuelo> listaVuelos;

    private List<Pasajero> listaPasajeros;


    private String fechaReserva;


    private String fechaLimite;

    private Mensaje mensaje;

    public String getCodigoPnr() {
        return codigoPnr;
    }

    public void setCodigoPnr(String codigoPnr) {
        this.codigoPnr = codigoPnr;
    }

    public List<Vuelo> getListaVuelos() {
        return listaVuelos;
    }

    public void setListaVuelos(List<Vuelo> listaVuelos) {
        this.listaVuelos = listaVuelos;
    }

    public List<Pasajero> getListaPasajeros() {
        return listaPasajeros;
    }

    public void setListaPasajeros(List<Pasajero> listaPasajeros) {
        this.listaPasajeros = listaPasajeros;
    }

    public String getFechaReserva() {
        return fechaReserva;
    }

    public void setFechaReserva(String fechaReserva) {
        this.fechaReserva = fechaReserva;
    }

    public String getFechaLimite() {
        return fechaLimite;
    }

    public void setFechaLimite(String fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ConsultaReservaResponse{");
        sb.append("codigoPnr='").append(codigoPnr).append('\'');
        sb.append(", listaVuelos=").append(listaVuelos);
        sb.append(", listaPasajeros=").append(listaPasajeros);
        sb.append(", fechaReserva='").append(fechaReserva).append('\'');
        sb.append(", fechaLimite='").append(fechaLimite).append('\'');
        sb.append(", mensaje=").append(mensaje);
        sb.append('}');
        return sb.toString();
    }
}
