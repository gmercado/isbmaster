package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 06-11-15.
 */
public class Canje {

    /**
     * posicion
     * Elemento “posicion” definido en el apartado 10.2.1.2.1
     * Identifica de manera única la información del pasajero en el PNR.
     */
    @JsonProperty(value = "posicion")
    private Posicion posicion;

    /**
     * inf
     * Carácter: “Y”, “N”
     * Indica si el pasajero se trata de un INF (“Y”), o no (“N”).
     */
    @JsonProperty(value = "inf")
    private String infante;

    /**
     * texto
     * Cadena de caracteres.
     * Texto que representa el canje entre los distintos elementos TKT (billete, emco)
     */
    @JsonProperty(value = "texto")
    private String textoCanje;

    /**
     * num_pax
     * Numérico
     * Número de pasajero al que van asociados los elementos oi.
     */
    @JsonProperty(value = "num_pax")
    private Long nroPasajero;


    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }

    public String getInfante() {
        return infante;
    }

    public void setInfante(String infante) {
        this.infante = infante;
    }

    public String getTextoCanje() {
        return textoCanje;
    }

    public void setTextoCanje(String textoCanje) {
        this.textoCanje = textoCanje;
    }

    public Long getNroPasajero() {
        return nroPasajero;
    }

    public void setNroPasajero(Long nroPasajero) {
        this.nroPasajero = nroPasajero;
    }
}
