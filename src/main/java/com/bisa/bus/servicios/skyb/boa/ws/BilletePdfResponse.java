package com.bisa.bus.servicios.skyb.boa.ws;

import com.bisa.bus.servicios.skyb.boa.model.Mensaje;

import java.io.DataInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.stream.Stream;

/**
 * Created by rchura on 20-11-15.
 */
public class BilletePdfResponse {

    private Long pagoID;

    private String pdfArray;

    private Mensaje mensaje;


    public Long getPagoID() {
        return pagoID;
    }

    public void setPagoID(Long pagoID) {
        this.pagoID = pagoID;
    }

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    public String getPdfArray() {
        return pdfArray;
    }

    public void setPdfArray(String pdfArray) {
        this.pdfArray = pdfArray;
    }

}
