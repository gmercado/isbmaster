package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Created by rchura on 30-12-15.
 */
public class ReporteMensual {

    /**
     * Entity, IP, RequestDate, Currency, TotalTransactions, TotalAmount
     * Entidad, IP, FechaSolicitud, Moneda, NumeroTransacciones, ImporteTotal
     *
     Entidad Entidad Financiera que realizo el cobro de los tickets aéreos
     IP Dirección IP desde la que se realizó el cobro de los tickets aéreos
     FechaSolicitud Fecha en la que se recibió la autorización para la emisión de los tickets
     Moneda Moneda en la que debió cobrarse la emisión de los tickets
     NumeroTransacciones Número de transacciones autorizadas para emisión, agregado por día y moneda
     ImporteTotal Suma del Importe Total autorizado para emisión, agregado por día y moneda
     */

    @JsonProperty(value = "Entity")
    private String banco;

    @JsonProperty(value = "IP")
    private String ip;

    @JsonProperty(value = "RequestDate")
    private String fechaAutorizacion;

    @JsonProperty(value = "Currency")
    private String moneda;

    @JsonProperty(value = "TotalAmount")
    private BigDecimal importeTotal;

    @JsonProperty(value = "TotalTransactions")
    private BigDecimal totalTransacciones;

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getFechaAutorizacion() {
        return fechaAutorizacion;
    }

    public void setFechaAutorizacion(String fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(BigDecimal importeTotal) {
        this.importeTotal = importeTotal;
    }

    public BigDecimal getTotalTransacciones() {
        return totalTransacciones;
    }

    public void setTotalTransacciones(BigDecimal totalTransacciones) {
        this.totalTransacciones = totalTransacciones;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ReporteMensual{");
        sb.append("banco='").append(banco).append('\'');
        sb.append(", ip='").append(ip).append('\'');
        sb.append(", fechaAutorizacion='").append(fechaAutorizacion).append('\'');
        sb.append(", moneda='").append(moneda).append('\'');
        sb.append(", importeTotal=").append(importeTotal);
        sb.append(", totalTransacciones=").append(totalTransacciones);
        sb.append('}');
        return sb.toString();
    }
}
