package com.bisa.bus.servicios.skyb.boa.ws;

import com.bisa.bus.servicios.skyb.boa.model.Autorizacion;
import com.bisa.bus.servicios.skyb.boa.model.Mensaje;

/**
 * Created by rchura on 20-11-15.
 */
public class PagoReservaResponse {

    private Autorizacion autorizacion;

    private Long pagoID;

    private Mensaje mensaje;


    public PagoReservaResponse() {
    }

    public PagoReservaResponse(Autorizacion autorizacion, Long pagoID, Mensaje mensaje) {
        this.autorizacion = autorizacion;
        this.pagoID = pagoID;
        this.mensaje = mensaje;
    }

    public Autorizacion getAutorizacion() {
        return autorizacion;
    }

    public void setAutorizacion(Autorizacion autorizacion) {
        this.autorizacion = autorizacion;
    }

    public Long getPagoID() {
        return pagoID;
    }

    public void setPagoID(Long pagoID) {
        this.pagoID = pagoID;
    }

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PagoReservaResponse{");
        sb.append("autorizacion=").append(autorizacion);
        sb.append(", pagoID=").append(pagoID);
        sb.append(", mensaje=").append(mensaje);
        sb.append('}');
        return sb.toString();
    }
}
