package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Created by rchura on 10-11-15.
 */
@JsonRootName(value = "GetTicketPNRResult")
public class ResultadoTicketPNR {

    @JsonProperty(value = "GetTicketPNRResult")
    private String respuesta;

    /**
     * En caso de que se produzca algún error, o que no exista información sobre la reserva emitida,
     * se devolverá el elemento “mensaje” en lugar del elemento “ResultGetTicketPNR” descrito anteriormente.
     * Se devolverá una cadena con la descripción del mensaje.
     */
    @JsonIgnore
    private Mensaje mensaje;

    @JsonIgnore
    private TicketPNR ticketPNR;


    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    public TicketPNR getTicketPNR() {
        return ticketPNR;
    }

    public void setTicketPNR(TicketPNR ticketPNR) {
        this.ticketPNR = ticketPNR;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ResultadoTicketPNR{");
        sb.append("respuesta='").append(respuesta).append('\'');
        sb.append(", mensaje=").append(mensaje);
        sb.append(", ticketPNR=").append(ticketPNR);
        sb.append('}');
        return sb.toString();
    }
}
