package com.bisa.bus.servicios.skyb.boa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by rchura on 31-12-15.
 */
public class TotalPagosBoa implements Serializable{

    private Date fecha;

    private int moneda;

    private BigDecimal total;

    private BigDecimal totalMonitor;

    private long cantidad;

    private String nota;

    // BISA, BOA, DIFERENCIA
    private String origen;

    public TotalPagosBoa() {
    }

    public TotalPagosBoa(Date fecha, int moneda, BigDecimal total, long cantidad, String origen) {
        this.fecha = fecha;
        this.moneda = moneda;
        this.total = total;
        this.cantidad = cantidad;
        this.origen = origen;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getMoneda() {
        return moneda;
    }

    public void setMoneda(int moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getTotalMonitor() {
        return totalMonitor;
    }

    public void setTotalMonitor(BigDecimal totalMonitor) {
        this.totalMonitor = totalMonitor;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TotalPagosBoa{");
        sb.append("fecha=").append(fecha);
        sb.append(", moneda=").append(moneda);
        sb.append(", total=").append(total);
        sb.append(", totalMonitor=").append(totalMonitor);
        sb.append(", cantidad=").append(cantidad);
        sb.append(", nota='").append(nota).append('\'');
        sb.append(", origen='").append(origen).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

