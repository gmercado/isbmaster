package com.bisa.bus.servicios.skyb.boa.ws;

import com.bisa.bus.servicios.skyb.boa.model.Pasajero;
import com.bisa.bus.servicios.skyb.boa.model.Vuelo;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by rchura on 20-11-15.
 */
public class PagoReservaRequest extends RequestBoa {

    private BigDecimal importeTotal;

    private int importeMoneda;

    private BigDecimal cuentaDebito;

    private int cuentaMoneda;

    private String cuentaTipo;

    private String nota;

    private BigDecimal nroOperacion;

    private String tipoDoc; // CI, NIT

    private String numeroDoc; // Numero identificacion

    private String razonSocial; // Razon Social

    private String proceso;

    // Datos importantes de la Reserva

    private List<Vuelo> listaVuelos;

    private List<Pasajero> listaPasajeros;

    private String fechaReserva;

    private String fechaLimite;

    private Long pagoID;

    private String email;


    public PagoReservaRequest(){}

    public PagoReservaRequest(String cliente, String usuario, String codigoPnr, String apellidoGrupo, String ipOrigen, String sistema) {
        super(cliente, usuario, codigoPnr, apellidoGrupo, ipOrigen, sistema);
    }

    public BigDecimal getCuentaDebito() {
        return cuentaDebito;
    }

    public void setCuentaDebito(BigDecimal cuentaDebito) {
        this.cuentaDebito = cuentaDebito;
    }

    public int getCuentaMoneda() {
        return cuentaMoneda;
    }

    public void setCuentaMoneda(int cuentaMoneda) {
        this.cuentaMoneda = cuentaMoneda;
    }

    public String getCuentaTipo() {
        return cuentaTipo;
    }

    public void setCuentaTipo(String cuentaTipo) {
        this.cuentaTipo = cuentaTipo;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public BigDecimal getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(BigDecimal importeTotal) {
        this.importeTotal = importeTotal;
    }

    public int getImporteMoneda() {
        return importeMoneda;
    }

    public void setImporteMoneda(int importeMoneda) {
        this.importeMoneda = importeMoneda;
    }

    public BigDecimal getNroOperacion() {
        return nroOperacion;
    }

    public void setNroOperacion(BigDecimal nroOperacion) {
        this.nroOperacion = nroOperacion;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getNumeroDoc() {
        return numeroDoc;
    }

    public void setNumeroDoc(String numeroDoc) {
        this.numeroDoc = numeroDoc;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public List<Vuelo> getListaVuelos() {
        return listaVuelos;
    }

    public void setListaVuelos(List<Vuelo> listaVuelos) {
        this.listaVuelos = listaVuelos;
    }

    public List<Pasajero> getListaPasajeros() {
        return listaPasajeros;
    }

    public void setListaPasajeros(List<Pasajero> listaPasajeros) {
        this.listaPasajeros = listaPasajeros;
    }

    public String getFechaLimite() {
        return fechaLimite;
    }

    public void setFechaLimite(String fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    public String getFechaReserva() {
        return fechaReserva;
    }

    public void setFechaReserva(String fechaReserva) {
        this.fechaReserva = fechaReserva;
    }

    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    public Long getPagoID() {
        return pagoID;
    }

    public void setPagoID(Long pagoID) {
        this.pagoID = pagoID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PagoReservaRequest{");
        sb.append("importeTotal=").append(importeTotal);
        sb.append(", importeMoneda=").append(importeMoneda);
        sb.append(", cuentaDebito=").append(cuentaDebito);
        sb.append(", cuentaMoneda=").append(cuentaMoneda);
        sb.append(", cuentaTipo='").append(cuentaTipo).append('\'');
        sb.append(", nota='").append(nota).append('\'');
        sb.append(", nroOperacion=").append(nroOperacion);
        sb.append(", tipoDoc='").append(tipoDoc).append('\'');
        sb.append(", numeroDoc='").append(numeroDoc).append('\'');
        sb.append(", proceso='").append(proceso).append('\'');
        sb.append(", listaVuelos=").append(listaVuelos);
        sb.append(", listaPasajeros=").append(listaPasajeros);
        sb.append(", fechaReserva='").append(fechaReserva).append('\'');
        sb.append(", fechaLimite='").append(fechaLimite).append('\'');
        sb.append(", pagoID=").append(pagoID);
        sb.append(", email='").append(email).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
