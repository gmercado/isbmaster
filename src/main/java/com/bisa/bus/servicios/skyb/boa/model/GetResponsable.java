package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 16-11-15.
 */
public class GetResponsable {

    @JsonProperty(value = "responsable")
    private Responsable responsable;

    public Responsable getResponsable() {
        return responsable;
    }

    public void setResponsable(Responsable responsable) {
        this.responsable = responsable;
    }
}
