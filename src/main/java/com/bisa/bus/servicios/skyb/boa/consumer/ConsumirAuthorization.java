package com.bisa.bus.servicios.skyb.boa.consumer;

import com.bisa.bus.servicios.skyb.boa.model.*;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;
import com.google.inject.Inject;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import com.lowagie.text.pdf.PdfShading;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.*;

/**
 * Created by rchura on 09-11-15.
 */
public class ConsumirAuthorization  extends ConsumidorServiciosBoa{

    @Inject
    public ConsumirAuthorization(@SQLFueraDeLinea ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente) {
        super(executor, consumoWebDao, medioAmbiente);
    }

    @Override
    protected String getUrl() {
        return medioAmbiente.getValorDe(SKYB_BOA_URL_RESERVA, SKYB_BOA_URL_RESERVA_DEFAULT);
    }

    @Override
    public String getNombreMetodo() {
        return medioAmbiente.getValorDe(SKYB_BOA_METODO_AUTHORIZATION, SKYB_BOA_METODO_AUTHORIZATION_DEFAULT);
    }

    protected String getParametros(Localizador localizador, String token, String codigoAutorizacion, String endosoFactura) {

        if(localizador==null){
            LOGGER.error("NO se puede obtener los datos del \'Localizador (pnr, identificador)\'");
            return null;
        }

        Map<String, String> map = getEntradaBase();
        EntradaAuthorization authorization = new EntradaAuthorization(map.get("credencial"),map.get("ip"), Boolean.FALSE,
                localizador, token, map.get("lenguaje"), codigoAutorizacion, endosoFactura);

        ObjectMapper mapper = new ObjectMapper();
        String solicitud = null;
        try {
            solicitud = mapper.writeValueAsString(authorization);
            LOGGER.debug("SOLICITUD JSON ES <ConsumirAuthorization> ---->: " + solicitud);
        } catch (JsonProcessingException e) {
            LOGGER.error("ERROR: <JsonProcessingException> al ejecutar el metodo: getParametros().", e);
            solicitud = null;
        }
        return solicitud;
    }

    public ResultadoAuthorization setAuthorization(Localizador localizador, String token, String codigoAutorizacion, String endosoFactura){

        ResultadoAuthorization autorizacion = null;
        String respuesta = null;

        try {
            respuesta = (String) sendPost(getParametros(localizador, token, codigoAutorizacion, endosoFactura), localizador.getDatosConsumidor());

        } catch (IOException e) {
            LOGGER.error("Error <IOException> : Ocurrio un error al obtener la respuesta del metodo <setAuthorization>. "+e.getMessage(), e);
            //LOGGER.error("ERROR: <IOException> al ejecutar el metodo: setAuthorization().", e);
            autorizacion = new ResultadoAuthorization();
            Autorizacion autoriza = new Autorizacion();
            autoriza.setEstado(ConsumidorServiciosBoa.SINRESPUESTA);
            autoriza.setMensaje(e.getMessage());
            autorizacion.setAutorizacion(autoriza);
            return autorizacion;
        }

        if(respuesta!=null){
            try {
                autorizacion = getAuthorization(respuesta);
            } catch (Exception e) {
                LOGGER.error("Error <BoAConsumerException> : Ocurrio un error al obtener la respuesta del metodo <setAuthorization>."+e.getMessage(), e);
                autorizacion = new ResultadoAuthorization();
                Autorizacion autoriza = new Autorizacion();
                autoriza.setEstado(ConsumidorServiciosBoa.SINRESPUESTA);
                autoriza.setMensaje(e.getMessage());
                autorizacion.setAutorizacion(autoriza);
                return autorizacion;
            }
        }

        return autorizacion;

    }


    private ResultadoAuthorization getAuthorization(String cadenaJson){

        ObjectMapper objectMapper = new ObjectMapper();
        ResultadoAuthorization resultadoAuthorization = null;
        LOGGER.debug("JSON--->"+cadenaJson);

        try {
            resultadoAuthorization = objectMapper.readValue(cadenaJson, ResultadoAuthorization.class);
            LOGGER.debug("ResultadoAuthorization OBJETO ---> " + resultadoAuthorization.getRespuesta());
        } catch (JsonMappingException e) {
            LOGGER.warn("ERROR: <JsonProcessingException> al ejecutar el metodo: getAuthorization().", e);
            resultadoAuthorization = null;
        } catch (JsonParseException e) {
            LOGGER.warn("ERROR: <JsonParseException> al ejecutar el metodo: getAuthorization().", e);
            resultadoAuthorization = null;
        } catch (IOException e) {
            LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getAuthorization().", e);
            resultadoAuthorization = null;
        }

        if(resultadoAuthorization!=null){
            GetAutorizacion getAutorizacion = null;
            try {
                getAutorizacion = objectMapper.readValue(resultadoAuthorization.getRespuesta(), GetAutorizacion.class);
                LOGGER.debug("Autorizacion OBJETO ---> " + getAutorizacion.getAutorizacion().toString());
            }catch (JsonMappingException e) {
                LOGGER.warn("ERROR: <JsonMappingException> al ejecutar el metodo: getAuthorization().", e);
                getAutorizacion = null;
            } catch (JsonParseException e) {
                LOGGER.warn("ERROR: <JsonParseException> al ejecutar el metodo: getAuthorization().", e);
                getAutorizacion = null;
            } catch (IOException e) {
                LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getAuthorization().", e);
                getAutorizacion = null;
            }
            if(getAutorizacion!=null)
                resultadoAuthorization.setAutorizacion(getAutorizacion.getAutorizacion());

            LOGGER.debug("OBJETO ResultadoAuthorization:--->"+resultadoAuthorization.toString());
        }

        return resultadoAuthorization;
    }


}
