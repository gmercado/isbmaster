package com.bisa.bus.servicios.skyb.boa.consumer;

/**
 * Created by rchura on 02-02-16.
 */
public class DatosConsumidor {

    private String stringReferencia;

    private Long idReferencia;


    public DatosConsumidor(String stringReferencia, Long idReferencia) {
        this.stringReferencia = stringReferencia;
        this.idReferencia = idReferencia;
    }

    public String getStringReferencia() {
        return stringReferencia;
    }

    public void setStringReferencia(String stringReferencia) {
        this.stringReferencia = stringReferencia;
    }

    public Long getIdReferencia() {
        return idReferencia;
    }

    public void setIdReferencia(Long idReferencia) {
        this.idReferencia = idReferencia;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DatosConsumidor{");
        sb.append("stringReferencia='").append(stringReferencia).append('\'');
        sb.append(", idReferencia=").append(idReferencia);
        sb.append('}');
        return sb.toString();
    }
}
