package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by rchura on 09-11-15.
 */

public abstract class Entrada {

    /**
     * credentials
     * 8C-4C-4C-4C-12C}{8C-4C-4C-4C-12C} (total 76 caracteres) donde c es un carácter alfanumérico {A->Z, excepto N y 0->9).
     * Identificación = Usuario + Password
     */
    @XmlElement(name = "credentials")
    @JsonProperty(value = "credentials")
    private String credenciales;


    /**
     * ipAddress
     * NNN.NNN.NNN.NNN donde N (entre 1 y 9).
     * IP válida para las credenciales habilitadas.
     */
    @XmlElement(name = "ipAddress")
    @JsonProperty(value = "ipAddress")
    private String ip;

    /**
     * xmlOrJson
     * Si el valor es true, el método devuelve la cadena resultado en formato XML.
     * Si el valor es false el método devuelve cadena resultado en formato JSON.
     */
    @XmlElement(name = "xmlOrJson")
    @JsonProperty(value = "xmlOrJson")
    private Boolean xmlOrJson;

    /**
     * locator
     * Elemento “locatorShort” definido en el apartado 10.1.1
     * Datos necesarios para poder recuperar la información del PNR y validarlo para el usuario introducido
     */
    //@XmlElement(name = "locator")
    @XmlElement(name = "locator")
    @JsonProperty(value = "locator")
    private Localizador localizador;

    protected Entrada() {
    }

    protected Entrada(String credenciales, String ip, Boolean xmlOrJson, Localizador localizador) {
        this.credenciales = credenciales;
        this.ip = ip;
        this.xmlOrJson = xmlOrJson;
        this.localizador = localizador;
    }

    public String getCredenciales() {
        return credenciales;
    }

    public void setCredenciales(String credenciales) {
        this.credenciales = credenciales;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Boolean getXmlOrJson() {
        return xmlOrJson;
    }

    public void setXmlOrJson(Boolean xmlOrJson) {
        this.xmlOrJson = xmlOrJson;
    }

    public Localizador getLocalizador() {
        return localizador;
    }

    public void setLocalizador(Localizador localizador) {
        this.localizador = localizador;
    }

}
