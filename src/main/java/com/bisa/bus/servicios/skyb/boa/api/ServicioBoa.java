package com.bisa.bus.servicios.skyb.boa.api;

import com.bisa.bus.servicios.skyb.boa.ws.ConsultaReservaRequest;
import com.bisa.bus.servicios.skyb.boa.ws.ConsultaReservaResponse;

/**
 * @author rsalvatierra on 27/09/2016.
 */
public interface ServicioBoa {

    ConsultaReservaResponse consultarReserva(ConsultaReservaRequest request);
}
