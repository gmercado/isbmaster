package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 05-11-15.
 */
public class Update {

    /**
     * num_agente
     * Numérico.
     * Número del agente que ha realizado la última actualización.
     */
    @JsonProperty(value = "num_agente")
    private Long numAgente;

    /**
     * ciudad_upd
     * AAA (A->Z, excepto Ñ).
     * Ciudad correspondiente a la oficina que ha realizado la última actualización.
     */
    @JsonProperty(value = "ciudad_upd")
    private String ciudadUpd;

    /**
     * num_off_upd
     * Cadena de caracteres alfanuméricos.
     * Número de oficina que ha realizado la última actualización.
     */
    @JsonProperty(value = "num_off_upd")
    private String numOffUpd;

    /**
     *  fecha_upd
     *  DDMMMYY
     *  Fecha en la que se produjo la última actualización.
     */
    @JsonProperty(value = "fecha_upd")
    private String fechaUpd;

    /**
     * hora_upd
     * HHMM
     * Hora a la que se produjo la actualización.
     */
    @JsonProperty(value = "hora_upd")
    private String horaUpd;


    public Long getNumAgente() {
        return numAgente;
    }

    public void setNumAgente(Long numAgente) {
        this.numAgente = numAgente;
    }

    public String getCiudadUpd() {
        return ciudadUpd;
    }

    public void setCiudadUpd(String ciudadUpd) {
        this.ciudadUpd = ciudadUpd;
    }

    public String getNumOffUpd() {
        return numOffUpd;
    }

    public void setNumOffUpd(String numOffUpd) {
        this.numOffUpd = numOffUpd;
    }

    public String getFechaUpd() {
        return fechaUpd;
    }

    public void setFechaUpd(String fechaUpd) {
        this.fechaUpd = fechaUpd;
    }

    public String getHoraUpd() {
        return horaUpd;
    }

    public void setHoraUpd(String horaUpd) {
        this.horaUpd = horaUpd;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Update{");
        sb.append("numAgente=").append(numAgente);
        sb.append(", ciudadUpd='").append(ciudadUpd).append('\'');
        sb.append(", numOffUpd='").append(numOffUpd).append('\'');
        sb.append(", fechaUpd='").append(fechaUpd).append('\'');
        sb.append(", horaUpd='").append(horaUpd).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
