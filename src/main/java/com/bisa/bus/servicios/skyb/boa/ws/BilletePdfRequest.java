package com.bisa.bus.servicios.skyb.boa.ws;

/**
 * Created by rchura on 20-11-15.
 */
public class BilletePdfRequest extends RequestBoa{

    private Long pagoID;

    public BilletePdfRequest(){}

    public BilletePdfRequest(String cliente, String usuario, String codigoPnr, String apellidoGrupo, String ipOrigen, String sistema) {
        super(cliente, usuario, codigoPnr, apellidoGrupo, ipOrigen, sistema);
    }

    public Long getPagoID() {
        return pagoID;
    }

    public void setPagoID(Long pagoID) {
        this.pagoID = pagoID;
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
