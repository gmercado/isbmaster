package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by rchura on 09-11-15.
 */
@XmlRootElement
public class EntradaInvoiceEmail extends Entrada{

    /**
     * language		X	AA {A->Z sin Ñ).
     * Idioma en el que se devuelven los mensajes al consumidor. Los valores aceptados son los siguientes:
     * ES: Español,      EN: Inglés
     * Si se envía un valor diferente de los anteriores, el sistema utilizará el idioma por defecto del usuario.
     */
    @XmlElement(name = "language")
    @JsonProperty(value = "language")
    private String lenguaje;

    /**
     * email
     * Cadena de, como máximo, 124 caracteres alfanuméricos (A->Z, excepto Ñ y 0->9),
     * y los caracteres especiales “.”, “/”, “+”, “-“, “_”.
     * El conjunto “_pnúmero” (ej.: prueba_p1@....) no será admitido.
     * E-mail de envio.
     */
    @JsonProperty(value = "email")
    private String mail;

    public EntradaInvoiceEmail(String credenciales, String ip, Boolean xmlOrJson, Localizador localizador,
                               String lenguaje, String mail) {
        super(credenciales, ip, xmlOrJson, localizador);
        this.lenguaje = lenguaje;
        this.mail = mail;
    }

    public String getLenguaje() {
        return lenguaje;
    }

    public void setLenguaje(String lenguaje) {
        this.lenguaje = lenguaje;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
