package com.bisa.bus.servicios.skyb.boa.consumer;

import com.bisa.bus.servicios.skyb.boa.model.EntradaInvoicePdf;
import com.bisa.bus.servicios.skyb.boa.model.Localizador;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.*;

/**
 * Created by rchura on 10-11-15.
 */
public class ConsumirInvoicePdf extends ConsumidorServiciosBoa{


    @Inject
    public ConsumirInvoicePdf(@SQLFueraDeLinea ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente) {
        super(executor, consumoWebDao, medioAmbiente);
    }


    @Override
    protected String getUrl() {
        return medioAmbiente.getValorDe(SKYB_BOA_URL_RESERVA, SKYB_BOA_URL_RESERVA_DEFAULT);
    }

    @Override
    public String getNombreMetodo() {
        return medioAmbiente.getValorDe(SKYB_BOA_METODO_INVOICEPDF, SKYB_BOA_METODO_INVOICEPDF_DEFAULT);
    }

    protected String getParametros(Localizador localizador) {

        if(localizador==null){
            LOGGER.error("NO se puede obtener los datos del \'Localizador (pnr, identificador)\'");
            return null;
        }

        Map<String, String> map = getEntradaBase();
        EntradaInvoicePdf invoicePdf = new EntradaInvoicePdf(map.get("credencial"), map.get("ip"), Boolean.FALSE,
                localizador, map.get("lenguaje"));

        ObjectMapper mapper = new ObjectMapper();
        String solicitud = null;
        try {
            solicitud = mapper.writeValueAsString(invoicePdf);
            LOGGER.debug("SOLICITUD JSON ES <ConsumirInvoicePdf> ---->: " + solicitud);
        } catch (JsonProcessingException e) {
            LOGGER.error("ERROR: <JsonProcessingException> al ejecutar el metodo: getParametros().", e);
            solicitud = null;
        }
        return solicitud;
    }


    public ByteArrayOutputStream getInvoicePDF(Localizador localizador){
        ByteArrayOutputStream respuesta = null;
        try {
            respuesta = (ByteArrayOutputStream) sendPostPDF(getParametros(localizador), localizador.getDatosConsumidor());
        } catch (IOException e) {
            LOGGER.error("ERROR: <IOException> al ejecutar el metodo: getInvoicePDF().", e);
            respuesta = null;
        } catch (Exception e) {
            LOGGER.error("ERROR: <Exception> al ejecutar el metodo: getInvoicePDF().", e);
            respuesta = null;
        }
        return respuesta;
    }


}

