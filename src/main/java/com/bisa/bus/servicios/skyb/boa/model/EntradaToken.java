package com.bisa.bus.servicios.skyb.boa.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by rchura on 09-11-15.
 */
@XmlRootElement
public class EntradaToken extends Entrada{

    public EntradaToken(String credenciales, String ip, Boolean xmlOrJson, Localizador localizador) {
        super(credenciales, ip, xmlOrJson, localizador);
    }
}
