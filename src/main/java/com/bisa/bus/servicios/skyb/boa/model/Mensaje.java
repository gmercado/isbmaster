package com.bisa.bus.servicios.skyb.boa.model;

import java.io.Serializable;

/**
 * @author rchura on 10-11-15
 */
public class Mensaje implements Serializable {

    private String codigo;

    private MessageType tipo;

    private String mensaje;

    public Mensaje() {
    }

    public Mensaje(String codigo, MessageType tipo, String mensaje) {
        this.codigo = codigo;
        this.tipo = tipo;
        this.mensaje = mensaje;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public MessageType getTipo() {
        return tipo;
    }

    public void setTipo(MessageType tipo) {
        this.tipo = tipo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "Mensaje{" + "codigo='" + codigo + '\'' + ", tipo=" + tipo + ", mensaje='" + mensaje + '\'' + '}';
    }
}
