package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;

/**
 * Created by rchura on 05-11-15.
 */
@JsonRootName(value = "reserva")
public class Reserva {

    /**
     * tipo_pv
     * Numérico: “0”, “1”.
     * Indica el tipo de punto de venta. Los valores significan:
     – 0: si en la reserva no se especificó ningún punto de venta.
     – 1: si en la reserva se especificó un punto de venta de tipo IATA.
     */
    @JsonProperty(value = "tipo_pv")
    private Short tipoPuntoVenta;

    /**
     * pv
     * Código de 8 dígitos.
     * Indica el punto de venta que generó la reserva.
     */
    //@JsonProperty(value = "pv")
    private String puntoVenta;

    /**
     * update
     * Elemento “update”, definido en el apartado 10.2.1.1
     * Información sobre la última actualización realizada en la reserva.
     */
    @JsonProperty(value = "update")
    private Update update;

    /**
     * grupo
     * Elemento “grupo” definido en el apartado 10.2.1.2
     * Información sobre el grupo generado.
     */
    //@JsonProperty(value = "grupo")
    private Grupo grupo;

    /**
     * pasajeros
     * Listado de elementos “pasajeroDR”, definido en apartado 10.2.1.3.
     * Todos los pasajeros incluidos en el PNR.
     */
    @JsonProperty(value = "pasajeros")
    private List<Pasajero> pasajeros;

    /**
     * vuelos
     * Listado de elementos “vuelo”, definido en apartado 10.2.1.4.
     * Todos los vuelos en los que se ha reservado plaza para todos los pasajeros.
     */
    @JsonProperty(value = "vuelos")
    private List<Vuelo> vuelos;

    /**
     * ssrs
     * Listado de elementos “ssr” definidos en el apartado 10.2.1.5.
     * Información de los SSRs incluidos en el PNR.
     */
    @JsonProperty(value = "ssrs")
    private List<SSR> ssrs;

    /**
     * osis
     * Listado de elementos “osi” definidos en el apartado 10.2.1.6.
     * Información de los elementos OSI incluidos en el PNR.
     */
    @JsonProperty(value = "osis")
    private List<OSI> osis;

    /**
     * ints
     * Listado de elementos “int” definidos en el apartado 10.2.1.7.
     * Información de los elementos INT incluidos en el PNR.
     *
     */
    //@JsonProperty(value = "ints")
    private List<INT> ints;

    /**
     * cts
     * Listado de elementos “ct”, definido en el apartado 10.2.1.8.
     * Información de formas de contacto establecidas en la reserva.
     */
    @JsonProperty(value = "cts")
    private List<Contacto> contactos;

    /**
     * tl
     * Elemento “tl” definido en el apartado 10.2.1.9.
     * Tiempo límite especificado para emitir la reserva.
     */
    @JsonProperty(value = "tl")
    private TiempoLimite tiempoLimite;

    /**
     * elementosTkt
     * Elemento “elementosTkt” definido en el apartado 10.2.1.10.
     * Información de los importes del itinerario
     */
    @JsonProperty(value = "elementosTkt")
    private TicketItinerario elementosTkt;

    /**
     * endosos
     * Listado de elementos “endoso” definido en el apartado 10.2.1.11.
     * Información de los endosos en la reserva.
     */
    //@JsonProperty(value = "endosos")
    private List<Endoso> endosos;

    /**
     * tour_code
     * Elemento “tour_code” definido en el apartado 10.2.1.12.
     * Información del tour code asociado a la reserva.
     */
    //@JsonProperty(value = "tour_code")
    private TourCode tourCode;

    /**
     * ois
     * Elemento “oi” definido en el apartado 10.2.1.13
     * Define los canjes establecidos entre elementos TKT.
     */
    @JsonProperty(value = "ois")
    private Canje ois;

    @JsonProperty(value = "fecha_creacion")
    private String fechaCreacion;

    @JsonProperty(value = "hora_creacion")
    private String horaCreacion;

    @JsonProperty(value = "responsable")
    private Responsable responsable;

    @JsonProperty(value = "localizador_resiber")
    private String localizadorResiber;




    public Short getTipoPuntoVenta() {
        return tipoPuntoVenta;
    }

    public void setTipoPuntoVenta(Short tipoPuntoVenta) {
        this.tipoPuntoVenta = tipoPuntoVenta;
    }

    public String getPuntoVenta() {
        return puntoVenta;
    }

    public void setPuntoVenta(String puntoVenta) {
        this.puntoVenta = puntoVenta;
    }

    public Update getUpdate() {
        return update;
    }

    public void setUpdate(Update update) {
        this.update = update;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public List<Pasajero> getPasajeros() {
        return pasajeros;
    }

    public void setPasajeros(List<Pasajero> pasajeros) {
        this.pasajeros = pasajeros;
    }

    public List<Vuelo> getVuelos() {
        return vuelos;
    }

    public void setVuelos(List<Vuelo> vuelos) {
        this.vuelos = vuelos;
    }

    public List<SSR> getSsrs() {
        return ssrs;
    }

    public void setSsrs(List<SSR> ssrs) {
        this.ssrs = ssrs;
    }

    public List<OSI> getOsis() {
        return osis;
    }

    public void setOsis(List<OSI> osis) {
        this.osis = osis;
    }

    public List<INT> getInts() {
        return ints;
    }

    public void setInts(List<INT> ints) {
        this.ints = ints;
    }

    public List<Contacto> getContactos() {
        return contactos;
    }

    public void setContactos(List<Contacto> contactos) {
        this.contactos = contactos;
    }

    public TiempoLimite getTiempoLimite() {
        return tiempoLimite;
    }

    public void setTiempoLimite(TiempoLimite tiempoLimite) {
        this.tiempoLimite = tiempoLimite;
    }

    public TicketItinerario getElementosTkt() {
        return elementosTkt;
    }

    public void setElementosTkt(TicketItinerario elementosTkt) {
        this.elementosTkt = elementosTkt;
    }

    public List<Endoso> getEndosos() {
        return endosos;
    }

    public void setEndosos(List<Endoso> endosos) {
        this.endosos = endosos;
    }

    public TourCode getTourCode() {
        return tourCode;
    }

    public void setTourCode(TourCode tourCode) {
        this.tourCode = tourCode;
    }

    public Canje getOis() {
        return ois;
    }

    public void setOis(Canje ois) {
        this.ois = ois;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getHoraCreacion() {
        return horaCreacion;
    }

    public void setHoraCreacion(String horaCreacion) {
        this.horaCreacion = horaCreacion;
    }

    public Responsable getResponsable() {
        return responsable;
    }

    public void setResponsable(Responsable responsable) {
        this.responsable = responsable;
    }

    public String getLocalizadorResiber() {
        return localizadorResiber;
    }

    public void setLocalizadorResiber(String localizadorResiber) {
        this.localizadorResiber = localizadorResiber;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Reserva{");
        sb.append("tipoPuntoVenta=").append(tipoPuntoVenta);
        sb.append(", puntoVenta='").append(puntoVenta).append('\'');
        sb.append(", update=").append(update);
        sb.append(", grupo=").append(grupo);
        sb.append(", pasajeros=").append(pasajeros);
        sb.append(", vuelos=").append(vuelos);
        sb.append(", ssrs=").append(ssrs);
        sb.append(", osis=").append(osis);
        sb.append(", ints=").append(ints);
        sb.append(", contactos=").append(contactos);
        sb.append(", tiempoLimite=").append(tiempoLimite);
        sb.append(", elementosTkt=").append(elementosTkt);
        sb.append(", endosos=").append(endosos);
        sb.append(", tourCode=").append(tourCode);
        sb.append(", ois=").append(ois);
        sb.append(", fechaCreacion='").append(fechaCreacion).append('\'');
        sb.append(", horaCreacion='").append(horaCreacion).append('\'');
        sb.append(", responsable=").append(responsable);
        sb.append(", localizadorResiber='").append(localizadorResiber).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
