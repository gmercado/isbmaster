package com.bisa.bus.servicios.skyb.boa.ws;

/**
 * @author rchura on 11-11-15
 */
public class ConsultaReservaRequest extends RequestBoa {

    public ConsultaReservaRequest() {
    }

    public ConsultaReservaRequest(String cliente, String usuario, String codigoPnr, String apellidoGrupo, String ipOrigen, String sistema) {
        super(cliente, usuario, codigoPnr, apellidoGrupo, ipOrigen, sistema);
    }

    @Override
    public String toString() {
        return super.toString();
    }


}
