package com.bisa.bus.servicios.skyb.boa.sched;

import bus.env.api.MedioAmbiente;
import bus.mail.api.MailerFactory;
import bus.mail.model.Mailer;
import bus.plumbing.file.ArchivoProfile;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.Monedas;
import com.bisa.bus.servicios.skyb.boa.api.ServiciosBoa;
import com.bisa.bus.servicios.skyb.boa.dao.ArchivosBoa;
import com.bisa.bus.servicios.skyb.boa.dao.PagosBoaDao;
import com.bisa.bus.servicios.skyb.boa.entities.*;
import com.bisa.bus.servicios.skyb.boa.model.ReporteDiario;
import com.bisa.bus.servicios.skyb.boa.model.ResultadoConciliacionDiaria;
import com.google.inject.Inject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static bus.env.api.Variables.*;

/**
 * Created by rchura on 04-11-15.
 */
public class ConciliacionDiariaPagosBoA implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConciliacionDiariaPagosBoA.class);

    private final ServiciosBoa serviciosBoa;
    private final PagosBoaDao pagosBoaDao;
    private final MedioAmbiente medioAmbiente;
    private final ArchivosBoa archivosBoa;
    private final MailerFactory mailerFactory;

    @Inject
    public ConciliacionDiariaPagosBoA(ServiciosBoa serviciosBoa, PagosBoaDao pagosBoaDao,
                                      MedioAmbiente medioAmbiente,
                                      ArchivosBoa archivosBoa, MailerFactory mailerFactory) {
        this.serviciosBoa = serviciosBoa;
        this.pagosBoaDao = pagosBoaDao;
        this.medioAmbiente = medioAmbiente;
        this.archivosBoa = archivosBoa;
        this.mailerFactory = mailerFactory;
    }



    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        String fechaString;
        Date   fechaDate = null;
        String manual = medioAmbiente.getValorDe(SKYB_BOA_CONCILIACION_MANUAL, SKYB_BOA_CONCILIACION_MANUAL_DEFAULT);
        if("true".equalsIgnoreCase(manual)){
            // Conciliacion manual, en base a la variable de medio ambiente
            LOGGER.info("Esta configurado ejecucion manual de Conciliacion de pagos BoA.");
            fechaString = medioAmbiente.getValorDe(SKYB_BOA_CONCILIACION_FECHA, SKYB_BOA_CONCILIACION_FECHA_DEFAULT);
            fechaDate = FormatosUtils.getFechaByString(fechaString);
        }else{
            // Conciliacin automatica
            LOGGER.info("Esta configurado ejecucion automatica de Conciliacion de pagos BoA.");
            Date fechaActual = new Date();
            fechaDate = FormatosUtils.getFechaXMenosYDias(fechaActual, 1);
            fechaString = FormatosUtils.formatoFechaISO(fechaDate);
        }

        LOGGER.debug("FECHA STRING [{}] Y FECHA DATE [{}]", fechaString, fechaDate);

        // Conciliacion de pagos totales
        try{
            conciliacionPagosTotales(fechaString, fechaDate);
        }catch (Exception e){
            LOGGER.error("Error: No se puede ejecutar la Conciliacion de pagos efectuados en fecha [{}].", fechaString);
        }

        // Envio de Archivo de pagos
        try{
            enviarArchivoCobranza(fechaString, fechaDate);
        }catch (Exception e){
            LOGGER.error("Error: No se puede enviar los archivos de Cobranza de pagos efectuados en fecha [{}].", fechaString);
        }

    }


    public void conciliacionPagosTotales(String fechaString, Date fechaDate){

        // Estableciendo fechas desde y hasta
        // Colocando la fecha en formato yyyy-MM-dd HH:mm:ss
        String cadenaDesde = FormatosUtils.fechaLargaFormateada(fechaDate)+" 00:00:00";
        String cadenaHasta = FormatosUtils.fechaLargaFormateada(fechaDate)+" 23:59:59";

        LOGGER.debug("----> desde {}, hasta {}", cadenaDesde, cadenaHasta);

        Date fechaIni = FormatosUtils.getFechaHoraLarga(cadenaDesde);
        Date fechaFin = FormatosUtils.getFechaHoraLarga(cadenaHasta);

        List<TotalPagosBoa> totalPagosBoA = null;

        try{
            totalPagosBoA = getPagosBoA(fechaString);
        }catch (Exception e){
            totalPagosBoA = null;
        }

        List<TotalPagosBoa> totalPagosBisa = null;
        try{
            totalPagosBisa = getPagosBisa(fechaIni, fechaFin);
        }catch (Exception e){
            totalPagosBisa = null;
        }

        if(totalPagosBoA==null && totalPagosBisa==null){
            LOGGER.error("No se puede obtener los totales pagados en BoA ni los totales pagados en el Banco BISA. " +
                    "Requiere revisión manual la conciliacion de la fecha [{}].", fechaString);
            return;
        }

        // Buscando diferencias
        List<TotalPagosBoa> diferenciaPagos = new ArrayList<TotalPagosBoa>();
        List<ReembolsoPagosBoa> reembolsoPagos = new ArrayList<ReembolsoPagosBoa>();

        TotalPagosBoa difPago;
        BigDecimal diferencia;
        boolean existePagosBoA = true;
        boolean existePagosBISA = true;

        if(totalPagosBisa!=null && totalPagosBisa.size()>0){

            Iterator<TotalPagosBoa> i= totalPagosBisa.iterator();
            while (i.hasNext()) {
                TotalPagosBoa pagoBISA = i.next();
                TotalPagosBoa pagoBoA = null;
                difPago = null;
                diferencia=BigDecimal.ZERO;
                if(totalPagosBoA!=null && totalPagosBoA.size()>0 ){

                    // Comparando con los pagos reportados por BOA
                    // DIFERENCIA
                    diferencia = obtieneDiferencias(totalPagosBoA, pagoBISA);
                    if(diferencia.doubleValue()!= 0){
                        difPago = new TotalPagosBoa(fechaDate, pagoBISA.getMoneda(), diferencia, 1, "DIFERENCIA");
                        if(diferencia.doubleValue()>0){
                            difPago.setNota("Existe diferencia de " + Monedas.getDescripcionByBisaCorta(new String("" + pagoBISA.getMoneda())) +
                                    " " + FormatosUtils.formatoImporte(diferencia) + " de más en el BISA");
                        }else{
                            difPago.setNota("Existe diferencia de "+ Monedas.getDescripcionByBisaCorta(new String(""+pagoBISA.getMoneda()))+
                                    " "+FormatosUtils.formatoImporte(diferencia)+" de menos en el BISA");
                        }
                        diferenciaPagos.add(difPago);
                    }

                    // REEMBOLSO

                    pagoBoA = obtieneTotalPagosBoa(totalPagosBoA, pagoBISA);

                    ReembolsoPagosBoa reembolso = new ReembolsoPagosBoa();
                    reembolso.setEmpresa(EmpresaBoa.BOA);
                    reembolso.setServicio(ServicioBoa.valorEnum(pagoBISA.getMoneda()));
                    reembolso.setFecha(fechaDate);
                    reembolso.setImporteBisa(pagoBISA.getTotal());
                    reembolso.setMoneda(Short.parseShort("" + pagoBISA.getMoneda()));
                    if(pagoBoA!=null) reembolso.setImporteBoa(pagoBoA.getTotal());
                    reembolso.setImporteDiferencia(diferencia);
                    reembolso.setCanal("e-Bisa");
                    reembolso.setCantidad(pagoBISA.getCantidad());

                    reembolsoPagos.add(reembolso);

                }else{
                    // No existen los totales reportados para BOA
                    LOGGER.error("Error: No se encontraron registros de totales pagados en BoA.");
                    existePagosBoA = false;
                    difPago = new TotalPagosBoa(fechaDate, pagoBISA.getMoneda(), pagoBISA.getTotal(), 1, "DIFERENCIA");
                    difPago.setNota("Existe diferencia de "+ Monedas.getDescripcionByBisaCorta(new String(""+pagoBISA.getMoneda()))+
                            " "+FormatosUtils.formatoImporte(pagoBISA.getTotal())+" en el BoA. No se tiene el reporte de pagos en BoA.");
                    diferenciaPagos.add(difPago);

                    ReembolsoPagosBoa reembolso = new ReembolsoPagosBoa();
                    reembolso.setEmpresa(EmpresaBoa.BOA);
                    reembolso.setServicio(ServicioBoa.valorEnum(pagoBISA.getMoneda()));
                    reembolso.setFecha(fechaDate);
                    reembolso.setImporteBisa(pagoBISA.getTotal());
                    reembolso.setMoneda(Short.parseShort("" + pagoBISA.getMoneda()));
                    reembolso.setImporteBoa(BigDecimal.ZERO);
                    reembolso.setImporteDiferencia(pagoBISA.getTotal());
                    reembolso.setCanal("e-Bisa");
                    reembolso.setCantidad(pagoBISA.getCantidad());

                    reembolsoPagos.add(reembolso);

                }
            } // FIN WHILE

            // Revisar si no existe algpun registro que no se haya conciliado
            if(totalPagosBoA!=null && totalPagosBisa!=null){
                if(totalPagosBoA.size() > totalPagosBisa.size()){
                    LOGGER.info("Existe diferencia en las listas -->  totalPagosBoA.size() > totalPagosBisa.size()");
                    List<TotalPagosBoa> difA = obtieneDiferenciaListaPagos(totalPagosBoA, totalPagosBisa);
                    if(difA!=null && difA.size()>0){
                        for(TotalPagosBoa b : difA){
                            LOGGER.info("Registro NO comparado: "+b.toString());
                            b.setNota("Existen pagos procesados en BoA que no se encuentran registrados en el BISA.");
                            diferenciaPagos.add(b);
                        }
                    }
                }
                if(totalPagosBisa.size() > totalPagosBoA.size()){
                    LOGGER.info("Existe diferencia en las listas -->  totalPagosBisa.size() > totalPagosBoA.size()");
                    List<TotalPagosBoa> difB = obtieneDiferenciaListaPagos(totalPagosBisa, totalPagosBoA);
                    if(difB!=null && difB.size()>0){
                        for(TotalPagosBoa b : difB){
                            LOGGER.info("Registro NO comparado: "+b.toString());
                            b.setNota("Existen pagos procesados en el BISA que no se encuentran registrados en BoA.");
                            diferenciaPagos.add(b);
                        }
                    }
                }
            }

        }else{
            // No existen los totales para BISA
            LOGGER.error("Error: No se encontraron registros de totales registrados en el BISA.");
            existePagosBISA = false;
            if(totalPagosBoA!=null && totalPagosBoA.size()>0 ){
                difPago = null;
                for(TotalPagosBoa pagoBoA : totalPagosBoA ){
                    difPago = new TotalPagosBoa(fechaDate, pagoBoA.getMoneda(), pagoBoA.getTotal(), 1, "DIFERENCIA");
                    difPago.setNota("Existe diferencia de "+ Monedas.getDescripcionByBisaCorta(new String(""+pagoBoA.getMoneda()))+
                            " "+FormatosUtils.formatoImporte(pagoBoA.getTotal())+" en el BISA. No se puede obtener el total pagado en el BISA.");
                    diferenciaPagos.add(difPago);
                }
            }else{
                existePagosBoA = false;
            }
        }

        // Reportando los totales a reembolsar
        String mensaje = null;
        if(!existePagosBISA && !existePagosBoA){
            mensaje ="No se encontraron registro de pagos para la fecha "+FormatosUtils.formatoFecha(fechaDate);
            enviarMensajeConciliacion(mensaje, null, null);
        }else{

            enviarMensajeConciliacion(null, reembolsoPagos, diferenciaPagos);

        }

    }


    public List<TotalPagosBoa> getPagosBoA(String fecha){

        ResultadoConciliacionDiaria conciliacionDiaria = null;
        List<TotalPagosBoa> listaPagos = null;

        if(fecha==null){
            LOGGER.error("Debe especificar una fecha de conciliacion.");
            return null;
        }

        BigDecimal totalBs=BigDecimal.ZERO;
        long cantidadBs=0;

        BigDecimal totalSus=BigDecimal.ZERO;
        long cantidadSus=0;

        conciliacionDiaria = serviciosBoa.obtenerReporteDiario(fecha);
        if(conciliacionDiaria==null){
            return null;
        }

        if(conciliacionDiaria!=null && conciliacionDiaria.getReporteDiario()!=null){

            for(ReporteDiario diario : conciliacionDiaria.getReporteDiario()){
                LOGGER.debug("--> "+diario.toString());
                if(ReporteDiario.PROCESADO.equalsIgnoreCase(StringUtils.trimToEmpty(diario.getEstado()))){
                    // Solo aplicacmos la conciliacion sobre registros PROCESADOS = "1"
                    // Sumatoria por tipo de moneda
                    if(MonedasBoa.BOB.name().equalsIgnoreCase(StringUtils.trimToEmpty(diario.getMoneda()))){
                        totalBs = totalBs.add(diario.getImporteTotal());
                        cantidadBs++;
                    }else if(MonedasBoa.USD.name().equalsIgnoreCase(StringUtils.trimToEmpty(diario.getMoneda()))){
                        totalSus = totalSus.add(diario.getImporteTotal());
                        cantidadSus++;
                    }
                }
            }
        }

        listaPagos = new ArrayList<TotalPagosBoa>();
        if(totalBs.doubleValue()>0){
            TotalPagosBoa pagosBoa = new TotalPagosBoa(FormatosUtils.getFechaByString(fecha), MonedasBoa.BOB.getCodigo(), totalBs, cantidadBs, "BOA");
            listaPagos.add(pagosBoa);
        }
        if(totalSus.doubleValue()>0){
            TotalPagosBoa pagosBoa = new TotalPagosBoa(FormatosUtils.getFechaByString(fecha), MonedasBoa.USD.getCodigo(), totalSus, cantidadSus, "BOA");
            listaPagos.add(pagosBoa);
        }

        return listaPagos;
    }



    public List<TotalPagosBoa> getPagosBisa(Date fechadesde, Date fechahasta){

        List<TotalPagosBoa> listaPagos = null;

        if(fechadesde==null){
            LOGGER.error("Debe especificar una fecha para obtener el total de pagos para la conciliacion.");
            return null;
        }
        listaPagos = pagosBoaDao.getTotalPagosByFecha(fechadesde, fechahasta);

        return listaPagos;
    }


    public void enviarMensajeConciliacion(String mensaje, List<ReembolsoPagosBoa> reembolsos, List<TotalPagosBoa> diferencias){

        String mails = medioAmbiente.getValorDe(SKYB_BOA_CONCILIACION_EMAIL_ENVIO, SKYB_BOA_CONCILIACION_EMAIL_ENVIO_DEFAULT);
        String[] arraysMail =  StringUtils.split(mails, ",;");
        Mailer mail =   mailerFactory.getMailer(arraysMail);

        StringBuilder sb = null;
        String asunto = null;
        if(mensaje!=null && mensaje.length()>0){
            sb = new StringBuilder();
            sb.append("\n");
            sb.append(mensaje);
            sb.append("\n");
            sb.append("Atte,\nSistema Aqua-ISB\n");
            sb.append("Banco Bisa S.A.\n");

            asunto = "Conciliacion para la Empresa: " + EmpresaBoa.BOA.getDescripcion();
            try {
                mail.mailAndForget(asunto, sb.toString());
            } catch (Exception e) {

                LOGGER.error("Error: No se puede eviar el correo electronico con el siguiente detalle: \n" + sb.toString() + "\n", e);
            }
            return;
        }



        if(reembolsos==null){
            LOGGER.error("Error no se puede efectuar el reembolso manual a BoA. No existen datos para el reembolso.");
        }else{

            for(ReembolsoPagosBoa reembolso: reembolsos){

                sb = new StringBuilder();
                sb.append("\n");
                sb.append("Reembolso manual para la empresa: " + reembolso.getEmpresa().getDescripcion()+ " fue exitoso.\n");
                sb.append("El importe a reembolsar: "+ FormatosUtils.formatoImporte(reembolso.getImporteBisa()) + ".-\n");
                sb.append("La moneda es: "+ Monedas.getDescripcionByBisaCorta(""+reembolso.getMoneda())+"\n");
                sb.append("Cantidad de pagos efectuados:  "+ reembolso.getCantidad() + "\n");
                sb.append("Correspondiente a la fecha: " + FormatosUtils.formatoFecha(reembolso.getFecha())+ "\n");
                sb.append("Por el servicio de: " + reembolso.getServicio().getDescripcion() + "\n");
                sb.append("Pagos efectuados por el Canal " + reembolso.getCanal()+ "\n");
                sb.append("\n");
                sb.append("Atte,\nSistema Aqua-ISB\n");
                sb.append("Banco Bisa S.A.\n");

                asunto = "Conciliacion para la Empresa: " + reembolso.getEmpresa().getDescripcion() + ", Servicio:"+reembolso.getServicio().getDescripcion();

                try {

                    mail.mailAndForget(asunto, sb.toString());
                } catch (Exception e) {
                    LOGGER.error("Error: No se puede eviar el correo electronico con el siguiente detalle: \n" + sb.toString() + "\n", e);
                }

                LOGGER.info("Remmbolso manual con la empresa [{}] y servicio [{}] fue exitosa", reembolso.getEmpresa().getDescripcion(), reembolso.getServicio().getDescripcion());

            }
        }

        if(diferencias!=null && diferencias.size()>0){

            for(TotalPagosBoa diferencia: diferencias){
                ReembolsoPagosBoa reembolsoPago = null;

                for(ReembolsoPagosBoa reembolso: reembolsos){
                    if (reembolso.getFecha().equals(diferencia.getFecha()) &&
                            reembolso.getMoneda()==diferencia.getMoneda()){
                        reembolsoPago =  reembolso;
                        break;
                    }
                }

                ServicioBoa servicio = ServicioBoa.valorEnum(diferencia.getMoneda());

                if(reembolsoPago!=null){
                    // Trabajar sobre los datos de REEMBOLSO

                    LOGGER.info("Importe Diferencia [{}] para la empresa [{}] y servicio [{}].",
                            new Object[]{diferencia.getTotal(), EmpresaBoa.BOA.getDescripcion(), servicio.getDescripcion()});
                    StringBuilder sbDif = new StringBuilder();
                    sbDif.append("\n");
                    sbDif.append("Existen diferencias para la empresa: " + EmpresaBoa.BOA.getDescripcion() + ".\n");
                    sbDif.append("El importe de la diferencia es de: "+Monedas.getDescripcionByBisaCorta(""+diferencia.getMoneda())+" " + FormatosUtils.formatoImporte(diferencia.getTotal())+ ".-\n");
                    sbDif.append("Por el servicio de: " + servicio.getDescripcion() + "\n");
                    sbDif.append("Para la fecha: " + FormatosUtils.formatoFecha(diferencia.getFecha())+"\n");
                    sbDif.append("Por el canal: " + reembolsoPago.getCanal()+ "\n");
                    sbDif.append("Importe Total en el Bisa: " + Monedas.getDescripcionByBisaCorta(""+reembolsoPago.getMoneda())+" " + FormatosUtils.formatoImporte(reembolsoPago.getImporteBisa())+ ".-\n");
                    sbDif.append("Importe Total en en BoA: " + Monedas.getDescripcionByBisaCorta(""+reembolsoPago.getMoneda())+" " + FormatosUtils.formatoImporte(reembolsoPago.getImporteBoa())+ ".-\n");
                    sbDif.append("\n");
                    sbDif.append("Atte,\nSistema Aqua-ISB\n");
                    sbDif.append("Banco Bisa S.A.\n");
                    asunto =  "Diferencias para la empresa: " + EmpresaBoa.BOA.getDescripcion()+ ", Servicio:"+servicio.getDescripcion();

                    try {
                        mail.mailAndForget(asunto, sbDif.toString());

                    } catch (Exception e) {
                        LOGGER.error("Error: No se puede eviar el correo electronico con el siguiente detalle: \n"+sb.toString()+"\n", e);
                    }
                }else{
                    // Trabajar sobre los datos de DIFERENCIA
                    LOGGER.info("Importe Diferencia [{}] para la empresa [{}] y servicio [{}].",
                            new Object[]{diferencia.getTotal(), EmpresaBoa.BOA.getDescripcion(), servicio.getDescripcion()});
                    StringBuilder sbDif = new StringBuilder();
                    sbDif.append("\n");
                    sbDif.append("Existen diferencias para la empresa: " + EmpresaBoa.BOA.getDescripcion() + ".\n");
                    sbDif.append("El importe de la diferencia es de: "+Monedas.getDescripcionByBisaCorta(""+diferencia.getMoneda())+" " + FormatosUtils.formatoImporte(diferencia.getTotal())+ ".-\n");
                    sbDif.append("Por el servicio de: " + servicio.getDescripcion() + "\n");
                    sbDif.append("Para la fecha: " + FormatosUtils.formatoFecha(diferencia.getFecha())+"\n");
                    sbDif.append("Por el canal: " + diferencia.getOrigen()+ "\n");
                    sbDif.append("Importe Total en el "+diferencia.getOrigen()+": " + Monedas.getDescripcionByBisaCorta(""+diferencia.getMoneda())+" " + FormatosUtils.formatoImporte(diferencia.getTotal())+ ".-\n");
                    sbDif.append("NOTA: " + diferencia.getNota()+ "\n");
                    //sbDif.append("Importe Total en en BoA: " + Monedas.getDescripcionByBisaCorta(""+reembolsoPago.getMoneda())+" " + FormatosUtils.formatoImporte(reembolsoPago.getImporteBoa())+ ".-\n");
                    sbDif.append("\n");
                    sbDif.append("Atte,\nSistema Aqua-ISB\n");
                    sbDif.append("Banco Bisa S.A.\n");
                    asunto =  "Diferencias para la empresa: " + EmpresaBoa.BOA.getDescripcion()+ ", Servicio:"+servicio.getDescripcion();

                    try {
                        mail.mailAndForget(asunto, sbDif.toString());

                    } catch (Exception e) {
                        LOGGER.error("Error: No se puede eviar el correo electronico con el siguiente detalle: \n"+sb.toString()+"\n", e);
                    }
                }



            }
        }

        LOGGER.debug("CORREO A ENVIARSE: \n" + sb.toString());


    }



    public BigDecimal obtieneDiferencias(List<TotalPagosBoa> listaPagos, TotalPagosBoa pagoBase){

        Iterator<TotalPagosBoa> i= listaPagos.iterator();
        BigDecimal diferencia=BigDecimal.ZERO;
        //SimpleDateFormat sdfBisa = new SimpleDateFormat("dd/MM/yy");
        //SimpleDateFormat sdfIntegrador = new SimpleDateFormat("yyyy-MM-dd");
        //String fecha;
        while (i.hasNext()) {
            TotalPagosBoa pagoSiguiente = i.next();
            //fecha=sdfBisa.format(sdfIntegrador.parse(cobroIntegrador.getFecha()));
            LOGGER.debug("Comparando el pago total en curso de la lista [{}].",pagoSiguiente.toString());
            if (pagoBase.getFecha().equals(pagoSiguiente.getFecha()) &&
                    pagoBase.getMoneda()==pagoSiguiente.getMoneda())
            {
                diferencia=pagoBase.getTotal().subtract(pagoSiguiente.getTotal());
                if(diferencia.doubleValue()!=0){
                    LOGGER.error("Existe diferencia por un monto de [{}] comparando con el pago base [{}].", diferencia.toPlainString(), pagoBase);
                }
            }
            //LOG.info("FECHA COMPARADA: "+cobroBisa.getFechaFormatoyyyyMMdd());
        }
        return diferencia;
    }

    public TotalPagosBoa obtieneTotalPagosBoa(List<TotalPagosBoa> listaPagos, TotalPagosBoa pagoBase){
        Iterator<TotalPagosBoa> i = listaPagos.iterator();
        TotalPagosBoa pagoSiguiente = null;
        while (i.hasNext()) {
            pagoSiguiente = i.next();
            //LOG.debug("Obtiene Cobro integrador fecha {}",sdfBisa.format(sdfIntegrador.parse(cobroIntegrador.getFecha())));
            LOGGER.debug("Obtiene el pago total en curso de la lista [{}].",pagoSiguiente.toString());
            //fecha=sdfBisa.format(sdfIntegrador.parse(cobroIntegrador.getFecha()));
            if (pagoBase.getFecha().equals(pagoSiguiente.getFecha()) &&
                    pagoBase.getMoneda()==pagoSiguiente.getMoneda()){

                return pagoSiguiente;
            }
        }
        return null;
    }


    public List<TotalPagosBoa> obtieneDiferenciaListaPagos(List<TotalPagosBoa> listaPagosA, List<TotalPagosBoa> listaB){
        List<TotalPagosBoa> listaDiferencia = new ArrayList<TotalPagosBoa>();

        Iterator<TotalPagosBoa> i = listaPagosA.iterator();
        TotalPagosBoa pagoSiguiente = null;
        boolean existe = false;
        while (i.hasNext()) {
            pagoSiguiente = i.next();
            for(TotalPagosBoa pago : listaB){
                if (pago.getFecha().equals(pagoSiguiente.getFecha()) &&
                        pago.getMoneda()==pagoSiguiente.getMoneda()){
                    existe = true;
                }
            }

            if(!existe){
                listaDiferencia.add(pagoSiguiente);
            }
            existe = false;
        }
        if(listaDiferencia.isEmpty() || listaDiferencia.size()==0) return null;

        return listaDiferencia;
    }


    public void enviarArchivoCobranza(String fechaString, Date fechaDate){

        DateTime dateTime = new DateTime(new Date());
        String saludo="Buenos d\u00edas";
        if (dateTime.getHourOfDay()>12) saludo="Buenas tardes";
        if (dateTime.getHourOfDay()>18) saludo="Buenas noches";

        // Estableciendo fechas desde y hasta
        Date fechaIni = fechaDate;
        Date fechaFin = fechaDate;
        // Colocando la fecha en formato yyyy-MM-dd HH:mm:ss
        String cadenaDesde = FormatosUtils.fechaLargaFormateada(fechaIni) + " 00:00:00";
        String cadenaHasta = FormatosUtils.fechaLargaFormateada(fechaFin) + " 23:59:59";

        fechaIni = FormatosUtils.getFechaHoraLarga(cadenaDesde);
        fechaFin = FormatosUtils.getFechaHoraLarga(cadenaHasta);

        // Generacion del archivo de pagos
        ArchivoProfile profileBs = archivosBoa.generaArchivoCobranza(fechaIni, fechaFin);

        // Generacion del mail
        String mails = medioAmbiente.getValorDe(SKYB_BOA_RECAUDACION_EMAIL_ENVIO, SKYB_BOA_RECAUDACION_EMAIL_ENVIO_DEFAULT);
        String[] arraysMail =  StringUtils.split(mails, ",;");

        String subject = medioAmbiente.getValorDe(SKYB_BOA_RECAUDACION_SUBJECT_MAIL, SKYB_BOA_RECAUDACION_SUBJECT_MAIL_DEFAULT)+
                " "+FormatosUtils.fechaFormateadaConYYYYMMDD(fechaIni);
        StringBuilder sb = new StringBuilder();

        Mailer mail =   mailerFactory.getMailer(arraysMail);

        if(profileBs!=null){
            sb.append(saludo + ",\n\n");
            sb.append("Se\u00f1ores Boliviana de Aviaci\u00f3n,\n\n");
            sb.append("Adjuntamos a la presente, el archivo correspondiente a los pagos efectuados en la fecha: ");
            sb.append( FormatosUtils.fechaFormateadaConYYYY(fechaDate));
            sb.append(" bajo el siguiente detalle:\n\n");
            sb.append("\tNombre del archivo                      Registros\n");
            sb.append("\t------------------------                ---------\n\t");

            if(profileBs!=null){
                sb.append(StringUtils.rightPad(profileBs.getArchivo().getName(), 36, " "));
                sb.append("\t");
                sb.append(profileBs.getNroFilas());
                sb.append("\n\t");
            }

            sb.append("\n\nAtte,");
            sb.append("\nSistema Aqua-ISB\n");
            sb.append("Banco Bisa S.A.\n");

            LOGGER.debug("MAIL GENERADO:\n" + sb.toString());
            mail.mailAndForgetTempFileExample(subject, sb.toString(), profileBs.getArchivo(), null);

        }else{
            LOGGER.info("No se genera archivo de pagos pra la Boliviana de Aviavion porque no hubo pagos");

            sb.append(saludo + ",\n\n");
            sb.append("Se\u00f1ores Boliviana de Aviaci\u00f3n,\n\n");
            sb.append("Enviamos el correo correspondiente a la fecha: ");
            sb.append( FormatosUtils.fechaFormateadaConYYYY(fechaDate));
            sb.append(" indicando que no hubieron pagos.");

            sb.append("\n\nAtte,");
            sb.append("\nSistema Aqua-ISB\n");
            sb.append("Banco Bisa S.A.\n");

            LOGGER.debug("MAIL GENERADO:\n"+sb.toString());
            mail.mailAndForget(subject, sb.toString());
        }

    }
}
