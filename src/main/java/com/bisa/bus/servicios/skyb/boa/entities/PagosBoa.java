package com.bisa.bus.servicios.skyb.boa.entities;

import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.Monedas;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by rchura on 10-11-15.
 */
@Entity
@Table(name = "EBPAGOSBOA")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "BOAFECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "BOAUSRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "BOAFECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "BOAUSRMOD"))
})
public class PagosBoa extends EntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BOAID", unique = true)
    private Long id;

    @Column(name = "BOANROOPE", precision = 18, scale = 0)
    private BigDecimal nroOperacion;

    @Column(name = "BOACLIENT", length = 10)
    private String nroCliente;

    @Column(name = "BOAUSRPRO", length = 25)
    private String userProceso;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "BOAFECPRO")
    private Date fechaProceso;

    @Column(name = "BOANUMPNR", length = 20)
    private String nroPNR;

    @Column(name = "BOAFECRSV", length = 10)
    private String fechaReserva;

    @Column(name = "BOAHORRSV", length = 10)
    private String horaReserva;

    @Column(name = "BOAFECLIM", length = 10)
    private String fechaLimite;

    @Column(name = "BOAESTADO")
    private int estado;

    @Column(name = "BOATPODOC", length = 4)
    private String tipoDocumento;

    @Column(name = "BOANRODOC", length = 15)
    private String nroDocumento;

    @Column(name = "BOARAZONSC", length = 50)
    private String razonSocial;

    @Column(name = "BOAIPORGN", length = 20)
    private String ipOrigen;

    @Column(name = "BOANOMBRE", length = 100)
    private String nombre;

    @Column(name = "BOATTOTAL",  scale = 12, precision = 2)
    private BigDecimal importeTotal;

    @Column(name = "BOATTOTMON")
    private int monedaImporteTotal;

    @Column(name = "BOACTAORI",  scale = 10, precision = 0)
    private BigDecimal cuentaDebito;

    @Column(name = "BOACTAOTP", length = 1)
    private String tipoCtaDebito;

    @Column(name = "BOACTAOMN")
    private int  monedaCtaDebito;

    @Column(name = "BOACTATIT", length = 80)
    private String titularCta;

    @Column(name = "BOANOTA", length = 60)
    private String nota;

    @Column(name = "BOAWNUMUSR",  scale = 4, precision = 0)
    private BigDecimal nroCaja;

    @Column(name = "BOAWNUMSEQ",  scale = 7, precision = 0)
    private BigDecimal nroSecuencia;

    @Column(name = "BOAWPOST", length = 1)
    private String posteo;

    @Column(name = "BOAWFECPRO",  scale = 8, precision = 0)
    private BigDecimal fechaMonitor;

    @Column(name = "BOAWFECSIS",  scale = 8, precision = 0)
    private BigDecimal fechaSistema;

    @Column(name = "BOAWHORA",  scale = 6, precision = 0)
    private BigDecimal horaSistema;

    @Column(name = "BOAWCODSEG",  scale = 11, precision = 0)
    private BigDecimal codigoSeguridad;

    @Column(name = "BOAWIMPTOT",  scale = 12, precision = 2)
    private BigDecimal totalMonitor;

    @Column(name = "BOAWCARGO",  scale = 12, precision = 2)
    private BigDecimal cargoMonitor;

    @Column(name = "BOAWCOTACT",  scale = 12, precision = 2)
    private BigDecimal cotizacionMonitor;

    @Column(name = "BOAWCOMVEN",  length = 1)
    private String compraVenta;

    @Column(name = "BOAWREJ1",  length = 3)
    private String errorMonitor;

    @Column(name = "BOAWNSQREV",  scale = 7, precision = 0)
    private BigDecimal secuenciaReversa;

    @Column(name = "BOAWPOSTRV",  length = 1)
    private String posteoReversa;

    @Column(name = "BOACODRES",  length = 5)
    private String estadoRespuesta;

    @Column(name = "BOADESCRES",  length = 200)
    private String mensajeRespuesta;

    @Column(name = "BOAAGENCIA",  scale = 4, precision = 0)
    private Long agencia;

    @Column(name = "BOATPVENTA",  length = 1)
    private String tipoVenta;

    @Column(name = "BOAPVENTA",  length = 100)
    private String puntoVenta;

    @Column(name = "BOANPASAJS",  scale = 3)
    private Long nroPasajeros;

    @Column(name = "BOACANVLOS",  scale = 3)
    private Long cantVuelos;

    @Column(name = "BOAMAILS",  length = 100)
    private String mails;

    @Column(name = "BOALVUELOS",  length = 1000)
    private String vuelos;

    @Column(name = "BOALPAXS",  length = 500)
    private String pasajeros;

    @Column(name = "BOALTIKETS",  length = 500)
    private String tickets;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "BOAFECEMTK")
    private Date fechaEmision;

    @Column(name = "BOAUSRREC", length = 25)
    private String userRechazo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "BOAFECREC")
    private Date fechaRechazo;

    @Column(name = "BOASISTEMA", length = 25)
    private String sistema;

    @Column(name = "BOATOKEN", length = 250)
    private String token;

    @Transient
    private EstadosBoa estadosBoa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getNroOperacion() {
        return nroOperacion;
    }

    public void setNroOperacion(BigDecimal nroOperacion) {
        this.nroOperacion = nroOperacion;
    }

    public String getNroCliente() {
        return nroCliente;
    }

    public void setNroCliente(String nroCliente) {
        this.nroCliente = nroCliente;
    }

    public String getUserProceso() {
        return userProceso;
    }

    public void setUserProceso(String userProceso) {
        this.userProceso = userProceso;
    }

    public Date getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(Date fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public String getNroPNR() {
        return nroPNR;
    }

    public void setNroPNR(String nroPNR) {
        this.nroPNR = nroPNR;
    }

    public String getFechaReserva() {
        return fechaReserva;
    }

    public void setFechaReserva(String fechaReserva) {
        this.fechaReserva = fechaReserva;
    }

    public String getHoraReserva() {
        return horaReserva;
    }

    public void setHoraReserva(String horaReserva) {
        this.horaReserva = horaReserva;
    }

    public String getFechaLimite() {
        return fechaLimite;
    }

    public void setFechaLimite(String fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getIpOrigen() {
        return ipOrigen;
    }

    public void setIpOrigen(String ipOrigen) {
        this.ipOrigen = ipOrigen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(BigDecimal importeTotal) {
        this.importeTotal = importeTotal;
    }

    public int getMonedaImporteTotal() {
        return monedaImporteTotal;
    }

    public void setMonedaImporteTotal(int monedaImporteTotal) {
        this.monedaImporteTotal = monedaImporteTotal;
    }

    public BigDecimal getCuentaDebito() {
        return cuentaDebito;
    }

    public void setCuentaDebito(BigDecimal cuentaDebito) {
        this.cuentaDebito = cuentaDebito;
    }

    public String getTipoCtaDebito() {
        return tipoCtaDebito;
    }

    public void setTipoCtaDebito(String tipoCtaDebito) {
        this.tipoCtaDebito = tipoCtaDebito;
    }

    public int getMonedaCtaDebito() {
        return monedaCtaDebito;
    }

    public void setMonedaCtaDebito(int monedaCtaDebito) {
        this.monedaCtaDebito = monedaCtaDebito;
    }

    public String getTitularCta() {
        return titularCta;
    }

    public void setTitularCta(String titularCta) {
        this.titularCta = titularCta;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public BigDecimal getNroCaja() {
        return nroCaja;
    }

    public void setNroCaja(BigDecimal nroCaja) {
        this.nroCaja = nroCaja;
    }

    public BigDecimal getNroSecuencia() {
        return nroSecuencia;
    }

    public void setNroSecuencia(BigDecimal nroSecuencia) {
        this.nroSecuencia = nroSecuencia;
    }

    public String getPosteo() {
        return posteo;
    }

    public void setPosteo(String posteo) {
        this.posteo = posteo;
    }

    public BigDecimal getFechaMonitor() {
        return fechaMonitor;
    }

    public void setFechaMonitor(BigDecimal fechaMonitor) {
        this.fechaMonitor = fechaMonitor;
    }

    public BigDecimal getFechaSistema() {
        return fechaSistema;
    }

    public void setFechaSistema(BigDecimal fechaSistema) {
        this.fechaSistema = fechaSistema;
    }

    public BigDecimal getHoraSistema() {
        return horaSistema;
    }

    public void setHoraSistema(BigDecimal horaSistema) {
        this.horaSistema = horaSistema;
    }

    public BigDecimal getCodigoSeguridad() {
        return codigoSeguridad;
    }

    public void setCodigoSeguridad(BigDecimal codigoSeguridad) {
        this.codigoSeguridad = codigoSeguridad;
    }

    public BigDecimal getTotalMonitor() {
        return totalMonitor;
    }

    public void setTotalMonitor(BigDecimal totalMonitor) {
        this.totalMonitor = totalMonitor;
    }

    public BigDecimal getCargoMonitor() {
        return cargoMonitor;
    }

    public void setCargoMonitor(BigDecimal cargoMonitor) {
        this.cargoMonitor = cargoMonitor;
    }

    public BigDecimal getCotizacionMonitor() {
        return cotizacionMonitor;
    }

    public void setCotizacionMonitor(BigDecimal cotizacionMonitor) {
        this.cotizacionMonitor = cotizacionMonitor;
    }

    public String getCompraVenta() {
        return compraVenta;
    }

    public void setCompraVenta(String compraVenta) {
        this.compraVenta = compraVenta;
    }

    public String getErrorMonitor() {
        return errorMonitor;
    }

    public void setErrorMonitor(String errorMonitor) {
        this.errorMonitor = errorMonitor;
    }

    public BigDecimal getSecuenciaReversa() {
        return secuenciaReversa;
    }

    public void setSecuenciaReversa(BigDecimal secuenciaReversa) {
        this.secuenciaReversa = secuenciaReversa;
    }

    public String getPosteoReversa() {
        return posteoReversa;
    }

    public void setPosteoReversa(String posteoReversa) {
        this.posteoReversa = posteoReversa;
    }

    public String getEstadoRespuesta() {
        return estadoRespuesta;
    }

    public void setEstadoRespuesta(String estadoRespuesta) {
        this.estadoRespuesta = estadoRespuesta;
    }

    public String getMensajeRespuesta() {
        return mensajeRespuesta;
    }

    public void setMensajeRespuesta(String mensajeRespuesta) {
        this.mensajeRespuesta = mensajeRespuesta;
    }

    public Long getAgencia() {
        return agencia;
    }

    public void setAgencia(Long agencia) {
        this.agencia = agencia;
    }

    public String getTipoVenta() {
        return tipoVenta;
    }

    public void setTipoVenta(String tipoVenta) {
        this.tipoVenta = tipoVenta;
    }

    public String getPuntoVenta() {
        return puntoVenta;
    }

    public void setPuntoVenta(String puntoVenta) {
        this.puntoVenta = puntoVenta;
    }

    public Long getNroPasajeros() {
        return nroPasajeros;
    }

    public void setNroPasajeros(Long nroPasajeros) {
        this.nroPasajeros = nroPasajeros;
    }

    public String getTickets() {
        return tickets;
    }

    public void setTickets(String tickets) {
        this.tickets = tickets;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getUserRechazo() {
        return userRechazo;
    }

    public void setUserRechazo(String userRechazo) {
        this.userRechazo = userRechazo;
    }

    public Date getFechaRechazo() {
        return fechaRechazo;
    }

    public void setFechaRechazo(Date fechaRechazo) {
        this.fechaRechazo = fechaRechazo;
    }

    public String getPasajeros() {
        return pasajeros;
    }

    public void setPasajeros(String pasajeros) {
        this.pasajeros = pasajeros;
    }

    public String getMails() {
        return mails;
    }

    public void setMails(String mails) {
        this.mails = mails;
    }

    public Long getCantVuelos() {
        return cantVuelos;
    }

    public void setCantVuelos(Long cantVuelos) {
        this.cantVuelos = cantVuelos;
    }

    public String getVuelos() {
        return vuelos;
    }

    public void setVuelos(String vuelos) {
        this.vuelos = vuelos;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PagosBoa{");
        sb.append("id=").append(id);
        sb.append(", nroOperacion=").append(nroOperacion);
        sb.append(", nroCliente='").append(nroCliente).append('\'');
        sb.append(", userProceso='").append(userProceso).append('\'');
        sb.append(", fechaProceso=").append(fechaProceso);
        sb.append(", nroPNR='").append(nroPNR).append('\'');
        sb.append(", fechaReserva='").append(fechaReserva).append('\'');
        sb.append(", horaReserva='").append(horaReserva).append('\'');
        sb.append(", fechaLimite='").append(fechaLimite).append('\'');
        sb.append(", estado=").append(estado);
        sb.append(", tipoDocumento='").append(tipoDocumento).append('\'');
        sb.append(", nroDocumento='").append(nroDocumento).append('\'');
        sb.append(", razonSocial='").append(razonSocial).append('\'');
        sb.append(", ipOrigen='").append(ipOrigen).append('\'');
        sb.append(", nombre='").append(nombre).append('\'');
        sb.append(", importeTotal=").append(importeTotal);
        sb.append(", monedaImporteTotal=").append(monedaImporteTotal);
        sb.append(", cuentaDebito=").append(cuentaDebito);
        sb.append(", tipoCtaDebito='").append(tipoCtaDebito).append('\'');
        sb.append(", monedaCtaDebito=").append(monedaCtaDebito);
        sb.append(", titularCta='").append(titularCta).append('\'');
        sb.append(", nota='").append(nota).append('\'');
        sb.append(", nroCaja=").append(nroCaja);
        sb.append(", nroSecuencia=").append(nroSecuencia);
        sb.append(", posteo='").append(posteo).append('\'');
        sb.append(", fechaMonitor=").append(fechaMonitor);
        sb.append(", fechaSistema=").append(fechaSistema);
        sb.append(", horaSistema=").append(horaSistema);
        sb.append(", codigoSeguridad=").append(codigoSeguridad);
        sb.append(", totalMonitor=").append(totalMonitor);
        sb.append(", cargoMonitor=").append(cargoMonitor);
        sb.append(", cotizacionMonitor=").append(cotizacionMonitor);
        sb.append(", compraVenta='").append(compraVenta).append('\'');
        sb.append(", errorMonitor='").append(errorMonitor).append('\'');
        sb.append(", secuenciaReversa=").append(secuenciaReversa);
        sb.append(", posteoReversa='").append(posteoReversa).append('\'');
        sb.append(", estadoRespuesta='").append(estadoRespuesta).append('\'');
        sb.append(", mensajeRespuesta='").append(mensajeRespuesta).append('\'');
        sb.append(", agencia=").append(agencia).append('\'');
        sb.append(", tipoVenta='").append(tipoVenta).append('\'');
        sb.append(", puntoVenta='").append(puntoVenta).append('\'');
        sb.append(", nroPasajeros=").append(nroPasajeros);
        sb.append(", cantVuelos=").append(cantVuelos);
        sb.append(", mails='").append(mails).append('\'');
        sb.append(", vuelos='").append(vuelos).append('\'');
        sb.append(", pasajeros='").append(pasajeros).append('\'');
        sb.append(", tickets='").append(tickets).append('\'');
        sb.append(", fechaEmision='").append(fechaEmision).append('\'');
        sb.append(", userRechazo='").append(userRechazo).append('\'');
        sb.append(", fechaRechazo=").append(fechaRechazo).append('\'');
        sb.append(", sistema='").append(sistema).append('\'');
        sb.append('}');
        return sb.toString();
    }


    public EstadosBoa getEstadosBoa() {
        return estadosBoa;
    }

    public void setEstadosBoa(EstadosBoa estadosBoa) {
        this.estadosBoa = estadosBoa;
    }

    public String getEstadoDescripcion(){
        return EstadosBoa.valorEnum(estado)!=null?EstadosBoa.valorEnum(estado).getDescripcion():"Desconocido";
    }

    public String getImporteFormato(){
        return FormatosUtils.formatoImporte(importeTotal);
    }

    public String getMonedaDescripcion(){
        short moneda= Short.parseShort(Integer.toString(monedaImporteTotal));

        return Monedas.getDescripcionByBisaCorta(Short.valueOf(moneda));
    }

    public String getCuentaDebitoFormato(){
       if (cuentaDebito!=null) return FormatosUtils.formatoCuentaBisa(cuentaDebito.toPlainString());
       else return "-";
    }

    public String getMonedaCtaDebitoFormato(){
        short moneda= Short.parseShort(Integer.toString(monedaCtaDebito));

        return Monedas.getDescripcionByBisaCorta(Short.valueOf(moneda));
    }

    public String getFechaProcesoFormato(){
        return FormatosUtils.formatoFechaHora(fechaProceso);
    }

    public String getFechaCreacionFormato(){
        return FormatosUtils.formatoFechaHora(fechaCreacion);
    }

    public String getFechaModificacionFormato(){
        return FormatosUtils.formatoFechaHora(fechaModificacion);
    }

    public String getFechaRechazoFormato(){
        return FormatosUtils.formatoFechaHora(fechaRechazo);
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}


