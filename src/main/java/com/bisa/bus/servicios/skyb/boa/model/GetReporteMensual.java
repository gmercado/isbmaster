package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Para un objeto individual cuando el objeto JSON no es un vector de objetos
 * Created by rchura on 30-12-15.
 *
 */
public class GetReporteMensual {

    @JsonProperty(value = "ResultMonthlyReconciliation")
    private ReporteMensual reporteMensual;

    public ReporteMensual getReporteMensual() {
        return reporteMensual;
    }

    public void setReporteMensual(ReporteMensual reporteMensual) {
        this.reporteMensual = reporteMensual;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GetReporteMensual{");
        sb.append("reporteMensual=").append(reporteMensual);
        sb.append('}');
        return sb.toString();
    }
}
