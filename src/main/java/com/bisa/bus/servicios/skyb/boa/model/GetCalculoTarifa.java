package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 17-11-15.
 */
public class GetCalculoTarifa {

    @JsonProperty(value ="calculo_tarifa")
    private CalculoTarifa calculo;

    public CalculoTarifa getCalculo() {
        return calculo;
    }

    public void setCalculo(CalculoTarifa calculo) {
        this.calculo = calculo;
    }
}
