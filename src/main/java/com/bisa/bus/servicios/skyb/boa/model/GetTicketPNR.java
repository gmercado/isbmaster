package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 18-11-15.
 */
public class GetTicketPNR {

    @JsonProperty(value = "ResultGetTicketPNR")
    private TicketPNR ticketPNR;

    public TicketPNR getTicketPNR() {
        return ticketPNR;
    }

    public void setTicketPNR(TicketPNR ticketPNR) {
        this.ticketPNR = ticketPNR;
    }
}
