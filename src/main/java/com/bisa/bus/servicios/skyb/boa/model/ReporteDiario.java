package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Created by rchura on 30-12-15.
 */
public class ReporteDiario {

    public static final String PROCESADO = "1";

    /**
     * Entity, IP, RequestDateTime, IssueDateTime, PNR, IdentifierPNR, Authorization, TotalAmount, Currency
     * Entidad, IP, FechaHoraSolicitud, FechaHoraEmision, PNR, IdentificadorPNR, Autorizacion, ImporteTotal, Moneda
     *
     Entidad Entidad Financiera que realizo el cobro de los tickets aéreos
     IP Dirección IP desde la que se realizó el cobro de los tickets aéreos
     FechaHoraSolicitud Fecha/Hora en la que se recibió la autorización para la emisión de los tickets
     FechaHoraEmision Fecha/Hora en la que se realizó la emisión de los tickets
     PNR Código de Reserva
     IdentificadorPNR Identificador del Código de Reserva (Apellido)
     Autorizacion Código de Autorización enviado por la Entidad Financiera
     ImporteTotal Importe Total que debió cobrarse por la emisión de los tickets
     Moneda Moneda en la que debió cobrarse la emisión de los tickets
     Status estado de la transaccion (1, 0)
     */

    @JsonProperty(value = "Entity")
    private String banco;

    @JsonProperty(value = "IP")
    private String ip;

    @JsonProperty(value = "RequestDateTime")
    private String fechaAutorizacion;

    @JsonProperty(value = "IssueDateTime")
    private String fechaEmision;

    @JsonProperty(value = "PNR")
    private String pnr;

    @JsonProperty(value = "IdentifierPNR")
    private String identificador;

    @JsonProperty(value = "Authorization")
    private String codigoAutorizacion;

    @JsonProperty(value = "TotalAmount")
    private BigDecimal importeTotal;

    @JsonProperty(value = "Currency")
    private String moneda;

    @JsonProperty(value = "Status")
    private String estado;

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getFechaAutorizacion() {
        return fechaAutorizacion;
    }

    public void setFechaAutorizacion(String fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getCodigoAutorizacion() {
        return codigoAutorizacion;
    }

    public void setCodigoAutorizacion(String codigoAutorizacion) {
        this.codigoAutorizacion = codigoAutorizacion;
    }

    public BigDecimal getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(BigDecimal importeTotal) {
        this.importeTotal = importeTotal;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ReporteDiario{");
        sb.append("banco='").append(banco).append('\'');
        sb.append(", ip='").append(ip).append('\'');
        sb.append(", fechaAutorizacion='").append(fechaAutorizacion).append('\'');
        sb.append(", fechaEmision='").append(fechaEmision).append('\'');
        sb.append(", pnr='").append(pnr).append('\'');
        sb.append(", identificador='").append(identificador).append('\'');
        sb.append(", codigoAutorizacion='").append(codigoAutorizacion).append('\'');
        sb.append(", importeTotal=").append(importeTotal);
        sb.append(", moneda='").append(moneda).append('\'');
        sb.append(", estado='").append(estado).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
