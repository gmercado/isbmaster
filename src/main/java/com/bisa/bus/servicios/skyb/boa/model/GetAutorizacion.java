package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 17-11-15.
 */
public class GetAutorizacion {

    @JsonProperty(value = "ResultSetAuthorization")
    private Autorizacion  autorizacion;

    public Autorizacion getAutorizacion() {
        return autorizacion;
    }

    public void setAutorizacion(Autorizacion autorizacion) {
        this.autorizacion = autorizacion;
    }


}
