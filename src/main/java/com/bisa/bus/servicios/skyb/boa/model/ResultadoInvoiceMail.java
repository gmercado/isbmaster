package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by rchura on 10-11-15.
 */
public class ResultadoInvoiceMail {

    @JsonProperty(value = "GetInvoicePNREmailResult")
    private String respuesta;

    @JsonIgnore
    private Mensaje mensaje;

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ResultadoInvoiceMail{");
        sb.append("respuesta='").append(respuesta).append('\'');
        sb.append(", mensaje=").append(mensaje);
        sb.append('}');
        return sb.toString();
    }
}
