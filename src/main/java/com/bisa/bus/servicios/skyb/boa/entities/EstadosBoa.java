package com.bisa.bus.servicios.skyb.boa.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author rchura on 18-11-15
 */
public enum EstadosBoa {

    GENERADO(1, "Registro Generado"),
    DEBITADO(2, "Importe Debitado"),
    ENPROCESO(3, "Pago en Proceso"),
    PAGOAUTORIZADO(4, "Pago Autorizado"),
    REVERSANDO(5, "Revertir Debito"),
    ERRORREVERSA(6, "Error al Revertir"),
    SINRESPUESTA(7, "Sin Respuesta"),
    ERRORPAGO(8, "Error en Pago"),
    ERRORDEBITO(9, "Error en Debito"),
    RECHAZADO(10, "Rechazado"),
    AUTORIZACION(11, "Requiere Autorizacion"),
    PAGOCOMPLETADO(12, "Pago Completado"),
    PAGOREVERTIDO(13, "Pago Revertido");

    EstadosBoa(int i, String descripcion) {
        this.estado = i;
        this.descripcion = descripcion;
    }

    private int estado;
    private String descripcion;

    public int getEstado() {
        return estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    // Obtiene un objeto EstadosBoa a partir de un valor entero
    public static EstadosBoa valorEnum(int valor) {
        List<EstadosBoa> estados = new ArrayList<>(Arrays.asList((EstadosBoa.values())));
        for (EstadosBoa es : estados) {
            if (valor == es.getEstado()) {
                return es;
            }
        }
        return null;
    }
}