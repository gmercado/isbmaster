package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by rchura on 09-11-15.
 */
@XmlRootElement
public class EntradaConciliacionMensual extends Entrada{

    /**
     * language		X	AA {A->Z sin Ñ).
     * Idioma en el que se devuelven los mensajes al consumidor. Los valores aceptados son los siguientes:
     * ES: Español,      EN: Inglés
     * Si se envía un valor diferente de los anteriores, el sistema utilizará el idioma por defecto del usuario.
     */
    @XmlElement(name = "language")
    @JsonProperty(value = "language")
    private String lenguaje;

    // month (Entero con valor entre 1 y 12)
    @XmlElement(name = "month")
    @JsonProperty(value = "month")
    private Integer mes;

    // year (Entero con la gestion que desea conciliar. P.Ej. 2015)
    @XmlElement(name = "year")
    @JsonProperty(value = "year")
    private Integer anio;


    public EntradaConciliacionMensual() {
    }

    public EntradaConciliacionMensual(String credenciales, String ip, Boolean xmlOrJson, Localizador localizador, String lenguaje,
                                      Integer mes, Integer anio) {
        super(credenciales, ip, xmlOrJson, localizador);
        this.lenguaje = lenguaje;
        this.mes = mes;
        this.anio = anio;
    }

    public String getLenguaje() {
        return lenguaje;
    }

    public void setLenguaje(String lenguaje) {
        this.lenguaje = lenguaje;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("EntradaConciliacionMensual{");
        sb.append("lenguaje='").append(lenguaje).append('\'');
        sb.append(", mes=").append(mes);
        sb.append(", anio=").append(anio);
        sb.append('}');
        return sb.toString();
    }
}
