package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Created by rchura on 06-11-15.
 */
@JsonRootName(value = "pasajeroDR")
public class Pasajero {

    /**
     * posicion
     * Elemento “posicion” definido en el apartado 10.2.1.2.1.
     * Identifica de manera única la información del pasajero en el PNR.
     */
    @JsonProperty(value = "posicion")
    private Posicion posicion;

    /**
     * apdos_nombre
     * Apellidos/nombre,  donde apellidos y nombre son cadenas de caracteres alfabéticos (A->Z, excepto Ñ, incluyendo espacio en blanco para nombre).
     * Nombre y apellidos del pasajero.
     */
    @JsonProperty(value = "apdos_nombre")
    private String nombreApellidos;

    /**
     * tipo_19
     * “CHD”, “INF”.
     * – CHD (Child), si al realizar la reserva se envió tipo_19 =CNN o UNN.
     * – INF (Infant), si al realizar la reserva se envió tipo_19 =INF.
     * Para cualquier otro tipo de pasajero, el valor de este campo es nulo.
     */
    @JsonProperty(value = "tipo_19")
    private String tipo19;

    /**
     * pago
     * Elemento “pago”, definido en el apartado 10.2.1.3.1
     * Importe total del itinerario.
     */
    @JsonProperty(value = "pago")
    private Pago pago;


    /**
     * tkts
     * Listado de elementos “tkt”.	Listado de billetes del pasajero, en caso de haberse emitido el PNR.
     * – Cada elemento “tkt” es un número entero de 13 dígitos.
     */
    @JsonProperty(value = "Tkts")
    //private List<BigDecimal> tkts;
    private String tkts;




    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }

    public String getNombreApellidos() {
        return nombreApellidos;
    }

    public void setNombreApellidos(String nombreApellidos) {
        this.nombreApellidos = nombreApellidos;
    }

    public String getTipo19() {
        return tipo19;
    }

    public void setTipo19(String tipo19) {
        this.tipo19 = tipo19;
    }

    public Pago getPago() {
        return pago;
    }

    public void setPago(Pago pago) {
        this.pago = pago;
    }

    public String getTkts() {
        return tkts;
    }

    public void setTkts(String tkts) {
        this.tkts = tkts;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Pasajero{");
        sb.append("posicion=").append(posicion.toString());
        sb.append(", nombreApellidos='").append(nombreApellidos).append('\'');
        sb.append(", tipo19='").append(tipo19).append('\'');
        sb.append(", pago=").append(pago);
        sb.append(", tkts=").append(tkts);
        sb.append('}');
        return sb.toString();
    }
}
