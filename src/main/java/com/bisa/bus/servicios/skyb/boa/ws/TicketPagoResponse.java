package com.bisa.bus.servicios.skyb.boa.ws;

import com.bisa.bus.servicios.skyb.boa.model.Mensaje;
import com.bisa.bus.servicios.skyb.boa.model.TicketPNR;

/**
 * Created by rchura on 20-11-15.
 */
public class TicketPagoResponse {

    protected Long pagoID;

    private Mensaje mensaje;

    private TicketPNR ticketPNR;

    public Long getPagoID() {
        return pagoID;
    }

    public void setPagoID(Long pagoID) {
        this.pagoID = pagoID;
    }

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

    public TicketPNR getTicketPNR() {
        return ticketPNR;
    }

    public void setTicketPNR(TicketPNR ticketPNR) {
        this.ticketPNR = ticketPNR;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("VerificarPagoResponse{");
        sb.append("pagoID=").append(pagoID);
        sb.append(", mensaje=").append(mensaje);
        sb.append(", ticketPNR=").append(ticketPNR);
        sb.append('}');
        return sb.toString();
    }
}
