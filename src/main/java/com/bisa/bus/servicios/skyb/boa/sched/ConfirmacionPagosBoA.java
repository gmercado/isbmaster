package com.bisa.bus.servicios.skyb.boa.sched;

import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.api.TransaccionNoEjecutadaCorrectamenteException;
import com.bisa.bus.servicios.skyb.boa.api.ProcesosBoa;
import com.bisa.bus.servicios.skyb.boa.api.ServiciosBoa;
import com.bisa.bus.servicios.skyb.boa.dao.PagosBoaDao;
import com.bisa.bus.servicios.skyb.boa.dao.TransaccionMonitorBoa;
import com.bisa.bus.servicios.skyb.boa.entities.EstadosBoa;
import com.bisa.bus.servicios.skyb.boa.entities.PagosBoa;
import com.bisa.bus.servicios.skyb.boa.model.Mensaje;
import com.bisa.bus.servicios.skyb.boa.model.MessageType;
import com.bisa.bus.servicios.skyb.boa.model.TicketPNR;
import com.bisa.bus.servicios.skyb.boa.ws.*;
import com.google.inject.Inject;
import bus.consumoweb.yellowpepper.dao.YellowPepperServicioDao;
import bus.env.api.MedioAmbiente;
import bus.plumbing.components.EMailValidator;
import org.apache.commons.lang.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import static bus.env.api.Variables.*;
/**
 * Created by rchura on 04-11-15.
 */
public class ConfirmacionPagosBoA implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfirmacionPagosBoA.class);

    private final ServiciosBoa serviciosBoa;
    private final PagosBoaDao pagosBoaDao;
    private final YellowPepperServicioDao yellowPepperServicioDao;
    private final MedioAmbiente medioAmbiente;
    private final ProcesosBoa procesosBoa;

    @Inject
    public ConfirmacionPagosBoA(ServiciosBoa serviciosBoa, PagosBoaDao pagosBoaDao, YellowPepperServicioDao yellowPepperServicioDao, MedioAmbiente medioAmbiente, ProcesosBoa procesosBoa) {
        this.serviciosBoa = serviciosBoa;
        this.pagosBoaDao = pagosBoaDao;
        this.yellowPepperServicioDao = yellowPepperServicioDao;
        this.medioAmbiente = medioAmbiente;
        this.procesosBoa = procesosBoa;
    }


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        // Obtencion de pagos autorizados pendientes de completarse
        // SkybTech encola las autorizaciones, es necesario obtener los tickets para confirmar el pago
        int minutos = medioAmbiente.getValorIntDe(SKYB_BOA_EMISION_BOLETO_MINUTOS_ATRAS, SKYB_BOA_EMISION_BOLETO_MINUTOS_ATRAS_DEFAULT);
        List<PagosBoa> listaPagos =  pagosBoaDao.getPagoByEstadoFecha(EstadosBoa.PAGOAUTORIZADO, new Date(), minutos);
        if(listaPagos!=null && listaPagos.size()>0){

            LOGGER.info("Existen pagos de reserva Autorizados pendientes de ser emitidos por BoA. Se encontraron {} registros.", listaPagos.size());

            for(PagosBoa pago: listaPagos) {
                TicketPagoRequest request = null;
                TicketPagoResponse response = null;
                String textoMsj = null;

                request  = new TicketPagoRequest(pago.getNroCliente(), pago.getUserProceso(), pago.getNroPNR(), pago.getNombre(), pago.getIpOrigen(), pago.getSistema());

                response = getTickets(request);

                LOGGER.info("OBTENCION DE TICKETS: "+response.toString());
                if(response.getMensaje()!=null && MessageType.ERROR.equals(response.getMensaje().getTipo())) {
                //if(response==null || response.getTicketPNR()==null || (response.getMensaje()!=null && MessageType.ERROR.equals(response.getMensaje().getTipo()))) {
                    String msj = response.getMensaje()!=null?response.getMensaje().getMensaje()+".":"";
                    textoMsj = medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_ERROR_EMISION_TICKETS, SKYB_BOA_MENSAJE_ERROR_EMISION_TICKETS_DEFAULT);
                    textoMsj = MessageFormat.format(textoMsj, msj);
                    LOGGER.error("Error: No se puede obtener los Tickets emitidos por BoA para confirmar el Pago de la reserva: [{}]. " +
                            "Mensaje respuesta BoA:[{}]", request.toString(), response.getMensaje()!=null?response.getMensaje().getMensaje():"");
                    pago.setEstadoRespuesta("900");
                    pago.setMensajeRespuesta(StringUtils.substring(textoMsj,0,200));
                    pago = pagosBoaDao.actualizar(pago, request.getUsuario());

                    // Mensaje de error en la respuesta de BoA
                    if (response.getMensaje()!=null && MessageType.ERROR.equals(response.getMensaje().getTipo())){
                        //Solo revertir en caso de ERROR
                        //Revertir el debito de la cuenta del cliente
                        revertirPagoBoa(pago, request.getUsuario());
                    }
                    continue;
                }

                // Obtiene el par Titular - CantidadTicket
                TicketPNR ticket = response.getTicketPNR();
                String titularTicket = ticket.getTitularesTickets();
                if(titularTicket==null){
                    textoMsj = response.getMensaje()!=null?response.getMensaje().getMensaje()+".":"";
                    textoMsj = textoMsj +" No se puede obtener los Tickets emitidos para cada Titular.";
                    LOGGER.error("Error: No se puede obtener los Tickets emitidos para cada Titular de la reserva: [{}]. " +
                            "Mensaje respuesta BoA:[{}]", request.toString(), response.getMensaje()!=null?response.getMensaje().getMensaje():"");
                    pago.setEstadoRespuesta("901");
                    pago.setMensajeRespuesta(StringUtils.substring(textoMsj,0,200));
                    pago = pagosBoaDao.actualizar(pago, request.getUsuario());
                    continue;
                }

                // Actualizacion de datos del pago
                pago.setTickets(StringUtils.substring(titularTicket,0,500));
                pago.setFechaEmision(new Date());
                pago.setEstado(EstadosBoa.PAGOCOMPLETADO.getEstado());
                pago.setEstadoRespuesta("");
                pago.setMensajeRespuesta(medioAmbiente.getValorDe(SKYB_BOA_MENSAJE_PAGO_COMPLETADO, SKYB_BOA_MENSAJE_PAGO_COMPLETADO_DEFAULT));

                pago = pagosBoaDao.actualizar(pago, request.getUsuario());


                // Confirmar la emisión de los tickets al cliente Bisa que realizó el pago
                // Consumir servicio a YellowPepper
                // Solo si la variable de medio amebiente es true
                String enviarSms = medioAmbiente.getValorDe(SKYB_BOA_SMSCONFIRMACION, SKYB_BOA_SMSCONFIRMACION_DEFAULT);
                if("true".equalsIgnoreCase(StringUtils.trimToEmpty(enviarSms))){
                    String mensaje = null;
                    String titulo = null;
                    if(ticket.getTitulares().getTitular().size() > 1){
                        // Varios tickets emitidos
                        mensaje = medioAmbiente.getValorDe(SKYB_BOA_NOTIFICACION_PAGO_GRUPO, SKYB_BOA_NOTIFICACION_PAGO_GRUPO_DEFAULT);
                        titulo = "SKYB_BOA_NOTIFICACION_PAGO_GRUPO";

                    }else if(ticket.getTitulares().getTitular().size()==1){
                        // Un solo CantidadTicket emitido
                        mensaje = medioAmbiente.getValorDe(SKYB_BOA_NOTIFICACION_PAGO_SIMPLE, SKYB_BOA_NOTIFICACION_PAGO_SIMPLE_DEFAULT);
                        titulo = "SKYB_BOA_NOTIFICACION_PAGO_SIMPLE";
                    }

                    String celular = StringUtils.trimToNull(pagosBoaDao.getNroCelularCliente(pago.getNroCliente()));

                    if(StringUtils.trimToNull(celular)!=null && StringUtils.trimToNull(mensaje)!=null){

                        LOGGER.debug("\nMENSAJE A ENVIAR POR YELLOWPEPPER: Celular [{}], Mensaje [{}]\n", celular, mensaje);

                        yellowPepperServicioDao.sendNotificacionYellowPepperBoA(celular, titulo, mensaje, request.getCliente(), request.getUsuario(), request.getSistema());

                    }else{
                        LOGGER.error("No se puede enviar notifiacion de emision de boleto(s) al cliente [{}], debido a que no se tiene registro de su celular.", pago.getNroCliente());
                    }
                }

                // Envio de los tickets por e-mail, siempre que el cliente haya registrado este dato.
                String email = StringUtils.trimToNull(pago.getMails());
                if(email!=null && email.length()>5){
                    //Validacion del mail
                    if(EMailValidator.isValid(pago.getMails())){
                        BilleteMailRequest mailRequest = new BilleteMailRequest(pago.getNroCliente(), pago.getUserProceso(), pago.getNroPNR(), pago.getNombre(), pago.getIpOrigen(), pago.getSistema());
                        mailRequest.setMails(email);
                        mailRequest.setPagoID(pago.getId());

                        BilleteMailResponse mailResponse = getInvoiceMail(mailRequest);
                        if(mailResponse==null){
                            LOGGER.error("No se puede enviar los billetes de la Reserva al correo electronico [{}] asociado al codigo de reserva PNR [{}].", email, pago.getNroPNR());
                        }else{
                            if(mailResponse.getMensaje()!=null){
                                if(MessageType.ERROR.equals(mailResponse.getMensaje().getTipo())){
                                    LOGGER.error("Error al enviar los billetes por e-mail. "+mailResponse.getMensaje().getMensaje());
                                }else {
                                    LOGGER.info("Los billetes se enviaron por e-mail. "+mailResponse.getMensaje().getMensaje());
                                }
                            }
                        }
                    }
                }
                // Fin envio de e-mail

            }

        }else{

            LOGGER.debug("No existen pagos de reserva Autorizados pendientes de ser emitidos por BoA.");
        }

    }


    private TicketPagoResponse getTickets(TicketPagoRequest request) {
        TicketPagoResponse response = null;
        if(serviciosBoa==null){
            LOGGER.debug(" OBJETO <serviciosBoa> ES NULO---> "+request.toString());
            response = new TicketPagoResponse();
        }
        else{
            LOGGER.debug(" OBJETO <serviciosBoa> EXISTE---> "+request.toString());
            //ServiciosBoa serviciosBoa = new ServiciosBoa();
            response = serviciosBoa.obtenerTicket(request);
        }
        return response;

    }


    private BilleteMailResponse getInvoiceMail(BilleteMailRequest request) {
        BilleteMailResponse response = null;

        if(serviciosBoa==null){
            LOGGER.debug(" OBJETO <serviciosBoa> ES NULO---> "+request.toString());
            response = new BilleteMailResponse();
        }
        else{
            LOGGER.debug(" OBJETO <serviciosBoa> EXISTE---> "+request.toString());
            //ServiciosBoa serviciosBoa = new ServiciosBoa();
            response = serviciosBoa.enviarBilleteMail(request);
        }
        return response;

    }


    private void revertirPagoBoa(PagosBoa pago, String usuario ){

        LOGGER.info("Efectuando la reversa del debito en la cuenta del cliente.");

        PagosBoa pagosBoa;
        String resp= "";

        pago.setEstado(EstadosBoa.REVERSANDO.getEstado());
        pago = pagosBoaDao.actualizar(pago, usuario);

        //Revertir debito
        pagosBoa =  procesosBoa.ejecutarReversa(pago, usuario);
        if(pagosBoa==null){
            resp = "Ocurri\u00f3 un error inesperado al revertir el d\u00e9bito. Por favor comun\u00edquese con Bisa Responde.";

            pagosBoa.setEstado(EstadosBoa.ERRORREVERSA.getEstado());
            pagosBoa.setEstadoRespuesta("902");
            pagosBoa.setMensajeRespuesta(StringUtils.substring(resp,0,200));

        }else{
            LOGGER.debug("La reversa del debito fue efectuada correctamente.");
            pagosBoa.setEstado(EstadosBoa.PAGOREVERTIDO.getEstado());
        }

        pagosBoa = pagosBoaDao.actualizar(pagosBoa, usuario);

        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            LOGGER.error("Error: Nos han interrumpido en plena espera.");
        }
    }


}
