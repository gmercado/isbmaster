package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Created by rchura on 06-11-15.
 */

@JsonRootName(value = "osi")
public class OSI {

    /**
     * posicion
     * Elemento “posicion” definido en el apartado 10.2.1.2.1
     * Identifica de manera única la información del pasajero en el PNR.
     */
    @JsonProperty(value = "estado")
    private Posicion posicion;


    /**
     * linea
     * CC (A->Z, excepto Ñ, y 0->9).	Código de la compañía del usuario.
     */
    @JsonProperty(value = "linea")
    private String linea;

    /**
     * texto
     * Cadena de caracteres alfanuméricos.	Texto que contiene la información relacionada con el OSI.
     */
    @JsonProperty(value = "texto")
    private String texto;


    /**
     * num_pax
     * Numérico.	Numero de pasajero al que va asociado el OSI.
     */
    @JsonProperty(value = "num_pax")
    private Long nroPasajero;


    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Long getNroPasajero() {
        return nroPasajero;
    }

    public void setNroPasajero(Long nroPasajero) {
        this.nroPasajero = nroPasajero;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("OSI{");
        sb.append("posicion=").append(posicion);
        sb.append(", linea='").append(linea).append('\'');
        sb.append(", texto='").append(texto).append('\'');
        sb.append(", nroPasajero=").append(nroPasajero);
        sb.append('}');
        return sb.toString();
    }
}
