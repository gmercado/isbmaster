package com.bisa.bus.servicios.skyb.boa.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Created by rchura on 06-11-15.
 */
@JsonRootName(value = "ssr")
public class SSR {

    /**
     * posicion
     * Elemento “posicion” definido en el apartado 10.2.1.2.1
     * Identifica de manera única la información del pasajero en el PNR.
     */
    @JsonProperty(value = "posicion")
    private Posicion posicion;

    /**
     * tipo
     * AAAA (A->Z excepto Ñ).	Código correspondiente al tipo de SSR.
     */
    @JsonProperty(value = "tipo")
    private String tipo;

    /**
     * linea
     * CC (A->Z, excepto Ñ, y 0->9).	Código de la compañía del usuario.
     */
    @JsonProperty(value = "linea")
    private String linea;

    /**
     * estado
     * AA (A-> Z, excepto Ñ).	Estado en el que se encuentra el SSR.
     * Entre los valores posibles del código de estado se encuentran:
     – NN: En petición.
     – HK: Confirmado.
     – XX: Cancelado.
     – UN: No disponible.
     Para una documentación más exhaustiva de los códigos de estado, véase referencia AIRIMP.*
     */
    @JsonProperty(value = "estado")
    private String estado;

    /**
     * texto
     * Cadena de caracteres alfanuméricos.	Texto que contiene la información relacionada con el SSR.
     */
    @JsonProperty(value = "texto")
    private String texto;

    /**
     * num_sgto
     * Numérico.
     * Numero de segmento al que va asociado el SSR.
     */
    @JsonProperty(value = "num_sgto")
    private Long nroSegmento;

    /**
     * num_pax
     * Numérico.	Numero de pasajero al que va asociado el SSR.
     */
    @JsonProperty(value = "num_pax")
    private Long nroPasajero;

    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Long getNroSegmento() {
        return nroSegmento;
    }

    public void setNroSegmento(Long nroSegmento) {
        this.nroSegmento = nroSegmento;
    }

    public Long getNroPasajero() {
        return nroPasajero;
    }

    public void setNroPasajero(Long nroPasajero) {
        this.nroPasajero = nroPasajero;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SSR{");
        sb.append("posicion=").append(posicion);
        sb.append(", tipo='").append(tipo).append('\'');
        sb.append(", linea='").append(linea).append('\'');
        sb.append(", estado='").append(estado).append('\'');
        sb.append(", texto='").append(texto).append('\'');
        sb.append(", nroSegmento=").append(nroSegmento);
        sb.append(", nroPasajero=").append(nroPasajero);
        sb.append('}');
        return sb.toString();
    }
}
