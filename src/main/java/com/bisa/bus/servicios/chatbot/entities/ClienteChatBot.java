package com.bisa.bus.servicios.chatbot.entities;

import bus.database.model.EntityBase;
import com.bisa.bus.servicios.chatbot.model.EstadoChatBot;

import javax.persistence.*;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
@Entity
@Table(name = "ICBOT100")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I100FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I100USRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I100FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I100USRMOD"))
})
public class ClienteChatBot extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I100CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "I100CODCLI", columnDefinition = "VARCHAR(10)")
    private String codigoCliente;

    @Column(name = "I100CODGEN", columnDefinition = "VARCHAR(64)")
    private String codigoClienteBisa;

    @Enumerated(EnumType.STRING)
    @Column(name = "I100ESTCLI", columnDefinition = "VARCHAR(3)")
    private EstadoChatBot estadoCliente;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getCodigoClienteBisa() {
        return codigoClienteBisa;
    }

    public void setCodigoClienteBisa(String codigoClienteBisa) {
        this.codigoClienteBisa = codigoClienteBisa;
    }

    public EstadoChatBot getEstadoCliente() {
        return estadoCliente;
    }

    public void setEstadoCliente(EstadoChatBot estadoCliente) {
        this.estadoCliente = estadoCliente;
    }
}
