package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 27/09/2017.
 */
public class JsonRequestBloqueaTD {
    private String codCliente;
    private String codTarjeta;
    private String tipo;
    private String motivo;

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getCodTarjeta() {
        return codTarjeta;
    }

    public void setCodTarjeta(String codTarjeta) {
        this.codTarjeta = codTarjeta;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "{" +
                "codCliente='" + codCliente + '\'' +
                ", codTarjeta='" + codTarjeta + '\'' +
                ", tipo='" + tipo + '\'' +
                ", motivo='" + motivo + '\'' +
                '}';
    }
}
