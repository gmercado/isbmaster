package com.bisa.bus.servicios.chatbot.sched;

import com.bisa.bus.servicios.chatbot.dao.SolicitudChatBotDao;
import com.bisa.bus.servicios.chatbot.entities.SolicitudChatBot;
import com.bisa.bus.servicios.chatbot.model.EstadoChatBot;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * @author by rsalvatierra on 15/12/2017.
 */
public class SchedOperacionesChatBot implements Job {

    private static final String USUARIO = "CBUSRJOB";
    private static final Logger LOGGER = LoggerFactory.getLogger(SchedOperacionesChatBot.class);
    private final SolicitudChatBotDao solicitudChatBotDao;

    @Inject
    public SchedOperacionesChatBot(SolicitudChatBotDao solicitudChatBotDao) {
        this.solicitudChatBotDao = solicitudChatBotDao;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Date fecTiempoVida;
        Date fecActual = new Date();
        //Recuperar registros en estado ACTIVO.
        List<SolicitudChatBot> solicitudChatBots = solicitudChatBotDao.getNoVigentes();
        if (solicitudChatBots != null && solicitudChatBots.size() > 0) {
            for (SolicitudChatBot solicitud : solicitudChatBots) {
                if (solicitud.getTiempoVida() != null) {
                    fecTiempoVida = solicitud.getTiempoVida();
                    LOGGER.debug("Tiempo de Vida {}, Fecha Actual {}", fecTiempoVida, fecActual);
                    if (fecActual.after(fecTiempoVida)) {
                        LOGGER.info("SchedOperacionesChatBot  Actualizando >>> {}", solicitud.getId());
                        solicitud.setEstadoSolicitud(EstadoChatBot.INA);
                        solicitudChatBotDao.actualizar(solicitud, USUARIO);
                    }
                }
            }
        } else {
            LOGGER.debug("CB: No existen solicitudes pendientes.");
        }
    }
}
