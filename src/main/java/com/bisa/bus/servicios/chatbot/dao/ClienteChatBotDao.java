package com.bisa.bus.servicios.chatbot.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.chatbot.entities.ClienteChatBot;
import com.bisa.bus.servicios.chatbot.model.EstadoChatBot;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.NoResultException;
import org.apache.openjpa.persistence.OpenJPAQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.LinkedList;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class ClienteChatBotDao extends DaoImpl<ClienteChatBot, Long> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClienteChatBotDao.class);
    private static final String CONTRATO_CHAT_BOT = "CBOT";
    private static final String ESTADO_ACTIVO = "A";


    @Inject
    protected ClienteChatBotDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public ClienteChatBot registrar(ClienteChatBot clienteChatBot, String usuario) {
        clienteChatBot.setFechaCreacion(new Date());
        clienteChatBot.setUsuarioCreador(usuario);
        clienteChatBot.setEstadoCliente(EstadoChatBot.ACT);
        return persist(clienteChatBot);
    }

    public ClienteChatBot actualizar(ClienteChatBot clienteChatBot, String usuario) {
        clienteChatBot.setFechaModificacion(new Date());
        clienteChatBot.setUsuarioModificador(usuario);
        return merge(clienteChatBot);
    }


    /**
     * Retorna Cliente Chat Bot
     *
     * @param pCodigoCliente Codigo cliente
     * @return ClienteJoven
     */
    public ClienteChatBot getByCodigoCliente(String pCodigoCliente) {
        LOGGER.debug("Obteniendo registro por Codigo Cliente:{}.", pCodigoCliente);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<ClienteChatBot> q = cb.createQuery(ClienteChatBot.class);
                        Root<ClienteChatBot> p = q.from(ClienteChatBot.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("codigoCliente"), StringUtils.trimToEmpty(pCodigoCliente)));
                        predicatesAnd.add(cb.equal(p.get("estadoCliente"), EstadoChatBot.ACT));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<ClienteChatBot> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    /**
     * Retorna Cliente ChatBot
     *
     * @param pCodigoClienteEncriptado Codigo cliente Cifrado
     * @return ClienteJoven
     */
    public ClienteChatBot getByCodigoCifrado(String pCodigoClienteEncriptado) {
        LOGGER.debug("Obteniendo registro por Codigo Cliente Cifrado:{}.", pCodigoClienteEncriptado);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<ClienteChatBot> q = cb.createQuery(ClienteChatBot.class);
                        Root<ClienteChatBot> p = q.from(ClienteChatBot.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("codigoClienteBisa"), StringUtils.trimToEmpty(pCodigoClienteEncriptado)));
                        predicatesAnd.add(cb.equal(p.get("estadoCliente"), EstadoChatBot.ACT));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<ClienteChatBot> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    /**
     * Verificar si tiene contrato para ChatBot
     *
     * @param numeroCliente codigo de cliente
     * @return String
     */
    @SuppressWarnings({"unchecked"})
    public Integer existeContrato(final String numeroCliente) {
        if (StringUtils.trimToNull(numeroCliente) == null) {
            LOGGER.error("Error: Debe especificar un numero de cliente.");
            return null;
        }
        return doWithTransaction((entityManager, t) -> {

            String cliente = StringUtils.leftPad(StringUtils.trimToEmpty(numeroCliente), 10, '0');

            String sql = "SELECT c.Q510codigo FROM AQP510 c " +
                    "WHERE c.q510nrocli = ? " +
                    "AND c.Q510tipser = ? " +
                    "AND c.q510estado = ? ";

            OpenJPAQuery<Integer> consultaCuenta =
                    entityManager.createNativeQuery(sql, Integer.class);

            consultaCuenta.setParameter(1, cliente);
            consultaCuenta.setParameter(2, CONTRATO_CHAT_BOT);
            consultaCuenta.setParameter(3, ESTADO_ACTIVO);

            try {
                return consultaCuenta.getSingleResult();
            } catch (NoResultException e) {
                LOGGER.error("AS400 <NoResultException>: No se encontraron registro de cuentas para el cliente [" + cliente + "] en <getCuentaCliente>. ", e);
            } catch (Exception e) {
                LOGGER.error("AS400 <Exception>: Error al ejecutar la consulta en <getCuentaCliente> para el cliente [" + cliente + "]. ", e);
            }
            return null;
        });
    }
}
