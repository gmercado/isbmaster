package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 15/09/2017.
 */
public class JsonRequestPersonasSucursal extends JsonRequestComun {
    private String nombreSucursal;

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    @Override
    public String toString() {
        return "{" +
                "nombreSucursal='" + nombreSucursal + '\'' +
                "} " + super.toString();
    }
}
