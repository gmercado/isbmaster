package com.bisa.bus.servicios.chatbot.services.types;

import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 23/10/2017.
 */
public class JsonRequestTransaccion {
    private String codCliente;
    private String codCuentaOrigen;
    private BigDecimal monto;
    private String moneda;

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getCodCuentaOrigen() {
        return codCuentaOrigen;
    }

    public void setCodCuentaOrigen(String codCuentaOrigen) {
        this.codCuentaOrigen = codCuentaOrigen;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    @Override
    public String toString() {
        return "{" +
                "codCliente='" + codCliente + '\'' +
                ", codCuentaOrigen='" + codCuentaOrigen + '\'' +
                ", monto=" + monto +
                ", moneda='" + moneda + '\'' +
                '}';
    }
}
