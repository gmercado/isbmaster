package com.bisa.bus.servicios.chatbot.services;

import com.bisa.bus.servicios.chatbot.services.types.*;

/**
 * @author by rsalvatierra on 06/09/2017.
 */
public interface GeneralesChatBotService {
    JsonResponseCliente adicionarCliente(JsonRequestAddCliente requestAddCliente, String usuario);

    JsonResponseSolToken solicitarToken(JsonRequestSolToken requestSolToken, String usuario);

    JsonResponseComun validarToken(JsonRequestConfToken requestConfToken, String usuario);

    JsonResponseTarjetasDebito consultarTarjetasDebito(JsonRequestTarjetasDebito requestTarjetasDebito, String usuario);

    JsonResponseComun bloqueTarjetaDebito(JsonRequestBloqueaTD requestBloqueaTD, String usuario);

    JsonResponseComun habilitaTarjetaDebitoExterior(JsonRequestHabTarjetaExterior requestHabTarjetaExterior, String usuario);

    JsonResponseTarjetaCredito consultarTarjetaCredito(JsonRequestTarjetaCredito requestTarjetaCredito, String usuario);

    JsonResponseBeneficiarios consultaBeneficiarios(JsonRequestBeneficiario requestBeneficiario, String usuario);
}
