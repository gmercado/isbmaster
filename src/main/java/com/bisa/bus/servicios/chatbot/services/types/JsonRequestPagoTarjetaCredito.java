package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 23/10/2017.
 */
public class JsonRequestPagoTarjetaCredito extends JsonRequestTransaccion {
    private String codTarjetaCredito;
    private int tipo;
    private String origenFondo;
    private String destinoFondo;

    public String getOrigenFondo() {
        return origenFondo;
    }

    public void setOrigenFondo(String origenFondo) {
        this.origenFondo = origenFondo;
    }

    public String getDestinoFondo() {
        return destinoFondo;
    }

    public void setDestinoFondo(String destinoFondo) {
        this.destinoFondo = destinoFondo;
    }

    public String getCodTarjetaCredito() {
        return codTarjetaCredito;
    }

    public void setCodTarjetaCredito(String codTarjetaCredito) {
        this.codTarjetaCredito = codTarjetaCredito;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "{" +
                "codTarjetaCredito='" + codTarjetaCredito + '\'' +
                ", tipo=" + tipo +
                "} " + super.toString();
    }
}
