package com.bisa.bus.servicios.chatbot.model;

import bus.interfaces.tipos.TipoDepartamento;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author by rsalvatierra on 01/09/2017.
 */
public class DepartamentoChatBot {

    private String codDepartamento;
    private String descripcion;

    private DepartamentoChatBot(TipoDepartamento tipoDepartamentoDao) {
        setCodDepartamento(tipoDepartamentoDao.getCodigo());
        setDescripcion(tipoDepartamentoDao.getDescripcion());
    }

    public static List<DepartamentoChatBot> getTipos() {
        return Lists.newArrayList(TipoDepartamento.values()).stream().map(DepartamentoChatBot::new).collect(Collectors.toList());
    }

    public String getCodDepartamento() {
        return codDepartamento;
    }

    private void setCodDepartamento(String codDepartamento) {
        this.codDepartamento = codDepartamento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "{" +
                "codDepartamento='" + codDepartamento + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
