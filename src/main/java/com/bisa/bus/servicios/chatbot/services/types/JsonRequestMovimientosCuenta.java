package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 27/09/2017.
 */
public class JsonRequestMovimientosCuenta {
    private String codCliente;
    private String codCuenta;
    private String tipo;

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getCodCuenta() {
        return codCuenta;
    }

    public void setCodCuenta(String codCuenta) {
        this.codCuenta = codCuenta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "{" +
                "codCliente='" + codCliente + '\'' +
                ", codCuenta='" + codCuenta + '\'' +
                ", tipo='" + tipo + '\'' +
                '}';
    }
}
