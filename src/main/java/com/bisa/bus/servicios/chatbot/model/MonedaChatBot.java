package com.bisa.bus.servicios.chatbot.model;

import bus.plumbing.utils.Monedas;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class MonedaChatBot {
    private String codMoneda;
    private String descripcion;

    private MonedaChatBot(String codMoneda) {
        setCodMoneda(codMoneda);
        setDescripcion(Monedas.getBisaMonedasByISO(codMoneda));
    }

    public static List<MonedaChatBot> getMonedas() {
        List<MonedaChatBot> ar = new ArrayList<>();
        ar.add(new MonedaChatBot(Monedas.ISO_BOLIVIANOS));
        ar.add(new MonedaChatBot(Monedas.ISO_DOLARES));
        return ar;
    }

    public String getCodMoneda() {
        return codMoneda;
    }

    private void setCodMoneda(String codMoneda) {
        this.codMoneda = codMoneda;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "{" +
                "codMoneda='" + codMoneda + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
