package com.bisa.bus.servicios.chatbot.model;

import bus.interfaces.tipos.TipoCotizacion;
import bus.plumbing.utils.Monedas;

import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 05/09/2017.
 */
public class CotizacionChatBot {
    private String moneda;
    private String tipo;
    private BigDecimal valor;
    private String simbolo;

    public CotizacionChatBot(short moneda, TipoCotizacion tipo, BigDecimal valor) {
        this.moneda = Monedas.getDescripcionByBisa(moneda);
        this.tipo = tipo.name();
        this.valor = valor;
        this.simbolo = Monedas.getDescripcionHumanaByBisa(moneda);
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }

    @Override
    public String toString() {
        return "{" +
                "moneda='" + moneda + '\'' +
                ", tipo='" + tipo + '\'' +
                ", valor=" + valor +
                ", simbolo='" + simbolo + '\'' +
                '}';
    }
}
