package com.bisa.bus.servicios.chatbot.model;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public enum EstadoChatBot {
    ACT("ACTIVO"),
    PRC("PROCESADO"),
    ERR("ERROR"),
    INA("INACTIVO");

    private String descripcion;

    EstadoChatBot(String descripcion) {
        setDescripcion(descripcion);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
