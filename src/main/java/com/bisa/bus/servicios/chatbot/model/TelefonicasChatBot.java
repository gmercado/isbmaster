package com.bisa.bus.servicios.chatbot.model;

import bus.consumoweb.pagoservicios.model.Servicio;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class TelefonicasChatBot {
    private String codTelefonica;
    private String descripcion;

    public TelefonicasChatBot(Servicio servicio) {
        setCodTelefonica(servicio.getEmpresaCodigo() + "-" + servicio.getCodigo());
        setDescripcion(servicio.getEmpresaNombre());
    }


    public String getCodTelefonica() {
        return codTelefonica;
    }

    public void setCodTelefonica(String codTelefonica) {
        this.codTelefonica = codTelefonica;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "{" +
                "codTelefonica='" + codTelefonica + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
