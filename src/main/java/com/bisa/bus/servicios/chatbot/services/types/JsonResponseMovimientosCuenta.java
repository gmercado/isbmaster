package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.MovimientosChatBot;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;

import java.util.List;

/**
 * @author by rsalvatierra on 27/09/2017.
 */
public class JsonResponseMovimientosCuenta extends JsonResponseComun {
    private List<MovimientosChatBot> extracto;

    public JsonResponseMovimientosCuenta(ResponseProcess responseProcess) {
        super(responseProcess);
    }


    public List<MovimientosChatBot> getExtracto() {
        return extracto;
    }

    public void setExtracto(List<MovimientosChatBot> extracto) {
        this.extracto = extracto;
    }
}
