package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 23/10/2017.
 */
public class JsonRequestTransferenciaPropia extends JsonRequestTransaccion {
    private String codCuentaDestino;
    private String origenFondo;
    private String destinoFondo;

    public String getOrigenFondo() {
        return origenFondo;
    }

    public void setOrigenFondo(String origenFondo) {
        this.origenFondo = origenFondo;
    }

    public String getDestinoFondo() {
        return destinoFondo;
    }

    public void setDestinoFondo(String destinoFondo) {
        this.destinoFondo = destinoFondo;
    }

    public String getCodCuentaDestino() {
        return codCuentaDestino;
    }

    public void setCodCuentaDestino(String codCuentaDestino) {
        this.codCuentaDestino = codCuentaDestino;
    }

    @Override
    public String toString() {
        return "{" +
                "codCuentaDestino='" + codCuentaDestino + '\'' +
                "} " + super.toString();
    }
}
