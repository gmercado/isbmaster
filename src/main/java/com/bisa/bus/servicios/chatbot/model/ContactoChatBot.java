package com.bisa.bus.servicios.chatbot.model;

import bus.interfaces.sqlite.entities.Contacto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by rsalvatierra on 05/09/2017.
 */
public class ContactoChatBot {
    private String descripcion;
    private String referencia;

    public ContactoChatBot(Contacto contacto) {
        if (contacto != null) {
            setDescripcion(contacto.getReferencia());
            setReferencia(contacto.getTelefono());
        }
    }

    public static List<ContactoChatBot> getTipos(List<Contacto> contactos) {
        List<ContactoChatBot> contactoChatBots = null;
        if (contactos != null) {
            contactoChatBots = new ArrayList<>();
            for (Contacto contacto : contactos) {
                contactoChatBots.add(new ContactoChatBot(contacto));
            }
        }
        return contactoChatBots;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @Override
    public String toString() {
        return "{" +
                "descripcion='" + descripcion + '\'' +
                ", referencia='" + referencia + '\'' +
                '}';
    }
}
