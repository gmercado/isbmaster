package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.DepartamentoChatBot;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;

import java.util.List;

/**
 * @author by rsalvatierra on 01/09/2017.
 */
public class JsonResponseDepartamentos extends JsonResponseComun {
    private List<DepartamentoChatBot> departamentos;

    public JsonResponseDepartamentos(ResponseProcess responseProcess) {
        if (responseProcess != null) {
            setCodRespuesta(responseProcess.getCodRespuesta());
            setMensaje(responseProcess.getMensaje());
        }
    }

    public List<DepartamentoChatBot> getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(List<DepartamentoChatBot> departamentos) {
        this.departamentos = departamentos;
    }

    @Override
    public String toString() {
        return "{" +
                "departamentos=" + departamentos +
                '}' + super.toString();
    }
}
