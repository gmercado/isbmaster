package com.bisa.bus.servicios.chatbot.model;

/**
 * @author by rsalvatierra on 26/09/2017.
 */
public enum TipoEstadoClaveChatBot {
    ACT("Activo"),
    DES("Inactivo");

    private String descripcion;

    TipoEstadoClaveChatBot(String descripcion) {
        setDescripcion(descripcion);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
