package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 29/09/2017.
 */
public class JsonRequestTarjetaCredito {
    private String codCliente;
    private String codTarjeta;

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getCodTarjeta() {
        return codTarjeta;
    }

    public void setCodTarjeta(String codTarjeta) {
        this.codTarjeta = codTarjeta;
    }

    @Override
    public String toString() {
        return "{" +
                "codCliente='" + codCliente + '\'' +
                ", codTarjeta='" + codTarjeta + '\'' +
                '}';
    }
}
