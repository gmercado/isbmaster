package com.bisa.bus.servicios.chatbot.entities;

import bus.database.model.EntityBase;
import com.bisa.bus.servicios.chatbot.model.EstadoChatBot;
import com.bisa.bus.servicios.chatbot.model.ProcesoChatBot;
import com.bisa.bus.servicios.chatbot.model.TipoProcesoChatBot;

import javax.persistence.*;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
@Entity
@Table(name = "ICBOT101")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I101FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I101USRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I101FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I101USRMOD"))
})
public class OperacionChatBot extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I101CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "I101I104UC")
    private UsuarioConectadoChatBot usuarioConectadoChatBot;

    @Column(name = "I101TRANID", columnDefinition = "VARCHAR(40)")
    private String codTransaccion;

    @Enumerated(EnumType.STRING)
    @Column(name = "I101TIPROC", columnDefinition = "VARCHAR(3)")
    private TipoProcesoChatBot tipoProcesoChatBot;

    @Enumerated(EnumType.STRING)
    @Column(name = "I101PROCES", columnDefinition = "VARCHAR(20)")
    private ProcesoChatBot procesoChatBot;

    @Enumerated(EnumType.STRING)
    @Column(name = "I101ESTOPE", columnDefinition = "VARCHAR(3)")
    private EstadoChatBot estado;

    @Column(name = "I101CDRESP", columnDefinition = "NUMERIC(5)")
    private int codRespuesta;

    @Column(name = "I101MENSAJ", columnDefinition = "VARCHAR(255)")
    private String mensaje;

    @Column(name = "I101CRIBUS", columnDefinition = "CLOB")
    private String criterioBusqueda;

    @Column(name = "I101RESBUS", columnDefinition = "CLOB")
    private String resultadoBusqueda;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodTransaccion() {
        return codTransaccion;
    }

    public void setCodTransaccion(String codTransaccion) {
        this.codTransaccion = codTransaccion;
    }

    public TipoProcesoChatBot getTipoProcesoChatBot() {
        return tipoProcesoChatBot;
    }

    public void setTipoProcesoChatBot(TipoProcesoChatBot tipoProcesoChatBot) {
        this.tipoProcesoChatBot = tipoProcesoChatBot;
    }

    public ProcesoChatBot getProcesoChatBot() {
        return procesoChatBot;
    }

    public void setProcesoChatBot(ProcesoChatBot procesoChatBot) {
        this.procesoChatBot = procesoChatBot;
    }

    public EstadoChatBot getEstado() {
        return estado;
    }

    public void setEstado(EstadoChatBot estado) {
        this.estado = estado;
    }

    public int getCodRespuesta() {
        return codRespuesta;
    }

    public void setCodRespuesta(int codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getCriterioBusqueda() {
        return criterioBusqueda;
    }

    public void setCriterioBusqueda(String criterioBusqueda) {
        this.criterioBusqueda = criterioBusqueda;
    }

    public String getResultadoBusqueda() {
        return resultadoBusqueda;
    }

    public void setResultadoBusqueda(String resultadoBusqueda) {
        this.resultadoBusqueda = resultadoBusqueda;
    }

    public UsuarioConectadoChatBot getUsuarioConectadoChatBot() {
        return usuarioConectadoChatBot;
    }

    public void setUsuarioConectadoChatBot(UsuarioConectadoChatBot usuarioConectadoChatBot) {
        this.usuarioConectadoChatBot = usuarioConectadoChatBot;
    }
}
