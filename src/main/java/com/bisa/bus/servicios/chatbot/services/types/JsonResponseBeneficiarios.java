package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.BeneficiarioChatBot;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;

import java.util.List;

/**
 * @author by rsalvatierra on 26/10/2017.
 */
public class JsonResponseBeneficiarios extends JsonResponseComun {
    private List<BeneficiarioChatBot> beneficiarios;

    public JsonResponseBeneficiarios(ResponseProcess responseProcess) {
        if (responseProcess != null) {
            setCodRespuesta(responseProcess.getCodRespuesta());
            setMensaje(responseProcess.getMensaje());
        }
    }

    public List<BeneficiarioChatBot> getBeneficiarios() {
        return beneficiarios;
    }

    public void setBeneficiarios(List<BeneficiarioChatBot> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }

    @Override
    public String toString() {
        return "{" +
                "beneficiarios=" + beneficiarios +
                "} " + super.toString();
    }
}
