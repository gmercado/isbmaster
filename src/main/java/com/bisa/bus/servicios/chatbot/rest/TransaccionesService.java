package com.bisa.bus.servicios.chatbot.rest;

import com.bisa.bus.servicios.chatbot.services.TransaccionesChatBotService;
import com.bisa.bus.servicios.chatbot.services.types.*;
import com.bisa.isb.commtools.CommonService;
import com.google.inject.Inject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * @author by rsalvatierra on 01/09/2017.
 */
@Path("/chb/trans")
@Produces({"application/json;charset=UTF-8"})
public class TransaccionesService extends CommonService {

    private static final String PRE_VALIDA_TRANSACCION = "/preValidaTransaccion";
    private static final String TRANSFERENCIA_PROPIA = "/transferenciaPropia";
    private static final String TRANSFERENCIA_TERCEROS = "/transferenciaTerceros";
    private static final String PAGO_TARJETA_CREDITO = "/pagoTarjetaCreditoPropia";
    private static final String RECARGA_CELULAR = "/recargaCelular";
    private static final String GIRO_MOVIL = "/emitirGiroMovil";

    @Inject
    private TransaccionesChatBotService transaccionesChatBotService;

    /**
     * Pre valida transaccion
     *
     * @param httpHeaders        headers
     * @param requestTransaccion datos basicos
     * @return JsonResponseComun
     */
    @POST
    @Path(PRE_VALIDA_TRANSACCION)
    public JsonResponseComun preValidaTransaccion(@Context HttpHeaders httpHeaders, JsonRequestTransaccion requestTransaccion) {
        if (requestTransaccion == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesChatBotService.preValidaTransaccion(requestTransaccion, getUser(httpHeaders));
        }
    }

    /**
     * Transferencia entre cuentas propias
     *
     * @param httpHeaders                headers
     * @param requestTransferenciaPropia datos basicos
     * @return JsonResponseComun
     */
    @POST
    @Path(TRANSFERENCIA_PROPIA)
    public JsonResponseComun transferenciaCuentasPropias(@Context HttpHeaders httpHeaders, JsonRequestTransferenciaPropia requestTransferenciaPropia) {
        if (requestTransferenciaPropia == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesChatBotService.transferenciaCuentasPropias(requestTransferenciaPropia, getUser(httpHeaders));
        }
    }

    /**
     * Transferencia a terceros
     *
     * @param httpHeaders                  headers
     * @param requestTransferenciaTerceros datos basicos
     * @return JsonResponseComun
     */
    @POST
    @Path(TRANSFERENCIA_TERCEROS)
    public JsonResponseComun transferenciaTerceros(@Context HttpHeaders httpHeaders, JsonRequestTransferenciaTerceros requestTransferenciaTerceros) {
        if (requestTransferenciaTerceros == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesChatBotService.transferenciaTerceros(requestTransferenciaTerceros, getUser(httpHeaders));
        }
    }

    /**
     * Pago Tarjeta Credito
     *
     * @param httpHeaders               headers
     * @param requestPagoTarjetaCredito datos basicos
     * @return JsonResponseComun
     */
    @POST
    @Path(PAGO_TARJETA_CREDITO)
    public JsonResponseComun pagoTarjetaCreditoPropia(@Context HttpHeaders httpHeaders, JsonRequestPagoTarjetaCredito requestPagoTarjetaCredito) {
        if (requestPagoTarjetaCredito == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesChatBotService.pagoTarjetaCreditoPropia(requestPagoTarjetaCredito, getUser(httpHeaders));
        }
    }

    /**
     * Recarga de Celular
     *
     * @param httpHeaders           headers
     * @param requestRecargaCelular datos basicos
     * @return JsonResponseComun
     */
    @POST
    @Path(RECARGA_CELULAR)
    public JsonResponseComun recargaCelular(@Context HttpHeaders httpHeaders, JsonRequestRecargaCelular requestRecargaCelular) {
        if (requestRecargaCelular == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesChatBotService.recargaCelular(requestRecargaCelular, getUser(httpHeaders));
        }
    }

    /**
     * Emitir Giro Movil
     *
     * @param httpHeaders      headers
     * @param requestGiroMovil datos basicos
     * @return JsonResponseComun
     */
    @POST
    @Path(GIRO_MOVIL)
    public JsonResponseComun emitirGiroMovil(@Context HttpHeaders httpHeaders, JsonRequestGiroMovil requestGiroMovil) {
        if (requestGiroMovil == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return transaccionesChatBotService.emitirGiroMovil(requestGiroMovil, getUser(httpHeaders));
        }
    }
}
