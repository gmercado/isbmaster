package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 23/09/2017.
 */
public class JsonRequestConfToken extends JsonRequestSolToken {
    private long identificador;
    private String valor;

    public long getIdentificador() {
        return identificador;
    }

    public void setIdentificador(long identificador) {
        this.identificador = identificador;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "{" +
                "identificador=" + identificador +
                ", valor=" + valor +
                "} " + super.toString();
    }
}
