package com.bisa.bus.servicios.chatbot.services;

import com.bisa.bus.servicios.chatbot.services.types.*;

/**
 * @author by rsalvatierra on 27/09/2017.
 */
public interface PrivadosChatBotService {
    JsonResponseCuentasCliente posicionConsolidada(JsonRequestPosicionCliente requestCliente, String usuario);

    JsonResponseMovimientosCuenta movimientosCuenta(JsonRequestMovimientosCuenta requestCliente, String usuario);

    JsonResponseVencimientos vencimientosCliente(JsonRequestPosicionCliente requestCliente, String usuario);

    JsonResponsePuntosCliente puntosCliente(JsonRequestPosicionCliente requestCliente, String usuario);

    JsonResponseComun solicitaEstadoCuenta(JsonRequestEstadoCuenta requestEstadoCuenta, String usuario);
}
