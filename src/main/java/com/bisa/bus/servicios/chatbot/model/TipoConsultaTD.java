package com.bisa.bus.servicios.chatbot.model;

/**
 * @author by rsalvatierra on 26/09/2017.
 */
public enum TipoConsultaTD {
    HABILITACION,
    EXTERIOR,
    VIGENTES,
    TODAS
}
