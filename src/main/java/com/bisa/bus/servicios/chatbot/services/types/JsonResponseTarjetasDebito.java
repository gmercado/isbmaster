package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.ResponseProcess;
import com.bisa.bus.servicios.chatbot.model.TarjetasDebitoChatBot;

import java.util.List;

/**
 * @author by rsalvatierra on 08/09/2017.
 */
public class JsonResponseTarjetasDebito extends JsonResponseComun {
    private List<TarjetasDebitoChatBot> tarjetas;

    public JsonResponseTarjetasDebito(ResponseProcess responseProcess) {
        super(responseProcess);
    }

    public List<TarjetasDebitoChatBot> getTarjetas() {
        return tarjetas;
    }

    public void setTarjetas(List<TarjetasDebitoChatBot> tarjetas) {
        this.tarjetas = tarjetas;
    }

    @Override
    public String toString() {
        return "{" +
                "tarjetas=" + tarjetas +
                "} " + super.toString();
    }
}
