package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.ResponseProcess;

import java.io.Serializable;

/**
 * @author by rsalvatierra on 02/09/2017.
 */
public class JsonResponseComun implements Serializable {

    private int codRespuesta;
    private String mensaje;

    public JsonResponseComun() {
    }

    public JsonResponseComun(ResponseProcess responseProcess) {
        if (responseProcess != null) {
            setCodRespuesta(responseProcess.getCodRespuesta());
            setMensaje(responseProcess.getMensaje());
        }
    }

    public int getCodRespuesta() {
        return codRespuesta;
    }

    public void setCodRespuesta(int codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "{" +
                "codRespuesta=" + codRespuesta +
                ", mensaje='" + mensaje + '\'' +
                '}';
    }
}
