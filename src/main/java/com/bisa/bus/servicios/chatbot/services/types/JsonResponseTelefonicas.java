package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.ResponseProcess;
import com.bisa.bus.servicios.chatbot.model.TelefonicasChatBot;

import java.util.List;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class JsonResponseTelefonicas extends JsonResponseComun {
    private List<TelefonicasChatBot> telefonicas;

    public JsonResponseTelefonicas(ResponseProcess responseProcess) {
        if (responseProcess != null) {
            setCodRespuesta(responseProcess.getCodRespuesta());
            setMensaje(responseProcess.getMensaje());
        }
    }

    public List<TelefonicasChatBot> getTelefonicas() {
        return telefonicas;
    }

    public void setTelefonicas(List<TelefonicasChatBot> telefonicas) {
        this.telefonicas = telefonicas;
    }

    @Override
    public String toString() {
        return "{" +
                "telefonicas=" + telefonicas +
                '}' + super.toString();
    }
}
