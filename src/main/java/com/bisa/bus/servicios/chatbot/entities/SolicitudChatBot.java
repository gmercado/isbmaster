package com.bisa.bus.servicios.chatbot.entities;

import bus.database.model.EntityBase;
import bus.interfaces.token.TresClaves;
import com.bisa.bus.servicios.chatbot.model.EstadoChatBot;
import com.bisa.bus.servicios.chatbot.model.EstadoToken;

import javax.persistence.*;
import java.util.Date;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
@Entity
@Table(name = "ICBOT102")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I102FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I102USRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I102FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I102USRMOD"))
})
public class SolicitudChatBot extends EntityBase implements TresClaves {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I102CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "I102I101OP")
    private OperacionChatBot operacionChatBot;

    @Column(name = "I102TIPROC")
    private long tipoProceso;

    @Column(name = "I102CDCONF", columnDefinition = "VARCHAR(64)")
    private String codigoConfirmacion;

    @Enumerated(EnumType.STRING)
    @Column(name = "I102ESTOPE")
    private EstadoChatBot estadoSolicitud;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "I102FECSOL", columnDefinition = "TIMESTAMP")
    private Date fechaSolicitudToken;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "I102TMPVDA", columnDefinition = "TIMESTAMP")
    private Date tiempoVida;

    @Enumerated(EnumType.STRING)
    @Column(name = "I102TUSADO", columnDefinition = "CHAR(1)")
    private EstadoToken tokenUsado;

    @Column(name = "I102TKNSMS", columnDefinition = "VARCHAR(64)")
    private String tokenSMS;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OperacionChatBot getOperacionChatBot() {
        return operacionChatBot;
    }

    public void setOperacionChatBot(OperacionChatBot operacionChatBot) {
        this.operacionChatBot = operacionChatBot;
    }

    public long getTipoProceso() {
        return tipoProceso;
    }

    public void setTipoProceso(long tipoProceso) {
        this.tipoProceso = tipoProceso;
    }

    public String getCodigoConfirmacion() {
        return codigoConfirmacion;
    }

    public void setCodigoConfirmacion(String codigoConfirmacion) {
        this.codigoConfirmacion = codigoConfirmacion;
    }

    public EstadoChatBot getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(EstadoChatBot estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }

    public Date getFechaSolicitudToken() {
        return fechaSolicitudToken;
    }

    public void setFechaSolicitudToken(Date fechaSolicitudToken) {
        this.fechaSolicitudToken = fechaSolicitudToken;
    }

    public Date getTiempoVida() {
        return tiempoVida;
    }

    public void setTiempoVida(Date tiempoVida) {
        this.tiempoVida = tiempoVida;
    }

    public EstadoToken getTokenUsado() {
        return tokenUsado;
    }

    public void setTokenUsado(EstadoToken tokenUsado) {
        this.tokenUsado = tokenUsado;
    }

    public String getTokenSMS() {
        return tokenSMS;
    }

    public void setTokenSMS(String tokenSMS) {
        this.tokenSMS = tokenSMS;
    }
}
