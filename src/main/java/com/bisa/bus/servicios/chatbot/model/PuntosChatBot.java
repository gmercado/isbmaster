package com.bisa.bus.servicios.chatbot.model;

import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 28/09/2017.
 */
public class PuntosChatBot {
    private String producto;
    private BigDecimal puntos;

    public PuntosChatBot(String producto, BigDecimal puntos) {
        this.producto = producto;
        this.puntos = puntos;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public BigDecimal getPuntos() {
        return puntos;
    }

    public void setPuntos(BigDecimal puntos) {
        this.puntos = puntos;
    }
}
