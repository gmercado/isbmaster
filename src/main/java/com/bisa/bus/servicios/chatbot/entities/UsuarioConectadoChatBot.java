package com.bisa.bus.servicios.chatbot.entities;


import bus.database.model.EntityBase;
import com.bisa.bus.servicios.chatbot.model.EstadoChatBot;

import javax.persistence.*;
import java.util.Date;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
@Entity
@Table(name = "ICBOT104")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I104FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I104USRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I104FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I104USRMOD"))
})
public class UsuarioConectadoChatBot extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I104CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @Column(name = "I104I103US")
    private UsuarioChatBot usuarioChatBot;

    @ManyToOne(fetch = FetchType.EAGER)
    @Column(name = "I104I100CL")
    private ClienteChatBot clienteChatBot;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "I104FECACT", columnDefinition = "VARCHAR(10)")
    private Date fechaUltimaTransaccion;

    @Enumerated(EnumType.STRING)
    @Column(name = "I104ESTSOC", columnDefinition = "VARCHAR(10)")
    private EstadoChatBot estadoUsuarioConectado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UsuarioChatBot getUsuarioChatBot() {
        return usuarioChatBot;
    }

    public void setUsuarioChatBot(UsuarioChatBot usuarioChatBot) {
        this.usuarioChatBot = usuarioChatBot;
    }

    public ClienteChatBot getClienteChatBot() {
        return clienteChatBot;
    }

    public void setClienteChatBot(ClienteChatBot clienteChatBot) {
        this.clienteChatBot = clienteChatBot;
    }

    public Date getFechaUltimaTransaccion() {
        return fechaUltimaTransaccion;
    }

    public void setFechaUltimaTransaccion(Date fechaUltimaTransaccion) {
        this.fechaUltimaTransaccion = fechaUltimaTransaccion;
    }

    public EstadoChatBot getEstadoUsuarioConectado() {
        return estadoUsuarioConectado;
    }

    public void setEstadoUsuarioConectado(EstadoChatBot estadoUsuarioConectado) {
        this.estadoUsuarioConectado = estadoUsuarioConectado;
    }
}
