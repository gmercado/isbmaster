package com.bisa.bus.servicios.chatbot.model;

import bus.plumbing.utils.FormatosUtils;

import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 27/09/2017.
 */
public class CuentasChatBot {
    private String codCuenta;
    private String numero;
    private String tipo;
    private BigDecimal saldo;
    private String moneda;

    public CuentasChatBot(String codCuenta, String numero, String tipo, BigDecimal saldo, String moneda) {
        setCodCuenta(codCuenta);
        if ("tc".equals(tipo)) {
            setNumero(numero);
        } else {
            setNumero(FormatosUtils.formatoCuentaBisaMask(numero));
        }
        setTipo(tipo);
        setSaldo(saldo);
        setMoneda(moneda);
    }

    public String getCodCuenta() {
        return codCuenta;
    }

    public void setCodCuenta(String codCuenta) {
        this.codCuenta = codCuenta;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }
}
