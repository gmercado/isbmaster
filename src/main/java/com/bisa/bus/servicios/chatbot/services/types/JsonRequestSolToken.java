package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 06/09/2017.
 */
public class JsonRequestSolToken {
    private String codCliente;
    private String operacion;

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    @Override
    public String toString() {
        return "{" +
                "codCliente='" + codCliente + '\'' +
                ", operacion='" + operacion + '\'' +
                '}';
    }
}
