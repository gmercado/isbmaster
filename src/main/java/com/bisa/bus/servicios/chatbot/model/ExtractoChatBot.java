package com.bisa.bus.servicios.chatbot.model;

import bus.plumbing.utils.FormatosUtils;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author rsalvatierra on 21/12/2017
 */
public class ExtractoChatBot implements Serializable {
    private String fecha;
    private String hora;
    private String nrocheque;
    private String importe;
    private String saldo;
    private String glosa;
    private String tipo;
    private String adicional;
    private String negsal;
    private BigDecimal fechaDecimal;
    private BigDecimal horaDecimal;

    public ExtractoChatBot(BigDecimal fecha, BigDecimal hora, BigDecimal nrocheque, BigDecimal importe, BigDecimal saldo, String glosa, String tipo, BigDecimal negsal, String adicional) {
        setFechaDecimal(fecha);
        setHoraDecimal(hora);

        StringBuilder stringBuilder = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String s = StringUtils.leftPad(hora.toPlainString(), 6, '0');
        stringBuilder.append(fecha).append(s);
        Date WFECHA;
        try {
            WFECHA = sdf.parse(stringBuilder.toString());
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
        setFecha(FormatosUtils.fechaFormateadaConYY(WFECHA));
        setHora(FormatosUtils.horaFormateadaConHH_mm_ss(WFECHA));

        setImporte(("D".equals(tipo) ? "-" : "") + FormatosUtils.formatoImporte(importe));
        setSaldo((BigDecimal.ZERO.equals(negsal) ? "" : "-") + FormatosUtils.formatoImporte(saldo));
        setNrocheque(String.valueOf(nrocheque));
        setGlosa(glosa);
        setTipo(tipo);
        setNegsal(String.valueOf(negsal));
        setAdicional(adicional);
    }

    public ExtractoChatBot() {
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getNrocheque() {
        return nrocheque;
    }

    public void setNrocheque(String nrocheque) {
        this.nrocheque = nrocheque;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getAdicional() {
        return adicional;
    }

    public void setAdicional(String adicional) {
        this.adicional = adicional;
    }

    public String getNegsal() {
        return negsal;
    }

    public void setNegsal(String negsal) {
        this.negsal = negsal;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public BigDecimal getFechaDecimal() {
        return fechaDecimal;
    }

    public void setFechaDecimal(BigDecimal fechaDecimal) {
        this.fechaDecimal = fechaDecimal;
    }

    public BigDecimal getHoraDecimal() {
        return horaDecimal;
    }

    public void setHoraDecimal(BigDecimal horaDecimal) {
        this.horaDecimal = horaDecimal;
    }

    @Override
    public String toString() {
        return "ExtractoChatBot{" +
                "fecha='" + fecha + '\'' +
                ", hora='" + hora + '\'' +
                ", nrocheque='" + nrocheque + '\'' +
                ", importe=" + importe +
                ", glosa='" + glosa + '\'' +
                ", tipo='" + tipo + '\'' +
                '}';
    }

}
