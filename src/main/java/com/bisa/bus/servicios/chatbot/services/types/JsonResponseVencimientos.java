package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.CuentasActivasChatBot;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;

import java.util.List;

/**
 * @author by rsalvatierra on 28/09/2017.
 */
public class JsonResponseVencimientos extends JsonResponseComun {
    private List<CuentasActivasChatBot> vencimientos;

    public JsonResponseVencimientos(ResponseProcess responseProcess) {
        super(responseProcess);
    }


    public List<CuentasActivasChatBot> getVencimientos() {
        return vencimientos;
    }

    public void setVencimientos(List<CuentasActivasChatBot> vencimientos) {
        this.vencimientos = vencimientos;
    }
}
