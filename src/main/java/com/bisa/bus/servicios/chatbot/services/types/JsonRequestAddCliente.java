package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 06/09/2017.
 */
public class JsonRequestAddCliente extends JsonRequestComun {
    private String codCliente;

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    @Override
    public String toString() {
        return "{" +
                "codCliente='" + codCliente + '\'' +
                "} " + super.toString();
    }
}
