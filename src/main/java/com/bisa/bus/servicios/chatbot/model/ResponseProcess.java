package com.bisa.bus.servicios.chatbot.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author by rsalvatierra on 02/09/2017.
 */
public class ResponseProcess implements Serializable {
    private int codRespuesta;
    private String mensaje;
    private String resultado;
    private List generic;
    private Object obj1;
    private Object obj2;

    public ResponseProcess(int codRespuesta, String mensaje) {
        setCodRespuesta(codRespuesta);
        setMensaje(mensaje);
    }

    public int getCodRespuesta() {
        return codRespuesta;
    }

    public void setCodRespuesta(int codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public List getGeneric() {
        return generic;
    }

    public void setGeneric(List generic) {
        this.generic = generic;
    }

    public Object getObj1() {
        return obj1;
    }

    public void setObj1(Object obj1) {
        this.obj1 = obj1;
    }

    public Object getObj2() {
        return obj2;
    }

    public void setObj2(Object obj2) {
        this.obj2 = obj2;
    }
}
