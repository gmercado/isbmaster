package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.MonedaChatBot;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;

import java.util.List;

/**
 * @author by rsalvatierra on 01/09/2017.
 */
public class JsonResponseMonedas extends JsonResponseComun {
    private List<MonedaChatBot> monedas;

    public JsonResponseMonedas(ResponseProcess result) {
        if (result != null) {
            setCodRespuesta(result.getCodRespuesta());
            setMensaje(result.getMensaje());
        }
    }

    public List<MonedaChatBot> getMonedas() {
        return monedas;
    }

    public void setMonedas(List<MonedaChatBot> monedas) {
        this.monedas = monedas;
    }

    @Override
    public String toString() {
        return "{" +
                "monedas=" + monedas +
                '}' + super.toString();
    }
}
