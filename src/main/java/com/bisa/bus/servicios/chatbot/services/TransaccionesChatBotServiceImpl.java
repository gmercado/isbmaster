package com.bisa.bus.servicios.chatbot.services;

import com.bisa.bus.servicios.chatbot.api.ProcesosChatBot;
import com.bisa.bus.servicios.chatbot.model.ProcesoChatBot;
import com.bisa.bus.servicios.chatbot.model.RequestProcess;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;
import com.bisa.bus.servicios.chatbot.model.TipoProcesoChatBot;
import com.bisa.bus.servicios.chatbot.services.types.*;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author by rsalvatierra on 23/10/2017.
 */
public class TransaccionesChatBotServiceImpl implements TransaccionesChatBotService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransaccionesChatBotServiceImpl.class);
    private final ProcesosChatBot procesosChatBot;

    @Inject
    public TransaccionesChatBotServiceImpl(ProcesosChatBot procesosChatBot) {
        this.procesosChatBot = procesosChatBot;
    }

    @Override
    public JsonResponseComun preValidaTransaccion(JsonRequestTransaccion requestTransaccion, String usuario) {
        LOGGER.info("inicio <preValidaTransaccion>");
        RequestProcess requestProcess = new RequestProcess(requestTransaccion.getCodCliente(), TipoProcesoChatBot.TRS, ProcesoChatBot.PRE_VALIDA_TRANSAC, requestTransaccion, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validatePCC01(requestProcess, requestTransaccion);
        //Obtener data
        JsonResponseComun jsonResponseComun = procesosChatBot.getJsonResponseComun(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <preValidaTransaccion>");
        return jsonResponseComun;
    }

    @Override
    public JsonResponseComun transferenciaCuentasPropias(JsonRequestTransferenciaPropia requestTransferenciaPropia, String usuario) {
        LOGGER.info("inicio <transferenciaCuentasPropias>");
        RequestProcess requestProcess = new RequestProcess(requestTransferenciaPropia.getCodCliente(), TipoProcesoChatBot.TRS, ProcesoChatBot.TRANSFERENCIA_PROPIA, requestTransferenciaPropia, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateTransferencia(requestProcess, requestTransferenciaPropia);
        //Obtener data
        JsonResponseComun jsonResponseComun = procesosChatBot.getJsonResponseComun(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <transferenciaCuentasPropias>");
        return jsonResponseComun;
    }

    @Override
    public JsonResponseComun transferenciaTerceros(JsonRequestTransferenciaTerceros requestTransferenciaTerceros, String usuario) {
        LOGGER.info("inicio <transferenciaTerceros>");
        RequestProcess requestProcess = new RequestProcess(requestTransferenciaTerceros.getCodCliente(), TipoProcesoChatBot.TRS, ProcesoChatBot.TRANSFERENCIA_TERCER, requestTransferenciaTerceros, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateTransferenciaTerceros(requestProcess, requestTransferenciaTerceros);
        //Obtener data
        JsonResponseComun jsonResponseComun = procesosChatBot.getJsonResponseComun(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <transferenciaTerceros>");
        return jsonResponseComun;
    }

    @Override
    public JsonResponseComun pagoTarjetaCreditoPropia(JsonRequestPagoTarjetaCredito requestPagoTarjetaCredito, String usuario) {
        LOGGER.info("inicio <pagoTarjetaCreditoPropia>");
        RequestProcess requestProcess = new RequestProcess(requestPagoTarjetaCredito.getCodCliente(), TipoProcesoChatBot.TRS, ProcesoChatBot.PAGO_TARJETA_CREDITO, requestPagoTarjetaCredito, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validatePagoTarjetaCredito(requestProcess, requestPagoTarjetaCredito);
        //Obtener data
        JsonResponseComun jsonResponseComun = procesosChatBot.getJsonResponseComun(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <pagoTarjetaCreditoPropia>");
        return jsonResponseComun;
    }

    @Override
    public JsonResponseComun recargaCelular(JsonRequestRecargaCelular requestRecargaCelular, String usuario) {
        LOGGER.info("inicio <recargaCelular>");
        RequestProcess requestProcess = new RequestProcess(requestRecargaCelular.getCodCliente(), TipoProcesoChatBot.TRS, ProcesoChatBot.RECARGA_CELULAR, requestRecargaCelular, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateRecargaCelular(requestProcess, requestRecargaCelular);
        //Obtener data
        JsonResponseComun jsonResponseComun = procesosChatBot.getJsonResponseComun(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <recargaCelular>");
        return jsonResponseComun;
    }

    @Override
    public JsonResponseComun emitirGiroMovil(JsonRequestGiroMovil requestGiroMovil, String usuario) {
        LOGGER.info("inicio <emitirGiroMovil>");
        RequestProcess requestProcess = new RequestProcess(requestGiroMovil.getCodCliente(), TipoProcesoChatBot.TRS, ProcesoChatBot.GIRO_MOVIL, requestGiroMovil, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateGiroMovil(requestProcess, requestGiroMovil);
        //Obtener data
        JsonResponseComun jsonResponseComun = procesosChatBot.getJsonResponseComun(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <emitirGiroMovil>");
        return jsonResponseComun;
    }
}
