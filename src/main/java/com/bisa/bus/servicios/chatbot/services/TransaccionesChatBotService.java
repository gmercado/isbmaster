package com.bisa.bus.servicios.chatbot.services;

import com.bisa.bus.servicios.chatbot.services.types.*;

/**
 * @author by rsalvatierra on 23/10/2017.
 */
public interface TransaccionesChatBotService {
    JsonResponseComun transferenciaCuentasPropias(JsonRequestTransferenciaPropia requestTransferenciaPropia, String usuario);

    JsonResponseComun pagoTarjetaCreditoPropia(JsonRequestPagoTarjetaCredito requestPagoTarjetaCredito, String usuario);

    JsonResponseComun recargaCelular(JsonRequestRecargaCelular requestRecargaCelular, String usuario);

    JsonResponseComun emitirGiroMovil(JsonRequestGiroMovil requestGiroMovil, String usuario);

    JsonResponseComun transferenciaTerceros(JsonRequestTransferenciaTerceros requestTransferenciaTerceros, String usuario);

    JsonResponseComun preValidaTransaccion(JsonRequestTransaccion requestTransaccion, String usuario);
}
