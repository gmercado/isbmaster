package com.bisa.bus.servicios.chatbot;

import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.bus.servicios.chatbot.sched.SchedConeccionesChatBot;
import com.bisa.bus.servicios.chatbot.sched.SchedLimitesChatBot;
import com.bisa.bus.servicios.chatbot.sched.SchedOperacionesChatBot;
import com.bisa.bus.servicios.chatbot.services.*;

/**
 * @author by rsalvatierra on 01/09/2017.
 */
public class ChatBotModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(ConsultasChatBotService.class).to(ConsultasChatBotServiceImpl.class);
        bind(GeneralesChatBotService.class).to(GeneralesChatBotServiceImpl.class);
        bind(PrivadosChatBotService.class).to(PrivadosChatBotServiceImpl.class);
        bind(TransaccionesChatBotService.class).to(TransaccionesChatBotServiceImpl.class);
        bindSched("CaducarTokens", "ChatBot", "0 * * * * ?", SchedOperacionesChatBot.class);
        bindSched("CaducarConecciones", "ChatBot", "0 * * * * ?", SchedConeccionesChatBot.class);
        bindSched("ResetLimites", "ChatBot", "0 * * * * ?", SchedLimitesChatBot.class);
        //bindUI("com/bisa/bus/servicios/mojix/ui");
    }
}
