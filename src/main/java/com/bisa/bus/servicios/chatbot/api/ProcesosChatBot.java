package com.bisa.bus.servicios.chatbot.api;

import bus.env.api.MedioAmbiente;
import bus.interfaces.as400.entities.Cliente;
import bus.interfaces.as400.entities.Cuenta;
import bus.interfaces.tipos.TipoCajero;
import bus.plumbing.utils.Monedas;
import bus.plumbing.utils.Par;
import com.bisa.bus.servicios.chatbot.entities.*;
import com.bisa.bus.servicios.chatbot.model.*;
import com.bisa.bus.servicios.chatbot.services.types.*;
import com.bisa.bus.servicios.mojix.model.TipoEmpresaServicio;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static bus.env.api.Variables.*;
import static com.bisa.bus.servicios.chatbot.model.MensajeChatBot.*;
import static com.bisa.bus.servicios.chatbot.model.MessageKeyword.NOMBRECAMPO;
import static com.bisa.bus.servicios.chatbot.model.ValidaCampos.*;
import static com.bisa.bus.servicios.mojix.model.MessageKeyword.TOKENSMS;
import static com.bisa.bus.servicios.mojix.model.MessageKeyword.TOKENTIEMPOVIDA;


/**
 * @author by rsalvatierra on 02/09/2017.
 */
public class ProcesosChatBot {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcesosChatBot.class);
    private static final String TITULO_SMS = "CHATBOT_NOTIFICACION_TOKEN";
    private final MedioAmbiente medioAmbiente;
    private final OperacionesChatBot operacionesChatBot;

    @Inject
    public ProcesosChatBot(MedioAmbiente medioAmbiente,
                           OperacionesChatBot operacionesChatBot) {
        this.medioAmbiente = medioAmbiente;
        this.operacionesChatBot = operacionesChatBot;
    }

    private String replaceKeywords(String msg, Map<String, Object> params) {
        for (String key : params.keySet()) {
            msg = msg.replace("$" + key, toText(params.get(key)));
        }
        msg = msg.replace("$SECONDS_UNTIL_NEXT_SEND", "" + TimeUnit.MILLISECONDS.toSeconds(Optional.ofNullable((Long) params.get("MILLIS_REMAINING_UNTIL_NEXT_ATTEMPT")).orElse(0L)));
        msg = msg.replace("$MINUTES_UNTIL_NEXT_SEND", "" + TimeUnit.MILLISECONDS.toSeconds(Optional.ofNullable((Long) params.get("MILLIS_REMAINING_UNTIL_NEXT_ATTEMPT")).orElse(0L)) / 60);
        return msg;
    }

    private String getMsgSMS(SolicitudChatBot solicitud) {
        return getMensaje(solicitud, CB_NOTIFICACION_TOKEN_SMS, CB_NOTIFICACION_TOKEN_SMS_DEFAULT);
    }

    private String getMsgEMAIL(SolicitudChatBot solicitud) {
        return getMensaje(solicitud, CB_NOTIFICACION_TOKEN_EMAIL, CB_NOTIFICACION_TOKEN_EMAIL_DEFAULT);
    }

    private String getMensaje(SolicitudChatBot solicitud, String cbNotificacionTokenEmail, String cbNotificacionTokenEmailDefault) {
        final SimpleDateFormat dff = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        final Map<String, Object> params = new HashMap<>();
        params.put(TOKENSMS.name(), solicitud.getTokenSMS());
        params.put(TOKENTIEMPOVIDA.name(), dff.format(solicitud.getTiempoVida()));
        String msg = medioAmbiente.getValorDe(cbNotificacionTokenEmail, cbNotificacionTokenEmailDefault);
        return replaceKeywords(msg, params);
    }

    private String toText(Object object) {
        if (object == null)
            return "";
        if (BigDecimal.class.isAssignableFrom(object.getClass()))
            return ((BigDecimal) object).toPlainString();

        return String.valueOf(object);
    }

    private String getMensaje(MensajeChatBot mensajeChatBot, Map<String, Object> params) {
        String mensaje;
        if (mensajeChatBot.isKeyword() && params != null) {
            mensaje = replaceKeywords(medioAmbiente.getValorDe(mensajeChatBot.getMensaje(), mensajeChatBot.getDescripcion()), params);
        } else {
            mensaje = medioAmbiente.getValorDe(mensajeChatBot.getMensaje(), mensajeChatBot.getDescripcion());
        }
        return mensaje;
    }

    private <T> T getResponse(MensajeChatBot mensajeChatBot, Map<String, Object> params, Class<T> clazz) {
        int codigo = -1;
        String mensaje = null;
        if (mensajeChatBot != null) {
            codigo = mensajeChatBot.getCodigo();
            mensaje = getMensaje(mensajeChatBot, params);
        }
        try {
            return clazz.getConstructor(int.class, String.class).newInstance(codigo, mensaje);
        } catch (Exception e) {
            LOGGER.error("Hubo un error al cargas los mensajes ->", e);
        }
        return null;
    }

    public ResponseProcess validateConsulta(RequestProcess requestInfo) {
        ResponseProcess responseOperacion;
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido cod usuario
            if (StringUtils.trimToNull(requestInfo.getCodigoUsuario()) == null) {
                params.put(NOMBRECAMPO.name(), COD_USUARIO);
                responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                return responseOperacion;
            }
            //Validar nombre usuario
            if (StringUtils.trimToNull(requestInfo.getNombreUsuario()) == null) {
                params.put(NOMBRECAMPO.name(), NOMBRE_USUARIO);
                responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                return responseOperacion;
            }
            responseOperacion = getResponse(OK, null, ResponseProcess.class);
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateConsulta>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateConsultaPersonasSucursal(RequestProcess requestProcess, JsonRequestPersonasSucursal requestConsulta) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateConsulta(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                //Datos de entrada
                Map<String, Object> params = new HashMap<>();
                //Valor requerido nombre sucursal
                if (StringUtils.trimToNull(requestConsulta.getNombreSucursal()) == null) {
                    params.put(NOMBRECAMPO.name(), NOMBRE_SUCURSAL);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateConsultaPersonasSucursal>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateAfiliarCliente(RequestProcess requestProcess, JsonRequestAddCliente requestAddCliente, String usuario) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateConsulta(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                //Datos de entrada
                Map<String, Object> params = new HashMap<>();
                //Valor requerido codigo cliente
                if (StringUtils.trimToNull(requestAddCliente.getCodCliente()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_CLIENTE);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //Validar contrato
                if (!operacionesChatBot.realizoContrato(StringUtils.trimToEmpty(requestAddCliente.getCodCliente()))) {
                    responseOperacion = getResponse(ERROR_CONTRATO, null, ResponseProcess.class);
                    return responseOperacion;
                }
                //Validar si existe cliente
                ClienteChatBot clienteChatBot = operacionesChatBot.getClienteChatBot(requestAddCliente.getCodCliente(), requestAddCliente.getCodUsuario(), usuario);
                //Validar si esta afiliado
                AfiliacionChatBot afiliacionChatBot = operacionesChatBot.getAfiliacion(clienteChatBot.getCodigoCliente(), requestProcess.getUsuarioCanal());
                if (afiliacionChatBot == null) {
                    responseOperacion = getResponse(ERROR_CLIENTE_AFILIACION, null, ResponseProcess.class);
                    return responseOperacion;
                }
                if (!TipoEstadoClaveChatBot.ACT.equals(afiliacionChatBot.getEstado())) {
                    responseOperacion = getResponse(ERROR_CLIENTE_INACTIVO, null, ResponseProcess.class);
                    return responseOperacion;
                }
                if (!TipoEstadoClaveChatBot.ACT.equals(afiliacionChatBot.getEstadoBloqueo())) {
                    responseOperacion = getResponse(ERROR_CLIENTE_BLOQUEADO, null, ResponseProcess.class);
                    return responseOperacion;
                }
                //Validar piloto
                if (!operacionesChatBot.ExistePiloto(StringUtils.trimToEmpty(requestAddCliente.getCodCliente()))) {
                    responseOperacion = getResponse(ERROR_PILOTO, null, ResponseProcess.class);
                    return responseOperacion;
                }
                responseOperacion.setObj1(operacionesChatBot.getCliente(clienteChatBot));
                responseOperacion.setObj2(clienteChatBot);
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateAfiliarCliente>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateSolicitudToken(RequestProcess requestProcess, JsonRequestSolToken requestSolToken) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                //Datos de entrada
                Map<String, Object> params = new HashMap<>();
                //Valor requerido codigo de operacion
                if (StringUtils.trimToNull(requestSolToken.getOperacion()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_OPERACION);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //verificar si esta afiliado

            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateSolicitudToken>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    private ResponseProcess validateCliente(RequestProcess requestInfo) {
        ResponseProcess responseOperacion;
        try {
            //Datos de entrada
            Map<String, Object> params = new HashMap<>();
            //Valor requerido cod usuario
            if (StringUtils.trimToNull(requestInfo.getCodigoCliente()) == null) {
                params.put(NOMBRECAMPO.name(), COD_CLIENTE);
                responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                return responseOperacion;
            }
            //Validar si existe cliente
            ClienteChatBot clienteChatBot = operacionesChatBot.getClienteByCodigo(requestInfo.getCodigoCliente());
            if (clienteChatBot == null) {
                responseOperacion = getResponse(ERROR_CLIENTE_NOEXISTE2, params, ResponseProcess.class);
                return responseOperacion;
            }
            //Validar si esta afiliado
            AfiliacionChatBot afiliacionChatBot = operacionesChatBot.getAfiliacion(clienteChatBot.getCodigoCliente(), requestInfo.getUsuarioCanal());
            if (afiliacionChatBot == null) {
                responseOperacion = getResponse(ERROR_CLIENTE_AFILIACION, null, ResponseProcess.class);
                return responseOperacion;
            }
            if (!TipoEstadoClaveChatBot.ACT.equals(afiliacionChatBot.getEstado())) {
                responseOperacion = getResponse(ERROR_CLIENTE_INACTIVO, null, ResponseProcess.class);
                return responseOperacion;
            }
            if (!TipoEstadoClaveChatBot.ACT.equals(afiliacionChatBot.getEstadoBloqueo())) {
                responseOperacion = getResponse(ERROR_CLIENTE_BLOQUEADO, null, ResponseProcess.class);
                return responseOperacion;
            }
            //Validar si esta conectado
            UsuarioConectadoChatBot usuarioConectadoChatBot = operacionesChatBot.getConeccion(clienteChatBot, requestInfo.getUsuarioCanal());
            if (usuarioConectadoChatBot == null) {
                responseOperacion = getResponse(ERROR_CLIENTE_DESCONECTADO, null, ResponseProcess.class);
                return responseOperacion;
            }
            responseOperacion = getResponse(OK, null, ResponseProcess.class);
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateCliente>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateConfirmarToken(RequestProcess requestProcess, JsonRequestConfToken requestConfToken, String usuario) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateSolicitudToken(requestProcess, requestConfToken);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                //Datos de entrada
                Map<String, Object> params = new HashMap<>();
                //Valor requerido codigo de identificador
                if (requestConfToken.getIdentificador() <= 0) {
                    params.put(NOMBRECAMPO.name(), IDENTIFICADOR_TOKEN);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //Valor requerido token
                if (StringUtils.trimToNull(requestConfToken.getValor()) == null) {
                    params.put(NOMBRECAMPO.name(), TOKEN);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //Validar token
                boolean tokenValido = false;
                SolicitudChatBot solicitudChatBot = operacionesChatBot.getSolicitud(requestConfToken);
                if (solicitudChatBot != null) {
                    OperacionChatBot operacionChatBot = solicitudChatBot.getOperacionChatBot();
                    if (operacionChatBot != null) {
                        UsuarioConectadoChatBot usuarioConectadoChatBot = operacionChatBot.getUsuarioConectadoChatBot();
                        UsuarioChatBot usuarioChatBot = usuarioConectadoChatBot.getUsuarioChatBot();
                        ClienteChatBot clienteChatBot = usuarioConectadoChatBot.getClienteChatBot();
                        if (usuarioChatBot != null && clienteChatBot != null
                                && StringUtils.trimToEmpty(requestConfToken.getCodCliente()).equals(StringUtils.trimToNull(clienteChatBot.getCodigoClienteBisa()))) {
                            if (operacionesChatBot.validarToken(String.valueOf(requestConfToken.getValor()), solicitudChatBot.getTokenSMS())) {
                                operacionesChatBot.setConfirmarClave(solicitudChatBot, usuario);
                                tokenValido = true;
                            }
                        }
                    }
                }
                if (!tokenValido) {
                    responseOperacion = getResponse(ERROR_CODIGO_SMS, params, ResponseProcess.class);
                    return responseOperacion;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateConfirmarToken>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateBloqueaTarjeta(RequestProcess requestProcess, JsonRequestBloqueaTD requestBloqueaTD) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                Map<String, Object> params = new HashMap<>();
                if (StringUtils.trimToNull(requestBloqueaTD.getCodTarjeta()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_TARJETA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestBloqueaTD.getMotivo()) == null) {
                    params.put(NOMBRECAMPO.name(), MOTIVO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestBloqueaTD.getTipo()) == null) {
                    params.put(NOMBRECAMPO.name(), TIPO_BLOQUEO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (!operacionesChatBot.setBloqueaTarjetaDebito(requestBloqueaTD.getCodTarjeta(), requestBloqueaTD.getTipo(), requestBloqueaTD.getMotivo())) {
                    responseOperacion = getResponse(ERROR_BLOQUEO, null, ResponseProcess.class);
                    return responseOperacion;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateBloqueaTarjeta>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateTarjetasCliente(RequestProcess requestInfo, JsonRequestTarjetasDebito requestTarjetasDebito) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestInfo);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                Map<String, Object> params = new HashMap<>();
                if (requestTarjetasDebito.getTipo() <= 0 || requestTarjetasDebito.getTipo() > 4) {
                    params.put(NOMBRECAMPO.name(), TIPO_TARJETA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //Obtener cliente
                Cliente cliente = operacionesChatBot.getCliente(requestTarjetasDebito.getCodCliente());
                responseOperacion.setGeneric(operacionesChatBot.getTarjetas(cliente, requestTarjetasDebito.getTipo()));
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateTarjetasCliente>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateCuentasCliente(RequestProcess requestInfo, JsonRequestPosicionCliente requestPosicionCliente) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestInfo);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                String tipoconsulta = StringUtils.trimToNull(requestPosicionCliente.getTipo());
                if (StringUtils.trimToNull(requestPosicionCliente.getTipo()) == null) {
                    tipoconsulta = "all";
                }
                //Obtener cliente
                Cliente cliente = operacionesChatBot.getCliente(requestPosicionCliente.getCodCliente());
                //Obtener posicion consolidada
                responseOperacion.setGeneric(operacionesChatBot.getCuentas(cliente, tipoconsulta));
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateCuentasCliente>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateMovimientosCuenta(RequestProcess requestProcess, JsonRequestMovimientosCuenta requestCliente) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                Map<String, Object> params = new HashMap<>();
                if (StringUtils.trimToNull(requestCliente.getTipo()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_CUENTA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestCliente.getTipo()) == null) {
                    params.put(NOMBRECAMPO.name(), TIPO_CUENTA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                Cuenta cuenta = operacionesChatBot.getCuenta(requestCliente.getCodCuenta());
                responseOperacion.setGeneric(operacionesChatBot.getMovimientoCuenta(cuenta));
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateMovimientosCuenta>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateHabilitacionTarjetaExterior(RequestProcess requestProcess, JsonRequestHabTarjetaExterior requestHabTarjetaExterior, String usuario) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                Map<String, Object> params = new HashMap<>();
                if (StringUtils.trimToNull(requestHabTarjetaExterior.getCodTarjeta()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_TARJETA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestHabTarjetaExterior.getDesde()) == null) {
                    params.put(NOMBRECAMPO.name(), FECHA_DESDE);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestHabTarjetaExterior.getHasta()) == null) {
                    params.put(NOMBRECAMPO.name(), FECHA_HASTA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestHabTarjetaExterior.getMotivo()) == null) {
                    params.put(NOMBRECAMPO.name(), MOTIVO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //Obtener cliente
                Cliente cliente = operacionesChatBot.getCliente(requestHabTarjetaExterior.getCodCliente());
                //Habilitar tarjeta exterior
                if (!operacionesChatBot.setHabilitaTarjetaDebito(cliente, requestHabTarjetaExterior, usuario)) {
                    responseOperacion = getResponse(ERROR_HABILITAR_TD_EXTERIOR, null, ResponseProcess.class);
                    return responseOperacion;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateHabilitacionTarjetaExterior>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateVencimientosCliente(RequestProcess requestProcess, JsonRequestPosicionCliente requestCliente) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                Map<String, Object> params = new HashMap<>();
                if (StringUtils.trimToNull(requestCliente.getTipo()) != null
                        && (!"pp".equals(requestCliente.getTipo())
                        && !"tc".equals(requestCliente.getTipo()))
                        ) {
                    params.put(NOMBRECAMPO.name(), TIPO_CUENTA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                String tipos = StringUtils.trimToNull(requestCliente.getTipo());
                if (StringUtils.trimToNull(requestCliente.getTipo()) == null) {
                    tipos = "all";
                }
                //Obtener cliente
                Cliente cliente = operacionesChatBot.getCliente(requestCliente.getCodCliente());
                List<CuentasActivasChatBot> vencimientos = operacionesChatBot.getVencimientos(cliente, tipos);
                if (vencimientos == null) {
                    responseOperacion = getResponse(ERROR_VENCIMIENTOS, params, ResponseProcess.class);
                    return responseOperacion;
                } else {
                    responseOperacion.setGeneric(operacionesChatBot.getVencimientos(cliente, tipos));
                }
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateVencimientosCliente>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validatePuntosCliente(RequestProcess requestProcess, JsonRequestPosicionCliente requestCliente) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                //Obtener cliente
                Cliente cliente = operacionesChatBot.getCliente(requestCliente.getCodCliente());
                responseOperacion.setGeneric(operacionesChatBot.getPuntos(cliente));
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateVencimientosCliente>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateEstadoCuenta(RequestProcess requestProcess, JsonRequestEstadoCuenta requestEstadoCuenta) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                Map<String, Object> params = new HashMap<>();
                if (StringUtils.trimToNull(requestEstadoCuenta.getCodCuenta()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_CUENTA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //obtener cliente
                Cliente cliente = operacionesChatBot.getCliente(requestEstadoCuenta.getCodCliente());
                if (cliente == null) {
                    responseOperacion = getResponse(ERROR_CLIENTE_NOEXISTE2, null, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(cliente.getMail()) == null) {
                    responseOperacion = getResponse(ERROR_CLIENTE_SINCORREO, null, ResponseProcess.class);
                    return responseOperacion;
                }
                //obtener cuenta
                Cuenta cuenta = operacionesChatBot.getCuenta(requestEstadoCuenta.getCodCuenta());
                //obtener nombre producto
                String nombreProducto = operacionesChatBot.getNombreProducto(cuenta);
                //obtener extracto
                Par<ArrayList<ExtractoChatBot>, HashMap<String, String>> parExtracto = operacionesChatBot.getExtracto(cuenta);
                if (parExtracto == null || parExtracto.getCar() == null || parExtracto.getCdr() == null) {
                    responseOperacion = getResponse(ERROR_SINEXTRACTO, null, ResponseProcess.class);
                    return responseOperacion;
                }
                //generar reporte
                byte[] reporte = ReporteExtracto.get(cuenta, cliente, parExtracto, nombreProducto);
                //enviar mail
                operacionesChatBot.sendExtracto(cliente, reporte);
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateEstadoCuenta>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateConsultaTC(RequestProcess requestProcess, JsonRequestTarjetaCredito requestTarjetaCredito) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                //Obtener datos tc
                responseOperacion.setGeneric(operacionesChatBot.getDatosTC(requestTarjetaCredito.getCodTarjeta()));
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateConsultaTC>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }


    public ResponseProcess validateTransferencia(RequestProcess requestProcess, JsonRequestTransferenciaPropia requestTransferenciaPropia) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                Map<String, Object> params = new HashMap<>();
                if (StringUtils.trimToNull(requestTransferenciaPropia.getCodCuentaOrigen()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_CUENTA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestTransferenciaPropia.getCodCuentaDestino()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_CUENTA_DESTINO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (BigDecimal.ZERO.equals(requestTransferenciaPropia.getMonto())) {
                    params.put(NOMBRECAMPO.name(), MONTO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestTransferenciaPropia.getMoneda()) == null) {
                    params.put(NOMBRECAMPO.name(), MONEDA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //Obtener cliente
                //Cliente cliente = operacionesChatBot.getCliente(requestPagoTarjetaCredito.getCodCliente());
                //Obtener cuenta
                Cuenta cuentaOrigen = operacionesChatBot.getCuenta(requestTransferenciaPropia.getCodCuentaOrigen());
                Cuenta cuentaDestino = operacionesChatBot.getCuenta(requestTransferenciaPropia.getCodCuentaDestino());
                //proceder transferencia
                String respuesta = operacionesChatBot.transferenciaBisa(requestTransferenciaPropia, cuentaOrigen, cuentaDestino, true);
                if (!"0".equals(respuesta)) {
                    MensajeChatBot error = getErrorMonitor(respuesta);
                    responseOperacion = getResponse(error, null, ResponseProcess.class);
                    return responseOperacion;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateTransferencia>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateTransferenciaTerceros(RequestProcess requestProcess, JsonRequestTransferenciaTerceros requestTransferenciaTerceros) {
        ResponseProcess responseOperacion;
        try {
            BigDecimal montoFinal = BigDecimal.ZERO;
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                Map<String, Object> params = new HashMap<>();
                if (StringUtils.trimToNull(requestTransferenciaTerceros.getCodCuentaOrigen()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_CUENTA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestTransferenciaTerceros.getCodBeneficiario()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_BENEFICIARIO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestTransferenciaTerceros.getCodCuentaBeneficiario()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_CUENTA_DESTINO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestTransferenciaTerceros.getNombreBeneficiario()) == null) {
                    params.put(NOMBRECAMPO.name(), NOMBRE_BENEFICIARIO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestTransferenciaTerceros.getTipoPago()) == null) {
                    params.put(NOMBRECAMPO.name(), TIPO_PAGO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestTransferenciaTerceros.getCodBanco()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_BANCO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestTransferenciaTerceros.getSucursalBanco()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_SUCURSAL);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (BigDecimal.ZERO.equals(requestTransferenciaTerceros.getMonto())) {
                    params.put(NOMBRECAMPO.name(), MONTO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestTransferenciaTerceros.getMoneda()) == null) {
                    params.put(NOMBRECAMPO.name(), MONEDA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //validar PCC01
                if (operacionesChatBot.validaPCC01(requestTransferenciaTerceros)) {
                    responseOperacion = getResponse(ERROR_MENSAJEPCC01, null, ResponseProcess.class);
                    return responseOperacion;
                }
                //Validar si existe cliente
                ClienteChatBot clienteChatBot = operacionesChatBot.getClienteByCodigo(requestTransferenciaTerceros.getCodCliente());
                if (clienteChatBot == null) {
                    responseOperacion = getResponse(ERROR_CLIENTE_NOEXISTE2, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //Validar limites
                AfiliacionChatBot afiliacionChatBot = operacionesChatBot.getAfiliacion(clienteChatBot.getCodigoCliente(), requestProcess.getUsuarioCanal());
                if (afiliacionChatBot != null) {
                    BigDecimal montoBs = operacionesChatBot.montoBolivianos(requestTransferenciaTerceros.getMonto(), Monedas.getBisaByISO(requestTransferenciaTerceros.getMoneda()));
                    //limite diario en dolares
                    BigDecimal limiteDiaBs = operacionesChatBot.montoBolivianos(afiliacionChatBot.getLimiteDia(), new Short("2"));
                    //Comparar
                    montoFinal = afiliacionChatBot.getTotalDia().add(montoBs);
                    if (montoFinal.compareTo(limiteDiaBs) >= 0) {
                        responseOperacion = getResponse(ERROR_LIMITES, null, ResponseProcess.class);
                        return responseOperacion;
                    }
                }
                //Obtener cliente
                //Cliente cliente = operacionesChatBot.getCliente(requestPagoTarjetaCredito.getCodCliente());
                //Obtener cuenta
                Cuenta cuentaOrigen = operacionesChatBot.getCuenta(requestTransferenciaTerceros.getCodCuentaOrigen());
                if (cuentaOrigen == null) {
                    responseOperacion = getResponse(ERROR_CUENTA_INEXISTENTE, null, ResponseProcess.class);
                    return responseOperacion;
                }
                //proceder recarga celular
                String respuesta;
                if ("1009".equals(requestTransferenciaTerceros.getCodBanco())) {
                    Cuenta cuentaDestino = operacionesChatBot.getCuenta(requestTransferenciaTerceros.getCodCuentaBeneficiario());
                    if (cuentaDestino == null) {
                        responseOperacion = getResponse(ERROR_CUENTA_INEXISTENTE, null, ResponseProcess.class);
                        return responseOperacion;
                    }
                    respuesta = operacionesChatBot.transferenciaBisa(requestTransferenciaTerceros, cuentaOrigen, cuentaDestino, false);
                } else {
                    respuesta = operacionesChatBot.transferenciaOtroBanco(requestTransferenciaTerceros, cuentaOrigen);
                }

                if (!"0".equals(respuesta)) {
                    MensajeChatBot error = getErrorMonitor(respuesta);
                    responseOperacion = getResponse(error, null, ResponseProcess.class);
                    return responseOperacion;
                } else {
                    if (afiliacionChatBot != null) {
                        afiliacionChatBot.setTotalDia(montoFinal);
                        afiliacionChatBot.setTotal(afiliacionChatBot.getTotal().add(montoFinal));
                        operacionesChatBot.setMontoAfiliacion(afiliacionChatBot, requestProcess.getUsuarioCanal());
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateTransferenciaTerceros>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    private MensajeChatBot getErrorMonitor(String respuesta) {
        MensajeChatBot error = null;
        switch (respuesta) {
            case "901":
                error = ERROR_SALDO_INSUFICIENTE;
                break;
            case "902":
                error = ERROR_PCC01;
                break;
            case "903":
                error = ERROR_MONITOR;
                break;
        }
        return error;
    }

    public ResponseProcess validatePagoTarjetaCredito(RequestProcess requestProcess, JsonRequestPagoTarjetaCredito requestPagoTarjetaCredito) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                Map<String, Object> params = new HashMap<>();
                if (StringUtils.trimToNull(requestPagoTarjetaCredito.getCodCuentaOrigen()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_CUENTA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestPagoTarjetaCredito.getCodTarjetaCredito()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_TARJETA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (BigDecimal.ZERO.equals(requestPagoTarjetaCredito.getMonto())) {
                    params.put(NOMBRECAMPO.name(), MONTO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestPagoTarjetaCredito.getMoneda()) == null) {
                    params.put(NOMBRECAMPO.name(), MONEDA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //Obtener cliente
                //Cliente cliente = operacionesChatBot.getCliente(requestPagoTarjetaCredito.getCodCliente());
                //Obtener cuenta
                Cuenta cuentaOrigen = operacionesChatBot.getCuenta(requestPagoTarjetaCredito.getCodCuentaOrigen());
                if (cuentaOrigen == null) {
                    responseOperacion = getResponse(ERROR_CUENTA_INEXISTENTE, null, ResponseProcess.class);
                    return responseOperacion;
                }
                //proceder pago tarjeta
                String respuesta = operacionesChatBot.pagarTarjetaCredito(requestPagoTarjetaCredito, cuentaOrigen);
                if (!"0".equals(respuesta)) {
                    MensajeChatBot error = getErrorMonitor(respuesta);
                    responseOperacion = getResponse(error, null, ResponseProcess.class);
                    return responseOperacion;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validatePagoTarjetaCredito>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateRecargaCelular(RequestProcess requestProcess, JsonRequestRecargaCelular requestRecargaCelular) {
        ResponseProcess responseOperacion;
        try {
            BigDecimal montoFinal;
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                Map<String, Object> params = new HashMap<>();
                if (StringUtils.trimToNull(requestRecargaCelular.getCodCuentaOrigen()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_CUENTA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestRecargaCelular.getTelefonica()) == null) {
                    params.put(NOMBRECAMPO.name(), NOMBRE_TELEFONICA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestRecargaCelular.getTelefono()) == null) {
                    params.put(NOMBRECAMPO.name(), TELEFONO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (BigDecimal.ZERO.equals(requestRecargaCelular.getMonto())) {
                    params.put(NOMBRECAMPO.name(), MONTO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestRecargaCelular.getMoneda()) == null) {
                    params.put(NOMBRECAMPO.name(), MONEDA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //Verificar montos minimos y maximos por operadora
                TipoEmpresaServicio tipoEmpresaServicio = TipoEmpresaServicio.get2(requestRecargaCelular.getTelefonica());
                if (tipoEmpresaServicio != null) {
                    String rango = medioAmbiente.getValorDe(RECARGA_RANGO + tipoEmpresaServicio.name().toLowerCase(), "");
                    BigDecimal montoMinimo = BigDecimal.ONE;
                    BigDecimal montoMaximo = BigDecimal.TEN;
                    List valores = null;
                    if (rango.contains("-")) {
                        String[] rangos = rango.split("-");
                        montoMinimo = new BigDecimal(rangos[0]);
                        montoMaximo = new BigDecimal(rangos[1]);
                    } else if (rango.contains(",")) {
                        String[] rangos = rango.split(",");
                        valores = Arrays.asList(rangos);
                    }
                    //Para Entel
                    if (TipoEmpresaServicio.ENTEL.equals(tipoEmpresaServicio)) {
                        if (montoMinimo.compareTo(requestRecargaCelular.getMonto()) > 0) {
                            responseOperacion = getResponse(ERROR_MONTO_RECARGA_MINIMO, params, ResponseProcess.class);
                            return responseOperacion;
                        }
                        if (montoMaximo.compareTo(requestRecargaCelular.getMonto()) < 0) {
                            responseOperacion = getResponse(ERROR_MONTO_RECARGA_MAXIMO, params, ResponseProcess.class);
                            return responseOperacion;
                        }
                        //Para Viva
                    } else if (TipoEmpresaServicio.VIVA.equals(tipoEmpresaServicio)) {
                        if (montoMinimo.compareTo(requestRecargaCelular.getMonto()) > 0) {
                            responseOperacion = getResponse(ERROR_MONTO_RECARGA_MINIMO, params, ResponseProcess.class);
                            return responseOperacion;
                        }
                        if (montoMaximo.compareTo(requestRecargaCelular.getMonto()) < 0) {
                            responseOperacion = getResponse(ERROR_MONTO_RECARGA_MAXIMO, params, ResponseProcess.class);
                            return responseOperacion;
                        }
                        //Para Tigo
                    } else if (TipoEmpresaServicio.TIGO.equals(tipoEmpresaServicio) && valores != null) {
                        if (!valores.contains(requestRecargaCelular.getMonto().toString())) {
                            responseOperacion = getResponse(ERROR_RANGO_RECARGA, params, ResponseProcess.class);
                            return responseOperacion;
                        }
                    }
                }
                //Validar si existe cliente
                ClienteChatBot clienteChatBot = operacionesChatBot.getClienteByCodigo(requestRecargaCelular.getCodCliente());
                if (clienteChatBot == null) {
                    responseOperacion = getResponse(ERROR_CLIENTE_NOEXISTE2, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //Validar limites
                AfiliacionChatBot afiliacionChatBot = operacionesChatBot.getAfiliacion(clienteChatBot.getCodigoCliente(), requestProcess.getUsuarioCanal());
                if (afiliacionChatBot == null) {
                    responseOperacion = getResponse(ERROR_CLIENTE_AFILIACION, null, ResponseProcess.class);
                    return responseOperacion;
                } else {
                    BigDecimal montoBs = operacionesChatBot.montoBolivianos(requestRecargaCelular.getMonto(), Monedas.getBisaByISO(requestRecargaCelular.getMoneda()));
                    montoFinal = afiliacionChatBot.getTotalDiaTrans1().add(montoBs);
                    if (montoFinal.compareTo(afiliacionChatBot.getLimiteDiaTrans1()) >= 0) {
                        responseOperacion = getResponse(ERROR_LIMITES, null, ResponseProcess.class);
                        return responseOperacion;
                    }
                }
                //Obtener cliente
                Cliente cliente = operacionesChatBot.getCliente(requestRecargaCelular.getCodCliente());


                requestRecargaCelular.setNitFactura(requestRecargaCelular.getNitFactura());
                requestRecargaCelular.setNombreFactura(requestRecargaCelular.getNombreFactura());
                //Obtener cuenta
                Cuenta cuentaOrigen = operacionesChatBot.getCuenta(requestRecargaCelular.getCodCuentaOrigen());
                if (cuentaOrigen == null) {
                    responseOperacion = getResponse(ERROR_CUENTA_INEXISTENTE, null, ResponseProcess.class);
                    return responseOperacion;
                }
                //proceder recarga celular
                String respuesta = operacionesChatBot.compraCredito(requestRecargaCelular, cliente, cuentaOrigen);
                if (!"0".equals(respuesta)) {
                    responseOperacion = getResponse(ERROR_RECARGA, null, ResponseProcess.class);
                    return responseOperacion;
                } else {
                    afiliacionChatBot.setTotalDiaTrans1(montoFinal);
                    afiliacionChatBot.setTotal(afiliacionChatBot.getTotal().add(montoFinal));
                    operacionesChatBot.setMontoAfiliacion(afiliacionChatBot, requestProcess.getUsuarioCanal());
                }
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateRecargaCelular>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateGiroMovil(RequestProcess requestProcess, JsonRequestGiroMovil requestGiroMovil) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                Map<String, Object> params = new HashMap<>();
                if (StringUtils.trimToNull(requestGiroMovil.getCodCuentaOrigen()) == null) {
                    params.put(NOMBRECAMPO.name(), COD_CUENTA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestGiroMovil.getNumeroDocumento()) == null) {
                    params.put(NOMBRECAMPO.name(), NUMERO_DOCUMENTO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestGiroMovil.getTipoDocumento()) == null) {
                    params.put(NOMBRECAMPO.name(), TIPO_DOCUMENTO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestGiroMovil.getNombreBeneficiario()) == null) {
                    params.put(NOMBRECAMPO.name(), NOMBRE_BENEFICIARIO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestGiroMovil.getTelefono()) == null) {
                    params.put(NOMBRECAMPO.name(), TELEFONO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (BigDecimal.ZERO.equals(requestGiroMovil.getMonto())) {
                    params.put(NOMBRECAMPO.name(), MONTO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestGiroMovil.getMoneda()) == null) {
                    params.put(NOMBRECAMPO.name(), MONEDA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestGiroMovil.getMotivo()) == null) {
                    params.put(NOMBRECAMPO.name(), MOTIVO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //Obtener cliente
                Cliente cliente = operacionesChatBot.getCliente(requestGiroMovil.getCodCliente());
                //Obtener cuenta
                Cuenta cuentaOrigen = operacionesChatBot.getCuenta(requestGiroMovil.getCodCuentaOrigen());
                if (cuentaOrigen == null) {
                    responseOperacion = getResponse(ERROR_CUENTA_INEXISTENTE, null, ResponseProcess.class);
                    return responseOperacion;
                }
                //proceder giro movil
                String respuesta = operacionesChatBot.procesarGiroMovil(requestGiroMovil, cliente, cuentaOrigen);
                if (!"0".equals(respuesta)) {
                    responseOperacion = getResponse(ERROR_GIROMOVIL, null, ResponseProcess.class);
                    return responseOperacion;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateGiroMovil>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validateConsultaBeneficiarios(RequestProcess requestProcess, JsonRequestBeneficiario requestBeneficiario) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                Map<String, Object> params = new HashMap<>();
                if (StringUtils.trimToNull(requestBeneficiario.getNombreBeneficiario()) == null) {
                    params.put(NOMBRECAMPO.name(), NOMBRE_BENEFICIARIO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //Obtener cliente
                Cliente cliente = operacionesChatBot.getCliente(requestBeneficiario.getCodCliente());
                //consulta beneficiarios
                List<BeneficiarioChatBot> beneficiarioChatBotList = operacionesChatBot.getBeneficiarios(cliente, requestBeneficiario.getNombreBeneficiario());
                if (beneficiarioChatBotList == null) {
                    responseOperacion = getResponse(ERROR_BENEFICIARIOS, null, ResponseProcess.class);
                    return responseOperacion;
                } else {
                    responseOperacion.setGeneric(beneficiarioChatBotList);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateGiroMovil>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public ResponseProcess validatePCC01(RequestProcess requestProcess, JsonRequestTransaccion requestTransaccion) {
        ResponseProcess responseOperacion;
        try {
            responseOperacion = validateCliente(requestProcess);
            if (OK.getCodigo() == responseOperacion.getCodRespuesta()) {
                Map<String, Object> params = new HashMap<>();
                if (BigDecimal.ZERO.equals(requestTransaccion.getMonto())) {
                    params.put(NOMBRECAMPO.name(), MONTO);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                if (StringUtils.trimToNull(requestTransaccion.getMoneda()) == null) {
                    params.put(NOMBRECAMPO.name(), MONEDA);
                    responseOperacion = getResponse(ERROR_REQUERIDO, params, ResponseProcess.class);
                    return responseOperacion;
                }
                //Obtener cliente
                //Cliente cliente = operacionesChatBot.getCliente(requestTransaccion.getCodCliente());
                //verifica pcc01
                //validar PCC01
                if (operacionesChatBot.validaPCC01(requestTransaccion)) {
                    responseOperacion = getResponse(ERROR_MENSAJEPCC01, null, ResponseProcess.class);
                    return responseOperacion;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Hubo un error inesperado <validateGiroMovil>", e);
            responseOperacion = getResponse(ERROR_PROCESO, null, ResponseProcess.class);
            return responseOperacion;
        }
        return responseOperacion;
    }

    public JsonResponseMonedas getMonedas(ResponseProcess responseProcess) {
        JsonResponseMonedas jsonResponseMonedas = new JsonResponseMonedas(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            jsonResponseMonedas.setMonedas(MonedaChatBot.getMonedas());
        }
        responseProcess.setResultado(jsonResponseMonedas.toString());
        return jsonResponseMonedas;
    }

    public JsonResponseTelefonicas getTelefonicas(ResponseProcess responseProcess) {
        JsonResponseTelefonicas jsonResponseTelefonicas = new JsonResponseTelefonicas(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            try {
                jsonResponseTelefonicas.setTelefonicas(operacionesChatBot.getEmpresasTelefonicas());
            } catch (Exception e) {
                LOGGER.error("Hubo un error al obtener telefoncias", e);
            }
        }
        responseProcess.setResultado(jsonResponseTelefonicas.toString());
        return jsonResponseTelefonicas;
    }

    public OperacionChatBot setOperacion(RequestProcess requestProcess, ResponseProcess responseProcess) {
        return operacionesChatBot.setOperacion(requestProcess, responseProcess);
    }

    public JsonResponseTiposDocumentos getTiposDocumento(ResponseProcess responseProcess) {
        JsonResponseTiposDocumentos responseTiposDocumentos = new JsonResponseTiposDocumentos(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responseTiposDocumentos.setTiposDocumento(TipoDocumentoChatBot.getTipos());
        }
        responseProcess.setResultado(responseTiposDocumentos.toString());
        return responseTiposDocumentos;
    }

    public JsonResponseDepartamentos getDepartamentos(ResponseProcess responseProcess) {
        JsonResponseDepartamentos responseDepartamentos = new JsonResponseDepartamentos(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responseDepartamentos.setDepartamentos(DepartamentoChatBot.getTipos());
        }
        responseProcess.setResultado(responseDepartamentos.toString());
        return responseDepartamentos;
    }

    public JsonResponseSucursales getSucursales(ResponseProcess responseProcess) {
        JsonResponseSucursales responseSucursales = new JsonResponseSucursales(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responseSucursales.setSucursales(SucursalChatBot.getTipos(operacionesChatBot.getCajeros(TipoCajero.Sucursal)));
        }
        //operacionesChatBot.setCajeros(TipoCajero.Sucursal, "USRISB");
        responseProcess.setResultado(responseSucursales.toString());
        return responseSucursales;
    }

    public JsonResponseCajeros getCajeros(ResponseProcess responseProcess) {
        JsonResponseCajeros responseSucursales = new JsonResponseCajeros(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responseSucursales.setCajeros(SucursalChatBot.getTipos(operacionesChatBot.getCajeros(TipoCajero.Cajero)));
        }
        //operacionesChatBot.setCajeros(TipoCajero.Cajero, "USRISB");
        responseProcess.setResultado(responseSucursales.toString());
        return responseSucursales;
    }

    public JsonResponsePromociones getPromociones(ResponseProcess responseProcess) {
        JsonResponsePromociones responsePromociones = new JsonResponsePromociones(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responsePromociones.setPromociones(PromocionChatBot.getTipos(medioAmbiente.getValorBulkDe(CB_PROMOCIONES), medioAmbiente.getValorBulkDe(CB_PROMOCIONES_URL)));
        }
        responseProcess.setResultado(responsePromociones.toString());
        return responsePromociones;
    }

    public JsonResponseContactos getContactos(ResponseProcess responseProcess) {
        JsonResponseContactos responseContactos = new JsonResponseContactos(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responseContactos.setTelefonos(ContactoChatBot.getTipos(operacionesChatBot.getContactos()));
        }
        responseProcess.setResultado(responseContactos.toString());
        return responseContactos;
    }

    public JsonResponseCotizaciones getCotizaciones(ResponseProcess responseProcess) {
        JsonResponseCotizaciones responseCotizaciones = new JsonResponseCotizaciones(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responseCotizaciones.setCotizaciones(operacionesChatBot.getCotizaciones());
        }
        responseProcess.setResultado(responseCotizaciones.toString());
        return responseCotizaciones;
    }

    public JsonResponsePersonasSucursal getPersonasSucursal(ResponseProcess responseProcess, JsonRequestPersonasSucursal requestConsulta) {
        JsonResponsePersonasSucursal responsePersonasSucursal = new JsonResponsePersonasSucursal(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responsePersonasSucursal.setCantidades(operacionesChatBot.getPersonasSucursal(requestConsulta.getNombreSucursal()));
        }
        responseProcess.setResultado(responsePersonasSucursal.toString());
        return responsePersonasSucursal;
    }

    public JsonResponseCliente getCliente(ResponseProcess responseProcess) {
        JsonResponseCliente responseCliente = new JsonResponseCliente(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responseCliente.setDatosCliente((Cliente) responseProcess.getObj1(), (ClienteChatBot) responseProcess.getObj2());
        }
        responseProcess.setResultado(responseCliente.toString());
        return responseCliente;
    }

    public JsonResponseSolToken setIdentificadorToken(ResponseProcess responseProcess, JsonRequestSolToken requestSolToken, OperacionChatBot operacionChatBot, String usuario) {
        JsonResponseSolToken responseSolToken = new JsonResponseSolToken(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            try {
                //Obtener cliente
                Cliente cliente = operacionesChatBot.getCliente(requestSolToken.getCodCliente());
                //Generar token
                SolicitudChatBot solicitudChatBot = operacionesChatBot.generarClave(requestSolToken.getOperacion(), operacionChatBot, usuario);
                responseSolToken.setIdentificador(solicitudChatBot.getTipoProceso());
                //Obtener tipo notificacion
                Par<String, String> tipoNotificacion = operacionesChatBot.getNotificacionAfiliacion(cliente, usuario);
                if (tipoNotificacion != null) {
                    //Enviar SMS
                    if ("S".equals(tipoNotificacion.getCar())) {
                        operacionesChatBot.enviarSMS(cliente.getTelefono(), cliente.getDocumento(), TITULO_SMS, getMsgSMS(solicitudChatBot), usuario);
                    }
                    //Enviar CORREO
                    if ("S".equals(tipoNotificacion.getCdr())) {
                        operacionesChatBot.enviarEMAIL(cliente, getMsgEMAIL(solicitudChatBot));
                    }
                }
            } catch (Exception e) {
                LOGGER.error("Hubo un error inesperado <setIdentificadorToken>", e);
            }
        }
        responseProcess.setResultado(responseSolToken.toString());
        return responseSolToken;
    }

    @SuppressWarnings("unchecked")
    public JsonResponseTarjetasDebito getTarjetasCliente(ResponseProcess responseProcess) {
        JsonResponseTarjetasDebito responseTarjetasDebito = new JsonResponseTarjetasDebito(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responseTarjetasDebito.setTarjetas(responseProcess.getGeneric());
        }
        responseProcess.setResultado(responseTarjetasDebito.toString());
        return responseTarjetasDebito;
    }

    @SuppressWarnings("unchecked")
    public JsonResponseCuentasCliente getCuentasCliente(ResponseProcess responseProcess) {
        JsonResponseCuentasCliente responseCuentasCliente = new JsonResponseCuentasCliente(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responseCuentasCliente.setCuentas(responseProcess.getGeneric());
        }
        responseProcess.setResultado(responseCuentasCliente.toString());
        return responseCuentasCliente;
    }

    @SuppressWarnings("unchecked")
    public JsonResponseMovimientosCuenta getMovimientosCuenta(ResponseProcess responseProcess) {
        JsonResponseMovimientosCuenta responseMovimientosCuenta = new JsonResponseMovimientosCuenta(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responseMovimientosCuenta.setExtracto(responseProcess.getGeneric());
        }
        responseProcess.setResultado(responseMovimientosCuenta.toString());
        return responseMovimientosCuenta;
    }

    public JsonResponseComun getJsonResponseComun(ResponseProcess responseProcess) {
        JsonResponseComun jsonResponseComun = new JsonResponseComun(responseProcess);
        responseProcess.setResultado(jsonResponseComun.toString());
        return jsonResponseComun;
    }

    @SuppressWarnings("unchecked")
    public JsonResponseVencimientos getVencimientosCliente(ResponseProcess responseProcess) {
        JsonResponseVencimientos responseVencimientos = new JsonResponseVencimientos(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responseVencimientos.setVencimientos(responseProcess.getGeneric());
        }
        responseProcess.setResultado(responseVencimientos.toString());
        return responseVencimientos;
    }

    @SuppressWarnings("unchecked")
    public JsonResponsePuntosCliente getPuntosCliente(ResponseProcess responseProcess) {
        JsonResponsePuntosCliente responsePuntosCliente = new JsonResponsePuntosCliente(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responsePuntosCliente.setPuntos(responseProcess.getGeneric());
        }
        responseProcess.setResultado(responsePuntosCliente.toString());
        return responsePuntosCliente;

    }

    @SuppressWarnings("unchecked")
    public JsonResponseTarjetaCredito getDatosTC(ResponseProcess responseProcess) {
        JsonResponseTarjetaCredito responseTarjetaCredito = new JsonResponseTarjetaCredito(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responseTarjetaCredito.setSaldos(responseProcess.getGeneric());
        }
        responseProcess.setResultado(responseTarjetaCredito.toString());
        return responseTarjetaCredito;
    }

    @SuppressWarnings("unchecked")
    public JsonResponseBeneficiarios getBeneficiariosCliente(ResponseProcess responseProcess) {
        JsonResponseBeneficiarios responseBeneficiarios = new JsonResponseBeneficiarios(responseProcess);
        if (OK.getCodigo() == responseProcess.getCodRespuesta()) {
            responseBeneficiarios.setBeneficiarios(responseProcess.getGeneric());
        }
        responseProcess.setResultado(responseBeneficiarios.toString());
        return responseBeneficiarios;
    }
}



