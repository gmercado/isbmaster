package com.bisa.bus.servicios.chatbot.model;

import java.io.Serializable;

/**
 * @author by rsalvatierra on 02/09/2017.
 */
public class RequestProcess implements Serializable {
    private String codigoUsuario;
    private String nombreUsuario;
    private String codigoCliente;
    private TipoProcesoChatBot tipoProcesoChatBot;
    private ProcesoChatBot procesoChatBot;
    private String peticion;
    private String usuarioCanal;

    public RequestProcess(String codigoUsuario, String nombreUsuario, TipoProcesoChatBot tipoProcesoChatBot, ProcesoChatBot procesoChatBot, Object peticion, String usuarioCanal) {
        setCodigoUsuario(codigoUsuario);
        setNombreUsuario(nombreUsuario);
        setTipoProcesoChatBot(tipoProcesoChatBot);
        setProcesoChatBot(procesoChatBot);
        if (peticion != null) {
            setPeticion(peticion.toString());
        }
        setUsuarioCanal(usuarioCanal);
    }

    public RequestProcess(String codCliente, TipoProcesoChatBot tipoProcesoChatBot, ProcesoChatBot procesoChatBot, Object peticion, String usuarioCanal) {
        setCodigoCliente(codCliente);
        setTipoProcesoChatBot(tipoProcesoChatBot);
        setProcesoChatBot(procesoChatBot);
        if (peticion != null) {
            setPeticion(peticion.toString());
        }
        setUsuarioCanal(usuarioCanal);
    }

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    private void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    private void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public TipoProcesoChatBot getTipoProcesoChatBot() {
        return tipoProcesoChatBot;
    }

    private void setTipoProcesoChatBot(TipoProcesoChatBot tipoProcesoChatBot) {
        this.tipoProcesoChatBot = tipoProcesoChatBot;
    }

    public ProcesoChatBot getProcesoChatBot() {
        return procesoChatBot;
    }

    private void setProcesoChatBot(ProcesoChatBot procesoChatBot) {
        this.procesoChatBot = procesoChatBot;
    }

    public String getPeticion() {
        return peticion;
    }

    private void setPeticion(String peticion) {
        this.peticion = peticion;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getUsuarioCanal() {
        return usuarioCanal;
    }

    public void setUsuarioCanal(String canal) {
        this.usuarioCanal = canal;
    }

    @Override
    public String toString() {
        return "{" +
                "codigoUsuario='" + codigoUsuario + '\'' +
                ", nombreUsuario='" + nombreUsuario + '\'' +
                ", codigoCliente='" + codigoCliente + '\'' +
                ", tipoProcesoChatBot=" + tipoProcesoChatBot +
                ", procesoChatBot=" + procesoChatBot +
                ", peticion='" + peticion + '\'' +
                ", canal='" + usuarioCanal + '\'' +
                '}';
    }
}
