package com.bisa.bus.servicios.chatbot.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.chatbot.entities.OperacionChatBot;
import com.bisa.bus.servicios.chatbot.model.EstadoChatBot;
import com.bisa.bus.servicios.chatbot.model.ProcesoChatBot;
import com.bisa.bus.servicios.chatbot.model.TipoProcesoChatBot;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class OperacionChatBotDao extends DaoImpl<OperacionChatBot, Long> {
    private static final Logger LOGGER = LoggerFactory.getLogger(OperacionChatBotDao.class);

    @Inject
    protected OperacionChatBotDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public OperacionChatBot registrar(OperacionChatBot operacionChatBot, String usuario) {
        operacionChatBot.setFechaCreacion(new Date());
        operacionChatBot.setUsuarioCreador(usuario);
        return persist(operacionChatBot);
    }

    public OperacionChatBot actualizar(OperacionChatBot operacionChatBot, String usuario) {
        operacionChatBot.setFechaModificacion(new Date());
        operacionChatBot.setUsuarioModificador(usuario);
        return merge(operacionChatBot);
    }

    public OperacionChatBot newOperacion(TipoProcesoChatBot tipoProcesoChatBot, ProcesoChatBot procesoChatBot) {
        OperacionChatBot operacionChatBot = new OperacionChatBot();
        operacionChatBot.setCodTransaccion(getTimeString());
        operacionChatBot.setTipoProcesoChatBot(tipoProcesoChatBot);
        operacionChatBot.setProcesoChatBot(procesoChatBot);
        operacionChatBot.setEstado(EstadoChatBot.ACT);
        return operacionChatBot;
    }

    public long getTime() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Instant instant = timestamp.toInstant();
        return instant.toEpochMilli();
    }

    private String getTimeString() {
        return String.valueOf(getTime());
    }

}
