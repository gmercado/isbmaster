package com.bisa.bus.servicios.chatbot.rest;

import com.bisa.bus.servicios.chatbot.services.ConsultasChatBotService;
import com.bisa.bus.servicios.chatbot.services.types.*;
import com.bisa.isb.commtools.CommonService;
import com.google.inject.Inject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * @author by rsalvatierra on 01/09/2017.
 */
@Path("/chb/pub")
@Produces({"application/json;charset=UTF-8"})
public class ConsultaService extends CommonService {

    private static final String CONSULTA_DEPARTAMENTOS = "/consultaDepartamentos";
    private static final String CONSULTA_DOCUMENTOS = "/consultaTiposDocumentos";
    private static final String CONSULTA_MONEDAS = "/consultaMonedas";
    private static final String CONSULTA_TELEFONICAS = "/consultaTelefonicas";
    private static final String CONSULTA_SUCURSALES = "/consultaSucursales";
    private static final String CONSULTA_PROMOCIONES = "/consultaPromociones";
    private static final String CONSULTA_CAJEROS = "/consultaCajeros";
    private static final String CONSULTA_CONTACTOS = "/consultaTelefonosContacto";
    private static final String CONSULTA_COTIZACIONES = "/consultaCotizaciones";
    private static final String CONSULTA_PERSONAS_SUCURSAL = "/consultaPersonasSucursal";

    @Inject
    private ConsultasChatBotService consultasChatBotService;

    /**
     * Consulta de departamentos
     *
     * @param httpHeaders     headers
     * @param requestConsulta datos basicos
     * @return JsonResponseDepartamentos
     */
    @POST
    @Path(CONSULTA_DEPARTAMENTOS)
    public JsonResponseDepartamentos consultaDepartamentos(@Context HttpHeaders httpHeaders, JsonRequestComun requestConsulta) {
        if (requestConsulta == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasChatBotService.consultaDepartamentos(requestConsulta, getUser(httpHeaders));
        }
    }

    /**
     * Consulta de tipos de documentos
     *
     * @param httpHeaders     headers
     * @param requestConsulta datos basicos
     * @return JsonResponseTiposDocumentos
     */
    @POST
    @Path(CONSULTA_DOCUMENTOS)
    public JsonResponseTiposDocumentos consultaDocumentos(@Context HttpHeaders httpHeaders, JsonRequestComun requestConsulta) {
        if (requestConsulta == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasChatBotService.consultaTiposDocumento(requestConsulta, getUser(httpHeaders));
        }
    }

    /**
     * Consulta de tipos de monedas
     *
     * @param httpHeaders     headers
     * @param requestConsulta datos basicos
     * @return JsonResponseMonedas
     */
    @POST
    @Path(CONSULTA_MONEDAS)
    public JsonResponseMonedas consultaMonedas(@Context HttpHeaders httpHeaders, JsonRequestComun requestConsulta) {
        if (requestConsulta == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasChatBotService.consultaMonedas(requestConsulta, getUser(httpHeaders));
        }
    }

    /**
     * Consulta de telefonicas
     *
     * @param httpHeaders     headers
     * @param requestConsulta datos basicos
     * @return JsonResponseTelefonicas
     */
    @POST
    @Path(CONSULTA_TELEFONICAS)
    public JsonResponseTelefonicas consultaTelefonicas(@Context HttpHeaders httpHeaders, JsonRequestComun requestConsulta) {
        if (requestConsulta == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasChatBotService.consultaTelefonicas(requestConsulta, getUser(httpHeaders));
        }
    }

    /**
     * Consulta de sucursales
     *
     * @param httpHeaders     headers
     * @param requestConsulta datos basicos
     * @return JsonResponseSucursales
     */
    @POST
    @Path(CONSULTA_SUCURSALES)
    public JsonResponseSucursales consultaSucursales(@Context HttpHeaders httpHeaders, JsonRequestComun requestConsulta) {
        if (requestConsulta == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasChatBotService.consultaSucusales(requestConsulta, getUser(httpHeaders));
        }
    }

    /**
     * Consulta de promociones
     *
     * @param httpHeaders     headers
     * @param requestConsulta datos basicos
     * @return JsonResponsePromociones
     */
    @POST
    @Path(CONSULTA_PROMOCIONES)
    public JsonResponsePromociones consultaPromociones(@Context HttpHeaders httpHeaders, JsonRequestComun requestConsulta) {
        if (requestConsulta == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasChatBotService.consultaPromociones(requestConsulta, getUser(httpHeaders));
        }
    }

    /**
     * Consulta de cajeros ATM
     *
     * @param httpHeaders     headers
     * @param requestConsulta datos basicos
     * @return JsonResponseCajeros
     */
    @POST
    @Path(CONSULTA_CAJEROS)
    public JsonResponseCajeros consultaCajeros(@Context HttpHeaders httpHeaders, JsonRequestComun requestConsulta) {
        if (requestConsulta == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasChatBotService.consultaCajeros(requestConsulta, getUser(httpHeaders));
        }
    }

    /**
     * Consulta de contactos
     *
     * @param httpHeaders     headers
     * @param requestConsulta datos basicos
     * @return JsonResponseContactos
     */
    @POST
    @Path(CONSULTA_CONTACTOS)
    public JsonResponseContactos consultaContactos(@Context HttpHeaders httpHeaders, JsonRequestComun requestConsulta) {
        if (requestConsulta == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasChatBotService.consultaContactos(requestConsulta, getUser(httpHeaders));
        }
    }

    /**
     * Consulta de cotizaciones
     *
     * @param httpHeaders     headers
     * @param requestConsulta datos basicos
     * @return JsonResponseCotizaciones
     */
    @POST
    @Path(CONSULTA_COTIZACIONES)
    public JsonResponseCotizaciones consultaCotizaciones(@Context HttpHeaders httpHeaders, JsonRequestComun requestConsulta) {
        if (requestConsulta == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasChatBotService.consultaCotizaciones(requestConsulta, getUser(httpHeaders));
        }
    }

    /**
     * Consulta de personas sucursal
     *
     * @param httpHeaders     headers
     * @param requestConsulta datos basicos
     * @return JsonResponsePersonasSucursal
     */
    @POST
    @Path(CONSULTA_PERSONAS_SUCURSAL)
    public JsonResponsePersonasSucursal consultaPersonasSucursal(@Context HttpHeaders httpHeaders, JsonRequestPersonasSucursal requestConsulta) {
        if (requestConsulta == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return consultasChatBotService.consultaPersonasEnSucursal(requestConsulta, getUser(httpHeaders));
        }
    }
}
