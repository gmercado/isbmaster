package com.bisa.bus.servicios.chatbot.model;

import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 29/09/2017.
 */
public class SaldosTCChatBot {
    private int tipo;
    private BigDecimal saldo;
    private String moneda;

    public SaldosTCChatBot(int tipo, BigDecimal saldo, String moneda) {
        this.tipo = tipo;
        this.saldo = saldo;
        this.moneda = moneda;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    @Override
    public String toString() {
        return "{" +
                "tipo='" + tipo + '\'' +
                ", saldo=" + saldo +
                ", moneda='" + moneda + '\'' +
                '}';
    }
}
