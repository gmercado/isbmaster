package com.bisa.bus.servicios.chatbot.sched;


import com.bisa.bus.servicios.chatbot.dao.AfiliacionChatBotDao;
import com.bisa.bus.servicios.chatbot.entities.AfiliacionChatBot;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author by rsalvatierra on 19/02/2018.
 */
public class SchedLimitesChatBot implements Job {

    private static final String USUARIO = "CBUSRJOB";
    private static final Logger LOGGER = LoggerFactory.getLogger(SchedLimitesChatBot.class);
    private final AfiliacionChatBotDao afiliacionChatBotDao;

    @Inject
    public SchedLimitesChatBot(AfiliacionChatBotDao afiliacionChatBotDao) {
        this.afiliacionChatBotDao = afiliacionChatBotDao;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        //Recuperar registros en estado ACTIVO.
        List<AfiliacionChatBot> afiliaciones = afiliacionChatBotDao.getVigentes();
        if (afiliaciones != null && afiliaciones.size() > 0) {
            for (AfiliacionChatBot afiliacionChatBot : afiliaciones) {
                afiliacionChatBot.setTotalDiaTrans1(BigDecimal.ZERO);
                afiliacionChatBot.setTotalDia(BigDecimal.ZERO);
                afiliacionChatBotDao.actualizar(afiliacionChatBot, USUARIO);
            }
        } else {
            LOGGER.debug("CB: No existen afiliaciones pendientes.");
        }
    }
}

