package com.bisa.bus.servicios.chatbot.services;

import com.bisa.bus.servicios.chatbot.api.ProcesosChatBot;
import com.bisa.bus.servicios.chatbot.model.ProcesoChatBot;
import com.bisa.bus.servicios.chatbot.model.RequestProcess;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;
import com.bisa.bus.servicios.chatbot.model.TipoProcesoChatBot;
import com.bisa.bus.servicios.chatbot.services.types.*;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author by rsalvatierra on 01/09/2017.
 */
public class ConsultasChatBotServiceImpl implements ConsultasChatBotService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultasChatBotServiceImpl.class);
    private final ProcesosChatBot procesosChatBot;

    @Inject
    public ConsultasChatBotServiceImpl(ProcesosChatBot procesosChatBot) {
        this.procesosChatBot = procesosChatBot;
    }

    @Override
    public JsonResponseDepartamentos consultaDepartamentos(JsonRequestComun requestConsulta, String usuario) {
        LOGGER.info("inicio <consultaDepartamentos>");
        RequestProcess requestProcess = new RequestProcess(requestConsulta.getCodUsuario(), requestConsulta.getNombre(), TipoProcesoChatBot.CON, ProcesoChatBot.DEPARTAMENTOS, requestConsulta, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateConsulta(requestProcess);
        //Obtener data
        JsonResponseDepartamentos jsonResponseDepartamentos = procesosChatBot.getDepartamentos(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <consultaDepartamentos>");
        return jsonResponseDepartamentos;
    }

    @Override
    public JsonResponseTiposDocumentos consultaTiposDocumento(JsonRequestComun requestConsulta, String usuario) {
        LOGGER.info("inicio <consultaTiposDocumento>");
        RequestProcess requestProcess = new RequestProcess(requestConsulta.getCodUsuario(), requestConsulta.getNombre(), TipoProcesoChatBot.CON, ProcesoChatBot.TIPOS_DOCUMENTO, requestConsulta, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateConsulta(requestProcess);
        //Obtener data
        JsonResponseTiposDocumentos jsonResponseTiposDocumentos = procesosChatBot.getTiposDocumento(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <consultaTiposDocumento>");
        return jsonResponseTiposDocumentos;
    }

    @Override
    public JsonResponseMonedas consultaMonedas(JsonRequestComun requestConsulta, String usuario) {
        LOGGER.info("inicio <consultaMonedas>");
        RequestProcess requestProcess = new RequestProcess(requestConsulta.getCodUsuario(), requestConsulta.getNombre(), TipoProcesoChatBot.CON, ProcesoChatBot.MONEDAS, requestConsulta, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateConsulta(requestProcess);
        //Obtener data
        JsonResponseMonedas jsonResponseMonedas = procesosChatBot.getMonedas(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <consultaMonedas>");
        return jsonResponseMonedas;
    }

    @Override
    public JsonResponseTelefonicas consultaTelefonicas(JsonRequestComun requestConsulta, String usuario) {
        LOGGER.info("inicio <consultaTelefonicas>");
        RequestProcess requestProcess = new RequestProcess(requestConsulta.getCodUsuario(), requestConsulta.getNombre(), TipoProcesoChatBot.CON, ProcesoChatBot.TELEFONICAS, requestConsulta, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateConsulta(requestProcess);
        //Obtener data
        JsonResponseTelefonicas jsonResponseTelefonicas = procesosChatBot.getTelefonicas(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <consultaTelefonicas>");
        return jsonResponseTelefonicas;
    }

    @Override
    public JsonResponseSucursales consultaSucusales(JsonRequestComun requestConsulta, String usuario) {
        LOGGER.info("inicio <consultaSucusales>");
        RequestProcess requestProcess = new RequestProcess(requestConsulta.getCodUsuario(), requestConsulta.getNombre(), TipoProcesoChatBot.CON, ProcesoChatBot.SUCURSALES, requestConsulta, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateConsulta(requestProcess);
        //Obtener data
        JsonResponseSucursales jsonResponseSucursales = procesosChatBot.getSucursales(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <consultaSucusales>");
        return jsonResponseSucursales;
    }

    @Override
    public JsonResponseCajeros consultaCajeros(JsonRequestComun requestConsulta, String usuario) {
        LOGGER.info("inicio <consultaCajeros>");
        RequestProcess requestProcess = new RequestProcess(requestConsulta.getCodUsuario(), requestConsulta.getNombre(), TipoProcesoChatBot.CON, ProcesoChatBot.CAJEROS, requestConsulta, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateConsulta(requestProcess);
        //Obtener data
        JsonResponseCajeros jsonResponseCajeros = procesosChatBot.getCajeros(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <consultaCajeros>");
        return jsonResponseCajeros;
    }

    @Override
    public JsonResponsePromociones consultaPromociones(JsonRequestComun requestConsulta, String usuario) {
        LOGGER.info("inicio <consultaPromociones>");
        RequestProcess requestProcess = new RequestProcess(requestConsulta.getCodUsuario(), requestConsulta.getNombre(), TipoProcesoChatBot.CON, ProcesoChatBot.PROMOCIONES, requestConsulta, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateConsulta(requestProcess);
        //Obtener data
        JsonResponsePromociones jsonResponsePromociones = procesosChatBot.getPromociones(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <consultaPromociones>");
        return jsonResponsePromociones;
    }

    @Override
    public JsonResponseContactos consultaContactos(JsonRequestComun requestConsulta, String usuario) {
        LOGGER.info("inicio <consultaContactos>");
        RequestProcess requestProcess = new RequestProcess(requestConsulta.getCodUsuario(), requestConsulta.getNombre(), TipoProcesoChatBot.CON, ProcesoChatBot.CONTACTOS, requestConsulta, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateConsulta(requestProcess);
        //Obtener data
        JsonResponseContactos jsonResponseContactos = procesosChatBot.getContactos(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <consultaContactos>");
        return jsonResponseContactos;
    }

    @Override
    public JsonResponseCotizaciones consultaCotizaciones(JsonRequestComun requestConsulta, String usuario) {
        LOGGER.info("inicio <consultaCotizaciones>");
        RequestProcess requestProcess = new RequestProcess(requestConsulta.getCodUsuario(), requestConsulta.getNombre(), TipoProcesoChatBot.CON, ProcesoChatBot.COTIZACIONES, requestConsulta, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateConsulta(requestProcess);
        //Obtener data
        JsonResponseCotizaciones jsonResponseCotizaciones = procesosChatBot.getCotizaciones(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <consultaCotizaciones>");
        return jsonResponseCotizaciones;
    }

    @Override
    public JsonResponsePersonasSucursal consultaPersonasEnSucursal(JsonRequestPersonasSucursal requestConsulta, String usuario) {
        LOGGER.info("inicio <consultaPersonasEnSucursal>");
        RequestProcess requestProcess = new RequestProcess(requestConsulta.getCodUsuario(), requestConsulta.getNombre(), TipoProcesoChatBot.CON, ProcesoChatBot.PERSONAS_SUCURSAL, requestConsulta, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateConsultaPersonasSucursal(requestProcess, requestConsulta);
        //Obtener data
        JsonResponsePersonasSucursal jsonResponsePersonasSucursal = procesosChatBot.getPersonasSucursal(responseProcess, requestConsulta);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <consultaPersonasEnSucursal>");
        return jsonResponsePersonasSucursal;
    }
}
