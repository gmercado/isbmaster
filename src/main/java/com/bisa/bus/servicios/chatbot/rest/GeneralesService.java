package com.bisa.bus.servicios.chatbot.rest;

import com.bisa.bus.servicios.chatbot.services.GeneralesChatBotService;
import com.bisa.bus.servicios.chatbot.services.types.*;
import com.bisa.isb.commtools.CommonService;
import com.google.inject.Inject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * @author by rsalvatierra on 01/09/2017.
 */
@Path("/chb/gen")
@Produces({"application/json;charset=UTF-8"})
public class GeneralesService extends CommonService {

    private static final String ADICIONAR_CLIENTE = "/adicionarCliente";
    private static final String SOLICITAR_TOKEN = "/solicitarToken";
    private static final String CONFIRMAR_TOKEN = "/confirmarToken";
    private static final String CONSULTA_TD = "/consultaTarjetasDebito";
    private static final String HABILITA_TD_EXTERIOR = "/habilitaTarjetaDebitoExterior";
    private static final String BLOQUEA_TD = "/bloqueaTarjetaDebito";
    private static final String CONSULTA_TC = "/consultaTarjetaCredito";
    private static final String CONSULTA_BENEFICIARIOS = "/consultaBeneficiarios";

    @Inject
    private GeneralesChatBotService generalesChatBotService;

    /**
     * Afiliar clientes al servicio
     *
     * @param httpHeaders       headers
     * @param requestAddCliente datos basicos
     * @return JsonResponseComun
     */
    @POST
    @Path(ADICIONAR_CLIENTE)
    public JsonResponseCliente adicionarCliente(@Context HttpHeaders httpHeaders, JsonRequestAddCliente requestAddCliente) {
        if (requestAddCliente == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return generalesChatBotService.adicionarCliente(requestAddCliente, getUser(httpHeaders));
        }
    }

    /**
     * Solicitar Token
     *
     * @param httpHeaders     headers
     * @param requestSolToken datos basicos
     * @return JsonResponseSolToken
     */
    @POST
    @Path(SOLICITAR_TOKEN)
    public JsonResponseSolToken solicitarToken(@Context HttpHeaders httpHeaders, JsonRequestSolToken requestSolToken) {
        if (requestSolToken == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return generalesChatBotService.solicitarToken(requestSolToken, getUser(httpHeaders));
        }
    }

    /**
     * Confirmar Token
     *
     * @param httpHeaders      headers
     * @param requestConfToken datos basicos
     * @return JsonResponseComun
     */
    @POST
    @Path(CONFIRMAR_TOKEN)
    public JsonResponseComun confirmarToken(@Context HttpHeaders httpHeaders, JsonRequestConfToken requestConfToken) {
        if (requestConfToken == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return generalesChatBotService.validarToken(requestConfToken, getUser(httpHeaders));
        }
    }

    /**
     * Consultar tarjetas debito
     *
     * @param httpHeaders           headers
     * @param requestTarjetasDebito datos basicos
     * @return JsonResponseTarjetasDebito
     */
    @POST
    @Path(CONSULTA_TD)
    public JsonResponseTarjetasDebito consultaTarjetasDebito(@Context HttpHeaders httpHeaders, JsonRequestTarjetasDebito requestTarjetasDebito) {
        if (requestTarjetasDebito == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return generalesChatBotService.consultarTarjetasDebito(requestTarjetasDebito, getUser(httpHeaders));
        }
    }

    /**
     * Habilitar tarjeta debito al exterior
     *
     * @param httpHeaders               headers
     * @param requestHabTarjetaExterior datos basicos
     * @return JsonResponseComun
     */
    @POST
    @Path(HABILITA_TD_EXTERIOR)
    public JsonResponseComun habilitaTarjetaDebitoExterior(@Context HttpHeaders httpHeaders, JsonRequestHabTarjetaExterior requestHabTarjetaExterior) {
        if (requestHabTarjetaExterior == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return generalesChatBotService.habilitaTarjetaDebitoExterior(requestHabTarjetaExterior, getUser(httpHeaders));
        }
    }

    /**
     * Bloquear tarjeta debito
     *
     * @param httpHeaders      headers
     * @param requestBloqueaTD datos basicos
     * @return JsonResponseComun
     */
    @POST
    @Path(BLOQUEA_TD)
    public JsonResponseComun bloquearTarjetaDebito(@Context HttpHeaders httpHeaders, JsonRequestBloqueaTD requestBloqueaTD) {
        if (requestBloqueaTD == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return generalesChatBotService.bloqueTarjetaDebito(requestBloqueaTD, getUser(httpHeaders));
        }
    }

    /**
     * Consultar tarjeta credito
     *
     * @param httpHeaders           headers
     * @param requestTarjetaCredito datos basicos
     * @return JsonRequestTarjetaCredito
     */
    @POST
    @Path(CONSULTA_TC)
    public JsonResponseTarjetaCredito consultaTarjetaCredito(@Context HttpHeaders httpHeaders, JsonRequestTarjetaCredito requestTarjetaCredito) {
        if (requestTarjetaCredito == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return generalesChatBotService.consultarTarjetaCredito(requestTarjetaCredito, getUser(httpHeaders));
        }
    }

    /**
     * Consulta beneficiarios
     *
     * @param httpHeaders         headers
     * @param requestBeneficiario datos basicos
     * @return JsonResponseBeneficiarios
     */
    @POST
    @Path(CONSULTA_BENEFICIARIOS)
    public JsonResponseBeneficiarios consultaBeneficiarios(@Context HttpHeaders httpHeaders, JsonRequestBeneficiario requestBeneficiario) {
        if (requestBeneficiario == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return generalesChatBotService.consultaBeneficiarios(requestBeneficiario, getUser(httpHeaders));
        }
    }
}
