package com.bisa.bus.servicios.chatbot.sched;

import bus.env.api.MedioAmbiente;
import com.bisa.bus.servicios.chatbot.dao.UsuarioConectadoChatBotDao;
import com.bisa.bus.servicios.chatbot.entities.UsuarioConectadoChatBot;
import com.bisa.bus.servicios.chatbot.model.EstadoChatBot;
import com.google.inject.Inject;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static bus.env.api.Variables.CB_TIEMPO_VIGENCIA_SESION;
import static bus.env.api.Variables.CB_TIEMPO_VIGENCIA_SESION_DEFAULT;

/**
 * @author by rsalvatierra on 15/12/2017.
 */
public class SchedConeccionesChatBot implements Job {

    private static final String USUARIO = "CBUSRJOB";
    private static final Logger LOGGER = LoggerFactory.getLogger(SchedConeccionesChatBot.class);
    private final UsuarioConectadoChatBotDao usuarioConectadoChatBotDao;
    private final MedioAmbiente medioAmbiente;

    @Inject
    public SchedConeccionesChatBot(UsuarioConectadoChatBotDao usuarioConectadoChatBotDao,
                                   MedioAmbiente medioAmbiente) {
        this.usuarioConectadoChatBotDao = usuarioConectadoChatBotDao;
        this.medioAmbiente = medioAmbiente;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        DateTime now = DateTime.now();
        DateTime fecTiempoVida;
        int vigencia = medioAmbiente.getValorIntDe(CB_TIEMPO_VIGENCIA_SESION, CB_TIEMPO_VIGENCIA_SESION_DEFAULT);
        //Recuperar registros en estado ACTIVO.
        List<UsuarioConectadoChatBot> usuarioVigentes = usuarioConectadoChatBotDao.getByEstado(EstadoChatBot.ACT);
        if (usuarioVigentes != null && usuarioVigentes.size() > 0) {
            for (UsuarioConectadoChatBot usuarioConectadoChatBot : usuarioVigentes) {
                if (usuarioConectadoChatBot.getFechaUltimaTransaccion() != null) {
                    fecTiempoVida = (new DateTime(usuarioConectadoChatBot.getFechaUltimaTransaccion())).plusMinutes(vigencia);
                    LOGGER.debug("Tiempo de Vida {}, Fecha Actual {}", fecTiempoVida, now);
                    if (now.toDate().after(fecTiempoVida.toDate())) {
                        LOGGER.info("SchedConeccionesChatBot  Actualizando >>> {}", usuarioConectadoChatBot.getId());
                        usuarioConectadoChatBot.setEstadoUsuarioConectado(EstadoChatBot.INA);
                        usuarioConectadoChatBotDao.actualizar(usuarioConectadoChatBot, USUARIO);
                    }
                }
            }
        } else {
            LOGGER.debug("CB: No existen solicitudes pendientes.");
        }
    }
}
