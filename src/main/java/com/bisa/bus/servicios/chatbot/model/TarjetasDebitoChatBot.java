package com.bisa.bus.servicios.chatbot.model;

import bus.plumbing.utils.Formateador;

/**
 * @author by rsalvatierra on 08/09/2017.
 */
public class TarjetasDebitoChatBot {
    private String codTarjeta;
    private String numero;

    public TarjetasDebitoChatBot(String codTarjeta, String numero) {
        if (numero != null) {
            this.codTarjeta = codTarjeta;
            this.numero = Formateador.tarjetaDebitoFormateadoOfuscado(numero);
        }
    }

    public String getCodTarjeta() {
        return codTarjeta;
    }

    public void setCodTarjeta(String codTarjeta) {
        this.codTarjeta = codTarjeta;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "{" +
                "codTarjeta='" + codTarjeta + '\'' +
                ", numero='" + numero + '\'' +
                '}';
    }
}
