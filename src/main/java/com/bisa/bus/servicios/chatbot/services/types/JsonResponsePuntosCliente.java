package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.PuntosChatBot;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;

import java.util.List;

/**
 * @author by rsalvatierra on 28/09/2017.
 */
public class JsonResponsePuntosCliente extends JsonResponseComun {
    private List<PuntosChatBot> puntos;

    public JsonResponsePuntosCliente(ResponseProcess responseProcess) {
        super(responseProcess);
    }

    public List<PuntosChatBot> getPuntos() {
        return puntos;
    }

    public void setPuntos(List<PuntosChatBot> puntos) {
        this.puntos = puntos;
    }
}
