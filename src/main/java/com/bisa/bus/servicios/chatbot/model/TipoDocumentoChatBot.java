package com.bisa.bus.servicios.chatbot.model;

import bus.interfaces.tipos.TipoDocumentoDao;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class TipoDocumentoChatBot {
    private String codDocumento;
    private String descripcion;

    private TipoDocumentoChatBot(TipoDocumentoDao tipoDocumento) {
        setCodDocumento(tipoDocumento.getCodigo());
        setDescripcion(tipoDocumento.getDescripcion());
    }

    public static List<TipoDocumentoChatBot> getTipos() {
        return Lists.newArrayList(TipoDocumentoDao.values()).stream().map(TipoDocumentoChatBot::new).collect(Collectors.toList());
    }

    public String getCodDocumento() {
        return codDocumento;
    }

    private void setCodDocumento(String codDocumento) {
        this.codDocumento = codDocumento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "{" +
                "codDocumento='" + codDocumento + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
