package com.bisa.bus.servicios.chatbot.model;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public enum TipoRedSocial {
    DEFAULT("default"),
    FACEBOOK("facebook"),
    TELEGRAM("telegram");
    private String descripcion;

    TipoRedSocial(String descripcion) {
        setDescripcion(descripcion);
    }

    public static TipoRedSocial get(String usuario) {
        if (usuario != null) {
            for (TipoRedSocial tipo : TipoRedSocial.values()) {
                if (usuario.equals(tipo.getDescripcion())) {
                    return tipo;
                }
            }
        }
        return null;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
