package com.bisa.bus.servicios.chatbot.entities;

import bus.database.model.EntityBase;
import com.bisa.bus.servicios.chatbot.model.TipoEstadoClaveChatBot;
import com.bisa.bus.servicios.chatbot.model.TipoRedSocial;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
@Entity
@Table(name = "ICBOT112")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I112FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I112USRALT", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I112FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I112USRMOD"))
})
public class AfiliacionChatBot extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I112CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "I112NROCLI", columnDefinition = "VARCHAR(10)")
    private String codigoCliente;

    @Column(name = "I112UEBISA", columnDefinition = "VARCHAR(50)")
    private String codigoEbisa;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "I112SOCIAL", columnDefinition = "VARCHAR(50)")
    private TipoRedSocial redSocial;

    @Column(name = "I112NOTCEL", columnDefinition = "VARCHAR(1)")
    private String notificacionTelefono;

    @Column(name = "I112NOTEMA", columnDefinition = "VARCHAR(1)")
    private String notificacionEmail;

    @Enumerated(EnumType.STRING)
    @Column(name = "I112ESTADO")
    private TipoEstadoClaveChatBot estado;

    @Enumerated(EnumType.STRING)
    @Column(name = "I112ESTCLA")
    private TipoEstadoClaveChatBot estadoClave;

    @Enumerated(EnumType.STRING)
    @Column(name = "I112BLOQUE")
    private TipoEstadoClaveChatBot estadoBloqueo;

    @Column(name = "I112INTNTS", columnDefinition = "VARCHAR(50)")
    private int intentos;

    @Column(name = "I112LIMDIA", columnDefinition = "VARCHAR(50)")
    private BigDecimal limiteDia;

    @Column(name = "I112TOTALD", columnDefinition = "VARCHAR(50)")
    private BigDecimal totalDia;

    @Column(name = "I112LIMTR1", columnDefinition = "VARCHAR(50)")
    private BigDecimal limiteDiaTrans1;

    @Column(name = "I112TOTAL1", columnDefinition = "VARCHAR(50)")
    private BigDecimal totalDiaTrans1;

    @Column(name = "I112TOTALT", columnDefinition = "VARCHAR(50)")
    private BigDecimal total;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getCodigoEbisa() {
        return codigoEbisa;
    }

    public void setCodigoEbisa(String codigoEbisa) {
        this.codigoEbisa = codigoEbisa;
    }

    public TipoRedSocial getRedSocial() {
        return redSocial;
    }

    public void setRedSocial(TipoRedSocial redSocial) {
        this.redSocial = redSocial;
    }

    public String getNotificacionTelefono() {
        return notificacionTelefono;
    }

    public void setNotificacionTelefono(String notificacionTelefono) {
        this.notificacionTelefono = notificacionTelefono;
    }

    public String getNotificacionEmail() {
        return notificacionEmail;
    }

    public void setNotificacionEmail(String notificacionEmail) {
        this.notificacionEmail = notificacionEmail;
    }

    public TipoEstadoClaveChatBot getEstado() {
        return estado;
    }

    public void setEstado(TipoEstadoClaveChatBot estado) {
        this.estado = estado;
    }

    public TipoEstadoClaveChatBot getEstadoClave() {
        return estadoClave;
    }

    public void setEstadoClave(TipoEstadoClaveChatBot estadoClave) {
        this.estadoClave = estadoClave;
    }

    public TipoEstadoClaveChatBot getEstadoBloqueo() {
        return estadoBloqueo;
    }

    public void setEstadoBloqueo(TipoEstadoClaveChatBot estadoBloqueo) {
        this.estadoBloqueo = estadoBloqueo;
    }

    public int getIntentos() {
        return intentos;
    }

    public void setIntentos(int intentos) {
        this.intentos = intentos;
    }

    public BigDecimal getLimiteDia() {
        return limiteDia;
    }

    public void setLimiteDia(BigDecimal limiteDia) {
        this.limiteDia = limiteDia;
    }

    public BigDecimal getTotalDia() {
        return totalDia;
    }

    public void setTotalDia(BigDecimal totalDia) {
        this.totalDia = totalDia;
    }

    public BigDecimal getLimiteDiaTrans1() {
        return limiteDiaTrans1;
    }

    public void setLimiteDiaTrans1(BigDecimal limiteDiaTrans1) {
        this.limiteDiaTrans1 = limiteDiaTrans1;
    }

    public BigDecimal getTotalDiaTrans1() {
        return totalDiaTrans1;
    }

    public void setTotalDiaTrans1(BigDecimal totalDiaTrans1) {
        this.totalDiaTrans1 = totalDiaTrans1;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
