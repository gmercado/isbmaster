package com.bisa.bus.servicios.chatbot.model;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public enum TipoProcesoChatBot {
    CON("PUBLICA"),
    GEN("GENERAL"),
    PRV("PRIVADA"),
    TRS("TRANSFERENCIA");

    private String descripcion;

    TipoProcesoChatBot(String descripcion) {
        setDescripcion(descripcion);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
