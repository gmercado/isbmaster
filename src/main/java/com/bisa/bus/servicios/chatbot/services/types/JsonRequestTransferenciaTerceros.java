package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 25/10/2017.
 */
public class JsonRequestTransferenciaTerceros extends JsonRequestTransaccion {
    private String codBeneficiario;
    private String codCuentaBeneficiario;
    private String documentoBeneficiario;
    private String nombreBeneficiario;
    private String tipoPago;
    private String codBanco;
    private String sucursalBanco;
    private String origenFondo;
    private String destinoFondo;

    public String getOrigenFondo() {
        return origenFondo;
    }

    public void setOrigenFondo(String origenFondo) {
        this.origenFondo = origenFondo;
    }

    public String getDestinoFondo() {
        return destinoFondo;
    }

    public void setDestinoFondo(String destinoFondo) {
        this.destinoFondo = destinoFondo;
    }

    public String getCodBeneficiario() {
        return codBeneficiario;
    }

    public void setCodBeneficiario(String codBeneficiario) {
        this.codBeneficiario = codBeneficiario;
    }

    public String getCodCuentaBeneficiario() {
        return codCuentaBeneficiario;
    }

    public void setCodCuentaBeneficiario(String codCuentaBeneficiario) {
        this.codCuentaBeneficiario = codCuentaBeneficiario;
    }

    public String getDocumentoBeneficiario() {
        return documentoBeneficiario;
    }

    public void setDocumentoBeneficiario(String documentoBeneficiario) {
        this.documentoBeneficiario = documentoBeneficiario;
    }

    public String getNombreBeneficiario() {
        return nombreBeneficiario;
    }

    public void setNombreBeneficiario(String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getCodBanco() {
        return codBanco;
    }

    public void setCodBanco(String codBanco) {
        this.codBanco = codBanco;
    }

    public String getSucursalBanco() {
        return sucursalBanco;
    }

    public void setSucursalBanco(String sucursalBanco) {
        this.sucursalBanco = sucursalBanco;
    }

    @Override
    public String toString() {
        return "{" +
                "codBeneficiario='" + codBeneficiario + '\'' +
                ", codCuentaBeneficiario='" + codCuentaBeneficiario + '\'' +
                ", documentoBeneficiario='" + documentoBeneficiario + '\'' +
                ", nombreBeneficiario='" + nombreBeneficiario + '\'' +
                ", tipoPago='" + tipoPago + '\'' +
                ", codBanco='" + codBanco + '\'' +
                ", sucursalBanco='" + sucursalBanco + '\'' +
                ", origenFondo='" + origenFondo + '\'' +
                ", destinoFondo='" + destinoFondo + '\'' +
                "} " + super.toString();
    }
}
