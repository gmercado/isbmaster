package com.bisa.bus.servicios.chatbot.services;

import com.bisa.bus.servicios.chatbot.api.ProcesosChatBot;
import com.bisa.bus.servicios.chatbot.model.ProcesoChatBot;
import com.bisa.bus.servicios.chatbot.model.RequestProcess;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;
import com.bisa.bus.servicios.chatbot.model.TipoProcesoChatBot;
import com.bisa.bus.servicios.chatbot.services.types.*;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author by rsalvatierra on 27/09/2017.
 */
public class PrivadosChatBotServiceImpl implements PrivadosChatBotService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PrivadosChatBotServiceImpl.class);
    private final ProcesosChatBot procesosChatBot;

    @Inject
    public PrivadosChatBotServiceImpl(ProcesosChatBot procesosChatBot) {
        this.procesosChatBot = procesosChatBot;
    }


    @Override
    public JsonResponseCuentasCliente posicionConsolidada(JsonRequestPosicionCliente requestPosicionCliente, String usuario) {
        LOGGER.info("inicio <posicionConsolidada>");
        RequestProcess requestProcess = new RequestProcess(requestPosicionCliente.getCodCliente(), TipoProcesoChatBot.PRV, ProcesoChatBot.CONSULTA_CUENTAS, requestPosicionCliente, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateCuentasCliente(requestProcess, requestPosicionCliente);
        //Obtener data
        JsonResponseCuentasCliente jsonResponseTarjetasDebito = procesosChatBot.getCuentasCliente(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <posicionConsolidada>");
        return jsonResponseTarjetasDebito;
    }

    @Override
    public JsonResponseMovimientosCuenta movimientosCuenta(JsonRequestMovimientosCuenta requestCliente, String usuario) {
        LOGGER.info("inicio <movimientosCuenta>");
        RequestProcess requestProcess = new RequestProcess(requestCliente.getCodCliente(), TipoProcesoChatBot.PRV, ProcesoChatBot.CONSULTA_MOVIMIENTOS, requestCliente, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateMovimientosCuenta(requestProcess, requestCliente);
        //Obtener data
        JsonResponseMovimientosCuenta jsonResponseMovimientosCuenta = procesosChatBot.getMovimientosCuenta(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <movimientosCuenta>");
        return jsonResponseMovimientosCuenta;
    }

    @Override
    public JsonResponseVencimientos vencimientosCliente(JsonRequestPosicionCliente requestCliente, String usuario) {
        LOGGER.info("inicio <vencimientosCliente>");
        RequestProcess requestProcess = new RequestProcess(requestCliente.getCodCliente(), TipoProcesoChatBot.PRV, ProcesoChatBot.CONSULTA_VENCIMIENTO, requestCliente, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateVencimientosCliente(requestProcess, requestCliente);
        //Obtener data
        JsonResponseVencimientos jsonResponseVencimientos = procesosChatBot.getVencimientosCliente(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <vencimientosCliente>");
        return jsonResponseVencimientos;
    }

    @Override
    public JsonResponsePuntosCliente puntosCliente(JsonRequestPosicionCliente requestCliente, String usuario) {
        LOGGER.info("inicio <puntosCliente>");
        RequestProcess requestProcess = new RequestProcess(requestCliente.getCodCliente(), TipoProcesoChatBot.PRV, ProcesoChatBot.CONSULTA_PUNTOS, requestCliente, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validatePuntosCliente(requestProcess, requestCliente);
        //Obtener data
        JsonResponsePuntosCliente jsonResponsePuntosCliente = procesosChatBot.getPuntosCliente(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <puntosCliente>");
        return jsonResponsePuntosCliente;
    }

    @Override
    public JsonResponseComun solicitaEstadoCuenta(JsonRequestEstadoCuenta requestEstadoCuenta, String usuario) {
        LOGGER.info("inicio <solicitaEstadoCuenta>");
        RequestProcess requestProcess = new RequestProcess(requestEstadoCuenta.getCodCliente(), TipoProcesoChatBot.PRV, ProcesoChatBot.ESTADO_CUENTA, requestEstadoCuenta, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateEstadoCuenta(requestProcess, requestEstadoCuenta);
        //Obtener data
        JsonResponseComun jsonResponseComun = procesosChatBot.getJsonResponseComun(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <solicitaEstadoCuenta>");
        return jsonResponseComun;
    }

}
