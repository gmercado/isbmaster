package com.bisa.bus.servicios.chatbot.rest;

import com.bisa.bus.servicios.chatbot.services.PrivadosChatBotService;
import com.bisa.bus.servicios.chatbot.services.types.*;
import com.bisa.isb.commtools.CommonService;
import com.google.inject.Inject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * @author by rsalvatierra on 27/09/2017.
 */
@Path("/chb/priv")
@Produces({"application/json;charset=UTF-8"})
public class PrivadosService extends CommonService {

    private static final String CONSULTA_POSICION = "/consultaPosicionConsolidada";
    private static final String CONSULTA_MOVIMIENTOS = "/consultaMovimientos";
    private static final String CONSULTA_VENCIMIENTOS = "/consultaVencimientos";
    private static final String CONSULTA_PUNTOS = "/consultaPuntos";
    private static final String SOLICITA_ESTADO_CUENTA = "/solicitaEstadoCuenta";

    @Inject
    private PrivadosChatBotService privadosChatBotService;

    /**
     * Consulta posicion consolidada
     *
     * @param httpHeaders    headers
     * @param requestCliente datos basicos
     * @return JsonResponseCuentasCliente
     */
    @POST
    @Path(CONSULTA_POSICION)
    public JsonResponseCuentasCliente consultaPosicionConsolidada(@Context HttpHeaders httpHeaders, JsonRequestPosicionCliente requestCliente) {
        if (requestCliente == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return privadosChatBotService.posicionConsolidada(requestCliente, getUser(httpHeaders));
        }
    }

    /**
     * Consulta movimientos
     *
     * @param httpHeaders    headers
     * @param requestCliente datos basicos
     * @return JsonResponseMovimientosCuenta
     */
    @POST
    @Path(CONSULTA_MOVIMIENTOS)
    public JsonResponseMovimientosCuenta consultaMovimientos(@Context HttpHeaders httpHeaders, JsonRequestMovimientosCuenta requestCliente) {
        if (requestCliente == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return privadosChatBotService.movimientosCuenta(requestCliente, getUser(httpHeaders));
        }
    }

    /**
     * Consulta vencimientos
     *
     * @param httpHeaders    headers
     * @param requestCliente datos basicos
     * @return JsonResponseCuentasCliente
     */
    @POST
    @Path(CONSULTA_VENCIMIENTOS)
    public JsonResponseVencimientos consultaVencimientos(@Context HttpHeaders httpHeaders, JsonRequestPosicionCliente requestCliente) {
        if (requestCliente == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return privadosChatBotService.vencimientosCliente(requestCliente, getUser(httpHeaders));
        }
    }

    /**
     * Consulta puntos
     *
     * @param httpHeaders    headers
     * @param requestCliente datos basicos
     * @return JsonResponseCuentasCliente
     */
    @POST
    @Path(CONSULTA_PUNTOS)
    public JsonResponsePuntosCliente consultaPuntos(@Context HttpHeaders httpHeaders, JsonRequestPosicionCliente requestCliente) {
        if (requestCliente == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return privadosChatBotService.puntosCliente(requestCliente, getUser(httpHeaders));
        }
    }

    /**
     * Solicita Estado Cuenta
     *
     * @param httpHeaders         headers
     * @param requestEstadoCuenta datos basicos
     * @return JsonResponseComun
     */
    @POST
    @Path(SOLICITA_ESTADO_CUENTA)
    public JsonResponseComun solicitaEstadoCuenta(@Context HttpHeaders httpHeaders, JsonRequestEstadoCuenta requestEstadoCuenta) {
        if (requestEstadoCuenta == null) {
            throw new WebApplicationException(getBuild(Response.Status.BAD_REQUEST));
        } else {
            return privadosChatBotService.solicitaEstadoCuenta(requestEstadoCuenta, getUser(httpHeaders));
        }
    }
}
