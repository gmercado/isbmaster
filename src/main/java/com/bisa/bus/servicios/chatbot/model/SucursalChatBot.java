package com.bisa.bus.servicios.chatbot.model;

import bus.interfaces.sqlite.entities.Cajero;
import bus.interfaces.tipos.TipoCajero;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by rsalvatierra on 05/09/2017.
 */
public class SucursalChatBot {
    public static final String SI = "SI";
    private static final String DEPOBISA = "DEPOBISA";
    private static final String DOLARES = "DOLARES";
    private String nombre;
    private String descripcion;
    private String direccion;
    private String url;
    private String telefonos;
    private String horarios;
    private String servicios;
    private String codDepartamento;
    private LocalizacionChatBot ubicacion;

    private SucursalChatBot(Cajero cajero) {
        if (cajero.getDepartamento() != null) {
            setNombre(cajero.get_id() + "-" + cajero.getDepartamento().getCodigo() + "-" + cajero.getDescripcion());
            setCodDepartamento(cajero.getDepartamento().getCodigo());
        } else {
            setNombre(cajero.get_id() + "-" + cajero.getDescripcion());
            setCodDepartamento("");
        }
        setDescripcion(cajero.getDescripcion());
        setUrl("http://bisa.com/repo/agencia.jpg");
        setDireccion(cajero.getCiudad() + ", " + cajero.getDireccion());
        setTelefonos(cajero.getTelefonos());
        setHorarios(cajero.getHorarios());
        if (TipoCajero.Cajero.equals(cajero.getTipo())) {
            StringBuilder servicios = new StringBuilder("");
            if (SI.equals(cajero.getDepobisa())) {
                servicios.append(DEPOBISA);
            }
            if (SI.equals(cajero.getDolares())) {
                if (servicios.length() != 0) {
                    servicios.append(",");
                }
                servicios.append(DOLARES);
            }
            setServicios(servicios.toString());
        } else {
            setServicios("Consultas al " + cajero.getDepobisa());
        }

        setUbicacion(new LocalizacionChatBot(cajero.getLatitud(), cajero.getLongitud()));
    }

    public static List<SucursalChatBot> getTipos(List<Cajero> cajeros) {
        List<SucursalChatBot> sucursalChatBotList = null;
        if (cajeros != null) {
            sucursalChatBotList = new ArrayList<>();
            for (Cajero cajero : cajeros) {
                sucursalChatBotList.add(new SucursalChatBot(cajero));
            }
        }
        return sucursalChatBotList;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTelefonos() {
        return telefonos;
    }

    private void setTelefonos(String telefonos) {
        this.telefonos = telefonos;
    }

    public String getHorarios() {
        return horarios;
    }

    private void setHorarios(String horarios) {
        this.horarios = horarios;
    }

    public String getServicios() {
        return servicios;
    }

    public void setServicios(String servicios) {
        this.servicios = servicios;
    }

    public String getCodDepartamento() {
        return codDepartamento;
    }

    private void setCodDepartamento(String codDepartamento) {
        this.codDepartamento = codDepartamento;
    }

    public LocalizacionChatBot getUbicacion() {
        return ubicacion;
    }

    private void setUbicacion(LocalizacionChatBot ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public String toString() {
        return "{" +
                "nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", direccion='" + direccion + '\'' +
                ", url='" + url + '\'' +
                ", telefonos='" + telefonos + '\'' +
                ", horarios='" + horarios + '\'' +
                ", servicios='" + servicios + '\'' +
                ", codDepartamento='" + codDepartamento + '\'' +
                ", ubicacion=" + ubicacion +
                '}';
    }
}
