package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.ResponseProcess;
import com.bisa.bus.servicios.chatbot.model.SaldosTCChatBot;

import java.util.List;

/**
 * @author by rsalvatierra on 29/09/2017.
 */
public class JsonResponseTarjetaCredito extends JsonResponseComun {
    private List<SaldosTCChatBot> saldos;

    public JsonResponseTarjetaCredito(ResponseProcess responseProcess) {
        super(responseProcess);
    }

    public List<SaldosTCChatBot> getSaldos() {
        return saldos;
    }

    public void setSaldos(List<SaldosTCChatBot> saldos) {
        this.saldos = saldos;
    }

    @Override
    public String toString() {
        return "{" +
                "saldos=" + saldos +
                "} " + super.toString();
    }
}
