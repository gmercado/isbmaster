package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.CotizacionChatBot;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;

import java.util.List;

/**
 * @author by rsalvatierra on 05/09/2017.
 */
public class JsonResponseCotizaciones extends JsonResponseComun {
    private List<CotizacionChatBot> cotizaciones;

    public JsonResponseCotizaciones(ResponseProcess responseProcess) {
        if (responseProcess != null) {
            setCodRespuesta(responseProcess.getCodRespuesta());
            setMensaje(responseProcess.getMensaje());
        }
    }

    public List<CotizacionChatBot> getCotizaciones() {
        return cotizaciones;
    }

    public void setCotizaciones(List<CotizacionChatBot> cotizaciones) {
        this.cotizaciones = cotizaciones;
    }
}
