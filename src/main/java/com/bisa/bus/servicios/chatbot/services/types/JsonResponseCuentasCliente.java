package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.CuentasChatBot;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;

import java.util.List;

/**
 * @author by rsalvatierra on 27/09/2017.
 */
public class JsonResponseCuentasCliente extends JsonResponseComun {
    private List<CuentasChatBot> cuentas;

    public JsonResponseCuentasCliente(ResponseProcess responseProcess) {
        super(responseProcess);
    }

    public List<CuentasChatBot> getCuentas() {
        return cuentas;
    }

    public void setCuentas(List<CuentasChatBot> cuentas) {
        this.cuentas = cuentas;
    }
}
