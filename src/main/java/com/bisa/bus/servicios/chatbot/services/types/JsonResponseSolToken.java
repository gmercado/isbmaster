package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.ResponseProcess;

/**
 * @author by rsalvatierra on 06/09/2017.
 */
public class JsonResponseSolToken extends JsonResponseComun {
    private long identificador;

    public JsonResponseSolToken(ResponseProcess responseProcess) {
        super(responseProcess);
    }

    public long getIdentificador() {
        return identificador;
    }

    public void setIdentificador(long identificador) {
        this.identificador = identificador;
    }

    @Override
    public String toString() {
        return "{" +
                "identificador=" + identificador +
                "} " + super.toString();
    }
}
