package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 23/10/2017.
 */
public class JsonRequestRecargaCelular extends JsonRequestTransaccion {
    private String telefono;
    private String telefonica;
    private String nitFactura;
    private String nombreFactura;

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefonica() {
        return telefonica;
    }

    public void setTelefonica(String telefonica) {
        this.telefonica = telefonica;
    }

    public String getNitFactura() {
        return nitFactura;
    }

    public void setNitFactura(String nitFactura) {
        this.nitFactura = nitFactura;
    }

    public String getNombreFactura() {
        return nombreFactura;
    }

    public void setNombreFactura(String nombreFactura) {
        this.nombreFactura = nombreFactura;
    }

    @Override
    public String toString() {
        return "{" +
                "telefono='" + telefono + '\'' +
                ", telefonica='" + telefonica + '\'' +
                "} " + super.toString();
    }
}
