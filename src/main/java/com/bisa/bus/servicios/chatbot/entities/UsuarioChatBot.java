package com.bisa.bus.servicios.chatbot.entities;

import bus.database.model.EntityBase;
import com.bisa.bus.servicios.chatbot.model.EstadoChatBot;
import com.bisa.bus.servicios.chatbot.model.TipoRedSocial;

import javax.persistence.*;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
@Entity
@Table(name = "ICBOT103")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I103FECCRE", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I103USRCRE", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I103FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I103USRMOD"))
})
public class UsuarioChatBot extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I103CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "I103TIPSOC", columnDefinition = "NUMERIC(1)")
    private TipoRedSocial tipoRedSocial;

    @Column(name = "I103CODSOC", columnDefinition = "VARCHAR(255)")
    private String codigoEnRedSocial;

    @Column(name = "I103NOMSOC", columnDefinition = "VARCHAR(500)")
    private String nombreEnRedSocial;

    @Enumerated(EnumType.STRING)
    @Column(name = "I103ESTSOC", columnDefinition = "VARCHAR(3)")
    private EstadoChatBot estado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoRedSocial getTipoRedSocial() {
        return tipoRedSocial;
    }

    public void setTipoRedSocial(TipoRedSocial tipoRedSocial) {
        this.tipoRedSocial = tipoRedSocial;
    }

    public String getCodigoEnRedSocial() {
        return codigoEnRedSocial;
    }

    public void setCodigoEnRedSocial(String codigoEnRedSocial) {
        this.codigoEnRedSocial = codigoEnRedSocial;
    }

    public String getNombreEnRedSocial() {
        return nombreEnRedSocial;
    }

    public void setNombreEnRedSocial(String nombreEnRedSocial) {
        this.nombreEnRedSocial = nombreEnRedSocial;
    }

    public EstadoChatBot getEstado() {
        return estado;
    }

    public void setEstado(EstadoChatBot estado) {
        this.estado = estado;
    }
}
