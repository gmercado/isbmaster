package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.ContactoChatBot;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;

import java.util.List;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class JsonResponseContactos extends JsonResponseComun {
    private List<ContactoChatBot> telefonos;

    public JsonResponseContactos(ResponseProcess responseProcess) {
        if (responseProcess != null) {
            setCodRespuesta(responseProcess.getCodRespuesta());
            setMensaje(responseProcess.getMensaje());
        }
    }


    public List<ContactoChatBot> getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(List<ContactoChatBot> telefonos) {
        this.telefonos = telefonos;
    }

    @Override
    public String toString() {
        return "{" +
                "telefonos=" + telefonos +
                '}' + super.toString();
    }
}
