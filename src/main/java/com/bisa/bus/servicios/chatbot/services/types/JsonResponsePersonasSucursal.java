package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.PersonasSucursalChatBot;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;

import java.util.List;

/**
 * @author by rsalvatierra on 15/09/2017.
 */
public class JsonResponsePersonasSucursal extends JsonResponseComun {
    private List<PersonasSucursalChatBot> cantidades;

    public JsonResponsePersonasSucursal(ResponseProcess responseProcess) {
        if (responseProcess != null) {
            setMensaje(responseProcess.getMensaje());
            setCodRespuesta(responseProcess.getCodRespuesta());
        }
    }

    public List<PersonasSucursalChatBot> getCantidades() {
        return cantidades;
    }

    public void setCantidades(List<PersonasSucursalChatBot> cantidades) {
        this.cantidades = cantidades;
    }

    @Override
    public String toString() {
        return "{" +
                "cantidades=" + cantidades +
                "} " + super.toString();
    }
}
