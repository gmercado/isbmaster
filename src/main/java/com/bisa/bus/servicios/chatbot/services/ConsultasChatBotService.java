package com.bisa.bus.servicios.chatbot.services;

import com.bisa.bus.servicios.chatbot.services.types.*;

/**
 * @author by rsalvatierra on 01/09/2017.
 */
public interface ConsultasChatBotService {
    JsonResponseDepartamentos consultaDepartamentos(JsonRequestComun requestConsulta, String usuario);

    JsonResponseTiposDocumentos consultaTiposDocumento(JsonRequestComun requestConsulta, String usuario);

    JsonResponseMonedas consultaMonedas(JsonRequestComun requestConsulta, String usuario);

    JsonResponseTelefonicas consultaTelefonicas(JsonRequestComun requestConsulta, String usuario);

    JsonResponseSucursales consultaSucusales(JsonRequestComun requestConsulta, String usuario);

    JsonResponseCajeros consultaCajeros(JsonRequestComun requestConsulta, String usuario);

    JsonResponsePromociones consultaPromociones(JsonRequestComun requestConsulta, String usuario);

    JsonResponseContactos consultaContactos(JsonRequestComun requestConsulta, String usuario);

    JsonResponseCotizaciones consultaCotizaciones(JsonRequestComun requestConsulta, String usuario);

    JsonResponsePersonasSucursal consultaPersonasEnSucursal(JsonRequestPersonasSucursal requestConsulta, String usuario);
}
