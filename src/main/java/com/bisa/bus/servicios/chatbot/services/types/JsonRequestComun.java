package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 02/09/2017.
 */
public class JsonRequestComun {
    private String codUsuario;
    private String nombre;

    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "{" +
                "codUsuario='" + codUsuario + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
