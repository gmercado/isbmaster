package com.bisa.bus.servicios.chatbot.api;

import bus.consumoweb.facturacion.model.FacturaSolicitud;
import bus.consumoweb.giromovil.GiroMovilDao;
import bus.consumoweb.giromovil.model.EmitirGiroResponse;
import bus.consumoweb.giromovil.model.GiroMovil;
import bus.consumoweb.pagoservicios.PagoServiciosDao;
import bus.consumoweb.pagoservicios.model.*;
import bus.consumoweb.yellowpepper.dao.YellowPepperServicioDao;
import bus.env.api.MedioAmbiente;
import bus.interfaces.as400.dao.AgenciaCajeroDao;
import bus.interfaces.as400.dao.ClienteDao;
import bus.interfaces.as400.dao.CuentaDao;
import bus.interfaces.as400.dao.TarjetaDebitoDao;
import bus.interfaces.as400.entities.AgenciaCajero;
import bus.interfaces.as400.entities.Cliente;
import bus.interfaces.as400.entities.Cuenta;
import bus.interfaces.as400.entities.TarjetaDebitoAutorizacion;
import bus.interfaces.ebisa.dao.BeneficiarioDao;
import bus.interfaces.ebisa.dao.PrevencionLavado;
import bus.interfaces.ebisa.dao.VerificarLimitesDao;
import bus.interfaces.ebisa.entities.Beneficiario;
import bus.interfaces.sqlite.dao.CajeroDao;
import bus.interfaces.sqlite.entities.Cajero;
import bus.interfaces.sqlite.entities.Contacto;
import bus.interfaces.ticketing.dao.AgenciaTicketDao;
import bus.interfaces.ticketing.dao.TicketDao;
import bus.interfaces.ticketing.entities.AgenciaTicket;
import bus.interfaces.tipos.EstadoTarjetaDebito;
import bus.interfaces.tipos.Pais;
import bus.interfaces.tipos.TipoCajero;
import bus.interfaces.tipos.TipoCotizacion;
import bus.interfaces.token.GeneradorPin;
import bus.interfaces.token.ParPines;
import bus.interfaces.token.TresClavesUtils;
import bus.mail.api.MailerFactory;
import bus.mail.model.Mailer;
import bus.monitor.api.*;
import bus.monitor.dao.TransaccionesClienteDao;
import bus.monitor.transaction.trxin.Reg020;
import bus.monitor.transaction.trxin.Reg021;
import bus.plumbing.cypher.GeneradorClave;
import bus.plumbing.cypher.ParClaves;
import bus.plumbing.cypher.TripleDes;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.Monedas;
import bus.plumbing.utils.Par;
import bus.plumbing.utils.TelefonoConverter;
import com.bisa.bus.servicios.chatbot.dao.*;
import com.bisa.bus.servicios.chatbot.entities.*;
import com.bisa.bus.servicios.chatbot.model.*;
import com.bisa.bus.servicios.chatbot.services.types.*;
import com.bisa.bus.servicios.mojix.model.TipoEmpresaServicio;
import com.google.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static bus.env.api.Variables.*;
import static bus.monitor.api.ParametersMonitor.*;
import static java.util.Collections.reverse;

/**
 * @author by rsalvatierra on 02/09/2017.
 */
class OperacionesChatBot {

    private static final Logger LOGGER = LoggerFactory.getLogger(OperacionesChatBot.class);

    private static final String PREPAGO = "PREPAGO";
    private static final String TIGO = "TIGO";
    private static final String ENTEL = "ENTEL";
    private static final String VIVA = "VIVA";
    private static final String SERVICIO_CHATBOT = "CB";
    private static final String APLICATION_SMS = "chatbot";
    private static final int PRIORIDAD_DEFAULT = 1;
    private static final String CAJA_AHORRO = "ca";
    private static final String CUENTA_CORRIENTE = "cc";
    private static final String TARJETA_CREDITO = "tc";

    private final OperacionChatBotDao operacionChatBotDao;
    private final UsuarioChatBotDao usuarioChatBotDao;
    private final PagoServiciosDao pagoServiciosDao;
    private final CajeroDao cajerosDao;
    private final TransaccionesClienteDao transaccionesClienteDao;
    private final ClienteDao clienteDao;
    private final CuentaDao cuentaDao;
    private final MedioAmbiente medioAmbiente;
    private final ClienteChatBotDao clienteChatBotDao;
    private final YellowPepperServicioDao yellowPepperServicioDao;
    private final SolicitudChatBotDao solicitudChatBotDao;
    private final TarjetaDebitoDao tarjetaDebitoDao;
    private final GiroMovilDao giroMovilDao;
    private final BeneficiarioDao beneficiarioDao;
    private final PrevencionLavado prevencionLavado;
    private final MailerFactory mailerFactory;
    private final AfiliacionChatBotDao afiliacionChatBotDao;
    private final UsuarioConectadoChatBotDao usuarioConectadoChatBotDao;
    private final AgenciaTicketDao agenciaTicketDao;
    private final TicketDao ticketDao;
    private final AgenciaCajeroDao agenciaCajeroDao;
    private final VerificarLimitesDao verificarLimitesDao;

    @Inject
    public OperacionesChatBot(OperacionChatBotDao operacionChatBotDao,
                              UsuarioChatBotDao usuarioChatBotDao,
                              PagoServiciosDao pagoServiciosDao,
                              CajeroDao cajerosDao,
                              TransaccionesClienteDao transaccionesClienteDao,
                              ClienteDao clienteDao,
                              CuentaDao cuentaDao,
                              MedioAmbiente medioAmbiente,
                              ClienteChatBotDao clienteChatBotDao,
                              YellowPepperServicioDao yellowPepperServicioDao,
                              SolicitudChatBotDao solicitudChatBotDao,
                              TarjetaDebitoDao tarjetaDebitoDao,
                              GiroMovilDao giroMovilDao,
                              BeneficiarioDao beneficiarioDao,
                              PrevencionLavado prevencionLavado,
                              MailerFactory mailerFactory,
                              AfiliacionChatBotDao afiliacionChatBotDao,
                              UsuarioConectadoChatBotDao usuarioConectadoChatBotDao,
                              AgenciaTicketDao agenciaTicketDao,
                              TicketDao ticketDao,
                              AgenciaCajeroDao agenciaCajeroDao,
                              VerificarLimitesDao verificarLimitesDao) {
        this.operacionChatBotDao = operacionChatBotDao;
        this.usuarioChatBotDao = usuarioChatBotDao;
        this.pagoServiciosDao = pagoServiciosDao;
        this.cajerosDao = cajerosDao;
        this.transaccionesClienteDao = transaccionesClienteDao;
        this.clienteDao = clienteDao;
        this.cuentaDao = cuentaDao;
        this.medioAmbiente = medioAmbiente;
        this.clienteChatBotDao = clienteChatBotDao;
        this.yellowPepperServicioDao = yellowPepperServicioDao;
        this.solicitudChatBotDao = solicitudChatBotDao;
        this.tarjetaDebitoDao = tarjetaDebitoDao;
        this.giroMovilDao = giroMovilDao;
        this.beneficiarioDao = beneficiarioDao;
        this.prevencionLavado = prevencionLavado;
        this.mailerFactory = mailerFactory;
        this.afiliacionChatBotDao = afiliacionChatBotDao;
        this.usuarioConectadoChatBotDao = usuarioConectadoChatBotDao;
        this.agenciaTicketDao = agenciaTicketDao;
        this.ticketDao = ticketDao;
        this.agenciaCajeroDao = agenciaCajeroDao;
        this.verificarLimitesDao = verificarLimitesDao;
    }

    List<TelefonicasChatBot> getEmpresasTelefonicas() throws ExecutionException, TimeoutException, JAXBException, IOException {
        List<TelefonicasChatBot> empresaTelefonicaMojixList = null;
        ListadoDeServiciosResponse serviciosResponse = pagoServiciosDao.consultarServicios();
        if (serviciosResponse != null && !serviciosResponse.getServicio().isEmpty()) {
            empresaTelefonicaMojixList = new ArrayList<>();
            for (Servicio servicio : serviciosResponse.getServicio()) {
                if (PREPAGO.equals(servicio.getCodigo())
                        && (TIGO.equals(servicio.getEmpresaCodigo())
                        || ENTEL.equals(servicio.getEmpresaCodigo())
                        || VIVA.equals(servicio.getEmpresaCodigo()))) {
                    LOGGER.debug("servicio -> {}", servicio);
                    empresaTelefonicaMojixList.add(new TelefonicasChatBot(servicio));
                }
            }
        }
        return empresaTelefonicaMojixList;
    }


    private UsuarioChatBot getUsuarioCB(TipoRedSocial redSocial, String codigo) {
        return usuarioChatBotDao.getByNombre(redSocial, codigo);
    }

    private UsuarioChatBot setUsuarioCB(TipoRedSocial tipoRedSocial, String codigo, String nombre, String usuario) {
        UsuarioChatBot usuarioChatBot = usuarioChatBotDao.newUsuario(tipoRedSocial);
        usuarioChatBot.setCodigoEnRedSocial(codigo);
        usuarioChatBot.setNombreEnRedSocial(nombre);
        return usuarioChatBotDao.registrar(usuarioChatBot, usuario);
    }

    public OperacionChatBot setOperacion(RequestProcess requestProcess, ResponseProcess responseProcess) {
        //Verificar usuario conectado
        UsuarioConectadoChatBot usuarioConectadoChatBot = getUsuarioConectadoChatBot(requestProcess, requestProcess.getUsuarioCanal());
        //Registrar operacion
        OperacionChatBot operacionChatBot = operacionChatBotDao.newOperacion(requestProcess.getTipoProcesoChatBot(), requestProcess.getProcesoChatBot());
        operacionChatBot.setUsuarioConectadoChatBot(usuarioConectadoChatBot);
        if (responseProcess != null) {
            operacionChatBot.setCodRespuesta(responseProcess.getCodRespuesta());
            operacionChatBot.setMensaje(responseProcess.getMensaje());
            operacionChatBot.setResultadoBusqueda(responseProcess.getResultado());
            if (responseProcess.getCodRespuesta() != 0) {
                operacionChatBot.setEstado(EstadoChatBot.ERR);
            }
        }
        operacionChatBot.setCriterioBusqueda(requestProcess.getPeticion());
        return operacionChatBotDao.registrar(operacionChatBot, requestProcess.getUsuarioCanal());
    }

    private UsuarioChatBot getUsuarioChatBot(RequestProcess requestProcess, String usuario) {
        UsuarioChatBot usuarioChatBot = null;
        //Verificar Red Social
        TipoRedSocial tipoRedSocial = TipoRedSocial.get(usuario);
        //Obtener usuario
        if (StringUtils.trimToNull(requestProcess.getCodigoUsuario()) != null) {
            usuarioChatBot = getUsuarioCB(tipoRedSocial, requestProcess.getCodigoUsuario());
            if (usuarioChatBot == null) {
                usuarioChatBot = setUsuarioCB(tipoRedSocial, requestProcess.getCodigoUsuario(), requestProcess.getNombreUsuario(), usuario);
            }
        }
        return usuarioChatBot;
    }

    private UsuarioConectadoChatBot getUsuarioConectadoChatBot(RequestProcess requestProcess, String usuario) {
        //Verificar usuario
        UsuarioChatBot usuarioChatBot = getUsuarioChatBot(requestProcess, usuario);
        //Verificar coneccion
        UsuarioConectadoChatBot usuarioConectadoChatBot = usuarioConectadoChatBotDao.getByUsuario(usuarioChatBot);
        if (usuarioConectadoChatBot == null) {
            //Verificar cliente
            ClienteChatBot clienteChatBot = null;
            if (StringUtils.trimToNull(requestProcess.getCodigoCliente()) != null) {
                clienteChatBot = clienteChatBotDao.getByCodigoCifrado(requestProcess.getCodigoCliente());
                if (clienteChatBot != null) {
                    usuarioConectadoChatBot = usuarioConectadoChatBotDao.getByCliente(clienteChatBot);
                }
            }
            if (usuarioConectadoChatBot == null) {
                usuarioConectadoChatBot = usuarioConectadoChatBotDao.newConect();
                usuarioConectadoChatBot.setUsuarioChatBot(usuarioChatBot);
                if (usuarioChatBot == null) {
                    usuarioConectadoChatBot.setClienteChatBot(null);
                    usuarioConectadoChatBot.setEstadoUsuarioConectado(EstadoChatBot.INA);
                } else {
                    usuarioConectadoChatBot.setClienteChatBot(clienteChatBot);
                }
                usuarioConectadoChatBot.setFechaUltimaTransaccion(new Date());
                return usuarioConectadoChatBotDao.registrar(usuarioConectadoChatBot, usuario);
            }
        }
        usuarioConectadoChatBot.setFechaUltimaTransaccion(new Date());
        return usuarioConectadoChatBotDao.actualizar(usuarioConectadoChatBot, usuario);
    }

    List<Cajero> getCajeros(TipoCajero tipoCajero) {
        String consultaSQLLite = medioAmbiente.getValorDe(IR_CONEXION_SQLITE, IR_CONEXION_SQLITE_DEFAULT);
        if (Boolean.parseBoolean(consultaSQLLite)) {
            return cajerosDao.getCajeros(tipoCajero);
        } else {
            List<Cajero> cajeros = new ArrayList<>();
            List<AgenciaCajero> agenciaCajeros = agenciaCajeroDao.getAgenciasActivas(tipoCajero);
            for (AgenciaCajero agenciaCajero : agenciaCajeros) {
                cajeros.add(new Cajero(agenciaCajero));
            }
            return cajeros;
        }
    }

    void setCajeros(TipoCajero tipoCajero, String usuario) {
        List<Cajero> cajeros = cajerosDao.getCajeros(tipoCajero);
        for (Cajero cajero : cajeros) {
            try {
                agenciaCajeroDao.registrar(new AgenciaCajero(cajero), usuario);
            } catch (Exception e) {
                LOGGER.info("cajero {}", cajero);
                LOGGER.error("hobo un error", e);
            }
        }
    }

    List<Contacto> getContactos() {
        return cajerosDao.getContactos();
    }

    List<CotizacionChatBot> getCotizaciones() {
        IRespuestasMonitor monitor;
        List<CotizacionChatBot> cotizacionChatBots = null;
        try {
            monitor = transaccionesClienteDao.consultaCotizacion(getDevice());
            if (monitor != null) {
                cotizacionChatBots = new ArrayList<>();
                while (monitor.next1()) {
                    cotizacionChatBots.add(new CotizacionChatBot(Monedas.BISA_DOLARES, TipoCotizacion.Oficial, monitor.getDatoRecibidoCuerpo1Decimal(MON_COT_USD_BOB_OF)));
                    cotizacionChatBots.add(new CotizacionChatBot(Monedas.BISA_DOLARES, TipoCotizacion.Compra, monitor.getDatoRecibidoCuerpo1Decimal(MON_COT_USD_BOB_COM)));
                    cotizacionChatBots.add(new CotizacionChatBot(Monedas.BISA_DOLARES, TipoCotizacion.Venta, monitor.getDatoRecibidoCuerpo1Decimal(MON_COT_USD_BOB_VEN)));
                    cotizacionChatBots.add(new CotizacionChatBot(Monedas.BISA_EUROS, TipoCotizacion.Oficial, monitor.getDatoRecibidoCuerpo1Decimal(MON_COT_EUR_BOB_OF)));
                    cotizacionChatBots.add(new CotizacionChatBot(Monedas.BISA_EUROS, TipoCotizacion.Compra, monitor.getDatoRecibidoCuerpo1Decimal(MON_COT_EUR_BOB_COM)));
                    cotizacionChatBots.add(new CotizacionChatBot(Monedas.BISA_EUROS, TipoCotizacion.Venta, monitor.getDatoRecibidoCuerpo1Decimal(MON_COT_EUR_BOB_VEN)));
                    cotizacionChatBots.add(new CotizacionChatBot(Monedas.BISA_UFV, TipoCotizacion.Oficial, monitor.getDatoRecibidoCuerpo1Decimal(MON_COT_UFV_BOB_OF)));
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error al procesar cotizaciones ", e);
        }
        return cotizacionChatBots;
    }

    List<PersonasSucursalChatBot> getPersonasSucursal(String nombreSucursal) {
        List<PersonasSucursalChatBot> personasSucursalChatBotList = new ArrayList<>();
        try {
            String[] datosSucursal = nombreSucursal.split("-");
            String codigoSucursal = datosSucursal[0];
            int cantidad = getCantidadPersonasSucursal(codigoSucursal);
            LOGGER.info("sucrsal > {} cantidad > {} ", codigoSucursal, cantidad);
            personasSucursalChatBotList.add(new PersonasSucursalChatBot("CAJAS_EN_ESPERA", cantidad));//CAJAS+ATENCION
        } catch (Exception e) {
            LOGGER.error("Hubo un error en la consulta de personas sucursal", e);
            personasSucursalChatBotList.add(new PersonasSucursalChatBot("CAJAS_EN_ESPERA", 0));
        }
        return personasSucursalChatBotList;
    }

    public Cliente getCliente(String numeroDocumento, BigDecimal fechaJuliana) {
        //Obtener cliente
        List<Cliente> clienteList = clienteDao.getListaClientesByDocumentoAndFecNacimiento(numeroDocumento, fechaJuliana);

        if (clienteList.size() == 1) {
            return clienteList.get(0);
        } else {
            return null;
        }
    }

    private String getCypher(String password) throws NoSuchAlgorithmException {
        GeneradorClave generadorClave = new GeneradorClave(medioAmbiente);
        ParClaves parClaves = generadorClave.secret(password);
        return parClaves.getClaveHasheado();
    }

    ClienteChatBot getClienteChatBot(String codCliente, String codUsuario, String usuario) throws NoSuchAlgorithmException {
        ClienteChatBot clienteChatBot = null;
        if (codCliente != null) {
            clienteChatBot = clienteChatBotDao.getByCodigoCliente(codCliente);
            //Generar encriptado
            if (clienteChatBot == null) {
                clienteChatBot = new ClienteChatBot();
                clienteChatBot.setCodigoCliente(codCliente);
                clienteChatBot.setCodigoClienteBisa(getCypher(codCliente));
                clienteChatBot = clienteChatBotDao.registrar(clienteChatBot, usuario);
            }
            if (StringUtils.trimToNull(codUsuario) != null) {
                UsuarioChatBot usuarioChatBot = getUsuarioCB(TipoRedSocial.get(usuario), codUsuario);
                UsuarioConectadoChatBot usuarioConectadoChatBot = usuarioConectadoChatBotDao.getByUsuario(usuarioChatBot);
                if (usuarioConectadoChatBot != null) {
                    usuarioConectadoChatBot.setEstadoUsuarioConectado(EstadoChatBot.INA);
                    usuarioConectadoChatBotDao.actualizar(usuarioConectadoChatBot, usuario);
                }
                usuarioConectadoChatBot = usuarioConectadoChatBotDao.getByCliente(clienteChatBot);
                if (usuarioConectadoChatBot != null) {
                    usuarioConectadoChatBot.setEstadoUsuarioConectado(EstadoChatBot.INA);
                    usuarioConectadoChatBotDao.actualizar(usuarioConectadoChatBot, usuario);
                }
                usuarioConectadoChatBot = usuarioConectadoChatBotDao.newConect();
                usuarioConectadoChatBot.setUsuarioChatBot(usuarioChatBot);
                usuarioConectadoChatBot.setClienteChatBot(clienteChatBot);
                usuarioConectadoChatBot.setFechaUltimaTransaccion(new Date());
                usuarioConectadoChatBotDao.registrar(usuarioConectadoChatBot, usuario);
            }
        }
        return clienteChatBot;
    }


    ClienteChatBot getClienteByCodigo(String codClienteCypher) {
        //Obtener cliente
        return clienteChatBotDao.getByCodigoCifrado(codClienteCypher);
    }

    boolean realizoContrato(String numeroCliente) {
        return clienteChatBotDao.existeContrato(numeroCliente) != null;
    }

    void enviarSMS(String nroTelefono, String nroDocumento, String titulo, String mensaje, String usuario) {
        yellowPepperServicioDao.sendNotificacionYellowPepper(StringUtils.trimToEmpty(nroTelefono),
                titulo,
                StringUtils.trimToEmpty(mensaje),
                StringUtils.trimToEmpty(nroDocumento),
                StringUtils.trimToEmpty(usuario),
                APLICATION_SMS,
                SERVICIO_CHATBOT,
                PRIORIDAD_DEFAULT);
    }

    SolicitudChatBot generarClave(String operacion, OperacionChatBot operacionChatBot, String usuario) throws NoSuchAlgorithmException {
        SolicitudChatBot solicitudChatBot = solicitudChatBotDao.newSolicitud(operacion);
        DateTime now = DateTime.now();
        String horasEntrePedidos = medioAmbiente.getValorDe(CB_HORAS_ENTRE_PEDIDOS_QUE_ANULAN, CB_HORAS_ENTRE_PEDIDOS_QUE_ANULAN_DEFAULT);
        int cantidadDigitos = medioAmbiente.getValorIntDe(CB_CANTIDAD_DE_DIGITOS, CB_CANTIDAD_DE_DIGITOS_DEFAULT);
        ParPines nuevoPin = TresClavesUtils.actualizarClaveMovil(solicitudChatBot, cantidadDigitos, horasEntrePedidos);
        solicitudChatBot.setOperacionChatBot(operacionChatBot);
        solicitudChatBot.setTiempoVida(now.plusMinutes(getTiempoVida()).toDate());
        solicitudChatBot = solicitudChatBotDao.registrar(solicitudChatBot, usuario);
        solicitudChatBot.setTokenSMS(nuevoPin.getPin());
        return solicitudChatBot;
    }

    private int getTiempoVida() {
        return medioAmbiente.getValorIntDe(CB_MENSAJE_TIEMPO_DE_VIDA_TOKEN_MINUTOS, CB_MENSAJE_TIEMPO_DE_VIDA_TOKEN_MINUTOS_DEFAULT);
    }

    public Cliente getCliente(String codClienteCypher) {
        return clienteDao.getByCodigoCliente(clienteChatBotDao.getByCodigoCifrado(codClienteCypher).getCodigoCliente());
    }

    SolicitudChatBot getSolicitud(JsonRequestConfToken requestConfToken) {
        return solicitudChatBotDao.getByProceso(requestConfToken.getIdentificador(), requestConfToken.getOperacion());
    }

    boolean validarToken(String token, String hash) throws NoSuchAlgorithmException {
        //Validar el código enviado a tu celular
        GeneradorPin generadorPin = new GeneradorPin();
        return generadorPin.verificar(token, hash);
    }

    void setConfirmarClave(SolicitudChatBot solicitudChatBot, String usuario) {
        solicitudChatBot.setTokenUsado(EstadoToken.U);
        solicitudChatBot.setEstadoSolicitud(EstadoChatBot.PRC);
        solicitudChatBotDao.actualizar(solicitudChatBot, usuario);
    }

    List<TarjetasDebitoChatBot> getTarjetas(Cliente cliente, int tipoConsulta) throws SistemaCerradoException, TransaccionNoEjecutadaCorrectamenteException, TransaccionEfectivaException, IOException, ImposibleLeerRespuestaException {
        IRespuestasMonitor monitor;
        List<TarjetasDebitoChatBot> tarjetasList = null;
        monitor = transaccionesClienteDao.consultaCuenta(getDevice(), cliente);
        if (monitor != null) {
            tarjetasList = new ArrayList<>();
            while (monitor.next1()) {
                String tipo = StringUtils.trimToEmpty(monitor.getDatoRecibidoCuerpo1String(MON_TPOCTA));
                String estado = StringUtils.trimToEmpty(monitor.getDatoRecibidoCuerpo1String(MON_ESTADO));
                String numero = (monitor.getDatoRecibidoCuerpo1Decimal(MON_CUENTA)).toString();
                if ("E".equals(tipo)) {
                    if ((TipoConsultaTD.TODAS.ordinal() + 1) == tipoConsulta) {
                        tarjetasList.add(new TarjetasDebitoChatBot(encriptar(numero), numero));
                    } else {
                        if ("A".equals(StringUtils.trimToEmpty(estado))) {
                            tarjetasList.add(new TarjetasDebitoChatBot(encriptar(numero), numero));
                        }
                    }
                }
            }
        }
        return tarjetasList;
    }

    private String encriptar(String cadena) {

        String nombreAlgoritmo = medioAmbiente.getValorDe(CB_ALGORITMO_ENCRIPTACION, CB_ALGORITMO_ENCRIPTACION_DEFAULT);
        String llaveEncriptacion = medioAmbiente.getValorDe(CB_LLAVE_ENCRIPTACION, CB_LLAVE_ENCRIPTACION_DEFAULT);

        TripleDes tripleDes = new TripleDes(nombreAlgoritmo, llaveEncriptacion);
        return tripleDes.encrypt(cadena);
    }


    private String desEncriptar(String cadena) {
        String nombreAlgoritmo = medioAmbiente.getValorDe(CB_ALGORITMO_ENCRIPTACION, CB_ALGORITMO_ENCRIPTACION_DEFAULT);
        String llaveEncriptacion = medioAmbiente.getValorDe(CB_LLAVE_ENCRIPTACION, CB_LLAVE_ENCRIPTACION_DEFAULT);

        TripleDes tripleDes = new TripleDes(nombreAlgoritmo, llaveEncriptacion);
        return StringUtils.trimToNull(tripleDes.decrypt(cadena));
    }

    List<CuentasChatBot> getCuentas(Cliente cliente, String tipoConsulta) {
        IRespuestasMonitor monitor;
        List<CuentasChatBot> cuentasChatBots = null;
        try {
            monitor = transaccionesClienteDao.consultaCuenta(getDevice(), cliente);
            if (monitor != null) {
                cuentasChatBots = new ArrayList<>();
                while (monitor.next1()) {
                    String tipo = StringUtils.trimToEmpty(monitor.getDatoRecibidoCuerpo1String(MON_TPOCTA));
                    String estado = StringUtils.trimToEmpty(monitor.getDatoRecibidoCuerpo1String(MON_ESTADO));
                    String moneda = Monedas.getDescripcionISOByBisa(monitor.getDatoRecibidoCuerpo1Decimal(MON_CODMONEDA).shortValue());
                    String numero = (monitor.getDatoRecibidoCuerpo1Decimal(MON_CUENTA)).toString();
                    String nombre = monitor.getDatoRecibidoCuerpo1String(MON_NOMBRE);
                    //BigDecimal saldo = monitor.getDatoRecibidoCuerpo1Decimal(MON_SALDO1);
                    BigDecimal saldo2 = monitor.getDatoRecibidoCuerpo1Decimal(MON_SALDO2);
                    if ("1".equals(estado)) {
                        if ((CAJA_AHORRO.equals(tipoConsulta) || "all".equals(tipoConsulta)) && "A".equals(tipo)) {
                            cuentasChatBots.add(new CuentasChatBot(encriptar(numero), numero, CAJA_AHORRO, saldo2, moneda));
                        } else if ((CUENTA_CORRIENTE.equals(tipoConsulta) || "all".equals(tipoConsulta)) && "C".equals(tipo)) {
                            cuentasChatBots.add(new CuentasChatBot(encriptar(numero), numero, CUENTA_CORRIENTE, saldo2, moneda));
                        }
                    } else if (!"I".equals(estado)) {
                        if ((TARJETA_CREDITO.equals(tipoConsulta) || "all".equals(tipoConsulta)) && "T".equals(tipo)) {
                            cuentasChatBots.add(new CuentasChatBot(encriptar(numero), nombre, TARJETA_CREDITO, saldo2, moneda));
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error al procesar getCuentas ", e);
        }
        return cuentasChatBots;
    }

    public Cuenta getCuenta(String codCuentaHash) {
        String codCuenta = desEncriptar(codCuentaHash);
        return cuentaDao.getByNumero(Long.parseLong(codCuenta));
    }

    public String getNombreProducto(Cuenta cuenta) {
        return cuentaDao.getProducto(cuenta.getTipoCuenta(), cuenta.getProducto().longValue());
    }

    List<MovimientosChatBot> getMovimientoCuenta(Cuenta cuenta) {
        IRespuestasMonitor monitor;
        List<MovimientosChatBot> cuentasChatBots = null;
        try {
            int cantidadMovimientos = medioAmbiente.getValorIntDe(CB_CANTIDAD_MOVIMIENTOS, CB_CANTIDAD_MOVIMIENTOS_DEFAULT);
            monitor = transaccionesClienteDao.consultaMovimientosCuenta(getDevice(), cuenta, cantidadMovimientos);
            if (monitor != null) {
                cuentasChatBots = new ArrayList<>();
                int cantidad = 0;
                while (monitor.next2()) {
                    cantidad++;
                    BigDecimal fecha = monitor.getDatoRecibidoCuerpo2Decimal(MON_FECHA);
                    BigDecimal hora = monitor.getDatoRecibidoCuerpo2Decimal(MON_HORA);
                    String debcre = StringUtils.trimToNull(monitor.getDatoRecibidoCuerpo2String(MON_DEBCRE));
                    BigDecimal monto = monitor.getDatoRecibidoCuerpo2Decimal(MON_MONTOTRAN);
                    String moneda = Monedas.getDescripcionISOByBisa(cuenta.getCodigoMoneda());
                    String desc = StringUtils.trimToNull(monitor.getDatoRecibidoCuerpo2String(MON_GLOSA));
                    //String desc2 = monitor.getDatoRecibidoCuerpo2String(MON_GLOSA2);
                    String usr2 = monitor.getDatoRecibidoCuerpo2Decimal(MON_USUARIO).toPlainString();
                    String seq = monitor.getDatoRecibidoCuerpo2Decimal(MON_NUMSEQ).toPlainString();
                    cuentasChatBots.add(new MovimientosChatBot(usr2 + "-" + seq + "-" + cantidad, fecha, hora, monto, moneda, desc, debcre));
                }
                reverse(cuentasChatBots);
            }
        } catch (Exception e) {
            LOGGER.error("Error al procesar getMovimientoCuenta ", e);
        }
        return cuentasChatBots;
    }

    Par<ArrayList<ExtractoChatBot>, HashMap<String, String>> getExtracto(Cuenta cuenta) {
        IRespuestasMonitor monitor;
        ArrayList<ExtractoChatBot> extractoChatBots = null;
        HashMap<String, String> datosCuenta = null;
        String nombreCuenta;
        String descEstadoCuenta;
        try {
            int cantidadMovimientos = medioAmbiente.getValorIntDe(CB_CANTIDAD_MOVIMIENTOS_EXTRACTO, CB_CANTIDAD_MOVIMIENTOS_EXTRACTO_DEFAULT);
            monitor = transaccionesClienteDao.consultaMovimientosCuenta(getDevice(), cuenta, cantidadMovimientos);
            if (monitor != null) {
                extractoChatBots = new ArrayList<>();
                datosCuenta = new HashMap<>();
                while (monitor.next1()) {
                    nombreCuenta = monitor.getDatoRecibidoCuerpo1String(MON_NOMBRE);
                    descEstadoCuenta = monitor.getDatoRecibidoCuerpo1String(MON_DESCEST);

                    datosCuenta.put("NOMBRE_CUENTA", nombreCuenta);
                    datosCuenta.put("DESC_ESTADO_CUENTA", descEstadoCuenta);
                }
                while (monitor.next2()) {
                    BigDecimal fecha = monitor.getDatoRecibidoCuerpo2Decimal(MON_FECHA_PROCESO);
                    BigDecimal hora = monitor.getDatoRecibidoCuerpo2Decimal(MON_HORA);
                    BigDecimal nroCheque = monitor.getDatoRecibidoCuerpo2Decimal(MON_NUMCHQ);
                    String glosa = StringUtils.trimToNull(monitor.getDatoRecibidoCuerpo2String(MON_GLOSA));
                    String debcre = StringUtils.trimToNull(monitor.getDatoRecibidoCuerpo2String(MON_DEBCRE));
                    BigDecimal importe = monitor.getDatoRecibidoCuerpo2Decimal(MON_MONTOTRAN);
                    BigDecimal negsal = monitor.getDatoRecibidoCuerpo2Decimal(MON_NEGSAL);
                    BigDecimal saldo = monitor.getDatoRecibidoCuerpo2Decimal(MON_SALDO3);
                    String glosaAdicional = StringUtils.trimToNull(monitor.getDatoRecibidoCuerpo2String(MON_GLOSA2));

                    extractoChatBots.add(new ExtractoChatBot(fecha, hora, nroCheque, importe, saldo, glosa, debcre, negsal, glosaAdicional));
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error al procesar getExtracto ", e);
        }
        return new Par<>(extractoChatBots, datosCuenta);
    }

    boolean setBloqueaTarjetaDebito(String codTarjetaHash, String tipoOperacion, String causal) throws SistemaCerradoException, TransaccionNoEjecutadaCorrectamenteException, TransaccionEfectivaException, IOException, ImposibleLeerRespuestaException {
        String tarjeta = desEncriptar(codTarjetaHash);
        IRespuestasMonitor monitor = transaccionesClienteDao.bloqueaTarjetaDebito(getDevice(), tarjeta, tipoOperacion, causal);
        return monitor != null;
    }

    boolean setHabilitaTarjetaDebito(Cliente cliente, JsonRequestHabTarjetaExterior requestHabTarjetaExterior, String usuario) {
        String codCliente = cliente.getCodCliente();
        if (StringUtils.isNotEmpty(codCliente) && codCliente.length() > 6) {
            codCliente = codCliente.substring(4);
        }
        String codTarjeta = desEncriptar(requestHabTarjetaExterior.getCodTarjeta());
        /*List<TarjetaDebitoAutorizacion> tarjetaDebitoAutorizacions = tarjetaDebitoDao.getActivaciones(codCliente);
        if (tarjetaDebitoAutorizacions != null) {
            for (TarjetaDebitoAutorizacion tarjetaDebitoAutorizacion : tarjetaDebitoAutorizacions) {
                LOGGER.info(" tarjetas {}", tarjetaDebitoAutorizacion);
            }
        }*/
        LOGGER.info("codcliente {} codtarjeta {}", codCliente, codTarjeta);
        TarjetaDebitoAutorizacion tarjetaDebitoAutorizacion = new TarjetaDebitoAutorizacion();
        tarjetaDebitoAutorizacion.setCodCliente(codCliente);
        tarjetaDebitoAutorizacion.setNroTarjeta(codTarjeta);
        tarjetaDebitoAutorizacion.setCodPais(Pais.DEFAULT_CODIGO);
        tarjetaDebitoAutorizacion.setPais(Pais.DEFAULT_NOMBRE_PAIS);
        tarjetaDebitoAutorizacion.setTipoTransaccion(TarjetaDebitoAutorizacion.TRANS_EXTERIOR);
        tarjetaDebitoAutorizacion.setEstado(EstadoTarjetaDebito.ACTIVO.getEstado());
        tarjetaDebitoAutorizacion.setMonto(TarjetaDebitoAutorizacion.MONTO_POR_DEFECTO);
        tarjetaDebitoAutorizacion.setFechaDesde(FormatosUtils.getFechaByFormatoYYYY(requestHabTarjetaExterior.getDesde()));
        tarjetaDebitoAutorizacion.setFechaHasta(FormatosUtils.getFechaByFormatoYYYY(requestHabTarjetaExterior.getHasta()));
        tarjetaDebitoAutorizacion.setTrnATM(TarjetaDebitoAutorizacion.TRANS_ACTIVA);
        tarjetaDebitoAutorizacion.setTrnPOS(TarjetaDebitoAutorizacion.TRANS_ACTIVA);
        tarjetaDebitoAutorizacion.setUsuario(usuario);
        tarjetaDebitoAutorizacion.setIp("WS");
        return tarjetaDebitoDao.guardarAutorizacion(tarjetaDebitoAutorizacion, getCanal());
    }

    List<CuentasActivasChatBot> getVencimientos(Cliente cliente, String tipoConsulta) {
        IRespuestasMonitor monitor;
        List<BigDecimal> tarjetas = null;
        List<BigDecimal> prestamos = null;
        try {
            monitor = transaccionesClienteDao.consultaCuenta(getDevice(), cliente);
            if (monitor != null) {
                tarjetas = new ArrayList<>();
                prestamos = new ArrayList<>();
                while (monitor.next1()) {
                    String tipo = StringUtils.trimToEmpty(monitor.getDatoRecibidoCuerpo1String(MON_TPOCTA));
                    BigDecimal cuenta = monitor.getDatoRecibidoCuerpo1Decimal(MON_CUENTA);
                    if ("T".equals(tipo)) {
                        if (!tarjetas.contains(cuenta)) {
                            tarjetas.add(cuenta);
                        }
                    } else if ("P".equals(tipo)) {
                        if (!prestamos.contains(cuenta)) {
                            prestamos.add(cuenta);
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error al procesar getCuentas ", e);
        }
        List<CuentasActivasChatBot> cuentasActivasChatBots = null;
        try {
            if (tarjetas != null && tarjetas.size() > 0
                    && ("all".equals(tipoConsulta) || TARJETA_CREDITO.equals(tipoConsulta))) {
                for (BigDecimal numTarjeta : tarjetas) {
                    monitor = transaccionesClienteDao.consultaTarjetaCredito(getDevice(), numTarjeta);
                    if (monitor != null) {
                        cuentasActivasChatBots = new ArrayList<>();
                        while (monitor.next1()) {
                            String numero2 = (monitor.getDatoRecibidoCuerpo1Decimal("WPNUTC")).toString();
                            String moneda = Monedas.getDescripcionISOByBisa(monitor.getDatoRecibidoCuerpo1Decimal("WPMONE").shortValue());
                            BigDecimal montoAdeudado = monitor.getDatoRecibidoCuerpo1Decimal("WPSAAC");
                            BigDecimal montoMinimo = monitor.getDatoRecibidoCuerpo1Decimal("WPPAMI");
                            BigDecimal fechaProximoPago = monitor.getDatoRecibidoCuerpo1Decimal("WPVEPA");
                            BigDecimal fechaVencimiento = monitor.getDatoRecibidoCuerpo1Decimal("WPVETC");
                            BigDecimal fechaCierre = monitor.getDatoRecibidoCuerpo1Decimal("WFECCIE");
                            cuentasActivasChatBots.add(new CuentasActivasChatBot(encriptar(numero2), numero2, fechaCierre, fechaVencimiento, fechaProximoPago, montoAdeudado, montoMinimo, moneda, TARJETA_CREDITO));
                        }
                    }
                }
            }
            if (prestamos != null && prestamos.size() > 0
                    && ("all".equals(tipoConsulta) || "pp".equals(tipoConsulta))) {
                for (BigDecimal numPrestamo : prestamos) {
                    monitor = transaccionesClienteDao.consultaPrestamo(getDevice(), numPrestamo);
                    if (monitor != null) {
                        if (cuentasActivasChatBots == null) {
                            cuentasActivasChatBots = new ArrayList<>();
                        }
                        while (monitor.next1()) {
                            String numero2 = (monitor.getDatoRecibidoCuerpo1Decimal("WNOTE")).toString();
                            String moneda = Monedas.getDescripcionISOByBisa(monitor.getDatoRecibidoCuerpo1Decimal("WCMCN").shortValue());
                            BigDecimal montoAdeudado = monitor.getDatoRecibidoCuerpo1Decimal("WTODEU");
                            BigDecimal montoMinimo = monitor.getDatoRecibidoCuerpo1Decimal("WBAL");
                            BigDecimal fechaProximoPago = monitor.getDatoRecibidoCuerpo1Decimal("WFEPRX");
                            BigDecimal fechaVencimiento = monitor.getDatoRecibidoCuerpo1Decimal("WNXMT");
                            BigDecimal fechaCierre = BigDecimal.ZERO;
                            cuentasActivasChatBots.add(new CuentasActivasChatBot(encriptar(numero2), numero2, fechaCierre, fechaVencimiento, fechaProximoPago, montoAdeudado, montoMinimo, moneda, "pp"));
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error al procesar getCuentas ", e);
        }
        return cuentasActivasChatBots;
    }

    List<PuntosChatBot> getPuntos(Cliente cliente) {
        List<PuntosChatBot> puntos = new ArrayList<>();
        long cantidadPuntos = 0;
        puntos.add(new PuntosChatBot("CONSUMOS_POR_TARJETA", new BigDecimal(cantidadPuntos)));
        return puntos;
    }

    List<SaldosTCChatBot> getDatosTC(String codTarjeta) throws SistemaCerradoException, TransaccionNoEjecutadaCorrectamenteException, TransaccionEfectivaException, IOException, ImposibleLeerRespuestaException {
        List<SaldosTCChatBot> saldosTCChatBots = new ArrayList<>();
        BigDecimal numTarjeta = new BigDecimal(desEncriptar(codTarjeta));
        IRespuestasMonitor monitor = transaccionesClienteDao.consultaTarjetaCredito(getDevice(), numTarjeta);
        if (monitor != null) {
            while (monitor.next1()) {
                String moneda = Monedas.getDescripcionISOByBisa(monitor.getDatoRecibidoCuerpo1Decimal("WPMONE").shortValue());
                BigDecimal montoAdeudado = monitor.getDatoRecibidoCuerpo1Decimal("WPSAAC");
                BigDecimal montoMinimo = monitor.getDatoRecibidoCuerpo1Decimal("WPPAMI");
                saldosTCChatBots.add(new SaldosTCChatBot(1, montoMinimo, moneda));
                saldosTCChatBots.add(new SaldosTCChatBot(2, montoAdeudado, moneda));
            }
        }
        return saldosTCChatBots;
    }

    String procesarGiroMovil(JsonRequestGiroMovil requestGiroMovil, Cliente cliente, Cuenta cuentaOrigen) {
        GiroMovil giroMovil = new GiroMovil();
        //Beneficiario
        giroMovil.setCanal(getCanal());
        giroMovil.setDocIdentidadBeneficiario(requestGiroMovil.getNumeroDocumento());
        giroMovil.setTipoIdentidadBeneficiario(requestGiroMovil.getTipoDocumento());
        giroMovil.setNombreCompletoBeneficiario(requestGiroMovil.getNombreBeneficiario());
        giroMovil.setCelularBeneficiario(TelefonoConverter.getCodigoInternacional(requestGiroMovil.getTelefono()));
        //Remitente
        giroMovil.setRemitente(cliente.getNombreCompleto());
        giroMovil.setCelularRemitente(TelefonoConverter.getCodigoInternacional(cliente.getTelefono()));
        //Datos de la transaccion
        giroMovil.setCuenta(cuentaOrigen.getNumeroCuenta());
        giroMovil.setTipoCuenta(cuentaOrigen.getTipoCta());
        giroMovil.setMonedaCuenta(cuentaOrigen.getCodigoMoneda());
        giroMovil.setImporte(requestGiroMovil.getMonto());
        giroMovil.setMoneda(Monedas.getBisaByISO(requestGiroMovil.getMoneda()));
        giroMovil.setMotivo(requestGiroMovil.getMotivo());
        EmitirGiroResponse emitirGiroResponse = giroMovilDao.procesarGiroMovil(giroMovil);
        LOGGER.debug("resultado giro movil {}", emitirGiroResponse);
        return emitirGiroResponse != null ? emitirGiroResponse.getEstado() : null;
    }

    String compraCredito(JsonRequestRecargaCelular requestRecarga, Cliente cliente, Cuenta cuentaOrigen) throws ExecutionException, TimeoutException, JAXBException, IOException {
        Servicio servicio = getServicio(requestRecarga.getTelefonica());
        Busqueda busqueda = new Busqueda();
        busqueda.setTipo(servicio.getTipo());
        busqueda.setCi(cliente.getDocumento());
        busqueda.setImporte(requestRecarga.getMonto());
        busqueda.setMoneda(Monedas.getBisaByISO(requestRecarga.getMoneda()));
        busqueda.setValor(String.valueOf(requestRecarga.getTelefono()));
        busqueda.setValor2(cliente.getDocumento());
        busqueda.setNit(requestRecarga.getNitFactura());
        busqueda.setRazonSocial(requestRecarga.getNombreFactura());
        BigDecimal nroCliente = new BigDecimal(cliente.getCodCliente());
        ElementoBusqueda elementoBusqueda = new ElementoBusqueda();
        FavoritoPagoServicios fav = new FavoritoPagoServicios();
        ConsultaCuentasResponse consultaCuentasResponse = pagoServiciosDao.consultarCuentas(servicio, busqueda, elementoBusqueda, nroCliente, fav);
        PagoDeudaDeServiciosResponse pagoDeudaDeServiciosResponse;
        LOGGER.info("consulta de cuenta -> {}", consultaCuentasResponse);
        if (consultaCuentasResponse != null) {
            ConsultaCuentasDeServiciosResponse consultaCuentasDeServiciosResponse = consultaCuentasResponse.getResponse();
            List<bus.consumoweb.pagoservicios.model.Cuenta> cuentaList = consultaCuentasDeServiciosResponse.getCuenta();
            if (cuentaList != null && cuentaList.size() == 1) {
                bus.consumoweb.pagoservicios.model.Cuenta cuenta = cuentaList.stream().findFirst().orElse(null);
                if (cuenta.getDeuda() != null) {
                    PagoDeudaServicio pagoDeudaServicio = new PagoDeudaServicio();
                    pagoDeudaServicio.setBusqueda(busqueda);
                    pagoDeudaServicio.setServicio(servicio);
                    pagoDeudaServicio.setCuenta(cuenta);
                    pagoDeudaServicio.setCuentaDebito(String.valueOf(cuentaOrigen.getNumeroCuenta()));
                    pagoDeudaServicio.setMonedaDebito(String.valueOf(cuentaOrigen.getCodigoMoneda()));
                    pagoDeudaServicio.setMonedaServicio(String.valueOf(Monedas.getBisaByISO(requestRecarga.getMoneda())));
                    pagoDeudaServicio.setTipoCuentaDebito(cuentaOrigen.getTipoCta());
                    pagoDeudaServicio.setFactura(cuenta.getDeuda().getFactura().stream().findFirst().orElse(null));
                    if (pagoDeudaServicio.getFactura() != null) {
                        pagoDeudaServicio.setTipoEntrega(TipoEntrega.AGENCIA);
                        //pagoDeudaServicio.setCiudad();
                        //pagoDeudaServicio.setDepartamento();
                        //pagoDeudaServicio.setDireccion();
                        pagoDeudaServicio.setAgencia(String.valueOf(cuentaOrigen.getAgencia()));
                        FacturaSolicitud facturaSolicitud = new FacturaSolicitud();
                        facturaSolicitud.setNombreFactura(requestRecarga.getNombreFactura());
                        facturaSolicitud.setNitciFactura(requestRecarga.getNitFactura());
                        pagoDeudaServicio.setFacturaSolicitud(facturaSolicitud);
                        pagoDeudaServicio.setNroCliente(cliente.getCodCliente());
                        pagoDeudaServicio.setIdUsuario(cliente.getDocumento());
                        pagoDeudaServicio.setFullName(cliente.getNombreCompleto());
                        pagoDeudaServicio.setUserName(cliente.getDocumento());
                        pagoDeudaDeServiciosResponse = pagoServiciosDao.pagarServicioConIntermediario(pagoDeudaServicio);
                        LOGGER.info("recarga -> {}", pagoDeudaDeServiciosResponse);
                        if (pagoDeudaDeServiciosResponse != null) {
                            return ("PAG".equals(pagoDeudaDeServiciosResponse.getEstado())) ? "0" : "-1";
                        }
                    }
                }

            }
        }
        return "-1";
    }

    public Servicio getServicio(String telefonica) {
        TipoEmpresaServicio tipoEmpresaServicio = TipoEmpresaServicio.get2(telefonica);
        Servicio servicio = new Servicio();
        if (tipoEmpresaServicio != null) {
            servicio.setEmpresaCodigo(tipoEmpresaServicio.name());
            servicio.setCodigo(tipoEmpresaServicio.getDescripcion());
            servicio.setTipo(tipoEmpresaServicio.getCodigo());
        }
        return servicio;
    }

    String pagarTarjetaCredito(JsonRequestPagoTarjetaCredito requestPagoTarjetaCredito, Cuenta cuentaOrigen) throws SistemaCerradoException, TransaccionNoEjecutadaCorrectamenteException, TransaccionEfectivaException, IOException, ImposibleLeerRespuestaException {
        Reg021 reg021 = new Reg021();
        reg021.setGlobal();
        reg021.setWCLSTRN("4");
        reg021.setWIMPORTE(requestPagoTarjetaCredito.getMonto().toPlainString());
        reg021.setWTPOIMP("2");
        reg021.setWCODMON(String.valueOf(Monedas.getBisaByISO(requestPagoTarjetaCredito.getMoneda())));
        reg021.setWCUENTA2(String.valueOf(cuentaOrigen.getNumeroCuenta()));
        reg021.setWTPOCTA2(cuentaOrigen.getTipoCta());
        reg021.setWCODMON2(String.valueOf(cuentaOrigen.getCodigoMoneda()));
        reg021.setWCODADI(desEncriptar(requestPagoTarjetaCredito.getCodTarjetaCredito()));
        reg021.setWNOTA("pago tc BISA Chat");
        String transaccion = medioAmbiente.getValorDe(TRANSACCION_PAGO_TARJETA_CREDITO_CHAT, TRANSACCION_PAGO_TARJETA_CREDITO_CHAT_DEFAULT);
        IRespuestasMonitor monitor = transaccionesClienteDao.pagoTarjetaCredito(getDevice(), reg021, transaccion);
        if (monitor == null) {
            return "999";
        }
        return getResult(monitor);
    }

    String transferenciaBisa(JsonRequestTransaccion requestTransferenciaPropia, Cuenta cuentaOrigen, Cuenta cuentaDestino, boolean propia) throws SistemaCerradoException, TransaccionNoEjecutadaCorrectamenteException, TransaccionEfectivaException, IOException, ImposibleLeerRespuestaException {
        Reg021 reg021 = new Reg021();
        reg021.setGlobal();
        reg021.setWCLSTRN("1");
        reg021.setWCUENTA(String.valueOf(cuentaOrigen.getNumeroCuenta()));
        reg021.setWTPOCTA(cuentaOrigen.getTipoCta());
        reg021.setWCODMON(String.valueOf(cuentaOrigen.getCodigoMoneda()));
        reg021.setWCUENTA2(String.valueOf(cuentaDestino.getNumeroCuenta()));
        reg021.setWTPOCTA2(cuentaDestino.getTipoCta());
        reg021.setWCODMON2(String.valueOf(cuentaDestino.getCodigoMoneda()));
        reg021.setWIMPORTE(requestTransferenciaPropia.getMonto().toPlainString());
        reg021.setWCODADI(String.valueOf(Monedas.getBisaByISO(requestTransferenciaPropia.getMoneda())));
        reg021.setWNOTA("transfer BISA Chat");
        String transaccion;
        if (propia) {
            transaccion = medioAmbiente.getValorDe(TRANSACCION_TRANSFERENCIAS_PROPIAS_CHAT, TRANSACCION_TRANSFERENCIAS_PROPIAS_CHAT_DEFAULT);
        } else {
            transaccion = medioAmbiente.getValorDe(TRANSACCION_TRANSFERENCIAS_TERCEROS_CHAT, TRANSACCION_TRANSFERENCIAS_TERCEROS_CHAT_DEFAULT);
        }
        IRespuestasMonitor monitor = transaccionesClienteDao.transferenciaCuentasBisa(getDevice(), reg021, transaccion);
        if (monitor == null) {
            return "999";
        }
        return getResult(monitor);
    }

    String transferenciaOtroBanco(JsonRequestTransferenciaTerceros requestTransferenciaTerceros, Cuenta cuentaOrigen) throws SistemaCerradoException, TransaccionNoEjecutadaCorrectamenteException, TransaccionEfectivaException, IOException, ImposibleLeerRespuestaException {
        Reg021 reg021 = new Reg021();
        reg021.setGlobal();
        reg021.setWCLSTRN("1");
        reg021.setWCUENTA(String.valueOf(cuentaOrigen.getNumeroCuenta()));
        reg021.setWTPOCTA(cuentaOrigen.getTipoCta());
        reg021.setWCODMON(String.valueOf(cuentaOrigen.getCodigoMoneda()));
        reg021.setWCODMON2(String.valueOf(Monedas.getBisaByISO(requestTransferenciaTerceros.getMoneda())));
        reg021.setWIMPORTE(requestTransferenciaTerceros.getMonto().toPlainString());
        reg021.setWNOTA("transfer BISA Chat");
        Reg020 reg020 = new Reg020();
        reg020.setWDTAADI("1");
        reg020.setWCTABEN(desEncriptar(requestTransferenciaTerceros.getCodCuentaBeneficiario()));
        reg020.setWNOMBEN(requestTransferenciaTerceros.getNombreBeneficiario());
        reg020.setWCODBCO(requestTransferenciaTerceros.getCodBanco());
        reg020.setWCODSUC(requestTransferenciaTerceros.getSucursalBanco());
        switch (requestTransferenciaTerceros.getTipoPago()) {
            case "C":
                reg020.setWTPOCTA("C0");
                break;
            case "P":
                reg020.setWTPOCTA("C1");
                break;
            case "T":
                reg020.setWTPOCTA("C2");
                break;
        }
        String transaccion = medioAmbiente.getValorDe(TRANSACCION_TRANSFERENCIAS_ACH_CHAT, TRANSACCION_TRANSFERENCIAS_ACH_CHAT_DEFAULT);
        IRespuestasMonitor monitor = transaccionesClienteDao.transferenciaOtrosBancos(getDevice(), reg021, reg020, transaccion);
        if (monitor == null) {
            return "999";
        }
        return getResult(monitor);
    }

    private String getResult(IRespuestasMonitor monitor) {
        BigDecimal numeroCaja = monitor.getDatoRecibidoCabeceraDecimal(MON_USUARIO);
        BigDecimal numeroSecuencial = monitor.getDatoRecibidoCabeceraDecimal(MON_NUMSEQ);
        String codigoPosteo = monitor.getDatoRecibidoCabeceraString(MON_POST);
        String mensaje = monitor.getDatoRecibidoCabeceraString(MON_REJ1);
        if (("P".equalsIgnoreCase(codigoPosteo) && numeroCaja != null && numeroSecuencial != null)) {
            LOGGER.info("caja {}, secuencia{}", numeroCaja.longValue(), numeroSecuencial.longValue());
        } else {
            if (cuentaSinFondo(StringUtils.trimToEmpty(mensaje))) {
                return "901";//saldo insuficiente
            } else if (errorPCC01(StringUtils.trimToEmpty(mensaje))) {
                return "902";//error pcc01
            } else {
                return "903";//error debito
            }
        }
        return "0";
    }

    private boolean cuentaSinFondo(String errorMonitor) {
        String[] codigos = StringUtils.splitPreserveAllTokens(medioAmbiente.getValorDe(MONITOR_CODIGO_SINFONDO, MONITOR_CODIGO_SINFONDO_DEFUALT), ":", 0);
        if (codigos != null) {
            for (String s : codigos) {
                if (s.equalsIgnoreCase(errorMonitor)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean errorPCC01(String errorMonitor) {
        String[] codigos = StringUtils.splitPreserveAllTokens(medioAmbiente.getValorDe(MONITOR_CODIGO_PCC01, MONITOR_CODIGO_PCC01_DEFUALT), ":", 0);
        if (codigos != null) {
            for (String s : codigos) {
                if (s.equalsIgnoreCase(errorMonitor)) {
                    return true;
                }
            }
        }
        return false;
    }

    private String getDevice() {
        return "CBT" + StringUtils.leftPad("" + new Random().nextInt(99999), 7, "0");
    }

    private String getCanal() {
        return medioAmbiente.getValorDe(CB_CANAL, CB_CANAL_DEFAULT);
    }

    List<BeneficiarioChatBot> getBeneficiarios(Cliente cliente, String nombreBeneficiario) {
        List<BeneficiarioChatBot> beneficiarioChatBotList = null;
        List<Beneficiario> beneficiarios = beneficiarioDao.getByNombre(new BigDecimal(cliente.getCodCliente()), nombreBeneficiario);
        if (beneficiarios != null && beneficiarios.size() > 0) {
            beneficiarioChatBotList = new ArrayList<>();
            for (Beneficiario beneficiario : beneficiarios) {
                beneficiarioChatBotList.add(new BeneficiarioChatBot(beneficiario, encriptar(beneficiario.getCuentaBeneficiario())));
            }
        }
        return beneficiarioChatBotList;
    }

    boolean validaPCC01(JsonRequestTransaccion requestTransaccion) {
        return prevencionLavado.necesitaFormularioPCC01(requestTransaccion.getMonto(), Monedas.getBisaByISO(requestTransaccion.getMoneda()));
    }

    BigDecimal montoBolivianos(BigDecimal monto, Short moneda) {
        return verificarLimitesDao.montoBolivianos(monto, moneda);
    }


    public void sendExtracto(Cliente cliente, byte[] reporte) throws IOException {
        String nombreArchivo = medioAmbiente.getValorDe(CB_NOMBRE_ARCHIVO_CORREO_EXTRACTO, CB_NOMBRE_ARCHIVO_CORREO_EXTRACTO_DEFAULT);
        String extensionArchivo = medioAmbiente.getValorDe(CB_EXTENSION_ARCHIVO_CORREO_EXTRACTO, CB_EXTENSION_ARCHIVO_CORREO_EXTRACTO_DEFAULT);
        String subject = medioAmbiente.getValorDe(CB_ASUNTO_CORREO_EXTRACTO, CB_ASUNTO_CORREO_EXTRACTO_DEFAULT);
        String mensaje = medioAmbiente.getValorDe(CB_CONTENIDO_CORREO_EXTRACTO, CB_CONTENIDO_CORREO_EXTRACTO_DEFAULT);
        //Convertir en Archivo
        File extractoPDF = File.createTempFile(nombreArchivo, extensionArchivo);
        FileUtils.writeByteArrayToFile(extractoPDF, reporte);
        //Enviar Mail
        Mailer mail = mailerFactory.getMailer(cliente.getMail());
        mail.mailAndForgetTempFileExample(subject, mensaje, extractoPDF, null);
    }

    AfiliacionChatBot getAfiliacion(String codCliente, String usuario) {
        return afiliacionChatBotDao.getByCodigoCliente(codCliente, TipoRedSocial.get(usuario));
    }

    AfiliacionChatBot setMontoAfiliacion(AfiliacionChatBot afiliacionChatBot, String usuario) {
        return afiliacionChatBotDao.actualizar(afiliacionChatBot, usuario);
    }


    Par<String, String> getNotificacionAfiliacion(Cliente cliente, String usuario) {
        AfiliacionChatBot afiliacionChatBot = afiliacionChatBotDao.getByCodigoCliente(cliente.getCodCliente(), TipoRedSocial.get(usuario));
        if (afiliacionChatBot == null) {
            return null;
        }
        return new Par<>(afiliacionChatBot.getNotificacionTelefono(), afiliacionChatBot.getNotificacionEmail());
    }

    public void enviarEMAIL(Cliente cliente, String mensaje) {
        String subject = medioAmbiente.getValorDe(CB_ASUNTO_CORREO_TOKEN, CB_ASUNTO_CORREO_TOKEN_DEFAULT);
        String correoCodigoBISA = medioAmbiente.getValorDe(CB_EMAIL_CODIGOBISA, CB_EMAIL_CODIGOBISA_DEFAULT);
        String email = "";
        if (!Boolean.parseBoolean(correoCodigoBISA)) {
            email = cliente.getMail();
        } else {
            AfiliacionChatBot afiliacionChatBot = afiliacionChatBotDao.getByCodigoCliente(cliente.getCodCliente(), TipoRedSocial.FACEBOOK);
            try {
                email = afiliacionChatBotDao.getEmailAddressFromEBanking(afiliacionChatBot.getCodigoEbisa());
            } catch (SQLException e) {
                LOGGER.error("Error al obtener el email ", e);
            }
        }
        //Enviar Mail
        Mailer mail = mailerFactory.getMailer(email);
        mail.mailAndForget(subject, mensaje);
    }

    public Cliente getCliente(ClienteChatBot clienteChatBot) {
        return clienteDao.getByCodigoCliente(clienteChatBot.getCodigoCliente());
    }

    public UsuarioConectadoChatBot getConeccion(ClienteChatBot clienteChatBot, String usuarioCanal) {
        return usuarioConectadoChatBotDao.getByCliente(clienteChatBot);
    }

    private int getCantidadPersonasSucursal(String codAgencia) {
        AgenciaTicket agenciaTicket = agenciaTicketDao.getURLAgencia(codAgencia);
        return ticketDao.getCantidadTickets(agenciaTicket, new java.sql.Date(new Date().getTime()));
    }

    public boolean ExistePiloto(String codCliente) {
        String variable = medioAmbiente.getValorDe(USUARIOS_PILOTO_EBISA, USUARIOS_PILOTO_EBISA_DEFAULT);
        if ("*".equals(variable)) {
            return true;
        } else {
            Cliente cliente = clienteDao.getByCodigoCliente(codCliente);
            if (cliente != null && variable != null && variable.contains(StringUtils.trimToEmpty(cliente.getDocumento()))) {
                return true;
            }
        }

        return false;
    }
}

