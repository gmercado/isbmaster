package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 06/09/2017.
 */
public class JsonRequestCliente extends JsonRequestComun {
    private String documento;
    private String fechaNacimiento;

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        return "{" +
                "documento='" + documento + '\'' +
                ", fechaNacimiento='" + fechaNacimiento + '\'' +
                "} " + super.toString();
    }
}
