package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.PromocionChatBot;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;

import java.util.List;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class JsonResponsePromociones extends JsonResponseComun {
    private List<PromocionChatBot> promociones;

    public JsonResponsePromociones(ResponseProcess responseProcess) {
        if (responseProcess != null) {
            setCodRespuesta(responseProcess.getCodRespuesta());
            setMensaje(responseProcess.getMensaje());
        }
    }

    public List<PromocionChatBot> getPromociones() {
        return promociones;
    }

    public void setPromociones(List<PromocionChatBot> promociones) {
        this.promociones = promociones;
    }

    @Override
    public String toString() {
        return "{" +
                "promociones=" + promociones +
                "} " + super.toString();
    }
}
