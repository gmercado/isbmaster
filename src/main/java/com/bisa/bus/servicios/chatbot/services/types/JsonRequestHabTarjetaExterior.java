package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 28/09/2017.
 */
public class JsonRequestHabTarjetaExterior {
    private String codCliente;
    private String codTarjeta;
    private String desde;
    private String hasta;
    private String motivo;

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getCodTarjeta() {
        return codTarjeta;
    }

    public void setCodTarjeta(String codTarjeta) {
        this.codTarjeta = codTarjeta;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getHasta() {
        return hasta;
    }

    public void setHasta(String hasta) {
        this.hasta = hasta;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @Override
    public String toString() {
        return "{" +
                "codCliente='" + codCliente + '\'' +
                ", codTarjeta='" + codTarjeta + '\'' +
                ", desde='" + desde + '\'' +
                ", hasta='" + hasta + '\'' +
                ", motivo='" + motivo + '\'' +
                '}';
    }
}
