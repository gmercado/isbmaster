package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 23/10/2017.
 */
public class JsonRequestGiroMovil extends JsonRequestTransaccion {
    private String telefono;
    private String tipoDocumento;
    private String numeroDocumento;
    private String nombreBeneficiario;
    private String motivo;

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNombreBeneficiario() {
        return nombreBeneficiario;
    }

    public void setNombreBeneficiario(String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @Override
    public String toString() {
        return "{" +
                "telefono='" + telefono + '\'' +
                ", tipoDocumento='" + tipoDocumento + '\'' +
                ", numeroDocumento='" + numeroDocumento + '\'' +
                ", nombreBeneficiario='" + nombreBeneficiario + '\'' +
                ", motivo='" + motivo + '\'' +
                "} " + super.toString();
    }
}
