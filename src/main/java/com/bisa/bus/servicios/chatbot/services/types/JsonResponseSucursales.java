package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.ResponseProcess;
import com.bisa.bus.servicios.chatbot.model.SucursalChatBot;

import java.util.List;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class JsonResponseSucursales extends JsonResponseComun {
    private List<SucursalChatBot> sucursales;

    public JsonResponseSucursales(ResponseProcess responseProcess) {
        if (responseProcess != null) {
            setCodRespuesta(responseProcess.getCodRespuesta());
            setMensaje(responseProcess.getMensaje());
        }
    }

    public List<SucursalChatBot> getSucursales() {
        return sucursales;
    }

    public void setSucursales(List<SucursalChatBot> sucursales) {
        this.sucursales = sucursales;
    }

    @Override
    public String toString() {
        return "{" +
                "sucursales=" + sucursales +
                '}' + super.toString();
    }
}
