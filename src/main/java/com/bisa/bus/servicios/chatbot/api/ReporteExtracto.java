package com.bisa.bus.servicios.chatbot.api;

import bus.interfaces.as400.entities.Cliente;
import bus.interfaces.as400.entities.Cuenta;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.Monedas;
import bus.plumbing.utils.Par;
import com.bisa.bus.servicios.chatbot.model.ExtractoChatBot;
import net.sf.jasperreports.engine.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ReporteExtracto {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReporteExtracto.class);

    private static final String LOGO_BANCO = "bancobisalogo.png";
    private static final String REP_EXTRACTO_JASPER = "repExtracto.jasper";

    public static byte[] get(Cuenta cuenta, Cliente cliente, Par<ArrayList<ExtractoChatBot>, HashMap<String, String>> par, String nombreProducto) {
        byte[] output = null;
        JasperPrint jasperPrint;
        Map<String, Object> paramMap = new HashMap<>();
        String logoPath = (ReporteExtracto.class.getResource("/images/")).getPath();
        String reportPath = (ReporteExtracto.class.getResource("/reports/chatbot/")).getPath();
        String subRepDir = reportPath;

        logoPath += LOGO_BANCO;
        reportPath += REP_EXTRACTO_JASPER;

        paramMap.put("SUBREPORT_DIR", subRepDir);
        paramMap.put("logo", logoPath);
        paramMap.put("fecha", FormatosUtils.formatoFechaHora(new Date()));
        paramMap.put("nro_cliente", StringUtils.trimToEmpty(cliente.getCodCliente()));
        paramMap.put("nombre_cliente", StringUtils.trimToEmpty(cliente.getNombreCompleto()));
        paramMap.put("numero_cuenta", StringUtils.trimToEmpty(String.valueOf(cuenta.getNumeroCuenta())));
        paramMap.put("estado_cuenta", StringUtils.trimToEmpty(par.getCdr().get("DESC_ESTADO_CUENTA")));
        paramMap.put("moneda", Monedas.getDescripcionByBisa(cuenta.getCodigoMoneda()));
        paramMap.put("producto", StringUtils.trimToEmpty(nombreProducto));
        paramMap.put("nombre_cuenta", StringUtils.trimToEmpty(par.getCdr().get("NOMBRE_CUENTA")));
        paramMap.put("movimientos", par.getCar());
        try {
            jasperPrint = JasperFillManager.fillReport(reportPath, paramMap, new JREmptyDataSource());
            output = JasperExportManager.exportReportToPdf(jasperPrint);
        } catch (JRException e) {
            LOGGER.error("Error en la generacion del extracto:", e);
        }
        return output;
    }
}
