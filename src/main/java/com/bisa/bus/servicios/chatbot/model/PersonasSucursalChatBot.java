package com.bisa.bus.servicios.chatbot.model;

/**
 * @author by rsalvatierra on 15/09/2017.
 */
public class PersonasSucursalChatBot {
    private String tipo;
    private Integer cantidad;

    public PersonasSucursalChatBot(String tipo, Integer cantidad) {
        this.tipo = tipo;
        this.cantidad = cantidad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "{" +
                "tipo='" + tipo + '\'' +
                ", cantidad=" + cantidad +
                '}';
    }
}
