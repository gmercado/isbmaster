package com.bisa.bus.servicios.chatbot.model;

/**
 * @author by rsalvatierra on 23/09/2017.
 */
public enum EstadoToken {
    N, U
}
