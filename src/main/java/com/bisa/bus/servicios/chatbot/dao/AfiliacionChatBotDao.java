package com.bisa.bus.servicios.chatbot.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.chatbot.entities.AfiliacionChatBot;
import com.bisa.bus.servicios.chatbot.model.TipoEstadoClaveChatBot;
import com.bisa.bus.servicios.chatbot.model.TipoRedSocial;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.commons.lang.StringUtils;
import org.openlogics.gears.jdbc.DataStore;
import org.openlogics.gears.jdbc.DataStoreFactory;
import org.openlogics.gears.jdbc.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class AfiliacionChatBotDao extends DaoImpl<AfiliacionChatBot, Long> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AfiliacionChatBotDao.class);

    @Inject
    protected AfiliacionChatBotDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public AfiliacionChatBot registrar(AfiliacionChatBot afiliacionChatBot, String usuario) {
        afiliacionChatBot.setFechaCreacion(new Date());
        afiliacionChatBot.setUsuarioCreador(usuario);
        return persist(afiliacionChatBot);
    }

    public AfiliacionChatBot actualizar(AfiliacionChatBot afiliacionChatBot, String usuario) {
        afiliacionChatBot.setFechaModificacion(new Date());
        afiliacionChatBot.setUsuarioModificador(usuario);
        return merge(afiliacionChatBot);
    }

    public String getEmailAddressFromEBanking(String logonName) throws SQLException {
        DataStore dataStore = DataStoreFactory.createObjectDataStore(getConnection());

        String query = "select\n" +
                "  pers.K08EMAIL\n" +
                "from\n" +
                "  ekp08 pers, ekp02 disp, ekp03 asoc\n" +
                "where\n" +
                "  asoc.k03identif = ?\n" +
                //"  and asoc.k03ebisaid = ?\n" +
                "  AND pers.k08id = disp.k02perid\n" +
                "  AND disp.k02id = asoc.k03tarjid\n" +
                "  and asoc.K03ESTADO=6";

        LOGGER.debug("Attempting to execute the following query: {} with params {}", query, ImmutableList.of(logonName));

        return dataStore.select(Query.of(query, logonName), new ScalarHandler<String>());
    }

    /**
     * Retorna Afiliacion Chat Bot
     *
     * @param pCodigoCliente Codigo cliente
     * @return AfiliacionChatBot
     */
    public AfiliacionChatBot getByCodigoCliente(final String pCodigoCliente,
                                                final TipoRedSocial pTipoRedSocial) {
        LOGGER.debug("Obteniendo registro por Codigo Cliente:{}.", pCodigoCliente);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<AfiliacionChatBot> q = cb.createQuery(AfiliacionChatBot.class);
                        Root<AfiliacionChatBot> p = q.from(AfiliacionChatBot.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("codigoCliente"), StringUtils.trimToEmpty(pCodigoCliente)));
                        predicatesAnd.add(cb.equal(p.get("redSocial"), pTipoRedSocial));
                        predicatesAnd.add(cb.equal(p.get("estado"), TipoEstadoClaveChatBot.ACT));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<AfiliacionChatBot> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public List<AfiliacionChatBot> getVigentes() {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<AfiliacionChatBot> q = cb.createQuery(AfiliacionChatBot.class);
                        Root<AfiliacionChatBot> p = q.from(AfiliacionChatBot.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.notEqual(p.get("totalDia"), BigDecimal.ZERO));
                        predicatesAnd.add(cb.notEqual(p.get("totalDiaTrans1"), BigDecimal.ZERO));
                        q.where(cb.or(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<AfiliacionChatBot> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }
}
