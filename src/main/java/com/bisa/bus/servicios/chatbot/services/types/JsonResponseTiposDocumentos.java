package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.ResponseProcess;
import com.bisa.bus.servicios.chatbot.model.TipoDocumentoChatBot;

import java.util.List;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class JsonResponseTiposDocumentos extends JsonResponseComun {
    private List<TipoDocumentoChatBot> tiposDocumento;

    public JsonResponseTiposDocumentos(ResponseProcess responseProcess) {
        if (responseProcess != null) {
            setCodRespuesta(responseProcess.getCodRespuesta());
            setMensaje(responseProcess.getMensaje());
        }
    }

    public List<TipoDocumentoChatBot> getTiposDocumento() {
        return tiposDocumento;
    }

    public void setTiposDocumento(List<TipoDocumentoChatBot> tiposDocumento) {
        this.tiposDocumento = tiposDocumento;
    }

    @Override
    public String toString() {
        return "{" +
                "tiposDocumento=" + tiposDocumento +
                "} " + super.toString();
    }
}
