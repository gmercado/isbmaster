package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 08/09/2017.
 */
public class JsonRequestTarjetasDebito {
    private String codCliente;
    private int tipo;

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "{" +
                "codCliente='" + codCliente + '\'' +
                ", tipo='" + tipo + '\'' +
                '}';
    }
}
