package com.bisa.bus.servicios.chatbot.services.types;

/**
 * @author by rsalvatierra on 26/10/2017.
 */
public class JsonRequestBeneficiario {
    private String codCliente;
    private String nombreBeneficiario;

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getNombreBeneficiario() {
        return nombreBeneficiario;
    }

    public void setNombreBeneficiario(String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
    }

    @Override
    public String toString() {
        return "{" +
                "codCliente='" + codCliente + '\'' +
                ", nombreBeneficiario='" + nombreBeneficiario + '\'' +
                '}';
    }
}
