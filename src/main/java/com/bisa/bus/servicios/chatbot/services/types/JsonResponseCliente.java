package com.bisa.bus.servicios.chatbot.services.types;

import bus.interfaces.as400.entities.Cliente;
import com.bisa.bus.servicios.chatbot.entities.ClienteChatBot;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;
import org.apache.commons.lang.StringUtils;

/**
 * @author by rsalvatierra on 06/09/2017.
 */
public class JsonResponseCliente extends JsonResponseComun {
    private String codCliente;
    private String nombre;
    private String telefono;
    private String email;

    public JsonResponseCliente(ResponseProcess responseProcess) {
        if (responseProcess != null) {
            setCodRespuesta(responseProcess.getCodRespuesta());
            setMensaje(responseProcess.getMensaje());
        }
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "{" +
                "codCliente='" + codCliente + '\'' +
                ", nombre='" + nombre + '\'' +
                ", telefono='" + telefono + '\'' +
                ", email='" + email + '\'' +
                "} " + super.toString();
    }

    public void setDatosCliente(Cliente cliente, ClienteChatBot clienteChatBot) {
        if (cliente != null) {
            setCodCliente(clienteChatBot.getCodigoClienteBisa());
            setNombre(StringUtils.trimToNull(cliente.getNombreCompleto()));
            setTelefono(StringUtils.trimToNull(cliente.getTelefono()));
            setEmail(StringUtils.trimToNull(cliente.getMail()));
        }
    }
}
