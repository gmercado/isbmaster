package com.bisa.bus.servicios.chatbot.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.chatbot.entities.UsuarioChatBot;
import com.bisa.bus.servicios.chatbot.model.EstadoChatBot;
import com.bisa.bus.servicios.chatbot.model.TipoRedSocial;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.LinkedList;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class UsuarioChatBotDao extends DaoImpl<UsuarioChatBot, Long> {
    private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioChatBotDao.class);

    @Inject
    protected UsuarioChatBotDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    /**
     * Retorna Usuario a partir de codigo
     *
     * @param pRedSocial Tipo red social
     * @param pCodigo    Codigo en red social
     * @return UsuarioChatBot
     */
    public UsuarioChatBot getByNombre(final TipoRedSocial pRedSocial, final String pCodigo) {
        LOGGER.debug("Obteniendo registro {} del codigo {}.", pRedSocial, pCodigo);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<UsuarioChatBot> q = cb.createQuery(UsuarioChatBot.class);
                        Root<UsuarioChatBot> p = q.from(UsuarioChatBot.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("tipoRedSocial"), pRedSocial));
                        predicatesAnd.add(cb.equal(p.get("codigoEnRedSocial"), pCodigo));
                        predicatesAnd.add(cb.equal(p.get("estado"), EstadoChatBot.ACT));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<UsuarioChatBot> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public UsuarioChatBot registrar(UsuarioChatBot usuarioChatBot, String usuario) {
        usuarioChatBot.setFechaCreacion(new Date());
        usuarioChatBot.setUsuarioCreador(usuario);
        return persist(usuarioChatBot);
    }

    public UsuarioChatBot actualizar(UsuarioChatBot usuarioChatBot, String usuario) {
        usuarioChatBot.setFechaModificacion(new Date());
        usuarioChatBot.setUsuarioModificador(usuario);
        return merge(usuarioChatBot);
    }

    public UsuarioChatBot newUsuario(TipoRedSocial tipoRedSocial) {
        UsuarioChatBot usuarioChatBot = new UsuarioChatBot();
        usuarioChatBot.setTipoRedSocial(tipoRedSocial);
        usuarioChatBot.setEstado(EstadoChatBot.ACT);
        return usuarioChatBot;
    }
}
