package com.bisa.bus.servicios.chatbot.services.types;

import com.bisa.bus.servicios.chatbot.model.ResponseProcess;
import com.bisa.bus.servicios.chatbot.model.SucursalChatBot;

import java.util.List;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class JsonResponseCajeros extends JsonResponseComun {

    private List<SucursalChatBot> cajeros;

    public JsonResponseCajeros(ResponseProcess responseProcess) {
        if (responseProcess != null) {
            setCodRespuesta(responseProcess.getCodRespuesta());
            setMensaje(responseProcess.getMensaje());
        }
    }

    public List<SucursalChatBot> getCajeros() {
        return cajeros;
    }

    public void setCajeros(List<SucursalChatBot> cajeros) {
        this.cajeros = cajeros;
    }

    @Override
    public String toString() {
        return "{" +
                "cajeros=" + cajeros +
                "} " + super.toString();
    }
}
