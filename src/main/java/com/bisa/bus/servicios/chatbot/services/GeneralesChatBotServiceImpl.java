package com.bisa.bus.servicios.chatbot.services;

import com.bisa.bus.servicios.chatbot.api.ProcesosChatBot;
import com.bisa.bus.servicios.chatbot.entities.OperacionChatBot;
import com.bisa.bus.servicios.chatbot.model.ProcesoChatBot;
import com.bisa.bus.servicios.chatbot.model.RequestProcess;
import com.bisa.bus.servicios.chatbot.model.ResponseProcess;
import com.bisa.bus.servicios.chatbot.model.TipoProcesoChatBot;
import com.bisa.bus.servicios.chatbot.services.types.*;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author by rsalvatierra on 06/09/2017.
 */
public class GeneralesChatBotServiceImpl implements GeneralesChatBotService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralesChatBotServiceImpl.class);
    private final ProcesosChatBot procesosChatBot;

    @Inject
    public GeneralesChatBotServiceImpl(ProcesosChatBot procesosChatBot) {
        this.procesosChatBot = procesosChatBot;
    }

    @Override
    public JsonResponseCliente adicionarCliente(JsonRequestAddCliente requestAddCliente, String usuario) {
        LOGGER.info("inicio <adicionarCliente>");
        RequestProcess requestProcess = new RequestProcess(requestAddCliente.getCodUsuario(), requestAddCliente.getNombre(), TipoProcesoChatBot.GEN, ProcesoChatBot.ADICIONAR_CLIENTE, requestAddCliente, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateAfiliarCliente(requestProcess, requestAddCliente, usuario);
        //Obtener data
        JsonResponseCliente jsonResponseCliente = procesosChatBot.getCliente(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <adicionarCliente>");
        return jsonResponseCliente;
    }

    @Override
    public JsonResponseSolToken solicitarToken(JsonRequestSolToken requestSolToken, String usuario) {
        LOGGER.info("inicio <solicitarToken>");
        RequestProcess requestProcess = new RequestProcess(requestSolToken.getCodCliente(), TipoProcesoChatBot.GEN, ProcesoChatBot.SOLICITAR_TOKEN, requestSolToken, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateSolicitudToken(requestProcess, requestSolToken);
        //Registrar proceso
        OperacionChatBot operacionChatBot = procesosChatBot.setOperacion(requestProcess, responseProcess);
        //Obtener data
        JsonResponseSolToken jsonResponseSolToken = procesosChatBot.setIdentificadorToken(responseProcess, requestSolToken, operacionChatBot, usuario);
        LOGGER.info("fin <solicitarToken>");
        return jsonResponseSolToken;
    }

    @Override
    public JsonResponseComun validarToken(JsonRequestConfToken requestConfToken, String usuario) {
        LOGGER.info("inicio <validarToken>");
        RequestProcess requestProcess = new RequestProcess(requestConfToken.getCodCliente(), TipoProcesoChatBot.GEN, ProcesoChatBot.VERIFICAR_TOKEN, requestConfToken, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateConfirmarToken(requestProcess, requestConfToken, usuario);
        //Obtener data
        JsonResponseComun jsonResponseComun = procesosChatBot.getJsonResponseComun(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <validarToken>");
        return jsonResponseComun;
    }

    @Override
    public JsonResponseTarjetasDebito consultarTarjetasDebito(JsonRequestTarjetasDebito requestTarjetasDebito, String usuario) {
        LOGGER.info("inicio <consultarTarjetasDebito>");
        RequestProcess requestProcess = new RequestProcess(requestTarjetasDebito.getCodCliente(), TipoProcesoChatBot.GEN, ProcesoChatBot.CONSULTA_TD, requestTarjetasDebito, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateTarjetasCliente(requestProcess, requestTarjetasDebito);
        //Obtener data
        JsonResponseTarjetasDebito jsonResponseTarjetasDebito = procesosChatBot.getTarjetasCliente(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <consultarTarjetasDebito>");
        return jsonResponseTarjetasDebito;
    }

    @Override
    public JsonResponseComun bloqueTarjetaDebito(JsonRequestBloqueaTD requestBloqueaTD, String usuario) {
        LOGGER.info("inicio <bloqueTarjetaDebito>");
        RequestProcess requestProcess = new RequestProcess(requestBloqueaTD.getCodCliente(), TipoProcesoChatBot.GEN, ProcesoChatBot.BLOQUEA_TD, requestBloqueaTD, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateBloqueaTarjeta(requestProcess, requestBloqueaTD);
        //Obtener data
        JsonResponseComun jsonResponseComun = procesosChatBot.getJsonResponseComun(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <bloqueTarjetaDebito>");
        return jsonResponseComun;
    }

    @Override
    public JsonResponseComun habilitaTarjetaDebitoExterior(JsonRequestHabTarjetaExterior requestHabTarjetaExterior, String usuario) {
        LOGGER.info("inicio <habilitaTarjetaDebitoExterior>");
        RequestProcess requestProcess = new RequestProcess(requestHabTarjetaExterior.getCodCliente(), TipoProcesoChatBot.GEN, ProcesoChatBot.HABILITAR_EXT_TD, requestHabTarjetaExterior, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateHabilitacionTarjetaExterior(requestProcess, requestHabTarjetaExterior, usuario);
        //Obtener data
        JsonResponseComun jsonResponseComun = procesosChatBot.getJsonResponseComun(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <habilitaTarjetaDebitoExterior>");
        return jsonResponseComun;
    }

    @Override
    public JsonResponseTarjetaCredito consultarTarjetaCredito(JsonRequestTarjetaCredito requestTarjetaCredito, String usuario) {
        LOGGER.info("inicio <consultarTarjetaCredito>");
        RequestProcess requestProcess = new RequestProcess(requestTarjetaCredito.getCodCliente(), TipoProcesoChatBot.GEN, ProcesoChatBot.CONSULTA_TC, requestTarjetaCredito, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateConsultaTC(requestProcess, requestTarjetaCredito);
        //Obtener data
        JsonResponseTarjetaCredito jsonResponseTarjetaCredito = procesosChatBot.getDatosTC(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <consultarTarjetaCredito>");
        return jsonResponseTarjetaCredito;
    }


    @Override
    public JsonResponseBeneficiarios consultaBeneficiarios(JsonRequestBeneficiario requestBeneficiario, String usuario) {
        LOGGER.info("inicio <consultaBeneficiarios>");
        RequestProcess requestProcess = new RequestProcess(requestBeneficiario.getCodCliente(), TipoProcesoChatBot.GEN, ProcesoChatBot.CONSULTA_BENEF, requestBeneficiario, usuario);
        //Validar proceso
        ResponseProcess responseProcess = procesosChatBot.validateConsultaBeneficiarios(requestProcess, requestBeneficiario);
        //Obtener data
        JsonResponseBeneficiarios jsonResponseBeneficiarios = procesosChatBot.getBeneficiariosCliente(responseProcess);
        //Registrar proceso
        procesosChatBot.setOperacion(requestProcess, responseProcess);
        LOGGER.info("fin <consultaBeneficiarios>");
        return jsonResponseBeneficiarios;
    }
}
