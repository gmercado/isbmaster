package com.bisa.bus.servicios.chatbot.model;

import bus.env.entities.Variable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by rsalvatierra on 05/09/2017.
 */
public class PromocionChatBot {
    private String nombre;
    private String url;

    public PromocionChatBot(Variable bnombre, Variable burl) {
        setNombre(bnombre.getValor());
        setUrl(burl.getValor());
    }

    public static List<PromocionChatBot> getTipos(List<Variable> nombre, List<Variable> url) {
        List<PromocionChatBot> promocionChatBots = null;
        if (nombre != null && url != null && nombre.size() == url.size()) {
            promocionChatBots = new ArrayList<>();
            for (int i = 0; i < nombre.size(); i++) {
                promocionChatBots.add(new PromocionChatBot(nombre.get(i), url.get(i)));
            }
        }
        return promocionChatBots;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "{" +
                "nombre='" + nombre + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
