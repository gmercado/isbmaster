package com.bisa.bus.servicios.chatbot.model;

/**
 * @author by rsalvatierra on 05/09/2017.
 */
public class LocalizacionChatBot {
    private double latitud;
    private double longitud;

    public LocalizacionChatBot(double latitud, double longitud) {
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    @Override
    public String toString() {
        return "{" +
                "latitud=" + latitud +
                ", longitud=" + longitud +
                '}';
    }
}
