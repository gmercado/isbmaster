package com.bisa.bus.servicios.chatbot.model;

import bus.interfaces.ebisa.entities.Beneficiario;
import bus.plumbing.utils.FormatosUtils;

/**
 * @author by rsalvatierra on 26/10/2017.
 */
public class BeneficiarioChatBot {
    private String codBeneficiario;
    private String codBanco;
    private String nombreBanco;
    private String sucursalBanco;
    private String tipoPago;
    private String descTipoPago;
    private String codCuentaBeneficiario;
    private String numeroCuenta;
    private String documentoBeneficiario;
    private String nombreBeneficiario;
    private String nombreCortoBeneficiario;

    public BeneficiarioChatBot(Beneficiario beneficiario, String cipher) {
        setCodBeneficiario(beneficiario.getCodigoBeneficiario());
        setCodBanco(String.valueOf(beneficiario.getBancoBeneficiario().getId()));
        setNombreBanco(beneficiario.getBancoBeneficiario().getNombreCorto());
        setSucursalBanco(String.valueOf(beneficiario.getSucursal()));
        setTipoPago(beneficiario.getTipoCuentaOtrosBancos().name());
        setDescTipoPago(beneficiario.getTipoCuentaOtrosBancos().getDescripcion());
        if ("1009".equals(getCodBanco())) {
            setNumeroCuenta(FormatosUtils.formatoCuentaBisaMask(beneficiario.getCuentaBeneficiario()));
        } else {
            setNumeroCuenta(FormatosUtils.formatoCuentaExterna(beneficiario.getCuentaBeneficiario()));
        }
        setCodCuentaBeneficiario(cipher);
        setDocumentoBeneficiario(beneficiario.getNroDocumentoIdentidad());
        setNombreCortoBeneficiario(beneficiario.getNombreCorto());
        setNombreBeneficiario(beneficiario.getNombreCompletoFinal());
    }

    public String getNombreCortoBeneficiario() {
        return nombreCortoBeneficiario;
    }

    public void setNombreCortoBeneficiario(String nombreCortoBeneficiario) {
        this.nombreCortoBeneficiario = nombreCortoBeneficiario;
    }

    public String getDescTipoPago() {
        return descTipoPago;
    }

    public void setDescTipoPago(String descTipoPago) {
        this.descTipoPago = descTipoPago;
    }

    public String getCodBeneficiario() {
        return codBeneficiario;
    }

    public void setCodBeneficiario(String codBeneficiario) {
        this.codBeneficiario = codBeneficiario;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public String getCodBanco() {
        return codBanco;
    }

    public void setCodBanco(String codBanco) {
        this.codBanco = codBanco;
    }

    public String getSucursalBanco() {
        return sucursalBanco;
    }

    public void setSucursalBanco(String sucursalBanco) {
        this.sucursalBanco = sucursalBanco;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getCodCuentaBeneficiario() {
        return codCuentaBeneficiario;
    }

    public void setCodCuentaBeneficiario(String codCuentaBeneficiario) {
        this.codCuentaBeneficiario = codCuentaBeneficiario;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getDocumentoBeneficiario() {
        return documentoBeneficiario;
    }

    public void setDocumentoBeneficiario(String documentoBeneficiario) {
        this.documentoBeneficiario = documentoBeneficiario;
    }

    public String getNombreBeneficiario() {
        return nombreBeneficiario;
    }

    public void setNombreBeneficiario(String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
    }

    @Override
    public String toString() {
        return "{" +
                "codBeneficiario='" + codBeneficiario + '\'' +
                ", codBanco='" + codBanco + '\'' +
                ", sucursalBanco='" + sucursalBanco + '\'' +
                ", tipoPago='" + tipoPago + '\'' +
                ", codCuentaBeneficiario='" + codCuentaBeneficiario + '\'' +
                ", numeroCuenta='" + numeroCuenta + '\'' +
                ", documentoBeneficiario='" + documentoBeneficiario + '\'' +
                ", nombreBeneficiario='" + nombreBeneficiario + '\'' +
                '}';
    }
}
