package com.bisa.bus.servicios.chatbot.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.chatbot.entities.SolicitudChatBot;
import com.bisa.bus.servicios.chatbot.model.EstadoChatBot;
import com.bisa.bus.servicios.chatbot.model.EstadoToken;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class SolicitudChatBotDao extends DaoImpl<SolicitudChatBot, Long> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SolicitudChatBotDao.class);

    @Inject
    protected SolicitudChatBotDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public SolicitudChatBot registrar(SolicitudChatBot solicitudChatBot, String usuario) {
        solicitudChatBot.setFechaCreacion(new Date());
        solicitudChatBot.setUsuarioCreador(usuario);
        return persist(solicitudChatBot);
    }

    public SolicitudChatBot actualizar(SolicitudChatBot solicitudChatBot, String usuario) {
        solicitudChatBot.setFechaModificacion(new Date());
        solicitudChatBot.setUsuarioModificador(usuario);
        return merge(solicitudChatBot);
    }

    public SolicitudChatBot newSolicitud(String operacion) {
        SolicitudChatBot solicitudChatBot = new SolicitudChatBot();
        solicitudChatBot.setTipoProceso(getTime());
        solicitudChatBot.setCodigoConfirmacion(operacion);
        solicitudChatBot.setFechaSolicitudToken(new Date());
        solicitudChatBot.setTokenUsado(EstadoToken.N);
        solicitudChatBot.setEstadoSolicitud(EstadoChatBot.ACT);
        return solicitudChatBot;
    }

    public long getTime() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Instant instant = timestamp.toInstant();
        return instant.toEpochMilli();
    }

    public SolicitudChatBot getByProceso(long identificador, String operacion) {
        LOGGER.debug("Obteniendo registro por Codigo identificador:{}.", identificador);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<SolicitudChatBot> q = cb.createQuery(SolicitudChatBot.class);
                        Root<SolicitudChatBot> p = q.from(SolicitudChatBot.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("tipoProceso"), identificador));
                        //predicatesAnd.add(cb.equal(p.get("codigoConfirmacion"), StringUtils.trimToEmpty(operacion)));
                        predicatesAnd.add(cb.equal(p.get("estadoSolicitud"), EstadoChatBot.ACT));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<SolicitudChatBot> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public List<SolicitudChatBot> getNoVigentes() {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<SolicitudChatBot> q = cb.createQuery(SolicitudChatBot.class);
                        Root<SolicitudChatBot> p = q.from(SolicitudChatBot.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(p.get("estadoSolicitud").in(EstadoChatBot.ACT));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<SolicitudChatBot> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }
}
