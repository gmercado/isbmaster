package com.bisa.bus.servicios.chatbot.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.chatbot.entities.ClienteChatBot;
import com.bisa.bus.servicios.chatbot.entities.UsuarioChatBot;
import com.bisa.bus.servicios.chatbot.entities.UsuarioConectadoChatBot;
import com.bisa.bus.servicios.chatbot.model.EstadoChatBot;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class UsuarioConectadoChatBotDao extends DaoImpl<UsuarioConectadoChatBot, Long> {
    private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioConectadoChatBotDao.class);

    @Inject
    protected UsuarioConectadoChatBotDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public UsuarioConectadoChatBot registrar(UsuarioConectadoChatBot solicitudChatBot, String usuario) {
        solicitudChatBot.setFechaCreacion(new Date());
        solicitudChatBot.setUsuarioCreador(usuario);
        return persist(solicitudChatBot);
    }

    public UsuarioConectadoChatBot actualizar(UsuarioConectadoChatBot solicitudChatBot, String usuario) {
        solicitudChatBot.setFechaModificacion(new Date());
        solicitudChatBot.setUsuarioModificador(usuario);
        return merge(solicitudChatBot);
    }

    public UsuarioConectadoChatBot newConect() {
        UsuarioConectadoChatBot usuarioConectadoChatBot = new UsuarioConectadoChatBot();
        usuarioConectadoChatBot.setEstadoUsuarioConectado(EstadoChatBot.ACT);
        return usuarioConectadoChatBot;
    }

    public UsuarioConectadoChatBot getByCliente(ClienteChatBot pClienteChatBot) {
        LOGGER.debug("Obteniendo registro el codigo {}.", pClienteChatBot);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<UsuarioConectadoChatBot> q = cb.createQuery(UsuarioConectadoChatBot.class);
                        Root<UsuarioConectadoChatBot> p = q.from(UsuarioConectadoChatBot.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("clienteChatBot"), pClienteChatBot));
                        predicatesAnd.add(cb.equal(p.get("estadoUsuarioConectado"), EstadoChatBot.ACT));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<UsuarioConectadoChatBot> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public UsuarioConectadoChatBot getByUsuario(UsuarioChatBot pUsuarioChatBot) {
        LOGGER.debug("Obteniendo registro el codigo {}.", pUsuarioChatBot);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<UsuarioConectadoChatBot> q = cb.createQuery(UsuarioConectadoChatBot.class);
                        Root<UsuarioConectadoChatBot> p = q.from(UsuarioConectadoChatBot.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("usuarioChatBot"), pUsuarioChatBot));
                        predicatesAnd.add(cb.equal(p.get("estadoUsuarioConectado"), EstadoChatBot.ACT));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<UsuarioConectadoChatBot> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public List<UsuarioConectadoChatBot> getByEstado(EstadoChatBot pEstado) {
        LOGGER.debug("Obteniendo por estado {}.", pEstado);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<UsuarioConectadoChatBot> q = cb.createQuery(UsuarioConectadoChatBot.class);
                        Root<UsuarioConectadoChatBot> p = q.from(UsuarioConectadoChatBot.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("estadoUsuarioConectado"), EstadoChatBot.ACT));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<UsuarioConectadoChatBot> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }
}
