package com.bisa.bus.servicios.chatbot.model;

/**
 * @author by rsalvatierra on 06/09/2017.
 */
public class ValidaCampos {
    public static final String COD_USUARIO = "Codigo de Usuario";
    public static final String COD_CLIENTE = "Codigo de Cliente";
    public static final String COD_CUENTA = "Codigo de Cuenta";
    public static final String COD_CUENTA_DESTINO = "Codigo de Cuenta destino";
    public static final String COD_BANCO = "Codigo de Banco";
    public static final String COD_SUCURSAL = "Codigo de Sucursal";
    public static final String COD_TARJETA = "Codigo de Tarjeta";
    public static final String COD_OPERACION = "Codigo de Operacion";
    public static final String COD_BENEFICIARIO = "Codigo de Beneficiario";
    public static final String NOMBRE_USUARIO = "Nombre de Usuario";
    public static final String NOMBRE_SUCURSAL = "Nombre de Sucursal";
    public static final String NOMBRE_TELEFONICA = "Empresa telefonica";
    public static final String NOMBRE_BENEFICIARIO = "Nombre de Beneficiario";
    public static final String NUMERO_DOCUMENTO = "Numero de Documento";
    public static final String TELEFONO = "Numero de Telefono";
    public static final String FECHA_NACIMIENTO = "Fecha de Nacimiento";
    public static final String IDENTIFICADOR_TOKEN = "Identificador Token";
    public static final String TOKEN = "Valor del Token";
    public static final String TIPO_TARJETA = "Tipo de Consulta";
    public static final String TIPO_CUENTA = "Tipo de Consulta";
    public static final String TIPO_BLOQUEO = "Tipo de Operacion";
    public static final String TIPO_DOCUMENTO = "Tipo de Documento";
    public static final String TIPO_PAGO = "Tipo de Pago";
    public static final String FECHA_DESDE = "Fecha Desde";
    public static final String FECHA_HASTA = "Fecha Hasta";
    public static final String MOTIVO = "Motivo/Causal";
    public static final String MONTO = "Importe";
    public static final String MONEDA = "Moneda";
}
