package com.bisa.bus.servicios.chatbot.model;

import bus.plumbing.utils.FormatosUtils;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author by rsalvatierra on 27/09/2017.
 */
public class MovimientosChatBot {
    private String codMovimiento;
    private String fecha;
    private String hora;
    private BigDecimal monto;
    private String moneda;
    private String glosa;
    private String tipo;

    public MovimientosChatBot(String codMovimiento, BigDecimal fecha, BigDecimal hora, BigDecimal monto, String moneda, String glosa, String tipo) {
        this.codMovimiento = codMovimiento;
        this.monto = monto;
        this.moneda = moneda;
        this.glosa = glosa;
        this.tipo = tipo;
        setFechas(fecha, hora);
    }

    private void setFechas(BigDecimal fecha, BigDecimal hora) {
        StringBuilder stringBuilder = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String s = StringUtils.leftPad(hora.toPlainString(), 6, '0');
        stringBuilder.append(fecha).append(s);
        Date WFECHA;
        try {
            WFECHA = sdf.parse(stringBuilder.toString());
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
        setFecha(FormatosUtils.fechaFormateadaConYY(WFECHA));
        setHora(FormatosUtils.horaFormateadaConHHmmss(WFECHA));
    }


    public String getCodMovimiento() {
        return codMovimiento;
    }

    public void setCodMovimiento(String codMovimiento) {
        this.codMovimiento = codMovimiento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    @Override
    public String toString() {
        return "{" +
                "codMovimiento='" + codMovimiento + '\'' +
                ", fecha='" + fecha + '\'' +
                ", monto=" + monto +
                ", moneda='" + moneda + '\'' +
                ", glosa='" + glosa + '\'' +
                ", tipo='" + tipo + '\'' +
                '}';
    }
}
