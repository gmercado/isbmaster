package com.bisa.bus.servicios.chatbot.model;

import bus.plumbing.utils.FormatosUtils;

import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 28/09/2017.
 */
public class CuentasActivasChatBot {
    private String codCuenta;
    private String numero;
    private String fechaCierre;
    private String fechaVencimiento;
    private String fechaProximoPago;
    private BigDecimal montoAdeudado;
    private BigDecimal montoMinimo;
    private String moneda;
    private String tipo;

    public CuentasActivasChatBot(String codCuenta, String numero, BigDecimal fechaCierre, BigDecimal fechaVencimiento, BigDecimal fechaProximoPago, BigDecimal montoAdeudado, BigDecimal montoMinimo, String moneda, String tipo) {
        this.codCuenta = codCuenta;
        this.numero = numero;
        if (!BigDecimal.ZERO.equals(fechaCierre)) {
            this.fechaCierre = FormatosUtils.fechaYYYYMMDD(fechaCierre);
        }
        if (!BigDecimal.ZERO.equals(fechaVencimiento)) {
            this.fechaVencimiento = FormatosUtils.fechaYYYYMMDD(fechaVencimiento);
        }
        if (!BigDecimal.ZERO.equals(fechaProximoPago)) {
            this.fechaProximoPago = FormatosUtils.fechaYYYYMMDD(fechaProximoPago);
        }
        this.montoAdeudado = montoAdeudado;
        this.montoMinimo = montoMinimo;
        this.moneda = moneda;
        this.tipo = tipo;
    }

    public String getCodCuenta() {
        return codCuenta;
    }

    public void setCodCuenta(String codCuenta) {
        this.codCuenta = codCuenta;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(String fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }


    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getMontoAdeudado() {
        return montoAdeudado;
    }

    public void setMontoAdeudado(BigDecimal montoAdeudado) {
        this.montoAdeudado = montoAdeudado;
    }

    public BigDecimal getMontoMinimo() {
        return montoMinimo;
    }

    public void setMontoMinimo(BigDecimal montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public String getFechaProximoPago() {
        return fechaProximoPago;
    }

    public void setFechaProximoPago(String fechaProximoPago) {
        this.fechaProximoPago = fechaProximoPago;
    }

    @Override
    public String toString() {
        return "{" +
                "codCuenta='" + codCuenta + '\'' +
                ", numero='" + numero + '\'' +
                ", fechaCierre='" + fechaCierre + '\'' +
                ", fechaVencimiento='" + fechaVencimiento + '\'' +
                ", montoAdeudado=" + montoAdeudado +
                ", montoMinimo=" + montoMinimo +
                ", moneda='" + moneda + '\'' +
                '}';
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
