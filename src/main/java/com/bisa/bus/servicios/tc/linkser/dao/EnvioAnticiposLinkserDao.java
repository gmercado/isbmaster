package com.bisa.bus.servicios.tc.linkser.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.linkser.entities.AnticiposLinkser;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.List;

/**
 * Created by atenorio on 21/11/2017.
 */
public class EnvioAnticiposLinkserDao extends DaoImpl<AnticiposLinkser, String> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(EnvioAnticiposLinkserDao.class);

    @Inject
    protected EnvioAnticiposLinkserDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean limpiarTodo() {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<AnticiposLinkser> query = entityManager.createQuery("DELETE FROM EnvioAnticiposLinkser");
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }

    public List<AnticiposLinkser> getAnticiposParaEnvio() {
        LOGGER.debug("Obteniendo solicituddes de DB para envio.");
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<AnticiposLinkser> q = cb.createQuery(AnticiposLinkser.class);
                    TypedQuery<AnticiposLinkser> query = entityManager.createQuery("select  p from AnticiposLinkser p", AnticiposLinkser.class);
                    return query.getResultList();
                }
        );
    }
}
