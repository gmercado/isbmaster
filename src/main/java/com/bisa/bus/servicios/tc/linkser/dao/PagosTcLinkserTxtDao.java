package com.bisa.bus.servicios.tc.linkser.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.linkser.entities.PagosTcLinkserTxt;
import com.bisa.bus.servicios.tc.sftp.entities.MovimientosTC;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.List;

/**
 * Created by atenorio on 10/11/2017.
 */
public class PagosTcLinkserTxtDao extends DaoImpl<PagosTcLinkserTxt, String> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(PagosTcLinkserTxtDao.class);

    @Inject
    protected PagosTcLinkserTxtDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean limpiarTodo() {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<MovimientosTC> query = entityManager.createQuery("DELETE FROM PagosTcLinkserTxt");//
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }

    public List<PagosTcLinkserTxt> getPagosParaEnvio() {
        LOGGER.debug("Obteniendo solicituddes de DB para envio.");
        return doWithTransaction(
                (entityManager, t) -> {

                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<PagosTcLinkserTxt> q = cb.createQuery(PagosTcLinkserTxt.class);
                    //Root<PagosTcLinkserTxt> p = q.from(PagosTcLinkserTxt.class);
                    //LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    //predicatesAnd.add(p.get("soest").in(EstadoSolicitudTC.GENE.name()));
                    //q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<PagosTcLinkserTxt> query = entityManager.createQuery("select  p from PagosTcLinkserTxt p", PagosTcLinkserTxt.class); //entityManager.createQuery(q); //
                    return query.getResultList();
                }
        );
    }
}

