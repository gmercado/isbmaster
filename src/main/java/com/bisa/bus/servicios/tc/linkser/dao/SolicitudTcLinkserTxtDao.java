package com.bisa.bus.servicios.tc.linkser.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.linkser.entities.SolicitudTcLinkserTxt;
import com.bisa.bus.servicios.tc.sftp.entities.MovimientosTC;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by atenorio on 10/11/2017.
 */
public class SolicitudTcLinkserTxtDao extends DaoImpl<SolicitudTcLinkserTxt, String> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SolicitudTcLinkserTxtDao.class);

    @Inject
    protected SolicitudTcLinkserTxtDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean limpiarTodo() {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<MovimientosTC> query = entityManager.createQuery("DELETE FROM SolicitudTcLinkserTxt");//
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }

    public List<SolicitudTcLinkserTxt> getSolicitudesParaEnvio() {
        LOGGER.debug("Obteniendo solicituddes de DB para envio.");
        return doWithTransaction(
                (entityManager, t) -> {

                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<SolicitudTcLinkserTxt> q = cb.createQuery(SolicitudTcLinkserTxt.class);
                    Root<SolicitudTcLinkserTxt> p = q.from(SolicitudTcLinkserTxt.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    //predicatesAnd.add(p.get("soest").in(EstadoSolicitudTC.GENE.name()));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<SolicitudTcLinkserTxt> query = entityManager.createQuery(q);
                    return query.getResultList();
                }
        );
    }
}
