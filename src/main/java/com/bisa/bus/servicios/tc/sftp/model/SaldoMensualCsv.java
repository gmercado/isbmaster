package com.bisa.bus.servicios.tc.sftp.model;


import bus.plumbing.csv.annotation.FormatCsv;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by atenorio on 22/06/2017.
 * ATCUC079 Registros de saldo mensual de la cartera liquidada a nivel de cuenta
 *
 */
public class SaldoMensualCsv implements Serializable{

    private static final long serialVersionUID = 1L;
    
    @FormatCsv(name = "SMBAEM", nullable = false, length = 4)
    private String codigoBanco;
    
    @FormatCsv(name = "SMSUCEM", nullable = false, length = 4)
    private String codigoSucursal;
    
    @FormatCsv(name = "SMNUTCNC", nullable = false, length = 10)
    private String numeroCuenta;
    
    @FormatCsv(name = "SMESTCTA", nullable = false, length = 2)
    private String estadoCuenta;
    
    @FormatCsv(name = "SMNOCL", nullable = false, length = 40)
    private String nombreCliente;
    
    @FormatCsv(name = "SMDIR", nullable = false, length = 44)
    private String direccion;
    
    @FormatCsv(name = "SMTELF", nullable = false, length = 8)
    private String telefono;
    
    @FormatCsv(name = "SMCASI", nullable = false, length = 5)
    private String casilla;
    
    @FormatCsv(name = "SMZONA", nullable = false, length = 15)
    private String direccionZona;
    
    @FormatCsv(name = "SMDIRPOS", nullable = false, length = 44)
    private String direccionPostal;
    
    @FormatCsv(name = "SMZPOST", nullable = false, length = 15)
    private String zonaPostal;
    
    @FormatCsv(name = "SMTCTABCO", nullable = false, length = 2)
    private String tipoCuentaBanco;
    
    @FormatCsv(name = "SMNCTABCO", nullable = false, length = 14)
    private String numeroCuentaBanco;
    
    @FormatCsv(name = "SMPMINANT", nullable = false, length = 12)
    private BigDecimal pagoMinimoAnterior;
    
    @FormatCsv(name = "SMSALANTBS", nullable = false, length = 12)
    private BigDecimal saldoAnteriorBs;
    
    @FormatCsv(name = "SMSALANTUS", nullable = false, length = 12)
    private BigDecimal saldoAnteriorUs;
    
    @FormatCsv(name = "SMPAGO", nullable = false, length = 12)
    private BigDecimal pago;
    
    @FormatCsv(name = "SMCARGBS", nullable = false, length = 12)
    private BigDecimal cargosBs;
    
    @FormatCsv(name = "SMCARGUS", nullable = false, length = 12)
    private BigDecimal cargosUs;
    
    @FormatCsv(name = "SMINTE", nullable = false, length = 12)
    private BigDecimal ineteres;
    
    @FormatCsv(name = "SMSALACTBS", nullable = false, length = 12)
    private BigDecimal saldoActualBs;
    
    @FormatCsv(name = "SMSALACTUS", nullable = false, length = 12)
    private BigDecimal saldoActualUs;
    
    @FormatCsv(name = "SMPAGMINAC", nullable = false, length = 12)
    private BigDecimal pagoMinimoActual;
    
    @FormatCsv(name = "SMLIMCOMP", nullable = false, length = 12)
    private BigDecimal limiteCompra;
    
    @FormatCsv(name = "SMLIMFINAN", nullable = false, length = 12)
    private BigDecimal limiteFinanciamiento;
    
    @FormatCsv(name = "SMFPROC", nullable = false, length = 8)
    private String fechaProceso;
    
    @FormatCsv(name = "SMFVTO", nullable = false, length = 8)
    private String fechaVencimiento;
    
    @FormatCsv(name = "SMFPAGO", nullable = false, length = 8)
    private String fechaPago;
    
    @FormatCsv(name = "SMFATACTA", nullable = false, length = 8)
    private String fechaAltaCuenta;
    
    @FormatCsv(name = "SMFVTOTAR", nullable = false, length = 8)
    private String fechaVencimientoTarj;
    
    @FormatCsv(name = "SMTCAMBIO", nullable = false, length = 6)
    private String tipoCambio;
    
    @FormatCsv(name = "SMNIVMORA", nullable = false, length = 2)
    private String nivelMora;
    
    @FormatCsv(name = "SMMONCTA", nullable = false, length = 3)
    private String monedaCuenta;
    
    @FormatCsv(name = "SMAFINI", nullable = false, length = 4)
    private String grupoAfinidad;


    public SaldoMensualCsv() {
    }


    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getCodigoSucursal() {
        return codigoSucursal;
    }

    public void setCodigoSucursal(String codigoSucursal) {
        this.codigoSucursal = codigoSucursal;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getEstadoCuenta() {
        return estadoCuenta;
    }

    public void setEstadoCuenta(String estadoCuenta) {
        this.estadoCuenta = estadoCuenta;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCasilla() {
        return casilla;
    }

    public void setCasilla(String casilla) {
        this.casilla = casilla;
    }

    public String getDireccionZona() {
        return direccionZona;
    }

    public void setDireccionZona(String direccionZona) {
        this.direccionZona = direccionZona;
    }

    public String getDireccionPostal() {
        return direccionPostal;
    }

    public void setDireccionPostal(String direccionPostal) {
        this.direccionPostal = direccionPostal;
    }

    public String getZonaPostal() {
        return zonaPostal;
    }

    public void setZonaPostal(String zonaPostal) {
        this.zonaPostal = zonaPostal;
    }

    public String getTipoCuentaBanco() {
        return tipoCuentaBanco;
    }

    public void setTipoCuentaBanco(String tipoCuentaBanco) {
        this.tipoCuentaBanco = tipoCuentaBanco;
    }

    public String getNumeroCuentaBanco() {
        return numeroCuentaBanco;
    }

    public void setNumeroCuentaBanco(String numeroCuentaBanco) {
        this.numeroCuentaBanco = numeroCuentaBanco;
    }

    public BigDecimal getPagoMinimoAnterior() {
        return pagoMinimoAnterior;
    }

    public void setPagoMinimoAnterior(BigDecimal pagoMinimoAnterior) {
        this.pagoMinimoAnterior = pagoMinimoAnterior;
    }

    public BigDecimal getSaldoAnteriorBs() {
        return saldoAnteriorBs;
    }

    public void setSaldoAnteriorBs(BigDecimal saldoAnteriorBs) {
        this.saldoAnteriorBs = saldoAnteriorBs;
    }

    public BigDecimal getSaldoAnteriorUs() {
        return saldoAnteriorUs;
    }

    public void setSaldoAnteriorUs(BigDecimal saldoAnteriorUs) {
        this.saldoAnteriorUs = saldoAnteriorUs;
    }

    public BigDecimal getPago() {
        return pago;
    }

    public void setPago(BigDecimal pago) {
        this.pago = pago;
    }

    public BigDecimal getCargosBs() {
        return cargosBs;
    }

    public void setCargosBs(BigDecimal cargosBs) {
        this.cargosBs = cargosBs;
    }

    public BigDecimal getCargosUs() {
        return cargosUs;
    }

    public void setCargosUs(BigDecimal cargosUs) {
        this.cargosUs = cargosUs;
    }

    public BigDecimal getIneteres() {
        return ineteres;
    }

    public void setIneteres(BigDecimal ineteres) {
        this.ineteres = ineteres;
    }

    public BigDecimal getSaldoActualBs() {
        return saldoActualBs;
    }

    public void setSaldoActualBs(BigDecimal saldoActualBs) {
        this.saldoActualBs = saldoActualBs;
    }

    public BigDecimal getSaldoActualUs() {
        return saldoActualUs;
    }

    public void setSaldoActualUs(BigDecimal saldoActualUs) {
        this.saldoActualUs = saldoActualUs;
    }

    public BigDecimal getPagoMinimoActual() {
        return pagoMinimoActual;
    }

    public void setPagoMinimoActual(BigDecimal pagoMinimoActual) {
        this.pagoMinimoActual = pagoMinimoActual;
    }

    public BigDecimal getLimiteCompra() {
        return limiteCompra;
    }

    public void setLimiteCompra(BigDecimal limiteCompra) {
        this.limiteCompra = limiteCompra;
    }

    public BigDecimal getLimiteFinanciamiento() {
        return limiteFinanciamiento;
    }

    public void setLimiteFinanciamiento(BigDecimal limiteFinanciamiento) {
        this.limiteFinanciamiento = limiteFinanciamiento;
    }

    public String getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(String fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getFechaAltaCuenta() {
        return fechaAltaCuenta;
    }

    public void setFechaAltaCuenta(String fechaAltaCuenta) {
        this.fechaAltaCuenta = fechaAltaCuenta;
    }

    public String getFechaVencimientoTarj() {
        return fechaVencimientoTarj;
    }

    public void setFechaVencimientoTarj(String fechaVencimientoTarj) {
        this.fechaVencimientoTarj = fechaVencimientoTarj;
    }

    public String getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public String getNivelMora() {
        return nivelMora;
    }

    public void setNivelMora(String nivelMora) {
        this.nivelMora = nivelMora;
    }

    public String getMonedaCuenta() {
        return monedaCuenta;
    }

    public void setMonedaCuenta(String monedaCuenta) {
        this.monedaCuenta = monedaCuenta;
    }

    public String getGrupoAfinidad() {
        return grupoAfinidad;
    }

    public void setGrupoAfinidad(String grupoAfinidad) {
        this.grupoAfinidad = grupoAfinidad;
    }

    @Override
    public String toString() {
        return "SaldoMensualCsv{" +
                "codigoBanco='" + codigoBanco + '\'' +
                ", codigoSucursal='" + codigoSucursal + '\'' +
                ", numeroCuenta='" + numeroCuenta + '\'' +
                ", estadoCuenta='" + estadoCuenta + '\'' +
                ", nombreCliente='" + nombreCliente + '\'' +
                ", direccion='" + direccion + '\'' +
                ", telefono='" + telefono + '\'' +
                ", casilla='" + casilla + '\'' +
                ", direccionZona='" + direccionZona + '\'' +
                ", direccionPostal='" + direccionPostal + '\'' +
                ", zonaPostal='" + zonaPostal + '\'' +
                ", tipoCuentaBanco='" + tipoCuentaBanco + '\'' +
                ", numeroCuentaBanco='" + numeroCuentaBanco + '\'' +
                ", pagoMinimoAnterior=" + pagoMinimoAnterior +
                ", saldoAnteriorBs=" + saldoAnteriorBs +
                ", saldoAnteriorUs=" + saldoAnteriorUs +
                ", pago=" + pago +
                ", cargosBs=" + cargosBs +
                ", cargosUs=" + cargosUs +
                ", ineteres=" + ineteres +
                ", saldoActualBs=" + saldoActualBs +
                ", saldoActualUs=" + saldoActualUs +
                ", pagoMinimoActual=" + pagoMinimoActual +
                ", limiteCompra=" + limiteCompra +
                ", limiteFinanciamiento=" + limiteFinanciamiento +
                ", fechaProceso='" + fechaProceso + '\'' +
                ", fechaVencimiento='" + fechaVencimiento + '\'' +
                ", fechaPago='" + fechaPago + '\'' +
                ", fechaAltaCuenta='" + fechaAltaCuenta + '\'' +
                ", fechaVencimientoTarj='" + fechaVencimientoTarj + '\'' +
                ", tipoCambio='" + tipoCambio + '\'' +
                ", nivelMora='" + nivelMora + '\'' +
                ", monedaCuenta='" + monedaCuenta + '\'' +
                ", grupoAfinidad='" + grupoAfinidad + '\'' +
                '}';
    }
}
