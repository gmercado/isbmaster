package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

/**
 * Created by atenorio on 19/05/2017.
 */
public class SolicitudInformacionEmpleoDefecto {
    @FormatCsv(name = "SOEITIRE", nullable = false, length = 2)
    private String soeitire;
    @FormatCsv(name = "FILL", nullable = false, length = 398)
    private String soeifill="";

    public String getSoeitire() {
        return soeitire;
    }

    public void setSoeitire(String soeitire) {
        this.soeitire = soeitire;
    }

    public String getSoeifill() {
        return soeifill;
    }

    public void setSoeifill(String soeifill) {
        this.soeifill = soeifill;
    }
}
