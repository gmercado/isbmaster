package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by atenorio on 04/07/2017.
 */
@Embeddable
public class IdFactura implements Serializable{
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "FCFFACT", nullable = false, length = 10)
    private String fechaEmision;

    @Basic(optional = false)
    @Column(name = "FCNFACT", nullable = false, length = 15)
    private String numeroFactura;

    @Basic(optional = false)
    @Column(name = "FCNAUTO", nullable = false, length = 15)
    private String numeroAutorizacion;

    @Basic(optional = false)
    @Column(name = "FCNITCI", nullable = false, length = 13)
    private String nit;

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdFactura idFactura = (IdFactura) o;

        if (!getFechaEmision().equals(idFactura.getFechaEmision())) return false;
        if (!getNumeroFactura().equals(idFactura.getNumeroFactura())) return false;
        if (!getNumeroAutorizacion().equals(idFactura.getNumeroAutorizacion())) return false;
        return getNit().equals(idFactura.getNit());
    }

    @Override
    public int hashCode() {
        int result = getFechaEmision().hashCode();
        result = 31 * result + getNumeroFactura().hashCode();
        result = 31 * result + getNumeroAutorizacion().hashCode();
        result = 31 * result + getNit().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "IdFactura{" +
                "fechaEmision='" + fechaEmision + '\'' +
                ", numeroFactura='" + numeroFactura + '\'' +
                ", numeroAutorizacion='" + numeroAutorizacion + '\'' +
                ", nit='" + nit + '\'' +
                '}';
    }
}
