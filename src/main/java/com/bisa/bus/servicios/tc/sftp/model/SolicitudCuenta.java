package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import javax.persistence.Basic;
import java.math.BigInteger;

/**
 * Created by atenorio on 18/05/2017.
 * Tipo de esctructura GA
 */
public class SolicitudCuenta {

    @Basic(optional = false)
    @FormatCsv(name = "SOGATIRE", nullable = false, length = 2)
    private String sogatire;
    @Basic(optional = false)
    @FormatCsv(name = "SOGAGRPR", nullable = false, length = 9, fillSpace = "0")
    private int sogagrpr;
    @Basic(optional = false)
    @FormatCsv(name = "SOGAPRCU", nullable = false, length = 1)
    private short sogaprcu;
    @Basic(optional = false)
    @FormatCsv(name = "SOGATICU", nullable = false, length = 1)
    private short sogaticu;
    @Basic(optional = false)
    @FormatCsv(name = "SOGACOEM", nullable = false, length = 4, fillSpace = "0")
    private String sogacoem;
    @Basic(optional = false)
    @FormatCsv(name = "SOGASUCU1", nullable = false, length = 9, align = FormatCsv.alignType.LEFT)
    private String sogasucu1;
    @Basic(optional = false)
    @FormatCsv(name = "SOGAAGEM", nullable = false, length = 5)
    private String sogaagem;
    @Basic(optional = false)
    @FormatCsv(name = "SOGAINEC", nullable = false, length = 1)
    private short sogainec;
    @Basic(optional = false)
    @FormatCsv(name = "SOGAINECM", nullable = false, length = 1)
    private short sogainecm;
    @Basic(optional = false)
    @FormatCsv(name = "SOGAEMAIL", nullable = false, length = 60)
    private String sogaemail;
    @Basic(optional = false)
    @FormatCsv(name = "SOGAINSE", nullable = false, length = 1)
    private short sogainse;
    @Basic(optional = false)
    @FormatCsv(name = "SOGAFILL", nullable = false, length = 283)
    private String sogafill="";
    @Basic(optional = false)
    @FormatCsv(name = "SOGANUTCNC", nullable = false, length = 20, fillSpace = "0")
    private BigInteger soganutcnc;
    @Basic(optional = false)
    @FormatCsv(name = "SOGACOER", nullable = false, length = 3)
    private String sogacoer;

    public SolicitudCuenta() {
    }

    public String getSogatire() {
        return sogatire;
    }

    public void setSogatire(String sogatire) {
        this.sogatire = sogatire;
    }

    public int getSogagrpr() {
        return sogagrpr;
    }

    public void setSogagrpr(int sogagrpr) {
        this.sogagrpr = sogagrpr;
    }

    public short getSogaprcu() {
        return sogaprcu;
    }

    public void setSogaprcu(short sogaprcu) {
        this.sogaprcu = sogaprcu;
    }

    public short getSogaticu() {
        return sogaticu;
    }

    public void setSogaticu(short sogaticu) {
        this.sogaticu = sogaticu;
    }

    public String getSogacoem() {
        return sogacoem;
    }

    public void setSogacoem(String sogacoem) {
        this.sogacoem = sogacoem;
    }

    public String getSogasucu1() {
        return sogasucu1;
    }

    public void setSogasucu1(String sogasucu1) {
        this.sogasucu1 = sogasucu1;
    }

    public String getSogaagem() {
        return sogaagem;
    }

    public void setSogaagem(String sogaagem) {
        this.sogaagem = sogaagem;
    }

    public short getSogainec() {
        return sogainec;
    }

    public void setSogainec(short sogainec) {
        this.sogainec = sogainec;
    }

    public short getSogainecm() {
        return sogainecm;
    }

    public void setSogainecm(short sogainecm) {
        this.sogainecm = sogainecm;
    }

    public String getSogaemail() {
        return sogaemail;
    }

    public void setSogaemail(String sogaemail) {
        this.sogaemail = sogaemail;
    }

    public short getSogainse() {
        return sogainse;
    }

    public void setSogainse(short sogainse) {
        this.sogainse = sogainse;
    }

    public String getSogafill() {
        return sogafill;
    }

    public void setSogafill(String sogafill) {
        this.sogafill = sogafill;
    }

    public BigInteger getSoganutcnc() {
        return soganutcnc;
    }

    public void setSoganutcnc(BigInteger soganutcnc) {
        this.soganutcnc = soganutcnc;
    }

    public String getSogacoer() {
        return sogacoer;
    }

    public void setSogacoer(String sogacoer) {
        this.sogacoer = sogacoer;
    }
}
