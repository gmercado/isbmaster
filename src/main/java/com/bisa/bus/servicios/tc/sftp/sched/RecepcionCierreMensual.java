package com.bisa.bus.servicios.tc.sftp.sched;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.bus.servicios.tc.sftp.api.SftpRecepcionMovimientoMensual;
import com.bisa.bus.servicios.tc.sftp.api.SftpRecepcionMovimientoMensualEmisor;
import com.bisa.bus.servicios.tc.sftp.api.SftpRecepcionSaldoMensual;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 23/06/2017.
 */
public class RecepcionCierreMensual implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecepcionCierreMensual.class);

    public static String NOMBRE_TAREA = "RecepcionCierreMensual";
    private final MedioAmbiente medioAmbiente;
    private final NotificacionesCorreo notificacionesCorreo;
    private final SftpRecepcionSaldoMensual sftpRecepcionSaldoMensual;
    private final SftpRecepcionMovimientoMensual sftpRecepcionMovimientoMensual;
    private final SftpRecepcionMovimientoMensualEmisor sftpRecepcionMovimientoMensualEmisor;

    private volatile boolean ok;

    @Inject
    public RecepcionCierreMensual(MedioAmbiente medioAmbiente, NotificacionesCorreo notificacionesCorreo,
                                  SftpRecepcionSaldoMensual sftpRecepcionSaldoMensual,
                                  SftpRecepcionMovimientoMensual sftpRecepcionMovimientoMensual,
                                  SftpRecepcionMovimientoMensualEmisor sftpRecepcionMovimientoMensualEmisor) {
        this.medioAmbiente = medioAmbiente;
        this.notificacionesCorreo = notificacionesCorreo;
        this.sftpRecepcionSaldoMensual = sftpRecepcionSaldoMensual;
        this.sftpRecepcionMovimientoMensual = sftpRecepcionMovimientoMensual;
        this.sftpRecepcionMovimientoMensualEmisor = sftpRecepcionMovimientoMensualEmisor;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        LOGGER.info("Realizando llamada a proceso");
        // Procesando saldo mensual
        ok = sftpRecepcionSaldoMensual.procesarArchivo();
        if (ok) {
            ok = sftpRecepcionMovimientoMensual.procesarArchivo(sftpRecepcionSaldoMensual.getFechaProceso());
        }
        LOGGER.info("Archivo procesado {}", ok);

        if (ok) {
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_CIERRE_MENSUAL_OPERADOR, Variables.FTP_MENSAJE_EMAIL_CIERRE_MENSUAL_OPERADOR_DEFAULT);
            if(!sftpRecepcionSaldoMensual.isValido()){
                mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_CIERRE_MENSUAL_VALIDAR_OPERADOR, Variables.FTP_MENSAJE_EMAIL_CIERRE_MENSUAL_VALIDAR_OPERADOR_DEFAULT);
            }
            notificacionesCorreo.notificarCorrectaTransferenciaOperador(
                    sftpRecepcionSaldoMensual.getNombreArchivoProcesado() + " " +
                            sftpRecepcionMovimientoMensual.getNombreArchivoProcesado(),
                    TipoArchivo.CIE, mensaje + "\n\nResultado: " + sftpRecepcionSaldoMensual.getMensajeDeValidacion());
        } else {
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_CIERRE_MANSUAL_ERROR_OPERADOR,
                    Variables.FTP_MENSAJE_EMAIL_CIERRE_MANSUAL_ERROR_OPERADOR_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador("", TipoArchivo.CIE, mensaje);
        }
    }
}
