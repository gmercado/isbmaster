package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by atenorio on 07/06/2017.
 */
@Embeddable
public class IdDatosGenerales implements Serializable {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "ESCADM", nullable = false)
    private String codigoAdm;

    @Basic(optional = false)
    @Column(name = "ESFEPRO1", nullable = false)
    private int fechaProceso;

    @Basic(optional = false)
    @Column(name = "ESNUTCNC", nullable = false, length = 10)
    private String numeroCuenta;

    public String getCodigoAdm() {
        return codigoAdm;
    }

    public void setCodigoAdm(String codigoAdm) {
        this.codigoAdm = codigoAdm;
    }

    public int getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(int fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdDatosGenerales that = (IdDatosGenerales) o;
        if (fechaProceso != that.fechaProceso) return false;
        if (!codigoAdm.equals(that.codigoAdm)) return false;
        return numeroCuenta.equals(that.numeroCuenta);
    }

    @Override
    public int hashCode() {
        int result = codigoAdm.hashCode();
        result = 31 * result + fechaProceso;
        result = 31 * result + numeroCuenta.hashCode();
        return result;
    }
}
