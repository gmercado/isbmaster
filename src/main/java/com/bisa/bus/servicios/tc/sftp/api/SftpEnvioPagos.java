package com.bisa.bus.servicios.tc.sftp.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.rpg.ImposibleLlamarProgramaRpgException;
import bus.plumbing.csv.GeneradorCSV;
import bus.plumbing.file.ArchivoProfile;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.tc.sftp.dao.PagosHistoricoTCDao;
import com.bisa.bus.servicios.tc.sftp.dao.PagosTCDao;
import com.bisa.bus.servicios.tc.sftp.entities.FtpArchivoTransferencia;
import com.bisa.bus.servicios.tc.sftp.entities.PagosHistoricoTC;
import com.bisa.bus.servicios.tc.sftp.entities.PagosTC;
import com.bisa.bus.servicios.tc.sftp.model.EstadoPagosTC;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.PagosCsv;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgProgramaTarjetaCredito;
import com.google.inject.Inject;
import com.ibm.as400.access.ConnectionPoolException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Clase para proceso de generación y envío de archivo de pago de tarjetas de crédito para administradora ATC
 * @author atenorio
 * @version 29/05/2017/A
 * Created on 29/05/2017.
 */
public class SftpEnvioPagos {
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpEnvioPagos.class);

    private final MedioAmbiente medioAmbiente;
    private final PagosTCDao pagosTCDao;
    private final PagosHistoricoTCDao pagosHistoricoTCDao;
    private final RpgProgramaTarjetaCredito rpgPrograma;
    private final NotificacionesCorreo notificacionesCorreo;
    SftpConfiguracion configuracion;
    SftpTransferencia sftpTransferencia;

    @Inject
    public SftpEnvioPagos(MedioAmbiente medioAmbiente,
                          PagosTCDao pagosTCDao, PagosHistoricoTCDao pagosHistoricoTCDao,
                          NotificacionesCorreo notificacionesCorreo, RpgProgramaTarjetaCredito rpgPrograma,
                          SftpConfiguracion configuracion, SftpTransferencia sftpTransferencia
                          ) {
        this.medioAmbiente = medioAmbiente;
        this.pagosTCDao = pagosTCDao;
        this.pagosHistoricoTCDao = pagosHistoricoTCDao;
        this.rpgPrograma = rpgPrograma;
        this.notificacionesCorreo = notificacionesCorreo;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para procesar generacion y envio de archivo de pagos a servidor
     * remoto SFTP.
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     * */
    public boolean procesarArchivo() {

        boolean ok = false;
        LOGGER.debug("Inicia proceso de envio de archivo de solicitud");
        // Obtener fecha de procesamiento de archivo estos podran ser recibidos hasta las 21:00 todos los días de lunes a domingo.
        String nombreArchivo = medioAmbiente.getValorDe(Variables.FTP_NOMBRE_ARCHIVO_PAGOS_ATC, Variables.FTP_NOMBRE_ARCHIVO_PAGOS_ATC_DEFAULT);
        String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_ATC, Variables.FECHA_DE_PROCESO_ATC_DEFAULT);
        Date fechaProcesar = new Date();
        if(StringUtils.isEmpty(fechaProceso) || "0".equals(fechaProceso)){
            fechaProceso = FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar);
        }else{
            fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
        }
        nombreArchivo = nombreArchivo + fechaProceso + ".txt";

        LOGGER.debug("1) Verificar archivo pendiente de respuesta");
        // Verificar que archivo no sea procesado con el mismo nombre a menos que este en estado de reproceso o error de reintento
        if (!sftpTransferencia.verificarPermitirTransferencia(nombreArchivo)) {
            LOGGER.warn("No se permite procesamiento y transferencia de archivo {} se finaliza la tarea.", nombreArchivo);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.PAG, "Procesamiento de archivo ya realizado.");
            return false;
        }
        FtpArchivoTransferencia ftpArchivoTransferencia = sftpTransferencia.crearRegistro(nombreArchivo, TipoArchivo.PAG);

        LOGGER.debug("2) Llamada a programa AS400 para cargar archivo de pagos");
        try {
            ok = ejecucionRpg();
        } catch (Exception e) {
            LOGGER.error("Error al llamar programa AS400 se finaliza tarea. {}", e.getMessage());
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.PAG, "Error al llamar programa AS400 se finaliza tarea");
            sftpTransferencia.transferenciaConErrorReintento("");
            return false;
        }

        // Verificar ejecucion correcta del programa
        if (!ok) {
            LOGGER.error("Error en ejecuci\u00F3n de programa AS400.");
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.PAG, "Error al llamar programa AS400 se finaliza tarea");
            sftpTransferencia.transferenciaConErrorReintento("");
            return false;
        }

        LOGGER.debug("3) Carga de archivo de a servidor SFTP");
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_ENVIO_PAGOS_ATC, Variables.FTP_RUTA_ENVIO_PAGOS_ATC_DEFAULT));
        configuracion.setRemoteTemp(medioAmbiente.getValorDe(Variables.FTP_RUTA_ENVIO_TEMPORAL_ATC, Variables.FTP_RUTA_ENVIO_TEMPORAL_ATC_DEFAULT));
        sftpTransferencia.configurar(configuracion);
        File archivo = sftpTransferencia.createFile(nombreArchivo);

        LOGGER.info("3.1) Recuperacion pagos pendientes de envio. {}",archivo.getAbsolutePath());
        List<PagosTC> pagos = pagosTCDao.getPagosParaEnvio();
        if(pagos.size()>0) {
            ArchivoProfile archivoProfile = poblarArchivo(archivo, pagos);
            if(archivoProfile == null){
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.PAG, "Error en generaci\u00F3n de archivo.");
                sftpTransferencia.transferenciaConErrorReintento("");
                return false;
            }
            LOGGER.debug("3.2) Creación local archivo de pagos con {}.", pagos.size());
            // Cargar archivo a servidor SFTP
            boolean cargado = sftpTransferencia.cargarArchivo(archivoProfile.getArchivo(), nombreArchivo, ftpArchivoTransferencia);

            // Modificar cambio de estado
            if (cargado) {
                LOGGER.debug("3.3) Cargando de archivo {} de solicitudes a servidor sftp finalizado correctamente.", nombreArchivo);
                TransferObject transferObject = new TransferObject();
                transferObject.initializate();
                for (PagosTC sol : pagos) {
                    sol.setEstadoPago(EstadoPagosTC.ENVI);
                    sol.setFechaEnvio(new Date());
                    sol.setIdArchivoEnvio(sftpTransferencia.getFtpArchivoTransferencia().getId());
                    ok = pagosTCDao.guardar(sol, "ISB");

                    PagosHistoricoTC pagosHistoricoTC = transferObject.convert(sol, PagosHistoricoTC.class);
                    pagosHistoricoTC.setIdPago(sol.getIdPago());
                    ok = pagosHistoricoTCDao.guardar(pagosHistoricoTC, "ISB");
                }

                StringBuffer sb = new StringBuffer();
                sb.append("Se ha generado el archivo de pagos para la administradora ATC de fecha: ");
                sb.append(FormatosUtils.fechaLargaFormateada(fechaProcesar));
                sb.append(" con ");
                sb.append(pagos.size());
                sb.append(" pagos.");
                notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivo, TipoArchivo.PAG, sb.toString());
                sftpTransferencia.transferenciaCompleta("");
                ok = true;
                LOGGER.info("Completado proceso de carga de archivo {} de pagos a servidor SFTP de ATC correctamente.", nombreArchivo);
            }else{
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.PAG, "");
                sftpTransferencia.transferenciaConErrorReintento("");
                ok = false;
                LOGGER.info("Cargando de archivo {} de pagos a servidor sftp finalizado incorrectamente.", nombreArchivo);
            }

        }else{
            StringBuffer sb = new StringBuffer();
            sb.append("No se ha generado el archivo de pagos para la administradora ATC de fecha: ");
            sb.append(FormatosUtils.fechaLargaFormateada(fechaProcesar));
            sb.append(" por no tener pagos por enviar.\n");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivo, TipoArchivo.PAG, sb.toString());
            sftpTransferencia.transferenciaCompleta("");
            ok = true;
            LOGGER.info("Completado proceso no se tienen pagos por enviar");
        }
        LOGGER.debug("3.4) Completado proceso de carga de archivo {} de pagos a servidor SFTP de ATC correctamente.", nombreArchivo);
        return ok;
    }

    /**
     * Generacion de archivo de envio de pagos
     * */
    private ArchivoProfile poblarArchivo(File archivo, List<PagosTC> pagos) {
        if (archivo == null) return null;
        long nroFilas = 0L;
        long valorFilas = 0L;
        FileOutputStream archivoWrite;
        try {
            archivoWrite = new FileOutputStream(archivo.getAbsoluteFile());
            BufferedWriter bufferWriter = new BufferedWriter(new OutputStreamWriter(archivoWrite, StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            bufferWriter.write(sb.toString());
            // generando archivo
            for (PagosTC pago: pagos) {
                TransferObject transferObject = new TransferObject();
                transferObject.initializate();
                // Grabar pago en archivo de pagos
                PagosCsv pagosCsv = transferObject.convert(pago, PagosCsv.class);
                pagosCsv.setTransaccion(pago.getIdPago().getTransaccion());
                pagosCsv.setCuentaTarjeta(pago.getIdPago().getCuentaTarjeta());
                GeneradorCSV.poblarLinea(bufferWriter, pagosCsv);
            }
            bufferWriter.close();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("DETALLE DEL ARCHIVO");
                FileReader filer = new FileReader(archivo);
                int valor = filer.read();
                while (valor != -1) {
                    LOGGER.debug("", valor);
                    valor = filer.read();
                }
            }

        } catch (IOException e) {
            LOGGER.error("Ocurrio un error inesperado", e);
            return null;
        }
        return new ArchivoProfile(archivo, valorFilas, nroFilas);
    }

    public boolean ejecucionRpg() throws IllegalArgumentException, IllegalStateException {
        boolean result = false;
        String libreriaPrograma = medioAmbiente.getValorDe(Variables.LIBRERIA_TC_ATC, Variables.LIBRERIA_TC_ATC_DEFAULT);
        String programa = medioAmbiente.getValorDe(Variables.PROGRAMA_TC_PAGOS_ATC, Variables.PROGRAMA_TC_PAGOS_ATC_DEFAULT);
        try {
            result = rpgPrograma.ejecutar(libreriaPrograma, programa);
            return result;
        } catch (ImposibleLeerRespuestaException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(Imposible leer respuesta)");
        } catch (IOException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(IOException)");
        } catch (TransaccionEfectivaException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(TransaccionEfectivaException)");
        } catch (SistemaCerradoException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(SistemaCerradoException)");
        } catch (ImposibleLlamarProgramaRpgException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(ImposibleLlamarProgramaRpgException)");
        } catch (ConnectionPoolException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(ConnectionPoolException)");
        } catch (TimeoutException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(Tiempo de espera agotado)");
        }
    }

}
