package com.bisa.bus.servicios.tc.sftp.entities;

import com.bisa.bus.servicios.tc.sftp.model.EstadoSolicitudTC;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * Created by atenorio on 07/09/2017.
 */
@Entity
@Table(name = "TCPASOLIEH")
public class SolicitudHistoricoTCEmpresarial implements Serializable {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "SOGATIRE", nullable = false, length = 2)
    private String sogatire;
    @Basic(optional = false)
    @Column(name = "SOGAGRPR", nullable = false)
    private int sogagrpr;
    @Basic(optional = false)
    @Column(name = "SOGAPRCU", nullable = false)
    private short sogaprcu;
    @Basic(optional = false)
    @Column(name = "SOGATICU", nullable = false)
    private short sogaticu;
    @Basic(optional = false)
    @Column(name = "SOGACOEM", nullable = false, length = 4)
    private String sogacoem;
    @Basic(optional = false)
    @Column(name = "SOGASUCU1", nullable = false, length = 9)
    private String sogasucu1;
    @Basic(optional = false)
    @Column(name = "SOGAAGEM", nullable = false, length = 5)
    private String sogaagem;
    @Basic(optional = false)
    @Column(name = "SOGAINEC", nullable = false)
    private short sogainec;
    @Basic(optional = false)
    @Column(name = "SOGAINECM", nullable = false)
    private short sogainecm;
    @Basic(optional = false)
    @Column(name = "SOGAEMAIL", nullable = false, length = 60)
    private String sogaemail;
    @Basic(optional = false)
    @Column(name = "SOGAINSE", nullable = false)
    private short sogainse;
    @Basic(optional = false)
    @Column(name = "SOGANUTCNC", nullable = false)
    private BigInteger soganutcnc;
    @Basic(optional = false)
    @Column(name = "SOGACOER", nullable = false, length = 3)
    private String sogacoer;
    @Basic(optional = false)
    @Column(name = "SOCCTIRE", nullable = false, length = 2)
    private String socctire;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "SOCCLICR", nullable = false, precision = 18, scale = 6)
    private BigDecimal socclicr;
    @Basic(optional = false)
    @Column(name = "SOCCMOLI", nullable = false)
    private short soccmoli;
    @Basic(optional = false)
    @Column(name = "SOCCFECI", nullable = false)
    private short soccfeci;
    @Basic(optional = false)
    @Column(name = "SOCCFOPA", nullable = false)
    private short soccfopa;
    @Basic(optional = false)
    @Column(name = "SOCCTIDE", nullable = false)
    private short socctide;
    @Basic(optional = false)
    @Column(name = "SOCCCOER", nullable = false, length = 3)
    private String socccoer;
    @Basic(optional = false)
    @Column(name = "SOCJTIRE", nullable = false, length = 2)
    private String socjtire;
    @Basic(optional = false)
    @Column(name = "SOCJRASO", nullable = false, length = 60)
    private String socjraso;
    @Basic(optional = false)
    @Column(name = "SOCJNOCO", nullable = false, length = 60)
    private String socjnoco;
    @Basic(optional = false)
    @Column(name = "SOCJTIDO", nullable = false)
    private short socjtido;
    @Basic(optional = false)
    @Column(name = "SOCJDOEM", nullable = false, length = 25)
    private String socjdoem;
    @Basic(optional = false)
    @Column(name = "SOCJFEIA", nullable = false, length = 8)
    private String socjfeia;
    @Basic(optional = false)
    @Column(name = "SOCJPARE", nullable = false)
    private short socjpare;
    @Basic(optional = false)
    @Column(name = "SOCJFEFE", nullable = false, length = 8)
    private String socjfefe;
    @Basic(optional = false)
    @Column(name = "SOCJPAFE", nullable = false)
    private short socjpafe;
    @Basic(optional = false)
    @Column(name = "SOCJACEC", nullable = false)
    private short socjacec;
    @Basic(optional = false)
    @Column(name = "SOCJTISO", nullable = false, length = 4)
    private String socjtiso;
    @Basic(optional = false)
    @Column(name = "SOCJALEM", nullable = false)
    private short socjalem;
    @Basic(optional = false)
    @Column(name = "SOCJCAEM", nullable = false, length = 10)
    private String socjcaem;
    @Basic(optional = false)
    @Column(name = "SOCJMOFA", nullable = false, length = 3)
    private String socjmofa;
    @Basic(optional = false)
    @Column(name = "SOCJVAFA", nullable = false, precision = 12, scale = 2)
    private BigDecimal socjvafa;
    @Basic(optional = false)
    @Column(name = "SOCJMOPA", nullable = false)
    private short socjmopa;
    @Basic(optional = false)
    @Column(name = "SOCJVAPA", nullable = false, precision = 12, scale = 2)
    private BigDecimal socjvapa;
    @Basic(optional = false)
    @Column(name = "SOCJDCPA", nullable = false)
    private short socjdcpa;
    @Basic(optional = false)
    @Column(name = "SOCJCOER", nullable = false, length = 3)
    private String socjcoer;
    @Basic(optional = false)
    @Column(name = "SOADTIREL", nullable = false, length = 2)
    private String soadtirel;
    @Basic(optional = false)
    @Column(name = "SOADTIDICL", nullable = false)
    private short soadtidicl;
    @Basic(optional = false)
    @Column(name = "SOADTICAL", nullable = false)
    private short soadtical;
    @Basic(optional = false)
    @Column(name = "SOADCALLEL", nullable = false, length = 100)
    private String soadcallel;
    @Basic(optional = false)
    @Column(name = "SOADNUPUL", nullable = false, length = 10)
    private String soadnupul;
    @Basic(optional = false)
    @Column(name = "SOADINADL", nullable = false, length = 100)
    private String soadinadl;
    @Basic(optional = false)
    @Column(name = "SOADCOPOL", nullable = false, length = 6)
    private String soadcopol;
    @Basic(optional = false)
    @Column(name = "SOADZONAL", nullable = false, length = 60)
    private String soadzonal;
    @Basic(optional = false)
    @Column(name = "SOADPAISL", nullable = false)
    private short soadpaisl;
    @Basic(optional = false)
    @Column(name = "SOADDPTOL", nullable = false, length = 5)
    private String soaddptol;
    @Basic(optional = false)
    @Column(name = "SOADLOCL", nullable = false)
    private int soadlocl;
    @Basic(optional = false)
    @Column(name = "SOADCOTEPL", nullable = false)
    private short soadcotepl;
    @Basic(optional = false)
    @Column(name = "SOADCOTEAL", nullable = false)
    private int soadcoteal;
    @Basic(optional = false)
    @Column(name = "SOADTELL", nullable = false)
    private long soadtell;
    @Basic(optional = false)
    @Column(name = "SOADTEINL", nullable = false)
    private int soadteinl;
    @Basic(optional = false)
    @Column(name = "SOADCOCEPL", nullable = false)
    private short soadcocepl;
    @Basic(optional = false)
    @Column(name = "SOADCELL", nullable = false)
    private long soadcell;
    @Basic(optional = false)
    @Column(name = "SOADCOERL", nullable = false, length = 3)
    private String soadcoerl;
    @Basic(optional = false)
    @Column(name = "SOADTIREM", nullable = false, length = 2)
    private String soadtirem;
    @Basic(optional = false)
    @Column(name = "SOADTIDICM", nullable = false)
    private short soadtidicm;
    @Basic(optional = false)
    @Column(name = "SOADTICAM", nullable = false)
    private short soadticam;
    @Basic(optional = false)
    @Column(name = "SOADCALLEM", nullable = false, length = 100)
    private String soadcallem;
    @Basic(optional = false)
    @Column(name = "SOADNUPUM", nullable = false, length = 10)
    private String soadnupum;
    @Basic(optional = false)
    @Column(name = "SOADINADM", nullable = false, length = 100)
    private String soadinadm;
    @Basic(optional = false)
    @Column(name = "SOADCOPOM", nullable = false, length = 6)
    private String soadcopom;
    @Basic(optional = false)
    @Column(name = "SOADZONAM", nullable = false, length = 60)
    private String soadzonam;
    @Basic(optional = false)
    @Column(name = "SOADPAISM", nullable = false)
    private short soadpaism;
    @Basic(optional = false)
    @Column(name = "SOADDPTOM", nullable = false, length = 5)
    private String soaddptom;
    @Basic(optional = false)
    @Column(name = "SOADLOCM", nullable = false)
    private int soadlocm;
    @Basic(optional = false)
    @Column(name = "SOADCOTEPM", nullable = false)
    private short soadcotepm;
    @Basic(optional = false)
    @Column(name = "SOADCOTEAM", nullable = false)
    private int soadcoteam;
    @Basic(optional = false)
    @Column(name = "SOADTELM", nullable = false)
    private long soadtelm;
    @Basic(optional = false)
    @Column(name = "SOADTEINM", nullable = false)
    private int soadteinm;
    @Basic(optional = false)
    @Column(name = "SOADCOCEPM", nullable = false)
    private short soadcocepm;
    @Basic(optional = false)
    @Column(name = "SOADCELM", nullable = false)
    private long soadcelm;
    @Basic(optional = false)
    @Column(name = "SOADCOERM", nullable = false, length = 3)
    private String soadcoerm;
    @Basic(optional = false)
    @Column(name = "SOCFTIRE", nullable = false, length = 2)
    private String socftire;
    @Basic(optional = false)
    @Column(name = "SOCFTICL", nullable = false)
    private short socfticl;
    @Basic(optional = false)
    @Column(name = "SOCFNOCL", nullable = false, length = 60)
    private String socfnocl;
    @Basic(optional = false)
    @Column(name = "SOCFAPCL", nullable = false, length = 60)
    private String socfapcl;
    @Basic(optional = false)
    @Column(name = "SOCFTIDO", nullable = false)
    private short socftido;
    @Basic(optional = false)
    @Column(name = "SOCFDOCL", nullable = false, length = 25)
    private String socfdocl;
    @Basic(optional = false)
    @Column(name = "SOCFTIDOA", nullable = false)
    private short socftidoa;
    @Basic(optional = false)
    @Column(name = "SOCFDOCLA", nullable = false, length = 25)
    private String socfdocla;
    @Basic(optional = false)
    @Column(name = "SOCFFENA", nullable = false, length = 8)
    private String socffena;
    @Basic(optional = false)
    @Column(name = "SOCFESCI", nullable = false)
    private short socfesci;
    @Basic(optional = false)
    @Column(name = "SOCFGECL", nullable = false)
    private short socfgecl;
    @Basic(optional = false)
    @Column(name = "SOCFPACL", nullable = false)
    private short socfpacl;
    @Basic(optional = false)
    @Column(name = "SOCFLUNA", nullable = false, length = 40)
    private String socfluna;
    @Basic(optional = false)
    @Column(name = "SOCFCAHI", nullable = false)
    private short socfcahi;
    @Basic(optional = false)
    @Column(name = "SOCFNOEM", nullable = false, length = 25)
    private String socfnoem;
    @Basic(optional = false)
    @Column(name = "SOCFEMAIL", nullable = false, length = 60)
    private String socfemail;
    @Basic(optional = false)
    @Column(name = "SOCFDCPA", nullable = false)
    private short socfdcpa;
    @Basic(optional = false)
    @Column(name = "SOCFDFPA", nullable = false)
    private short socfdfpa;
    @Basic(optional = false)
    @Column(name = "SOCFNUSOL", nullable = false)
    private int socfnusol;
    @Basic(optional = false)
    @Column(name = "SOCFCOER", nullable = false, length = 3)
    private String socfcoer;
    @Basic(optional = false)
    @Column(name = "SOEITIRE", nullable = false, length = 2)
    private String soeitire;
    @Basic(optional = false)
    @Column(name = "SOEIEXPO", nullable = false)
    private short soeiexpo;
    @Basic(optional = false)
    @Column(name = "SOEICONTR", nullable = false)
    private short soeicontr;
    @Basic(optional = false)
    @Column(name = "SOEIFEVECO", nullable = false, length = 8)
    private String soeifeveco;
    @Basic(optional = false)
    @Column(name = "SOEIEMPL", nullable = false, length = 60)
    private String soeiempl;
    @Basic(optional = false)
    @Column(name = "SOEITIDO", nullable = false)
    private short soeitido;
    @Basic(optional = false)
    @Column(name = "SOEIDOEM", nullable = false, length = 25)
    private String soeidoem;
    @Basic(optional = false)
    @Column(name = "SOEIFEIN", nullable = false, length = 8)
    private String soeifein;
    @Basic(optional = false)
    @Column(name = "SOEIINPR", nullable = false)
    private short soeiinpr;
    @Basic(optional = false)
    @Column(name = "SOEIINEXT", nullable = false)
    private short soeiinext;
    @Basic(optional = false)
    @Column(name = "SOEITOIN", nullable = false)
    private long soeitoin;
    @Basic(optional = false)
    @Column(name = "SOEICAEM", nullable = false, length = 60)
    private String soeicaem;
    @Basic(optional = false)
    @Column(name = "SOEIOCUP", nullable = false)
    private int soeiocup;
    @Basic(optional = false)
    @Column(name = "SOEISEEM", nullable = false)
    private int soeiseem;
    @Basic(optional = false)
    @Column(name = "SOEICOER", nullable = false, length = 3)
    private String soeicoer;
    @Basic(optional = false)
    @Column(name = "SOADTIREP", nullable = false, length = 2)
    private String soadtirep;
    @Basic(optional = false)
    @Column(name = "SOADTIDICP", nullable = false)
    private short soadtidicp;
    @Basic(optional = false)
    @Column(name = "SOADTICAP", nullable = false)
    private short soadticap;
    @Basic(optional = false)
    @Column(name = "SOADCALLEP", nullable = false, length = 100)
    private String soadcallep;
    @Basic(optional = false)
    @Column(name = "SOADNUPUP", nullable = false, length = 10)
    private String soadnupup;
    @Basic(optional = false)
    @Column(name = "SOADINADP", nullable = false, length = 100)
    private String soadinadp;
    @Basic(optional = false)
    @Column(name = "SOADCOPOP", nullable = false, length = 6)
    private String soadcopop;
    @Basic(optional = false)
    @Column(name = "SOADZONAP", nullable = false, length = 60)
    private String soadzonap;
    @Basic(optional = false)
    @Column(name = "SOADPAISP", nullable = false)
    private short soadpaisp;
    @Basic(optional = false)
    @Column(name = "SOADDPTOP", nullable = false, length = 5)
    private String soaddptop;
    @Basic(optional = false)
    @Column(name = "SOADLOCP", nullable = false)
    private int soadlocp;
    @Basic(optional = false)
    @Column(name = "SOADCOTEPP", nullable = false)
    private short soadcotepp;
    @Basic(optional = false)
    @Column(name = "SOADCOTEAP", nullable = false)
    private int soadcoteap;
    @Basic(optional = false)
    @Column(name = "SOADTELP", nullable = false)
    private long soadtelp;
    @Basic(optional = false)
    @Column(name = "SOADTEINP", nullable = false)
    private int soadteinp;
    @Basic(optional = false)
    @Column(name = "SOADCOCEPP", nullable = false)
    private short soadcocepp;
    @Basic(optional = false)
    @Column(name = "SOADCELP", nullable = false)
    private long soadcelp;
    @Basic(optional = false)
    @Column(name = "SOADCOERP", nullable = false, length = 3)
    private String soadcoerp;
    @Basic(optional = false)
    @Column(name = "SOADTIREC", nullable = false, length = 2)
    private String soadtirec;
    @Basic(optional = false)
    @Column(name = "SOADTIDICC", nullable = false)
    private short soadtidicc;
    @Basic(optional = false)
    @Column(name = "SOADTICAC", nullable = false)
    private short soadticac;
    @Basic(optional = false)
    @Column(name = "SOADCALLEC", nullable = false, length = 100)
    private String soadcallec;
    @Basic(optional = false)
    @Column(name = "SOADNUPUC", nullable = false, length = 10)
    private String soadnupuc;
    @Basic(optional = false)
    @Column(name = "SOADINADC", nullable = false, length = 100)
    private String soadinadc;
    @Basic(optional = false)
    @Column(name = "SOADCOPOC", nullable = false, length = 6)
    private String soadcopoc;
    @Basic(optional = false)
    @Column(name = "SOADZONAC", nullable = false, length = 60)
    private String soadzonac;
    @Basic(optional = false)
    @Column(name = "SOADPAISC", nullable = false)
    private short soadpaisc;
    @Basic(optional = false)
    @Column(name = "SOADDPTOC", nullable = false, length = 5)
    private String soaddptoc;
    @Basic(optional = false)
    @Column(name = "SOADLOCC", nullable = false)
    private int soadlocc;
    @Basic(optional = false)
    @Column(name = "SOADCOTEPC", nullable = false)
    private short soadcotepc;
    @Basic(optional = false)
    @Column(name = "SOADCOTEAC", nullable = false)
    private int soadcoteac;
    @Basic(optional = false)
    @Column(name = "SOADTELC", nullable = false)
    private long soadtelc;
    @Basic(optional = false)
    @Column(name = "SOADTEINC", nullable = false)
    private int soadteinc;
    @Basic(optional = false)
    @Column(name = "SOADCOCEPC", nullable = false)
    private short soadcocepc;
    @Basic(optional = false)
    @Column(name = "SOADCELC", nullable = false)
    private long soadcelc;
    @Basic(optional = false)
    @Column(name = "SOADCOERC", nullable = false, length = 3)
    private String soadcoerc;
    @Basic(optional = false)
    @Column(name = "SOPRTIRE", nullable = false, length = 2)
    private String soprtire;
    @Basic(optional = false)
    @Column(name = "SOPRCOPR", nullable = false, length = 10)
    private String soprcopr;
    @Basic(optional = false)
    @Column(name = "SOPRGRAF", nullable = false, length = 4)
    private String soprgraf;
    @Basic(optional = false)
    @Column(name = "SOPRNUTAEX", nullable = false, length = 19)
    private String soprnutaex;
    @Basic(optional = false)
    @Column(name = "SOPRFEVEEX", nullable = false, length = 6)
    private String soprfeveex;
    @Basic(optional = false)
    @Column(name = "SOPRBIN", nullable = false, length = 19)
    private String soprbin;
    @Basic(optional = false)
    @Column(name = "SOPRNUTCEN", nullable = false, length = 36)
    private String soprnutcen;
    @Basic(optional = false)
    @Column(name = "SOPRINSEG", nullable = false)
    private short soprinseg;
    @Basic(optional = false)
    @Column(name = "SOPRNUTC", nullable = false, length = 19)
    private String soprnutc;
    @Basic(optional = false)
    @Column(name = "SOPRFEVETC", nullable = false, length = 6)
    private String soprfevetc;
    @Basic(optional = false)
    @Column(name = "SOPRSENUTC", nullable = false)
    private short soprsenutc;
    @Basic(optional = false)
    @Column(name = "SOPRCOER", nullable = false, length = 3)
    private String soprcoer;
    @Basic(optional = false)
    @Column(name = "SOCADM", nullable = false)
    private Character socadm;
    @Basic(optional = false)
    @Column(name = "SOSUCU", nullable = false)
    private short sosucu;
    @Id
    @Basic(optional = false)
    @Column(name = "SONSOL", nullable = false)
    private int sonsol;
    @Basic(optional = false)
    @Column(name = "SOFEPRO1", nullable = false)
    private int sofepro1;
    @Basic(optional = false)
    @Column(name = "SOTDOC", nullable = false, length = 2)
    private String tipoDocumento;
    @Basic(optional = false)
    @Column(name = "SOESTC", nullable = false)
    private Character soestc;
    @Basic(optional = false)
    @Column(name = "SOSEX", nullable = false)
    private Character sosex;
    @Basic(optional = false)
    @Column(name = "SOTTAR", nullable = false)
    private String tipoTarjeta;
    @Basic(optional = false)
    @Column(name = "SONUTC", nullable = false)
    private long sonutc;
    @Basic(optional = false)
    @Column(name = "SONUT1", nullable = false, length = 9)
    private String sonut1;
    @Basic(optional = false)
    @Column(name = "SOFEENV", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date sofeenv;
    @Basic(optional = false)
    @Column(name = "SOFERES", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date soferes;
    @Basic(optional = false)
    @Column(name = "SOEST", nullable = false, length = 4)
    private String soest;

    @Transient
    private EstadoSolicitudTC estado;

    @Basic(optional = false)
    @Column(name = "REIDEN", nullable = false)
    private long idArchivoEnvio;
    @Basic(optional = false)
    @Column(name = "REIDRE", nullable = false)
    private long idArchivoRecepcion;

    @Basic(optional = false)
    @Column(name = "REPGEN", nullable = false, length = 10)
    private String usuarioCreador;
    @Basic(optional = false)
    @Column(name = "REFEEN", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Basic(optional = false)
    @Column(name = "REPGRE", nullable = false, length = 10)
    private String usuarioModificador;
    @Basic(optional = false)
    @Column(name = "REFERE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;


    public String getSogatire() {
        return sogatire;
    }

    public void setSogatire(String sogatire) {
        this.sogatire = sogatire;
    }

    public int getSogagrpr() {
        return sogagrpr;
    }

    public void setSogagrpr(int sogagrpr) {
        this.sogagrpr = sogagrpr;
    }

    public short getSogaprcu() {
        return sogaprcu;
    }

    public void setSogaprcu(short sogaprcu) {
        this.sogaprcu = sogaprcu;
    }

    public short getSogaticu() {
        return sogaticu;
    }

    public void setSogaticu(short sogaticu) {
        this.sogaticu = sogaticu;
    }

    public String getSogacoem() {
        return sogacoem;
    }

    public void setSogacoem(String sogacoem) {
        this.sogacoem = sogacoem;
    }

    public String getSogasucu1() {
        return sogasucu1;
    }

    public void setSogasucu1(String sogasucu1) {
        this.sogasucu1 = sogasucu1;
    }

    public String getSogaagem() {
        return sogaagem;
    }

    public void setSogaagem(String sogaagem) {
        this.sogaagem = sogaagem;
    }

    public short getSogainec() {
        return sogainec;
    }

    public void setSogainec(short sogainec) {
        this.sogainec = sogainec;
    }

    public short getSogainecm() {
        return sogainecm;
    }

    public void setSogainecm(short sogainecm) {
        this.sogainecm = sogainecm;
    }

    public String getSogaemail() {
        return sogaemail;
    }

    public void setSogaemail(String sogaemail) {
        this.sogaemail = sogaemail;
    }

    public short getSogainse() {
        return sogainse;
    }

    public void setSogainse(short sogainse) {
        this.sogainse = sogainse;
    }

    public BigInteger getSoganutcnc() {
        return soganutcnc;
    }

    public void setSoganutcnc(BigInteger soganutcnc) {
        this.soganutcnc = soganutcnc;
    }

    public String getSogacoer() {
        return sogacoer;
    }

    public void setSogacoer(String sogacoer) {
        this.sogacoer = sogacoer;
    }

    public String getSocctire() {
        return socctire;
    }

    public void setSocctire(String socctire) {
        this.socctire = socctire;
    }

    public BigDecimal getSocclicr() {
        return socclicr;
    }

    public void setSocclicr(BigDecimal socclicr) {
        this.socclicr = socclicr;
    }

    public short getSoccmoli() {
        return soccmoli;
    }

    public void setSoccmoli(short soccmoli) {
        this.soccmoli = soccmoli;
    }

    public short getSoccfeci() {
        return soccfeci;
    }

    public void setSoccfeci(short soccfeci) {
        this.soccfeci = soccfeci;
    }

    public short getSoccfopa() {
        return soccfopa;
    }

    public void setSoccfopa(short soccfopa) {
        this.soccfopa = soccfopa;
    }

    public short getSocctide() {
        return socctide;
    }

    public void setSocctide(short socctide) {
        this.socctide = socctide;
    }

    public String getSocccoer() {
        return socccoer;
    }

    public void setSocccoer(String socccoer) {
        this.socccoer = socccoer;
    }

    public String getSocjtire() {
        return socjtire;
    }

    public void setSocjtire(String socjtire) {
        this.socjtire = socjtire;
    }

    public String getSocjraso() {
        return socjraso;
    }

    public void setSocjraso(String socjraso) {
        this.socjraso = socjraso;
    }

    public String getSocjnoco() {
        return socjnoco;
    }

    public void setSocjnoco(String socjnoco) {
        this.socjnoco = socjnoco;
    }

    public short getSocjtido() {
        return socjtido;
    }

    public void setSocjtido(short socjtido) {
        this.socjtido = socjtido;
    }

    public String getSocjdoem() {
        return socjdoem;
    }

    public void setSocjdoem(String socjdoem) {
        this.socjdoem = socjdoem;
    }

    public String getSocjfeia() {
        return socjfeia;
    }

    public void setSocjfeia(String socjfeia) {
        this.socjfeia = socjfeia;
    }

    public short getSocjpare() {
        return socjpare;
    }

    public void setSocjpare(short socjpare) {
        this.socjpare = socjpare;
    }

    public String getSocjfefe() {
        return socjfefe;
    }

    public void setSocjfefe(String socjfefe) {
        this.socjfefe = socjfefe;
    }

    public short getSocjpafe() {
        return socjpafe;
    }

    public void setSocjpafe(short socjpafe) {
        this.socjpafe = socjpafe;
    }

    public short getSocjacec() {
        return socjacec;
    }

    public void setSocjacec(short socjacec) {
        this.socjacec = socjacec;
    }

    public String getSocjtiso() {
        return socjtiso;
    }

    public void setSocjtiso(String socjtiso) {
        this.socjtiso = socjtiso;
    }

    public short getSocjalem() {
        return socjalem;
    }

    public void setSocjalem(short socjalem) {
        this.socjalem = socjalem;
    }

    public String getSocjcaem() {
        return socjcaem;
    }

    public void setSocjcaem(String socjcaem) {
        this.socjcaem = socjcaem;
    }

    public String getSocjmofa() {
        return socjmofa;
    }

    public void setSocjmofa(String socjmofa) {
        this.socjmofa = socjmofa;
    }

    public BigDecimal getSocjvafa() {
        return socjvafa;
    }

    public void setSocjvafa(BigDecimal socjvafa) {
        this.socjvafa = socjvafa;
    }

    public short getSocjmopa() {
        return socjmopa;
    }

    public void setSocjmopa(short socjmopa) {
        this.socjmopa = socjmopa;
    }

    public BigDecimal getSocjvapa() {
        return socjvapa;
    }

    public void setSocjvapa(BigDecimal socjvapa) {
        this.socjvapa = socjvapa;
    }

    public short getSocjdcpa() {
        return socjdcpa;
    }

    public void setSocjdcpa(short socjdcpa) {
        this.socjdcpa = socjdcpa;
    }

    public String getSocjcoer() {
        return socjcoer;
    }

    public void setSocjcoer(String socjcoer) {
        this.socjcoer = socjcoer;
    }

    public String getSoadtirel() {
        return soadtirel;
    }

    public void setSoadtirel(String soadtirel) {
        this.soadtirel = soadtirel;
    }

    public short getSoadtidicl() {
        return soadtidicl;
    }

    public void setSoadtidicl(short soadtidicl) {
        this.soadtidicl = soadtidicl;
    }

    public short getSoadtical() {
        return soadtical;
    }

    public void setSoadtical(short soadtical) {
        this.soadtical = soadtical;
    }

    public String getSoadcallel() {
        return soadcallel;
    }

    public void setSoadcallel(String soadcallel) {
        this.soadcallel = soadcallel;
    }

    public String getSoadnupul() {
        return soadnupul;
    }

    public void setSoadnupul(String soadnupul) {
        this.soadnupul = soadnupul;
    }

    public String getSoadinadl() {
        return soadinadl;
    }

    public void setSoadinadl(String soadinadl) {
        this.soadinadl = soadinadl;
    }

    public String getSoadcopol() {
        return soadcopol;
    }

    public void setSoadcopol(String soadcopol) {
        this.soadcopol = soadcopol;
    }

    public String getSoadzonal() {
        return soadzonal;
    }

    public void setSoadzonal(String soadzonal) {
        this.soadzonal = soadzonal;
    }

    public short getSoadpaisl() {
        return soadpaisl;
    }

    public void setSoadpaisl(short soadpaisl) {
        this.soadpaisl = soadpaisl;
    }

    public String getSoaddptol() {
        return soaddptol;
    }

    public void setSoaddptol(String soaddptol) {
        this.soaddptol = soaddptol;
    }

    public int getSoadlocl() {
        return soadlocl;
    }

    public void setSoadlocl(int soadlocl) {
        this.soadlocl = soadlocl;
    }

    public short getSoadcotepl() {
        return soadcotepl;
    }

    public void setSoadcotepl(short soadcotepl) {
        this.soadcotepl = soadcotepl;
    }

    public int getSoadcoteal() {
        return soadcoteal;
    }

    public void setSoadcoteal(int soadcoteal) {
        this.soadcoteal = soadcoteal;
    }

    public long getSoadtell() {
        return soadtell;
    }

    public void setSoadtell(long soadtell) {
        this.soadtell = soadtell;
    }

    public int getSoadteinl() {
        return soadteinl;
    }

    public void setSoadteinl(int soadteinl) {
        this.soadteinl = soadteinl;
    }

    public short getSoadcocepl() {
        return soadcocepl;
    }

    public void setSoadcocepl(short soadcocepl) {
        this.soadcocepl = soadcocepl;
    }

    public long getSoadcell() {
        return soadcell;
    }

    public void setSoadcell(long soadcell) {
        this.soadcell = soadcell;
    }

    public String getSoadcoerl() {
        return soadcoerl;
    }

    public void setSoadcoerl(String soadcoerl) {
        this.soadcoerl = soadcoerl;
    }

    public String getSoadtirem() {
        return soadtirem;
    }

    public void setSoadtirem(String soadtirem) {
        this.soadtirem = soadtirem;
    }

    public short getSoadtidicm() {
        return soadtidicm;
    }

    public void setSoadtidicm(short soadtidicm) {
        this.soadtidicm = soadtidicm;
    }

    public short getSoadticam() {
        return soadticam;
    }

    public void setSoadticam(short soadticam) {
        this.soadticam = soadticam;
    }

    public String getSoadcallem() {
        return soadcallem;
    }

    public void setSoadcallem(String soadcallem) {
        this.soadcallem = soadcallem;
    }

    public String getSoadnupum() {
        return soadnupum;
    }

    public void setSoadnupum(String soadnupum) {
        this.soadnupum = soadnupum;
    }

    public String getSoadinadm() {
        return soadinadm;
    }

    public void setSoadinadm(String soadinadm) {
        this.soadinadm = soadinadm;
    }

    public String getSoadcopom() {
        return soadcopom;
    }

    public void setSoadcopom(String soadcopom) {
        this.soadcopom = soadcopom;
    }

    public String getSoadzonam() {
        return soadzonam;
    }

    public void setSoadzonam(String soadzonam) {
        this.soadzonam = soadzonam;
    }

    public short getSoadpaism() {
        return soadpaism;
    }

    public void setSoadpaism(short soadpaism) {
        this.soadpaism = soadpaism;
    }

    public String getSoaddptom() {
        return soaddptom;
    }

    public void setSoaddptom(String soaddptom) {
        this.soaddptom = soaddptom;
    }

    public int getSoadlocm() {
        return soadlocm;
    }

    public void setSoadlocm(int soadlocm) {
        this.soadlocm = soadlocm;
    }

    public short getSoadcotepm() {
        return soadcotepm;
    }

    public void setSoadcotepm(short soadcotepm) {
        this.soadcotepm = soadcotepm;
    }

    public int getSoadcoteam() {
        return soadcoteam;
    }

    public void setSoadcoteam(int soadcoteam) {
        this.soadcoteam = soadcoteam;
    }

    public long getSoadtelm() {
        return soadtelm;
    }

    public void setSoadtelm(long soadtelm) {
        this.soadtelm = soadtelm;
    }

    public int getSoadteinm() {
        return soadteinm;
    }

    public void setSoadteinm(int soadteinm) {
        this.soadteinm = soadteinm;
    }

    public short getSoadcocepm() {
        return soadcocepm;
    }

    public void setSoadcocepm(short soadcocepm) {
        this.soadcocepm = soadcocepm;
    }

    public long getSoadcelm() {
        return soadcelm;
    }

    public void setSoadcelm(long soadcelm) {
        this.soadcelm = soadcelm;
    }

    public String getSoadcoerm() {
        return soadcoerm;
    }

    public void setSoadcoerm(String soadcoerm) {
        this.soadcoerm = soadcoerm;
    }

    public String getSocftire() {
        return socftire;
    }

    public void setSocftire(String socftire) {
        this.socftire = socftire;
    }

    public short getSocfticl() {
        return socfticl;
    }

    public void setSocfticl(short socfticl) {
        this.socfticl = socfticl;
    }

    public String getSocfnocl() {
        return socfnocl;
    }

    public void setSocfnocl(String socfnocl) {
        this.socfnocl = socfnocl;
    }

    public String getSocfapcl() {
        return socfapcl;
    }

    public void setSocfapcl(String socfapcl) {
        this.socfapcl = socfapcl;
    }

    public short getSocftido() {
        return socftido;
    }

    public void setSocftido(short socftido) {
        this.socftido = socftido;
    }

    public String getSocfdocl() {
        return socfdocl;
    }

    public void setSocfdocl(String socfdocl) {
        this.socfdocl = socfdocl;
    }

    public short getSocftidoa() {
        return socftidoa;
    }

    public void setSocftidoa(short socftidoa) {
        this.socftidoa = socftidoa;
    }

    public String getSocfdocla() {
        return socfdocla;
    }

    public void setSocfdocla(String socfdocla) {
        this.socfdocla = socfdocla;
    }

    public String getSocffena() {
        return socffena;
    }

    public void setSocffena(String socffena) {
        this.socffena = socffena;
    }

    public short getSocfesci() {
        return socfesci;
    }

    public void setSocfesci(short socfesci) {
        this.socfesci = socfesci;
    }

    public short getSocfgecl() {
        return socfgecl;
    }

    public void setSocfgecl(short socfgecl) {
        this.socfgecl = socfgecl;
    }

    public short getSocfpacl() {
        return socfpacl;
    }

    public void setSocfpacl(short socfpacl) {
        this.socfpacl = socfpacl;
    }

    public String getSocfluna() {
        return socfluna;
    }

    public void setSocfluna(String socfluna) {
        this.socfluna = socfluna;
    }

    public short getSocfcahi() {
        return socfcahi;
    }

    public void setSocfcahi(short socfcahi) {
        this.socfcahi = socfcahi;
    }

    public String getSocfnoem() {
        return socfnoem;
    }

    public void setSocfnoem(String socfnoem) {
        this.socfnoem = socfnoem;
    }

    public String getSocfemail() {
        return socfemail;
    }

    public void setSocfemail(String socfemail) {
        this.socfemail = socfemail;
    }

    public short getSocfdcpa() {
        return socfdcpa;
    }

    public void setSocfdcpa(short socfdcpa) {
        this.socfdcpa = socfdcpa;
    }

    public short getSocfdfpa() {
        return socfdfpa;
    }

    public void setSocfdfpa(short socfdfpa) {
        this.socfdfpa = socfdfpa;
    }

    public int getSocfnusol() {
        return socfnusol;
    }

    public void setSocfnusol(int socfnusol) {
        this.socfnusol = socfnusol;
    }

    public String getSocfcoer() {
        return socfcoer;
    }

    public void setSocfcoer(String socfcoer) {
        this.socfcoer = socfcoer;
    }

    public String getSoeitire() {
        return soeitire;
    }

    public void setSoeitire(String soeitire) {
        this.soeitire = soeitire;
    }

    public short getSoeiexpo() {
        return soeiexpo;
    }

    public void setSoeiexpo(short soeiexpo) {
        this.soeiexpo = soeiexpo;
    }

    public short getSoeicontr() {
        return soeicontr;
    }

    public void setSoeicontr(short soeicontr) {
        this.soeicontr = soeicontr;
    }

    public String getSoeifeveco() {
        return soeifeveco;
    }

    public void setSoeifeveco(String soeifeveco) {
        this.soeifeveco = soeifeveco;
    }

    public String getSoeiempl() {
        return soeiempl;
    }

    public void setSoeiempl(String soeiempl) {
        this.soeiempl = soeiempl;
    }

    public short getSoeitido() {
        return soeitido;
    }

    public void setSoeitido(short soeitido) {
        this.soeitido = soeitido;
    }

    public String getSoeidoem() {
        return soeidoem;
    }

    public void setSoeidoem(String soeidoem) {
        this.soeidoem = soeidoem;
    }

    public String getSoeifein() {
        return soeifein;
    }

    public void setSoeifein(String soeifein) {
        this.soeifein = soeifein;
    }

    public short getSoeiinpr() {
        return soeiinpr;
    }

    public void setSoeiinpr(short soeiinpr) {
        this.soeiinpr = soeiinpr;
    }

    public short getSoeiinext() {
        return soeiinext;
    }

    public void setSoeiinext(short soeiinext) {
        this.soeiinext = soeiinext;
    }

    public long getSoeitoin() {
        return soeitoin;
    }

    public void setSoeitoin(long soeitoin) {
        this.soeitoin = soeitoin;
    }

    public String getSoeicaem() {
        return soeicaem;
    }

    public void setSoeicaem(String soeicaem) {
        this.soeicaem = soeicaem;
    }

    public int getSoeiocup() {
        return soeiocup;
    }

    public void setSoeiocup(int soeiocup) {
        this.soeiocup = soeiocup;
    }

    public int getSoeiseem() {
        return soeiseem;
    }

    public void setSoeiseem(int soeiseem) {
        this.soeiseem = soeiseem;
    }

    public String getSoeicoer() {
        return soeicoer;
    }

    public void setSoeicoer(String soeicoer) {
        this.soeicoer = soeicoer;
    }

    public String getSoadtirep() {
        return soadtirep;
    }

    public void setSoadtirep(String soadtirep) {
        this.soadtirep = soadtirep;
    }

    public short getSoadtidicp() {
        return soadtidicp;
    }

    public void setSoadtidicp(short soadtidicp) {
        this.soadtidicp = soadtidicp;
    }

    public short getSoadticap() {
        return soadticap;
    }

    public void setSoadticap(short soadticap) {
        this.soadticap = soadticap;
    }

    public String getSoadcallep() {
        return soadcallep;
    }

    public void setSoadcallep(String soadcallep) {
        this.soadcallep = soadcallep;
    }

    public String getSoadnupup() {
        return soadnupup;
    }

    public void setSoadnupup(String soadnupup) {
        this.soadnupup = soadnupup;
    }

    public String getSoadinadp() {
        return soadinadp;
    }

    public void setSoadinadp(String soadinadp) {
        this.soadinadp = soadinadp;
    }

    public String getSoadcopop() {
        return soadcopop;
    }

    public void setSoadcopop(String soadcopop) {
        this.soadcopop = soadcopop;
    }

    public String getSoadzonap() {
        return soadzonap;
    }

    public void setSoadzonap(String soadzonap) {
        this.soadzonap = soadzonap;
    }

    public short getSoadpaisp() {
        return soadpaisp;
    }

    public void setSoadpaisp(short soadpaisp) {
        this.soadpaisp = soadpaisp;
    }

    public String getSoaddptop() {
        return soaddptop;
    }

    public void setSoaddptop(String soaddptop) {
        this.soaddptop = soaddptop;
    }

    public int getSoadlocp() {
        return soadlocp;
    }

    public void setSoadlocp(int soadlocp) {
        this.soadlocp = soadlocp;
    }

    public short getSoadcotepp() {
        return soadcotepp;
    }

    public void setSoadcotepp(short soadcotepp) {
        this.soadcotepp = soadcotepp;
    }

    public int getSoadcoteap() {
        return soadcoteap;
    }

    public void setSoadcoteap(int soadcoteap) {
        this.soadcoteap = soadcoteap;
    }

    public long getSoadtelp() {
        return soadtelp;
    }

    public void setSoadtelp(long soadtelp) {
        this.soadtelp = soadtelp;
    }

    public int getSoadteinp() {
        return soadteinp;
    }

    public void setSoadteinp(int soadteinp) {
        this.soadteinp = soadteinp;
    }

    public short getSoadcocepp() {
        return soadcocepp;
    }

    public void setSoadcocepp(short soadcocepp) {
        this.soadcocepp = soadcocepp;
    }

    public long getSoadcelp() {
        return soadcelp;
    }

    public void setSoadcelp(long soadcelp) {
        this.soadcelp = soadcelp;
    }

    public String getSoadcoerp() {
        return soadcoerp;
    }

    public void setSoadcoerp(String soadcoerp) {
        this.soadcoerp = soadcoerp;
    }

    public String getSoadtirec() {
        return soadtirec;
    }

    public void setSoadtirec(String soadtirec) {
        this.soadtirec = soadtirec;
    }

    public short getSoadtidicc() {
        return soadtidicc;
    }

    public void setSoadtidicc(short soadtidicc) {
        this.soadtidicc = soadtidicc;
    }

    public short getSoadticac() {
        return soadticac;
    }

    public void setSoadticac(short soadticac) {
        this.soadticac = soadticac;
    }

    public String getSoadcallec() {
        return soadcallec;
    }

    public void setSoadcallec(String soadcallec) {
        this.soadcallec = soadcallec;
    }

    public String getSoadnupuc() {
        return soadnupuc;
    }

    public void setSoadnupuc(String soadnupuc) {
        this.soadnupuc = soadnupuc;
    }

    public String getSoadinadc() {
        return soadinadc;
    }

    public void setSoadinadc(String soadinadc) {
        this.soadinadc = soadinadc;
    }

    public String getSoadcopoc() {
        return soadcopoc;
    }

    public void setSoadcopoc(String soadcopoc) {
        this.soadcopoc = soadcopoc;
    }

    public String getSoadzonac() {
        return soadzonac;
    }

    public void setSoadzonac(String soadzonac) {
        this.soadzonac = soadzonac;
    }

    public short getSoadpaisc() {
        return soadpaisc;
    }

    public void setSoadpaisc(short soadpaisc) {
        this.soadpaisc = soadpaisc;
    }

    public String getSoaddptoc() {
        return soaddptoc;
    }

    public void setSoaddptoc(String soaddptoc) {
        this.soaddptoc = soaddptoc;
    }

    public int getSoadlocc() {
        return soadlocc;
    }

    public void setSoadlocc(int soadlocc) {
        this.soadlocc = soadlocc;
    }

    public short getSoadcotepc() {
        return soadcotepc;
    }

    public void setSoadcotepc(short soadcotepc) {
        this.soadcotepc = soadcotepc;
    }

    public int getSoadcoteac() {
        return soadcoteac;
    }

    public void setSoadcoteac(int soadcoteac) {
        this.soadcoteac = soadcoteac;
    }

    public long getSoadtelc() {
        return soadtelc;
    }

    public void setSoadtelc(long soadtelc) {
        this.soadtelc = soadtelc;
    }

    public int getSoadteinc() {
        return soadteinc;
    }

    public void setSoadteinc(int soadteinc) {
        this.soadteinc = soadteinc;
    }

    public short getSoadcocepc() {
        return soadcocepc;
    }

    public void setSoadcocepc(short soadcocepc) {
        this.soadcocepc = soadcocepc;
    }

    public long getSoadcelc() {
        return soadcelc;
    }

    public void setSoadcelc(long soadcelc) {
        this.soadcelc = soadcelc;
    }

    public String getSoadcoerc() {
        return soadcoerc;
    }

    public void setSoadcoerc(String soadcoerc) {
        this.soadcoerc = soadcoerc;
    }

    public String getSoprtire() {
        return soprtire;
    }

    public void setSoprtire(String soprtire) {
        this.soprtire = soprtire;
    }

    public String getSoprcopr() {
        return soprcopr;
    }

    public void setSoprcopr(String soprcopr) {
        this.soprcopr = soprcopr;
    }

    public String getSoprgraf() {
        return soprgraf;
    }

    public void setSoprgraf(String soprgraf) {
        this.soprgraf = soprgraf;
    }

    public String getSoprnutaex() {
        return soprnutaex;
    }

    public void setSoprnutaex(String soprnutaex) {
        this.soprnutaex = soprnutaex;
    }

    public String getSoprfeveex() {
        return soprfeveex;
    }

    public void setSoprfeveex(String soprfeveex) {
        this.soprfeveex = soprfeveex;
    }

    public String getSoprbin() {
        return soprbin;
    }

    public void setSoprbin(String soprbin) {
        this.soprbin = soprbin;
    }

    public String getSoprnutcen() {
        return soprnutcen;
    }

    public void setSoprnutcen(String soprnutcen) {
        this.soprnutcen = soprnutcen;
    }

    public short getSoprinseg() {
        return soprinseg;
    }

    public void setSoprinseg(short soprinseg) {
        this.soprinseg = soprinseg;
    }

    public String getSoprnutc() {
        return soprnutc;
    }

    public void setSoprnutc(String soprnutc) {
        this.soprnutc = soprnutc;
    }

    public String getSoprfevetc() {
        return soprfevetc;
    }

    public void setSoprfevetc(String soprfevetc) {
        this.soprfevetc = soprfevetc;
    }

    public short getSoprsenutc() {
        return soprsenutc;
    }

    public void setSoprsenutc(short soprsenutc) {
        this.soprsenutc = soprsenutc;
    }

    public String getSoprcoer() {
        return soprcoer;
    }

    public void setSoprcoer(String soprcoer) {
        this.soprcoer = soprcoer;
    }

    public Character getSocadm() {
        return socadm;
    }

    public void setSocadm(Character socadm) {
        this.socadm = socadm;
    }

    public short getSosucu() {
        return sosucu;
    }

    public void setSosucu(short sosucu) {
        this.sosucu = sosucu;
    }

    public int getSonsol() {
        return sonsol;
    }

    public void setSonsol(int sonsol) {
        this.sonsol = sonsol;
    }

    public int getSofepro1() {
        return sofepro1;
    }

    public void setSofepro1(int sofepro1) {
        this.sofepro1 = sofepro1;
    }

    public Character getSoestc() {
        return soestc;
    }

    public void setSoestc(Character soestc) {
        this.soestc = soestc;
    }

    public Character getSosex() {
        return sosex;
    }

    public void setSosex(Character sosex) {
        this.sosex = sosex;
    }

    public long getSonutc() {
        return sonutc;
    }

    public void setSonutc(long sonutc) {
        this.sonutc = sonutc;
    }

    public String getSonut1() {
        return sonut1;
    }

    public void setSonut1(String sonut1) {
        this.sonut1 = sonut1;
    }

    public Date getSofeenv() {
        return sofeenv;
    }

    public void setSofeenv(Date sofeenv) {
        this.sofeenv = sofeenv;
    }

    public Date getSoferes() {
        return soferes;
    }

    public void setSoferes(Date soferes) {
        this.soferes = soferes;
    }

    public String getSoest() {
        return soest;
    }

    public void setSoest(String soest) {
        this.soest = soest;
    }

    public EstadoSolicitudTC getEstado() {
        return estado;
    }

    public void setEstado(EstadoSolicitudTC estado) {
        this.estado = estado;
    }

    public long getIdArchivoEnvio() {
        return idArchivoEnvio;
    }

    public void setIdArchivoEnvio(long idArchivoEnvio) {
        this.idArchivoEnvio = idArchivoEnvio;
    }

    public long getIdArchivoRecepcion() {
        return idArchivoRecepcion;
    }

    public void setIdArchivoRecepcion(long idArchivoRecepcion) {
        this.idArchivoRecepcion = idArchivoRecepcion;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getTipoTarjeta() {
        return tipoTarjeta;
    }

    public void setTipoTarjeta(String tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }
}
