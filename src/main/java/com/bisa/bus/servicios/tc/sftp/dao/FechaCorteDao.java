package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.FechaCorte;
import com.bisa.bus.servicios.tc.sftp.entities.IdFechaCorte;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 * Created by atenorio on 05/12/2017.
 */
public class FechaCorteDao extends DaoImpl<FechaCorte, IdFechaCorte> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(FacturasATCDao.class);

    @Inject
    protected FechaCorteDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

}
