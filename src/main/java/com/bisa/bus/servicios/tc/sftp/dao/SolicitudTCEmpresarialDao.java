package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.SolicitudTCEmpresarial;
import com.bisa.bus.servicios.tc.sftp.model.EstadoSolicitudTC;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by atenorio on 08/09/2017.
 */
public class SolicitudTCEmpresarialDao extends DaoImpl<SolicitudTCEmpresarial, Integer> implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(SolicitudTCEmpresarialDao.class);

    @Inject
    protected SolicitudTCEmpresarialDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(SolicitudTCEmpresarial solicitud, String user) {
        if (solicitud.getSonsol() > 0) {
            solicitud.setUsuarioModificador(user);
            solicitud.setFechaModificacion(new Date());
            merge(solicitud);
        } else {
            solicitud.setUsuarioCreador(user);
            solicitud.setFechaCreacion(new Date());
            persist(solicitud);
        }
        return true;
    }


    public List<SolicitudTCEmpresarial> getSolicitudesParaEnvio() {
        LOGGER.debug("Obteniendo solicituddes de DB para envio.");
        return doWithTransaction(
                (entityManager, t) -> {

                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<SolicitudTCEmpresarial> q = cb.createQuery(SolicitudTCEmpresarial.class);
                    Root<SolicitudTCEmpresarial> p = q.from(SolicitudTCEmpresarial.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(p.get("soest").in(EstadoSolicitudTC.GENE.name()));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<SolicitudTCEmpresarial> query = entityManager.createQuery(q);
                    return query.getResultList();
                }
        );
    }

    public SolicitudTCEmpresarial getSolicitudCodigoExterno(int codigo) {
        LOGGER.debug("Obteniendo registro de solicitud con nombre:{}.", codigo);

        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<SolicitudTCEmpresarial> q = cb.createQuery(SolicitudTCEmpresarial.class);
                    Root<SolicitudTCEmpresarial> p = q.from(SolicitudTCEmpresarial.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("socfnusol"), codigo));

                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));

                    TypedQuery<SolicitudTCEmpresarial> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                        return query.getResultList().get(0);
                    } else {
                        return null;
                    }
                }
        );

    }

    @Override
    protected Path<Integer> countPath(Root<SolicitudTCEmpresarial> from) {
        return from.get("sonsol");
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<SolicitudTCEmpresarial> from, SolicitudTCEmpresarial example, SolicitudTCEmpresarial example2) {
        List<Predicate> predicates = new LinkedList<>();
        Date fecha = example.getFechaCreacion();

        String cuenta = (example.getSoganutcnc() == null) ? null : StringUtils.trimToNull(example.getSoganutcnc().toString());

        if (fecha != null && example2.getFechaCreacion() != null) {
            Date inicio = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH);
            Date inicio2 = DateUtils.truncate(example2.getFechaCreacion(), Calendar.DAY_OF_MONTH);
            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
            LOGGER.debug("   FECHAS ENTRE [{}] A [{}]", inicio, inicio2);
            predicates.add(cb.between(from.get("fechaCreacion"), inicio, inicio2));
        }

        if (example.getSonsol() != 0) {
            LOGGER.debug(">>>>> solicitud=" + example.getSonsol());
            predicates.add(cb.equal(from.get("sonsol"), example.getSonsol()));
        }
        if (cuenta != null) {
            try {
                Long s = Long.parseLong(cuenta);
                LOGGER.debug(">>>>> cuenta=" + s);
                predicates.add(cb.equal(from.get("soganutcnc"), s));
            } catch (Exception ex) {
                LOGGER.error("Error en ", ex);
            }
        }

        if (example.getSoest() != null) {
            LOGGER.debug(">>>>> estado=" + example.getSoest());
            predicates.add(cb.equal(from.get("soest"), example.getSoest()));
        }
        return predicates.toArray(new Predicate[predicates.size()]);
    }
}
