package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.ExtractoMensual;
import com.bisa.bus.servicios.tc.sftp.entities.IdExtractoMensual;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 02/08/2017.
 */
public class ExtractoMensualDao extends DaoImpl<ExtractoMensual, IdExtractoMensual> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExtractoMensualDao.class);

    @Inject
    protected ExtractoMensualDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(ExtractoMensual registro, String user) {
        registro.setUsuarioCreador(user);
        registro.setFechaCreacion(new Date());
        persist(registro);
        return true;
    }


    public boolean limpiarTodo() {
        LOGGER.debug("Limpiando tabla Extracto Mensual.");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<ExtractoMensual> query = entityManager.createQuery("DELETE FROM ExtractoMensual");
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }
}
