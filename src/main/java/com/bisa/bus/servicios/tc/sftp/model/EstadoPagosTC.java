package com.bisa.bus.servicios.tc.sftp.model;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by atenorio on 30/05/2017.
 */
public enum EstadoPagosTC {
    GENE("Generado"),
    ENVI("Enviado a proceso"),
    ERRV("Error de validaci\u00F3n"),
    ERRI("Error interno"),
    ERRE("Error en envio"),
    ERRP("Error en proceso"),
    PROC("Pago procesado");
    protected static final Logger LOGGER = LoggerFactory.getLogger(EstadoPagosTC.class);

    private String descripcion;

    private EstadoPagosTC(String descripcion) {
        this.descripcion = descripcion;
    }

    public static EstadoPagosTC valorEnum(String valor) {
        if(StringUtils.isEmpty(valor)) valor="";
        List<EstadoPagosTC> estados = new ArrayList<>(Arrays.asList((EstadoPagosTC.values())));
        for (EstadoPagosTC es : estados) {
            if (valor.equals(es.name())){
                return es;
            }
        }
        return null;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
