package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.FtpArchivoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.EstadoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.*;

/**
 * Created by atenorio on 03/05/2017.
 */
public class FtpTransferenciaDao extends DaoImpl<FtpArchivoTransferencia, Long> implements Serializable, IFtpTransferencia {
    private static final Logger LOGGER = LoggerFactory.getLogger(FtpTransferenciaDao.class);

    @Inject
    protected FtpTransferenciaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {

        super(entityManagerFactory);
    }

    @Override
    public FtpArchivoTransferencia getFtpTransferencia(String filename) {
        LOGGER.debug("Obteniendo registro de archivo con nombre:{}.", filename);

        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<FtpArchivoTransferencia> q = cb.createQuery(FtpArchivoTransferencia.class);
                    Root<FtpArchivoTransferencia> p = q.from(FtpArchivoTransferencia.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("archivo"), StringUtils.trimToEmpty(filename)));
                    predicatesAnd.add(p.get("estado").in(EstadoTransferencia.PRTE));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));

                    TypedQuery<FtpArchivoTransferencia> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                        return query.getResultList().get(0);
                    } else {
                        return null;
                    }
                }
        );
    }

    @Override
    public FtpArchivoTransferencia getFtpTransferencia(String filename, Integer fechaProceso) {
        LOGGER.debug("Obteniendo registro de archivo con nombre:{}.", filename);

        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<FtpArchivoTransferencia> q = cb.createQuery(FtpArchivoTransferencia.class);
                    Root<FtpArchivoTransferencia> p = q.from(FtpArchivoTransferencia.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("archivo"), StringUtils.trimToEmpty(filename)));
                    predicatesAnd.add(p.get("estado").in(EstadoTransferencia.PRTE));
                    predicatesAnd.add(p.get("fechaProceso").in(fechaProceso));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    q.orderBy(cb.desc(p.get("fechaCreacion")));
                    TypedQuery<FtpArchivoTransferencia> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                        return query.getResultList().get(0);
                    } else {
                        return null;
                    }
                }
        );
    }

    @Override
    public FtpArchivoTransferencia getFtpTransferenciaPendiente(TipoArchivo tipo, EstadoTransferencia estadoTransferencia) {
        LOGGER.debug("Obteniendo registro de archivo con nombre:{}.", estadoTransferencia);

        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<FtpArchivoTransferencia> q = cb.createQuery(FtpArchivoTransferencia.class);
                    Root<FtpArchivoTransferencia> p = q.from(FtpArchivoTransferencia.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(p.get("estado").in(estadoTransferencia));
                    predicatesAnd.add(p.get("tipo").in(tipo));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    q.orderBy(cb.desc(p.get("fechaCreacion")));

                    TypedQuery<FtpArchivoTransferencia> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                        return query.getResultList().get(0);
                    } else {
                        return null;
                    }
                }
        );
    }

    @Override
    public FtpArchivoTransferencia getFtpTransferenciaPendiente(TipoArchivo tipo) {
        LOGGER.debug("Obteniendo registro de archivo de tipo:{}.", tipo);

        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<FtpArchivoTransferencia> q = cb.createQuery(FtpArchivoTransferencia.class);
                    Root<FtpArchivoTransferencia> p = q.from(FtpArchivoTransferencia.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(p.get("respuesta").in(0L));
                    predicatesAnd.add(p.get("estado").in(EstadoTransferencia.PRTE));
                    predicatesAnd.add(p.get("tipo").in(tipo));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    q.orderBy(cb.desc(p.get("fechaCreacion")));

                    TypedQuery<FtpArchivoTransferencia> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                        return query.getResultList().get(0);
                    } else {
                        return null;
                    }
                }
        );
    }

    @Override
    public List<FtpArchivoTransferencia> getFtpTransferenciaPorEstado(EstadoTransferencia estado) {
        return null;
    }

    @Override
    public boolean guardar(FtpArchivoTransferencia tranferencia, String user) {
        if (tranferencia.getId() != null && tranferencia.getId() > 0) {
            tranferencia.setUsuarioModificador(user);
            tranferencia.setFechaModificacion(new Date());
            merge(tranferencia);
        } else {
            tranferencia.setUsuarioCreador(user);
            tranferencia.setFechaCreacion(new Date());
            persist(tranferencia);
        }
        return true;
    }

    @Override
    protected Path<Long> countPath(Root<FtpArchivoTransferencia> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<FtpArchivoTransferencia> p) {
        Path<String> archivo = p.get("archivo");
        Path<String> tipo = p.get("tipo");
        return Arrays.asList(archivo, tipo);
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<FtpArchivoTransferencia> from, FtpArchivoTransferencia example, FtpArchivoTransferencia example2) {
        List<Predicate> predicates = new LinkedList<Predicate>();
        Date fecha = example.getFechaCreacion();
        String nombreArchivo = StringUtils.trimToNull(example.getArchivo());
        EstadoTransferencia estadoTransferencia = example.getEstado();
        if (nombreArchivo != null) {
            predicates.add(cb.like(from.get("archivo"), "%" + nombreArchivo + "%"));
            LOGGER.debug("Filtro nombre de archivo: {}", nombreArchivo);
        }
        if (estadoTransferencia != null) {
            predicates.add(cb.equal(from.get("estado"), estadoTransferencia));
        }
        if (fecha != null && example2.getFechaCreacion() != null) {
            Date inicio = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH);
            Date inicio2 = DateUtils.truncate(example2.getFechaCreacion(), Calendar.DAY_OF_MONTH);
            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
            LOGGER.debug("   FECHAS ENTRE [{}] A [{}]", inicio, inicio2);
            predicates.add(cb.between(from.get("fechaCreacion"), inicio, inicio2));
        }
        return predicates.toArray(new Predicate[predicates.size()]); //super.createQBE(cb, from, example, example2);
    }
}
