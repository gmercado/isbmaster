package com.bisa.bus.servicios.tc.linkser.model;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by atenorio on 12/01/2017.
 *
 * Descripcion: clase de definicion de parametros para consultas a servicios tc o consulta monitor
 */
public class ConsultaMovimientoSolicitudForm implements  IConsultaMovimientoSolicitud, Serializable {

    private static final long serialVersionUID = 1L;

    private String cuenta;
    private String tipoCuenta;
    private String numeroTarjeta;
    private int fechaInicio;
    private int fechaFin;
    private Date fechaInicial;
    private Date fechaFinal;
    private boolean infoAdicional=false;

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
        this.fechaInicio = formatoFecha(this.fechaInicial);
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
        this.fechaFin = formatoFecha(this.fechaFinal);
    }

    public boolean isInfoAdicional() {
        return infoAdicional;
    }

    public void setInfoAdicional(boolean infoAdicional) {
        this.infoAdicional = infoAdicional;
    }

    public int getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(int fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public int getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(int fechaFin) {
        this.fechaFin = fechaFin;
    }

    public static final SimpleDateFormat FECHA_FORMATO = new SimpleDateFormat("yyyyMMdd");
    public static final Integer formatoFecha(Date fecha) {
        if (fecha == null) {
            return null;
        }
        return Integer.parseInt(FECHA_FORMATO.format(fecha));
    }

    @Override
    public String toString() {
        return "ConsultaMovimientoSolicitud{" +
                "cuenta='"+ StringUtils.trimToEmpty(getCuenta())+"'"+
                ", tipoCuenta='"+ StringUtils.trimToEmpty(getTipoCuenta())+"'"+
                ", numeroTarjeta='"+ StringUtils.trimToEmpty(getNumeroTarjeta())+"'"+
                "}";
    }
}
