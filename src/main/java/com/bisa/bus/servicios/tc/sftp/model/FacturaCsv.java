package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Created by atenorio on 04/07/2017.
 */
public class FacturaCsv {

    
    @FormatCsv(name = "FCESPESIF", nullable = false, length = 1)
    private Character especificacion;
    
    @FormatCsv(name = "FCNRO", nullable = false, length = 6)
    private String numeroSeq;
    @Id
    
    @FormatCsv(name = "FCFFACT", nullable = false, length = 10)
    private String fechaEmision;
    
    @FormatCsv(name = "FCNFACT", nullable = false, length = 15)
    private String numeroFactura;
    
    @FormatCsv(name = "FCNAUTO", nullable = false, length = 15)
    private String numeroAutorizacion;
    
    @FormatCsv(name = "FCESTADO", nullable = false, length = 1)
    private Character estadoFactura;
    
    @FormatCsv(name = "FCNITCI", nullable = false, length = 13)
    private String nit;
    
    @FormatCsv(name = "FCRSOCIAL", nullable = false, length = 150)
    private String razonSocial;

    
    @FormatCsv(name = "FCIMPTOTN", nullable = false, length = 12, precision = 10, scale = 2)
    private BigDecimal importeTotal;
    
    @FormatCsv(name = "FCIMPOTRN", nullable = false, length = 12, precision = 10, scale = 2)
    private BigDecimal importeTasas;
    
    @FormatCsv(name = "FCIMPEOEN", nullable = false, length = 12, precision = 10, scale = 2)
    private BigDecimal importeOperacionExentas;
    
    @FormatCsv(name = "FCIMPVGRN", nullable = false, length = 12, precision = 10, scale = 2)
    private BigDecimal ventasGravadasTasaCero;
    
    @FormatCsv(name = "FCIMPSUBTN", nullable = false, length = 12, precision = 10, scale = 2)
    private BigDecimal subTotal;
    
    @FormatCsv(name = "FCIMPDESCN", nullable = false, length = 12, precision = 10, scale = 2)
    private BigDecimal importeDescuentos;
    
    @FormatCsv(name = "FCIMPBASEN", nullable = false, length = 12,  precision = 10, scale = 2)
    private BigDecimal importeBase;
    
    @FormatCsv(name = "FCDFISCALN", nullable = false,  length = 12, precision = 10, scale = 2)
    private BigDecimal debitoFiscal;

    
    @FormatCsv(name = "FCDCCTRL", nullable = false, length = 17)
    private String codigoControl;

    public Character getEspecificacion() {
        return especificacion;
    }

    public void setEspecificacion(Character especificacion) {
        this.especificacion = especificacion;
    }

    public String getNumeroSeq() {
        return numeroSeq;
    }

    public void setNumeroSeq(String numeroSeq) {
        this.numeroSeq = numeroSeq;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public Character getEstadoFactura() {
        return estadoFactura;
    }

    public void setEstadoFactura(Character estadoFactura) {
        this.estadoFactura = estadoFactura;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public BigDecimal getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(BigDecimal importeTotal) {
        this.importeTotal = importeTotal;
    }

    public BigDecimal getImporteTasas() {
        return importeTasas;
    }

    public void setImporteTasas(BigDecimal importeTasas) {
        this.importeTasas = importeTasas;
    }

    public BigDecimal getImporteOperacionExentas() {
        return importeOperacionExentas;
    }

    public void setImporteOperacionExentas(BigDecimal importeOperacionExentas) {
        this.importeOperacionExentas = importeOperacionExentas;
    }

    public BigDecimal getVentasGravadasTasaCero() {
        return ventasGravadasTasaCero;
    }

    public void setVentasGravadasTasaCero(BigDecimal ventasGravadasTasaCero) {
        this.ventasGravadasTasaCero = ventasGravadasTasaCero;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getImporteDescuentos() {
        return importeDescuentos;
    }

    public void setImporteDescuentos(BigDecimal importeDescuentos) {
        this.importeDescuentos = importeDescuentos;
    }

    public BigDecimal getImporteBase() {
        return importeBase;
    }

    public void setImporteBase(BigDecimal importeBase) {
        this.importeBase = importeBase;
    }

    public BigDecimal getDebitoFiscal() {
        return debitoFiscal;
    }

    public void setDebitoFiscal(BigDecimal debitoFiscal) {
        this.debitoFiscal = debitoFiscal;
    }

    public String getCodigoControl() {
        return codigoControl;
    }

    public void setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
    }
}
