package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.FacturaATC;
import com.bisa.bus.servicios.tc.sftp.entities.IdFactura;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 04/07/2017.
 */
public class FacturasATCDao extends DaoImpl<FacturaATC, IdFactura> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(FacturasATCDao.class);

    @Inject
    protected FacturasATCDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(FacturaATC registro, String user) {
        registro.setUsuarioCreador(user);
        registro.setFechaCreacion(new Date());
        persist(registro);
        return true;
    }


    public boolean limpiarTodo() {
        LOGGER.debug("Limpiando tabla Factura.");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<FacturaATC> query = entityManager.createQuery("DELETE FROM FacturaATC");//
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }
}
