package com.bisa.bus.servicios.tc.sftp.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.csv.LecturaCSV;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.tc.sftp.dao.PagosHistoricoTCDao;
import com.bisa.bus.servicios.tc.sftp.dao.PagosTCDao;
import com.bisa.bus.servicios.tc.sftp.entities.IdPagosTC;
import com.bisa.bus.servicios.tc.sftp.entities.PagosHistoricoTC;
import com.bisa.bus.servicios.tc.sftp.entities.PagosTC;
import com.bisa.bus.servicios.tc.sftp.model.*;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgProgramaTarjetaCredito;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by atenorio on 29/05/2017.
 */
public class SftpRecepcionPagos implements Serializable {

    private static final long serialVersionUID = -7112978535165279100L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpRecepcionPagos.class);

    private final MedioAmbiente medioAmbiente;
    private final PagosTCDao pagosTCDao;
    private final PagosHistoricoTCDao pagosHistoricoTCDao;
    private final RpgProgramaTarjetaCredito rpgPrograma;
    private final NotificacionesCorreo notificacionesCorreo;
    SftpConfiguracion configuracion;
    SftpTransferencia sftpTransferencia;

    @Inject
    public SftpRecepcionPagos(MedioAmbiente medioAmbiente,
                              PagosTCDao pagosTCDao, PagosHistoricoTCDao pagosHistoricoTCDao,
                              NotificacionesCorreo notificacionesCorreo, RpgProgramaTarjetaCredito rpgPrograma,
                              SftpConfiguracion configuracion, SftpTransferencia sftpTransferencia) {
        this.medioAmbiente = medioAmbiente;
        this.pagosTCDao = pagosTCDao;
        this.pagosHistoricoTCDao = pagosHistoricoTCDao;
        this.rpgPrograma = rpgPrograma;
        this.notificacionesCorreo = notificacionesCorreo;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para procesar recepcion de archivo desde servidor remoto SFTP.
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    String PREF = "#fecha";

    public boolean procesarArchivo() {
        String detalle = "";
        boolean ok = true;
        LOGGER.debug("Inicia proceso de recepcion de archivo de pagos");
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_PAGOS_ATC, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_PAGOS_ATC_DEFAULT);
        String nombreArchivoRegexForzado = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_PAGOS_ATC_FORZADO, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_PAGOS_ATC_FORZADO_DEFAULT);
        String nombreArchivo[] = Iterables.toArray(Splitter.on("|").trimResults().split(nombreArchivoRegex), String.class);

        // El proceso normal realiza la recepcion de pagos de un dia anterior,
        // debido a la disponibilidad del archivo de respuesta.
        Calendar hoy = Calendar.getInstance();
        hoy.add(Calendar.DAY_OF_YEAR, -1);
        Date fechaProcesar = hoy.getTime();

        if (StringUtils.isEmpty(nombreArchivoRegexForzado) || "NULL".equals(nombreArchivoRegexForzado)) {
            String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_ATC, Variables.FECHA_DE_PROCESO_ATC_DEFAULT);
            if (StringUtils.isNotEmpty(fechaProceso) && !"0".equals(fechaProceso)) {
                fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
                if (fechaProcesar == null) {
                    notificacionesCorreo.notificarError("Error en fecha de proceso recepci\u00F3n pago", "");
                    return false;
                }
            }
            nombreArchivoRegex = nombreArchivo[0].replace(PREF, new SimpleDateFormat(nombreArchivo[1]).format(fechaProcesar));
        } else {
            nombreArchivoRegex = nombreArchivoRegexForzado;
        }

        LOGGER.info("Descargar archivo con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        LOGGER.debug("1) Descarga de archivo de a servidor SFTP con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_PAGOS_ATC, Variables.FTP_RUTA_RECEPCION_PAGOS_ATC_DEFAULT));
        sftpTransferencia.configurar(configuracion);

        ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.PAR);
        LOGGER.debug("Descarga completa correctamente: {}", ok);
        // Verificar respuesta en carpeta de descarte en caso de no existir respuesta
        if (!ok) {
            medioAmbiente.getValorDe(Variables.FTP_RUTA_DESCARTE_PAGOS_ATC, Variables.FTP_RUTA_DESCARTE_PAGOS_ATC_DEFAULT);
            ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.PAR);
            if (ok) {
                // Respuesta a proceso para permitir reintento para archivo de misma fecha
                LOGGER.warn("Archivo de respuesta en servidor SFTP rechazado (descartado), problemas en estructura.");
                sftpTransferencia.transferenciaConErrorReintento("");
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.PAR, "Archivo de respuesta descartado en servidor SFTP.");
                return true;
            }
        }

        if (!ok || sftpTransferencia.getArchivoLocal() == null) {
            // Cerrar el registro con error y permitir reintento
            detalle = "Problema en transferencia o no existen archivos de respuesta en servidor SFTP.";
            sftpTransferencia.transferenciaConErrorReintento("");
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.PAR, detalle);
            LOGGER.warn(detalle);
            return false;
        }

        // Procesamiento de archivo local
        LOGGER.debug("2) Proceso de mapeo de archivos de respuesta");
        File archivo = sftpTransferencia.getArchivoLocal();
        List<PagosCsv> pagos = mapearArchivo(archivo);
        // En caso de problemas en lectura de archivo
        if (pagos == null || pagos.size() == 0) {
            detalle = "Error en mapeo de archivo de respuesta o archivo vac\u00EDo";
            LOGGER.warn(detalle);
            sftpTransferencia.transferenciaConErrorReintento("");
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.PAR, detalle);
            return false;
        }
        // Validar que se realizo el mapeo de forma exitosa de la estructura de pagos
        LOGGER.debug("3) Validar mapeo");
        for (PagosCsv pago : pagos) {
            if (pago == null) {
                detalle = "Error en mapeo de una estructura se requiere revisar el archivo de respuesta.";
                LOGGER.error(detalle);
                sftpTransferencia.transferenciaConErrorReintento("");
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.PAR, detalle);
                return false;
            } else {
                // Validar que se tenga pendiente de respuesta a pagos
                IdPagosTC id = new IdPagosTC();
                id.setCuentaTarjeta(pago.getCuentaTarjeta());
                id.setTransaccion(StringUtils.leftPad(pago.getTransaccion(), 20, " "));
                PagosTC pagoTc = pagosTCDao.getPagoId(id);
                if (pagoTc == null) {
                    LOGGER.error("Error en actualizacion de informacion recibida no existe registro de pago pendiente de respuesta CuentaTarjeta:{}, Transaccion:{}", id.getCuentaTarjeta(), id.getTransaccion());
                    return false;
                }
            }
        }

        LOGGER.debug("4) Actualizar tabla de pagos");
        LOGGER.debug("4.1) Verificar si un registro se encuentra en un estado inconsistente");
        int totalRegistros = 1;
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();
        ok = true;
        int correctos = 0;
        int incorrectos = 0;
        // Verificar consistencia de registro
        for (PagosCsv pago : pagos) {
            IdPagosTC id = new IdPagosTC();
            id.setCuentaTarjeta(pago.getCuentaTarjeta());
            id.setTransaccion(StringUtils.leftPad(pago.getTransaccion(), 20, " "));
            // Validar que se tenga pendiente de respuesta a pagos
            PagosTC pagoTc = pagosTCDao.getPagoId(id);
            if (pagoTc == null) {
                LOGGER.error("Error en actualizacion de informacion recibida no existe registro de pago pendiente de respuesta");
                return false;
            }
            // Registrar pago
            pagoTc.setCodError(pago.getCodError());
            if (CodigoErrorPagoAtc.NO_ERROR.getCodigo().equals(pagoTc.getCodError())) {
                correctos++;
            } else {
                incorrectos++;
            }
            pagoTc.setCodigoPago(pago.getCodigoPago());
            pagoTc.setFechaRecepcion(new Date());
            pagoTc.setIdArchivoRecepcion(sftpTransferencia.getFtpArchivoTransferencia().getId());
            pagoTc.setEstadoPago(("000".equals(pago.getCodError())) ? EstadoPagosTC.PROC : EstadoPagosTC.ERRP);
            ok = ok && pagosTCDao.guardar(pagoTc, "ISB");

            PagosHistoricoTC pagosHistoricoTC = transferObject.convert(pagoTc, PagosHistoricoTC.class);
            pagosHistoricoTC.setIdPago(pagoTc.getIdPago());
            ok = ok && pagosHistoricoTCDao.guardar(pagosHistoricoTC, "ISB");
        }
        if (ok) {
            detalle = "Recepci\u00F3n de Pagos, con " + correctos + " pagos aceptados y " + incorrectos + " pagos con error.";
            sftpTransferencia.transferenciaCompleta(detalle);
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.PAR, detalle);
        } else {
            detalle = "Archivo procesado con problemas en registro en DB.";
            sftpTransferencia.transferenciaConErrorInterno(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.PAR, detalle);
        }
        LOGGER.debug("4.2) Completado proceso de carga de archivo {} de pagos a servidor SFTP de ATC correctamente.", sftpTransferencia.getNombreArchivoRemoto());
        return ok;
    }


    /**
     * Mapeo de estructura de pagos
     **/
    public List<PagosCsv> mapearArchivo(File archivo) {
        LOGGER.info("Ruta de archivo local a procesar:{}", archivo.getAbsolutePath());
        FileInputStream is = null;
        BufferedReader br = null;
        boolean respuesta;
        List<PagosCsv> pagosCsvs = new ArrayList<PagosCsv>();
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();
        try {
            is = new FileInputStream(archivo);
            br = new BufferedReader(new InputStreamReader(is));
            String linea = br.readLine();
            List<String> lineas = new ArrayList<String>();
            while (linea != null) {
                if (StringUtils.isNotEmpty(linea) && linea.length() > 1) {
                    String cabecera = linea.substring(0, 2);
                    if (cabecera.equals("PA")) {
                        PagosCsv pago = LecturaCSV.procesar(PagosCsv.class, linea);
                        pagosCsvs.add(pago);
                    }
                }
                linea = br.readLine();
            }
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al procesar el archivo "
                    + archivo.getName() + ".", e);
            return null;
        } finally {
            IOUtils.closeQuietly(is);
        }
        LOGGER.info("Se realiza la lectura de {} registros.", pagosCsvs.size());
        return pagosCsvs;
    }
}
