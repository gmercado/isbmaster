package com.bisa.bus.servicios.tc.sftp.sched;

import bus.env.api.MedioAmbiente;
import bus.mail.api.MailerFactory;
import com.bisa.bus.servicios.tc.sftp.api.SftpEnvioPagos;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 03/05/2017.
 */
public class EnvioPagos implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(EnvioPagos.class);

    public static String NOMBRE_TAREA = "PagoEnvio-Hilo";
    private final MedioAmbiente medioAmbiente;
    private final MailerFactory mailerFactory;
    private final SftpEnvioPagos sftpEnvioPagos;

    private volatile boolean run;
    private volatile boolean ok;

    @Inject
    public EnvioPagos(MedioAmbiente medioAmbiente, MailerFactory mailerFactory, SftpEnvioPagos sftpEnvioPagos){
        this.medioAmbiente = medioAmbiente;
        this.mailerFactory = mailerFactory;
        this.sftpEnvioPagos = sftpEnvioPagos;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {


        LOGGER.info("Realizando llamada a proceso");
        ok = sftpEnvioPagos.procesarArchivo();
        LOGGER.info("Archivo encontrado {}", ok);

        run = true;
        ok = false;
    }

}
