package com.bisa.bus.servicios.tc.sftp.entities;

import bus.plumbing.utils.FormatosUtils;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 04/12/2017.
 */
@Entity
@Table(name = "TCPFEVEN")
public class FechaCorte implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private IdFechaCorte idFechaCorte;

    @Basic(optional = false)
    @Column(name = "FVFEVE", nullable = false)
    private int fechaVencimiento;
    @Basic(optional = false)
    @Column(name = "FVFECI", nullable = false)
    private int fechaCierre;
    @Basic(optional = false)
    @Column(name = "FVFECIB", nullable = false)
    private int fechaCierreBanco;

    public FechaCorte() {
    }

    public IdFechaCorte getIdFechaCorte() {
        return idFechaCorte;
    }

    public void setIdFechaCorte(IdFechaCorte idFechaCorte) {
        this.idFechaCorte = idFechaCorte;
    }

    public int getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(int fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public int getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(int fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public int getFechaCierreBanco() {
        return fechaCierreBanco;
    }

    public void setFechaCierreBanco(int fechaCierreBanco) {
        this.fechaCierreBanco = fechaCierreBanco;
    }
    @Transient
    private boolean fechaCierreValido = true;
    @Transient
    private boolean fechaVencimientoPagoValido = true;
    @Transient
    private boolean fechaCierreBancoValido = true;

    public boolean validarFechaCierre(String fecha) {
        if (fecha == null) return fechaCierreValido = false;
        Long valor = FormatosUtils.fechaYYYYMMDD(FormatosUtils.deYYYYMMDDaFecha(fecha));
        if (valor == null) return fechaCierreValido = false;
        return fechaCierreValido = (valor.intValue() == fechaCierre);
    }

    public boolean validarFechaVencimientoPago(String fecha) {
        if (fecha == null) return fechaVencimientoPagoValido = false;
        Long valor = FormatosUtils.fechaYYYYMMDD(FormatosUtils.deYYYYMMDDaFecha(fecha));
        if (valor == null) return fechaVencimientoPagoValido = false;
        return fechaVencimientoPagoValido = (valor.intValue() == fechaVencimiento);
    }

    public boolean validarFechaCierreBanco(String fecha) {
        if (fecha == null) return fechaCierreBancoValido = false;
        Long valor = FormatosUtils.fechaYYYYMMDD(FormatosUtils.deYYYYMMDDaFecha(fecha));
        if (valor == null) return fechaCierreBancoValido = false;
        return fechaCierreBancoValido = (valor.intValue() == fechaCierre);
    }

    public boolean isFechaCierreValido() {
        return fechaCierreValido;
    }

    public void setFechaCierreValido(boolean fechaCierreValido) {
        this.fechaCierreValido = fechaCierreValido;
    }

    public boolean isFechaVencimientoPagoValido() {
        return fechaVencimientoPagoValido;
    }

    public void setFechaVencimientoPagoValido(boolean fechaVencimientoPagoValido) {
        this.fechaVencimientoPagoValido = fechaVencimientoPagoValido;
    }

    public boolean isFechaCierreBancoValido() {
        return fechaCierreBancoValido;
    }

    public void setFechaCierreBancoValido(boolean fechaCierreBancoValido) {
        this.fechaCierreBancoValido = fechaCierreBancoValido;
    }
}
