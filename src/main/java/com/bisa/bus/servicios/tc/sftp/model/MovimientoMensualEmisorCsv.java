package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by atenorio on 23/06/2017.
 * ATCUC111 Registros de movimiento emisor mensual contiene movimientos desde perspectiva emisora.
 * Contiene cargos, intereses, comisiones como parte del proceso de cierre mensual.
 */
public class MovimientoMensualEmisorCsv implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
    @FormatCsv(name = "MEBAEM", nullable = false, length = 4)
    private String codigoBanco;
    
    @FormatCsv(name = "MESUEM", nullable = false, length = 4)
    private String sucursalEmisor;
    
    @FormatCsv(name = "MENUTCNC", nullable = false, length = 10)
    private String numeroCuenta;
    
    @FormatCsv(name = "MENUTCEN", nullable = false, length = 19)
    private String numeroTarjeta;
    
    @FormatCsv(name = "MEACTC", nullable = false, length = 12)
    private String codigoAceptanteTc;
    
    @FormatCsv(name = "MECAESCU", nullable = false, length = 4)
    private String categoriaEstadoCuenta;

    @FormatCsv(name = "MECAFI", nullable = false, length = 4)
    private String categoriaFinanc;

    @FormatCsv(name = "MECABA", nullable = false, length = 4)
    private String categoriaBalance;

    @FormatCsv(name = "MENUCOMP", nullable = false, length = 12)
    private String numeroComprobante;
    
    @FormatCsv(name = "MEFETR", nullable = false, length = 8)
    private String fechaTransaccion;

    @FormatCsv(name = "MEIMMOORN", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeMonedaOrigen;

    @FormatCsv(name = "MEMOOR", nullable = false, length = 3)
    private String monedaOrigen;

    @FormatCsv(name = "MEIMMOBAN", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeMonedaBalance;
    
    @FormatCsv(name = "MEMOBA", nullable = false, length = 3)
    private String monedaBalance;

    @FormatCsv(name = "MEIMMOLIN", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeMonedaLiquidacion;

    @FormatCsv(name = "MEMOLI", nullable = false, length = 3)
    private String monedaLiquidacion;

    @FormatCsv(name = "MEALTR", nullable = false)
    private Character alcanceTransaccion;
    
    @FormatCsv(name = "MENULO", nullable = false, length = 4)
    private String numeroLote;
    // Indica modo de entrada de transaccion
    
    @FormatCsv(name = "MEMOTR", nullable = false, length = 2)
    private String modoTransaccion;
    // Nombre de establecimiento o dispositivo
    
    @FormatCsv(name = "MENOEST", nullable = false, length = 30)
    private String nombreEstablecimiento;
    
    @FormatCsv(name = "MEMTI", nullable = false, length = 4)
    private String codigoMTI;
    
    @FormatCsv(name = "MECOPRO", nullable = false, length = 6)
    private String codigoProceso;
    
    @FormatCsv(name = "MECOAUT", nullable = false, length = 6)
    private String codigoAutorizacion;
    
    @FormatCsv(name = "MECOOP", nullable = false)
    private Character codigoOperacion;
    
    @FormatCsv(name = "MEIDTERM", nullable = false, length = 8)
    private String idTerminal;
    
    @FormatCsv(name = "MECACUO", nullable = false, length = 2)
    private String planCuotas;
    
    @FormatCsv(name = "MENUCUO", nullable = false, length = 2)
    private String numeroCuota;
    
    @FormatCsv(name = "MEMOADIN", nullable = false, length = 10)
    private BigDecimal montoAdicional;
    
    @FormatCsv(name = "MEIMEFECN", nullable = false, length = 10)
    private BigDecimal importeEfectivo;
    
    @FormatCsv(name = "MECADISP", nullable = false, length = 3)
    private String capacidadDispositivo;
    
    @FormatCsv(name = "MECOINTC", nullable = false, length = 3)
    private String condicionIngreso;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getSucursalEmisor() {
        return sucursalEmisor;
    }

    public void setSucursalEmisor(String sucursalEmisor) {
        this.sucursalEmisor = sucursalEmisor;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getCodigoAceptanteTc() {
        return codigoAceptanteTc;
    }

    public void setCodigoAceptanteTc(String codigoAceptanteTc) {
        this.codigoAceptanteTc = codigoAceptanteTc;
    }

    public String getCategoriaEstadoCuenta() {
        return categoriaEstadoCuenta;
    }

    public void setCategoriaEstadoCuenta(String categoriaEstadoCuenta) {
        this.categoriaEstadoCuenta = categoriaEstadoCuenta;
    }

    public String getCategoriaBalance() {
        return categoriaBalance;
    }

    public void setCategoriaBalance(String categoriaBalance) {
        this.categoriaBalance = categoriaBalance;
    }

    public String getCategoriaFinanc() {
        return categoriaFinanc;
    }

    public void setCategoriaFinanc(String categoriaFinanc) {
        this.categoriaFinanc = categoriaFinanc;
    }

    public String getNumeroComprobante() {
        return numeroComprobante;
    }

    public void setNumeroComprobante(String numeroComprobante) {
        this.numeroComprobante = numeroComprobante;
    }

    public String getFechaTransaccion() {
        return fechaTransaccion;
    }

    public void setFechaTransaccion(String fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    

    public BigDecimal getImporteMonedaOrigen() {
        return importeMonedaOrigen;
    }

    public void setImporteMonedaOrigen(BigDecimal importeMonedaOrigen) {
        this.importeMonedaOrigen = importeMonedaOrigen;
    }

    public String getMonedaOrigen() {
        return monedaOrigen;
    }

    public void setMonedaOrigen(String monedaOrigen) {
        this.monedaOrigen = monedaOrigen;
    }

   

    public BigDecimal getImporteMonedaBalance() {
        return importeMonedaBalance;
    }

    public void setImporteMonedaBalance(BigDecimal importeMonedaBalance) {
        this.importeMonedaBalance = importeMonedaBalance;
    }

    public String getMonedaBalance() {
        return monedaBalance;
    }

    public void setMonedaBalance(String monedaBalance) {
        this.monedaBalance = monedaBalance;
    }

    public BigDecimal getImporteMonedaLiquidacion() {
        return importeMonedaLiquidacion;
    }

    public void setImporteMonedaLiquidacion(BigDecimal importeMonedaLiquidacion) {
        this.importeMonedaLiquidacion = importeMonedaLiquidacion;
    }

    public String getMonedaLiquidacion() {
        return monedaLiquidacion;
    }

    public void setMonedaLiquidacion(String monedaLiquidacion) {
        this.monedaLiquidacion = monedaLiquidacion;
    }

    public Character getAlcanceTransaccion() {
        return alcanceTransaccion;
    }

    public void setAlcanceTransaccion(Character alcanceTransaccion) {
        this.alcanceTransaccion = alcanceTransaccion;
    }

    public String getNumeroLote() {
        return numeroLote;
    }

    public void setNumeroLote(String numeroLote) {
        this.numeroLote = numeroLote;
    }

    public String getModoTransaccion() {
        return modoTransaccion;
    }

    public void setModoTransaccion(String modoTransaccion) {
        this.modoTransaccion = modoTransaccion;
    }

    public String getNombreEstablecimiento() {
        return nombreEstablecimiento;
    }

    public void setNombreEstablecimiento(String nombreEstablecimiento) {
        this.nombreEstablecimiento = nombreEstablecimiento;
    }

    public String getCodigoMTI() {
        return codigoMTI;
    }

    public void setCodigoMTI(String codigoMTI) {
        this.codigoMTI = codigoMTI;
    }

    public String getCodigoProceso() {
        return codigoProceso;
    }

    public void setCodigoProceso(String codigoProceso) {
        this.codigoProceso = codigoProceso;
    }

    public String getCodigoAutorizacion() {
        return codigoAutorizacion;
    }

    public void setCodigoAutorizacion(String codigoAutorizacion) {
        this.codigoAutorizacion = codigoAutorizacion;
    }

    public Character getCodigoOperacion() {
        return codigoOperacion;
    }

    public void setCodigoOperacion(Character codigoOperacion) {
        this.codigoOperacion = codigoOperacion;
    }

    public String getIdTerminal() {
        return idTerminal;
    }

    public void setIdTerminal(String idTerminal) {
        this.idTerminal = idTerminal;
    }

    public String getPlanCuotas() {
        return planCuotas;
    }

    public void setPlanCuotas(String planCuotas) {
        this.planCuotas = planCuotas;
    }

    public String getNumeroCuota() {
        return numeroCuota;
    }

    public void setNumeroCuota(String numeroCuota) {
        this.numeroCuota = numeroCuota;
    }

    public BigDecimal getMontoAdicional() {
        return montoAdicional;
    }

    public void setMontoAdicional(BigDecimal montoAdicional) {
        this.montoAdicional = montoAdicional;
    }

    public BigDecimal getImporteEfectivo() {
        return importeEfectivo;
    }

    public void setImporteEfectivo(BigDecimal importeEfectivo) {
        this.importeEfectivo = importeEfectivo;
    }

    public String getCapacidadDispositivo() {
        return capacidadDispositivo;
    }

    public void setCapacidadDispositivo(String capacidadDispositivo) {
        this.capacidadDispositivo = capacidadDispositivo;
    }

    public String getCondicionIngreso() {
        return condicionIngreso;
    }

    public void setCondicionIngreso(String condicionIngreso) {
        this.condicionIngreso = condicionIngreso;
    }
}
