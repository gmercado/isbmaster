package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

/**
 * Created by atenorio on 08/09/2017.
 */
public class SolicitudDireccionEmpresa {

    @FormatCsv(name = "SOADTIREL", nullable = false, length = 2)
    private String soadtirel;

    @FormatCsv(name = "SOADTIDICL", nullable = false, length = 1)
    private short soadtidicl;

    @FormatCsv(name = "SOADTICAL", nullable = false, length = 4)
    private short soadtical;

    @FormatCsv(name = "SOADCALLEL", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadcallel;

    @FormatCsv(name = "SOADNUPUL", nullable = false, length = 10, align = FormatCsv.alignType.LEFT)
    private String soadnupul;

    @FormatCsv(name = "SOADINADL", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadinadl;

    @FormatCsv(name = "SOADCOPOL", nullable = false, length = 6)
    private String soadcopol;

    @FormatCsv(name = "SOADZONAL", nullable = false, length = 60, align = FormatCsv.alignType.LEFT)
    private String soadzonal;

    @FormatCsv(name = "SOADPAISL", nullable = false, length = 3, fillSpace = "0")
    private short soadpaisl;

    @FormatCsv(name = "SOADDPTOL", nullable = false, length = 5, align = FormatCsv.alignType.LEFT)
    private String soaddptol;

    @FormatCsv(name = "SOADLOCL", nullable = false, length = 9, fillSpace = "0")
    private int soadlocl;

    @FormatCsv(name = "SOADCOTEPL", nullable = false, length = 3, fillSpace = "0")
    private short soadcotepl;

    @FormatCsv(name = "SOADCOTEAL", nullable = false, length = 5, fillSpace = "0")
    private int soadcoteal;

    @FormatCsv(name = "SOADTELL", nullable = false, length = 12, fillSpace = "0")
    private long soadtell;

    @FormatCsv(name = "SOADTEINL", nullable = false, length = 6, fillSpace = "0")
    private int soadteinl;

    @FormatCsv(name = "SOADCOCEPL", nullable = false, length = 3, fillSpace = "0")
    private short soadcocepl;

    @FormatCsv(name = "SOADCELL", nullable = false, length = 12, fillSpace = "0")
    private long soadcell;

    @FormatCsv(name = "SOADFILL", nullable = false, length = 56)
    private long soadfilll;

    @FormatCsv(name = "SOADCOERL", nullable = false, length = 3)
    private String soadcoerl;

    public String getSoadtirel() {
        return soadtirel;
    }

    public void setSoadtirel(String soadtirel) {
        this.soadtirel = soadtirel;
    }

    public short getSoadtidicl() {
        return soadtidicl;
    }

    public void setSoadtidicl(short soadtidicl) {
        this.soadtidicl = soadtidicl;
    }

    public short getSoadtical() {
        return soadtical;
    }

    public void setSoadtical(short soadtical) {
        this.soadtical = soadtical;
    }

    public String getSoadcallel() {
        return soadcallel;
    }

    public void setSoadcallel(String soadcallel) {
        this.soadcallel = soadcallel;
    }

    public String getSoadnupul() {
        return soadnupul;
    }

    public void setSoadnupul(String soadnupul) {
        this.soadnupul = soadnupul;
    }

    public String getSoadinadl() {
        return soadinadl;
    }

    public void setSoadinadl(String soadinadl) {
        this.soadinadl = soadinadl;
    }

    public String getSoadcopol() {
        return soadcopol;
    }

    public void setSoadcopol(String soadcopol) {
        this.soadcopol = soadcopol;
    }

    public String getSoadzonal() {
        return soadzonal;
    }

    public void setSoadzonal(String soadzonal) {
        this.soadzonal = soadzonal;
    }

    public short getSoadpaisl() {
        return soadpaisl;
    }

    public void setSoadpaisl(short soadpaisl) {
        this.soadpaisl = soadpaisl;
    }

    public String getSoaddptol() {
        return soaddptol;
    }

    public void setSoaddptol(String soaddptol) {
        this.soaddptol = soaddptol;
    }

    public int getSoadlocl() {
        return soadlocl;
    }

    public void setSoadlocl(int soadlocl) {
        this.soadlocl = soadlocl;
    }

    public short getSoadcotepl() {
        return soadcotepl;
    }

    public void setSoadcotepl(short soadcotepl) {
        this.soadcotepl = soadcotepl;
    }

    public int getSoadcoteal() {
        return soadcoteal;
    }

    public void setSoadcoteal(int soadcoteal) {
        this.soadcoteal = soadcoteal;
    }

    public long getSoadtell() {
        return soadtell;
    }

    public void setSoadtell(long soadtell) {
        this.soadtell = soadtell;
    }

    public int getSoadteinl() {
        return soadteinl;
    }

    public void setSoadteinl(int soadteinl) {
        this.soadteinl = soadteinl;
    }

    public short getSoadcocepl() {
        return soadcocepl;
    }

    public void setSoadcocepl(short soadcocepl) {
        this.soadcocepl = soadcocepl;
    }

    public long getSoadcell() {
        return soadcell;
    }

    public void setSoadcell(long soadcell) {
        this.soadcell = soadcell;
    }

    public long getSoadfilll() {
        return soadfilll;
    }

    public void setSoadfilll(long soadfilll) {
        this.soadfilll = soadfilll;
    }

    public String getSoadcoerl() {
        return soadcoerl;
    }

    public void setSoadcoerl(String soadcoerl) {
        this.soadcoerl = soadcoerl;
    }
}
