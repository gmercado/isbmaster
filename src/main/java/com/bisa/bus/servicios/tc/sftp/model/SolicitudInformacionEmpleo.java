package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import javax.persistence.Basic;


/**
 * Created by atenorio on 18/05/2017.
 * Informacion empleo EI
 */
public class SolicitudInformacionEmpleo {

    @Basic(optional = false)
    @FormatCsv(name = "SOEITIRE", nullable = false, length = 2)
    private String soeitire;
    @Basic(optional = false)
    @FormatCsv(name = "SOEICONTR", nullable = false,length = 1)
    private short soeicontr;
    @Basic(optional = false)
    @FormatCsv(name = "SOEIFEVECO", nullable = false, length = 8)
    private String soeifeveco;
    @Basic(optional = false)
    @FormatCsv(name = "SOEIEMPL", nullable = false, length = 60)
    private String soeiempl;
    @Basic(optional = false)
    @FormatCsv(name = "SOEITIDO", nullable = false, length = 4, fillSpace = "0")
    private short soeitido;
    @Basic(optional = false)
    @FormatCsv(name = "SOEIDOEM", nullable = false, length = 25)
    private String soeidoem;
    @Basic(optional = false)
    @FormatCsv(name = "SOEIFEIN", nullable = false, length = 8)
    private String soeifein;
    @Basic(optional = false)
    @FormatCsv(name = "SOEIINPR", nullable = false, length = 1, fillSpace = "0")
    private short soeiinpr;
    @Basic(optional = false)
    @FormatCsv(name = "SOEIINEXT", nullable = false, length = 1, fillSpace = "0")
    private short soeiinext;
    @Basic(optional = false)
    @FormatCsv(name = "SOEITOIN", nullable = false, length = 12, fillSpace = "0")
    private long soeitoin;
    @Basic(optional = false)
    @FormatCsv(name = "SOEICAEM", nullable = false, length = 60)
    private String soeicaem;
    @Basic(optional = false)
    @FormatCsv(name = "SOEIOCUP", nullable = false, length = 9, fillSpace = "0")
    private int soeiocup;
    @Basic(optional = false)
    @FormatCsv(name = "SOEISEEM", nullable = false, length = 9, fillSpace = "0")
    private int soeiseem;
    @FormatCsv(name = "FILL", nullable = false, length = 196)
    private String soeifill="";
    @Basic(optional = false)
    @FormatCsv(name = "SOEICOER", nullable = false, length = 3)
    private String soeicoer;

    public String getSoeitire() {
        return soeitire;
    }

    public void setSoeitire(String soeitire) {
        this.soeitire = soeitire;
    }

    public short getSoeicontr() {
        return soeicontr;
    }

    public void setSoeicontr(short soeicontr) {
        this.soeicontr = soeicontr;
    }

    public String getSoeifeveco() {
        return soeifeveco;
    }

    public void setSoeifeveco(String soeifeveco) {
        this.soeifeveco = soeifeveco;
    }

    public String getSoeiempl() {
        return soeiempl;
    }

    public void setSoeiempl(String soeiempl) {
        this.soeiempl = soeiempl;
    }

    public short getSoeitido() {
        return soeitido;
    }

    public void setSoeitido(short soeitido) {
        this.soeitido = soeitido;
    }

    public String getSoeidoem() {
        return soeidoem;
    }

    public void setSoeidoem(String soeidoem) {
        this.soeidoem = soeidoem;
    }

    public String getSoeifein() {
        return soeifein;
    }

    public void setSoeifein(String soeifein) {
        this.soeifein = soeifein;
    }

    public short getSoeiinpr() {
        return soeiinpr;
    }

    public void setSoeiinpr(short soeiinpr) {
        this.soeiinpr = soeiinpr;
    }

    public short getSoeiinext() {
        return soeiinext;
    }

    public void setSoeiinext(short soeiinext) {
        this.soeiinext = soeiinext;
    }

    public long getSoeitoin() {
        return soeitoin;
    }

    public void setSoeitoin(long soeitoin) {
        this.soeitoin = soeitoin;
    }

    public String getSoeicaem() {
        return soeicaem;
    }

    public void setSoeicaem(String soeicaem) {
        this.soeicaem = soeicaem;
    }

    public int getSoeiocup() {
        return soeiocup;
    }

    public void setSoeiocup(int soeiocup) {
        this.soeiocup = soeiocup;
    }

    public int getSoeiseem() {
        return soeiseem;
    }

    public void setSoeiseem(int soeiseem) {
        this.soeiseem = soeiseem;
    }

    public String getSoeifill() {
        return soeifill;
    }

    public void setSoeifill(String soeifill) {
        this.soeifill = soeifill;
    }

    public String getSoeicoer() {
        return soeicoer;
    }

    public void setSoeicoer(String soeicoer) {
        this.soeicoer = soeicoer;
    }
}
