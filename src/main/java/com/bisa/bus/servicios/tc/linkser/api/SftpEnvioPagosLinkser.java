package com.bisa.bus.servicios.tc.linkser.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.csv.GeneradorCSV;
import bus.plumbing.file.ArchivoBase;
import bus.plumbing.file.ArchivoProfile;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.tc.linkser.dao.FtpArchivoPreparadoDao;
import com.bisa.bus.servicios.tc.linkser.dao.PagosTcLinkserTxtDao;
import com.bisa.bus.servicios.tc.linkser.entities.FtpArchivoPreparado;
import com.bisa.bus.servicios.tc.linkser.entities.IdFtpArchivoPreparado;
import com.bisa.bus.servicios.tc.linkser.entities.PagosTcLinkserTxt;
import com.bisa.bus.servicios.tc.sftp.api.SftpTransferencia;
import com.bisa.bus.servicios.tc.sftp.entities.FtpArchivoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.EstadoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgProgramaTarjetaCredito;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by atenorio on 10/11/2017.
 */
public class SftpEnvioPagosLinkser {
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpEnvioPagosLinkser.class);

    private final MedioAmbiente medioAmbiente;
    private final ArchivoBase archivoBase;
    private final PagosTcLinkserTxtDao iPagosTcLinkserTxtDao;
    private final FtpArchivoPreparadoDao iFtpArchivoPreparadoDao;

    private final RpgProgramaTarjetaCredito iRpgPrograma;
    private final NotificacionesCorreo notificacionesCorreo;
    ISftpConfiguracionLinkser configuracion;
    SftpTransferencia sftpTransferencia;

    @Inject
    public SftpEnvioPagosLinkser(ArchivoBase archivoBase, MedioAmbiente medioAmbiente,
                                 PagosTcLinkserTxtDao iPagosTcLinkserTxtDao,
                                 RpgProgramaTarjetaCredito iRpgPrograma, NotificacionesCorreo notificacionesCorreo,
                                 ISftpConfiguracionLinkser configuracion, SftpTransferencia sftpTransferencia,
                                 FtpArchivoPreparadoDao iFtpArchivoPreparadoDao) {
        this.medioAmbiente = medioAmbiente;
        this.archivoBase = archivoBase;
        this.iPagosTcLinkserTxtDao = iPagosTcLinkserTxtDao;
        this.iRpgPrograma = iRpgPrograma;
        this.notificacionesCorreo = notificacionesCorreo;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
        this.iFtpArchivoPreparadoDao = iFtpArchivoPreparadoDao;
    }

    /**
     * Metodo para generar y enviar archivo de pagos de tarjetas linkser de servidor remoto SFTP.
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    String PREF = "#fecha";

    public boolean procesarArchivo() {
        boolean ok = false;
        LOGGER.debug("Inicia proceso de envio de archivo de pagos");
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_NOMBRE_ARCHIVO_PAGOS_LINKSER, Variables.FTP_NOMBRE_ARCHIVO_PAGOS_LINKSER_DEFAULT);

        String nombreArchivo[] = Iterables.toArray(Splitter.on("|").trimResults().split(nombreArchivoRegex), String.class);

        String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_LINKSER, Variables.FECHA_DE_PROCESO_LINKSER_DEFAULT);
        Calendar hoy = Calendar.getInstance();
        Date fechaProcesar = hoy.getTime();
        FtpArchivoPreparado preparado = null;

        // Procesar a fecha determinada por parametro
        if (StringUtils.isNotEmpty(fechaProceso) && !"0".equals(fechaProceso)) {
            LOGGER.debug("1) Verificar archivo preparado para.");
            fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
            if (fechaProcesar == null) {
                notificacionesCorreo.notificarError("Error en fecha de proceso recepci\u00F3n alta", "");
                return false;
            }
            IdFtpArchivoPreparado id = new IdFtpArchivoPreparado();
            id.setTipo(TipoArchivo.PLI);
            id.setProceso(Integer.getInteger(fechaProceso));
            try {
                preparado = iFtpArchivoPreparadoDao.find(id);
            } catch (Exception e) {
                LOGGER.error("Error al verificar existencia de pagos pendientes de envio. {}", e);
                return false;
            }
            // Si no existen pagos notificar a cliente
            if (preparado == null || preparado.getId() == null || !EstadoTransferencia.PRTE.equals(preparado.getEstado())) {
                LOGGER.error("No existe archivo de pago pendiente de envio en estado PRTE en tabla TCPCTRPRO de fecha {}.", fechaProceso);
                return false;
            }
        } else {
            LOGGER.debug("1) Revision de archivos disponibles para ser enviado.");
            try {
                preparado = iFtpArchivoPreparadoDao.getFtpTransferenciaPendiente(TipoArchivo.PLI, EstadoTransferencia.GENE);
            } catch (Exception e) {
                LOGGER.error("Error al verificar existencia de pagos pendientes de envio. {}", e);
                return false;
            }
            // Si no existen pagos notificar a cliente
            if (preparado == null || preparado.getId() == null) {
                LOGGER.error("No existe archivo de pago pendiente de envio en estado GENE en tabla TCPCTRPRO.");
                return false;
            }
        }


        LOGGER.info("Datos de archivo: {} ", preparado.toString());
        fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(String.valueOf(preparado.getId().getProceso()));

        nombreArchivoRegex = nombreArchivo[0].replace(PREF, new SimpleDateFormat(nombreArchivo[1]).format(fechaProcesar));

        LOGGER.debug("Verificar archivo si el archivo {} ya fue procesado.", nombreArchivoRegex);
        // Verificar que archivo no sea procesado con el mismo nombre a menos que este en estado de reproceso o error de reintento
        if (!sftpTransferencia.verificarPermitirTransferencia(nombreArchivoRegex)) {
            LOGGER.error("No se permite procesamiento y transferencia de archivo {} se finaliza la tarea.", nombreArchivoRegex);
            return false;
        }

        // Creacion de registro tipo pago
        sftpTransferencia.setFechaProceso(Integer.valueOf(FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar)));
        FtpArchivoTransferencia ftpArchivoTransferencia = sftpTransferencia.crearRegistro(nombreArchivoRegex, TipoArchivo.PLI);

        LOGGER.debug("3) Carga de archivo de pagos a servidor SFTP");
        LOGGER.debug("3.1) Recuperacion pagos pendientes de envio");
        List<PagosTcLinkserTxt> pagos = iPagosTcLinkserTxtDao.getPagosParaEnvio();
        if (pagos.size() > 1) {
            prepararConfiguracionSftp();
            // Creacion de archivo temporal para preparar envio
            File archivo = sftpTransferencia.createFile(nombreArchivoRegex);

            ArchivoProfile archivoProfile = poblarArchivo(archivo, pagos);
            // Verificar si existio problema en la generacion de archivo
            if (archivoProfile == null) {
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.PLI, "Error en la generaci\u00F3n de archivo");
                return false;
            }
            LOGGER.debug("3.2) Creación local archivo de pagos con {} altas.", pagos.size());
            // Cargar archivo a servidor SFTP si se completa correctamente se registra intercambio con estado PRTE
            boolean cargado = sftpTransferencia.cargarArchivo(archivoProfile.getArchivo(), nombreArchivoRegex, ftpArchivoTransferencia);

            // Modificar cambio de estado
            if (cargado) {
                LOGGER.debug("3.3) Cargando de archivo {} de pagos a servidor sftp finalizado correctamente.", nombreArchivoRegex);
                TransferObject transferObject = new TransferObject();
                transferObject.initializate();
                int totalRegistros = pagos.size();
                // Reducir cabecera con fecha del archivo
                totalRegistros--;

                // Actualizar registro de control de generación de archivos de pagos TCPCTRPRO
                preparado.setEstado(EstadoTransferencia.PRTE);
                iFtpArchivoPreparadoDao.guardar(preparado, "ISB");

                // Envio de correspondencia
                StringBuffer sb = new StringBuffer();
                sb.append("Se ha generado el archivo de pagos para administradora LINKSER ");
                sb.append(" con ");
                sb.append(totalRegistros);
                sb.append(" pagos.");
                notificacionesCorreo.notificarCorrectaTransferenciaOperador(nombreArchivoRegex, TipoArchivo.PLI, sb.toString());
                ok = true;
                LOGGER.info("Completado proceso de carga de archivo {} de pagos a servidor SFTP de LINKSER correctamente.", nombreArchivoRegex);
            } else {
                LOGGER.debug("3.3) Cargando de archivo {} de pagos a servidor sftp finalizado incorrectamente.", nombreArchivoRegex);
                notificacionesCorreo.notificarErrorTransferenciaOperador(nombreArchivoRegex, TipoArchivo.PLI, "Error en carga de archivo");
                return false;
            }
            LOGGER.debug("3.4) Completado proceso de carga de archivo {} de pagos a servidor SFTP de LINKSER correctamente.", nombreArchivoRegex);
        } else {
            sftpTransferencia.transferenciaCompleta("No se genera archivo en el servidor SFTP, sin pagos a enviar");
            StringBuffer sb = new StringBuffer();
            sb.append("No se ha generado el archivo de pagos para administradora LINKSER ");
            sb.append("por no tener pagos pendientes de envio.");
            notificacionesCorreo.notificarCorrectaTransferenciaOperador(nombreArchivoRegex, TipoArchivo.PLI, sb.toString());

            // Actualizar registro de control de generación de archivos de pagos TCPCTRPRO
            preparado.setEstado(EstadoTransferencia.PRTE);
            iFtpArchivoPreparadoDao.guardar(preparado, "ISB");

            LOGGER.debug("3.4) Sin solicitudes para enviar");
        }
        return ok;
    }


    public boolean prepararConfiguracionSftp() {
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_ENVIO_PAGOS_LINKSER, Variables.FTP_RUTA_ENVIO_PAGOS_LINKSER_DEFAULT));
        configuracion.setRemoteTemp(medioAmbiente.getValorDe(Variables.FTP_RUTA_ENVIO_TEMPORAL_LINKSER, Variables.FTP_RUTA_ENVIO_TEMPORAL_LINKSER_DEFAULT));
        return sftpTransferencia.configurar(configuracion);
    }

    private ArchivoProfile poblarArchivo(File archivo, List<PagosTcLinkserTxt> pagos) {
        if (archivo == null) return null;
        FileOutputStream archivoWrite;
        try {
            archivoWrite = new FileOutputStream(archivo.getAbsoluteFile());
            BufferedWriter bufferWriter = new BufferedWriter(new OutputStreamWriter(archivoWrite, StandardCharsets.UTF_8)); // StandardCharsets.ISO_8859_1));
            StringBuilder sb = new StringBuilder();
            bufferWriter.write(sb.toString());

            for (PagosTcLinkserTxt pago : pagos) {
                TransferObject transferObject = new TransferObject();
                transferObject.initializate();
                // Grabar datos generales de cuenta
                GeneradorCSV.adicionarLinea(bufferWriter, pago.getTexto());
            }
            bufferWriter.close();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("DETALLE DEL ARCHIVO");
                FileReader filer = new FileReader(archivo);
                int valor = filer.read();
                while (valor != -1) {
                    LOGGER.debug("", valor);
                    valor = filer.read();
                }
            }

        } catch (IOException e) {
            LOGGER.error("Ocurrio un error inesperado al preparar ", e);
            return null;
        }
        return new ArchivoProfile(archivo, 0L, 0L);
    }
}
