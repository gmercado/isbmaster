package com.bisa.bus.servicios.tc.sftp.model;

import bus.env.api.Variables;

/**
 * Created by atenorio on 12/05/2017.
 */
public interface VariablesFTP extends Variables {

    String FTP_TC_PAGOS_MAIL_ENVIO = "ftp.tarjetacredito.email.destino";
    String FTP_TC_PAGOS_MAIL_ENVIO_DEFAULT = "atenorio@grupopbisa.com";

    String FTP_TC_PAGOS_SUBJECT_MAIL = "ftp.tarjetacredito.email.subject.mail";
    String FTP_TC_PAGOS_SUBJECT_MAIL_DEFAULT = "Envio pagos tarjeta de credito - ATC";




}
