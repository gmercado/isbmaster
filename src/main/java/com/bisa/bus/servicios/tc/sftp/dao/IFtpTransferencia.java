package com.bisa.bus.servicios.tc.sftp.dao;

import com.bisa.bus.servicios.tc.sftp.entities.FtpArchivoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.EstadoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;

import java.util.List;

/**
 * Created by atenorio on 05/05/2017.
 */
public interface IFtpTransferencia {

    FtpArchivoTransferencia getFtpTransferencia(String filename);
    FtpArchivoTransferencia getFtpTransferencia(String filename, Integer fechaProceso);
    FtpArchivoTransferencia getFtpTransferenciaPendiente(TipoArchivo tipo, EstadoTransferencia estadoTransferencia);
    FtpArchivoTransferencia getFtpTransferenciaPendiente(TipoArchivo tipo);

    List<FtpArchivoTransferencia> getFtpTransferenciaPorEstado(EstadoTransferencia estado);

    boolean guardar(FtpArchivoTransferencia transferencia, String user);



}
