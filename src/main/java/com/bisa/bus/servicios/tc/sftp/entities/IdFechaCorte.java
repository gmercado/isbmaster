package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by atenorio on 04/12/2017.
 */
@Embeddable
public class IdFechaCorte implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "FVCADM", nullable = false, length = 1)
    private String administradora;
    @Basic(optional = false)
    @Column(name = "FVPERI", nullable = false)
    private int periodo;

    public IdFechaCorte(){
    }

    public IdFechaCorte(String adm, int periodo){
        this.administradora = adm;
        this.periodo = periodo;
    }

    public String getAdministradora() {
        return administradora;
    }

    public void setAdministradora(String administradora) {
        this.administradora = administradora;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdFechaCorte that = (IdFechaCorte) o;

        if (getPeriodo() != that.getPeriodo()) return false;
        return getAdministradora() != null ? getAdministradora().equals(that.getAdministradora()) : that.getAdministradora() == null;
    }

    @Override
    public int hashCode() {
        int result = getAdministradora() != null ? getAdministradora().hashCode() : 0;
        result = 31 * result + getPeriodo();
        return result;
    }

    @Override
    public String toString() {
        return "IdFechaCorte{" +
                "administradora='" + administradora + '\'' +
                ", periodo=" + periodo +
                '}';
    }
}
