package com.bisa.bus.servicios.tc.linkser.api;

import bus.database.model.AmbienteActual;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.csv.GeneradorCSV;
import bus.plumbing.file.ArchivoBase;
import bus.plumbing.file.ArchivoProfile;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.tc.linkser.dao.EnvioAnticiposLinkserDao;
import com.bisa.bus.servicios.tc.linkser.entities.AnticiposLinkser;
import com.bisa.bus.servicios.tc.sftp.api.SftpTransferencia;
import com.bisa.bus.servicios.tc.sftp.entities.FtpArchivoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by atenorio on 21/11/2017.
 */
public class SftpEnvioAnticiposLinkser {
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpEnvioAnticiposLinkser.class);

    private final MedioAmbiente medioAmbiente;
    private final ArchivoBase archivoBase;
    private final EnvioAnticiposLinkserDao iEnvioAnticiposLinkserDao;
    private final NotificacionesCorreo notificacionesCorreo;
    private final AmbienteActual ambienteActual;
    ISftpConfiguracionLinkser configuracion;
    SftpTransferencia sftpTransferencia;

    @Inject
    public SftpEnvioAnticiposLinkser(ArchivoBase archivoBase, MedioAmbiente medioAmbiente,
                                     EnvioAnticiposLinkserDao iEnvioAnticiposLinkserDao, NotificacionesCorreo notificacionesCorreo,
                                     ISftpConfiguracionLinkser configuracion, SftpTransferencia sftpTransferencia,
                                     AmbienteActual ambienteActual) {
        this.medioAmbiente = medioAmbiente;
        this.archivoBase = archivoBase;
        this.iEnvioAnticiposLinkserDao = iEnvioAnticiposLinkserDao;
        this.notificacionesCorreo = notificacionesCorreo;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
        this.ambienteActual = ambienteActual;
    }

    /**
     * Metodo para generar y enviar archivo de anticipos generados en e-BISA de tarjetas linkser de servidor remoto SFTP.
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    String PREF = "#fecha";

    public boolean procesarArchivo() {
        boolean ok = false;
        LOGGER.debug("Inicia proceso de envio de archivo de anticipos");
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_NOMBRE_ARCHIVO_ANTICIPO_LINKSER, Variables.FTP_NOMBRE_ARCHIVO_ANTICIPO_LINKSER_DEFAULT);

        String nombreArchivo[] = Iterables.toArray(Splitter.on("|").trimResults().split(nombreArchivoRegex), String.class);

        String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_LINKSER, Variables.FECHA_DE_PROCESO_LINKSER_DEFAULT);
        Calendar hoy = Calendar.getInstance();
        Date fechaProcesar = hoy.getTime();

        // Verificar que estamos en ambiente produccion
        if (ambienteActual.isAmbienteTemporal()) {
            notificacionesCorreo.notificarErrorTransferenciaOperador("Anticipo", TipoArchivo.ALI, "No se puede generar el archivo en ambiente temporal, por favor reintentar proceso en ambiente de producción.");
            return false;
        }

        // Procesar a fecha determinada por parametro
        if (StringUtils.isNotEmpty(fechaProceso) && !"0".equals(fechaProceso)) {
            fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
            if (fechaProcesar == null) {
                notificacionesCorreo.notificarErrorTransferenciaOperador("Anticipo", TipoArchivo.ALI,"Error en fecha de proceso de envio de archivo.");
                return false;
            }
        }
        nombreArchivoRegex = nombreArchivo[0].replace(PREF, new SimpleDateFormat(nombreArchivo[1]).format(fechaProcesar));

        LOGGER.debug("1) Verificar archivo si el archivo {} ya fue procesado.", nombreArchivoRegex);
        // Verificar que archivo no sea procesado con el mismo nombre a menos que este en estado de reproceso o error de reintento
        if (!sftpTransferencia.verificarPermitirTransferencia(nombreArchivoRegex)) {
            LOGGER.warn("No se permite procesamiento y transferencia de archivo {} se finaliza la tarea.", nombreArchivoRegex);
            return false;
        }

        List<AnticiposLinkser> anticipos = iEnvioAnticiposLinkserDao.getAnticiposParaEnvio();
        if (anticipos.size() > 1) {
            // Verificar fecha de proceso de archivo
            AnticiposLinkser anticipo = anticipos.get(0);
            String fecha = StringUtils.trimToEmpty(anticipo.getTexto());
            String fechaProcesarTxt = FormatosUtils.fechaFormateadaConYYMMDD(fechaProcesar);
            // Verificar valor de fecha de proceso sea igual a la del archivo
            if (fecha.length() != 7) {
                LOGGER.warn("No existe archivo de anticipos pendiente de envio, fecha de proceso {} y fecha de archivo {}.", fechaProcesarTxt, fecha);
                return false;
            }
            // Recuperar fecha de archivo en caso que no igualen las fechas
            if(!(("1" + fechaProcesarTxt).equals(fecha))){
                fecha = StringUtils.substring(fecha,1);
                SimpleDateFormat FORMATO_DATE_yyMMdd = new SimpleDateFormat("yyMMdd");
                try {
                    fechaProcesar = FORMATO_DATE_yyMMdd.parse(fecha);
                } catch (ParseException e) {
                    LOGGER.error("No se permite procesamiento y transferencia de archivo {} se finaliza la tarea.", nombreArchivoRegex, e);
                    return false;
                }
                nombreArchivoRegex = nombreArchivo[0].replace(PREF, new SimpleDateFormat(nombreArchivo[1]).format(fechaProcesar));

                LOGGER.debug("1) Verificar archivo si el archivo {} ya fue procesado.", nombreArchivoRegex);
                // Verificar que archivo no sea procesado con el mismo nombre a menos que este en estado de reproceso o error de reintento
                if (!sftpTransferencia.verificarPermitirTransferencia(nombreArchivoRegex)) {
                    LOGGER.warn("No se permite procesamiento y transferencia de archivo {} se finaliza la tarea.", nombreArchivoRegex);
                    return false;
                }
            }

            // Creacion de archivo
            FtpArchivoTransferencia ftpArchivoTransferencia = sftpTransferencia.crearRegistro(nombreArchivoRegex, TipoArchivo.ALI);

            prepararConfiguracionSftp();
            // Creacion de archivo temporal para preparar envio
            File archivo = sftpTransferencia.createFile(nombreArchivoRegex);

            ArchivoProfile archivoProfile = poblarArchivo(archivo, anticipos);
            // Verificar si existio problema en la generacion de archivo
            if (archivoProfile == null) {
                notificacionesCorreo.notificarErrorTransferenciaOperador(nombreArchivoRegex, TipoArchivo.ALI, "Error en la generaci\u00F3n de archivo");
                return false;
            }
            LOGGER.debug("3.2) Creación local archivo de de alticipos con {} filas.", anticipos.size());
            // Cargar archivo a servidor SFTP si se completa correctamente se registra intercambio con estado PRTE
            boolean cargado = sftpTransferencia.cargarArchivo(archivoProfile.getArchivo(), nombreArchivoRegex, ftpArchivoTransferencia);

            // Modificar cambio de estado
            if (cargado) {
                LOGGER.debug("3.3) Cargando de archivo {} a servidor sftp finalizado correctamente.", nombreArchivoRegex);
                TransferObject transferObject = new TransferObject();
                transferObject.initializate();
                int totalRegistros = 0;
                totalRegistros++;

                // Envio de correspondencia
                StringBuffer sb = new StringBuffer();
                sb.append("Se ha generado el archivo de anticipos para administradora LINKSER ");
                sb.append(" con ");
                sb.append(totalRegistros);
                sb.append(" anticipos.");
                ok = true;
                notificacionesCorreo.notificarCorrectaTransferenciaOperador(nombreArchivoRegex, TipoArchivo.ALI, sb.toString());
                LOGGER.info("Completado proceso de carga de archivo {} de anticipos a servidor SFTP de LINKSER correctamente.", nombreArchivoRegex);
            } else {
                LOGGER.debug("3.3) Cargando de archivo {} a servidor sftp finalizado incorrectamente.", nombreArchivoRegex);
                notificacionesCorreo.notificarErrorTransferenciaOperador(nombreArchivoRegex, TipoArchivo.ALI, "Error en carga de archivo");
                return false;
            }
            LOGGER.debug("3.4) Completado proceso de carga de archivo {} de anticipos a servidor SFTP de LINKSER correctamente.", nombreArchivoRegex);
        } else {
            sftpTransferencia.transferenciaCompleta("No se genera archivo en el servidor SFTP, sin anticipos a enviar");
            StringBuffer sb = new StringBuffer();
            sb.append("No se ha generado el archivo de anticipos para administradora LINKSER ");
            sb.append(" por no tener anticipos pendientes de envio.");
            notificacionesCorreo.notificarCorrectaTransferenciaOperador(nombreArchivoRegex, TipoArchivo.ALI, sb.toString());

            LOGGER.debug("3.4) Sin anticipos para enviar");
        }
        return ok;
    }


    public boolean prepararConfiguracionSftp() {
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_ENVIO_ANTICIPOS_LINKSER, Variables.FTP_RUTA_ENVIO_ANTICIPOS_LINKSER_DEFAULT));
        return sftpTransferencia.configurar(configuracion);
    }

    private ArchivoProfile poblarArchivo(File archivo, List<AnticiposLinkser> anticipos) {
        if (archivo == null) return null;
        FileOutputStream archivoWrite;
        try {
            archivoWrite = new FileOutputStream(archivo.getAbsoluteFile());
            BufferedWriter bufferWriter = new BufferedWriter(new OutputStreamWriter(archivoWrite, StandardCharsets.UTF_8)); // StandardCharsets.ISO_8859_1));
            StringBuilder sb = new StringBuilder();
            bufferWriter.write(sb.toString());

            for (AnticiposLinkser anticipo : anticipos) {
                TransferObject transferObject = new TransferObject();
                transferObject.initializate();
                // Grabar anticipo en archivo
                GeneradorCSV.adicionarLinea(bufferWriter, anticipo.getTexto());
            }
            bufferWriter.close();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("DETALLE DEL ARCHIVO");
                FileReader filer = new FileReader(archivo);
                int valor = filer.read();
                while (valor != -1) {
                    LOGGER.debug("", valor);
                    valor = filer.read();
                }
            }

        } catch (IOException e) {
            LOGGER.error("Ocurrio un error inesperado al preparar ", e);
            return null;
        }
        return new ArchivoProfile(archivo, 0L, 0L);
    }
}
