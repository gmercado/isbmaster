package com.bisa.bus.servicios.tc.linkser.sched;

import bus.env.api.MedioAmbiente;
import com.bisa.bus.servicios.tc.linkser.api.SftpEnvioAnticiposLinkser;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 21/11/2017.
 */
public class EnvioAnticiposLinkser implements Job{
    private static final Logger LOGGER = LoggerFactory.getLogger(EnvioAnticiposLinkser.class);

    private final MedioAmbiente medioAmbiente;
    private final SftpEnvioAnticiposLinkser sftpEnvioAnticiposLinkser;

    @Inject
    public EnvioAnticiposLinkser(MedioAmbiente medioAmbiente, SftpEnvioAnticiposLinkser sftpEnvioAnticiposLinkser){
        this.medioAmbiente = medioAmbiente;
        this.sftpEnvioAnticiposLinkser = sftpEnvioAnticiposLinkser;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Inicio de proceso envio de anticipos");
        boolean ok = sftpEnvioAnticiposLinkser.procesarArchivo();
        LOGGER.info("Respuesta proceso envio de anticipos: {}", ok);
        // Completar proceso de carga notificando al operador
    }
}
