package com.bisa.bus.servicios.tc.sftp.model;

/**
 * Created by atenorio on 22/05/2017.
 */
public class MensajeNotificacion {

    public static String getMensajeCorrecto(String nombreArchivo) {
        return "Estimados usuarios" +
                ",\n\nEl archivo " + nombreArchivo + " fue procesado exitosamente." +
                "\n\nAtte.," +
                "\nBanco BISA S.A.";
    }

    public static String getDescargaFtpCorrecto(String nombreArchivo) {
        return "Estimados usuarios" +
                ",\n\nEl archivo " + nombreArchivo + " fue descargado exitosamente." +
                "\n\nAtte.," +
                "\nBanco BISA S.A.";
    }

    public static String getMensajeError(String nombreArchivo, String mensaje) {
        return "Estimados usuarios" +
                ",\n\nEl archivo " + nombreArchivo + " fue rechazado." +
                "\nMensaje:\n" + mensaje +
                "\n\nPor favor es necesario revisar el archivo y verificar si ingreso en el directorio correcto." +
                "\n\nAtte.," +
                "\nBanco BISA S.A.";
    }

    public static String getDescargaFtpError(String nombreArchivo) {
        return "Estimados usuarios" +
                ",\n\nEl archivo " + nombreArchivo + " No pudo ser descargado de la FTP de Inforcred." +
                "\n\nPor favor es necesario revisar el archivo original en la FTP y/o proceder con la descarga manual si es necesario." +
                "\n\nAtte.," +
                "\nBanco BISA S.A.";
    }
}
