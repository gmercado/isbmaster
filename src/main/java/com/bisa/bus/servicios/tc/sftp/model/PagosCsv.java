package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;
import com.bisa.bus.servicios.tc.sftp.entities.IdPagosTC;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by atenorio on 30/05/2017.
 */
public class PagosCsv implements Serializable{
    private static final long serialVersionUID = 1L;

    private IdPagosTC idPago;

    private Long id;
    
    @FormatCsv(name = "PATIPREG", nullable = false, length = 2)
    private String tipoRegistro;
    
    @FormatCsv(name = "PANUCUEN", nullable = false, length = 20, fillSpace = "0")
    private BigInteger cuentaTarjeta;
    
    @FormatCsv(name = "PACOMON", nullable = false, length = 3, fillSpace = "0")
    private short tipoMoneda;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    
    @FormatCsv(name = "PAIMPPG", nullable = false, pattern = "##.000000", scale = 6, length = 18, fillSpace = "0")
    private BigDecimal importe;
    
    @FormatCsv(name = "PAFPAG", nullable = false, length = 8)
    private String fechaPago;

    @FormatCsv(name = "ESP", nullable = false, length = 1)
    private String espacio=" ";
    
    @FormatCsv(name = "PAFPAGH", nullable = false, length = 8)
    private String horaPago;
    
    @FormatCsv(name = "PAFRMPAG", nullable = false, length = 1)
    private short forma;
    
    @FormatCsv(name = "PACNLPAG", nullable = false, length = 1)
    private short canal;
    
    @FormatCsv(name = "PATRN", nullable = false, length = 20)
    private String transaccion;
    
    @FormatCsv(name = "PAINFAD", nullable = false, length = 120)
    private String informacionAdicional;
    
    @FormatCsv(name = "PACONFIR", nullable = false, length = 1)
    private Character confirmacion;

    @FormatCsv(name = "ENBLANCO", nullable = false, length = 74)
    private String pafill;

    // Identificador devuelto por sistema Nazir para el pago enviado    
    @FormatCsv(name = "PACODIDF", nullable = false, length = 20)
    private String codigoPago;
    
    @FormatCsv(name = "PACODERR", nullable = false, length = 3)
    private String codError;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoRegistro() {
        return tipoRegistro;
    }

    public void setTipoRegistro(String tipoRegistro) {
        this.tipoRegistro = tipoRegistro;
    }

    public short getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(short tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getHoraPago() {
        return horaPago;
    }

    public void setHoraPago(String horaPago) {
        this.horaPago = horaPago;
    }

    public short getForma() {
        return forma;
    }

    public void setForma(short forma) {
        this.forma = forma;
    }

    public short getCanal() {
        return canal;
    }

    public void setCanal(short canal) {
        this.canal = canal;
    }

    public String getTransaccion() {
        //if(idPago != null) return idPago.getTransaccion();
        return transaccion;
    }

    public void setTransaccion(String transaccion) {
        this.transaccion = transaccion;
    }

    public String getInformacionAdicional() {
        return informacionAdicional;
    }

    public void setInformacionAdicional(String informacionAdicional) {
        this.informacionAdicional = informacionAdicional;
    }

    public Character getConfirmacion() {
        return confirmacion;
    }

    public void setConfirmacion(Character confirmacion) {
        this.confirmacion = confirmacion;
    }

    public String getCodigoPago() {
        return codigoPago;
    }

    public void setCodigoPago(String codigoPago) {
        this.codigoPago = codigoPago;
    }

    public String getCodError() {
        return codError;
    }

    public void setCodError(String codError) {
        this.codError = codError;
    }

    public BigInteger getCuentaTarjeta() {
        return cuentaTarjeta;
    }

    public void setCuentaTarjeta(BigInteger cuentaTarjeta) {
        this.cuentaTarjeta = cuentaTarjeta;
    }

    public String getEspacio() {
        return espacio;
    }

    public void setEspacio(String espacio) {
        this.espacio = espacio;
    }

    public IdPagosTC getIdPago() {
        return idPago;
    }

    public void setIdPago(IdPagosTC idPago) {
        this.idPago = idPago;
    }

    public String getPafill() {
        return pafill;
    }

    public void setPafill(String pafill) {
        this.pafill = pafill;
    }


    @Override
    public String toString() {
        return "cuentaTarjeta: " + cuentaTarjeta + "\\n" +
               "transaccion: " + transaccion + "\\n" +
               "codError: " + codError + "\\n";
    }
}
