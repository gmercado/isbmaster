package com.bisa.bus.servicios.tc.linkser.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.tc.linkser.model.ConsultaMovimientoRespuesta;
import com.bisa.bus.servicios.tc.linkser.model.ConsultaMovimientoSolicitudForm;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

/**
 * Created by atenorio on 17/01/2017.
 */
@AuthorizeInstantiation({RolesBisa.LINKSER_CONSUMO, RolesBisa.LINKSER_CONSULTAS})
@Titulo("Respuesta Linkser")
public class RespuestaLinkser extends BisaWebPage {

    private final ConsultaMovimientoSolicitudForm solicitud;
    private final ConsultaMovimientoRespuesta respuestaLinkser;


    public RespuestaLinkser(ConsultaMovimientoSolicitudForm solicitud, ConsultaMovimientoRespuesta respuestaLinkser) {
        super();
        this.solicitud = solicitud;
        this.respuestaLinkser = respuestaLinkser;
        inicio();
    }

    public void inicio() {
        //Model
        IModel<ConsultaMovimientoRespuesta> objectModel = new CompoundPropertyModel<>(respuestaLinkser);
        //Form
        Form<ConsultaMovimientoRespuesta> form = new Form<>("respuestaLinkser", objectModel);
        add(form);

        //Cuerpo
        form.add(new Label("numeroTarjeta", Model.of(solicitud.getNumeroTarjeta())));

        form.add(new Label("codigoRespuestaBean", new PropertyModel<Object>(objectModel,"codRespuesta")));
        form.add(new Label("descripcionRespuestaBean", new PropertyModel<Object>(objectModel,"dscRespuesta")));
        //botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(new ConsultaTCLinkser(solicitud));
            }
        });
    }
}
