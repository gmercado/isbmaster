package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.MovimientoMensualHistorico;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 29/06/2017.
 */
public class MovimientoMensualHistoricoDao extends DaoImpl<MovimientoMensualHistorico, Long> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovimientoMensualHistoricoDao.class);

    @Inject
    protected MovimientoMensualHistoricoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(MovimientoMensualHistorico registro, String user) {
        registro.setUsuarioCreador(user);
        registro.setFechaModificacion(new Date());
        persist(registro);
        return true;
    }
}