package com.bisa.bus.servicios.tc.sftp.model;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by atenorio on 06/06/2017.
 */
public enum CodigoErrorPagoAtc {

    /*000 - GENERAL ERRORS - 049*/
    NO_ERROR("000", "Sin Error"), //Sin Error
    MALFORMED_REGISTRY("001", "Registro mal formado,no pasa validación física"), //Registro mal formado,no pasa validación física
    INVALID_REGISTRY("002", "Alguno de los datos no tiene el formato correcto o no está dentro de los valores permitidos."),// Alguno de los datos no tiene el formato correcto o no está dentro de los valores permitidos.
    NEXT_REGISTRY_EXPECTED("003", "Se espera un próximo registro"),//Se espera un próximo registro
    PA_PU_REGISTRY_EXPECTED("004", "Registro invalido según la estructura definida."),//Registro invalido según la estructura definida.
    PA_PU_OR_NULL_REGISTRY_EXPECTED("005", "Se espera un registro vacio o un PR o un CF"),//Se espera un registro vacio o un PR o un CF

        /*100 - 199*/
    GENERAL_ACCOUNT_NOT_FOUND("100", "No se encontr\u00F3 la cuenta."),// No se encontró la cuenta .
    INVALID_CURRENCY("101", "CurrencyCode que viene en el archivo no existe."),// CurrencyCode que viene en el archivo no existe.
    INVALID_PAYMENT_METHOD("102", "El m\u00F3todo de pagos no existe en el enumerado"),// El método de pagos no existe en el enumerado
    INVALID_PAYMENT_CHANNEL("103", "El canal de pagos no existe en el enumerado"),// El canal de pagos no existe en el enumerado
    INVALID_FORMAT_DATE("104", "Formato de la fecha no es (DDMMYYYY HH:mm:ss)"),// Formato de la fecha no es (DDMMYYYY HH:mm:ss)
    INVALID_IS_CONFIRMED("105", "IsConfirmed no es 'S' o 'N'"),// IsConfirmed no es 'S' o 'N'
    INVALID_GENERAL_ACCOUNT_NUMBER("106", "El INTERNAL_ACCOUNT_NUMBER es muy largo. Debe ser menor o igual a 999999999"),//El INTERNAL_ACCOUNT_NUMBER es muy largo. Debe ser menor o igual a 999999999

        /* Errores posibles en AcceptPayment o UndoPayment */
    ERROR_IN_PROCESS("110", "Error al llamar AcceptPayment o UndoPayment"), //Error al llamar AcceptPayment o UndoPayment
    CURRENCY_NOT_FOUND("111", "CURRENCY_NOT_FOUND"),
    CURRENCY_EXCHANGE_ATTRIBUTE_NOT_SPECIFIED("112", "CURRENCY_EXCHANGE_ATTRIBUTE_NOT_SPECIFIED"),
    PAYMENT_AMOUNT_LESS_THAN_ZERO("113", "PAYMENT_AMOUNT_LESS_THAN_ZERO"),
    GENERAL_ACCOUNT_NUMBER_NOT_SPECIFIED("114", "GENERAL_ACCOUNT_NUMBER_NOT_SPECIFIED"),
    PAYMENT_ALREADY_PROCESSED("115", "PAYMENT_ALREADY_PROCESSED"),
    GENERAL_ACCOUNT_IS_CANCELLED("116", "GENERAL_ACCOUNT_IS_CANCELLED"),
    MISCONFIGURED_CYCLE_CALENDAR("117", "MISCONFIGURED_CYCLE_CALENDAR"),
    PAYMENT_DATE_OUTSIDE_CURRENT_PERIOD("118", "PAYMENT_DATE_OUTSIDE_CURRENT_PERIOD"),
    PAYMENT_TO_UNDO_NOT_FOUND("119", "PAYMENT_TO_UNDO_NOT_FOUND"),
    UNDO_PAYMENT_DATE_DIFFERS_FROM_BUSINESS_DATE("120", "UNDO_PAYMENT_DATE_DIFFERS_FROM_BUSINESS_DATE"),
    PAYMENT_ALREADY_UNDONE("121", "PAYMENT_ALREADY_UNDONE"),
    PAYMENT_UNDO_DATE_INVALID("122", "PAYMENT_UNDO_DATE_INVALID"),

        /*900 - 999*/
    UNEXPECTED_ERROR("996", "Error inesperado"); // Error inesperado

    private String codigo;
    private String descripcion;

    CodigoErrorPagoAtc(String codigo, String descripcion){
        this.setCodigo(codigo);
        this.setDescripcion(descripcion);
    }

    public static CodigoErrorPagoAtc valorEnum(String valor) {
        if(StringUtils.isEmpty(valor)) valor="";
        List<CodigoErrorPagoAtc> codigos = new ArrayList<>(Arrays.asList((CodigoErrorPagoAtc.values())));
        for (CodigoErrorPagoAtc es : codigos) {
            if (valor.equals(es.codigo)){
                return es;
            }
        }
        return null;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
