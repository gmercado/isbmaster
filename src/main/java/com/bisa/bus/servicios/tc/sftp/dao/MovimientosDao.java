package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.IdMovimientosTC;
import com.bisa.bus.servicios.tc.sftp.entities.MovimientosTC;
import com.bisa.bus.servicios.tc.sftp.entities.RespuestaSolicitudTarjeta;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 09/06/2017.
 */
public class MovimientosDao extends DaoImpl<MovimientosTC, IdMovimientosTC> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovimientosDao.class);

    @Inject
    protected MovimientosDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }


    public boolean guardar(MovimientosTC registro, String user) {
        registro.setUsuarioCreador(user);
        registro.setFechaModificacion(new Date());
        persist(registro);
        return true;
    }

    public boolean limpiarTodo() {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<MovimientosTC> query = entityManager.createQuery("DELETE FROM MovimientosTC");//
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }
}