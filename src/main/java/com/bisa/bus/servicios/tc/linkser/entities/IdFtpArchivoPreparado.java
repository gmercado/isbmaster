package com.bisa.bus.servicios.tc.linkser.entities;

import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 20/11/2017.
 */
@Embeddable
public class IdFtpArchivoPreparado implements Serializable {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "CTPFEI", nullable = false)
    private Integer proceso;

    @Basic(optional = false)
    @Column(name = "CTPTIP", nullable = false, length = 3)
    @Enumerated(EnumType.STRING)
    private TipoArchivo tipo;

    public String getTipoDescripcion(){
        return tipo.getDescripcion();
    }

    public TipoArchivo getTipo() {
        return tipo;
    }

    public void setTipo(TipoArchivo tipo) {
        this.tipo = tipo;
    }


    public Integer getProceso() {
        return proceso;
    }

    public void setProceso(Integer proceso) {
        this.proceso = proceso;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdFtpArchivoPreparado that = (IdFtpArchivoPreparado) o;

        if (getProceso() != null ? !getProceso().equals(that.getProceso()) : that.getProceso() != null) return false;
        return getTipo() == that.getTipo();
    }

    @Override
    public int hashCode() {
        int result = getProceso() != null ? getProceso().hashCode() : 0;
        result = 31 * result + (getTipo() != null ? getTipo().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "IdFtpArchivoPreparado{" +
                "proceso=" + proceso +
                ", tipo=" + tipo +
                '}';
    }
}
