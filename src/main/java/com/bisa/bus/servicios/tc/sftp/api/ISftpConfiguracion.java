package com.bisa.bus.servicios.tc.sftp.api;

/**
 * Created by atenorio on 13/11/2017.
 */
public interface ISftpConfiguracion {
    void inicializar();

    String getRemoteTemp();

    void setRemoteTemp(String remoteTemp);

    String getUrl();

    void setUrl(String url);

    String getPassword();

    void setPassword(String password);

    String getUsuario();

    void setUsuario(String usuario);

    String getLocalTemp();

    void setLocalTemp(String localTemp);

    String getRemotePath();

    void setRemotePath(String remotePath);

    boolean getMd5Subida();

    void setMd5Subida(boolean md5Subida);

    boolean getMd5Bajada();

    void setMd5Bajada(boolean md5Bajada);
}
