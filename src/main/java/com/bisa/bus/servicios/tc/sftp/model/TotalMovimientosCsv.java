package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import java.util.List;

/**
 * Registro trailer
 * Para coneto y recuperacion de fecha de procesamiento
 * Created by atenorio on 09/06/2017.
 * */

public class TotalMovimientosCsv {
    @FormatCsv(name = "banco", nullable = false, length = 4)
    private Short codigoBanco;
    @FormatCsv(name = "fecha", nullable = false, length = 4)
    private int fechaProceso;
    @FormatCsv(name = "cantidad", nullable = false, length = 4)
    private long cantidad;
    @FormatCsv(name = "espacio", nullable = false, length = 4)
    private String espacio;

    private List<MovimientoCsv> movimientos;

    public Short getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(Short codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public int getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(int fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public List<MovimientoCsv> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(List<MovimientoCsv> movimientos) {
        this.movimientos = movimientos;
    }

    public String getEspacio() {
        return espacio;
    }

    public void setEspacio(String espacio) {
        this.espacio = espacio;
    }
}
