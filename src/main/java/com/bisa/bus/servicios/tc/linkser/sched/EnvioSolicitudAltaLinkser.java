package com.bisa.bus.servicios.tc.linkser.sched;

import bus.env.api.MedioAmbiente;
import com.bisa.bus.servicios.tc.linkser.api.SftpEnvioSolicitudLinkser;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 09/11/2017.
 */
public class EnvioSolicitudAltaLinkser implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(EnvioSolicitudAltaLinkser.class);

    private final MedioAmbiente medioAmbiente;
    private final SftpEnvioSolicitudLinkser sftpEnvioSolicitud;

    @Inject
    public EnvioSolicitudAltaLinkser(MedioAmbiente medioAmbiente, SftpEnvioSolicitudLinkser sftpEnvioSolicitud){
        this.medioAmbiente = medioAmbiente;
        this.sftpEnvioSolicitud = sftpEnvioSolicitud;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Inicio de proceso envio de solicitud alta");
        boolean ok = sftpEnvioSolicitud.procesarArchivo();
        LOGGER.info("Respuesta de proceso envio de solcitud alta: {}", ok);
    }
}
