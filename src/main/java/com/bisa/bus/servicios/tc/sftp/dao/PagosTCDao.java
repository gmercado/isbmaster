package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.IdPagosTC;
import com.bisa.bus.servicios.tc.sftp.entities.PagosTC;
import com.bisa.bus.servicios.tc.sftp.model.EstadoPagosTC;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.*;

/**
 * Created by atenorio on 29/05/2017.
 */
public class PagosTCDao extends DaoImpl<PagosTC, IdPagosTC> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(PagosTCDao.class);

    @Inject
    protected PagosTCDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }


    public boolean guardar(PagosTC pago, String user) {
        if (pago.getIdPago() != null) {
            pago.setUsuarioModificador(user);
            pago.setFechaModificacion(new Date());
            merge(pago);
        } else {
            pago.setUsuarioCreador(user);
            pago.setFechaModificacion(new Date());
            persist(pago);
        }
        return true;
    }

    public List<PagosTC> getPagosParaEnvio() {
        LOGGER.debug("Obteniendo pagos de tarjetas de credito de DB para envio.");
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<PagosTC> q = cb.createQuery(PagosTC.class);
                    Root<PagosTC> p = q.from(PagosTC.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(p.get("estado").in(EstadoPagosTC.GENE.name()));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<PagosTC> query = entityManager.createQuery(q);
                    return query.getResultList();
                }
        );
    }

    public PagosTC getPagoId(IdPagosTC id) {
        LOGGER.debug("Obteniendo registro de pago con nombre:{}.", id);

        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<PagosTC> q = cb.createQuery(PagosTC.class);
                    Root<PagosTC> p = q.from(PagosTC.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("idPago").get("cuentaTarjeta"), id.getCuentaTarjeta()));
                    predicatesAnd.add(cb.equal(p.get("idPago").get("transaccion"), id.getTransaccion()));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));

                    TypedQuery<PagosTC> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                        return query.getResultList().get(0);
                    } else {
                        return null;
                    }
                }
        );

    }


    @Override
    protected Iterable<Path<String>> getFullTexts(Root<PagosTC> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.<String>get("nombre"));
        paths.add(p.<String>get("valor"));
        return paths;
    }

    @Override
    protected Path<IdPagosTC> countPath(Root<PagosTC> from) {
        return from.get("idPago");
    }


    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<PagosTC> from, PagosTC example, PagosTC example2) {
        List<Predicate> predicates = new LinkedList<>();
        Date fecha = example.getFechaCreacion();
        String cuenta = (example.getCuentaTarjeta()==null)?null:StringUtils.trimToNull(example.getCuentaTarjeta().toString());
        EstadoPagosTC estado = null;
        if (example.getEstadoPago() != null) {
            estado = example.getEstadoPago();
        }
        if (fecha != null && example2.getFechaCreacion() != null) {
            Date inicio = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH);
            Date inicio2 = DateUtils.truncate(example2.getFechaCreacion(), Calendar.DAY_OF_MONTH);
            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
            LOGGER.debug("   FECHAS ENTRE [{}] A [{}]", inicio, inicio2);
            predicates.add(cb.between(from.get("fechaCreacion"), inicio, inicio2));
        }
        if (cuenta != null) {
            try {
                Long s = Long.parseLong(cuenta);
                LOGGER.debug(">>>>> cuenta=" + s);
                predicates.add(cb.equal(from.get("cuentaTarjeta"), s));
            }catch (Exception ex){
                LOGGER.error("Error en " , ex);
            }
        }
        if (estado != null) {
            LOGGER.debug(">>>>> estado=" + estado.getDescripcion());
            predicates.add(cb.equal(from.get("estado"), estado.name()));
        }
        return predicates.toArray(new Predicate[predicates.size()]);
    }
}
