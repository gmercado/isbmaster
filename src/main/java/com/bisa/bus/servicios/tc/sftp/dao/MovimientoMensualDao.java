package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.IdMovimientoMensual;
import com.bisa.bus.servicios.tc.sftp.entities.MovimientoMensual;
import com.bisa.bus.servicios.tc.sftp.entities.RespuestaSolicitudTarjeta;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 23/06/2017.
 */
public class MovimientoMensualDao extends DaoImpl<MovimientoMensual, Long> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovimientoMensualDao.class);

    @Inject
    protected MovimientoMensualDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(MovimientoMensual registro, String user) {
        registro.setUsuarioCreador(user);
        registro.setFechaModificacion(new Date());
        persist(registro);
        return true;
    }

    public boolean limpiarTodo() {
        LOGGER.debug("Limpiando tabla MovimientoMensual.");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<MovimientoMensual> query = entityManager.createQuery("DELETE FROM MovimientoMensual");//
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }
}