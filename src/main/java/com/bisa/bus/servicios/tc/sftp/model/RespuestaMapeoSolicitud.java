package com.bisa.bus.servicios.tc.sftp.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by atenorio on 12/09/2017.
 */
public class RespuestaMapeoSolicitud {
    private static final Logger LOGGER = LoggerFactory.getLogger(RespuestaMapeoSolicitud.class);
    private List<SolicitudCompleto> solicitudes = new ArrayList<SolicitudCompleto>();
    private List<SolicitudCompletoEmpresarial> solicitudesEmpresariales = new ArrayList<SolicitudCompletoEmpresarial>();

    public List<SolicitudCompleto> getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(List<SolicitudCompleto> solicitudes) {
        this.solicitudes = solicitudes;
    }

    public List<SolicitudCompletoEmpresarial> getSolicitudesEmpresariales() {
        return solicitudesEmpresariales;
    }

    public void setSolicitudesEmpresariales(List<SolicitudCompletoEmpresarial> solicitudesEmpresariales) {
        this.solicitudesEmpresariales = solicitudesEmpresariales;
    }

    public boolean existeRespuesta() {
        if (solicitudes == null || solicitudesEmpresariales == null){
            return false;
        }
        return solicitudes.size() > 0 || solicitudesEmpresariales.size() > 0;
    }
}
