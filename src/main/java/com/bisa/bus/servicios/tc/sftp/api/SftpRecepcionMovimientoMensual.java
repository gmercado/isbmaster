package com.bisa.bus.servicios.tc.sftp.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.csv.LecturaCSV;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.tc.sftp.dao.MovimientoMensualDao;
import com.bisa.bus.servicios.tc.sftp.dao.MovimientoMensualHistoricoDao;
import com.bisa.bus.servicios.tc.sftp.entities.MovimientoMensual;
import com.bisa.bus.servicios.tc.sftp.entities.MovimientoMensualHistorico;
import com.bisa.bus.servicios.tc.sftp.model.EstadoPagosTC;
import com.bisa.bus.servicios.tc.sftp.model.MovimientoMensualCsv;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgProgramaTarjetaCredito;
import com.bisa.bus.servicios.tc.sftp.utils.FormatoNumericoTexto;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by atenorio on 23/06/2017.
 */
public class SftpRecepcionMovimientoMensual {

    private static final Logger LOGGER = LoggerFactory.getLogger(SftpRecepcionMovimientoMensual.class);

    private final MedioAmbiente medioAmbiente;
    private final MovimientoMensualDao movimientoMensualDao;
    private final MovimientoMensualHistoricoDao movimientoMensualHistoricoDao;

    private final RpgProgramaTarjetaCredito rpgPrograma;
    private final NotificacionesCorreo notificacionesCorreo;
    SftpConfiguracion configuracion;
    SftpTransferencia sftpTransferencia;
    private String nombreArchivoProcesado = "";

    @Inject
    public SftpRecepcionMovimientoMensual(MedioAmbiente medioAmbiente,
                                          MovimientoMensualDao movimientoMensualDao, MovimientoMensualHistoricoDao movimientoMensualHistoricoDao,
                                          NotificacionesCorreo notificacionesCorreo, RpgProgramaTarjetaCredito rpgPrograma,
                                          SftpConfiguracion configuracion, SftpTransferencia sftpTransferencia) {
        this.medioAmbiente = medioAmbiente;
        this.movimientoMensualDao = movimientoMensualDao;
        this.movimientoMensualHistoricoDao = movimientoMensualHistoricoDao;
        this.rpgPrograma = rpgPrograma;
        this.notificacionesCorreo = notificacionesCorreo;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para procesar archivo de movimiento mensual 080 enviado por ATC en SFTP remoto.
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    public boolean procesarArchivo(int fechaProcesoSaldo) {
        String detalle;
        boolean ok = true;
        LOGGER.info("Inicia proceso de carga movimientos mensual");
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_MOV_MENSUAL_080, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_MOV_MENSUAL_080_DEFAULT);
        LOGGER.info("Descargar archivo con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        LOGGER.debug("Descarga de archivo de a servidor SFTP.");
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_MOVIMIENTOS_MENSUAL_ATC, Variables.FTP_RUTA_RECEPCION_MOVIMIENTOS_MENSUAL_ATC_DEFAULT));
        sftpTransferencia.configurar(configuracion);
        String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_ATC, Variables.FECHA_DE_PROCESO_ATC_DEFAULT);
        // Calcular fecha de un dia anterior
        Calendar hoy = Calendar.getInstance();
        hoy.add(Calendar.DAY_OF_YEAR, -1);
        Date fechaProcesar = hoy.getTime();

        if (StringUtils.isEmpty(fechaProceso) || "0".equals(fechaProceso)) {
            fechaProceso = FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar);
        } else {
            fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
            if (fechaProcesar == null) {
                notificacionesCorreo.notificarError("Error en fecha de proceso recepci\u00F3n movimiento mensual", "");
                return false;
            }
        }
        String nombreArchivo = "MOVMENSUAL" + fechaProceso;
        nombreArchivoProcesado = nombreArchivo;

        List<File> archivos = sftpTransferencia.descargarListaArchivos(nombreArchivoRegex, nombreArchivo, TipoArchivo.CIE);
        ok = (archivos == null || archivos.size() == 0) ? false : true;
        LOGGER.info("Descarga completa:" + ok);

        if (!ok) {
            // Cerrar el registro con error y permitir reintento
            detalle = "Problema en transferencia o no existen archivos de respuesta en servidor SFTP.";
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.CIE, detalle);
            LOGGER.warn(detalle);
            return false;
        }

        // Procesamiento de archivo local
        List<MovimientoMensualCsv> lista = new ArrayList<MovimientoMensualCsv>();
        for (File archivo : archivos) {
            List<MovimientoMensualCsv> listaMapeo = mapearArchivo(archivo);
            if (listaMapeo == null || listaMapeo.size() == 0) {
                detalle = "Error en mapeo de archivo de respuesta o archivo vac\u00EDo";
                LOGGER.warn(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.CIE, detalle);
                return false;
            }
            lista.addAll(listaMapeo);
        }
        for (MovimientoMensualCsv dg : lista) {
            // Validar que se realizo el mapeo de forma exitosa de la estructura de pago
            if (dg == null) {
                detalle = "Error en mapeo de una estructura se requiere revisar el archivo de respuesta";
                LOGGER.error(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.CIE, detalle);
                return false;
            }
        }

        LOGGER.debug("Actualizar tabla de datos generales");
        LOGGER.debug("Verificar si un registro se encuentra en un estado inconsistente");

        TransferObject transferObject = new TransferObject();
        transferObject.initializate();

        // Limpiar tablas temporales
        if (!movimientoMensualDao.limpiarTodo()) {
            detalle = "Error al limpiar tabla temporal movimientos.";
            LOGGER.error(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.CIE, detalle);
            return false;
        }
        int totalRegistros = 0;
        // Verificar consistencia de registro
        for (MovimientoMensualCsv movimiento : lista) {
            totalRegistros++;
            // Validar que se tenga pendiente de respuesta a pagos
            MovimientoMensual movimientoMensual = transferObject.convert(movimiento, MovimientoMensual.class);
            movimientoMensual.setEstado(EstadoPagosTC.GENE.name());
            movimientoMensual.setMmcadm('A');
            movimientoMensual.setFechaProceso(fechaProcesoSaldo); //FormatosUtils.fechaYYYYMMDD(fechaProcesar).intValue());
            movimientoMensual.setImporteBsSigno(FormatoNumericoTexto.getSigno(movimientoMensual.getImporteBs()));
            movimientoMensual.setImporteBsTexto(FormatoNumericoTexto.getNumero(movimientoMensual.getImporteBs(), 10));
            movimientoMensual.setImporteUsSigno(FormatoNumericoTexto.getSigno(movimientoMensual.getImporteUs()));
            movimientoMensual.setImporteUsTexto(FormatoNumericoTexto.getNumero(movimientoMensual.getImporteUs(), 10));

            movimientoMensual.setFechaRecepcion(new Date());
            movimientoMensual.setIdArchivoRecepcion(sftpTransferencia.getFtpArchivoTransferencia().getId());

            movimientoMensual.setFechaCreacion(new Date());
            movimientoMensual.setUsuarioCreador("");
            movimientoMensual.setFechaModificacion(new Date());
            movimientoMensual.setUsuarioModificador("");

            // Registrar mivimiento
            ok = ok && movimientoMensualDao.guardar(movimientoMensual, "ISB");
            MovimientoMensualHistorico historico = transferObject.convert(movimientoMensual, MovimientoMensualHistorico.class);
            ok = ok && movimientoMensualHistoricoDao.guardar(historico, "ISB");
        }

        if (ok) {
            nombreArchivoProcesado = StringUtils.trimToEmpty(sftpTransferencia.getNombreArchivoRemoto());
            detalle = "Recepci\u00F3n de Movimientos Mensuales finalizada correctamente con " + totalRegistros
                    + " registros en archivo(s) " + getNombreArchivoProcesado() + ".";
            sftpTransferencia.transferenciaCompleta(StringUtils.substring(detalle, 0,254));
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivo, TipoArchivo.CIE, detalle);
        } else {
            detalle = "Problemas en registro de base de datos.";
            sftpTransferencia.transferenciaConErrorInterno(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.CIE, detalle);
        }
        LOGGER.debug("Completado proceso de carga de archivo {} de saldo mensual 079 SFTP de ATC correctamente.", sftpTransferencia.getNombreArchivoRemoto());
        return ok;
    }

    public List<MovimientoMensualCsv> mapearArchivo(File archivo) {
        LOGGER.info("Ruta local de archivo a procesar:{}", archivo.getAbsolutePath());
        FileInputStream is = null;
        BufferedReader br = null;
        List<MovimientoMensualCsv> lista = new ArrayList<MovimientoMensualCsv>();
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();
        try {
            is = new FileInputStream(archivo);
            String codificacion = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC, Variables.FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC_DEFAULT);
            br = new BufferedReader(new InputStreamReader(is, codificacion));
            String linea = br.readLine();
            List<String> lineas = new ArrayList<String>();

            while (linea != null) {
                LOGGER.debug("Detalle de registro recuperado: {}", linea);
                if (StringUtils.isNotEmpty(linea) && linea.length() > 1) {
                    MovimientoMensualCsv dg = LecturaCSV.procesar(MovimientoMensualCsv.class, linea, "|");
                    lista.add(dg);
                }
                linea = br.readLine();
            }
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al procesar el archivo "
                    + archivo.getName() + ".", e);
            return null;
        } finally {
            IOUtils.closeQuietly(is);
        }
        return lista;
    }

    public String getNombreArchivoProcesado() {
        return sftpTransferencia.getNombreArchivoRemotoFecha();
    }

}

