package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

/**
 * Created by atenorio on 25/05/2017.
 */
public class RespuestaTarjeta {

    @FormatCsv(name = "tipodoc", length = 2, nullable = true, fillSpace = "0")
    private String tipoDocumento;

    @FormatCsv(name = "nrodoc", length = 15, nullable = true, fillSpace = "0")
    private
    String numeroDocumento = "";

    @FormatCsv(name = "nroTrj", length = 16, nullable = true)
    private String numeroTarjeta = "";

    @FormatCsv(name = "tipoTrj", length = 1, nullable = true)
    private String tipoTarjeta = "";

    @FormatCsv(name = "nroSolicitud", length = 9, nullable = true, fillSpace = "0")
    private int numeroSolicitud;

    @FormatCsv(name = "procesado", length = 7, nullable = true)
    private String procesado = "";

    public String getProcesado() {
        return procesado;
    }

    public void setProcesado(String procesado) {
        this.procesado = procesado;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getTipoTarjeta() {
        return tipoTarjeta;
    }

    public void setTipoTarjeta(String tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    public int getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(int numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

}
