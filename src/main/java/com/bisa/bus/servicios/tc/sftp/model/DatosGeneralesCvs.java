package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import java.math.BigDecimal;

/**
 * Created by atenorio on 08/06/2017.
 */
public class DatosGeneralesCvs {
    
    @FormatCsv(name = "ESBAEM", nullable = false, length = 4)
    private String codigoBanco;
    
    @FormatCsv(name = "ESNUTCNC", nullable = false, length = 10)
    private String numeroCuenta;
    
    @FormatCsv(name = "ESIMLIBS", nullable = false, precision = 11, scale = 2)
    private BigDecimal impBalanceLiqBs;
    
    @FormatCsv(name = "ESIMLIUS", nullable = false, precision = 11, scale = 2)
    private BigDecimal impBalanceLiqUs;
    
    @FormatCsv(name = "ESPAMI", nullable = false, precision = 11, scale = 2)
    private BigDecimal pagoMinimo;
    
    @FormatCsv(name = "ESINT", nullable = false, precision = 11, scale = 2)
    private BigDecimal totalIntereses;
    
    @FormatCsv(name = "ESCOBS", nullable = false, precision = 11, scale = 2)
    private BigDecimal consumoBs;
    
    @FormatCsv(name = "ESCOUS", nullable = false, precision = 11, scale = 2)
    private BigDecimal consumoUs;
    
    @FormatCsv(name = "ESADBS", nullable = false, precision = 11, scale = 2)
    private BigDecimal adelantoBs;
    
    @FormatCsv(name = "ESADUS", nullable = false, precision = 11, scale = 2)
    private BigDecimal adelantoUs;
    
    @FormatCsv(name = "ESCOAUBS", nullable = false, precision = 11, scale = 2)
    private BigDecimal consumoAutorizadoBs;
    
    @FormatCsv(name = "ESCOAUUS", nullable = false, precision = 11, scale = 2)
    private BigDecimal consumoAutorizadoUs;
    
    @FormatCsv(name = "ESADAUBS", nullable = false, precision = 11, scale = 2)
    private BigDecimal adelantoAutorizadoBs;
    
    @FormatCsv(name = "ESADAUUS", nullable = false, precision = 11, scale = 2)
    private BigDecimal adelantoAutorizadoUs;
    
    @FormatCsv(name = "ESCAADAU", nullable = false, length = 3)
    private String cantAdelantoAutorizado;
    
    @FormatCsv(name = "ESAJBS", nullable = false, precision = 11, scale = 2)
    private BigDecimal ajustesBs;
    
    @FormatCsv(name = "ESAJUS", nullable = false, precision = 11, scale = 2)
    private BigDecimal ajustesUs;
    
    @FormatCsv(name = "ESCABS", nullable = false, precision = 11, scale = 2)
    private BigDecimal cargosBs;
    
    @FormatCsv(name = "ESPABS", nullable = false, precision = 11, scale = 2)
    private BigDecimal pagoBs;
    
    @FormatCsv(name = "ESNIMO", nullable = false, length = 2)
    private String nivelMora;

    @FormatCsv(name = "ESPACIOS", nullable = false, length = 39)
    private String espacio;

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public BigDecimal getImpBalanceLiqBs() {
        return impBalanceLiqBs;
    }

    public void setImpBalanceLiqBs(BigDecimal impBalanceLiqBs) {
        this.impBalanceLiqBs = impBalanceLiqBs;
    }

    public BigDecimal getImpBalanceLiqUs() {
        return impBalanceLiqUs;
    }

    public void setImpBalanceLiqUs(BigDecimal impBalanceLiqUs) {
        this.impBalanceLiqUs = impBalanceLiqUs;
    }

    public BigDecimal getPagoMinimo() {
        return pagoMinimo;
    }

    public void setPagoMinimo(BigDecimal pagoMinimo) {
        this.pagoMinimo = pagoMinimo;
    }

    public BigDecimal getTotalIntereses() {
        return totalIntereses;
    }

    public void setTotalIntereses(BigDecimal totalIntereses) {
        this.totalIntereses = totalIntereses;
    }

    public BigDecimal getConsumoBs() {
        return consumoBs;
    }

    public void setConsumoBs(BigDecimal consumoBs) {
        this.consumoBs = consumoBs;
    }

    public BigDecimal getConsumoUs() {
        return consumoUs;
    }

    public void setConsumoUs(BigDecimal consumoUs) {
        this.consumoUs = consumoUs;
    }

    public BigDecimal getAdelantoBs() {
        return adelantoBs;
    }

    public void setAdelantoBs(BigDecimal adelantoBs) {
        this.adelantoBs = adelantoBs;
    }

    public BigDecimal getAdelantoUs() {
        return adelantoUs;
    }

    public void setAdelantoUs(BigDecimal adelantoUs) {
        this.adelantoUs = adelantoUs;
    }

    public BigDecimal getConsumoAutorizadoBs() {
        return consumoAutorizadoBs;
    }

    public void setConsumoAutorizadoBs(BigDecimal consumoAutorizadoBs) {
        this.consumoAutorizadoBs = consumoAutorizadoBs;
    }

    public BigDecimal getConsumoAutorizadoUs() {
        return consumoAutorizadoUs;
    }

    public void setConsumoAutorizadoUs(BigDecimal consumoAutorizadoUs) {
        this.consumoAutorizadoUs = consumoAutorizadoUs;
    }

    public BigDecimal getAdelantoAutorizadoBs() {
        return adelantoAutorizadoBs;
    }

    public void setAdelantoAutorizadoBs(BigDecimal adelantoAutorizadoBs) {
        this.adelantoAutorizadoBs = adelantoAutorizadoBs;
    }

    public BigDecimal getAdelantoAutorizadoUs() {
        return adelantoAutorizadoUs;
    }

    public void setAdelantoAutorizadoUs(BigDecimal adelantoAutorizadoUs) {
        this.adelantoAutorizadoUs = adelantoAutorizadoUs;
    }

    public String getCantAdelantoAutorizado() {
        return cantAdelantoAutorizado;
    }

    public void setCantAdelantoAutorizado(String cantAdelantoAutorizado) {
        this.cantAdelantoAutorizado = cantAdelantoAutorizado;
    }

    public BigDecimal getAjustesBs() {
        return ajustesBs;
    }

    public void setAjustesBs(BigDecimal ajustesBs) {
        this.ajustesBs = ajustesBs;
    }

    public BigDecimal getAjustesUs() {
        return ajustesUs;
    }

    public void setAjustesUs(BigDecimal ajustesUs) {
        this.ajustesUs = ajustesUs;
    }

    public BigDecimal getCargosBs() {
        return cargosBs;
    }

    public void setCargosBs(BigDecimal cargosBs) {
        this.cargosBs = cargosBs;
    }

    public BigDecimal getPagoBs() {
        return pagoBs;
    }

    public void setPagoBs(BigDecimal pagoBs) {
        this.pagoBs = pagoBs;
    }

    public String getNivelMora() {
        return nivelMora;
    }

    public void setNivelMora(String nivelMora) {
        this.nivelMora = nivelMora;
    }

    public String getEspacio() {
        return espacio;
    }

    public void setEspacio(String espacio) {
        this.espacio = espacio;
    }
}
