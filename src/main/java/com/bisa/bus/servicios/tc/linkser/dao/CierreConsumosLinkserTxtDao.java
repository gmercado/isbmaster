package com.bisa.bus.servicios.tc.linkser.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.linkser.entities.CierreConsumosLinkserTxt;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.io.Serializable;

/**
 * Created by atenorio on 16/11/2017.
 */
public class CierreConsumosLinkserTxtDao extends DaoImpl<CierreConsumosLinkserTxt, String> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(CierreConsumosLinkserTxtDao.class);

    @Inject
    protected CierreConsumosLinkserTxtDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean limpiarTodo() {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<CierreConsumosLinkserTxt> query = entityManager.createQuery("DELETE FROM CierreConsumosLinkserTxt");
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }
}
