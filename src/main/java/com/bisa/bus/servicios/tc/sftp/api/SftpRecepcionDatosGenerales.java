package com.bisa.bus.servicios.tc.sftp.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.interfaces.as400.dao.TipoAtt;
import bus.interfaces.as400.dao.ValorAttDao;
import bus.interfaces.as400.entities.ValorAtt;
import bus.plumbing.csv.LecturaCSV;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.tc.sftp.dao.DatosGeneralesDao;
import com.bisa.bus.servicios.tc.sftp.dao.DatosGeneralesHistoricoDao;
import com.bisa.bus.servicios.tc.sftp.entities.DatosGeneralesHistoricoTC;
import com.bisa.bus.servicios.tc.sftp.entities.DatosGeneralesTC;
import com.bisa.bus.servicios.tc.sftp.entities.IdDatosGenerales;
import com.bisa.bus.servicios.tc.sftp.model.*;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgProgramaTarjetaCredito;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by atenorio on 09/06/2017.
 */
public class SftpRecepcionDatosGenerales implements Serializable {

    private static final long serialVersionUID = -7112978535165279100L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpRecepcionDatosGenerales.class);

    private final MedioAmbiente medioAmbiente;
    private final DatosGeneralesDao datosGeneralesDao;
    private final DatosGeneralesHistoricoDao datosGeneralesHistoricoDao;
    private final RpgProgramaTarjetaCredito rpgPrograma;
    private final NotificacionesCorreo notificacionesCorreo;
    private final ValorAttDao valorAttDao;

    SftpConfiguracion configuracion;
    SftpTransferencia sftpTransferencia;
    private String nombreArchivoProcesado = "";

    @Inject
    public SftpRecepcionDatosGenerales(MedioAmbiente medioAmbiente,
                                       DatosGeneralesDao datosGeneralesDao, DatosGeneralesHistoricoDao datosGeneralesHistoricoDao,
                                       NotificacionesCorreo notificacionesCorreo, RpgProgramaTarjetaCredito rpgPrograma,
                                       SftpConfiguracion configuracion, SftpTransferencia sftpTransferencia,
                                       ValorAttDao valorAttDao) {
        this.medioAmbiente = medioAmbiente;
        this.datosGeneralesDao = datosGeneralesDao;
        this.rpgPrograma = rpgPrograma;
        this.datosGeneralesHistoricoDao = datosGeneralesHistoricoDao;
        this.notificacionesCorreo = notificacionesCorreo;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
        this.valorAttDao = valorAttDao;
    }

    /**
     * Metodo para procesar generacion de archivo y envio de archivo a servidor
     * remoto SFTP.
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    String PREF = "#fecha";

    public boolean procesarArchivo() {
        String detalle = "";
        boolean ok = true;
        LOGGER.debug("Inicia proceso de envio de archivo de solicitud");
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_BALANCE_077, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_BALANCE_077_DEFAULT);
        String nombreArchivoRegexForzado = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_BALANCE_077_FORZADO, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_BALANCE_077_FORZADO_DEFAULT);
        String nombreArchivo[] = Iterables.toArray(Splitter.on("|").trimResults().split(nombreArchivoRegex), String.class);

        // Procesar a solicitud fecha de pago
        String fechaProcesoTexto = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_ATC, Variables.FECHA_DE_PROCESO_ATC_DEFAULT);
        // Calcular fecha de un anterior
        Calendar hoy = Calendar.getInstance();
        // No disminuir un dia debido a que el archivo normalmente se generara con la fecha de proceso de sistema
        // no la fecha de proceso de negocio.
        Date fechaProcesar = hoy.getTime();

        if (StringUtils.isEmpty(fechaProcesoTexto) || "0".equals(fechaProcesoTexto)) {
            fechaProcesoTexto = FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar);
        } else {
            fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProcesoTexto);
            if (fechaProcesar == null) {
                notificacionesCorreo.notificarError("Error en fecha de proceso recepci\u00F3n datos generales", "");
                return false;
            }
        }

        LOGGER.info("Fecha de proceso: {}, {}", fechaProcesar, fechaProcesoTexto);
        nombreArchivoRegex = nombreArchivo[0].replace(PREF, new SimpleDateFormat(nombreArchivo[1]).format(fechaProcesar));
        if (!StringUtils.isEmpty(nombreArchivoRegexForzado) && !"NULL".equals(nombreArchivoRegexForzado)) {
            nombreArchivoRegex = nombreArchivoRegexForzado;
        }

        LOGGER.info("Descargar archivo de servidor SFTP con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_BALANCE_ATC, Variables.FTP_RUTA_RECEPCION_BALANCE_ATC_DEFAULT));
        sftpTransferencia.configurar(configuracion);

        ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.MOV);
        LOGGER.info("Descarga completa:" + ok);

        if (!ok || sftpTransferencia.getArchivoLocal() == null) {
            // Cerrar el registro con error y permitir reintento
            detalle = "Problema en transferencia o no existen archivos de respuesta en servidor SFTP.";
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.MOV, detalle);
            LOGGER.warn(detalle);
            return false;
        }

        // Procesamiento de archivo local
        File archivo = sftpTransferencia.getArchivoLocal();
        List<DatosGeneralesCvs> lista = mapearArchivo(archivo);
        if (lista == null || lista.size() == 0) {
            detalle = "Error en mapeo de archivo de respuesta o archivo vac\u00EDo";
            LOGGER.warn(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.MOV, detalle);
            return false;
        }
        int fila = 0;
        for (DatosGeneralesCvs dg : lista) {
            // Validar que se realizo el mapeo de forma exitosa de la estructura de pago
            fila++;
            if (dg == null) {
                detalle = "Error en mapeo de una estructura se requiere revisar el archivo de respuesta.";
                LOGGER.error(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.MOV, detalle);
                return false;
            }
            String permitirValidar = medioAmbiente.getValorDe(Variables.FTP_VALIDAR_RECEPCION_ARCHIVO_ATC, Variables.FTP_VALIDAR_RECEPCION_ARCHIVO_ATC_DEFAULT);
            if("true".equals(StringUtils.trimToEmpty(permitirValidar))) {
                detalle = validacion(dg, fila);
                if (StringUtils.isNotEmpty(detalle)) {
                    LOGGER.error(detalle);
                    sftpTransferencia.transferenciaConErrorValidacion(detalle);
                    notificacionesCorreo.notificarErrorTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.CIE, detalle);
                    return false;
                }
            }
        }

        LOGGER.debug("Actualizar tabla de datos generales");
        LOGGER.debug("Limpieza de tabla de datos generales.");
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();

        // Limpiar tablas temporales
        if (!datosGeneralesDao.limpiarTodo()) {
            detalle = "Error al limpiar tabla temporal de datos generales.";
            LOGGER.error(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.MOV, detalle);
            return false;
        }
        int totalRegistros = 0;
        LOGGER.debug("Carga de datos generales tabla intermedia.");
        // Verificar consistencia de registro
        for (DatosGeneralesCvs dg : lista) {
            totalRegistros++;
            IdDatosGenerales id = new IdDatosGenerales();
            id.setCodigoAdm("A");
            id.setFechaProceso(FormatosUtils.fechaYYYYMMDD(fechaProcesar).intValue());
            id.setNumeroCuenta(dg.getNumeroCuenta());

            // Validar que se tenga pendiente de respuesta a pagos
            DatosGeneralesTC datosGeneralesTC = transferObject.convert(dg, DatosGeneralesTC.class);
            datosGeneralesTC.setEstado(EstadoPagosTC.GENE.name());
            datosGeneralesTC.setImpBalanceLiqBsSigno(getSigno(datosGeneralesTC.getImpBalanceLiqBs()));
            datosGeneralesTC.setImpBalanceLiqBsTexto(getNumero(datosGeneralesTC.getImpBalanceLiqBs(), 10));
            datosGeneralesTC.setImpBalanceLiqUsSigno(getSigno(datosGeneralesTC.getImpBalanceLiqUs()));
            datosGeneralesTC.setImpBalanceLiqUsTexto(getNumero(datosGeneralesTC.getImpBalanceLiqUs(), 10));
            datosGeneralesTC.setPagoMinimoSigno(getSigno(datosGeneralesTC.getPagoMinimo()));
            datosGeneralesTC.setPagoMinimoTexto(getNumero(datosGeneralesTC.getPagoMinimo(), 10));
            datosGeneralesTC.setTotalInteresesSigno(getSigno(datosGeneralesTC.getTotalIntereses()));
            datosGeneralesTC.setTotalInteresesTexto(getNumero(datosGeneralesTC.getTotalIntereses(), 10));
            datosGeneralesTC.setConsumoBsSigno(getSigno(datosGeneralesTC.getConsumoBs()));
            datosGeneralesTC.setConsumoBsTexto(getNumero(datosGeneralesTC.getConsumoBs(), 10));
            datosGeneralesTC.setConsumoUsSigno(getSigno(datosGeneralesTC.getConsumoUs()));
            datosGeneralesTC.setConsumoUsTexto(getNumero(datosGeneralesTC.getConsumoUs(), 10));
            datosGeneralesTC.setAdelantoBsSigno(getSigno(datosGeneralesTC.getAdelantoBs()));
            datosGeneralesTC.setAdelantoBsTexto(getNumero(datosGeneralesTC.getAdelantoBs(), 10));
            datosGeneralesTC.setAdelantoUsSigno(getSigno(datosGeneralesTC.getAdelantoUs()));
            datosGeneralesTC.setAdelantoUsTexto(getNumero(datosGeneralesTC.getAdelantoUs(), 10));
            datosGeneralesTC.setConsumoAutorizadoBsSigno(getSigno(datosGeneralesTC.getConsumoAutorizadoBs()));
            datosGeneralesTC.setConsumoAutorizadoBsTexto(getNumero(datosGeneralesTC.getConsumoAutorizadoBs(), 10));
            datosGeneralesTC.setConsumoAutorizadoUsSigno(getSigno(datosGeneralesTC.getConsumoAutorizadoUs()));
            datosGeneralesTC.setConsumoAutorizadoUsTexto(getNumero(datosGeneralesTC.getConsumoAutorizadoUs(), 10));
            datosGeneralesTC.setAdelantoAutorizadoBsSigno(getSigno(datosGeneralesTC.getAdelantoAutorizadoBs()));
            datosGeneralesTC.setAdelantoAutorizadoBsTexto(getNumero(datosGeneralesTC.getAdelantoAutorizadoBs(), 10));
            datosGeneralesTC.setAdelantoAutorizadoUsSigno(getSigno(datosGeneralesTC.getAdelantoAutorizadoUs()));
            datosGeneralesTC.setAdelantoAutorizadoUsTexto(getNumero(datosGeneralesTC.getAdelantoAutorizadoUs(), 10));
            datosGeneralesTC.setAjustesBsSigno(getSigno(datosGeneralesTC.getAjustesBs()));
            datosGeneralesTC.setAjustesBsTexto(getNumero(datosGeneralesTC.getAjustesBs(), 10));
            datosGeneralesTC.setAjustesUsSigno(getSigno(datosGeneralesTC.getAjustesUs()));
            datosGeneralesTC.setAjustesUsTexto(getNumero(datosGeneralesTC.getAjustesUs(), 10));
            datosGeneralesTC.setCargosBsSigno(getSigno(datosGeneralesTC.getCargosBs()));
            datosGeneralesTC.setCargosBsTexto(getNumero(datosGeneralesTC.getCargosBs(), 10));
            datosGeneralesTC.setPagoBsSigno(getSigno(datosGeneralesTC.getPagoBs()));
            datosGeneralesTC.setPagoBsTexto(getNumero(datosGeneralesTC.getPagoBs(), 10));

            datosGeneralesTC.setFechaRecepcion(new Date());
            datosGeneralesTC.setIdArchivoRecepcion(sftpTransferencia.getFtpArchivoTransferencia().getId());
            nombreArchivoProcesado = sftpTransferencia.getFtpArchivoTransferencia().getArchivo();
            // Registrar
            datosGeneralesTC.setIdDatosGenerales(id);
            ok = ok && datosGeneralesDao.guardar(datosGeneralesTC, "ISB");

            DatosGeneralesHistoricoTC datosGeneralesHistoricoTC = transferObject.convert(datosGeneralesTC, DatosGeneralesHistoricoTC.class);
            datosGeneralesHistoricoTC.setIdDatosGenerales(datosGeneralesTC.getIdDatosGenerales());
            ok = ok && datosGeneralesHistoricoDao.guardar(datosGeneralesHistoricoTC, "ISB");
        }

        if (ok) {
            detalle = "Proceso de carga de archivo completado con " + totalRegistros + " registros.";
            sftpTransferencia.transferenciaCompleta(detalle);
            notificacionesCorreo.notificarCorrectaTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.MOV, detalle);
        } else {
            detalle = "Problema en proceso de carga de archivo, almacenamiento en DB.";
            sftpTransferencia.transferenciaConErrorInterno(detalle);
            notificacionesCorreo.notificarErrorTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.MOV, detalle);
        }
        LOGGER.debug("Completado proceso de carga de archivo {} de datos generales.", sftpTransferencia.getNombreArchivoRemoto());
        return ok;
    }


    public List<DatosGeneralesCvs> mapearArchivo(File archivo) {
        LOGGER.info("Ruta local de archivo a procesar:{}", archivo.getAbsolutePath());
        FileInputStream is = null;
        BufferedReader br = null;
        List<DatosGeneralesCvs> lista = new ArrayList<DatosGeneralesCvs>();
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();
        try {
            is = new FileInputStream(archivo);
            String codificacion = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC, Variables.FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC_DEFAULT);
            br = new BufferedReader(new InputStreamReader(is, codificacion));
            String linea = br.readLine();
            List<String> lineas = new ArrayList<String>();

            while (linea != null) {
                LOGGER.debug("Detalle de registro recuperado: {}", linea);
                if (StringUtils.isNotEmpty(linea) && linea.length() > 1) {
                    DatosGeneralesCvs dg = LecturaCSV.procesar(DatosGeneralesCvs.class, linea, "|");
                    lista.add(dg);
                }
                linea = br.readLine();
            }
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al procesar el archivo "
                    + archivo.getName() + ".", e);
            return null;
        } finally {
            IOUtils.closeQuietly(is);
        }
        LOGGER.info("Registros identificados en archivo {}.", lista.size());
        return lista;
    }

    public Character getSigno(BigDecimal numero) {
        if (numero == null) return new Character(' ');
        return (numero.doubleValue() < 0) ? new Character('-') : new Character('+');
    }

    public String getNumero(BigDecimal numero, int size) {
        if (numero == null) numero = new BigDecimal(0);
        DecimalFormat decimalFormat = new DecimalFormat("##.00");
        String valor = decimalFormat.format(numero);
        valor = valor.replaceAll("\\.", "");
        valor = valor.replace(",", "");
        valor = valor.replace("-", "");
        valor = StringUtils.leftPad(valor, size, "0");
        return valor;
    }

    public String getNombreArchivoProcesado() {
        return nombreArchivoProcesado;
    }

    public void setNombreArchivoProcesado(String nombreArchivoProcesado) {
        this.nombreArchivoProcesado = nombreArchivoProcesado;
    }

    private String validacion(DatosGeneralesCvs extracto, int fila) {
        if (StringUtils.isNotEmpty(extracto.getCodigoBanco())) {
            List<ValorAtt> lista = valorAttDao.getListaDe(TipoAtt.TARJETA_CREDITO_EMISOR);
            if (lista != null && lista.size() > 0 &&
                    !extracto.getCodigoBanco().equals(StringUtils.trimToEmpty(lista.get(0).getValor()))) {
                LOGGER.warn("Error en validación registro: {}", extracto);
                return "Error de validaci\u00F3n con c\u00F3digo de banco incorrecto en fila " + fila + ".";
            }
        }
        return "";
    }
}
