package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;
import javax.persistence.Basic;
import java.math.BigDecimal;

/**
 * Created by atenorio on 18/05/2017.
 * Tipo de esctructura CC
 */
public class SolicitudCondicionesComerciales {
    @Basic(optional = false)
    @FormatCsv(name = "SOCCTIRE", nullable = false, length = 2)
    private String socctire;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @FormatCsv(name = "SOCCLICR", nullable = false, pattern = "##.000000", scale = 6, length = 18, fillSpace = "0") //precision = 18, scale = 6
    private BigDecimal socclicr;
    @Basic(optional = false)
    @FormatCsv(name = "SOCCMOLI", nullable = false, length = 4, fillSpace = "0")
    private short soccmoli;
    @Basic(optional = false)
    @FormatCsv(name = "SOCCFECI", nullable = false, length = 4, fillSpace = "0")
    private short soccfeci;
    @Basic(optional = false)
    @FormatCsv(name = "SOCCFOPA", nullable = false, length = 1)
    private short soccfopa;
    @Basic(optional = false)
    @FormatCsv(name = "SOCCTIDE", nullable = false, length = 1)
    private short socctide;
    @Basic(optional = false)
    @FormatCsv(name = "FILL", nullable = false, length = 367)
    private String soccfill="";
    @FormatCsv(name = "SOCCCOER", nullable = false, length = 3)
    private String socccoer;

    public String getSocctire() {
        return socctire;
    }

    public void setSocctire(String socctire) {
        this.socctire = socctire;
    }

    public BigDecimal getSocclicr() {
        return socclicr;
    }

    public void setSocclicr(BigDecimal socclicr) {
        this.socclicr = socclicr;
    }

    public short getSoccmoli() {
        return soccmoli;
    }

    public void setSoccmoli(short soccmoli) {
        this.soccmoli = soccmoli;
    }

    public short getSoccfeci() {
        return soccfeci;
    }

    public void setSoccfeci(short soccfeci) {
        this.soccfeci = soccfeci;
    }

    public short getSoccfopa() {
        return soccfopa;
    }

    public void setSoccfopa(short soccfopa) {
        this.soccfopa = soccfopa;
    }

    public short getSocctide() {
        return socctide;
    }

    public void setSocctide(short socctide) {
        this.socctide = socctide;
    }

    public String getSocccoer() {
        return socccoer;
    }

    public void setSocccoer(String socccoer) {
        this.socccoer = socccoer;
    }

    public String getSoccfill() {
        return soccfill;
    }

    public void setSoccfill(String soccfill) {
        this.soccfill = soccfill;
    }
}
