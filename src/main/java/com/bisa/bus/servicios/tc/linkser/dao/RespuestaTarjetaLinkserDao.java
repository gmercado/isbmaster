package com.bisa.bus.servicios.tc.linkser.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.linkser.entities.RespuestaSolicitudTarjetaLinkser;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.io.Serializable;

/**
 * Created by atenorio on 14/11/2017.
 */
public class RespuestaTarjetaLinkserDao  extends DaoImpl<RespuestaSolicitudTarjetaLinkser, String> implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(RespuestaTarjetaLinkserDao.class);

    @Inject
    protected RespuestaTarjetaLinkserDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(RespuestaSolicitudTarjetaLinkser cuenta, String user) {
        persist(cuenta);
        return true;
    }

    public boolean limpiarTodo() {
        LOGGER.debug("Limpiando tabla RespuestaSolicitudTarjetaLinkser.");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<RespuestaSolicitudTarjetaLinkser> query = entityManager.createQuery("DELETE FROM RespuestaSolicitudTarjetaLinkser");
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }
}
