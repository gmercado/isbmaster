package com.bisa.bus.servicios.tc.sftp.ui;

import bus.database.components.FiltroPanel;
import com.bisa.bus.servicios.tc.sftp.entities.SolicitudTCEmpresarial;
import com.bisa.bus.servicios.tc.sftp.model.EstadoSolicitudTC;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by atenorio on 06/12/2017.
 */
public class ListaSolicitudEmpresarialFiltroPanel extends FiltroPanel<SolicitudTCEmpresarial> {

    protected ListaSolicitudEmpresarialFiltroPanel(String id, IModel<SolicitudTCEmpresarial> model1, IModel<SolicitudTCEmpresarial> model2) {
        super(id, model1, model2);

        final String datePattern = "dd/MM/yyyy";
        DateTextField fechaDesde = new DateTextField("fechaDesde", new PropertyModel<>(model1, "fechaCreacion"), datePattern);
        fechaDesde.setRequired(true);
        fechaDesde.add(new DatePicker());

        DateTextField fechaHasta = new DateTextField("fechaHasta", new PropertyModel<>(model2, "fechaCreacion"), datePattern);
        fechaHasta.setRequired(true);
        fechaHasta.add(new DatePicker());
        add(new TextField<>("solicitud", new PropertyModel<>(model1, "sonsol")));
        add(new TextField<>("cuenta", new PropertyModel<>(model1, "soganutcnc")));
        add(fechaDesde);
        add(fechaHasta);

        add(new DropDownChoice<>("estado", new PropertyModel<>(model1, "estado"),
                new LoadableDetachableModel<List<? extends EstadoSolicitudTC>>() {
                    @Override
                    protected List<? extends EstadoSolicitudTC> load() {
                        return new ArrayList<>(Arrays.asList((EstadoSolicitudTC.values())));
                    }
                }, new IChoiceRenderer<EstadoSolicitudTC>() {
            @Override
            public Object getDisplayValue(EstadoSolicitudTC object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(EstadoSolicitudTC object, int index) {
                return Integer.toString(index);
            }

            @Override
            public EstadoSolicitudTC getObject(String id, IModel<? extends List<? extends EstadoSolicitudTC>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));
    }
}
