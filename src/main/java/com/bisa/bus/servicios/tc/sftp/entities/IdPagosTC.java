package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created by atenorio on 31/05/2017.
 */
@Embeddable
public class IdPagosTC implements Serializable{
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "PATRN", nullable = false, length = 20)
    private String transaccion;

    @Basic(optional = false)
    @Column(name = "PANUCUEN", nullable = false)
    private BigInteger cuentaTarjeta;

    public BigInteger getCuentaTarjeta() {
        return cuentaTarjeta;
    }

    public void setCuentaTarjeta(BigInteger cuentaTarjeta) {
        this.cuentaTarjeta = cuentaTarjeta;
    }

    public String getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(String transaccion) {
        this.transaccion = transaccion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdPagosTC that = (IdPagosTC) o;

        if (transaccion != that.transaccion) return false;
        if (cuentaTarjeta != that.cuentaTarjeta) return false;
        return true;

    }

    @Override
    public int hashCode() {
        int result = transaccion.hashCode();
        result = 31 * result + cuentaTarjeta.hashCode();
        return result;
    }


}
