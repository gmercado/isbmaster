package com.bisa.bus.servicios.tc.sftp.model;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by atenorio on 17/05/2017.
 */
public enum EstadoSolicitudTC {
    GENE("Generado"),
    ENVI("Enviado a proceso"),
    ERRV("Error de validaci\u00F3n"),
    ERRI("Error en interno"),
    ERRE("Error en envio"),
    ERRP("Error en proceso"),
    PROC("Proceso completado");
    private EstadoSolicitudTC(String descripcion) {
        this.descripcion = descripcion;
    }


    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public static EstadoSolicitudTC valorEnum(String valor) {
        if(StringUtils.isEmpty(valor)) valor="";
        List<EstadoSolicitudTC> estados = new ArrayList<>(Arrays.asList((EstadoSolicitudTC.values())));
        for (EstadoSolicitudTC es : estados) {
            if (valor.equals(es.name())){
                return es;
            }
        }
        return null;
    }

    public boolean equals(EstadoSolicitudTC estado) {
        if ((this.descripcion == null && estado.descripcion != null) || (this.descripcion != null && !this.descripcion.equals(estado.descripcion))) {
            return false;
        }
        return true;
    }
}
