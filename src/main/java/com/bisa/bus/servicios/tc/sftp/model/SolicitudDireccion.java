package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import javax.persistence.Basic;


/**
 * Created by atenorio on 18/05/2017.
 */
public class SolicitudDireccion {

    @Basic(optional = false)
    @FormatCsv(name = "SOADTIREP", nullable = false, length = 2)
    private String soadtirep;
    @Basic(optional = false)
    @FormatCsv(name = "SOADTIDICP", nullable = false, length = 1)
    private short soadtidicp;
    // TODO ATC ejemplo con codigo de calle en blanco
    @Basic(optional = false)
    @FormatCsv(name = "SOADTICAP", nullable = false, length = 4)
    private short soadticap;
    @Basic(optional = false)
    @FormatCsv(name = "SOADCALLEP", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadcallep;
    @Basic(optional = false)
    @FormatCsv(name = "SOADNUPUP", nullable = false, length = 10, align = FormatCsv.alignType.LEFT)
    private String soadnupup;
    @Basic(optional = false)
    @FormatCsv(name = "SOADINADP", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadinadp;
    @Basic(optional = false)
    @FormatCsv(name = "SOADCOPOP", nullable = false, length = 6)
    private String soadcopop;
    @Basic(optional = false)
    @FormatCsv(name = "SOADZONAP", nullable = false, length = 60, align = FormatCsv.alignType.LEFT)
    private String soadzonap;
    @Basic(optional = false)
    @FormatCsv(name = "SOADPAISP", nullable = false, length = 3, fillSpace = "0")
    private short soadpaisp;
    @Basic(optional = false)
    @FormatCsv(name = "SOADDPTOP", nullable = false, length = 5, align = FormatCsv.alignType.LEFT)
    private String soaddptop;
    @Basic(optional = false)
    @FormatCsv(name = "SOADLOCP", nullable = false, length = 9, fillSpace = "0")
    private int soadlocp;
    @Basic(optional = false)
    @FormatCsv(name = "SOADCOTEPP", nullable = false, length = 3, fillSpace = "0")
    private short soadcotepp;

    @Basic(optional = false)
    @FormatCsv(name = "SOADCOTEAP", nullable = false, length = 5, fillSpace = "0")
    private int soadcoteap;
    @Basic(optional = false)
    @FormatCsv(name = "SOADTELP", nullable = false, length = 12, fillSpace = "0")
    private long soadtelp;
    @Basic(optional = false)
    @FormatCsv(name = "SOADTEINP", nullable = false, length = 6, fillSpace = "0")
    private int soadteinp;
    @Basic(optional = false)
    @FormatCsv(name = "SOADCOCEPP", nullable = false, length = 3, fillSpace = "0")
    private short soadcocepp;
    @Basic(optional = false)
    @FormatCsv(name = "SOADCELP", nullable = false, length = 12, fillSpace = "0")
    private long soadcelp;
    @FormatCsv(name = "FILL", nullable = false, length = 56)
    private String soadfill="";
    @Basic(optional = false)
    @FormatCsv(name = "SOADCOERP", nullable = false, length = 3)
    private String soadcoerp;

    public String getSoadtirep() {
        return soadtirep;
    }

    public void setSoadtirep(String soadtirep) {
        this.soadtirep = soadtirep;
    }

    public short getSoadtidicp() {
        return soadtidicp;
    }

    public void setSoadtidicp(short soadtidicp) {
        this.soadtidicp = soadtidicp;
    }

    public short getSoadticap() {
        return soadticap;
    }

    public void setSoadticap(short soadticap) {
        this.soadticap = soadticap;
    }

    public String getSoadcallep() {
        return soadcallep;
    }

    public void setSoadcallep(String soadcallep) {
        this.soadcallep = soadcallep;
    }

    public String getSoadnupup() {
        return soadnupup;
    }

    public void setSoadnupup(String soadnupup) {
        this.soadnupup = soadnupup;
    }

    public String getSoadinadp() {
        return soadinadp;
    }

    public void setSoadinadp(String soadinadp) {
        this.soadinadp = soadinadp;
    }

    public String getSoadcopop() {
        return soadcopop;
    }

    public void setSoadcopop(String soadcopop) {
        this.soadcopop = soadcopop;
    }

    public String getSoadzonap() {
        return soadzonap;
    }

    public void setSoadzonap(String soadzonap) {
        this.soadzonap = soadzonap;
    }

    public short getSoadpaisp() {
        return soadpaisp;
    }

    public void setSoadpaisp(short soadpaisp) {
        this.soadpaisp = soadpaisp;
    }

    public String getSoaddptop() {
        return soaddptop;
    }

    public void setSoaddptop(String soaddptop) {
        this.soaddptop = soaddptop;
    }

    public int getSoadlocp() {
        return soadlocp;
    }

    public void setSoadlocp(int soadlocp) {
        this.soadlocp = soadlocp;
    }

    public short getSoadcotepp() {
        return soadcotepp;
    }

    public void setSoadcotepp(short soadcotepp) {
        this.soadcotepp = soadcotepp;
    }

    public int getSoadcoteap() {
        return soadcoteap;
    }

    public void setSoadcoteap(int soadcoteap) {
        this.soadcoteap = soadcoteap;
    }

    public long getSoadtelp() {
        return soadtelp;
    }

    public void setSoadtelp(long soadtelp) {
        this.soadtelp = soadtelp;
    }

    public int getSoadteinp() {
        return soadteinp;
    }

    public void setSoadteinp(int soadteinp) {
        this.soadteinp = soadteinp;
    }

    public short getSoadcocepp() {
        return soadcocepp;
    }

    public void setSoadcocepp(short soadcocepp) {
        this.soadcocepp = soadcocepp;
    }

    public long getSoadcelp() {
        return soadcelp;
    }

    public void setSoadcelp(long soadcelp) {
        this.soadcelp = soadcelp;
    }

    public String getSoadcoerp() {
        return soadcoerp;
    }

    public void setSoadcoerp(String soadcoerp) {
        this.soadcoerp = soadcoerp;
    }

    public String getSoadfill() {
        return soadfill;
    }

    public void setSoadfill(String soadfill) {
        this.soadfill = soadfill;
    }
}
