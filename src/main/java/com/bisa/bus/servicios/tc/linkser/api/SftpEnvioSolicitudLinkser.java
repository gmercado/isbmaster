package com.bisa.bus.servicios.tc.linkser.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.rpg.ImposibleLlamarProgramaRpgException;
import bus.plumbing.csv.GeneradorCSV;
import bus.plumbing.file.ArchivoBase;
import bus.plumbing.file.ArchivoProfile;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.tc.linkser.dao.SolicitudTcLinkserTxtDao;
import com.bisa.bus.servicios.tc.linkser.entities.SolicitudTcLinkserTxt;
import com.bisa.bus.servicios.tc.sftp.api.SftpTransferencia;
import com.bisa.bus.servicios.tc.sftp.entities.FtpArchivoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.bisa.bus.servicios.tc.sftp.model.TipoCodigoError;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgProgramaTarjetaCredito;
import com.google.inject.Inject;
import com.ibm.as400.access.ConnectionPoolException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

/**
 * Created by atenorio on 09/11/2017.
 */
public class SftpEnvioSolicitudLinkser {
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpEnvioSolicitudLinkser.class);

    private final MedioAmbiente medioAmbiente;
    private final ArchivoBase archivoBase;
    private final SolicitudTcLinkserTxtDao iSolicitudTcLinkserTxtDao;

    private final RpgProgramaTarjetaCredito iRpgCargarSolicitud;
    private final NotificacionesCorreo notificacionesCorreo;
    ISftpConfiguracionLinkser configuracion;
    SftpTransferencia sftpTransferencia;

    @Inject
    public SftpEnvioSolicitudLinkser(ArchivoBase archivoBase, MedioAmbiente medioAmbiente,
                                     SolicitudTcLinkserTxtDao iSolicitudTcLinkserTxtDao,
                                     RpgProgramaTarjetaCredito iRpgCargarSolicitud, NotificacionesCorreo notificacionesCorreo,
                                     ISftpConfiguracionLinkser configuracion, SftpTransferencia sftpTransferencia) {
        this.medioAmbiente = medioAmbiente;
        this.archivoBase = archivoBase;
        this.iSolicitudTcLinkserTxtDao = iSolicitudTcLinkserTxtDao;
        this.iRpgCargarSolicitud = iRpgCargarSolicitud;
        this.notificacionesCorreo = notificacionesCorreo;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para generar y enviar archivo de solicitud de alta de tarjetas linkser de servidor remoto SFTP.
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    public boolean procesarArchivo() {
        boolean ok = false;
        LOGGER.debug("Inicia proceso de envio de archivo de solicitud");
        String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_LINKSER, Variables.FECHA_DE_PROCESO_LINKSER_DEFAULT);
        Date fechaProcesar = new Date();
        if (StringUtils.isEmpty(fechaProceso) || "0".equals(fechaProceso)) {
            fechaProceso = FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar);
        } else {
            fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
        }
        // Verificar fecha no valida
        if(fechaProcesar == null){
            LOGGER.error("Fecha no valida para proceso {}.", fechaProceso);
            return false;
        }

        // Verificar archivo a espera de ser enviado
        List<SolicitudTcLinkserTxt> solicitudes = iSolicitudTcLinkserTxtDao.getSolicitudesParaEnvio();
        String nombreArchivo = obtenerNombreArchivo(solicitudes);
        LOGGER.debug("1) Verificar si el archivo {} ya fue procesado.", nombreArchivo);
        // Verificar nombre de archivo no vacio o fecha invalida
        if (StringUtils.isEmpty(nombreArchivo)) {
            LOGGER.error("Fecha de archuvo nulo");
            return false;
        }

        // Verificar por nombre de archivo transferencia en estado PRTE, permitir reenvio en caso de estado reproceso o error de reintento
        if (!sftpTransferencia.verificarPermitirTransferencia(nombreArchivo, false)) {
            // Llamada a programa para preparar nuevo archivo de solicitud
            LOGGER.debug("2) Llamada a programa AS400, para cargar listado de solicitudes ha enviar.");
            try {
                ok = esCargaDeSolicitudes();
            } catch (Exception e) {
                LOGGER.error("Error al llamar programa AS400, generador de solicitudes aprobadas.", e);
                //sftpTransferencia.transferenciaConErrorInterno(TipoCodigoError.ERR_PROGRAMA.getDescripcion());
                return false;
            }
            // En caso de no existir solicitudes notificar a usuario
            if (!ok) {
                LOGGER.warn("Error al llamar programa AS400, generador de solicitudes aprobadas. Mensaje {}" + iRpgCargarSolicitud.getMensajeRespuesta());
                sftpTransferencia.transferenciaConErrorInterno(TipoCodigoError.ERR_PROGRAMA.getDescripcion());
                return false;
            }
            // Obtener nuevo nombre de archivo generado
            solicitudes = iSolicitudTcLinkserTxtDao.getSolicitudesParaEnvio();
            nombreArchivo = obtenerNombreArchivo(solicitudes);
            LOGGER.debug("3) Verificar nombre de archivo {} ya fue procesado.", nombreArchivo);
            // Verificar por nombre de archivo transferencia en estado PRTE, permitir reenvio en caso de estado reproceso o error de reintento
            if (!sftpTransferencia.verificarPermitirTransferencia(nombreArchivo)) {
                // Proceder con cargado de nuevos datos
                LOGGER.warn("No se permite procesamiento y transferencia de archivo {} se finaliza la tarea.", nombreArchivo);
                return false;
            }
        }

        sftpTransferencia.setFechaProceso(Integer.valueOf(FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar)));
        // Creacion de registro tipo solicitud
        FtpArchivoTransferencia ftpArchivoTransferencia = sftpTransferencia.crearRegistro(nombreArchivo, TipoArchivo.SLI);

        LOGGER.debug("4) Carga de archivo de solicitudes a servidor SFTP");

        LOGGER.debug("4.1) Recuperacion solicitudes pendientes de envio");

        if (solicitudes.size() > 2) {
            SolicitudTcLinkserTxt cabecera = solicitudes.get(0);
            SolicitudTcLinkserTxt pie = solicitudes.get(solicitudes.size() - 1);
            // Verificar cabecera y pie de archivo
            cabecera.setTexto(StringUtils.trimToEmpty(cabecera.getTexto()));
            if (cabecera == null || pie == null ||
                    !StringUtils.contains(cabecera.getTexto(), "HEADER") ||
                    !StringUtils.contains(pie.getTexto(), "TRAILER")) {
                LOGGER.warn("Problema en cabecera o pie de archivo a generar.");
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.SLI, "Problema en cabecera o pie de archivo a generar.");
                return false;
            }

            StringUtils.substring(cabecera.getTexto(), 7, 30);

            prepararConfiguracionSftp();
            // Creacion de archivo temporal para preparar envio
            String nombreArchivoLocal = nombreArchivo + "-" + UUID.randomUUID().toString();
            LOGGER.info("Nombre de archivo local: {}", nombreArchivoLocal);
            File archivo = sftpTransferencia.createFile(nombreArchivoLocal);

            ArchivoProfile archivoProfile = poblarArchivo(archivo, solicitudes);
            // Verificar si existio problema en la generacion de archivo
            if (archivoProfile == null) {
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.SLI, "Error en la generaci\u00F3n de archivo");
                return false;
            }
            LOGGER.debug("4.2) Creación local archivo de solicitudes con {} altas.", solicitudes.size());
            // Cargar archivo a servidor SFTP
            boolean cargado = sftpTransferencia.cargarArchivo(archivoProfile.getArchivo(), nombreArchivo, ftpArchivoTransferencia);

            // Modificar cambio de estado
            if (cargado) {
                LOGGER.debug("4.3) Cargando de archivo {} de solicitudes a servidor sftp finalizado correctamente.", nombreArchivo);
                TransferObject transferObject = new TransferObject();
                transferObject.initializate();
                // Reducir cabecera y pie en la revision
                int totalRegistros = solicitudes.size() - 2;

                // Envio de correspondencia
                StringBuffer sb = new StringBuffer();
                sb.append("Se ha generado el archivo de solicitud de altas para administradora LINKSER ");
                sb.append(" con ");
                sb.append(totalRegistros);
                sb.append(" solicitudes.");
                notificacionesCorreo.notificarCorrectaTransferenciaOperador(nombreArchivo, TipoArchivo.SLI, sb.toString());
                LOGGER.info("Completado proceso de carga de archivo {} de solicitudes a servidor SFTP de LINKSER correctamente.", nombreArchivo);
                ok = true;
            } else {
                LOGGER.debug("4.3) Cargando de archivo {} de solicitudes a servidor sftp finalizado incorrectamente.", nombreArchivo);
                notificacionesCorreo.notificarErrorTransferenciaOperador(nombreArchivo, TipoArchivo.SLI, "Error en carga de archivo");
                return false;
            }
            LOGGER.debug("4.4) Completado proceso de carga de archivo {} de solicitudes a servidor SFTP de LINKSER correctamente.", nombreArchivo);
        } else {
            sftpTransferencia.transferenciaCompleta("Sin solicitudes a enviar");
            StringBuffer sb = new StringBuffer();
            sb.append("\nNo se ha generado el archivo de solicitud de altas para administradora LINKSER ");
            sb.append(" por no tener solicitudes aprobadas por enviar.\n");
            notificacionesCorreo.notificarCorrectaTransferenciaOperador(nombreArchivo, TipoArchivo.SLI, sb.toString());
            LOGGER.debug("4.4) Sin solicitudes para enviar");
        }
        return ok;
    }


    public boolean prepararConfiguracionSftp() {
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_SOLICITUD_ALTAS_LINKSER, Variables.FTP_RUTA_SOLICITUD_ALTAS_LINKSER_DEFAULT));
        configuracion.setRemoteTemp(medioAmbiente.getValorDe(Variables.FTP_RUTA_TEMPORAL_ALTA_LINKSER, Variables.FTP_RUTA_TEMPORAL_ALTA_LINKSER_DEFAULT));
        return sftpTransferencia.configurar(configuracion);
    }

    private ArchivoProfile poblarArchivo(File archivo, List<SolicitudTcLinkserTxt> solicitudes) {
        if (archivo == null) return null;
        FileOutputStream archivoWrite;
        String codificacion = medioAmbiente.getValorDe(Variables.FTP_ENVIO_ARCHIVO_CODIFICACION_LINKSER, Variables.FTP_ENVIO_ARCHIVO_CODIFICACION_LINKSER_DEFAULT);
        try {
            archivoWrite = new FileOutputStream(archivo.getAbsoluteFile());
            BufferedWriter bufferWriter = new BufferedWriter(new OutputStreamWriter(archivoWrite, Charset.forName(codificacion)));
            StringBuilder sb = new StringBuilder();
            bufferWriter.write(sb.toString());
            // generando archivo persona natural
            for (SolicitudTcLinkserTxt sol : solicitudes) {
                TransferObject transferObject = new TransferObject();
                transferObject.initializate();
                // Grabar datos generales de cuenta
                GeneradorCSV.adicionarLinea(bufferWriter, sol.getTexto());
            }
            bufferWriter.close();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("DETALLE DEL ARCHIVO");
                FileReader filer = new FileReader(archivo);
                int valor = filer.read();
                while (valor != -1) {
                    LOGGER.debug("", valor);
                    valor = filer.read();
                }
            }

        } catch (IOException e) {
            LOGGER.error("Ocurrio un error inesperado al preparar ", e);
            return null;
        }
        return new ArchivoProfile(archivo, 0L, 0L);
    }

    public boolean esCargaDeSolicitudes() throws IllegalArgumentException, IllegalStateException {
        boolean result = false;
        try {
            String libreriaPrograma = medioAmbiente.getValorDe(Variables.LIBRERIA_TC_LINKSER, Variables.LIBRERIA_TC_LINKSER_DEFAULT);
            String programa = medioAmbiente.getValorDe(Variables.PROGRAMA_TC_SOLICITUD_LINKSER, Variables.PROGRAMA_TC_SOLICITUD_LINKSER_DEFAULT);
            result = iRpgCargarSolicitud.ejecutar(libreriaPrograma, programa);
            return result;
        } catch (ImposibleLeerRespuestaException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(Imposible leer respuesta)");
        } catch (IOException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(IOException)");
        } catch (TransaccionEfectivaException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(TransaccionEfectivaException)");
        } catch (SistemaCerradoException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(SistemaCerradoException)");
        } catch (ImposibleLlamarProgramaRpgException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(ImposibleLlamarProgramaRpgException)");
        } catch (ConnectionPoolException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(ConnectionPoolException)");
        } catch (TimeoutException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(Tiempo de espera agotado)");
        }
    }

    private String obtenerNombreArchivo(List<SolicitudTcLinkserTxt> solicitudes) {
        String nombreArchivo = "";
        SolicitudTcLinkserTxt cabecera = solicitudes.get(0);
        cabecera.setTexto(StringUtils.trimToEmpty(cabecera.getTexto()));
        if (StringUtils.isNotEmpty(cabecera.getTexto()) &&
                StringUtils.contains(cabecera.getTexto(), "HEADER")) {
            nombreArchivo = StringUtils.substring(cabecera.getTexto(), 7, 30);
        }
        return nombreArchivo;
    }
}