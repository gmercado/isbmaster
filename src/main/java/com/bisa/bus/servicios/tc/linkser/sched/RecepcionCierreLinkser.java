package com.bisa.bus.servicios.tc.linkser.sched;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.bus.servicios.tc.linkser.api.SftpCierreConsumosLinkser;
import com.bisa.bus.servicios.tc.linkser.api.SftpCierreOperaLinkser;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 16/11/2017.
 */
public class RecepcionCierreLinkser implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecepcionCierreLinkser.class);

    private final MedioAmbiente medioAmbiente;
    private final NotificacionesCorreo notificacionesCorreo;
    private final SftpCierreOperaLinkser iSftpCierreOperaLinkser;
    private final SftpCierreConsumosLinkser iSftpCierreConsumosLinkser;

    private volatile boolean ok;

    @Inject
    public RecepcionCierreLinkser(MedioAmbiente medioAmbiente, NotificacionesCorreo notificacionesCorreo,
                                  SftpCierreOperaLinkser iSftpCierreOperaLinkser,
                                  SftpCierreConsumosLinkser iSftpCierreConsumosLinkser){
        this.medioAmbiente = medioAmbiente;
        this.notificacionesCorreo = notificacionesCorreo;
        this.iSftpCierreOperaLinkser = iSftpCierreOperaLinkser;
        this.iSftpCierreConsumosLinkser = iSftpCierreConsumosLinkser;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        // Procesando operaciones
        LOGGER.info("Inicio de proceso recepcion operaciones de cierre");
        ok = iSftpCierreOperaLinkser.procesarArchivo();
        LOGGER.info("Respuesta de proceso recepcion operaciones de cierre: {}", ok);
        // Procesando consumos
        if(ok) {
            LOGGER.info("Inicio de proceso recepcion consumos");
            ok = iSftpCierreConsumosLinkser.procesarArchivo();
            LOGGER.info("Respuesta de proceso recepcion consumos: {}", ok);
        }

        // Completar proceso de carga notificando al operador
        if(ok){
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_CIERRE_MENSUAL_OPERADOR_LINKSER, Variables.FTP_MENSAJE_EMAIL_CIERRE_MENSUAL_OPERADOR_LINKSER_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador(iSftpCierreOperaLinkser.getNombreArchivoProcesado() + ", " + iSftpCierreConsumosLinkser.getNombreArchivoProcesado(), TipoArchivo.CLI, mensaje);
        }else{
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_CIERRE_MANSUAL_ERROR_OPERADOR_LINKSER, Variables.FTP_MENSAJE_EMAIL_CIERRE_MANSUAL_ERROR_OPERADOR_LINKSER_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador("", TipoArchivo.CLI, mensaje);
        }
    }
}