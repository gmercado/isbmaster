package com.bisa.bus.servicios.tc.sftp.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;

import com.bisa.bus.servicios.tc.sftp.dao.SolicitudTCEmpresarialDao;
import com.bisa.bus.servicios.tc.sftp.entities.SolicitudTCEmpresarial;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by atenorio on 01/12/2017.
 */
@AuthorizeInstantiation({RolesBisa.FTP_TRANSFERENCIA})
@Menu(value = "Env\u00EDo de Solicitudes Empresarial", subMenu = SubMenu.FTP)
public class ListadoSolicitudEmpresarial extends Listado<SolicitudTCEmpresarial, Integer> {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ListadoSolicitudEmpresarial.class);

    public ListadoSolicitudEmpresarial() {
        super();
    }

    @Override
    protected ListadoPanel<SolicitudTCEmpresarial, Integer> newListadoPanel(String id) {
        return new ListadoPanel<SolicitudTCEmpresarial, Integer>(id) {
            @Override
            protected List<IColumn<SolicitudTCEmpresarial, String>> newColumns(ListadoDataProvider<SolicitudTCEmpresarial, Integer> dataProvider) {
                List<IColumn<SolicitudTCEmpresarial, String>> columns = new LinkedList<>();
                columns.add(new LinkPropertyColumn<SolicitudTCEmpresarial>(Model.of("Solicitud"), "sonsol", "sonsol") {
                    @Override
                    protected void onClick(SolicitudTCEmpresarial object) {
                        PageParameters params  = new PageParameters();
                        params.add("solicitud", object.getSonsol());
                        setResponsePage(DetalleSolicitudEmpresarial.class, params);
                    }
                });

                columns.add(new PropertyColumn<SolicitudTCEmpresarial, String>(Model.of("Nro. Solicitud"), "sonsol", "sonsol"));

                columns.add(new PropertyColumn<>(Model.of("Nombre Cliente"), "socfnocl", "socfnocl"));

                columns.add(new PropertyColumn<>(Model.of("Apellido Cliente"), "socfapcl", "socfapcl"));

                columns.add(new PropertyColumn<>(Model.of("Doc Cliente"), "socfdocl", "socfdocl"));

                columns.add(new PropertyColumn<>(Model.of("Cta. Tar."), "soganutcnc", "soganutcnc"));

                columns.add(new PropertyColumn<>(Model.of("Cta. Tar. Banco"), "sonutc", "sonutc"));

                columns.add(new PropertyColumn<>(Model.of("Fecha env\u00EDo"), "sofeenv", "fechaEnvioFormato"));

                columns.add(new PropertyColumn<>(Model.of("Fecha recepci\u00F3n"), "soferes", "fechaRecepcionFormato"));

                columns.add(new PropertyColumn<SolicitudTCEmpresarial, String>(Model.of("Estado"), "soest", "estadoDescripcion") {
                    @Override
                    public void populateItem(Item<ICellPopulator<SolicitudTCEmpresarial>> item, String componentId, IModel<SolicitudTCEmpresarial> rowModel) {
                        SolicitudTCEmpresarial solicitud = rowModel.getObject();
                        item.add(new Label(componentId, solicitud.getEstadoDescripcion()).add(new AttributeAppender("style", Model.of("text-align:center"))));
                    }
                });

                columns.add(new PropertyColumn<>(Model.of("Fecha Proceso"), "fechaCreacion", "fechaProcesoFormato"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<SolicitudTCEmpresarial, Integer>> getProviderClazz() {
                return SolicitudTCEmpresarialDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<SolicitudTCEmpresarial> newFiltroPanel(String id, IModel<SolicitudTCEmpresarial> model1, IModel<SolicitudTCEmpresarial> model2) {
                return new ListaSolicitudEmpresarialFiltroPanel(id, model1, model2);
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("fechaCreacion", false);
            }

            @Override
            protected SolicitudTCEmpresarial newObject2() {
                return new SolicitudTCEmpresarial();
            }

            @Override
            protected SolicitudTCEmpresarial newObject1() {
                return new SolicitudTCEmpresarial();
            }

            @Override
            protected boolean isVisibleData() {
                SolicitudTCEmpresarial pago1 = getModel1().getObject();
                SolicitudTCEmpresarial pago2 = getModel2().getObject();
                return !((pago1.getFechaCreacion() == null || pago2.getFechaCreacion() == null));
            }
        };
    }
}