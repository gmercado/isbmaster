package com.bisa.bus.servicios.tc.sftp.sched;

import bus.env.api.MedioAmbiente;
import bus.mail.api.MailerFactory;
import com.bisa.bus.servicios.tc.sftp.api.SftpRecepcionPagos;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 03/05/2017.
 */
public class RecepcionPagos implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecepcionPagos.class);

    public static String NOMBRE_TAREA = "PagoEnvio-Hilo";
    private final MedioAmbiente medioAmbiente;
    private final MailerFactory mailerFactory;
    private final SftpRecepcionPagos sftpRecepcionPagos;

    private volatile boolean ok;

    @Inject
    public RecepcionPagos(MedioAmbiente medioAmbiente, MailerFactory mailerFactory, SftpRecepcionPagos sftpRecepcionPagos){
        this.medioAmbiente = medioAmbiente;
        this.mailerFactory = mailerFactory;
        this.sftpRecepcionPagos = sftpRecepcionPagos;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Realizando llamada a proceso");
        ok = sftpRecepcionPagos.procesarArchivo();
        LOGGER.info("Archivo procesado encontrado {}", ok);
    }
}
