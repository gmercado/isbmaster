package com.bisa.bus.servicios.tc.linkser.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.file.ArchivoBase;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.tc.linkser.dao.RespuestaCuentaLinkserDao;
import com.bisa.bus.servicios.tc.linkser.entities.RespuestaSolicitudCuentaLinkser;
import com.bisa.bus.servicios.tc.sftp.api.SftpTransferencia;
import com.bisa.bus.servicios.tc.sftp.entities.FtpArchivoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by atenorio on 14/11/2017.
 */
public class SftpRecepcionSolicitudCuentaLinkser implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpRecepcionSolicitudCuentaLinkser.class);


    private final MedioAmbiente medioAmbiente;
    private final ArchivoBase archivoBase;
    private final NotificacionesCorreo notificacionesCorreo;

    private final RespuestaCuentaLinkserDao iRespuestaCuentaDao;

    ISftpConfiguracionLinkser configuracion;
    SftpTransferencia sftpTransferencia;
    private String nombreArchivoProcesado = "";
    private String nombreArchivoRegex;
    FtpArchivoTransferencia archivoPendiente;
    boolean pendienteRespuesta = false;

    @Inject
    public SftpRecepcionSolicitudCuentaLinkser(ArchivoBase archivoBase,
                                               MedioAmbiente medioAmbiente,
                                               NotificacionesCorreo notificacionesCorreo,
                                               RespuestaCuentaLinkserDao iRespuestaCuentaDao,
                                               ISftpConfiguracionLinkser configuracion, SftpTransferencia sftpTransferencia) {
        this.medioAmbiente = medioAmbiente;
        this.archivoBase = archivoBase;
        this.notificacionesCorreo = notificacionesCorreo;
        this.iRespuestaCuentaDao = iRespuestaCuentaDao;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para procesar recepcion de archivo de respuesta a solicitud de alta de tarjetas de servidor remoto SFTP.
     * ISS00012CARDDI7xxxxx.OUT
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    String PREF = "#fecha";

    public boolean procesarArchivo() {
        boolean ok = false;
        String detalle = "";
        LOGGER.debug("Inicia proceso recepcion de archivo de solicitud");
        LOGGER.debug("1) Verificar archivo pendiente de respuesta");
        nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_LINKSER, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_LINKSER_DEFAULT);
        String nombreArchivoRegexForzado = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_LINKSER_FORZADO, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_LINKSER_FORZADO_DEFAULT);
        if (StringUtils.isEmpty(nombreArchivoRegexForzado) || "NULL".equals(nombreArchivoRegexForzado)) {
            nombreArchivoRegex = nombreArchivoRegexForzado;
        }
        // El proceso normal realiza la recepcion de archivo de alta de un dia anterior,
        // debido a la disponibilidad del archivo de respuesta.
        Calendar hoy = Calendar.getInstance();
        hoy.add(Calendar.DAY_OF_YEAR, -1);
        Date fechaProcesar = hoy.getTime();

        archivoPendiente = sftpTransferencia.getFtpArchivoTransferenciaPendiente(TipoArchivo.SLI);
        // Registrando que no existe respuesta pendiente
        if (archivoPendiente == null) {
            //TODO ATC
            //sftpTransferencia.transferenciaConErrorReintento("");
            //notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RLI, "No existen archivos de respuesta en servidor SFTP.");
            LOGGER.warn("No existen archivos pendientes de respuesta.");
            return false;
        }
        pendienteRespuesta = true;
        nombreArchivoRegex = archivoPendiente.getArchivo().replace(".IN", ".OUT");

        LOGGER.debug("2) Decargar archivo de servidor SFTP");
        prepararConfiguracionSftp();

        LOGGER.debug("2.1) Descarga de archivo de a servidor SFTP con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        sftpTransferencia.setFechaProceso(Integer.valueOf(FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar)));
        ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.RLI);
        // Verificar respuesta en carpeta de descarte
        if (!ok) {
            String remotePath = medioAmbiente.getValorDe(Variables.FTP_RUTA_DESCARTE_ALTAS_ATC, Variables.FTP_RUTA_DESCARTE_ALTAS_ATC_DEFAULT);
            ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.RLI);
            if (ok) {
                // Respuesta a proceso para permitir reintento para archivo de misma fecha
                // TODO ATC confirmar si lo mas conveniente es reintento en archivo errado
                detalle = "Archivo de respuesta descartado en servidor SFTP.";
                LOGGER.warn(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RLI, detalle);
                return false;
            }
        }

        LOGGER.debug("3) Validar archivo descargado");
        LOGGER.debug("3.1) Validar estructura");
        LOGGER.debug("3.2) Validar mapeo");

        // Cerrar el registro con error y permitir reintento
        if (!ok || sftpTransferencia.getArchivoLocal() == null) {
            sftpTransferencia.transferenciaConErrorReintento("");
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RLI, "No existen archivos de respuesta en servidor SFTP.");
            LOGGER.warn("No existen archivos de respuesta en servidor SFTP.");
            return false;
        }

        // Procesamiento de archivo local
        File archivo = sftpTransferencia.getArchivoLocal();
        List<String> mapeo = mapearArchivo(archivo);
        // Verificar si existen problemas en mape de archivo de respuesta
        if (mapeo == null || mapeo.size() == 0) {
            LOGGER.warn("Error en mapeo de archivo de respuesta");
            sftpTransferencia.transferenciaConErrorReintento("");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.RLI, "Error en mapeo de archivo de respuesta.");
            return false;
        }


        LOGGER.debug("4) Actualizar tabla de solicitudes");
        LOGGER.debug("4.1) Verificar si un registro se encuentra en un estado inconsistente");
        int totalRegistros = 0;

        for (String solicitud : mapeo) {
            // Validar que se realizo el mapeo de forma exitosa en todas las estructuras
            if (solicitud == null) {
                LOGGER.error("Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                sftpTransferencia.transferenciaConErrorReintento("");
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RLI, "Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                return false;
            }
            // Validar que se tenga pendiente de respuesta las solicitudes de la respuesta
            // TODO ATC
            /*
            SolicitudTC solicitudDB = iSolicitudTCDao.getSolicitudCodigoExterno(solicitud.getSocfnusol());
            if (solicitudDB == null) {
                detalle = "Error en actualización de informacion recibida no existe registro de solicitud.";
                LOGGER.error(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RAL, detalle);
                return false;
            }
            */
        }

        // Limpiar tablas temporales
        if (!iRespuestaCuentaDao.limpiarTodo()) {
            detalle = "Error al limpiar tabla temporal de cuentas.";
            LOGGER.error(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RLI, detalle);
            return false;
        }

        for (String solicitud : mapeo) {
            // Grabar
            if (ok) {
                LOGGER.debug("====================================");
                LOGGER.debug("NRO DOC:{}", solicitud);
                iRespuestaCuentaDao.guardar(new RespuestaSolicitudCuentaLinkser(solicitud), "ISB");
            }
            totalRegistros++;
        }

        // Eliminar archivo de servidor local
        /*
        if (FileUtils.deleteQuietly(archivo)) {
            LOGGER.info("Se ha eliminado el archivo de altas, {}", archivo.getName());
        }*/

        LOGGER.debug("4.2) Verificar si un registro se encuentra en un estado inconsistente");

        // Modificar estado de solicitudes de alta y notificar por correo
        if (ok) {
            sftpTransferencia.transferenciaCompleta("");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.RLI, "Transferencia completa con respuesta a " + totalRegistros + " solicitudes.");
            LOGGER.info("Proceso finalizado de recepcion de solicitud.");
        } else {
            detalle = "Error en proceso de recepcion de solicitudes.";
            LOGGER.warn(detalle);
            sftpTransferencia.transferenciaConErrorInterno(detalle);
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.RLI, detalle);
        }
        return ok;
    }

    public boolean prepararConfiguracionSftp() {
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_ALTAS_LINKSER, Variables.FTP_RUTA_RECEPCION_ALTAS_LINKSER_DEFAULT));
        return sftpTransferencia.configurar(configuracion);
    }

    public List<String> mapearArchivo(File archivo) {
        List<String> respuesta = new ArrayList<String>();
        FileInputStream is = null;
        BufferedReader br = null;
        try {
            is = new FileInputStream(archivo);
            br = new BufferedReader(new InputStreamReader(is));
            String linea = br.readLine();
            List<String> lineas = new ArrayList<String>();
            boolean esPersonaNatural = true;
            StringBuffer solicitudTexto = new StringBuffer();
            while (linea != null) {
                if (StringUtils.isNotEmpty(linea) && linea.length() > 10) {
                    respuesta.add(linea);
                }
                linea = br.readLine();
            }
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al procesar el archivo "
                    + archivo.getName() + ".", e);
            return new ArrayList<String>();
        } finally {
            IOUtils.closeQuietly(is);
        }
        return respuesta;
    }

    public String getNombreArchivoProcesado() {
        return sftpTransferencia.getNombreArchivoRemoto();
    }

    public void setNombreArchivoProcesado(String nombreArchivoProcesado) {
        this.nombreArchivoProcesado = nombreArchivoProcesado;
    }

    public String getNombreArchivoRegex() {
        return this.nombreArchivoRegex;
    }


    public boolean registrarRespuestaCorrecta() {
        if (archivoPendiente != null && sftpTransferencia.getFtpArchivoTransferencia() != null) {
            archivoPendiente.setRespuesta(sftpTransferencia.getFtpArchivoTransferencia().getId());
            sftpTransferencia.guadarRegistro(archivoPendiente);
        }
        return true;
    }

    public boolean getPendienteRespuesta() {
        return pendienteRespuesta;
    }

    public String eliminarArchivo() {
        return sftpTransferencia.eliminarArchivo(sftpTransferencia.getNombreArchivoRemoto());
    }
}
