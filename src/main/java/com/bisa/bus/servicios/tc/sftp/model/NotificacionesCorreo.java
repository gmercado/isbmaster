package com.bisa.bus.servicios.tc.sftp.model;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.mail.api.MailerFactory;
import bus.mail.model.Mailer;
import bus.mail.model.MailerLista;
import bus.plumbing.utils.FormatosUtils;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by atenorio on 10/07/2017.
 */
public class NotificacionesCorreo {
    private static final Logger LOGGER = LoggerFactory.getLogger(NotificacionesCorreo.class);

    private final MailerFactory mailerFactory;
    private final MedioAmbiente medioAmbiente;

    @Inject
    public NotificacionesCorreo(MedioAmbiente medioAmbiente, MailerFactory mailerFactory) {
        this.mailerFactory = mailerFactory;
        this.medioAmbiente = medioAmbiente;
    }

    public String obtenerFechaProceso(TipoArchivo tipo){
        String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_ATC, Variables.FECHA_DE_PROCESO_ATC_DEFAULT);
        if(tipo != null || tipo.equals(TipoArchivo.SLI) || tipo.equals(TipoArchivo.PLI) || tipo.equals(TipoArchivo.ELI)
                || tipo.equals(TipoArchivo.RLI) || tipo.equals(TipoArchivo.CLI)
                || tipo.equals(TipoArchivo.TLI) || tipo.equals(TipoArchivo.FLI)
                || tipo.equals(TipoArchivo.ALI) || tipo.equals(TipoArchivo.MLI)){
            fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_LINKSER, Variables.FECHA_DE_PROCESO_LINKSER_DEFAULT);
        }
        Date fechaProcesar = new Date();
        if(StringUtils.isEmpty(fechaProceso) || "0".equals(fechaProceso)){
            fechaProceso = FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar);
        }else{
            fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
        }
        return FormatosUtils.fechaLargaFormateada(fechaProcesar);
    }

    public void notificarCorrectaTransferencia(String nombreArchivo, TipoArchivo tipo, String mensaje) {
        Mailer mail = mailerFactory.getMailer(MailerLista.SOPORTE);
        if(tipo.equals(TipoArchivo.SOL) || tipo.equals(TipoArchivo.PAG) ||
                tipo.equals(TipoArchivo.SLI) || tipo.equals(TipoArchivo.PLI)){
            mail = mailerFactory.getMailer(MailerLista.TARJETA_CREDITO);
        }
        String subject = medioAmbiente.getValorDe(VariablesFTP.TC_NOTIFICAR_EMAIL_TITULO, VariablesFTP.TC_NOTIFICAR_EMAIL_TITULO_DEFAULT) +
                " ["+ tipo.getDescripcion()  +"] de " + obtenerFechaProceso(tipo);
        mail.mailAndForget(subject, NotificacionesCorreo.getMensajeCorrecto(nombreArchivo, mensaje));
    }

    public void notificarErrorTransferencia(String nombreArchivo, TipoArchivo tipo, String mensaje) {
        Mailer mail = mailerFactory.getMailer(MailerLista.SOPORTE);
        if(tipo.equals(TipoArchivo.SOL) || tipo.equals(TipoArchivo.PAG)){
            mail = mailerFactory.getMailer(MailerLista.TARJETA_CREDITO);
        }
        String subject = medioAmbiente.getValorDe(VariablesFTP.TC_NOTIFICAR_EMAIL_TITULO, VariablesFTP.TC_NOTIFICAR_EMAIL_TITULO_DEFAULT) +
                " ["+ tipo.getDescripcion()  +"] de fecha " + obtenerFechaProceso(tipo);
        mail.mailAndForget(subject, NotificacionesCorreo.getMensajeError(nombreArchivo, mensaje));
    }

    public void notificarErrorTransferenciaOperador(String nombreArchivo, TipoArchivo tipo, String mensaje) {
        Mailer mail = mailerFactory.getMailer(MailerLista.OPERADORES);

        String subject = medioAmbiente.getValorDe(VariablesFTP.TC_NOTIFICAR_EMAIL_TITULO, VariablesFTP.TC_NOTIFICAR_EMAIL_TITULO_DEFAULT) +
                " ["+ tipo.getDescripcion()  +"] de fecha " + obtenerFechaProceso(tipo);
        mail.mailAndForget(subject, NotificacionesCorreo.getMensajeError(nombreArchivo, mensaje));
    }

    public void notificarCorrectaTransferenciaOperador(String nombreArchivo, TipoArchivo tipo, String mensaje) {
        String subject = medioAmbiente.getValorDe(VariablesFTP.TC_NOTIFICAR_EMAIL_TITULO, VariablesFTP.TC_NOTIFICAR_EMAIL_TITULO_DEFAULT) +
                " ["+ tipo.getDescripcion()  +"] de fecha " + obtenerFechaProceso(tipo);
        Mailer mail = mailerFactory.getMailer(MailerLista.OPERADORES);
        Mailer mailNegocio = mailerFactory.getMailer(MailerLista.TARJETA_CREDITO);

        if(StringUtils.isNotEmpty(nombreArchivo)){
            mensaje = NotificacionesCorreo.getMensajeCorrecto(nombreArchivo, mensaje);
        }else{
            mensaje = NotificacionesCorreo.getMensaje(mensaje);
        }
        mail.mailAndForget(subject, mensaje);
        // Notificar transferencia de archivos de retroalimentacion a negocios
        mailNegocio.mailAndForget(subject, mensaje);
    }

    public void notificarError(String subject, String mensaje) {
        Mailer mail = mailerFactory.getMailer(MailerLista.TARJETA_CREDITO);
        mail.mailAndForget(subject, mensaje);
    }

    public void notificarSoporte(String subject, String mensaje) {
        Mailer mail = mailerFactory.getMailer(MailerLista.OPERADORES);
        mensaje = "Estimados usuarios" +
                "\n\n"+ mensaje +
                "\n\n"+
                "Atte,\nSistema Aqua-ISB"+
                "\nBanco BISA S.A.";
        mail.mailAndForget(subject, mensaje);
    }

    public static String getMensaje(String mensaje) {
        return "Estimados usuarios" +
                "\n\n"+ mensaje +
                "\n\n"+
                "Atte,\nSistema Aqua-ISB"+
                "\nBanco BISA S.A.";
    }

    public static String getMensajeCorrecto(String nombreArchivo, String mensaje) {
        return "Estimados usuarios" +
                "\n\nArchivo(s) " + nombreArchivo + " procesado(s) exitosamente." +
                "\n\n"+ mensaje +
                "\n\n"+
                "Atte,\nSistema Aqua-ISB"+
                "\nBanco BISA S.A.";
    }

    public static String getMensajeError(String nombreArchivo, String mensaje) {
        return "Estimados usuarios" +
                "\n\nArchivo(s) " + nombreArchivo + " con error al ser procesado(s)." +
                "\nMensaje: " + mensaje +
                "\n\nPor favor es necesario revisar el archivo y verificar estado de proceso." +
                "\n\n"+
                "Atte,\nSistema Aqua-ISB"+
                "\nBanco BISA S.A.";
    }

}
