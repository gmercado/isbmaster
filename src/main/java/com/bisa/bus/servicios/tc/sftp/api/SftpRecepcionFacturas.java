package com.bisa.bus.servicios.tc.sftp.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.csv.LecturaCSV;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.tc.sftp.dao.FacturasATCDao;
import com.bisa.bus.servicios.tc.sftp.dao.FacturasATCHistoricoDao;
import com.bisa.bus.servicios.tc.sftp.entities.FacturaATC;
import com.bisa.bus.servicios.tc.sftp.entities.FacturaATCHistorico;
import com.bisa.bus.servicios.tc.sftp.entities.IdFactura;
import com.bisa.bus.servicios.tc.sftp.model.EstadoPagosTC;
import com.bisa.bus.servicios.tc.sftp.model.FacturaCsv;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgProgramaTarjetaCredito;
import com.bisa.bus.servicios.tc.sftp.utils.FormatoNumericoTexto;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by atenorio on 03/07/2017.
 */
public class SftpRecepcionFacturas implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpRecepcionFacturas.class);

    private final MedioAmbiente medioAmbiente;
    private final FacturasATCDao facturaDao;
    private final FacturasATCHistoricoDao facturasATCHistoricoDao;
    private final RpgProgramaTarjetaCredito rpgPrograma;
    private final NotificacionesCorreo notificacionesCorreo;
    SftpConfiguracion configuracion;
    SftpTransferencia sftpTransferencia;

    @Inject
    public SftpRecepcionFacturas(MedioAmbiente medioAmbiente,
                                 FacturasATCDao facturaDao, FacturasATCHistoricoDao facturasATCHistoricoDao,
                                 NotificacionesCorreo notificacionesCorreo, RpgProgramaTarjetaCredito rpgPrograma,
                                 SftpConfiguracion configuracion, SftpTransferencia sftpTransferencia) {
        this.medioAmbiente = medioAmbiente;
        this.facturaDao = facturaDao;
        this.facturasATCHistoricoDao = facturasATCHistoricoDao;
        this.rpgPrograma = rpgPrograma;
        this.notificacionesCorreo = notificacionesCorreo;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para procesar recepcion de archivo desde servidor remoto SFTP.
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */

    String PREF = "#fecha";

    public boolean procesarArchivo() {
        String detalle = "";
        boolean ok = true;
        LOGGER.debug("Inicia proceso de recepcion de archivo de factura");
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_ATC, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_ATC_DEFAULT);
        String nombreArchivoRegexForzado = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_ATC_FORZADO, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_ATC_FORZADO_DEFAULT);
        String nombreArchivo[] = Iterables.toArray(Splitter.on("|").trimResults().split(nombreArchivoRegex), String.class);

        // Obtener fecha a procesar extracto
        String fechaProcesoTexto = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_ATC, Variables.FECHA_DE_PROCESO_ATC_DEFAULT);
        // La fecha de proceso es la fecha del banco
        Calendar hoy = Calendar.getInstance();
        Date fechaProcesar = hoy.getTime();

        if (StringUtils.isEmpty(fechaProcesoTexto) || "0".equals(fechaProcesoTexto)) {
            fechaProcesoTexto = FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar);
        } else {
            fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProcesoTexto);
            if (fechaProcesar == null) {
                notificacionesCorreo.notificarError("Error en fecha de proceso recepci\u00F3n facturas", "");
                return false;
            }
            hoy.setTime(fechaProcesar);
        }

        // Se debe disminuir un dia debido a que el archivo se generara con la fecha de proceso de negocio
        // cierre al 25 de cada mes.
        hoy.add(Calendar.DAY_OF_YEAR, -1);
        nombreArchivoRegex = nombreArchivo[0].replace(PREF, new SimpleDateFormat(nombreArchivo[1]).format(hoy.getTime()));
        if (!StringUtils.isEmpty(nombreArchivoRegexForzado) && !"NULL".equals(nombreArchivoRegexForzado)) {
            nombreArchivoRegex = nombreArchivoRegexForzado;
        }
        // Actualizar la fecha de proceso
        hoy.add(Calendar.DAY_OF_YEAR, +1);
        fechaProcesar = hoy.getTime();
        LOGGER.info("Fecha de proceso: {}, {}", fechaProcesar, fechaProcesoTexto);

        LOGGER.info("Descarga de archivo de a servidor SFTP con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_FACTURAS_ATC, Variables.FTP_RUTA_RECEPCION_FACTURAS_ATC_DEFAULT));
        sftpTransferencia.configurar(configuracion);

        ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.FAC);
        LOGGER.debug("Descarga completa correctamente: {}", ok);

        if (!ok || sftpTransferencia.getArchivoLocal() == null) {
            // Cerrar el registro con error y permitir reintento
            detalle = "Problema en transferencia o no existen archivos de respuesta en servidor SFTP.";
            LOGGER.warn(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.FAC, detalle);
            return false;
        }

        // Procesamiento de archivo local
        LOGGER.debug("Proceso de mapeo de archivos de respuesta");
        File archivo = sftpTransferencia.getArchivoLocal();
        List<FacturaCsv> facturas = mapearArchivo(archivo);
        // En caso de problemas en lectura de archivo
        if (facturas == null || facturas.size() == 0) {
            detalle = "Error en mapeo de archivo de respuesta o archivo vac\u00EDo";
            LOGGER.warn(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.FAC, detalle);
            return false;
        }
        // Validar que se realizo el mapeo de forma exitosa
        int fila = 0;
        for (FacturaCsv factura : facturas) {
            fila++;
            if (factura == null) {
                detalle = "Error en mapeo de una estructura se requiere revisar el archivo de respuesta en fila " + fila + ".";
                LOGGER.error(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.FAC, detalle);
                return false;
            }
        }
        LOGGER.info("Registros cargados de archivo: {}", facturas.size());

        LOGGER.debug("Actualizar tabla de extracto");
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();

        // Limpiar tablas intermedias
        if (!facturaDao.limpiarTodo()) {
            detalle = "Error al limpiar tabla temporal extracto.";
            LOGGER.error(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.FAC, detalle);
            return false;
        }

        ok = true;

        // Registro de informacion recuperada
        for (FacturaCsv factura : facturas) {
            IdFactura id = new IdFactura();
            id.setFechaEmision(factura.getFechaEmision());
            id.setNit(factura.getNit());
            id.setNumeroAutorizacion(factura.getNumeroAutorizacion());
            id.setNumeroFactura(factura.getNumeroFactura());

            // Recuperando informacion de mapeo
            FacturaATC facturaATCDB = transferObject.convert(factura, FacturaATC.class);
            facturaATCDB.setIdFactura(id);
            facturaATCDB.setEstado(EstadoPagosTC.GENE.name());
            facturaATCDB.setFechaProceso(FormatosUtils.fechaYYYYMMDD(fechaProcesar).intValue());
            facturaATCDB.setCodigoAdm('A');

            // Registrar factura
            facturaATCDB.setDebitoFiscalTexto(FormatoNumericoTexto.getNumero(facturaATCDB.getDebitoFiscal(), 12));
            facturaATCDB.setImporteBaseTexto(FormatoNumericoTexto.getNumero(facturaATCDB.getImporteBase(), 12));
            facturaATCDB.setImporteDescuentosTexto(FormatoNumericoTexto.getNumero(facturaATCDB.getImporteDescuentos(), 12));
            facturaATCDB.setImporteTasasTexto(FormatoNumericoTexto.getNumero(facturaATCDB.getImporteTasas(), 12));
            facturaATCDB.setImporteTotalTexto(FormatoNumericoTexto.getNumero(facturaATCDB.getImporteTotal(), 12));
            facturaATCDB.setSubTotalTexto(FormatoNumericoTexto.getNumero(facturaATCDB.getSubTotal(), 12));
            facturaATCDB.setImporteOperacionExentasTexto(FormatoNumericoTexto.getNumero(facturaATCDB.getImporteOperacionExentas(), 12));
            facturaATCDB.setVentasGravadasTasaCeroTexto(FormatoNumericoTexto.getNumero(facturaATCDB.getVentasGravadasTasaCero(), 12));

            facturaATCDB.setFechaRecepcion(new Date());
            facturaATCDB.setIdArchivoRecepcion(sftpTransferencia.getFtpArchivoTransferencia().getId());
            facturaATCDB.setUsuarioModificador("ISB");
            facturaATCDB.setFechaModificacion(new Date());

            ok = ok && facturaDao.guardar(facturaATCDB, "ISB");
            FacturaATCHistorico historico = transferObject.convert(facturaATCDB, FacturaATCHistorico.class);
            historico.setIdFactura(facturaATCDB.getIdFactura());
            ok = ok && facturasATCHistoricoDao.guardar(historico, "ISB");
        }
        if (ok) {
            detalle = "Recepci\u00F3n de Facturas finalizada correctamente con " + fila + " registros.";
            sftpTransferencia.transferenciaCompleta(detalle);
            notificacionesCorreo.notificarCorrectaTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.FAC, detalle);
        } else {
            detalle = "Problemas en registro de base de datos.";
            sftpTransferencia.transferenciaConErrorInterno(detalle);
            notificacionesCorreo.notificarErrorTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.FAC, detalle);
        }

        LOGGER.debug("Completado proceso de carga de archivo {} de facturacion a servidor SFTP de ATC respuesta {}.", sftpTransferencia.getNombreArchivoRemoto(), ok);
        return ok;
    }

    /**
     * Mapeo de estructura de facturas
     **/
    public List<FacturaCsv> mapearArchivo(File archivo) {
        FileInputStream is = null;
        BufferedReader br = null;
        boolean respuesta;
        List<FacturaCsv> facturaCsvs = new ArrayList<FacturaCsv>();
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();
        int total = 0;
        try {
            is = new FileInputStream(archivo);
            String codificacion = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC, Variables.FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC_DEFAULT);
            br = new BufferedReader(new InputStreamReader(is, codificacion));
            List<String> lineas = new ArrayList<String>();
            String linea = br.readLine();
            // Recuperar titulo de libro de ventas
            if (linea != null){
                LOGGER.info(linea);
                linea = br.readLine();
            }
            while (linea != null) {
                total++;
                LOGGER.debug("linea {}:{}", total, linea);
                if (StringUtils.isNotEmpty(linea) && linea.length() > 1 && total > 0) {
                    FacturaCsv factura = LecturaCSV.procesar(FacturaCsv.class, linea);
                    facturaCsvs.add(factura);
                }
                // lectura desde la segunda linea
                linea = br.readLine();
            }
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al procesar el archivo "
                    + archivo.getName() + ".", e);
            return null;
        } finally {
            IOUtils.closeQuietly(is);
        }
        return facturaCsvs;
    }

    /**
     * Validaciones de registro de
     */
    private String validarFactura(FacturaCsv factura) {
        // Verificar que la factura siempre sea numerico
        if (StringUtils.isEmpty(factura.getNumeroFactura())
                || factura.getNumeroFactura().matches("^[0-9]")) {
            return "Valor no permitido en n\u00FAmero de factura.";
        }
        // Verificar que autorización siempre sea numerico
        if (StringUtils.isEmpty(factura.getNumeroAutorizacion())
                || factura.getNumeroAutorizacion().matches("^[0-9]")) {
            return "Valor no permitido en n\u00FAmero de autorizaci\u00F3n.";
        }
        return "";
    }

    public String getNombreArchivoProcesado() {
        return sftpTransferencia.getNombreArchivoRemoto();
    }
}
