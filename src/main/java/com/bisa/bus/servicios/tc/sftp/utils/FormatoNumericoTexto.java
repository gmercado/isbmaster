package com.bisa.bus.servicios.tc.sftp.utils;

import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by atenorio on 02/08/2017.
 */
public class FormatoNumericoTexto {

    public static synchronized Character getSigno(BigDecimal numero) {
        if (numero == null) return new Character(' ');
        return (numero.doubleValue() < 0) ? new Character('-') : new Character('+');
    }


    public static synchronized String getNumero(BigDecimal numero, int size) {
        if (numero == null) numero = new BigDecimal(0);
        DecimalFormat decimalFormat = new DecimalFormat("##.00");
        String valor = decimalFormat.format(numero);
        valor = valor.replaceAll("\\.", "");
        valor = valor.replace(",", "");
        valor = valor.replace("-", "");
        valor = StringUtils.leftPad(valor, size, "0");
        return valor;
    }
}
