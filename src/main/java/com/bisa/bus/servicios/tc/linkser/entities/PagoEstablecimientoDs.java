package com.bisa.bus.servicios.tc.linkser.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 21/11/2017.
 */
@Entity
@Table(name = "TCPESTXD")
public class PagoEstablecimientoDs implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "ESTABL", nullable = false, length = 150)
    private String texto;

    public PagoEstablecimientoDs(String texto){
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}

