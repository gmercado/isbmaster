package com.bisa.bus.servicios.tc.linkser.sched;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.bus.servicios.tc.linkser.api.SftpRecepcionPagoEstablecimientoBs;
import com.bisa.bus.servicios.tc.linkser.api.SftpRecepcionPagoEstablecimientoDs;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 22/11/2017.
 */
public class RecepcionPagoEstablecimientoLinkser implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecepcionFacturasLinkser.class);

    private final MedioAmbiente medioAmbiente;
    private final NotificacionesCorreo notificacionesCorreo;
    private final SftpRecepcionPagoEstablecimientoBs sftpRecepcionPagoEstablecimientoBs;
    private final SftpRecepcionPagoEstablecimientoDs sftpRecepcionPagoEstablecimientoDs;

    private volatile boolean ok;

    @Inject
    public RecepcionPagoEstablecimientoLinkser(MedioAmbiente medioAmbiente, NotificacionesCorreo notificacionesCorreo,
                                               SftpRecepcionPagoEstablecimientoBs sftpRecepcionPagoEstablecimientoBs,
                                               SftpRecepcionPagoEstablecimientoDs sftpRecepcionPagoEstablecimientoDs){
        this.medioAmbiente = medioAmbiente;
        this.notificacionesCorreo = notificacionesCorreo;
        this.sftpRecepcionPagoEstablecimientoBs = sftpRecepcionPagoEstablecimientoBs;
        this.sftpRecepcionPagoEstablecimientoDs = sftpRecepcionPagoEstablecimientoDs;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        // Procesando Bs
        LOGGER.info("Inicio de proceso recepcion pago a establecimientos Bs");
        ok = sftpRecepcionPagoEstablecimientoBs.procesarArchivo();
        LOGGER.info("Respuesta proceso de recepcion pago a establecimientos Bs: {}", ok);
        // Procesando Ds
        if(ok) {
            LOGGER.info("Inicio de proceso recepcion pago a establecimientos Ds");
            ok = sftpRecepcionPagoEstablecimientoDs.procesarArchivo();
            LOGGER.info("Respuesta proceso de recepcion pago a establecimientos Ds: {}", ok);
        }
        // Completar proceso de carga notificando al operador
        if(ok){
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_PAGO_ESTABLECIMIENTO_LINKSER, Variables.FTP_MENSAJE_EMAIL_PAGO_ESTABLECIMIENTO_LINKSER_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador(sftpRecepcionPagoEstablecimientoBs.getNombreArchivoProcesado() + ", " + sftpRecepcionPagoEstablecimientoDs.getNombreArchivoProcesado(), TipoArchivo.ELI, mensaje);
        }else{
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_PAGO_ESTABLECIMIENTO_LINKSER_ERROR_LINKSER, Variables.FTP_MENSAJE_EMAIL_PAGO_ESTABLECIMIENTO_LINKSER_ERROR_LINKSER_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador("", TipoArchivo.ELI, mensaje);
        }
    }
}

