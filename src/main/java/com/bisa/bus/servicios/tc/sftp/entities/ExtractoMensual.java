package com.bisa.bus.servicios.tc.sftp.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by atenorio on 01/08/2017.
 */
@Entity
@Table(name = "TCPANENEX")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "REFECR")),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "REPGCR")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "REFEMO")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "REPGMO"))})
public class ExtractoMensual extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private IdExtractoMensual idExtractoMensual;

    @Basic(optional = false)
    @Column(name = "EEBAEM", nullable = false, length = 4)
    private String codigoBanco;
    @Basic(optional = false)
    @Column(name = "EESUEM", nullable = false, length = 4)
    private String codigoSucursal;

    @Basic(optional = false)
    @Column(name = "EEPEBA", nullable = false, length = 6)
    private String periodoBalance;
    @Basic(optional = false)
    @Column(name = "EESAANBSS", nullable = false)
    private Character saldoAnteriorBsSigno;
    @Basic(optional = false)
    @Column(name = "EESAANBS", nullable = false, length = 10)
    private String saldoAnteriorBsTexto;
    @Basic(optional = false)
    @Column(name = "EESAANUSS", nullable = false)
    private Character saldoAnteriorUsSigno;
    @Basic(optional = false)
    @Column(name = "EESAANUS", nullable = false, length = 10)
    private String saldoAnteriorUsTexto;
    @Basic(optional = false)
    @Column(name = "EEINANS", nullable = false)
    private Character interesAnteriorSigno;
    @Basic(optional = false)
    @Column(name = "EEINAN", nullable = false, length = 10)
    private String interesAnteriorTexto;
    @Basic(optional = false)
    @Column(name = "EEPAMIANS", nullable = false)
    private Character pagoMinimoAnteriorSigno;
    @Basic(optional = false)
    @Column(name = "EEPAMIAN", nullable = false, length = 10)
    private String pagoMinimoAnteriorTexto;
    @Basic(optional = false)
    @Column(name = "EESAACBSS", nullable = false)
    private Character saldoActualBsSigno;
    @Basic(optional = false)
    @Column(name = "EESAACBS", nullable = false, length = 10)
    private String saldoActualBsTexto;
    @Basic(optional = false)
    @Column(name = "EESAACUSS", nullable = false)
    private Character saldoActualUsSigno;
    @Basic(optional = false)
    @Column(name = "EESAACUS", nullable = false, length = 10)
    private String saldoActualUsTexto;
    @Basic(optional = false)
    @Column(name = "EEINACS", nullable = false)
    private Character interesActualSigno;
    @Basic(optional = false)
    @Column(name = "EEINAC", nullable = false, length = 10)
    private String interesActualTexto;
    @Basic(optional = false)
    @Column(name = "EEPAMIACS", nullable = false)
    private Character pagoMinimoActualSigno;
    @Basic(optional = false)
    @Column(name = "EEPAMIAC", nullable = false, length = 10)
    private String pagoMinimoActualTexto;
    @Basic(optional = false)
    @Column(name = "EEFEVEAN", nullable = false, length = 8)
    private String fechaVencimientoAnterior;
    @Basic(optional = false)
    @Column(name = "EEFEVEAC", nullable = false, length = 8)
    private String fechaVencimientoActual;
    @Basic(optional = false)
    @Column(name = "EETAEFCO", nullable = false, length = 9)
    private String tasaEfectivaActivaConsumo;
    @Basic(optional = false)
    @Column(name = "EETAEFAE", nullable = false, length = 9)
    private String tasaEfectivaActivaAvance;
    @Basic(optional = false)
    @Column(name = "EETANOAC", nullable = false, length = 9)
    private String tasaNominalActiva;
    @Basic(optional = false)
    @Column(name = "EEFEVEPR", nullable = false, length = 8)
    private String fechaProximoVencimiento;
    @Basic(optional = false)
    @Column(name = "EELIFIS", nullable = false)
    private Character limiteFinanciamientoSigno;
    @Basic(optional = false)
    @Column(name = "EELIFI", nullable = false, length = 10)
    private String limiteFinanciamientoTexto;
    @Basic(optional = false)
    @Column(name = "EECOPEANS", nullable = false)
    private Character consumoPeriodoAnteriorSigno;
    @Basic(optional = false)
    @Column(name = "EECOPEAN", nullable = false, length = 10)
    private String consumoPeriodoAnteriorTexto;
    @Basic(optional = false)
    @Column(name = "EECOPEACS", nullable = false)
    private Character consumoPeriodoActualBsSigno;
    @Basic(optional = false)
    @Column(name = "EECOPEAC", nullable = false, length = 10)
    private String consumoPeriodoActualBsTexto;
    @Basic(optional = false)
    @Column(name = "EECOPEAUSS", nullable = false)
    private Character consumoPeriodoActualUsSigno;
    @Basic(optional = false)
    @Column(name = "EECOPEAUS", nullable = false, length = 10)
    private String consumoPeriodoActualUsTexto;



    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "EESAANBSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal saldoAnteriorBs;
    @Basic(optional = false)
    @Column(name = "EESAANUSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal saldoAnteriorUs;
    @Basic(optional = false)
    @Column(name = "EEINANN", nullable = false, precision = 11, scale = 2)
    private BigDecimal interesAnterior;
    @Basic(optional = false)
    @Column(name = "EEPAMIANN", nullable = false, precision = 11, scale = 2)
    private BigDecimal pagoMinimoAnterior;
    @Basic(optional = false)
    @Column(name = "EESAACBSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal saldoActualBs;
    @Basic(optional = false)
    @Column(name = "EESAACUSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal saldoActualUs;
    @Basic(optional = false)
    @Column(name = "EEINACN", nullable = false, precision = 11, scale = 2)
    private BigDecimal interesActual;
    @Basic(optional = false)
    @Column(name = "EEPAMIACN", nullable = false, precision = 11, scale = 2)
    private BigDecimal pagoMinimoActual;
    @Basic(optional = false)
    @Column(name = "EELIFIN", nullable = false, precision = 11, scale = 2)
    private BigDecimal limiteFinanciamiento;
    @Basic(optional = false)
    @Column(name = "EECOPEANN", nullable = false, precision = 11, scale = 2)
    private BigDecimal consumoPeriodoAnterior;
    @Basic(optional = false)
    @Column(name = "EECOPEACN", nullable = false, precision = 11, scale = 2)
    private BigDecimal consumoPeriodoActualBs;
    @Basic(optional = false)
    @Column(name = "EECOPEAUSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal consumoPeriodoActualUs;
    @Basic(optional = false)
    @Column(name = "EENUTC", nullable = false)
    private long cuentaTarjeta;
    @Basic(optional = false)
    @Column(name = "EENUT1", nullable = false, length = 9)
    private String cuentaTarjetaReducida;
    @Basic(optional = false)
    @Column(name = "EEEST", nullable = false, length = 4)
    private String estado;

    @Basic(optional = false)
    @Column(name = "EEFERE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRecepcion;

    public ExtractoMensual() {
    }

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getCodigoSucursal() {
        return codigoSucursal;
    }

    public void setCodigoSucursal(String codigoSucursal) {
        this.codigoSucursal = codigoSucursal;
    }


    public String getPeriodoBalance() {
        return periodoBalance;
    }

    public void setPeriodoBalance(String periodoBalance) {
        this.periodoBalance = periodoBalance;
    }

    public Character getSaldoAnteriorBsSigno() {
        return saldoAnteriorBsSigno;
    }

    public void setSaldoAnteriorBsSigno(Character saldoAnteriorBsSigno) {
        this.saldoAnteriorBsSigno = saldoAnteriorBsSigno;
    }

    public String getSaldoAnteriorBsTexto() {
        return saldoAnteriorBsTexto;
    }

    public void setSaldoAnteriorBsTexto(String saldoAnteriorBsTexto) {
        this.saldoAnteriorBsTexto = saldoAnteriorBsTexto;
    }

    public Character getSaldoAnteriorUsSigno() {
        return saldoAnteriorUsSigno;
    }

    public void setSaldoAnteriorUsSigno(Character saldoAnteriorUsSigno) {
        this.saldoAnteriorUsSigno = saldoAnteriorUsSigno;
    }

    public String getSaldoAnteriorUsTexto() {
        return saldoAnteriorUsTexto;
    }

    public void setSaldoAnteriorUsTexto(String saldoAnteriorUsTexto) {
        this.saldoAnteriorUsTexto = saldoAnteriorUsTexto;
    }

    public Character getInteresAnteriorSigno() {
        return interesAnteriorSigno;
    }

    public void setInteresAnteriorSigno(Character interesAnteriorSigno) {
        this.interesAnteriorSigno = interesAnteriorSigno;
    }

    public String getInteresAnteriorTexto() {
        return interesAnteriorTexto;
    }

    public void setInteresAnteriorTexto(String interesAnteriorTexto) {
        this.interesAnteriorTexto = interesAnteriorTexto;
    }

    public Character getPagoMinimoAnteriorSigno() {
        return pagoMinimoAnteriorSigno;
    }

    public void setPagoMinimoAnteriorSigno(Character pagoMinimoAnteriorSigno) {
        this.pagoMinimoAnteriorSigno = pagoMinimoAnteriorSigno;
    }

    public String getPagoMinimoAnteriorTexto() {
        return pagoMinimoAnteriorTexto;
    }

    public void setPagoMinimoAnteriorTexto(String pagoMinimoAnteriorTexto) {
        this.pagoMinimoAnteriorTexto = pagoMinimoAnteriorTexto;
    }

    public Character getSaldoActualBsSigno() {
        return saldoActualBsSigno;
    }

    public void setSaldoActualBsSigno(Character saldoActualBsSigno) {
        this.saldoActualBsSigno = saldoActualBsSigno;
    }

    public String getSaldoActualBsTexto() {
        return saldoActualBsTexto;
    }

    public void setSaldoActualBsTexto(String saldoActualBsTexto) {
        this.saldoActualBsTexto = saldoActualBsTexto;
    }

    public Character getSaldoActualUsSigno() {
        return saldoActualUsSigno;
    }

    public void setSaldoActualUsSigno(Character saldoActualUsSigno) {
        this.saldoActualUsSigno = saldoActualUsSigno;
    }

    public String getSaldoActualUsTexto() {
        return saldoActualUsTexto;
    }

    public void setSaldoActualUsTexto(String saldoActualUsTexto) {
        this.saldoActualUsTexto = saldoActualUsTexto;
    }

    public Character getInteresActualSigno() {
        return interesActualSigno;
    }

    public void setInteresActualSigno(Character interesActualSigno) {
        this.interesActualSigno = interesActualSigno;
    }

    public String getInteresActualTexto() {
        return interesActualTexto;
    }

    public void setInteresActualTexto(String interesActualTexto) {
        this.interesActualTexto = interesActualTexto;
    }

    public Character getPagoMinimoActualSigno() {
        return pagoMinimoActualSigno;
    }

    public void setPagoMinimoActualSigno(Character pagoMinimoActualSigno) {
        this.pagoMinimoActualSigno = pagoMinimoActualSigno;
    }

    public String getPagoMinimoActualTexto() {
        return pagoMinimoActualTexto;
    }

    public void setPagoMinimoActualTexto(String pagoMinimoActualTexto) {
        this.pagoMinimoActualTexto = pagoMinimoActualTexto;
    }

    public String getFechaVencimientoAnterior() {
        return fechaVencimientoAnterior;
    }

    public void setFechaVencimientoAnterior(String fechaVencimientoAnterior) {
        this.fechaVencimientoAnterior = fechaVencimientoAnterior;
    }

    public String getFechaVencimientoActual() {
        return fechaVencimientoActual;
    }

    public void setFechaVencimientoActual(String fechaVencimientoActual) {
        this.fechaVencimientoActual = fechaVencimientoActual;
    }

    public String getTasaEfectivaActivaConsumo() {
        return tasaEfectivaActivaConsumo;
    }

    public void setTasaEfectivaActivaConsumo(String tasaEfectivaActivaConsumo) {
        this.tasaEfectivaActivaConsumo = tasaEfectivaActivaConsumo;
    }

    public String getTasaEfectivaActivaAvance() {
        return tasaEfectivaActivaAvance;
    }

    public void setTasaEfectivaActivaAvance(String tasaEfectivaActivaAvance) {
        this.tasaEfectivaActivaAvance = tasaEfectivaActivaAvance;
    }

    public String getTasaNominalActiva() {
        return tasaNominalActiva;
    }

    public void setTasaNominalActiva(String tasaNominalActiva) {
        this.tasaNominalActiva = tasaNominalActiva;
    }

    public String getFechaProximoVencimiento() {
        return fechaProximoVencimiento;
    }

    public void setFechaProximoVencimiento(String fechaProximoVencimiento) {
        this.fechaProximoVencimiento = fechaProximoVencimiento;
    }

    public Character getLimiteFinanciamientoSigno() {
        return limiteFinanciamientoSigno;
    }

    public void setLimiteFinanciamientoSigno(Character limiteFinanciamientoSigno) {
        this.limiteFinanciamientoSigno = limiteFinanciamientoSigno;
    }

    public String getLimiteFinanciamientoTexto() {
        return limiteFinanciamientoTexto;
    }

    public void setLimiteFinanciamientoTexto(String limiteFinanciamientoTexto) {
        this.limiteFinanciamientoTexto = limiteFinanciamientoTexto;
    }

    public Character getConsumoPeriodoAnteriorSigno() {
        return consumoPeriodoAnteriorSigno;
    }

    public void setConsumoPeriodoAnteriorSigno(Character consumoPeriodoAnteriorSigno) {
        this.consumoPeriodoAnteriorSigno = consumoPeriodoAnteriorSigno;
    }

    public String getConsumoPeriodoAnteriorTexto() {
        return consumoPeriodoAnteriorTexto;
    }

    public void setConsumoPeriodoAnteriorTexto(String consumoPeriodoAnteriorTexto) {
        this.consumoPeriodoAnteriorTexto = consumoPeriodoAnteriorTexto;
    }

    public Character getConsumoPeriodoActualBsSigno() {
        return consumoPeriodoActualBsSigno;
    }

    public void setConsumoPeriodoActualBsSigno(Character consumoPeriodoActualBsSigno) {
        this.consumoPeriodoActualBsSigno = consumoPeriodoActualBsSigno;
    }

    public String getConsumoPeriodoActualBsTexto() {
        return consumoPeriodoActualBsTexto;
    }

    public void setConsumoPeriodoActualBsTexto(String consumoPeriodoActualBsTexto) {
        this.consumoPeriodoActualBsTexto = consumoPeriodoActualBsTexto;
    }

    public Character getConsumoPeriodoActualUsSigno() {
        return consumoPeriodoActualUsSigno;
    }

    public void setConsumoPeriodoActualUsSigno(Character consumoPeriodoActualUsSigno) {
        this.consumoPeriodoActualUsSigno = consumoPeriodoActualUsSigno;
    }

    public String getConsumoPeriodoActualUsTexto() {
        return consumoPeriodoActualUsTexto;
    }

    public void setConsumoPeriodoActualUsTexto(String consumoPeriodoActualUsTexto) {
        this.consumoPeriodoActualUsTexto = consumoPeriodoActualUsTexto;
    }

    public BigDecimal getSaldoAnteriorBs() {
        return saldoAnteriorBs;
    }

    public void setSaldoAnteriorBs(BigDecimal saldoAnteriorBs) {
        this.saldoAnteriorBs = saldoAnteriorBs;
    }

    public BigDecimal getSaldoAnteriorUs() {
        return saldoAnteriorUs;
    }

    public void setSaldoAnteriorUs(BigDecimal saldoAnteriorUs) {
        this.saldoAnteriorUs = saldoAnteriorUs;
    }

    public BigDecimal getInteresAnterior() {
        return interesAnterior;
    }

    public void setInteresAnterior(BigDecimal interesAnterior) {
        this.interesAnterior = interesAnterior;
    }

    public BigDecimal getPagoMinimoAnterior() {
        return pagoMinimoAnterior;
    }

    public void setPagoMinimoAnterior(BigDecimal pagoMinimoAnterior) {
        this.pagoMinimoAnterior = pagoMinimoAnterior;
    }

    public BigDecimal getSaldoActualBs() {
        return saldoActualBs;
    }

    public void setSaldoActualBs(BigDecimal saldoActualBs) {
        this.saldoActualBs = saldoActualBs;
    }

    public BigDecimal getSaldoActualUs() {
        return saldoActualUs;
    }

    public void setSaldoActualUs(BigDecimal saldoActualUs) {
        this.saldoActualUs = saldoActualUs;
    }

    public BigDecimal getInteresActual() {
        return interesActual;
    }

    public void setInteresActual(BigDecimal interesActual) {
        this.interesActual = interesActual;
    }

    public BigDecimal getPagoMinimoActual() {
        return pagoMinimoActual;
    }

    public void setPagoMinimoActual(BigDecimal pagoMinimoActual) {
        this.pagoMinimoActual = pagoMinimoActual;
    }

    public BigDecimal getLimiteFinanciamiento() {
        return limiteFinanciamiento;
    }

    public void setLimiteFinanciamiento(BigDecimal limiteFinanciamiento) {
        this.limiteFinanciamiento = limiteFinanciamiento;
    }

    public BigDecimal getConsumoPeriodoAnterior() {
        return consumoPeriodoAnterior;
    }

    public void setConsumoPeriodoAnterior(BigDecimal consumoPeriodoAnterior) {
        this.consumoPeriodoAnterior = consumoPeriodoAnterior;
    }

    public BigDecimal getConsumoPeriodoActualBs() {
        return consumoPeriodoActualBs;
    }

    public void setConsumoPeriodoActualBs(BigDecimal consumoPeriodoActualBs) {
        this.consumoPeriodoActualBs = consumoPeriodoActualBs;
    }

    public BigDecimal getConsumoPeriodoActualUs() {
        return consumoPeriodoActualUs;
    }

    public void setConsumoPeriodoActualUs(BigDecimal consumoPeriodoActualUs) {
        this.consumoPeriodoActualUs = consumoPeriodoActualUs;
    }

    public long getCuentaTarjeta() {
        return cuentaTarjeta;
    }

    public void setCuentaTarjeta(long cuentaTarjeta) {
        this.cuentaTarjeta = cuentaTarjeta;
    }

    public String getCuentaTarjetaReducida() {
        return cuentaTarjetaReducida;
    }

    public void setCuentaTarjetaReducida(String cuentaTarjetaReducida) {
        this.cuentaTarjetaReducida = cuentaTarjetaReducida;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public IdExtractoMensual getIdExtractoMensual() {
        return idExtractoMensual;
    }

    public void setIdExtractoMensual(IdExtractoMensual idExtractoMensual) {
        this.idExtractoMensual = idExtractoMensual;
    }

    @Override
    public String toString() {
        return "ExtractoMensual{" +
                "idExtractoMensual=" + idExtractoMensual +
                ", codigoBanco='" + codigoBanco + '\'' +
                ", codigoSucursal='" + codigoSucursal + '\'' +
                ", periodoBalance='" + periodoBalance + '\'' +
                ", saldoAnteriorBsSigno=" + saldoAnteriorBsSigno +
                ", saldoAnteriorBsTexto='" + saldoAnteriorBsTexto + '\'' +
                ", saldoAnteriorUsSigno=" + saldoAnteriorUsSigno +
                ", saldoAnteriorUsTexto='" + saldoAnteriorUsTexto + '\'' +
                ", interesAnteriorSigno=" + interesAnteriorSigno +
                ", interesAnteriorTexto='" + interesAnteriorTexto + '\'' +
                ", pagoMinimoAnteriorSigno=" + pagoMinimoAnteriorSigno +
                ", pagoMinimoAnteriorTexto='" + pagoMinimoAnteriorTexto + '\'' +
                ", saldoActualBsSigno=" + saldoActualBsSigno +
                ", saldoActualBsTexto='" + saldoActualBsTexto + '\'' +
                ", saldoActualUsSigno=" + saldoActualUsSigno +
                ", saldoActualUsTexto='" + saldoActualUsTexto + '\'' +
                ", interesActualSigno=" + interesActualSigno +
                ", interesActualTexto='" + interesActualTexto + '\'' +
                ", pagoMinimoActualSigno=" + pagoMinimoActualSigno +
                ", pagoMinimoActualTexto='" + pagoMinimoActualTexto + '\'' +
                ", fechaVencimientoAnterior='" + fechaVencimientoAnterior + '\'' +
                ", fechaVencimientoActual='" + fechaVencimientoActual + '\'' +
                ", tasaEfectivaActivaConsumo='" + tasaEfectivaActivaConsumo + '\'' +
                ", tasaEfectivaActivaAvance='" + tasaEfectivaActivaAvance + '\'' +
                ", tasaNominalActiva='" + tasaNominalActiva + '\'' +
                ", fechaProximoVencimiento='" + fechaProximoVencimiento + '\'' +
                ", limiteFinanciamientoSigno=" + limiteFinanciamientoSigno +
                ", limiteFinanciamientoTexto='" + limiteFinanciamientoTexto + '\'' +
                ", consumoPeriodoAnteriorSigno=" + consumoPeriodoAnteriorSigno +
                ", consumoPeriodoAnteriorTexto='" + consumoPeriodoAnteriorTexto + '\'' +
                ", consumoPeriodoActualBsSigno=" + consumoPeriodoActualBsSigno +
                ", consumoPeriodoActualBsTexto='" + consumoPeriodoActualBsTexto + '\'' +
                ", consumoPeriodoActualUsSigno=" + consumoPeriodoActualUsSigno +
                ", consumoPeriodoActualUsTexto='" + consumoPeriodoActualUsTexto + '\'' +
                ", saldoAnteriorBs=" + saldoAnteriorBs +
                ", saldoAnteriorUs=" + saldoAnteriorUs +
                ", interesAnterior=" + interesAnterior +
                ", pagoMinimoAnterior=" + pagoMinimoAnterior +
                ", saldoActualBs=" + saldoActualBs +
                ", saldoActualUs=" + saldoActualUs +
                ", interesActual=" + interesActual +
                ", pagoMinimoActual=" + pagoMinimoActual +
                ", limiteFinanciamiento=" + limiteFinanciamiento +
                ", consumoPeriodoAnterior=" + consumoPeriodoAnterior +
                ", consumoPeriodoActualBs=" + consumoPeriodoActualBs +
                ", consumoPeriodoActualUs=" + consumoPeriodoActualUs +
                ", cuentaTarjeta=" + cuentaTarjeta +
                ", cuentaTarjetaReducida='" + cuentaTarjetaReducida + '\'' +
                ", estado='" + estado + '\'' +
                ", fechaRecepcion=" + fechaRecepcion +
                '}';
    }
}
