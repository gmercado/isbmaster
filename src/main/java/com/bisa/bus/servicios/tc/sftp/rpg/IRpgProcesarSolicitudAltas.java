package com.bisa.bus.servicios.tc.sftp.rpg;

import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.rpg.ImposibleLlamarProgramaRpgException;
import com.ibm.as400.access.ConnectionPoolException;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by atenorio on 25/05/2017.
 */
public interface IRpgProcesarSolicitudAltas {
    boolean ejecutar() throws ImposibleLeerRespuestaException, IOException, TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException, TimeoutException;
}
