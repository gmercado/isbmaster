package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import javax.persistence.Basic;

/**
 * Created by atenorio on 19/05/2017.
 */
public class SolicitudDireccionCorreo {

    @Basic(optional = false)
    @FormatCsv(name = "SOADTIREC", nullable = false, length = 2)
    private String soadtirec;
    @Basic(optional = false)
    @FormatCsv(name = "SOADTIDICC", nullable = false, length = 1)
    private short soadtidicc;
    @Basic(optional = false)
    @FormatCsv(name = "SOADTICAC", nullable = false, length = 4)
    private short soadticac;
    @Basic(optional = false)
    @FormatCsv(name = "SOADCALLEC", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadcallec;
    @Basic(optional = false)
    @FormatCsv(name = "SOADNUPUC", nullable = false, length = 10, align = FormatCsv.alignType.LEFT)
    private String soadnupuc;
    @Basic(optional = false)
    @FormatCsv(name = "SOADINADC", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadinadc;
    @Basic(optional = false)
    @FormatCsv(name = "SOADCOPOC", nullable = false, length = 6)
    private String soadcopoc;
    @Basic(optional = false)
    @FormatCsv(name = "SOADZONAC", nullable = false, length = 60, align = FormatCsv.alignType.LEFT)
    private String soadzonac;
    @Basic(optional = false)
    @FormatCsv(name = "SOADPAISC", nullable = false, length = 3, fillSpace = "0")
    private short soadpaisc;
    @Basic(optional = false)
    @FormatCsv(name = "SOADDPTOC", nullable = false, length = 5, align = FormatCsv.alignType.LEFT)
    private String soaddptoc;
    @Basic(optional = false)
    @FormatCsv(name = "SOADLOCC", nullable = false, length = 9, fillSpace = "0")
    private int soadlocc;
    @Basic(optional = false)
    @FormatCsv(name = "SOADCOTEPC", nullable = false, length = 3, fillSpace = "0")
    private short soadcotepc;
    @Basic(optional = false)
    @FormatCsv(name = "SOADCOTEAC", nullable = false, length = 5, fillSpace = "0")
    private int soadcoteac;
    @Basic(optional = false)
    @FormatCsv(name = "SOADTELC", nullable = false, length = 12, fillSpace = "0")
    private long soadtelc;
    @Basic(optional = false)
    @FormatCsv(name = "SOADTEINC", nullable = false, length = 6, fillSpace = "0")
    private int soadteinc;
    @Basic(optional = false)
    @FormatCsv(name = "SOADCOCEPC", nullable = false, length = 3, fillSpace = "0")
    private short soadcocepc;
    @Basic(optional = false)
    @FormatCsv(name = "SOADCELC", nullable = false, length = 12, fillSpace = "0")
    private long soadcelc;
    @FormatCsv(name = "FILL", nullable = false, length = 56)
    private String soadfill="";
    @Basic(optional = false)
    @FormatCsv(name = "SOADCOERC", nullable = false, length = 3)
    private String soadcoerc;

    public String getSoadtirec() {
        return soadtirec;
    }

    public void setSoadtirec(String soadtirec) {
        this.soadtirec = soadtirec;
    }

    public short getSoadtidicc() {
        return soadtidicc;
    }

    public void setSoadtidicc(short soadtidicc) {
        this.soadtidicc = soadtidicc;
    }

    public short getSoadticac() {
        return soadticac;
    }

    public void setSoadticac(short soadticac) {
        this.soadticac = soadticac;
    }

    public String getSoadcallec() {
        return soadcallec;
    }

    public void setSoadcallec(String soadcallec) {
        this.soadcallec = soadcallec;
    }

    public String getSoadnupuc() {
        return soadnupuc;
    }

    public void setSoadnupuc(String soadnupuc) {
        this.soadnupuc = soadnupuc;
    }

    public String getSoadinadc() {
        return soadinadc;
    }

    public void setSoadinadc(String soadinadc) {
        this.soadinadc = soadinadc;
    }

    public String getSoadcopoc() {
        return soadcopoc;
    }

    public void setSoadcopoc(String soadcopoc) {
        this.soadcopoc = soadcopoc;
    }

    public String getSoadzonac() {
        return soadzonac;
    }

    public void setSoadzonac(String soadzonac) {
        this.soadzonac = soadzonac;
    }

    public short getSoadpaisc() {
        return soadpaisc;
    }

    public void setSoadpaisc(short soadpaisc) {
        this.soadpaisc = soadpaisc;
    }

    public String getSoaddptoc() {
        return soaddptoc;
    }

    public void setSoaddptoc(String soaddptoc) {
        this.soaddptoc = soaddptoc;
    }

    public int getSoadlocc() {
        return soadlocc;
    }

    public void setSoadlocc(int soadlocc) {
        this.soadlocc = soadlocc;
    }

    public short getSoadcotepc() {
        return soadcotepc;
    }

    public void setSoadcotepc(short soadcotepc) {
        this.soadcotepc = soadcotepc;
    }

    public int getSoadcoteac() {
        return soadcoteac;
    }

    public void setSoadcoteac(int soadcoteac) {
        this.soadcoteac = soadcoteac;
    }

    public long getSoadtelc() {
        return soadtelc;
    }

    public void setSoadtelc(long soadtelc) {
        this.soadtelc = soadtelc;
    }

    public int getSoadteinc() {
        return soadteinc;
    }

    public void setSoadteinc(int soadteinc) {
        this.soadteinc = soadteinc;
    }

    public short getSoadcocepc() {
        return soadcocepc;
    }

    public void setSoadcocepc(short soadcocepc) {
        this.soadcocepc = soadcocepc;
    }

    public long getSoadcelc() {
        return soadcelc;
    }

    public void setSoadcelc(long soadcelc) {
        this.soadcelc = soadcelc;
    }

    public String getSoadcoerc() {
        return soadcoerc;
    }

    public void setSoadcoerc(String soadcoerc) {
        this.soadcoerc = soadcoerc;
    }

    public String getSoadfill() {
        return soadfill;
    }

    public void setSoadfill(String soadfill) {
        this.soadfill = soadfill;
    }
}
