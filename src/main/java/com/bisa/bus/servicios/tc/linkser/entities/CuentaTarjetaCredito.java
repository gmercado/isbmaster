package com.bisa.bus.servicios.tc.linkser.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "TCPCTA")
public class CuentaTarjetaCredito implements Serializable {

    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "CTNUTE", nullable = false, length = 16)
    private String tarjetaCredito;
    @Basic(optional = false)
    @Column(name = "CTNOPE", nullable = false, length = 9)
    private String numeroOperacion;
    @Id
    @Basic(optional = false)
    @Column(name = "CTNCTO", nullable = false)
    private long cuentaTarjetaOriginal;
    @Basic(optional = false)
    @Column(name = "CTSUCO", nullable = false)
    private short sucursal;
    @Basic(optional = false)
    @Column(name = "CTNCTA", nullable = false)
    private long cuentaTarjeta;
    @Basic(optional = false)
    @Column(name = "CTFLGC", nullable = false)
    private Character flagConversion;
    @Basic(optional = false)
    @Column(name = "CTUSR", nullable = false, length = 10)
    private String usuario;
    @Basic(optional = false)
    @Column(name = "CTDEV", nullable = false, length = 10)
    private String terminal;
    @Basic(optional = false)
    @Column(name = "CTPGM", nullable = false, length = 10)
    private String programa;
    @Basic(optional = false)
    @Column(name = "CTFEC", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSistema;

    public String getTarjetaCredito() {
        return tarjetaCredito;
    }

    public void setTarjetaCredito(String tarjetaCredito) {
        this.tarjetaCredito = tarjetaCredito;
    }

    public String getNumeroOperacion() {
        return numeroOperacion;
    }

    public void setNumeroOperacion(String numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }

    public long getCuentaTarjetaOriginal() {
        return cuentaTarjetaOriginal;
    }

    public void setCuentaTarjetaOriginal(long cuentaTarjetaOriginal) {
        this.cuentaTarjetaOriginal = cuentaTarjetaOriginal;
    }

    public short getSucursal() {
        return sucursal;
    }

    public void setSucursal(short sucursal) {
        this.sucursal = sucursal;
    }

    public long getCuentaTarjeta() {
        return cuentaTarjeta;
    }

    public void setCuentaTarjeta(long cuentaTarjeta) {
        this.cuentaTarjeta = cuentaTarjeta;
    }

    public Character getFlagConversion() {
        return flagConversion;
    }

    public void setFlagConversion(Character flagConversion) {
        this.flagConversion = flagConversion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public Date getFechaSistema() {
        return fechaSistema;
    }

    public void setFechaSistema(Date fechaSistema) {
        this.fechaSistema = fechaSistema;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CuentaTarjetaCredito that = (CuentaTarjetaCredito) o;
        return getCuentaTarjetaOriginal() == that.getCuentaTarjetaOriginal() &&
                getSucursal() == that.getSucursal() &&
                getCuentaTarjeta() == that.getCuentaTarjeta() &&
                Objects.equals(getTarjetaCredito(), that.getTarjetaCredito()) &&
                Objects.equals(getNumeroOperacion(), that.getNumeroOperacion()) &&
                Objects.equals(getFlagConversion(), that.getFlagConversion()) &&
                Objects.equals(getUsuario(), that.getUsuario()) &&
                Objects.equals(getTerminal(), that.getTerminal()) &&
                Objects.equals(getPrograma(), that.getPrograma()) &&
                Objects.equals(getFechaSistema(), that.getFechaSistema());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getTarjetaCredito(), getNumeroOperacion(), getCuentaTarjetaOriginal(), getSucursal(), getCuentaTarjeta(), getFlagConversion(), getUsuario(), getTerminal(), getPrograma(), getFechaSistema());
    }
}
