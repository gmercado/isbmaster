package com.bisa.bus.servicios.tc.sftp.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by atenorio on 12/05/2017.
 */
public enum TipoArchivo {
    SOL("Solicitud de tarjeta ATC"),
    ALT("Alta de tarjeta ATC"),
    RAL("Respuesta alta de tarjeta ATC"),
    PAG("Envio de pago ATC"),
    PAR("Respuesta a pago ATC"),
    MOV("Datos generales y movimientos ATC"),
    CIE("Corte mensual ATC"),
    FAC("Facturas ATC"),
    SLI("Solicitud de tarjeta Linker"),
    PLI("Envio de pago Linkser"),
    ELI("Pago a establecimientos Linkser"),
    ALI("Envio de anticipo Linkser"),
    RLI("Respuesta alta de tarjeta Linkser"),
    REL("Renovaciones Linkser"),
    MLI("Datos generales y movimientos Linkser"),
    CLI("Corte mensual Linkser"),
    FLI("Facturas Linkser"),
    TLI("Transferencia ATC a Linkser");
    //BAL("Balance Diario")

    private TipoArchivo(String descripcion) {
        this.descripcion = descripcion;
    }

    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public static TipoArchivo valorEnum(String valor) {
        List<TipoArchivo> estados = new ArrayList<>(Arrays.asList((TipoArchivo.values())));
        for (TipoArchivo es : estados) {
            if (valor == es.name()) {
                return es;
            }
        }
        return null;
    }
}
