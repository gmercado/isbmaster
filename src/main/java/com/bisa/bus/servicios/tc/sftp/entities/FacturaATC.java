package com.bisa.bus.servicios.tc.sftp.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by atenorio on 03/07/2017.
 */
@Entity
@Table(name = "TCPANLVEN")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "FCFECR")),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "FCPGCR")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "FCFEMO")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "FCPGMO"))})
public class FacturaATC extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private IdFactura idFactura;
    @Basic(optional = false)
    @Column(name = "FCESPESIF", nullable = false)
    private Character especificacion;
    @Basic(optional = false)
    @Column(name = "FCNRO", nullable = false, length = 6)
    private String numeroSeq;
    @Basic(optional = false)
    @Column(name = "FCESTADO", nullable = false)
    private Character estadoFactura;

    @Basic(optional = false)
    @Column(name = "FCRSOCIAL", nullable = false, length = 150)
    private String razonSocial;
    @Basic(optional = false)
    @Column(name = "FCIMPTOT", nullable = false, length = 12)
    private String importeTotalTexto;
    @Basic(optional = false)
    @Column(name = "FCIMPOTR", nullable = false, length = 12)
    private String importeTasasTexto;
    @Basic(optional = false)
    @Column(name = "FCIMPEOE", nullable = false, length = 12)
    private String importeOperacionExentasTexto;
    @Basic(optional = false)
    @Column(name = "FCIMPVGR", nullable = false, length = 12)
    private String ventasGravadasTasaCeroTexto;
    @Basic(optional = false)
    @Column(name = "FCIMPSUBT", nullable = false, length = 12)
    private String subTotalTexto;
    @Basic(optional = false)
    @Column(name = "FCIMPDESC", nullable = false, length = 12)
    private String importeDescuentosTexto;
    @Basic(optional = false)
    @Column(name = "FCIMPBASE", nullable = false, length = 12)
    private String importeBaseTexto;
    @Basic(optional = false)
    @Column(name = "FCDFISCAL", nullable = false, length = 12)
    private String debitoFiscalTexto;
    @Basic(optional = false)
    @Column(name = "FCDCCTRL", nullable = false, length = 17)
    private String codigoControl;
    @Basic(optional = false)
    @Column(name = "FCCADM", nullable = false)
    private Character codigoAdm;
    @Basic(optional = false)
    @Column(name = "FCFEPRO1", nullable = false)
    private int fechaProceso;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "FCIMPTOTN", nullable = false, precision = 10, scale = 2)
    private BigDecimal importeTotal;
    @Basic(optional = false)
    @Column(name = "FCIMPOTRN", nullable = false, precision = 10, scale = 2)
    private BigDecimal importeTasas;
    @Basic(optional = false)
    @Column(name = "FCIMPEOEN", nullable = false, precision = 10, scale = 2)
    private BigDecimal importeOperacionExentas;
    @Basic(optional = false)
    @Column(name = "FCIMPVGRN", nullable = false, precision = 10, scale = 2)
    private BigDecimal ventasGravadasTasaCero;
    @Basic(optional = false)
    @Column(name = "FCIMPSUBTN", nullable = false, precision = 10, scale = 2)
    private BigDecimal subTotal;
    @Basic(optional = false)
    @Column(name = "FCIMPDESCN", nullable = false, precision = 10, scale = 2)
    private BigDecimal importeDescuentos;
    @Basic(optional = false)
    @Column(name = "FCIMPBASEN", nullable = false, precision = 10, scale = 2)
    private BigDecimal importeBase;
    @Basic(optional = false)
    @Column(name = "FCDFISCALN", nullable = false, precision = 10, scale = 2)
    private BigDecimal debitoFiscal;
//    @Basic(optional = false)
//    @Column(name = "FCNUTC", nullable = false)
//    private long cuentaTarjeta;
//    @Basic(optional = false)
//    @Column(name = "FCNUT1", nullable = false, length = 9)
//    private String cuentaTarjetaCorto;
    @Basic(optional = false)
    @Column(name = "FCEST", nullable = false, length = 4)
    private String estado;
    @Basic(optional = false)
    @Column(name = "FCIDRE", nullable = false)
    private long idArchivoRecepcion;
    @Basic(optional = false)
    @Column(name = "FCFERE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRecepcion;
//    @Basic(optional = false)
//    @Column(name = "FCPGCR", nullable = false, length = 10)
//    private String usuarioCreador;
//    @Basic(optional = false)
//    @Column(name = "FCFECR", nullable = false)
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date fechaCreacion;
//    @Basic(optional = false)
//    @Column(name = "FCPGMO", nullable = false, length = 10)
//    private String usuarioModificador;
//    @Basic(optional = false)
//    @Column(name = "FCFEMO", nullable = false)
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date fechaModificacion;

    public FacturaATC() {
    }


    public Character getEspecificacion() {
        return especificacion;
    }

    public void setEspecificacion(Character especificacion) {
        this.especificacion = especificacion;
    }

    public String getNumeroSeq() {
        return numeroSeq;
    }

    public void setNumeroSeq(String numeroSeq) {
        this.numeroSeq = numeroSeq;
    }



    public Character getEstadoFactura() {
        return estadoFactura;
    }

    public void setEstadoFactura(Character estadoFactura) {
        this.estadoFactura = estadoFactura;
    }



    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getImporteTotalTexto() {
        return importeTotalTexto;
    }

    public void setImporteTotalTexto(String importeTotalTexto) {
        this.importeTotalTexto = importeTotalTexto;
    }

    public String getImporteTasasTexto() {
        return importeTasasTexto;
    }

    public void setImporteTasasTexto(String importeTasasTexto) {
        this.importeTasasTexto = importeTasasTexto;
    }

    public String getImporteOperacionExentasTexto() {
        return importeOperacionExentasTexto;
    }

    public void setImporteOperacionExentasTexto(String importeOperacionExentasTexto) {
        this.importeOperacionExentasTexto = importeOperacionExentasTexto;
    }

    public String getVentasGravadasTasaCeroTexto() {
        return ventasGravadasTasaCeroTexto;
    }

    public void setVentasGravadasTasaCeroTexto(String ventasGravadasTasaCeroTexto) {
        this.ventasGravadasTasaCeroTexto = ventasGravadasTasaCeroTexto;
    }

    public String getSubTotalTexto() {
        return subTotalTexto;
    }

    public void setSubTotalTexto(String subTotalTexto) {
        this.subTotalTexto = subTotalTexto;
    }

    public String getImporteDescuentosTexto() {
        return importeDescuentosTexto;
    }

    public void setImporteDescuentosTexto(String importeDescuentosTexto) {
        this.importeDescuentosTexto = importeDescuentosTexto;
    }

    public String getImporteBaseTexto() {
        return importeBaseTexto;
    }

    public void setImporteBaseTexto(String importeBaseTexto) {
        this.importeBaseTexto = importeBaseTexto;
    }

    public String getDebitoFiscalTexto() {
        return debitoFiscalTexto;
    }

    public void setDebitoFiscalTexto(String debitoFiscalTexto) {
        this.debitoFiscalTexto = debitoFiscalTexto;
    }

    public String getCodigoControl() {
        return codigoControl;
    }

    public void setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
    }

    public Character getCodigoAdm() {
        return codigoAdm;
    }

    public void setCodigoAdm(Character codigoAdm) {
        this.codigoAdm = codigoAdm;
    }

    public int getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(int fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public BigDecimal getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(BigDecimal importeTotal) {
        this.importeTotal = importeTotal;
    }

    public BigDecimal getImporteTasas() {
        return importeTasas;
    }

    public void setImporteTasas(BigDecimal importeTasas) {
        this.importeTasas = importeTasas;
    }

    public BigDecimal getImporteOperacionExentas() {
        return importeOperacionExentas;
    }

    public void setImporteOperacionExentas(BigDecimal importeOperacionExentas) {
        this.importeOperacionExentas = importeOperacionExentas;
    }

    public BigDecimal getVentasGravadasTasaCero() {
        return ventasGravadasTasaCero;
    }

    public void setVentasGravadasTasaCero(BigDecimal ventasGravadasTasaCero) {
        this.ventasGravadasTasaCero = ventasGravadasTasaCero;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getImporteDescuentos() {
        return importeDescuentos;
    }

    public void setImporteDescuentos(BigDecimal importeDescuentos) {
        this.importeDescuentos = importeDescuentos;
    }

    public BigDecimal getImporteBase() {
        return importeBase;
    }

    public void setImporteBase(BigDecimal importeBase) {
        this.importeBase = importeBase;
    }

    public BigDecimal getDebitoFiscal() {
        return debitoFiscal;
    }

    public void setDebitoFiscal(BigDecimal debitoFiscal) {
        this.debitoFiscal = debitoFiscal;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public long getIdArchivoRecepcion() {
        return idArchivoRecepcion;
    }

    public void setIdArchivoRecepcion(long idArchivoRecepcion) {
        this.idArchivoRecepcion = idArchivoRecepcion;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

//    @Override
//    public String getUsuarioCreador() {
//        return usuarioCreador;
//    }
//
//    @Override
//    public void setUsuarioCreador(String usuarioCreador) {
//        this.usuarioCreador = usuarioCreador;
//    }
//
//    @Override
//    public Date getFechaCreacion() {
//        return fechaCreacion;
//    }
//
//    @Override
//    public void setFechaCreacion(Date fechaCreacion) {
//        this.fechaCreacion = fechaCreacion;
//    }
//
//    @Override
//    public String getUsuarioModificador() {
//        return usuarioModificador;
//    }
//
//    @Override
//    public void setUsuarioModificador(String usuarioModificador) {
//        this.usuarioModificador = usuarioModificador;
//    }
//
//    @Override
//    public Date getFechaModificacion() {
//        return fechaModificacion;
//    }
//
//    @Override
//    public void setFechaModificacion(Date fechaModificacion) {
//        this.fechaModificacion = fechaModificacion;
//    }

    public IdFactura getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(IdFactura idFactura) {
        this.idFactura = idFactura;
    }

    @Override
    public String toString() {
        return "Factura{" +
                "idFactura=" + idFactura +
                ", especificacion=" + especificacion +
                ", numeroSeq='" + numeroSeq + '\'' +
                ", estadoFactura=" + estadoFactura +
                ", razonSocial='" + razonSocial + '\'' +
                ", importeTotalTexto='" + importeTotalTexto + '\'' +
                ", importeTasasTexto='" + importeTasasTexto + '\'' +
                ", importeOperacionExentasTexto='" + importeOperacionExentasTexto + '\'' +
                ", ventasGravadasTasaCeroTexto='" + ventasGravadasTasaCeroTexto + '\'' +
                ", subTotalTexto='" + subTotalTexto + '\'' +
                ", importeDescuentosTexto='" + importeDescuentosTexto + '\'' +
                ", importeBaseTexto='" + importeBaseTexto + '\'' +
                ", debitoFiscalTexto='" + debitoFiscalTexto + '\'' +
                ", codigoControl='" + codigoControl + '\'' +
                ", codigoAdm=" + codigoAdm +
                ", fechaProceso=" + fechaProceso +
                ", importeTotal=" + importeTotal +
                ", importeTasas=" + importeTasas +
                ", importeOperacionExentas=" + importeOperacionExentas +
                ", ventasGravadasTasaCero=" + ventasGravadasTasaCero +
                ", subTotal=" + subTotal +
                ", importeDescuentos=" + importeDescuentos +
                ", importeBase=" + importeBase +
                ", debitoFiscal=" + debitoFiscal +
                ", estado='" + estado + '\'' +
                ", idArchivoRecepcion=" + idArchivoRecepcion +
                ", fechaRecepcion=" + fechaRecepcion +
                '}';
    }
}
