package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.IdSaldoMensual;
import com.bisa.bus.servicios.tc.sftp.entities.SaldoMensual;
import com.google.inject.Inject;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by atenorio on 22/06/2017.
 */
public class SaldoMensualDao extends DaoImpl<SaldoMensual, IdSaldoMensual> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SaldoMensualDao.class);

    @Inject
    protected SaldoMensualDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(SaldoMensual registro, String user) {
        registro.setUsuarioCreador(user);
        registro.setFechaModificacion(new Date());
        persist(registro);
        return true;
    }

    public boolean limpiarTodo() {
        LOGGER.debug("Limpiando tabla SaldoMensual.");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<SaldoMensual> query = entityManager.createQuery("DELETE FROM SaldoMensual");//
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }

    public Boolean verificarCargaCorrecta() {
        LOGGER.debug("Verificar carga de todas las sucursales.");
        try {
            return doWithTransaction(
                    new WithTransaction<Boolean>() {
                        @Override
                        public Boolean call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {

                            String sql = "SELECT DISTINCT SOGASUCU1 FROM ZTARJETA/TCPASOLIC WHERE SOGASUCU1 NOT IN (SELECT DISTINCT SMSUCEM FROM ZTARJETA/TCPANSAME)";
                            Query q = entityManager.createNativeQuery(sql, String.class);
                            List<String> totalPagosBoa = null;
                            try {
                                totalPagosBoa = (List<String>) q.getResultList();
                                if (totalPagosBoa.size() == 0) return true;
                            } catch (NonUniqueResultException e) {
                                LOGGER.error("Error al verificar consitencia de carga de archivo.", e);
                                return false;
                            } catch (Exception e) {
                                LOGGER.error("Error al verificar consitencia de carga de archivo.", e);
                                return false;
                            }
                            return false;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }

    public List<SaldoMensual> getListaSinCuentaBanco(){
        LOGGER.debug("Obteniendo solicitudes sin cuenta de banco.");
        return doWithTransaction(
                (entityManager, t) -> {
                    StringBuilder sbSql = new StringBuilder();
                    sbSql.append("SELECT * FROM TCPANSAME WHERE  CAST(SMNUTCNC AS int) NOT IN ");
                    sbSql.append("((SELECT SOGANUTCNC FROM TCPASOLIC WHERE SONUTC <> 0) union (SELECT SOGANUTCNC FROM TCPASOLIE WHERE SONUTC <> 0))");
                    TypedQuery<SaldoMensual> query = entityManager.createNativeQuery(sbSql.toString(), SaldoMensual.class);
                    return query.getResultList();
                }
        );
    }
}
