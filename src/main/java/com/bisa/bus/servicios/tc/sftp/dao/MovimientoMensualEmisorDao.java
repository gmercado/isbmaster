package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.IdMovimientoMensualEmisor;
import com.bisa.bus.servicios.tc.sftp.entities.MovimientoMensualEmisor;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 26/06/2017.
 */
public class MovimientoMensualEmisorDao  extends DaoImpl<MovimientoMensualEmisor, IdMovimientoMensualEmisor> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovimientoMensualEmisorDao.class);

    @Inject
    protected MovimientoMensualEmisorDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(MovimientoMensualEmisor registro, String user) {
        registro.setUsuarioCreador(user);
        registro.setFechaModificacion(new Date());
        persist(registro);
        return true;
    }
}
