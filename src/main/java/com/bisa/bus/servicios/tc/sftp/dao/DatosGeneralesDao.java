package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.DatosGeneralesTC;
import com.bisa.bus.servicios.tc.sftp.entities.IdDatosGenerales;
import com.bisa.bus.servicios.tc.sftp.entities.RespuestaSolicitudTarjeta;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 09/06/2017.
 */
public class DatosGeneralesDao extends DaoImpl<DatosGeneralesTC, IdDatosGenerales> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatosGeneralesDao.class);

    @Inject
    protected DatosGeneralesDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }


    public boolean guardar(DatosGeneralesTC registro, String user) {

            registro.setUsuarioCreador(user);
            registro.setFechaModificacion(new Date());
            persist(registro);

        return true;
    }

    public boolean limpiarTodo() {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
//                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//                        CriteriaQuery<DatosGeneralesTC> q = cb.createQuery(DatosGeneralesTC.class);
//                        Root<DatosGeneralesTC> p = q.from(DatosGeneralesTC.class);
//                        TypedQuery<DatosGeneralesTC> query = entityManager.createQuery(q);
//                        List<DatosGeneralesTC> lista = query.getResultList();
//                        for (DatosGeneralesTC cuenta: lista) {
//                            entityManager.remove(cuenta);
//                        }
                        TypedQuery<DatosGeneralesTC> query = entityManager.createQuery("DELETE FROM DatosGeneralesTC");//
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }
}
