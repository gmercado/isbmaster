package com.bisa.bus.servicios.tc.linkser.model;

/**
 * Created by atenorio on 13/01/2017.
 */
public class ConsultaMovimientoSolicitud implements IConsultaMovimientoSolicitud {
    private String cuenta;
    private String numeroTarjeta;
    private int periodo;

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }
}
