package com.bisa.bus.servicios.tc.linkser.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 10/11/2017.
 */
@Entity
@Table(name = "TCPPATXT")
public class PagosTcLinkserTxt implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "PAGOS", nullable = false)
    private String texto;

    public PagosTcLinkserTxt(String texto) {
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
