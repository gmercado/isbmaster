package com.bisa.bus.servicios.tc.linkser.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.file.ArchivoBase;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.tc.linkser.dao.MovimientosLinkserDao;
import com.bisa.bus.servicios.tc.linkser.entities.MovimientosLinkser;
import com.bisa.bus.servicios.tc.linkser.utils.MapeoArchivo;
import com.bisa.bus.servicios.tc.sftp.api.SftpTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by atenorio on 20/11/2017.
 */
public class SftpRecepcionMovimientos implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpRecepcionMovimientos.class);

    private final MedioAmbiente medioAmbiente;
    private final ArchivoBase archivoBase;
    private final NotificacionesCorreo notificacionesCorreo;
    private final MovimientosLinkserDao iMovimientosLinkserDao;

    ISftpConfiguracionLinkser configuracion;
    SftpTransferencia sftpTransferencia;
    private String nombreArchivoProcesado = "";
    private int totalRegistros = 0;

    @Inject
    public SftpRecepcionMovimientos(ArchivoBase archivoBase,
                                       MedioAmbiente medioAmbiente,
                                       NotificacionesCorreo notificacionesCorreo, MovimientosLinkserDao iMovimientosLinkserDao,
                                       ISftpConfiguracionLinkser configuracion, SftpTransferencia sftpTransferencia) {
        this.medioAmbiente = medioAmbiente;
        this.archivoBase = archivoBase;
        this.notificacionesCorreo = notificacionesCorreo;
        this.iMovimientosLinkserDao = iMovimientosLinkserDao;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para procesar recepcion de archivo de movimientos de tarjetas de servidor remoto SFTP.
     * TX081117  RSddMMyyyy
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    String PREF = "#fecha";

    public boolean procesarArchivo() {
        boolean ok = false;
        String detalle = "";
        LOGGER.debug("Inicia proceso recepcion de archivo de solicitud");
        LOGGER.debug("1) Verificar archivo pendiente de respuesta");
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_MOVIMIENTOS_LINKSER, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_MOVIMIENTOS_LINKSER_DEFAULT);
        String nombreArchivoRegexForzado = "NULL";
        String nombreArchivo[] = Iterables.toArray(Splitter.on("|").trimResults().split(nombreArchivoRegex), String.class);

        String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_LINKSER, Variables.FECHA_DE_PROCESO_LINKSER_DEFAULT);
        // La nomenclatura de nombres no solo usa mmyyyy por lo que se usa la fecha de ejecución de tarea
        Calendar hoy = Calendar.getInstance();
        hoy.add(Calendar.DAY_OF_YEAR, -1);
        Date fechaProcesar = hoy.getTime();

        if (StringUtils.isEmpty(nombreArchivoRegexForzado) || "NULL".equals(nombreArchivoRegexForzado)) {
            // Procesar a fecha determinada por parametro
            if (StringUtils.isNotEmpty(fechaProceso) && !"0".equals(fechaProceso)) {
                fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
                if (fechaProcesar == null) {
                    notificacionesCorreo.notificarError("Error en fecha de proceso recepci\u00F3n alta", "");
                    return false;
                }
                // Disminuir un dia de la fecha programada
                hoy.setTime(fechaProcesar);
                hoy.add(Calendar.DAY_OF_YEAR, -1);
                fechaProcesar = hoy.getTime();
            }
            nombreArchivoRegex = nombreArchivo[0].replace(PREF, new SimpleDateFormat(nombreArchivo[1]).format(fechaProcesar));
        } else {
            nombreArchivoRegex = nombreArchivoRegexForzado;
        }
        LOGGER.debug("2) Decargar archivo de servidor SFTP");

        prepararConfiguracionSftp();
        LOGGER.info("2.1) Descarga de archivo de a servidor SFTP con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.MLI);

        LOGGER.debug("3) Validar archivo descargado");
        LOGGER.debug("3.1) Validar estructura");
        LOGGER.debug("3.2) Validar mapeo");

        // Cerrar el registro con error y permitir reintento
        if (!ok || sftpTransferencia.getArchivoLocal() == null) {
            sftpTransferencia.transferenciaConErrorReintento("");
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.MLI, "No existen archivos de respuesta en servidor SFTP.");
            LOGGER.warn("No existen archivos de respuesta en servidor SFTP.");
            return false;
        }
        if (!iMovimientosLinkserDao.limpiarTodo()) {
            detalle = "Error al limpiar tabla temporal.";
            LOGGER.error(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.MLI, detalle);
            return false;
        }

        // Procesamiento de archivo local
        File archivo = sftpTransferencia.getArchivoLocal();
        List<String> mapeo = MapeoArchivo.mapearArchivo(archivo);
        // Verificar si existen problemas en mape de archivo de respuesta
        if (mapeo == null || mapeo.size() == 0) {
            LOGGER.warn("Archivo de respuesta sin datos");
            sftpTransferencia.transferenciaCompleta("");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.MLI, "Archivo de respuesta sin datos");
            return true;
        }


        LOGGER.debug("4) Actualizar tabla de solicitudes");
        LOGGER.debug("4.1) Verificar si un registro se encuentra en un estado inconsistente");


        for (String solicitud : mapeo) {
            // Validar que se realizo el mapeo de forma exitosa en todas las estructuras
            if (solicitud == null) {
                LOGGER.error("Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                sftpTransferencia.transferenciaConErrorReintento("");
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.MLI, "Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                return false;
            }
        }



        //for (SolicitudCompleto solicitud : solicitudes) {
        for (String datos: mapeo) {

            // Grabar
            if (ok) {
                LOGGER.debug("====================================");
                LOGGER.debug("Datos:{}", datos);
                iMovimientosLinkserDao.persist(new MovimientosLinkser(datos));
            }
            totalRegistros = totalRegistros + 1;
        }

        // Eliminar archivo de servidor local
        // TODO ATC eliminar archivo remoto
        /*
        if (FileUtils.deleteQuietly(archivo)) {
            LOGGER.info("Se ha eliminado el archivo de altas, {}", archivo.getName());
        }*/

        LOGGER.debug("4.2) Verificar si un registro se encuentra en un estado inconsistente");

        // Modificar estado de solicitudes de alta y notificar por correo
        if (ok) {
            sftpTransferencia.transferenciaCompleta("");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.MLI, "Transferencia completa con respuesta a " + totalRegistros + " movimientos.");
            LOGGER.info("Proceso finalizado de recepcion de movimientos.");
        } else {
            detalle = "Error en proceso de recepcion de movimientos.";
            LOGGER.warn(detalle);
            sftpTransferencia.transferenciaConErrorInterno(detalle);
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.MLI, detalle);
        }
        return ok;
    }

    public boolean prepararConfiguracionSftp() {
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_MOVIMIENTOS_LINKSER, Variables.FTP_RUTA_RECEPCION_MOVIMIENTOS_LINKSER_DEFAULT));
        return sftpTransferencia.configurar(configuracion);
    }

    public String getNombreArchivoProcesado() {
        return sftpTransferencia.getNombreArchivoRemotoFecha();
    }

    public int getTotalRegistros() {
        return totalRegistros;
    }
}
