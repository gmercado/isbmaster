package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 26/05/2017.
 */
@Entity
@Table(name = "TCPANLOC")
public class LocalidadesAtc implements Serializable{
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private IdLocalidadAtc id;

    @Basic(optional = false)
    @Column(name = "T15LOCAN", nullable = false, length = 40)
    private String t15locan;
    @Basic(optional = false)
    @Column(name = "T15BRCH", nullable = false)
    private short t15brch;
    @Basic(optional = false)
    @Column(name = "NCOLOC", nullable = false)
    private int ncoloc;
    @Basic(optional = false)
    @Column(name = "NCODEPTO", nullable = false, length = 5)
    private String ncodepto;
    @Basic(optional = false)
    @Column(name = "REUSR", nullable = false, length = 10)
    private String reusr;
    @Basic(optional = false)
    @Column(name = "REDEV", nullable = false, length = 10)
    private String redev;
    @Basic(optional = false)
    @Column(name = "REPGM", nullable = false, length = 10)
    private String repgm;
    @Basic(optional = false)
    @Column(name = "REFEC", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date refec;


    public LocalidadesAtc() {
    }


    public String getT15locan() {
        return t15locan;
    }

    public void setT15locan(String t15locan) {
        this.t15locan = t15locan;
    }

    public short getT15brch() {
        return t15brch;
    }

    public void setT15brch(short t15brch) {
        this.t15brch = t15brch;
    }

    public int getNcoloc() {
        return ncoloc;
    }

    public void setNcoloc(int ncoloc) {
        this.ncoloc = ncoloc;
    }

    public String getNcodepto() {
        return ncodepto;
    }

    public void setNcodepto(String ncodepto) {
        this.ncodepto = ncodepto;
    }

    public String getReusr() {
        return reusr;
    }

    public void setReusr(String reusr) {
        this.reusr = reusr;
    }

    public String getRedev() {
        return redev;
    }

    public void setRedev(String redev) {
        this.redev = redev;
    }

    public String getRepgm() {
        return repgm;
    }

    public void setRepgm(String repgm) {
        this.repgm = repgm;
    }

    public Date getRefec() {
        return refec;
    }

    public void setRefec(Date refec) {
        this.refec = refec;
    }


    @Override
    public String toString() {
        return "LocalidadesTC[" +
                " codDepto=" + id.getCodDepatamento() +
                ", codLoc="+id.getCodLocalidad()+" ]";
    }

}
