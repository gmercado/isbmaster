package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.IdSaldoMensual;
import com.bisa.bus.servicios.tc.sftp.entities.SaldoMensualHistorico;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 28/06/2017.
 */
public class SaldoMensualHistoricoDao extends DaoImpl<SaldoMensualHistorico, IdSaldoMensual> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SaldoMensualHistoricoDao.class);

    @Inject
    protected SaldoMensualHistoricoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(SaldoMensualHistorico registro, String user) {
        registro.setUsuarioCreador(user);
        registro.setFechaModificacion(new Date());
        persist(registro);
        return true;
    }
}