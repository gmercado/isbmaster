package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 25/05/2017.
 * Registro de cuenta, nro de tarjeta, sucursal
 */
@Entity
@Table(name = "TCPINIARC")
public class RespuestaSolicitudCuenta implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "SADATC", nullable = false)
    private String texto;

    public RespuestaSolicitudCuenta(String texto){
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
