package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import javax.persistence.Basic;


/**
 * Created by atenorio on 18/05/2017.
 */
public class SolicitudProducto {

    @Basic(optional = false)
    @FormatCsv(name = "SOPRTIRE", nullable = false, length = 2)
    private String soprtire;
    @Basic(optional = false)
    @FormatCsv(name = "SOPRCOPR", nullable = false, length = 10, align = FormatCsv.alignType.LEFT)
    private String soprcopr;
    @Basic(optional = false)
    @FormatCsv(name = "SOPRGRAF", nullable = false, length = 4, fillSpace = "0")
    private String soprgraf;
    @Basic(optional = false)
    @FormatCsv(name = "SOPRNUTAEX", nullable = false, length = 19)
    private String soprnutaex;
    @Basic(optional = false)
    @FormatCsv(name = "SOPRFEVEEX", nullable = false, length = 6)
    private String soprfeveex;
    @Basic(optional = false)
    @FormatCsv(name = "SOPRBIN", nullable = false, length = 19)
    private String soprbin;
    @Basic(optional = false)
    @FormatCsv(name = "SOPRNUTCEN", nullable = false, length = 36)
    private String soprnutcen;
    @Basic(optional = false)
    @FormatCsv(name = "SOPRINSEG", nullable = false,length = 1)
    private short soprinseg;
    @FormatCsv(name = "FILL", nullable = false, length = 271)
    private String soprfill="";
    @Basic(optional = false)
    @FormatCsv(name = "SOPRNUTC", nullable = false, length = 19)
    private String soprnutc;
    @Basic(optional = false)
    @FormatCsv(name = "SOPRFEVETC", nullable = false, length = 6)
    private String soprfevetc;
    @Basic(optional = false)
    @FormatCsv(name = "SOPRSENUTC", nullable = false, length = 4, fillSpace = "0")
    private short soprsenutc;
    @Basic(optional = false)
    @FormatCsv(name = "SOPRCOER", nullable = false, length = 3)
    private String soprcoer;

    public String getSoprtire() {
        return soprtire;
    }

    public void setSoprtire(String soprtire) {
        this.soprtire = soprtire;
    }

    public String getSoprcopr() {
        return soprcopr;
    }

    public void setSoprcopr(String soprcopr) {
        this.soprcopr = soprcopr;
    }

    public String getSoprgraf() {
        return soprgraf;
    }

    public void setSoprgraf(String soprgraf) {
        this.soprgraf = soprgraf;
    }

    public String getSoprnutaex() {
        return soprnutaex;
    }

    public void setSoprnutaex(String soprnutaex) {
        this.soprnutaex = soprnutaex;
    }

    public String getSoprfeveex() {
        return soprfeveex;
    }

    public void setSoprfeveex(String soprfeveex) {
        this.soprfeveex = soprfeveex;
    }

    public String getSoprbin() {
        return soprbin;
    }

    public void setSoprbin(String soprbin) {
        this.soprbin = soprbin;
    }

    public String getSoprnutcen() {
        return soprnutcen;
    }

    public void setSoprnutcen(String soprnutcen) {
        this.soprnutcen = soprnutcen;
    }

    public short getSoprinseg() {
        return soprinseg;
    }

    public void setSoprinseg(short soprinseg) {
        this.soprinseg = soprinseg;
    }

    public String getSoprnutc() {
        return soprnutc;
    }

    public void setSoprnutc(String soprnutc) {
        this.soprnutc = soprnutc;
    }

    public String getSoprfevetc() {
        return soprfevetc;
    }

    public void setSoprfevetc(String soprfevetc) {
        this.soprfevetc = soprfevetc;
    }

    public short getSoprsenutc() {
        return soprsenutc;
    }

    public void setSoprsenutc(short soprsenutc) {
        this.soprsenutc = soprsenutc;
    }

    public String getSoprcoer() {
        return soprcoer;
    }

    public void setSoprcoer(String soprcoer) {
        this.soprcoer = soprcoer;
    }

    public String getSoprfill() {
        return soprfill;
    }

    public void setSoprfill(String soprfill) {
        this.soprfill = soprfill;
    }
}
