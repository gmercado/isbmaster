package com.bisa.bus.servicios.tc.linkser.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 16/11/2017.
 */
@Entity
@Table(name = "TCPCONTX")
public class CierreConsumosLinkserTxt implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "CONSUM", nullable = false, length = 250)
    private String texto;

    public CierreConsumosLinkserTxt(String texto){
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
