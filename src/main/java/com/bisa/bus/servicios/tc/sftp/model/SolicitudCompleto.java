package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by atenorio on 25/05/2017.
 */

public class SolicitudCompleto {
    
    @FormatCsv(name = "SOGATIRE", nullable = false, length = 2)
    private String sogatire;
    
    @FormatCsv(name = "SOGAGRPR", nullable = false, length = 9, fillSpace = "0")
    private int sogagrpr;
    
    @FormatCsv(name = "SOGAPRCU", nullable = false, length = 1)
    private short sogaprcu;
    
    @FormatCsv(name = "SOGATICU", nullable = false, length = 1)
    private short sogaticu;
    
    @FormatCsv(name = "SOGACOEM", nullable = false, length = 4, fillSpace = "0")
    private String sogacoem;
    
    @FormatCsv(name = "SOGASUCU1", nullable = false, length = 9, align = FormatCsv.alignType.LEFT)
    private String sogasucu1;
    
    @FormatCsv(name = "SOGAAGEM", nullable = false, length = 5)
    private String sogaagem;
    
    @FormatCsv(name = "SOGAINEC", nullable = false, length = 1)
    private short sogainec;
    
    @FormatCsv(name = "SOGAINECM", nullable = false, length = 1)
    private short sogainecm;
    
    @FormatCsv(name = "SOGAEMAIL", nullable = false, length = 60)
    private String sogaemail;
    
    @FormatCsv(name = "SOGAINSE", nullable = false, length = 1)
    private short sogainse;
    
    @FormatCsv(name = "SOGAFILL", nullable = false, length = 283)
    private String sogafill="";
    
    @FormatCsv(name = "SOGANUTCNC", nullable = false, length = 20)
    private BigInteger soganutcnc;
    
    @FormatCsv(name = "SOGACOER", nullable = false, length = 3)
    private String sogacoer;
    
    @FormatCsv(name = "SOCCTIRE", nullable = false, length = 2)
    private String socctire;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    
    @FormatCsv(name = "SOCCLICR", nullable = false, pattern = "##.000000", scale = 6, length = 18, fillSpace = "0") //precision = 18, scale = 6
    private BigDecimal socclicr;
    
    @FormatCsv(name = "SOCCMOLI", nullable = false, length = 4, fillSpace = "0")
    private short soccmoli;
    
    @FormatCsv(name = "SOCCFECI", nullable = false, length = 4, fillSpace = "0")
    private short soccfeci;
    
    @FormatCsv(name = "SOCCFOPA", nullable = false, length = 1)
    private short soccfopa;
    
    @FormatCsv(name = "SOCCTIDE", nullable = false, length = 1)
    private short socctide;
    
    @FormatCsv(name = "FILL", nullable = false, length = 367)
    private String soccfill="";
    
    @FormatCsv(name = "SOCCCOER", nullable = false, length = 3)
    private String socccoer;

    
    @FormatCsv(name = "SOCFTIRE", nullable = false, length = 2)
    private String socftire;
    
    @FormatCsv(name = "SOCFTICL", nullable = false, length = 1)
    private short socfticl;
    
    @FormatCsv(name = "SOCFNOCL", nullable = false, length = 60)
    private String socfnocl;
    
    @FormatCsv(name = "SOCFAPCL", nullable = false, length = 60)
    private String socfapcl;
    
    @FormatCsv(name = "SOCFTIDO", nullable = false, length = 4)
    private short socftido;
    
    @FormatCsv(name = "SOCFDOCL", nullable = false, length = 25)
    private String socfdocl;
    
    @FormatCsv(name = "SOCFTIDOA", nullable = false, length = 4)
    private short socftidoa;
    
    @FormatCsv(name = "SOCFDOCLA", nullable = false, length = 25)
    private String socfdocla;
    
    @FormatCsv(name = "SOCFFENA", nullable = false, length = 8)
    private String socffena;
    
    @FormatCsv(name = "SOCFESCI", nullable = false, length = 1, fillSpace = "0")
    private short socfesci;
    
    @FormatCsv(name = "SOCFGECL", nullable = false, length = 1, fillSpace = "0")
    private short socfgecl;
    
    @FormatCsv(name = "SOCFPACL", nullable = false, length = 3, fillSpace = "0")
    private short socfpacl;
    
    @FormatCsv(name = "SOCFLUNA", nullable = false, length = 40)
    private String socfluna;
    
    @FormatCsv(name = "SOCFCAHI", nullable = false, length = 2)
    private short socfcahi;
    
    @FormatCsv(name = "SOCFNOEM", nullable = false, length = 25)
    private String socfnoem;
    
    @FormatCsv(name = "SOCFEMAIL", nullable = false, length = 60)
    private String socfemail;
    
    @FormatCsv(name = "SOCFDCPA", nullable = false, length = 1)
    private short socfdcpa;
    
    @FormatCsv(name = "SOCFDFPA", nullable = false, length = 1)
    private short socfdfpa;
    
    @FormatCsv(name = "SOCFNUSOL", nullable = false, length = 9)
    private int socfnusol;
    @FormatCsv(name = "FILL", nullable = false, length = 65)
    private String socffill="";
    
    @FormatCsv(name = "SOCFCOER", nullable = false, length = 3)
    private String socfcoer;

    @FormatCsv(name = "SOEITIRE", nullable = false, length = 2)
    private String soeitire;

    @FormatCsv(name = "FILL", nullable = false, length = 395)
    private String soeifill="";

    @FormatCsv(name = "SOEICOER", nullable = false, length = 3)
    private String soeicoer;
    
    @FormatCsv(name = "SOADTIREP", nullable = false, length = 2)
    private String soadtirep;
    
    @FormatCsv(name = "SOADTIDICP", nullable = false, length = 1)
    private short soadtidicp;
    // TODO ejemplo con codigo de calle en blanco
    
    @FormatCsv(name = "SOADTICAP", nullable = false, length = 4)
    private short soadticap;
    
    @FormatCsv(name = "SOADCALLEP", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadcallep;
    
    @FormatCsv(name = "SOADNUPUP", nullable = false, length = 10, align = FormatCsv.alignType.LEFT)
    private String soadnupup;
    
    @FormatCsv(name = "SOADINADP", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadinadp;
    
    @FormatCsv(name = "SOADCOPOP", nullable = false, length = 6)
    private String soadcopop;
    
    @FormatCsv(name = "SOADZONAP", nullable = false, length = 60, align = FormatCsv.alignType.LEFT)
    private String soadzonap;
    
    @FormatCsv(name = "SOADPAISP", nullable = false, length = 3, fillSpace = "0")
    private short soadpaisp;
    
    @FormatCsv(name = "SOADDPTOP", nullable = false, length = 5, align = FormatCsv.alignType.LEFT)
    private String soaddptop;
    
    @FormatCsv(name = "SOADLOCP", nullable = false, length = 9, fillSpace = "0")
    private int soadlocp;
    
    @FormatCsv(name = "SOADCOTEPP", nullable = false, length = 3)
    private short soadcotepp;

    
    @FormatCsv(name = "SOADCOTEAP", nullable = false, length = 5)
    private int soadcoteap;
    
    @FormatCsv(name = "SOADTELP", nullable = false, length = 12)
    private long soadtelp;
    
    @FormatCsv(name = "SOADTEINP", nullable = false, length = 6)
    private int soadteinp;
    
    @FormatCsv(name = "SOADCOCEPP", nullable = false, length = 3)
    private short soadcocepp;
    
    @FormatCsv(name = "SOADCELP", nullable = false, length = 12)
    private long soadcelp;

    @FormatCsv(name = "FILL", nullable = false, length = 56)
    private String soadfill="";
    
    @FormatCsv(name = "SOADCOERP", nullable = false, length = 3)
    private String soadcoerp;

    
    @FormatCsv(name = "SOADTIREC", nullable = false, length = 2)
    private String soadtirec;
    
    @FormatCsv(name = "SOADTIDICC", nullable = false, length = 1)
    private short soadtidicc;
    
    @FormatCsv(name = "SOADTICAC", nullable = false, length = 4)
    private short soadticac;
    
    @FormatCsv(name = "SOADCALLEC", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadcallec;
    
    @FormatCsv(name = "SOADNUPUC", nullable = false, length = 10, align = FormatCsv.alignType.LEFT)
    private String soadnupuc;
    
    @FormatCsv(name = "SOADINADC", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadinadc;
    
    @FormatCsv(name = "SOADCOPOC", nullable = false, length = 6)
    private String soadcopoc;
    
    @FormatCsv(name = "SOADZONAC", nullable = false, length = 60, align = FormatCsv.alignType.LEFT)
    private String soadzonac;
    
    @FormatCsv(name = "SOADPAISC", nullable = false, length = 3, fillSpace = "0")
    private short soadpaisc;
    
    @FormatCsv(name = "SOADDPTOC", nullable = false, length = 5, align = FormatCsv.alignType.LEFT)
    private String soaddptoc;
    
    @FormatCsv(name = "SOADLOCC", nullable = false, length = 9, fillSpace = "0")
    private int soadlocc;
    
    @FormatCsv(name = "SOADCOTEPC", nullable = false, length = 3)
    private short soadcotepc;
    
    @FormatCsv(name = "SOADCOTEAC", nullable = false, length = 5)
    private int soadcoteac;
    
    @FormatCsv(name = "SOADTELC", nullable = false, length = 12)
    private long soadtelc;
    
    @FormatCsv(name = "SOADTEINC", nullable = false, length = 6)
    private int soadteinc;
    
    @FormatCsv(name = "SOADCOCEPC", nullable = false, length = 3)
    private short soadcocepc;
    
    @FormatCsv(name = "SOADCELC", nullable = false, length = 12)
    private long soadcelc;
    
    @FormatCsv(name = "FILL", nullable = false, length = 56)
    private String soadfillc="";
    
    @FormatCsv(name = "SOADCOERC", nullable = false, length = 3)
    private String soadcoerc;

    
    @FormatCsv(name = "SOPRTIRE", nullable = false, length = 2)
    private String soprtire;
    
    @FormatCsv(name = "SOPRCOPR", nullable = false, length = 10, align = FormatCsv.alignType.LEFT)
    private String soprcopr;
    
    @FormatCsv(name = "SOPRGRAF", nullable = false, length = 4, fillSpace = "0")
    private String soprgraf;
    
    @FormatCsv(name = "SOPRNUTAEX", nullable = false, length = 19)
    private String soprnutaex;
    
    @FormatCsv(name = "SOPRFEVEEX", nullable = false, length = 6)
    private String soprfeveex;
    
    @FormatCsv(name = "SOPRBIN", nullable = false, length = 19)
    private String soprbin;
    
    @FormatCsv(name = "SOPRNUTCEN", nullable = false, length = 36)
    private String soprnutcen;
    
    @FormatCsv(name = "SOPRINSEG", nullable = false,length = 1)
    private short soprinseg;

    @FormatCsv(name = "FILL", nullable = false, length = 271)
    private String soprfill="";
    
    @FormatCsv(name = "SOPRNUTC", nullable = false, length = 19)
    private String soprnutc;
    
    @FormatCsv(name = "SOPRFEVETC", nullable = false, length = 6)
    private String soprfevetc;
    
    @FormatCsv(name = "SOPRSENUTC", nullable = false, length = 4)
    private short soprsenutc;
    
    @FormatCsv(name = "SOPRCOER", nullable = false, length = 3)
    private String soprcoer;

    public String getSogatire() {
        return sogatire;
    }

    public void setSogatire(String sogatire) {
        this.sogatire = sogatire;
    }

    public int getSogagrpr() {
        return sogagrpr;
    }

    public void setSogagrpr(int sogagrpr) {
        this.sogagrpr = sogagrpr;
    }

    public short getSogaprcu() {
        return sogaprcu;
    }

    public void setSogaprcu(short sogaprcu) {
        this.sogaprcu = sogaprcu;
    }

    public short getSogaticu() {
        return sogaticu;
    }

    public void setSogaticu(short sogaticu) {
        this.sogaticu = sogaticu;
    }

    public String getSogacoem() {
        return sogacoem;
    }

    public void setSogacoem(String sogacoem) {
        this.sogacoem = sogacoem;
    }

    public String getSogasucu1() {
        return sogasucu1;
    }

    public void setSogasucu1(String sogasucu1) {
        this.sogasucu1 = sogasucu1;
    }

    public String getSogaagem() {
        return sogaagem;
    }

    public void setSogaagem(String sogaagem) {
        this.sogaagem = sogaagem;
    }

    public short getSogainec() {
        return sogainec;
    }

    public void setSogainec(short sogainec) {
        this.sogainec = sogainec;
    }

    public short getSogainecm() {
        return sogainecm;
    }

    public void setSogainecm(short sogainecm) {
        this.sogainecm = sogainecm;
    }

    public String getSogaemail() {
        return sogaemail;
    }

    public void setSogaemail(String sogaemail) {
        this.sogaemail = sogaemail;
    }

    public short getSogainse() {
        return sogainse;
    }

    public void setSogainse(short sogainse) {
        this.sogainse = sogainse;
    }

    public String getSogafill() {
        return sogafill;
    }

    public void setSogafill(String sogafill) {
        this.sogafill = sogafill;
    }

    public BigInteger getSoganutcnc() {
        return soganutcnc;
    }

    public void setSoganutcnc(BigInteger soganutcnc) {
        this.soganutcnc = soganutcnc;
    }

    public String getSogacoer() {
        return sogacoer;
    }

    public void setSogacoer(String sogacoer) {
        this.sogacoer = sogacoer;
    }

    public String getSocctire() {
        return socctire;
    }

    public void setSocctire(String socctire) {
        this.socctire = socctire;
    }

    public BigDecimal getSocclicr() {
        return socclicr;
    }

    public void setSocclicr(BigDecimal socclicr) {
        this.socclicr = socclicr;
    }

    public short getSoccmoli() {
        return soccmoli;
    }

    public void setSoccmoli(short soccmoli) {
        this.soccmoli = soccmoli;
    }

    public short getSoccfeci() {
        return soccfeci;
    }

    public void setSoccfeci(short soccfeci) {
        this.soccfeci = soccfeci;
    }

    public short getSoccfopa() {
        return soccfopa;
    }

    public void setSoccfopa(short soccfopa) {
        this.soccfopa = soccfopa;
    }

    public short getSocctide() {
        return socctide;
    }

    public void setSocctide(short socctide) {
        this.socctide = socctide;
    }

    public String getSoccfill() {
        return soccfill;
    }

    public void setSoccfill(String soccfill) {
        this.soccfill = soccfill;
    }

    public String getSocccoer() {
        return socccoer;
    }

    public void setSocccoer(String socccoer) {
        this.socccoer = socccoer;
    }

    public String getSocftire() {
        return socftire;
    }

    public void setSocftire(String socftire) {
        this.socftire = socftire;
    }

    public short getSocfticl() {
        return socfticl;
    }

    public void setSocfticl(short socfticl) {
        this.socfticl = socfticl;
    }

    public String getSocfnocl() {
        return socfnocl;
    }

    public void setSocfnocl(String socfnocl) {
        this.socfnocl = socfnocl;
    }

    public String getSocfapcl() {
        return socfapcl;
    }

    public void setSocfapcl(String socfapcl) {
        this.socfapcl = socfapcl;
    }

    public short getSocftido() {
        return socftido;
    }

    public void setSocftido(short socftido) {
        this.socftido = socftido;
    }

    public String getSocfdocl() {
        return socfdocl;
    }

    public void setSocfdocl(String socfdocl) {
        this.socfdocl = socfdocl;
    }

    public short getSocftidoa() {
        return socftidoa;
    }

    public void setSocftidoa(short socftidoa) {
        this.socftidoa = socftidoa;
    }

    public String getSocfdocla() {
        return socfdocla;
    }

    public void setSocfdocla(String socfdocla) {
        this.socfdocla = socfdocla;
    }

    public String getSocffena() {
        return socffena;
    }

    public void setSocffena(String socffena) {
        this.socffena = socffena;
    }

    public short getSocfesci() {
        return socfesci;
    }

    public void setSocfesci(short socfesci) {
        this.socfesci = socfesci;
    }

    public short getSocfgecl() {
        return socfgecl;
    }

    public void setSocfgecl(short socfgecl) {
        this.socfgecl = socfgecl;
    }

    public short getSocfpacl() {
        return socfpacl;
    }

    public void setSocfpacl(short socfpacl) {
        this.socfpacl = socfpacl;
    }

    public String getSocfluna() {
        return socfluna;
    }

    public void setSocfluna(String socfluna) {
        this.socfluna = socfluna;
    }

    public short getSocfcahi() {
        return socfcahi;
    }

    public void setSocfcahi(short socfcahi) {
        this.socfcahi = socfcahi;
    }

    public String getSocfnoem() {
        return socfnoem;
    }

    public void setSocfnoem(String socfnoem) {
        this.socfnoem = socfnoem;
    }

    public String getSocfemail() {
        return socfemail;
    }

    public void setSocfemail(String socfemail) {
        this.socfemail = socfemail;
    }

    public short getSocfdcpa() {
        return socfdcpa;
    }

    public void setSocfdcpa(short socfdcpa) {
        this.socfdcpa = socfdcpa;
    }

    public short getSocfdfpa() {
        return socfdfpa;
    }

    public void setSocfdfpa(short socfdfpa) {
        this.socfdfpa = socfdfpa;
    }

    public int getSocfnusol() {
        return socfnusol;
    }

    public void setSocfnusol(int socfnusol) {
        this.socfnusol = socfnusol;
    }

    public String getSocffill() {
        return socffill;
    }

    public void setSocffill(String socffill) {
        this.socffill = socffill;
    }

    public String getSocfcoer() {
        return socfcoer;
    }

    public void setSocfcoer(String socfcoer) {
        this.socfcoer = socfcoer;
    }

    public String getSoeitire() {
        return soeitire;
    }

    public void setSoeitire(String soeitire) {
        this.soeitire = soeitire;
    }

    public String getSoeifill() {
        return soeifill;
    }

    public void setSoeifill(String soeifill) {
        this.soeifill = soeifill;
    }

    public String getSoadtirep() {
        return soadtirep;
    }

    public void setSoadtirep(String soadtirep) {
        this.soadtirep = soadtirep;
    }

    public short getSoadtidicp() {
        return soadtidicp;
    }

    public void setSoadtidicp(short soadtidicp) {
        this.soadtidicp = soadtidicp;
    }

    public short getSoadticap() {
        return soadticap;
    }

    public void setSoadticap(short soadticap) {
        this.soadticap = soadticap;
    }

    public String getSoadcallep() {
        return soadcallep;
    }

    public void setSoadcallep(String soadcallep) {
        this.soadcallep = soadcallep;
    }

    public String getSoadnupup() {
        return soadnupup;
    }

    public void setSoadnupup(String soadnupup) {
        this.soadnupup = soadnupup;
    }

    public String getSoadinadp() {
        return soadinadp;
    }

    public void setSoadinadp(String soadinadp) {
        this.soadinadp = soadinadp;
    }

    public String getSoadcopop() {
        return soadcopop;
    }

    public void setSoadcopop(String soadcopop) {
        this.soadcopop = soadcopop;
    }

    public String getSoadzonap() {
        return soadzonap;
    }

    public void setSoadzonap(String soadzonap) {
        this.soadzonap = soadzonap;
    }

    public short getSoadpaisp() {
        return soadpaisp;
    }

    public void setSoadpaisp(short soadpaisp) {
        this.soadpaisp = soadpaisp;
    }

    public String getSoaddptop() {
        return soaddptop;
    }

    public void setSoaddptop(String soaddptop) {
        this.soaddptop = soaddptop;
    }

    public int getSoadlocp() {
        return soadlocp;
    }

    public void setSoadlocp(int soadlocp) {
        this.soadlocp = soadlocp;
    }

    public short getSoadcotepp() {
        return soadcotepp;
    }

    public void setSoadcotepp(short soadcotepp) {
        this.soadcotepp = soadcotepp;
    }

    public int getSoadcoteap() {
        return soadcoteap;
    }

    public void setSoadcoteap(int soadcoteap) {
        this.soadcoteap = soadcoteap;
    }

    public long getSoadtelp() {
        return soadtelp;
    }

    public void setSoadtelp(long soadtelp) {
        this.soadtelp = soadtelp;
    }

    public int getSoadteinp() {
        return soadteinp;
    }

    public void setSoadteinp(int soadteinp) {
        this.soadteinp = soadteinp;
    }

    public short getSoadcocepp() {
        return soadcocepp;
    }

    public void setSoadcocepp(short soadcocepp) {
        this.soadcocepp = soadcocepp;
    }

    public long getSoadcelp() {
        return soadcelp;
    }

    public void setSoadcelp(long soadcelp) {
        this.soadcelp = soadcelp;
    }

    public String getSoadfill() {
        return soadfill;
    }

    public void setSoadfill(String soadfill) {
        this.soadfill = soadfill;
    }

    public String getSoadcoerp() {
        return soadcoerp;
    }

    public void setSoadcoerp(String soadcoerp) {
        this.soadcoerp = soadcoerp;
    }

    public String getSoadtirec() {
        return soadtirec;
    }

    public void setSoadtirec(String soadtirec) {
        this.soadtirec = soadtirec;
    }

    public short getSoadtidicc() {
        return soadtidicc;
    }

    public void setSoadtidicc(short soadtidicc) {
        this.soadtidicc = soadtidicc;
    }

    public short getSoadticac() {
        return soadticac;
    }

    public void setSoadticac(short soadticac) {
        this.soadticac = soadticac;
    }

    public String getSoadcallec() {
        return soadcallec;
    }

    public void setSoadcallec(String soadcallec) {
        this.soadcallec = soadcallec;
    }

    public String getSoadnupuc() {
        return soadnupuc;
    }

    public void setSoadnupuc(String soadnupuc) {
        this.soadnupuc = soadnupuc;
    }

    public String getSoadinadc() {
        return soadinadc;
    }

    public void setSoadinadc(String soadinadc) {
        this.soadinadc = soadinadc;
    }

    public String getSoadcopoc() {
        return soadcopoc;
    }

    public void setSoadcopoc(String soadcopoc) {
        this.soadcopoc = soadcopoc;
    }

    public String getSoadzonac() {
        return soadzonac;
    }

    public void setSoadzonac(String soadzonac) {
        this.soadzonac = soadzonac;
    }

    public short getSoadpaisc() {
        return soadpaisc;
    }

    public void setSoadpaisc(short soadpaisc) {
        this.soadpaisc = soadpaisc;
    }

    public String getSoaddptoc() {
        return soaddptoc;
    }

    public void setSoaddptoc(String soaddptoc) {
        this.soaddptoc = soaddptoc;
    }

    public int getSoadlocc() {
        return soadlocc;
    }

    public void setSoadlocc(int soadlocc) {
        this.soadlocc = soadlocc;
    }

    public short getSoadcotepc() {
        return soadcotepc;
    }

    public void setSoadcotepc(short soadcotepc) {
        this.soadcotepc = soadcotepc;
    }

    public int getSoadcoteac() {
        return soadcoteac;
    }

    public void setSoadcoteac(int soadcoteac) {
        this.soadcoteac = soadcoteac;
    }

    public long getSoadtelc() {
        return soadtelc;
    }

    public void setSoadtelc(long soadtelc) {
        this.soadtelc = soadtelc;
    }

    public int getSoadteinc() {
        return soadteinc;
    }

    public void setSoadteinc(int soadteinc) {
        this.soadteinc = soadteinc;
    }

    public short getSoadcocepc() {
        return soadcocepc;
    }

    public void setSoadcocepc(short soadcocepc) {
        this.soadcocepc = soadcocepc;
    }

    public long getSoadcelc() {
        return soadcelc;
    }

    public void setSoadcelc(long soadcelc) {
        this.soadcelc = soadcelc;
    }

    public String getSoadcoerc() {
        return soadcoerc;
    }

    public void setSoadcoerc(String soadcoerc) {
        this.soadcoerc = soadcoerc;
    }

    public String getSoprtire() {
        return soprtire;
    }

    public void setSoprtire(String soprtire) {
        this.soprtire = soprtire;
    }

    public String getSoprcopr() {
        return soprcopr;
    }

    public void setSoprcopr(String soprcopr) {
        this.soprcopr = soprcopr;
    }

    public String getSoprgraf() {
        return soprgraf;
    }

    public void setSoprgraf(String soprgraf) {
        this.soprgraf = soprgraf;
    }

    public String getSoprnutaex() {
        return soprnutaex;
    }

    public void setSoprnutaex(String soprnutaex) {
        this.soprnutaex = soprnutaex;
    }

    public String getSoprfeveex() {
        return soprfeveex;
    }

    public void setSoprfeveex(String soprfeveex) {
        this.soprfeveex = soprfeveex;
    }

    public String getSoprbin() {
        return soprbin;
    }

    public void setSoprbin(String soprbin) {
        this.soprbin = soprbin;
    }

    public String getSoprnutcen() {
        return soprnutcen;
    }

    public void setSoprnutcen(String soprnutcen) {
        this.soprnutcen = soprnutcen;
    }

    public short getSoprinseg() {
        return soprinseg;
    }

    public void setSoprinseg(short soprinseg) {
        this.soprinseg = soprinseg;
    }

    public String getSoprfill() {
        return soprfill;
    }

    public void setSoprfill(String soprfill) {
        this.soprfill = soprfill;
    }

    public String getSoprnutc() {
        return soprnutc;
    }

    public void setSoprnutc(String soprnutc) {
        this.soprnutc = soprnutc;
    }

    public String getSoprfevetc() {
        return soprfevetc;
    }

    public void setSoprfevetc(String soprfevetc) {
        this.soprfevetc = soprfevetc;
    }

    public short getSoprsenutc() {
        return soprsenutc;
    }

    public void setSoprsenutc(short soprsenutc) {
        this.soprsenutc = soprsenutc;
    }

    public String getSoprcoer() {
        return soprcoer;
    }

    public void setSoprcoer(String soprcoer) {
        this.soprcoer = soprcoer;
    }

    public String getSoadfillc() {
        return soadfillc;
    }

    public void setSoadfillc(String soadfillc) {
        this.soadfillc = soadfillc;
    }

    public String getSoeicoer() {
        return soeicoer;
    }

    public void setSoeicoer(String soeicoer) {
        this.soeicoer = soeicoer;
    }
}
