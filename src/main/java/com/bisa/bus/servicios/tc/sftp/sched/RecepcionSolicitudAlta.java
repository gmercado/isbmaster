package com.bisa.bus.servicios.tc.sftp.sched;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.bus.servicios.tc.sftp.api.SftpRecepcionSolicitudAlta;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 25/05/2017.
 */
public class RecepcionSolicitudAlta implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecepcionSolicitudAlta.class);

    public static String NOMBRE_TAREA = "PagoEnvio-Hilo";
    private final MedioAmbiente medioAmbiente;
    private final NotificacionesCorreo notificacionesCorreo;
    private final SftpRecepcionSolicitudAlta sftpRecepcionSolicitudAlta;

    @Inject
    public RecepcionSolicitudAlta(MedioAmbiente medioAmbiente, NotificacionesCorreo notificacionesCorreo,
                                  SftpRecepcionSolicitudAlta sftpRecepcionSolicitudAlta){
        this.medioAmbiente = medioAmbiente;
        this.notificacionesCorreo = notificacionesCorreo;
        this.sftpRecepcionSolicitudAlta = sftpRecepcionSolicitudAlta;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Inicio de proceso de recepcion solicitud");
        boolean ok = sftpRecepcionSolicitudAlta.procesarArchivo();
        LOGGER.info("Fin de proceso de de recepcion solicitud con respuesta: {}", ok);
        if(ok){
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_PROCESO_DIARIO_OPERADOR, Variables.FTP_MENSAJE_EMAIL_PROCESO_DIARIO_OPERADOR_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador(sftpRecepcionSolicitudAlta.getNombreArchivoProcesado(), TipoArchivo.RAL, mensaje);
        }else{
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_PROCESO_DIARIO_ERROR_OPERADOR, Variables.FTP_MENSAJE_EMAIL_PROCESO_DIARIO_ERROR_OPERADOR_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador("", TipoArchivo.RAL, mensaje);
        }
    }

}
