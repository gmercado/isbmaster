package com.bisa.bus.servicios.tc.sftp.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import com.bisa.bus.servicios.tc.sftp.dao.PagosTCDao;
import com.bisa.bus.servicios.tc.sftp.entities.IdPagosTC;
import com.bisa.bus.servicios.tc.sftp.entities.PagosTC;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;

/**
 * Created by atenorio on 08/05/2017.
 */
@Titulo("Detalle de Pago ATC")
public class DetallePagoAtc extends BisaWebPage {

    protected static final Logger LOGGER = LoggerFactory.getLogger(DetallePagoAtc.class);

    public DetallePagoAtc(PageParameters parameters) {
        super();
        IdPagosTC id = new IdPagosTC();
        id.setCuentaTarjeta(BigInteger.valueOf(parameters.get("cuenta").toLong()));
        id.setTransaccion(parameters.get("transaccion").toString());

        IModel<PagosTC> model = new LoadableDetachableModel<PagosTC>() {
            @Override
            protected PagosTC load() {
                PagosTCDao dao = getInstance(PagosTCDao.class);
                return dao.find(id);
            }
        };

        setDefaultModel(new CompoundPropertyModel<>(model));
        //Form
        Form<PagosTC> form = new Form<>("detalle", model);
        add(form);

        // detalle
        form.add(new Label("cuentaTarjetaAtc", new PropertyModel(model,"idPago.cuentaTarjeta")));
        form.add(new Label("cuentaTarjeta"));
        form.add(new Label("transaccion", new PropertyModel(model,"idPago.transaccion")));
        form.add(new Label("importeFormato"));
        form.add(new Label("tipoMoneda"));
        form.add(new Label("fechaPago"));
        form.add(new Label("estadoDescripcion"));
        form.add(new Label("codError"));
        form.add(new Label("respuestaDescripcion"));
        form.add(new Label("idArchivoEnvio"));
        form.add(new Label("fechaEnvioFormato"));
        form.add(new Label("idArchivoRecepcion"));
        form.add(new Label("fechaRecepcionFormato"));
        // botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(new ListadoPagos());
            }
        });

    }
}
