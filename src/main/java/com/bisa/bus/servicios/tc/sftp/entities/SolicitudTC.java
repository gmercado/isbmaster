package com.bisa.bus.servicios.tc.sftp.entities;

import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.tc.sftp.model.EstadoSolicitudTC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * Created by atenorio on 17/05/2017.
 */
@Entity
@Table(name = "TCPASOLIC")
public class SolicitudTC implements Serializable {
    private static final long serialVersionUID = 1L;
    protected static final Logger LOGGER = LoggerFactory.getLogger(SolicitudTC.class);

    @Basic(optional = false)
    @Column(name = "SOGATIRE", nullable = false, length = 2)
    private String sogatire;
    @Basic(optional = false)
    @Column(name = "SOGAGRPR", nullable = false)
    private int sogagrpr;
    @Basic(optional = false)
    @Column(name = "SOGAPRCU", nullable = false)
    private short sogaprcu;
    @Basic(optional = false)
    @Column(name = "SOGATICU", nullable = false)
    private short sogaticu;
    @Basic(optional = false)
    @Column(name = "SOGACOEM", nullable = false, length = 4)
    private String sogacoem;
    @Basic(optional = false)
    @Column(name = "SOGASUCU1", nullable = false, length = 9)
    private String sogasucu1;
    @Basic(optional = false)
    @Column(name = "SOGAAGEM", nullable = false, length = 5)
    private String sogaagem;
    @Basic(optional = false)
    @Column(name = "SOGAINEC", nullable = false)
    private short sogainec;
    @Basic(optional = false)
    @Column(name = "SOGAINECM", nullable = false)
    private short sogainecm;
    @Basic(optional = false)
    @Column(name = "SOGAEMAIL", nullable = false, length = 60)
    private String sogaemail;
    @Basic(optional = false)
    @Column(name = "SOGAINSE", nullable = false)
    private short sogainse;
    @Basic(optional = false)
    @Column(name = "SOGANUTCNC", nullable = false)
    private BigInteger soganutcnc;
    @Basic(optional = false)
    @Column(name = "SOGACOER", nullable = false, length = 3)
    private String sogacoer;
    @Basic(optional = false)
    @Column(name = "SOCCTIRE", nullable = false, length = 2)
    private String socctire;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "SOCCLICR", nullable = false, precision = 18, scale = 6)
    private BigDecimal socclicr;
    @Basic(optional = false)
    @Column(name = "SOCCMOLI", nullable = false)
    private short soccmoli;
    @Basic(optional = false)
    @Column(name = "SOCCFECI", nullable = false)
    private short soccfeci;
    @Basic(optional = false)
    @Column(name = "SOCCFOPA", nullable = false)
    private short soccfopa;
    @Basic(optional = false)
    @Column(name = "SOCCTIDE", nullable = false)
    private short socctide;
    @Basic(optional = false)
    @Column(name = "SOCCCOER", nullable = false, length = 3)
    private String socccoer;
    @Basic(optional = false)
    @Column(name = "SOCFTIRE", nullable = false, length = 2)
    private String socftire;
    @Basic(optional = false)
    @Column(name = "SOCFTICL", nullable = false)
    private short socfticl;
    @Basic(optional = false)
    @Column(name = "SOCFNOCL", nullable = false, length = 60)
    private String socfnocl;
    @Basic(optional = false)
    @Column(name = "SOCFAPCL", nullable = false, length = 60)
    private String socfapcl;
    @Basic(optional = false)
    @Column(name = "SOCFTIDO", nullable = false)
    private short socftido;
    @Basic(optional = false)
    @Column(name = "SOCFDOCL", nullable = false, length = 25)
    private String socfdocl;
    @Basic(optional = false)
    @Column(name = "SOCFTIDOA", nullable = false)
    private short socftidoa;
    @Basic(optional = false)
    @Column(name = "SOCFDOCLA", nullable = false, length = 25)
    private String socfdocla;
    @Basic(optional = false)
    @Column(name = "SOCFFENA", nullable = false, length = 8)
    private String socffena;
    @Basic(optional = false)
    @Column(name = "SOCFESCI", nullable = false)
    private short socfesci;
    @Basic(optional = false)
    @Column(name = "SOCFGECL", nullable = false)
    private short socfgecl;
    @Basic(optional = false)
    @Column(name = "SOCFPACL", nullable = false)
    private short socfpacl;
    @Basic(optional = false)
    @Column(name = "SOCFLUNA", nullable = false, length = 40)
    private String socfluna;
    @Basic(optional = false)
    @Column(name = "SOCFCAHI", nullable = false)
    private short socfcahi;
    @Basic(optional = false)
    @Column(name = "SOCFNOEM", nullable = false, length = 25)
    private String socfnoem;
    @Basic(optional = false)
    @Column(name = "SOCFEMAIL", nullable = false, length = 60)
    private String socfemail;
    @Basic(optional = false)
    @Column(name = "SOCFDCPA", nullable = false)
    private short socfdcpa;
    @Basic(optional = false)
    @Column(name = "SOCFDFPA", nullable = false)
    private short socfdfpa;
    @Basic(optional = false)
    @Column(name = "SOCFNUSOL", nullable = false)
    private int socfnusol;
    @Basic(optional = false)
    @Column(name = "SOCFCOER", nullable = false, length = 3)
    private String socfcoer;
    @Basic(optional = false)
    @Column(name = "SOEITIRE", nullable = false, length = 2)
    private String soeitire;
    @Basic(optional = false)
    @Column(name = "SOEICONTR", nullable = false)
    private short soeicontr;
    @Basic(optional = false)
    @Column(name = "SOEIFEVECO", nullable = false, length = 8)
    private String soeifeveco;
    @Basic(optional = false)
    @Column(name = "SOEIEMPL", nullable = false, length = 60)
    private String soeiempl;
    @Basic(optional = false)
    @Column(name = "SOEITIDO", nullable = false)
    private short soeitido;
    @Basic(optional = false)
    @Column(name = "SOEIDOEM", nullable = false, length = 25)
    private String soeidoem;
    @Basic(optional = false)
    @Column(name = "SOEIFEIN", nullable = false, length = 8)
    private String soeifein;
    @Basic(optional = false)
    @Column(name = "SOEIINPR", nullable = false)
    private short soeiinpr;
    @Basic(optional = false)
    @Column(name = "SOEIINEXT", nullable = false)
    private short soeiinext;
    @Basic(optional = false)
    @Column(name = "SOEITOIN", nullable = false)
    private long soeitoin;
    @Basic(optional = false)
    @Column(name = "SOEICAEM", nullable = false, length = 60)
    private String soeicaem;
    @Basic(optional = false)
    @Column(name = "SOEIOCUP", nullable = false)
    private int soeiocup;
    @Basic(optional = false)
    @Column(name = "SOEISEEM", nullable = false)
    private int soeiseem;
    @Basic(optional = false)
    @Column(name = "SOEICOER", nullable = false, length = 3)
    private String soeicoer;
    @Basic(optional = false)
    @Column(name = "SOADTIREP", nullable = false, length = 2)
    private String soadtirep;
    @Basic(optional = false)
    @Column(name = "SOADTIDICP", nullable = false)
    private short soadtidicp;
    @Basic(optional = false)
    @Column(name = "SOADTICAP", nullable = false)
    private short soadticap;
    @Basic(optional = false)
    @Column(name = "SOADCALLEP", nullable = false, length = 100)
    private String soadcallep;
    @Basic(optional = false)
    @Column(name = "SOADNUPUP", nullable = false, length = 10)
    private String soadnupup;
    @Basic(optional = false)
    @Column(name = "SOADINADP", nullable = false, length = 100)
    private String soadinadp;
    @Basic(optional = false)
    @Column(name = "SOADCOPOP", nullable = false, length = 6)
    private String soadcopop;
    @Basic(optional = false)
    @Column(name = "SOADZONAP", nullable = false, length = 60)
    private String soadzonap;
    @Basic(optional = false)
    @Column(name = "SOADPAISP", nullable = false)
    private short soadpaisp;
    @Basic(optional = false)
    @Column(name = "SOADDPTOP", nullable = false, length = 5)
    private String soaddptop;
    @Basic(optional = false)
    @Column(name = "SOADLOCP", nullable = false)
    private int soadlocp;
    @Basic(optional = false)
    @Column(name = "SOADCOTEPP", nullable = false)
    private short soadcotepp;
    @Basic(optional = false)
    @Column(name = "SOADCOTEAP", nullable = false)
    private int soadcoteap;
    @Basic(optional = false)
    @Column(name = "SOADTELP", nullable = false)
    private long soadtelp;
    @Basic(optional = false)
    @Column(name = "SOADTEINP", nullable = false)
    private int soadteinp;
    @Basic(optional = false)
    @Column(name = "SOADCOCEPP", nullable = false)
    private short soadcocepp;
    @Basic(optional = false)
    @Column(name = "SOADCELP", nullable = false)
    private long soadcelp;
    @Basic(optional = false)
    @Column(name = "SOADCOERP", nullable = false, length = 3)
    private String soadcoerp;
    @Basic(optional = false)
    @Column(name = "SOADTIREC", nullable = false, length = 2)
    private String soadtirec;
    @Basic(optional = false)
    @Column(name = "SOADTIDICC", nullable = false)
    private short soadtidicc;
    @Basic(optional = false)
    @Column(name = "SOADTICAC", nullable = false)
    private short soadticac;
    @Basic(optional = false)
    @Column(name = "SOADCALLEC", nullable = false, length = 100)
    private String soadcallec;
    @Basic(optional = false)
    @Column(name = "SOADNUPUC", nullable = false, length = 10)
    private String soadnupuc;
    @Basic(optional = false)
    @Column(name = "SOADINADC", nullable = false, length = 100)
    private String soadinadc;
    @Basic(optional = false)
    @Column(name = "SOADCOPOC", nullable = false, length = 6)
    private String soadcopoc;
    @Basic(optional = false)
    @Column(name = "SOADZONAC", nullable = false, length = 60)
    private String soadzonac;
    @Basic(optional = false)
    @Column(name = "SOADPAISC", nullable = false)
    private short soadpaisc;
    @Basic(optional = false)
    @Column(name = "SOADDPTOC", nullable = false, length = 5)
    private String soaddptoc;
    @Basic(optional = false)
    @Column(name = "SOADLOCC", nullable = false)
    private int soadlocc;
    @Basic(optional = false)
    @Column(name = "SOADCOTEPC", nullable = false)
    private short soadcotepc;
    @Basic(optional = false)
    @Column(name = "SOADCOTEAC", nullable = false)
    private int soadcoteac;
    @Basic(optional = false)
    @Column(name = "SOADTELC", nullable = false)
    private long soadtelc;
    @Basic(optional = false)
    @Column(name = "SOADTEINC", nullable = false)
    private int soadteinc;
    @Basic(optional = false)
    @Column(name = "SOADCOCEPC", nullable = false)
    private short soadcocepc;
    @Basic(optional = false)
    @Column(name = "SOADCELC", nullable = false)
    private long soadcelc;
    @Basic(optional = false)
    @Column(name = "SOADCOERC", nullable = false, length = 3)
    private String soadcoerc;
    @Basic(optional = false)
    @Column(name = "SOPRTIRE", nullable = false, length = 2)
    private String soprtire;
    @Basic(optional = false)
    @Column(name = "SOPRCOPR", nullable = false, length = 10)
    private String soprcopr;
    @Basic(optional = false)
    @Column(name = "SOPRGRAF", nullable = false, length = 4)
    private String soprgraf;
    @Basic(optional = false)
    @Column(name = "SOPRNUTAEX", nullable = false, length = 19)
    private String soprnutaex;
    @Basic(optional = false)
    @Column(name = "SOPRFEVEEX", nullable = false, length = 6)
    private String soprfeveex;
    @Basic(optional = false)
    @Column(name = "SOPRBIN", nullable = false, length = 19)
    private String soprbin;
    @Basic(optional = false)
    @Column(name = "SOPRNUTCEN", nullable = false, length = 36)
    private String soprnutcen;
    @Basic(optional = false)
    @Column(name = "SOPRINSEG", nullable = false)
    private short soprinseg;
    @Basic(optional = false)
    @Column(name = "SOPRNUTC", nullable = false, length = 19)
    private String soprnutc;
    @Basic(optional = false)
    @Column(name = "SOPRFEVETC", nullable = false, length = 6)
    private String soprfevetc;
    @Basic(optional = false)
    @Column(name = "SOPRSENUTC", nullable = false)
    private short soprsenutc;
    @Basic(optional = false)
    @Column(name = "SOPRCOER", nullable = false, length = 3)
    private String soprcoer;

    //@Id
    @Basic(optional = false)
    @Column(name = "SOCADM", nullable = false)
    private Character socadm;
    //@Id
    @Basic(optional = false)
    @Column(name = "SOSUCU", nullable = false)
    private short sosucu;
    @Id
    @Basic(optional = false)
    @Column(name = "SONSOL", nullable = false)
    private int sonsol;
    //@Id
    @Basic(optional = false)
    @Column(name = "SOFEPRO1", nullable = false)
    private int sofepro1;

    @Basic(optional = false)
    @Column(name = "SOFEENV", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date sofeenv;
    @Basic(optional = false)
    @Column(name = "SOFERES", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date soferes;
    @Basic(optional = false)
    @Column(name = "SOEST", nullable = false)
    private String soest;

    @Transient
    private EstadoSolicitudTC estado;

    @Basic(optional = false)
    @Column(name = "REPGEN", nullable = false, length = 10)
    private String usuarioCreador;
    @Basic(optional = false)
    @Column(name = "REFEEN", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @Column(name = "REPGRE", nullable = false, length = 10)
    private String usuarioModificador;
    @Basic(optional = false)
    @Column(name = "REFERE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Basic(optional = false)
    @Column(name = "REIDEN", nullable = false)
    private BigInteger idArchivoEnvio;

    @Basic(optional = false)
    @Column(name = "REIDRE", nullable = false)
    private BigInteger idArchivoRecepcion;

    @Basic(optional = false)
    @Column(name = "SOTDOC", nullable = false, length = 2)
    private String tipoDocumento;

    @Basic(optional = false)
    @Column(name = "SOESTC", nullable = false, length = 1)
    private String estadoCivil;

    @Basic(optional = false)
    @Column(name = "SOSEX", nullable = false, length = 1)
    private String sexo;

    @Basic(optional = false)
    @Column(name = "SOTTAR", nullable = false, length = 1)
    private String tipoTarjeta;

    @Basic(optional = false)
    @Column(name = "SONUTC", nullable = false)
    private long sonutc;
    @Basic(optional = false)
    @Column(name = "SONUT1", nullable = false, length = 9)
    private String sonut1;

    public SolicitudTC() {
    }


    public SolicitudTC(Long i23id, String sogatire, int sogagrpr, short sogaprcu, short sogaticu, String sogacoem, String sogasucu1, String sogaagem, short sogainec, short sogainecm, String sogaemail, short sogainse, BigInteger soganutcnc, String sogacoer, String socctire, BigDecimal socclicr, short soccmoli, short soccfeci, short soccfopa, short socctide, String socccoer, String socftire, short socfticl, String socfnocl, String socfapcl, short socftido, String socfdocl, short socftidoa, String socfdocla, String socffena, short socfesci, short socfgecl, short socfpacl, String socfluna, short socfcahi, String socfnoem, String socfemail, short socfdcpa, short socfdfpa, int socfnusol, String socfcoer, String soeitire, short soeicontr, String soeifeveco, String soeiempl, short soeitido, String soeidoem, String soeifein, short soeiinpr, short soeiinext, long soeitoin, String soeicaem, int soeiocup, int soeiseem, String soeicoer, String soadtirep, short soadtidicp, short soadticap, String soadcallep, String soadnupup, String soadinadp, String soadcopop, String soadzonap, short soadpaisp, String soaddptop, int soadlocp, short soadcotepp, int soadcoteap, long soadtelp, int soadteinp, short soadcocepp, long soadcelp, String soadcoerp, String soadtirec, short soadtidicc, short soadticac, String soadcallec, String soadnupuc, String soadinadc, String soadcopoc, String soadzonac, short soadpaisc, String soaddptoc, int soadlocc, short soadcotepc, int soadcoteac, long soadtelc, int soadteinc, short soadcocepc, long soadcelc, String soadcoerc, String soprtire, String soprcopr, String soprgraf, String soprnutaex, String soprfeveex, String soprbin, String soprnutcen, short soprinseg, String soprnutc, String soprfevetc, short soprsenutc, String soprcoer, Character socadm, short sosucu, int sonsol, int sofepro1, Date sofeenv, Date soferes,
                       String soest, String reusre, Date redate, String reusrr, Date redatr) {
        this.sogatire = sogatire;
        this.sogagrpr = sogagrpr;
        this.sogaprcu = sogaprcu;
        this.sogaticu = sogaticu;
        this.sogacoem = sogacoem;
        this.sogasucu1 = sogasucu1;
        this.sogaagem = sogaagem;
        this.sogainec = sogainec;
        this.sogainecm = sogainecm;
        this.sogaemail = sogaemail;
        this.sogainse = sogainse;
        this.soganutcnc = soganutcnc;
        this.sogacoer = sogacoer;
        this.socctire = socctire;
        this.socclicr = socclicr;
        this.soccmoli = soccmoli;
        this.soccfeci = soccfeci;
        this.soccfopa = soccfopa;
        this.socctide = socctide;
        this.socccoer = socccoer;
        this.socftire = socftire;
        this.socfticl = socfticl;
        this.socfnocl = socfnocl;
        this.socfapcl = socfapcl;
        this.socftido = socftido;
        this.socfdocl = socfdocl;
        this.socftidoa = socftidoa;
        this.socfdocla = socfdocla;
        this.socffena = socffena;
        this.socfesci = socfesci;
        this.socfgecl = socfgecl;
        this.socfpacl = socfpacl;
        this.socfluna = socfluna;
        this.socfcahi = socfcahi;
        this.socfnoem = socfnoem;
        this.socfemail = socfemail;
        this.socfdcpa = socfdcpa;
        this.socfdfpa = socfdfpa;
        this.socfnusol = socfnusol;
        this.socfcoer = socfcoer;
        this.soeitire = soeitire;
        this.soeicontr = soeicontr;
        this.soeifeveco = soeifeveco;
        this.soeiempl = soeiempl;
        this.soeitido = soeitido;
        this.soeidoem = soeidoem;
        this.soeifein = soeifein;
        this.soeiinpr = soeiinpr;
        this.soeiinext = soeiinext;
        this.soeitoin = soeitoin;
        this.soeicaem = soeicaem;
        this.soeiocup = soeiocup;
        this.soeiseem = soeiseem;
        this.soeicoer = soeicoer;
        this.soadtirep = soadtirep;
        this.soadtidicp = soadtidicp;
        this.soadticap = soadticap;
        this.soadcallep = soadcallep;
        this.soadnupup = soadnupup;
        this.soadinadp = soadinadp;
        this.soadcopop = soadcopop;
        this.soadzonap = soadzonap;
        this.soadpaisp = soadpaisp;
        this.soaddptop = soaddptop;
        this.soadlocp = soadlocp;
        this.soadcotepp = soadcotepp;
        this.soadcoteap = soadcoteap;
        this.soadtelp = soadtelp;
        this.soadteinp = soadteinp;
        this.soadcocepp = soadcocepp;
        this.soadcelp = soadcelp;
        this.soadcoerp = soadcoerp;
        this.soadtirec = soadtirec;
        this.soadtidicc = soadtidicc;
        this.soadticac = soadticac;
        this.soadcallec = soadcallec;
        this.soadnupuc = soadnupuc;
        this.soadinadc = soadinadc;
        this.soadcopoc = soadcopoc;
        this.soadzonac = soadzonac;
        this.soadpaisc = soadpaisc;
        this.soaddptoc = soaddptoc;
        this.soadlocc = soadlocc;
        this.soadcotepc = soadcotepc;
        this.soadcoteac = soadcoteac;
        this.soadtelc = soadtelc;
        this.soadteinc = soadteinc;
        this.soadcocepc = soadcocepc;
        this.soadcelc = soadcelc;
        this.soadcoerc = soadcoerc;
        this.soprtire = soprtire;
        this.soprcopr = soprcopr;
        this.soprgraf = soprgraf;
        this.soprnutaex = soprnutaex;
        this.soprfeveex = soprfeveex;
        this.soprbin = soprbin;
        this.soprnutcen = soprnutcen;
        this.soprinseg = soprinseg;
        this.soprnutc = soprnutc;
        this.soprfevetc = soprfevetc;
        this.soprsenutc = soprsenutc;
        this.soprcoer = soprcoer;
        this.socadm = socadm;
        this.sosucu = sosucu;
        this.sonsol = sonsol;
        this.sofepro1 = sofepro1;
        this.sofeenv = sofeenv;
        this.soferes = soferes;
        this.soest = soest;
        this.setUsuarioCreador(reusre);
        this.setFechaCreacion(redate);
        this.setUsuarioModificador(reusrr);
        this.setFechaModificacion(redatr);
    }

    public String getSogatire() {
        return sogatire;
    }

    public void setSogatire(String sogatire) {
        this.sogatire = sogatire;
    }

    public int getSogagrpr() {
        return sogagrpr;
    }

    public void setSogagrpr(int sogagrpr) {
        this.sogagrpr = sogagrpr;
    }

    public short getSogaprcu() {
        return sogaprcu;
    }

    public void setSogaprcu(short sogaprcu) {
        this.sogaprcu = sogaprcu;
    }

    public short getSogaticu() {
        return sogaticu;
    }

    public void setSogaticu(short sogaticu) {
        this.sogaticu = sogaticu;
    }

    public String getSogacoem() {
        return sogacoem;
    }

    public void setSogacoem(String sogacoem) {
        this.sogacoem = sogacoem;
    }

    public String getSogasucu1() {
        return sogasucu1;
    }

    public void setSogasucu1(String sogasucu1) {
        this.sogasucu1 = sogasucu1;
    }

    public String getSogaagem() {
        return sogaagem;
    }

    public void setSogaagem(String sogaagem) {
        this.sogaagem = sogaagem;
    }

    public short getSogainec() {
        return sogainec;
    }

    public void setSogainec(short sogainec) {
        this.sogainec = sogainec;
    }

    public short getSogainecm() {
        return sogainecm;
    }

    public void setSogainecm(short sogainecm) {
        this.sogainecm = sogainecm;
    }

    public String getSogaemail() {
        return sogaemail;
    }

    public void setSogaemail(String sogaemail) {
        this.sogaemail = sogaemail;
    }

    public short getSogainse() {
        return sogainse;
    }

    public void setSogainse(short sogainse) {
        this.sogainse = sogainse;
    }

    public BigInteger getSoganutcnc() {
        return soganutcnc;
    }

    public void setSoganutcnc(BigInteger soganutcnc) {
        this.soganutcnc = soganutcnc;
    }

    public String getSogacoer() {
        return sogacoer;
    }

    public void setSogacoer(String sogacoer) {
        this.sogacoer = sogacoer;
    }

    public String getSocctire() {
        return socctire;
    }

    public void setSocctire(String socctire) {
        this.socctire = socctire;
    }

    public BigDecimal getSocclicr() {
        return socclicr;
    }

    public void setSocclicr(BigDecimal socclicr) {
        this.socclicr = socclicr;
    }

    public short getSoccmoli() {
        return soccmoli;
    }

    public void setSoccmoli(short soccmoli) {
        this.soccmoli = soccmoli;
    }

    public short getSoccfeci() {
        return soccfeci;
    }

    public void setSoccfeci(short soccfeci) {
        this.soccfeci = soccfeci;
    }

    public short getSoccfopa() {
        return soccfopa;
    }

    public void setSoccfopa(short soccfopa) {
        this.soccfopa = soccfopa;
    }

    public short getSocctide() {
        return socctide;
    }

    public void setSocctide(short socctide) {
        this.socctide = socctide;
    }

    public String getSocccoer() {
        return socccoer;
    }

    public void setSocccoer(String socccoer) {
        this.socccoer = socccoer;
    }

    public String getSocftire() {
        return socftire;
    }

    public void setSocftire(String socftire) {
        this.socftire = socftire;
    }

    public short getSocfticl() {
        return socfticl;
    }

    public void setSocfticl(short socfticl) {
        this.socfticl = socfticl;
    }

    public String getSocfnocl() {
        return socfnocl;
    }

    public void setSocfnocl(String socfnocl) {
        this.socfnocl = socfnocl;
    }

    public String getSocfapcl() {
        return socfapcl;
    }

    public void setSocfapcl(String socfapcl) {
        this.socfapcl = socfapcl;
    }

    public short getSocftido() {
        return socftido;
    }

    public void setSocftido(short socftido) {
        this.socftido = socftido;
    }

    public String getSocfdocl() {
        return socfdocl;
    }

    public void setSocfdocl(String socfdocl) {
        this.socfdocl = socfdocl;
    }

    public short getSocftidoa() {
        return socftidoa;
    }

    public void setSocftidoa(short socftidoa) {
        this.socftidoa = socftidoa;
    }

    public String getSocfdocla() {
        return socfdocla;
    }

    public void setSocfdocla(String socfdocla) {
        this.socfdocla = socfdocla;
    }

    public String getSocffena() {
        return socffena;
    }

    public void setSocffena(String socffena) {
        this.socffena = socffena;
    }

    public short getSocfesci() {
        return socfesci;
    }

    public void setSocfesci(short socfesci) {
        this.socfesci = socfesci;
    }

    public short getSocfgecl() {
        return socfgecl;
    }

    public void setSocfgecl(short socfgecl) {
        this.socfgecl = socfgecl;
    }

    public short getSocfpacl() {
        return socfpacl;
    }

    public void setSocfpacl(short socfpacl) {
        this.socfpacl = socfpacl;
    }

    public String getSocfluna() {
        return socfluna;
    }

    public void setSocfluna(String socfluna) {
        this.socfluna = socfluna;
    }

    public short getSocfcahi() {
        return socfcahi;
    }

    public void setSocfcahi(short socfcahi) {
        this.socfcahi = socfcahi;
    }

    public String getSocfnoem() {
        return socfnoem;
    }

    public void setSocfnoem(String socfnoem) {
        this.socfnoem = socfnoem;
    }

    public String getSocfemail() {
        return socfemail;
    }

    public void setSocfemail(String socfemail) {
        this.socfemail = socfemail;
    }

    public short getSocfdcpa() {
        return socfdcpa;
    }

    public void setSocfdcpa(short socfdcpa) {
        this.socfdcpa = socfdcpa;
    }

    public short getSocfdfpa() {
        return socfdfpa;
    }

    public void setSocfdfpa(short socfdfpa) {
        this.socfdfpa = socfdfpa;
    }

    public int getSocfnusol() {
        return socfnusol;
    }

    public void setSocfnusol(int socfnusol) {
        this.socfnusol = socfnusol;
    }

    public String getSocfcoer() {
        return socfcoer;
    }

    public void setSocfcoer(String socfcoer) {
        this.socfcoer = socfcoer;
    }

    public String getSoeitire() {
        return soeitire;
    }

    public void setSoeitire(String soeitire) {
        this.soeitire = soeitire;
    }

    public short getSoeicontr() {
        return soeicontr;
    }

    public void setSoeicontr(short soeicontr) {
        this.soeicontr = soeicontr;
    }

    public String getSoeifeveco() {
        return soeifeveco;
    }

    public void setSoeifeveco(String soeifeveco) {
        this.soeifeveco = soeifeveco;
    }

    public String getSoeiempl() {
        return soeiempl;
    }

    public void setSoeiempl(String soeiempl) {
        this.soeiempl = soeiempl;
    }

    public short getSoeitido() {
        return soeitido;
    }

    public void setSoeitido(short soeitido) {
        this.soeitido = soeitido;
    }

    public String getSoeidoem() {
        return soeidoem;
    }

    public void setSoeidoem(String soeidoem) {
        this.soeidoem = soeidoem;
    }

    public String getSoeifein() {
        return soeifein;
    }

    public void setSoeifein(String soeifein) {
        this.soeifein = soeifein;
    }

    public short getSoeiinpr() {
        return soeiinpr;
    }

    public void setSoeiinpr(short soeiinpr) {
        this.soeiinpr = soeiinpr;
    }

    public short getSoeiinext() {
        return soeiinext;
    }

    public void setSoeiinext(short soeiinext) {
        this.soeiinext = soeiinext;
    }

    public long getSoeitoin() {
        return soeitoin;
    }

    public void setSoeitoin(long soeitoin) {
        this.soeitoin = soeitoin;
    }

    public String getSoeicaem() {
        return soeicaem;
    }

    public void setSoeicaem(String soeicaem) {
        this.soeicaem = soeicaem;
    }

    public int getSoeiocup() {
        return soeiocup;
    }

    public void setSoeiocup(int soeiocup) {
        this.soeiocup = soeiocup;
    }

    public int getSoeiseem() {
        return soeiseem;
    }

    public void setSoeiseem(int soeiseem) {
        this.soeiseem = soeiseem;
    }

    public String getSoeicoer() {
        return soeicoer;
    }

    public void setSoeicoer(String soeicoer) {
        this.soeicoer = soeicoer;
    }

    public String getSoadtirep() {
        return soadtirep;
    }

    public void setSoadtirep(String soadtirep) {
        this.soadtirep = soadtirep;
    }

    public short getSoadtidicp() {
        return soadtidicp;
    }

    public void setSoadtidicp(short soadtidicp) {
        this.soadtidicp = soadtidicp;
    }

    public short getSoadticap() {
        return soadticap;
    }

    public void setSoadticap(short soadticap) {
        this.soadticap = soadticap;
    }

    public String getSoadcallep() {
        return soadcallep;
    }

    public void setSoadcallep(String soadcallep) {
        this.soadcallep = soadcallep;
    }

    public String getSoadnupup() {
        return soadnupup;
    }

    public void setSoadnupup(String soadnupup) {
        this.soadnupup = soadnupup;
    }

    public String getSoadinadp() {
        return soadinadp;
    }

    public void setSoadinadp(String soadinadp) {
        this.soadinadp = soadinadp;
    }

    public String getSoadcopop() {
        return soadcopop;
    }

    public void setSoadcopop(String soadcopop) {
        this.soadcopop = soadcopop;
    }

    public String getSoadzonap() {
        return soadzonap;
    }

    public void setSoadzonap(String soadzonap) {
        this.soadzonap = soadzonap;
    }

    public short getSoadpaisp() {
        return soadpaisp;
    }

    public void setSoadpaisp(short soadpaisp) {
        this.soadpaisp = soadpaisp;
    }

    public String getSoaddptop() {
        return soaddptop;
    }

    public void setSoaddptop(String soaddptop) {
        this.soaddptop = soaddptop;
    }

    public int getSoadlocp() {
        return soadlocp;
    }

    public void setSoadlocp(int soadlocp) {
        this.soadlocp = soadlocp;
    }

    public short getSoadcotepp() {
        return soadcotepp;
    }

    public void setSoadcotepp(short soadcotepp) {
        this.soadcotepp = soadcotepp;
    }

    public int getSoadcoteap() {
        return soadcoteap;
    }

    public void setSoadcoteap(int soadcoteap) {
        this.soadcoteap = soadcoteap;
    }

    public long getSoadtelp() {
        return soadtelp;
    }

    public void setSoadtelp(long soadtelp) {
        this.soadtelp = soadtelp;
    }

    public int getSoadteinp() {
        return soadteinp;
    }

    public void setSoadteinp(int soadteinp) {
        this.soadteinp = soadteinp;
    }

    public short getSoadcocepp() {
        return soadcocepp;
    }

    public void setSoadcocepp(short soadcocepp) {
        this.soadcocepp = soadcocepp;
    }

    public long getSoadcelp() {
        return soadcelp;
    }

    public void setSoadcelp(long soadcelp) {
        this.soadcelp = soadcelp;
    }

    public String getSoadcoerp() {
        return soadcoerp;
    }

    public void setSoadcoerp(String soadcoerp) {
        this.soadcoerp = soadcoerp;
    }

    public String getSoadtirec() {
        return soadtirec;
    }

    public void setSoadtirec(String soadtirec) {
        this.soadtirec = soadtirec;
    }

    public short getSoadtidicc() {
        return soadtidicc;
    }

    public void setSoadtidicc(short soadtidicc) {
        this.soadtidicc = soadtidicc;
    }

    public short getSoadticac() {
        return soadticac;
    }

    public void setSoadticac(short soadticac) {
        this.soadticac = soadticac;
    }

    public String getSoadcallec() {
        return soadcallec;
    }

    public void setSoadcallec(String soadcallec) {
        this.soadcallec = soadcallec;
    }

    public String getSoadnupuc() {
        return soadnupuc;
    }

    public void setSoadnupuc(String soadnupuc) {
        this.soadnupuc = soadnupuc;
    }

    public String getSoadinadc() {
        return soadinadc;
    }

    public void setSoadinadc(String soadinadc) {
        this.soadinadc = soadinadc;
    }

    public String getSoadcopoc() {
        return soadcopoc;
    }

    public void setSoadcopoc(String soadcopoc) {
        this.soadcopoc = soadcopoc;
    }

    public String getSoadzonac() {
        return soadzonac;
    }

    public void setSoadzonac(String soadzonac) {
        this.soadzonac = soadzonac;
    }

    public short getSoadpaisc() {
        return soadpaisc;
    }

    public void setSoadpaisc(short soadpaisc) {
        this.soadpaisc = soadpaisc;
    }

    public String getSoaddptoc() {
        return soaddptoc;
    }

    public void setSoaddptoc(String soaddptoc) {
        this.soaddptoc = soaddptoc;
    }

    public int getSoadlocc() {
        return soadlocc;
    }

    public void setSoadlocc(int soadlocc) {
        this.soadlocc = soadlocc;
    }

    public short getSoadcotepc() {
        return soadcotepc;
    }

    public void setSoadcotepc(short soadcotepc) {
        this.soadcotepc = soadcotepc;
    }

    public int getSoadcoteac() {
        return soadcoteac;
    }

    public void setSoadcoteac(int soadcoteac) {
        this.soadcoteac = soadcoteac;
    }

    public long getSoadtelc() {
        return soadtelc;
    }

    public void setSoadtelc(long soadtelc) {
        this.soadtelc = soadtelc;
    }

    public int getSoadteinc() {
        return soadteinc;
    }

    public void setSoadteinc(int soadteinc) {
        this.soadteinc = soadteinc;
    }

    public short getSoadcocepc() {
        return soadcocepc;
    }

    public void setSoadcocepc(short soadcocepc) {
        this.soadcocepc = soadcocepc;
    }

    public long getSoadcelc() {
        return soadcelc;
    }

    public void setSoadcelc(long soadcelc) {
        this.soadcelc = soadcelc;
    }

    public String getSoadcoerc() {
        return soadcoerc;
    }

    public void setSoadcoerc(String soadcoerc) {
        this.soadcoerc = soadcoerc;
    }

    public String getSoprtire() {
        return soprtire;
    }

    public void setSoprtire(String soprtire) {
        this.soprtire = soprtire;
    }

    public String getSoprcopr() {
        return soprcopr;
    }

    public void setSoprcopr(String soprcopr) {
        this.soprcopr = soprcopr;
    }

    public String getSoprgraf() {
        return soprgraf;
    }

    public void setSoprgraf(String soprgraf) {
        this.soprgraf = soprgraf;
    }

    public String getSoprnutaex() {
        return soprnutaex;
    }

    public void setSoprnutaex(String soprnutaex) {
        this.soprnutaex = soprnutaex;
    }

    public String getSoprfeveex() {
        return soprfeveex;
    }

    public void setSoprfeveex(String soprfeveex) {
        this.soprfeveex = soprfeveex;
    }

    public String getSoprbin() {
        return soprbin;
    }

    public void setSoprbin(String soprbin) {
        this.soprbin = soprbin;
    }

    public String getSoprnutcen() {
        return soprnutcen;
    }

    public void setSoprnutcen(String soprnutcen) {
        this.soprnutcen = soprnutcen;
    }

    public short getSoprinseg() {
        return soprinseg;
    }

    public void setSoprinseg(short soprinseg) {
        this.soprinseg = soprinseg;
    }

    public String getSoprnutc() {
        return soprnutc;
    }

    public void setSoprnutc(String soprnutc) {
        this.soprnutc = soprnutc;
    }

    public String getSoprfevetc() {
        return soprfevetc;
    }

    public void setSoprfevetc(String soprfevetc) {
        this.soprfevetc = soprfevetc;
    }

    public short getSoprsenutc() {
        return soprsenutc;
    }

    public void setSoprsenutc(short soprsenutc) {
        this.soprsenutc = soprsenutc;
    }

    public String getSoprcoer() {
        return soprcoer;
    }

    public void setSoprcoer(String soprcoer) {
        this.soprcoer = soprcoer;
    }

    public Character getSocadm() {
        return socadm;
    }

    public void setSocadm(Character socadm) {
        this.socadm = socadm;
    }

    public short getSosucu() {
        return sosucu;
    }

    public void setSosucu(short sosucu) {
        this.sosucu = sosucu;
    }

    public int getSonsol() {
        return sonsol;
    }

    public void setSonsol(int sonsol) {
        this.sonsol = sonsol;
    }

    public int getSofepro1() {
        return sofepro1;
    }

    public void setSofepro1(int sofepro1) {
        this.sofepro1 = sofepro1;
    }

    public Date getSofeenv() {
        return sofeenv;
    }

    public void setSofeenv(Date sofeenv) {
        this.sofeenv = sofeenv;
    }

    public Date getSoferes() {
        return soferes;
    }

    public void setSoferes(Date soferes) {
        this.soferes = soferes;
    }

    public String getSoest() {
        return soest;
    }

    public void setSoest(String soest) {
        this.soest = soest;
    } //Character

    @Override
    public int hashCode() {
        int hash = 0;
        //hash += (sonsol >0? sonsol.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudTC)) {
            return false;
        }
        /*
        SolicitudTC other = (SolicitudTC) object;
        if ((this.i23id == null && other.i23id != null) || (this.i23id != null && !this.i23id.equals(other.i23id))) {
            return false;
        }*/
        return true;
    }

    @Override
    public String toString() {
        return "SolicitudTC{" +
                "sogatire='" + sogatire + '\'' +
                ", sogagrpr=" + sogagrpr +
                ", sogaprcu=" + sogaprcu +
                ", sogaticu=" + sogaticu +
                ", sogacoem='" + sogacoem + '\'' +
                ", sogasucu1='" + sogasucu1 + '\'' +
                ", sogaagem='" + sogaagem + '\'' +
                ", sogainec=" + sogainec +
                ", sogainecm=" + sogainecm +
                ", sogaemail='" + sogaemail + '\'' +
                ", sogainse=" + sogainse +
                ", soganutcnc=" + soganutcnc +
                ", sogacoer='" + sogacoer + '\'' +
                ", socctire='" + socctire + '\'' +
                ", socclicr=" + socclicr +
                ", soccmoli=" + soccmoli +
                ", soccfeci=" + soccfeci +
                ", soccfopa=" + soccfopa +
                ", socctide=" + socctide +
                ", socccoer='" + socccoer + '\'' +
                ", socftire='" + socftire + '\'' +
                ", socfticl=" + socfticl +
                ", socfnocl='" + socfnocl + '\'' +
                ", socfapcl='" + socfapcl + '\'' +
                ", socftido=" + socftido +
                ", socfdocl='" + socfdocl + '\'' +
                ", socftidoa=" + socftidoa +
                ", socfdocla='" + socfdocla + '\'' +
                ", socffena='" + socffena + '\'' +
                ", socfesci=" + socfesci +
                ", socfgecl=" + socfgecl +
                ", socfpacl=" + socfpacl +
                ", socfluna='" + socfluna + '\'' +
                ", socfcahi=" + socfcahi +
                ", socfnoem='" + socfnoem + '\'' +
                ", socfemail='" + socfemail + '\'' +
                ", socfdcpa=" + socfdcpa +
                ", socfdfpa=" + socfdfpa +
                ", socfnusol=" + socfnusol +
                ", socfcoer='" + socfcoer + '\'' +
                ", soeitire='" + soeitire + '\'' +
                ", soeicontr=" + soeicontr +
                ", soeifeveco='" + soeifeveco + '\'' +
                ", soeiempl='" + soeiempl + '\'' +
                ", soeitido=" + soeitido +
                ", soeidoem='" + soeidoem + '\'' +
                ", soeifein='" + soeifein + '\'' +
                ", soeiinpr=" + soeiinpr +
                ", soeiinext=" + soeiinext +
                ", soeitoin=" + soeitoin +
                ", soeicaem='" + soeicaem + '\'' +
                ", soeiocup=" + soeiocup +
                ", soeiseem=" + soeiseem +
                ", soeicoer='" + soeicoer + '\'' +
                ", soadtirep='" + soadtirep + '\'' +
                ", soadtidicp=" + soadtidicp +
                ", soadticap=" + soadticap +
                ", soadcallep='" + soadcallep + '\'' +
                ", soadnupup='" + soadnupup + '\'' +
                ", soadinadp='" + soadinadp + '\'' +
                ", soadcopop='" + soadcopop + '\'' +
                ", soadzonap='" + soadzonap + '\'' +
                ", soadpaisp=" + soadpaisp +
                ", soaddptop='" + soaddptop + '\'' +
                ", soadlocp=" + soadlocp +
                ", soadcotepp=" + soadcotepp +
                ", soadcoteap=" + soadcoteap +
                ", soadtelp=" + soadtelp +
                ", soadteinp=" + soadteinp +
                ", soadcocepp=" + soadcocepp +
                ", soadcelp=" + soadcelp +
                ", soadcoerp='" + soadcoerp + '\'' +
                ", soadtirec='" + soadtirec + '\'' +
                ", soadtidicc=" + soadtidicc +
                ", soadticac=" + soadticac +
                ", soadcallec='" + soadcallec + '\'' +
                ", soadnupuc='" + soadnupuc + '\'' +
                ", soadinadc='" + soadinadc + '\'' +
                ", soadcopoc='" + soadcopoc + '\'' +
                ", soadzonac='" + soadzonac + '\'' +
                ", soadpaisc=" + soadpaisc +
                ", soaddptoc='" + soaddptoc + '\'' +
                ", soadlocc=" + soadlocc +
                ", soadcotepc=" + soadcotepc +
                ", soadcoteac=" + soadcoteac +
                ", soadtelc=" + soadtelc +
                ", soadteinc=" + soadteinc +
                ", soadcocepc=" + soadcocepc +
                ", soadcelc=" + soadcelc +
                ", soadcoerc='" + soadcoerc + '\'' +
                ", soprtire='" + soprtire + '\'' +
                ", soprcopr='" + soprcopr + '\'' +
                ", soprgraf='" + soprgraf + '\'' +
                ", soprnutaex='" + soprnutaex + '\'' +
                ", soprfeveex='" + soprfeveex + '\'' +
                ", soprbin='" + soprbin + '\'' +
                ", soprnutcen='" + soprnutcen + '\'' +
                ", soprinseg=" + soprinseg +
                ", soprnutc='" + soprnutc + '\'' +
                ", soprfevetc='" + soprfevetc + '\'' +
                ", soprsenutc=" + soprsenutc +
                ", soprcoer='" + soprcoer + '\'' +
                ", socadm=" + socadm +
                ", sosucu=" + sosucu +
                ", sonsol=" + sonsol +
                ", sofepro1=" + sofepro1 +
                ", sofeenv=" + sofeenv +
                ", soferes=" + soferes +
                ", soest='" + soest + '\'' +
                ", estado=" + estado +
                ", reusre='" + getUsuarioCreador() + '\'' +
                ", redate=" + getFechaCreacion() +
                ", reusrr='" + getUsuarioModificador() + '\'' +
                ", redatr=" + getFechaModificacion() +
                ", idArchivoEnvio=" + idArchivoEnvio +
                ", idArchivoRecepcion=" + idArchivoRecepcion +
                ", tipoDocumento='" + tipoDocumento + '\'' +
                ", estadoCivil='" + estadoCivil + '\'' +
                ", sexo='" + sexo + '\'' +
                ", tipoTarjeta='" + tipoTarjeta + '\'' +
                '}';
    }

    public EstadoSolicitudTC getEstado() {
        return EstadoSolicitudTC.valorEnum(soest) != null ? EstadoSolicitudTC.valorEnum(soest) : null; // EstadoSolicitudTC.GENE
    }

    public void setEstado(EstadoSolicitudTC estado) {
        if (estado != null) {
            this.soest = estado.name();
            this.estado = estado;
        } else {
            this.soest = "";
            this.estado = null;
        }
    }

    public String getEstadoDescripcion() {
        return EstadoSolicitudTC.valorEnum(soest) != null ? EstadoSolicitudTC.valorEnum(soest).getDescripcion() : "Desconocido";
    }

    public BigInteger getIdArchivoEnvio() {
        return idArchivoEnvio;
    }

    public void setIdArchivoEnvio(BigInteger idArchivoEnvio) {
        this.idArchivoEnvio = idArchivoEnvio;
    }

    public BigInteger getIdArchivoRecepcion() {
        return idArchivoRecepcion;
    }

    public void setIdArchivoRecepcion(BigInteger idArchivoRecepcion) {
        this.idArchivoRecepcion = idArchivoRecepcion;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTipoTarjeta() {
        return tipoTarjeta;
    }

    public void setTipoTarjeta(String tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getFechaEnvioFormato() {
        return FormatosUtils.formatoFechaHora(sofeenv);
    }

    public String getFechaRecepcionFormato() {
        return FormatosUtils.formatoFechaHora(soferes);
    }

    public String getFechaProcesoFormato() {
        return FormatosUtils.formatoFechaHora(fechaCreacion);
    }

    public long getSonutc() {
        return sonutc;
    }

    public void setSonutc(long sonutc) {
        this.sonutc = sonutc;
    }

    public String getSonut1() {
        return sonut1;
    }

    public void setSonut1(String sonut1) {
        this.sonut1 = sonut1;
    }
}
