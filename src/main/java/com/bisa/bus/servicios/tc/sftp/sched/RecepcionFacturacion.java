package com.bisa.bus.servicios.tc.sftp.sched;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.bus.servicios.tc.sftp.api.SftpRecepcionExtractoMensual;
import com.bisa.bus.servicios.tc.sftp.api.SftpRecepcionFacturas;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 08/08/2017.
 */
public class RecepcionFacturacion implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecepcionFacturacion.class);

    public static String NOMBRE_TAREA = "RecepcionFacturacion";
    private final MedioAmbiente medioAmbiente;
    private final NotificacionesCorreo notificacionesCorreo;
    private final SftpRecepcionFacturas sftpRecepcionFacturas;
    private final SftpRecepcionExtractoMensual sftpRecepcionExtractoMensual;

    private volatile boolean ok;

    @Inject
    public RecepcionFacturacion(MedioAmbiente medioAmbiente, NotificacionesCorreo notificacionesCorreo,
                                SftpRecepcionFacturas sftpRecepcionFacturas,
                                SftpRecepcionExtractoMensual sftpRecepcionExtractoMensual){
        this.medioAmbiente = medioAmbiente;
        this.notificacionesCorreo = notificacionesCorreo;
        this.sftpRecepcionFacturas = sftpRecepcionFacturas;
        this.sftpRecepcionExtractoMensual = sftpRecepcionExtractoMensual;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        LOGGER.info("Realizando llamada a proceso");
        // Procesando facturacion
        ok = sftpRecepcionFacturas.procesarArchivo();
        ok = ok && sftpRecepcionExtractoMensual.procesarArchivo();
        LOGGER.info("Archivos procesados {}", ok);

        if (ok) {
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_FACTURACION_OPERADOR, Variables.FTP_MENSAJE_EMAIL_FACTURACION_OPERADOR_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador(sftpRecepcionFacturas.getNombreArchivoProcesado() +" y "+ sftpRecepcionExtractoMensual.getNombreArchivoProcesado() , TipoArchivo.FAC, mensaje);
        } else {
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_FACTURACION_ERROR_OPERADOR, Variables.FTP_MENSAJE_EMAIL_FACTURACION_ERROR_OPERADOR_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador("", TipoArchivo.FAC, mensaje);
        }
    }
}
