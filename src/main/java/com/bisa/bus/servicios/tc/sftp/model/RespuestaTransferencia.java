package com.bisa.bus.servicios.tc.sftp.model;

import java.util.HashMap;

/**
 * Created by atenorio on 01/12/2017.
 */
public class RespuestaTransferencia {
    private HashMap<String, String> archivosProcesados = new HashMap<String, String>();
    private boolean valido = true;
    private boolean detalleValidacion = true;
    private int totalCorrectos = 0;
    private int totalIncorrectos = 0;

    public HashMap<String, String> getArchivosProcesados() {
        return archivosProcesados;
    }

    public void setArchivosProcesados(HashMap<String, String> archivosProcesados) {
        this.archivosProcesados = archivosProcesados;
    }

    public boolean isValido() {
        return valido;
    }

    public void setValido(boolean valido) {
        this.valido = valido;
    }

    public boolean isDetalleValidacion() {
        return detalleValidacion;
    }

    public void setDetalleValidacion(boolean detalleValidacion) {
        this.detalleValidacion = detalleValidacion;
    }

    public int getTotalCorrectos() {
        return totalCorrectos;
    }

    public void setTotalCorrectos(int totalCorrectos) {
        this.totalCorrectos = totalCorrectos;
    }

    public int getTotalIncorrectos() {
        return totalIncorrectos;
    }

    public void setTotalIncorrectos(int totalIncorrectos) {
        this.totalIncorrectos = totalIncorrectos;
    }
}
