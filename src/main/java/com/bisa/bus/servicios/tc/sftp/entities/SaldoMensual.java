package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by atenorio on 20/06/2017.
 * ATCUC079 Registros de saldo mensual de la cartera liquidada a nivel de cuenta
 *
 */
@Entity
@Table(name = "TCPANSAME")
public class SaldoMensual implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private IdSaldoMensual idSaldoMensual;

    @Basic(optional = false)
    @Column(name = "SMBAEM", nullable = false, length = 4)
    private String codigoBanco;


    @Basic(optional = false)
    @Column(name = "SMNOCL", nullable = false, length = 40)
    private String nombreCliente;
    @Basic(optional = false)
    @Column(name = "SMDIR", nullable = false, length = 44)
    private String direccion;
    @Basic(optional = false)
    @Column(name = "SMTELF", nullable = false, length = 8)
    private String telefono;
    @Basic(optional = false)
    @Column(name = "SMCASI", nullable = false, length = 5)
    private String casilla;
    @Basic(optional = false)
    @Column(name = "SMZONA", nullable = false, length = 15)
    private String direccionZona;
    @Basic(optional = false)
    @Column(name = "SMDIRPOS", nullable = false, length = 44)
    private String direccionPostal;
    @Basic(optional = false)
    @Column(name = "SMZPOST", nullable = false, length = 15)
    private String zonaPostal;
    @Basic(optional = false)
    @Column(name = "SMTCTABCO", nullable = false, length = 2)
    private String tipoCuentaBanco;
    @Basic(optional = false)
    @Column(name = "SMNCTABCO", nullable = false, length = 14)
    private String numeroCuentaBanco;
    @Basic(optional = false)
    @Column(name = "SMPMINANTS", nullable = false)
    private Character pagoMinimoAnteriorSigno;
    @Basic(optional = false)
    @Column(name = "SMPMINANT", nullable = false, length = 10)
    private String pagoMinimoAnteriorTexto;
    @Basic(optional = false)
    @Column(name = "SMPMINANTN", nullable = false, precision = 11, scale = 2)
    private BigDecimal pagoMinimoAnterior;

    @Basic(optional = false)
    @Column(name = "SMSALANBSS", nullable = false)
    private Character saldoAnteriorBsSigno;
    @Basic(optional = false)
    @Column(name = "SMSALANBS", nullable = false, length = 10)
    private String saldoAnteriorBsTexto;
    @Basic(optional = false)
    @Column(name = "SMSALANBSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal saldoAnteriorBs;

    @Basic(optional = false)
    @Column(name = "SMSALANUSS", nullable = false)
    private Character saldoAnteriorUsSigno;
    @Basic(optional = false)
    @Column(name = "SMSALANUS", nullable = false, length = 10)
    private String saldoAnteriorUsTexto;
    @Basic(optional = false)
    @Column(name = "SMSALANUSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal saldoAnteriorUs;

    @Basic(optional = false)
    @Column(name = "SMPAGOS", nullable = false)
    private Character pagoSigno;
    @Basic(optional = false)
    @Column(name = "SMPAGO", nullable = false, length = 10)
    private String pagoTexto;
    @Basic(optional = false)
    @Column(name = "SMPAGON", nullable = false, precision = 11, scale = 2)
    private BigDecimal pago;

    @Basic(optional = false)
    @Column(name = "SMCARGBSS", nullable = false)
    private Character cargosBsSigno;
    @Basic(optional = false)
    @Column(name = "SMCARGBS", nullable = false, length = 10)
    private String cargosBstexto;
    @Basic(optional = false)
    @Column(name = "SMCARGBSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal cargosBs;

    @Basic(optional = false)
    @Column(name = "SMCARGUSS", nullable = false)
    private Character cargosUsSigno;
    @Basic(optional = false)
    @Column(name = "SMCARGUS", nullable = false, length = 10)
    private String cargosUsTexto;
    @Basic(optional = false)
    @Column(name = "SMCARGUSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal cargosUs;

    @Basic(optional = false)
    @Column(name = "SMINTES", nullable = false)
    private Character ineteresSigno;
    @Basic(optional = false)
    @Column(name = "SMINTE", nullable = false, length = 10)
    private String ineteresTexto;
    @Basic(optional = false)
    @Column(name = "SMINTEN", nullable = false, precision = 11, scale = 2)
    private BigDecimal ineteres;

    @Basic(optional = false)
    @Column(name = "SMSALACBSS", nullable = false)
    private Character saldoActualBsSigno;
    @Basic(optional = false)
    @Column(name = "SMSALACBS", nullable = false, length = 10)
    private String saldoActualBsTexto;
    @Basic(optional = false)
    @Column(name = "SMSALACBSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal saldoActualBs;

    @Basic(optional = false)
    @Column(name = "SMSALACUSS", nullable = false)
    private Character saldoActualUsSigno;
    @Basic(optional = false)
    @Column(name = "SMSALACUS", nullable = false, length = 10)
    private String saldoActualUstexto;
    @Basic(optional = false)
    @Column(name = "SMSALACUSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal saldoActualUs;

    @Basic(optional = false)
    @Column(name = "SMPAGMINAS", nullable = false)
    private Character pagoMinimoActualSigno;
    @Basic(optional = false)
    @Column(name = "SMPAGMINA", nullable = false, length = 10)
    private String pagoMinimoActualTexto;
    @Basic(optional = false)
    @Column(name = "SMPAGMINAN", nullable = false, precision = 11, scale = 2)
    private BigDecimal pagoMinimoActual;

    @Basic(optional = false)
    @Column(name = "SMLIMCOMPS", nullable = false)
    private Character limiteCompraSigno;
    @Basic(optional = false)
    @Column(name = "SMLIMCOMP", nullable = false, length = 10)
    private String limiteCompraTexto;
    @Basic(optional = false)
    @Column(name = "SMLIMCOMPN", nullable = false, precision = 11, scale = 2)
    private BigDecimal limiteCompra;

    @Basic(optional = false)
    @Column(name = "SMLIMFINAS", nullable = false)
    private Character limiteFinanciamientoSigno;
    @Basic(optional = false)
    @Column(name = "SMLIMFINA", nullable = false, length = 10)
    private String limiteFinanciamientoTexto;
    @Basic(optional = false)
    @Column(name = "SMLIMFINAN", nullable = false, precision = 11, scale = 2)
    private BigDecimal limiteFinanciamiento;

    @Basic(optional = false)
    @Column(name = "SMFPROC", nullable = false, length = 8)
    private String fechaProceso;
    @Basic(optional = false)
    @Column(name = "SMFVTO", nullable = false, length = 8)
    private String fechaVencimiento;
    @Basic(optional = false)
    @Column(name = "SMFPAGO", nullable = false, length = 8)
    private String fechaPago;
    @Basic(optional = false)
    @Column(name = "SMFATACTA", nullable = false, length = 8)
    private String fechaAltaCuenta;
    @Basic(optional = false)
    @Column(name = "SMFVTOTAR", nullable = false, length = 8)
    private String fechaVencimientoTarj;
    @Basic(optional = false)
    @Column(name = "SMTCAMBIO", nullable = false, length = 6)
    private String tipoCambio;
    @Basic(optional = false)
    @Column(name = "SMNIVMORA", nullable = false, length = 2)
    private String nivelMora;
    @Basic(optional = false)
    @Column(name = "SMMONCTA", nullable = false, length = 3)
    private String monedaCuenta;
    @Basic(optional = false)
    @Column(name = "SMAFINI", nullable = false, length = 4)
    private String grupoAfinidad;


    @Basic(optional = false)
    @Column(name = "SMCADM", nullable = false)
    private Character codigoAdm;

    @Basic(optional = false)
    @Column(name = "SMFEPRO1", nullable = false)
    private int fechaProcesoArchivo;

    @Basic(optional = false)
    @Column(name = "SMNUTC", nullable = false)
    private long cuentaTarjeta;
    @Basic(optional = false)
    @Column(name = "SMNUT1", nullable = false, length = 9)
    private String cuentaTarjetaReducida = "";
    @Basic(optional = false)
    @Column(name = "SMEST", nullable = false, length = 4)
    private String estado="";

    @Basic(optional = false)
    @Column(name = "SMIDRE")
    private long idArchivoRecepcion;
    @Basic(optional = false)
    @Column(name = "SMFERE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRecepcion;

    @Basic(optional = false)
    @Column(name = "SMPGCR", nullable = false, length = 10)
    private String usuarioCreador;
    @Basic(optional = false)
    @Column(name = "SMFECR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @Column(name = "SMPGMO", nullable = false, length = 10)
    private String usuarioModificador;
    @Basic(optional = false)
    @Column(name = "SMFEMO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    public SaldoMensual() {
    }


    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCasilla() {
        return casilla;
    }

    public void setCasilla(String casilla) {
        this.casilla = casilla;
    }

    public String getDireccionZona() {
        return direccionZona;
    }

    public void setDireccionZona(String direccionZona) {
        this.direccionZona = direccionZona;
    }

    public String getDireccionPostal() {
        return direccionPostal;
    }

    public void setDireccionPostal(String direccionPostal) {
        this.direccionPostal = direccionPostal;
    }

    public String getZonaPostal() {
        return zonaPostal;
    }

    public void setZonaPostal(String zonaPostal) {
        this.zonaPostal = zonaPostal;
    }

    public String getTipoCuentaBanco() {
        return tipoCuentaBanco;
    }

    public void setTipoCuentaBanco(String tipoCuentaBanco) {
        this.tipoCuentaBanco = tipoCuentaBanco;
    }

    public String getNumeroCuentaBanco() {
        return numeroCuentaBanco;
    }

    public void setNumeroCuentaBanco(String numeroCuentaBanco) {
        this.numeroCuentaBanco = numeroCuentaBanco;
    }

    public String getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(String fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getFechaAltaCuenta() {
        return fechaAltaCuenta;
    }

    public void setFechaAltaCuenta(String fechaAltaCuenta) {
        this.fechaAltaCuenta = fechaAltaCuenta;
    }

    public String getFechaVencimientoTarj() {
        return fechaVencimientoTarj;
    }

    public void setFechaVencimientoTarj(String fechaVencimientoTarj) {
        this.fechaVencimientoTarj = fechaVencimientoTarj;
    }

    public String getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public String getNivelMora() {
        return nivelMora;
    }

    public void setNivelMora(String nivelMora) {
        this.nivelMora = nivelMora;
    }

    public String getMonedaCuenta() {
        return monedaCuenta;
    }

    public void setMonedaCuenta(String monedaCuenta) {
        this.monedaCuenta = monedaCuenta;
    }

    public String getGrupoAfinidad() {
        return grupoAfinidad;
    }

    public void setGrupoAfinidad(String grupoAfinidad) {
        this.grupoAfinidad = grupoAfinidad;
    }

    public Character getCodigoAdm() {
        return codigoAdm;
    }

    public void setCodigoAdm(Character codigoAdm) {
        this.codigoAdm = codigoAdm;
    }


    public long getCuentaTarjeta() {
        return cuentaTarjeta;
    }

    public void setCuentaTarjeta(long cuentaTarjeta) {
        this.cuentaTarjeta = cuentaTarjeta;
    }

    public String getCuentaTarjetaReducida() {
        return cuentaTarjetaReducida;
    }

    public void setCuentaTarjetaReducida(String cuentaTarjetaReducida) {
        this.cuentaTarjetaReducida = cuentaTarjetaReducida;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public long getIdArchivoRecepcion() {
        return idArchivoRecepcion;
    }

    public void setIdArchivoRecepcion(long idArchivoRecepcion) {
        this.idArchivoRecepcion = idArchivoRecepcion;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public int getFechaProcesoArchivo() {
        return fechaProcesoArchivo;
    }

    public void setFechaProcesoArchivo(int fechaProcesoArchivo) {
        this.fechaProcesoArchivo = fechaProcesoArchivo;
    }

    public IdSaldoMensual getIdSaldoMensual() {
        return idSaldoMensual;
    }

    public void setIdSaldoMensual(IdSaldoMensual idSaldoMensual) {
        this.idSaldoMensual = idSaldoMensual;
    }

    public Character getPagoMinimoAnteriorSigno() {
        return pagoMinimoAnteriorSigno;
    }

    public void setPagoMinimoAnteriorSigno(Character pagoMinimoAnteriorSigno) {
        this.pagoMinimoAnteriorSigno = pagoMinimoAnteriorSigno;
    }

    public String getPagoMinimoAnteriorTexto() {
        return pagoMinimoAnteriorTexto;
    }

    public void setPagoMinimoAnteriorTexto(String pagoMinimoAnteriorTexto) {
        this.pagoMinimoAnteriorTexto = pagoMinimoAnteriorTexto;
    }

    public BigDecimal getPagoMinimoAnterior() {
        return pagoMinimoAnterior;
    }

    public void setPagoMinimoAnterior(BigDecimal pagoMinimoAnterior) {
        this.pagoMinimoAnterior = pagoMinimoAnterior;
    }

    public Character getSaldoAnteriorBsSigno() {
        return saldoAnteriorBsSigno;
    }

    public void setSaldoAnteriorBsSigno(Character saldoAnteriorBsSigno) {
        this.saldoAnteriorBsSigno = saldoAnteriorBsSigno;
    }

    public String getSaldoAnteriorBsTexto() {
        return saldoAnteriorBsTexto;
    }

    public void setSaldoAnteriorBsTexto(String saldoAnteriorBsTexto) {
        this.saldoAnteriorBsTexto = saldoAnteriorBsTexto;
    }

    public BigDecimal getSaldoAnteriorBs() {
        return saldoAnteriorBs;
    }

    public void setSaldoAnteriorBs(BigDecimal saldoAnteriorBs) {
        this.saldoAnteriorBs = saldoAnteriorBs;
    }

    public Character getSaldoAnteriorUsSigno() {
        return saldoAnteriorUsSigno;
    }

    public void setSaldoAnteriorUsSigno(Character saldoAnteriorUsSigno) {
        this.saldoAnteriorUsSigno = saldoAnteriorUsSigno;
    }

    public String getSaldoAnteriorUsTexto() {
        return saldoAnteriorUsTexto;
    }

    public void setSaldoAnteriorUsTexto(String saldoAnteriorUsTexto) {
        this.saldoAnteriorUsTexto = saldoAnteriorUsTexto;
    }

    public BigDecimal getSaldoAnteriorUs() {
        return saldoAnteriorUs;
    }

    public void setSaldoAnteriorUs(BigDecimal saldoAnteriorUs) {
        this.saldoAnteriorUs = saldoAnteriorUs;
    }

    public Character getPagoSigno() {
        return pagoSigno;
    }

    public void setPagoSigno(Character pagoSigno) {
        this.pagoSigno = pagoSigno;
    }

    public String getPagoTexto() {
        return pagoTexto;
    }

    public void setPagoTexto(String pagoTexto) {
        this.pagoTexto = pagoTexto;
    }

    public BigDecimal getPago() {
        return pago;
    }

    public void setPago(BigDecimal pago) {
        this.pago = pago;
    }

    public Character getCargosBsSigno() {
        return cargosBsSigno;
    }

    public void setCargosBsSigno(Character cargosBsSigno) {
        this.cargosBsSigno = cargosBsSigno;
    }

    public String getCargosBstexto() {
        return cargosBstexto;
    }

    public void setCargosBstexto(String cargosBstexto) {
        this.cargosBstexto = cargosBstexto;
    }

    public BigDecimal getCargosBs() {
        return cargosBs;
    }

    public void setCargosBs(BigDecimal cargosBs) {
        this.cargosBs = cargosBs;
    }

    public Character getCargosUsSigno() {
        return cargosUsSigno;
    }

    public void setCargosUsSigno(Character cargosUsSigno) {
        this.cargosUsSigno = cargosUsSigno;
    }

    public String getCargosUsTexto() {
        return cargosUsTexto;
    }

    public void setCargosUsTexto(String cargosUsTexto) {
        this.cargosUsTexto = cargosUsTexto;
    }

    public BigDecimal getCargosUs() {
        return cargosUs;
    }

    public void setCargosUs(BigDecimal cargosUs) {
        this.cargosUs = cargosUs;
    }

    public Character getIneteresSigno() {
        return ineteresSigno;
    }

    public void setIneteresSigno(Character ineteresSigno) {
        this.ineteresSigno = ineteresSigno;
    }

    public String getIneteresTexto() {
        return ineteresTexto;
    }

    public void setIneteresTexto(String ineteresTexto) {
        this.ineteresTexto = ineteresTexto;
    }

    public BigDecimal getIneteres() {
        return ineteres;
    }

    public void setIneteres(BigDecimal ineteres) {
        this.ineteres = ineteres;
    }

    public Character getSaldoActualBsSigno() {
        return saldoActualBsSigno;
    }

    public void setSaldoActualBsSigno(Character saldoActualBsSigno) {
        this.saldoActualBsSigno = saldoActualBsSigno;
    }

    public String getSaldoActualBsTexto() {
        return saldoActualBsTexto;
    }

    public void setSaldoActualBsTexto(String saldoActualBsTexto) {
        this.saldoActualBsTexto = saldoActualBsTexto;
    }

    public BigDecimal getSaldoActualBs() {
        return saldoActualBs;
    }

    public void setSaldoActualBs(BigDecimal saldoActualBs) {
        this.saldoActualBs = saldoActualBs;
    }

    public Character getSaldoActualUsSigno() {
        return saldoActualUsSigno;
    }

    public void setSaldoActualUsSigno(Character saldoActualUsSigno) {
        this.saldoActualUsSigno = saldoActualUsSigno;
    }

    public String getSaldoActualUstexto() {
        return saldoActualUstexto;
    }

    public void setSaldoActualUstexto(String saldoActualUstexto) {
        this.saldoActualUstexto = saldoActualUstexto;
    }

    public BigDecimal getSaldoActualUs() {
        return saldoActualUs;
    }

    public void setSaldoActualUs(BigDecimal saldoActualUs) {
        this.saldoActualUs = saldoActualUs;
    }

    public Character getPagoMinimoActualSigno() {
        return pagoMinimoActualSigno;
    }

    public void setPagoMinimoActualSigno(Character pagoMinimoActualSigno) {
        this.pagoMinimoActualSigno = pagoMinimoActualSigno;
    }

    public String getPagoMinimoActualTexto() {
        return pagoMinimoActualTexto;
    }

    public void setPagoMinimoActualTexto(String pagoMinimoActualTexto) {
        this.pagoMinimoActualTexto = pagoMinimoActualTexto;
    }

    public BigDecimal getPagoMinimoActual() {
        return pagoMinimoActual;
    }

    public void setPagoMinimoActual(BigDecimal pagoMinimoActual) {
        this.pagoMinimoActual = pagoMinimoActual;
    }

    public Character getLimiteCompraSigno() {
        return limiteCompraSigno;
    }

    public void setLimiteCompraSigno(Character limiteCompraSigno) {
        this.limiteCompraSigno = limiteCompraSigno;
    }

    public String getLimiteCompraTexto() {
        return limiteCompraTexto;
    }

    public void setLimiteCompraTexto(String limiteCompraTexto) {
        this.limiteCompraTexto = limiteCompraTexto;
    }

    public BigDecimal getLimiteCompra() {
        return limiteCompra;
    }

    public void setLimiteCompra(BigDecimal limiteCompra) {
        this.limiteCompra = limiteCompra;
    }

    public Character getLimiteFinanciamientoSigno() {
        return limiteFinanciamientoSigno;
    }

    public void setLimiteFinanciamientoSigno(Character limiteFinanciamientoSigno) {
        this.limiteFinanciamientoSigno = limiteFinanciamientoSigno;
    }

    public String getLimiteFinanciamientoTexto() {
        return limiteFinanciamientoTexto;
    }

    public void setLimiteFinanciamientoTexto(String limiteFinanciamientoTexto) {
        this.limiteFinanciamientoTexto = limiteFinanciamientoTexto;
    }

    public BigDecimal getLimiteFinanciamiento() {
        return limiteFinanciamiento;
    }

    public void setLimiteFinanciamiento(BigDecimal limiteFinanciamiento) {
        this.limiteFinanciamiento = limiteFinanciamiento;
    }

    @Override
    public String toString() {
        return "SaldoMensual{" +
                "idSaldoMensual=" + idSaldoMensual +
                ", codigoBanco='" + codigoBanco + '\'' +
                ", nombreCliente='" + nombreCliente + '\'' +
                ", direccion='" + direccion + '\'' +
                ", telefono='" + telefono + '\'' +
                ", casilla='" + casilla + '\'' +
                ", direccionZona='" + direccionZona + '\'' +
                ", direccionPostal='" + direccionPostal + '\'' +
                ", zonaPostal='" + zonaPostal + '\'' +
                ", tipoCuentaBanco='" + tipoCuentaBanco + '\'' +
                ", numeroCuentaBanco='" + numeroCuentaBanco + '\'' +
                ", pagoMinimoAnteriorSigno=" + pagoMinimoAnteriorSigno +
                ", pagoMinimoAnteriorTexto='" + pagoMinimoAnteriorTexto + '\'' +
                ", pagoMinimoAnterior=" + pagoMinimoAnterior +
                ", saldoAnteriorBsSigno=" + saldoAnteriorBsSigno +
                ", saldoAnteriorBsTexto='" + saldoAnteriorBsTexto + '\'' +
                ", saldoAnteriorBs=" + saldoAnteriorBs +
                ", saldoAnteriorUsSigno=" + saldoAnteriorUsSigno +
                ", saldoAnteriorUsTexto='" + saldoAnteriorUsTexto + '\'' +
                ", saldoAnteriorUs=" + saldoAnteriorUs +
                ", pagoSigno=" + pagoSigno +
                ", pagoTexto='" + pagoTexto + '\'' +
                ", pago=" + pago +
                ", cargosBsSigno=" + cargosBsSigno +
                ", cargosBstexto='" + cargosBstexto + '\'' +
                ", cargosBs=" + cargosBs +
                ", cargosUsSigno=" + cargosUsSigno +
                ", cargosUsTexto='" + cargosUsTexto + '\'' +
                ", cargosUs=" + cargosUs +
                ", ineteresSigno=" + ineteresSigno +
                ", ineteresTexto='" + ineteresTexto + '\'' +
                ", ineteres=" + ineteres +
                ", saldoActualBsSigno=" + saldoActualBsSigno +
                ", saldoActualBsTexto='" + saldoActualBsTexto + '\'' +
                ", saldoActualBs=" + saldoActualBs +
                ", saldoActualUsSigno=" + saldoActualUsSigno +
                ", saldoActualUstexto='" + saldoActualUstexto + '\'' +
                ", saldoActualUs=" + saldoActualUs +
                ", pagoMinimoActualSigno=" + pagoMinimoActualSigno +
                ", pagoMinimoActualTexto='" + pagoMinimoActualTexto + '\'' +
                ", pagoMinimoActual=" + pagoMinimoActual +
                ", limiteCompraSigno=" + limiteCompraSigno +
                ", limiteCompraTexto='" + limiteCompraTexto + '\'' +
                ", limiteCompra=" + limiteCompra +
                ", limiteFinanciamientoSigno=" + limiteFinanciamientoSigno +
                ", limiteFinanciamientoTexto='" + limiteFinanciamientoTexto + '\'' +
                ", limiteFinanciamiento=" + limiteFinanciamiento +
                ", fechaProceso='" + fechaProceso + '\'' +
                ", fechaVencimiento='" + fechaVencimiento + '\'' +
                ", fechaPago='" + fechaPago + '\'' +
                ", fechaAltaCuenta='" + fechaAltaCuenta + '\'' +
                ", fechaVencimientoTarj='" + fechaVencimientoTarj + '\'' +
                ", tipoCambio='" + tipoCambio + '\'' +
                ", nivelMora='" + nivelMora + '\'' +
                ", monedaCuenta='" + monedaCuenta + '\'' +
                ", grupoAfinidad='" + grupoAfinidad + '\'' +
                ", codigoAdm=" + codigoAdm +
                ", fechaProcesoArchivo=" + fechaProcesoArchivo +
                ", cuentaTarjeta=" + cuentaTarjeta +
                ", cuentaTarjetaReducida='" + cuentaTarjetaReducida + '\'' +
                ", estado='" + estado + '\'' +
                ", idArchivoRecepcion=" + idArchivoRecepcion +
                ", fechaRecepcion=" + fechaRecepcion +
                ", usuarioCreador='" + usuarioCreador + '\'' +
                ", fechaCreacion=" + fechaCreacion +
                ", usuarioModificador='" + usuarioModificador + '\'' +
                ", fechaModificacion=" + fechaModificacion +
                '}';
    }
}