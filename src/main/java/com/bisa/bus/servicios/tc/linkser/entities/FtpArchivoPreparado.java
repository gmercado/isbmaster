package com.bisa.bus.servicios.tc.linkser.entities;

import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.tc.sftp.model.EstadoTransferencia;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 16/11/2017.
 */
@Entity
@Table(name = "TCPCTRPRO")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "CTPFEC")),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "CTPPGC")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "CTPFEM")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "CTPPGM"))})
public class FtpArchivoPreparado  extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private IdFtpArchivoPreparado id;
    /*@Id
    @Basic(optional = false)
    @Column(name = "CTPFEI", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "CTPTIP", nullable = false, length = 3)
    @Enumerated(EnumType.STRING)
    private TipoArchivo tipo;
    */
    @Basic(optional = false)
    @Column(name = "CTPARC", length = 30)
    private String archivo;
    @Basic(optional = false)
    @Column(name = "CTPDET", length = 77)
    private String detalle;
    @Basic(optional = false)
    @Column(name = "CTPEST")
    @Enumerated(EnumType.STRING)
    private EstadoTransferencia estado;

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }
    /*
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoArchivo getTipo() {
        return tipo;
    }

    public void setTipo(TipoArchivo tipo) {
        this.tipo = tipo;
    }
    */

    public EstadoTransferencia getEstado() {
        return estado;
    }

    public void setEstado(EstadoTransferencia estado) {
        this.estado = estado;
    }

    public String getEstadoDescripcion(){
        return estado.getDescripcion();
    }

    public String getFechaProcesoFormato(){
        return FormatosUtils.formatoFechaHora(fechaCreacion);
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    @Override
    public String toString() {
        return "FtpArchivoPreparado{" +
                "id=" + getId() +
                ", archivo='" + archivo + '\'' +
                ", detalle='" + detalle + '\'' +
                ", estado=" + estado +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FtpArchivoPreparado that = (FtpArchivoPreparado) o;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getArchivo() != null ? !getArchivo().equals(that.getArchivo()) : that.getArchivo() != null) return false;
        if (getDetalle() != null ? !getDetalle().equals(that.getDetalle()) : that.getDetalle() != null) return false;
        return getEstado() == that.getEstado();
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getArchivo() != null ? getArchivo().hashCode() : 0);
        result = 31 * result + (getDetalle() != null ? getDetalle().hashCode() : 0);
        result = 31 * result + (getEstado() != null ? getEstado().hashCode() : 0);
        return result;
    }


    public IdFtpArchivoPreparado getId() {
        return id;
    }

    public void setId(IdFtpArchivoPreparado id) {
        this.id = id;
    }
}