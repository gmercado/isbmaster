package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.IdParametroAtc;
import com.bisa.bus.servicios.tc.sftp.entities.ParametrosAtc;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.LinkedList;

/**
 * Created by atenorio on 26/05/2017.
 */
public class ParametrosAtcDao extends DaoImpl<ParametrosAtc, IdParametroAtc> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParametrosAtcDao.class);
    @Inject
    protected ParametrosAtcDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public ParametrosAtc getParametroBanco(String categoria, String parametro) {
        LOGGER.debug("Obteniendo registro categoria:{} y parametro: {}.", categoria, parametro);

        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<ParametrosAtc> q = cb.createQuery(ParametrosAtc.class);
                    Root<ParametrosAtc> p = q.from(ParametrosAtc.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("id").get("tcbk"), 1));
                    predicatesAnd.add(cb.equal(p.get("id").get("tcapp"), 94));
                    predicatesAnd.add(cb.equal(p.get("id").get("tcrela"), categoria));
                    predicatesAnd.add(cb.equal(p.get("tct2c1"), parametro));

                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));

                    TypedQuery<ParametrosAtc> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                        return query.getResultList().get(0);
                    } else {
                        return null;
                    }
                }
        );

    }
}
