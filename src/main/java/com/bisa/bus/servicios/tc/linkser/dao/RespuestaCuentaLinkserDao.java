package com.bisa.bus.servicios.tc.linkser.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.linkser.entities.RespuestaSolicitudCuentaLinkser;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.io.Serializable;

/**
 * Created by atenorio on 14/11/2017.
 */
public class RespuestaCuentaLinkserDao extends DaoImpl<RespuestaSolicitudCuentaLinkser, String> implements Serializable{

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(RespuestaCuentaLinkserDao.class);

    @Inject
    protected RespuestaCuentaLinkserDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(RespuestaSolicitudCuentaLinkser cuenta, String user) {
        persist(cuenta);
        return true;
    }

    public boolean limpiarTodo() {
        LOGGER.debug("Limpiando tabla RespuestaSolicitudCuentaLinkser.");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<RespuestaSolicitudCuentaLinkser> query = entityManager.createQuery("DELETE FROM RespuestaSolicitudCuentaLinkser");//
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }
}