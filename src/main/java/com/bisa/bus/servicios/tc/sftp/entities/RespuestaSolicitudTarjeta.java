package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 25/05/2017.
 */
@Entity
@Table(name = "TCPINIAR")
public class RespuestaSolicitudTarjeta implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "SADATO", nullable = false)
    private String texto;

    public RespuestaSolicitudTarjeta(String texto){
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
