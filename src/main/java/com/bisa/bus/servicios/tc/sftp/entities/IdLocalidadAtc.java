package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Created by atenorio on 26/05/2017.
 */
@Embeddable
public class IdLocalidadAtc {
    @Basic(optional = false)
    @Column(name = "T15DPTO", nullable = false, length = 2)
    private String codDepatamento;
    @Basic(optional = false)
    @Column(name = "T15LOCA", nullable = false, length = 2)
    private String codLocalidad;

    public String getCodLocalidad() {
        return codLocalidad;
    }

    public void setCodLocalidad(String codLocalidad) {
        this.codLocalidad = codLocalidad;
    }

    public String getCodDepatamento() {
        return codDepatamento;
    }

    public void setCodDepatamento(String codDepatamento) {
        this.codDepatamento = codDepatamento;
    }
}
