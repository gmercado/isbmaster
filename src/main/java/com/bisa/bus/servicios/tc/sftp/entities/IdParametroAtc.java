package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by atenorio on 26/05/2017.
 */
@Embeddable
public class IdParametroAtc implements Serializable{
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "TCBK", nullable = false)
    private Short tcbk;
    @Basic(optional = false)
    @Column(name = "TCAPP", nullable = false)
    private short tcapp;
    @Basic(optional = false)
    @Column(name = "TCRELA", nullable = false, length = 12)
    private String tcrela;
    @Basic(optional = false)
    @Column(name = "TCT1C1", nullable = false, length = 9)
    private String tct1c1;
    @Basic(optional = false)
    @Column(name = "TCT1C2", nullable = false, length = 9)
    private String tct1c2;

    public Short getTcbk() {
        return tcbk;
    }

    public void setTcbk(Short tcbk) {
        this.tcbk = tcbk;
    }

    public short getTcapp() {
        return tcapp;
    }

    public void setTcapp(short tcapp) {
        this.tcapp = tcapp;
    }

    public String getTcrela() {
        return tcrela;
    }

    public void setTcrela(String tcrela) {
        this.tcrela = tcrela;
    }

    public String getTct1c1() {
        return tct1c1;
    }

    public void setTct1c1(String tct1c1) {
        this.tct1c1 = tct1c1;
    }

    public String getTct1c2() {
        return tct1c2;
    }

    public void setTct1c2(String tct1c2) {
        this.tct1c2 = tct1c2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdParametroAtc that = (IdParametroAtc) o;

        if (tcbk != that.tcbk) return false;
        if (tcapp != that.tcapp) return false;
        if (!tcrela.equals(that.tcrela)) return false;
        if (!tct1c1.equals(that.tct1c1)) return false;
        if (!tct1c2.equals(that.tct1c2)) return false;
        return true;

    }

    @Override
    public int hashCode() {
        int result = tcbk.hashCode();
        result = 31 * result + tcapp;
        result = 31 * result + tcrela.hashCode();
        result = 31 * result + tct1c1.hashCode();
        result = 31 * result + tct1c2.hashCode();
        return result;
    }
}
