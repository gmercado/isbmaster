package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by atenorio on 22/06/2017.
 * ATCUC80 Registros de acumulado de movimientos finnacieros agrupados por cuenta y tipo de movimiento
 */
public class MovimientoMensualCsv implements Serializable {
    private static final long serialVersionUID = 1L;

    
    @FormatCsv(name = "MMBAEM", nullable = false, length = 4)
    private String codigoBanco;
    
    @FormatCsv(name = "MMSUEM", nullable = false, length = 4)
    private String codigoSucursal;
    
    @FormatCsv(name = "MMNUTCNC", nullable = false, length = 10)
    private String numeroCuenta;
    
    @FormatCsv(name = "MMCAESCU", nullable = false, length = 4)
    private String categoriaEstadoCuenta;
    
    @FormatCsv(name = "MMCABA", nullable = false, length = 4)
    private String categoriaBalance;
    
    @FormatCsv(name = "MMCAFI", nullable = false, length = 4)
    private String categoriaFinanc;
    
    @FormatCsv(name = "MMCOOP", nullable = false)
    private String codigoOperacion;
    
    @FormatCsv(name = "MMIMMOLIN", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeBs;
    
    @FormatCsv(name = "MMIMMOUSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeUs;

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getCodigoSucursal() {
        return codigoSucursal;
    }

    public void setCodigoSucursal(String codigoSucursal) {
        this.codigoSucursal = codigoSucursal;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getCategoriaEstadoCuenta() {
        return categoriaEstadoCuenta;
    }

    public void setCategoriaEstadoCuenta(String categoriaEstadoCuenta) {
        this.categoriaEstadoCuenta = categoriaEstadoCuenta;
    }

    public String getCategoriaBalance() {
        return categoriaBalance;
    }

    public void setCategoriaBalance(String categoriaBalance) {
        this.categoriaBalance = categoriaBalance;
    }

    public String getCategoriaFinanc() {
        return categoriaFinanc;
    }

    public void setCategoriaFinanc(String categoriaFinanc) {
        this.categoriaFinanc = categoriaFinanc;
    }

    public String getCodigoOperacion() {
        return codigoOperacion;
    }

    public void setCodigoOperacion(String codigoOperacion) {
        this.codigoOperacion = codigoOperacion;
    }

    public BigDecimal getImporteBs() {
        return importeBs;
    }

    public void setImporteBs(BigDecimal importeBs) {
        this.importeBs = importeBs;
    }

    public BigDecimal getImporteUs() {
        return importeUs;
    }

    public void setImporteUs(BigDecimal importeUs) {
        this.importeUs = importeUs;
    }
}
