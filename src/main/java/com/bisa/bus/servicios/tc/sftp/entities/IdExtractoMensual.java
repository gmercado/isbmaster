package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by atenorio on 01/08/2017.
 */
@Embeddable
public class IdExtractoMensual implements Serializable {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "EECADM", nullable = false)
    private Character codigoAdm;

    @Basic(optional = false)
    @Column(name = "EEFEPRO1", nullable = false)
    private int fechaProceso;

    @Basic(optional = false)
    @Column(name = "EENUTCNC", nullable = false, length = 10)
    private String numeroCuenta;

    @Basic(optional = false)
    @Column(name = "EENUFA", nullable = false, length = 10)
    private String numeroFactura;

    @Basic(optional = false)
    @Column(name = "EEIDRE", nullable = false)
    private long idArchivoRecepcion;

    public Character getCodigoAdm() {
        return codigoAdm;
    }

    public void setCodigoAdm(Character codigoAdm) {
        this.codigoAdm = codigoAdm;
    }

    public int getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(int fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public long getIdArchivoRecepcion() {
        return idArchivoRecepcion;
    }

    public void setIdArchivoRecepcion(long idArchivoRecepcion) {
        this.idArchivoRecepcion = idArchivoRecepcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdExtractoMensual that = (IdExtractoMensual) o;

        if (getFechaProceso() != that.getFechaProceso()) return false;
        if (getIdArchivoRecepcion() != that.getIdArchivoRecepcion()) return false;
        if (!getCodigoAdm().equals(that.getCodigoAdm())) return false;
        if (!getNumeroCuenta().equals(that.getNumeroCuenta())) return false;
        return getNumeroFactura().equals(that.getNumeroFactura());
    }

    @Override
    public int hashCode() {
        int result = getCodigoAdm().hashCode();
        result = 31 * result + getFechaProceso();
        result = 31 * result + getNumeroCuenta().hashCode();
        result = 31 * result + getNumeroFactura().hashCode();
        result = 31 * result + (int) (getIdArchivoRecepcion() ^ (getIdArchivoRecepcion() >>> 32));
        return result;
    }
}
