package com.bisa.bus.servicios.tc.linkser.model;

import java.io.Serializable;

/**
 * Created by atenorio on 12/01/2017.
 */
public class ConsultaMovimientoRespuesta implements Serializable {

    private static final long serialVersionUID = -6494683952317493467L;

    private String codRespuesta;
    private String dscRespuesta;



    public String getCodRespuesta() {
        return codRespuesta;
    }

    public void setCodRespuesta(String codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    public String getDscRespuesta() {
        return dscRespuesta;
    }

    public void setDscRespuesta(String dscRespuesta) {
        this.dscRespuesta = dscRespuesta;
    }


}
