package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 26/05/2017.
 */
@Entity
@Table(name = "TCPANPAR")
public class ParametrosAtc implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private IdParametroAtc id;

    @Basic(optional = false)
    @Column(name = "TCT1DESC", nullable = false, length = 40)
    private String tct1desc;
    @Basic(optional = false)
    @Column(name = "TCT1C3", nullable = false, length = 9)
    private String tct1c3;
    @Basic(optional = false)
    @Column(name = "TCT2C1", nullable = false, length = 9)
    private String tct2c1;
    @Basic(optional = false)
    @Column(name = "TCT2C2", nullable = false, length = 9)
    private String tct2c2;
    @Basic(optional = false)
    @Column(name = "TCT2DESC", nullable = false, length = 40)
    private String tct2desc;
    @Basic(optional = false)
    @Column(name = "TCT2C3", nullable = false, length = 9)
    private String tct2c3;
    @Basic(optional = false)
    @Column(name = "REUSR", nullable = false, length = 10)
    private String reusr;
    @Basic(optional = false)
    @Column(name = "REDEV", nullable = false, length = 10)
    private String redev;
    @Basic(optional = false)
    @Column(name = "REPGM", nullable = false, length = 10)
    private String repgm;
    @Basic(optional = false)
    @Column(name = "REFEC", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date refec;

    public ParametrosAtc() {
    }


    public String getTct1desc() {
        return tct1desc;
    }

    public void setTct1desc(String tct1desc) {
        this.tct1desc = tct1desc;
    }

    public String getTct1c3() {
        return tct1c3;
    }

    public void setTct1c3(String tct1c3) {
        this.tct1c3 = tct1c3;
    }

    public String getTct2c1() {
        return tct2c1;
    }

    public void setTct2c1(String tct2c1) {
        this.tct2c1 = tct2c1;
    }

    public String getTct2c2() {
        return tct2c2;
    }

    public void setTct2c2(String tct2c2) {
        this.tct2c2 = tct2c2;
    }

    public String getTct2desc() {
        return tct2desc;
    }

    public void setTct2desc(String tct2desc) {
        this.tct2desc = tct2desc;
    }

    public String getTct2c3() {
        return tct2c3;
    }

    public void setTct2c3(String tct2c3) {
        this.tct2c3 = tct2c3;
    }

    public String getReusr() {
        return reusr;
    }

    public void setReusr(String reusr) {
        this.reusr = reusr;
    }

    public String getRedev() {
        return redev;
    }

    public void setRedev(String redev) {
        this.redev = redev;
    }

    public String getRepgm() {
        return repgm;
    }

    public void setRepgm(String repgm) {
        this.repgm = repgm;
    }

    public Date getRefec() {
        return refec;
    }

    public void setRefec(Date refec) {
        this.refec = refec;
    }

    @Override
    public String toString() {
        return "ParametrosAtc[ Tcbk=" + id + " ]";
    }

    public IdParametroAtc getId() {
        return id;
    }

    public void setId(IdParametroAtc id) {
        this.id = id;
    }
}
