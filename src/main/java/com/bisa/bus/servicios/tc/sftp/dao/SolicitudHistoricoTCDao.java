package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.SolicitudHistoricoTC;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 * Created by atenorio on 22/05/2017.
 */
public class SolicitudHistoricoTCDao extends DaoImpl<SolicitudHistoricoTC, Integer> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SolicitudHistoricoTCDao.class);
    @Inject
    protected SolicitudHistoricoTCDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(SolicitudHistoricoTC solicitud, String user) {
        // Siempre almacenar nuevo registro de historico
        persist(solicitud);
        return true;
    }
}
