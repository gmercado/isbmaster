package com.bisa.bus.servicios.tc.sftp.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.interfaces.as400.dao.TipoAtt;
import bus.interfaces.as400.dao.ValorAttDao;
import bus.interfaces.as400.entities.ValorAtt;
import bus.plumbing.csv.LecturaCSV;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.tc.sftp.dao.FechaCorteDao;
import com.bisa.bus.servicios.tc.sftp.dao.SaldoMensualDao;
import com.bisa.bus.servicios.tc.sftp.dao.SaldoMensualHistoricoDao;
import com.bisa.bus.servicios.tc.sftp.entities.*;
import com.bisa.bus.servicios.tc.sftp.model.EstadoPagosTC;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.SaldoMensualCsv;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgProgramaTarjetaCredito;
import com.bisa.bus.servicios.tc.sftp.utils.FormatoNumericoTexto;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by atenorio on 23/06/2017.
 */
public class SftpRecepcionSaldoMensual {

    private static final long serialVersionUID = -7112978535165279100L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpRecepcionSaldoMensual.class);

    private final MedioAmbiente medioAmbiente;
    private final FechaCorteDao fechaCorteDao;
    private final SaldoMensualDao saldoMensualDao;
    private final SaldoMensualHistoricoDao saldoMensualHistoricoDao;
    private final ValorAttDao valorAttDao;

    private final RpgProgramaTarjetaCredito rpgPrograma;
    private final NotificacionesCorreo notificacionesCorreo;
    SftpConfiguracion configuracion;
    SftpTransferencia sftpTransferencia;
    private String nombreArchivoProcesado = "";
    private String CODIGO_ADMIN_ATC = "A";
    private int fechaProceso = 0;
    private StringBuffer mensajeDeValidacion = new StringBuffer();
    private boolean valido = true;
    String VARIABLE = "#variable";

    @Inject
    public SftpRecepcionSaldoMensual(MedioAmbiente medioAmbiente,
                                     SaldoMensualDao saldoMensualDao, SaldoMensualHistoricoDao saldoMensualHistoricoDao,
                                     NotificacionesCorreo notificacionesCorreo, RpgProgramaTarjetaCredito rpgPrograma,
                                     SftpConfiguracion configuracion, SftpTransferencia sftpTransferencia,
                                     ValorAttDao valorAttDao, FechaCorteDao fechaCorteDao) {
        this.rpgPrograma = rpgPrograma;
        this.medioAmbiente = medioAmbiente;
        this.saldoMensualDao = saldoMensualDao;
        this.saldoMensualHistoricoDao = saldoMensualHistoricoDao;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
        this.notificacionesCorreo = notificacionesCorreo;
        this.valorAttDao = valorAttDao;
        this.fechaCorteDao = fechaCorteDao;
    }

    /**
     * Metodo para procesar archivo de saldo mensual enviado por ATC en SFTP remoto.
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    public boolean procesarArchivo() {
        String detalle = "";
        boolean ok = true;
        LOGGER.debug("Inicia proceso de descarga de archivos mensuales.");
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_SALDO_MENSUAL_079, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_SALDO_MENSUAL_079_DEFAULT);
        LOGGER.info("Descargar archivo con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        LOGGER.debug("1) Descarga de archivo de a servidor SFTP");
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_SALDO_MENSUAL_ATC, Variables.FTP_RUTA_RECEPCION_SALDO_MENSUAL_ATC_DEFAULT));
        sftpTransferencia.configurar(configuracion);
        String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_ATC, Variables.FECHA_DE_PROCESO_ATC_DEFAULT);
        // Calcular fecha de un anterior
        Calendar hoy = Calendar.getInstance();
        //hoy.add(Calendar.DAY_OF_YEAR, -1);
        Date fechaProcesar = hoy.getTime();

        if (StringUtils.isEmpty(fechaProceso) || "0".equals(fechaProceso)) {
            fechaProceso = FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar);
        } else {
            fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
        }
        String nombreArchivo = "SALDOMENSUAL" + fechaProceso;
        nombreArchivoProcesado = nombreArchivo;
        List<File> archivos = sftpTransferencia.descargarListaArchivos(nombreArchivoRegex, nombreArchivo, TipoArchivo.CIE);
        ok = (archivos == null || archivos.size() == 0) ? false : true;

        LOGGER.info("Descarga completa:" + ok);

        if (!ok) {
            // Cerrar el registro con error y permitir reintento
            detalle = "Problema en transferencia o no existen archivos de respuesta en servidor SFTP.";
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.CIE, detalle);
            LOGGER.warn(detalle);
            return false;
        }

        // Procesamiento de archivo local
        LOGGER.debug("2) Proceso de mapeo de archivos de respuesta");
        List<SaldoMensualCsv> lista = new ArrayList<SaldoMensualCsv>();
        for (File archivo : archivos) {
            List<SaldoMensualCsv> listaMapeo = mapearArchivo(archivo);
            if (listaMapeo == null || listaMapeo.size() == 0) {
                detalle = "Error en mapeo de archivo de respuesta o archivo vac\u00EDo.";
                LOGGER.warn(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.CIE, detalle);
                return false;
            }
            lista.addAll(listaMapeo);
        }
        LOGGER.debug("4) Actualizar tabla de datos generales");
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();
        LOGGER.debug("4.1) Limpiesa de tabla de saldo mensual.");
        // Limpiar tablas temporales
        if (!saldoMensualDao.limpiarTodo()) {
            detalle = "Error al limpiar tabla temporal de datos generales.";
            LOGGER.error(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.CIE, detalle);
            return false;
        }

        int fila = 0;
        StringBuffer validacionResultado = new StringBuffer();
        // Verificar que los registros no tengan fallas en parseo
        for (SaldoMensualCsv dg : lista) {
            fila++;
            // Validar que se realizo el mapeo de forma exitosa de la estructura de pago
            if (dg == null) {
                detalle = "Error en mapeo de una estructura se requiere revisar el archivo de respuesta en fila " + fila + ".";
                LOGGER.error(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.CIE, detalle);
                return false;
            }
            // Realizar validaciones no restringen carga de archivo pero si almacenamiento
            String permitirValidar = medioAmbiente.getValorDe(Variables.FTP_VALIDAR_RECEPCION_ARCHIVO_ATC, Variables.FTP_VALIDAR_RECEPCION_ARCHIVO_ATC_DEFAULT);
            if ("true".equals(StringUtils.trimToEmpty(permitirValidar))) {
                detalle = validacion(dg, fila);
                if (StringUtils.isNotEmpty(detalle)) {
                    validacionResultado.append(detalle);
                    validacionResultado.append("\n");
                    LOGGER.error(detalle);
                    LOGGER.warn("Error en validacion registro: {}", dg);
                    sftpTransferencia.transferenciaConErrorValidacion(detalle);
                    notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.CIE, detalle);
                    return false;
                }
            }
        }
        int periodo = 0;
        String periodoCadena = FormatosUtils.periodoFormateado(fechaProcesar);
        if (periodoCadena != null) {
            periodo = new Integer(periodoCadena);
        }
        FechaCorte fechaCierre = fechaCorteDao.find(new IdFechaCorte(CODIGO_ADMIN_ATC, periodo));
        String mensaje = "";
        if (fechaCierre == null) {
            mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_VALIDAR_TABLA_FECHA_CORTE_ATC, Variables.FTP_MENSAJE_EMAIL_VALIDAR_TABLA_FECHA_CORTE_ATC_DEFAULT);
            mensaje = mensaje.replaceAll(VARIABLE, periodoCadena);
            mensajeDeValidacion.append("\n");
            mensajeDeValidacion.append(mensaje);
            valido = false;
        } else {
            mensajeDeValidacion.append("\nTCPFEVEN Fecha de cierre: ");
            mensajeDeValidacion.append(fechaCierre.getFechaCierre());
            mensajeDeValidacion.append(" ");
            mensajeDeValidacion.append("Fecha de cierre Banco: ");
            mensajeDeValidacion.append(fechaCierre.getFechaCierreBanco());
            mensajeDeValidacion.append(" ");
            mensajeDeValidacion.append("Fecha de vencimiento de pago: ");
            mensajeDeValidacion.append(fechaCierre.getFechaVencimiento());
            mensajeDeValidacion.append(".");
        }

        int totalRegistros = 0;
        // Verificar consistencia de registro
        for (SaldoMensualCsv saldo : lista) {

            totalRegistros++;
            IdSaldoMensual id = new IdSaldoMensual();
            id.setCodigoSucursal(saldo.getCodigoSucursal());
            id.setEstadoCuenta(saldo.getEstadoCuenta());
            id.setNumeroCuenta(saldo.getNumeroCuenta());
            // Validar que se tenga pendiente de respuesta a pagos
            SaldoMensual saldoMensual = transferObject.convert(saldo, SaldoMensual.class);
            saldoMensual.setEstado(EstadoPagosTC.GENE.name());
            saldoMensual.setCodigoAdm('A');
            this.fechaProceso = FormatosUtils.fechaYYYYMMDD(fechaProcesar).intValue();
            saldoMensual.setFechaProcesoArchivo(this.fechaProceso);
            saldoMensual.setPagoMinimoAnteriorSigno(FormatoNumericoTexto.getSigno(saldoMensual.getPagoMinimoAnterior()));
            saldoMensual.setPagoMinimoAnteriorTexto(FormatoNumericoTexto.getNumero(saldoMensual.getPagoMinimoAnterior(), 10));
            saldoMensual.setSaldoAnteriorBsSigno(FormatoNumericoTexto.getSigno(saldoMensual.getSaldoAnteriorBs()));
            saldoMensual.setSaldoAnteriorBsTexto(FormatoNumericoTexto.getNumero(saldoMensual.getSaldoAnteriorBs(), 10));
            saldoMensual.setSaldoAnteriorUsSigno(FormatoNumericoTexto.getSigno(saldoMensual.getSaldoAnteriorUs()));
            saldoMensual.setSaldoAnteriorUsTexto(FormatoNumericoTexto.getNumero(saldoMensual.getSaldoAnteriorUs(), 10));
            saldoMensual.setPagoSigno(FormatoNumericoTexto.getSigno(saldoMensual.getPago()));
            saldoMensual.setPagoTexto(FormatoNumericoTexto.getNumero(saldoMensual.getPago(), 10));
            saldoMensual.setCargosBsSigno(FormatoNumericoTexto.getSigno(saldoMensual.getCargosBs()));
            saldoMensual.setCargosBstexto(FormatoNumericoTexto.getNumero(saldoMensual.getCargosBs(), 10));
            saldoMensual.setCargosUsSigno(FormatoNumericoTexto.getSigno(saldoMensual.getCargosUs()));
            saldoMensual.setCargosUsTexto(FormatoNumericoTexto.getNumero(saldoMensual.getCargosUs(), 10));
            saldoMensual.setIneteresSigno(FormatoNumericoTexto.getSigno(saldoMensual.getIneteres()));
            saldoMensual.setIneteresTexto(FormatoNumericoTexto.getNumero(saldoMensual.getIneteres(), 10));
            saldoMensual.setSaldoActualBsSigno(FormatoNumericoTexto.getSigno(saldoMensual.getSaldoActualBs()));
            saldoMensual.setSaldoActualBsTexto(FormatoNumericoTexto.getNumero(saldoMensual.getSaldoActualBs(), 10));
            saldoMensual.setSaldoActualUsSigno(FormatoNumericoTexto.getSigno(saldoMensual.getSaldoActualUs()));
            saldoMensual.setSaldoActualUstexto(FormatoNumericoTexto.getNumero(saldoMensual.getSaldoActualUs(), 10));
            saldoMensual.setPagoMinimoActualSigno(FormatoNumericoTexto.getSigno(saldoMensual.getPagoMinimoActual()));
            saldoMensual.setPagoMinimoActualTexto(FormatoNumericoTexto.getNumero(saldoMensual.getPagoMinimoActual(), 10));
            saldoMensual.setLimiteCompraSigno(FormatoNumericoTexto.getSigno(saldoMensual.getLimiteCompra()));
            saldoMensual.setLimiteCompraTexto(FormatoNumericoTexto.getNumero(saldoMensual.getLimiteCompra(), 10));
            saldoMensual.setLimiteFinanciamientoSigno(FormatoNumericoTexto.getSigno(saldoMensual.getLimiteFinanciamiento()));
            saldoMensual.setLimiteFinanciamientoTexto(FormatoNumericoTexto.getNumero(saldoMensual.getLimiteFinanciamiento(), 10));
            if (StringUtils.isNotEmpty(saldoMensual.getTipoCambio())) {
                saldoMensual.setTipoCambio(StringUtils.leftPad(saldoMensual.getTipoCambio(), 6, "0"));
            }

            saldoMensual.setFechaRecepcion(new Date());
            saldoMensual.setIdArchivoRecepcion(sftpTransferencia.getFtpArchivoTransferencia().getId());

            saldoMensual.setFechaCreacion(new Date());
            saldoMensual.setUsuarioCreador("ISB");
            saldoMensual.setFechaModificacion(new Date());
            saldoMensual.setUsuarioModificador("");

            // Registrar pago
            saldoMensual.setIdSaldoMensual(id);
            ok = ok && saldoMensualDao.guardar(saldoMensual, "ISB");

            SaldoMensualHistorico historico = transferObject.convert(saldoMensual, SaldoMensualHistorico.class);
            historico.setIdSaldoMensual(saldoMensual.getIdSaldoMensual());
            ok = ok && saldoMensualHistoricoDao.guardar(historico, "ISB");

            if (fechaCierre != null && fechaCierre.isFechaCierreValido() && !fechaCierre.validarFechaCierre(saldoMensual.getFechaProceso())) {
                mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_VALIDAR_FECHA_CORTE_ATC, Variables.FTP_MENSAJE_EMAIL_VALIDAR_FECHA_CORTE_ATC_DEFAULT);
                mensaje = mensaje.replaceAll(VARIABLE, saldo.getFechaProceso());
                mensajeDeValidacion.append("\n");
                mensajeDeValidacion.append(mensaje);
                valido = false;
            }
            if (fechaCierre != null && fechaCierre.isFechaVencimientoPagoValido() && !fechaCierre.validarFechaVencimientoPago(saldoMensual.getFechaVencimiento())) {
                mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_VALIDAR_FECHA_VENCIMIENTO_ATC, Variables.FTP_MENSAJE_EMAIL_VALIDAR_FECHA_VENCIMIENTO_ATC_DEFAULT);
                mensaje = mensaje.replaceAll(VARIABLE, saldo.getFechaVencimiento());
                mensajeDeValidacion.append("\n");
                mensajeDeValidacion.append(mensaje);
                valido = false;
            }
        }
        if(fechaCierre != null && fechaCierre.isFechaVencimientoPagoValido() && fechaCierre.isFechaCierreValido()){
            mensajeDeValidacion.append("\nLas fechas de cierre y vencimiento de pago corresponden al periodo.\n");
        }
        // Verificar que cuentas de archivo de saldos esten presentes en tablas del Banco
        List<SaldoMensual> sincuenta = saldoMensualDao.getListaSinCuentaBanco();
        if(sincuenta.size()>0){
            StringBuffer cuentas = new StringBuffer();
            for(SaldoMensual saldo: sincuenta){
                cuentas.append(saldo.getIdSaldoMensual().getNumeroCuenta());
                cuentas.append(" ");
            }
            mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_VALIDAR_ALTAS_ATC, Variables.FTP_MENSAJE_EMAIL_VALIDAR_ALTAS_ATC_DEFAULT);
            mensaje = mensaje.replaceAll(VARIABLE, cuentas.toString());
            mensajeDeValidacion.append("\n");
            mensajeDeValidacion.append(mensaje);
            valido = false;
        }

        // validar consistencia de carga, verificar que se cargaron todos los archivos por sucursal
        detalle = "Problemas en registro de base de datos.";
        if (ok) {
            nombreArchivoProcesado = StringUtils.trimToEmpty(sftpTransferencia.getNombreArchivoRemoto());
            detalle = "Recepci\u00F3n de Saldos de cierre finalizada correctamente con " + totalRegistros
                    + " registros en archivo(s) " + getNombreArchivoProcesado() + ".";
            sftpTransferencia.transferenciaCompleta(StringUtils.substring(detalle, 0, 254));
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivo, TipoArchivo.CIE, detalle);
        } else {
            sftpTransferencia.transferenciaConErrorInterno(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.CIE, detalle);
        }

        LOGGER.debug("Finalizado proceso de carga de saldo mensual.");
        return ok;
    }

    public List<SaldoMensualCsv> mapearArchivo(File archivo) {
        LOGGER.debug("Nombre de archivo local a procesar:{}", archivo.getAbsolutePath());
        FileInputStream is = null;
        BufferedReader br = null;
        boolean respuesta;
        List<SaldoMensualCsv> lista = new ArrayList<SaldoMensualCsv>();
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();
        try {
            is = new FileInputStream(archivo);
            String codificacion = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC, Variables.FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC_DEFAULT);
            br = new BufferedReader(new InputStreamReader(is, codificacion));
            String linea = br.readLine();
            List<String> lineas = new ArrayList<String>();
            int conteo = 0;
            while (linea != null) {
                conteo++;
                LOGGER.debug("Linea {}: {}", conteo, linea);
                if (StringUtils.isNotEmpty(linea) && linea.length() > 1) {
                    SaldoMensualCsv dg = LecturaCSV.procesar(SaldoMensualCsv.class, linea, "|");
                    lista.add(dg);
                }
                linea = br.readLine();
            }
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al procesar el archivo "
                    + archivo.getName() + ".", e);
            return null;
        } finally {
            IOUtils.closeQuietly(is);
        }
        return lista;
    }

    public String getNombreArchivoProcesado() {
        return sftpTransferencia.getNombreArchivoRemotoFecha();
    }

    private String validacion(SaldoMensualCsv extracto, int fila) {
        // Validar correcto codigo de emisor
        if (StringUtils.isNotEmpty(extracto.getCodigoBanco())) {
            List<ValorAtt> lista = valorAttDao.getListaDe(TipoAtt.TARJETA_CREDITO_EMISOR);
            if (lista != null && lista.size() > 0 &&
                    !extracto.getCodigoBanco().equals(StringUtils.trimToEmpty(lista.get(0).getValor()))) {
                return "Error de validaci\u00F3n con c\u00F3digo de Banco incorrecto en fila " + fila + ".";
            }
        }
        // Validar fechas limites
        return "";
    }

    static String FECHA_VENCIMIENTO = "";

    private HashMap<String, Integer> validacionFechas(SaldoMensualCsv extracto, FechaCorte fechaCorte, int fila) {
        extracto.getFechaPago();
        extracto.getFechaProceso();
        extracto.getFechaProceso();
        // Validar correcto codigo de emisor
        if (StringUtils.isNotEmpty(extracto.getCodigoBanco())) {
            List<ValorAtt> lista = valorAttDao.getListaDe(TipoAtt.TARJETA_CREDITO_EMISOR);
            if (lista != null && lista.size() > 0 &&
                    !extracto.getCodigoBanco().equals(StringUtils.trimToEmpty(lista.get(0).getValor()))) {
                return null; // "Error de validaci\u00F3n con c\u00F3digo de Banco incorrecto en fila " + fila + ".";
            }
        }
        // Validar fechas limites
        return null;
    }

    public int getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(int fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public StringBuffer getMensajeDeValidacion() {
        return mensajeDeValidacion;
    }

    public boolean isValido() {
        return valido;
    }
}
