package com.bisa.bus.servicios.tc.linkser.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.file.ArchivoBase;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.tc.linkser.dao.RespuestaRenovacionTarjetaLinkserDao;
import com.bisa.bus.servicios.tc.linkser.entities.RespuestaRenovacionTarjetaLinkser;
import com.bisa.bus.servicios.tc.sftp.api.SftpTransferencia;
import com.bisa.bus.servicios.tc.sftp.entities.FtpArchivoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SftpRecepcionBajasTarjetaLinkser implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpRecepcionBajasTarjetaLinkser.class);

    private final MedioAmbiente medioAmbiente;
    private final ArchivoBase archivoBase;
    private final NotificacionesCorreo notificacionesCorreo;
    private final RespuestaRenovacionTarjetaLinkserDao iRespuestaTarjetaDao;

    ISftpConfiguracionLinkser configuracion;
    SftpTransferencia sftpTransferencia;
    private String nombreArchivoProcesado = "";
    private int totalRegistros = 0;

    @Inject
    public SftpRecepcionBajasTarjetaLinkser(ArchivoBase archivoBase,
                                                 MedioAmbiente medioAmbiente,
                                                 NotificacionesCorreo notificacionesCorreo,
                                                 RespuestaRenovacionTarjetaLinkserDao iRespuestaTarjetaDao,
                                                 ISftpConfiguracionLinkser configuracion, SftpTransferencia sftpTransferencia) {
        this.medioAmbiente = medioAmbiente;
        this.archivoBase = archivoBase;
        this.notificacionesCorreo = notificacionesCorreo;
        this.iRespuestaTarjetaDao = iRespuestaTarjetaDao;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para procesar recepcion de archivo de bajas de tarjetas de servidor remoto SFTP.
     * SDOI.yyMMdd
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    String PREF = "#fecha";

    public boolean procesarArchivo() {
        return procesarArchivo(true);
    }
    public boolean procesarArchivo(boolean alta) {
        boolean ok = false;
        String detalle = "";
        LOGGER.debug("Inicia proceso recepcion de archivo de renovaciones");
        LOGGER.debug("1) Verificar archivo pendiente de respuesta");
        String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_LINKSER, Variables.FECHA_DE_PROCESO_LINKSER_DEFAULT);
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_BAJA_TARJETA_LINKSER, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_BAJA_TARJETA_LINKSER_DEFAULT);
        String nombreArchivoRegexForzado = "";  // No se tiene archivo forzado
        String nombreArchivo[] = Iterables.toArray(Splitter.on("|").trimResults().split(nombreArchivoRegex), String.class);

        // El proceso normal realiza la recepcion de archivo de alta de un dia anterior,
        // debido a la disponibilidad del archivo de respuesta.
        Calendar hoy = Calendar.getInstance();
        hoy.add(Calendar.DAY_OF_YEAR, -1);
        Date fechaProcesar = hoy.getTime();

        if (StringUtils.isEmpty(nombreArchivoRegexForzado) || "NULL".equals(nombreArchivoRegexForzado)) {
            // Procesar a fecha determinada por parametro
            if(alta) {
                FtpArchivoTransferencia archivoPendiente = sftpTransferencia.getFtpArchivoTransferenciaPendiente(TipoArchivo.SLI);
                // Registrando que no existe respuesta pendiente
                if (archivoPendiente == null) {
                    LOGGER.warn("No existen archivos pendientes de respuesta.");
                    return false;
                }
                fechaProceso = archivoPendiente.getFechaProceso().toString();
            }
            LOGGER.info("PROCESANDO ::::{}:::::", fechaProceso);
            if (StringUtils.isNotEmpty(fechaProceso) && !"0".equals(fechaProceso)) {
                fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
                if (fechaProcesar == null) {
                    notificacionesCorreo.notificarError("Error en fecha de proceso bajas", "");
                    return false;
                }
            }
            nombreArchivoRegex = nombreArchivo[0].replace(PREF, new SimpleDateFormat(nombreArchivo[1]).format(fechaProcesar));
        } else {
            nombreArchivoRegex = nombreArchivoRegexForzado;
        }
        nombreArchivoProcesado = nombreArchivoRegex;
        LOGGER.debug("2) Decargar archivo de servidor SFTP");        ;
        prepararConfiguracionSftp();
        LOGGER.info("2.1) Descarga de archivo de a servidor SFTP con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.RLI);

        // Cerrar el registro con error y permitir reintento
        if (!ok || sftpTransferencia.getArchivoLocal() == null) {
            sftpTransferencia.transferenciaConErrorReintento("");
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RLI, "No existen archivos de respuesta en servidor SFTP.");
            LOGGER.warn("No existen archivos de respuesta en servidor SFTP.");
            return false;
        }

        // Procesamiento de archivo local
        File archivo = sftpTransferencia.getArchivoLocal();
        List<String> mapeo = mapearArchivo(archivo);
        // Verificar si existen problemas en mape de archivo de respuesta
        if (mapeo == null || mapeo.size() == 0) {
            LOGGER.warn("Archivo de respuesta sin datos");
            sftpTransferencia.transferenciaCompleta("Archivo sin datos");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.RLI, "Archivo de respuesta sin datos");
            return true;
        }

        LOGGER.debug("4) Actualizar tabla de solicitudes");

        for (String renovacion : mapeo) {
            // Validar que se realizo el mapeo de forma exitosa en todas las estructuras
            if (renovacion == null) {
                LOGGER.error("Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                sftpTransferencia.transferenciaConErrorReintento("");
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RLI, "Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                return false;
            }
        }

        // No se realiza limpiesa de datos debido a que exije limpiesa en la carga inicial

        LOGGER.debug("BAJA");
        LOGGER.debug("====================================");

        for (String renovacion : mapeo) {
            // Grabar
            if (ok) {
                LOGGER.debug("{}", renovacion);
                iRespuestaTarjetaDao.guardar(new RespuestaRenovacionTarjetaLinkser(renovacion), "ISB");
            }
            totalRegistros++;
        }

        LOGGER.debug("4.2) Verificar si un registro se encuentra en un estado inconsistente");

        // Modificar estado de solicitudes de alta y notificar por correo
        if (ok) {
            sftpTransferencia.transferenciaCompleta("");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.RLI, "Transferencia completa con respuesta a " + totalRegistros + " solicitudes.");
            LOGGER.info("Proceso finalizado de recepcion de solicitud.");
        } else {
            detalle = "Error en proceso de recepcion de solicitudes.";
            LOGGER.warn(detalle);
            sftpTransferencia.transferenciaConErrorInterno(detalle);
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.RLI, detalle);
        }
        return ok;
    }

    public boolean prepararConfiguracionSftp() {
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_RENOVACIONES_LINKSER, Variables.FTP_RUTA_RECEPCION_RENOVACIONES_LINKSER_DEFAULT));
        return sftpTransferencia.configurar(configuracion);
    }

    public List<String> mapearArchivo(File archivo) {
        List<String> respuesta = new ArrayList<String>();
        FileInputStream is = null;
        BufferedReader br = null;
        try {
            is = new FileInputStream(archivo);
            br = new BufferedReader(new InputStreamReader(is));
            String linea = br.readLine();
            while (linea != null) {
                if (StringUtils.isNotEmpty(linea) && linea.length() > 10) {
                    respuesta.add(linea);
                }
                linea = br.readLine();
            }
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al procesar el archivo "
                    + archivo.getName() + ".", e);
            return new ArrayList<String>();
        } finally {
            IOUtils.closeQuietly(is);
        }
        return respuesta;
    }

    public String getNombreArchivoProcesado() {
        return (StringUtils.isNotEmpty(sftpTransferencia.getNombreArchivoRemoto())?sftpTransferencia.getNombreArchivoRemoto():nombreArchivoProcesado);
    }

    public String eliminarArchivo(){
        return sftpTransferencia.eliminarArchivo(sftpTransferencia.getNombreArchivoRemoto());
    }

    public int getTotalRegistros() {
        return totalRegistros;
    }
}
