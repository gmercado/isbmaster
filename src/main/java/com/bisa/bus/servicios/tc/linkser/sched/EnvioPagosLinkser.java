package com.bisa.bus.servicios.tc.linkser.sched;

import bus.env.api.MedioAmbiente;
import com.bisa.bus.servicios.tc.linkser.api.SftpEnvioPagosLinkser;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 09/11/2017.
 */
public class EnvioPagosLinkser implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(EnvioPagosLinkser.class);

    private final MedioAmbiente medioAmbiente;
    private final SftpEnvioPagosLinkser sftpEnvioPagosLinkser;

    @Inject
    public EnvioPagosLinkser(MedioAmbiente medioAmbiente, SftpEnvioPagosLinkser sftpEnvioPagosLinkser){
        this.medioAmbiente = medioAmbiente;
        this.sftpEnvioPagosLinkser = sftpEnvioPagosLinkser;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Inicio de proceso envio de pagos");
        boolean ok = sftpEnvioPagosLinkser.procesarArchivo();
        LOGGER.info("Respuesta proceso envio de pagos: {}", ok);
    }


}