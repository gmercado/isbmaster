package com.bisa.bus.servicios.tc.linkser.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 21/11/2017.
 */
@Entity
@Table(name = "TCPESTXT")
public class PagoEstablecimientoBs implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "ESTABL", nullable = false, length = 150)
    private String texto;

    public PagoEstablecimientoBs(String texto){
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}