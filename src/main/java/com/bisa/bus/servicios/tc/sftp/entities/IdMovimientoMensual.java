package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by atenorio on 26/06/2017.
 */
@Embeddable
public class IdMovimientoMensual implements Serializable {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "MMNUTCNC", nullable = false, length = 10)
    private String numeroCuenta;

    @Basic(optional = false)
    @Column(name = "MMCAESCU", nullable = false, length = 4)
    private String categoriaEstadoCuenta;

    @Basic(optional = false)
    @Column(name = "MMCABA", nullable = false, length = 4)
    private String categoriaBalance;
    @Basic(optional = false)
    @Column(name = "MMCAFI", nullable = false, length = 4)
    private String categoriaFinanc;

    @Basic(optional = false)
    @Column(name = "MMFEPRO1", nullable = false)
    private int fechaProceso;

    @Basic(optional = false)
    @Column(name = "MMCADM", nullable = false)
    private Character mmcadm;

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getCategoriaEstadoCuenta() {
        return categoriaEstadoCuenta;
    }

    public void setCategoriaEstadoCuenta(String categoriaEstadoCuenta) {
        this.categoriaEstadoCuenta = categoriaEstadoCuenta;
    }

    public String getCategoriaBalance() {
        return categoriaBalance;
    }

    public void setCategoriaBalance(String categoriaBalance) {
        this.categoriaBalance = categoriaBalance;
    }

    public String getCategoriaFinanc() {
        return categoriaFinanc;
    }

    public void setCategoriaFinanc(String categoriaFinanc) {
        this.categoriaFinanc = categoriaFinanc;
    }

    public Character getMmcadm() {
        return mmcadm;
    }

    public void setMmcadm(Character mmcadm) {
        this.mmcadm = mmcadm;
    }

    public int getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(int fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdMovimientoMensual that = (IdMovimientoMensual) o;

        if (getFechaProceso() != that.getFechaProceso()) return false;
        if (!getNumeroCuenta().equals(that.getNumeroCuenta())) return false;
        if (!getCategoriaEstadoCuenta().equals(that.getCategoriaEstadoCuenta())) return false;
        if (!getCategoriaBalance().equals(that.getCategoriaBalance())) return false;
        if (!getCategoriaFinanc().equals(that.getCategoriaFinanc())) return false;
        return getMmcadm().equals(that.getMmcadm());
    }

    @Override
    public int hashCode() {
        int result = getNumeroCuenta().hashCode();
        result = 31 * result + getCategoriaEstadoCuenta().hashCode();
        result = 31 * result + getCategoriaBalance().hashCode();
        result = 31 * result + getCategoriaFinanc().hashCode();
        result = 31 * result + getFechaProceso();
        result = 31 * result + getMmcadm().hashCode();
        return result;
    }
}
