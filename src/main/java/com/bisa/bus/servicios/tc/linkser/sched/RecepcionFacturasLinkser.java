package com.bisa.bus.servicios.tc.linkser.sched;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.bus.servicios.tc.linkser.api.SftpFacturaLinkser;
import com.bisa.bus.servicios.tc.linkser.api.SftpFacturaCuentaLinkser;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 16/11/2017.
 */
public class RecepcionFacturasLinkser implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecepcionFacturasLinkser.class);

    private final MedioAmbiente medioAmbiente;
    private final NotificacionesCorreo notificacionesCorreo;
    private final SftpFacturaLinkser iSftpFacturaLinkser;
    private final SftpFacturaCuentaLinkser iSftpFacturaCuentaLinkser;

    private volatile boolean ok;

    @Inject
    public RecepcionFacturasLinkser(MedioAmbiente medioAmbiente, NotificacionesCorreo notificacionesCorreo,
                                    SftpFacturaLinkser iSftpFacturaLinkser,
                                    SftpFacturaCuentaLinkser iSftpFacturaCuentaLinkser){
        this.medioAmbiente = medioAmbiente;
        this.notificacionesCorreo = notificacionesCorreo;
        this.iSftpFacturaLinkser = iSftpFacturaLinkser;
        this.iSftpFacturaCuentaLinkser = iSftpFacturaCuentaLinkser;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        // Procesando facturas
        LOGGER.info("Inicio de proceso recepcion facturas");
        ok = iSftpFacturaLinkser.procesarArchivo();
        LOGGER.info("Respuesta proceso de recepcion archivo de facturas: {}", ok);
        // Procesando facturas cuenta
        if(ok) {
            LOGGER.info("Inicio de proceso recepcion facturas cuenta");
            ok = iSftpFacturaCuentaLinkser.procesarArchivo();
            LOGGER.info("Respuesta proceso de recepcion archivo de facturas cuenta: {}", ok);
        }
        // Completar proceso de carga notificando al operador
        if(ok){
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_FACTURACION_OPERADOR_LINKSER, Variables.FTP_MENSAJE_EMAIL_FACTURACION_OPERADOR_LINKSER_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador(iSftpFacturaLinkser.getNombreArchivoProcesado() + ", " + iSftpFacturaCuentaLinkser.getNombreArchivoProcesado(), TipoArchivo.FLI, mensaje);
        }else{
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_FACTURACION_ERROR_OPERADOR_LINKSER, Variables.FTP_MENSAJE_EMAIL_FACTURACION_ERROR_OPERADOR_LINKSER_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador("", TipoArchivo.FLI, mensaje);
        }
    }
}

