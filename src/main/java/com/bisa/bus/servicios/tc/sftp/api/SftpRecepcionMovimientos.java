package com.bisa.bus.servicios.tc.sftp.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.csv.LecturaCSV;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.tc.sftp.dao.MovimientosDao;
import com.bisa.bus.servicios.tc.sftp.dao.MovimientosHistoricoDao;
import com.bisa.bus.servicios.tc.sftp.entities.MovimientosHistoricoTC;
import com.bisa.bus.servicios.tc.sftp.entities.MovimientosTC;
import com.bisa.bus.servicios.tc.sftp.model.*;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgProgramaTarjetaCredito;
import com.bisa.bus.servicios.tc.sftp.utils.FormatoNumericoTexto;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by atenorio on 07/06/2017.
 */
public class SftpRecepcionMovimientos implements Serializable {

    private static final long serialVersionUID = -7112978535165279100L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpRecepcionMovimientos.class);

    private final MedioAmbiente medioAmbiente;
    private final MovimientosDao movimientosDao;
    private final MovimientosHistoricoDao movimientosHistoricoDao;
    private final RpgProgramaTarjetaCredito rpgPrograma;
    private final NotificacionesCorreo notificacionesCorreo;
    SftpConfiguracion configuracion;
    SftpTransferencia sftpTransferencia;
    private String nombreArchivoProcesado = "";

    @Inject
    public SftpRecepcionMovimientos(MedioAmbiente medioAmbiente,
                                    MovimientosDao movimientosDao, MovimientosHistoricoDao movimientosHistoricoDao,
                                    NotificacionesCorreo notificacionesCorreo, RpgProgramaTarjetaCredito rpgPrograma,
                                    SftpConfiguracion configuracion, SftpTransferencia sftpTransferencia) {
        this.medioAmbiente = medioAmbiente;
        this.movimientosDao = movimientosDao;
        this.movimientosHistoricoDao = movimientosHistoricoDao;
        this.notificacionesCorreo = notificacionesCorreo;
        this.configuracion = configuracion;
        this.rpgPrograma = rpgPrograma;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para procesar generacion de archivo y envio de archivo a servidor
     * remoto SFTP.
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    String PREF = "#fecha";

    public boolean procesarArchivo() {
        String detalle = "";
        boolean ok = true;
        LOGGER.debug("Inicia proceso de recepcion de movimientos");
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_MOVIMIETOS_086, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_MOVIMIETOS_086_DEFAULT);
        String nombreArchivo[] = Iterables.toArray(Splitter.on("|").trimResults().split(nombreArchivoRegex), String.class);

        // Procesar a solicitud fecha de pago
        String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_ATC, Variables.FECHA_DE_PROCESO_ATC_DEFAULT);
        // Calcular fecha de un anterior
        Calendar hoy = Calendar.getInstance();
        hoy.add(Calendar.DAY_OF_YEAR, -1);
        Date fechaProcesar = hoy.getTime();

        if (StringUtils.isEmpty(fechaProceso) || "0".equals(fechaProceso)) {
            fechaProceso = FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar);
        } else {
            fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
            if (fechaProcesar == null) {
                notificacionesCorreo.notificarError("Error en fecha de proceso recepci\u00F3n movimientos diarios", "");
                return false;
            }
        }
        nombreArchivoRegex = nombreArchivo[0].replace(PREF, new SimpleDateFormat(nombreArchivo[1]).format(fechaProcesar));

        LOGGER.info("Descargar archivo con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_MOVIMIENTOS_ATC, Variables.FTP_RUTA_RECEPCION_MOVIMIENTOS_ATC_DEFAULT));
        sftpTransferencia.configurar(configuracion);

        ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.MOV);
        LOGGER.info("Descarga completa:" + ok);

        if (!ok || sftpTransferencia.getArchivoLocal() == null) {
            String error = "Problema en transferencia o no existen archivos de respuesta en servidor SFTP.";
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.MOV, error);
            LOGGER.warn(error);
            return false;
        }

        // Procesamiento de archivo local
        File archivo = sftpTransferencia.getArchivoLocal();
        TotalMovimientosCsv total = mapearArchivo(archivo);

        List<MovimientoCsv> lista = null;
        if (total != null) lista = total.getMovimientos();
        if (lista == null || lista.size() == 0) {
            detalle = "Error en mapeo de archivo de respuesta o archivo vac\u00EDo";
            LOGGER.warn(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.MOV, detalle);
            return false;
        }
        for (MovimientoCsv dg : lista) {
            // Validar que se realizo el mapeo de forma exitosa de la estructura de pago
            if (dg == null) {
                detalle = "Error en mapeo de una estructura se requiere revisar el archivo de respuesta.";
                LOGGER.error(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.MOV, detalle);
                return false;
            }
        }
        nombreArchivoProcesado = sftpTransferencia.getNombreArchivoRemoto();

        LOGGER.debug("Actualizar tabla de movimientos");
        LOGGER.debug("Verificar si un registro se encuentra en un estado inconsistente");
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();

        // Limpiar tablas temporales
        if (!movimientosDao.limpiarTodo()) {
            detalle = "Error al limpiar tabla temporal de datos generales.";
            LOGGER.error(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.MOV, detalle);
            return false;
        }
        int totalRegistros = 0;

        // Verificar consistencia de registro
        for (MovimientoCsv mov : lista) {
            totalRegistros++;
            MovimientosTC movimiento = transferObject.convert(mov, MovimientosTC.class);
            movimiento.setCodigoAdm('A');
            movimiento.setFechaProceso(total.getFechaProceso());
            movimiento.setEstado(EstadoSolicitudTC.GENE.name());
            movimiento.setImporteOrigenSigno(FormatoNumericoTexto.getSigno(movimiento.getImporteOrigen()));
            movimiento.setImporteOrigenTexto(FormatoNumericoTexto.getNumero(movimiento.getImporteOrigen(), 10));
            movimiento.setImporteBalanceSigno(FormatoNumericoTexto.getSigno(movimiento.getImporteBalance()));
            movimiento.setImporteBalanceTexto(FormatoNumericoTexto.getNumero(movimiento.getImporteBalance(), 10));
            movimiento.setImporteLiquidacionSigno(FormatoNumericoTexto.getSigno(movimiento.getImporteLiquidacion()));
            movimiento.setImporteLiquidacionTexto(FormatoNumericoTexto.getNumero(movimiento.getImporteLiquidacion(), 10));
            movimiento.setImporteCashBackSigno(FormatoNumericoTexto.getSigno(movimiento.getImporteCashBack()));
            movimiento.setImporteCashBackTexto(FormatoNumericoTexto.getNumero(movimiento.getImporteCashBack(), 10));
            movimiento.setMontoAdicionalSigno(FormatoNumericoTexto.getSigno(movimiento.getMontoAdicional()));
            movimiento.setMontoAdicionalTexto(FormatoNumericoTexto.getNumero(movimiento.getMontoAdicional(), 10));
            movimiento.setFechaRecepcion(new Date());
            movimiento.setIdArchivoRecepcion(
                    (sftpTransferencia.getFtpArchivoTransferencia() == null) ? 0 : sftpTransferencia.getFtpArchivoTransferencia().getId());

            ok = ok && movimientosDao.guardar(movimiento, "ISB");
            MovimientosHistoricoTC movimientosHistorico = transferObject.convert(movimiento, MovimientosHistoricoTC.class);
            ok = ok && movimientosHistoricoDao.guardar(movimientosHistorico, "ISB");
        }
        if (ok) {
            detalle = "Completada carga de archivos correctamente con " + totalRegistros + " registros.";
            sftpTransferencia.transferenciaCompleta(detalle);
            notificacionesCorreo.notificarCorrectaTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.MOV, detalle);
        } else {
            detalle = "Problema en proceso de carga de archivo.";
            sftpTransferencia.transferenciaConErrorInterno(detalle);
            notificacionesCorreo.notificarErrorTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.MOV, detalle);
        }

        LOGGER.debug("Completado proceso de carga de archivo {} de pagos a servidor SFTP de ATC correctamente.", sftpTransferencia.getNombreArchivoRemoto());
        return ok;
    }


    public TotalMovimientosCsv mapearArchivo(File archivo) {
        LOGGER.info("Ruta de archivo local a procesar:{}", archivo.getAbsolutePath());
        FileInputStream is = null;
        BufferedReader br = null;
        TotalMovimientosCsv total = null;
        List<MovimientoCsv> lista = new ArrayList<MovimientoCsv>();
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();
        try {
            is = new FileInputStream(archivo);
            String codificacion = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC, Variables.FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC_DEFAULT);
            br = new BufferedReader(new InputStreamReader(is, codificacion));
            String linea = br.readLine();
            List<String> lineas = new ArrayList<String>();

            while (linea != null) {
                LOGGER.debug("Detalle de registros recuperados: {}", linea);
                if (StringUtils.isNotEmpty(linea) && linea.length() > 1) {
                    MovimientoCsv dg = LecturaCSV.procesar(MovimientoCsv.class, linea, "|");
                    if (dg == null) {
                        total = LecturaCSV.procesar(TotalMovimientosCsv.class, linea, "|");
                    } else {
                        lista.add(dg);
                    }
                }
                linea = br.readLine();
            }
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al procesar el archivo "
                    + archivo.getName() + ".", e);
            return null;
        } finally {
            IOUtils.closeQuietly(is);
        }
        LOGGER.info("Fecha de proceso {} de archivo y registros {}", total.getFechaProceso(), total.getCantidad());
        if (total != null) total.setMovimientos(lista);
        return total;
    }

    public String getNombreArchivoProcesado() {
        return nombreArchivoProcesado;
    }

    public void setNombreArchivoProcesado(String nombreArchivoProcesado) {
        this.nombreArchivoProcesado = nombreArchivoProcesado;
    }
}
