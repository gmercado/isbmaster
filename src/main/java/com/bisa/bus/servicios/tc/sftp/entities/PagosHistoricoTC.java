package com.bisa.bus.servicios.tc.sftp.entities;

import com.bisa.bus.servicios.tc.sftp.model.EstadoPagosTC;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by atenorio on 01/06/2017.
 */
@Entity
@Table(name = "TCPPAGOAH")
public class PagosHistoricoTC implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private IdPagosTC idPago;

    @Basic(optional = false)
    @Column(name = "PANUMPAG", nullable = false)
    private Long id;

    @Basic(optional = false)
    @Column(name = "PATIPREG", nullable = false, length = 2)
    private String tipoRegistro;
    //    @Basic(optional = false)
//    @Column(name = "PANUCUEN", nullable = false)
//    private BigInteger cuentaTarjeta;
    @Basic(optional = false)
    @Column(name = "PACOMON", nullable = false)
    private short tipoMoneda;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "PAIMPPG", nullable = false, precision = 18, scale = 6)
    private BigDecimal importe;
    @Basic(optional = false)
    @Column(name = "PAFPAG", nullable = false, length = 8)
    private String fechaPago;
    @Basic(optional = false)
    @Column(name = "PAFPAGH", nullable = false, length = 8)
    private String horaPago;
    @Basic(optional = false)
    @Column(name = "PAFRMPAG", nullable = false)
    private short forma;
    @Basic(optional = false)
    @Column(name = "PACNLPAG", nullable = false)
    private short canal;
    //    @Basic(optional = false)
//    @Column(name = "PATRN", nullable = false, length = 20)
    //   private String transaccion;
    @Basic(optional = false)
    @Column(name = "PAINFAD", nullable = false, length = 120)
    private String informacionAdicional;
    @Basic(optional = false)
    @Column(name = "PACONFIR", nullable = false)
    private Character confirmacion;

    // Identificador devuelto por sistema Nazir para el pago enviado
    @Basic(optional = false)
    @Column(name = "PACODIDF", nullable = false, length = 20)
    private String codigoPago;
    @Basic(optional = false)
    @Column(name = "PACODERR", nullable = false, length = 3)
    private String codError;
    @Basic(optional = false)
    @Column(name = "PAESTADO", nullable = false, length = 3)
    private String estado;

    @Basic(optional = false)
    @Column(name = "PAIDEN", nullable = false, length = 3)
    private Long idArchivoEnvio;
    @Basic(optional = false)
    @Column(name = "PAFEEN", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    @Basic(optional = false)
    @Column(name = "PAIDRE", nullable = false, length = 3)
    private Long idArchivoRecepcion;
    @Basic(optional = false)
    @Column(name = "PANUTC", nullable = false)
    private Long cuentaTarjeta;
    @Basic(optional = false)
    @Column(name = "PANUT1", nullable = false)
    private String cuentaTarjetaCorto;

    @Basic(optional = false)
    @Column(name = "PAFERE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRecepcion;

    @Basic(optional = false)
    @Column(name = "PAPGCR", nullable = false, length = 10)
    private String usuarioCreador;
    @Basic(optional = false)
    @Column(name = "PAFECR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @Column(name = "PAPGMO", nullable = false, length = 10)
    private String usuarioModificador;
    @Basic(optional = false)
    @Column(name = "PAFEMO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    @Transient
    private EstadoPagosTC estadoPago;

    public EstadoPagosTC getEstadoPago() {
        return EstadoPagosTC.valorEnum(getEstado())!=null?EstadoPagosTC.valorEnum(getEstado()):EstadoPagosTC.GENE;
    }

    public void setEstadoPago(EstadoPagosTC estado) {
        this.setEstado(estado.name());
        this.estadoPago = estado;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "PagosTC[ idPago=" + getId() + " ]";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoRegistro() {
        return tipoRegistro;
    }

    public void setTipoRegistro(String tipoRegistro) {
        this.tipoRegistro = tipoRegistro;
    }

    public short getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(short tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getHoraPago() {
        return horaPago;
    }

    public void setHoraPago(String horaPago) {
        this.horaPago = horaPago;
    }

    public short getForma() {
        return forma;
    }

    public void setForma(short forma) {
        this.forma = forma;
    }

    public short getCanal() {
        return canal;
    }

    public void setCanal(short canal) {
        this.canal = canal;
    }

//    public String getTransaccion() {
//        return transaccion;
//    }
//
//    public void setTransaccion(String transaccion) {
//        this.transaccion = transaccion;
//    }

    public String getInformacionAdicional() {
        return informacionAdicional;
    }

    public void setInformacionAdicional(String informacionAdicional) {
        this.informacionAdicional = informacionAdicional;
    }

    public Character getConfirmacion() {
        return confirmacion;
    }

    public void setConfirmacion(Character confirmacion) {
        this.confirmacion = confirmacion;
    }

    public String getCodigoPago() {
        return codigoPago;
    }

    public void setCodigoPago(String codigoPago) {
        this.codigoPago = codigoPago;
    }

    public String getCodError() {
        return codError;
    }

    public void setCodError(String codError) {
        this.codError = codError;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

//    public BigInteger getCuentaTarjeta() {
//        return cuentaTarjeta;
//    }
//
//    public void setCuentaTarjeta(BigInteger cuentaTarjeta) {
//        this.cuentaTarjeta = cuentaTarjeta;
//    }

    public IdPagosTC getIdPago() {
        return idPago;
    }

    public void setIdPago(IdPagosTC idPago) {
        this.idPago = idPago;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public Long getIdArchivoEnvio() {
        return idArchivoEnvio;
    }

    public void setIdArchivoEnvio(Long idArchivoEnvio) {
        this.idArchivoEnvio = idArchivoEnvio;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public Long getIdArchivoRecepcion() {
        return idArchivoRecepcion;
    }

    public void setIdArchivoRecepcion(Long idArchivoRecepcion) {
        this.idArchivoRecepcion = idArchivoRecepcion;
    }

    public Long getCuentaTarjeta() {
        return cuentaTarjeta;
    }

    public void setCuentaTarjeta(Long cuentaTarjeta) {
        this.cuentaTarjeta = cuentaTarjeta;
    }

    public String getCuentaTarjetaCorto() {
        return cuentaTarjetaCorto;
    }

    public void setCuentaTarjetaCorto(String cuentaTarjetaCorto) {
        this.cuentaTarjetaCorto = cuentaTarjetaCorto;
    }
}
