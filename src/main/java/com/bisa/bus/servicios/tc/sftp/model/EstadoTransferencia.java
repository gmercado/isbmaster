package com.bisa.bus.servicios.tc.sftp.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by atenorio on 03/05/2017.
 */
public enum EstadoTransferencia {
    LEAR("Lectura de archivo"),
    GENE("Datos preparados para envio"),
    PROC("Enviado a proceso"),
    ERVA("Error de validaci\u00F3n"),
    ERPR("Error en proceso"),
    ERIN("Error en interno"),
    PRTE("Proceso terminado"),
    REPR("Reprocesar archivo");

    //AREN("Archivo enviado"),
    //ARRE("Archivo recibido"),;

    private EstadoTransferencia(String descripcion) {
        this.descripcion = descripcion;
    }

    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public static EstadoTransferencia valorEnum(String valor) {
        List<EstadoTransferencia> estados = new ArrayList<>(Arrays.asList((EstadoTransferencia.values())));
        for (EstadoTransferencia es : estados) {
            if (valor == es.name()) {
                return es;
            }
        }
        return null;
    }
}
