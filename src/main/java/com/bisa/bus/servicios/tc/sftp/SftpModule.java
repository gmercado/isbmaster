package com.bisa.bus.servicios.tc.sftp;

import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.bus.servicios.tc.linkser.sched.*;
import com.bisa.bus.servicios.tc.sftp.api.SftpConfiguracion;
import com.bisa.bus.servicios.tc.sftp.api.SftpConfiguracionATC;
import com.bisa.bus.servicios.tc.sftp.dao.FtpTransferenciaDao;
import com.bisa.bus.servicios.tc.sftp.dao.IFtpTransferencia;
import com.bisa.bus.servicios.tc.linkser.api.ISftpConfiguracionLinkser;
import com.bisa.bus.servicios.tc.linkser.api.SftpConfiguracionLinkser;
import com.bisa.bus.servicios.tc.sftp.rpg.IRpgCargarSolicitud;
import com.bisa.bus.servicios.tc.sftp.rpg.IRpgProcesarSolicitudAltas;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgCargarSolicitud;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgProcesarSolicitudAltas;
import com.bisa.bus.servicios.tc.sftp.sched.*;

/**
 * Created by atenorio on 03/05/2017.
 */
public class SftpModule extends AbstractModuleBisa {
    @Override
    protected void configure() {
        bind(IFtpTransferencia.class).to(FtpTransferenciaDao.class);
        bind(IRpgCargarSolicitud.class).to(RpgCargarSolicitud.class);
        bind(IRpgProcesarSolicitudAltas.class).to(RpgProcesarSolicitudAltas.class);
        bind(SftpConfiguracion.class).to(SftpConfiguracionATC.class);
        bind(ISftpConfiguracionLinkser.class).to(SftpConfiguracionLinkser.class);

        // Definicion de horarios en base a correo de ATC

        //Estos podran ser enviados hasta las 19:00 todos los días de lunes a domingo.
        bindSched("EnvioSolicitud", "ATC", "0 30 18 * * ?", EnvioSolicitud.class);
        //Estos podrán ser recibidos despues las 19:00 todos los días de lunes a domingo, se programa el inicio a las 19:15.
        bindSched("RecepcionSolicitudAlta", "ATC", "0 15 19 * * ?", RecepcionSolicitudAlta.class); //0 */10 10-11 * * ?
        // Los archivos de pagos podran ser enviados hasta las 21:00 todos los días de lunes a domingo, se programa inicio a las 20:30.
        bindSched("EnvioPagos", "ATC", "0 30 20 * * ?", EnvioPagos.class); //"0 */20 20-21 * * ?"
        // Los archivos de pagos deberian ser recibidos despues de las 21:15 todos los días de lunes a domingo.
        bindSched("RecepcionPagos", "ATC", "0 15 21 * * ?", RecepcionPagos.class); //"*/15 * * * * ?"
        //Estos archivos podrán ser entregados entre las 3:00 y 6:00 todos los días de lunes a domingo con fecha a un dia anterior.
        bindSched("RecepcionCierreDiario", "ATC", "0 0 6 * * ?", RecepcionCierreDiario.class); //0 */59 3-6 * * ?
        //Se mantiene el horario para los procesos mensuales 06:00 – 12:00 posterior al día de cierre de ATC, generalmente de fecha 25 de mes
        bindSched("RecepcionCierreMensual", "ATC", "0 0 12 26 * ?", RecepcionCierreMensual.class);
        bindSched("RecepcionFacturacion", "ATC", "0 0 12 26 * ?", RecepcionFacturacion.class);

        //Trabajo programado linkser  4	0 0 16 ? * MON-FRI
        bindSched("EnvioSolicitudAltaLinkser", "LINKSER", "0 30 20 ? * MON-FRI", EnvioSolicitudAltaLinkser.class);
        bindSched("EnvioPagosLinkser", "LINKSER", "0 0 21 ? * MON-FRI", EnvioPagosLinkser.class);
        bindSched("EnvioAnticiposLinkser", "LINKSER", "0 0 7 ? * MON-FRI", EnvioAnticiposLinkser.class);
        bindSched("RecepcionSolicitudAltaLinkser", "LINKSER", "0 0 10 * * ?", RecepcionSolicitudAltaLinkser.class); //0 */10 10-11 * * ?
        bindSched("RecepcionCierreDiarioLinkser", "LINKSER", "0 0 12 * * ?", RecepcionCierreDiarioLinkser.class);
        bindSched("RecepcionCierreMensualLinkser", "LINKSER", "0 0 12 26 * ?", RecepcionCierreLinkser.class);
        bindSched("RecepcionFacturasLinkser", "LINKSER", "0 0 12 26 * ?", RecepcionFacturasLinkser.class);
        // Procesamiento a pago de establecminetos de linkser
        bindSched("RecepcionPagoEstablecimientoLinkser", "LINKSER", "0 30 9 ? * MON-FRI", RecepcionPagoEstablecimientoLinkser.class);
        // Procesamiento de transferencia de archivos de atc a linkser
        bindSched("TransferenciaAtcLinkser", "LINKSER", "0 45 7 * * ?", TransferenciaAtcLinkser.class);

        bindUI("com/bisa/bus/servicios/tc/sftp/ui");
        bindUI("com/bisa/bus/servicios/tc/linkser/ui");
    }
}
