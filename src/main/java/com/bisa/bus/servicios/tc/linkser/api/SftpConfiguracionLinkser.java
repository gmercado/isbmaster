package com.bisa.bus.servicios.tc.linkser.api;

import bus.config.dao.CryptUtils;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.google.inject.Inject;

/**
 * Created by atenorio on 09/11/2017.
 */
public class SftpConfiguracionLinkser implements ISftpConfiguracionLinkser {

    static String MODO_MD5 = "MD5";
    static String MODO_TEMPORAL = "TEMP";
    static String MODO_COPIA = "COPIA";

    private final MedioAmbiente medioAmbiente;
    private final CryptUtils cryptUtils;

    @Inject
    private SftpConfiguracionLinkser(MedioAmbiente medioAmbiente, CryptUtils cryptUtils) {
        this.medioAmbiente = medioAmbiente;
        this.cryptUtils = cryptUtils;
    }

    public void inicializar(){
        setUrl(medioAmbiente.getValorDe(Variables.FTP_URL_LINKSER, Variables.FTP_URL_LINKSER_DEFAULT));
        setUsuario(medioAmbiente.getValorDe(Variables.FTP_USER_LINKSER, Variables.FTP_USER_LINKSER_DEFAULT));
        setPassword(medioAmbiente.getValorDe(Variables.FTP_PASSWORD_LINKSER, Variables.FTP_PASSWORD_LINKSER_DEFAULT));
        //setRemotePath(medioAmbiente.getValorDe();
        setLocalTemp(medioAmbiente.getValorDe(Variables.FTP_LOCAL_TEMP_LINKSER, Variables.FTP_LOCAL_TEMP_LINKSER_DEFAULT));
        setMd5Bajada("true".equals(medioAmbiente.getValorDe(Variables.FTP_MD5_BAJADA_LINKSER, Variables.FTP_MD5_BAJADA_LINKSER_DEFAULT)));
        setMd5Subida("true".equals(medioAmbiente.getValorDe(Variables.FTP_MD5_SUBIDA_LINKSER, Variables.FTP_MD5_SUBIDA_LINKSER_DEFAULT)));

        // verifica si esta encriptado
        if (cryptUtils.esCryptLegado(getPassword())) {
            setPassword(cryptUtils.dec(getPassword()));
        } else if (cryptUtils.esOpenssl(getPassword())) {
            setPassword(cryptUtils.decssl(getPassword()));
        }
    }

    private String url;
    private String password;
    private String usuario;
    private String localTemp;
    private String remotePath;
    private String remoteTemp;
    private boolean md5Bajada;
    private boolean md5Subida;

    public String getRemoteTemp() {
        return remoteTemp;
    }

    public void setRemoteTemp(String remoteTemp) {
        this.remoteTemp = remoteTemp;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getLocalTemp() {
        return localTemp;
    }

    public void setLocalTemp(String localTemp) {
        this.localTemp = localTemp;
    }

    public String getRemotePath() {
        return remotePath;
    }

    public void setRemotePath(String remotePath) {
        this.remotePath = remotePath;
    }

    @Override
    public boolean getMd5Subida() {
        return false;
    }

    public boolean isMd5Bajada() {
        return md5Bajada;
    }

    @Override
    public void setMd5Bajada(boolean md5Bajada) {
        this.md5Bajada = md5Bajada;
    }

    public boolean isMd5Subida() {
        return md5Subida;
    }

    @Override
    public void setMd5Subida(boolean md5Subida) {
        this.md5Subida = md5Subida;
    }

    @Override
    public boolean getMd5Bajada() {
        return false;
    }
}
