package com.bisa.bus.servicios.tc.linkser.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.file.ArchivoBase;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.tc.sftp.api.SftpTransferencia;
import com.bisa.bus.servicios.tc.linkser.dao.CierreConsumosLinkserTxtDao;
import com.bisa.bus.servicios.tc.linkser.entities.CierreConsumosLinkserTxt;
import com.bisa.bus.servicios.tc.linkser.utils.MapeoArchivo;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import com.jcraft.jsch.SftpException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by atenorio on 16/11/2017.
 */
public class SftpCierreConsumosLinkser implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpCierreConsumosLinkser.class);


    private final MedioAmbiente medioAmbiente;
    private final ArchivoBase archivoBase;
    private final NotificacionesCorreo notificacionesCorreo;
    private final CierreConsumosLinkserTxtDao iCierreConsumosLinkserTxtDao;

    ISftpConfiguracionLinkser configuracion;
    SftpTransferencia sftpTransferencia;
    private String nombreArchivoProcesado = "";
    Date fechaProcesar = null;

    @Inject
    public SftpCierreConsumosLinkser(ArchivoBase archivoBase,
                                     MedioAmbiente medioAmbiente,
                                     NotificacionesCorreo notificacionesCorreo,
                                     CierreConsumosLinkserTxtDao iCierreConsumosLinkserTxtDao,
                                     ISftpConfiguracionLinkser configuracion, SftpTransferencia sftpTransferencia) {
        this.medioAmbiente = medioAmbiente;
        this.archivoBase = archivoBase;
        this.notificacionesCorreo = notificacionesCorreo;
        this.iCierreConsumosLinkserTxtDao = iCierreConsumosLinkserTxtDao;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para procesar recepcion de archivo de consumos de cierre mensual de servidor remoto SFTP.
     * CONSUMOS .DAT
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    String PREF = "#fecha";

    public boolean procesarArchivo() {
        boolean ok = false;
        String detalle = "";
        LOGGER.debug("Inicia proceso recepcion de archivo de solicitud");
        LOGGER.debug("1) Verificar archivo pendiente de respuesta");
        String nombreArchivoOriginal = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_LINKSER_CIERRE_CONSUMOS_ORIGINAL, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_LINKSER_CIERRE_CONSUMOS_ORIGINAL_DEFAULT);
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_LINKSER_CIERRE_CONSUMOS, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_LINKSER_CIERRE_CONSUMOS_DEFAULT);

        String nombreArchivo[] = Iterables.toArray(Splitter.on("|").trimResults().split(nombreArchivoRegex), String.class);

        String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_LINKSER, Variables.FECHA_DE_PROCESO_LINKSER_DEFAULT);
        // El proceso normal realiza la recepcion de archivo de alta de un dia anterior,
        // debido a la disponibilidad del archivo de respuesta.
        Calendar hoy = Calendar.getInstance();
        hoy.add(Calendar.DAY_OF_YEAR, -1);
        fechaProcesar = hoy.getTime();

        // Procesar a fecha determinada por parametro
        if (StringUtils.isNotEmpty(fechaProceso) && !"0".equals(fechaProceso)) {
            fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
            if (fechaProcesar == null) {
                notificacionesCorreo.notificarError("Error en fecha de proceso recepci\u00F3n consumos", "");
                return false;
            }
        }
        nombreArchivoRegex = nombreArchivo[0].replace(PREF, new SimpleDateFormat(nombreArchivo[1]).format(fechaProcesar));
        prepararConfiguracionSftp();
        try {
            sftpTransferencia.renombrarArchivo(getRutaRemota(), nombreArchivoOriginal, nombreArchivoRegex);
        } catch (SftpException e) {
            LOGGER.error("Error al renombrar archivo: {}", e.getMessage());
        }
        ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.CLI);

        LOGGER.debug("3) Validar archivo descargado");
        LOGGER.debug("3.1) Validar estructura");
        LOGGER.debug("3.2) Validar mapeo");

        // Cerrar el registro con error y permitir reintento
        if (!ok || sftpTransferencia.getArchivoLocal() == null) {
            sftpTransferencia.transferenciaConErrorReintento("");
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.CLI, "No existen archivos de respuesta en servidor SFTP.");
            LOGGER.warn("No existen archivos de respuesta en servidor SFTP.");
            return false;
        }

        // Procesamiento de archivo local
        File archivo = sftpTransferencia.getArchivoLocal();
        List<String> mapeo = MapeoArchivo.mapearArchivo(archivo);
        // Verificar si existen problemas en mape de archivo de respuesta
        if (mapeo == null || mapeo.size() == 0) {
            LOGGER.warn("Error en mapeo de archivo de respuesta");
            sftpTransferencia.transferenciaConErrorReintento("");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.CLI, "Error en mapeo de archivo de respuesta.");
            return false;
        }


        LOGGER.debug("4) Actualizar tabla de solicitudes");
        LOGGER.debug("4.1) Verificar si un registro se encuentra en un estado inconsistente");
        int totalRegistros = 0;

        for (String solicitud : mapeo) {
            // Validar que se realizo el mapeo de forma exitosa en todas las estructuras
            if (solicitud == null) {
                LOGGER.error("Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                sftpTransferencia.transferenciaConErrorReintento("");
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.CLI, "Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                return false;
            }
            // TODO ATC confirmar validaciones
        }

        if (!iCierreConsumosLinkserTxtDao.limpiarTodo()) {
            detalle = "Error al limpiar tabla temporal de tarjetas.";
            LOGGER.error(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.CLI, detalle);
            return false;
        }

        for (String solicitud : mapeo) {
            // Grabar
            LOGGER.debug("====================================");
            LOGGER.debug("LINEA {}:{}", totalRegistros, solicitud);
            iCierreConsumosLinkserTxtDao.persist(new CierreConsumosLinkserTxt(solicitud));
            totalRegistros++;
        }

        // Eliminar archivo de servidor local
        // TODO ATC eliminar archivo remoto
        /*
        if (FileUtils.deleteQuietly(archivo)) {
            LOGGER.info("Se ha eliminado el archivo de altas, {}", archivo.getName());
        }*/

        LOGGER.debug("4.2) Verificar si un registro se encuentra en un estado inconsistente");

        // Modificar estado de solicitudes de alta y notificar por correo
        if (ok) {
            sftpTransferencia.transferenciaCompleta("");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.CLI, "Transferencia completa con respuesta a " + totalRegistros + " consumos.");
            LOGGER.info("Proceso finalizado de recepcion de consumos.");
        } else {
            detalle = "Error en proceso de recepcion de consumos.";
            LOGGER.warn(detalle);
            sftpTransferencia.transferenciaConErrorInterno(detalle);
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.CLI, detalle);
        }
        return ok;
    }

    public boolean prepararConfiguracionSftp() {
        configuracion.inicializar();
        configuracion.setRemotePath(getRutaRemota());
        return sftpTransferencia.configurar(configuracion);
    }

    private String getRutaRemota() {
        String ruta = medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_CIERRE_LINKSER, Variables.FTP_RUTA_RECEPCION_CIERRE_LINKSER_DEFAULT);
        String rutas[] = Iterables.toArray(Splitter.on("|").trimResults().split(ruta), String.class);
        String mes = new SimpleDateFormat(rutas[2], new Locale("es", "ES")).format(fechaProcesar);
        mes = StringUtils.capitalize(mes);
        ruta = rutas[0].replace("#anio", new SimpleDateFormat(rutas[1]).format(fechaProcesar));
        ruta = ruta.replace("#mes", mes);
        return ruta;
    }

    public String getNombreArchivoProcesado() {
        return sftpTransferencia.getNombreArchivoRemotoFecha();
    }

}