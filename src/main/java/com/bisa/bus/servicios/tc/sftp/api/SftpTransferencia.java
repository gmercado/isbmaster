package com.bisa.bus.servicios.tc.sftp.api;

import bus.env.api.MedioAmbiente;
import bus.plumbing.file.ArchivoBase;
import bus.plumbing.sftp.SftpOperations;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.tc.sftp.dao.IFtpTransferencia;
import com.bisa.bus.servicios.tc.sftp.entities.FtpArchivoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.EstadoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.vfs.FileSystemException;
import org.apache.commons.vfs.provider.URLFileName;
import org.apache.commons.vfs.provider.URLFileNameParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by atenorio on 30/05/2017.
 */
public class SftpTransferencia implements DisposableBean {

    public static final int DELAY = 10000;

    public static final int INTENTOS_RECONEXION = 2;

    private static final Logger LOGGER = LoggerFactory.getLogger(SftpTransferencia.class);

    private SftpOperations operations;

    private URLFileName parsedUri;

    private Map<String, String> parameterMap;

    private File archivoLocal;

    private Integer fechaProceso = 0;

    private String nombreArchivoRemoto = "";

    private Hashtable<String, Date> fechaArchivoRemoto = new Hashtable<String, Date>();

    private FtpArchivoTransferencia ftpArchivoTransferencia;

    private TipoArchivo tipoArchivo;

    ISftpConfiguracion configuracion;

    private final MedioAmbiente medioAmbiente;
    private final IFtpTransferencia iFtpTransferencia;
    private final ArchivoBase archivoBase;
    private final NotificacionesCorreo notificacionesCorreo;

    boolean ok;

    @Inject
    public SftpTransferencia(MedioAmbiente medioAmbiente, IFtpTransferencia iFtpTransferencia,
                             ArchivoBase archivoBase, NotificacionesCorreo notificacionesCorreo) {
        this.medioAmbiente = medioAmbiente;
        this.iFtpTransferencia = iFtpTransferencia;
        this.archivoBase = archivoBase;
        this.notificacionesCorreo = notificacionesCorreo;
    }

    /**
     * Configurar parametros iniciales de conexión con SFTP
     **/
    public boolean configurar(ISftpConfiguracion conf) {
        this.configuracion = conf;
        URLFileNameParser nameParser = new URLFileNameParser(22);
        try {
            parsedUri = (URLFileName) nameParser.parseUri(null, null, configuracion.getUrl());
        } catch (FileSystemException e) {
            LOGGER.error("Error parseo URL: {} ", e);
            return false;
        }

        if (parsedUri == null || parsedUri.getScheme() == null || !parsedUri.getScheme().startsWith("sftp")) {
            throw new IllegalStateException("Solo aceptamos conexion sftp");
        }

        parameterMap = new LinkedHashMap<String, String>();
        String[] parameters = StringUtils.split(parsedUri.getQueryString(), ";&?");
        for (String parameter : parameters) {
            int p = parameter.indexOf("=");
            if (p >= 0) {
                String name = parameter.substring(0, p);
                String value = parameter.substring(p + 1);
                parameterMap.put(name, value);
            } else {
                parameterMap.put(parameter, null);
            }
        }
        LOGGER.debug("Parametros de la conexion: {}", parameterMap);
        operations = new SftpOperations(DELAY, INTENTOS_RECONEXION, configuracion.getLocalTemp());
        return true;
    }

    /**
     * Configurar parametros iniciales de conexión con SFTP
     **/
    public boolean configurar(SftpConfiguracion conf) {
        this.configuracion = conf;
        URLFileNameParser nameParser = new URLFileNameParser(22);
        try {
            parsedUri = (URLFileName) nameParser.parseUri(null, null, configuracion.getUrl());
        } catch (FileSystemException e) {
            LOGGER.error("Error parseo URL: {} ", e);
            return false;
        }

        if (parsedUri == null || parsedUri.getScheme() == null || !parsedUri.getScheme().startsWith("sftp")) {
            throw new IllegalStateException("Solo aceptamos conexion sftp");
        }

        parameterMap = new LinkedHashMap<String, String>();
        String[] parameters = StringUtils.split(parsedUri.getQueryString(), ";&?");
        for (String parameter : parameters) {
            int p = parameter.indexOf("=");
            if (p >= 0) {
                String name = parameter.substring(0, p);
                String value = parameter.substring(p + 1);
                parameterMap.put(name, value);
            } else {
                parameterMap.put(parameter, null);
            }
        }
        LOGGER.debug("Parametros de la conexion: {}", parameterMap);
        operations = new SftpOperations(DELAY, INTENTOS_RECONEXION, configuracion.getLocalTemp());
        return true;
    }

    /**
     * Establecimiento de conexion a sftp
     **/
    private void conectar() throws JSchException, FileSystemException, SftpException {
        operations.connect(parsedUri.getHostName(), parsedUri.getPort(), configuracion.getUsuario(), configuracion.getPassword(), //parsedUri.getUserName()
                parameterMap.get("known_hosts"), parameterMap.get("private_key_file"),
                parameterMap.get("private_key_file_passphrase"), parameterMap.get("accept_all_hosts") != null);
    }

    /**
     * Desconexión a servicio sftp
     **/
    private void desconectar() {
        if (operations != null) {
            operations.disconnect();
        }
    }

    /**
     * Proceso de descarga de archivo de respuesta de servidor sftp
     */
    public boolean descargarArchivo(String REGEX_NOMBRE, TipoArchivo tipo) {

        this.tipoArchivo = tipo;
        HashMap<String, String> erroresDeValidacion = new HashMap<String, String>();
        LOGGER.debug("Inicio de descarga");
        LOGGER.debug("1) Conexion a servidor FTP");

        if (!operations.isConnected()) {
            try {
                conectar();
                ok = true;
//                notificacionesCorreo.notificarSoporte("Nos hemos podido conectar nuevamente al servicio de SFTP",
//                "La aplicacion habia perdido comunicacion y ahora la tiene nuevamente");
                LOGGER.info("Nos hemos podido conectar nuevamente al servicio de SSH.");
            } catch (Exception ex) {
                LOGGER.warn("La conexion SSH sigue mal", ex);
                return false;
            }
        }

        try {
            // Obtener listado de archivos
            LOGGER.info("2) Intentando obtener el listado de archivos de ruta {}", configuracion.getRemotePath());
            operations.changeCurrentDirectory(configuracion.getRemotePath());
            List<ChannelSftp.LsEntry> listFiles = operations.listFiles();
            LOGGER.info("2.1) Se ha obtenido el listado de archivos, son {} entradas.", listFiles.size());
            for (ChannelSftp.LsEntry lsEntry : listFiles) {
                ok = false;
                String filename = lsEntry.getFilename();
                LOGGER.debug("2.2) Verificar nombre de archivo <{}>.", filename);

                if (!validarNombre(REGEX_NOMBRE, filename)) {
                    LOGGER.debug("... el archivo <{}> no cumple con la convencion del nombre <{}>, ignorando",
                            filename, REGEX_NOMBRE);
                    continue;
                }

                if(validarNombreEnTransferencia(filename)){
                    LOGGER.debug("... el archivo <{}> no cumple con extencion temporal, ignorando",
                            filename);
                    continue;
                }

                LOGGER.info("Inicia transferecia del archivo <{}> ", filename);
                nombreArchivoRemoto = filename;

                // Verificamos si existen archivos en la base de datos con ese nombre para restringir descarga
                if (!verificarPermitirTransferencia(filename)) {
                    continue;
                }
                Date dateModify = new Date(lsEntry.getAttrs().getMTime() * 1000L);
                // Recuperar datos de archivo transferido
                fechaArchivoRemoto.put(filename,dateModify);

                LOGGER.info("Registrando archivo {} de tipo {}.", filename, tipo);
                // Registrar descarga en base de datos
                ftpArchivoTransferencia = crearRegistro(filename, tipo);

                LOGGER.info("Permitida la transaferencia del archivo <{}> ", filename);
                // Obtenemos el archivo en nuestra servidor local
                String filenameLocal = filename + "-" + UUID.randomUUID().toString();
                setArchivoLocal(operations.retrieveFileToFileInLocalWorkDirectory(
                        filename, filenameLocal));

                // Verificar si se aplica control por md5
                if (configuracion.getMd5Bajada()) {
                    // Aqui calculamos el MD5
                    String[] md5calculado = calcularMd5Archivo(getArchivoLocal());

                    // Ahora verificamos que no lo hayamos validado antes
                    String md5delError = erroresDeValidacion.get(filename);
                    LOGGER.info("Errores de validacion: {}, archivo actual {} md5 {}",
                            new Object[]{erroresDeValidacion, filename, md5calculado});
                    if (md5delError != null && md5delError.equals(md5calculado[0])) {
                        LOGGER.info("Ignorando el archivo '{}' para no pasar nuevamente por validaciones y mandar correo. " +
                                        "Paramos el lector de archivos...",
                                filename);
                        notificacionesCorreo.notificarSoporte("Verificaci\u00F3n MD5",
                                "Archivo no pasa verificaci\u00F3n MD5");
                        continue;
                    } else {
                        erroresDeValidacion.remove(filename);
                    }
                    LOGGER.info("Se completa validaciones del archivo <{}> ", filename);
                    // Obtenemos el archivo MD5SUM y verificamos que sea el mismo md5
                    StringBuilder nuevoMd5 = new StringBuilder(); // Contiene el nuevo archivo md5!
                    if (verificarElMd5sumRemoto(filename, md5calculado, nuevoMd5)) {
                        continue;
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        } catch (Exception ex) {
            if (ok) {
                LOGGER.error("Ha ocurrido un error muy grave al procesar archivo.", ex);
                ok = false;
            } else {
                LOGGER.warn("Ha ocurrido un error al procesar archivo sftp.");
                LOGGER.debug("El error en transferencia de archivo.", ex);
            }
        } finally {
            desconectar();
            LOGGER.debug("Desconectando, esperando 1 min y volviendo a conectar");
        }
        return ok;
    }

    /**
     * Proceso de descarga de archivo de respuesta de servidor sftp
     */
    public List<File> descargarListaArchivos(String REGEX_NOMBRE, String nombreProceso, TipoArchivo tipo) {

        // Verificamos si existen archivos en la base de datos con ese nombre agrupado para restringir descarga
        if (!verificarPermitirTransferencia(nombreProceso)) {
            return new ArrayList<File>();
        }
        nombreArchivoRemoto = "";
        List<File> listaArchivos = new ArrayList<File>();
        HashMap<String, String> erroresDeValidacion = new HashMap<String, String>();
        LOGGER.debug("Inicio de descarga");
        LOGGER.debug("1) Conexion a servidor FTP");
        if (!operations.isConnected()) {
            try {
                conectar();
                ok = true;
//                notificacionesCorreo.notificarSoporte("Nos hemos podido conectar nuevamente al servicio de SFTP",
//                        "La aplicacion habia perdido comunicacion y ahora la tiene nuevamente");
                LOGGER.info("Nos hemos podido conectar nuevamente al servicio de SSH.");
            } catch (Exception ex) {
                LOGGER.warn("La conexion SSH sigue mal", ex);
                return null;
            }
        }

        try {
            // Obtener listado de archivos
            LOGGER.info("2) Intentando obtener el listado de archivos de ruta {}", configuracion.getRemotePath());
            operations.changeCurrentDirectory(configuracion.getRemotePath());
            List<ChannelSftp.LsEntry> listFiles = operations.listFiles();
            LOGGER.debug("2.1) Se ha obtenido el listado de archivos, son {} entradas.", listFiles.size());
            for (ChannelSftp.LsEntry lsEntry : listFiles) {
                ok = false;
                String filename = lsEntry.getFilename();
                LOGGER.debug("2.2) Verificar nombre de archivo <{}>.", filename);

                if (!validarNombre(REGEX_NOMBRE, filename)) {
                    LOGGER.debug("... el archivo <{}> no cumple con la convencion del nombre <{}>, ignorando",
                            filename, REGEX_NOMBRE);
                    continue;
                }

                LOGGER.info("Procesando el archivo <{}> ", filename);
                nombreArchivoRemoto += " " + filename;
                if (ftpArchivoTransferencia == null)
                    ftpArchivoTransferencia = crearRegistro(nombreProceso, tipo);

                LOGGER.info("Permitido el procesamiento del archivo <{}> ", filename);
                // Obtenemos el archivo en nuestra servidor local
                String filenameLocal = filename + "-" + UUID.randomUUID().toString();
                listaArchivos.add(operations.retrieveFileToFileInLocalWorkDirectory(
                        filename, filenameLocal));

                Date dateModify = new Date(lsEntry.getAttrs().getMTime() * 1000L);
                // Recuperar datos de archivo transferido
                fechaArchivoRemoto.put(filename,dateModify);

            }
        } catch (Exception ex) {
            if (ok) {
                LOGGER.error("Ha ocurrido un error muy grave al procesar archivo.", ex);
                ok = false;
            } else {
                LOGGER.warn("Ha ocurrido un error al procesar archivo sftp.");
                LOGGER.debug("El error en transferencia de archivo.", ex);
            }
        } finally {
            desconectar();
            LOGGER.debug("Desconectando, esperando 1 min y volviendo a conectar");
        }
        return listaArchivos;
    }

    /**
     * Proceso de descarga de archivo de respuesta de servidor sftp
     */
    public List<File> descargarListaArchivos(String REGEX_NOMBRE, TipoArchivo tipo) {

        nombreArchivoRemoto = "";
        List<File> listaArchivos = new ArrayList<File>();
        HashMap<String, String> erroresDeValidacion = new HashMap<String, String>();
        LOGGER.debug("Inicio de descarga");
        LOGGER.debug("1) Conexion a servidor FTP");
        if (!operations.isConnected()) {
            try {
                conectar();
                ok = true;
//                notificacionesCorreo.notificarSoporte("Nos hemos podido conectar nuevamente al servicio de SFTP",
//                        "La aplicacion habia perdido comunicacion y ahora la tiene nuevamente");
                LOGGER.info("Nos hemos podido conectar nuevamente al servicio de SSH.");
            } catch (Exception ex) {
                LOGGER.warn("La conexion SSH sigue mal", ex);
                return null;
            }
        }

        try {
            // Obtener listado de archivos
            LOGGER.info("2) Intentando obtener el listado de archivos de ruta {}", configuracion.getRemotePath());
            operations.changeCurrentDirectory(configuracion.getRemotePath());
            List<ChannelSftp.LsEntry> listFiles = operations.listFiles();
            LOGGER.debug("2.1) Se ha obtenido el listado de archivos, son {} entradas.", listFiles.size());
            for (ChannelSftp.LsEntry lsEntry : listFiles) {
                ok = false;
                String filename = lsEntry.getFilename();
                LOGGER.debug("2.2) Verificar nombre de archivo <{}>.", filename);

                if (!validarNombre(REGEX_NOMBRE, filename)) {
                    LOGGER.debug("... el archivo <{}> no cumple con la convencion del nombre <{}>, ignorando",
                            filename, REGEX_NOMBRE);
                    continue;
                }
                // Verificamos si existen archivos en la base de datos con ese nombre agrupado para restringir descarga
                if (!verificarPermitirTransferencia(filename)) {
                    LOGGER.debug("No se permite procesamiento de archivo <{}>.", filename);
                    continue;
                }
                LOGGER.info("Procesando el archivo <{}> ", filename);
                nombreArchivoRemoto += " " + filename;
                if (ftpArchivoTransferencia == null)
                    ftpArchivoTransferencia = crearRegistro(filename, tipo);

                LOGGER.info("Permitido el procesamiento del archivo <{}> ", filename);
                // Obtenemos el archivo en nuestra servidor local
                String filenameLocal = filename + "-" + UUID.randomUUID().toString();
                listaArchivos.add(operations.retrieveFileToFileInLocalWorkDirectory(
                        filename, filenameLocal));

            }
        } catch (Exception ex) {
            if (ok) {
                LOGGER.error("Ha ocurrido un error muy grave al procesar archivo.", ex);
                ok = false;
            } else {
                LOGGER.warn("Ha ocurrido un error al procesar archivo sftp.");
                LOGGER.debug("El error en transferencia de archivo.", ex);
            }
        } finally {
            desconectar();
            LOGGER.debug("Desconectando, esperando 1 min y volviendo a conectar");
        }
        return listaArchivos;
    }


    /**
     * Metodo para carga de archivo de respuesta de servidor sftp
     */
    public boolean cargarArchivo(File archivoTemporal, String filename, TipoArchivo tipo) {
        FtpArchivoTransferencia fat = crearRegistro(filename, tipo);
        return cargarArchivo(archivoTemporal, filename, fat);
    }

    public boolean cargarArchivo(File archivoTemporal, String filename, FtpArchivoTransferencia fat) {
        int count = 0;
        HashMap<String, String> erroresDeValidacion = new HashMap<String, String>();
        LOGGER.info("1) Conexion a servidor SFTP");
        while (true) {
            if (!operations.isConnected()) {
                try {
                    conectar();
                    ok = true;
//                    notificacionesCorreo.notificarSoporte("Nos hemos podido conectar nuevamente al servicio de SFTP",
//                            "La aplicacion habia perdido comunicacion y ahora la tiene nuevamente");
                    LOGGER.info("Nos hemos podido conectar al servicio de SSH.");
                } catch (Exception ex) {
                    LOGGER.warn("La conexion SSH sigue mal", ex);
                    fat.setEstado(EstadoTransferencia.ERPR);
                    guadarRegistro(fat);
                    return false;
                }
            }
            LOGGER.debug("2) Proceso de transferencia");
            String md5AsHexString = "";
            try {
                FileInputStream fis = new FileInputStream(archivoTemporal);
                String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
                fis.close();
                md5AsHexString = md5;
                // Registrando procesamiento de transferencia
                fat.setEstado(EstadoTransferencia.PROC);
                guadarRegistro(fat);
                if (configuracion.getMd5Subida()) {
                    actualizarMd5DeRespuesta(md5AsHexString, filename, configuracion.getRemotePath());
                }
                LOGGER.info("Registro en ruta remota temporal: {}, remoto: {}, archivo: {}", configuracion.getRemoteTemp(), configuracion.getRemotePath(), filename);
                if (StringUtils.isEmpty(configuracion.getRemoteTemp()) || "NULL".equals(configuracion.getRemoteTemp())) {
                    guardarArchivoDeRespuesta(filename, archivoTemporal, configuracion.getRemotePath());
                } else {
                    guardarArchivoDeRespuesta(filename, archivoTemporal, configuracion.getRemoteTemp());
                    operations.moveFiles(configuracion.getRemoteTemp(), configuracion.getRemotePath(), Arrays.asList(filename));
                }
                fat.setEstado(EstadoTransferencia.PRTE);
                guadarRegistro(fat);
                LOGGER.info("Transferencia completa");
                return true;
            } catch (SftpException e) {
                if (++count >= INTENTOS_RECONEXION) {
                    LOGGER.warn("Superamos la cantidad de intentos, mandamos correo de advertencia.", e);
                    LOGGER.error("Ha ocurrido un error al actualizar el archivo de respuesta y el MD5." +
                            " El nombre del archivo de respuesta que debia generarse es '" +
                            filename + "', y su contenido se puede encontrar en el archivo '" +
                            archivoTemporal.getAbsolutePath() + "'. El MD5 calculado del archivo debe ser '" +
                            md5AsHexString + "'. El procesamiento continua, pero se intenta enviar dichos" +
                            " archivos a los encargados de comunicaciones.");
                    LOGGER.error("Error {}", e);
                    fat.setEstado(EstadoTransferencia.ERPR);
                    guadarRegistro(fat);
                    return false;
                } else {
                    LOGGER.warn("Se genero un error, seguiremos reintentando en [" + DELAY + "] milisegundos", e);
                    try {
                        Thread.sleep(DELAY);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
            } catch (Exception ex) {
                if (ok) {
                    LOGGER.error("Ha ocurrido un error muy grave al procesar archivo.", ex);
                    ok = false;
                } else {
                    LOGGER.warn("Ha ocurrido un error al procesar archivo sftp.");
                    LOGGER.debug("El error en transferencia de archivo.", ex);
                }
                return false;
            } finally {
                desconectar();
                LOGGER.info("Desconectando de servidor SFTP");
            }
        }
    }

    public boolean copiarArchivoEntreRutas(String filename, String fromPath, String endPath) throws SftpException {
        if (!operations.isConnected()) {
            try {
                conectar();
                ok = true;
                LOGGER.info("Nos hemos podido conectar nuevamente al servicio de SSH.");
            } catch (Exception ex) {
                LOGGER.warn("La conexion SSH sigue mal", ex);
                return false;
            }
        }
        try {
            operations.moveFiles(fromPath, endPath, Arrays.asList(filename));
            ok = true;
        } catch (Exception ex) {
            if (ok) {
                LOGGER.error("Ha ocurrido un error muy grave al procesar archivo.", ex);
                ok = false;
            } else {
                LOGGER.warn("Ha ocurrido un error al procesar archivo sftp.");
                LOGGER.debug("El error en transferencia de archivo.", ex);
            }
        } finally {
            desconectar();
            LOGGER.debug("Desconectando, esperando 1 min y volviendo a conectar");
        }

        return ok;
    }

    public boolean renombrarArchivo(String fromPath, String filename, String filenameRename) throws SftpException {
        if (!operations.isConnected()) {
            try {
                conectar();
                ok = true;
                LOGGER.info("Nos hemos podido conectar nuevamente al servicio de SSH.");
            } catch (Exception ex) {
                LOGGER.warn("La conexion SSH sigue mal", ex);
                return false;
            }
        }
        try {
            operations.renameFile(fromPath, filename, filenameRename);
            ok = true;
        } catch (Exception ex) {
            if (ok) {
                LOGGER.error("Ha ocurrido un error muy grave al procesar archivo. {}", ex.getMessage());
                ok = false;
            } else {
                LOGGER.warn("Ha ocurrido un error al procesar archivo sftp.");
                LOGGER.debug("El error en transferencia de archivo. {}", ex.getMessage());
            }
        } finally {
            desconectar();
            LOGGER.debug("Desconectando, esperando 1 min y volviendo a conectar");
        }
        return ok;
    }

    @Override
    public void destroy() throws Exception {
        desconectar();
    }


    /**
     * Verificar en base de datos si se permite la descarga o carga de archivo y su posterior procesamiento.
     * Condiciones que permiten el reprocesamiento son:
     * ERPR: Existio un problema en comunicaciones por lo que se realiza el reintento.
     * REPR: Existio un problema interno por lo que se procede a un reintento bajo una solicitud explicita.
     */
    public boolean verificarPermitirTransferencia(String nombreArchivo) {
        return verificarPermitirTransferencia(nombreArchivo, true);
    }

    public boolean verificarPermitirTransferencia(String nombreArchivo, boolean notificar) {
        FtpArchivoTransferencia ftpArchivoTransferencia = null;
        if (fechaProceso.intValue() > 0) {
            ftpArchivoTransferencia = iFtpTransferencia.getFtpTransferencia(nombreArchivo, fechaProceso);
        } else {
            ftpArchivoTransferencia = iFtpTransferencia.getFtpTransferencia(nombreArchivo);
        }

        if (ftpArchivoTransferencia != null) {
            if (ftpArchivoTransferencia.getEstado().equals(EstadoTransferencia.ERPR) ||
                    ftpArchivoTransferencia.getEstado().equals(EstadoTransferencia.REPR)) {
                LOGGER.info("Archivo de nombre {}, en estado {} se vuelve a intentar proceso.", nombreArchivo, ftpArchivoTransferencia.getEstado().getDescripcion());
            } else {
                LOGGER.info("Archivo de nombre {}, en estado {} no se vuelve a intentar proceso.", nombreArchivo, ftpArchivoTransferencia.getEstado().getDescripcion());
                if (notificar) {
                    notificacionesCorreo.notificarSoporte("Error en transferencia de archivo " + nombreArchivo,
                            "Archivo de nombre " + nombreArchivo + ", se encuentra en estado "
                                    + ftpArchivoTransferencia.getEstado().getDescripcion() + " no se vuelve a intentar proceso.");
                    if(tipoArchivo != null) {
                        notificacionesCorreo.notificarErrorTransferenciaOperador(nombreArchivo, tipoArchivo,
                                "Archivo de nombre " + nombreArchivo + ", se encuentra en estado \""
                                        + ftpArchivoTransferencia.getEstado().getDescripcion() + "\" por lo que no se vuelve a intentar proceso.");
                    }
                }
                return false;
            }
        }
        return true;
    }

    /**
     * Actualizar estado ha terminado un regitro de trasnferencia
     **/
    public boolean transferenciaCompleta(String detalle) {
        ftpArchivoTransferencia.setEstado(EstadoTransferencia.PRTE);
        ftpArchivoTransferencia.setDetalle(detalle);
        iFtpTransferencia.guardar(ftpArchivoTransferencia, "ISB");
        return true;
    }

    /**
     * Actualizr estado con error para reintento de una nueva trasnferencia
     **/
    public boolean transferenciaConErrorReintento(String detalle) {
        if (ftpArchivoTransferencia != null) {
            ftpArchivoTransferencia.setEstado(EstadoTransferencia.ERPR);
            ftpArchivoTransferencia.setDetalle(detalle);
            iFtpTransferencia.guardar(ftpArchivoTransferencia, "ISB");
        }
        return true;
    }

    /**
     * Actualizr estado con error interno un regitro de trasnferencia
     **/
    public boolean transferenciaConErrorInterno(String detalle) {
        if (ftpArchivoTransferencia != null) {
            ftpArchivoTransferencia.setEstado(EstadoTransferencia.ERIN);
            ftpArchivoTransferencia.setDetalle(detalle);
            iFtpTransferencia.guardar(ftpArchivoTransferencia, "ISB");
        }
        return true;
    }

    public boolean transferenciaConErrorValidacion(String detalle) {
        if (ftpArchivoTransferencia != null) {
            ftpArchivoTransferencia.setEstado(EstadoTransferencia.ERVA);
            ftpArchivoTransferencia.setDetalle(detalle);
            iFtpTransferencia.guardar(ftpArchivoTransferencia, "ISB");
        }
        return true;
    }


    public FtpArchivoTransferencia crearRegistro(String filename, TipoArchivo tipo) {
        ftpArchivoTransferencia = new FtpArchivoTransferencia();
        ftpArchivoTransferencia.setArchivo(filename);
        ftpArchivoTransferencia.setEstado(EstadoTransferencia.LEAR);
        ftpArchivoTransferencia.setTipo(tipo);
        ftpArchivoTransferencia.setFechaProceso(fechaProceso);
        iFtpTransferencia.guardar(ftpArchivoTransferencia, "ISB");
        return ftpArchivoTransferencia;
    }

    /**
     * Metodo para creacion de archivo de forma local para su posterior transferencia
     *
     * @param nombreArchivo: nombre de archivo
     * @return file: archivo creado
     **/
    public File createFile(String nombreArchivo) {
        File archivo = null;
        try {
            archivo = archivoBase.crearArchivoSimple(nombreArchivo);
            if (!archivo.exists()) {
                if (!archivo.createNewFile()) {
                    LOGGER.warn("Verificar archivo de generado.");
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error: Ocurrio un error al generar el archivo de recaudaciones. ", e);
            archivo = null;
        }
        return archivo;
    }

    public boolean guadarRegistro(FtpArchivoTransferencia fat) {
        return iFtpTransferencia.guardar(fat, "ISB");
    }

    public FtpArchivoTransferencia getFtpArchivoTransferenciaPendiente(TipoArchivo tipo, EstadoTransferencia estado) {
        return iFtpTransferencia.getFtpTransferenciaPendiente(tipo, estado);
    }

    public FtpArchivoTransferencia getFtpArchivoTransferenciaPendiente(TipoArchivo tipo) {
        return iFtpTransferencia.getFtpTransferenciaPendiente(tipo);
    }

    public ArchivoBase getArchivoBase() {
        return archivoBase;
    }

    /**
     * Calculo de archivo md5
     **/
    private String[] calcularMd5Archivo(File archivoLocal) throws NoSuchAlgorithmException, IOException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        FileInputStream calularmd5fis = null;
        try {
            calularmd5fis = new FileInputStream(archivoLocal);
            byte[] buffer = new byte[1024];
            int bytes;
            while ((bytes = calularmd5fis.read(buffer)) != -1) {
                md.update(buffer, 0, bytes);
            }
        } finally {
            IOUtils.closeQuietly(calularmd5fis);
        }
        String md5Original = StringUtils.lowerCase(new String(Hex.encodeHex(md.digest())));

        // OTROS 2 MD5: con y sin retorno de carro.
        md = MessageDigest.getInstance("MD5");
        try {
            calularmd5fis = new FileInputStream(archivoLocal);
            String archivo = IOUtils.toString(calularmd5fis);
            archivo = StringUtils.replace(archivo, "\n", "\r\n");
            md.update(archivo.getBytes());
        } finally {
            IOUtils.closeQuietly(calularmd5fis);
        }
        String md5ConRetornoDeCarro = StringUtils.lowerCase(new String(Hex.encodeHex(md.digest())));

        md = MessageDigest.getInstance("MD5");
        try {
            calularmd5fis = new FileInputStream(archivoLocal);
            String archivo = IOUtils.toString(calularmd5fis);
            archivo = StringUtils.replace(archivo, "\r", "");
            md.update(archivo.getBytes());
        } finally {
            IOUtils.closeQuietly(calularmd5fis);
        }
        String md5SinRetornoDeCarro = StringUtils.lowerCase(new String(Hex.encodeHex(md.digest())));

        LOGGER.debug("MD5s: {} {} {}", new Object[]{md5Original, md5ConRetornoDeCarro, md5SinRetornoDeCarro});
        return new String[]{md5Original, md5ConRetornoDeCarro, md5SinRetornoDeCarro};
    }

    /**
     * Verificar archivo MD5
     **/
    private boolean verificarElMd5sumRemoto(String filename, String[] md5calculado, StringBuilder nuevoMd5) {
        try {
            String nombreArchivomd5 = "MD5SUM-" + UUID.randomUUID().toString();
            File archivomd5 = operations.retrieveFileToFileInLocalWorkDirectory("MD5SUM", nombreArchivomd5);
            LineIterator lineIterator = FileUtils.lineIterator(archivomd5);
            String md5Obtenido = null;
            while (lineIterator.hasNext()) {
                String linea = lineIterator.nextLine();
                if (StringUtils.contains(linea, filename)) {
                    md5Obtenido = StringUtils.lowerCase(linea);
                } else {
                    nuevoMd5.append(linea);
                    nuevoMd5.append('\n');
                }
            }
            if (md5Obtenido == null) {
                throw new IllegalStateException("No se encuentra el registro md5 del archivo " + filename);
            }
            LOGGER.info("Md5: calculado={}, obtenido={}", md5calculado, md5Obtenido);
            for (String md5 : md5calculado) {
                if (md5Obtenido.contains(md5)) {
                    LOGGER.info("MD5 OK");
                    return false;
                }
            }
            throw new IllegalStateException("El md5 calculado no es igual al obtenido calculado=" +
                    Arrays.asList(md5calculado) + " obtenido=" + md5Obtenido);
        } catch (Exception e) {
            LOGGER.error("No se ha podido leer el archivo md5 MD5SUM correspondiente al archivo " + filename +
                    ", ignorando", e);
            return true;
        }
    }

    /**
     * Actualizar el valor en archivo remoto
     */
    private void actualizarMd5DeRespuesta(String digestAsHexString, String nombreArchivoRespuesta, String path)
            throws SftpException, IOException {
        StringBuilder md5respsum = new StringBuilder();
        md5respsum.append(digestAsHexString);
        md5respsum.append("  ");
        md5respsum.append(nombreArchivoRespuesta);
        md5respsum.append('\n');
        File f = null;
        try {
            LOGGER.debug("Intentando traer el archivo MD5SUM del lugar de respuesta");
            operations.changeCurrentDirectory(path);
            f =
                    operations.retrieveFileToFileInLocalWorkDirectory("MD5SUM",
                            "MD5SUMResp" + UUID.randomUUID().toString());
        } catch (Exception e) {
            LOGGER.debug("Error al intentar traer el archivo MD5SUM de la respuesta, creando uno nuevo", e);
            f = new File("MD5SUMResp" + UUID.randomUUID().toString());
        } finally {
            operations.changeCurrentDirectory(parsedUri.getPathDecoded());
        }
        FileWriter md5RespFileWriter = null;
        try {
            LOGGER.debug("Escribiendo el MD5recalculado en un archivo temporal");
            md5RespFileWriter = new FileWriter(f, true);
            md5RespFileWriter.write(md5respsum.toString());
        } finally {
            IOUtils.closeQuietly(md5RespFileWriter);
        }
        FileInputStream actualizarMd5sumRespuesta = null;
        try {
            try {
                operations.changeCurrentDirectory(path);
                actualizarMd5sumRespuesta = new FileInputStream(f);
                operations.storeFileOverTheExistingOne("MD5SUM", actualizarMd5sumRespuesta);
            } finally {
                IOUtils.closeQuietly(actualizarMd5sumRespuesta);
            }
        } finally {
            operations.changeCurrentDirectory(parsedUri.getPathDecoded());
        }
    }

    /**
     * Guardar archivo remotamente
     */
    private void guardarArchivoDeRespuesta(String nombreArchivoRespuesta, File archivoTemporal, String path)
            throws SftpException, FileSystemException {
        LOGGER.debug("Guardando la respuesta en el archivo {}", nombreArchivoRespuesta);
        FileInputStream fis = null;
        try {
            try {
                fis = new FileInputStream(archivoTemporal);
                operations.changeCurrentDirectory(path);
                operations.storeFileOverTheExistingOne(nombreArchivoRespuesta, fis);
            } catch (Exception e) {
                LOGGER.error(
                        "Ha ocurrido un error al intentar escribir el archivo de respuesta. El archivo de respuesta se encuentra en el temporal " +
                                archivoTemporal, e);
            } finally {
                IOUtils.closeQuietly(fis);
            }
        } finally {
            operations.changeCurrentDirectory(parsedUri.getPathDecoded());
        }
        LOGGER.debug("Completado guardado archivo de respuesta.");
    }

    /**
     * Verificar nombre de archivo
     */
    private boolean validarNombre(String regxName, String filename) {
        Pattern pattern = Pattern.compile(regxName, Pattern.CASE_INSENSITIVE);
        return pattern.matcher(filename).find();
    }

    private boolean validarNombreEnTransferencia(String filename) {
        if(filename == null)return true;
        String regxName = ".part";
        return filename.toLowerCase().endsWith(regxName);
    }

    public FtpArchivoTransferencia getFtpArchivoTransferencia() {
        return ftpArchivoTransferencia;
    }

    public void setFtpArchivoTransferencia(FtpArchivoTransferencia ftpArchivoTransferencia) {
        this.ftpArchivoTransferencia = ftpArchivoTransferencia;
    }

    public File getArchivoLocal() {
        return archivoLocal;
    }

    public void setArchivoLocal(File archivoLocal) {
        this.archivoLocal = archivoLocal;
    }

    public String getNombreArchivoRemoto() {
        return nombreArchivoRemoto;
    }

    public Hashtable<String, Date> getFechaArchivoRemoto() {
        return fechaArchivoRemoto;
    }

    public String getNombreArchivoRemotoFecha() {
        StringBuffer sb = new StringBuffer();
        Enumeration keys = fechaArchivoRemoto.keys();
        while(keys.hasMoreElements()){
            String key = (String)keys.nextElement();
            sb.append("\n");
            sb.append(StringUtils.left(FormatosUtils.fechaHoraLargaFormateada(fechaArchivoRemoto.get(key)), 20));
            sb.append(" ");
            sb.append(key);
        }
        sb.append("\n");
        return sb.toString();
    }

    public Integer getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(Integer fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public String eliminarArchivo(String nombreArchivo){
        ok = false;
        StringBuffer respuesta = new StringBuffer();
        if (!operations.isConnected()) {
            try {
                conectar();
                ok = true;
                LOGGER.info("Nos hemos podido conectar nuevamente al servicio de SSH.");
            } catch (Exception ex) {
                LOGGER.warn("La conexion SSH sigue mal", ex);
                return "Error al conectarase a servidor";
            }
        }
        try {
            operations.changeCurrentDirectory(configuracion.getRemotePath());
            operations.deleteFile(nombreArchivo);
            respuesta.append("Completa recuperaci\u00F3n del archivo: ");
            respuesta.append(nombreArchivo);
        } catch (Exception ex) {
            if (ok) {
                LOGGER.error("Ha ocurrido un error muy grave al recuperar el archivo.", ex);
                respuesta.append("Ha ocurrido un error muy grave al recuperar el archivo.");
                ok = false;
            } else {
                LOGGER.warn("Ha ocurrido un error al eliminar sftp.");
                respuesta.append("Ha ocurrido un error al completar recuperaci\u00F3n sftp.");
            }
        } finally {
            desconectar();
            LOGGER.debug("Desconectando, esperando 1 min y volviendo a conectar");
        }

        return respuesta.toString();
    }
}
