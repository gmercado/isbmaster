package com.bisa.bus.servicios.tc.sftp.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.mail.api.MailerFactory;
import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.rpg.ImposibleLlamarProgramaRpgException;
import bus.plumbing.utils.TransferObject;
import bus.plumbing.csv.LecturaCSV;
import com.bisa.bus.servicios.tc.sftp.dao.MovimientoMensualEmisorDao;
import com.bisa.bus.servicios.tc.sftp.entities.IdMovimientoMensualEmisor;
import com.bisa.bus.servicios.tc.sftp.entities.MovimientoMensualEmisor;
import com.bisa.bus.servicios.tc.sftp.model.EstadoPagosTC;
import com.bisa.bus.servicios.tc.sftp.model.MovimientoMensualEmisorCsv;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.bisa.bus.servicios.tc.sftp.model.TotalMovimientosEmisorCsv;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgProgramaTarjetaCredito;
import com.google.inject.Inject;
import com.ibm.as400.access.ConnectionPoolException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Created by atenorio on 23/06/2017.
 */
public class SftpRecepcionMovimientoMensualEmisor {

    private static final Logger LOGGER = LoggerFactory.getLogger(SftpRecepcionMovimientoMensualEmisor.class);

    private final MedioAmbiente medioAmbiente;
    private final MovimientoMensualEmisorDao movimientoMensualEmisorDao;

    private final RpgProgramaTarjetaCredito rpgPrograma;
    private final MailerFactory mailerFactory;
    SftpConfiguracion configuracion;
    SftpTransferencia sftpTransferencia;

    @Inject
    public SftpRecepcionMovimientoMensualEmisor(MedioAmbiente medioAmbiente,
                                          MovimientoMensualEmisorDao movimientoMensualEmisorDao,
                                          MailerFactory mailerFactory, RpgProgramaTarjetaCredito rpgPrograma,
                                          SftpConfiguracion configuracion, SftpTransferencia sftpTransferencia) {
        this.medioAmbiente = medioAmbiente;
        this.movimientoMensualEmisorDao = movimientoMensualEmisorDao;
        this.rpgPrograma = rpgPrograma;
        this.mailerFactory = mailerFactory;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para procesar archivo de movimiento mensual emisor 111 enviado por ATC en SFTP remoto.
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    public boolean procesarArchivo() {

        boolean ok = true;
        LOGGER.debug("Inicia proceso de envio de archivo de solicitud");
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_MOV_MENSUAL_EMISOR_111, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_MOV_MENSUAL_EMISOR_111_DEFAULT);
        LOGGER.info("Descargar archivo con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        LOGGER.debug("1) Descarga de archivo de a servidor SFTP");
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_MOVIMIENTOS_MENSUAL_EMISOR_ATC, Variables.FTP_RUTA_RECEPCION_MOVIMIENTOS_MENSUAL_EMISOR_ATC_DEFAULT));
        sftpTransferencia.configurar(configuracion);

        ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.CIE);
        LOGGER.info("Descarga completa:" + ok);

        if (!ok || sftpTransferencia.getArchivoLocal() == null) {
            // Cerrar el registro con error y permitir reintento
            LOGGER.warn("Problema en transferencia o no existen archivos de respuesta en servidor SFTP.");
            return false;
        }

        // Procesamiento de archivo local
        File archivo = sftpTransferencia.getArchivoLocal();
        TotalMovimientosEmisorCsv total = mapearArchivo(archivo);

        if (total == null || total.getMovimientos() == null || total.getMovimientos().size() == 0) {
            LOGGER.warn("Error en mapeo de archivo de respuesta o archivo vac\u00EDo");
            return false;
        }
        for (MovimientoMensualEmisorCsv dg : total.getMovimientos()) {
            // Validar que se realizo el mapeo de forma exitosa de la estructura de pago
            if (dg == null) {
                LOGGER.error("Error en mapeo de una estructura se requiere revisar el archivo de respuesta");
                return false;
            }
        }

        LOGGER.debug("4) Actualizar tabla de datos generales");
        LOGGER.debug("4.1) Verificar si un registro se encuentra en un estado inconsistente");
        int totalRegistros = 1;
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();



        // Verificar consistencia de registro
        for (MovimientoMensualEmisorCsv movimiento: total.getMovimientos()) {
            IdMovimientoMensualEmisor id = new IdMovimientoMensualEmisor();
            id.setNumeroCuenta(movimiento.getNumeroCuenta());
            id.setNumeroComprobante(movimiento.getNumeroComprobante());
            id.setFechaProceso(total.getFechaProceso());
            id.setCodigoAdm('A');

            // Validar que se tenga pendiente de respuesta a pagos
            MovimientoMensualEmisor movimientoMensual = transferObject.convert(movimiento, MovimientoMensualEmisor.class);
            movimientoMensual.setEstado(EstadoPagosTC.GENE.name());
            movimientoMensual.setImporteMonedaOrigenSigno(getSigno(movimientoMensual.getImporteMonedaOrigen()));
            movimientoMensual.setImporteMonedaOrigenTexto(getNumero(movimientoMensual.getImporteMonedaOrigen(), 10));
            movimientoMensual.setImporteEfectivoSigno(getSigno(movimientoMensual.getImporteEfectivo()));
            movimientoMensual.setImporteEfectivoTexto(getNumero(movimientoMensual.getImporteEfectivo(), 10));
            movimientoMensual.setImporteMonedaBalanceSigno(getSigno(movimientoMensual.getImporteMonedaBalance()));
            movimientoMensual.setImporteMonedaBalanceTexto(getNumero(movimientoMensual.getImporteMonedaBalance(), 10));
            movimientoMensual.setImporteMonedaLiquidacionSigno(getSigno(movimientoMensual.getImporteMonedaLiquidacion()));
            movimientoMensual.setImporteMonedaLiquidacionTexto(getNumero(movimientoMensual.getImporteMonedaLiquidacion(), 10));
            movimientoMensual.setMontoAdicionalSigno(getSigno(movimientoMensual.getMontoAdicional()));
            movimientoMensual.setMontoAdicionalTexto(getNumero(movimientoMensual.getMontoAdicional(), 10));


            movimientoMensual.setFechaRecepcion(new Date());
            movimientoMensual.setIdArchivoRecepcion(sftpTransferencia.getFtpArchivoTransferencia().getId());

            movimientoMensual.setFechaCreacion(new Date());
            movimientoMensual.setUsuarioCreador("");
            movimientoMensual.setFechaModificacion(new Date());
            movimientoMensual.setUsuarioModificador("");

            // Registrar pago
            movimientoMensual.setIdMovimientoMensualEmisor(id);
            ok = ok && movimientoMensualEmisorDao.guardar(movimientoMensual, "ISB");

//            SaldoMensualHistorico historico = transferObject.convert(saldoMensual, SaldoMensualHistorico.class);
//            historico.setIdSaldoMensual(saldoMensual.getIdSaldoMensual());
//            ok = ok && saldoMensualHistoricoDao.guardar(historico, "ISB");
        }
        if (ok) {
            sftpTransferencia.transferenciaCompleta("");
        } else {
            sftpTransferencia.transferenciaConErrorInterno("");
        }
        LOGGER.debug("5) Llamar a programa para procesamiento de saldo mensual");
        try {
            ok = true; //ejecucionRpg();
        } catch (Exception e) {
            LOGGER.error("Error al llamar programa AS400, procesamiento de saldo mensual. {}", e);
            sftpTransferencia.transferenciaConErrorInterno("");
            return false;
        }
        LOGGER.debug("4.2) Completado proceso de carga de archivo {} de saldo mensual 079 SFTP de ATC correctamente.", sftpTransferencia.getNombreArchivoRemoto());
        return ok;
    }

    public TotalMovimientosEmisorCsv mapearArchivo(File archivo) {
        LOGGER.info("Ruta local de archivo a procesar:{}", archivo.getAbsolutePath());
        FileInputStream is = null;
        BufferedReader br = null;
        TotalMovimientosEmisorCsv total = null;
        List<MovimientoMensualEmisorCsv> lista = new ArrayList<MovimientoMensualEmisorCsv>();
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();
        try {
            is = new FileInputStream(archivo);
            br = new BufferedReader(new InputStreamReader(is, "ISO-8859-1"));
            String linea = br.readLine();
            List<String> lineas = new ArrayList<String>();

            while (linea != null) {
                LOGGER.debug("Detalle de registro recuperado: {}", linea);
                if (StringUtils.isNotEmpty(linea) && linea.length() > 1) {
                    MovimientoMensualEmisorCsv dg = LecturaCSV.procesar(MovimientoMensualEmisorCsv.class, linea, "|");
                    if(dg==null){
                        total = LecturaCSV.procesar(TotalMovimientosEmisorCsv.class, linea, "|");
                    }else {
                        lista.add(dg);
                    }
                }
                linea = br.readLine();
            }
//            fat.setEstado(EstadoTransferencia.PROC);
//            guadarRegistro(fat);
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al procesar el archivo "
                    + archivo.getName() + ".", e);
//            fat.setEstado(EstadoTransferencia.ERPR);
//            guadarRegistro(fat);
            return null;
        } finally {
            IOUtils.closeQuietly(is);
        }
        LOGGER.info("Lista de registros:::{}", total.getFechaProceso());
        LOGGER.info("Lista de registros:::{}", total.getCantidad());
        if(total != null)total.setMovimientos(lista);
        return total;
    }

    public Character getSigno(BigDecimal numero) {
        if (numero == null) return new Character(' ');
        return (numero.doubleValue() < 0) ? new Character('-') : new Character('+');
    }

    public String getNumero(BigDecimal numero, int size) {
        if(numero ==null)numero = new BigDecimal(0);
        DecimalFormat decimalFormat = new DecimalFormat("##.00");
        String valor = decimalFormat.format(numero);
        valor = valor.replaceAll("\\.", "");
        valor = valor.replace(",", "");
        valor = valor.replace("-", "");
        valor = StringUtils.leftPad(valor, size, "0");
        return valor;
    }

    public boolean ejecucionRpg() throws IllegalArgumentException, IllegalStateException {
        boolean result = false;
        String libreriaPrograma = medioAmbiente.getValorDe(Variables.LIBRERIA_TC_ATC, Variables.LIBRERIA_TC_ATC_DEFAULT);
        String programa = medioAmbiente.getValorDe(Variables.PROGRAMA_TC_DG_ATC, Variables.PROGRAMA_TC_DG_ATC_DEFAULT);
        try {
            result = rpgPrograma.ejecutar(libreriaPrograma, programa);
            return result;
        } catch (ImposibleLeerRespuestaException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(Imposible leer respuesta)");
        } catch (IOException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(IOException)");
        } catch (TransaccionEfectivaException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(TransaccionEfectivaException)");
        } catch (SistemaCerradoException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(SistemaCerradoException)");
        } catch (ImposibleLlamarProgramaRpgException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(ImposibleLlamarProgramaRpgException)");
        } catch (ConnectionPoolException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(ConnectionPoolException)");
        } catch (TimeoutException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(Tiempo de espera agotado)");
        }
    }
}

