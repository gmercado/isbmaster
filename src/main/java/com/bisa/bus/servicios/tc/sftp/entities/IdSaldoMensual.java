package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by atenorio on 22/06/2017.
 */
@Embeddable
public class IdSaldoMensual implements Serializable {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "SMSUCEM", nullable = false, length = 4)
    private String codigoSucursal;

    @Basic(optional = false)
    @Column(name = "SMNUTCNC", nullable = false, length = 10)
    private String numeroCuenta;

    @Basic(optional = false)
    @Column(name = "SMESTCTA", nullable = false, length = 2)
    private String estadoCuenta;


    public String getCodigoSucursal() {
        return codigoSucursal;
    }

    public void setCodigoSucursal(String codigoSucursal) {
        this.codigoSucursal = codigoSucursal;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getEstadoCuenta() {
        return estadoCuenta;
    }

    public void setEstadoCuenta(String estadoCuenta) {
        this.estadoCuenta = estadoCuenta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdSaldoMensual that = (IdSaldoMensual) o;

        if (!getCodigoSucursal().equals(that.getCodigoSucursal())) return false;
        if (!getNumeroCuenta().equals(that.getNumeroCuenta())) return false;
        return getEstadoCuenta().equals(that.getEstadoCuenta());
    }

    @Override
    public int hashCode() {
        int result = getCodigoSucursal().hashCode();
        result = 31 * result + getNumeroCuenta().hashCode();
        result = 31 * result + getEstadoCuenta().hashCode();
        return result;
    }


}
