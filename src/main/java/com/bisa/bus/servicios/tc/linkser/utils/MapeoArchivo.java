package com.bisa.bus.servicios.tc.linkser.utils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by atenorio on 16/11/2017.
 */
public class MapeoArchivo {
    private static final Logger LOGGER = LoggerFactory.getLogger(MapeoArchivo.class);

    static public List<String> mapearArchivo(File archivo) {
        List<String> respuesta = new ArrayList<String>();
        FileInputStream is = null;
        BufferedReader br = null;
        try {
            is = new FileInputStream(archivo);
            br = new BufferedReader(new InputStreamReader(is));
            String linea = br.readLine();
            while (linea != null) {
                if (StringUtils.isNotEmpty(linea) && linea.length() > 10) {
                    respuesta.add(linea);
                }
                linea = br.readLine();
            }
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al procesar el archivo "
                    + archivo.getName() + ".", e);
            return new ArrayList<String>();
        } finally {
            IOUtils.closeQuietly(is);
        }
        return respuesta;
    }

    public static void main(String[] args) {
        SimpleDateFormat formato =new SimpleDateFormat("MMMMyyyy' de ' MM", new Locale("es", "ES"));
                //new SimpleDateFormat("EEEE d 'de' MMMM 'de' yyyy", new Locale("es", "ES"));
        String fecha = formato.format(new Date());
        System.out.println("fecha:"+fecha+":::"+StringUtils.capitalize(fecha));

    }
}
