package com.bisa.bus.servicios.tc.sftp.model;

/**
 * Created by atenorio on 04/07/2017.
 */
public enum TipoCodigoError {
    OK("000","Correcto"),
    ERR_DB("101","Error con base de datos"),
    ERR_ORIGEN("901", "El origen de la solicitud no es valido"),
    ERR_FORMATO("902", "El formato de entrada no es valido"),
    ERR_TIPODOC("903", "El tipo documento no es valido"),
    SERV_NO_HAB("904", "Servidor remoto no habilitado para su conexion"),
    ERR_PROGRAMA("905", "Error al llamar programa AS400"),
    ERR_SIN_REG("906", "Sin registros que enviar"),
    ERR_INESPERADO("999", "Error inesperado");

    private String codigo;
    private String descripcion;

    TipoCodigoError(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
