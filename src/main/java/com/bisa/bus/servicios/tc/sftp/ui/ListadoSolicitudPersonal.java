package com.bisa.bus.servicios.tc.sftp.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;
import org.apache.wicket.markup.repeater.Item;
import com.bisa.bus.servicios.tc.sftp.dao.SolicitudTCDao;
import com.bisa.bus.servicios.tc.sftp.entities.SolicitudTC;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by atenorio on 01/12/2017.
 */
@AuthorizeInstantiation({RolesBisa.FTP_TRANSFERENCIA})
@Menu(value = "Env\u00EDo de Solicitudes Personal", subMenu = SubMenu.FTP)
public class ListadoSolicitudPersonal extends Listado<SolicitudTC, Integer> {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ListadoSolicitudPersonal.class);

    public ListadoSolicitudPersonal() {
        super();
    }

    @Override
    protected ListadoPanel<SolicitudTC, Integer> newListadoPanel(String id) {
        return new ListadoPanel<SolicitudTC, Integer>(id) {
            @Override
            protected List<IColumn<SolicitudTC, String>> newColumns(ListadoDataProvider<SolicitudTC, Integer> dataProvider) {
                List<IColumn<SolicitudTC, String>> columns = new LinkedList<>();
                columns.add(new LinkPropertyColumn<SolicitudTC>(Model.of("Solicitud"), "sonsol", "sonsol") {
                    @Override
                    protected void onClick(SolicitudTC object) {
                        PageParameters params  = new PageParameters();
                        params.add("solicitud", object.getSonsol());
                        setResponsePage(DetalleSolicitudPersonal.class, params);
                    }
                });

                columns.add(new PropertyColumn<>(Model.of("Nombre Cliente"), "socfnocl", "socfnocl"));

                columns.add(new PropertyColumn<>(Model.of("Apellido Cliente"), "socfapcl", "socfapcl"));

                columns.add(new PropertyColumn<>(Model.of("Doc Cliente"), "socfdocl", "socfdocl"));

                columns.add(new PropertyColumn<>(Model.of("Cta. Tar."), "soganutcnc", "soganutcnc"));

                columns.add(new PropertyColumn<>(Model.of("Cta. Tar. Banco"), "sonutc", "sonutc"));

                columns.add(new PropertyColumn<>(Model.of("Fecha env\u00EDo"), "sofeenv", "fechaEnvioFormato"));

                columns.add(new PropertyColumn<>(Model.of("Fecha recepci\u00F3n"), "soferes", "fechaRecepcionFormato"));

                columns.add(new PropertyColumn<SolicitudTC, String>(Model.of("Estado"), "soest", "estadoDescripcion") {
                    @Override
                    public void populateItem(Item<ICellPopulator<SolicitudTC>> item, String componentId, IModel<SolicitudTC> rowModel) {
                        SolicitudTC solicitud = rowModel.getObject();
                        item.add(new Label(componentId, solicitud.getEstadoDescripcion()).add(new AttributeAppender("style", Model.of("text-align:center"))));
                    }
                });

                columns.add(new PropertyColumn<>(Model.of("Fecha Proceso"), "fechaCreacion", "fechaProcesoFormato"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<SolicitudTC, Integer>> getProviderClazz() {
                return SolicitudTCDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<SolicitudTC> newFiltroPanel(String id, IModel<SolicitudTC> model1, IModel<SolicitudTC> model2) {
                return new ListaSolicitudFiltroPanel(id, model1, model2);
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("fechaCreacion", false);
            }

            @Override
            protected SolicitudTC newObject2() {
                return new SolicitudTC();
            }

            @Override
            protected SolicitudTC newObject1() {
                return new SolicitudTC();
            }

            @Override
            protected boolean isVisibleData() {
                SolicitudTC pago1 = getModel1().getObject();
                SolicitudTC pago2 = getModel2().getObject();
                return !((pago1.getFechaCreacion() == null || pago2.getFechaCreacion() == null));
            }
        };
    }
}