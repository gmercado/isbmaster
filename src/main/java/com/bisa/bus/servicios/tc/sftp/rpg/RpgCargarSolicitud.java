package com.bisa.bus.servicios.tc.sftp.rpg;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.as400.AS400Factory;
import bus.monitor.rpg.CallProgramRPGv2;
import bus.monitor.rpg.ImposibleLlamarProgramaRpgException;
import bus.monitor.rpg.ParametrosRPG;
import com.google.inject.Inject;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ConnectionPoolException;
import com.ibm.as400.access.ProgramParameter;
import com.ibm.as400.access.QSYSObjectPathName;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by atenorio on 19/05/2017.
 */
public class RpgCargarSolicitud implements IRpgCargarSolicitud {

    private static final Logger LOGGER = LoggerFactory.getLogger(RpgCargarSolicitud.class);

    private final CallProgramRPGv2 callProgramRPGv2;
    private final MedioAmbiente medioAmbiente;
    private final AS400Factory as400Factory;

    @Inject
    public RpgCargarSolicitud(AS400Factory as400factory, MedioAmbiente medioAmbiente,
                              CallProgramRPGv2 callProgramRPGv2) throws ConnectionPoolException, ImposibleLlamarProgramaRpgException {
        this.as400Factory = as400factory;
        this.callProgramRPGv2 = callProgramRPGv2;
        this.medioAmbiente = medioAmbiente;
    }


    public boolean ejecutar() throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException, TimeoutException {
        String libreriaPrograma = medioAmbiente.getValorDe(Variables.LIBRERIA_TC_ATC, Variables.LIBRERIA_TC_ATC_DEFAULT);
        String rutaPrograma = new QSYSObjectPathName(libreriaPrograma,
                medioAmbiente.getValorDe(Variables.PROGRAMA_TC_SOLICITUD_ATC, Variables.PROGRAMA_TC_SOLICITUD_ATC_DEFAULT), "PGM").getPath();
        return ejecutar(rutaPrograma);
    }

    public boolean ejecutar(String libreriaPrograma, String programa) throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException, TimeoutException {
        String rutaPrograma = new QSYSObjectPathName(libreriaPrograma, programa, "PGM").getPath();
        return ejecutar(rutaPrograma);
    }

    public boolean ejecutar(String rutaPrograma) throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException, TimeoutException {
        boolean resultado = false;
        ParametrosRPG parametrosRPG = new ParametrosRPG() {
            @Override
            public String getFullPathRPG() {
                return rutaPrograma;
            }

            @Override
            public ProgramParameter[] loadParameters() {
                // Vector de parametros
                ProgramParameter[] parametros = new ProgramParameter[2];
                // Conversion de parametros de entrada
                AS400Text paramconveter40 = new AS400Text(5, as400Factory.getCcsid());

                ProgramParameter parm0 = new ProgramParameter(paramconveter40.toBytes("ISB")); // Parametro de ingreso
                ProgramParameter parm1 = new ProgramParameter(1);   // Parametro de salida

                parametros[0] = parm0;
                parametros[1] = parm1;
                return parametros;
            }

        };

        // Invocando al programa RPG de AS400
        try {
            callProgramRPGv2.ejecutar(parametrosRPG);
        } catch (TimeoutException e) {
            LOGGER.warn("Timeout al llamar a carga de solicitudes Tarjeta de Credito para ATC");
            throw e;
        }

        // Respuesta del programa
        // Evaluando la respuesta del programa
        String valor1 = StringUtils.trimToEmpty(parametrosRPG.obtenerParametro(as400Factory, 1));
        if (StringUtils.trimToNull(valor1) != null) {
            LOGGER.info("Respuesta RPG: {}", valor1);
            if ("1".equalsIgnoreCase(valor1)) {
                resultado = true;
            } else {
                LOGGER.warn("La respuesta de la llamada a carga de solicitudes de tarjeta de credito: {}.", valor1);
            }

        } else {
            LOGGER.warn("Sin respuesta de la llamada a carga de solicitudes de tarjeta de credito.");
        }
        return resultado;
    }
}
