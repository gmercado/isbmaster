package com.bisa.bus.servicios.tc.linkser.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 14/11/2017.
 */
@Entity
@Table(name = "TCPINICRC")
public class RespuestaSolicitudCuentaLinkser implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "SADATC", nullable = false)
    private String texto;

    public RespuestaSolicitudCuentaLinkser(String texto){
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
