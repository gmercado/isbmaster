package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.ExtractoMensual;
import com.bisa.bus.servicios.tc.sftp.entities.ExtractoMensualHistorico;
import com.bisa.bus.servicios.tc.sftp.entities.IdExtractoMensual;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 02/08/2017.
 */
public class ExtractoMensualHistoricoDao extends DaoImpl<ExtractoMensualHistorico, IdExtractoMensual> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExtractoMensualHistoricoDao.class);

    @Inject
    protected ExtractoMensualHistoricoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(ExtractoMensualHistorico registro, String user) {
        registro.setUsuarioCreador(user);
        registro.setFechaCreacion(new Date());
        persist(registro);
        return true;
    }


    public boolean limpiarTodo() {
        LOGGER.debug("Limpiando tabla Extracto Mensual Historico.");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<ExtractoMensualHistorico> query = entityManager.createQuery("DELETE FROM ExtractoMensualHistorico");
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }
}
