package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by atenorio on 08/09/2017.
 */
public class SolicitudCompletoEmpresarial implements Serializable {
    private static final long serialVersionUID = 1L;

    @FormatCsv(name = "SOGATIRE", nullable = false, length = 2)
    private String sogatire;

    @FormatCsv(name = "SOGAGRPR", nullable = false, length = 9, fillSpace = "0")
    private int sogagrpr;

    @FormatCsv(name = "SOGAPRCU", nullable = false, length = 1)
    private short sogaprcu;

    @FormatCsv(name = "SOGATICU", nullable = false, length = 1)
    private short sogaticu;

    @FormatCsv(name = "SOGACOEM", nullable = false, length = 4, fillSpace = "0")
    private String sogacoem;

    @FormatCsv(name = "SOGASUCU1", nullable = false, length = 9, align = FormatCsv.alignType.LEFT)
    private String sogasucu1;

    @FormatCsv(name = "SOGAAGEM", nullable = false, length = 5)
    private String sogaagem;

    @FormatCsv(name = "SOGAINEC", nullable = false, length = 1)
    private short sogainec;

    @FormatCsv(name = "SOGAINECM", nullable = false, length = 1)
    private short sogainecm;

    @FormatCsv(name = "SOGAEMAIL", nullable = false, length = 60)
    private String sogaemail;

    @FormatCsv(name = "SOGAINSE", nullable = false, length = 1)
    private short sogainse;

    @FormatCsv(name = "SOGAFILL", nullable = false, length = 283)
    private String sogafill = "";

    @FormatCsv(name = "SOGANUTCNC", nullable = false, length = 20)
    private BigInteger soganutcnc;

    @FormatCsv(name = "SOGACOER", nullable = false, length = 3)
    private String sogacoer;

    @FormatCsv(name = "SOCCTIRE", nullable = false, length = 2)
    private String socctire;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation

    @FormatCsv(name = "SOCCLICR", nullable = false, pattern = "##.000000", scale = 6, length = 18, fillSpace = "0")
    //precision = 18, scale = 6
    private BigDecimal socclicr;

    @FormatCsv(name = "SOCCMOLI", nullable = false, length = 4, fillSpace = "0")
    private short soccmoli;

    @FormatCsv(name = "SOCCFECI", nullable = false, length = 4, fillSpace = "0")
    private short soccfeci;

    @FormatCsv(name = "SOCCFOPA", nullable = false, length = 1)
    private short soccfopa;

    @FormatCsv(name = "SOCCTIDE", nullable = false, length = 1)
    private short socctide;

    @FormatCsv(name = "FILL", nullable = false, length = 367)
    private String soccfill = "";

    @FormatCsv(name = "SOCCCOER", nullable = false, length = 3)
    private String socccoer;

    //CJ Cliente juridico
    @FormatCsv(name = "SOCJTIRE", nullable = false, length = 2)
    private String socjtire;

    @FormatCsv(name = "SOCJRASO", nullable = false, length = 60)
    private String socjraso;

    @FormatCsv(name = "SOCJNOCO", nullable = false, length = 60)
    private String socjnoco;

    @FormatCsv(name = "SOCJTIDO", nullable = false, length = 4)
    private short socjtido;

    @FormatCsv(name = "SOCJDOEM", nullable = false, length = 25)
    private String socjdoem;

    @FormatCsv(name = "SOCJFEIA", nullable = false, length = 8)
    private String socjfeia;

    @FormatCsv(name = "SOCJPARE", nullable = false, length = 3)
    private short socjpare;

    @FormatCsv(name = "SOCJFEFE", nullable = false, length = 8)
    private String socjfefe;

    @FormatCsv(name = "SOCJPAFE", nullable = false, length = 3)
    private short socjpafe;

    @FormatCsv(name = "SOCJACEC", nullable = false, length = 4)
    private short socjacec;

    @FormatCsv(name = "SOCJTISO", nullable = false, length = 4)
    private String socjtiso;

    @FormatCsv(name = "SOCJALEM", nullable = false, length = 1)
    private short socjalem;

    @FormatCsv(name = "SOCJCAEM", nullable = false, length = 10)
    private String socjcaem;

    @FormatCsv(name = "SOCJMOFA", nullable = false, length = 3)
    private String socjmofa;

    @FormatCsv(name = "SOCJVAFA", nullable = false, pattern = "##.00", scale = 2, length = 12, fillSpace = "0")
    //precision = 12, scale = 2)
    private BigDecimal socjvafa;

    @FormatCsv(name = "SOCJMOPA", nullable = false, length = 3)
    private short socjmopa;

    @FormatCsv(name = "SOCJVAPA", nullable = false, pattern = "##.00", scale = 2, length = 12, fillSpace = "0")
    //precision = 12, scale = 2)
    private BigDecimal socjvapa;

    @FormatCsv(name = "SOCJDCPA", nullable = false, length = 1)
    private short socjdcpa;

    @FormatCsv(name = "FILL", nullable = false, length = 174)
    private String socjfill = "";

    @FormatCsv(name = "SOCJCOER", nullable = false, length = 3)
    private String socjcoer;

    // AD direccion fisica de empresa
    @FormatCsv(name = "SOADTIREL", nullable = false, length = 2)
    private String soadtirel;

    @FormatCsv(name = "SOADTIDICL", nullable = false, length = 1)
    private short soadtidicl;

    @FormatCsv(name = "SOADTICAL", nullable = false, length = 4)
    private short soadtical;

    @FormatCsv(name = "SOADCALLEL", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadcallel;

    @FormatCsv(name = "SOADNUPUL", nullable = false, length = 10, align = FormatCsv.alignType.LEFT)
    private String soadnupul;

    @FormatCsv(name = "SOADINADL", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadinadl;

    @FormatCsv(name = "SOADCOPOL", nullable = false, length = 6)
    private String soadcopol;

    @FormatCsv(name = "SOADZONAL", nullable = false, length = 60, align = FormatCsv.alignType.LEFT)
    private String soadzonal;

    @FormatCsv(name = "SOADPAISL", nullable = false, length = 3, fillSpace = "0")
    private short soadpaisl;

    @FormatCsv(name = "SOADDPTOL", nullable = false, length = 5, align = FormatCsv.alignType.LEFT)
    private String soaddptol;

    @FormatCsv(name = "SOADLOCL", nullable = false, length = 9, fillSpace = "0")
    private int soadlocl;

    @FormatCsv(name = "SOADCOTEPL", nullable = false, length = 3, fillSpace = "0")
    private short soadcotepl;

    @FormatCsv(name = "SOADCOTEAL", nullable = false, length = 5, fillSpace = "0")
    private int soadcoteal;

    @FormatCsv(name = "SOADTELL", nullable = false, length = 12, fillSpace = "0")
    private long soadtell;

    @FormatCsv(name = "SOADTEINL", nullable = false, length = 6, fillSpace = "0")
    private int soadteinl;

    @FormatCsv(name = "SOADCOCEPL", nullable = false, length = 3, fillSpace = "0")
    private short soadcocepl;

    @FormatCsv(name = "SOADCELL", nullable = false, length = 12, fillSpace = "0")
    private long soadcell;

    @FormatCsv(name = "SOADFILL", nullable = false, length = 56)
    private String soadfilll;

    @FormatCsv(name = "SOADCOERL", nullable = false, length = 3)
    private String soadcoerl;

    @FormatCsv(name = "SOADTIREM", nullable = false, length = 2)
    private String soadtirem;

    @FormatCsv(name = "SOADTIDICM", nullable = false, length = 1)
    private short soadtidicm;

    @FormatCsv(name = "SOADTICAM", nullable = false, length = 4)
    private short soadticam;

    @FormatCsv(name = "SOADCALLEM", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadcallem;

    @FormatCsv(name = "SOADNUPUM", nullable = false, length = 10, align = FormatCsv.alignType.LEFT)
    private String soadnupum;

    @FormatCsv(name = "SOADINADM", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadinadm;

    @FormatCsv(name = "SOADCOPOM", nullable = false, length = 6)
    private String soadcopom;

    @FormatCsv(name = "SOADZONAM", nullable = false, length = 60, align = FormatCsv.alignType.LEFT)
    private String soadzonam;

    @FormatCsv(name = "SOADPAISM", nullable = false, length = 3, fillSpace = "0")
    private short soadpaism;

    @FormatCsv(name = "SOADDPTOM", nullable = false, length = 5, align = FormatCsv.alignType.LEFT)
    private String soaddptom;

    @FormatCsv(name = "SOADLOCM", nullable = false, length = 9, fillSpace = "0")
    private int soadlocm;

    @FormatCsv(name = "SOADCOTEPM", nullable = false, length = 3, fillSpace = "0")
    private short soadcotepm;

    @FormatCsv(name = "SOADCOTEAM", nullable = false, length = 5, fillSpace = "0")
    private int soadcoteam;

    @FormatCsv(name = "SOADTELM", nullable = false, length = 12, fillSpace = "0")
    private long soadtelm;

    @FormatCsv(name = "SOADTEINM", nullable = false, length = 6, fillSpace = "0")
    private int soadteinm;

    @FormatCsv(name = "SOADCOCEPM", nullable = false, length = 3, fillSpace = "0")
    private short soadcocepm;

    @FormatCsv(name = "SOADCELM", nullable = false, length = 12, fillSpace = "0")
    private long soadcelm;

    @FormatCsv(name = "SOADFILL", nullable = false, length = 56)
    private String soadfillm;

    @FormatCsv(name = "SOADCOERM", nullable = false, length = 3)
    private String soadcoerm;

    @FormatCsv(name = "SOCFTIRE", nullable = false, length = 2)
    private String socftire;

    @FormatCsv(name = "SOCFTICL", nullable = false, length = 1)
    private short socfticl;

    @FormatCsv(name = "SOCFNOCL", nullable = false, length = 60)
    private String socfnocl;

    @FormatCsv(name = "SOCFAPCL", nullable = false, length = 60)
    private String socfapcl;

    @FormatCsv(name = "SOCFTIDO", nullable = false, length = 4)
    private short socftido;

    @FormatCsv(name = "SOCFDOCL", nullable = false, length = 25)
    private String socfdocl;

    @FormatCsv(name = "SOCFTIDOA", nullable = false, length = 4)
    private short socftidoa;

    @FormatCsv(name = "SOCFDOCLA", nullable = false, length = 25)
    private String socfdocla;

    @FormatCsv(name = "SOCFFENA", nullable = false, length = 8)
    private String socffena;

    @FormatCsv(name = "SOCFESCI", nullable = false, length = 1, fillSpace = "0")
    private short socfesci;

    @FormatCsv(name = "SOCFGECL", nullable = false, length = 1, fillSpace = "0")
    private short socfgecl;

    @FormatCsv(name = "SOCFPACL", nullable = false, length = 3, fillSpace = "0")
    private short socfpacl;

    @FormatCsv(name = "SOCFLUNA", nullable = false, length = 40)
    private String socfluna;

    @FormatCsv(name = "SOCFCAHI", nullable = false, length = 2)
    private short socfcahi;

    @FormatCsv(name = "SOCFNOEM", nullable = false, length = 25)
    private String socfnoem;

    @FormatCsv(name = "SOCFEMAIL", nullable = false, length = 60)
    private String socfemail;

    @FormatCsv(name = "SOCFDCPA", nullable = false, length = 1)
    private short socfdcpa;

    @FormatCsv(name = "SOCFDFPA", nullable = false, length = 1)
    private short socfdfpa;

    @FormatCsv(name = "SOCFNUSOL", nullable = false, length = 9)
    private int socfnusol;
    @FormatCsv(name = "FILL", nullable = false, length = 65)
    private String socffill = "";

    @FormatCsv(name = "SOCFCOER", nullable = false, length = 3)
    private String socfcoer;
    // EI informacion de empleo
    @FormatCsv(name = "SOEITIRE", nullable = false, length = 2)
    private String soeitire;

    @FormatCsv(name = "FILL", nullable = false, length = 395)
    private String soeifill = "";

    @FormatCsv(name = "SOEICOER", nullable = false, length = 3)
    private String soeicoer;
    // AD Direccion de empleado    
    @FormatCsv(name = "SOADTIREP", nullable = false, length = 2)
    private String soadtirep;

    @FormatCsv(name = "SOADTIDICP", nullable = false, length = 1)
    private short soadtidicp;
    // TODO ATC ejemplo con codigo de calle en blanco
    @FormatCsv(name = "SOADTICAP", nullable = false, length = 4)
    private short soadticap;

    @FormatCsv(name = "SOADCALLEP", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadcallep;

    @FormatCsv(name = "SOADNUPUP", nullable = false, length = 10, align = FormatCsv.alignType.LEFT)
    private String soadnupup;

    @FormatCsv(name = "SOADINADP", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadinadp;

    @FormatCsv(name = "SOADCOPOP", nullable = false, length = 6)
    private String soadcopop;

    @FormatCsv(name = "SOADZONAP", nullable = false, length = 60, align = FormatCsv.alignType.LEFT)
    private String soadzonap;

    @FormatCsv(name = "SOADPAISP", nullable = false, length = 3, fillSpace = "0")
    private short soadpaisp;

    @FormatCsv(name = "SOADDPTOP", nullable = false, length = 5, align = FormatCsv.alignType.LEFT)
    private String soaddptop;

    @FormatCsv(name = "SOADLOCP", nullable = false, length = 9, fillSpace = "0")
    private int soadlocp;

    @FormatCsv(name = "SOADCOTEPP", nullable = false, length = 3, fillSpace = "0")
    private short soadcotepp;

    @FormatCsv(name = "SOADCOTEAP", nullable = false, length = 5, fillSpace = "0")
    private int soadcoteap;

    @FormatCsv(name = "SOADTELP", nullable = false, length = 12, fillSpace = "0")
    private long soadtelp;

    @FormatCsv(name = "SOADTEINP", nullable = false, length = 6, fillSpace = "0")
    private int soadteinp;

    @FormatCsv(name = "SOADCOCEPP", nullable = false, length = 3, fillSpace = "0")
    private short soadcocepp;

    @FormatCsv(name = "SOADCELP", nullable = false, length = 12, fillSpace = "0")
    private long soadcelp;

    @FormatCsv(name = "FILL", nullable = false, length = 56)
    private String soadfillp = "";

    @FormatCsv(name = "SOADCOERP", nullable = false, length = 3)
    private String soadcoerp;

    // PR producto de empresa
    @FormatCsv(name = "SOPRTIRE", nullable = false, length = 2)
    private String soprtire;

    @FormatCsv(name = "SOPRCOPR", nullable = false, length = 10, align = FormatCsv.alignType.LEFT)
    private String soprcopr;

    @FormatCsv(name = "SOPRGRAF", nullable = false, length = 4, fillSpace = "0")
    private String soprgraf;

    @FormatCsv(name = "SOPRNUTAEX", nullable = false, length = 19)
    private String soprnutaex;

    @FormatCsv(name = "SOPRFEVEEX", nullable = false, length = 6)
    private String soprfeveex;

    @FormatCsv(name = "SOPRBIN", nullable = false, length = 19)
    private String soprbin;

    @FormatCsv(name = "SOPRNUTCEN", nullable = false, length = 36)
    private String soprnutcen;

    @FormatCsv(name = "SOPRINSEG", nullable = false, length = 1)
    private short soprinseg;

    @FormatCsv(name = "SOPRFILL", nullable = false, length = 271)
    private String soprfill = "";

    @FormatCsv(name = "SOPRNUTC", nullable = false, length = 19)
    private String soprnutc;

    @FormatCsv(name = "SOPRFEVETC", nullable = false, length = 6)
    private String soprfevetc;

    @FormatCsv(name = "SOPRSENUTC", nullable = false, length = 4, fillSpace = "0")
    private short soprsenutc;

    @FormatCsv(name = "SOPRCOER", nullable = false, length = 3)
    private String soprcoer;

    public String getSogatire() {
        return sogatire;
    }

    public void setSogatire(String sogatire) {
        this.sogatire = sogatire;
    }

    public int getSogagrpr() {
        return sogagrpr;
    }

    public void setSogagrpr(int sogagrpr) {
        this.sogagrpr = sogagrpr;
    }

    public short getSogaprcu() {
        return sogaprcu;
    }

    public void setSogaprcu(short sogaprcu) {
        this.sogaprcu = sogaprcu;
    }

    public short getSogaticu() {
        return sogaticu;
    }

    public void setSogaticu(short sogaticu) {
        this.sogaticu = sogaticu;
    }

    public String getSogacoem() {
        return sogacoem;
    }

    public void setSogacoem(String sogacoem) {
        this.sogacoem = sogacoem;
    }

    public String getSogasucu1() {
        return sogasucu1;
    }

    public void setSogasucu1(String sogasucu1) {
        this.sogasucu1 = sogasucu1;
    }

    public String getSogaagem() {
        return sogaagem;
    }

    public void setSogaagem(String sogaagem) {
        this.sogaagem = sogaagem;
    }

    public short getSogainec() {
        return sogainec;
    }

    public void setSogainec(short sogainec) {
        this.sogainec = sogainec;
    }

    public short getSogainecm() {
        return sogainecm;
    }

    public void setSogainecm(short sogainecm) {
        this.sogainecm = sogainecm;
    }

    public String getSogaemail() {
        return sogaemail;
    }

    public void setSogaemail(String sogaemail) {
        this.sogaemail = sogaemail;
    }

    public short getSogainse() {
        return sogainse;
    }

    public void setSogainse(short sogainse) {
        this.sogainse = sogainse;
    }

    public String getSogafill() {
        return sogafill;
    }

    public void setSogafill(String sogafill) {
        this.sogafill = sogafill;
    }

    public BigInteger getSoganutcnc() {
        return soganutcnc;
    }

    public void setSoganutcnc(BigInteger soganutcnc) {
        this.soganutcnc = soganutcnc;
    }

    public String getSogacoer() {
        return sogacoer;
    }

    public void setSogacoer(String sogacoer) {
        this.sogacoer = sogacoer;
    }

    public String getSocctire() {
        return socctire;
    }

    public void setSocctire(String socctire) {
        this.socctire = socctire;
    }

    public BigDecimal getSocclicr() {
        return socclicr;
    }

    public void setSocclicr(BigDecimal socclicr) {
        this.socclicr = socclicr;
    }

    public short getSoccmoli() {
        return soccmoli;
    }

    public void setSoccmoli(short soccmoli) {
        this.soccmoli = soccmoli;
    }

    public short getSoccfeci() {
        return soccfeci;
    }

    public void setSoccfeci(short soccfeci) {
        this.soccfeci = soccfeci;
    }

    public short getSoccfopa() {
        return soccfopa;
    }

    public void setSoccfopa(short soccfopa) {
        this.soccfopa = soccfopa;
    }

    public short getSocctide() {
        return socctide;
    }

    public void setSocctide(short socctide) {
        this.socctide = socctide;
    }

    public String getSoccfill() {
        return soccfill;
    }

    public void setSoccfill(String soccfill) {
        this.soccfill = soccfill;
    }

    public String getSocccoer() {
        return socccoer;
    }

    public void setSocccoer(String socccoer) {
        this.socccoer = socccoer;
    }

    public String getSocjtire() {
        return socjtire;
    }

    public void setSocjtire(String socjtire) {
        this.socjtire = socjtire;
    }

    public String getSocjraso() {
        return socjraso;
    }

    public void setSocjraso(String socjraso) {
        this.socjraso = socjraso;
    }

    public String getSocjnoco() {
        return socjnoco;
    }

    public void setSocjnoco(String socjnoco) {
        this.socjnoco = socjnoco;
    }

    public short getSocjtido() {
        return socjtido;
    }

    public void setSocjtido(short socjtido) {
        this.socjtido = socjtido;
    }

    public String getSocjdoem() {
        return socjdoem;
    }

    public void setSocjdoem(String socjdoem) {
        this.socjdoem = socjdoem;
    }

    public String getSocjfeia() {
        return socjfeia;
    }

    public void setSocjfeia(String socjfeia) {
        this.socjfeia = socjfeia;
    }

    public short getSocjpare() {
        return socjpare;
    }

    public void setSocjpare(short socjpare) {
        this.socjpare = socjpare;
    }

    public String getSocjfefe() {
        return socjfefe;
    }

    public void setSocjfefe(String socjfefe) {
        this.socjfefe = socjfefe;
    }

    public short getSocjpafe() {
        return socjpafe;
    }

    public void setSocjpafe(short socjpafe) {
        this.socjpafe = socjpafe;
    }

    public short getSocjacec() {
        return socjacec;
    }

    public void setSocjacec(short socjacec) {
        this.socjacec = socjacec;
    }

    public String getSocjtiso() {
        return socjtiso;
    }

    public void setSocjtiso(String socjtiso) {
        this.socjtiso = socjtiso;
    }

    public short getSocjalem() {
        return socjalem;
    }

    public void setSocjalem(short socjalem) {
        this.socjalem = socjalem;
    }

    public String getSocjcaem() {
        return socjcaem;
    }

    public void setSocjcaem(String socjcaem) {
        this.socjcaem = socjcaem;
    }

    public String getSocjmofa() {
        return socjmofa;
    }

    public void setSocjmofa(String socjmofa) {
        this.socjmofa = socjmofa;
    }

    public BigDecimal getSocjvafa() {
        return socjvafa;
    }

    public void setSocjvafa(BigDecimal socjvafa) {
        this.socjvafa = socjvafa;
    }

    public short getSocjmopa() {
        return socjmopa;
    }

    public void setSocjmopa(short socjmopa) {
        this.socjmopa = socjmopa;
    }

    public BigDecimal getSocjvapa() {
        return socjvapa;
    }

    public void setSocjvapa(BigDecimal socjvapa) {
        this.socjvapa = socjvapa;
    }

    public short getSocjdcpa() {
        return socjdcpa;
    }

    public void setSocjdcpa(short socjdcpa) {
        this.socjdcpa = socjdcpa;
    }

    public String getSocjfill() {
        return socjfill;
    }

    public void setSocjfill(String socjfill) {
        this.socjfill = socjfill;
    }

    public String getSocjcoer() {
        return socjcoer;
    }

    public void setSocjcoer(String socjcoer) {
        this.socjcoer = socjcoer;
    }

    public String getSoadtirel() {
        return soadtirel;
    }

    public void setSoadtirel(String soadtirel) {
        this.soadtirel = soadtirel;
    }

    public short getSoadtidicl() {
        return soadtidicl;
    }

    public void setSoadtidicl(short soadtidicl) {
        this.soadtidicl = soadtidicl;
    }

    public short getSoadtical() {
        return soadtical;
    }

    public void setSoadtical(short soadtical) {
        this.soadtical = soadtical;
    }

    public String getSoadcallel() {
        return soadcallel;
    }

    public void setSoadcallel(String soadcallel) {
        this.soadcallel = soadcallel;
    }

    public String getSoadnupul() {
        return soadnupul;
    }

    public void setSoadnupul(String soadnupul) {
        this.soadnupul = soadnupul;
    }

    public String getSoadinadl() {
        return soadinadl;
    }

    public void setSoadinadl(String soadinadl) {
        this.soadinadl = soadinadl;
    }

    public String getSoadcopol() {
        return soadcopol;
    }

    public void setSoadcopol(String soadcopol) {
        this.soadcopol = soadcopol;
    }

    public String getSoadzonal() {
        return soadzonal;
    }

    public void setSoadzonal(String soadzonal) {
        this.soadzonal = soadzonal;
    }

    public short getSoadpaisl() {
        return soadpaisl;
    }

    public void setSoadpaisl(short soadpaisl) {
        this.soadpaisl = soadpaisl;
    }

    public String getSoaddptol() {
        return soaddptol;
    }

    public void setSoaddptol(String soaddptol) {
        this.soaddptol = soaddptol;
    }

    public int getSoadlocl() {
        return soadlocl;
    }

    public void setSoadlocl(int soadlocl) {
        this.soadlocl = soadlocl;
    }

    public short getSoadcotepl() {
        return soadcotepl;
    }

    public void setSoadcotepl(short soadcotepl) {
        this.soadcotepl = soadcotepl;
    }

    public int getSoadcoteal() {
        return soadcoteal;
    }

    public void setSoadcoteal(int soadcoteal) {
        this.soadcoteal = soadcoteal;
    }

    public long getSoadtell() {
        return soadtell;
    }

    public void setSoadtell(long soadtell) {
        this.soadtell = soadtell;
    }

    public int getSoadteinl() {
        return soadteinl;
    }

    public void setSoadteinl(int soadteinl) {
        this.soadteinl = soadteinl;
    }

    public short getSoadcocepl() {
        return soadcocepl;
    }

    public void setSoadcocepl(short soadcocepl) {
        this.soadcocepl = soadcocepl;
    }

    public long getSoadcell() {
        return soadcell;
    }

    public void setSoadcell(long soadcell) {
        this.soadcell = soadcell;
    }

    public String getSoadfilll() {
        return soadfilll;
    }

    public void setSoadfilll(String soadfilll) {
        this.soadfilll = soadfilll;
    }

    public String getSoadcoerl() {
        return soadcoerl;
    }

    public void setSoadcoerl(String soadcoerl) {
        this.soadcoerl = soadcoerl;
    }

    public String getSoadtirem() {
        return soadtirem;
    }

    public void setSoadtirem(String soadtirem) {
        this.soadtirem = soadtirem;
    }

    public short getSoadtidicm() {
        return soadtidicm;
    }

    public void setSoadtidicm(short soadtidicm) {
        this.soadtidicm = soadtidicm;
    }

    public short getSoadticam() {
        return soadticam;
    }

    public void setSoadticam(short soadticam) {
        this.soadticam = soadticam;
    }

    public String getSoadcallem() {
        return soadcallem;
    }

    public void setSoadcallem(String soadcallem) {
        this.soadcallem = soadcallem;
    }

    public String getSoadnupum() {
        return soadnupum;
    }

    public void setSoadnupum(String soadnupum) {
        this.soadnupum = soadnupum;
    }

    public String getSoadinadm() {
        return soadinadm;
    }

    public void setSoadinadm(String soadinadm) {
        this.soadinadm = soadinadm;
    }

    public String getSoadcopom() {
        return soadcopom;
    }

    public void setSoadcopom(String soadcopom) {
        this.soadcopom = soadcopom;
    }

    public String getSoadzonam() {
        return soadzonam;
    }

    public void setSoadzonam(String soadzonam) {
        this.soadzonam = soadzonam;
    }

    public short getSoadpaism() {
        return soadpaism;
    }

    public void setSoadpaism(short soadpaism) {
        this.soadpaism = soadpaism;
    }

    public String getSoaddptom() {
        return soaddptom;
    }

    public void setSoaddptom(String soaddptom) {
        this.soaddptom = soaddptom;
    }

    public int getSoadlocm() {
        return soadlocm;
    }

    public void setSoadlocm(int soadlocm) {
        this.soadlocm = soadlocm;
    }

    public short getSoadcotepm() {
        return soadcotepm;
    }

    public void setSoadcotepm(short soadcotepm) {
        this.soadcotepm = soadcotepm;
    }

    public int getSoadcoteam() {
        return soadcoteam;
    }

    public void setSoadcoteam(int soadcoteam) {
        this.soadcoteam = soadcoteam;
    }

    public long getSoadtelm() {
        return soadtelm;
    }

    public void setSoadtelm(long soadtelm) {
        this.soadtelm = soadtelm;
    }

    public int getSoadteinm() {
        return soadteinm;
    }

    public void setSoadteinm(int soadteinm) {
        this.soadteinm = soadteinm;
    }

    public short getSoadcocepm() {
        return soadcocepm;
    }

    public void setSoadcocepm(short soadcocepm) {
        this.soadcocepm = soadcocepm;
    }

    public long getSoadcelm() {
        return soadcelm;
    }

    public void setSoadcelm(long soadcelm) {
        this.soadcelm = soadcelm;
    }

    public String getSoadfillm() {
        return soadfillm;
    }

    public void setSoadfillm(String soadfillm) {
        this.soadfillm = soadfillm;
    }

    public String getSoadcoerm() {
        return soadcoerm;
    }

    public void setSoadcoerm(String soadcoerm) {
        this.soadcoerm = soadcoerm;
    }

    public String getSocftire() {
        return socftire;
    }

    public void setSocftire(String socftire) {
        this.socftire = socftire;
    }

    public short getSocfticl() {
        return socfticl;
    }

    public void setSocfticl(short socfticl) {
        this.socfticl = socfticl;
    }

    public String getSocfnocl() {
        return socfnocl;
    }

    public void setSocfnocl(String socfnocl) {
        this.socfnocl = socfnocl;
    }

    public String getSocfapcl() {
        return socfapcl;
    }

    public void setSocfapcl(String socfapcl) {
        this.socfapcl = socfapcl;
    }

    public short getSocftido() {
        return socftido;
    }

    public void setSocftido(short socftido) {
        this.socftido = socftido;
    }

    public String getSocfdocl() {
        return socfdocl;
    }

    public void setSocfdocl(String socfdocl) {
        this.socfdocl = socfdocl;
    }

    public short getSocftidoa() {
        return socftidoa;
    }

    public void setSocftidoa(short socftidoa) {
        this.socftidoa = socftidoa;
    }

    public String getSocfdocla() {
        return socfdocla;
    }

    public void setSocfdocla(String socfdocla) {
        this.socfdocla = socfdocla;
    }

    public String getSocffena() {
        return socffena;
    }

    public void setSocffena(String socffena) {
        this.socffena = socffena;
    }

    public short getSocfesci() {
        return socfesci;
    }

    public void setSocfesci(short socfesci) {
        this.socfesci = socfesci;
    }

    public short getSocfgecl() {
        return socfgecl;
    }

    public void setSocfgecl(short socfgecl) {
        this.socfgecl = socfgecl;
    }

    public short getSocfpacl() {
        return socfpacl;
    }

    public void setSocfpacl(short socfpacl) {
        this.socfpacl = socfpacl;
    }

    public String getSocfluna() {
        return socfluna;
    }

    public void setSocfluna(String socfluna) {
        this.socfluna = socfluna;
    }

    public short getSocfcahi() {
        return socfcahi;
    }

    public void setSocfcahi(short socfcahi) {
        this.socfcahi = socfcahi;
    }

    public String getSocfnoem() {
        return socfnoem;
    }

    public void setSocfnoem(String socfnoem) {
        this.socfnoem = socfnoem;
    }

    public String getSocfemail() {
        return socfemail;
    }

    public void setSocfemail(String socfemail) {
        this.socfemail = socfemail;
    }

    public short getSocfdcpa() {
        return socfdcpa;
    }

    public void setSocfdcpa(short socfdcpa) {
        this.socfdcpa = socfdcpa;
    }

    public short getSocfdfpa() {
        return socfdfpa;
    }

    public void setSocfdfpa(short socfdfpa) {
        this.socfdfpa = socfdfpa;
    }

    public int getSocfnusol() {
        return socfnusol;
    }

    public void setSocfnusol(int socfnusol) {
        this.socfnusol = socfnusol;
    }

    public String getSocffill() {
        return socffill;
    }

    public void setSocffill(String socffill) {
        this.socffill = socffill;
    }

    public String getSocfcoer() {
        return socfcoer;
    }

    public void setSocfcoer(String socfcoer) {
        this.socfcoer = socfcoer;
    }

    public String getSoeitire() {
        return soeitire;
    }

    public void setSoeitire(String soeitire) {
        this.soeitire = soeitire;
    }

    public String getSoeifill() {
        return soeifill;
    }

    public void setSoeifill(String soeifill) {
        this.soeifill = soeifill;
    }

    public String getSoeicoer() {
        return soeicoer;
    }

    public void setSoeicoer(String soeicoer) {
        this.soeicoer = soeicoer;
    }

    public String getSoadtirep() {
        return soadtirep;
    }

    public void setSoadtirep(String soadtirep) {
        this.soadtirep = soadtirep;
    }

    public short getSoadtidicp() {
        return soadtidicp;
    }

    public void setSoadtidicp(short soadtidicp) {
        this.soadtidicp = soadtidicp;
    }

    public short getSoadticap() {
        return soadticap;
    }

    public void setSoadticap(short soadticap) {
        this.soadticap = soadticap;
    }

    public String getSoadcallep() {
        return soadcallep;
    }

    public void setSoadcallep(String soadcallep) {
        this.soadcallep = soadcallep;
    }

    public String getSoadnupup() {
        return soadnupup;
    }

    public void setSoadnupup(String soadnupup) {
        this.soadnupup = soadnupup;
    }

    public String getSoadinadp() {
        return soadinadp;
    }

    public void setSoadinadp(String soadinadp) {
        this.soadinadp = soadinadp;
    }

    public String getSoadcopop() {
        return soadcopop;
    }

    public void setSoadcopop(String soadcopop) {
        this.soadcopop = soadcopop;
    }

    public String getSoadzonap() {
        return soadzonap;
    }

    public void setSoadzonap(String soadzonap) {
        this.soadzonap = soadzonap;
    }

    public short getSoadpaisp() {
        return soadpaisp;
    }

    public void setSoadpaisp(short soadpaisp) {
        this.soadpaisp = soadpaisp;
    }

    public String getSoaddptop() {
        return soaddptop;
    }

    public void setSoaddptop(String soaddptop) {
        this.soaddptop = soaddptop;
    }

    public int getSoadlocp() {
        return soadlocp;
    }

    public void setSoadlocp(int soadlocp) {
        this.soadlocp = soadlocp;
    }

    public short getSoadcotepp() {
        return soadcotepp;
    }

    public void setSoadcotepp(short soadcotepp) {
        this.soadcotepp = soadcotepp;
    }

    public int getSoadcoteap() {
        return soadcoteap;
    }

    public void setSoadcoteap(int soadcoteap) {
        this.soadcoteap = soadcoteap;
    }

    public long getSoadtelp() {
        return soadtelp;
    }

    public void setSoadtelp(long soadtelp) {
        this.soadtelp = soadtelp;
    }

    public int getSoadteinp() {
        return soadteinp;
    }

    public void setSoadteinp(int soadteinp) {
        this.soadteinp = soadteinp;
    }

    public short getSoadcocepp() {
        return soadcocepp;
    }

    public void setSoadcocepp(short soadcocepp) {
        this.soadcocepp = soadcocepp;
    }

    public long getSoadcelp() {
        return soadcelp;
    }

    public void setSoadcelp(long soadcelp) {
        this.soadcelp = soadcelp;
    }

    public String getSoadfillp() {
        return soadfillp;
    }

    public void setSoadfillp(String soadfillp) {
        this.soadfillp = soadfillp;
    }

    public String getSoadcoerp() {
        return soadcoerp;
    }

    public void setSoadcoerp(String soadcoerp) {
        this.soadcoerp = soadcoerp;
    }

    /*
        public String getSoadtirec() {
            return soadtirec;
        }

        public void setSoadtirec(String soadtirec) {
            this.soadtirec = soadtirec;
        }

        public short getSoadtidicc() {
            return soadtidicc;
        }

        public void setSoadtidicc(short soadtidicc) {
            this.soadtidicc = soadtidicc;
        }

        public short getSoadticac() {
            return soadticac;
        }

        public void setSoadticac(short soadticac) {
            this.soadticac = soadticac;
        }

        public String getSoadcallec() {
            return soadcallec;
        }

        public void setSoadcallec(String soadcallec) {
            this.soadcallec = soadcallec;
        }

        public String getSoadnupuc() {
            return soadnupuc;
        }

        public void setSoadnupuc(String soadnupuc) {
            this.soadnupuc = soadnupuc;
        }

        public String getSoadinadc() {
            return soadinadc;
        }

        public void setSoadinadc(String soadinadc) {
            this.soadinadc = soadinadc;
        }

        public String getSoadcopoc() {
            return soadcopoc;
        }

        public void setSoadcopoc(String soadcopoc) {
            this.soadcopoc = soadcopoc;
        }

        public String getSoadzonac() {
            return soadzonac;
        }

        public void setSoadzonac(String soadzonac) {
            this.soadzonac = soadzonac;
        }

        public short getSoadpaisc() {
            return soadpaisc;
        }

        public void setSoadpaisc(short soadpaisc) {
            this.soadpaisc = soadpaisc;
        }

        public String getSoaddptoc() {
            return soaddptoc;
        }

        public void setSoaddptoc(String soaddptoc) {
            this.soaddptoc = soaddptoc;
        }

        public int getSoadlocc() {
            return soadlocc;
        }

        public void setSoadlocc(int soadlocc) {
            this.soadlocc = soadlocc;
        }

        public short getSoadcotepc() {
            return soadcotepc;
        }

        public void setSoadcotepc(short soadcotepc) {
            this.soadcotepc = soadcotepc;
        }

        public int getSoadcoteac() {
            return soadcoteac;
        }

        public void setSoadcoteac(int soadcoteac) {
            this.soadcoteac = soadcoteac;
        }

        public long getSoadtelc() {
            return soadtelc;
        }

        public void setSoadtelc(long soadtelc) {
            this.soadtelc = soadtelc;
        }

        public int getSoadteinc() {
            return soadteinc;
        }

        public void setSoadteinc(int soadteinc) {
            this.soadteinc = soadteinc;
        }

        public short getSoadcocepc() {
            return soadcocepc;
        }

        public void setSoadcocepc(short soadcocepc) {
            this.soadcocepc = soadcocepc;
        }

        public long getSoadcelc() {
            return soadcelc;
        }

        public void setSoadcelc(long soadcelc) {
            this.soadcelc = soadcelc;
        }

        public String getSoadfillc() {
            return soadfillc;
        }

        public void setSoadfillc(String soadfillc) {
            this.soadfillc = soadfillc;
        }

        public String getSoadcoerc() {
            return soadcoerc;
        }

        public void setSoadcoerc(String soadcoerc) {
            this.soadcoerc = soadcoerc;
        }
    */
    public String getSoprtire() {
        return soprtire;
    }

    public void setSoprtire(String soprtire) {
        this.soprtire = soprtire;
    }

    public String getSoprcopr() {
        return soprcopr;
    }

    public void setSoprcopr(String soprcopr) {
        this.soprcopr = soprcopr;
    }

    public String getSoprgraf() {
        return soprgraf;
    }

    public void setSoprgraf(String soprgraf) {
        this.soprgraf = soprgraf;
    }

    public String getSoprnutaex() {
        return soprnutaex;
    }

    public void setSoprnutaex(String soprnutaex) {
        this.soprnutaex = soprnutaex;
    }

    public String getSoprfeveex() {
        return soprfeveex;
    }

    public void setSoprfeveex(String soprfeveex) {
        this.soprfeveex = soprfeveex;
    }

    public String getSoprbin() {
        return soprbin;
    }

    public void setSoprbin(String soprbin) {
        this.soprbin = soprbin;
    }

    public String getSoprnutcen() {
        return soprnutcen;
    }

    public void setSoprnutcen(String soprnutcen) {
        this.soprnutcen = soprnutcen;
    }

    public short getSoprinseg() {
        return soprinseg;
    }

    public void setSoprinseg(short soprinseg) {
        this.soprinseg = soprinseg;
    }

    public String getSoprfill() {
        return soprfill;
    }

    public void setSoprfill(String soprfill) {
        this.soprfill = soprfill;
    }

    public String getSoprnutc() {
        return soprnutc;
    }

    public void setSoprnutc(String soprnutc) {
        this.soprnutc = soprnutc;
    }

    public String getSoprfevetc() {
        return soprfevetc;
    }

    public void setSoprfevetc(String soprfevetc) {
        this.soprfevetc = soprfevetc;
    }

    public short getSoprsenutc() {
        return soprsenutc;
    }

    public void setSoprsenutc(short soprsenutc) {
        this.soprsenutc = soprsenutc;
    }

    public String getSoprcoer() {
        return soprcoer;
    }

    public void setSoprcoer(String soprcoer) {
        this.soprcoer = soprcoer;
    }
}
