package com.bisa.bus.servicios.tc.linkser.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.linkser.entities.FtpArchivoPreparado;
import com.bisa.bus.servicios.tc.linkser.entities.IdFtpArchivoPreparado;
import com.bisa.bus.servicios.tc.sftp.model.EstadoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.*;

/**
 * Created by atenorio on 16/11/2017.
 */
public class FtpArchivoPreparadoDao extends DaoImpl<FtpArchivoPreparado, IdFtpArchivoPreparado> implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(FtpArchivoPreparadoDao.class);

    @Inject
    protected FtpArchivoPreparadoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public FtpArchivoPreparado getFtpTransferencia(String filename) {
        LOGGER.debug("Obteniendo registro de archivo con nombre:{}.", filename);
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<FtpArchivoPreparado> q = cb.createQuery(FtpArchivoPreparado.class);
                    Root<FtpArchivoPreparado> p = q.from(FtpArchivoPreparado.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("archivo"), StringUtils.trimToEmpty(filename)));
                    predicatesAnd.add(p.get("estado").in(EstadoTransferencia.PRTE));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));

                    TypedQuery<FtpArchivoPreparado> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                        return query.getResultList().get(0);
                    } else {
                        return null;
                    }
                }
        );
    }

   public FtpArchivoPreparado getFtpTransferenciaPendiente(TipoArchivo tipo, EstadoTransferencia estadoTransferencia) {
        LOGGER.info("Obteniendo registro de archivo TCPCTRPRO con estado:{}.", estadoTransferencia);
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<FtpArchivoPreparado> q = cb.createQuery(FtpArchivoPreparado.class);
                    Root<FtpArchivoPreparado> p = q.from(FtpArchivoPreparado.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(p.get("estado").in(EstadoTransferencia.GENE));
                    //predicatesAnd.add(p.get("estado").in(estadoTransferencia));
                    predicatesAnd.add(p.get("id").get("tipo").in(tipo));
                    //predicatesAnd.add(p.get("id").get("estado").in(estadoTransferencia.name()));
                    //predicatesAnd.add(cb.equal(p.get("id").get("tipo"), (tipo)));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    q.orderBy(cb.desc(p.get("fechaCreacion")));

                    TypedQuery<FtpArchivoPreparado> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                        return query.getResultList().get(0);
                    } else {
                        return null;
                    }
                }
        );
    }

    public FtpArchivoPreparado getFtpTransferenciaPendiente(TipoArchivo tipo) {
        LOGGER.debug("Obteniendo registro de archivo de tipo:{}.", tipo);
        return doWithTransaction(
                (entityManager, t) -> {
                    //OpenJPACriteriaBuilder
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<FtpArchivoPreparado> q = cb.createQuery(FtpArchivoPreparado.class);
                    Root<FtpArchivoPreparado> p = q.from(FtpArchivoPreparado.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    //predicates.add(cb.equal(from.get("estado"), estadoTransferencia));
                    predicatesAnd.add(cb.equal(p.get("estado"), (EstadoTransferencia.GENE)));
                    predicatesAnd.add(cb.equal(p.get("id").get("tipo"), (tipo.name())));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    q.orderBy(cb.desc(p.get("fechaCreacion")));

                    TypedQuery<FtpArchivoPreparado> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                        return query.getResultList().get(0);
                    } else {
                        return null;
                    }
                }
        );
    }

    public boolean guardar(FtpArchivoPreparado tranferencia, String user) {
        if (tranferencia.getId() != null) {
            tranferencia.setUsuarioModificador(user);
            tranferencia.setFechaModificacion(new Date());
            merge(tranferencia);
        } else {
            tranferencia.setUsuarioCreador(user);
            tranferencia.setFechaCreacion(new Date());
            persist(tranferencia);
        }
        return true;
    }
    /*
    @Override
    protected Path<Long> countPath(Root<FtpArchivoPreparado> from) {
        return from.get("id");
    }*/

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<FtpArchivoPreparado> p) {
        Path<String> archivo = p.get("archivo");
        Path<String> tipo = p.get("tipo").get("tipo");
        return Arrays.asList(archivo, tipo);
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<FtpArchivoPreparado> from, FtpArchivoPreparado example, FtpArchivoPreparado example2) {
        List<Predicate> predicates = new LinkedList<Predicate>();
        Date fecha = example.getFechaCreacion();
        String nombreArchivo = StringUtils.trimToNull(example.getArchivo());
        EstadoTransferencia estadoTransferencia = example.getEstado();
        if (nombreArchivo != null) {
            predicates.add(cb.like(from.get("archivo"), "%" + nombreArchivo + "%"));
            LOGGER.debug("Filtro nombre de archivo: {}", nombreArchivo);
        }
        if (estadoTransferencia != null) {
            predicates.add(cb.equal(from.get("estado"), estadoTransferencia));
        }
        if (fecha != null && example2.getFechaCreacion() != null) {
            Date inicio = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH);
            Date inicio2 = DateUtils.truncate(example2.getFechaCreacion(), Calendar.DAY_OF_MONTH);
            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
            LOGGER.debug("   FECHAS ENTRE [{}] A [{}]", inicio, inicio2);
            predicates.add(cb.between(from.get("fechaCreacion"), inicio, inicio2));
        }
        return predicates.toArray(new Predicate[predicates.size()]); //super.createQBE(cb, from, example, example2);
    }
}

