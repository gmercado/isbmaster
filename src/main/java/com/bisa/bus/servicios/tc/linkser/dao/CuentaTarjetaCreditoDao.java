package com.bisa.bus.servicios.tc.linkser.dao;


import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.linkser.entities.CuentaTarjetaCredito;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.LinkedList;

public class CuentaTarjetaCreditoDao  extends DaoImpl<CuentaTarjetaCredito, Long> implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(CuentaTarjetaCreditoDao.class);

    @Inject
    protected CuentaTarjetaCreditoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public CuentaTarjetaCredito getCuentaTarjetaId(long id) {
        LOGGER.debug("Obteniendo registro de cuenta:{}.", id);

        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<CuentaTarjetaCredito> q = cb.createQuery(CuentaTarjetaCredito.class);
                    Root<CuentaTarjetaCredito> p = q.from(CuentaTarjetaCredito.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("cuentaTarjeta"), id));

                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));

                    TypedQuery<CuentaTarjetaCredito> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                        return query.getResultList().get(0);
                    } else {
                        return null;
                    }
                }
        );
    }

}
