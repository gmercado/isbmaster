package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.IdMovimientosTC;
import com.bisa.bus.servicios.tc.sftp.entities.MovimientosHistoricoTC;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 09/06/2017.
 */
public class MovimientosHistoricoDao extends DaoImpl<MovimientosHistoricoTC, IdMovimientosTC> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovimientosHistoricoDao.class);

    @Inject
    protected MovimientosHistoricoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(MovimientosHistoricoTC registro, String user) {
        registro.setUsuarioCreador(user);
        registro.setFechaModificacion(new Date());
        persist(registro);
        return true;
    }
}