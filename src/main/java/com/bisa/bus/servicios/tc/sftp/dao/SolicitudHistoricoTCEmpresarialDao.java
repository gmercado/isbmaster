package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.SolicitudHistoricoTCEmpresarial;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 * Created by atenorio on 15/09/2017.
 */
public class SolicitudHistoricoTCEmpresarialDao extends DaoImpl<SolicitudHistoricoTCEmpresarial, Integer> implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(SolicitudHistoricoTCEmpresarialDao.class);

    @Inject
    protected SolicitudHistoricoTCEmpresarialDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(SolicitudHistoricoTCEmpresarial solicitud, String user) {
        // Siempre almacenar nuevo registro de historico
        persist(solicitud);
        return true;
    }
}
