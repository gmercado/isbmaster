package com.bisa.bus.servicios.tc.sftp.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.interfaces.as400.dao.TipoAtt;
import bus.interfaces.as400.dao.ValorAttDao;
import bus.interfaces.as400.entities.ValorAtt;
import bus.plumbing.csv.LecturaCSV;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.tc.sftp.dao.ExtractoMensualDao;
import com.bisa.bus.servicios.tc.sftp.dao.ExtractoMensualHistoricoDao;
import com.bisa.bus.servicios.tc.sftp.entities.ExtractoMensual;
import com.bisa.bus.servicios.tc.sftp.entities.ExtractoMensualHistorico;
import com.bisa.bus.servicios.tc.sftp.entities.IdExtractoMensual;
import com.bisa.bus.servicios.tc.sftp.model.EstadoPagosTC;
import com.bisa.bus.servicios.tc.sftp.model.ExtractoMensualCsv;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgProgramaTarjetaCredito;
import com.bisa.bus.servicios.tc.sftp.utils.FormatoNumericoTexto;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by atenorio on 02/08/2017.
 */
public class SftpRecepcionExtractoMensual {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpRecepcionExtractoMensual.class);

    private final MedioAmbiente medioAmbiente;
    private final ExtractoMensualDao extractoMensualDao;
    private final ExtractoMensualHistoricoDao extractoMensualHistoricoDao;
    private final RpgProgramaTarjetaCredito rpgPrograma;
    private final NotificacionesCorreo notificacionesCorreo;
    private final ValorAttDao valorAttDao;

    SftpConfiguracion configuracion;
    SftpTransferencia sftpTransferencia;

    @Inject
    public SftpRecepcionExtractoMensual(MedioAmbiente medioAmbiente,
                                        ExtractoMensualDao extractoMensualDao, ExtractoMensualHistoricoDao extractoMensualHistoricoDao,
                                        NotificacionesCorreo notificacionesCorreo, RpgProgramaTarjetaCredito rpgPrograma,
                                        SftpConfiguracion configuracion, SftpTransferencia sftpTransferencia,
                                        ValorAttDao valorAttDao) {
        this.medioAmbiente = medioAmbiente;
        this.extractoMensualDao = extractoMensualDao;
        this.extractoMensualHistoricoDao = extractoMensualHistoricoDao;
        this.rpgPrograma = rpgPrograma;
        this.notificacionesCorreo = notificacionesCorreo;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
        this.valorAttDao = valorAttDao;
    }


    /**
     * Metodo para procesar recepcion de archivo desde servidor remoto SFTP.
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    String PREF = "#fecha";

    public boolean procesarArchivo() {
        String detalle = "";
        boolean ok = true;
        LOGGER.debug("Inicia proceso de recepcion de archivo de extarto mensual");
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_EXTRACTO_MENSUAL_ATC, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_EXTRACTO_MENSUAL_ATC_DEFAULT);
        String nombreArchivoRegexForzado = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_EXTRACTO_MENSUAL_ATC_FORZADO, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_EXTRACTO_MENSUAL_ATC_FORZADO_DEFAULT);
        String nombreArchivo[] = Iterables.toArray(Splitter.on("|").trimResults().split(nombreArchivoRegex), String.class);

        // Procesar a fecha de facturacion
        String fechaProcesoTexto = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_ATC, Variables.FECHA_DE_PROCESO_ATC_DEFAULT);
        // Calcular fecha del dia
        Calendar hoy = Calendar.getInstance();
        // No se debe disminuir un dia debido a que el archivo se generara con la fecha de proceso de sistema,
        // no la fecha de proceso de negocio cierre al 25 de cada mes.
        Date fechaProcesar = hoy.getTime();

        if (StringUtils.isEmpty(fechaProcesoTexto) || "0".equals(fechaProcesoTexto)) {
            fechaProcesoTexto = FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar);
        } else {
            fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProcesoTexto);
            if (fechaProcesar == null) {
                notificacionesCorreo.notificarError("Error en fecha de proceso recepci\u00F3n extracto", "");
                return false;
            }
        }

        LOGGER.info("Fecha de proceso: {}, {}", fechaProcesar, fechaProcesoTexto);
        nombreArchivoRegex = nombreArchivo[0].replace(PREF, new SimpleDateFormat(nombreArchivo[1]).format(fechaProcesar));
        if (!StringUtils.isEmpty(nombreArchivoRegexForzado) && !"NULL".equals(nombreArchivoRegexForzado)) {
            nombreArchivoRegex = nombreArchivoRegexForzado;
        }

        LOGGER.info("Descarga de archivo de a servidor SFTP con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_EXTRACTO_ATC, Variables.FTP_RUTA_RECEPCION_EXTRACTO_ATC_DEFAULT));
        sftpTransferencia.configurar(configuracion);

        ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.CIE);
        LOGGER.debug("Descarga completa correctamente: {}", ok);

        if (!ok || sftpTransferencia.getArchivoLocal() == null) {
            // Cerrar el registro con error y permitir reintento
            detalle = "Problema en transferencia o no existen archivos de respuesta en servidor SFTP.";
            LOGGER.warn(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.CIE, detalle);
            return false;
        }

        // Procesamiento de archivo local
        LOGGER.debug("Proceso de mapeo de archivos de respuesta");
        File archivo = sftpTransferencia.getArchivoLocal();
        List<ExtractoMensualCsv> lista = mapearArchivo(archivo);
        // En caso de problemas en lectura de archivo
        if (lista == null || lista.size() == 0) {
            detalle = "Error en mapeo de archivo de respuesta o archivo vac\u00EDo";
            LOGGER.warn(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.CIE, detalle);
            return false;
        }

        // Validar que se realizo el mapeo de forma exitosa de la estructura de pagos
        int fila = 0;
        for (ExtractoMensualCsv extracto : lista) {
            fila++;
            if (extracto == null) {
                detalle = "Error en mapeo de una estructura se requiere revisar el archivo de respuesta en fila " + fila + ".";
                LOGGER.error(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.CIE, detalle);
                return false;
            }
            String permitirValidar = medioAmbiente.getValorDe(Variables.FTP_VALIDAR_RECEPCION_ARCHIVO_ATC, Variables.FTP_VALIDAR_RECEPCION_ARCHIVO_ATC_DEFAULT);
            if("true".equals(StringUtils.trimToEmpty(permitirValidar))) {
                detalle = validacion(extracto, fila);
                if (StringUtils.isNotEmpty(detalle)) {
                    LOGGER.error(detalle);
                    LOGGER.warn("Error en validacion registro: {}", extracto);
                    sftpTransferencia.transferenciaConErrorValidacion(detalle);
                    notificacionesCorreo.notificarErrorTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.CIE, detalle);
                    return false;
                }
            }
        }
        LOGGER.info("Registros cargado a lista: {}", lista.size());

        LOGGER.debug("Actualizar tabla de extracto mensual");

        TransferObject transferObject = new TransferObject();
        transferObject.initializate();

        // Limpiar tablas intermedias
        if (!extractoMensualDao.limpiarTodo()) {
            detalle = "Error al limpiar tabla temporal movimientos.";
            LOGGER.error(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.CIE, detalle);
            return false;
        }

        ok = true;

        // Verificar consistencia de registro
        for (ExtractoMensualCsv extracto : lista) {
            IdExtractoMensual id = new IdExtractoMensual();
            id.setCodigoAdm('A');
            id.setFechaProceso(FormatosUtils.fechaYYYYMMDD(fechaProcesar).intValue());
            id.setNumeroCuenta(extracto.getNumeroCuenta());
            id.setNumeroFactura(extracto.getNumeroFactura());
            id.setIdArchivoRecepcion(sftpTransferencia.getFtpArchivoTransferencia().getId());

            // Recuperando información de mapeo
            ExtractoMensual extractoDB = transferObject.convert(extracto, ExtractoMensual.class);
            extractoDB.setIdExtractoMensual(id);

            extractoDB.setEstado(EstadoPagosTC.GENE.name());
            extractoDB.setCuentaTarjetaReducida("");
            // Registrar extracto
            extractoDB.setSaldoAnteriorBsSigno(FormatoNumericoTexto.getSigno(extractoDB.getSaldoAnteriorBs()));
            extractoDB.setSaldoAnteriorBsTexto(FormatoNumericoTexto.getNumero(extractoDB.getSaldoAnteriorBs(), 10));
            extractoDB.setSaldoAnteriorUsSigno(FormatoNumericoTexto.getSigno(extractoDB.getSaldoAnteriorUs()));
            extractoDB.setSaldoAnteriorUsTexto(FormatoNumericoTexto.getNumero(extractoDB.getSaldoAnteriorUs(), 10));
            extractoDB.setInteresAnteriorSigno(FormatoNumericoTexto.getSigno(extractoDB.getInteresAnterior()));
            extractoDB.setInteresAnteriorTexto(FormatoNumericoTexto.getNumero(extractoDB.getInteresAnterior(), 10));
            extractoDB.setPagoMinimoAnteriorSigno(FormatoNumericoTexto.getSigno(extractoDB.getPagoMinimoAnterior()));
            extractoDB.setPagoMinimoAnteriorTexto(FormatoNumericoTexto.getNumero(extractoDB.getPagoMinimoAnterior(), 10));
            extractoDB.setSaldoActualBsSigno(FormatoNumericoTexto.getSigno(extractoDB.getSaldoActualBs()));
            extractoDB.setSaldoActualBsTexto(FormatoNumericoTexto.getNumero(extractoDB.getSaldoActualBs(), 10));
            extractoDB.setSaldoActualUsSigno(FormatoNumericoTexto.getSigno(extractoDB.getSaldoActualUs()));
            extractoDB.setSaldoActualUsTexto(FormatoNumericoTexto.getNumero(extractoDB.getSaldoActualUs(), 10));
            extractoDB.setInteresActualSigno(FormatoNumericoTexto.getSigno(extractoDB.getInteresActual()));
            extractoDB.setInteresActualTexto(FormatoNumericoTexto.getNumero(extractoDB.getInteresActual(), 10));
            extractoDB.setPagoMinimoActualSigno(FormatoNumericoTexto.getSigno(extractoDB.getPagoMinimoActual()));
            extractoDB.setPagoMinimoActualTexto(FormatoNumericoTexto.getNumero(extractoDB.getPagoMinimoActual(), 10));
            extractoDB.setLimiteFinanciamientoSigno(FormatoNumericoTexto.getSigno(extractoDB.getLimiteFinanciamiento()));
            extractoDB.setLimiteFinanciamientoTexto(FormatoNumericoTexto.getNumero(extractoDB.getLimiteFinanciamiento(), 10));
            extractoDB.setConsumoPeriodoAnteriorSigno(FormatoNumericoTexto.getSigno(extractoDB.getConsumoPeriodoAnterior()));
            extractoDB.setConsumoPeriodoAnteriorTexto(FormatoNumericoTexto.getNumero(extractoDB.getConsumoPeriodoAnterior(), 10));
            extractoDB.setConsumoPeriodoActualBsSigno(FormatoNumericoTexto.getSigno(extractoDB.getConsumoPeriodoActualBs()));
            extractoDB.setConsumoPeriodoActualBsTexto(FormatoNumericoTexto.getNumero(extractoDB.getConsumoPeriodoActualBs(), 10));
            extractoDB.setConsumoPeriodoActualUsSigno(FormatoNumericoTexto.getSigno(extractoDB.getConsumoPeriodoActualUs()));
            extractoDB.setConsumoPeriodoActualUsTexto(FormatoNumericoTexto.getNumero(extractoDB.getConsumoPeriodoActualUs(), 10));

            extractoDB.setFechaRecepcion(new Date());

            extractoDB.setUsuarioModificador("ISB");
            extractoDB.setFechaModificacion(new Date());

            LOGGER.debug("Extracto: {}", extractoDB);
            ok = ok && extractoMensualDao.guardar(extractoDB, "ISB");

            ExtractoMensualHistorico historico = transferObject.convert(extractoDB, ExtractoMensualHistorico.class);
            historico.setIdExtractoMensual(extractoDB.getIdExtractoMensual());
            ok = ok && extractoMensualHistoricoDao.guardar(historico, "ISB");
        }
        if (ok) {
            detalle = "\nRecepci\u00F3n de Extractos finalizado correctamente con " + fila + " registros.";
            sftpTransferencia.transferenciaCompleta(detalle);
            notificacionesCorreo.notificarCorrectaTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.CIE, detalle);
        } else {
            detalle = "Problemas en registro de base de datos.";
            sftpTransferencia.transferenciaConErrorInterno(detalle);
            notificacionesCorreo.notificarErrorTransferencia(sftpTransferencia.getNombreArchivoRemoto(), TipoArchivo.CIE, detalle);
        }

        LOGGER.info("Completado proceso de carga de archivo {} de pagos a servidor SFTP de ATC.", sftpTransferencia.getNombreArchivoRemoto());
        return ok;
    }

    /**
     * Mapeo de estructura de facturas
     **/
    public List<ExtractoMensualCsv> mapearArchivo(File archivo) {
        FileInputStream is = null;
        BufferedReader br = null;

        List<ExtractoMensualCsv> facturaCsvs = new ArrayList<>();
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();
        int total = 0;
        try {
            is = new FileInputStream(archivo);
            String codificacion = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC, Variables.FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC_DEFAULT);
            br = new BufferedReader(new InputStreamReader(is, codificacion));
            List<String> lineas = new ArrayList<String>();

            String linea = br.readLine();
            while (linea != null) {
                total++;
                LOGGER.debug("linea {}:{}", total, linea);
                if (StringUtils.isNotEmpty(linea) && linea.length() > 1 && total > 0) {
                    ExtractoMensualCsv factura = LecturaCSV.procesar(ExtractoMensualCsv.class, linea, "|");
                    facturaCsvs.add(factura);
                }
                // lectura desde la segunda linea
                linea = br.readLine();
            }
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al procesar el archivo "
                    + archivo.getName() + ".", e);
            return null;
        } finally {
            IOUtils.closeQuietly(is);
        }
        return facturaCsvs;
    }

    private String validacion(ExtractoMensualCsv extracto, int fila) {
        if (StringUtils.isNotEmpty(extracto.getCodigoBanco())) {
            List<ValorAtt> lista = valorAttDao.getListaDe(TipoAtt.TARJETA_CREDITO_EMISOR);
            if (lista != null && lista.size() > 0 &&
                    !extracto.getCodigoBanco().equals(StringUtils.trimToEmpty(lista.get(0).getValor()))) {
                return "Error de validaci\u00F3n con c\u00F3digo de banco incorrecto en fila " + fila + ".";
            }
        }
        return "";
    }

    public String getNombreArchivoProcesado() {
        return StringUtils.trimToEmpty(sftpTransferencia.getNombreArchivoRemoto());
    }
}
