package com.bisa.bus.servicios.tc.linkser.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 20/11/2017.
 */
@Entity
@Table(name = "TCPTCTXT")
public class DatosGeneralesLinkser implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "DATOSGT", nullable = false, length = 100)
    private String texto;

    public DatosGeneralesLinkser(String texto){
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}

