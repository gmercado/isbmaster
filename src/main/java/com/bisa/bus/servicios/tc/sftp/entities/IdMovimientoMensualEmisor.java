package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by atenorio on 26/06/2017.
 */
@Embeddable
public class IdMovimientoMensualEmisor implements Serializable {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "MMCADM", nullable = false)
    private Character codigoAdm;

    @Basic(optional = false)
    @Column(name = "MMFEPRO1", nullable = false)
    private int fechaProceso;

    @Basic(optional = false)
    @Column(name = "MMNUTCNC", nullable = false, length = 10) //MENUTCNC
    private String numeroCuenta;

    @Basic(optional = false)
    @Column(name = "MMCOMPR", nullable = false, length = 12) //MENUCOMP
    private String numeroComprobante;

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getNumeroComprobante() {
        return numeroComprobante;
    }

    public void setNumeroComprobante(String numeroComprobante) {
        this.numeroComprobante = numeroComprobante;
    }

    public Character getCodigoAdm() {
        return codigoAdm;
    }

    public void setCodigoAdm(Character codigoAdm) {
        this.codigoAdm = codigoAdm;
    }

    public int getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(int fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdMovimientoMensualEmisor that = (IdMovimientoMensualEmisor) o;

        if (getFechaProceso() != that.getFechaProceso()) return false;
        if (!getCodigoAdm().equals(that.getCodigoAdm())) return false;
        if (!getNumeroCuenta().equals(that.getNumeroCuenta())) return false;
        return getNumeroComprobante().equals(that.getNumeroComprobante());
    }

    @Override
    public int hashCode() {
        int result = getCodigoAdm().hashCode();
        result = 31 * result + getFechaProceso();
        result = 31 * result + getNumeroCuenta().hashCode();
        result = 31 * result + getNumeroComprobante().hashCode();
        return result;
    }
}
