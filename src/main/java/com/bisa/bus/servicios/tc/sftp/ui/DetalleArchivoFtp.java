package com.bisa.bus.servicios.tc.sftp.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import com.bisa.bus.servicios.tc.sftp.dao.FtpTransferenciaDao;
import com.bisa.bus.servicios.tc.sftp.entities.FtpArchivoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.EstadoTransferencia;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 08/05/2017.
 */
@Titulo("Detalle de Archivo FTP")
public class DetalleArchivoFtp extends BisaWebPage {

    protected static final Logger LOGGER = LoggerFactory.getLogger(DetalleArchivoFtp.class);

    public DetalleArchivoFtp(PageParameters parameters) {
        super(parameters);
        Long id = parameters.get("id").toLong();

        IModel<FtpArchivoTransferencia> model = new LoadableDetachableModel<FtpArchivoTransferencia>() {
            @Override
            protected FtpArchivoTransferencia load() {
                FtpTransferenciaDao dao = getInstance(FtpTransferenciaDao.class);
                return dao.find(id);
            }
        };

        setDefaultModel(new CompoundPropertyModel<>(model));
        //Form
        Form<FtpArchivoTransferencia> form = new Form<>("detalle", model);
        add(form);

        // detalle
        form.add(new Label("id"));
        form.add(new Label("archivo"));
        form.add(new Label("tipoDescripcion"));
        form.add(new Label("estadoDescripcion"));

        // botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(ListadoArchivos.class);
            }
        });
        form.add(new IndicatingAjaxButton("reprocesar") {
            @Override
            public void onSubmit(AjaxRequestTarget target, Form<?> form) {
                try {
                    FtpTransferenciaDao dao = getInstance(FtpTransferenciaDao.class);
                    FtpArchivoTransferencia transferencia = (FtpArchivoTransferencia) DetalleArchivoFtp.this.getDefaultModelObject();
                    transferencia.setEstado(EstadoTransferencia.REPR);
                    dao.guardar(transferencia, "USER");
                    getSession().info("Reproceso registrado");
                    setResponsePage(ListadoArchivos.class);
                } catch (Exception e) {
                    LOGGER.error("Error inesperado al guardar reproceso", e);
                    //error(e.getMessage());
                    error("Error inesperado al guardar reproceso.");
                    target.add(feedbackPanel);
                }
            }

            @Override
            public boolean isVisible() {
                FtpArchivoTransferencia transferencia = (FtpArchivoTransferencia) DetalleArchivoFtp.this.getDefaultModelObject();
                boolean value = transferencia.getEstado().equals(EstadoTransferencia.ERPR) ||
                        transferencia.getEstado().equals(EstadoTransferencia.ERVA) ||
                        transferencia.getEstado().equals(EstadoTransferencia.PRTE) ||
                        transferencia.getEstado().equals(EstadoTransferencia.LEAR);
                return value;
            }
        });

    }
}

