package com.bisa.bus.servicios.tc.sftp.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;
import com.bisa.bus.servicios.tc.sftp.dao.FtpTransferenciaDao;
import com.bisa.bus.servicios.tc.sftp.entities.FtpArchivoTransferencia;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by atenorio on 08/05/2017.
 */
@AuthorizeInstantiation({RolesBisa.FTP_TRANSFERENCIA})
@Menu(value = "Transferencia de archivos FTP", subMenu = SubMenu.FTP)
public class ListadoArchivos extends Listado<FtpArchivoTransferencia, Long> {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ListadoArchivos.class);

    public ListadoArchivos() {
        super();
    }

    @Override
    protected ListadoPanel<FtpArchivoTransferencia, Long> newListadoPanel(String id) {
        return new ListadoPanel<FtpArchivoTransferencia, Long>(id) {
            @Override
            protected List<IColumn<FtpArchivoTransferencia, String>> newColumns(ListadoDataProvider<FtpArchivoTransferencia, Long> dataProvider) {
                List<IColumn<FtpArchivoTransferencia, String>> columns = new LinkedList<>();
                columns.add(new LinkPropertyColumn<FtpArchivoTransferencia>(Model.of("ID"), "id", "id") {
                    @Override
                    protected void onClick(FtpArchivoTransferencia object) {
                        setResponsePage(DetalleArchivoFtp.class, new PageParameters().add("id", object.getId()));
                    }
                });
                columns.add(new PropertyColumn<>(Model.of("Archivo"), "archivo", "archivo"));

                columns.add(new PropertyColumn<FtpArchivoTransferencia, String>(Model.of("Estado"), "estado", "estadoDescripcion") {
                    @Override
                    public void populateItem(Item<ICellPopulator<FtpArchivoTransferencia>> item, String componentId, IModel<FtpArchivoTransferencia> rowModel) {
                        FtpArchivoTransferencia archivo = rowModel.getObject();
                        item.add(new Label(componentId, archivo.getEstadoDescripcion()).add(new AttributeAppender("style", Model.of("text-align:center"))));
                    }
                });

                columns.add(new PropertyColumn<FtpArchivoTransferencia, String>(Model.of("Tipo"), "tipo", "tipoDescripcion") {
                    @Override
                    public void populateItem(Item<ICellPopulator<FtpArchivoTransferencia>> item, String componentId, IModel<FtpArchivoTransferencia> rowModel) {
                        FtpArchivoTransferencia archivo = rowModel.getObject();
                        item.add(new Label(componentId, archivo.getTipoDescripcion()).add(new AttributeAppender("style", Model.of("text-align:center"))));
                    }
                });

                columns.add(new PropertyColumn<>(Model.of("Fecha Proceso"), "fechaCreacion", "fechaProcesoFormato"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<FtpArchivoTransferencia, Long>> getProviderClazz() {
                return FtpTransferenciaDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<FtpArchivoTransferencia> newFiltroPanel(String id, IModel<FtpArchivoTransferencia> model1, IModel<FtpArchivoTransferencia> model2) {
                return new ListaArchivoFiltroPanel(id, model1, model2);

            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("fechaCreacion", false);
            }

            @Override
            protected FtpArchivoTransferencia newObject2() {
                return new FtpArchivoTransferencia();
            }

            @Override
            protected FtpArchivoTransferencia newObject1() {
                return new FtpArchivoTransferencia();
            }

            @Override
            protected boolean isVisibleData() {
                FtpArchivoTransferencia ftp1 = getModel1().getObject();
                FtpArchivoTransferencia ftp2 = getModel2().getObject();
                return !((ftp1.getFechaCreacion() == null || ftp2.getFechaCreacion() == null));
            }
        };
    }
}
