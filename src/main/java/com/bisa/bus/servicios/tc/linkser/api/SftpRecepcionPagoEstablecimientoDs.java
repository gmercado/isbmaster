package com.bisa.bus.servicios.tc.linkser.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.plumbing.file.ArchivoBase;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.tc.linkser.dao.PagoEstablecimientoDsDao;
import com.bisa.bus.servicios.tc.linkser.entities.PagoEstablecimientoDs;
import com.bisa.bus.servicios.tc.sftp.api.SftpTransferencia;
import com.bisa.bus.servicios.tc.linkser.utils.MapeoArchivo;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by atenorio on 21/11/2017.
 */
public class SftpRecepcionPagoEstablecimientoDs implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpRecepcionPagoEstablecimientoDs.class);
    private final MedioAmbiente medioAmbiente;
    private final ArchivoBase archivoBase;
    private final NotificacionesCorreo notificacionesCorreo;
    private final PagoEstablecimientoDsDao iPagoEstablecimientoDsDao;

    ISftpConfiguracionLinkser configuracion;
    SftpTransferencia sftpTransferencia;
    private String nombreArchivoProcesado = "";

    @Inject
    public SftpRecepcionPagoEstablecimientoDs(ArchivoBase archivoBase,
                                              MedioAmbiente medioAmbiente,
                                              NotificacionesCorreo notificacionesCorreo,
                                              PagoEstablecimientoDsDao iPagoEstablecimientoDsDao,
                                              ISftpConfiguracionLinkser configuracion, SftpTransferencia sftpTransferencia) {
        this.medioAmbiente = medioAmbiente;
        this.archivoBase = archivoBase;
        this.notificacionesCorreo = notificacionesCorreo;
        this.iPagoEstablecimientoDsDao = iPagoEstablecimientoDsDao;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para procesar recepcion de archivo de pago a estableciminetos de servidor remoto SFTP.
     * BISA1711.DOL
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    String PREF = "#fecha";

    public boolean procesarArchivo() {
        boolean ok = false;
        String detalle = "";
        LOGGER.debug("Inicia proceso recepcion de archivo ");
        LOGGER.debug("1) Verificar archivo pendiente de respuesta");
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_NOMBRE_ARCHIVO_PAGO_ESTABLECIMIENTO_DS_LINKSER, Variables.FTP_NOMBRE_ARCHIVO_PAGO_ESTABLECIMIENTO_DS_LINKSER_DEFAULT);
        String nombreArchivoRegexForzado = ""; //medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_LINKSER_FORZADO, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_LINKSER_FORZADO_DEFAULT);
        String nombreArchivo[] = Iterables.toArray(Splitter.on("|").trimResults().split(nombreArchivoRegex), String.class);

        String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_LINKSER, Variables.FECHA_DE_PROCESO_LINKSER_DEFAULT);
        // La nomenclatura de nombres usa ddMM
        Calendar hoy = Calendar.getInstance();
        Date fechaProcesar = hoy.getTime();

        if (StringUtils.isEmpty(nombreArchivoRegexForzado) || "NULL".equals(nombreArchivoRegexForzado)) {
            // Procesar a fecha determinada por parametro
            if (StringUtils.isNotEmpty(fechaProceso) && !"0".equals(fechaProceso)) {
                fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
                if (fechaProcesar == null) {
                    notificacionesCorreo.notificarError("Error en fecha de proceso recepci\u00F3n a pago de establecimiento", "");
                    return false;
                }
            }
            nombreArchivoRegex = nombreArchivo[0].replace(PREF, new SimpleDateFormat(nombreArchivo[1]).format(fechaProcesar));
        } else {
            nombreArchivoRegex = nombreArchivoRegexForzado;
        }
        LOGGER.debug("2) Decargar archivo de servidor SFTP");
        sftpTransferencia.setFechaProceso(Integer.valueOf(FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar)));
        prepararConfiguracionSftp();
        LOGGER.info("2.1) Descarga de archivo de a servidor SFTP con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.ELI);

        LOGGER.debug("3) Validar archivo descargado");
        LOGGER.debug("3.1) Validar estructura");
        LOGGER.debug("3.2) Validar mapeo");

        // Cerrar el registro con error y permitir reintento
        if (!ok || sftpTransferencia.getArchivoLocal() == null) {
            sftpTransferencia.transferenciaConErrorReintento("");
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.ELI, "No existen archivos de respuesta en servidor SFTP.");
            LOGGER.warn("No existen archivos de respuesta en servidor SFTP.");
            return false;
        }

        // Procesamiento de archivo local
        File archivo = sftpTransferencia.getArchivoLocal();
        List<String> mapeo = MapeoArchivo.mapearArchivo(archivo);
        // Verificar si existen problemas en mape de archivo de respuesta
        if (mapeo == null || mapeo.size() == 0) {
            LOGGER.warn("Error en mapeo de archivo de respuesta");
            sftpTransferencia.transferenciaConErrorReintento("");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.ELI, "Error en mapeo de archivo de respuesta.");
            return false;
        }


        LOGGER.debug("4) Actualizar tabla de solicitudes");
        LOGGER.debug("4.1) Verificar si un registro se encuentra en un estado inconsistente");
        int totalRegistros = 0;

        for (String solicitud : mapeo) {
            // Validar que se realizo el mapeo de forma exitosa en todas las estructuras
            if (solicitud == null) {
                LOGGER.error("Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                sftpTransferencia.transferenciaConErrorReintento("");
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.ELI, "Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                return false;
            }
            // Validar que se tenga pendiente de respuesta las solicitudes de la respuesta
            // TODO ATC
            /*
            SolicitudTC solicitudDB = iSolicitudTCDao.getSolicitudCodigoExterno(solicitud.getSocfnusol());
            if (solicitudDB == null) {
                detalle = "Error en actualización de informacion recibida no existe registro de solicitud.";
                LOGGER.error(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RAL, detalle);
                return false;
            }
            */
        }

        if (!iPagoEstablecimientoDsDao.limpiarTodo()) {
            detalle = "Error al limpiar tabla temporal.";
            LOGGER.error(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.ELI, detalle);
            return false;
        }

        //for (SolicitudCompleto solicitud : solicitudes) {
        for (String dato: mapeo) {
            // Grabar
            if (ok) {
                LOGGER.debug("====================================");
                LOGGER.debug("DATO:{}", dato);
                iPagoEstablecimientoDsDao.persist(new PagoEstablecimientoDs(dato));
            }
            totalRegistros++;
        }
        // Eliminar archivo de servidor local
        // TODO ATC eliminar archivo remoto
        /*
        if (FileUtils.deleteQuietly(archivo)) {
            LOGGER.info("Se ha eliminado el archivo de altas, {}", archivo.getName());
        }*/
        LOGGER.debug("4.2) Verificar si un registro se encuentra en un estado inconsistente");

        // Modificar estado de solicitudes de alta y notificar por correo
        if (ok) {
            sftpTransferencia.transferenciaCompleta("");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.ELI, "Transferencia completa con " + totalRegistros + " registros.");
            LOGGER.info("Proceso finalizado de recepcion de pago a establecimientos bolivianos.");
        } else {
            detalle = "Error en proceso de recepcion de pago a establecimientos bolivianos.";
            LOGGER.warn(detalle);
            sftpTransferencia.transferenciaConErrorInterno(detalle);
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.ELI, detalle);
        }
        return ok;
    }

    public boolean prepararConfiguracionSftp() {
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_PAGO_ESTABLECIMIENTO_LINKSER, Variables.FTP_RUTA_RECEPCION_PAGO_ESTABLECIMIENTO_LINKSER_DEFAULT));
        return sftpTransferencia.configurar(configuracion);
    }

    public String getNombreArchivoProcesado() {
        return sftpTransferencia.getNombreArchivoRemotoFecha();
    }
}
