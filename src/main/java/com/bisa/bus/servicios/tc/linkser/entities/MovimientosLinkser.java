package com.bisa.bus.servicios.tc.linkser.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 20/11/2017.
 */
@Entity
@Table(name = "TCPTRTXT")
public class MovimientosLinkser implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "TRANDAT", nullable = false, length = 100)
    private String texto;

    public MovimientosLinkser(String texto){
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
