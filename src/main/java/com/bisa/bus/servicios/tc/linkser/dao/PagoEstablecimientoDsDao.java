package com.bisa.bus.servicios.tc.linkser.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.linkser.entities.PagoEstablecimientoDs;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.io.Serializable;

/**
 * Created by atenorio on 21/11/2017.
 */
public class PagoEstablecimientoDsDao extends DaoImpl<PagoEstablecimientoDs, String> implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(PagoEstablecimientoDsDao.class);

    @Inject
    protected PagoEstablecimientoDsDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(PagoEstablecimientoDs cuenta, String user) {
        persist(cuenta);
        return true;
    }

    public boolean limpiarTodo() {
        LOGGER.debug("Limpiando tabla PagoEstablecimientoDsDao.");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<PagoEstablecimientoDs> query = entityManager.createQuery("DELETE FROM PagoEstablecimientoDs");//
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }
}