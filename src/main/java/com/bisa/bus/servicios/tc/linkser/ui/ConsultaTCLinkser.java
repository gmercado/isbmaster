package com.bisa.bus.servicios.tc.linkser.ui;

import bus.env.api.MedioAmbiente;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.rpg.ImposibleLlamarProgramaRpgException;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.ButtonAuthorize;
import bus.plumbing.components.ControlGroupBorder;
import com.bisa.bus.servicios.tc.linkser.dao.CuentaTarjetaCreditoDao;
import com.bisa.bus.servicios.tc.linkser.entities.CuentaTarjetaCredito;
import com.bisa.bus.servicios.tc.linkser.model.ConsultaMovimientoRespuesta;
import com.bisa.bus.servicios.tc.linkser.model.ConsultaMovimientoSolicitudForm;
import com.bisa.bus.servicios.tc.sftp.rpg.RpgProgramaTarjetaCredito;
import com.ibm.as400.access.ConnectionPoolException;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.validator.DateValidator;
import org.joda.time.DateTime;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.TimeoutException;

/**
 * Created by atenorio on 17/01/2017.
 */
@AuthorizeInstantiation({RolesBisa.LINKSER_CONSUMO, RolesBisa.LINKSER_CONSULTAS})
@Menu(value = "Consulta a LINKSER", subMenu = SubMenu.LINKSER)
public class ConsultaTCLinkser extends BisaWebPage {

    Model<String> css = new Model<>(HIDE);
    private IModel<ConsultaMovimientoSolicitudForm> objectModel;
    boolean metodo = false;

    public ConsultaTCLinkser() {
        inicio(new ConsultaMovimientoSolicitudForm());
    }

    public ConsultaTCLinkser(ConsultaMovimientoSolicitudForm solicitud) {
        inicio(solicitud);
    }

    private void inicio(ConsultaMovimientoSolicitudForm solicitud) {
        RequiredTextField<String> numeroTarjeta;
        RequiredTextField<String> cuentaTarjeta;
        Component infoAdicional;
        DateTextField fechaInicial;
        DateTextField fechaFinal;
        ControlGroupBorder ctrlFecha;
        WebMarkupContainer containerInfoAdicional;
        this.objectModel = new CompoundPropertyModel<>(solicitud);
        //Form
        Form<ConsultaMovimientoSolicitudForm> form = new Form<>("buscarLinkser", objectModel);
        add(form);
        /*
        //Número tarjeta
        numeroTarjeta = new RequiredTextField<>("numeroTarjeta");
        numeroTarjeta.add(StringValidator.minimumLength(16));
        numeroTarjeta.add(StringValidator.maximumLength(16));
        //numeroTarjeta.add(new PatternValidator(processService.getFormato(1)));
        numeroTarjeta.setOutputMarkupId(true);
        form.add(new ControlGroupBorder("numeroTarjeta", numeroTarjeta, feedbackPanel, Model.of("N\u00famero de Tarjeta:")).add(numeroTarjeta));
        */

        //Número cuenta tarjeta
        cuentaTarjeta = new RequiredTextField<>("cuenta");
        form.add(new ControlGroupBorder("cuenta", cuentaTarjeta, feedbackPanel, Model.of("N\u00famero de Cuenta Tarjeta:")).add(cuentaTarjeta));


        //Fecha inicial
        final String datePattern = "dd/MM/yyyy";
        fechaInicial = new DateTextField("fechaInicial", datePattern);
        fechaInicial.setRequired(false).add(DateValidator.maximum(new DateTime().plusDays(30).toDate()))
                .add(DateValidator.minimum(new DateTime().minusYears(1).toDate()));
        fechaInicial.add(new DatePicker());
        fechaInicial.setOutputMarkupId(true);
        fechaInicial.setOutputMarkupPlaceholderTag(true);
        //Fecha final
        fechaFinal = new DateTextField("fechaFinal", datePattern);
        fechaFinal.setRequired(false).add(DateValidator.maximum(new DateTime().plusDays(30).toDate()))
                .add(DateValidator.minimum(new DateTime().minusYears(1).toDate()));
        fechaFinal.add(new DatePicker());
        fechaFinal.setOutputMarkupId(true);
        fechaFinal.setOutputMarkupPlaceholderTag(true);

        containerInfoAdicional = new WebMarkupContainer("panelAdicional");
        containerInfoAdicional.setOutputMarkupPlaceholderTag(true);
        containerInfoAdicional.add(fechaInicial);
        containerInfoAdicional.add(fechaFinal);
        containerInfoAdicional.add(AttributeModifier.append("class", css));
        form.add(containerInfoAdicional);

        //Información adicional
        infoAdicional = new AjaxCheckBox("infoAdicional", new PropertyModel<>(objectModel, "infoAdicional")) {
            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                if (getObjetoModel().isInfoAdicional()) {
                    // css.setObject(SHOW);
                    // fechaInicial.setRequired(true);
                    metodo = true;
                } else {
                    //css.setObject(HIDE);
                    //fechaInicial.setRequired(false);
                    metodo = false;
                }
                //target.add(containerInfoAdicional);
            }
        }.setLabel(Model.of("Metodo encriptado")).setOutputMarkupId(true);
        form.add(new ControlGroupBorder("infoAdicional", infoAdicional, feedbackPanel, Model.of("Metodo encriptado:")).add(infoAdicional));

        //Boton consulta
        form.add(new ButtonAuthorize("consulta", RolesBisa.LINKSER_CONSULTAS) {
            @Override
            public void onSubmit() {
                LOGGER.info("====================INICIO====================");
                //Obtender datos
                String numeroTarjeta = null;
                if (getObjetoModel() != null) {
                    if (StringUtils.isNotBlank(getObjetoModel().getNumeroTarjeta()) && StringUtils.isNotEmpty(getObjetoModel().getNumeroTarjeta())) {
                        numeroTarjeta = StringUtils.trimToNull(getObjetoModel().getNumeroTarjeta().toUpperCase());
                    }
                    Long tarjeta = 0L;
                    // Realizar consulta de servicios y devolver resultado
                    ConsultaMovimientoRespuesta response = null;
                    //Consumir servicio
                    ConsultaMovimientoSolicitudForm request = (ConsultaMovimientoSolicitudForm) getObjetoModel();

                    if (StringUtils.isNotBlank(getObjetoModel().getCuenta()) && StringUtils.isNotEmpty(getObjetoModel().getCuenta())) {
                        if (metodo) {
                            String cuentaTarjeta = StringUtils.trimToNull(getObjetoModel().getCuenta().toUpperCase());
                            LOGGER.info("Cuenta: {}", cuentaTarjeta);
                            CuentaTarjetaCreditoDao dao = getInstance(CuentaTarjetaCreditoDao.class);
                            RpgProgramaTarjetaCredito rpg = getInstance(RpgProgramaTarjetaCredito.class);
                            MedioAmbiente medioAmbiente = getInstance(MedioAmbiente.class);

                            CuentaTarjetaCredito cuenta = dao.getCuentaTarjetaId(Long.parseLong(cuentaTarjeta));

                            if (cuenta != null) {
                                LOGGER.info("===================Se recupera valor de cuenta====================");
                                LOGGER.info(cuenta.getTarjetaCredito());

                                try {
                                    tarjeta = rpg.obtenerTarjetaCredito(cuenta.getTarjetaCredito());
                                } catch (ImposibleLeerRespuestaException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                } catch (TransaccionEfectivaException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                } catch (SistemaCerradoException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                } catch (ImposibleLlamarProgramaRpgException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                } catch (ConnectionPoolException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                } catch (TimeoutException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                }
                                response = new ConsultaMovimientoRespuesta();
                                response.setCodRespuesta("Metodo encriptado");
                                if (tarjeta.toString().length() == 16) {
                                    LOGGER.info("Tarjeta: {}" + tarjeta);
                                    request.setNumeroTarjeta(formatearTarjetaCredito(tarjeta.toString()));
                                } else {
                                    request.setNumeroTarjeta(tarjeta.toString());
                                    response.setDscRespuesta("Error al desencriptar tarjeta");
                                }
                            }else{
                                getSession().error("No se tiene datos de cuenta linkser");
                                return;
                            }

                        } else {
                            LOGGER.info("Cuenta: {}", getObjetoModel().getCuenta());
                            RpgProgramaTarjetaCredito rpg = getInstance(RpgProgramaTarjetaCredito.class);

                                try {
                                    tarjeta = rpg.obtenerTarjetaCredito(BigDecimal.valueOf(Long.valueOf(getObjetoModel().getCuenta())));
                                } catch (ImposibleLeerRespuestaException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                } catch (TransaccionEfectivaException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                } catch (SistemaCerradoException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                } catch (ImposibleLlamarProgramaRpgException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                } catch (ConnectionPoolException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                } catch (TimeoutException e) {
                                    e.printStackTrace();
                                    response.setDscRespuesta(e.getMessage());
                                }
                                response = new ConsultaMovimientoRespuesta();
                                response.setCodRespuesta("Metodo solo cuenta");
                                if (tarjeta.toString().length() == 16) {
                                    LOGGER.info("Tarjeta: {}" + tarjeta);
                                    request.setNumeroTarjeta(formatearTarjetaCredito(tarjeta.toString()));
                                } else {
                                    request.setNumeroTarjeta(tarjeta.toString());
                                    response.setDscRespuesta("Error al desencriptar tarjeta");
                                }


                        }

                    }

                    if (response != null) {
                        setResponsePage(new RespuestaLinkser(request, response));
                    } else {
                        getSession().error("Error en consulta de tarjeta"); //processService.getMensaje(MENSAJE_ERROR_CONSULTALINKSER));
                    }
                }
                LOGGER.info("====================FIN====================");
            }
        });
        form.add(new Button("limpiar") {
            @Override
            public void onSubmit() {
                if (StringUtils.isNotBlank(getObjetoModel().getNumeroTarjeta()) && StringUtils.isNotEmpty(getObjetoModel().getNumeroTarjeta())) {
                    setResponsePage(new ConsultaTCLinkser());
                } else {
                    form.clearInput();
                    getObjetoModel().setInfoAdicional(false);
                    getObjetoModel().setFechaInicial(null);
                    getObjetoModel().setFechaFinal(null);
                    fechaInicial.setRequired(false);
                    fechaFinal.setRequired(false);
                    css.setObject(HIDE);
                }
            }
        }.setDefaultFormProcessing(false));
    }

    private ConsultaMovimientoSolicitudForm getObjetoModel() {
        return this.objectModel.getObject();
    }

    public static String formatearTarjetaCredito(String str) {
        StringBuilder stringBuilder = new StringBuilder(16);
        stringBuilder.append(org.apache.commons.lang.StringUtils.substring(str, 0, 4)).
                append("-").
                append(org.apache.commons.lang.StringUtils.substring(str, 4, 6)).
                append("XX-XXXX-").
                append(org.apache.commons.lang.StringUtils.substring(str, 12, 16));
        return stringBuilder.toString();
    }
}
