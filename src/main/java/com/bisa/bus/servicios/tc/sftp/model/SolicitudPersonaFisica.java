package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import javax.persistence.Basic;

/**
 * Created by atenorio on 18/05/2017.
 */
public class SolicitudPersonaFisica {
    @Basic(optional = false)
    @FormatCsv(name = "SOCFTIRE", nullable = false, length = 2)
    private String socftire;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFTICL", nullable = false, length = 1)
    private short socfticl;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFNOCL", nullable = false, length = 60)
    private String socfnocl;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFAPCL", nullable = false, length = 60)
    private String socfapcl;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFTIDO", nullable = false, length = 4, fillSpace = "0")
    private short socftido;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFDOCL", nullable = false, length = 25, align = FormatCsv.alignType.LEFT)
    private String socfdocl;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFTIDOA", nullable = false, length = 4, fillSpace = "0")
    private short socftidoa;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFDOCLA", nullable = false, length = 25)
    private String socfdocla;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFFENA", nullable = false, length = 8)
    private String socffena;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFESCI", nullable = false, length = 1, fillSpace = "0")
    private short socfesci;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFGECL", nullable = false, length = 1, fillSpace = "0")
    private short socfgecl;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFPACL", nullable = false, length = 3, fillSpace = "0")
    private short socfpacl;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFLUNA", nullable = false, length = 40)
    private String socfluna;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFCAHI", nullable = false, length = 2, fillSpace = "0")
    private short socfcahi;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFNOEM", nullable = false, length = 25)
    private String socfnoem;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFEMAIL", nullable = false, length = 60)
    private String socfemail;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFDCPA", nullable = false, length = 1)
    private short socfdcpa;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFDFPA", nullable = false, length = 1)
    private short socfdfpa;
    @Basic(optional = false)
    @FormatCsv(name = "SOCFNUSOL", nullable = false, length = 9, fillSpace = "0")
    private int socfnusol;
    @FormatCsv(name = "FILL", nullable = false, length = 65)
    private String socffill="";
    @Basic(optional = false)
    @FormatCsv(name = "SOCFCOER", nullable = false, length = 3)
    private String socfcoer;

    public String getSocftire() {
        return socftire;
    }

    public void setSocftire(String socftire) {
        this.socftire = socftire;
    }

    public short getSocfticl() {
        return socfticl;
    }

    public void setSocfticl(short socfticl) {
        this.socfticl = socfticl;
    }

    public String getSocfnocl() {
        return socfnocl;
    }

    public void setSocfnocl(String socfnocl) {
        this.socfnocl = socfnocl;
    }

    public String getSocfapcl() {
        return socfapcl;
    }

    public void setSocfapcl(String socfapcl) {
        this.socfapcl = socfapcl;
    }

    public short getSocftido() {
        return socftido;
    }

    public void setSocftido(short socftido) {
        this.socftido = socftido;
    }

    public String getSocfdocl() {
        return socfdocl;
    }

    public void setSocfdocl(String socfdocl) {
        this.socfdocl = socfdocl;
    }

    public short getSocftidoa() {
        return socftidoa;
    }

    public void setSocftidoa(short socftidoa) {
        this.socftidoa = socftidoa;
    }

    public String getSocfdocla() {
        return socfdocla;
    }

    public void setSocfdocla(String socfdocla) {
        this.socfdocla = socfdocla;
    }

    public String getSocffena() {
        return socffena;
    }

    public void setSocffena(String socffena) {
        this.socffena = socffena;
    }

    public short getSocfesci() {
        return socfesci;
    }

    public void setSocfesci(short socfesci) {
        this.socfesci = socfesci;
    }

    public short getSocfgecl() {
        return socfgecl;
    }

    public void setSocfgecl(short socfgecl) {
        this.socfgecl = socfgecl;
    }

    public short getSocfpacl() {
        return socfpacl;
    }

    public void setSocfpacl(short socfpacl) {
        this.socfpacl = socfpacl;
    }

    public String getSocffill() {
        return socffill;
    }

    public void setSocffill(String socffill) {
        this.socffill = socffill;
    }

    public String getSocfluna() {
        return socfluna;
    }

    public void setSocfluna(String socfluna) {
        this.socfluna = socfluna;
    }

    public short getSocfcahi() {
        return socfcahi;
    }

    public void setSocfcahi(short socfcahi) {
        this.socfcahi = socfcahi;
    }

    public String getSocfnoem() {
        return socfnoem;
    }

    public void setSocfnoem(String socfnoem) {
        this.socfnoem = socfnoem;
    }

    public String getSocfemail() {
        return socfemail;
    }

    public void setSocfemail(String socfemail) {
        this.socfemail = socfemail;
    }

    public short getSocfdcpa() {
        return socfdcpa;
    }

    public void setSocfdcpa(short socfdcpa) {
        this.socfdcpa = socfdcpa;
    }

    public short getSocfdfpa() {
        return socfdfpa;
    }

    public void setSocfdfpa(short socfdfpa) {
        this.socfdfpa = socfdfpa;
    }

    public int getSocfnusol() {
        return socfnusol;
    }

    public void setSocfnusol(int socfnusol) {
        this.socfnusol = socfnusol;
    }

    public String getSocfcoer() {
        return socfcoer;
    }

    public void setSocfcoer(String socfcoer) {
        this.socfcoer = socfcoer;
    }
}
