package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.RespuestaSolicitudCuenta;
import com.bisa.bus.servicios.tc.sftp.entities.RespuestaSolicitudTarjeta;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.io.Serializable;

/**
 * Created by atenorio on 25/05/2017.
 */
public class RespuestaCuentaDao extends DaoImpl<RespuestaSolicitudCuenta, String> implements Serializable{

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(RespuestaCuentaDao.class);

    @Inject
    protected RespuestaCuentaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(RespuestaSolicitudCuenta cuenta, String user) {
        persist(cuenta);
        return true;
    }

    public boolean limpiarTodo() {
        LOGGER.debug("Limpiando tabla RespuestaSolicitudCuenta.");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<RespuestaSolicitudCuenta> query = entityManager.createQuery("DELETE FROM RespuestaSolicitudCuenta");//
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }
}
