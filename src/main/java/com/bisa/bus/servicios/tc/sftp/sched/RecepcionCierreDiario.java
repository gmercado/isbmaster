package com.bisa.bus.servicios.tc.sftp.sched;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.bus.servicios.tc.sftp.api.SftpRecepcionDatosGenerales;
import com.bisa.bus.servicios.tc.sftp.api.SftpRecepcionMovimientos;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 07/06/2017.
 */
public class RecepcionCierreDiario implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecepcionCierreDiario.class);

    public static String NOMBRE_TAREA = "RecepcionCierreDiario";
    private final MedioAmbiente medioAmbiente;
    private final NotificacionesCorreo notificacionesCorreo;
    private final SftpRecepcionDatosGenerales sftpRecepcionDatosGenerales;
    private final SftpRecepcionMovimientos sftpRecepcionMovimientos;

    private volatile boolean ok;

    @Inject
    public RecepcionCierreDiario(MedioAmbiente medioAmbiente, NotificacionesCorreo notificacionesCorreo,
                                 SftpRecepcionDatosGenerales sftpRecepcionDatosGenerales, SftpRecepcionMovimientos sftpRecepcionMovimientos){
        this.medioAmbiente = medioAmbiente;
        this.notificacionesCorreo = notificacionesCorreo;
        this.sftpRecepcionDatosGenerales = sftpRecepcionDatosGenerales;
        this.sftpRecepcionMovimientos = sftpRecepcionMovimientos;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        // Procesando datos generales
        LOGGER.info("Inicio de proceso datos generales");
        ok = sftpRecepcionDatosGenerales.procesarArchivo();
        LOGGER.info("Respuesta de archivo procesado datos generales: {}", ok);
        // Procesando movimientos
        if(ok) {
            LOGGER.info("Inicio de proceso movimientos");
            ok = sftpRecepcionMovimientos.procesarArchivo();
            LOGGER.info("Respuesta de archivo procesado movimientos: {}", ok);
        }
        // Completar proceso de carga notificando al operador
        if(ok){
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_OPERADOR, Variables.FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_OPERADOR_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador(sftpRecepcionDatosGenerales.getNombreArchivoProcesado() + ", " + sftpRecepcionMovimientos.getNombreArchivoProcesado(), TipoArchivo.MOV, mensaje);
        }else{
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_ERROR_OPERADOR, Variables.FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_ERROR_OPERADOR_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador("", TipoArchivo.MOV, mensaje);
        }
    }
}
