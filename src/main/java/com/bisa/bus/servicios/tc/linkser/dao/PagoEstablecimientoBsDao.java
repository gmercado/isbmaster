package com.bisa.bus.servicios.tc.linkser.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.linkser.entities.PagoEstablecimientoBs;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.io.Serializable;

/**
 * Created by atenorio on 21/11/2017.
 */
public class PagoEstablecimientoBsDao extends DaoImpl<PagoEstablecimientoBs, String> implements Serializable{

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(PagoEstablecimientoBsDao.class);

    @Inject
    protected PagoEstablecimientoBsDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(PagoEstablecimientoBs cuenta, String user) {
        persist(cuenta);
        return true;
    }

    public boolean limpiarTodo() {
        LOGGER.debug("Limpiando tabla PagoEstablecimientoBsDao.");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        TypedQuery<PagoEstablecimientoBs> query = entityManager.createQuery("DELETE FROM PagoEstablecimientoBs");//
                        int i = query.executeUpdate();
                        return true;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return false;
        }
    }
}
