package com.bisa.bus.servicios.tc.sftp.entities;

import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.tc.sftp.model.EstadoTransferencia;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 03/05/2017.
 */
@Entity
@Table(name = "ISBP42")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "I42FECA")),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "I42USRA")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "I42FECM")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "I42USRM"))})
public class FtpArchivoTransferencia  extends EntityBase implements Serializable {

    private static final long serialVersionUID = 98127219783L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I42ID", unique = true)
    private Long id;

    @Column(name = "I42ARCHIVO")
    private String archivo;

    @Column(name = "I42DETA", length = 255)
    private String detalle;

    @Column(name = "I42TIPO")
    @Enumerated(EnumType.STRING)
    private TipoArchivo tipo;

    @Column(name = "I42ESTADO", length = 4)
    @Enumerated(EnumType.STRING)
    private EstadoTransferencia estado;

    @Column(name = "I42RES")
    private Long respuesta = 0L;

    @Column(name = "I42FECP")
    private Integer fechaProceso;

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EstadoTransferencia getEstado() {
        return estado;
    }

    public void setEstado(EstadoTransferencia estado) {
        this.estado = estado;
    }

    public String getEstadoDescripcion(){
        return estado.getDescripcion();
    }

    public String getTipoDescripcion(){
        return tipo.getDescripcion();
    }

    public TipoArchivo getTipo() {
        return tipo;
    }

    public void setTipo(TipoArchivo tipo) {
        this.tipo = tipo;
    }

    public String getFechaProcesoFormato(){
        return FormatosUtils.formatoFechaHora(fechaCreacion);
    }

    @Override
    public String toString() {
        return "Archivo =" + archivo + " id= " + id + " estado=" + estado + " tipo=" + tipo;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Long getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Long respuesta) {
        this.respuesta = respuesta;
    }

    public Integer getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(Integer fechaProceso) {
        this.fechaProceso = fechaProceso;
    }
}

