package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import java.math.BigDecimal;

/**
 * Created by atenorio on 07/09/2017.
 */
public class SolicitudPersonaJuridica {

    
    @FormatCsv(name = "SOCJTIRE", nullable = false, length = 2)
    private String socjtire;
    
    @FormatCsv(name = "SOCJRASO", nullable = false, length = 60)
    private String socjraso;
    
    @FormatCsv(name = "SOCJNOCO", nullable = false, length = 60)
    private String socjnoco;
    
    @FormatCsv(name = "SOCJTIDO", nullable = false, length = 4)
    private short socjtido;
    
    @FormatCsv(name = "SOCJDOEM", nullable = false, length = 25)
    private String socjdoem;
    
    @FormatCsv(name = "SOCJFEIA", nullable = false, length = 8)
    private String socjfeia;
    
    @FormatCsv(name = "SOCJPARE", nullable = false, length = 3)
    private short socjpare;
    
    @FormatCsv(name = "SOCJFEFE", nullable = false, length = 8)
    private String socjfefe;
    
    @FormatCsv(name = "SOCJPAFE", nullable = false, length = 3)
    private short socjpafe;
    
    @FormatCsv(name = "SOCJACEC", nullable = false, length = 4)
    private short socjacec;
    
    @FormatCsv(name = "SOCJTISO", nullable = false, length = 4)
    private String socjtiso;
    
    @FormatCsv(name = "SOCJALEM", nullable = false, length = 1)
    private short socjalem;
    
    @FormatCsv(name = "SOCJCAEM", nullable = false, length = 10)
    private String socjcaem;
    
    @FormatCsv(name = "SOCJMOFA", nullable = false, length = 3)
    private String socjmofa;
    
    @FormatCsv(name = "SOCJVAFA", nullable = false,  pattern = "##.00", scale = 2, length = 12, fillSpace = "0") //precision = 12, scale = 2)
    private BigDecimal socjvafa;
    
    @FormatCsv(name = "SOCJMOPA", nullable = false, length = 3)
    private short socjmopa;
    
    @FormatCsv(name = "SOCJVAPA", nullable = false,  pattern = "##.00", scale = 2, length = 12, fillSpace = "0") //precision = 12, scale = 2)
    private BigDecimal socjvapa;
    
    @FormatCsv(name = "SOCJDCPA", nullable = false, length = 1)
    private short socjdcpa;
    
    @FormatCsv(name = "FILL", nullable = false, length = 174)
    private String socjfill="";
    
    @FormatCsv(name = "SOCJCOER", nullable = false, length = 3)
    private String socjcoer;

    public String getSocjtire() {
        return socjtire;
    }

    public void setSocjtire(String socjtire) {
        this.socjtire = socjtire;
    }

    public String getSocjraso() {
        return socjraso;
    }

    public void setSocjraso(String socjraso) {
        this.socjraso = socjraso;
    }

    public String getSocjnoco() {
        return socjnoco;
    }

    public void setSocjnoco(String socjnoco) {
        this.socjnoco = socjnoco;
    }

    public short getSocjtido() {
        return socjtido;
    }

    public void setSocjtido(short socjtido) {
        this.socjtido = socjtido;
    }

    public String getSocjdoem() {
        return socjdoem;
    }

    public void setSocjdoem(String socjdoem) {
        this.socjdoem = socjdoem;
    }

    public String getSocjfeia() {
        return socjfeia;
    }

    public void setSocjfeia(String socjfeia) {
        this.socjfeia = socjfeia;
    }

    public short getSocjpare() {
        return socjpare;
    }

    public void setSocjpare(short socjpare) {
        this.socjpare = socjpare;
    }

    public String getSocjfefe() {
        return socjfefe;
    }

    public void setSocjfefe(String socjfefe) {
        this.socjfefe = socjfefe;
    }

    public short getSocjpafe() {
        return socjpafe;
    }

    public void setSocjpafe(short socjpafe) {
        this.socjpafe = socjpafe;
    }

    public short getSocjacec() {
        return socjacec;
    }

    public void setSocjacec(short socjacec) {
        this.socjacec = socjacec;
    }

    public String getSocjtiso() {
        return socjtiso;
    }

    public void setSocjtiso(String socjtiso) {
        this.socjtiso = socjtiso;
    }

    public short getSocjalem() {
        return socjalem;
    }

    public void setSocjalem(short socjalem) {
        this.socjalem = socjalem;
    }

    public String getSocjcaem() {
        return socjcaem;
    }

    public void setSocjcaem(String socjcaem) {
        this.socjcaem = socjcaem;
    }

    public String getSocjmofa() {
        return socjmofa;
    }

    public void setSocjmofa(String socjmofa) {
        this.socjmofa = socjmofa;
    }

    public BigDecimal getSocjvafa() {
        return socjvafa;
    }

    public void setSocjvafa(BigDecimal socjvafa) {
        this.socjvafa = socjvafa;
    }

    public short getSocjmopa() {
        return socjmopa;
    }

    public void setSocjmopa(short socjmopa) {
        this.socjmopa = socjmopa;
    }

    public BigDecimal getSocjvapa() {
        return socjvapa;
    }

    public void setSocjvapa(BigDecimal socjvapa) {
        this.socjvapa = socjvapa;
    }

    public short getSocjdcpa() {
        return socjdcpa;
    }

    public void setSocjdcpa(short socjdcpa) {
        this.socjdcpa = socjdcpa;
    }

    public String getSocjfill() {
        return socjfill;
    }

    public void setSocjfill(String socjfill) {
        this.socjfill = socjfill;
    }

    public String getSocjcoer() {
        return socjcoer;
    }

    public void setSocjcoer(String socjcoer) {
        this.socjcoer = socjcoer;
    }
}
