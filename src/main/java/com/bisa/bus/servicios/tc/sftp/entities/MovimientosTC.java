package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by atenorio on 07/06/2017.
 */
@Entity
@Table(name = "TCPANMOEM")
public class MovimientosTC implements Serializable{
    private static final long serialVersionUID = 1L;

//    @EmbeddedId
//    private IdMovimientosTC idMovimiento;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MEID", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Basic(optional = false)
    @Column(name = "MECADM", nullable = false)
    private Character codigoAdm;

    @Basic(optional = false)
    @Column(name = "MEFEPRO1", nullable = false)
    private int fechaProceso;

    @Basic(optional = false)
    @Column(name = "MENUTCNC", nullable = false)
    private String numeroCuenta;

    @Basic(optional = false)
    @Column(name = "MENUCOMP", nullable = false)
    private String comprobante;


    @Basic(optional = false)
    @Column(name = "MEBAEM", nullable = false)
    private String codigoBanco;

    @Basic(optional = false)
    @Column(name = "MESUEM", nullable = false)
    private String codigoSucursal;

    @Basic(optional = false)
    @Column(name = "MENUTCEN", nullable = false)
    private String numeroTarjeta;

    @Basic(optional = false)
    @Column(name = "MEACTC", nullable = false)
    private String codigoAceptanteTarjeta;

    @Basic(optional = false)
    @Column(name = "MECOOP", nullable = false)
    private Character codigoOperacion;
    @Basic(optional = false)
    @Column(name = "MECAESCU", nullable = false, length = 4)
    private String categoriaEstadoCuenta;
    @Basic(optional = false)
    @Column(name = "MECABA", nullable = false, length = 4)
    private String categoriaBalance;
    @Basic(optional = false)
    @Column(name = "MECAFI", nullable = false, length = 4)
    private String categoriaFinanciacion;
    @Basic(optional = false)
    @Column(name = "MEFETR", nullable = false)
    private String fechaTransaccion;
    @Basic(optional = false)
    @Column(name = "MEIMMOORS")
    private Character importeOrigenSigno;
    @Basic(optional = false)
    @Column(name = "MEIMMOOR")
    private String importeOrigenTexto;
    @Basic(optional = false)
    @Column(name = "MEIMMOORN", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeOrigen;
    @Basic(optional = false)
    @Column(name = "MEMOOR", nullable = false)
    private String monedaOrigen;
    @Basic(optional = false)
    @Column(name = "MEIMMOBAS")
    private Character importeBalanceSigno;
    @Basic(optional = false)
    @Column(name = "MEIMMOBA")
    private String importeBalanceTexto;

//    @Basic(optional = false)
//    @Column(name = "MEIMMOBA", nullable = false, precision = 11, scale = 2)
    @Transient
    private BigDecimal importeBalance;

    @Basic(optional = false)
    @Column(name = "MEMOBA", nullable = false)
    private String monedaBalance;

    @Basic(optional = false)
    @Column(name = "MEIMMOLIS")
    private Character importeLiquidacionSigno;
    @Basic(optional = false)
    @Column(name = "MEIMMOLI")
    private String importeLiquidacionTexto;

    @Basic(optional = false)
    @Column(name = "MEIMMOLIN", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeLiquidacion;
    @Basic(optional = false)
    @Column(name = "MEMOLI", nullable = false)
    private String monedaLiquidacion;
    @Basic(optional = false)
    @Column(name = "MEALTR", nullable = false)
    private Character alcanceTransaccion;
    @Basic(optional = false)
    @Column(name = "MENULO", nullable = false)
    private String numeroLote;
    @Basic(optional = false)
    @Column(name = "MEMOTR", nullable = false, length = 2)
    private String modoTransaccion;
    @Basic(optional = false)
    @Column(name = "MENOEST", nullable = false, length = 30)
    private String establecimiento;
    @Basic(optional = false)
    @Column(name = "MEMTI", nullable = false, length = 4)
    private String codigoMti;
    @Basic(optional = false)
    @Column(name = "MECOPRO", nullable = false, length = 6)
    private String codigoProceso;
    @Basic(optional = false)
    @Column(name = "MECOAUT", nullable = false, length = 6)
    private String codigoAutorizacion;
    @Basic(optional = false)
    @Column(name = "MEIDTERM", nullable = false, length = 8)
    private String identificadorTerminal;
    @Basic(optional = false)
    @Column(name = "MECACUO", nullable = false)
    private String totalCuota;
    @Basic(optional = false)
    @Column(name = "MENUCUO", nullable = false)
    private String numeroCuota;
    @Basic(optional = false)
    @Column(name = "MEMOADIS")
    private Character montoAdicionalSigno;
    @Basic(optional = false)
    @Column(name = "MEMOADI")
    private String montoAdicionalTexto;

//    @Basic(optional = false)
//    @Column(name = "MEMOADI", nullable = false, precision = 11, scale = 2)
    @Transient
    private BigDecimal montoAdicional;

    @Basic(optional = false)
    @Column(name = "MEIMEFECS")
    private Character importeCashBackSigno;
    @Basic(optional = false)
    @Column(name = "MEIMEFEC")
    private String importeCashBackTexto;

//    @Basic(optional = false)
//    @Column(name = "MEIMEFEC", nullable = false, precision = 11, scale = 2)
    @Transient
    private BigDecimal importeCashBack;
    @Basic(optional = false)
    @Column(name = "MECADISP", nullable = false)
    private String capacidadTerminal;
    @Basic(optional = false)
    @Column(name = "MECOINTC", nullable = false)
    private String condicionIngreso;


    @Basic(optional = false)
    @Column(name = "MENUTC", nullable = false)
    private long cuentaTarjeta = 0;
    @Basic(optional = false)
    @Column(name = "MENUT1", nullable = false, length = 9)
    private String cuentaTarjetaReducido = "";
    @Basic(optional = false)
    @Column(name = "MEEST", nullable = false, length = 4)
    private String estado="";

    @Basic(optional = false)
    @Column(name = "MEIDRE", nullable = false, length = 3)
    private Long idArchivoRecepcion;
    @Basic(optional = false)
    @Column(name = "MEFERE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRecepcion=new Date();
    @Basic(optional = false)
    @Column(name = "REPGCR", nullable = false, length = 10)
    private String usuarioCreador = "";
    @Basic(optional = false)
    @Column(name = "REFECR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion = new Date();
    @Basic(optional = false)
    @Column(name = "REPGMO", nullable = false, length = 10)
    private String usuarioModificador = "";
    @Basic(optional = false)
    @Column(name = "REFEMO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion = new Date();

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getIdArchivoRecepcion() {
        return idArchivoRecepcion;
    }

    public void setIdArchivoRecepcion(Long idArchivoRecepcion) {
        this.idArchivoRecepcion = idArchivoRecepcion;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getCodigoSucursal() {
        return codigoSucursal;
    }

    public void setCodigoSucursal(String codigoSucursal) {
        this.codigoSucursal = codigoSucursal;
    }



    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }


    public Character getCodigoOperacion() {
        return codigoOperacion;
    }

    public void setCodigoOperacion(Character codigoOperacion) {
        this.codigoOperacion = codigoOperacion;
    }

    public String getCategoriaEstadoCuenta() {
        return categoriaEstadoCuenta;
    }

    public void setCategoriaEstadoCuenta(String categoriaEstadoCuenta) {
        this.categoriaEstadoCuenta = categoriaEstadoCuenta;
    }

    public String getCategoriaBalance() {
        return categoriaBalance;
    }

    public void setCategoriaBalance(String categoriaBalance) {
        this.categoriaBalance = categoriaBalance;
    }

    public String getCategoriaFinanciacion() {
        return categoriaFinanciacion;
    }

    public void setCategoriaFinanciacion(String categoriaFinanciacion) {
        this.categoriaFinanciacion = categoriaFinanciacion;
    }


    public String getFechaTransaccion() {
        return fechaTransaccion;
    }

    public void setFechaTransaccion(String fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    public BigDecimal getImporteOrigen() {
        return importeOrigen;
    }

    public void setImporteOrigen(BigDecimal importeOrigen) {
        this.importeOrigen = importeOrigen;
    }

    public String getMonedaOrigen() {
        return monedaOrigen;
    }

    public void setMonedaOrigen(String monedaOrigen) {
        this.monedaOrigen = monedaOrigen;
    }

    public BigDecimal getImporteBalance() {
        return importeBalance;
    }

    public void setImporteBalance(BigDecimal importeBalance) {
        this.importeBalance = importeBalance;
    }

    public String getMonedaBalance() {
        return monedaBalance;
    }


    public BigDecimal getImporteLiquidacion() {
        return importeLiquidacion;
    }

    public void setImporteLiquidacion(BigDecimal importeLiquidacion) {
        this.importeLiquidacion = importeLiquidacion;
    }

    public String getMonedaLiquidacion() {
        return monedaLiquidacion;
    }

    public void setMonedaLiquidacion(String monedaLiquidacion) {
        this.monedaLiquidacion = monedaLiquidacion;
    }

    public Character getAlcanceTransaccion() {
        return alcanceTransaccion;
    }

    public void setAlcanceTransaccion(Character alcanceTransaccion) {
        this.alcanceTransaccion = alcanceTransaccion;
    }

    public String getNumeroLote() {
        return numeroLote;
    }

    public void setNumeroLote(String numeroLote) {
        this.numeroLote = numeroLote;
    }

    public String getModoTransaccion() {
        return modoTransaccion;
    }

    public void setModoTransaccion(String modoTransaccion) {
        this.modoTransaccion = modoTransaccion;
    }

    public String getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(String establecimiento) {
        this.establecimiento = establecimiento;
    }

    public String getCodigoMti() {
        return codigoMti;
    }

    public void setCodigoMti(String codigoMti) {
        this.codigoMti = codigoMti;
    }

    public String getCodigoProceso() {
        return codigoProceso;
    }

    public void setCodigoProceso(String codigoProceso) {
        this.codigoProceso = codigoProceso;
    }

    public String getCodigoAutorizacion() {
        return codigoAutorizacion;
    }

    public void setCodigoAutorizacion(String codigoAutorizacion) {
        this.codigoAutorizacion = codigoAutorizacion;
    }

    public String getIdentificadorTerminal() {
        return identificadorTerminal;
    }

    public void setIdentificadorTerminal(String identificadorTerminal) {
        this.identificadorTerminal = identificadorTerminal;
    }

    public String getTotalCuota() {
        return totalCuota;
    }

    public void setTotalCuota(String totalCuota) {
        this.totalCuota = totalCuota;
    }

    public String getNumeroCuota() {
        return numeroCuota;
    }

    public void setNumeroCuota(String numeroCuota) {
        this.numeroCuota = numeroCuota;
    }

    public BigDecimal getMontoAdicional() {
        return montoAdicional;
    }

    public void setMontoAdicional(BigDecimal montoAdicional) {
        this.montoAdicional = montoAdicional;
    }

    public BigDecimal getImporteCashBack() {
        return importeCashBack;
    }

    public void setImporteCashBack(BigDecimal importeCashBack) {
        this.importeCashBack = importeCashBack;
    }

    public String getCapacidadTerminal() {
        return capacidadTerminal;
    }

    public void setCapacidadTerminal(String capacidadTerminal) {
        this.capacidadTerminal = capacidadTerminal;
    }

    public String getCondicionIngreso() {
        return condicionIngreso;
    }

    public void setCondicionIngreso(String condicionIngreso) {
        this.condicionIngreso = condicionIngreso;
    }

    public long getCuentaTarjeta() {
        return cuentaTarjeta;
    }

    public void setCuentaTarjeta(long cuentaTarjeta) {
        this.cuentaTarjeta = cuentaTarjeta;
    }

    public String getCuentaTarjetaReducido() {
        return cuentaTarjetaReducido;
    }

    public void setCuentaTarjetaReducido(String cuentaTarjetaReducido) {
        this.cuentaTarjetaReducido = cuentaTarjetaReducido;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }



    public String getCodigoAceptanteTarjeta() {
        return codigoAceptanteTarjeta;
    }

    public void setCodigoAceptanteTarjeta(String codigoAceptanteTarjeta) {
        this.codigoAceptanteTarjeta = codigoAceptanteTarjeta;
    }

    public Character getImporteOrigenSigno() {
        return importeOrigenSigno;
    }

    public void setImporteOrigenSigno(Character importeOrigenSigno) {
        this.importeOrigenSigno = importeOrigenSigno;
    }

    public String getImporteOrigenTexto() {
        return importeOrigenTexto;
    }

    public void setImporteOrigenTexto(String importeOrigenTexto) {
        this.importeOrigenTexto = importeOrigenTexto;
    }

    public Character getImporteBalanceSigno() {
        return importeBalanceSigno;
    }

    public void setImporteBalanceSigno(Character importeBalanceSigno) {
        this.importeBalanceSigno = importeBalanceSigno;
    }

    public String getImporteBalanceTexto() {
        return importeBalanceTexto;
    }

    public void setImporteBalanceTexto(String importeBalanceTexto) {
        this.importeBalanceTexto = importeBalanceTexto;
    }

    public Character getImporteLiquidacionSigno() {
        return importeLiquidacionSigno;
    }

    public void setImporteLiquidacionSigno(Character importeLiquidacionSigno) {
        this.importeLiquidacionSigno = importeLiquidacionSigno;
    }

    public String getImporteLiquidacionTexto() {
        return importeLiquidacionTexto;
    }

    public void setImporteLiquidacionTexto(String importeLiquidacionTexto) {
        this.importeLiquidacionTexto = importeLiquidacionTexto;
    }

    public Character getMontoAdicionalSigno() {
        return montoAdicionalSigno;
    }

    public void setMontoAdicionalSigno(Character montoAdicionalSigno) {
        this.montoAdicionalSigno = montoAdicionalSigno;
    }

    public String getMontoAdicionalTexto() {
        return montoAdicionalTexto;
    }

    public void setMontoAdicionalTexto(String montoAdicionalTexto) {
        this.montoAdicionalTexto = montoAdicionalTexto;
    }

    public Character getImporteCashBackSigno() {
        return importeCashBackSigno;
    }

    public void setImporteCashBackSigno(Character importeCashBackSigno) {
        this.importeCashBackSigno = importeCashBackSigno;
    }

    public String getImporteCashBackTexto() {
        return importeCashBackTexto;
    }

    public void setImporteCashBackTexto(String importeCashBackTexto) {
        this.importeCashBackTexto = importeCashBackTexto;
    }

    public void setMonedaBalance(String monedaBalance) {
        this.monedaBalance = monedaBalance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MovimientosTC that = (MovimientosTC) o;

        if (getCuentaTarjeta() != that.getCuentaTarjeta()) return false;
        if (!getCodigoBanco().equals(that.getCodigoBanco())) return false;
        if (!getCodigoSucursal().equals(that.getCodigoSucursal())) return false;
        if (!getNumeroTarjeta().equals(that.getNumeroTarjeta())) return false;
        if (!getCodigoAceptanteTarjeta().equals(that.getCodigoAceptanteTarjeta())) return false;
        if (!getCodigoOperacion().equals(that.getCodigoOperacion())) return false;
        if (!getCategoriaEstadoCuenta().equals(that.getCategoriaEstadoCuenta())) return false;
        if (!getCategoriaBalance().equals(that.getCategoriaBalance())) return false;
        if (!getCategoriaFinanciacion().equals(that.getCategoriaFinanciacion())) return false;
        if (!getFechaTransaccion().equals(that.getFechaTransaccion())) return false;
        if (!getImporteOrigenSigno().equals(that.getImporteOrigenSigno())) return false;
        if (!getImporteOrigenTexto().equals(that.getImporteOrigenTexto())) return false;
        if (!getImporteOrigen().equals(that.getImporteOrigen())) return false;
        if (!getMonedaOrigen().equals(that.getMonedaOrigen())) return false;
        if (!getImporteBalanceSigno().equals(that.getImporteBalanceSigno())) return false;
        if (!getImporteBalanceTexto().equals(that.getImporteBalanceTexto())) return false;
        if (!getImporteBalance().equals(that.getImporteBalance())) return false;
        if (!getMonedaBalance().equals(that.getMonedaBalance())) return false;
        if (!getImporteLiquidacionSigno().equals(that.getImporteLiquidacionSigno())) return false;
        if (!getImporteLiquidacionTexto().equals(that.getImporteLiquidacionTexto())) return false;
        if (!getImporteLiquidacion().equals(that.getImporteLiquidacion())) return false;
        if (!getMonedaLiquidacion().equals(that.getMonedaLiquidacion())) return false;
        if (!getAlcanceTransaccion().equals(that.getAlcanceTransaccion())) return false;
        if (!getNumeroLote().equals(that.getNumeroLote())) return false;
        if (!getModoTransaccion().equals(that.getModoTransaccion())) return false;
        if (!getEstablecimiento().equals(that.getEstablecimiento())) return false;
        if (!getCodigoMti().equals(that.getCodigoMti())) return false;
        if (!getCodigoProceso().equals(that.getCodigoProceso())) return false;
        if (!getCodigoAutorizacion().equals(that.getCodigoAutorizacion())) return false;
        if (!getIdentificadorTerminal().equals(that.getIdentificadorTerminal())) return false;
        if (!getTotalCuota().equals(that.getTotalCuota())) return false;
        if (!getNumeroCuota().equals(that.getNumeroCuota())) return false;
        if (!getMontoAdicionalSigno().equals(that.getMontoAdicionalSigno())) return false;
        if (!getMontoAdicionalTexto().equals(that.getMontoAdicionalTexto())) return false;
        if (!getMontoAdicional().equals(that.getMontoAdicional())) return false;
        if (!getImporteCashBackSigno().equals(that.getImporteCashBackSigno())) return false;
        if (!getImporteCashBackTexto().equals(that.getImporteCashBackTexto())) return false;
        if (!getImporteCashBack().equals(that.getImporteCashBack())) return false;
        if (!getCapacidadTerminal().equals(that.getCapacidadTerminal())) return false;
        if (!getCondicionIngreso().equals(that.getCondicionIngreso())) return false;
        if (!getCuentaTarjetaReducido().equals(that.getCuentaTarjetaReducido())) return false;
        if (!getEstado().equals(that.getEstado())) return false;
        if (!getIdArchivoRecepcion().equals(that.getIdArchivoRecepcion())) return false;
        if (!getFechaRecepcion().equals(that.getFechaRecepcion())) return false;
        if (!getUsuarioCreador().equals(that.getUsuarioCreador())) return false;
        if (!getFechaCreacion().equals(that.getFechaCreacion())) return false;
        if (!getUsuarioModificador().equals(that.getUsuarioModificador())) return false;
        if (!getFechaModificacion().equals(that.getFechaModificacion())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = getCodigoBanco().hashCode();
        result = 31 * result + getCodigoSucursal().hashCode();
        result = 31 * result + getNumeroTarjeta().hashCode();
        result = 31 * result + getCodigoAceptanteTarjeta().hashCode();
        result = 31 * result + getCodigoOperacion().hashCode();
        result = 31 * result + getCategoriaEstadoCuenta().hashCode();
        result = 31 * result + getCategoriaBalance().hashCode();
        result = 31 * result + getCategoriaFinanciacion().hashCode();
        result = 31 * result + getFechaTransaccion().hashCode();
        result = 31 * result + getImporteOrigenSigno().hashCode();
        result = 31 * result + getImporteOrigenTexto().hashCode();
        result = 31 * result + getImporteOrigen().hashCode();
        result = 31 * result + getMonedaOrigen().hashCode();
        result = 31 * result + getImporteBalanceSigno().hashCode();
        result = 31 * result + getImporteBalanceTexto().hashCode();
        result = 31 * result + getImporteBalance().hashCode();
        result = 31 * result + getMonedaBalance().hashCode();
        result = 31 * result + getImporteLiquidacionSigno().hashCode();
        result = 31 * result + getImporteLiquidacionTexto().hashCode();
        result = 31 * result + getImporteLiquidacion().hashCode();
        result = 31 * result + getMonedaLiquidacion().hashCode();
        result = 31 * result + getAlcanceTransaccion().hashCode();
        result = 31 * result + getNumeroLote().hashCode();
        result = 31 * result + getModoTransaccion().hashCode();
        result = 31 * result + getEstablecimiento().hashCode();
        result = 31 * result + getCodigoMti().hashCode();
        result = 31 * result + getCodigoProceso().hashCode();
        result = 31 * result + getCodigoAutorizacion().hashCode();
        result = 31 * result + getIdentificadorTerminal().hashCode();
        result = 31 * result + getTotalCuota().hashCode();
        result = 31 * result + getNumeroCuota().hashCode();
        result = 31 * result + getMontoAdicionalSigno().hashCode();
        result = 31 * result + getMontoAdicionalTexto().hashCode();
        result = 31 * result + getMontoAdicional().hashCode();
        result = 31 * result + getImporteCashBackSigno().hashCode();
        result = 31 * result + getImporteCashBackTexto().hashCode();
        result = 31 * result + getImporteCashBack().hashCode();
        result = 31 * result + getCapacidadTerminal().hashCode();
        result = 31 * result + getCondicionIngreso().hashCode();
        result = 31 * result + (int) (getCuentaTarjeta() ^ (getCuentaTarjeta() >>> 32));
        result = 31 * result + getCuentaTarjetaReducido().hashCode();
        result = 31 * result + getEstado().hashCode();
        result = 31 * result + getIdArchivoRecepcion().hashCode();
        result = 31 * result + getFechaRecepcion().hashCode();
        result = 31 * result + getUsuarioCreador().hashCode();
        result = 31 * result + getFechaCreacion().hashCode();
        result = 31 * result + getUsuarioModificador().hashCode();
        result = 31 * result + getFechaModificacion().hashCode();
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getCodigoAdm() {
        return codigoAdm;
    }

    public void setCodigoAdm(Character codigoAdm) {
        this.codigoAdm = codigoAdm;
    }

    public int getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(int fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getComprobante() {
        return comprobante;
    }

    public void setComprobante(String comprobante) {
        this.comprobante = comprobante;
    }
}
