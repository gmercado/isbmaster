package com.bisa.bus.servicios.tc.linkser.sched;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.bus.servicios.tc.linkser.api.SftpTransferenciaAtcLinkser;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 22/11/2017.
 */
public class TransferenciaAtcLinkser implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransferenciaAtcLinkser.class);

    private final MedioAmbiente medioAmbiente;
    private final NotificacionesCorreo notificacionesCorreo;
    private final SftpTransferenciaAtcLinkser sftpTransferenciaAtcLinkser;

    private volatile boolean ok;

    @Inject
    public TransferenciaAtcLinkser(MedioAmbiente medioAmbiente, NotificacionesCorreo notificacionesCorreo,
                                   SftpTransferenciaAtcLinkser sftpTransferenciaAtcLinkser){
        this.medioAmbiente = medioAmbiente;
        this.notificacionesCorreo = notificacionesCorreo;
        this.sftpTransferenciaAtcLinkser = sftpTransferenciaAtcLinkser;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        // Procesando datos generales
        LOGGER.info("Inicio de proceso transferencia archivo atc - linkser");
        ok = sftpTransferenciaAtcLinkser.procesarArchivo();
        LOGGER.info("Respuesta de archivo procesado transferencia archivo atc - linkser: {}", ok);

        // Completar proceso de carga notificando al operador
        if(ok){
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_TRANSFERENCIA_ATC_LINKSER, Variables.FTP_MENSAJE_EMAIL_TRANSFERENCIA_ATC_LINKSER_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador(sftpTransferenciaAtcLinkser.getNombreArchivoProcesado() , TipoArchivo.TLI, mensaje);
        }else{
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_TRANSFERENCIA_ATC_LINKSER_ERROR, Variables.FTP_MENSAJE_EMAIL_TRANSFERENCIA_ATC_LINKSER_ERROR_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador("", TipoArchivo.TLI, mensaje);
        }
    }
}