package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import java.util.List;

/**
 * Created by atenorio on 29/06/2017.
 */
public class TotalMovimientosEmisorCsv {
    @FormatCsv(name = "banco", nullable = false, length = 4)
    private Short codigoBanco;
    @FormatCsv(name = "fecha", nullable = false, length = 4)
    private int fechaProceso;
    @FormatCsv(name = "cantidad", nullable = false, length = 4)
    private long cantidad;
    @FormatCsv(name = "espacio", nullable = false, length = 4)
    private String espacio;

    private List<MovimientoMensualEmisorCsv> movimientos;

    public Short getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(Short codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public int getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(int fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public List<MovimientoMensualEmisorCsv> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(List<MovimientoMensualEmisorCsv> movimientos) {
        this.movimientos = movimientos;
    }

    public String getEspacio() {
        return espacio;
    }

    public void setEspacio(String espacio) {
        this.espacio = espacio;
    }
}
