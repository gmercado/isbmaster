package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.DatosGeneralesHistoricoTC;
import com.bisa.bus.servicios.tc.sftp.entities.IdDatosGenerales;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 09/06/2017.
 */
public class DatosGeneralesHistoricoDao extends DaoImpl<DatosGeneralesHistoricoTC, IdDatosGenerales> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatosGeneralesHistoricoDao.class);

    @Inject
    protected DatosGeneralesHistoricoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }


    public boolean guardar(DatosGeneralesHistoricoTC registro, String user) {

        registro.setUsuarioCreador(user);
        registro.setFechaModificacion(new Date());
        persist(registro);

        return true;
    }
}