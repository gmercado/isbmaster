package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by atenorio on 20/06/2017.
 * ATCUC80 Registros de acumulado de movimientos finnacieros agrupados por cuenta y tipo de movimiento
 */
@Entity
@Table(name = "TCPANMOME")
public class MovimientoMensual implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MMID", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Basic(optional = false)
    @Column(name = "MMNUTCNC", nullable = false, length = 10)
    private String numeroCuenta;

    @Basic(optional = false)
    @Column(name = "MMCAESCU", nullable = false, length = 4)
    private String categoriaEstadoCuenta;

    @Basic(optional = false)
    @Column(name = "MMCABA", nullable = false, length = 4)
    private String categoriaBalance;
    @Basic(optional = false)
    @Column(name = "MMCAFI", nullable = false, length = 4)
    private String categoriaFinanc;

    @Basic(optional = false)
    @Column(name = "MMFEPRO1", nullable = false)
    private int fechaProceso;

    @Basic(optional = false)
    @Column(name = "MMCADM", nullable = false)
    private Character mmcadm;


    @Basic(optional = false)
    @Column(name = "MMBAEM", nullable = false, length = 4)
    private String codigoBanco;
    @Basic(optional = false)
    @Column(name = "MMSUEM", nullable = false, length = 4)
    private String codigoSucursal;

    @Basic(optional = false)
    @Column(name = "MMCOOP", nullable = false)
    private Character codigoOperacion;
    @Basic(optional = false)
    @Column(name = "MMIMMOLIS", nullable = false)
    private Character importeBsSigno;
    @Basic(optional = false)
    @Column(name = "MMIMMOLI", nullable = false, length = 10)
    private String importeBsTexto;
    @Basic(optional = false)
    @Column(name = "MMIMMOUSS", nullable = false)
    private Character importeUsSigno;
    @Basic(optional = false)
    @Column(name = "MMIMMOUS", nullable = false, length = 10)
    private String importeUsTexto;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "MMIMMOLIN", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeBs;
    @Basic(optional = false)
    @Column(name = "MMIMMOUSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeUs;
    @Basic(optional = false)
    @Column(name = "MMNUTC", nullable = false)
    private long cuentaTarjeta;
    @Basic(optional = false)
    @Column(name = "MMNUT1", nullable = false, length = 9)
    private String cuentaTarjetaReducida = "";
    @Basic(optional = false)
    @Column(name = "MMEST", nullable = false, length = 4)
    private String estado;
    @Basic(optional = false)
    @Column(name = "MMIDRE", nullable = false)
    private long idArchivoRecepcion;
    @Basic(optional = false)
    @Column(name = "MMFERE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRecepcion;
    @Basic(optional = false)
    @Column(name = "REPGCR", nullable = false, length = 10)
    private String usuarioCreador;
    @Basic(optional = false)
    @Column(name = "REFECR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @Column(name = "REPGMO", nullable = false, length = 10)
    private String usuarioModificador;
    @Basic(optional = false)
    @Column(name = "REFEMO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getCodigoSucursal() {
        return codigoSucursal;
    }

    public void setCodigoSucursal(String codigoSucursal) {
        this.codigoSucursal = codigoSucursal;
    }

    public Character getCodigoOperacion() {
        return codigoOperacion;
    }

    public void setCodigoOperacion(Character codigoOperacion) {
        this.codigoOperacion = codigoOperacion;
    }

    public Character getImporteBsSigno() {
        return importeBsSigno;
    }

    public void setImporteBsSigno(Character importeBsSigno) {
        this.importeBsSigno = importeBsSigno;
    }

    public String getImporteBsTexto() {
        return importeBsTexto;
    }

    public void setImporteBsTexto(String importeBsTexto) {
        this.importeBsTexto = importeBsTexto;
    }

    public Character getImporteUsSigno() {
        return importeUsSigno;
    }

    public void setImporteUsSigno(Character importeUsSigno) {
        this.importeUsSigno = importeUsSigno;
    }

    public String getImporteUsTexto() {
        return importeUsTexto;
    }

    public void setImporteUsTexto(String importeUsTexto) {
        this.importeUsTexto = importeUsTexto;
    }



    public BigDecimal getImporteBs() {
        return importeBs;
    }

    public void setImporteBs(BigDecimal importeBs) {
        this.importeBs = importeBs;
    }

    public BigDecimal getImporteUs() {
        return importeUs;
    }

    public void setImporteUs(BigDecimal importeUs) {
        this.importeUs = importeUs;
    }

    public long getCuentaTarjeta() {
        return cuentaTarjeta;
    }

    public void setCuentaTarjeta(long cuentaTarjeta) {
        this.cuentaTarjeta = cuentaTarjeta;
    }

    public String getCuentaTarjetaReducida() {
        return cuentaTarjetaReducida;
    }

    public void setCuentaTarjetaReducida(String cuentaTarjetaReducida) {
        this.cuentaTarjetaReducida = cuentaTarjetaReducida;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public long getIdArchivoRecepcion() {
        return idArchivoRecepcion;
    }

    public void setIdArchivoRecepcion(long idArchivoRecepcion) {
        this.idArchivoRecepcion = idArchivoRecepcion;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getCategoriaEstadoCuenta() {
        return categoriaEstadoCuenta;
    }

    public void setCategoriaEstadoCuenta(String categoriaEstadoCuenta) {
        this.categoriaEstadoCuenta = categoriaEstadoCuenta;
    }

    public String getCategoriaBalance() {
        return categoriaBalance;
    }

    public void setCategoriaBalance(String categoriaBalance) {
        this.categoriaBalance = categoriaBalance;
    }

    public String getCategoriaFinanc() {
        return categoriaFinanc;
    }

    public void setCategoriaFinanc(String categoriaFinanc) {
        this.categoriaFinanc = categoriaFinanc;
    }

    public int getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(int fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public Character getMmcadm() {
        return mmcadm;
    }

    public void setMmcadm(Character mmcadm) {
        this.mmcadm = mmcadm;
    }
}
