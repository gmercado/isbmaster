package com.bisa.bus.servicios.tc.sftp.model;

/**
 * Created by atenorio on 06/06/2017.
 */
public enum CodigoErrorATC {

    NO_ERROR("000"), //Sin Error
    MALFORMED_REGISTRY("001"), //Registo mal formado, no pasa validación física
    INVALID_REGISTRY ("002"), //Registro inválido. Alguno de los datos no tiene el formato correcto o no está dentro de los valores permitidos.
    NEXT_REGISTRY_EXPECTED ("003"), //Se espera un proximo registro
    GA_CF_CJ_PM_PR_REGISTRY_EXPECTED("004"), //Registro invalido según la estructura definida.
    PR_CF_GA_OR_NULL_REGISTRY_EXPECTED("005"), //Se espera un registro vacio o un PR o un CF
    PR_CL_CF_GA_OR_NULL_REGISTRY_EXPECTED("006"),
    PR_CL_OR_CF_REGISTRY_EXPECTED("007"),
    PR_OR_CF_REGISTRY_EXPECTED("008"),
    CF_GA_OR_NULL_REGISTRY_EXPECTED("009"),
    PR_CL_CF_OR_CJ_REGISTRY_EXPECTED("010"),
    PR_CF_OR_CJ_REGISTRY_EXPECTED("011"),
    CL_CF_GA_OR_NULL_REGISTRY_EXPECTED("012"),
    CL_OR_CF_REGISTRY_EXPECTED("013"),
    CL_CF_OR_CJ_REGISTRY_EXPECTED("014"),
    INVALID_ACTION_TYPE("015"),
    CC_CF_OR_CJ_REGISTRY_EXPECTED("016"),
    CF_OR_CJ_REGISTRY_EXPECTED("017"),
    GA_OR_NULL_REGISTRY_EXPECTED("018"),

        /*050 - GA - 099*/
    GA_NO_EXIST_PRODUCT_GROUP("050"), //Grupo de productos no existe
    GA_INVALID_PRODUCT_GROUP_TYPE_ACCOUNT("051"), //El tipo de cuenta indicado no corresponde al grupo de productos seleccionado
    GA_INVALID_RESPONSIBLE_TYPE("052"), //El Tipo de responsable indicado no es correcto
    GA_CAN_NOT_GENERATE_ACCOUNT_STATUS("053"), //La opción de generar estado de cuenta impreso es inválida para el tipo de producto de la cuenta.
    GA_CAN_NOT_SEND_ACCOUNT_STATUS("054"), //La opción de enviar estado de cuenta por mail es inválida para el tipo de producto de la cuenta.
    GA_MANDATORY_MAIL("055"), //El ingreso de dirección para envío de mail es obligatorio cuando Enviar estado de cuenta por mail es Sí.
    GA_REGISTRY_EXPECTED("056"),//Se espera un registro del tipo GA
    GA_INVALID_PRODUCT_TYPE("057"), //General Account Product Tipe inválido
    GA_INVALID_CUSTOMER_TYPE("058"), //General Account Customer Type Inválido
    GA_ACCOUNT_NOT_FOUND("059"), //No se pudo encontrar la account asociada al AccountNumber
    GA_INVALID_STATUS_TRANSITION("060"), //No se puede dar de baja una cuenta porque no es posible hacer la transicion de estados
    GA_INVALID_ISSUER_CODE("061"), //Issuer Invalido
    GA_CANCEL_STATUS_TYPE_NOT_CONFIGURED_FOR_ISSUER("062"), //No se configuro la transicion de estados para el issuer
    GA_INVALID_PRODUCT_GROUP("063"),
    GA_INVALID_PRODUCT_GROUP_FOR_ACCOUNT_PRODUCT_TYPE("064"),
    GA_INVALID_ISSUER_BRANCH_CODE("065"),
    GA_INVALID_ISSUER_BRANCH_AGENCY_CODE("066"),


        /*100 - CC - 149*/
    CC_SETTLEMENT_MODEL_NOT_FOUND("100"), //No se encontró el modelo de liquidación indicado.
    CC_CLOSE_DATE_CODE_NOT_FOUND("101"), //No se encontró la fecha de cierre indicada.
    CC_INVALID_PAYMENT_METHOD("102"), //La opción seleccionada para el campo PaymentMethod no es válida
    CC_INVALID_DEBIT_TYPE("103"), //La opción seleccionada para el campo DebitType no es válida
    CC_DEBIT_PREPAY_ACCOUNT("104"), //Se encontró un registro CC para cuentas débito o prepago
    CC_MUST_BE_UNIQUE_BY_ACCOUNT("105"), //Se encontró más de un registro CC para cuenta crédito
    CC_REGISTRY_EXPECTED("106"), //Se espera un registro CC
    CC_OR_CJ_REGISTRY_EXPECTED("107"), //Se espera un registro CJ o CC
    CC_INVALID_CLOSE_DATE_CODE("108"), //Campo CloseDateCode inválido
    CC_INVALID_CREDIT_LIMIT("109"),
    CC_INVALID_SETTLEMENT_MODEL("110"),
    CC_DEBIT_TYPE_REQUIRED("111"),

        /*150 - DC - 199*/
    DC_MANDATORY_DEBIT_REGISTRY("150"), //No se cargaron registros DC a pesar de que es obligatorio dado el PaymentMethod seleccionado.
    DC_INVALID_DEBIT_REGISTRY("151"), //Se incluyeron registros DC incorrectamente para el tipo de producto / forma de pago"),
    DC_DEBIT_REGISTRY_MUST_BE_UNIQUE_BY_CURRENCY("152"), //Se debe incluir un único registro DC por cada moneda de liquidación para cuentas crédito.
    DC_BANK_NOT_FOUND("153"), //No se encontró el banco indicado en el sistema
    DC_BANK_AGENCY_NOT_FOUND("154"), //No se encontró la agencia bancaria indicada en el sistema
    DC_INVALID_BANK_ACCOUNT_TYPE("155"), //El tipo de cuenta indicado no es correcto conforme layout
    DC_REQUIRED_BANK_ACCOUNT_HOLDER("156"), //Se debe ingresar el nombre del titular de la cuenta para cuentas de débito del balance crédito.
    DC_OR_CF_REGISTRY_EXPECTED("157"), //Se espera un registro DC o CF
    DC_REGISTRY_EXPECTED("158"), //Se espera un registro DC
    DC_REQUIRED_BANK_CODE("159"),
    DC_REQUIRED_BANK_ACCOUNT_NUMBER("160"),
    DC_REQUIRED_BANK_AGENCY_CODE("161"),
    DC_INVALID_CURRENCY("162"),
    DC_CURRENCY_NOT_FOUND_IN_PRODUCT_GROUP_SETT_CURRENCIES("163"),
    DC_DUPLICATED_CURRENCY("164"),
    DC_CASH_PAYMENT_METHOD_NOT_ALLOW_DEBIT("165"),

        /*200 - PR - 249*/
    PR_INVALID_PRODUCT("201"), //El producto seleccionado para dar el alta debe formar parte del grupo de productos seleccionado para la cuenta
    PR_INVALID_AFFINITY_GROUP("202"),//El grupo de afinidad solicitado para la tarjeta debe ser uno de los configurados para el producto
    PR_CREDIT_CARD_WITH_SAME_PRODUCT_AND_CLIENT_ON_ACCOUNT("203"), //Ya se solicitó una tarjeta con el mismo producto para el mismo cliente dentro de la cuenta
    PR_BIN_NOT_CONFIGURED_FOR_ISSUER("204"), //El bin del número de tarjeta externo debe estar en la lista de los bines configurados para el emisor
    PR_INVALID_EXPIRATION_DATE("205"), //La fecha de vencimiento no es válida"), o es en el pasado
    PR_MANDATORY_EXTERNAL_CARD_NUMBER_AND_EXTERNAL_DUE_DATE("206"), //Los campos ExternalCardNumber y ExternalDueDate son obligatorios para el producto seleccionado
    PR_EXTERNAL_CARD_NUMBER_AND_EXTERNAL_DUE_DATE_MUST_BE_UNIQUE("207"), //Ya existe otra tarjeta en la base de datos con el mismo ExternalCardNumber / ExternalDueDate

    PR_ACCOUNT_NOT_FOUND("208"), //No se pudo obtener la cuenta
    PR_INVALID_ISSUER_CODE("209"), //El código del emisor ingresado en el archivo es distinto al emisor para el que se ejecuta el batch.
    PR_INVALID_DOCUMENT_TYPE("210"), //Tipo de documento invalido
    PR_CUSTOMER_NOT_FOUND("211"), //No se encontro el cliente para el documento y el tipo de documento ingresados en el archivo
    PR_REQUIRED_AFFINITY_GROUP("212"), //Grupo de afinidad requerido
    PR_REQUIRED_PRODUCT_CODE("213"), //Product Code requerido
    PR_PRODUCT_CODE_NOT_IN_ACCOUNT_PRODUCT_GROUP("214"), //El producto ingresado no pertenece al grupo de productos de la cuenta
    PR_AFFINITY_GROUP_NOT_CONTAIN_PRODUCT("215"), //El grupo de afinidad ingresado no existe o no contiene al producto ingresado.
    PR_PRODUCT_NOT_IN_PRIMARY_RESPONSIBLE_PRODUCTS("216"), //El Producto ingresado debe estar asociado al titular de la cuenta si quiero asignarlo a un adicional

    PR_INVALID_EXT_DUE_DATE("217"),
    PR_DUPLICATED_PRODUCT("218"),
    PR_PRODUCT_NOT_IN_PRODUCT_GROUP("219"),
    PR_INVALID_EXTERNAL_CARD_NUMBER("220"),
    PR_INVALID_EXTERNAL_TRANSACTION_MASKED_PAN("221"),
    PR_INVALID_EXTERNAL_BIN("222"),
    PR_INVALID_EXTERNAL_DUE_DATE("223"),

        /*250 - CF - 299*/
    CF_REGISTRY_EXPECTED("250"), //Se espera un registro CF
    CF_NOT_UNIQUE_PRIMARY_RESPONSIBLE("251"), //Existe más de un cliente Titular para la cuenta
    CF_NOT_UNIQUE_SECONDARY_RESPONSIBLE("252"), //Existe más de un cliente Conyuge para la cuenta
    CF_INVALID_CUSTOMER_TYPE("253"), //Customer Type inválido para cuenta corporativa
    CF_MAIN_RESP_CANT_HAVE_SAME_PHYSICAL_ADDRESS_AS_MAIN_RESP("254"), //El Titular no puede tener en True la opción SamePhysicalAddressAsMainResp


    CF_INVALID_DOCUMENT_TYPE("255"),
    CF_INVALID_DOCUMENT("256"),
    CF_INVALID_CUSTOMER_NAME("257"),
    CF_INVALID_CUSTOMER_SURNAME("258"),
    CF_INVALID_NATIONALITY("259"),
    CF_INVALID_BIRTH_DATE("260"),
    CF_INVALID_GENDER("261"),
    CF_INVALID_MARITIAL_STATUS("262"),
    CF_DUPLICATED_DOCUMENT("263"),
    CF_EI_REGISTRY_EXPECTED("264"),
    CF_AD_REGISTRY_EXPECTED("265"),
    CF_PR_REGISTRY_EXPECTED("266"),
    CF_NOT_POSSIBLE_TO_ADD_PRIMARY_RESPONSIBLE_CUSTOMER("267"),
    CF_NOT_POSSIBLE_TO_ADD_ADDITIONAL_PREPAY_ACCOUNT("268"),
    CF_NOT_POSSIBLE_TO_ADD_SECONDARY_RESPONSIBLE_DEBIT_ACCOUNT("269"),
    CF_NOT_POSSIBLE_TO_ADD_SECONDARY_RESPONSIBLE_CORPORATE_ACCOUNT("270"),
    CF_INVALID_ISSUER_CODE("271"),
    CF_ACCOUNT_NOT_FOUND("272"),
    CF_ADDITIONAL_DOCUMENT_CAN_NOT_BE_EMPTY("273"),
    CF_INVALID_ADDITIONAL_DOCUMENT("274"),
    CF_NOT_POSSIBLE_TO_ADD_SECONDARY_RESPONSIBLE_ALREADY_EXISTS("275"),
    CF_CAN_NOT_USE_PRIMARY_ADDRESS_IN_CORPORATE_ACCOUNT("276"),
    CF_CUSTOMER_NOT_FOUND("277"),
    CF_INVALID_ADDITIONAL_DOCUMENT_TYPE("278"),

        /*300 - CJ - 349*/
    CJ_REGISTRY_EXPECTED("300"), //Se espera un registro CJ
    CJ_INVALID_DOCUMENT_TYPE("301"),
    CJ_INVALID_DOCUMENT("302"),
    CJ_INVALID_LEGAL_NAME("303"),
    CJ_INVALID_START_ACTIVITY_DATE("304"),
    CJ_INVALID_FOUNDATION_DATE("305"),
    CJ_INVALID_ACTIVITY_COUNTRY_CODE("306"),
    CJ_INVALID_FOUNDATION_COUNTRY_CODE("307"),
    CJ_INVALID_MERCHANT_CATEGORY_CODE("308"),
    CJ_INVALID_BUSINESS_ENTITY_TYPE_CODE("309"),
    CJ_INVALID_ACTIVITY_SCOPE("310"),
    CJ_INVALID_ANUAL_BILLING_CURRENCY("311"),
    CJ_INVALID_NET_WORTH_CURRENCY("312"),
    CJ_ACCOUNT_NOT_FOUND("313"),
    CJ_INVALID_ISSUER_CODE("314"),
    CJ_LEGAL_CUSTOMER_NOT_FOUND("315"),

        /*350 - EI - 399*/
    EI_REGISTRY_EXPECTED("350"), //Se espera un registro EI
    EI_INVALID_CONTRACT_DUE_DATE("351"),
    EI_INVALID_DOCUMENT_TYPE("352"),
    EI_INVALID_DOCUMENT("353"),
    EI_INVALID_REGISTRATION_DATE("354"),
    EI_INVALID_TOTAL_INCOMES("355"),
    EI_INVALID_OCUPATION_CODE("356"),
    EI_INVALID_SECTOR_CODE("357"),

        /*400 - AD - 449*/
    AD_REGISTRY_EXPECTED("400"), //Se espera un registro AD
    AD_INVALID_CUSTOMER_ADDRESS_TYPE("401"), // CustomerAddressType inválido
    AD_DUPLICATED_AD_CUSTOMER_ADDRESS_TYPE("402"), //Ya existe un addres para ese cliente con igual CustomerAddressType
    AD_PHYSICAL_ADDRESS_NOT_FOUND("403"),
    //AD_LABORAL_ADDRESS_NOT_FOUND("404"),
    AD_CORRESPONDANCE_ADDRESS_NOT_FOUND("405"),
    AD_INVALID_STREET_TYPE("406"),
    AD_INVALID_STREET("407"),
    AD_INVALID_DOOR_NUMBER("408"),
    AD_INVALID_NEIGHBOURHOOD("409"),
    AD_INVALID_COUNTRY_CODE("410"),
    AD_INVALID_GEOGRAPHIC_CODE("411"),
    AD_INVALID_CITY_CODE("412"),
    AD_LABORAL_ADDRESS_NOT_REQUIRED("413"),
    AD_PHYSICAL_ADDRESS_NOT_REQUIRED("414"),
    AD_CORRESPONDANCE_ADDRESS_NOT_REQUIRED("415"),

        /*450 - PR - 499*/
    PR_REGISTRY_EXPECTED("450"), //Se espera un registro PR

        /*500 - PM - 549*/
    PM_PAYMENT_MEDIA_NOT_FOUND("500"), //No se pudo encontrar el payment media para los datos suministrados
    PM_STATUS_TYPE_NOT_AVAILABLE_FOR_PAYMENT_MEDIA("501"), //No se encuentra un estado
    PM_REQUIRED_REASON_CODE("502"),
    PM_INVALID_NEW_DUE_DATE("503"),//lLa fecha de vencimiento fue ingresada incorrectamente (Vacia o con formato incorrecto
    PM_INVALID_EXTERNAL_BIN("504"),
    PM_INVALID_EXTERNAL_BIN_OR_MASKED_PAN_OR_NEW_CARD_NUMBER("505"),

        /*550 - CL - 599*/
    CL_INVALID_CREDIT_LINE_VALUE_TYPE("550"), //El tipo de linea de credito debe ser P o F
    CL_INVALID_CREDIT_LINE_CODE("551"),  //No se pudo encontrar el credit line code.
    CL_INVALID_CREDIT_LINE_VALUE("552"),
    CL_DUPLICATED_CODE("553"),

        /*900 - 999*/
    UNEXPECTED_ERROR("996"); // Error inesperado

    String codigo;
    CodigoErrorATC(String codigo){
        this.codigo=codigo;
    }
}
