package com.bisa.bus.servicios.tc.linkser.sched;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.bus.servicios.tc.linkser.api.SftpRecepcionMovimientos;
import com.bisa.bus.servicios.tc.linkser.api.SftpRecepcionDatosGenerales;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 20/11/2017.
 */
public class RecepcionCierreDiarioLinkser implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecepcionCierreDiarioLinkser.class);

    private final MedioAmbiente medioAmbiente;
    private final NotificacionesCorreo notificacionesCorreo;
    private final SftpRecepcionDatosGenerales iSftpRecepcionDatosGenerales;
    private final SftpRecepcionMovimientos iSftpRecepcionMovimientos;

    private volatile boolean ok;

    @Inject
    public RecepcionCierreDiarioLinkser(MedioAmbiente medioAmbiente, NotificacionesCorreo notificacionesCorreo,
                                        SftpRecepcionDatosGenerales iSftpRecepcionDatosGenerales,
                                        SftpRecepcionMovimientos iSftpRecepcionMovimientos){
        this.medioAmbiente = medioAmbiente;
        this.notificacionesCorreo = notificacionesCorreo;
        this.iSftpRecepcionDatosGenerales = iSftpRecepcionDatosGenerales;
        this.iSftpRecepcionMovimientos = iSftpRecepcionMovimientos;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        // Procesando datos generales
        LOGGER.info("Inicio de proceso recepcion datos generales");
        ok = iSftpRecepcionDatosGenerales.procesarArchivo();
        LOGGER.info("Respuesta de archivo procesada datos generales: {}", ok);
        // Procesando movimientos
        if(ok) {
            LOGGER.info("Inicio de proceso recepcion movimientos");
            ok = iSftpRecepcionMovimientos.procesarArchivo();
            LOGGER.info("Respuesta de archivo procesado recepcion movimientos: {}", ok);
        }

        // Completar proceso de carga notificando al operador
        if(ok){
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_OPERADOR_LINKSER, Variables.FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_OPERADOR_LINKSER_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador(iSftpRecepcionDatosGenerales.getNombreArchivoProcesado() + ", " + iSftpRecepcionMovimientos.getNombreArchivoProcesado(), TipoArchivo.MLI, mensaje);
        }else{
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_ERROR_OPERADOR_LINKSER, Variables.FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_ERROR_OPERADOR_LINKSER_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador("", TipoArchivo.MLI, mensaje);
        }
    }
}
