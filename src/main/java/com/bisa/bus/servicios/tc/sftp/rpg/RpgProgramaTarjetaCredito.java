package com.bisa.bus.servicios.tc.sftp.rpg;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.as400.AS400Factory;
import bus.monitor.rpg.CallProgramRPGv2;
import bus.monitor.rpg.ImposibleLlamarProgramaRpgException;
import bus.monitor.rpg.ParametrosRPG;
import com.google.inject.Inject;
import com.ibm.as400.access.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.TimeoutException;

/**
 * Created by atenorio on 30/05/2017.
 */
public class RpgProgramaTarjetaCredito {
    private static final Logger LOGGER = LoggerFactory.getLogger(RpgCargarSolicitud.class);

    private final CallProgramRPGv2 callProgramRPGv2;
    private final MedioAmbiente medioAmbiente;
    private final AS400Factory as400Factory;
    private String mensajeRespuesta;

    @Inject
    public RpgProgramaTarjetaCredito(AS400Factory as400factory, MedioAmbiente medioAmbiente,
                                     CallProgramRPGv2 callProgramRPGv2) throws ConnectionPoolException, ImposibleLlamarProgramaRpgException {
        this.as400Factory = as400factory;
        this.callProgramRPGv2 = callProgramRPGv2;
        this.medioAmbiente = medioAmbiente;
    }

    public boolean ejecutar(String libreriaPrograma, String programa) throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException, TimeoutException {
        String rutaPrograma = new QSYSObjectPathName(libreriaPrograma, programa, "PGM").getPath();
        return ejecutar(rutaPrograma);
    }

    public boolean ejecutar(String libreriaPrograma, String programa, int sizeInputParam, int sizeOutParam) throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException, TimeoutException {
        String rutaPrograma = new QSYSObjectPathName(libreriaPrograma, programa, "PGM").getPath();
        return ejecutarPgm(rutaPrograma, sizeInputParam, sizeOutParam);
    }

    public boolean ejecutar(String rutaPrograma) throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException, TimeoutException {
        boolean resultado = false;
        ParametrosRPG parametrosRPG = new ParametrosRPG() {
            @Override
            public String getFullPathRPG() {
                return rutaPrograma;
            }

            @Override
            public ProgramParameter[] loadParameters() {
                // Vector de parametros
                ProgramParameter[] parametros = new ProgramParameter[1];
                // Conversion de parametros de entrada
                //AS400Text paramconveter40 = new AS400Text(5, as400Factory.getCcsid());

                //ProgramParameter parm0 = new ProgramParameter(paramconveter40.toBytes("ISBP1")); // Parametro de ingreso
                ProgramParameter parm1 = new ProgramParameter(52);   // Parametro de salida

                //parametros[0] = parm0;
                parametros[0] = parm1;
                return parametros;
            }

        };

        // Invocando al programa RPG de AS400
        try {
            callProgramRPGv2.ejecutar(parametrosRPG);
        } catch (TimeoutException e) {
            LOGGER.warn("Timeout al llamar a carga de solicitudes Tarjeta de Credito para ATC");
            throw e;
        }

        // Respuesta del programa
        // Evaluando la respuesta del programa
        String valor1 = StringUtils.trimToEmpty(parametrosRPG.obtenerParametro(as400Factory, 0));
        if (StringUtils.trimToNull(valor1) != null) {
            LOGGER.info("Respuesta RPG: {}", valor1);
            if ("1".equalsIgnoreCase(valor1)) {
                resultado = true;
            } else {
                mensajeRespuesta = valor1;
                LOGGER.warn("La respuesta de la llamada a carga de solicitudes de tarjeta de credito: {}.", valor1);
            }

        } else {
            mensajeRespuesta = "Sin respuesta de la llamada a carga de solicitudes de tarjeta de credito.";
            LOGGER.warn("Sin respuesta de la llamada a carga de solicitudes de tarjeta de credito.");
        }
        return resultado;
    }

    public boolean ejecutarPgm(String rutaPrograma, int sizeInputParam, int sizeOutParam) throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException, TimeoutException {
        boolean resultado = false;
        ParametrosRPG parametrosRPG = new ParametrosRPG() {
            @Override
            public String getFullPathRPG() {
                return rutaPrograma;
            }

            @Override
            public ProgramParameter[] loadParameters() {
                // Vector de parametros
                ProgramParameter[] parametros = new ProgramParameter[2];
                // Conversion de parametros de entrada
                AS400Text paramconveter40 = new AS400Text(sizeInputParam, as400Factory.getCcsid());

                ProgramParameter parm0 = new ProgramParameter(paramconveter40.toBytes("ISBP1")); // Parametro de ingreso
                ProgramParameter parm1 = new ProgramParameter(sizeOutParam);   // Parametro de salida

                parametros[0] = parm0;
                parametros[1] = parm1;
                return parametros;
            }

        };

        // Invocando al programa RPG de AS400
        try {
            callProgramRPGv2.ejecutar(parametrosRPG);
        } catch (TimeoutException e) {
            LOGGER.warn("Timeout al llamar a carga de solicitudes Tarjeta de Credito para ATC");
            throw e;
        }

        // Respuesta del programa
        // Evaluando la respuesta del programa
        String valor1 = StringUtils.trimToEmpty(parametrosRPG.obtenerParametro(as400Factory, 1));
        if (StringUtils.trimToNull(valor1) != null) {
            LOGGER.info("Respuesta RPG: {}", valor1);
            if ("1".equalsIgnoreCase(valor1)) {
                resultado = true;
            } else {
                mensajeRespuesta = valor1;
                LOGGER.warn("La respuesta de la llamada a carga de solicitudes de tarjeta de credito: {}.", valor1);
            }

        } else {
            mensajeRespuesta = "Sin respuesta de la llamada a carga de solicitudes de tarjeta de credito.";
            LOGGER.warn("Sin respuesta de la llamada a carga de solicitudes de tarjeta de credito.");
        }
        return resultado;
    }

    public Long obtenerTarjetaCredito(BigDecimal cuentaTarjetaCredito) throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException, TimeoutException {
        LOGGER.info("Inicia obtenerTarjetaCredito");
        String resultado = "";
        String libreriaPrograma = medioAmbiente.getValorDe(Variables.LIBRERIA_TC_ATC, Variables.LIBRERIA_TC_ATC_DEFAULT);
        String rutaPrograma = new QSYSObjectPathName(libreriaPrograma,
                medioAmbiente.getValorDe(Variables.PROGRAMA_TC_OBTENER_NUMERO_TARJETA_CREDITO, Variables.PROGRAMA_TC_OBTENER_NUMERO_TARJETA_CREDITO_DEFAULT), "PGM").getPath();
        LOGGER.info("CCSID utilizado {}", as400Factory.getCcsid());
        ParametrosRPG parametrosRPG = new ParametrosRPG() {
            @Override
            public String getFullPathRPG() {
                return rutaPrograma;
            }

            @Override
            public ProgramParameter[] loadParameters() {
                // Vector de parametros
                ProgramParameter[] parametros = new ProgramParameter[2];
                // Conversion de parametros de entrada
                AS400PackedDecimal decParm = new AS400PackedDecimal(16,0);

                ProgramParameter parm0 = new ProgramParameter(decParm.toBytes(cuentaTarjetaCredito)); // Parametro de ingreso
                ProgramParameter parm1 = new ProgramParameter(16);   // Parametro de salida

                parametros[0] = parm0;
                parametros[1] = parm1;
                return parametros;
            }

        };

        // Invocando al programa RPG de AS400
        try {
            callProgramRPGv2.ejecutar(parametrosRPG);
        } catch (TimeoutException e) {
            LOGGER.warn("Timeout al llamar a carga de solicitudes Tarjeta de Credito para ATC");
            throw e;
        }

        // Respuesta del programa
        // Evaluando la respuesta del programa
        String valor1 = StringUtils.trimToEmpty(parametrosRPG.obtenerParametro(as400Factory, 1));
        if (StringUtils.trimToNull(valor1) != null) {
            LOGGER.info("Respuesta RPG: {}", valor1);
            if (StringUtils.isNotEmpty(valor1)) {
                resultado = StringUtils.trimToEmpty(valor1);
            } else {
                mensajeRespuesta = valor1;
                LOGGER.warn("La respuesta de la llamada a carga de solicitudes de tarjeta de credito: {}.", valor1);
            }

        } else {
            mensajeRespuesta = "Sin respuesta de la llamada a carga de solicitudes de tarjeta de credito.";
            LOGGER.warn("Sin respuesta de la llamada a carga de solicitudes de tarjeta de credito.");
        }
        if(StringUtils.isEmpty(resultado) || !resultado.matches("[0-9]+")){
            LOGGER.warn("Error al obtener tarjeta respuesta RPG:{}", resultado);
            return 0L;
        }
        return Long.parseLong(resultado);
    }

    public Long obtenerTarjetaCredito(String tarjetaCredito) throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException, TimeoutException {
        LOGGER.info("Inicia obtenerTarjetaCreditoDeEncriptado");
        String resultado = "";
        String libreriaPrograma = medioAmbiente.getValorDe(Variables.LIBRERIA_TC_ATC, Variables.LIBRERIA_TC_ATC_DEFAULT);
        String rutaPrograma = new QSYSObjectPathName(libreriaPrograma,
                medioAmbiente.getValorDe(Variables.PROGRAMA_TC_DESENCRIPTAR_NUMERO_TARJETA_CREDITO, Variables.PROGRAMA_TC_DESENCRIPTAR_NUMERO_TARJETA_CREDITO_DEFAULT), "PGM").getPath();

        ParametrosRPG parametrosRPG = new ParametrosRPG() {
            @Override
            public String getFullPathRPG() {
                return rutaPrograma;
            }

            @Override
            public ProgramParameter[] loadParameters() {
                // Vector de parametros
                ProgramParameter[] parametros = new ProgramParameter[2];
                int ccsid = medioAmbiente.getValorIntDe(Variables.PROGRAMA_TC_DESENCRIPTAR_TARJETA_CREDITO_CCSID, Variables.PROGRAMA_TC_DESENCRIPTAR_TARJETA_CREDITO_CCSID_DEFAULT);
                if(ccsid == 0){
                    ccsid = as400Factory.getCcsid();
                    LOGGER.info("CCSID asignando por defecto ccsid");
                }
                LOGGER.info("CCSID utilizado {}", ccsid);

                // Conversion de parametros de entrada
                AS400Text paramconveter40 = new AS400Text(16, ccsid);

                ProgramParameter parm0 = new ProgramParameter(paramconveter40.toBytes(tarjetaCredito));
                ProgramParameter parm1 = new ProgramParameter(16);   // Parametro de salida

                parametros[0] = parm0;
                parametros[1] = parm1;
                return parametros;
            }

        };

        // Invocando al programa RPG de AS400
        try {
            callProgramRPGv2.ejecutar(parametrosRPG);
        } catch (TimeoutException e) {
            LOGGER.warn("Timeout al llamar a carga de solicitudes Tarjeta de Credito para ATC");
            throw e;
        }

        // Respuesta del programa
        // Evaluando la respuesta del programa
        String valor1 = StringUtils.trimToEmpty(parametrosRPG.obtenerParametro(as400Factory, 1));
        if (StringUtils.trimToNull(valor1) != null) {
            LOGGER.info("Respuesta RPG: {}", valor1);
            if (StringUtils.isNotEmpty(valor1)) {
                resultado = StringUtils.trimToEmpty(valor1);
            } else {
                mensajeRespuesta = valor1;
                LOGGER.warn("La respuesta de la llamada a carga de solicitudes de tarjeta de credito: {}.", valor1);
            }

        } else {
            mensajeRespuesta = "Sin respuesta de la llamada a carga de solicitudes de tarjeta de credito.";
            LOGGER.warn("Sin respuesta de la llamada a carga de solicitudes de tarjeta de credito.");
        }
        if(StringUtils.isEmpty(resultado)){// || !resultado.matches("\\d+")){
            LOGGER.warn("Error al obtener tarjeta respuesta RPG:{}", resultado);
            return 0L;
        }
        return Long.parseLong(resultado);
    }

    public String getMensajeRespuesta() {
        return mensajeRespuesta;
    }

    public void setMensajeRespuesta(String mensajeRespuesta) {
        this.mensajeRespuesta = mensajeRespuesta;
    }
}
