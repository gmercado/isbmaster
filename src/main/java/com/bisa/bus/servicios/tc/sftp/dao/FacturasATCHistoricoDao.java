package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.FacturaATCHistorico;
import com.bisa.bus.servicios.tc.sftp.entities.IdFactura;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 04/07/2017.
 */
public class FacturasATCHistoricoDao extends DaoImpl<FacturaATCHistorico, IdFactura> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(FacturasATCHistoricoDao.class);

    @Inject
    protected FacturasATCHistoricoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public boolean guardar(FacturaATCHistorico registro, String user) {
        registro.setUsuarioCreador(user);
        registro.setFechaModificacion(new Date());
        persist(registro);
        return true;
    }
}
