package com.bisa.bus.servicios.tc.sftp.api;

import bus.config.dao.CryptUtils;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.rpg.ImposibleLlamarProgramaRpgException;
import bus.plumbing.file.ArchivoBase;
import bus.plumbing.file.ArchivoProfile;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import bus.plumbing.csv.GeneradorCSV;
import com.bisa.bus.servicios.tc.sftp.dao.SolicitudHistoricoTCDao;
import com.bisa.bus.servicios.tc.sftp.dao.SolicitudHistoricoTCEmpresarialDao;
import com.bisa.bus.servicios.tc.sftp.dao.SolicitudTCDao;
import com.bisa.bus.servicios.tc.sftp.dao.SolicitudTCEmpresarialDao;
import com.bisa.bus.servicios.tc.sftp.entities.*;
import com.bisa.bus.servicios.tc.sftp.model.*;
import com.bisa.bus.servicios.tc.sftp.rpg.IRpgCargarSolicitud;
import com.google.inject.Inject;
import com.ibm.as400.access.ConnectionPoolException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Created by atenorio on 18/05/2017.
 */
public class SftpEnvioSolicitud {
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpEnvioSolicitud.class);

    private final MedioAmbiente medioAmbiente;
    private final ArchivoBase archivoBase;
    private final SolicitudTCDao iSolicitudTCDao;
    private final SolicitudTCEmpresarialDao iSolicitudTCEmpresarialDao;
    private final SolicitudHistoricoTCDao iSolicitudHistoricoTCDao;
    private final SolicitudHistoricoTCEmpresarialDao iSolicitudHistoricoTCEmpresarialDao;
    private final IRpgCargarSolicitud iRpgCargarSolicitud;
    private final NotificacionesCorreo notificacionesCorreo;
    SftpConfiguracion configuracion;
    SftpTransferencia sftpTransferencia;

    @Inject
    public SftpEnvioSolicitud(CryptUtils cryptUtils, ArchivoBase archivoBase,
                              MedioAmbiente medioAmbiente, SolicitudTCDao iSolicitudTCDao,
                              SolicitudHistoricoTCEmpresarialDao iSolicitudHistoricoTCEmpresarialDao,
                              SolicitudHistoricoTCDao iSolicitudHistoricoTCDao, SolicitudTCEmpresarialDao iSolicitudTCEmpresarialDao,
                              IRpgCargarSolicitud iRpgCargarSolicitud, NotificacionesCorreo notificacionesCorreo,
                              SftpConfiguracion configuracion, SftpTransferencia sftpTransferencia) {
        this.medioAmbiente = medioAmbiente;
        this.archivoBase = archivoBase;
        this.iSolicitudTCDao = iSolicitudTCDao;
        this.iSolicitudTCEmpresarialDao = iSolicitudTCEmpresarialDao;
        this.iRpgCargarSolicitud = iRpgCargarSolicitud;
        this.iSolicitudHistoricoTCDao = iSolicitudHistoricoTCDao;
        this.iSolicitudHistoricoTCEmpresarialDao = iSolicitudHistoricoTCEmpresarialDao;
        this.notificacionesCorreo = notificacionesCorreo;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para generar y enviar archivo de solicitud de alta de tarjetas de servidor remoto SFTP.
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    public boolean procesarArchivo() {
        boolean ok = false;
        LOGGER.debug("Inicia proceso de envio de archivo de solicitud");
        String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_ATC, Variables.FECHA_DE_PROCESO_ATC_DEFAULT);
        Date fechaProcesar = new Date();
        if (StringUtils.isEmpty(fechaProceso) || "0".equals(fechaProceso)) {
            fechaProceso = FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar);
        } else {
            fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
        }
        String nombreArchivo = medioAmbiente.getValorDe(Variables.FTP_FILENAME_ATC_SOLITITUD_ALTAS, Variables.FTP_FILENAME_ATC_SOLITITUD_ALTAS_DEFAULT);
        nombreArchivo = nombreArchivo + FormatosUtils.fechaFormateadaConYYYYMMDD(fechaProcesar) + ".txt";

        LOGGER.debug("1) Verificar archivo si el archivo {} ya fue procesado.", nombreArchivo);
        // Verificar que archivo no sea procesado con el mismo nombre a menos que este en estado de reproceso o error de reintento
        if (!sftpTransferencia.verificarPermitirTransferencia(nombreArchivo)) {
            LOGGER.warn("No se permite procesamiento y transferencia de archivo {} se finaliza la tarea.", nombreArchivo);
            return false;
        }
        // Creacion de registro tipo solicitud
        FtpArchivoTransferencia ftpArchivoTransferencia = sftpTransferencia.crearRegistro(nombreArchivo, TipoArchivo.SOL);

        LOGGER.debug("2) Llamada a programa AS400, para cargar listado de solicitudes ha enviar.");
        try {
            ok = esCargaDeSolicitudes();
        } catch (Exception e) {
            LOGGER.error("Error al llamar programa AS400, generador de solicitudes aprobadas. {}", e);
            sftpTransferencia.transferenciaConErrorInterno(TipoCodigoError.ERR_PROGRAMA.getDescripcion());
            return false;
        }
        // Si no existen solicitudes notificar a cliente
        if (!ok) {
            LOGGER.warn("Error al llamar programa AS400, generador de solicitudes aprobadas.");
            sftpTransferencia.transferenciaConErrorInterno(TipoCodigoError.ERR_PROGRAMA.getDescripcion());
            return false;
        }

        LOGGER.debug("3) Carga de archivo de solicitudes a servidor SFTP");

        LOGGER.debug("3.1) Recuperacion solicitudes pendientes de envio");
        List<SolicitudTC> solicitudes = iSolicitudTCDao.getSolicitudesParaEnvio();
        LOGGER.debug("3.2) Recuperacion solicitudes empresariales pendientes de envio");
        List<SolicitudTCEmpresarial> solicitudesEmpresariales = iSolicitudTCEmpresarialDao.getSolicitudesParaEnvio();
        if (solicitudes.size() > 0 || solicitudesEmpresariales.size() > 0) {
            prepararConfiguracionSftp();
            // Creacion de archivo temporal para preparar envio
            File archivo = sftpTransferencia.createFile(nombreArchivo);

            ArchivoProfile archivoProfile = poblarArchivo(archivo, solicitudes, solicitudesEmpresariales);
            // Verificar si existio problema en la generacion de archivo
            if (archivoProfile == null) {
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.SOL, "Error en la generaci\u00F3n de archivo");
                return false;
            }
            LOGGER.debug("3.2) Creación local archivo de solicitudes con {} altas.", solicitudes.size());
            // Cargar archivo a servidor SFTP
            boolean cargado = sftpTransferencia.cargarArchivo(archivoProfile.getArchivo(), nombreArchivo, ftpArchivoTransferencia);

            // Modificar cambio de estado
            if (cargado) {
                LOGGER.debug("3.3) Cargando de archivo {} de solicitudes a servidor sftp finalizado correctamente.", nombreArchivo);
                TransferObject transferObject = new TransferObject();
                transferObject.initializate();
                int totalRegistros = 0;
                for (SolicitudTC sol : solicitudes) {
                    sol.setSofeenv(new Date());
                    sol.setIdArchivoEnvio(BigInteger.valueOf(sftpTransferencia.getFtpArchivoTransferencia().getId()));
                    sol.setEstado(EstadoSolicitudTC.ENVI);
                    ok = iSolicitudTCDao.guardar(sol, "ISB");
                    SolicitudHistoricoTC solicitudHistorico = transferObject.convert(sol, SolicitudHistoricoTC.class);
                    ok = iSolicitudHistoricoTCDao.guardar(solicitudHistorico, "ISB");
                    totalRegistros++;
                }
                for (SolicitudTCEmpresarial sol : solicitudesEmpresariales) {
                    sol.setSofeenv(new Date());
                    sol.setIdArchivoEnvio(sftpTransferencia.getFtpArchivoTransferencia().getId());
                    sol.setSoest(EstadoSolicitudTC.ENVI.name());
                    ok = iSolicitudTCEmpresarialDao.guardar(sol, "ISB");
                    SolicitudHistoricoTCEmpresarial solicitudHistorico = transferObject.convert(sol, SolicitudHistoricoTCEmpresarial.class);
                    ok = iSolicitudHistoricoTCEmpresarialDao.guardar(solicitudHistorico, "ISB");
                    totalRegistros++;
                }

                // Envio de correspondencia
                StringBuffer sb = new StringBuffer();
                sb.append("Se ha generado el archivo de solicitud de altas para administradora ATC ");
                sb.append(" con ");
                sb.append(totalRegistros);
                sb.append(" solicitudes.");
                notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivo, TipoArchivo.SOL, sb.toString());
                LOGGER.info("Completado proceso de carga de archivo {} de solicitudes a servidor SFTP de ATC correctamente.", nombreArchivo);
            } else {
                LOGGER.debug("3.3) Cargando de archivo {} de solicitudes a servidor sftp finalizado incorrectamente.", nombreArchivo);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivo, TipoArchivo.SOL, "Error en carga de archivo");
                return false;
            }
            LOGGER.debug("3.4) Completado proceso de carga de archivo {} de solicitudes a servidor SFTP de ATC correctamente.", nombreArchivo);
        } else {
            sftpTransferencia.transferenciaCompleta("Sin solicitudes a enviar");
            StringBuffer sb = new StringBuffer();
            sb.append("\nNo se ha generado el archivo de solicitud de altas para administradora ATC ");
            sb.append(" por no tener solicitudes aprobadas por enviar.\n");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivo, TipoArchivo.SOL, sb.toString());
            LOGGER.debug("3.4) Sin solicitudes para enviar");
        }
        return ok;
    }


    public boolean prepararConfiguracionSftp() {
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_SOLICITUD_ALTAS_ATC, Variables.FTP_RUTA_SOLICITUD_ALTAS_ATC_DEFAULT));
        configuracion.setRemoteTemp(medioAmbiente.getValorDe(Variables.FTP_RUTA_TEMPORAL_ALTA_ATC, Variables.FTP_RUTA_TEMPORAL_ALTA_ATC_DEFAULT));
        return sftpTransferencia.configurar(configuracion);
    }

    private ArchivoProfile poblarArchivo(File archivo, List<SolicitudTC> solicitudes, List<SolicitudTCEmpresarial> solicitudesEmpresariales) {
        if (archivo == null) return null;
        FileOutputStream archivoWrite;
        try {
            archivoWrite = new FileOutputStream(archivo.getAbsoluteFile());
            BufferedWriter bufferWriter = new BufferedWriter(new OutputStreamWriter(archivoWrite, StandardCharsets.UTF_8)); // StandardCharsets.ISO_8859_1));
            StringBuilder sb = new StringBuilder();
            bufferWriter.write(sb.toString());
            // generando archivo persona natural
            for (SolicitudTC sol : solicitudes) {
                TransferObject transferObject = new TransferObject();
                transferObject.initializate();
                // Grabar datos generales de cuenta
                SolicitudCuenta solicitudCuenta = transferObject.convert(sol, SolicitudCuenta.class);
                GeneradorCSV.poblarLinea(bufferWriter, solicitudCuenta);
                // Grabar condiciones comerciales
                SolicitudCondicionesComerciales cc = transferObject.convert(sol, SolicitudCondicionesComerciales.class);
                GeneradorCSV.poblarLinea(bufferWriter, cc);
                // Grabar datos de persona fisica
                SolicitudPersonaFisica cf = transferObject.convert(sol, SolicitudPersonaFisica.class);
                if (cf.getSocfnoem() != null) {
//                    cf.setSocfnoem(cf.getSocfnoem().replaceAll("/", " "));
//                    cf.setSocfnoem(cf.getSocfnoem().replaceAll("\\u00d1", " "));
                }
                GeneradorCSV.poblarLinea(bufferWriter, cf);
                // Grabar datos
                SolicitudInformacionEmpleoDefecto ie = new SolicitudInformacionEmpleoDefecto();
                ie.setSoeitire("EI");
                ie.setSoeifill("");
                GeneradorCSV.poblarLinea(bufferWriter, ie);
                // Grabar datos dirección cliente
                SolicitudDireccion ad = transferObject.convert(sol, SolicitudDireccion.class);
                GeneradorCSV.poblarLinea(bufferWriter, ad);
                // Grabar datos dirección tarjeta
                SolicitudDireccionCorreo ad2 = transferObject.convert(sol, SolicitudDireccionCorreo.class);
                GeneradorCSV.poblarLinea(bufferWriter, ad2);
                // Grabar datos de producto
                SolicitudProducto pr = transferObject.convert(sol, SolicitudProducto.class);
                GeneradorCSV.poblarLinea(bufferWriter, pr);
            }
            // generando archivo persona juridica
            for (SolicitudTCEmpresarial sol: solicitudesEmpresariales) {
                TransferObject transferObject = new TransferObject();
                transferObject.initializate();

                // Grabar datos generales de cuenta
                SolicitudCuenta solicitudCuenta = transferObject.convert(sol, SolicitudCuenta.class);
                GeneradorCSV.poblarLinea(bufferWriter, solicitudCuenta);

                // Grabar condiciones comerciales
                SolicitudCondicionesComerciales cc = transferObject.convert(sol, SolicitudCondicionesComerciales.class);
                GeneradorCSV.poblarLinea(bufferWriter, cc);

                // Grabar datos de persona juridica
                SolicitudPersonaJuridica cj = transferObject.convert(sol, SolicitudPersonaJuridica.class);
                GeneradorCSV.poblarLinea(bufferWriter, cj);

                // Grabar datos direccion empresa
                SolicitudDireccionEmpresa ade = transferObject.convert(sol, SolicitudDireccionEmpresa.class);
                GeneradorCSV.poblarLinea(bufferWriter, ade);

                // Grabar datos direccion correspondencia de empresa
                SolicitudDireccionCorreoEmpresa adec = transferObject.convert(sol, SolicitudDireccionCorreoEmpresa.class);
                GeneradorCSV.poblarLinea(bufferWriter, adec);

                // Grabar datos de persona fisica
                SolicitudPersonaFisica cf = transferObject.convert(sol, SolicitudPersonaFisica.class);
                if (cf.getSocfnoem() != null) {

                }
                GeneradorCSV.poblarLinea(bufferWriter, cf);
                // Grabar datos
                SolicitudInformacionEmpleoDefecto ie = new SolicitudInformacionEmpleoDefecto();
                ie.setSoeitire("EI");
                ie.setSoeifill("");
                GeneradorCSV.poblarLinea(bufferWriter, ie);
                // Grabar datos dirección cliente
                SolicitudDireccion ad = transferObject.convert(sol, SolicitudDireccion.class);
                GeneradorCSV.poblarLinea(bufferWriter, ad);

                // Grabar datos de producto
                SolicitudProducto pr = transferObject.convert(sol, SolicitudProducto.class);
                GeneradorCSV.poblarLinea(bufferWriter, pr);
            }


            bufferWriter.close();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("DETALLE DEL ARCHIVO");
                FileReader filer = new FileReader(archivo);
                int valor = filer.read();
                while (valor != -1) {
                    LOGGER.debug("", valor);
                    valor = filer.read();
                }
            }

        } catch (IOException e) {
            LOGGER.error("Ocurrio un error inesperado al preparar ", e);
            return null;
        }
        return new ArchivoProfile(archivo, 0L, 0L);
    }

    public boolean esCargaDeSolicitudes() throws IllegalArgumentException, IllegalStateException {
        boolean result = false;
        try {
            result = iRpgCargarSolicitud.ejecutar();
            return result;
        } catch (ImposibleLeerRespuestaException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(Imposible leer respuesta)");
        } catch (IOException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(IOException)");
        } catch (TransaccionEfectivaException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(TransaccionEfectivaException)");
        } catch (SistemaCerradoException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(SistemaCerradoException)");
        } catch (ImposibleLlamarProgramaRpgException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(ImposibleLlamarProgramaRpgException)");
        } catch (ConnectionPoolException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(ConnectionPoolException)");
        } catch (TimeoutException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(Tiempo de espera agotado)");
        }
    }
}
