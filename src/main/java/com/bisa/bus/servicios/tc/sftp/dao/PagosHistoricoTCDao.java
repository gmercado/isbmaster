package com.bisa.bus.servicios.tc.sftp.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.tc.sftp.entities.IdPagosTC;
import com.bisa.bus.servicios.tc.sftp.entities.PagosHistoricoTC;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by atenorio on 29/05/2017.
 */
public class PagosHistoricoTCDao extends DaoImpl<PagosHistoricoTC, IdPagosTC> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(PagosHistoricoTCDao.class);

    @Inject
    protected PagosHistoricoTCDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }


    public boolean guardar(PagosHistoricoTC pago, String user) {
        pago.setUsuarioCreador(user);
        pago.setFechaModificacion(new Date());
        persist(pago);
        return true;
    }

}
