package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

/**
 * Created by atenorio on 08/09/2017.
 */
public class SolicitudDireccionCorreoEmpresa {

    @FormatCsv(name = "SOADTIREM", nullable = false, length = 2)
    private String soadtirem;

    @FormatCsv(name = "SOADTIDICM", nullable = false, length = 1)
    private short soadtidicm;

    @FormatCsv(name = "SOADTICAM", nullable = false, length = 4)
    private short soadticam;

    @FormatCsv(name = "SOADCALLEM", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadcallem;

    @FormatCsv(name = "SOADNUPUM", nullable = false, length = 10, align = FormatCsv.alignType.LEFT)
    private String soadnupum;

    @FormatCsv(name = "SOADINADM", nullable = false, length = 100, align = FormatCsv.alignType.LEFT)
    private String soadinadm;

    @FormatCsv(name = "SOADCOPOM", nullable = false, length = 6)
    private String soadcopom;

    @FormatCsv(name = "SOADZONAM", nullable = false, length = 60, align = FormatCsv.alignType.LEFT)
    private String soadzonam;

    @FormatCsv(name = "SOADPAISM", nullable = false, length = 3, fillSpace = "0")
    private short soadpaism;

    @FormatCsv(name = "SOADDPTOM", nullable = false, length = 5, align = FormatCsv.alignType.LEFT)
    private String soaddptom;

    @FormatCsv(name = "SOADLOCM", nullable = false, length = 9, fillSpace = "0")
    private int soadlocm;

    @FormatCsv(name = "SOADCOTEPM", nullable = false, length = 3, fillSpace = "0")
    private short soadcotepm;

    @FormatCsv(name = "SOADCOTEAM", nullable = false, length = 5, fillSpace = "0")
    private int soadcoteam;

    @FormatCsv(name = "SOADTELM", nullable = false, length = 12, fillSpace = "0")
    private long soadtelm;

    @FormatCsv(name = "SOADTEINM", nullable = false, length = 6, fillSpace = "0")
    private int soadteinm;

    @FormatCsv(name = "SOADCOCEPM", nullable = false, length = 3, fillSpace = "0")
    private short soadcocepm;

    @FormatCsv(name = "SOADCELM", nullable = false, length = 12, fillSpace = "0")
    private long soadcelm;

    @FormatCsv(name = "SOADFILL", nullable = false, length = 56)
    private long soadfillm;

    @FormatCsv(name = "SOADCOERM", nullable = false, length = 3)
    private String soadcoerm;

    public String getSoadtirem() {
        return soadtirem;
    }

    public void setSoadtirem(String soadtirem) {
        this.soadtirem = soadtirem;
    }

    public short getSoadtidicm() {
        return soadtidicm;
    }

    public void setSoadtidicm(short soadtidicm) {
        this.soadtidicm = soadtidicm;
    }

    public short getSoadticam() {
        return soadticam;
    }

    public void setSoadticam(short soadticam) {
        this.soadticam = soadticam;
    }

    public String getSoadcallem() {
        return soadcallem;
    }

    public void setSoadcallem(String soadcallem) {
        this.soadcallem = soadcallem;
    }

    public String getSoadnupum() {
        return soadnupum;
    }

    public void setSoadnupum(String soadnupum) {
        this.soadnupum = soadnupum;
    }

    public String getSoadinadm() {
        return soadinadm;
    }

    public void setSoadinadm(String soadinadm) {
        this.soadinadm = soadinadm;
    }

    public String getSoadcopom() {
        return soadcopom;
    }

    public void setSoadcopom(String soadcopom) {
        this.soadcopom = soadcopom;
    }

    public String getSoadzonam() {
        return soadzonam;
    }

    public void setSoadzonam(String soadzonam) {
        this.soadzonam = soadzonam;
    }

    public short getSoadpaism() {
        return soadpaism;
    }

    public void setSoadpaism(short soadpaism) {
        this.soadpaism = soadpaism;
    }

    public String getSoaddptom() {
        return soaddptom;
    }

    public void setSoaddptom(String soaddptom) {
        this.soaddptom = soaddptom;
    }

    public int getSoadlocm() {
        return soadlocm;
    }

    public void setSoadlocm(int soadlocm) {
        this.soadlocm = soadlocm;
    }

    public short getSoadcotepm() {
        return soadcotepm;
    }

    public void setSoadcotepm(short soadcotepm) {
        this.soadcotepm = soadcotepm;
    }

    public int getSoadcoteam() {
        return soadcoteam;
    }

    public void setSoadcoteam(int soadcoteam) {
        this.soadcoteam = soadcoteam;
    }

    public long getSoadtelm() {
        return soadtelm;
    }

    public void setSoadtelm(long soadtelm) {
        this.soadtelm = soadtelm;
    }

    public int getSoadteinm() {
        return soadteinm;
    }

    public void setSoadteinm(int soadteinm) {
        this.soadteinm = soadteinm;
    }

    public short getSoadcocepm() {
        return soadcocepm;
    }

    public void setSoadcocepm(short soadcocepm) {
        this.soadcocepm = soadcocepm;
    }

    public long getSoadcelm() {
        return soadcelm;
    }

    public void setSoadcelm(long soadcelm) {
        this.soadcelm = soadcelm;
    }

    public long getSoadfillm() {
        return soadfillm;
    }

    public void setSoadfillm(long soadfillm) {
        this.soadfillm = soadfillm;
    }

    public String getSoadcoerm() {
        return soadcoerm;
    }

    public void setSoadcoerm(String soadcoerm) {
        this.soadcoerm = soadcoerm;
    }
}
