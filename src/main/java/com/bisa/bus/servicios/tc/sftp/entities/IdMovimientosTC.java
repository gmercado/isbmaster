package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by atenorio on 09/06/2017.
 */
@Embeddable
public class IdMovimientosTC implements Serializable {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "MECADM", nullable = false)
    private Character codigoAdm;

    @Basic(optional = false)
    @Column(name = "MEFEPRO1", nullable = false)
    private int fechaProceso;

    @Basic(optional = false)
    @Column(name = "MENUTCNC", nullable = false)
    private String numeroCuenta;

    @Basic(optional = false)
    @Column(name = "MENUCOMP", nullable = false)
    private String comprobante;

    public Character getCodigoAdm() {
        return codigoAdm;
    }

    public void setCodigoAdm(Character codigoAdm) {
        this.codigoAdm = codigoAdm;
    }

    public int getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(int fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getComprobante() {
        return comprobante;
    }

    public void setComprobante(String comprobante) {
        this.comprobante = comprobante;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdMovimientosTC that = (IdMovimientosTC) o;
        if (fechaProceso != that.fechaProceso) return false;
        if (numeroCuenta != that.numeroCuenta) return false;
        if (comprobante != that.comprobante) return false;
        return codigoAdm.equals(that.codigoAdm);
    }

    @Override
    public int hashCode() {
        int result = codigoAdm.hashCode();
        result = 31 * result + fechaProceso;
        result = 31 * result + numeroCuenta.hashCode();
        result = 31 * result + comprobante.hashCode();
        return result;
    }

    @Override
    public String toString(){
         String objeto = "";
        objeto = "CodigoAdministradora:" + codigoAdm + "\n";
        objeto += "Fecha:" + fechaProceso + "\n";
        objeto += "NumeroCuenta:" +numeroCuenta + "\n";
        objeto += "Comprobante:" + comprobante;
        return objeto;
    }
}
