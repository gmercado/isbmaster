package com.bisa.bus.servicios.tc.linkser.sched;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.bus.servicios.tc.linkser.api.*;
import com.bisa.bus.servicios.tc.sftp.model.NotificacionesCorreo;
import com.bisa.bus.servicios.tc.sftp.model.TipoArchivo;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 15/11/2017.
 */
public class RecepcionSolicitudAltaLinkser implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecepcionSolicitudAltaLinkser.class);

    private final MedioAmbiente medioAmbiente;
    private final NotificacionesCorreo notificacionesCorreo;
    private final SftpRecepcionSolicitudCuentaLinkser sftpRecepcionCuenta;
    private final SftpRecepcionSolicitudTarjetaLinkser sftpRecepcionTarjeta;
    private final SftpRecepcionRenovacionTarjetaLinkser sftpRecepcionRenovacionTarjeta;
    private final SftpRecepcionSolicitudErrorLinkser sftpRecepcionSolicitudErrorLinkser;
    private final SftpRecepcionBajasTarjetaLinkser sftpRecepcionBajasTarjetaLinkser;

    private volatile boolean ok;

    @Inject
    public RecepcionSolicitudAltaLinkser(MedioAmbiente medioAmbiente, NotificacionesCorreo notificacionesCorreo,
                                         SftpRecepcionSolicitudCuentaLinkser sftpRecepcionCuenta,
                                         SftpRecepcionSolicitudTarjetaLinkser sftpRecepciontarjeta,
                                         SftpRecepcionRenovacionTarjetaLinkser sftpRecepcionRenovacionTarjeta,
                                         SftpRecepcionSolicitudErrorLinkser sftpRecepcionSolicitudErrorLinkser,
                                         SftpRecepcionBajasTarjetaLinkser sftpRecepcionBajasTarjetaLinkser) {
        this.medioAmbiente = medioAmbiente;
        this.notificacionesCorreo = notificacionesCorreo;
        this.sftpRecepcionCuenta = sftpRecepcionCuenta;
        this.sftpRecepcionTarjeta = sftpRecepciontarjeta;
        this.sftpRecepcionRenovacionTarjeta = sftpRecepcionRenovacionTarjeta;
        this.sftpRecepcionSolicitudErrorLinkser = sftpRecepcionSolicitudErrorLinkser;
        this.sftpRecepcionBajasTarjetaLinkser = sftpRecepcionBajasTarjetaLinkser;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        // Procesando datos generales
        LOGGER.info("Inicio de proceso alta de cuenta");
        ok = sftpRecepcionCuenta.procesarArchivo();
        LOGGER.info("Respuesta de archivo procesado alta de cuenta: {}", ok);
        // Procesando movimientos
        if (ok) {
            LOGGER.info("Inicio de proceso alta de tarjeta");
            ok = sftpRecepcionTarjeta.procesarArchivo();
            LOGGER.info("Respuesta de archivo procesado alta de tarjeta: {}", ok);
            if (ok) {
                LOGGER.info("Inicio de proceso recuperacion de archivo de errores");
                ok = sftpRecepcionSolicitudErrorLinkser.procesarArchivo();
                LOGGER.info("Respuesta de archivo de errores: {}", ok);
                if (ok) {
                    LOGGER.info("Inicio de proceso renovaciones de tarjeta");
                    ok = sftpRecepcionRenovacionTarjeta.procesarArchivo();
                    LOGGER.info("Respuesta de archivo procesado renovaciones de tarjeta: {}", ok);
                    if (ok) {
                        LOGGER.info("Inicio de proceso bajas de tarjeta");
                        ok = sftpRecepcionBajasTarjetaLinkser.procesarArchivo();
                        LOGGER.info("Respuesta de archivo procesado bajas de tarjeta: {}", ok);

                    }
                }
            }
        }
        StringBuffer respuesta = new StringBuffer();
        // Completar proceso de carga notificando al operador
        if (ok) {
            // Eliminacion de registros
            respuesta.append("\n");
            respuesta.append(sftpRecepcionCuenta.eliminarArchivo());
            respuesta.append("\n");
            respuesta.append(sftpRecepcionTarjeta.eliminarArchivo());
            respuesta.append("\n");
            respuesta.append(sftpRecepcionSolicitudErrorLinkser.eliminarArchivo());
            respuesta.append("\n");
            respuesta.append(sftpRecepcionRenovacionTarjeta.eliminarArchivo());
            respuesta.append("\n");
            respuesta.append(sftpRecepcionBajasTarjetaLinkser.eliminarArchivo());

            sftpRecepcionCuenta.registrarRespuestaCorrecta();
            // Eliminar archivos cargados
            String archivos = sftpRecepcionCuenta.getNombreArchivoProcesado() + ", " + sftpRecepcionTarjeta.getNombreArchivoProcesado()
                    + ", " + sftpRecepcionSolicitudErrorLinkser.getNombreArchivoProcesado() + ", " + sftpRecepcionRenovacionTarjeta.getNombreArchivoProcesado()
                    + ", " + sftpRecepcionBajasTarjetaLinkser.getNombreArchivoProcesado();
            String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_PROCESO_DIARIO_OPERADOR_LINKSER, Variables.FTP_MENSAJE_EMAIL_PROCESO_DIARIO_OPERADOR_LINKSER_DEFAULT);
            notificacionesCorreo.notificarCorrectaTransferenciaOperador(archivos, TipoArchivo.RLI, mensaje + respuesta.toString());

        } else {
            if (sftpRecepcionCuenta.getPendienteRespuesta()) {
                // Pendiente respuesta de alta completo
                String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_PROCESO_DIARIO_ERROR_OPERADOR_LINKSER, Variables.FTP_MENSAJE_EMAIL_PROCESO_DIARIO_ERROR_OPERADOR_LINKSER_DEFAULT);
                notificacionesCorreo.notificarCorrectaTransferenciaOperador(sftpRecepcionCuenta.getNombreArchivoProcesado(), TipoArchivo.RLI, mensaje);
            } else if (sftpRecepcionRenovacionTarjeta.procesarArchivo(false) && sftpRecepcionBajasTarjetaLinkser.procesarArchivo(false)) {
                // Sin pendiente de altas se realiza carga de renovaciones
                respuesta.append("\n");
                respuesta.append(sftpRecepcionRenovacionTarjeta.eliminarArchivo());
                respuesta.append("\n");
                respuesta.append(sftpRecepcionBajasTarjetaLinkser.eliminarArchivo());
                respuesta.append("\n");
                respuesta.append("Numero de registros renovaciones: ");
                respuesta.append(sftpRecepcionRenovacionTarjeta.getTotalRegistros());
                respuesta.append("Numero de registros bajas: ");
                respuesta.append(sftpRecepcionBajasTarjetaLinkser.getTotalRegistros());

                String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_PROCESO_RENOVACION_OPERADOR_LINKSER, Variables.FTP_MENSAJE_EMAIL_PROCESO_RENOVACION_OPERADOR_LINKSER_DEFAULT);
                if (sftpRecepcionRenovacionTarjeta.getTotalRegistros() == 0) {
                    mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_PROCESO_RENOVACION_SIN_DATOS_OPERADOR_LINKSER, Variables.FTP_MENSAJE_EMAIL_PROCESO_RENOVACION_SIN_DATOS_OPERADOR_LINKSER_DEFAULT);
                }
                notificacionesCorreo.notificarCorrectaTransferenciaOperador(sftpRecepcionRenovacionTarjeta.getNombreArchivoProcesado() + ", " +
                                sftpRecepcionBajasTarjetaLinkser.getNombreArchivoProcesado(),
                        TipoArchivo.RLI, mensaje + respuesta.toString());
            } else {
                // No existen archivos de renovaciones ni altas
                String mensaje = medioAmbiente.getValorDe(Variables.FTP_MENSAJE_EMAIL_PROCESO_RENOVACION_ERROR_OPERADOR_LINKSER, Variables.FTP_MENSAJE_EMAIL_PROCESO_RENOVACION_ERROR_OPERADOR_LINKSER_DEFAULT);
                notificacionesCorreo.notificarErrorTransferenciaOperador(sftpRecepcionRenovacionTarjeta.getNombreArchivoProcesado() + ", " +
                        sftpRecepcionBajasTarjetaLinkser.getNombreArchivoProcesado(), TipoArchivo.RLI, mensaje);
            }
        }
    }


}
