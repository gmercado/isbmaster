package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import java.math.BigInteger;

/**
 * Created by atenorio on 25/05/2017.
 */
public class RespuestaCuenta {

    @FormatCsv(name = "nroTrj", length = 16, nullable = true)
    private String numeroTarjeta = "";

    @FormatCsv(name = "nroCuenta", length = 15, nullable = true, fillSpace = "0")
    private BigInteger numeroCuenta;

    @FormatCsv(name = "sucursal", length = 2, nullable = true, fillSpace = "0")
    private int sucursal = 0;

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public BigInteger getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(BigInteger numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public int getSucursal() {
        return sucursal;
    }

    public void setSucursal(int sucursal) {
        this.sucursal = sucursal;
    }
}
