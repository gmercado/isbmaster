package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import java.math.BigDecimal;

/**
 * Created by atenorio on 09/06/2017.
 */
public class MovimientoCsv {
    
    @FormatCsv(name = "MEBAEM", nullable = false)
    private String codigoBanco;
    
    @FormatCsv(name = "MESUEM", nullable = false)
    private String codigoSucursal;
    
    @FormatCsv(name = "MENUTCNC", nullable = false)
    private String numeroCuenta;
        
    
    @FormatCsv(name = "MENUTCEN", nullable = false)
    private String numeroTarjeta;
    
    @FormatCsv(name = "MEACTC", nullable = false)
    private String codigoAceptanteTarjeta;
    
    @FormatCsv(name = "MECOOP", nullable = false)
    private Character codigoOperacion;
    
    @FormatCsv(name = "MECAESCU", nullable = false, length = 4)
    private String categoriaEstadoCuenta;
    
    @FormatCsv(name = "MECABA", nullable = false, length = 4)
    private String categoriaBalance;
    
    @FormatCsv(name = "MECAFI", nullable = false, length = 4)
    private String categoriaFinanciacion;
    
    @FormatCsv(name = "MENUCOMP", nullable = false)
    private String comprobante;
    
    @FormatCsv(name = "MEFETR", nullable = false)
    private String fechaTransaccion;

    @FormatCsv(name = "MEIMMOOR", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeOrigen;
    
    @FormatCsv(name = "MEMOOR", nullable = false)
    private String monedaOrigen;
    
    @FormatCsv(name = "MEIMMOBA", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeBalance;
    
    @FormatCsv(name = "MEMOBA", nullable = false)
    private String monedaBalance;
    
    @FormatCsv(name = "MEIMMOLI", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeLiquidacion;
    
    @FormatCsv(name = "MEMOLI", nullable = false)
    private String monedaLiquidacion;
    
    @FormatCsv(name = "MEALTR", nullable = false)
    private Character alcanceTransaccion;
    
    @FormatCsv(name = "MENULO", nullable = false)
    private String numeroLote;
    
    @FormatCsv(name = "MEMOTR", nullable = false, length = 2)
    private String modoTransaccion;
    
    @FormatCsv(name = "MENOEST", nullable = false, length = 30)
    private String establecimiento;
    
    @FormatCsv(name = "MEMTI", nullable = false, length = 4)
    private String codigoMti;
    
    @FormatCsv(name = "MECOPRO", nullable = false, length = 6)
    private String codigoProceso;
    
    @FormatCsv(name = "MECOAUT", nullable = false, length = 6)
    private String codigoAutorizacion;
    
    @FormatCsv(name = "MEIDTERM", nullable = false, length = 8)
    private String identificadorTerminal;
    
    @FormatCsv(name = "MECACUO", nullable = false)
    private String totalCuota;
    
    @FormatCsv(name = "MENUCUO", nullable = false)
    private String numeroCuota;
    
    @FormatCsv(name = "MEMOADI", nullable = false, precision = 11, scale = 2)
    private BigDecimal montoAdicional;
    
    @FormatCsv(name = "MEIMEFEC", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeCashBack;
    
    @FormatCsv(name = "MECADISP", nullable = false)
    private String capacidadTerminal;
    
    @FormatCsv(name = "MECOINTC", nullable = false)
    private String condicionIngreso;

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getCodigoSucursal() {
        return codigoSucursal;
    }

    public void setCodigoSucursal(String codigoSucursal) {
        this.codigoSucursal = codigoSucursal;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }



    public Character getCodigoOperacion() {
        return codigoOperacion;
    }

    public void setCodigoOperacion(Character codigoOperacion) {
        this.codigoOperacion = codigoOperacion;
    }

    public String getCategoriaEstadoCuenta() {
        return categoriaEstadoCuenta;
    }

    public void setCategoriaEstadoCuenta(String categoriaEstadoCuenta) {
        this.categoriaEstadoCuenta = categoriaEstadoCuenta;
    }

    public String getCategoriaBalance() {
        return categoriaBalance;
    }

    public void setCategoriaBalance(String categoriaBalance) {
        this.categoriaBalance = categoriaBalance;
    }

    public String getCategoriaFinanciacion() {
        return categoriaFinanciacion;
    }

    public void setCategoriaFinanciacion(String categoriaFinanciacion) {
        this.categoriaFinanciacion = categoriaFinanciacion;
    }

    public String getComprobante() {
        return comprobante;
    }

    public void setComprobante(String comprobante) {
        this.comprobante = comprobante;
    }

    public String getFechaTransaccion() {
        return fechaTransaccion;
    }

    public void setFechaTransaccion(String fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    public BigDecimal getImporteOrigen() {
        return importeOrigen;
    }

    public void setImporteOrigen(BigDecimal importeOrigen) {
        this.importeOrigen = importeOrigen;
    }

    public String getMonedaOrigen() {
        return monedaOrigen;
    }

    public void setMonedaOrigen(String monedaOrigen) {
        this.monedaOrigen = monedaOrigen;
    }

    public BigDecimal getImporteBalance() {
        return importeBalance;
    }

    public void setImporteBalance(BigDecimal importeBalance) {
        this.importeBalance = importeBalance;
    }

    public String getMonedaBalance() {
        return monedaBalance;
    }

    public void setMonedaBalance(String monedaBalance) {
        this.monedaBalance = monedaBalance;
    }

    public BigDecimal getImporteLiquidacion() {
        return importeLiquidacion;
    }

    public void setImporteLiquidacion(BigDecimal importeLiquidacion) {
        this.importeLiquidacion = importeLiquidacion;
    }

    public String getMonedaLiquidacion() {
        return monedaLiquidacion;
    }

    public void setMonedaLiquidacion(String monedaLiquidacion) {
        this.monedaLiquidacion = monedaLiquidacion;
    }

    public Character getAlcanceTransaccion() {
        return alcanceTransaccion;
    }

    public void setAlcanceTransaccion(Character alcanceTransaccion) {
        this.alcanceTransaccion = alcanceTransaccion;
    }

    public String getNumeroLote() {
        return numeroLote;
    }

    public void setNumeroLote(String numeroLote) {
        this.numeroLote = numeroLote;
    }

    public String getModoTransaccion() {
        return modoTransaccion;
    }

    public void setModoTransaccion(String modoTransaccion) {
        this.modoTransaccion = modoTransaccion;
    }

    public String getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(String establecimiento) {
        this.establecimiento = establecimiento;
    }

    public String getCodigoMti() {
        return codigoMti;
    }

    public void setCodigoMti(String codigoMti) {
        this.codigoMti = codigoMti;
    }

    public String getCodigoProceso() {
        return codigoProceso;
    }

    public void setCodigoProceso(String codigoProceso) {
        this.codigoProceso = codigoProceso;
    }

    public String getCodigoAutorizacion() {
        return codigoAutorizacion;
    }

    public void setCodigoAutorizacion(String codigoAutorizacion) {
        this.codigoAutorizacion = codigoAutorizacion;
    }

    public String getIdentificadorTerminal() {
        return identificadorTerminal;
    }

    public void setIdentificadorTerminal(String identificadorTerminal) {
        this.identificadorTerminal = identificadorTerminal;
    }

    public String getTotalCuota() {
        return totalCuota;
    }

    public void setTotalCuota(String totalCuota) {
        this.totalCuota = totalCuota;
    }

    public String getNumeroCuota() {
        return numeroCuota;
    }

    public void setNumeroCuota(String numeroCuota) {
        this.numeroCuota = numeroCuota;
    }

    public BigDecimal getMontoAdicional() {
        return montoAdicional;
    }

    public void setMontoAdicional(BigDecimal montoAdicional) {
        this.montoAdicional = montoAdicional;
    }

    public BigDecimal getImporteCashBack() {
        return importeCashBack;
    }

    public void setImporteCashBack(BigDecimal importeCashBack) {
        this.importeCashBack = importeCashBack;
    }

    public String getCapacidadTerminal() {
        return capacidadTerminal;
    }

    public void setCapacidadTerminal(String capacidadTerminal) {
        this.capacidadTerminal = capacidadTerminal;
    }

    public String getCondicionIngreso() {
        return condicionIngreso;
    }

    public void setCondicionIngreso(String condicionIngreso) {
        this.condicionIngreso = condicionIngreso;
    }

    public String getCodigoAceptanteTarjeta() {
        return codigoAceptanteTarjeta;
    }

    public void setCodigoAceptanteTarjeta(String codigoAceptanteTarjeta) {
        this.codigoAceptanteTarjeta = codigoAceptanteTarjeta;
    }
}
