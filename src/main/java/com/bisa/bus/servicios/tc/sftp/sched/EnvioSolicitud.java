package com.bisa.bus.servicios.tc.sftp.sched;

import bus.env.api.MedioAmbiente;
import com.bisa.bus.servicios.tc.sftp.api.SftpEnvioSolicitud;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 17/05/2017.
 */

public class EnvioSolicitud implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(EnvioSolicitud.class);

    public static String NOMBRE_TAREA = "EnvioSolicitud";
    private final MedioAmbiente medioAmbiente;
    private final SftpEnvioSolicitud sftpEnvioSolicitud;

    @Inject
    public EnvioSolicitud(MedioAmbiente medioAmbiente, SftpEnvioSolicitud sftpEnvioSolicitud){
        this.medioAmbiente = medioAmbiente;
        this.sftpEnvioSolicitud = sftpEnvioSolicitud;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Inicio de proceso de generaci\u00F3n y envio");
        boolean ok = sftpEnvioSolicitud.procesarArchivo();
        LOGGER.info("Fin de proceso de generaci\u00F3n y envio respuesta: {}", ok);
    }


}
