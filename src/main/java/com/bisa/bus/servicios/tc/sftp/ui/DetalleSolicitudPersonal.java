package com.bisa.bus.servicios.tc.sftp.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import com.bisa.bus.servicios.tc.sftp.dao.SolicitudTCDao;
import com.bisa.bus.servicios.tc.sftp.entities.SolicitudTC;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 04/12/2017.
 */
@Titulo("Detalle de Solicitud Personal")
public class DetalleSolicitudPersonal extends BisaWebPage {

    protected static final Logger LOGGER = LoggerFactory.getLogger(DetalleSolicitudPersonal.class);

    public DetalleSolicitudPersonal(PageParameters parameters) {
        super();
        int id = parameters.get("solicitud").toInt();

        IModel<SolicitudTC> model = new LoadableDetachableModel<SolicitudTC>() {
            @Override
            protected SolicitudTC load() {
                SolicitudTCDao dao = getInstance(SolicitudTCDao.class);
                return dao.find(id);
            }
        };

        setDefaultModel(new CompoundPropertyModel<>(model));
        //Form
        Form<SolicitudTC> form = new Form<>("detalle", model);
        add(form);

        // detalle
        form.add(new Label("sonsol"));
        form.add(new Label("socfnocl"));
        form.add(new Label("socfapcl")); //, new PropertyModel(model,"idPago.transaccion")
        form.add(new Label("socftido"));
        form.add(new Label("socfdocl"));

        form.add(new Label("estadoDescripcion"));


        form.add(new Label("idArchivoEnvio"));
        form.add(new Label("fechaEnvioFormato"));
        form.add(new Label("idArchivoRecepcion"));
        form.add(new Label("fechaRecepcionFormato"));
        // botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(new ListadoSolicitudPersonal());
            }
        });

    }
}
