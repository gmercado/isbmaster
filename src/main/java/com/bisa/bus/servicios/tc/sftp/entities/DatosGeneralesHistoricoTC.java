package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by atenorio on 09/06/2017.
 */
@Entity
@Table(name = "TCPANENSAH")
public class DatosGeneralesHistoricoTC implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private IdDatosGenerales idDatosGenerales;

    @Basic(optional = false)
    @Column(name = "ESBAEM", nullable = false, length = 4)
    private String codigoBanco;

    @Basic(optional = false)
    @Column(name = "ESIMLIBSS")
    private Character impBalanceLiqBsSigno;
    @Basic(optional = false)
    @Column(name = "ESIMLIBS")
    private String impBalanceLiqBsTexto;
    @Basic(optional = false)
    @Column(name = "ESIMLIUSS")
    private Character impBalanceLiqUsSigno;
    @Basic(optional = false)
    @Column(name = "ESIMLIUS")
    private String impBalanceLiqUsTexto;
    @Basic(optional = false)
    @Column(name = "ESPAMIS")
    private Character pagoMinimoSigno;
    @Basic(optional = false)
    @Column(name = "ESPAMI")
    private String pagoMinimoTexto;
    @Basic(optional = false)
    @Column(name = "ESINTS")
    private Character totalInteresesSigno;
    @Basic(optional = false)
    @Column(name = "ESINT")
    private String totalInteresesTexto;
    @Basic(optional = false)
    @Column(name = "ESCOBSS")
    private Character consumoBsSigno;
    @Basic(optional = false)
    @Column(name = "ESCOBS")
    private String consumoBsTexto;
    @Basic(optional = false)
    @Column(name = "ESCOUSS")
    private Character consumoUsSigno;
    @Basic(optional = false)
    @Column(name = "ESCOUS")
    private String consumoUsTexto;
    @Basic(optional = false)
    @Column(name = "ESADBSS")
    private Character adelantoBsSigno;
    @Basic(optional = false)
    @Column(name = "ESADBS")
    private String adelantoBsTexto;
    @Basic(optional = false)
    @Column(name = "ESADUSS")
    private Character adelantoUsSigno;
    @Basic(optional = false)
    @Column(name = "ESADUS")
    private String adelantoUsTexto;
    @Basic(optional = false)
    @Column(name = "ESCOAUBSS")
    private Character consumoAutorizadoBsSigno;
    @Basic(optional = false)
    @Column(name = "ESCOAUBS")
    private String consumoAutorizadoBsTexto;
    @Basic(optional = false)
    @Column(name = "ESCOAUUSS")
    private Character consumoAutorizadoUsSigno;
    @Basic(optional = false)
    @Column(name = "ESCOAUUS")
    private String consumoAutorizadoUsTexto;

    @Basic(optional = false)
    @Column(name = "ESADAUBSS")
    private Character adelantoAutorizadoBsSigno;
    @Basic(optional = false)
    @Column(name = "ESADAUBS")
    private String adelantoAutorizadoBsTexto;
    @Basic(optional = false)
    @Column(name = "ESADAUUSS")
    private Character adelantoAutorizadoUsSigno;
    @Basic(optional = false)
    @Column(name = "ESADAUUS")
    private String adelantoAutorizadoUsTexto;
    @Basic(optional = false)
    @Column(name = "ESAJBSS")
    private Character ajustesBsSigno;
    @Basic(optional = false)
    @Column(name = "ESAJBS")
    private String ajustesBsTexto;
    @Basic(optional = false)
    @Column(name = "ESAJUSS")
    private Character ajustesUsSigno;
    @Basic(optional = false)
    @Column(name = "ESAJUS")
    private String ajustesUsTexto;
    @Basic(optional = false)
    @Column(name = "ESCABSS")
    private Character cargosBsSigno;
    @Basic(optional = false)
    @Column(name = "ESCABS")
    private String cargosBsTexto;
    @Basic(optional = false)
    @Column(name = "ESPABSS")
    private Character pagoBsSigno;
    @Basic(optional = false)
    @Column(name = "ESPABS")
    private String pagoBsTexto;

    @Basic(optional = false)
    @Column(name = "ESIMLIBSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal impBalanceLiqBs;
    //@Basic(optional = false)
    //@Column(name = "ESIMLIUSN", nullable = false, precision = 11, scale = 2)
    @Transient
    private BigDecimal impBalanceLiqUs;
    @Basic(optional = false)
    @Column(name = "ESPAMIN", nullable = false, precision = 11, scale = 2)
    private BigDecimal pagoMinimo;
    //@Basic(optional = false)
    //@Column(name = "ESINTN", nullable = false, precision = 11, scale = 2)
    @Transient
    private BigDecimal totalIntereses;
    @Basic(optional = false)
    @Column(name = "ESCOBSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal consumoBs;
    @Basic(optional = false)
    @Column(name = "ESCOUSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal consumoUs;
    @Basic(optional = false)
    @Column(name = "ESADBSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal adelantoBs;
    @Basic(optional = false)
    @Column(name = "ESADUSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal adelantoUs;
    //    @Basic(optional = false)
//    @Column(name = "ESCOAUBSN", nullable = false, precision = 11, scale = 2)
    @Transient
    private BigDecimal consumoAutorizadoBs;
    //    @Basic(optional = false)
//    @Column(name = "ESCOAUUSN", nullable = false, precision = 11, scale = 2)
    @Transient
    private BigDecimal consumoAutorizadoUs;
    //    @Basic(optional = false)
//    @Column(name = "ESADAUBSN", nullable = false, precision = 11, scale = 2)
    @Transient
    private BigDecimal adelantoAutorizadoBs;
    //    @Basic(optional = false)
//    @Column(name = "ESADAUUSN", nullable = false, precision = 11, scale = 2)
    @Transient
    private BigDecimal adelantoAutorizadoUs;
    //    @Basic(optional = false)
//    @Column(name = "ESCAADAUN", nullable = false, length = 3)
    @Transient
    private String cantAdelantoAutorizado;
    @Basic(optional = false)
    @Column(name = "ESAJBSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal ajustesBs;
    @Basic(optional = false)
    @Column(name = "ESAJUSN", nullable = false, precision = 11, scale = 2)
    private BigDecimal ajustesUs;
    //    @Basic(optional = false)
//    @Column(name = "ESCABSN", nullable = false, precision = 11, scale = 2)
    @Transient
    private BigDecimal cargosBs;
    //@Basic(optional = false)
    //@Column(name = "ESPABSN", nullable = false, precision = 11, scale = 2)
    @Transient
    private BigDecimal pagoBs;
    @Basic(optional = false)
    @Column(name = "ESNIMO", nullable = false, length = 2)
    private String nivelMora;


    @Basic(optional = false)
    @Column(name = "ESNUTC", nullable = false)
    private long cuentaTarjeta;
    @Basic(optional = false)
    @Column(name = "ESNUT1", nullable = false, length = 9)
    private String cuentaTarjetaReducida = "";
    @Basic(optional = false)
    @Column(name = "ESEST", nullable = false, length = 4)
    private String estado="";

    @Basic(optional = false)
    @Column(name = "ESIDRE")
    private long idArchivoRecepcion;
    @Basic(optional = false)
    @Column(name = "ESFERE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRecepcion;
    @Basic(optional = false)
    @Column(name = "REPGCR", nullable = false, length = 10)
    private String usuarioCreador="";
    @Basic(optional = false)
    @Column(name = "REFECR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion = new Date();
    @Basic(optional = false)
    @Column(name = "REPGMO", nullable = false, length = 10)
    private String usuarioModificador="";
    @Basic(optional = false)
    @Column(name = "REFEMO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion=new Date();

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public BigDecimal getImpBalanceLiqBs() {
        return impBalanceLiqBs;
    }

    public void setImpBalanceLiqBs(BigDecimal impBalanceLiqBs) {
        this.impBalanceLiqBs = impBalanceLiqBs;
    }

    public BigDecimal getImpBalanceLiqUs() {
        return impBalanceLiqUs;
    }

    public void setImpBalanceLiqUs(BigDecimal impBalanceLiqUs) {
        this.impBalanceLiqUs = impBalanceLiqUs;
    }

    public BigDecimal getPagoMinimo() {
        return pagoMinimo;
    }

    public void setPagoMinimo(BigDecimal pagoMinimo) {
        this.pagoMinimo = pagoMinimo;
    }

    public BigDecimal getTotalIntereses() {
        return totalIntereses;
    }

    public void setTotalIntereses(BigDecimal totalIntereses) {
        this.totalIntereses = totalIntereses;
    }

    public BigDecimal getConsumoBs() {
        return consumoBs;
    }

    public void setConsumoBs(BigDecimal consumoBs) {
        this.consumoBs = consumoBs;
    }

    public BigDecimal getConsumoUs() {
        return consumoUs;
    }

    public void setConsumoUs(BigDecimal consumoUs) {
        this.consumoUs = consumoUs;
    }

    public BigDecimal getAdelantoBs() {
        return adelantoBs;
    }

    public void setAdelantoBs(BigDecimal adelantoBs) {
        this.adelantoBs = adelantoBs;
    }

    public BigDecimal getAdelantoUs() {
        return adelantoUs;
    }

    public void setAdelantoUs(BigDecimal adelantoUs) {
        this.adelantoUs = adelantoUs;
    }

    public BigDecimal getConsumoAutorizadoBs() {
        return consumoAutorizadoBs;
    }

    public void setConsumoAutorizadoBs(BigDecimal consumoAutorizadoBs) {
        this.consumoAutorizadoBs = consumoAutorizadoBs;
    }

    public BigDecimal getConsumoAutorizadoUs() {
        return consumoAutorizadoUs;
    }

    public void setConsumoAutorizadoUs(BigDecimal consumoAutorizadoUs) {
        this.consumoAutorizadoUs = consumoAutorizadoUs;
    }

    public BigDecimal getAdelantoAutorizadoBs() {
        return adelantoAutorizadoBs;
    }

    public void setAdelantoAutorizadoBs(BigDecimal adelantoAutorizadoBs) {
        this.adelantoAutorizadoBs = adelantoAutorizadoBs;
    }

    public BigDecimal getAdelantoAutorizadoUs() {
        return adelantoAutorizadoUs;
    }

    public void setAdelantoAutorizadoUs(BigDecimal adelantoAutorizadoUs) {
        this.adelantoAutorizadoUs = adelantoAutorizadoUs;
    }

    public String getCantAdelantoAutorizado() {
        return cantAdelantoAutorizado;
    }

    public void setCantAdelantoAutorizado(String cantAdelantoAutorizado) {
        this.cantAdelantoAutorizado = cantAdelantoAutorizado;
    }

    public BigDecimal getAjustesBs() {
        return ajustesBs;
    }

    public void setAjustesBs(BigDecimal ajustesBs) {
        this.ajustesBs = ajustesBs;
    }

    public BigDecimal getAjustesUs() {
        return ajustesUs;
    }

    public void setAjustesUs(BigDecimal ajustesUs) {
        this.ajustesUs = ajustesUs;
    }

    public BigDecimal getCargosBs() {
        return cargosBs;
    }

    public void setCargosBs(BigDecimal cargosBs) {
        this.cargosBs = cargosBs;
    }

    public BigDecimal getPagoBs() {
        return pagoBs;
    }

    public void setPagoBs(BigDecimal pagoBs) {
        this.pagoBs = pagoBs;
    }

    public String getNivelMora() {
        return nivelMora;
    }

    public void setNivelMora(String nivelMora) {
        this.nivelMora = nivelMora;
    }

    public long getCuentaTarjeta() {
        return cuentaTarjeta;
    }

    public void setCuentaTarjeta(long cuentaTarjeta) {
        this.cuentaTarjeta = cuentaTarjeta;
    }

    public String getCuentaTarjetaReducida() {
        return cuentaTarjetaReducida;
    }

    public void setCuentaTarjetaReducida(String cuentaTarjetaReducida) {
        this.cuentaTarjetaReducida = cuentaTarjetaReducida;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public IdDatosGenerales getIdDatosGenerales() {
        return idDatosGenerales;
    }

    public void setIdDatosGenerales(IdDatosGenerales idDatosGenerales) {
        this.idDatosGenerales = idDatosGenerales;
    }

    public Character getImpBalanceLiqBsSigno() {
        return impBalanceLiqBsSigno;
    }

    public void setImpBalanceLiqBsSigno(Character impBalanceLiqBsSigno) {
        this.impBalanceLiqBsSigno = impBalanceLiqBsSigno;
    }

    public String getImpBalanceLiqBsTexto() {
        return impBalanceLiqBsTexto;
    }

    public void setImpBalanceLiqBsTexto(String impBalanceLiqBsTexto) {
        this.impBalanceLiqBsTexto = impBalanceLiqBsTexto;
    }

    public Character getImpBalanceLiqUsSigno() {
        return impBalanceLiqUsSigno;
    }

    public void setImpBalanceLiqUsSigno(Character impBalanceLiqUsSigno) {
        this.impBalanceLiqUsSigno = impBalanceLiqUsSigno;
    }

    public String getImpBalanceLiqUsTexto() {
        return impBalanceLiqUsTexto;
    }

    public void setImpBalanceLiqUsTexto(String impBalanceLiqUsTexto) {
        this.impBalanceLiqUsTexto = impBalanceLiqUsTexto;
    }

    public Character getPagoMinimoSigno() {
        return pagoMinimoSigno;
    }

    public void setPagoMinimoSigno(Character pagoMinimoSigno) {
        this.pagoMinimoSigno = pagoMinimoSigno;
    }

    public String getPagoMinimoTexto() {
        return pagoMinimoTexto;
    }

    public void setPagoMinimoTexto(String pagoMinimoTexto) {
        this.pagoMinimoTexto = pagoMinimoTexto;
    }

    public Character getTotalInteresesSigno() {
        return totalInteresesSigno;
    }

    public void setTotalInteresesSigno(Character totalInteresesSigno) {
        this.totalInteresesSigno = totalInteresesSigno;
    }

    public String getTotalInteresesTexto() {
        return totalInteresesTexto;
    }

    public void setTotalInteresesTexto(String totalInteresesTexto) {
        this.totalInteresesTexto = totalInteresesTexto;
    }

    public Character getConsumoBsSigno() {
        return consumoBsSigno;
    }

    public void setConsumoBsSigno(Character consumoBsSigno) {
        this.consumoBsSigno = consumoBsSigno;
    }

    public String getConsumoBsTexto() {
        return consumoBsTexto;
    }

    public void setConsumoBsTexto(String consumoBsTexto) {
        this.consumoBsTexto = consumoBsTexto;
    }

    public Character getConsumoUsSigno() {
        return consumoUsSigno;
    }

    public void setConsumoUsSigno(Character consumoUsSigno) {
        this.consumoUsSigno = consumoUsSigno;
    }

    public String getConsumoUsTexto() {
        return consumoUsTexto;
    }

    public void setConsumoUsTexto(String consumoUsTexto) {
        this.consumoUsTexto = consumoUsTexto;
    }

    public Character getAdelantoBsSigno() {
        return adelantoBsSigno;
    }

    public void setAdelantoBsSigno(Character adelantoBsSigno) {
        this.adelantoBsSigno = adelantoBsSigno;
    }

    public String getAdelantoBsTexto() {
        return adelantoBsTexto;
    }

    public void setAdelantoBsTexto(String adelantoBsTexto) {
        this.adelantoBsTexto = adelantoBsTexto;
    }

    public Character getAdelantoUsSigno() {
        return adelantoUsSigno;
    }

    public void setAdelantoUsSigno(Character adelantoUsSigno) {
        this.adelantoUsSigno = adelantoUsSigno;
    }

    public String getAdelantoUsTexto() {
        return adelantoUsTexto;
    }

    public void setAdelantoUsTexto(String adelantoUsTexto) {
        this.adelantoUsTexto = adelantoUsTexto;
    }

    public Character getConsumoAutorizadoBsSigno() {
        return consumoAutorizadoBsSigno;
    }

    public void setConsumoAutorizadoBsSigno(Character consumoAutorizadoBsSigno) {
        this.consumoAutorizadoBsSigno = consumoAutorizadoBsSigno;
    }

    public String getConsumoAutorizadoBsTexto() {
        return consumoAutorizadoBsTexto;
    }

    public void setConsumoAutorizadoBsTexto(String consumoAutorizadoBsTexto) {
        this.consumoAutorizadoBsTexto = consumoAutorizadoBsTexto;
    }

    public Character getConsumoAutorizadoUsSigno() {
        return consumoAutorizadoUsSigno;
    }

    public void setConsumoAutorizadoUsSigno(Character consumoAutorizadoUsSigno) {
        this.consumoAutorizadoUsSigno = consumoAutorizadoUsSigno;
    }

    public String getConsumoAutorizadoUsTexto() {
        return consumoAutorizadoUsTexto;
    }

    public void setConsumoAutorizadoUsTexto(String consumoAutorizadoUsTexto) {
        this.consumoAutorizadoUsTexto = consumoAutorizadoUsTexto;
    }

    public Character getAdelantoAutorizadoBsSigno() {
        return adelantoAutorizadoBsSigno;
    }

    public void setAdelantoAutorizadoBsSigno(Character adelantoAutorizadoBsSigno) {
        this.adelantoAutorizadoBsSigno = adelantoAutorizadoBsSigno;
    }

    public String getAdelantoAutorizadoBsTexto() {
        return adelantoAutorizadoBsTexto;
    }

    public void setAdelantoAutorizadoBsTexto(String adelantoAutorizadoBsTexto) {
        this.adelantoAutorizadoBsTexto = adelantoAutorizadoBsTexto;
    }

    public Character getAdelantoAutorizadoUsSigno() {
        return adelantoAutorizadoUsSigno;
    }

    public void setAdelantoAutorizadoUsSigno(Character adelantoAutorizadoUsSigno) {
        this.adelantoAutorizadoUsSigno = adelantoAutorizadoUsSigno;
    }

    public String getAdelantoAutorizadoUsTexto() {
        return adelantoAutorizadoUsTexto;
    }

    public void setAdelantoAutorizadoUsTexto(String adelantoAutorizadoUsTexto) {
        this.adelantoAutorizadoUsTexto = adelantoAutorizadoUsTexto;
    }

    public Character getAjustesBsSigno() {
        return ajustesBsSigno;
    }

    public void setAjustesBsSigno(Character ajustesBsSigno) {
        this.ajustesBsSigno = ajustesBsSigno;
    }

    public String getAjustesBsTexto() {
        return ajustesBsTexto;
    }

    public void setAjustesBsTexto(String ajustesBsTexto) {
        this.ajustesBsTexto = ajustesBsTexto;
    }

    public Character getAjustesUsSigno() {
        return ajustesUsSigno;
    }

    public void setAjustesUsSigno(Character ajustesUsSigno) {
        this.ajustesUsSigno = ajustesUsSigno;
    }

    public String getAjustesUsTexto() {
        return ajustesUsTexto;
    }

    public void setAjustesUsTexto(String ajustesUsTexto) {
        this.ajustesUsTexto = ajustesUsTexto;
    }

    public Character getCargosBsSigno() {
        return cargosBsSigno;
    }

    public void setCargosBsSigno(Character cargosBsSigno) {
        this.cargosBsSigno = cargosBsSigno;
    }

    public String getCargosBsTexto() {
        return cargosBsTexto;
    }

    public void setCargosBsTexto(String cargosBsTexto) {
        this.cargosBsTexto = cargosBsTexto;
    }

    public Character getPagoBsSigno() {
        return pagoBsSigno;
    }

    public void setPagoBsSigno(Character pagoBsSigno) {
        this.pagoBsSigno = pagoBsSigno;
    }

    public String getPagoBsTexto() {
        return pagoBsTexto;
    }

    public void setPagoBsTexto(String pagoBsTexto) {
        this.pagoBsTexto = pagoBsTexto;
    }

    public long getIdArchivoRecepcion() {
        return idArchivoRecepcion;
    }

    public void setIdArchivoRecepcion(long idArchivoRecepcion) {
        this.idArchivoRecepcion = idArchivoRecepcion;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }
}