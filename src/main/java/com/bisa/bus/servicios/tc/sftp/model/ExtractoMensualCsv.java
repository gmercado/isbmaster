package com.bisa.bus.servicios.tc.sftp.model;

import bus.plumbing.csv.annotation.FormatCsv;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by atenorio on 02/08/2017.
 */
public class ExtractoMensualCsv implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @FormatCsv(name = "EEBAEM", nullable = false, length = 4)
    private String codigoBanco;
    
    @FormatCsv(name = "EESUEM", nullable = false, length = 4)
    private String codigoSucursal;
    
    @FormatCsv(name = "EENUTCNC", nullable = false, length = 10)
    private String numeroCuenta;
    
    @FormatCsv(name = "EEPEBA", nullable = false, length = 6)
    private String periodoBalance;

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @FormatCsv(name = "EESAANBSN", nullable = false, length = 11, precision = 11, scale = 2)
    private BigDecimal saldoAnteriorBs;

    @FormatCsv(name = "EESAANUSN", nullable = false, length = 11, precision = 11, scale = 2)
    private BigDecimal saldoAnteriorUs;

    @FormatCsv(name = "EEINANN", nullable = false, length = 11, precision = 11, scale = 2)
    private BigDecimal interesAnterior;

    @FormatCsv(name = "EEPAMIANN", nullable = false, length = 11, precision = 11, scale = 2)
    private BigDecimal pagoMinimoAnterior;

    @FormatCsv(name = "EESAACBSN", nullable = false, length = 11, precision = 11, scale = 2)
    private BigDecimal saldoActualBs;

    @FormatCsv(name = "EESAACUSN", nullable = false, length = 11, precision = 11, scale = 2)
    private BigDecimal saldoActualUs;

    @FormatCsv(name = "EEINACN", nullable = false, length = 11, precision = 11, scale = 2)
    private BigDecimal interesActual;

    @FormatCsv(name = "EEPAMIACN", nullable = false, length = 11, precision = 11, scale = 2)
    private BigDecimal pagoMinimoActual;
    
    @FormatCsv(name = "EEFEVEAN", nullable = false, length = 8)
    private String fechaVencimientoAnterior;
    
    @FormatCsv(name = "EEFEVEAC", nullable = false, length = 8)
    private String fechaVencimientoActual;
    
    @FormatCsv(name = "EETAEFCO", nullable = false, length = 9)
    private String tasaEfectivaActivaConsumo;
    
    @FormatCsv(name = "EETAEFAE", nullable = false, length = 9)
    private String tasaEfectivaActivaAvance;
    
    @FormatCsv(name = "EETANOAC", nullable = false, length = 9)
    private String tasaNominalActiva;
    
    @FormatCsv(name = "EEFEVEPR", nullable = false, length = 8)
    private String fechaProximoVencimiento;

    @FormatCsv(name = "EELIFIN", nullable = false, length = 11, precision = 11, scale = 2)
    private BigDecimal limiteFinanciamiento;

    @FormatCsv(name = "EECOPEANN", nullable = false, length = 11, precision = 11, scale = 2)
    private BigDecimal consumoPeriodoAnterior;

    @FormatCsv(name = "EECOPEACN", nullable = false, length = 11, precision = 11, scale = 2)
    private BigDecimal consumoPeriodoActualBs;

    @FormatCsv(name = "EECOPEAUSN", nullable = false, length = 11, precision = 11, scale = 2)
    private BigDecimal consumoPeriodoActualUs;
    
    @FormatCsv(name = "EENUFA", nullable = false, length = 10)
    private String numeroFactura;


    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getCodigoSucursal() {
        return codigoSucursal;
    }

    public void setCodigoSucursal(String codigoSucursal) {
        this.codigoSucursal = codigoSucursal;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getPeriodoBalance() {
        return periodoBalance;
    }

    public void setPeriodoBalance(String periodoBalance) {
        this.periodoBalance = periodoBalance;
    }

    public BigDecimal getSaldoAnteriorBs() {
        return saldoAnteriorBs;
    }

    public void setSaldoAnteriorBs(BigDecimal saldoAnteriorBs) {
        this.saldoAnteriorBs = saldoAnteriorBs;
    }

    public BigDecimal getSaldoAnteriorUs() {
        return saldoAnteriorUs;
    }

    public void setSaldoAnteriorUs(BigDecimal saldoAnteriorUs) {
        this.saldoAnteriorUs = saldoAnteriorUs;
    }

    public BigDecimal getInteresAnterior() {
        return interesAnterior;
    }

    public void setInteresAnterior(BigDecimal interesAnterior) {
        this.interesAnterior = interesAnterior;
    }

    public BigDecimal getPagoMinimoAnterior() {
        return pagoMinimoAnterior;
    }

    public void setPagoMinimoAnterior(BigDecimal pagoMinimoAnterior) {
        this.pagoMinimoAnterior = pagoMinimoAnterior;
    }

    public BigDecimal getSaldoActualBs() {
        return saldoActualBs;
    }

    public void setSaldoActualBs(BigDecimal saldoActualBs) {
        this.saldoActualBs = saldoActualBs;
    }

    public BigDecimal getSaldoActualUs() {
        return saldoActualUs;
    }

    public void setSaldoActualUs(BigDecimal saldoActualUs) {
        this.saldoActualUs = saldoActualUs;
    }

    public BigDecimal getInteresActual() {
        return interesActual;
    }

    public void setInteresActual(BigDecimal interesActual) {
        this.interesActual = interesActual;
    }

    public BigDecimal getPagoMinimoActual() {
        return pagoMinimoActual;
    }

    public void setPagoMinimoActual(BigDecimal pagoMinimoActual) {
        this.pagoMinimoActual = pagoMinimoActual;
    }

    public String getFechaVencimientoAnterior() {
        return fechaVencimientoAnterior;
    }

    public void setFechaVencimientoAnterior(String fechaVencimientoAnterior) {
        this.fechaVencimientoAnterior = fechaVencimientoAnterior;
    }

    public String getFechaVencimientoActual() {
        return fechaVencimientoActual;
    }

    public void setFechaVencimientoActual(String fechaVencimientoActual) {
        this.fechaVencimientoActual = fechaVencimientoActual;
    }

    public String getTasaEfectivaActivaConsumo() {
        return tasaEfectivaActivaConsumo;
    }

    public void setTasaEfectivaActivaConsumo(String tasaEfectivaActivaConsumo) {
        this.tasaEfectivaActivaConsumo = tasaEfectivaActivaConsumo;
    }

    public String getTasaEfectivaActivaAvance() {
        return tasaEfectivaActivaAvance;
    }

    public void setTasaEfectivaActivaAvance(String tasaEfectivaActivaAvance) {
        this.tasaEfectivaActivaAvance = tasaEfectivaActivaAvance;
    }

    public String getTasaNominalActiva() {
        return tasaNominalActiva;
    }

    public void setTasaNominalActiva(String tasaNominalActiva) {
        this.tasaNominalActiva = tasaNominalActiva;
    }

    public String getFechaProximoVencimiento() {
        return fechaProximoVencimiento;
    }

    public void setFechaProximoVencimiento(String fechaProximoVencimiento) {
        this.fechaProximoVencimiento = fechaProximoVencimiento;
    }

    public BigDecimal getLimiteFinanciamiento() {
        return limiteFinanciamiento;
    }

    public void setLimiteFinanciamiento(BigDecimal limiteFinanciamiento) {
        this.limiteFinanciamiento = limiteFinanciamiento;
    }

    public BigDecimal getConsumoPeriodoAnterior() {
        return consumoPeriodoAnterior;
    }

    public void setConsumoPeriodoAnterior(BigDecimal consumoPeriodoAnterior) {
        this.consumoPeriodoAnterior = consumoPeriodoAnterior;
    }

    public BigDecimal getConsumoPeriodoActualBs() {
        return consumoPeriodoActualBs;
    }

    public void setConsumoPeriodoActualBs(BigDecimal consumoPeriodoActualBs) {
        this.consumoPeriodoActualBs = consumoPeriodoActualBs;
    }

    public BigDecimal getConsumoPeriodoActualUs() {
        return consumoPeriodoActualUs;
    }

    public void setConsumoPeriodoActualUs(BigDecimal consumoPeriodoActualUs) {
        this.consumoPeriodoActualUs = consumoPeriodoActualUs;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    @Override
    public String toString() {
        return "ExtractoMensualCsv{" +
                "codigoBanco='" + codigoBanco + '\'' +
                ", codigoSucursal='" + codigoSucursal + '\'' +
                ", numeroCuenta='" + numeroCuenta + '\'' +
                ", periodoBalance='" + periodoBalance + '\'' +
                ", saldoAnteriorBs=" + saldoAnteriorBs +
                ", saldoAnteriorUs=" + saldoAnteriorUs +
                ", interesAnterior=" + interesAnterior +
                ", pagoMinimoAnterior=" + pagoMinimoAnterior +
                ", saldoActualBs=" + saldoActualBs +
                ", saldoActualUs=" + saldoActualUs +
                ", interesActual=" + interesActual +
                ", pagoMinimoActual=" + pagoMinimoActual +
                ", fechaVencimientoAnterior='" + fechaVencimientoAnterior + '\'' +
                ", fechaVencimientoActual='" + fechaVencimientoActual + '\'' +
                ", tasaEfectivaActivaConsumo='" + tasaEfectivaActivaConsumo + '\'' +
                ", tasaEfectivaActivaAvance='" + tasaEfectivaActivaAvance + '\'' +
                ", tasaNominalActiva='" + tasaNominalActiva + '\'' +
                ", fechaProximoVencimiento='" + fechaProximoVencimiento + '\'' +
                ", limiteFinanciamiento=" + limiteFinanciamiento +
                ", consumoPeriodoAnterior=" + consumoPeriodoAnterior +
                ", consumoPeriodoActualBs=" + consumoPeriodoActualBs +
                ", consumoPeriodoActualUs=" + consumoPeriodoActualUs +
                ", numeroFactura='" + numeroFactura + '\'' +
                '}';
    }
}
