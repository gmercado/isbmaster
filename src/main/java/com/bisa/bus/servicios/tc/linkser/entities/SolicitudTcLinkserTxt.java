package com.bisa.bus.servicios.tc.linkser.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by atenorio on 09/11/2017.
 */
@Entity
@Table(name = "TCPSOLTXT")
public class SolicitudTcLinkserTxt implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "TCSOLI", nullable = false)
    private String texto;

    public SolicitudTcLinkserTxt(String texto){
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
