package com.bisa.bus.servicios.tc.sftp.api;

import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.rpg.ImposibleLlamarProgramaRpgException;
import bus.plumbing.csv.GeneradorCSV;
import bus.plumbing.csv.LecturaCSV;
import bus.plumbing.file.ArchivoBase;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.tc.sftp.dao.*;
import com.bisa.bus.servicios.tc.sftp.entities.*;
import com.bisa.bus.servicios.tc.sftp.model.*;
import com.bisa.bus.servicios.tc.sftp.rpg.IRpgProcesarSolicitudAltas;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import com.ibm.as400.access.ConnectionPoolException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

/**
 * Created by atenorio on 25/05/2017.
 */
public class SftpRecepcionSolicitudAlta implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SftpRecepcionSolicitudAlta.class);


    private final MedioAmbiente medioAmbiente;
    private final ArchivoBase archivoBase;
    private final SolicitudTCDao iSolicitudTCDao;
    private final SolicitudHistoricoTCDao iSolicitudHistoricoTCDao;
    private final SolicitudTCEmpresarialDao iSolicitudTCEmpresarialDao;
    private final SolicitudHistoricoTCEmpresarialDao iSolicitudHistoricoTCEmpresarialDao;
    private final IRpgProcesarSolicitudAltas iRpgProcesarSolicitudAltas;
    private final NotificacionesCorreo notificacionesCorreo;

    private final RespuestaCuentaDao iRespuestaCuentaDao;
    private final RespuestaTarjetaDao iRespuestaTarjetaDao;

    SftpConfiguracion configuracion;
    SftpTransferencia sftpTransferencia;
    private String nombreArchivoProcesado = "";

    @Inject
    public SftpRecepcionSolicitudAlta(ArchivoBase archivoBase,
                                      MedioAmbiente medioAmbiente,
                                      SolicitudTCDao iSolicitudTCDao, SolicitudHistoricoTCDao iSolicitudHistoricoTCDao,
                                      SolicitudTCEmpresarialDao iSolicitudTCEmpresarialDao,
                                      SolicitudHistoricoTCEmpresarialDao iSolicitudHistoricoTCEmpresarialDao,
                                      IRpgProcesarSolicitudAltas iRpgProcesarSolicitudAltas, NotificacionesCorreo notificacionesCorreo,
                                      RespuestaCuentaDao iRespuestaCuentaDao, RespuestaTarjetaDao iRespuestaTarjetaDao,
                                      SftpConfiguracion configuracion, SftpTransferencia sftpTransferencia) {
        this.medioAmbiente = medioAmbiente;
        this.archivoBase = archivoBase;
        this.iSolicitudTCDao = iSolicitudTCDao;
        this.iSolicitudHistoricoTCDao = iSolicitudHistoricoTCDao;
        this.iRpgProcesarSolicitudAltas = iRpgProcesarSolicitudAltas;
        this.iSolicitudTCEmpresarialDao = iSolicitudTCEmpresarialDao;
        this.iSolicitudHistoricoTCEmpresarialDao = iSolicitudHistoricoTCEmpresarialDao;
        this.notificacionesCorreo = notificacionesCorreo;
        this.iRespuestaCuentaDao = iRespuestaCuentaDao;
        this.iRespuestaTarjetaDao = iRespuestaTarjetaDao;
        this.configuracion = configuracion;
        this.sftpTransferencia = sftpTransferencia;
    }

    /**
     * Metodo para procesar recepcion de archivo de respuesta a solicitud de alta de tarjetas de servidor remoto SFTP.
     *
     * @return boolean: verdadero o falso para indicar procesamiento finalizado satisfactoriamente
     */
    String PREF = "#fecha";

    public boolean procesarArchivo() {
        boolean ok = false;
        String detalle = "";
        LOGGER.debug("Inicia proceso recepcion de archivo de solicitud");
        LOGGER.debug("1) Verificar archivo pendiente de respuesta");
        String nombreArchivoRegex = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_ATC, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_ATC_DEFAULT);
        String nombreArchivoRegexForzado = medioAmbiente.getValorDe(Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_ATC_FORZADO, Variables.FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_ATC_FORZADO_DEFAULT);
        String nombreArchivo[] = Iterables.toArray(Splitter.on("|").trimResults().split(nombreArchivoRegex), String.class);

        // El proceso normal realiza la recepcion de archivo de alta de un dia anterior,
        // debido a la disponibilidad del archivo de respuesta.
        Calendar hoy = Calendar.getInstance();
        hoy.add(Calendar.DAY_OF_YEAR, -1);
        Date fechaProcesar = hoy.getTime();


        if (StringUtils.isEmpty(nombreArchivoRegexForzado) || "NULL".equals(nombreArchivoRegexForzado)) {
            // Procesar a fecha determinada por parametro
            String fechaProceso = medioAmbiente.getValorDe(Variables.FECHA_DE_PROCESO_ATC, Variables.FECHA_DE_PROCESO_ATC_DEFAULT);
            if (StringUtils.isNotEmpty(fechaProceso) && !"0".equals(fechaProceso)) {
                fechaProcesar = FormatosUtils.deYYYYMMDDaFecha(fechaProceso);
                if (fechaProcesar == null) {
                    notificacionesCorreo.notificarError("Error en fecha de proceso recepci\u00F3n alta", "");
                    return false;
                }
            }
            nombreArchivoRegex = nombreArchivo[0].replace(PREF, new SimpleDateFormat(nombreArchivo[1]).format(fechaProcesar));
        } else {
            nombreArchivoRegex = nombreArchivoRegexForzado;
        }

        LOGGER.debug("2) Decargar archivo de servidor SFTP");
        prepararConfiguracionSftp();

        LOGGER.debug("2.1) Descarga de archivo de a servidor SFTP con la convenci\u00F3n de nombre: {}", nombreArchivoRegex);
        ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.RAL);
        // Verificar respuesta en carpeta de descarte
        if (!ok) {
            String remotePath = medioAmbiente.getValorDe(Variables.FTP_RUTA_DESCARTE_ALTAS_ATC, Variables.FTP_RUTA_DESCARTE_ALTAS_ATC_DEFAULT);
            ok = sftpTransferencia.descargarArchivo(nombreArchivoRegex, TipoArchivo.RAL);
            if (ok) {
                // Respuesta a proceso para permitir reintento para archivo de misma fecha
                detalle = "Archivo de respuesta descartado en servidor SFTP.";
                LOGGER.warn(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RAL, detalle);
                return false;
            }
        }

        LOGGER.debug("3) Validar archivo descargado");
        LOGGER.debug("3.1) Validar estructura");
        LOGGER.debug("3.2) Validar mapeo");

        // Cerrar el registro con error y permitir reintento
        if (!ok || sftpTransferencia.getArchivoLocal() == null) {
            sftpTransferencia.transferenciaConErrorReintento("");
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RAL, "No existen archivos de respuesta en servidor SFTP.");
            LOGGER.warn("No existen archivos de respuesta en servidor SFTP.");
            return false;
        }

        // Procesamiento de archivo local
        File archivo = sftpTransferencia.getArchivoLocal();
        RespuestaMapeoSolicitud mapeo = mapearArchivo(archivo);

        if (!mapeo.existeRespuesta()) {
            LOGGER.warn("Error en mapeo de archivo de respuesta");
            sftpTransferencia.transferenciaConErrorReintento("");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.RAL, "Error en mapeo de archivo de respuesta.");
            return false;
        }


        LOGGER.debug("4) Actualizar tabla de solicitudes");
        LOGGER.debug("4.1) Verificar si un registro se encuentra en un estado inconsistente");
        int totalRegistros = 0;
        int totalRegistrosError = 0;
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();

        List<SolicitudCompleto> solicitudes = mapeo.getSolicitudes();
        for (SolicitudCompleto solicitud : solicitudes) {
            // Validar que se realizo el mapeo de forma exitosa en todas las estructuras
            if (solicitud == null) {
                LOGGER.error("Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                sftpTransferencia.transferenciaConErrorReintento("");
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RAL, "Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                return false;
            }
            // Validar que se tenga pendiente de respuesta las solicitudes de la respuesta
            SolicitudTC solicitudDB = iSolicitudTCDao.getSolicitudCodigoExterno(solicitud.getSocfnusol());
            if (solicitudDB == null) {
                detalle = "Error en actualización de informacion recibida no existe registro de solicitud.";
                LOGGER.error(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RAL, detalle);
                return false;
            }
        }

        List<SolicitudCompletoEmpresarial> solicitudesEmpresariales = mapeo.getSolicitudesEmpresariales();
        for (SolicitudCompletoEmpresarial solicitud : solicitudesEmpresariales) {
            // Validar que se realizo el mapeo de forma exitosa en todas las estructuras
            if (solicitud == null) {
                LOGGER.error("Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                sftpTransferencia.transferenciaConErrorReintento("");
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RAL, "Error en mapeo de una estructura se requiere revisar el archivo de respuesta.");
                return false;
            }
            // Validar que se tenga pendiente de respuesta las solicitudes de la respuesta
            SolicitudTCEmpresarial solicitudDB = iSolicitudTCEmpresarialDao.getSolicitudCodigoExterno(solicitud.getSocfnusol());
            if (solicitudDB == null) {
                detalle = "Error en actualización de informacion recibida no existe registro de solicitud.";
                LOGGER.error(detalle);
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RAL, detalle);
                return false;
            }
        }

        // Limpiar tablas temporales
        if (!iRespuestaCuentaDao.limpiarTodo()) {
            detalle = "Error al limpiar tabla temporal de cuentas.";
            LOGGER.error(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RAL, detalle);
            return false;
        }
        if (!iRespuestaTarjetaDao.limpiarTodo()) {
            detalle = "Error al limpiar tabla temporal de tarjetas.";
            LOGGER.error(detalle);
            sftpTransferencia.transferenciaConErrorReintento(detalle);
            notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RAL, detalle);
            return false;
        }

        for (SolicitudCompleto solicitud : solicitudes) {
            SolicitudTC solicitudDB = iSolicitudTCDao.getSolicitudCodigoExterno(solicitud.getSocfnusol());

            StringBuffer codRes = new StringBuffer();
            solicitudDB.setSogacoer(solicitud.getSogacoer());
            codRes.append(solicitud.getSogacoer());
            solicitudDB.setSocccoer(solicitud.getSocccoer());
            codRes.append(solicitud.getSocccoer());
            solicitudDB.setSocfcoer(solicitud.getSocfcoer());
            codRes.append(solicitud.getSocfcoer());
            solicitudDB.setSoeicoer(solicitud.getSoeicoer());
            codRes.append(solicitud.getSoeicoer());
            solicitudDB.setSoadcoerc(solicitud.getSoadcoerc());
            codRes.append(solicitud.getSoadcoerc());
            solicitudDB.setSoadcoerp(solicitud.getSoadcoerp());
            codRes.append(solicitud.getSoadcoerp());
            solicitudDB.setSoprcoer(solicitud.getSoprcoer());
            codRes.append(solicitud.getSoprcoer());

            solicitudDB.setIdArchivoRecepcion(BigInteger.valueOf(sftpTransferencia.getFtpArchivoTransferencia().getId().longValue()));
            // grabar numero de cuenta
            solicitudDB.setSoganutcnc(solicitud.getSoganutcnc());
            solicitudDB.setSoprnutc(solicitud.getSoprnutc());
            solicitudDB.setSoprfevetc(solicitud.getSoprfevetc());
            solicitudDB.setSoprsenutc(solicitud.getSoprsenutc());
            solicitudDB.setEstado((Pattern.compile("[0]*").matcher(codRes).matches()) ? EstadoSolicitudTC.PROC : EstadoSolicitudTC.ERRP);

            solicitudDB.setSoferes(new Date());
            ok = iSolicitudTCDao.guardar(solicitudDB, "ISB");
            SolicitudHistoricoTC solicitudHistorico = transferObject.convert(solicitudDB, SolicitudHistoricoTC.class);
            solicitudHistorico.setSoprnutc("");
            ok = iSolicitudHistoricoTCDao.guardar(solicitudHistorico, "ISB");
            // Grabar
            if (ok && solicitudDB.getEstado() != null && solicitudDB.getEstado().equals(EstadoSolicitudTC.PROC)) {
                LOGGER.debug("====================================");
                LOGGER.debug("NRO DOC:{}", solicitudDB.getSocfdocl().trim());
                LOGGER.debug("TOP DOC:{}", solicitudDB.getSocftido());
                LOGGER.debug("NRO SOL:{}", solicitudDB.getSocfnusol());

                RespuestaCuenta cuenta = new RespuestaCuenta();
                cuenta.setNumeroCuenta(solicitudDB.getSoganutcnc());
                cuenta.setNumeroTarjeta(solicitudDB.getSoprnutc());

                cuenta.setSucursal(solicitudDB.getSosucu());
                String cuentaTexto = GeneradorCSV.generarCsv(cuenta);
                iRespuestaCuentaDao.guardar(new RespuestaSolicitudCuenta(cuentaTexto), "ISB");

                RespuestaTarjeta tarjeta = new RespuestaTarjeta();
                tarjeta.setNumeroTarjeta(solicitudDB.getSoprnutc());

                tarjeta.setTipoTarjeta(solicitudDB.getTipoTarjeta());
                tarjeta.setNumeroDocumento(solicitudDB.getSocfdocl().trim());
                //ParametrosAtc parametro = parametrosAtcDao.getParametroBanco("NTIDOPE", solicitudDB.getSocftido() + "");
                tarjeta.setTipoDocumento(solicitudDB.getTipoDocumento());
                tarjeta.setNumeroSolicitud(solicitudDB.getSocfnusol());
                String tarjetaTexto = GeneradorCSV.generarCsv(tarjeta);

                LOGGER.debug("====================================");
                LOGGER.debug("CUENTA:{}", cuentaTexto);
                LOGGER.debug("TARJETA:{}", tarjetaTexto);
                LOGGER.debug("====================================");
                iRespuestaTarjetaDao.guardar(new RespuestaSolicitudTarjeta(tarjetaTexto), "ISB");
                totalRegistros++;
                // Limpiando campo numero tarjeta
                solicitudDB.setSoprnutc("");
                ok = iSolicitudTCDao.guardar(solicitudDB, "ISB");
            } else {
                totalRegistrosError++;
            }

        }

        for (SolicitudCompletoEmpresarial solicitud : solicitudesEmpresariales) {
            SolicitudTCEmpresarial solicitudDB = iSolicitudTCEmpresarialDao.getSolicitudCodigoExterno(solicitud.getSocfnusol());

            StringBuffer codRes = new StringBuffer();
            solicitudDB.setSogacoer(solicitud.getSogacoer());
            codRes.append(solicitud.getSogacoer());
            solicitudDB.setSocccoer(solicitud.getSocccoer());
            codRes.append(solicitud.getSocccoer());
            solicitudDB.setSocfcoer(solicitud.getSocfcoer());
            codRes.append(solicitud.getSocfcoer());
            solicitudDB.setSoeicoer(solicitud.getSoeicoer());
            codRes.append(solicitud.getSoeicoer());
            //solicitudDB.setSoadcoerc(solicitud.getSoadcoerc());
            //codRes.append(solicitud.getSoadcoerc());
            solicitudDB.setSoadcoerp(solicitud.getSoadcoerp());
            codRes.append(solicitud.getSoadcoerp());
            solicitudDB.setSoprcoer(solicitud.getSoprcoer());
            codRes.append(solicitud.getSoprcoer());
            solicitudDB.setIdArchivoRecepcion(sftpTransferencia.getFtpArchivoTransferencia().getId().longValue());

            // grabar numero de cuenta
            solicitudDB.setSoganutcnc(solicitud.getSoganutcnc());
            solicitudDB.setSoprnutc(solicitud.getSoprnutc());
            solicitudDB.setSoprfevetc(solicitud.getSoprfevetc());
            solicitudDB.setSoprsenutc(solicitud.getSoprsenutc());
            solicitudDB.setEstado((Pattern.compile("[0]*").matcher(codRes).matches()) ? EstadoSolicitudTC.PROC : EstadoSolicitudTC.ERRP);

            solicitudDB.setSoferes(new Date());
            ok = iSolicitudTCEmpresarialDao.guardar(solicitudDB, "ISB");
            SolicitudHistoricoTCEmpresarial solicitudHistorico = transferObject.convert(solicitudDB, SolicitudHistoricoTCEmpresarial.class);

            ok = iSolicitudHistoricoTCEmpresarialDao.guardar(solicitudHistorico, "ISB");
            // Grabar en base de datos
            if (ok && solicitudDB.getEstado() != null
                    && EstadoSolicitudTC.PROC.equals(solicitudDB.getEstado())) { //&& solicitudDB.getEstado().equals(EstadoSolicitudTC.PROC)
                LOGGER.debug("====================================");
                LOGGER.debug("NRO DOC:{}", solicitudDB.getSocjdoem().trim());
                LOGGER.debug("TOP DOC:{}", solicitudDB.getTipoDocumento());
                LOGGER.debug("NRO SOL:{}", solicitudDB.getSocfnusol());

                RespuestaCuenta cuenta = new RespuestaCuenta();
                cuenta.setNumeroCuenta(solicitudDB.getSoganutcnc());
                cuenta.setNumeroTarjeta(solicitudDB.getSoprnutc());

                cuenta.setSucursal(solicitudDB.getSosucu());
                String cuentaTexto = GeneradorCSV.generarCsv(cuenta);
                iRespuestaCuentaDao.guardar(new RespuestaSolicitudCuenta(cuentaTexto), "ISB");

                RespuestaTarjeta tarjeta = new RespuestaTarjeta();
                tarjeta.setNumeroTarjeta(solicitudDB.getSoprnutc());
                tarjeta.setTipoTarjeta(solicitudDB.getTipoTarjeta());
                tarjeta.setNumeroDocumento(solicitudDB.getSocjdoem().trim());
                tarjeta.setTipoDocumento(solicitudDB.getTipoDocumento());
                tarjeta.setNumeroSolicitud(solicitudDB.getSocfnusol());
                String tarjetaTexto = GeneradorCSV.generarCsv(tarjeta);

                LOGGER.debug("====================================");
                LOGGER.debug("CUENTA:{}", cuentaTexto);
                LOGGER.debug("TARJETA:{}", tarjetaTexto);
                LOGGER.debug("====================================");
                iRespuestaTarjetaDao.guardar(new RespuestaSolicitudTarjeta(tarjetaTexto), "ISB");
                totalRegistros++;
            } else {
                totalRegistrosError++;
            }
        }

        // Eliminar archivo de servidor local
        if (FileUtils.deleteQuietly(archivo)) {
            LOGGER.info("Se ha eliminado el archivo de altas, {}", archivo.getName());
        }

        LOGGER.debug("4.2) Verificar si un registro se encuentra en un estado inconsistente");

        if ("true".equals(medioAmbiente.getValorDe(Variables.PROGRAMA_TC_RECEPCION_ALTA_ATC_HABILITADO,
                Variables.PROGRAMA_TC_RECEPCION_ALTA_ATC_HABILITADO_DEFAULT))) {
            LOGGER.debug("5) Llamar a programa para procesamiento de alta");
            try {
                ok = esCargaDeSolicitudes();
            } catch (Exception e) {
                LOGGER.error("Error al llamar programa AS400, generador de solicitudes aprobadas. {}", e);
                detalle = "Error al llamar programa AS400, generador de solicitudes aprobadas.";
                sftpTransferencia.transferenciaConErrorReintento(detalle);
                notificacionesCorreo.notificarErrorTransferencia(nombreArchivoRegex, TipoArchivo.RAL, detalle);
                return false;
            }
        }
        // Modificar estado de solicitudes de alta y notificar por correo
        if (ok) {
            sftpTransferencia.transferenciaCompleta("");
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.RAL,
                    "Transferencia completa con respuesta correcta a " + totalRegistros + " solicitudes y respuesta con error a "+ totalRegistrosError +".");
            LOGGER.info("Proceso finalizado de recepcion de solicitud.");
        } else {
            detalle = "Error en proceso de recepcion de solicitudes.";
            LOGGER.warn(detalle);
            sftpTransferencia.transferenciaConErrorInterno(detalle);
            notificacionesCorreo.notificarCorrectaTransferencia(nombreArchivoRegex, TipoArchivo.RAL, detalle);
        }
        return ok;
    }

    public boolean prepararConfiguracionSftp() {
        configuracion.inicializar();
        configuracion.setRemotePath(medioAmbiente.getValorDe(Variables.FTP_RUTA_RECEPCION_ALTAS_ATC, Variables.FTP_RUTA_RECEPCION_ALTAS_ATC_DEFAULT));
        return sftpTransferencia.configurar(configuracion);
    }

    public boolean esCargaDeSolicitudes() throws IllegalArgumentException, IllegalStateException {
        boolean result = false;
        try {
            result = iRpgProcesarSolicitudAltas.ejecutar();
            return result;
        } catch (ImposibleLeerRespuestaException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(Imposible leer respuesta)");
        } catch (IOException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(IOException)");
        } catch (TransaccionEfectivaException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(TransaccionEfectivaException)");
        } catch (SistemaCerradoException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(SistemaCerradoException)");
        } catch (ImposibleLlamarProgramaRpgException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(ImposibleLlamarProgramaRpgException)");
        } catch (ConnectionPoolException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(ConnectionPoolException)");
        } catch (TimeoutException e) {
            throw new IllegalStateException("No se puede cargar solicitudes(Tiempo de espera agotado)");
        }
    }

    public RespuestaMapeoSolicitud mapearArchivo(File archivo) {
        RespuestaMapeoSolicitud respuesta = new RespuestaMapeoSolicitud();
        FileInputStream is = null;
        BufferedReader br = null;
        List<SolicitudCompleto> solicitudes = new ArrayList<SolicitudCompleto>();
        List<SolicitudCompletoEmpresarial> solicitudesEmpresariales = new ArrayList<SolicitudCompletoEmpresarial>();
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();
        try {
            is = new FileInputStream(archivo);
            br = new BufferedReader(new InputStreamReader(is));
            String linea = br.readLine();
            List<String> lineas = new ArrayList<String>();
            boolean esPersonaNatural = true;
            StringBuffer solicitudTexto = new StringBuffer();
            while (linea != null) {
                if (StringUtils.isNotEmpty(linea) && linea.length() > 300) {
                    String cabecera = linea.substring(0, 2);
                    if (cabecera.equals("GA")) {
                        if (StringUtils.isNotEmpty(solicitudTexto.toString())) {
                            if (esPersonaNatural) {
                                lineas.add(solicitudTexto.toString());
                            } else {
                                lineas.add("CJ" + solicitudTexto.toString());
                                esPersonaNatural = true;
                            }
                            solicitudTexto = new StringBuffer();
                        }
                    }
                    // Verificar que exista persona juridica en estructura
                    if (cabecera.equals("CJ")) {
                        esPersonaNatural = false;
                    }
                    solicitudTexto.append(linea);
                }
                linea = br.readLine();
            }
            if (StringUtils.isNotEmpty(solicitudTexto.toString())) {
                if (esPersonaNatural) {
                    lineas.add(solicitudTexto.toString());
                } else {
                    lineas.add("CJ" + solicitudTexto.toString());
                }
            }
            for (String fila : lineas) {
                LOGGER.debug("Detalle de registro recuperado: {}", fila);
                if (StringUtils.startsWith(fila, "CJ")) {
                    fila = fila.substring(2);
                    SolicitudCompletoEmpresarial solicitud = LecturaCSV.procesar(SolicitudCompletoEmpresarial.class, fila);
                    respuesta.getSolicitudesEmpresariales().add(solicitud);
                } else {
                    SolicitudCompleto solicitud = LecturaCSV.procesar(SolicitudCompleto.class, fila);
                    respuesta.getSolicitudes().add(solicitud);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al procesar el archivo "
                    + archivo.getName() + ".", e);
            return new RespuestaMapeoSolicitud();
        } finally {
            IOUtils.closeQuietly(is);
        }
        return respuesta;
    }

    public String getNombreArchivoProcesado() {
        return nombreArchivoProcesado;
    }

    public void setNombreArchivoProcesado(String nombreArchivoProcesado) {
        this.nombreArchivoProcesado = nombreArchivoProcesado;
    }
}
