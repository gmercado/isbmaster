package com.bisa.bus.servicios.tc.sftp.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by atenorio on 20/06/2017.
 * ATCUC111 Registros de movimiento emisor mensual contiene movimientos desde perspectiva emisora.
 * Contiene cargos, intereses, comisiones como parte del proceso de cierre mensual.
 *
 */
@Entity
@Table(name = "TCPANMOMS")
public class MovimientoMensualEmisor implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private
    IdMovimientoMensualEmisor idMovimientoMensualEmisor;

    @Basic(optional = false)
    @Column(name = "MMBCOEMI", nullable = false, length = 4) //MEBAEM
    private String codigoBanco;
    @Basic(optional = false)
    @Column(name = "MMSUCEMI", nullable = false, length = 4) //MESUEM
    private String sucursalEmisor;
//    @Basic(optional = false)
//    @Column(name = "MENUTCNC", nullable = false, length = 10)
//    private String numeroCuenta;
    @Basic(optional = false)
    @Column(name = "MMNUMTAR", nullable = false, length = 19) //MENUTCEN
    private String numeroTarjeta;
    @Basic(optional = false)
    @Column(name = "MMCODCAC", nullable = false, length = 12) //MEACTC
    private String codigoAceptanteTc;
    @Basic(optional = false)
    @Column(name = "MMCATESCTA", nullable = false, length = 4) //MECAESCU
    private String categoriaEstadoCuenta;

    @Basic(optional = false)
    @Column(name = "MMCATFIN", nullable = false, length = 4) //MECAFI
    private String categoriaFinanc;

    @Basic(optional = false)
    @Column(name = "MMCATBAL", nullable = false, length = 4) //MECABA
    private String categoriaBalance;
//    @Basic(optional = false)
//    @Column(name = "MENUCOMP", nullable = false, length = 12)
//    private String numeroComprobante;
    @Basic(optional = false)
    @Column(name = "MMFECTRN", nullable = false, length = 8) //MEFETR
    private String fechaTransaccion;
    @Basic(optional = false)
    @Column(name = "MMIMPORS", nullable = false)  //MEIMMOORS
    private Character importeMonedaOrigenSigno;
    @Basic(optional = false)
    @Column(name = "MMIMPOR", nullable = false, length = 10) //MEIMMOOR
    private String importeMonedaOrigenTexto;

    @Basic(optional = false)
    @Column(name = "MMIMPORN", nullable = false, precision = 11, scale = 2) //MEIMMOORN
    private BigDecimal importeMonedaOrigen;

    @Basic(optional = false)
    @Column(name = "MMMONOR", nullable = false, length = 3) //MEMOOR
    private String monedaOrigen;

    @Basic(optional = false)
    @Column(name = "MMIMPBALS", nullable = false)  //MEIMMOBAS
    private Character importeMonedaBalanceSigno;
    @Basic(optional = false)
    @Column(name = "MMIMPBAL", nullable = false, length = 10)  //MEIMMOBA
    private String importeMonedaBalanceTexto;
    @Basic(optional = false)
    @Column(name = "MMIMPBALN", nullable = false, precision = 11, scale = 2)
    private BigDecimal importeMonedaBalance;
    @Basic(optional = false)
    @Column(name = "MMMONBAL", nullable = false, length = 3)  //MEMOBA
    private String monedaBalance;

    @Basic(optional = false)
    @Column(name = "MMIMPLIQS", nullable = false) //MEIMMOLIS
    private Character importeMonedaLiquidacionSigno;
    @Basic(optional = false)
    @Column(name = "MMIMPLIQ", nullable = false, length = 10) //MEIMMOLI
    private String importeMonedaLiquidacionTexto;

    @Basic(optional = false)
    @Column(name = "MMIMPLIQN", nullable = false, precision = 11, scale = 2) //MEIMMOLIN
    private BigDecimal importeMonedaLiquidacion;

    @Basic(optional = false)
    @Column(name = "MMMONLIQ", nullable = false, length = 3) //MEMOLI
    private String monedaLiquidacion;

    @Basic(optional = false)
    @Column(name = "MMSCOPE", nullable = false)  //MEALTR
    private Character alcanceTransaccion;
    @Basic(optional = false)
    @Column(name = "MMLOTE", nullable = false, length = 4) //MENULO
    private String numeroLote;
    // Indica modo de entrada de transaccion
    @Basic(optional = false)
    @Column(name = "MMPEM", nullable = false, length = 2) //MEMOTR
    private String modoTransaccion;
    // Nombre de establecimiento o dispositivo
    @Basic(optional = false)
    @Column(name = "MMTEXTO", nullable = false, length = 30) //MENOEST
    private String nombreEstablecimiento;
    @Basic(optional = false)
    @Column(name = "MMMTI", nullable = false, length = 4)  //MEMTI
    private String codigoMTI;
    @Basic(optional = false)
    @Column(name = "MMCODPROC", nullable = false, length = 6) //MECOPRO
    private String codigoProceso;
    @Basic(optional = false)
    @Column(name = "MMCODAUT", nullable = false, length = 6) //MECOAUT
    private String codigoAutorizacion;
    @Basic(optional = false)
    @Column(name = "MMCODOPE", nullable = false)  //MECOOP  MECOAUT
    private Character codigoOperacion;
    @Basic(optional = false)
    @Column(name = "MMTERMID", nullable = false, length = 8) //MEIDTERM
    private String idTerminal;
    @Basic(optional = false)
    @Column(name = "MMPLCUOTA", nullable = false, length = 2) //MECACUO
    private String planCuotas;
    @Basic(optional = false)
    @Column(name = "MMNUMCUOTA", nullable = false, length = 2) //MENUCUO
    private String numeroCuota;
    @Basic(optional = false)
    @Column(name = "MMIMPADICS", nullable = false)  //MEMOADIS
    private Character montoAdicionalSigno;
    @Basic(optional = false)
    @Column(name = "MMIMPADIC", nullable = false, length = 10) //MEMOADI
    private String montoAdicionalTexto;
    @Column(name = "MMIMPADICN", nullable = false, length = 10)
    private BigDecimal montoAdicional;

    @Basic(optional = false)
    @Column(name = "MMIMPENEFS", nullable = false)  //MEIMEFECS
    private Character importeEfectivoSigno;
    @Basic(optional = false)
    @Column(name = "MMIMPENEF", nullable = false, length = 10) //MEIMEFEC
    private String importeEfectivoTexto;
    @Column(name = "MMIMPENEFN", nullable = false, length = 10)
    private BigDecimal importeEfectivo;

    @Basic(optional = false)
    @Column(name = "MMTERCAP", nullable = false, length = 3) //MECADISP
    private String capacidadDispositivo;
    @Basic(optional = false)
    @Column(name = "MMPOS", nullable = false, length = 3) //MECOINTC
    private String condicionIngreso;

//    @Basic(optional = false)
//    @Column(name = "MECADM", nullable = false)
//    private Character codigoAdm;
//    @Basic(optional = false)
//    @Column(name = "MEFEPRO1", nullable = false)
//    private int fechaProceso;

    @Basic(optional = false)
    @Column(name = "MMNUTC", nullable = false) //MENUTC
    private long cuentaTarjeta;
    @Basic(optional = false)
    @Column(name = "MMNUT1", nullable = false, length = 9) //MENUT1
    private String cuentaTarjetaReducida="";

    @Basic(optional = false)
    @Column(name = "MMEST", nullable = false, length = 4) //MEEST
    private String estado="";
    @Basic(optional = false)
    @Column(name = "MMIDRE", nullable = false) //MEIDRE
    private long idArchivoRecepcion;
    @Basic(optional = false)
    @Column(name = "MMFERE", nullable = false)  //MEFERE
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRecepcion;
    @Basic(optional = false)
    @Column(name = "MMPGCR", nullable = false, length = 10)
    private String usuarioCreador;
    @Basic(optional = false)
    @Column(name = "MMFECR", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @Column(name = "MMPGMO", nullable = false, length = 10)
    private String usuarioModificador;
    @Basic(optional = false)
    @Column(name = "MMFEMO", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getSucursalEmisor() {
        return sucursalEmisor;
    }

    public void setSucursalEmisor(String sucursalEmisor) {
        this.sucursalEmisor = sucursalEmisor;
    }



    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getCodigoAceptanteTc() {
        return codigoAceptanteTc;
    }

    public void setCodigoAceptanteTc(String codigoAceptanteTc) {
        this.codigoAceptanteTc = codigoAceptanteTc;
    }

    public String getCategoriaEstadoCuenta() {
        return categoriaEstadoCuenta;
    }

    public void setCategoriaEstadoCuenta(String categoriaEstadoCuenta) {
        this.categoriaEstadoCuenta = categoriaEstadoCuenta;
    }

    public String getCategoriaBalance() {
        return categoriaBalance;
    }

    public void setCategoriaBalance(String categoriaBalance) {
        this.categoriaBalance = categoriaBalance;
    }

    public String getCategoriaFinanc() {
        return categoriaFinanc;
    }

    public void setCategoriaFinanc(String categoriaFinanc) {
        this.categoriaFinanc = categoriaFinanc;
    }


    public String getFechaTransaccion() {
        return fechaTransaccion;
    }

    public void setFechaTransaccion(String fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    public Character getImporteMonedaOrigenSigno() {
        return importeMonedaOrigenSigno;
    }

    public void setImporteMonedaOrigenSigno(Character importeMonedaOrigenSigno) {
        this.importeMonedaOrigenSigno = importeMonedaOrigenSigno;
    }

    public String getImporteMonedaOrigenTexto() {
        return importeMonedaOrigenTexto;
    }

    public void setImporteMonedaOrigenTexto(String importeMonedaOrigenTexto) {
        this.importeMonedaOrigenTexto = importeMonedaOrigenTexto;
    }

    public BigDecimal getImporteMonedaOrigen() {
        return importeMonedaOrigen;
    }

    public void setImporteMonedaOrigen(BigDecimal importeMonedaOrigen) {
        this.importeMonedaOrigen = importeMonedaOrigen;
    }

    public String getMonedaOrigen() {
        return monedaOrigen;
    }

    public void setMonedaOrigen(String monedaOrigen) {
        this.monedaOrigen = monedaOrigen;
    }

    public Character getImporteMonedaBalanceSigno() {
        return importeMonedaBalanceSigno;
    }

    public void setImporteMonedaBalanceSigno(Character importeMonedaBalanceSigno) {
        this.importeMonedaBalanceSigno = importeMonedaBalanceSigno;
    }

    public String getImporteMonedaBalanceTexto() {
        return importeMonedaBalanceTexto;
    }

    public void setImporteMonedaBalanceTexto(String importeMonedaBalanceTexto) {
        this.importeMonedaBalanceTexto = importeMonedaBalanceTexto;
    }

    public BigDecimal getImporteMonedaBalance() {
        return importeMonedaBalance;
    }

    public void setImporteMonedaBalance(BigDecimal importeMonedaBalance) {
        this.importeMonedaBalance = importeMonedaBalance;
    }

    public String getMonedaBalance() {
        return monedaBalance;
    }

    public void setMonedaBalance(String monedaBalance) {
        this.monedaBalance = monedaBalance;
    }

    public Character getImporteMonedaLiquidacionSigno() {
        return importeMonedaLiquidacionSigno;
    }

    public void setImporteMonedaLiquidacionSigno(Character importeMonedaLiquidacionSigno) {
        this.importeMonedaLiquidacionSigno = importeMonedaLiquidacionSigno;
    }

    public String getImporteMonedaLiquidacionTexto() {
        return importeMonedaLiquidacionTexto;
    }

    public void setImporteMonedaLiquidacionTexto(String importeMonedaLiquidacionTexto) {
        this.importeMonedaLiquidacionTexto = importeMonedaLiquidacionTexto;
    }

    public BigDecimal getImporteMonedaLiquidacion() {
        return importeMonedaLiquidacion;
    }

    public void setImporteMonedaLiquidacion(BigDecimal importeMonedaLiquidacion) {
        this.importeMonedaLiquidacion = importeMonedaLiquidacion;
    }

    public String getMonedaLiquidacion() {
        return monedaLiquidacion;
    }

    public void setMonedaLiquidacion(String monedaLiquidacion) {
        this.monedaLiquidacion = monedaLiquidacion;
    }

    public Character getAlcanceTransaccion() {
        return alcanceTransaccion;
    }

    public void setAlcanceTransaccion(Character alcanceTransaccion) {
        this.alcanceTransaccion = alcanceTransaccion;
    }

    public String getNumeroLote() {
        return numeroLote;
    }

    public void setNumeroLote(String numeroLote) {
        this.numeroLote = numeroLote;
    }

    public String getModoTransaccion() {
        return modoTransaccion;
    }

    public void setModoTransaccion(String modoTransaccion) {
        this.modoTransaccion = modoTransaccion;
    }

    public String getNombreEstablecimiento() {
        return nombreEstablecimiento;
    }

    public void setNombreEstablecimiento(String nombreEstablecimiento) {
        this.nombreEstablecimiento = nombreEstablecimiento;
    }

    public String getCodigoMTI() {
        return codigoMTI;
    }

    public void setCodigoMTI(String codigoMTI) {
        this.codigoMTI = codigoMTI;
    }

    public String getCodigoProceso() {
        return codigoProceso;
    }

    public void setCodigoProceso(String codigoProceso) {
        this.codigoProceso = codigoProceso;
    }

    public String getCodigoAutorizacion() {
        return codigoAutorizacion;
    }

    public void setCodigoAutorizacion(String codigoAutorizacion) {
        this.codigoAutorizacion = codigoAutorizacion;
    }

    public Character getCodigoOperacion() {
        return codigoOperacion;
    }

    public void setCodigoOperacion(Character codigoOperacion) {
        this.codigoOperacion = codigoOperacion;
    }

    public String getIdTerminal() {
        return idTerminal;
    }

    public void setIdTerminal(String idTerminal) {
        this.idTerminal = idTerminal;
    }

    public String getPlanCuotas() {
        return planCuotas;
    }

    public void setPlanCuotas(String planCuotas) {
        this.planCuotas = planCuotas;
    }

    public String getNumeroCuota() {
        return numeroCuota;
    }

    public void setNumeroCuota(String numeroCuota) {
        this.numeroCuota = numeroCuota;
    }

    public Character getMontoAdicionalSigno() {
        return montoAdicionalSigno;
    }

    public void setMontoAdicionalSigno(Character montoAdicionalSigno) {
        this.montoAdicionalSigno = montoAdicionalSigno;
    }

    public String getMontoAdicionalTexto() {
        return montoAdicionalTexto;
    }

    public void setMontoAdicionalTexto(String montoAdicionalTexto) {
        this.montoAdicionalTexto = montoAdicionalTexto;
    }

    public BigDecimal getMontoAdicional() {
        return montoAdicional;
    }

    public void setMontoAdicional(BigDecimal montoAdicional) {
        this.montoAdicional = montoAdicional;
    }

    public Character getImporteEfectivoSigno() {
        return importeEfectivoSigno;
    }

    public void setImporteEfectivoSigno(Character importeEfectivoSigno) {
        this.importeEfectivoSigno = importeEfectivoSigno;
    }

    public String getImporteEfectivoTexto() {
        return importeEfectivoTexto;
    }

    public void setImporteEfectivoTexto(String importeEfectivoTexto) {
        this.importeEfectivoTexto = importeEfectivoTexto;
    }

    public BigDecimal getImporteEfectivo() {
        return importeEfectivo;
    }

    public void setImporteEfectivo(BigDecimal importeEfectivo) {
        this.importeEfectivo = importeEfectivo;
    }

    public String getCapacidadDispositivo() {
        return capacidadDispositivo;
    }

    public void setCapacidadDispositivo(String capacidadDispositivo) {
        this.capacidadDispositivo = capacidadDispositivo;
    }

    public String getCondicionIngreso() {
        return condicionIngreso;
    }

    public void setCondicionIngreso(String condicionIngreso) {
        this.condicionIngreso = condicionIngreso;
    }


    public long getCuentaTarjeta() {
        return cuentaTarjeta;
    }

    public void setCuentaTarjeta(long cuentaTarjeta) {
        this.cuentaTarjeta = cuentaTarjeta;
    }

    public String getCuentaTarjetaReducida() {
        return cuentaTarjetaReducida;
    }

    public void setCuentaTarjetaReducida(String cuentaTarjetaReducida) {
        this.cuentaTarjetaReducida = cuentaTarjetaReducida;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public long getIdArchivoRecepcion() {
        return idArchivoRecepcion;
    }

    public void setIdArchivoRecepcion(long idArchivoRecepcion) {
        this.idArchivoRecepcion = idArchivoRecepcion;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public IdMovimientoMensualEmisor getIdMovimientoMensualEmisor() {
        return idMovimientoMensualEmisor;
    }

    public void setIdMovimientoMensualEmisor(IdMovimientoMensualEmisor idMovimientoMensualEmisor) {
        this.idMovimientoMensualEmisor = idMovimientoMensualEmisor;
    }
}
