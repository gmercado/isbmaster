package com.bisa.bus.servicios.tc.sftp.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;
import com.bisa.bus.servicios.tc.sftp.dao.PagosTCDao;
import com.bisa.bus.servicios.tc.sftp.entities.IdPagosTC;
import com.bisa.bus.servicios.tc.sftp.entities.PagosTC;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by atenorio on 05/06/2017.
 */
@AuthorizeInstantiation({RolesBisa.FTP_TRANSFERENCIA})
@Menu(value = "Pagos de Tarjeta de Cr\u00E9dito", subMenu = SubMenu.FTP)
public class ListadoPagos extends Listado<PagosTC, IdPagosTC> {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ListadoPagos.class);

    public ListadoPagos() {
        super();
    }

    @Override
    protected ListadoPanel<PagosTC, IdPagosTC> newListadoPanel(String id) {
        return new ListadoPanel<PagosTC, IdPagosTC>(id) {
            @Override
            protected List<IColumn<PagosTC, String>> newColumns(ListadoDataProvider<PagosTC, IdPagosTC> dataProvider) {
                List<IColumn<PagosTC, String>> columns = new LinkedList<>();
                columns.add(new LinkPropertyColumn<PagosTC>(Model.of("Cuenta Atc"), "idPago.cuentaTarjeta", "idPago.cuentaTarjeta") {
                    @Override
                    protected void onClick(PagosTC object) {
                        PageParameters params  = new PageParameters();
                        params.add("cuenta", object.getIdPago().getCuentaTarjeta());
                        params.add("transaccion", object.getIdPago().getTransaccion());
                        setResponsePage(DetallePagoAtc.class, params);
                    }
                });

                columns.add(new PropertyColumn<>(Model.of("Transacci\u00F3n"), "idPago.transaccion", "idPago.transaccion"));

                columns.add(new PropertyColumn<>(Model.of("Moneda"), "tipoMoneda", "tipoMoneda"));

                columns.add(new PropertyColumn<>(Model.of("Importe"), "importe", "importe"));

                columns.add(new PropertyColumn<>(Model.of("Fecha de pago"), "fechaPago", "fechaPago"));

                columns.add(new PropertyColumn<>(Model.of("Cuenta de tarjeta"), "cuentaTarjeta", "cuentaTarjeta"));

                columns.add(new PropertyColumn<>(Model.of("Fecha env\u00EDo"), "fechaEnvio", "fechaEnvioFormato"));

                columns.add(new PropertyColumn<>(Model.of("Fecha recepci\u00F3n"), "fechaRecepcion", "fechaRecepcionFormato"));

                columns.add(new PropertyColumn<PagosTC, String>(Model.of("Estado"), "estado", "estadoDescripcion") {
                    @Override
                    public void populateItem(Item<ICellPopulator<PagosTC>> item, String componentId, IModel<PagosTC> rowModel) {
                        PagosTC pago = rowModel.getObject();
                        item.add(new Label(componentId, pago.getEstadoDescripcion()).add(new AttributeAppender("style", Model.of("text-align:center"))));
                    }
                });

                columns.add(new PropertyColumn<>(Model.of("Fecha Proceso"), "fechaCreacion", "fechaProcesoFormato"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<PagosTC, IdPagosTC>> getProviderClazz() {
                return PagosTCDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<PagosTC> newFiltroPanel(String id, IModel<PagosTC> model1, IModel<PagosTC> model2) {
                return new ListaPagosFiltroPanel(id, model1, model2);
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("fechaCreacion", false);
            }

            @Override
            protected PagosTC newObject2() {
                return new PagosTC();
            }

            @Override
            protected PagosTC newObject1() {
                return new PagosTC();
            }

            @Override
            protected boolean isVisibleData() {
                PagosTC pago1 = getModel1().getObject();
                PagosTC pago2 = getModel2().getObject();
                return !((pago1.getFechaCreacion() == null || pago2.getFechaCreacion() == null));
            }
        };
    }
}
