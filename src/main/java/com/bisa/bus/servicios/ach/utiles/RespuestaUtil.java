package com.bisa.bus.servicios.ach.utiles;

import com.bisa.bus.servicios.ach.entities.AchpMaestra;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.bisa.bus.servicios.ach.enums.EstadosTipo.ESTADO_PROCESADO;
import static com.bisa.bus.servicios.ach.enums.EstadosTipo.ESTADO_RECHAZADO;
import static com.bisa.bus.servicios.ach.enums.EstadosTipo.ESTADO_REVERTIDO;
import static com.bisa.bus.servicios.ach.enums.ParticipanteTipo.BISA;

/**
 * Created by gmercado on 20/10/2017.
 */
public class RespuestaUtil {

    public static Integer toInteger(String valor) {
        try {
            return Integer.parseInt(valor);
        } catch (NumberFormatException e) {
        } catch (Exception e) {
        }
        return 0;
    }

    public static BigDecimal toBigDecimal(String valor) {
        try {
            return new BigDecimal(valor);
        } catch (NumberFormatException e) {
        } catch (Exception e) {
        }
        return BigDecimal.ZERO;
    }

    public static BigInteger toBigInteger(String valor) {
        try {
            return new BigInteger(valor);
        } catch (NumberFormatException e) {
        } catch (Exception e) {
        }
        return BigInteger.ZERO;
    }

    public static String cortaString(String valor, int tamanio) {
        if (Strings.isNullOrEmpty(valor) || tamanio > valor.length()) {
            return Strings.nullToEmpty(valor);
        }
        return valor.substring(0, tamanio);
    }

    public static Long validarNroOrden(Long nroOrden) {
        String soloOrden = String.valueOf(nroOrden);
        if (String.valueOf(nroOrden).startsWith(BISA.getCodigo())) {
            soloOrden = soloOrden.substring(BISA.getCodigo().length(), soloOrden.length());
        }
        return Long.parseLong(soloOrden);
    }

    public static boolean ordenEnEstadoFinal(AchpMaestra orden) {
        if (ESTADO_PROCESADO.getCodigo().equals(orden.getAchestado()) || ESTADO_RECHAZADO.getCodigo().equals(orden.getAchestado())
                || ESTADO_REVERTIDO.getCodigo().equals(orden.getAchestado())) {
            return true;
        }
        return false;
    }

    public static boolean validaLongitud(int longEntrada, int longTabla) {
        if(longEntrada > longTabla){
            return true;
        }
        return false;
    }

    public static BigDecimal validaCuenta(String accountNumber) {
        if (validaString(accountNumber).isEmpty()) {
            return BigDecimal.ZERO;
        }
        accountNumber = accountNumber.replace("-", "");
        try {
            return new BigDecimal(accountNumber);
        } catch (Throwable x) {
            return BigDecimal.ZERO;
        }
    }

    public static String validaString(Object valor) {
        try {
            if(valor != null) {
                return String.valueOf(valor).trim();
            }
        } catch (NullPointerException ign) {}
        return "";
    }

    public static BigDecimal validaMontos(Object valor) {
        try {
            BigDecimal bd = toBigDecimal(String.valueOf(valor));
            return bd.setScale(2, BigDecimal.ROUND_HALF_DOWN);
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    public static Integer validaHora(String hora) {
        try{
            hora = Strings.nullToEmpty(hora).trim();
            SimpleDateFormat formateador = new SimpleDateFormat("HH:mm:ss");
            // Hace el parseo para controlar que sea el formato correcto de hora
            formateador.parse(hora);
            Integer horafo = Integer.parseInt(hora.replace(":",""));
            return horafo;
        }catch(Exception e){
            return 0;
        }
    }

    public static String descripcionCodigo(String valores, String codigo, int retornoSize) {
        if (Strings.isNullOrEmpty(valores) || Strings.isNullOrEmpty(codigo)) {
            return "Sin descripcion";
        }
        boolean retornar = false;
        Splitter splitter = Splitter.on("|").trimResults();
        for (String s : splitter.split(valores)) {
            if (retornar) {
                return cortaString(s, retornoSize);
            }
            if (s.equalsIgnoreCase(codigo.trim())) {
                retornar = true;
            }
        }
        return "Sin descripcion para ".concat(codigo);
    }
}