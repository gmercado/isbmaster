package com.bisa.bus.servicios.ach.dao;

import bus.database.dao.DaoImpl;
import com.bisa.bus.servicios.ach.api.AchpParticipanteService;
import com.bisa.bus.servicios.ach.entities.AchpParticipante;
import com.bisa.bus.servicios.ach.enums.RespuestaTipo;
import com.bisa.bus.servicios.ach.model.Response;
import com.bisa.bus.servicios.ach.obj.JsonRequestParticipanteBasic;
import com.bisa.bus.servicios.ach.obj.Respuesta;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.bisa.bus.servicios.ach.utiles.RespuestaUtil.*;

import bus.database.model.BasePrincipal;

/**
 * Created by gmercado on 26/10/2017.
 */
public class AchpParticipanteDao extends DaoImpl<AchpParticipante, Long> implements Serializable, AchpParticipanteService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AchpParticipanteDao.class);

    @Inject
    protected AchpParticipanteDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public Response insertarNuevoParticipante(JsonRequestParticipanteBasic body) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        if (validaLongitud(body.getId().toString().trim().length(), 5)) {
                            return new Respuesta(RespuestaTipo.PARTICIPANTE_LONGITUD_PARTICPANTE.getCodigo(),
                                    RespuestaTipo.PARTICIPANTE_LONGITUD_PARTICPANTE.getDescripcion());
                        }
                        if (validaLongitud(body.getFull_name().length(), 30)) {
                            return new Respuesta(RespuestaTipo.PARTICIPANTE_LONGITUD_NOMBRE_LARGO.getCodigo(),
                                    RespuestaTipo.PARTICIPANTE_LONGITUD_NOMBRE_LARGO.getDescripcion());
                        }
                        if (validaLongitud(body.getAbbrev_name().length(), 15)) {
                            return new Respuesta(RespuestaTipo.PARTICIPANTE_LONGITUD_NOMBRE_CORTO.getCodigo(),
                                    RespuestaTipo.PARTICIPANTE_LONGITUD_NOMBRE_CORTO.getDescripcion());
                        }
                        if (buscarParticipante(Integer.parseInt(body.getId())).size() > 0) {
                            return new Respuesta(RespuestaTipo.PARTICIPANTE_EXISTENTE.getCodigo(),
                                    RespuestaTipo.PARTICIPANTE_EXISTENTE.getDescripcion());
                        }
                        AchpParticipante participante = new AchpParticipante(body);
                        persist(participante);
                        return new Respuesta(RespuestaTipo.PARTICIPANTE_INSERTADO.getCodigo(),
                                RespuestaTipo.PARTICIPANTE_INSERTADO.getDescripcion());
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error al buscar los participante ", e);
        }
        return new Respuesta(RespuestaTipo.ERROR.getCodigo(),
                RespuestaTipo.ERROR.getDescripcion());
    }

    public Response editarParticipante(JsonRequestParticipanteBasic body) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        if (validaLongitud(body.getId().toString().trim().length(), 5)) {
                            return new Respuesta(RespuestaTipo.PARTICIPANTE_LONGITUD_PARTICPANTE.getCodigo(),
                                    RespuestaTipo.PARTICIPANTE_LONGITUD_PARTICPANTE.getDescripcion());
                        }
                        if (validaLongitud(body.getFull_name().length(), 30)) {
                            return new Respuesta(RespuestaTipo.PARTICIPANTE_LONGITUD_NOMBRE_LARGO.getCodigo(),
                                    RespuestaTipo.PARTICIPANTE_LONGITUD_NOMBRE_LARGO.getDescripcion());
                        }
                        if (validaLongitud(body.getAbbrev_name().length(), 15)) {
                            return new Respuesta(RespuestaTipo.PARTICIPANTE_LONGITUD_NOMBRE_CORTO.getCodigo(),
                                    RespuestaTipo.PARTICIPANTE_LONGITUD_NOMBRE_CORTO.getDescripcion());
                        }
                        if (buscarParticipante(Integer.parseInt(body.getId())).size() > 0) {
                            AchpParticipante participante = new AchpParticipante(body);
                            merge(participante);
                            return new Respuesta(RespuestaTipo.PARTICIPANTE_EDITADO.getCodigo(),
                                    RespuestaTipo.PARTICIPANTE_EDITADO.getDescripcion());
                        }
                        return new Respuesta(RespuestaTipo.PARTICIPANTE_NO_EXISTE.getCodigo(),
                                RespuestaTipo.PARTICIPANTE_NO_EXISTE.getDescripcion());
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error al buscar los participante ", e);
        }
        return new Respuesta(RespuestaTipo.ERROR.getCodigo(),
                RespuestaTipo.ERROR.getDescripcion());
    }


    public List<AchpParticipante> buscarParticipante(Integer bncban) {
        try {
            LOGGER.info("Buscando participante ... ");
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            String query = "SELECT a FROM AchpParticipante a " +
                    " WHERE a.bncban = :bncban ";
            Query q = entityManager.createQuery(query);
            q.setParameter("bncban", bncban);
            return q.getResultList();
        } catch (Exception e) {
            LOGGER.error("Error inesperado al buscar participante ", e);
        }
        LOGGER.info("No se encontro el participante con codigo {}", bncban);
        return new ArrayList<>();
    }
}
