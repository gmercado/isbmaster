package com.bisa.bus.servicios.ach.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by gmercado on 01/12/2017.
 * Tabla transaccion monitor de e-Bisa
 * Esta es utilizada para obtener celular y correo del cliente
 */
@Entity
@Table(name = "EBP01")
public class EbpTransaccion implements Serializable {

    @Id
    @Column(name = "E01ID")
    private BigInteger e01id;

    @Column(name = "E01NROCLI")
    private BigDecimal e01nrocli;

    @Column(name = "E01CODTRN")
    private String e01codtrn;

    @Column(name = "E01IMPORTE")
    private BigDecimal e01importe;

    @Column(name = "E01NUMCAJA")
    private BigDecimal e01numcaja;

    @Column(name = "E01NUMSEC")
    private BigDecimal e01numsec;

    @Column(name = "E01SENDCOR")
    private String e01sendcor;

    @Column(name = "E01SENDSMS")
    private String e01sendsms;

    @Column(name = "E01CORREO")
    private String e01correo;

    @Column(name = "E01CELULAR")
    private String e01celular;

    public EbpTransaccion() {
    }

    public BigInteger getE01id() {
        return e01id;
    }

    public void setE01id(BigInteger e01id) {
        this.e01id = e01id;
    }

    public BigDecimal getE01nrocli() {
        return e01nrocli;
    }

    public void setE01nrocli(BigDecimal e01nrocli) {
        this.e01nrocli = e01nrocli;
    }

    public String getE01codtrn() {
        return e01codtrn;
    }

    public void setE01codtrn(String e01codtrn) {
        this.e01codtrn = e01codtrn;
    }

    public BigDecimal getE01importe() {
        return e01importe;
    }

    public void setE01importe(BigDecimal e01importe) {
        this.e01importe = e01importe;
    }

    public BigDecimal getE01numcaja() {
        return e01numcaja;
    }

    public void setE01numcaja(BigDecimal e01numcaja) {
        this.e01numcaja = e01numcaja;
    }

    public BigDecimal getE01numsec() {
        return e01numsec;
    }

    public void setE01numsec(BigDecimal e01numsec) {
        this.e01numsec = e01numsec;
    }

    public String getE01sendcor() {
        return e01sendcor;
    }

    public void setE01sendcor(String e01sendcor) {
        this.e01sendcor = e01sendcor;
    }

    public String getE01sendsms() {
        return e01sendsms;
    }

    public void setE01sendsms(String e01sendsms) {
        this.e01sendsms = e01sendsms;
    }

    public String getE01correo() {
        return e01correo;
    }

    public void setE01correo(String e01correo) {
        this.e01correo = e01correo;
    }

    public String getE01celular() {
        return e01celular;
    }

    public void setE01celular(String e01celular) {
        this.e01celular = e01celular;
    }

    @Override
    public String toString() {
        return "EbpTransaccion{" +
                "e01id=" + e01id +
                ", e01nrocli=" + e01nrocli +
                ", e01codtrn='" + e01codtrn + '\'' +
                ", e01importe=" + e01importe +
                ", e01numcaja=" + e01numcaja +
                ", e01numsec=" + e01numsec +
                ", e01sendcor='" + e01sendcor + '\'' +
                ", e01sendsms='" + e01sendsms + '\'' +
                ", e01correo='" + e01correo + '\'' +
                ", e01celular='" + e01celular + '\'' +
                '}';
    }
}
