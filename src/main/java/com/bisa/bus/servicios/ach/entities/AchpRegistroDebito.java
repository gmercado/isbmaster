package com.bisa.bus.servicios.ach.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author Gary mercado
 * Yrag Knup 29/10/2015.
 */
@Entity
@Table(name = "ACHP20")
public class AchpRegistroDebito {

    @Column
    private BigDecimal A20ACHP12;
    @Column
    private BigDecimal A20NUMORD;
    @Column
    @Temporal(TemporalType.DATE)
    private Date A20FECHA;

    public AchpRegistroDebito() {
    }

    public AchpRegistroDebito(BigDecimal a20ACHP12, BigDecimal a20NUMORD, Date a20FECHA) {
        A20ACHP12 = a20ACHP12;
        A20NUMORD = a20NUMORD;
        A20FECHA = a20FECHA;
    }

    public BigDecimal getA20ACHP12() {
        return A20ACHP12;
    }

    public void setA20ACHP12(BigDecimal a20ACHP12) {
        A20ACHP12 = a20ACHP12;
    }

    public BigDecimal getA20NUMORD() {
        return A20NUMORD;
    }

    public void setA20NUMORD(BigDecimal a20NUMORD) {
        A20NUMORD = a20NUMORD;
    }

    public Date getA20FECHA() {
        return A20FECHA;
    }

    public void setA20FECHA(Date a20FECHA) {
        A20FECHA = a20FECHA;
    }

    @Override
    public String toString() {
        return "Achp20{" +
                "A20ACHP12=" + A20ACHP12 +
                ", A20NUMORD=" + A20NUMORD +
                ", A20FECHA=" + A20FECHA +
                '}';
    }
}
