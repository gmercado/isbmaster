package com.bisa.bus.servicios.ach.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.env.api.MedioAmbiente;
import com.bisa.bus.servicios.ach.api.AchpCiclosService;
import com.bisa.bus.servicios.ach.entities.AchpCiclos;
import com.bisa.bus.servicios.ach.entities.PKAchpCiclos;
import com.bisa.bus.servicios.ach.enums.RespuestaTipo;
import com.bisa.bus.servicios.ach.model.Response;
import com.bisa.bus.servicios.ach.obj.JsonRequestCicloBasic;
import com.bisa.bus.servicios.ach.obj.Respuesta;
import com.bisa.bus.servicios.ach.utiles.DateUtil;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.common.base.Strings;

import static bus.env.api.Variables.ACH_CICLO_UMBRAL;
import static bus.env.api.Variables.ACH_CICLO_UMBRAL_DEFAULT;
import static com.bisa.bus.servicios.ach.utiles.RespuestaUtil.*;

/**
 * Created by gmercado on 26/10/2017.
 */
public class AchpCiclosDao extends DaoImpl<AchpCiclos, Long> implements Serializable, AchpCiclosService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AchpCiclosDao.class);
    private MedioAmbiente medioAmbiente;

    @Inject
    protected AchpCiclosDao(@BasePrincipal EntityManagerFactory entityManagerFactory, MedioAmbiente medioAmbiente) {
        super(entityManagerFactory);
        this.medioAmbiente = medioAmbiente;
    }

    public Response insertarNuevoCiclo(JsonRequestCicloBasic body) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        // Si envia los parametros start_time o end_time con valores distinto null, es dia HABIL
                        // El horario valido de camaras es desde las 01:00:00 hasta 23:00:00, de manera que no habrá valor 00:00:00
                        if (!(Strings.isNullOrEmpty(body.getStart_time())) && !(Strings.isNullOrEmpty(body.getEnd_time()))) {
                            Integer horaInicio = validaHora(body.getStart_time());
                            Integer horaFin = validaHora(body.getEnd_time());
                            if (horaInicio == 0) {
                                return new Respuesta(RespuestaTipo.CICLO_FORMATO_HORAINI.getCodigo(),
                                        RespuestaTipo.CICLO_FORMATO_HORAINI.getDescripcion());
                            }
                            if (horaFin == 0) {
                                return new Respuesta(RespuestaTipo.CICLO_FORMATO_HORAFIN.getCodigo(),
                                        RespuestaTipo.CICLO_FORMATO_HORAFIN.getDescripcion());
                            }
                            if (horaInicio >= horaFin) {
                                return new Respuesta(RespuestaTipo.CICLO_HORAINI_MAYOR.getCodigo(),
                                        RespuestaTipo.CICLO_HORAINI_MAYOR.getDescripcion());
                            }
                            PKAchpCiclos pk = new PKAchpCiclos(body, true);
                            if (buscarCicloId(pk).size() > 0) {
                                return new Respuesta(RespuestaTipo.CICLO_EXISTENTE.getCodigo(),
                                        RespuestaTipo.CICLO_EXISTENTE.getDescripcion());
                            }
                            AchpCiclos ciclo = new AchpCiclos(body, true);
                            ciclo.setPk_achpCiclos(pk);
                            persist(ciclo);
                        }
                        // Si envia los parametros dayoff_start_time y dayoff_end_time con valores distinto a null, es dia NO HABIL
                        if (!(Strings.isNullOrEmpty(body.getDayoff_start_time())) && !(Strings.isNullOrEmpty(body.getDayoff_end_time()))) {
                            Integer horaInicio = validaHora(body.getDayoff_start_time());
                            Integer horaFin = validaHora(body.getDayoff_end_time());
                            if (horaInicio == 0) {
                                return new Respuesta(RespuestaTipo.CICLO_FORMATO_HORAINI.getCodigo(),
                                        RespuestaTipo.CICLO_FORMATO_HORAINI.getDescripcion());
                            }
                            if (horaFin == 0) {
                                return new Respuesta(RespuestaTipo.CICLO_FORMATO_HORAFIN.getCodigo(),
                                        RespuestaTipo.CICLO_FORMATO_HORAFIN.getDescripcion());
                            }
                            if (horaInicio >= horaFin) {
                                return new Respuesta(RespuestaTipo.CICLO_HORAINI_MAYOR.getCodigo(),
                                        RespuestaTipo.CICLO_HORAINI_MAYOR.getDescripcion());
                            }
                            PKAchpCiclos pk = new PKAchpCiclos(body, false);
                            if (buscarCicloId(pk).size() > 0) {
                                return new Respuesta(RespuestaTipo.CICLO_EXISTENTE.getCodigo(),
                                        RespuestaTipo.CICLO_EXISTENTE.getDescripcion());
                            }
                            AchpCiclos ciclo = new AchpCiclos(body, false);
                            ciclo.setPk_achpCiclos(pk);
                            persist(ciclo);
                        }
                        return new Respuesta(RespuestaTipo.CICLO_INSERTADO.getCodigo(),
                                RespuestaTipo.CICLO_INSERTADO.getDescripcion());
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error al crear nuevo Ciclo", e);
        }
        return new Respuesta(RespuestaTipo.ERROR.getCodigo(),
                RespuestaTipo.ERROR.getDescripcion());
    }

    public Response editarCiclo(JsonRequestCicloBasic body) {
        try {
            LOGGER.info(body.toString());
            int umbral = medioAmbiente.getValorIntDe(ACH_CICLO_UMBRAL, ACH_CICLO_UMBRAL_DEFAULT);
            return doWithTransaction(
                    (entityManager, t) -> {
                        AchpCiclos ciclo = null;
                        // Si envia los parametros start_time o end_time con valores distinto a cero o null, es dia HABIL
                        // El horario valido de camaras es desde las 01:00:00 hasta 23:00:00, de manera que no habrá valor 00:00:00
                        if (!Strings.isNullOrEmpty(body.getStart_time()) && !Strings.isNullOrEmpty(body.getEnd_time())) {
                            Integer horaInicio = validaHora(body.getStart_time());
                            Integer horaFin = validaHora(body.getEnd_time());
                            if (horaInicio == 0) {
                                return new Respuesta(RespuestaTipo.CICLO_FORMATO_HORAINI.getCodigo(),
                                        RespuestaTipo.CICLO_FORMATO_HORAINI.getDescripcion());
                            }
                            if (horaFin == 0) {
                                return new Respuesta(RespuestaTipo.CICLO_FORMATO_HORAFIN.getCodigo(),
                                        RespuestaTipo.CICLO_FORMATO_HORAFIN.getDescripcion());
                            }
                            if (horaInicio >= horaFin) {
                                return new Respuesta(RespuestaTipo.CICLO_HORAINI_MAYOR.getCodigo(),
                                        RespuestaTipo.CICLO_HORAINI_MAYOR.getDescripcion());
                            }
                            PKAchpCiclos pk = new PKAchpCiclos(body, true);
                            List<AchpCiclos> ciclos = buscarCicloId(pk);
                            if (ciclos.size() > 0) {
                                ciclo = ciclos.get(0);
                                ciclo.setAchhoraini(DateUtil.horaFormato235959(body.getStart_time()));
                                ciclo.setAchhorafin(DateUtil.horaFormato235959RestaMin(body.getEnd_time(), umbral));
                                ciclo.setAchesta(Boolean.parseBoolean(body.getEnabled()) ? "A" : "I");
                                ciclo.setAchusr("SISTEMA");
                                ciclo.setAchdev("ISBSVC");
                                ciclo.setAchfec(new Date());
                                ciclo.setAchipadd("172.20.100.109");
                                ciclo.setAchpgmna("ACHM03");
                                ciclo.setAchjobnu("158140");
                                merge(ciclo);
                            }
                        }
                        // Si envia los parametros dayoff_start_time y dayoff_end_time con valores distinto a null, es dia NO HABIL
                        if (!Strings.isNullOrEmpty(body.getDayoff_start_time()) && !Strings.isNullOrEmpty(body.getDayoff_end_time())) {
                            Integer horaInicio = validaHora(body.getDayoff_start_time());
                            Integer horaFin = validaHora(body.getDayoff_end_time());
                            if (horaInicio == 0) {
                                return new Respuesta(RespuestaTipo.CICLO_FORMATO_HORAINI.getCodigo(),
                                        RespuestaTipo.CICLO_FORMATO_HORAINI.getDescripcion());
                            }
                            if (horaFin == 0) {
                                return new Respuesta(RespuestaTipo.CICLO_FORMATO_HORAFIN.getCodigo(),
                                        RespuestaTipo.CICLO_FORMATO_HORAFIN.getDescripcion());
                            }
                            if (horaInicio >= horaFin) {
                                return new Respuesta(RespuestaTipo.CICLO_HORAINI_MAYOR.getCodigo(),
                                        RespuestaTipo.CICLO_HORAINI_MAYOR.getDescripcion());
                            }
                            PKAchpCiclos pk = new PKAchpCiclos(body, false);
                            List<AchpCiclos> ciclos = buscarCicloId(pk);
                            if (ciclos.size() > 0) {
                                ciclo = ciclos.get(0);
                                ciclo.setAchhoraini(DateUtil.horaFormato235959(body.getDayoff_start_time()));
                                ciclo.setAchhorafin(DateUtil.horaFormato235959RestaMin(body.getDayoff_end_time(), umbral));
                                ciclo.setAchesta(Boolean.parseBoolean(body.getEnabled()) ? "A" : "I");
                                ciclo.setAchusr("SISTEMA");
                                ciclo.setAchdev("ISBSVC");
                                ciclo.setAchfec(new Date());
                                ciclo.setAchipadd("172.20.100.109");
                                ciclo.setAchpgmna("ACHM03");
                                ciclo.setAchjobnu("158140");
                                merge(ciclo);
                            }
                        }
                        if (ciclo != null) {
                            return new Respuesta(RespuestaTipo.CICLO_EDITADO.getCodigo(),
                                    RespuestaTipo.CICLO_EDITADO.getDescripcion());
                        }
                        return new Respuesta(RespuestaTipo.CICLO_NO_EXISTE.getCodigo(),
                                RespuestaTipo.CICLO_NO_EXISTE.getDescripcion());
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error al modificar Ciclos ", e);
        }
        return new Respuesta(RespuestaTipo.ERROR.getCodigo(),
                RespuestaTipo.ERROR.getDescripcion());
    }

    public List<AchpCiclos> buscarCicloId(PKAchpCiclos id) {
        try {
            LOGGER.info("Buscando ciclo ... {}", id);
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            String query = "SELECT a FROM AchpCiclos a " +
                    " WHERE a.pk_achpCiclos.achbanco = :achbanco " +
                    " AND a.pk_achpCiclos.achtipo = :achtipo " +
                    " AND a.pk_achpCiclos.achcodcam = :achcodcam";
            Query q = entityManager.createQuery(query);
            q.setParameter("achbanco", id.getAchbanco());
            q.setParameter("achtipo", id.getAchtipo());
            q.setParameter("achcodcam", id.getAchcodcam());
            return q.getResultList();
        } catch (Exception e) {
            LOGGER.error("Error inesperado al buscar ciclo ", e);
        }
        LOGGER.info("No se encontro el ciclo con id {}", id);
        return new ArrayList<>();
    }
}