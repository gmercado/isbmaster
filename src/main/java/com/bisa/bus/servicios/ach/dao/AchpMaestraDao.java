package com.bisa.bus.servicios.ach.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.AmbienteActual;
import bus.database.model.BasePrincipal;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.monitor.rpg.ImposibleLlamarProgramaRpgException;
import com.bisa.bus.servicios.ach.api.AchpDomiciliacionService;
import com.bisa.bus.servicios.ach.api.AchpMaestraService;
import com.bisa.bus.servicios.ach.entities.AchpComplemento;
import com.bisa.bus.servicios.ach.entities.AchpMaestra;
import com.bisa.bus.servicios.ach.enums.ParticipanteTipo;
import com.bisa.bus.servicios.ach.enums.ProgramasTipo;
import com.bisa.bus.servicios.ach.enums.RespuestaTipo;
import com.bisa.bus.servicios.ach.model.Response;
import com.bisa.bus.servicios.ach.obj.*;
import com.bisa.bus.servicios.ach.rpg.CallProgramACH;
import com.bisa.bus.servicios.ach.utiles.DateUtil;
import com.bisa.bus.servicios.asfi.infocred.ui.ReSendMailWrapper;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ConnectionPoolException;
import com.ibm.as400.access.ProgramParameter;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.InvalidStateException;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.TimeoutException;

import static bus.env.api.Variables.*;
import static com.bisa.bus.servicios.ach.enums.CausalErrorTipo.RA08;
import static com.bisa.bus.servicios.ach.enums.EstadosTipo.*;
import static com.bisa.bus.servicios.ach.enums.ParticipanteTipo.BISA;
import static com.bisa.bus.servicios.ach.enums.ProgramasTipo.*;
import static com.bisa.bus.servicios.ach.enums.RespuestaTipo.*;
import static com.bisa.bus.servicios.ach.utiles.RespuestaUtil.*;

/**
 * Created by gmercado on 20/10/2017.
 * Yrag Knup
 * Dao ACHP01
 */
public class AchpMaestraDao extends DaoImpl<AchpMaestra, Long> implements Serializable, AchpMaestraService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AchpMaestraDao.class);

    private MedioAmbiente medioAmbiente;
    private AmbienteActual ambienteActual;
    private CallProgramACH callProgramACH;
    private final ReSendMailWrapper mailWrapper;

    private EbpTransaccionDao ebpTransaccionDao;
    private AchpDomiciliacionService achpDomiciliacionService;
    // variables staticas
    private static String TIPO_ORDEN = "200";
    private static String PROCEDIMIENTO = "10";

    @Inject
    protected AchpMaestraDao(@BasePrincipal EntityManagerFactory entityManagerFactory, MedioAmbiente medioAmbiente, AmbienteActual ambienteActual,
                             CallProgramACH callProgramACH, AchpDomiciliacionService achpDomiciliacionService, EbpTransaccionDao ebpTransaccionDao, ReSendMailWrapper mailWrapper) {
        super(entityManagerFactory);
        this.mailWrapper = mailWrapper;
        this.medioAmbiente = medioAmbiente;
        this.ambienteActual = ambienteActual;
        this.callProgramACH = callProgramACH;
        this.ebpTransaccionDao = ebpTransaccionDao;
        this.achpDomiciliacionService = achpDomiciliacionService;
    }

    /**
     * @return Listado de pendientes de procesar 1A,
     * buscar todas las ordenes en estado 1 y 8 de la fecha actual
     */
    public Map<String, List<AchpMaestra>> getPendientes1A(Integer idOrdenes, Integer limit) {
        LOGGER.info("Obteniendo ordenes pendientes de envio 1A limite: {}", limit);
        Map map = new HashMap<String, List<AchpMaestra>>();
        try {
            String camara = medioAmbiente.getValorDe(Variables.ACH_CAMARA_COMPENSACION, Variables.ACH_CAMARA_COMPENSACION_DEFAULT);
            map.put(camara, new ArrayList<>());
            List<AchpMaestra> lista = buscaPendientes1A(idOrdenes, limit);
            if (lista.size() > 0) {
                map.put(camara, lista);
                return map;
            }
            // restamos 5 al limite original, parche para el cliente que en ocaciones no despacha el lote completo
            int limiteMenosCinco = limit - 5;
            actualizarEstadoTransito(idOrdenes, limiteMenosCinco);
            map.put(camara, buscaPendientes1A(idOrdenes, limit));
            return map;

        } catch (Exception e) {
            LOGGER.error("Error al buscar los pendientes 1A ", e);
        }
        return map;
    }

    /**
     * Procesa las ordenes 4A para ser llevadas a un estado final
     * Aceptado o Rechazado
     * @param nroOrden
     * @return
     */
    public Respuesta procesar4A(Long nroOrden, JsonRequestBasic body) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        AchpMaestra orden;
                        try {
                            LOGGER.info("Codigo de error {}, enviado en mensaje 4A para la orden: {}",
                                    body.getTransfer().getError(), nroOrden);
                            // Antes se procesaba el psc307CL con la respuesta del 1B ahora en 4A
                            ProgramasTipo programa = PSC307CL;
                            if (ambienteActual.isAmbienteTemporal()) {
                                programa = PSC307CLT;
                            }
                            LOGGER.info("El programa que se llamara para el ambiente actual sera: {}", programa);
                            //El clienteACH puede enviarnos la orden con el 1009 por delante del nro de orden
                            Long soloOrden = validarNroOrden(nroOrden);
                            orden = getAchPorNroOrden(soloOrden);
                            if (ordenEnEstadoFinal(orden)) {
                                return new Respuesta(OK.getCodigo(), ORDEN_YA_PROCESADA.getDescripcion());
                            }
                            // Si el mensaje de error es diferente a OK (0000) se hara la reversa
                            String estado = validaString(body.getTransfer().getError());
                            if (ESTADO_SIN_ERROR.getCodigo().equals(estado)) {
                                orden.setAchestado(ESTADO_PROCESADO.getCodigo());
                            } else {
                                orden.setAchestado(ESTADO_RECHAZADO.getCodigo());
                            }
                            // Cargamos los valores para la descripcion de código
                            String descripciones = medioAmbiente.getValorDe(ACH_DESCRIPCION_CODIGOS, ACH_DESCRIPCION_CODIGOS_DEFAULT);
                            // Estos estados necesitan AS400 - ACHP01
                            //orden.setAchtipresp(ESTADO_PROCESADO.getCodigo());
                            orden.setAchnumach(body.getTransfer().getNum_orden_ach());
                            orden.setAchtitdes(body.getTransfer().getTitular_destinatario());
                            orden.setAchstdcom(ESTADO_ENVIADA_RECIBIDA.getCodigo());
                            orden.setAchtmeproc(DateUtil.horaSistema());
                            orden.setAchcodresp(cortaString(body.getTransfer().getError(),5));
                            orden.setAchdesresp(descripcionCodigo(descripciones, body.getTransfer().getError(), 50));
                            merge(orden);
                            if(!callProgramACH(programa, orden.getAchnumord().toString())) {
                                return new Respuesta(ERROR_LLAMADA_PROGRAMA.getCodigo(),
                                        ERROR_LLAMADA_PROGRAMA.getDescripcion());
                            }
                            // Se notifica por sms o correo al cliente una vez obtenida respuesta del Destinatario
                            ebpTransaccionDao.notificar(orden);
                        } catch (NoResultException e) {
                            LOGGER.error("No se encontro en base de datos la orden: {}, revisar urgente!", nroOrden);
                            return new Respuesta(SIN_RESULTADO.getCodigo(), SIN_RESULTADO.getDescripcion());
                        }
                        LOGGER.info("Proceso 4A concluido satisfactoriamente!");
                        return new Respuesta(RespuestaTipo.OK.getCodigo(), RespuestaTipo.OK.getDescripcion());
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error inesperado al procesar 4A con numero de orden: {} ", nroOrden, e);
        }
        return new Respuesta(RespuestaTipo.ERROR.getCodigo(), RespuestaTipo.ERROR.getDescripcion());
    }

    /**
     * @param nroOrden
     * @param body
     * @return
     */
    public Respuesta procesar5A(Long nroOrden, JsonRequestBasic body) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        AchpMaestra orden;
                        try {
                            LOGGER.info("Codigo de error {}, enviado en mensaje 5A para la orden: {}",
                                    body.getTransfer().getError(), nroOrden);
                            // Se busca la orden ya sea de entrada o de salida
                            Long soloOrden = validarNroOrden(nroOrden);
                            orden = getAchPorNroOrden(soloOrden);

                            ProgramasTipo programa = PSC307CL;
                            // Solo se valida el ambiente para el psc307, en temporal no cierra ninguna cámara de manera no hay 5A
                            if (ambienteActual.isAmbienteTemporal()) {
                                programa = PSC307CLT;
                            }
                            // Si es orden de entrada se debe revertir utilizando el PSC308
                            if (validaString(orden.getAchdestino()).equals(ParticipanteTipo.BISA.getCodigo())) {
                                programa = PSC308CL;
                            }
                            LOGGER.info("Proceso 5A validando orden: {}, con estado: {}", orden.getAchnumord(), orden.getAchestado());
                            if (ESTADO_REVERTIDO.getCodigo().equals(orden.getAchestado())) {
                                return new Respuesta(OK.getCodigo(), ORDEN_YA_PROCESADA.getDescripcion());
                            }
                            if (validaString(orden.getAchdestino()).equals(ParticipanteTipo.BISA.getCodigo())
                                    && ESTADO_RECHAZADO.getCodigo().equals(validaString(orden.getAchestado()))) {
                                return new Respuesta(OK.getCodigo(), ORDEN_YA_PROCESADA.getDescripcion());
                            }
                            // Cargamos los valores para la descripcion de codigo
                            String descripciones = medioAmbiente.getValorDe(ACH_DESCRIPCION_CODIGOS, ACH_DESCRIPCION_CODIGOS_DEFAULT);
                            orden.setAchnumach(body.getTransfer().getNum_orden_ach());
                            orden.setAchnumori(body.getTransfer().getNum_orden_originante());
                            orden.setAchestado(ESTADO_RECHAZADO.getCodigo());
                            if (validaString(orden.getAchdestino()).equals(ParticipanteTipo.BISA.getCodigo())) {
                                // códigos de error para ordenes de entrada
                                orden.setAcherr3b(cortaString(body.getTransfer().getError(), 4));
                                orden.setAcherr3bd(descripcionCodigo(descripciones, body.getTransfer().getError_mensaje(),240));
                            } else {
                                // codigos de error para ordenes de salida
                                orden.setAcherr1b(cortaString(body.getTransfer().getError(), 4));
                                orden.setAcherr1bd(descripcionCodigo(descripciones, body.getTransfer().getError_mensaje(), 240));
                            }
                            orden.setAchcodresp(cortaString(body.getTransfer().getError(),5));
                            orden.setAchdesresp(descripcionCodigo(descripciones, body.getTransfer().getError(), 50) );
                            orden.setAchtmeproc(DateUtil.horaSistema());
                            orden.setAchfecproc(DateUtil.fechaSistema());
                            merge(orden);
                            // Se llama al programa PSC307 y se envia como parametro el nroOden
                            if (!callProgramACH(programa, orden.getAchnumord().toString())) {
                                String mails = medioAmbiente.getValorDe(ACH_USUARIOS_A_NOTIFICAR, ACH_USUARIOS_A_NOTIFICAR_DEFAULT);
                                String[] arraysMail =  StringUtils.split(mails, ",;");
                                mailWrapper.sendMessage("Error de reversa",
                                        "La orden: " + body.getTransfer().getNum_orden_ach() + " no pudo ser revertida en proceso 5A verificar y proceder manualmente",
                                        Arrays.asList(arraysMail));
                                // Para casos de error se ingresa en estado 6 para poder revertir en un siguiente intento si corresponde
                                orden.setAchestado(ESTADO_RECHAZADO.getCodigo());
                                merge(orden);

                                return new Respuesta(RespuestaTipo.OK.getCodigo(),
                                        RespuestaTipo.ERROR_LLAMADA_PROGRAMA.getDescripcion()
                                                .concat(programa.name()).concat(" generar alerta!"));
                            }
                            LOGGER.info("Orden revertida en Proceso 5A correctamente: {} ", orden.getAchnumord());
                            return new Respuesta(RespuestaTipo.OK.getCodigo(),
                                    RespuestaTipo.OK.getDescripcion());
                        } catch (NoResultException e) {
                            LOGGER.error("No se encontro en base de datos la orden: {}, revisar urgente!", nroOrden);
                            return new Respuesta(SIN_RESULTADO.getCodigo(),
                                    SIN_RESULTADO.getDescripcion());
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error inesperado al procesar 5A ", e);
        }
        return new Respuesta(RespuestaTipo.ERROR.getCodigo(), RespuestaTipo.ERROR.getDescripcion());
    }

    public Response procesar2A(JsonRequest2ABasic body) {
        if (DateUtil.fechaSistema().compareTo(body.getTransfer2A().getFec_camara()) != 0) {
            return new Respuesta(RespuestaTipo.FECHA_CAMARA_INCORRECTA.getCodigo(),
                    RespuestaTipo.FECHA_CAMARA_INCORRECTA.getDescripcion());
        }
        // Se valida por nro de orden ACH, si ya esta procesada por el PSC302 se retorna su respuesta directamente,
        // El psc302 procesa todos los registros pendientes de la ACHP01 y no uno a uno por eso este control
        AchpMaestra achControl = getAchPorNroOrden(body.getTransfer2A().getNum_orden_ach());
        if (achControl != null && ordenEnEstadoFinal(achControl)) {
            LOGGER.info("La orden ach {} ya se encuentra en un estado final, se retorna respuesta ", body.getTransfer2A().getNum_orden_ach());
            return new Respuesta2A(RespuestaTipo.OK.getCodigo(), RespuestaTipo.OK.getDescripcion(),
                    new RespuestaTransfer2A(
                            achControl.getAchnumord(),
                            achControl.getAchcodresp(),
                            achControl.getAchsucdes(),
                            achControl.getAchtitdes()));
        }
        // obtiene numero de orden entrante para registro interno en la ACHP01
        Long numOrden = obtenerNroOrden();
        if (numOrden.compareTo(0L) == 0) {
            return new Respuesta(RespuestaTipo.DB_ERROR.getCodigo(), RespuestaTipo.DB_ERROR.getDescripcion());
        }
        OpenJPAEntityManager entityManager = null;
        OpenJPAEntityTransaction transaction = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            transaction = entityManager.getTransaction();
            transaction.begin();
            // Insertamos a la achp01 los registros enviados por TESABIZ, para ordenes de entrada
            AchpMaestra achpMaestra = new AchpMaestra(numOrden, body);
            achpMaestra.setAchsucbisa(obtenerSucursal(body.getTransfer2A().getCod_sucursal_originante()));
            // Debemos obtener la fecha de proceso de la tap00101 (obtenerFechaProcesoYYYYMMdd) y asignar esto a la orden
            achpMaestra.setAchtmeproc(DateUtil.horaSistema());
            achpMaestra.setAchfecproc(obtenerFechaProcesoYYYYMMdd());
            achpMaestra.setAchcodresp(ESTADO_SIN_ERROR.getCodigo());
            // Data complementaria de ACHP01
            AchpComplemento achpComplemento = new AchpComplemento(numOrden,
                    cortaString(achpMaestra.getAchcodser(), 50),
                    cortaString(achpMaestra.getAchcanal(), 10));
            // Control para ordenes de debito si es tipo_orden 200 y cod_procedimiento 10
            if (TIPO_ORDEN.equals(validaString(body.getTransfer2A().getTip_orden()))
                    && PROCEDIMIENTO.equals(validaString(body.getTransfer2A().getCod_procedimiento()))) {
                Respuesta2A respDom = achpDomiciliacionService.procesaOrdenDebito(achpMaestra);
                if (!RespuestaTipo.OK.getDescripcion().equals(respDom.getstatusDesc())) {
                    // Se persiste orden de entrada de domiciliacion
                    entityManager.persist(achpMaestra);
                    // Insertamos data complementaria al ACHP01 en la tabla ACHP22
                    entityManager.persist(achpComplemento);
                    transaction.commit();
                    return respDom;
                }
            }
            entityManager.persist(achpMaestra);
            // Insertamos data complementaria al ACHP01 en la tabla ACHP22
            entityManager.persist(achpComplemento);
            transaction.commit();
            // Programa AS400 para el proceso de ordenes de entrada
            ProgramasTipo programa = PSC303;
            if (ambienteActual.isAmbienteTemporal()) {
                programa = PSC303T;
            }
            LOGGER.info("Programa que se llamara para el ambiente actual {}", programa);
            // Se llama al PSC303 para tener el codigo de respuesta para envio el 3A a la ACCL
            if (!callProgramACH(programa, "0")) {
                // Para cualquier problema en AS400 con el llamado de programa retornar Error para el reintento del clienteACH
                LOGGER.error("ERROR en proceso de 2A, error al llamar al programa ************** {}", programa);
                String mails = medioAmbiente.getValorDe(ACH_USUARIOS_A_NOTIFICAR, ACH_USUARIOS_A_NOTIFICAR_DEFAULT);
                String[] arraysMail =  StringUtils.split(mails, ",;");
                mailWrapper.sendMessage("Error llamada a programa",
                        "La orden ach: " + body.getTransfer2A().getNum_orden_ach() + " tuvo un error en la llamada al programa " + programa.name(),
                        Arrays.asList(arraysMail));
                return new Respuesta2A(RespuestaTipo.ERROR_LLAMADA_PROGRAMA.getCodigo(), RespuestaTipo.ERROR_LLAMADA_PROGRAMA.getDescripcion(),
                        new RespuestaTransfer2A(
                                achpMaestra.getAchnumord(),
                                achpMaestra.getAchcodresp(),
                                achpMaestra.getAchsucdes(),
                                achpMaestra.getAchtitdes()));
            }
            LOGGER.info("============== Correcta llamada al programa de AS400 ==============");
            achpMaestra = getAchPorNroOrden(numOrden);
            // Si el programa PC302 retorna ERROR RA08 es causal de falla de Monitores
            // Fatal se debe notificar a los encargados y verificar si Banco Bisa no fue intervenido XD
            if (RA08.name().equals(validaString(achpMaestra.getAchcodresp()))) {
                String mails = medioAmbiente.getValorDe(ACH_USUARIOS_A_NOTIFICAR, ACH_USUARIOS_A_NOTIFICAR_DEFAULT);
                String[] arraysMail =  StringUtils.split(mails, ",;");
                mailWrapper.sendMessage("Error orden de entrada",
                        "La orden: " + body.getTransfer2A().getNum_orden_ach() + " verificar si su estado es valido y credito",
                        Arrays.asList(arraysMail));
            }
            // Si no existe error de persitencia se envia el OK al cliente para que envie el 3A
            return new Respuesta2A(RespuestaTipo.OK.getCodigo(), RespuestaTipo.OK.getDescripcion(),
                    new RespuestaTransfer2A(
                            achpMaestra.getAchnumord(),
                            achpMaestra.getAchcodresp(),
                            achpMaestra.getAchsucdes(),
                            achpMaestra.getAchtitdes()));
        } catch (Throwable e) {
            LOGGER.error("ERROR inesperado en proceso de 2A, revisar incidente! ************** ", e);
            if (transaction != null) {
                try {
                    transaction.rollback();
                } catch (InvalidStateException e1) {
                    LOGGER.info("Rollback programado no ha sido necesario: ", e1.getMessage());
                } catch (Exception e1) {
                    LOGGER.warn("Ha fallado el rollback del proceso 2A", e1);
                }
            }

            /** Se notifica error en caso que que existe un error inesperado **/
            String mails = medioAmbiente.getValorDe(ACH_USUARIOS_A_NOTIFICAR, ACH_USUARIOS_A_NOTIFICAR_DEFAULT);
            String[] arraysMail =  StringUtils.split(mails, ",;");
            mailWrapper.sendMessage("Error orden de entrada",
                    "La orden: " + body.getTransfer2A().getNum_orden_ach() + " presento un error de tipo " + e.getMessage(),
                    Arrays.asList(arraysMail));

            return new Respuesta2A(RespuestaTipo.ERROR_LLAMADA_PROGRAMA.getCodigo(), RespuestaTipo.ERROR_LLAMADA_PROGRAMA.getDescripcion(),
                    new RespuestaTransfer2A(numOrden, RA08.name(),"LPZ",
                            body.getTransfer2A().getTitular_destinatario()));
        } finally {
            try {
                if (entityManager != null) {
                    entityManager.close();
                }
            } catch (Exception e) {
                LOGGER.error("Error al cerrar la sesion", e);
            }
        }
    }

    /**
     * Proceso de 3B acuse de recibo de ACCL a banco bisa
     * @param nroOrden
     * @param body
     * @return
     */
    public Response procesar3B(Long nroOrden, JsonRequest3BBasic body) {
        LOGGER.info("Procesando 3B con el numero de orden {} ", nroOrden);
        try {
            // Actualizar acherr3b, acherr3bd, achstdcom por nro de orden para dejarlo en estados finales
            return doWithTransaction(
                    (entityManager, t) -> {
                        String query =
                                "UPDATE ACHP01 a SET a.ACHSTDCOM = ?, a.ACHERR3B = ?, a.ACHERR3BD = ? " +
                                        "WHERE a.ACHNUMORD = ?";
                        // asignamos el estado 0002, solo deberia existir este estado dando por
                        // concluido el envio 3A como satisfactorio del lado del clienteAch
                        Query q = entityManager.createNativeQuery(query);
                        q.setParameter(1, ESTADO_RECIBIDA_ENVIADA.getCodigo());
                        q.setParameter(2, cortaString(body.getTransfer3A().getError(), 4));
                        q.setParameter(3, cortaString(body.getTransfer3A().getError_mensaje(), 250));
                        q.setParameter(4, validarNroOrden(nroOrden));
                        q.executeUpdate();
                        return new Respuesta(RespuestaTipo.OK.getCodigo(), RespuestaTipo.OK.getDescripcion());
                    }
            );
        } catch (NoResultException e) {
            LOGGER.error("No se encontró la orden {} para el proceso de 3B, verificar si existe la orden en ACHP01 ", nroOrden, e);
        } catch (Exception e) {
            LOGGER.error("No se pudo procesar mensaje 3B, revisar incidente!", e);
        }
        return new Respuesta(RespuestaTipo.ERROR.getCodigo(), RespuestaTipo.ERROR.getDescripcion());
    }

    /**
     * @param achnumord
     * @return boolean
     * Se actualiza el estado segun la respuesta que nos envia el clienteACH
     * Si la respuesta es diferente a "0000" procedemos a revertir la orden
     */
    public boolean callProgramACH(ProgramasTipo programa, String achnumord) {
        AS400Text numeroOrden = new AS400Text(20);
        ProgramParameter[] parametros = new ProgramParameter[1];
        parametros[0] = new ProgramParameter(numeroOrden.toBytes(achnumord));
        try {
            return callProgramACH.ejecutar(programa.getLibreria(), programa.name(), parametros);
        } catch (ImposibleLlamarProgramaRpgException e) {
            LOGGER.error("No se pudo llamar al programa {} con la orden: {}, revisar urgente!", programa.name(), achnumord, e);
        } catch (ConnectionPoolException e) {
            LOGGER.error("Error al tratar de conectarse AS400 para llamar al programa {}, orden: {}, revisar urgente!", programa.name(), achnumord, e);
        } catch (TimeoutException e) {
            LOGGER.error("Tiempo de intento de conexion al AS400 expiro, orden: {}, revisar urgente!", achnumord, e);
        } catch (Throwable e) {
            LOGGER.error("Ocurrio un error inesperado al tratar de llamar al programa {}, orden: {} revisar urgente!", programa.name(), achnumord, e);
        }
        return false;
    }

    public List<AchpMaestra> buscaPendientes1A(Integer idOrdenes, Integer limit) {
        return doWithTransaction(
                (entityManager, t) -> {
                    String query = "SELECT a FROM AchpMaestra a " +
                            "WHERE a.achfeccam = :fecha " +
                            "AND a.achorigen = :participante " +
                            "AND a.achestado = :estado " +
                            "AND a.achiso8583 = :idOrdenes " +
                            "ORDER BY a.achtmecaja asc ";
                    Query q = entityManager.createQuery(query);
                    q.setParameter("fecha", DateUtil.fechaSistema());
                    q.setParameter("participante", BISA.getCodigo());
                    q.setParameter("estado", ESTADO_TRANSITO.getCodigo());
                    q.setParameter("idOrdenes", idOrdenes.toString());
                    q.setMaxResults(limit);
                    return q.getResultList();
                }
        );
    }

    /**
     * @CreateNativeQuey mejor perfomance para el update masivo
     * Todas las ordenes a enviar al clienteACH
     * pasan de estado 1 a estado 3 de transito
     * [ACHESTADO = 1 ACHSTDCOM = 1000] [ACHESTADO = 3 ACHSTDCOM = 2000]
     * dando por exito que se envio a la ACCL
     * restamos 5 al limite para que el cliente no deje pendientes de envio
     */
    public boolean actualizarEstadoTransito(Integer idOrdenes, Integer limite) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        String query =
                                "UPDATE ACHP01 a SET a.ACHESTADO = ?, a.ACHSTDCOM = ?, a.ACHISO8583 = ? " +
                                        "WHERE a.ACHNUMORD IN (" +
                                        "SELECT b.ACHNUMORD " +
                                        "FROM ACHP01 b " +
                                        "WHERE b.ACHFECCAM = ?" +
                                        " AND b.ACHORIGEN = ? " +
                                        " AND b.ACHESTADO IN ('1', '8') " +
                                        " ORDER BY b.ACHTMECAJA DESC " +
                                        " FETCH FIRST " + limite + " ROWS ONLY )";
                        Query q = entityManager.createNativeQuery(query);
                        q.setParameter(1, ESTADO_TRANSITO.getCodigo());
                        q.setParameter(2, ESTADO_ENVIADO_1A.getCodigo());
                        q.setParameter(3, idOrdenes.toString());
                        q.setParameter(4, DateUtil.fechaSistema());
                        q.setParameter(5, BISA.getCodigo());
                        q.executeUpdate();
                        return true;
                    });
        } catch (Exception e) {
            LOGGER.error("Error al tratar de cambiar de estado a ordenes de salida ", e);
        }
        return false;
    }
    public AchpMaestra getAchPorNroOrden(Long nroOrden) throws NoResultException {
        return getAchPorNroOrden(nroOrden, null);
    }
    public AchpMaestra getAchPorNroOrden(BigInteger nroAch) throws NoResultException {
        return getAchPorNroOrden(null, nroAch);
    }
    /**
     * @param nroOrden
     * @return Retorna orden ach de la ACHP01 con filtros [(achnumord o achnumach) y achfeccam]
     */
    public AchpMaestra getAchPorNroOrden(Long nroOrden, BigInteger nroAch) throws NoResultException {
        LOGGER.info("Buscando orden ach : {}", nroOrden != null ?  nroOrden : nroAch);
        try {
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<AchpMaestra> q = cb.createQuery(AchpMaestra.class);
            Root<AchpMaestra> p = q.from(AchpMaestra.class);
            LinkedList<Predicate> predicatesAnd = new LinkedList<>();
            if (nroOrden != null) {
                predicatesAnd.add(cb.equal(p.get("achnumord"), nroOrden));
            } else if (nroAch != null) {
                predicatesAnd.add(cb.equal(p.get("achnumach"), nroAch));
            }
            predicatesAnd.add(cb.equal(p.get("achfeccam"), DateUtil.fechaSistema()));
            q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
            TypedQuery<AchpMaestra> query = entityManager.createQuery(q);
            if (query.getResultList().size() > 0) {
                return query.getResultList().get(0);
            }
        } catch (NoResultException e) {
            LOGGER.error("No se encontro resultado con los parametros achnumord: {}, achfeccam: {} ", nroOrden, DateUtil.fechaSistema());
            throw e;
        } catch (Exception e) {
            LOGGER.error("Error inesperado la buscar orden por achnumord", e);
        }
        return null;
    }

    /**
     * Obtiene el secuencial para el numero de ordenes de entrada
     * Y asignamos a la izquierda 8888 para nuestro estandar
     * @return
     */
    public Long obtenerNroOrden() {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        String query = "SELECT nextval for ZBNKPRD01.ACHSEQ FROM sysibm.sysdummy1";
                        Query q = entityManager.createNativeQuery(query);
                        Long nroOrden = (Long) q.getSingleResult();
                        return Long.parseLong("8888".concat(nroOrden.toString()));
                    });
        } catch (NoResultException e) {
            LOGGER.error("No se pudo obtener el numero de orden ach para la orden de entrada, revisar!", e);
        } catch (Exception e) {
            LOGGER.error("Error inesperado no se pudo obtener el numero de orden, revisar!", e);
        }
        return Long.parseLong("0");
    }

    public Integer obtenerFechaProcesoYYYYMMdd() {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        String query = "SELECT dsdt FROM tap00101 WHERE dsbk = 1";
                        Query q = entityManager.createNativeQuery(query);
                        BigDecimal fechaProceso = (BigDecimal) q.getSingleResult();
                        Date fechaDate = DateUtil.fechaPattern(fechaProceso.intValue(), "yyyyD");
                        return Integer.parseInt(DateUtil.fechaPattern(fechaDate, "yyyyMMdd"));
                    });
        } catch (NoResultException e) {
            LOGGER.error("No se pudo obtener la fecha de proceso, revisar!", e);
        } catch (Exception e) {
            LOGGER.error("Error inesperado al obtener la fecha de proceso, revisar!", e);
        }
        return 0;
    }

    /**
     * @param sucursalDestino para respuesta 3A
     * @return Por defecto se retorna 101 - sucursal (LPZ)
     */
    public Long obtenerSucursal(String sucursalDestino) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        if (Strings.isNullOrEmpty(sucursalDestino)) {
                            return 101L;
                        }
                        String query = "SELECT CMSUCPRN FROM PSP899 WHERE CMCIUDAD IN " +
                                "(SELECT ACHCIUBIS FROM ACHP07 WHERE ACHSUCACH = ?)";
                        Query q = entityManager.createNativeQuery(query);
                        q.setParameter(1, sucursalDestino);
                        List list = q.getResultList();
                        if (!list.isEmpty()) {
                            BigDecimal value = (BigDecimal) list.get(0);
                            return value.longValue();
                        }
                        return 101L;
                    });
        } catch (NoResultException e) {
            LOGGER.error("No se pudo obtener la agencia por cuenta de cliente, revisar!");
        } catch (Exception e) {
            LOGGER.error("Error inesperado no se pudo obtener la agencia por cuenta de cliente, revisar!", e);
        }
        return 101L;
    }
}