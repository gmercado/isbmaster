package com.bisa.bus.servicios.ach.obj;

import com.bisa.bus.servicios.ach.model.RequestSaliente;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by gmercado on 20/10/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestBasic implements Serializable, RequestSaliente {

    private Integer attempt;
    private Transfer transfer;

    public JsonRequestBasic() {
    }

    public JsonRequestBasic(Integer attempt, Transfer transfer) {
        this.attempt = attempt;
        this.transfer = transfer;
    }

    public void setAttempt(Integer attempt) {
        this.attempt = attempt;
    }

    @Override
    public Integer getAttempt() {
        return attempt;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    @Override
    public Transfer getTransfer() {
        return transfer;
    }

    @Override
    public String toString() {
        return "JsonRequesBasic{" +
                "attempt=" + attempt +
                ", transfer=" + transfer +
                '}';
    }
}
