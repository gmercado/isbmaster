package com.bisa.bus.servicios.ach.obj;

import com.bisa.bus.servicios.ach.model.Response;

/**
 * Created by gmercado on 18/10/2017.
 */
public class Respuesta implements Response {

    private Integer status;

    private String statusDesc;

    public Respuesta() {
    }

    public Respuesta(Integer status, String statusDesc) {
        this.status = status;
        this.statusDesc = statusDesc;
    }

    @Override
    public Integer getStatus() {
        return status;
    }

    @Override
    public String getstatusDesc() {
        return statusDesc;
    }

    @Override
    public String toString() {
        return "Respuesta{" +
                "status=" + status +
                ", statusDesc='" + statusDesc + '\'' +
                '}';
    }
}
