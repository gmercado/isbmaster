package com.bisa.bus.servicios.ach.obj;

import com.bisa.bus.servicios.ach.model.RequestEntrante;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by gmercado on 26/10/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestParticipanteBasic implements Serializable, RequestEntrante {

    private Integer attempt;
    private String clearinghouse;
    private String id;
    private String abbrev_name;
    private String full_name;
    private Integer subpaty_id;
    private String enabled;

    @Override
    public Integer getAttempt() {
        return attempt;
    }

    public void setAttempt(Integer attempt) {
        this.attempt = attempt;
    }

    public String getClearinghouse() {
        return clearinghouse;
    }

    public void setClearinghouse(String clearinghouse) {
        this.clearinghouse = clearinghouse;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAbbrev_name() {
        return abbrev_name;
    }

    public void setAbbrev_name(String abbrev_name) {
        this.abbrev_name = abbrev_name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public Integer getSubpaty_id() {
        return subpaty_id;
    }

    public void setSubpaty_id(Integer subpaty_id) {
        this.subpaty_id = subpaty_id;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "JsonRequestParticipanteBasic{" +
                "attempt=" + attempt +
                ", clearinghouse='" + clearinghouse + '\'' +
                ", id='" + id + '\'' +
                ", abbrev_name='" + abbrev_name + '\'' +
                ", full_name='" + full_name + '\'' +
                ", subpaty_id=" + subpaty_id +
                ", enabled='" + enabled + '\'' +
                '}';
    }
}
