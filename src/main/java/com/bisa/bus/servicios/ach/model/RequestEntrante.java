package com.bisa.bus.servicios.ach.model;

/**
 * Created by gmercado on 23/10/2017.
 */
public interface RequestEntrante {

    Integer getAttempt();
    String toString();
}
