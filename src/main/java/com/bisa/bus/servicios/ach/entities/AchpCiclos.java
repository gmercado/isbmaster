package com.bisa.bus.servicios.ach.entities;

import com.bisa.bus.servicios.ach.obj.JsonRequestCicloBasic;
import com.bisa.bus.servicios.ach.utiles.DateUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by gmercado on 26/10/2017.
 */
@Entity
@Table(name = "ACHP03")
public class AchpCiclos implements Serializable {

    @EmbeddedId
    private PKAchpCiclos pk_achpCiclos;

    @Column(name = "ACHHORAINI")
    private Integer achhoraini;

    @Column(name = "ACHHORAFIN")
    private Integer achhorafin;

    @Column(name = "ACHESTA")
    private String achesta;

    @Column(name = "ACHUSR")
    private String achusr;

    @Column(name = "ACHDEV")
    private String achdev;

    @Column(name = "ACHFEC")
    @Temporal(TemporalType.DATE)
    private Date achfec;

    @Column(name = "ACHIPADD")
    private String achipadd;

    @Column(name = "ACHPGMNA")
    private String achpgmna;

    @Column(name = "ACHJOBNU")
    private String achjobnu;

    public AchpCiclos() {
    }

    public AchpCiclos(JsonRequestCicloBasic body, boolean esHabil) {
        this.achhoraini = esHabil ? DateUtil.horaFormato235959(body.getStart_time()) : DateUtil.horaFormato235959(body.getDayoff_start_time());
        this.achhorafin = esHabil ? DateUtil.horaFormato235959(body.getEnd_time()) : DateUtil.horaFormato235959(body.getDayoff_end_time());
        this.achesta = Boolean.parseBoolean(body.getEnabled()) ? "A" : "I";
        this.achusr = "SISTEMA";
        this.achdev = "ISBSVC";
        this.achfec = new Date();
        this.achipadd = "172.20.100.109";
        this.achpgmna = "ACHM03";
        this.achjobnu = "158140";
    }

    public PKAchpCiclos getPk_achpCiclos() {
        return pk_achpCiclos;
    }

    public void setPk_achpCiclos(PKAchpCiclos pk_achpCiclos) {
        this.pk_achpCiclos = pk_achpCiclos;
    }

    public Integer getAchhoraini() {
        return achhoraini;
    }

    public void setAchhoraini(Integer achhoraini) {
        this.achhoraini = achhoraini;
    }

    public Integer getAchhorafin() {
        return achhorafin;
    }

    public void setAchhorafin(Integer achhorafin) {
        this.achhorafin = achhorafin;
    }

    public String getAchesta() {
        return achesta;
    }

    public void setAchesta(String achesta) {
        this.achesta = achesta;
    }

    public String getAchusr() {
        return achusr;
    }

    public void setAchusr(String achusr) {
        this.achusr = achusr;
    }

    public String getAchdev() {
        return achdev;
    }

    public void setAchdev(String achdev) {
        this.achdev = achdev;
    }

    public Date getAchfec() {
        return achfec;
    }

    public void setAchfec(Date achfec) {
        this.achfec = achfec;
    }

    public String getAchipadd() {
        return achipadd;
    }

    public void setAchipadd(String achipadd) {
        this.achipadd = achipadd;
    }

    public String getAchpgmna() {
        return achpgmna;
    }

    public void setAchpgmna(String achpgmna) {
        this.achpgmna = achpgmna;
    }

    public String getAchjobnu() {
        return achjobnu;
    }

    public void setAchjobnu(String achjobnu) {
        this.achjobnu = achjobnu;
    }

    @Override
    public String toString() {
        return "AchpCiclos{" +
                "achpCiclosPK=" + pk_achpCiclos +
                ", achhoraini='" + achhoraini + '\'' +
                ", achhorafin='" + achhorafin + '\'' +
                ", achesta='" + achesta + '\'' +
                ", achusr='" + achusr + '\'' +
                ", achdev='" + achdev + '\'' +
                ", achfec='" + achfec + '\'' +
                ", achipadd='" + achipadd + '\'' +
                ", achpgmna='" + achpgmna + '\'' +
                ", achjobnu='" + achjobnu + '\'' +
                '}';
    }
}
