package com.bisa.bus.servicios.ach.utiles;

/**
 * Created by gmercado on 15/12/2017.
 */
public interface DatosACH {

    String TITULO = "BISA";
    String EMAIL = "EMAIL";
    String MONEDA = "MONEDA";
    String SERVICIO_SJ = "SJ";
    //
    String ORDEN = "ORDEN";
    String DESTINATARIO = "DESTINATARIO";
    String ESTADO = "ESTADO";
    String CELULAR = "CELULAR";
    String IMPORTE = "IMPORTE";
    String APLICACION_SMS = "ACH";
    String IMPORTESMS = "IMPORTESMS";

    String CUENTA_DESTINO = "CCAD";
}
