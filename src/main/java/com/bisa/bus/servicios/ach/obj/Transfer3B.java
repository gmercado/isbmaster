package com.bisa.bus.servicios.ach.obj;

import com.fasterxml.jackson.annotation.JsonRootName;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created by gmercado on 25/10/2017.
 */
@JsonRootName("transfer")
public class Transfer3B implements JsonSerial, Serializable {

    private String clearinghouse;
    private BigInteger num_orden_ach;
    private String num_orden_originante;
    private String num_orden_destinatario;
    private String error;
    private String error_mensaje;
    private String titular_destinatario;

    public Transfer3B() {
    }

    public String getClearinghouse() {
        return clearinghouse;
    }

    public void setClearinghouse(String clearinghouse) {
        this.clearinghouse = clearinghouse;
    }

    public BigInteger getNum_orden_ach() {
        return num_orden_ach;
    }

    public void setNum_orden_ach(BigInteger num_orden_ach) {
        this.num_orden_ach = num_orden_ach;
    }

    public String getNum_orden_originante() {
        return num_orden_originante;
    }

    public void setNum_orden_originante(String num_orden_originante) {
        this.num_orden_originante = num_orden_originante;
    }

    public String getNum_orden_destinatario() {
        return num_orden_destinatario;
    }

    public void setNum_orden_destinatario(String num_orden_destinatario) {
        this.num_orden_destinatario = num_orden_destinatario;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError_mensaje() {
        return error_mensaje;
    }

    public void setError_mensaje(String error_mensaje) {
        this.error_mensaje = error_mensaje;
    }

    public String getTitular_destinatario() {
        return titular_destinatario;
    }

    public void setTitular_destinatario(String titular_destinatario) {
        this.titular_destinatario = titular_destinatario;
    }

    @Override
    public String toString() {
        return "Transfer3B{" +
                "clearinghouse='" + clearinghouse + '\'' +
                ", num_orden_ach='" + num_orden_ach + '\'' +
                ", num_orden_originante='" + num_orden_originante + '\'' +
                ", num_orden_destinatario='" + num_orden_destinatario + '\'' +
                ", error='" + error + '\'' +
                ", error_mensaje='" + error_mensaje + '\'' +
                ", titular_destinatario='" + titular_destinatario + '\'' +
                '}';
    }
}
