package com.bisa.bus.servicios.ach.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.ach.api.AchpComplementoService;
import com.bisa.bus.servicios.ach.entities.AchpComplemento;
import com.google.inject.Inject;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 * Created by gmercado on 24/10/2017.
 */
public class AchpComplementoDao extends DaoImpl<AchpComplemento, Long> implements Serializable, AchpComplementoService {

    @Inject
    protected AchpComplementoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    public AchpComplemento guardar(AchpComplemento achpComplemento) {
        return super.persist(achpComplemento);
    }
}
