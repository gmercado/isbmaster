package com.bisa.bus.servicios.ach.obj;

import com.bisa.bus.servicios.ach.model.RequestEntrante;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by gmercado on 25/10/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequest3BBasic implements Serializable, RequestEntrante {

    private Integer attempt;
    @JsonProperty("transfer")
    private Transfer3B transfer;

    public JsonRequest3BBasic() {
    }

    public JsonRequest3BBasic(Integer attempt, Transfer3B transfer) {
        this.attempt = attempt;
        this.transfer = transfer;
    }

    @Override
    public Integer getAttempt() {
        return attempt;
    }

    public Transfer3B getTransfer3A() {
        return transfer;
    }

    @Override
    public String toString() {
        return "JsonRequest3BBasic{" +
                "attempt=" + attempt +
                ", transfer=" + transfer +
                '}';
    }
}
