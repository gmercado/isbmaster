package com.bisa.bus.servicios.ach.utiles;

import com.bisa.bus.servicios.ach.obj.JsonSerial;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

/**
 * Created by gmercado on 20/10/2017.
 * Yrag Knup
 */
public class JsonUtil {

    public static <G> String objectToJson(JsonSerial object) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(object);
        return jsonInString;
    }

    public static <G> G jsonToObject(String json, Class<G> clazz) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        G obj = mapper.readValue(json, clazz);
        return obj;
    }

    public static <G> String listToJson(List list) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(list);
        return jsonInString;
    }

}
