package com.bisa.bus.servicios.ach.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by gmercado on 24/10/2017.
 */
@Entity
@Table(name = "ACHP22")
public class AchpComplemento implements Serializable {

    @Id
    @Column(name = "A22NUMORD")
    private Long a22numord;

    @Column(name = "A22CODSER")
    private String a22codser;

    @Column(name = "A22CANAL")
    private String a22canal;

    public AchpComplemento() {
    }

    public AchpComplemento(Long a22numord, String a22codser, String a22canal) {
        this.a22numord = a22numord;
        this.a22codser = a22codser;
        this.a22canal = a22canal;
    }

    public Long getA22numord() {
        return a22numord;
    }

    public void setA22numord(Long a22numord) {
        this.a22numord = a22numord;
    }

    public String getA22codser() {
        return a22codser;
    }

    public void setA22codser(String a22codser) {
        this.a22codser = a22codser;
    }

    public String getA22canal() {
        return a22canal;
    }

    public void setA22canal(String a22canal) {
        this.a22canal = a22canal;
    }

    @Override
    public String toString() {
        return "AchpComplemento{" +
                "a22numord=" + a22numord +
                ", a22codser='" + a22codser + '\'' +
                ", a22canal='" + a22canal + '\'' +
                '}';
    }
}
