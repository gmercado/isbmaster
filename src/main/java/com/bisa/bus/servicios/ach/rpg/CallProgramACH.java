package com.bisa.bus.servicios.ach.rpg;

import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.as400.AS400Factory;
import bus.monitor.rpg.CallProgramRPGv2;
import bus.monitor.rpg.ImposibleLlamarProgramaRpgException;
import bus.monitor.rpg.ParametrosRPG;
import com.google.inject.Inject;
import com.ibm.as400.access.ConnectionPoolException;
import com.ibm.as400.access.ProgramParameter;
import com.ibm.as400.access.QSYSObjectPathName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by gmercado on 30/10/2017.
 */
public class CallProgramACH {

    private static final Logger LOGGER = LoggerFactory.getLogger(CallProgramACH.class);

    private final CallProgramRPGv2 callProgramRPGv2;
    private final AS400Factory as400Factory;

    @Inject
    public CallProgramACH(AS400Factory as400factory, CallProgramRPGv2 callProgramRPGv2) throws ConnectionPoolException, ImposibleLlamarProgramaRpgException {
        this.as400Factory = as400factory;
        this.callProgramRPGv2 = callProgramRPGv2;
    }

    public boolean ejecutar(String libreriaPrograma, String programa, ProgramParameter[] parametros) throws ImposibleLlamarProgramaRpgException,
            IOException, SistemaCerradoException, ImposibleLeerRespuestaException, ConnectionPoolException, TransaccionEfectivaException, TimeoutException {
        String rutaPrograma = QSYSObjectPathName.toPath(libreriaPrograma, programa, "PGM");
        ParametrosRPG parametrosRPG = new ParametrosRPG() {
            @Override
            public String getFullPathRPG() {
                return rutaPrograma;
            }

            @Override
            public ProgramParameter[] loadParameters() {
                return parametros;
            }
        };
        try {
            callProgramRPGv2.ejecutar(parametrosRPG);
            return true;
        } catch (TimeoutException e) {
            LOGGER.warn("Timeout al tratar de llamar al programa {}", programa);
        } catch (Throwable e) {
            LOGGER.warn("Error inesperado al llamara al programa {} ", programa, e);
        }

        return false;
    }
}
