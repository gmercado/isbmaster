package com.bisa.bus.servicios.ach.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by gmercado on 24/10/2017.
 */
@Entity
@Table(name = "ACHP07")
public class Sucursales implements Serializable {

    @Id
    @Column(name = "ACHCIUBIS")
    private Long achciubis;

    @Column(name = "ACHSUCACH")
    private String achsucach;

    @Column(name = "ACHSUCDESC")
    private Long achsucdesc;

    @Column(name = "ACHSUCBANK")
    private Long achsucbank;

    public Sucursales() {
    }

    public Long getAchciubis() {
        return achciubis;
    }

    public void setAchciubis(Long achciubis) {
        this.achciubis = achciubis;
    }

    public String getAchsucach() {
        return achsucach;
    }

    public void setAchsucach(String achsucach) {
        this.achsucach = achsucach;
    }

    public Long getAchsucdesc() {
        return achsucdesc;
    }

    public void setAchsucdesc(Long achsucdesc) {
        this.achsucdesc = achsucdesc;
    }

    public Long getAchsucbank() {
        return achsucbank;
    }

    public void setAchsucbank(Long achsucbank) {
        this.achsucbank = achsucbank;
    }

    @Override
    public String toString() {
        return "Sucursales{" +
                "achciubis=" + achciubis +
                ", achsucach='" + achsucach + '\'' +
                ", achsucdesc=" + achsucdesc +
                ", achsucbank=" + achsucbank +
                '}';
    }
}
