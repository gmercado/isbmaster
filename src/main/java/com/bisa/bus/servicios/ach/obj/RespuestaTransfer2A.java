package com.bisa.bus.servicios.ach.obj;

import com.fasterxml.jackson.annotation.JsonRootName;

import java.io.Serializable;

/**
 * Created by gmercado on 24/10/2017.
 */
@JsonRootName("transfer")
public class RespuestaTransfer2A implements JsonSerial, Serializable {

    public Long num_orden_destinatario;
    public String cod_respuesta;
    public String cod_sucursal_destinatario;
    public String titular_destinatario;

    public RespuestaTransfer2A() {
    }

    public RespuestaTransfer2A(Long num_orden_destinatario, String cod_respuesta, String cod_sucursal_destinatario, String titular_destinatario) {
        this.num_orden_destinatario = num_orden_destinatario;
        this.cod_respuesta = cod_respuesta;
        this.cod_sucursal_destinatario = cod_sucursal_destinatario;
        this.titular_destinatario = titular_destinatario;
    }

    public Long getNum_orden_destinatario() {
        return num_orden_destinatario;
    }

    public void setNum_orden_destinatario(Long num_orden_destinatario) {
        this.num_orden_destinatario = num_orden_destinatario;
    }

    public String getCod_respuesta() {
        return cod_respuesta;
    }

    public void setCod_respuesta(String cod_respuesta) {
        this.cod_respuesta = cod_respuesta;
    }

    public String getCod_sucursal_destinatario() {
        return cod_sucursal_destinatario;
    }

    public void setCod_sucursal_destinatario(String cod_sucursal_destinatario) {
        this.cod_sucursal_destinatario = cod_sucursal_destinatario;
    }

    public String getTitular_destinatario() {
        return titular_destinatario;
    }

    public void setTitular_destinatario(String titular_destinatario) {
        this.titular_destinatario = titular_destinatario;
    }

    @Override
    public String toString() {
        return "RespuestaTransfer2A{" +
                "num_orden_destinatario=" + num_orden_destinatario +
                ", cod_respuesta='" + cod_respuesta + '\'' +
                ", cod_sucursal_destinatario='" + cod_sucursal_destinatario + '\'' +
                ", titular_destinatario='" + titular_destinatario + '\'' +
                '}';
    }
}
