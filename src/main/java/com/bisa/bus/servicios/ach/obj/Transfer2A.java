package com.bisa.bus.servicios.ach.obj;

import com.fasterxml.jackson.annotation.JsonRootName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by gmercado on 23/10/2017.
 */
@JsonRootName("transfer")
public class Transfer2A implements JsonSerial, Serializable {

    private BigInteger num_orden_ach;
    private BigInteger num_orden_originante;
    private String cod_pais_originante;
    private String cod_sucursal_originante;
    private Integer cod_originante;
    private Integer cod_destinatario;
    private String cod_pais_destinatario;
    private String cod_moneda;
    private BigDecimal importe;
    private String tip_orden;
    private String cod_procedimiento;
    private String tip_cuenta_origen;
    private String tip_cuenta_destino;
    private String cod_camara;
    private Integer fec_camara;
    private String cuenta_origen;
    private String cuenta_destino;
    private String glosa;
    private String tipo_documento;
    private String ci_nit_originante;
    private String titular_originante;
    private String ci_nit_destinatario;
    private String titular_destinatario;
    private String cod_sub_originante;
    private String cod_sub_destinatario;
    private String origen_fondos;
    private String destino_fondos;
    private String cod_servicio;
    private String canal;

//    private BigDecimal importe;
//    private String origen_fondos;
//    private String titular_destinatario;
//    private String cod_sub_destinatario;
//    private String tipo_documento;
//    private String cod_destinatario;
//    private String cod_sucursal_originante;
//    private String ci_nit_originante;
//    private String destino_fondos;
//    private String titular_originante;
//    private String tip_cuenta_origen;
//    private String cod_moneda;
//    private String cuenta_destino;
//    private String ci_nit_destinatario;
//    private String cod_procedimiento;
//    private Integer cod_camara;
//    private Integer fec_camara;
//    private String glosa;
//    private String cod_servicio;
//    private String num_orden_originante;
//    private String cuenta_origen;
//    private String canal;
//    private BigInteger num_orden_ach;
//    private String tip_cuenta_destino;

    public Transfer2A() {
    }

    public BigInteger getNum_orden_ach() {
        return num_orden_ach;
    }

    public void setNum_orden_ach(BigInteger num_orden_ach) {
        this.num_orden_ach = num_orden_ach;
    }

    public BigInteger getNum_orden_originante() {
        return num_orden_originante;
    }

    public void setNum_orden_originante(BigInteger num_orden_originante) {
        this.num_orden_originante = num_orden_originante;
    }

    public String getCod_pais_originante() {
        return cod_pais_originante;
    }

    public void setCod_pais_originante(String cod_pais_originante) {
        this.cod_pais_originante = cod_pais_originante;
    }

    public String getCod_sucursal_originante() {
        return cod_sucursal_originante;
    }

    public void setCod_sucursal_originante(String cod_sucursal_originante) {
        this.cod_sucursal_originante = cod_sucursal_originante;
    }

    public Integer getCod_originante() {
        return cod_originante;
    }

    public void setCod_originante(Integer cod_originante) {
        this.cod_originante = cod_originante;
    }

    public Integer getCod_destinatario() {
        return cod_destinatario;
    }

    public void setCod_destinatario(Integer cod_destinatario) {
        this.cod_destinatario = cod_destinatario;
    }

    public String getCod_pais_destinatario() {
        return cod_pais_destinatario;
    }

    public void setCod_pais_destinatario(String cod_pais_destinatario) {
        this.cod_pais_destinatario = cod_pais_destinatario;
    }

    public String getCod_moneda() {
        return cod_moneda;
    }

    public void setCod_moneda(String cod_moneda) {
        this.cod_moneda = cod_moneda;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getTip_orden() {
        return tip_orden;
    }

    public void setTip_orden(String tip_orden) {
        this.tip_orden = tip_orden;
    }

    public String getCod_procedimiento() {
        return cod_procedimiento;
    }

    public void setCod_procedimiento(String cod_procedimiento) {
        this.cod_procedimiento = cod_procedimiento;
    }

    public String getTip_cuenta_origen() {
        return tip_cuenta_origen;
    }

    public void setTip_cuenta_origen(String tip_cuenta_origen) {
        this.tip_cuenta_origen = tip_cuenta_origen;
    }

    public String getTip_cuenta_destino() {
        return tip_cuenta_destino;
    }

    public void setTip_cuenta_destino(String tip_cuenta_destino) {
        this.tip_cuenta_destino = tip_cuenta_destino;
    }

    public String getCod_camara() {
        return cod_camara;
    }

    public void setCod_camara(String cod_camara) {
        this.cod_camara = cod_camara;
    }

    public Integer getFec_camara() {
        return fec_camara;
    }

    public void setFec_camara(Integer fec_camara) {
        this.fec_camara = fec_camara;
    }

    public String getCuenta_origen() {
        return cuenta_origen;
    }

    public void setCuenta_origen(String cuenta_origen) {
        this.cuenta_origen = cuenta_origen;
    }

    public String getCuenta_destino() {
        return cuenta_destino;
    }

    public void setCuenta_destino(String cuenta_destino) {
        this.cuenta_destino = cuenta_destino;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getCi_nit_originante() {
        return ci_nit_originante;
    }

    public void setCi_nit_originante(String ci_nit_originante) {
        this.ci_nit_originante = ci_nit_originante;
    }

    public String getTitular_originante() {
        return titular_originante;
    }

    public void setTitular_originante(String titular_originante) {
        this.titular_originante = titular_originante;
    }

    public String getCi_nit_destinatario() {
        return ci_nit_destinatario;
    }

    public void setCi_nit_destinatario(String ci_nit_destinatario) {
        this.ci_nit_destinatario = ci_nit_destinatario;
    }

    public String getTitular_destinatario() {
        return titular_destinatario;
    }

    public void setTitular_destinatario(String titular_destinatario) {
        this.titular_destinatario = titular_destinatario;
    }

    public String getCod_sub_originante() {
        return cod_sub_originante;
    }

    public void setCod_sub_originante(String cod_sub_originante) {
        this.cod_sub_originante = cod_sub_originante;
    }

    public String getCod_sub_destinatario() {
        return cod_sub_destinatario;
    }

    public void setCod_sub_destinatario(String cod_sub_destinatario) {
        this.cod_sub_destinatario = cod_sub_destinatario;
    }

    public String getOrigen_fondos() {
        return origen_fondos;
    }

    public void setOrigen_fondos(String origen_fondos) {
        this.origen_fondos = origen_fondos;
    }

    public String getDestino_fondos() {
        return destino_fondos;
    }

    public void setDestino_fondos(String destino_fondos) {
        this.destino_fondos = destino_fondos;
    }

    public String getCod_servicio() {
        return cod_servicio;
    }

    public void setCod_servicio(String cod_servicio) {
        this.cod_servicio = cod_servicio;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    @Override
    public String toString() {
        return "Transfer2A{" +
                "num_orden_ach=" + num_orden_ach +
                ", num_orden_originante=" + num_orden_originante +
                ", cod_pais_originante='" + cod_pais_originante + '\'' +
                ", cod_sucursal_originante='" + cod_sucursal_originante + '\'' +
                ", cod_originante=" + cod_originante +
                ", cod_destinatario=" + cod_destinatario +
                ", cod_pais_destinatario='" + cod_pais_destinatario + '\'' +
                ", cod_moneda='" + cod_moneda + '\'' +
                ", importe=" + importe +
                ", tip_orden='" + tip_orden + '\'' +
                ", cod_procedimiento='" + cod_procedimiento + '\'' +
                ", tip_cuenta_origen='" + tip_cuenta_origen + '\'' +
                ", tip_cuenta_destino='" + tip_cuenta_destino + '\'' +
                ", cod_camara='" + cod_camara + '\'' +
                ", fec_camara=" + fec_camara +
                ", cuenta_origen='" + cuenta_origen + '\'' +
                ", cuenta_destino='" + cuenta_destino + '\'' +
                ", glosa='" + glosa + '\'' +
                ", tipo_documento='" + tipo_documento + '\'' +
                ", ci_nit_originante='" + ci_nit_originante + '\'' +
                ", titular_originante='" + titular_originante + '\'' +
                ", ci_nit_destinatario='" + ci_nit_destinatario + '\'' +
                ", titular_destinatario='" + titular_destinatario + '\'' +
                ", cod_sub_originante='" + cod_sub_originante + '\'' +
                ", cod_sub_destinatario='" + cod_sub_destinatario + '\'' +
                ", origen_fondos='" + origen_fondos + '\'' +
                ", destino_fondos='" + destino_fondos + '\'' +
                ", cod_servicio='" + cod_servicio + '\'' +
                ", canal='" + canal + '\'' +
                '}';
    }
}
