package com.bisa.bus.servicios.ach.dao;

import bus.consumoweb.yellowpepper.dao.YellowPepperServicioDao;
import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.bus.servicios.ach.api.EbpTransaccionService;
import com.bisa.bus.servicios.ach.entities.AchpMaestra;
import com.bisa.bus.servicios.ach.entities.EbpTransaccion;
import com.bisa.bus.servicios.ach.enums.EstadosTipo;
import com.bisa.bus.servicios.ach.utiles.DatosACH;
import com.bisa.bus.servicios.ach.utiles.MonedaUtil;
import com.bisa.bus.servicios.asfi.infocred.ui.ReSendMailWrapper;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by gmercado on 12/12/2017.
 */
public class EbpTransaccionDao extends DaoImpl<EbpTransaccion, BigInteger> implements Serializable, EbpTransaccionService {

    private MedioAmbiente medioAmbiente;
    private ReSendMailWrapper mailWrapper;
    private YellowPepperServicioDao yellowPepperServicioDao;

    private static final Logger LOGGER = LoggerFactory.getLogger(EbpTransaccionDao.class);

    @Inject
    protected EbpTransaccionDao(@BasePrincipal EntityManagerFactory entityManagerFactory, MedioAmbiente medioAmbiente,
                                ReSendMailWrapper mailWrapper, YellowPepperServicioDao yellowPepperServicioDao) {
        super(entityManagerFactory);
        this.mailWrapper = mailWrapper;
        this.medioAmbiente = medioAmbiente;
        this.yellowPepperServicioDao = yellowPepperServicioDao;
    }

    /**
     * Este metodo busca el celular y correo del cliente si este tiene
     * registrado estos datos en la TBL_Parameters
     * @param orden
     */
    public void notificar(AchpMaestra orden) {
        try {
            LOGGER.info("Buscando informacion para la notificacion de la orden {}", orden.getAchnumord());
            // Buscar celular y correo en Ebp01 por caja y secuencia
            EbpTransaccion datos = buscarPorCajaYSecuencia(BigDecimal.valueOf(orden.getAchnumcaja()), BigDecimal.valueOf(orden.getAchnumseq()));
            if(datos == null) {
                LOGGER.info("No existen datos del cliente para porder notificar {}", datos);
                return;
            }
            LOGGER.info("Datos encontrados para la notificacion {}", datos);
            boolean mandarCorreo = "S".equalsIgnoreCase(datos.getE01sendcor());
            boolean mandarSMS = "S".equalsIgnoreCase(datos.getE01sendsms());
            Map<String, Object> map = new HashMap<>();

            map.put(DatosACH.ORDEN, orden.getAchnumord());
            map.put(DatosACH.ESTADO, estadoPorCodigo(orden.getAchestado()));
            map.put(DatosACH.DESTINATARIO, orden.getAchtitdes());
            map.put(DatosACH.MONEDA, MonedaUtil.getSigla(orden.getAchmoneda()));
            map.put(DatosACH.EMAIL, mandarCorreo ? datos.getE01correo() : "");
            map.put(DatosACH.CELULAR, mandarSMS ? datos.getE01celular() : "");
            map.put(DatosACH.IMPORTE, orden.getAchimporte());
            map.put(DatosACH.IMPORTESMS, medioAmbiente.getValorBigDecimalDe(Variables.ACH_IMPORTE_ENVIO_SMS, Variables.ACH_IMPORTE_ENVIO_SMS_DEFAULT));
            AchpNotificarClienteDao notifica = new AchpNotificarClienteDao(yellowPepperServicioDao, mailWrapper, map);
            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.execute(notifica);
            executor.shutdown();
        } catch (Exception e) {
            LOGGER.error("Error al notificar la transferencia a otros bancos", e);
        }
    }

    /**
     * Busca en tabla EBP01 solo si el cliente registro sus datos e hizo el check para
     * el envio de sms o correo al momento de aceptar la transaccion
     * @param caja
     * @param secuencia
     * @return
     * @throws NoResultException
     */
    public EbpTransaccion buscarPorCajaYSecuencia(BigDecimal caja, BigDecimal secuencia) throws NoResultException {
        LOGGER.info("Buscando celular y e-mail por caja: {} y secuencia: {}", caja, secuencia);
        try {
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<EbpTransaccion> q = cb.createQuery(EbpTransaccion.class);
            Root<EbpTransaccion> p = q.from(EbpTransaccion.class);
            LinkedList<Predicate> predicatesAnd = new LinkedList<>();
            predicatesAnd.add(cb.equal(p.get("e01numcaja"), caja));
            predicatesAnd.add(cb.equal(p.get("e01numsec"), secuencia));
            q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
            TypedQuery<EbpTransaccion> query = entityManager.createQuery(q);
            if (query.getResultList().isEmpty()) {
                return null;
            }
            return query.getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.error("No se encontro resultados de celular o correo en la EBP01", e);
            throw e;
        } catch (Exception e) {
            LOGGER.error("Error inesperado al buscar celular y correo en la EBP01", e);
        }
        return null;
    }

    public String estadoPorCodigo(String codigo) {
        for (EstadosTipo estadosTipo : EstadosTipo.values()) {
            if (estadosTipo.getCodigo().equals(codigo.trim()))
                return estadosTipo.getDescripcion();
        }
        return codigo;
    }
}
