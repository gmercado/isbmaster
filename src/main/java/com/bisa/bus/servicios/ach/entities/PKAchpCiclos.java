package com.bisa.bus.servicios.ach.entities;

import com.bisa.bus.servicios.ach.obj.JsonRequestCicloBasic;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Created by gmercado on 26/10/2017.
 * Yrag Knup
 */
@Embeddable
public class PKAchpCiclos {

    @Column(name = "ACHBANCO")
    private Long achbanco;

    @Column(name = "ACHTIPO")
    private String achtipo;

    @Column(name = "ACHCODCAM")
    private String achcodcam;

    public PKAchpCiclos() {
    }

    public PKAchpCiclos(JsonRequestCicloBasic body, boolean esHabil) {
        this.achbanco = "ACCL".equals(body.getClearinghouse()) ? 1L : 2L;
        this.achtipo = esHabil ? "H" : "N";
        this.achcodcam = body.getId().toString();
    }

    public PKAchpCiclos(Long achbanco, String achtipo, String achcodcam) {
        this.achbanco = achbanco;
        this.achtipo = achtipo;
        this.achcodcam = achcodcam;
    }

    public Long getAchbanco() {
        return achbanco;
    }

    public void setAchbanco(Long achbanco) {
        this.achbanco = achbanco;
    }

    public String getAchtipo() {
        return achtipo;
    }

    public void setAchtipo(String achtipo) {
        this.achtipo = achtipo;
    }

    public String getAchcodcam() {
        return achcodcam;
    }

    public void setAchcodcam(String achcodcam) {
        this.achcodcam = achcodcam;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PKAchpCiclos that = (PKAchpCiclos) o;

        if (!achbanco.equals(that.achbanco)) return false;
        if (!achtipo.equals(that.achtipo)) return false;
        return achcodcam.equals(that.achcodcam);
    }

    @Override
    public int hashCode() {
        int result = achbanco.hashCode();
        result = 31 * result + achtipo.hashCode();
        result = 31 * result + achcodcam.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "AchpCiclosPK{" +
                "achbanco=" + achbanco +
                ", achtipo='" + achtipo + '\'' +
                ", achcodcam='" + achcodcam + '\'' +
                '}';
    }
}
