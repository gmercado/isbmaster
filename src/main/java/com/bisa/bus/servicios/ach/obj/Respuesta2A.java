package com.bisa.bus.servicios.ach.obj;

import com.bisa.bus.servicios.ach.model.Response;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by gmercado on 24/10/2017.
 */
public class Respuesta2A implements Response {

    private Integer status;

    private String statusDesc;

    @JsonProperty("transfer")
    private RespuestaTransfer2A respuestaTransfer2A;

    public Respuesta2A() {
    }

    public Respuesta2A(Integer status, String statusDesc, RespuestaTransfer2A respuestaTransfer2A) {
        this.status = status;
        this.statusDesc = statusDesc;
        this.respuestaTransfer2A = respuestaTransfer2A;
    }

    @Override
    public Integer getStatus() {
        return status;
    }

    @Override
    public String getstatusDesc() {
        return statusDesc;
    }

    @Override
    public String toString() {
        return "Respuesta2A{" +
                "status=" + status +
                ", statusDesc='" + statusDesc + '\'' +
                ", respuestaTransfer2A=" + respuestaTransfer2A +
                '}';
    }
}
