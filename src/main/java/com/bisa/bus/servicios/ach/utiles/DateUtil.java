package com.bisa.bus.servicios.ach.utiles;

import com.bisa.bus.servicios.ach.dao.AchpCiclosDao;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by gmercado on 19/10/2017.
 */
public class DateUtil {

    public static String FECHA_RFC3339 = "yyyy-MM-dd'T'HH:mm:ssXXX";

    public static int horaSistema() {
        try {
            Calendar fecha = Calendar.getInstance();
            return Integer.parseInt(String.format("%1$tH%1$tM%1$tS", fecha));
        } catch (Exception e) {
            return 0;
        }
    }

    public static Integer horaFormato235959(String hora) {
        try {
            hora = Strings.nullToEmpty(hora).trim();
            return Integer.parseInt(hora.replaceAll(":", ""));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static Integer horaFormato235959RestaMin(String hora, int resta_minutos) {
        try {
            hora = Strings.nullToEmpty(hora).trim();
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            String [] HORA_ARRAY = hora.split(":");
            int HORA = Integer.parseInt(HORA_ARRAY[0]);
            int MINUTO = Integer.parseInt(HORA_ARRAY[1]);
            c.set(Calendar.HOUR_OF_DAY, HORA);
            c.set(Calendar.MINUTE, MINUTO);
            c.set(Calendar.SECOND, 0);
            c.add(Calendar.MINUTE, - resta_minutos);
            SimpleDateFormat sdf = new SimpleDateFormat("kkmmss");
            return Integer.parseInt(sdf.format(c.getTime()));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static Integer fechaSistema() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String fecha = format.format(new Date());

        return Integer.parseInt(fecha);
    }

    public static Integer fechaSistemaJulian() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyDDD");
        String fecha = format.format(new Date());

        return Integer.parseInt(fecha);
    }

    public static String fechaPattern(String patter) {
        return fechaPattern(new Date(), patter);
    }

    public static String fechaPattern(Date fecha, String patter) {
        SimpleDateFormat format = new SimpleDateFormat(patter);
        return format.format(fecha);
    }

    public static Date fechaPattern(Integer fecha, String patter) {
        SimpleDateFormat format = new SimpleDateFormat(patter);
        try {
            return format.parse(fecha.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
