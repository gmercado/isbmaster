package com.bisa.bus.servicios.ach.enums;

/**
 * @Author Gary Mercado
 * Yrag Knup 23/10/2015.
 */

public enum EstadoFUDTipo {
    A("Activo"),
    C("Cancelado/Anulado"),
    I("Inactivo");

    private String descripcion;

    private EstadoFUDTipo(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
