package com.bisa.bus.servicios.ach.model;

import com.bisa.bus.servicios.ach.obj.Transfer;

/**
 * Created by gmercado on 18/10/2017.
 * Yrag knup
 */
public interface RequestSaliente {
    Integer getAttempt();
    Transfer getTransfer();

    String toString();
}
