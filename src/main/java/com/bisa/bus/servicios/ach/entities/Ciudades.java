package com.bisa.bus.servicios.ach.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by gmercado on 24/10/2017.
 */
@Entity
@Table(name = "PSP899")
public class Ciudades implements Serializable {

    @Id
    @Column(name = "CMCIUDAD")
    private Long cmciudad;

    @Column(name = "CMDESCCIU")
    private String cmdescciu;

    @Column(name = "CMOFCTRN")
    private Long cmofctrn;

    @Column(name = "CMLOTCAN")
    private Long cmlotcan;

    @Column(name = "CMSUCPRN")
    private Long cmsucprn;

    @Column(name = "CMCAMPO1")
    private Long cmcampo1;

    @Column(name = "CMFLAG1")
    private String cmflag1;

    @Column(name = "CMFLAG2")
    private String cmflag2;

    @Column(name = "CMFLAG3")
    private String cmflag3;

    public Ciudades() {
    }

    public Long getCmciudad() {
        return cmciudad;
    }

    public void setCmciudad(Long cmciudad) {
        this.cmciudad = cmciudad;
    }

    public String getCmdescciu() {
        return cmdescciu;
    }

    public void setCmdescciu(String cmdescciu) {
        this.cmdescciu = cmdescciu;
    }

    public Long getCmofctrn() {
        return cmofctrn;
    }

    public void setCmofctrn(Long cmofctrn) {
        this.cmofctrn = cmofctrn;
    }

    public Long getCmlotcan() {
        return cmlotcan;
    }

    public void setCmlotcan(Long cmlotcan) {
        this.cmlotcan = cmlotcan;
    }

    public Long getCmsucprn() {
        return cmsucprn;
    }

    public void setCmsucprn(Long cmsucprn) {
        this.cmsucprn = cmsucprn;
    }

    public Long getCmcampo1() {
        return cmcampo1;
    }

    public void setCmcampo1(Long cmcampo1) {
        this.cmcampo1 = cmcampo1;
    }

    public String getCmflag1() {
        return cmflag1;
    }

    public void setCmflag1(String cmflag1) {
        this.cmflag1 = cmflag1;
    }

    public String getCmflag2() {
        return cmflag2;
    }

    public void setCmflag2(String cmflag2) {
        this.cmflag2 = cmflag2;
    }

    public String getCmflag3() {
        return cmflag3;
    }

    public void setCmflag3(String cmflag3) {
        this.cmflag3 = cmflag3;
    }

    @Override
    public String toString() {
        return "Ciudades{" +
                "cmciudad=" + cmciudad +
                ", cmdescciu='" + cmdescciu + '\'' +
                ", cmofctrn=" + cmofctrn +
                ", cmlotcan='" + cmlotcan + '\'' +
                ", cmsucprn='" + cmsucprn + '\'' +
                ", cmcampo1='" + cmcampo1 + '\'' +
                ", cmflag1='" + cmflag1 + '\'' +
                ", cmflag2='" + cmflag2 + '\'' +
                ", cmflag3='" + cmflag3 + '\'' +
                '}';
    }
}
