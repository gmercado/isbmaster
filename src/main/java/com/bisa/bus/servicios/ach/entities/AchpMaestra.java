package com.bisa.bus.servicios.ach.entities;

import com.bisa.bus.servicios.ach.obj.JsonRequest2ABasic;
import com.bisa.bus.servicios.ach.utiles.DateUtil;
import com.bisa.bus.servicios.ach.utiles.MonedaUtil;
import com.bisa.bus.servicios.ach.utiles.RespuestaUtil;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;

import static com.bisa.bus.servicios.ach.enums.EstadosTipo.ESTADO_RECIBIDA_2A;

/**
 * Created by gmercado on 20/10/2017.
 * Tabla principal de ACHs ACHP01
 */
@Entity
@Table(name = "ACHP01")
public class AchpMaestra implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ACHNUMORD")
    private Long achnumord;

    @Column(name = "ACHNUMORI")
    private String achnumori;

    @Column(name = "ACHNUMACH")
    private BigInteger achnumach;

    @Column(name = "ACHCODALFA")
    private String achcodalfa;

    @Column(name = "ACHORIGEN")
    private String achorigen;

    @Column(name = "ACHPASORI")
    private String achpasori;

    @Column(name = "ACHSUCORI")
    private String achsucori;

    @Column(name = "ACHDESTINO")
    private String achdestino;

    @Column(name = "ACHPASDES")
    private String achpasdes;

    @Column(name = "ACHSUCDES")
    private String achsucdes;

    @Column(name = "ACHCOMPEN")
    private String achcompen;

    @Column(name = "ACHMNDDES")
    private String achmnddes;

    @Column(name = "ACHMNDORI")
    private String achmndori;

    @Column(name = "ACHMONEDA")
    private String achmoneda;

    @Column(name = "ACHIMPORTE")
    private BigDecimal achimporte;

    @Column(name = "ACHTIPDOC")
    private String achtipdoc;

    @Column(name = "ACHTIPORD")
    private String achtipord;

    @Column(name = "ACHCODPRD")
    private String achcodprd;

    @Column(name = "ACHTIPCNTO")
    private String achtipcnto;

    @Column(name = "ACHTIPCNTD")
    private String achtipcntd;

    @Column(name = "ACHCODCAM")
    private String achcodcam;

    @Column(name = "ACHFECCAM")
    private Integer achfeccam;

    @Column(name = "ACHFECENV")
    private Integer achfecenv;

    @Column(name = "ACHCNTORIG")
    private String achcntorig;

    @Column(name = "ACHCNTDEST")
    private String achcntdest;

    @Column(name = "ACHNITORI")
    private String achnitori;

    @Column(name = "ACHTITORI")
    private String achtitori;

    @Column(name = "ACHNITDES")
    private String achnitdes;

    @Column(name = "ACHTITDES")
    private String achtitdes;

    @Column(name = "ACHNUMANG")
    private String achnumang;

    @Column(name = "ACHISO8583")
    private String achiso8583;

    @Column(name = "ACHGLOSA")
    private String achglosa;

    @Column(name = "ACHSTDCOM")
    private String achstdcom;

    @Column(name = "ACHTIPMEN")
    private String achtipmen;

    @Column(name = "ACHERR1B")
    private String acherr1b;

    @Column(name = "ACHERR1BD")
    private String acherr1bd;

    @Column(name = "ACHERR3B")
    private String acherr3b;

    @Column(name = "ACHERR3BD")
    private String acherr3bd;

    @Column(name = "ACHCODRESP")
    private String achcodresp;

    @Column(name = "ACHDESRESP")
    private String achdesresp;

    @Column(name = "ACHTIPRESP")
    private String achtipresp;

    @Column(name = "ACHESTADO")
    private String achestado;

//    @Column(name = "ACHESTREV")
//    private String achestrev;

    @Column(name = "ACHTMECAJA")
    private Integer achtmecaja;

    @Column(name = "ACHTMEAPRO")
    private Integer achtmeapro;

    @Column(name = "ACHTMEPROC")
    private Integer achtmeproc;

    @Column(name = "ACHTMEREV")
    private Integer achtmerev;

    @Column(name = "ACHFECPROC")
    private Integer achfecproc;

//    @Column(name = "ACHCODTRN")
//    private String achcodtrn;
//
    @Column(name = "ACHNUMCAJA")
    private Integer achnumcaja;

    @Column(name = "ACHNUMSEQ")
    private Integer achnumseq;
//
//    @Column(name = "ACHNUMSEQP")
//    private Integer achnumseqp;
//
//    @Column(name = "ACHNUMSEQR")
//    private Integer achnumseqr;
//
//    @Column(name = "ACHUSRAPR")
//    private String achusrapr;

    @Column(name = "ACHSUCBISA")
    private Long achsucbisa;

//    @Column(name = "ACHERRBIS1")
//    private String acherrbis1;
//
//    @Column(name = "ACHERRBIS2")
//    private String acherrbis2;
//
//    @Column(name = "ACHERRBIS3")
//    private String acherrbis3;
//
//    @Column(name = "ACHERRBIS4")
//    private String acherrbis4;
//
//    @Column(name = "ACHERRBIS5")
//    private String acherrbis5;
//
    @Column(name = "ACHNROLOT")
    private Long achnrolot = 0L;
//
//    @Column(name = "ACHNROPAG")
//    private Integer achnropag;
//
//    @Column(name = "ACHNROCEL")
//    private String achnrocel;
//
//    @Column(name = "ACHCODAPL")
//    private String achcodapl;
//
//    @Column(name = "ACHAPLID")
//    private long achaplid;
//
//    @Column(name = "ACHREINT")
//    private Integer achreInteger;
//
//    @Column(name = "ACHCUST")
//    private String achcust;

    @Column(name = "ACHORIGFON")
    private String achorigfon;

    @Column(name = "ACHDESTFON")
    private String achdestfon;

    @Column(name = "ACHSUBORIG")
    private String achsuborig;

    @Column(name = "ACHSUBDEST")
    private String achsubdest;

//    @Column(name = "ACHTITRESP")
//    private String achtitresp;

    @Transient
    private String achcodser;

    @Transient
    private String achcanal;

    public AchpMaestra() {
    }

    public AchpMaestra(Long numOrden, JsonRequest2ABasic body) {
        this.achnumord = numOrden;
        this.achnumach = body.getTransfer2A().getNum_orden_ach();
        this.achorigen = String.valueOf(body.getTransfer2A().getCod_originante());
        this.achpasori = body.getTransfer2A().getCod_pais_originante();
        this.achsucori = body.getTransfer2A().getCod_sucursal_originante();
        this.achdestino = String.valueOf(body.getTransfer2A().getCod_destinatario());
        this.achpasdes = body.getTransfer2A().getCod_pais_destinatario();
        this.achtipord = body.getTransfer2A().getTip_orden();
        this.achtipcnto = body.getTransfer2A().getTip_cuenta_origen();
        this.achtipcntd = body.getTransfer2A().getTip_cuenta_destino();
        this.achfecenv = DateUtil.fechaSistema();
        this.achimporte = RespuestaUtil.validaMontos(body.getTransfer2A().getImporte());
        this.achorigfon = body.getTransfer2A().getOrigen_fondos();
        this.achtitdes = RespuestaUtil.cortaString(body.getTransfer2A().getTitular_destinatario(), 50);
        this.achsubdest = RespuestaUtil.validaString(body.getTransfer2A().getCod_sub_destinatario());
        this.achsuborig = RespuestaUtil.validaString(body.getTransfer2A().getCod_sub_originante());
        this.achtipdoc = body.getTransfer2A().getTipo_documento();
        this.achnitori = body.getTransfer2A().getCi_nit_originante();
        this.achdestfon = body.getTransfer2A().getDestino_fondos();
        this.achtitori = RespuestaUtil.cortaString(body.getTransfer2A().getTitular_originante(), 50);
        this.achcntorig = body.getTransfer2A().getCuenta_origen();
        this.achmoneda = MonedaUtil.getId(body.getTransfer2A().getCod_moneda());
        this.achcntdest = RespuestaUtil.validaCuenta(body.getTransfer2A().getCuenta_destino()).toString();
        this.achnitdes = body.getTransfer2A().getCi_nit_destinatario();
        this.achcodprd = body.getTransfer2A().getCod_procedimiento();
        this.achcodcam = String.valueOf(body.getTransfer2A().getCod_camara());
        this.achfeccam = body.getTransfer2A().getFec_camara();
        this.achglosa = RespuestaUtil.cortaString(body.getTransfer2A().getGlosa(), 100);
        this.achnumori = String.valueOf(body.getTransfer2A().getNum_orden_originante());
        this.achcodser = body.getTransfer2A().getCod_servicio();
        this.achcanal = body.getTransfer2A().getCanal();
        // Debe ser insertada los valores de inicio
        this.achstdcom = ESTADO_RECIBIDA_2A.getCodigo();
        this.achtipmen = "ENTRANTE";
        this.achestado = "0"; // valor incial de proceso
        this.achtmerev = 0;
        this.achtmeapro = 0;
        this.achtmecaja = DateUtil.horaSistema();
        this.achcodcam = RespuestaUtil.cortaString(body.getTransfer2A().getCod_camara(),1);
        // Data que no puede ser nula
        this.achnrolot = 0L;
        this.acherr1b = "";
        this.acherr1bd = "";
        this.acherr3b = "";
        this.acherr3bd = "";
        this.achcodresp = "";
        this.achdesresp = "";
        this.achtipresp = "";
        this.achcodalfa = "sin valor";
        this.achcompen = "1004"; // Banco Central
        // Ya no tenemos esta informacion en el 1A, Doc ACCL 2.1
        this.achsucdes = "LPZ";
        this.achsucbisa = 200L; // Por defecto sucursal LPZ
        this.achmnddes = "";
        this.achmndori = "-";
        this.achnumang = "-";
        this.achiso8583 = "";
        this.achnumcaja = 0;
        this.achnumseq = 0;
    }

    public Long getAchnumord() {
        return achnumord;
    }

    public void setAchnumord(Long achnumord) {
        this.achnumord = achnumord;
    }

    public String getAchnumori() {
        return achnumori;
    }

    public void setAchnumori(String achnumori) {
        this.achnumori = achnumori;
    }

    public BigInteger getAchnumach() {
        return achnumach;
    }

    public void setAchnumach(BigInteger achnumach) {
        this.achnumach = achnumach;
    }

    public String getAchcodalfa() {
        return achcodalfa;
    }

    public void setAchcodalfa(String achcodalfa) {
        this.achcodalfa = achcodalfa;
    }

    public String getAchorigen() {
        return achorigen;
    }

    public void setAchorigen(String achorigen) {
        this.achorigen = achorigen;
    }

    public String getAchpasori() {
        return achpasori;
    }

    public void setAchpasori(String achpasori) {
        this.achpasori = achpasori;
    }

    public String getAchsucori() {
        return achsucori;
    }

    public void setAchsucori(String achsucori) {
        this.achsucori = achsucori;
    }

    public String getAchdestino() {
        return achdestino;
    }

    public void setAchdestino(String achdestino) {
        this.achdestino = achdestino;
    }

    public String getAchpasdes() {
        return achpasdes;
    }

    public void setAchpasdes(String achpasdes) {
        this.achpasdes = achpasdes;
    }

    public String getAchsucdes() {
        return achsucdes;
    }

    public void setAchsucdes(String achsucdes) {
        this.achsucdes = achsucdes;
    }

    public String getAchcompen() {
        return achcompen;
    }

    public void setAchcompen(String achcompen) {
        this.achcompen = achcompen;
    }

    public String getAchmnddes() {
        return achmnddes;
    }

    public void setAchmnddes(String achmnddes) {
        this.achmnddes = achmnddes;
    }

    public String getAchmndori() {
        return achmndori;
    }

    public void setAchmndori(String achmndori) {
        this.achmndori = achmndori;
    }

    public String getAchmoneda() {
        return achmoneda;
    }

    public void setAchmoneda(String achmoneda) {
        this.achmoneda = achmoneda;
    }

    public BigDecimal getAchimporte() {
        return achimporte;
    }

    public void setAchimporte(BigDecimal achimporte) {
        this.achimporte = achimporte;
    }

    public String getAchtipdoc() {
        return achtipdoc;
    }

    public void setAchtipdoc(String achtipdoc) {
        this.achtipdoc = achtipdoc;
    }

    public String getAchtipord() {
        return achtipord;
    }

    public void setAchtipord(String achtipord) {
        this.achtipord = achtipord;
    }

    public String getAchcodprd() {
        return achcodprd;
    }

    public void setAchcodprd(String achcodprd) {
        this.achcodprd = achcodprd;
    }

    public String getAchtipcnto() {
        return achtipcnto;
    }

    public void setAchtipcnto(String achtipcnto) {
        this.achtipcnto = achtipcnto;
    }

    public String getAchtipcntd() {
        return achtipcntd;
    }

    public void setAchtipcntd(String achtipcntd) {
        this.achtipcntd = achtipcntd;
    }

    public String getAchcodcam() {
        return achcodcam;
    }

    public void setAchcodcam(String achcodcam) {
        this.achcodcam = achcodcam;
    }

    public Integer getAchfeccam() {
        return achfeccam;
    }

    public void setAchfeccam(Integer achfeccam) {
        this.achfeccam = achfeccam;
    }

    public Integer getAchfecenv() {
        return achfecenv;
    }

    public void setAchfecenv(Integer achfecenv) {
        this.achfecenv = achfecenv;
    }

    public String getAchcntorig() {
        return achcntorig;
    }

    public void setAchcntorig(String achcntorig) {
        this.achcntorig = achcntorig;
    }

    public String getAchcntdest() {
        return achcntdest;
    }

    public void setAchcntdest(String achcntdest) {
        this.achcntdest = achcntdest;
    }

    public String getAchnitori() {
        return achnitori;
    }

    public void setAchnitori(String achnitori) {
        this.achnitori = achnitori;
    }

    public String getAchtitori() {
        return achtitori;
    }

    public void setAchtitori(String achtitori) {
        this.achtitori = achtitori;
    }

    public String getAchnitdes() {
        return achnitdes;
    }

    public void setAchnitdes(String achnitdes) {
        this.achnitdes = achnitdes;
    }

    public String getAchtitdes() {
        return achtitdes;
    }

    public void setAchtitdes(String achtitdes) {
        this.achtitdes = achtitdes;
    }

    public String getAchnumang() {
        return achnumang;
    }

    public void setAchnumang(String achnumang) {
        this.achnumang = achnumang;
    }

    public String getAchiso8583() {
        return achiso8583;
    }

    public void setAchiso8583(String achiso8583) {
        this.achiso8583 = achiso8583;
    }

    public String getAchglosa() {
        return achglosa;
    }

    public void setAchglosa(String achglosa) {
        this.achglosa = achglosa;
    }

    public String getAchstdcom() {
        return achstdcom;
    }

    public void setAchstdcom(String achstdcom) {
        this.achstdcom = achstdcom;
    }

    public String getAchtipmen() {
        return achtipmen;
    }

    public void setAchtipmen(String achtipmen) {
        this.achtipmen = achtipmen;
    }

    public String getAcherr1b() {
        return acherr1b;
    }

    public void setAcherr1b(String acherr1b) {
        this.acherr1b = acherr1b;
    }

    public String getAcherr1bd() {
        return acherr1bd;
    }

    public void setAcherr1bd(String acherr1bd) {
        this.acherr1bd = acherr1bd;
    }

    public String getAcherr3b() {
        return acherr3b;
    }

    public void setAcherr3b(String acherr3b) {
        this.acherr3b = acherr3b;
    }

    public String getAcherr3bd() {
        return acherr3bd;
    }

    public void setAcherr3bd(String acherr3bd) {
        this.acherr3bd = acherr3bd;
    }

    public String getAchcodresp() {
        return achcodresp;
    }

    public void setAchcodresp(String achcodresp) {
        this.achcodresp = achcodresp;
    }

    public String getAchdesresp() {
        return achdesresp;
    }

    public void setAchdesresp(String achdesresp) {
        this.achdesresp = achdesresp;
    }

    public String getAchtipresp() {
        return achtipresp;
    }

    public void setAchtipresp(String achtipresp) {
        this.achtipresp = achtipresp;
    }

    public String getAchestado() {
        return achestado;
    }

    public void setAchestado(String achestado) {
        this.achestado = achestado;
    }

//    public String getAchestrev() {
//        return achestrev;
//    }
//
//    public void setAchestrev(String achestrev) {
//        this.achestrev = achestrev;
//    }

    public Integer getAchtmecaja() {
        return achtmecaja;
    }

    public void setAchtmecaja(Integer achtmecaja) {
        this.achtmecaja = achtmecaja;
    }

    public Integer getAchtmeapro() {
        return achtmeapro;
    }

    public void setAchtmeapro(Integer achtmeapro) {
        this.achtmeapro = achtmeapro;
    }

    public Integer getAchtmeproc() {
        return achtmeproc;
    }

    public void setAchtmeproc(Integer achtmeproc) {
        this.achtmeproc = achtmeproc;
    }

    public Integer getAchtmerev() {
        return achtmerev;
    }

    public void setAchtmerev(Integer achtmerev) {
        this.achtmerev = achtmerev;
    }

    public Integer getAchfecproc() {
        return achfecproc;
    }

    public void setAchfecproc(Integer achfecproc) {
        this.achfecproc = achfecproc;
    }

    public Integer getAchnumcaja() {
        return achnumcaja;
    }

    public void setAchnumcaja(Integer achnumcaja) {
        this.achnumcaja = achnumcaja;
    }

    public Integer getAchnumseq() {
        return achnumseq;
    }

    public void setAchnumseq(Integer achnumseq) {
        this.achnumseq = achnumseq;
    }

    //    public String getAchcodtrn() {
//        return achcodtrn;
//    }
//
//    public void setAchcodtrn(String achcodtrn) {
//        this.achcodtrn = achcodtrn;
//    }
//
//    public Integer getAchnumcaja() {
//        return achnumcaja;
//    }
//
//    public void setAchnumcaja(Integer achnumcaja) {
//        this.achnumcaja = achnumcaja;
//    }
//
//    public Integer getAchnumseq() {
//        return achnumseq;
//    }
//
//    public void setAchnumseq(Integer achnumseq) {
//        this.achnumseq = achnumseq;
//    }
//
//    public Integer getAchnumseqp() {
//        return achnumseqp;
//    }
//
//    public void setAchnumseqp(Integer achnumseqp) {
//        this.achnumseqp = achnumseqp;
//    }
//
//    public Integer getAchnumseqr() {
//        return achnumseqr;
//    }
//
//    public void setAchnumseqr(Integer achnumseqr) {
//        this.achnumseqr = achnumseqr;
//    }
//
//    public String getAchusrapr() {
//        return achusrapr;
//    }
//
//    public void setAchusrapr(String achusrapr) {
//        this.achusrapr = achusrapr;
//    }

    public Long getAchsucbisa() {
        return achsucbisa;
    }

    public void setAchsucbisa(Long achsucbisa) {
        this.achsucbisa = achsucbisa;
    }

//    public String getAcherrbis1() {
//        return acherrbis1;
//    }
//
//    public void setAcherrbis1(String acherrbis1) {
//        this.acherrbis1 = acherrbis1;
//    }
//
//    public String getAcherrbis2() {
//        return acherrbis2;
//    }
//
//    public void setAcherrbis2(String acherrbis2) {
//        this.acherrbis2 = acherrbis2;
//    }
//
//    public String getAcherrbis3() {
//        return acherrbis3;
//    }
//
//    public void setAcherrbis3(String acherrbis3) {
//        this.acherrbis3 = acherrbis3;
//    }
//
//    public String getAcherrbis4() {
//        return acherrbis4;
//    }
//
//    public void setAcherrbis4(String acherrbis4) {
//        this.acherrbis4 = acherrbis4;
//    }
//
//    public String getAcherrbis5() {
//        return acherrbis5;
//    }
//
//    public void setAcherrbis5(String acherrbis5) {
//        this.acherrbis5 = acherrbis5;
//    }

    public Long getAchnrolot() {
        return achnrolot;
    }

    public void setAchnrolot(Long achnrolot) {
        this.achnrolot = achnrolot;
    }

//    public Integer getAchnropag() {
//        return achnropag;
//    }
//
//    public void setAchnropag(Integer achnropag) {
//        this.achnropag = achnropag;
//    }
//
//    public String getAchnrocel() {
//        return achnrocel;
//    }
//
//    public void setAchnrocel(String achnrocel) {
//        this.achnrocel = achnrocel;
//    }
//
//    public String getAchcodapl() {
//        return achcodapl;
//    }
//
//    public void setAchcodapl(String achcodapl) {
//        this.achcodapl = achcodapl;
//    }
//
//    public long getAchaplid() {
//        return achaplid;
//    }
//
//    public void setAchaplid(long achaplid) {
//        this.achaplid = achaplid;
//    }
//
//    public Integer getAchreInteger() {
//        return achreInteger;
//    }
//
//    public void setAchreInteger(Integer achreInteger) {
//        this.achreInteger = achreInteger;
//    }
//
//    public String getAchcust() {
//        return achcust;
//    }
//
//    public void setAchcust(String achcust) {
//        this.achcust = achcust;
//    }

    public String getAchorigfon() {
        return achorigfon;
    }

    public void setAchorigfon(String achorigfon) {
        this.achorigfon = achorigfon;
    }

    public String getAchdestfon() {
        return achdestfon;
    }

    public void setAchdestfon(String achdestfon) {
        this.achdestfon = achdestfon;
    }

    public String getAchsuborig() {
        return achsuborig;
    }

    public void setAchsuborig(String achsuborig) {
        this.achsuborig = achsuborig;
    }

    public String getAchsubdest() {
        return achsubdest;
    }

    public void setAchsubdest(String achsubdest) {
        this.achsubdest = achsubdest;
    }

//    public String getAchtitresp() {
//        return achtitresp;
//    }
//
//    public void setAchtitresp(String achtitresp) {
//        this.achtitresp = achtitresp;
//    }

    public String getAchcodser() {
        return achcodser;
    }

    public void setAchcodser(String achcodser) {
        this.achcodser = achcodser;
    }

    public String getAchcanal() {
        return achcanal;
    }

    public void setAchcanal(String achcanal) {
        this.achcanal = achcanal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (achnumord != null ? achnumord.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AchpMaestra)) {
            return false;
        }
        AchpMaestra other = (AchpMaestra) object;
        if ((this.achnumord == null && other.achnumord != null) || (this.achnumord != null && !this.achnumord.equals(other.achnumord))) {
            return false;
        }
        return true;
    }

    @PostLoad
    public void repairTrim() {
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.getType().getName().equals(String.class.getName())) {
                try {
                    field.setAccessible(true);
                    Object valor = field.get(this);
                    if (valor != null) {
                        field.set(this, ((String) valor).trim());
                    }
                } catch (IllegalAccessException ign) {
                }
            }
        }
    }



    @Override
    public String toString() {
        return "AchpMaestra{" +
                "achnumord=" + achnumord +
                ", achnumori='" + achnumori + '\'' +
                ", achnumach=" + achnumach +
                ", achcodalfa='" + achcodalfa + '\'' +
                ", achorigen='" + achorigen + '\'' +
                ", achpasori='" + achpasori + '\'' +
                ", achsucori='" + achsucori + '\'' +
                ", achdestino='" + achdestino + '\'' +
                ", achpasdes='" + achpasdes + '\'' +
                ", achsucdes='" + achsucdes + '\'' +
                ", achcompen='" + achcompen + '\'' +
                ", achmnddes='" + achmnddes + '\'' +
                ", achmndori='" + achmndori + '\'' +
                ", achmoneda='" + achmoneda + '\'' +
                ", achimporte=" + achimporte +
                ", achtipdoc='" + achtipdoc + '\'' +
                ", achtipord='" + achtipord + '\'' +
                ", achcodprd='" + achcodprd + '\'' +
                ", achtipcnto='" + achtipcnto + '\'' +
                ", achtipcntd='" + achtipcntd + '\'' +
                ", achcodcam='" + achcodcam + '\'' +
                ", achfeccam=" + achfeccam +
                ", achfecenv=" + achfecenv +
                ", achcntorig='" + achcntorig + '\'' +
                ", achcntdest='" + achcntdest + '\'' +
                ", achnitori='" + achnitori + '\'' +
                ", achtitori='" + achtitori + '\'' +
                ", achnitdes='" + achnitdes + '\'' +
                ", achtitdes='" + achtitdes + '\'' +
                ", achnumang='" + achnumang + '\'' +
                ", achiso8583='" + achiso8583 + '\'' +
                ", achglosa='" + achglosa + '\'' +
                ", achstdcom='" + achstdcom + '\'' +
                ", achtipmen='" + achtipmen + '\'' +
                ", achcodresp='" + achcodresp + '\'' +
                ", achestado='" + achestado + '\'' +
                ", achtmecaja=" + achtmecaja +
                ", achtmeapro=" + achtmeapro +
                ", achtmeproc=" + achtmeproc +
                ", achtmerev=" + achtmerev +
                ", achfecproc=" + achfecproc +
                ", achsucbisa=" + achsucbisa +
                ", achorigfon='" + achorigfon + '\'' +
                ", achdestfon='" + achdestfon + '\'' +
                ", achsuborig='" + achsuborig + '\'' +
                ", achsubdest='" + achsubdest + '\'' +
                ", achcodser='" + achcodser + '\'' +
                ", achcanal='" + achcanal + '\'' +
                '}';
    }
}
