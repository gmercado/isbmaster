package com.bisa.bus.servicios.ach.api;

import com.bisa.bus.servicios.ach.entities.AchpMaestra;
import com.bisa.bus.servicios.ach.obj.Respuesta2A;

/**
 * Created by gmercado on 13/11/2017.
 */
public interface AchpDomiciliacionService {
    Respuesta2A procesaOrdenDebito(AchpMaestra achpMaestra);
}
