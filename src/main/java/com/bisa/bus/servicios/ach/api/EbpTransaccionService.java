package com.bisa.bus.servicios.ach.api;

import com.bisa.bus.servicios.ach.entities.AchpMaestra;

/**
 * Created by gmercado on 12/12/2017.
 */
public interface EbpTransaccionService {
    void notificar(AchpMaestra orden);
}
