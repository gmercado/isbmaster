package com.bisa.bus.servicios.ach.enums;

/**
 * @Author Gary Mercado
 * Yrag Knup 28/10/2015.
 */
public enum CausalErrorTipo {
    RC01(1,"La cuenta a debitar no existe"),
    RC03(2,"El tipo de cuenta no es valida"),
    RC04(3,"El codigo de moneda no valido"),
    RC05(4,"No se tiene algun registro FUD"),
    RC07(5,"El importe es mayor al monto maximo permitido"),
    RC08(6,"Existen preacuerdos vencidos"),
    RC09(7,"El FUD esta anulado"),
    RA08(8,"Error interno de la aplicacion"),

    POR_PERIODO_FECHAS(9,"Fecha inicial"),
    D(10,"Definido"),
    I(11,"Indefinido"),

    ;

    private int id;
    private String descripcion;

    CausalErrorTipo(int id,String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
