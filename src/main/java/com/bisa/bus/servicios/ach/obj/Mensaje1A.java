package com.bisa.bus.servicios.ach.obj;

import com.bisa.bus.servicios.ach.entities.AchpMaestra;
import com.bisa.bus.servicios.ach.enums.RespuestaTipo;
import com.bisa.bus.servicios.ach.model.Response;
import com.bisa.bus.servicios.ach.utiles.DateUtil;
import com.bisa.bus.servicios.ach.utiles.JsonUtil;
import com.bisa.bus.servicios.ach.utiles.MonedaUtil;
import com.bisa.isb.api.security.PasswordHash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import static com.bisa.bus.servicios.ach.enums.ParticipanteTipo.BISA;
import static com.bisa.bus.servicios.ach.utiles.RespuestaUtil.cortaString;
import static com.bisa.bus.servicios.ach.utiles.RespuestaUtil.toInteger;
import static com.bisa.bus.servicios.ach.utiles.RespuestaUtil.validaString;

/**
 * Created by gmercado on 18/10/2017.
 */
public class Mensaje1A implements Response {

    private static final Logger LOGGER = LoggerFactory.getLogger(Mensaje1A.class);

    private Integer status;
    private String statusDesc;
    private String deliveryTime;
    private String clearingHouse;
    private String hash;
    private Integer resultCount;
    private List<Item1A> transfers = new ArrayList<>();

    public Mensaje1A() {
    }

    public Mensaje1A(RespuestaTipo tipo) {
        this.status = tipo.getCodigo();
        this.statusDesc = tipo.getDescripcion();
        this.deliveryTime = DateUtil.fechaPattern(DateUtil.FECHA_RFC3339);
        this.clearingHouse = "";
        this.resultCount = 0;
        this.hash = "0";
    }

    public Mensaje1A(String clearingHouse, List<AchpMaestra> items) {
        this.status = RespuestaTipo.OK.getCodigo();
        this.statusDesc = RespuestaTipo.OK.getDescripcion();
        if (items.isEmpty()) {
            this.status = RespuestaTipo.OK.getCodigo();
            this.statusDesc = RespuestaTipo.NO_HAY_PENDIENTES_1A.getDescripcion();
        }
        this.deliveryTime = DateUtil.fechaPattern(DateUtil.FECHA_RFC3339);
        this.clearingHouse = clearingHouse;
        this.resultCount = items.size();
        this.hash = getHash(items);
        for (AchpMaestra item : items) {
            Item1A ordenItem1A =
                    new Item1A(
                            item.getAchfeccam(),
                            BISA.getCodigo().concat(item.getAchnumord().toString()),
                            item.getAchsucori(),
                            item.getAchdestino(),
                            MonedaUtil.getSigla(item.getAchmoneda()),
                            item.getAchimporte(),
                            /*toInteger(item.getAchcodcam()),*/
                            toInteger(item.getAchcodprd()),
                            item.getAchtipcnto(),
                            item.getAchtipcntd(),
                            item.getAchcntorig(),
                            item.getAchcntdest(),
                            validaString(item.getAchglosa()).isEmpty() ? "Sin detalle" : item.getAchglosa(),
                            item.getAchtipdoc(),
                            // TODO: esto de donde obtener es MLD?
                            "",
                            validaString(item.getAchnitori()).isEmpty() ? "0" : item.getAchnitori(),
                            validaString(item.getAchtitori()),
                            //item.getAchnitdes(), Comentamos esto para enviar  el valor 0 ci_nit_destinatario
                            //"0",
                            validaString(item.getAchnitdes()).isEmpty() ? "0" : item.getAchnitori(),
                            item.getAchtitdes(),
                            // TODO: no solicitamos esta informacion
                            "Sin detalle",
                            "Sin detalle",
                            // TODO: aun no tenemos para saliente FUD
                            "",
                            "WEB"
                    );
            transfers.add(ordenItem1A);
        }
    }

    @Override
    //@JsonIgnore
    public Integer getStatus() {
        return status;
    }

    @Override
    //@JsonIgnore
    public String getstatusDesc() {
        return statusDesc;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getClearingHouse() {
        return clearingHouse;
    }

    public void setClearingHouse(String clearingHouse) {
        this.clearingHouse = clearingHouse;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Integer getResultCount() {
        return resultCount;
    }

    public void setResultCount(Integer resultCount) {
        this.resultCount = resultCount;
    }

    public List<Item1A> getTransfers() {
        return transfers;
    }

    public void setTransfers(List<Item1A> transfers) {
        this.transfers = transfers;
    }

    public String getHash(List list) {
        try {
            //TODO: cambiar por md5
            hash = PasswordHash.createHash(JsonUtil.listToJson(list));
            String temp[] = hash.split(":");
            LOGGER.debug("LLAVE GENERADA PARA EL LISTADO DE 1A ENTREGADO {}, {}", temp[0], temp[1]);
            return list.size() > 0 ? temp[1] : "0";
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Error al generar el hash", e);
        } catch (InvalidKeySpecException e) {
            LOGGER.error("Error al generar el hash, llave invalida", e);
        } catch (IOException e) {
            LOGGER.error("Error al generar el hash", e);
        } catch (Exception e) {
            LOGGER.error("Error inesperado al generar el hash", e);
        }
        return "0";
    }

    @Override
    public String toString() {
        return "Mensaje1A{" +
                "status=" + status +
                ", statusDesc='" + statusDesc + '\'' +
                ", deliveryTime=" + deliveryTime +
                ", clearingHouse='" + clearingHouse + '\'' +
                ", hash='" + hash + '\'' +
                ", resultCount=" + resultCount +
                ", transfers=" + transfers +
                '}';
    }
}
