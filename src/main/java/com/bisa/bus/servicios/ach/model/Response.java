package com.bisa.bus.servicios.ach.model;

/**
 * Created by gmercado on 18/10/2017.
 */
public interface Response {

    Integer getStatus();

    String getstatusDesc();

    String toString();
}
