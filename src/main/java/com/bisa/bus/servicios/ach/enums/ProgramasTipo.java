package com.bisa.bus.servicios.ach.enums;

/**
 * Created by gmercado on 01/11/2017.
 */
public enum ProgramasTipo {

    PSC303("ZICBSBISA"),
    PSC307CL("ZICBSBISA"),
    PSC308CL("ZICBSBISA"),
    // Ambiente temporal
    PSC303T("ZICBSBISA"),
    PSC307CLT("ZICBSBISA"),
    PSC308CLT("ZICBSBISA")
    ;
    private String libreria;
    ProgramasTipo(String libreria) {
        this.libreria = libreria;
    }

    public String getLibreria() {
        return libreria;
    }
}
