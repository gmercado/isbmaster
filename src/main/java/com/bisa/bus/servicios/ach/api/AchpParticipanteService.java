package com.bisa.bus.servicios.ach.api;

import com.bisa.bus.servicios.ach.model.Response;
import com.bisa.bus.servicios.ach.obj.JsonRequestParticipanteBasic;

import java.util.List;

/**
 * Created by gmercado on 26/10/2017.
 */
public interface AchpParticipanteService {
    Response insertarNuevoParticipante(JsonRequestParticipanteBasic body);
    Response editarParticipante(JsonRequestParticipanteBasic body);
}
