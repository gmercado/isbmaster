package com.bisa.bus.servicios.ach.entities;

import com.bisa.bus.servicios.ach.obj.JsonRequestParticipanteBasic;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by gmercado on 26/10/2017.
 */
@Entity
@Table(name = "PSP879")
public class AchpParticipante implements Serializable {

    @Id
    @Column(name = "BNCBAN")
    private Integer bncban;

    @Column(name = "BNNOML")
    private String bnnoml;

    @Column(name = "BNNOMC")
    private String bnnomc;

    @Column(name = "BNACTACH")
    private String bnactach;

    public AchpParticipante() {
    }

    public AchpParticipante(JsonRequestParticipanteBasic body) {
        this.bncban = Integer.parseInt(body.getId());
        this.bnnoml = body.getFull_name();
        this.bnnomc = body.getAbbrev_name();
        this.bnactach = Boolean.parseBoolean(body.getEnabled()) ?"S":"N";
    }

    public Integer getBncban() {
        return bncban;
    }

    public void setBncban(Integer bncban) {
        this.bncban = bncban;
    }

    public String getBnnoml() {
        return bnnoml;
    }

    public void setBnnoml(String bnnoml) {
        this.bnnoml = bnnoml;
    }

    public String getBnnomc() {
        return bnnomc;
    }

    public void setBnnomc(String bnnomc) {
        this.bnnomc = bnnomc;
    }

    public String getBnactach() {
        return bnactach;
    }

    public void setBnactach(String bnactach) {
        this.bnactach = bnactach;
    }

    @Override
    public String toString() {
        return "AchpParticipante{" +
                "bncban=" + bncban +
                ", bnnoml='" + bnnoml + '\'' +
                ", bnnomc='" + bnnomc + '\'' +
                ", bnactach='" + bnactach + '\'' +
                '}';
    }
}
