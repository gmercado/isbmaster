package com.bisa.bus.servicios.ach.api;

import com.bisa.bus.servicios.ach.entities.AchpComplemento;

/**
 * Created by gmercado on 24/10/2017.
 */
public interface AchpComplementoService {

    AchpComplemento guardar(AchpComplemento achpComplemento);
}
