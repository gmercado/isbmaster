package com.bisa.bus.servicios.ach.obj;

import java.math.BigDecimal;

/**
 * Created by gmercado on 18/10/2017.
 * Yrag Knup
 */
public class Item1A {

    private Integer fec_camara;
    private String num_orden_originante;
    private String cod_sucursal_originante;
    private String cod_destinatario;
    private String cod_moneda;
    private BigDecimal importe;
    private Integer cod_procedimiento;
    private String tip_cuenta_origen;
    private String tip_cuenta_destino;
    private String cuenta_origen;
    private String cuenta_destino;
    private String glosa;
    private String tipo_documento;
    private String cod_sub_destinatario;
    private String ci_nit_originante;
    private String titular_originante;
    private String ci_nit_destinatario;
    private String titular_destinatario;
    private String origen_fondos;
    private String destino_fondos;
    private String cod_servicio;
    private String canal;

    public Item1A() {
    }

    public Item1A(Integer fec_camara, String num_orden_originante, String cod_sucursal_originante,
                  String cod_destinatario, String cod_moneda, BigDecimal importe, Integer cod_procedimiento,
                  String tip_cuenta_origen, String tip_cuenta_destino, String cuenta_origen, String cuenta_destino,
                  String glosa, String tipo_documento, String cod_sub_destinatario, String ci_nit_originante,
                  String titular_originante, String ci_nit_destinatario, String titular_destinatario,
                  String origen_fondos, String destino_fondos, String cod_servicio, String canal) {
        this.fec_camara = fec_camara;
        this.num_orden_originante = num_orden_originante;
        this.cod_sucursal_originante = cod_sucursal_originante;
        this.cod_destinatario = cod_destinatario;
        this.cod_moneda = cod_moneda;
        this.importe = importe;
        this.cod_procedimiento = cod_procedimiento;
        this.tip_cuenta_origen = tip_cuenta_origen;
        this.tip_cuenta_destino = tip_cuenta_destino;
        this.cuenta_origen = cuenta_origen;
        this.cuenta_destino = cuenta_destino;
        this.glosa = glosa;
        this.tipo_documento = tipo_documento;
        this.cod_sub_destinatario = cod_sub_destinatario;
        this.ci_nit_originante = ci_nit_originante;
        this.titular_originante = titular_originante;
        this.ci_nit_destinatario = ci_nit_destinatario;
        this.titular_destinatario = titular_destinatario;
        this.origen_fondos = origen_fondos;
        this.destino_fondos = destino_fondos;
        this.cod_servicio = cod_servicio;
        this.canal = canal;
    }

    public Integer getFec_camara() {
        return fec_camara;
    }

    public void setFec_camara(Integer fec_camara) {
        this.fec_camara = fec_camara;
    }

    public String getNum_orden_originante() {
        return num_orden_originante;
    }

    public void setNum_orden_originante(String num_orden_originante) {
        this.num_orden_originante = num_orden_originante;
    }

    public String getCod_sucursal_originante() {
        return cod_sucursal_originante;
    }

    public void setCod_sucursal_originante(String cod_sucursal_originante) {
        this.cod_sucursal_originante = cod_sucursal_originante;
    }

    public String getCod_destinatario() {
        return cod_destinatario;
    }

    public void setCod_destinatario(String cod_destinatario) {
        this.cod_destinatario = cod_destinatario;
    }

    public String getCod_moneda() {
        return cod_moneda;
    }

    public void setCod_moneda(String cod_moneda) {
        this.cod_moneda = cod_moneda;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public Integer getCod_procedimiento() {
        return cod_procedimiento;
    }

    public void setCod_procedimiento(Integer cod_procedimiento) {
        this.cod_procedimiento = cod_procedimiento;
    }

    public String getTip_cuenta_origen() {
        return tip_cuenta_origen;
    }

    public void setTip_cuenta_origen(String tip_cuenta_origen) {
        this.tip_cuenta_origen = tip_cuenta_origen;
    }

    public String getTip_cuenta_destino() {
        return tip_cuenta_destino;
    }

    public void setTip_cuenta_destino(String tip_cuenta_destino) {
        this.tip_cuenta_destino = tip_cuenta_destino;
    }

    public String getCuenta_origen() {
        return cuenta_origen;
    }

    public void setCuenta_origen(String cuenta_origen) {
        this.cuenta_origen = cuenta_origen;
    }

    public String getCuenta_destino() {
        return cuenta_destino;
    }

    public void setCuenta_destino(String cuenta_destino) {
        this.cuenta_destino = cuenta_destino;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getCod_sub_destinatario() {
        return cod_sub_destinatario;
    }

    public void setCod_sub_destinatario(String cod_sub_destinatario) {
        this.cod_sub_destinatario = cod_sub_destinatario;
    }

    public String getCi_nit_originante() {
        return ci_nit_originante;
    }

    public void setCi_nit_originante(String ci_nit_originante) {
        this.ci_nit_originante = ci_nit_originante;
    }

    public String getTitular_originante() {
        return titular_originante;
    }

    public void setTitular_originante(String titular_originante) {
        this.titular_originante = titular_originante;
    }

    public String getCi_nit_destinatario() {
        return ci_nit_destinatario;
    }

    public void setCi_nit_destinatario(String ci_nit_destinatario) {
        this.ci_nit_destinatario = ci_nit_destinatario;
    }

    public String getTitular_destinatario() {
        return titular_destinatario;
    }

    public void setTitular_destinatario(String titular_destinatario) {
        this.titular_destinatario = titular_destinatario;
    }

    public String getOrigen_fondos() {
        return origen_fondos;
    }

    public void setOrigen_fondos(String origen_fondos) {
        this.origen_fondos = origen_fondos;
    }

    public String getDestino_fondos() {
        return destino_fondos;
    }

    public void setDestino_fondos(String destino_fondos) {
        this.destino_fondos = destino_fondos;
    }

    public String getCod_servicio() {
        return cod_servicio;
    }

    public void setCod_servicio(String cod_servicio) {
        this.cod_servicio = cod_servicio;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    @Override
    public String toString() {
        return "OrdenItem1A{" +
                "fec_camara=" + fec_camara +
                ", num_orden_originante='" + num_orden_originante + '\'' +
                ", cod_sucursal_originante='" + cod_sucursal_originante + '\'' +
                ", cod_destinatario='" + cod_destinatario + '\'' +
                ", cod_moneda='" + cod_moneda + '\'' +
                ", importe=" + importe +
                ", cod_procedimiento=" + cod_procedimiento +
                ", tip_cuenta_origen='" + tip_cuenta_origen + '\'' +
                ", tip_cuenta_destino='" + tip_cuenta_destino + '\'' +
                ", cuenta_origen='" + cuenta_origen + '\'' +
                ", cuenta_destino='" + cuenta_destino + '\'' +
                ", glosa='" + glosa + '\'' +
                ", tipo_documento='" + tipo_documento + '\'' +
                ", cod_sub_destinatario='" + cod_sub_destinatario + '\'' +
                ", ci_nit_originante='" + ci_nit_originante + '\'' +
                ", titular_originante='" + titular_originante + '\'' +
                ", ci_nit_destinatario='" + ci_nit_destinatario + '\'' +
                ", titular_destinatario='" + titular_destinatario + '\'' +
                ", origen_fondos='" + origen_fondos + '\'' +
                ", destino_fondos='" + destino_fondos + '\'' +
                ", cod_servicio='" + cod_servicio + '\'' +
                ", canal='" + canal + '\'' +
                '}';
    }
}
