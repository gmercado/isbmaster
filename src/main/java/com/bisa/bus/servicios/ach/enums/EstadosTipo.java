package com.bisa.bus.servicios.ach.enums;

import com.google.common.base.Strings;

/**
 * Created by gmercado on 25/10/2017.
 */
public enum EstadosTipo {

    /* ENTRANTES */
    ESTADO_3A_3B_SIN_ERROR("000", "ENTRANTE enviado 3A, 3B sin errores"),

    ESTADO_RECIBIDA_2A("0001", "ENTRANTE 2A registrado sin errores"),
    ESTADO_RECIBIDA_ENVIADA("0002", "ENTRANTE enviado 3A sin errores"),
    ESTADO_RECIBIDA_ENVIADA_ERROR("0003", "ENTRANTE enviado 3A con errores"),

    ESTADO_ENVIADO_1A("2000", "SALIENTE 1A enviado sin errores"),
//    ESTADO_RECIBIDA_REINTENTO_ENVIO("0004", "ENTRANTE reintento enviado 3A con errores"),
//    ESTADO_RECIBIDA_REINTENTO_FALLIDO("0005", "ENTRANTE reintento fallido 3A con errores"),
//    ESTADO_RECIBIDA_REINTENTO_EXITOSO("0006", "ENTRANTE reintento 3A sin errores"),
    /* SALIENTES */
    ESTADO_ENVIADA_RECIBIDA("4000", "SALIENTE con respuesta final de proceso 4A"),

    ESTADO_SIN_ERROR("0000", "SALIENTE respuesta 4A sin errores o 2A sin error"),
    ESTADO_TRANSITO("3", "Transito"),
    ESTADO_PROCESADO("4", "Aceptado"),
    ESTADO_RECHAZADO("5", "Rechazado"),
    ESTADO_REVERTIDO("9", "Revertido"),
//    Ordenes de debito


    ;

    private String codigo;
    private String descripcion;

    EstadosTipo(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String decripcion) {
        this.descripcion = decripcion;
    }

    @Override
    public String toString() {
        return "Estado3ATipo{" +
                "codigo='" + codigo + '\'' +
                ", decripcion='" + descripcion + '\'' +
                '}';
    }
}
