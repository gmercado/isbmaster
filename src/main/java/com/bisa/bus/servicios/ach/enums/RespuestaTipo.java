package com.bisa.bus.servicios.ach.enums;

/**
 * Created by gmercado on 23/10/2017.
 */
public enum RespuestaTipo {

    OK(0, "Satisfactorio"),
    REVERTIDA_OK(0, "Orden Revertida correctamente"),

    CICLO_INSERTADO(201, "Ciclo insertado correctamente"),
    CICLO_EXISTENTE(409, "El ciclo ya existe!"),
    CICLO_EDITADO(201, "El ciclo fue editado correctamente"),
    CICLO_NO_EXISTE(409, "El ciclo no existe para ser editado"),
    CICLO_ERROR(410, "Error al insertar ciclo"),
    CICLO_FORMATO_HORAINI(430, "El formato del Campo Hora Inicio no es correcto"),
    CICLO_FORMATO_HORAFIN(431, "El formato del Campo Hora Final no es correcto"),
    CICLO_FORMATO_DIAHORAINI(430, "El formato del Campo Día Hora Inicio no es correcto"),
    CICLO_FORMATO_DIAHORAFIN(431, "El formato del Campo Día Hora Final no es correcto"),
    CICLO_HORAINI_MAYOR(430, "El valor de Hora Inicio es Mayor a la Hora Final"),
    CICLO_DIAHORAINI_MAYOR(431, "El valor de Día Hora Inicio es mayor a Día Hora Final"),



    PARTICIPANTE_INSERTADO(201, "Participante insertado correctamente"),
    PARTICIPANTE_EXISTENTE(409, "El participante ya existe!"),
    PARTICIPANTE_EDITADO(201, "Participante editado correctamente"),
    PARTICIPANTE_NO_EXISTE(409, "El participante no existe para su edicion"),
    PARTICIPANTE_LONGITUD_PARTICPANTE(330, "La longitud del Codigo de Participantes es muy grande"),
    PARTICIPANTE_LONGITUD_NOMBRE_LARGO(331, "La longitud del Nombre Largo es muy grande"),
    PARTICIPANTE_LONGITUD_NOMBRE_CORTO(332, "La longitud del Nombre Corto es muy grande"),


    EVENTO_INSERTADO(201, "Evento correctamnte procesado"),

    AMBIENTE_CERRADO(777, "La aplicación se encuentra en ambiente cerrado, intentar mas tarde"),

    ERROR_DOMICILIACION(898, "Error al registrar la orden de debito"),
    PARAMETROS_FALTANTES(899, "No se puede consumir por falta de envio de parametros al servicio "),
    LIMITE_SUPERADO(900, "Limite incorrecto, este debe ser numerico positivo y menor a 1000"),
    FECHA_CAMARA_INCORRECTA(991, "La fecha de camara no es correcta"),
    ERROR_LLAMADA_PROGRAMA(992, "No se ejecuto el programa RPG "),
    ORDEN_YA_PROCESADA(993, "La orden se encuentra en un estado final"),
    ERROR_AL_REVERTIR(994, "Error al revertir orden rechazada, re-intentar proceso"),
    NO_HAY_PENDIENTES_1A(995, "No se encontraron pendientes de envio"),
    NRO_ACH_DUPLICADO(996, "Orden ACH ACCL duplicado"),
    SIN_RESULTADO(997, "No se encontro el registro en base de datos con el numero orden "),
    DB_ERROR(998, "Error de base de datos"),
    ERROR(999, "Error Inesperado")
    ;
    private Integer codigo;
    private String descripcion;

    RespuestaTipo(Integer codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
