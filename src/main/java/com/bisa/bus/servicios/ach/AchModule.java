package com.bisa.bus.servicios.ach;

import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.bus.servicios.ach.api.*;
import com.bisa.bus.servicios.ach.dao.*;

/**
 * Created by gmercado on 20/10/2017.
 */
public class AchModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(AchpMaestraService.class).to(AchpMaestraDao.class);
        bind(AchpComplementoService.class).to(AchpComplementoDao.class);
        bind(AchpCiclosService.class).to(AchpCiclosDao.class);
        bind(AchpParticipanteService.class).to(AchpParticipanteDao.class);
        bind(AchpDomiciliacionService.class).to(AchpDomiciliacionDao.class);
        bind(EbpTransaccionService.class).to(EbpTransaccionDao.class);

        bind(AchCiudadesService.class).to(AchCiudadesDao.class);
        bindUI("com/bisa/bus/servicios/ach/ui");
    }
}
