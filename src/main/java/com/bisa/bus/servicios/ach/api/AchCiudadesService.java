package com.bisa.bus.servicios.ach.api;

/**
 * Created by gmercado on 24/10/2017.
 */
public interface AchCiudadesService {

    Long getSucursal(String sucursal);
}
