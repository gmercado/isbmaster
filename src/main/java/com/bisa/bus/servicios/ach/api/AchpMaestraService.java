package com.bisa.bus.servicios.ach.api;

import com.bisa.bus.servicios.ach.entities.AchpMaestra;
import com.bisa.bus.servicios.ach.model.Response;
import com.bisa.bus.servicios.ach.obj.*;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * Created by gmercado on 20/10/2017.
 */
public interface AchpMaestraService {

    Map<String, List<AchpMaestra>> getPendientes1A (Integer idOrdenes, Integer limit);
    Response procesar4A (Long nroOrden, JsonRequestBasic body);
    Response procesar5A (Long nroOrden, JsonRequestBasic body);
    Response procesar2A (JsonRequest2ABasic body);
    Response procesar3B (Long nroOrden, JsonRequest3BBasic body);
}
