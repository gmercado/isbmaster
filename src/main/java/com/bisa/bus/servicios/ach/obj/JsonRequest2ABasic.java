package com.bisa.bus.servicios.ach.obj;

import com.bisa.bus.servicios.ach.model.RequestEntrante;
import com.fasterxml.jackson.annotation.*;

import java.io.Serializable;

/**
 * Created by gmercado on 23/10/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequest2ABasic implements Serializable, RequestEntrante {

    private Integer attempt;
    @JsonProperty("transfer")
    private Transfer2A transfer2A;

    public JsonRequest2ABasic() {
    }

    public JsonRequest2ABasic(Integer attempt, Transfer2A transfer2A) {
        this.attempt = attempt;
        this.transfer2A = transfer2A;
    }

    @Override
    public Integer getAttempt() {
        return attempt;
    }

    public Transfer2A getTransfer2A() {
        return transfer2A;
    }

    @Override
    public String toString() {
        return "JsonRequest2ABasic{" +
                "attempt=" + attempt +
                ", transfer2A=" + transfer2A +
                '}';
    }
}
