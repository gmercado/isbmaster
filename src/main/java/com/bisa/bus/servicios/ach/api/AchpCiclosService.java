package com.bisa.bus.servicios.ach.api;

import com.bisa.bus.servicios.ach.model.Response;
import com.bisa.bus.servicios.ach.obj.JsonRequestCicloBasic;

/**
 * Created by gmercado on 26/10/2017.
 */
public interface AchpCiclosService {

    Response insertarNuevoCiclo(JsonRequestCicloBasic body);
    Response editarCiclo(JsonRequestCicloBasic body);
}
