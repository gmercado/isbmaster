package com.bisa.bus.servicios.ach.enums;

/**
 * Created by gmercado on 26/10/2017.
 */
public enum ParticipanteTipo {
    // Para habilitar el participante alasita en pruebas de desarrollo
    // ingresar en el enum BISA el codigo 1109
    // BISA("1109"),
    BISA("1009"),
    ACCL("1426001");

    private String codigo;

    ParticipanteTipo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
