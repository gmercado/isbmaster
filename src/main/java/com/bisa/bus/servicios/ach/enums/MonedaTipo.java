package com.bisa.bus.servicios.ach.enums;

/**
 * Created by gmercado on 31/10/2017.
 * Yrag Knup
 */
public enum MonedaTipo {
    BOLIVIANO_1("1", "BOB"),
    DOLAR("2", "USD");

    private String codigo;
    private String sigla;

    MonedaTipo(String codigo, String sigla) {
        this.codigo = codigo;
        this.sigla = sigla;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
}
