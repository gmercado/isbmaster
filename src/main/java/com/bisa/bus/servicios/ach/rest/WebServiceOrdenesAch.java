package com.bisa.bus.servicios.ach.rest;

import bus.database.model.AmbienteActual;
import com.bisa.bus.servicios.ach.api.AchpCiclosService;
import com.bisa.bus.servicios.ach.api.AchpParticipanteService;
import com.bisa.bus.servicios.ach.api.AchpMaestraService;
import com.bisa.bus.servicios.ach.entities.AchpMaestra;
import com.bisa.bus.servicios.ach.enums.RespuestaTipo;
import com.bisa.bus.servicios.ach.model.Response;
import com.bisa.bus.servicios.ach.obj.*;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import java.util.List;
import java.util.Map;

/**
 * Created by gmercado on 18/10/2017.
 * Yrag knup
 */
@Path("/ordenes")
@Produces("application/json;charset=UTF-8")
public class WebServiceOrdenesAch {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebServiceOrdenesAch.class);

    @Inject
    private AmbienteActual ambienteActual;
    @Inject
    private AchpMaestraService achpMaestraService;
    @Inject
    private AchpCiclosService achpCiclosService;
    @Inject
    private AchpParticipanteService achpParticipanteService;

    /**
     * SALIENTES
     **/
    @GET
    @Path("/salientes")
    public Response pendientes1A(@QueryParam("attempt") Integer attempt,
                                 @QueryParam("requestId") Integer requestId,
                                 @QueryParam("limit") Integer limit) {
        LOGGER.info("Buscando ordenes pendientes 1A, para ser enviadas ... ");
        if (ambienteActual.isOffline()) {
            LOGGER.info("El sistema se encuentra en ambiente cerrado, no procede con la peticion");
            return new Mensaje1A(RespuestaTipo.AMBIENTE_CERRADO);
        }
        if (attempt == null || requestId == null || limit == null) {
            return new Respuesta(RespuestaTipo.PARAMETROS_FALTANTES.getCodigo(),
                    RespuestaTipo.PARAMETROS_FALTANTES.getDescripcion());
        }
        if (limit < 0 || limit > 999) {
            return new Respuesta(RespuestaTipo.LIMITE_SUPERADO.getCodigo(),
                    RespuestaTipo.LIMITE_SUPERADO.getDescripcion());
        }
        LOGGER.info(" Limite de ordenes a retornar: {}", limit);
        Map<String, List<AchpMaestra>> pendientes = achpMaestraService.getPendientes1A(requestId, limit);
        for (String clearingHouse : pendientes.keySet()) {
            return new Mensaje1A(clearingHouse, pendientes.get(clearingHouse));
        }
        return new Mensaje1A(RespuestaTipo.NO_HAY_PENDIENTES_1A);
    }

    @POST
    @Path("/salientes/{numOrden}/mensaje4A")
    public Response procesa4A(@PathParam("numOrden") String numOrden, @Context HttpHeaders httpHeaders, JsonRequestBasic body) {
        LOGGER.info("Procesando mensaje 4A, se lleva a un estado final la orden: {}, body: {} ", numOrden, body);
        if (ambienteActual.isOffline()) {
            LOGGER.info("El sistema se encuentra en ambiente cerrado, no procede con la peticion");
            return new Respuesta(RespuestaTipo.AMBIENTE_CERRADO.getCodigo(),
                    RespuestaTipo.AMBIENTE_CERRADO.getDescripcion());
        }
        if (numOrden == null) {
            return new Respuesta(RespuestaTipo.PARAMETROS_FALTANTES.getCodigo(),
                    RespuestaTipo.PARAMETROS_FALTANTES.getDescripcion().concat("numOrden nulo"));
        }
        return achpMaestraService.procesar4A(Long.parseLong(numOrden), body);
    }

    @POST
    @Path("/salientes/{numOrden}/mensaje5A")
    public Response procesaSaliente5A(@PathParam("numOrden") String numOrden, @Context HttpHeaders httpHeaders, JsonRequestBasic body) {
        LOGGER.info("Procesando mensaje 5A saliente, se lleva a un estado final la orden: {} que no completo su ciclo hasta el final de camara, body: {} ", numOrden, body);
        if (ambienteActual.isOffline()) {
            LOGGER.info("El sistema se encuentra en ambiente cerrado, no procede con la peticion");
            return new Respuesta(RespuestaTipo.AMBIENTE_CERRADO.getCodigo(),
                    RespuestaTipo.AMBIENTE_CERRADO.getDescripcion());
        }
        if (numOrden == null) {
            return new Respuesta(RespuestaTipo.PARAMETROS_FALTANTES.getCodigo(),
                    RespuestaTipo.PARAMETROS_FALTANTES.getDescripcion().concat("numOrden nulo"));
        }
        return achpMaestraService.procesar5A(Long.parseLong(numOrden), body);
    }

    /**
     * ENTRANTES
     **/
    @POST
    @Path("/entrantes")
    public Response procesa2A(@Context HttpHeaders httpHeaders, JsonRequest2ABasic body) {
        LOGGER.info("Procesando mensaje 2A, se inserta en ACHP01 el siguiente registro, body: {} ", body);
        if (ambienteActual.isOffline()) {
            LOGGER.info("El sistema se encuentra en ambiente cerrado, no procede con la peticion");
            return new Respuesta(RespuestaTipo.AMBIENTE_CERRADO.getCodigo(),
                    RespuestaTipo.AMBIENTE_CERRADO.getDescripcion());
        }
        return achpMaestraService.procesar2A(body);
    }

    @POST
    @Path("/entrantes/{numOrden}/mensaje3B")
    public Response procesa3B(@PathParam("numOrden") String numOrden, @Context HttpHeaders httpHeaders, JsonRequest3BBasic body) {
        LOGGER.info("Procesando acuse de recibo 3B, orden: {}, body: {} ", numOrden, body);
        if (ambienteActual.isOffline()) {
            LOGGER.info("El sistema se encuentra en ambiente cerrado, no procede con la peticion");
            return new Respuesta(RespuestaTipo.AMBIENTE_CERRADO.getCodigo(),
                    RespuestaTipo.AMBIENTE_CERRADO.getDescripcion());
        }
        if (numOrden == null) {
            return new Respuesta(RespuestaTipo.PARAMETROS_FALTANTES.getCodigo(),
                    RespuestaTipo.PARAMETROS_FALTANTES.getDescripcion().concat("numOrden nulo"));
        }
        return achpMaestraService.procesar3B(Long.parseLong(numOrden), body);
    }

    @POST
    @Path("/entrantes/{numOrden}/mensaje5A")
    public Response procesaEntrante5A(@PathParam("numOrden") String numOrden, @Context HttpHeaders httpHeaders, JsonRequestBasic body) {
        LOGGER.info("Procesando mensaje 5A entrante, se lleva a un estado final la orden: {} que no completo su ciclo hasta el final de camara, body: {} ", numOrden, body);
        if (ambienteActual.isOffline()) {
            LOGGER.info("El sistema se encuentra en ambiente cerrado, no procede con la peticion");
            return new Respuesta(RespuestaTipo.AMBIENTE_CERRADO.getCodigo(),
                    RespuestaTipo.AMBIENTE_CERRADO.getDescripcion());
        }
        if (numOrden == null) {
            return new Respuesta(RespuestaTipo.PARAMETROS_FALTANTES.getCodigo(),
                    RespuestaTipo.PARAMETROS_FALTANTES.getDescripcion().concat("numOrden nulo"));
        }
        return achpMaestraService.procesar5A(Long.parseLong(numOrden), body);
    }

    /**
     * CICLOS
     **/
    @POST
    @Path("/ach/ciclos")
    public Response nuevoCiclo(@Context HttpHeaders httpHeaders, JsonRequestCicloBasic body) {
        LOGGER.info("Post ciclos: se intenta crear un nuevo recurso: {} ", body);
        if (ambienteActual.isOffline()) {
            LOGGER.info("El sistema se encuentra en ambiente cerrado, no procede con la peticion");
            return new Respuesta(RespuestaTipo.AMBIENTE_CERRADO.getCodigo(),
                    RespuestaTipo.AMBIENTE_CERRADO.getDescripcion());
        }
        if (Strings.isNullOrEmpty(body.getClearinghouse())) {
            return new Respuesta(RespuestaTipo.PARAMETROS_FALTANTES.getCodigo(),
                    RespuestaTipo.PARAMETROS_FALTANTES.getDescripcion());
        }
        if (Strings.isNullOrEmpty(body.getDayoff_end_time()) || Strings.isNullOrEmpty(body.getDayoff_start_time())) {
            if (Strings.isNullOrEmpty(body.getEnd_time()) || Strings.isNullOrEmpty(body.getStart_time())) {
                return new Respuesta(RespuestaTipo.PARAMETROS_FALTANTES.getCodigo(),
                        RespuestaTipo.PARAMETROS_FALTANTES.getDescripcion());
            }
        }
        return achpCiclosService.insertarNuevoCiclo(body);
    }

    @PUT
    @Path("/ach/ciclos")
    public Response actualizarCiclo(@Context HttpHeaders httpHeaders, JsonRequestCicloBasic body) {
        LOGGER.info("Put ciclos: se intenta actualizar un recurso: {} ", body);
        if (ambienteActual.isOffline()) {
            LOGGER.info("El sistema se encuentra en ambiente cerrado, no procede con la peticion");
            return new Respuesta(RespuestaTipo.AMBIENTE_CERRADO.getCodigo(),
                    RespuestaTipo.AMBIENTE_CERRADO.getDescripcion());
        }
        if (Strings.isNullOrEmpty(body.getClearinghouse())) {
            return new Respuesta(RespuestaTipo.PARAMETROS_FALTANTES.getCodigo(),
                    RespuestaTipo.PARAMETROS_FALTANTES.getDescripcion());
        }
        if (Strings.isNullOrEmpty(body.getDayoff_end_time()) || Strings.isNullOrEmpty(body.getDayoff_start_time())) {
            if (Strings.isNullOrEmpty(body.getEnd_time()) || Strings.isNullOrEmpty(body.getStart_time())) {
                return new Respuesta(RespuestaTipo.PARAMETROS_FALTANTES.getCodigo(),
                        RespuestaTipo.PARAMETROS_FALTANTES.getDescripcion());
            }
        }
        return achpCiclosService.editarCiclo(body);
    }

    /**
     * PARTICIPANTES
     **/
    @POST
    @Path("/ach/participantes")
    public Response nuevoParticipante(@Context HttpHeaders httpHeaders, JsonRequestParticipanteBasic body) {
        LOGGER.info("Post participante: se intenta crear un nuevo recurso: {} ", body);
        if (ambienteActual.isOffline()) {
            LOGGER.info("El sistema se encuentra en ambiente cerrado, no procede con la peticion");
            return new Respuesta(RespuestaTipo.AMBIENTE_CERRADO.getCodigo(),
                    RespuestaTipo.AMBIENTE_CERRADO.getDescripcion());
        }
        if (Strings.isNullOrEmpty(body.getClearinghouse())
                || Strings.isNullOrEmpty(body.getId()) || Strings.isNullOrEmpty(body.getAbbrev_name())
                || Strings.isNullOrEmpty(body.getFull_name())) {
            return new Respuesta(RespuestaTipo.PARAMETROS_FALTANTES.getCodigo(),
                    RespuestaTipo.PARAMETROS_FALTANTES.getDescripcion());
        }
        return achpParticipanteService.insertarNuevoParticipante(body);
    }

    @PUT
    @Path("/ach/participantes")
    public Response actualizarParticipante(@Context HttpHeaders httpHeaders, JsonRequestParticipanteBasic body) {
        LOGGER.info("Put participante: se intenta actualizar un registro: {} ", body);
        if (ambienteActual.isOffline()) {
            LOGGER.info("El sistema se encuentra en ambiente cerrado, no procede con la peticion");
            return new Respuesta(RespuestaTipo.AMBIENTE_CERRADO.getCodigo(),
                    RespuestaTipo.AMBIENTE_CERRADO.getDescripcion());
        }
        if (Strings.isNullOrEmpty(body.getClearinghouse())
                || Strings.isNullOrEmpty(body.getId()) || Strings.isNullOrEmpty(body.getAbbrev_name())
                || Strings.isNullOrEmpty(body.getFull_name())) {
            return new Respuesta(RespuestaTipo.PARAMETROS_FALTANTES.getCodigo(),
                    RespuestaTipo.PARAMETROS_FALTANTES.getDescripcion());
        }
        return achpParticipanteService.editarParticipante(body);
    }

    /**
     * EVENTOS
     **/
    @POST
    @Path("/ach/eventos")
    public Response nuevoEvento(@Context HttpHeaders httpHeaders, JsonRequestCicloBasic body) {
        LOGGER.info("Post eventos: se intenta crear un nuevo recurso: {} ", body);
        if (ambienteActual.isOffline()) {
            LOGGER.info("El sistema se encuentra en ambiente cerrado, no procede con la peticion");
            return new Respuesta(RespuestaTipo.AMBIENTE_CERRADO.getCodigo(),
                    RespuestaTipo.AMBIENTE_CERRADO.getDescripcion());
        }
        return new Respuesta(RespuestaTipo.EVENTO_INSERTADO.getCodigo(),
                RespuestaTipo.EVENTO_INSERTADO.getDescripcion());
    }
}
