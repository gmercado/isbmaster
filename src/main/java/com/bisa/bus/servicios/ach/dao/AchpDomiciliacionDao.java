package com.bisa.bus.servicios.ach.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.ach.api.AchpDomiciliacionService;
import com.bisa.bus.servicios.ach.entities.AchpMaestra;
import com.bisa.bus.servicios.ach.entities.AchpRegistroDebito;
import com.bisa.bus.servicios.ach.enums.CausalErrorTipo;
import com.bisa.bus.servicios.ach.enums.EstadoFUDTipo;
import com.bisa.bus.servicios.ach.enums.MonedaTipo;
import com.bisa.bus.servicios.ach.enums.RespuestaTipo;
import com.bisa.bus.servicios.ach.obj.Respuesta2A;
import com.bisa.bus.servicios.ach.obj.RespuestaTransfer2A;
import com.bisa.bus.servicios.ach.utiles.DateUtil;
import com.bisa.bus.servicios.ach.utiles.MonedaUtil;
import com.bisa.bus.servicios.ach.utiles.RespuestaUtil;
import com.google.inject.Inject;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.openlogics.gears.jdbc.DataStoreFactory;
import org.openlogics.gears.jdbc.ObjectDataStore;
import org.openlogics.gears.jdbc.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static bus.util.Sentencias.*;
import static com.bisa.bus.servicios.ach.enums.CausalErrorTipo.*;
import static com.bisa.bus.servicios.ach.enums.EstadosTipo.ESTADO_RECHAZADO;
import static com.bisa.bus.servicios.ach.utiles.DatosACH.CUENTA_DESTINO;
import static com.bisa.bus.servicios.ach.utiles.RespuestaUtil.validaString;

/**
 * Created by gmercado on 13/11/2017.
 * Yrag Knup
 * Esta logica se llevó tal cual se tiene en el antiguo clienteACH
 * ¿Será necesario llevar al estandar de JPA?
 */
public class AchpDomiciliacionDao extends DaoImpl<AchpMaestra, Long> implements Serializable, AchpDomiciliacionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AchpDomiciliacionDao.class);

    @Inject
    protected AchpDomiciliacionDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public Respuesta2A procesaOrdenDebito(AchpMaestra orden) {
        ObjectDataStore ds = null;
        try {
            ds = DataStoreFactory.createObjectDataStore(getConnection());
            ds.setAutoClose(false);
            // Toda orden de debito debe ser a una cuenta
            if (!CUENTA_DESTINO.equals(orden.getAchtipcntd()))
                return rechazoDomiciliacion(orden, RC03);

            final BigDecimal cuenta = RespuestaUtil.validaCuenta(orden.getAchcntdest());
            // Se verifica que la cuenta a debitar exista
            if (!existeCuenta(ds, cuenta))
                return rechazoDomiciliacion(orden, RC01);

            // Obtener el FUD con el numero de cuenta destinatario y NIT originante
            LOGGER.info("Buscando FUD (achp12) con numero de cuenta: " + cuenta + " Y NIT: " + orden.getAchnitori());
            List<Map<String, Object>> fudListMap = ds.select(org.openlogics.gears.jdbc.Query.of(BUSCAR_FUD, cuenta,
                    orden.getAchnitori()), new MapListHandler());
            Map<String, Object> achp12Map = new HashMap<>();
            if (!fudListMap.isEmpty()) {
                // obtenemos el FUD de la empresa por servicio
                for (Map<String, Object> fud : fudListMap) {
                    String codServicio = validaString(fud.get("A15CODSER"));
                    if (codServicio.toUpperCase().trim().equals(orden.getAchcodser().toUpperCase())) {
                        achp12Map.putAll(fud);
                        break;
                    }
                }
            }
            LOGGER.info("FUD encontrado : {} ", achp12Map);
            if (achp12Map.isEmpty() || achp12Map.size() == 0) {
                return rechazoDomiciliacion(orden, RC05);
            }

            BigDecimal afecIni = (BigDecimal) achp12Map.get("A12FECINI");
            BigDecimal afecFin = (BigDecimal) achp12Map.get("A12FECFIN");
            BigDecimal cuotaMaxima = (BigDecimal) achp12Map.get("A12CUOTAS");
            String tipoFUD = validaString(achp12Map.get("A12FIJO"));

            if (D.name().equals(tipoFUD) && cuotaMaxima.compareTo(BigDecimal.ZERO) == 0
                    && afecIni.compareTo(BigDecimal.ZERO) == 0 && afecFin.compareTo(BigDecimal.ZERO) == 0) {
                LOGGER.error("El FUD no cuenta con valores para control de vigencia");
                return rechazoDomiciliacion(orden, RA08);
            }

            // Controlar restricciones por FUD
            Integer cuota = ds.select(org.openlogics.gears.jdbc.Query.of(NRO_DEBITOS, achp12Map.get("A12ID")), new ScalarHandler<Integer>());
            LOGGER.info("Se controla cantidad de cuotas canceladas CUOTA ACTUAL: " + cuota + " CUOTA LIMITE: " + cuotaMaxima);
            if (cuotaMaxima != null && cuotaMaxima.compareTo(BigDecimal.ZERO) > 0 && cuota.compareTo(cuotaMaxima.intValue()) >= 0) {
                LOGGER.info("El total de las cuotas ya fueron debitas, ya no se procedera a debitar: {}", RC08.getDescripcion());
                // se debe actualizar el FUD con estado inactivo
                ds.update(org.openlogics.gears.jdbc.Query.of(CAMBIO_ESTADO_FUD, EstadoFUDTipo.I.name(), "Total de cuotas ya debitadas", achp12Map.get("A12ID")));
                return rechazoDomiciliacion(orden, RC08);
            }
            // Si el FUD es D entonceso no hacer validaciones de fechas limites
            if (D.name().equalsIgnoreCase(tipoFUD) && afecIni.compareTo(BigDecimal.ZERO) > 0 && afecFin.compareTo(BigDecimal.ZERO) > 0) {
                LOGGER.info("Se procesa la restriccion: {}" + POR_PERIODO_FECHAS.getDescripcion());
                BigDecimal fechaActual = new BigDecimal(DateUtil.fechaSistema());
                if (afecIni.compareTo(fechaActual) > 0 || afecFin.compareTo(fechaActual) < 0) {
                    LOGGER.info("Las fechas no estan dentro del rango de validacion.");
                    // El periodo no esta vigente
                    ds.update(org.openlogics.gears.jdbc.Query.of(CAMBIO_ESTADO_FUD, EstadoFUDTipo.I.name(), "Periodo de fechas es invalido", achp12Map.get("A12ID")));
                    return rechazoDomiciliacion(orden, RC08);
                }
            }
            // verificamos limites de importe de pago
            BigDecimal importe = orden.getAchimporte();
            BigDecimal maxAmount = (BigDecimal) achp12Map.get("A15IMPMAX");
            String monedaTrx = MonedaUtil.getSigla(orden.getAchmoneda());
            String monedaFud = validaString(achp12Map.get("A15MONEDA"));

            if (maxAmount == null || maxAmount.compareTo(BigDecimal.ZERO) == 0) {
                LOGGER.error("El FUD no tiene registro maximo de cuota en Empresa");
                return rechazoDomiciliacion(orden, RA08);
            }
            LOGGER.debug("Las monedas son iguales para el debito? : {} " + monedaTrx.toUpperCase() + " " + monedaFud.toUpperCase());
            if (!monedaTrx.toUpperCase().equals(monedaFud.trim().toUpperCase())) {
                // en la consulta se obtiene el tipo de cambio mas actual
                BigDecimal tipoCambio = ds.select(Query.of(TIPO_DE_CAMBIO), new ScalarHandler<BigDecimal>());
                // Nos quedamos con el ultimo tipoDeCambio ingresado
                if (tipoCambio != null && tipoCambio.compareTo(BigDecimal.ZERO) == 0) {
                    LOGGER.error("No se encontro el tipo de cambio!");
                    return rechazoDomiciliacion(orden, RA08);
                }
                // TODO: si es compra o venta preferencial?
                // Si es en bolivianos el destinatario, entonces el cliente paga en dolares
                if (MonedaTipo.BOLIVIANO_1.getSigla().equals(monedaTrx.toUpperCase())) {
                    LOGGER.info("Cambiando a dolares ....");
                    importe = importe.divide(tipoCambio, MathContext.DECIMAL32);
                } else if (MonedaTipo.BOLIVIANO_1.getSigla().equals(monedaTrx.toUpperCase())) {
                    LOGGER.info("Cambiando a bolivianos ....");
                    importe = importe.multiply(tipoCambio);
                } else {
                    LOGGER.error("El tipo de moneda de la cuenta no es valida");
                    return rechazoDomiciliacion(orden, RC04);
                }
                LOGGER.info("IMPORTE: {} COTIZACION: {} TIPO CAMBIO: ", orden.getAchimporte(), tipoCambio, importe);
            }
            LOGGER.info("IMPORTE: " + importe + " IMPORTE MAXIMO: " + maxAmount);
            if (importe.compareTo(maxAmount) > 0) {
                LOGGER.info("El importe es mayor al monto maximo permitido.");
                return rechazoDomiciliacion(orden, RC07);
            }

            AchpRegistroDebito achp20 = new AchpRegistroDebito((BigDecimal) achp12Map.get("A12ID"),
                    new BigDecimal(orden.getAchnumord()), new Date());
            ds.add(achp20);

            return new Respuesta2A(RespuestaTipo.OK.getCodigo(), RespuestaTipo.OK.getDescripcion(), null);
        } catch (SQLException e) {
            LOGGER.error("Ocurrio un error en base de datos en el proceso de Domiciliacion! ", e);
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error inesperado en el proceso de Domiciliacion! ", e);
        } finally {
            if (ds != null) {
                try {
                    ds.tryCommitAndClose();
                } catch (SQLException ign) {}
            }
        }
        return rechazoDomiciliacion(orden, RA08);
    }

    public Respuesta2A rechazoDomiciliacion(AchpMaestra orden, CausalErrorTipo tipo) {
        LOGGER.info("Respuesta del sistema para la orden de domiciliacion: {}", tipo.getDescripcion());
        orden.setAchcodresp(tipo.name());
        orden.setAchdesresp(tipo.getDescripcion());
        orden.setAchestado(ESTADO_RECHAZADO.getCodigo());
        return new Respuesta2A(RespuestaTipo.OK.getCodigo(), tipo.getDescripcion(),
                new RespuestaTransfer2A(orden.getAchnumord(), tipo.name(), orden.getAchsucdes(),
                        orden.getAchtitdes()));
    }

    /**
     * Valida que la cuenta a debitar exista
     * @param ds ObjectDataStore
     * @param cuenta
     * @return true, false
     */
    public boolean existeCuenta(ObjectDataStore ds, BigDecimal cuenta) {
        Integer retorna = 0;
        try {
            retorna = ds.select(org.openlogics.gears.jdbc.Query.of(CUENTA_EXISTE, cuenta), new ScalarHandler<Integer>());
        } catch (SQLException e) {
            LOGGER.error("No se pudo verificar la existencia de la cuenta en el proceso de Domiciliacion", e);
        }
        if (retorna != null) {
            return retorna == 1 ? true : false;
        }
        return false;
    }
}
