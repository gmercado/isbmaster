package com.bisa.bus.servicios.ach.utiles;

import com.bisa.bus.servicios.ach.enums.MonedaTipo;
import com.google.common.base.Strings;

/**
 * Created by gmercado on 31/10/2017.
 */
public class MonedaUtil {

    public static String getId(String valor) throws NumberFormatException {
        String sigla = Strings.emptyToNull(valor).trim();
        for (MonedaTipo monedaTipo : MonedaTipo.values()) {
            if (monedaTipo.getSigla().equals(sigla)) {
                return monedaTipo.getCodigo();
            }
        }
        // Si retornamos cero, el programa PSC302 procesara la orden como RA08
        return "0";
    }

    public static String getSigla(String valor) throws NumberFormatException {
        String codigo = Strings.emptyToNull(valor).trim();
        for (MonedaTipo monedaTipo : MonedaTipo.values()) {
            if (monedaTipo.getCodigo().equals(codigo)) {
                return monedaTipo.getSigla();
            }
        }
        return "XX";
    }
}
