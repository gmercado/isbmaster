package com.bisa.bus.servicios.ach.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.ach.api.AchCiudadesService;
import com.bisa.bus.servicios.ach.entities.Ciudades;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.io.Serializable;

/**
 * Created by gmercado on 24/10/2017.
 */
public class AchCiudadesDao extends DaoImpl<Ciudades, Long> implements Serializable, AchCiudadesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AchCiudadesDao.class);

    @Inject
    protected AchCiudadesDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    /**
     * Obtiene codigo de sucursal por ciudad
     * @param sucursal
     * @return
     */
    public Long getSucursal(String sucursal) {
        try {
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            String query = "SELECT a.cmsucprn FROM Ciudades a " +
                    "WHERE a.cmciudad IN (SELECT b.achciubis FROM Sucursales b WHERE b.achsucach= :sucursal) " +
                    "ORDER BY  a.cmsucprn ASC";
            Query q = entityManager.createQuery(query);
            q.setParameter("sucursal", sucursal);
            q.setFirstResult(0);
            return (Long) q.setMaxResults(1).getResultList().get(0);
        } catch (NoResultException e) {
            LOGGER.error("No se pudo obtener la sucursal por ciudad, revisar!", e);
        } catch (Exception e) {
            LOGGER.error("Error inesperado no se pudo obtener la sucursal por ciudad, revisar!", e);
        }
        return null;
    }
}
