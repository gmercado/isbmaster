package com.bisa.bus.servicios.ach.obj;

import com.bisa.bus.servicios.ach.model.RequestEntrante;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by gmercado on 25/10/2017.
 * Yrag Knup
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonRequestCicloBasic implements Serializable, RequestEntrante {

    private Integer attempt;
    private String clearinghouse;
    private Integer id;
    private String start_time;
    private String end_time;
    private String dayoff_start_time;
    private String dayoff_end_time;
    private String enabled;

    public JsonRequestCicloBasic() {
    }

    public JsonRequestCicloBasic(Integer attempt, String clearinghouse, Integer id, String start_time, String end_time,
                                 String dayoff_start_time, String dayoff_end_time, String enabled) {
        this.attempt = attempt;
        this.clearinghouse = clearinghouse;
        this.id = id;
        this.start_time = start_time;
        this.end_time = end_time;
        this.dayoff_start_time = dayoff_start_time;
        this.dayoff_end_time = dayoff_end_time;
        this.enabled = enabled;
    }

    @Override
    public Integer getAttempt() {
        return attempt;
    }

    public void setAttempt(Integer attempt) {
        this.attempt = attempt;
    }

    public String getClearinghouse() {
        return clearinghouse;
    }

    public void setClearinghouse(String clearinghouse) {
        this.clearinghouse = clearinghouse;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getDayoff_start_time() {
        return dayoff_start_time;
    }

    public void setDayoff_start_time(String dayoff_start_time) {
        this.dayoff_start_time = dayoff_start_time;
    }

    public String getDayoff_end_time() {
        return dayoff_end_time;
    }

    public void setDayoff_end_time(String dayoff_end_time) {
        this.dayoff_end_time = dayoff_end_time;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "JsonRequestCicloBasic{" +
                "attempt=" + attempt +
                ", clearinghouse='" + clearinghouse + '\'' +
                ", id='" + id + '\'' +
                ", start_time='" + start_time + '\'' +
                ", end_time='" + end_time + '\'' +
                ", dayoff_start_time='" + dayoff_start_time + '\'' +
                ", dayoff_end_time='" + dayoff_end_time + '\'' +
                ", enabled=" + enabled +
                '}';
    }
}
