package com.bisa.bus.servicios.ach.dao;

import bus.consumoweb.yellowpepper.dao.YellowPepperServicioDao;
import com.bisa.bus.servicios.ach.utiles.RespuestaUtil;
import com.bisa.bus.servicios.asfi.infocred.ui.ReSendMailWrapper;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.bisa.bus.servicios.ach.utiles.DatosACH.*;

/**
 * Created by gmercado on 21/11/2017.
 */
public class AchpNotificarClienteDao implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(AchpNotificarClienteDao.class);

    private static Map<String, Object> params;
    private final ReSendMailWrapper mailWrapper;
    private YellowPepperServicioDao yellowPepperServicioDao;

    private String MENSAJE = "Banco Bisa: Transferencia a otro banco $ESTADO en banco destino por $MONEDA $IMPORTE";
    private static final int PRIORIDAD_DEFAULT = 1;

    public AchpNotificarClienteDao(YellowPepperServicioDao yellowPepperServicioDao, ReSendMailWrapper mailWrapper, Map<String, Object> params) {
        this.params = params;
        this.mailWrapper = mailWrapper;
        this.yellowPepperServicioDao = yellowPepperServicioDao;
    }

    @Override
    public void run() {
        try {
            String email = RespuestaUtil.validaString(params.get(EMAIL));
            String orden = RespuestaUtil.validaString(params.get(ORDEN));
            String moneda = RespuestaUtil.validaString(params.get(MONEDA));
            String estado = RespuestaUtil.validaString(params.get(ESTADO));
            String celular = RespuestaUtil.validaString(params.get(CELULAR));
            String destinatario = RespuestaUtil.validaString(params.get(DESTINATARIO));
            BigDecimal importe = (BigDecimal) params.get(IMPORTE);
            BigDecimal importeSMS = (BigDecimal) params.get(IMPORTESMS);

            if (!Strings.isNullOrEmpty(email)) {
                LOGGER.info("Mandar correo al cliente");
                List<String> correos = new ArrayList<>();
                correos.add(email);
                new MailNotifierACH(mailWrapper).notify(orden, importe.toPlainString(), destinatario, moneda, estado, correos);
            }
            if (!Strings.isNullOrEmpty(celular) && importe.compareTo(importeSMS) >= 0) {
                LOGGER.info("Enviando SMS al {} por orden ACH de importe: {} ");
                yellowPepperServicioDao.sendNotificacionYellowPepper(celular,
                        TITULO, getMENSAJE(), "", "", APLICACION_SMS, SERVICIO_SJ, PRIORIDAD_DEFAULT);
            }
        } catch (Exception e) {
            LOGGER.info("No se pudo notificar al cliente del estado final de su transaccion ", e);
        }
    }

    private String getMENSAJE() {
        for (String key : params.keySet()) {
            MENSAJE = MENSAJE.replace("$" + key, toText(params.get(key)));
        }
        return MENSAJE;
    }

    private static String toText(Object object) {
        if (object == null)
            return "";
        if (BigDecimal.class.isAssignableFrom(object.getClass()))
            return ((BigDecimal) object).toPlainString();

        return String.valueOf(object);
    }

    public static class MailNotifierACH {

        private ReSendMailWrapper mailWrapper;
        private static String imagen = "bisalogopeque.png";

        public MailNotifierACH(ReSendMailWrapper mailWrapper) {
            this.mailWrapper = mailWrapper;
        }

        public void notify(String orden, String importe, String destinatario, String moneda, String estado, List<String> destinatarios) {
            String logoPath = (AchpNotificarClienteDao.class.getResource("/images/")).getPath().concat(imagen);
            mailWrapper.sendHtmlMessage("Transferencia a otros bancos", MailNotifierACH.getBodyMailACH(orden, importe, destinatario, moneda, estado), null/*logoPath*/, destinatarios);
        }

        public static final String getBodyMailACH(String orden, String importe, String destinatario, String moneda, String estado) {
            String mensaje = "Estimado Cliente: su transferencia a otro Banco por medio del Banco BISA,";
            mensaje += "<br>fue " + estado + " por el Banco Destino";
            mensaje += "<br><b>N&uacute;mero de transacci&oacute;n:</b> " + orden;
            mensaje += "<br><b>Importe:</b> " + moneda + " " + importe + ".";
            //mensaje += "<br><b>Destinatario:</b> " + destinatario;

            String cuerpo =
                    "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"> \n" +
                            "<table border=\"1\" style=\"border-style: dotted\" bordercolor=\"#003B79\" width=\"500\" cellspacing=\"2\" cellpadding=\"2\">\n" +
                            "<tbody>\n" +
                            "<tr bgcolor=\"#003B79\">\n" +
                            "<td align=\"center\" valign=\"middle\" nowrap=\"nowrap\" width=\"100%\">\n" +
                            "<span style=\"color: #FFFFFF; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: small;\"> Transferencia a otros bancos </span></td>\n" +
                            "</tr>\n" +
                            "<tr>\n" +
                            "<td style=\"background: #FFFFFF; font: 'Lucida Console'; font-size: 14px;\" align=\"left\" valign=\"middle\" nowrap=\"nowrap\" width=\"100%\">\n" +
                            mensaje +
                            "</td>\n" +
                            "</tr>\n" +
                            "</tbody>\n" +
                            "</table>" +
                            "<br/>";
//                            "<img src=\"cid:image\" alt=\"Banco Bisa S.A.\" />";
            return cuerpo;
        }
    }
}