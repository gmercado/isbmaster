package com.bisa.bus.servicios.bisa.ui;


import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.interfaces.ebisa.dao.EstadisticasEbisaDao;
import bus.interfaces.ebisa.entities.EstadisticasEbisa;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

//@AuthorizeInstantiation({RolesBisa.MESA_AYUDA})
//@Menu(value = "Cargar Contratos", subMenu = SubMenu.MESA_AYUDA)
public class CargaMasiva extends Listado<EstadisticasEbisa, String> {

    private final IModel<HashSet<EstadisticasEbisa>> seleccionados;

    public CargaMasiva(){
        super();
        seleccionados = new Model<>(new HashSet<>());
    }

    @Override
    protected ListadoPanel<EstadisticasEbisa, String> newListadoPanel(String id) {
        return new ListadoPanel<EstadisticasEbisa, String>(id) {
            @Override
            protected List<IColumn<EstadisticasEbisa, String>> newColumns(ListadoDataProvider<EstadisticasEbisa, String> dataProvider) {
                List<IColumn<EstadisticasEbisa, String>> columns = new LinkedList<>();
                columns.add(new LinkPropertyColumn<EstadisticasEbisa>(Model.of("Operacion"), "id", "id") {
                    @Override
                    protected void onClick(EstadisticasEbisa object) {
                        setResponsePage(new OperacionStadistica(object));
                    }
                });
                columns.add(new PropertyColumn<>(Model.of("Fecha Hora"), "fechaRegistro", "fecha"));
                columns.add(new PropertyColumn<>(Model.of("Cod. Cliente"), "codigoCliente"));
                columns.add(new PropertyColumn<>(Model.of("Num. Cuenta"), "numeroCuenta"));
                columns.add(new PropertyColumn<>(Model.of("Usuario"), "nombreUSuario"));

                return columns;
            }

            @Override
            protected Class<? extends Dao<EstadisticasEbisa, String>> getProviderClazz() {
                return EstadisticasEbisaDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<EstadisticasEbisa> newFiltroPanel(String id, IModel<EstadisticasEbisa> model1, IModel<EstadisticasEbisa> model2) {
                FiltroCargaContratos filtro = new FiltroCargaContratos(id, model1, model2);
                /*filtro.add(new Link("saveExcel") {
                @Override
                public void onClick() {
                    if (getModel1() == null || getModel1().getObject() == null) {
                        getSession().error("Hubo un error al exportar el archivo.");
                        return;
                    }
                    if (getModel1().getObject().getFecha() == null || getModel2().getObject().getFecha() == null) {
                        getSession().error("Las fechas son requeridas.");
                        return;
                    }
                    AbstractResourceStreamWriter resourceStreamWriter = new AbstractResourceStreamWriter() {
                        @Override
                        public void write(final OutputStream output) throws IOException {
                            BitacoraFacturaDao bitacoraFacturaDao = getInstance(BitacoraFacturaDao.class);
                            List<BitacoraFactura> consumos = bitacoraFacturaDao.getConsumos(getModel1().getObject(), getModel2().getObject());
                            byte[] reporte = ReporteConsumosPortal.get(consumos);
                            if (reporte != null) {
                                output.write(reporte);
                                output.flush();
                                output.close();
                            }
                        }
                    };

                    getRequestCycle().scheduleRequestHandlerAfterCurrent(new ResourceStreamRequestHandler(resourceStreamWriter)
                            .setFileName(ReporteConsumosPortal.NOMBRE_REPORTE + ReporteConsumosPortal.EXTENSION_REPORTE));
                }
            });*/
                return filtro;
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("fechaRegistro", false);
            }


            @Override
            protected EstadisticasEbisa newObject2() {
                return new EstadisticasEbisa();
            }

            @Override
            protected EstadisticasEbisa newObject1() {
                return new EstadisticasEbisa();
            }

            @Override
            protected boolean isVisibleData() {
                EstadisticasEbisa operacion1 = getModel1().getObject();
                EstadisticasEbisa operacion2 = getModel2().getObject();
                return !((operacion1.getFechaRegistro() == null || operacion2.getFechaRegistro() == null));
            }

        };
    }
}
