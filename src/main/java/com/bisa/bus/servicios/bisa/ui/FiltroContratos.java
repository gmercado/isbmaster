package com.bisa.bus.servicios.bisa.ui;

import bus.database.components.FiltroPanel;
import com.bisa.bus.servicios.bisa.model.Contratos;
import com.bisa.bus.servicios.bisa.model.TipoContrato;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.bisa.bus.servicios.mojix.model.EstadoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoProceso;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.validator.DateValidator;

import java.util.*;

class FiltroContratos extends FiltroPanel<Contratos> {

    private DateTextField fechaHasta;
    private DateTextField fechaDesde;

    FiltroContratos(String id, IModel<Contratos> model1, IModel<Contratos> model2) {
        super(id, model1, model2);
        final String datePattern = "dd/MM/yyyy";
        fechaDesde = new DateTextField("fechaDesde", new PropertyModel<>(model1, "fechaRegistro"), datePattern);
        fechaDesde.setRequired(true);
        fechaDesde.add(new DatePicker());

        fechaHasta = new DateTextField("fechaHasta", new PropertyModel<>(model2, "fechaRegistro"), datePattern);
        fechaHasta.setRequired(true);
        fechaHasta.add(new DatePicker());


        add(new TextField<>("codigoCliente", new PropertyModel<>(model1, "codigoCliente")));
        add(new TextField<>("numeroCuenta", new PropertyModel<>(model1, "numeroCuenta")));
        add(fechaDesde);
        add(fechaHasta);


        IValidator<Date> iValidator = new IValidator<Date>() {
            @Override
            public void validate(IValidatable<Date> validatable) {
                Date fi=getFechaDesde().getConvertedInput();
                Date f2=getFechaHasta().getConvertedInput();
                if(fi==null){
                    error("El campo 'fechaDesde' es obligatorio.");
                    return;
                }
                Calendar c =Calendar.getInstance();
                c.setTime(fi);
                int yearSystem=c.get(Calendar.YEAR);
                if(yearSystem<1753){
                    error("Fecha Invalida.");
                }
                if(fi.compareTo(f2)>0){
                    error("Rango de Fechas Invalido.");
                }
            }
        };

        fechaHasta.add(iValidator);
        //fechaHasta.add(iValidatorFD);



        //add(validator);
        //add(validator);
        //Tipo Proceso
        add(new DropDownChoice<>("proceso", new PropertyModel<>(model1, "tipoContrato"),
                new LoadableDetachableModel<List<? extends TipoContrato>>() {
                    @Override
                    protected List<? extends TipoContrato> load() {
                        return new ArrayList<>(Arrays.asList((TipoContrato.values())));
                    }
                }, new IChoiceRenderer<TipoContrato>() {
            @Override
            public Object getDisplayValue(TipoContrato object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(TipoContrato object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoContrato getObject(String id, IModel<? extends List<? extends TipoContrato>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));

        //Tipo Operacion
       /* add(new DropDownChoice<>("tipoOperacion", new PropertyModel<>(model1, "tipoOperacion"),
                new LoadableDetachableModel<List<? extends TipoContrato>>() {
                    @Override
                    protected List<? extends TipoContrato> load() {
                        return new ArrayList<>(Arrays.asList((TipoContrato.values())));
                    }
                }, new IChoiceRenderer<TipoContrato>() {
            @Override
            public Object getDisplayValue(TipoContrato object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(TipoContrato object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoContrato getObject(String id, IModel<? extends List<? extends TipoContrato>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));*/
        //Estado
        /*add(new DropDownChoice<>("estadoOperacion", new PropertyModel<>(model1, "estadoOperacion"),
                new LoadableDetachableModel<List<? extends EstadoOperacion>>() {
                    @Override
                    protected List<? extends EstadoOperacion> load() {
                        return new ArrayList<>(Arrays.asList((EstadoOperacion.values())));
                    }
                }, new IChoiceRenderer<EstadoOperacion>() {
            @Override
            public Object getDisplayValue(EstadoOperacion object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(EstadoOperacion object, int index) {
                return Integer.toString(index);
            }

            @Override
            public EstadoOperacion getObject(String id, IModel<? extends List<? extends EstadoOperacion>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));*/
    }

    public DateTextField getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(DateTextField fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public DateTextField getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(DateTextField fechaDesde) {
        this.fechaDesde = fechaDesde;
    }
}

