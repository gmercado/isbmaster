package com.bisa.bus.servicios.bisa.model;

import bus.plumbing.utils.FormatosUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;


@Entity
@Table(name = "contract_log_web")
public class Contratos  implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "RqUID", length = 18, precision = 0)
    private BigDecimal id;

    @Column(name = "CustPermID", length = 20)
    private String  codigoCliente;

    @Column(name = "AccountFrom")
    private String numeroCuenta;

    @Column(name = "RqDate")
    private Date fechaRegistro;

    @Enumerated(EnumType.STRING)
    @Column(name = "TypeContract")
    private TipoContrato tipoContrato;

    @Lob
    @Column(name = "ContractFile", length = 1337)
    private String detalleContrato;

    @Column(name = "CreationDate")
    private Date fechaCreacion;

    @Column(name = "CreationApp")
    private String usuarioCreador;

    @Column(name = "UpdateDate")
    private Date fechaModificacion;

    @Column(name = "UpdateApp")
    private String usuarioModificador;

    @Column(name = "contractStatus")
    private String estadoContrato;

    @Lob
    @Column(name = "ContractPFD")
    private byte[] archivoPDF;

    @Column(name = "rebuild")
    private String reconstruido;

    public String getEnlace(){
        return "Imprimir";
    }

    public String getVisor(){
        return "Visor";
    }
    public String getDescripcionContrato(){
        return TipoContrato.valueOf(tipoContrato.name()).getDescripcion();
    }

    public String getFecha(){
        return FormatosUtils.fechaHoraLargaFormateada(fechaRegistro);
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public TipoContrato getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(TipoContrato tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public String getDetalleContrato() {
        return detalleContrato;
    }

    public void setDetalleContrato(String detalleContrato) {
        this.detalleContrato = detalleContrato;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @Override
    public String toString() {
        return "Contratos{" +
                "id=" + id +
                ", codigoCliente='" + codigoCliente + '\'' +
                ", numeroCuenta='" + numeroCuenta + '\'' +
                ", fechaRegistro=" + fechaRegistro +
                ", tipoContrato='" + tipoContrato + '\'' +
                ", detalleContrato='" + detalleContrato + '\'' +
                ", fechaCreacion=" + fechaCreacion +
                ", usuarioCreador='" + usuarioCreador + '\'' +
                ", fechaModificacion=" + fechaModificacion +
                ", usuarioModificador='" + usuarioModificador + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contratos contratos = (Contratos) o;
        return Objects.equals(id, contratos.id) &&
                Objects.equals(codigoCliente, contratos.codigoCliente) &&
                Objects.equals(numeroCuenta, contratos.numeroCuenta) &&
                Objects.equals(fechaRegistro, contratos.fechaRegistro) &&
                Objects.equals(tipoContrato, contratos.tipoContrato) &&
                Objects.equals(detalleContrato, contratos.detalleContrato) &&
                Objects.equals(fechaCreacion, contratos.fechaCreacion) &&
                Objects.equals(usuarioCreador, contratos.usuarioCreador) &&
                Objects.equals(fechaModificacion, contratos.fechaModificacion) &&
                Objects.equals(usuarioModificador, contratos.usuarioModificador);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, codigoCliente, numeroCuenta, fechaRegistro, tipoContrato, detalleContrato, fechaCreacion, usuarioCreador, fechaModificacion, usuarioModificador);
    }

    public String getEstadoContrato() {
        return estadoContrato;
    }

    public void setEstadoContrato(String estadoContrato) {
        this.estadoContrato = estadoContrato;
    }

    public byte[] getArchivoPDF() {
        return archivoPDF;
    }

    public void setArchivoPDF(byte[] archivoPDF) {
        this.archivoPDF = archivoPDF;
    }

    public String getReconstruido() {
        return reconstruido;
    }

    public void setReconstruido(String reconstruido) {
        this.reconstruido = reconstruido;
    }
}
/*RqUID         numeric(18)    NOT NULL,
   RqDate        datetime       NOT NULL,
   CustPermID    char(20),
   CustLOginID   char(20),
   UserName      char(100),
   AccountFrom   char(200),
   ContractFile  varchar(max),
   UserID        varchar(20),
   CreationDate  datetime,
   CreationApp   varchar(50)    DEFAULT ('isb'),
   UpdateApp     varchar(50)*/