package com.bisa.bus.servicios.bisa.model;

public enum TipoCargaContrato {

    BOL("BOLETAS","bg-aperturaConfirmacion01.htm"),
    PRE("DESEMBOLSO","lvdesem02.htm");
    //DPF("DPF","");

    private String descripcion;

    private String filtro;

    TipoCargaContrato(String descripcion, String filtro) {
        this.setDescripcion(descripcion);
        this.setFiltro(filtro);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFiltro() {
        return filtro;
    }

    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }

    public static TipoCargaContrato getDetalle(String filtro){
        for (TipoCargaContrato tipoCargaContrato : TipoCargaContrato.values()) {
            if(filtro.trim().equalsIgnoreCase(tipoCargaContrato.getFiltro().trim())){
                return tipoCargaContrato;
            }
        }
        return null;
    }
}
