package com.bisa.bus.servicios.bisa.ui;

import bus.database.components.FiltroPanel;
import bus.interfaces.ebisa.entities.EstadisticasEbisa;
import com.bisa.bus.servicios.bisa.model.TipoCargaContrato;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

class FiltroCargaContratos extends FiltroPanel<EstadisticasEbisa> {

    FiltroCargaContratos(String id, IModel<EstadisticasEbisa> model1, IModel<EstadisticasEbisa> model2) {
        super(id, model1, model2);
        final String datePattern = "dd/MM/yyyy";
        DateTextField fechaDesde = new DateTextField("fechaDesde", new PropertyModel<>(model1, "fechaRegistro"), datePattern);
        fechaDesde.setRequired(true);
        fechaDesde.add(new DatePicker());

        DateTextField fechaHasta = new DateTextField("fechaHasta", new PropertyModel<>(model2, "fechaRegistro"), datePattern);
        fechaHasta.setRequired(true);
        fechaHasta.add(new DatePicker());

        add(new TextField<>("codigoCliente", new PropertyModel<>(model1, "codigoCliente")));
        add(new TextField<>("numeroCuenta", new PropertyModel<>(model1, "numeroCuenta")));
        add(fechaDesde);
        add(fechaHasta);
        IValidator<Date> iValidator = new IValidator<Date>() {
            @Override
            public void validate(IValidatable<Date> validatable) {
                Date fi=fechaDesde.getConvertedInput();
                Date f2=fechaHasta.getConvertedInput();
                if(fi.compareTo(f2)>0){
                    getSession().info("Rango de Fechas Invalido.");
                    return;
                }
            }
        };

        fechaHasta.add(iValidator);

        //Tipo Contrato
        add(new DropDownChoice<>("proceso", new PropertyModel<>(model1, "tipoCargaContrato"),
                new LoadableDetachableModel<List<? extends TipoCargaContrato>>() {
                    @Override
                    protected List<? extends TipoCargaContrato> load() {
                        return new ArrayList<>(Arrays.asList((TipoCargaContrato.values())));
                    }
                }, new IChoiceRenderer<TipoCargaContrato>() {
            @Override
            public Object getDisplayValue(TipoCargaContrato object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(TipoCargaContrato object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoCargaContrato getObject(String id, IModel<? extends List<? extends TipoCargaContrato>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }


        }).setNullValid(true).setOutputMarkupId(true));

        //Tipo Operacion
       /* add(new DropDownChoice<>("tipoOperacion", new PropertyModel<>(model1, "tipoOperacion"),
                new LoadableDetachableModel<List<? extends TipoContrato>>() {
                    @Override
                    protected List<? extends TipoContrato> load() {
                        return new ArrayList<>(Arrays.asList((TipoContrato.values())));
                    }
                }, new IChoiceRenderer<TipoContrato>() {
            @Override
            public Object getDisplayValue(TipoContrato object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(TipoContrato object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoContrato getObject(String id, IModel<? extends List<? extends TipoContrato>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));*/
        //Estado
        /*add(new DropDownChoice<>("estadoOperacion", new PropertyModel<>(model1, "estadoOperacion"),
                new LoadableDetachableModel<List<? extends EstadoOperacion>>() {
                    @Override
                    protected List<? extends EstadoOperacion> load() {
                        return new ArrayList<>(Arrays.asList((EstadoOperacion.values())));
                    }
                }, new IChoiceRenderer<EstadoOperacion>() {
            @Override
            public Object getDisplayValue(EstadoOperacion object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(EstadoOperacion object, int index) {
                return Integer.toString(index);
            }

            @Override
            public EstadoOperacion getObject(String id, IModel<? extends List<? extends EstadoOperacion>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));*/
    }
}

