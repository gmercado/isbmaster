package com.bisa.bus.servicios.bisa.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;
import bus.plumbing.file.FileTemp;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.bisa.api.ReporteContrato;
import com.bisa.bus.servicios.bisa.dao.ContratosAuditoriaDao;
import com.bisa.bus.servicios.bisa.dao.ContratosDao;
import com.bisa.bus.servicios.bisa.model.Contratos;
import com.bisa.bus.servicios.bisa.model.ContratosAuditoria;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler;
import org.apache.wicket.util.resource.AbstractResourceStreamWriter;
import org.apache.wicket.util.resource.IResourceStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static bus.users.api.Metadatas.USER_META_DATA_KEY;


@AuthorizeInstantiation({RolesBisa.MESA_AYUDA})
@Menu(value = "Consulta Contratos", subMenu = SubMenu.MESA_AYUDA)
public class ProcesoContratos extends Listado<Contratos, BigDecimal> {

    private ContratosAuditoria auditoria;
    @Override
    protected ListadoPanel<Contratos, BigDecimal> newListadoPanel(String id) {
        return new ListadoPanel<Contratos, BigDecimal>(id) {
            @Override
            protected List<IColumn<Contratos, String>> newColumns(ListadoDataProvider<Contratos, BigDecimal> dataProvider) {
                List<IColumn<Contratos, String>> columns = new LinkedList<>();
                columns.add(new PropertyColumn<>(Model.of("ID"), "id", "id"));
                columns.add(new PropertyColumn<>(Model.of("Fecha Hora"), "fecha", "fecha"));
                columns.add(new PropertyColumn<>(Model.of("Cod. Cliente"), "codigoCliente"));
                columns.add(new PropertyColumn<>(Model.of("Num. Operaci\u00f3n"), "numeroCuenta"));
                columns.add(new PropertyColumn<>(Model.of("Tipo Operaci\u00f3n"), "descripcionContrato"));
                columns.add(new LinkPropertyColumn<Contratos>(new Model<String>("Ver Contrato"), "enlace") {
                    @Override
                    protected void onClick(Contratos object) {
                        byte [] pdfBytes;
                        FileOutputStream fos;
                        File temp = null;
                        String usr=getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase();
                        String strImpresion="COPIA IMPRESA POR "+usr+ " EN FECHA "+ FormatosUtils.formatoFechaHora(new Date());
                        if("N".equalsIgnoreCase(object.getReconstruido())){
                            pdfBytes= object.getArchivoPDF();
                            try {
                                PdfReader pdfReader=new PdfReader(pdfBytes);
                                BaseFont baseFont = BaseFont.createFont(
                                        BaseFont.TIMES_ROMAN,
                                        BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                                temp=FileTemp.create("contrato_final","pdf",pdfBytes);
                                fos=new FileOutputStream(temp);
                                PdfStamper  pdfStamper=new PdfStamper(pdfReader,fos);
                                int pages = pdfReader.getNumberOfPages();
                                for(int i=1; i<=pages; i++) {
                                    //Contain the pdf data.
                                    PdfContentByte pageContentByte =
                                            pdfStamper.getOverContent(i);

                                    pageContentByte.beginText();
                                    //Set text font and size.
                                    pageContentByte.setFontAndSize(baseFont, 5);

                                    pageContentByte.setTextMatrix(70, 55);

                                    //Write text
                                    pageContentByte.showText(strImpresion);
                                    pageContentByte.endText();
                                }

                                pdfStamper.close();
                                pdfBytes= FileUtils.readFileToByteArray(temp);
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (DocumentException e) {
                                e.printStackTrace();
                            }
                        }else {
                            pdfBytes = ReporteContrato.get(object.getDetalleContrato());
                        }

                        ContratosAuditoriaDao contratosAuditoriaDao=getInstance(ContratosAuditoriaDao.class);
                        auditoria=new ContratosAuditoria();
                        auditoria.setUsuario(usr);
                        auditoria.setFechaCreacion(new Date());
                        auditoria.setUsuarioCreador(usr);
                        auditoria.setTipoContrato(object.getTipoContrato());
                        contratosAuditoriaDao.persist(auditoria);
                        getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase();
                        byte[] finalPdfBytes = pdfBytes;
                        IResourceStream resourceStream = new AbstractResourceStreamWriter(){
                            private static final long serialVersionUID = 1L;
                            public void write(OutputStream output) {
                                try {
                                    output.write(finalPdfBytes);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }


                            public String getContentType() {
                                return "application/pdf";
                            }
                        };
                        getRequestCycle().scheduleRequestHandlerAfterCurrent(
                                new ResourceStreamRequestHandler(resourceStream)
                                        .setFileName("contrato.pdf"));
                    }
                });
                return columns;
            }

            @Override
            protected Class<? extends Dao<Contratos, BigDecimal>> getProviderClazz() {
                return ContratosDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<Contratos> newFiltroPanel(String id, IModel<Contratos> model1, IModel<Contratos> model2) {
                FiltroContratos filtro = new FiltroContratos(id, model1, model2);
                return filtro;
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("fechaRegistro", false);
            }


            @Override
            protected Contratos newObject2() {
                return new Contratos();
            }

            @Override
            protected Contratos newObject1() {
                return new Contratos();
            }

            @Override
            protected boolean isVisibleData() {
                Contratos operacion1 = getModel1().getObject();
                Contratos operacion2 = getModel2().getObject();
                return !((operacion1.getFechaRegistro() == null || operacion2.getFechaRegistro() == null));
            }

        };
    }
}

