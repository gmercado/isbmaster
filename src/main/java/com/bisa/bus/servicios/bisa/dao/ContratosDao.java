package com.bisa.bus.servicios.bisa.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BaseAuxiliar;
import bus.database.model.BaseContract;
import bus.interfaces.ebisa.entities.Tbl_Customer;
import com.bisa.bus.servicios.bisa.model.Contratos;
import com.bisa.bus.servicios.bisa.model.TipoContrato;
import com.bisa.bus.servicios.mojix.dao.OperacionDao;
import com.bisa.bus.servicios.mojix.entities.Operacion;
import com.bisa.bus.servicios.mojix.model.EstadoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoOperacion;
import com.bisa.bus.servicios.mojix.model.TipoProceso;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.*;

public class ContratosDao extends DaoImpl<Contratos, BigDecimal> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContratosDao.class);

    @Inject
    protected ContratosDao(@BaseContract EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<Contratos> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.get("id"));
        return paths;
    }

    @Override
    protected Path<BigDecimal> countPath(Root<Contratos> from) {
        return from.get("codigoCliente");
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<Contratos> from, Contratos example, Contratos example2) {

        List<Predicate> predicates = new LinkedList<>();
        Date fecha = example.getFechaRegistro();
        String codigoCliente = (example.getCodigoCliente() != null) ? StringUtils.trimToNull(example.getCodigoCliente()) : null;
        String numeroCuenta = example.getNumeroCuenta();
//        TipoContrato tipoContrato = Tipoexample.getTipoContrato();
        TipoContrato tipoContrato = example.getTipoContrato();
        if (fecha != null && example2.getFechaRegistro() != null) {
            Date inicio = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH);
            Date inicio2 = DateUtils.truncate(example2.getFechaRegistro(), Calendar.DAY_OF_MONTH);
            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
            LOGGER.debug("   FECHAS ENTRE [{}] A [{}]", inicio, inicio2);
            predicates.add(cb.between(from.get("fechaRegistro"), inicio, inicio2));
        }

        if (codigoCliente != null) {
            final String s = StringUtils.trimToEmpty(codigoCliente);
            LOGGER.debug(">>>>> codigoCliente=" + s);
            predicates.add(cb.equal(from.get("codigoCliente"), s));
        }

        if (tipoContrato != null) {
            LOGGER.debug(">>>>> tipoContrato =" + tipoContrato);
            predicates.add(cb.equal(from.get("tipoContrato"), tipoContrato));
        }

        if (numeroCuenta!= null) {
            LOGGER.debug(">>>>> numeroCuenta =" + numeroCuenta);
            predicates.add(cb.equal(from.get("numeroCuenta"), numeroCuenta));
        }
        return predicates.toArray(new Predicate[predicates.size()]);
    }
}
