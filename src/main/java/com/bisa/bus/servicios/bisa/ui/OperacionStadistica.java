package com.bisa.bus.servicios.bisa.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.interfaces.ebisa.entities.EstadisticasEbisa;
import bus.monitor.dao.LogWebDao;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.bisa.model.Contratos;
import com.bisa.bus.servicios.bisa.model.EstadoContratos;
import com.bisa.bus.servicios.bisa.model.TipoCargaContrato;
import com.bisa.bus.servicios.bisa.model.TipoContrato;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.model.*;
import org.apache.wicket.validation.validator.StringValidator;

import java.sql.SQLException;
import java.util.Date;

import static bus.users.api.Metadatas.USER_META_DATA_KEY;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
@Titulo("Datos Operacion Estadisticas")
@AuthorizeInstantiation({RolesBisa.MESA_AYUDA})
public class OperacionStadistica extends BisaWebPage {

    public OperacionStadistica(EstadisticasEbisa pOperacion) {

        IModel<EstadisticasEbisa> userModel = Model.of(pOperacion);
        Form<EstadisticasEbisa> nuevoOperacion;
        add(nuevoOperacion = new Form<>("form-migracion", new CompoundPropertyModel<>(userModel)));

        nuevoOperacion.add(new RequiredTextField<String>("nombreUSuario").
                add(StringValidator.maximumLength(25)).
                setOutputMarkupId(true).setEnabled(false));

        nuevoOperacion.add(new RequiredTextField<String>("codigoCliente").
                add(StringValidator.maximumLength(20)).
                setOutputMarkupId(true).setEnabled(false));

        nuevoOperacion.add(new RequiredTextField<String>("id").
                add(StringValidator.maximumLength(100)).
                setOutputMarkupId(true).setEnabled(false));


        nuevoOperacion.add(new RequiredTextField<String>("fecha").
                add(StringValidator.maximumLength(100)).
                setOutputMarkupId(true).setEnabled(false));

        /*nuevoOperacion.add(new RequiredTextField<String>("descripcionTipoContrato").
                add(StringValidator.maximumLength(100)).
                setOutputMarkupId(true));*/

        nuevoOperacion.add(new Button("migrar", Model.of("Migrar")) {
            private static final long serialVersionUID = -7498462827371758458L;
            @Override
            public void onSubmit() {

                Contratos contrato=new Contratos();
                EstadisticasEbisa stadistica= (EstadisticasEbisa) getForm().getModelObject();
                contrato.setFechaCreacion(new Date());
                contrato.setDetalleContrato(stadistica.getDetalleHtml());
                TipoCargaContrato tipoCargaContrato=TipoCargaContrato.getDetalle(stadistica.getPagina().trim());
                if(tipoCargaContrato==null){
                    getSession().info("No corresponde a un contrato valido.");
                    return;
                }
                contrato.setTipoContrato(TipoContrato.valueOf(tipoCargaContrato.name()));
//                contrato.setId(new BigDecimal(stadistica.getId()));
                LogWebDao  logWebDao=getInstance(LogWebDao.class);

                contrato.setCodigoCliente(stadistica.getCodigoCliente());
                contrato.setNumeroCuenta(stadistica.getNumeroCuenta());
                contrato.setFechaRegistro(stadistica.getFechaRegistro());
                contrato.setUsuarioCreador(getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase());
                contrato.setEstadoContrato(EstadoContratos.ACT.name());
                //TODO cambiar un tipo de dato
                contrato.setReconstruido("B");
                try {
                    String status=logWebDao.registTintLogWeb(contrato);
                    if("success".equalsIgnoreCase(status)){
                        getSession().info("Proceso de migraci\u00f3n se realizo satisfactoriamente");
                        setResponsePage(CargaMasiva.class);
                    }else {
                        getSession().info("No se realizo la migraci\u00f3n, vuelva a intentar.");
                        setResponsePage(CargaMasiva.class);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    return;
                }
//                usuarioHabilitadoDao.persist(modelObject);

            }
        });


        nuevoOperacion.add(new Button("cancelar", Model.of("Cancelar")) {
            private static final long serialVersionUID = 1L;
            @Override
            public void onSubmit() {
                setResponsePage(CargaMasiva.class);
                getSession().info("Operaci\u00f3n cancelada, No se migrar la informaci\u00f3n.");
            }
        }.setDefaultFormProcessing(false));

    }
}
