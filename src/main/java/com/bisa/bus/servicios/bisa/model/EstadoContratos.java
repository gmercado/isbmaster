package com.bisa.bus.servicios.bisa.model;


public enum EstadoContratos {
    ACT("ACTIVO"),
    PRC("PROCESADO"),
    ERR("ERROR"),
    INA("INACTIVO");

    private String descripcion;

    EstadoContratos(String descripcion) {
        setDescripcion(descripcion);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
