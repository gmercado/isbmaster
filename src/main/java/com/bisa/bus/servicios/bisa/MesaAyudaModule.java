package com.bisa.bus.servicios.bisa.ui;

import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.bus.servicios.mojix.sched.SchedOperacionesMojix;
import com.bisa.bus.servicios.mojix.services.*;

/**
 * @author by josanchez on 13/07/2016.
 */
public class MesaAyudaModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bindUI("com/bisa/bus/servicios/bisa/ui");
    }
}
