package com.bisa.bus.servicios.bisa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;


@Entity
@Table(name = "contract_audit_log")
public class ContratosAuditoria implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID", length = 18, precision = 0)
    private BigDecimal id;

    @Column(name = "UserID")
    private String usuario;

    @Column(name = "CreationDate")
    private Date fechaCreacion;

    @Column(name = "CreationApp")
    private String usuarioCreador;

    @Column(name = "UpdateDate")
    private Date fechaModificacion;

    @Column(name = "UpdateApp")
    private String usuarioModificador;

    @Enumerated(EnumType.STRING)
    @Column(name = "TypeContract")
    private TipoContrato tipoContrato;



    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContratosAuditoria that = (ContratosAuditoria) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(usuario, that.usuario) &&
                Objects.equals(fechaCreacion, that.fechaCreacion) &&
                Objects.equals(usuarioCreador, that.usuarioCreador) &&
                Objects.equals(fechaModificacion, that.fechaModificacion) &&
                Objects.equals(usuarioModificador, that.usuarioModificador);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, usuario, fechaCreacion, usuarioCreador, fechaModificacion, usuarioModificador);
    }

    public TipoContrato getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(TipoContrato tipoContrato) {
        this.tipoContrato = tipoContrato;
    }
}
