package com.bisa.bus.servicios.bisa.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BaseContract;
import com.bisa.bus.servicios.bisa.model.Contratos;
import com.bisa.bus.servicios.bisa.model.ContratosAuditoria;
import com.bisa.bus.servicios.bisa.model.TipoContrato;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.*;

public class ContratosAuditoriaDao extends DaoImpl<ContratosAuditoria, BigDecimal> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContratosAuditoriaDao.class);

    @Inject
    protected ContratosAuditoriaDao(@BaseContract EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<ContratosAuditoria> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.get("id"));
        return paths;
    }

    @Override
    protected Path<BigDecimal> countPath(Root<ContratosAuditoria> from) {
        return from.get("id");
    }
}
