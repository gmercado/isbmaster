package com.bisa.bus.servicios.bisa.model;

/**
 * @author by josanchez on 17/08/2016.
 */
public enum TipoContrato {

    BOL("BOLETAS"),
    PRE("DESEMBOLSO");

//    DPF("DPF");

    private String descripcion;

    TipoContrato(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


}
