package com.bisa.bus.servicios.bisa.api;

import net.sf.jasperreports.engine.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class ReporteContrato {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReporteContrato.class);

    private static final String LOGO_BANCO = "bancobisalogo.png";
    private static final String REP_EXTRACTO_JASPER = "rptContrato.jasper";

    public static byte[] get(String contenidoContrato) {
        byte[] output = null;
        JasperPrint jasperPrint;
        Map<String, Object> paramMap = new HashMap<>();
        String logoPath = (ReporteContrato.class.getResource("/images/")).getPath();
        String reportPath = (ReporteContrato.class.getResource("/reports/bisa/")).getPath();
        String subRepDir = reportPath;

        logoPath += LOGO_BANCO;
        reportPath += REP_EXTRACTO_JASPER;

        paramMap.put("SUBREPORT_DIR", subRepDir);
        paramMap.put("logo", logoPath);
        paramMap.put("titulo","CONTRATO");
        paramMap.put("concepto", contenidoContrato);
        try {
            jasperPrint = JasperFillManager.fillReport(reportPath, paramMap, new JREmptyDataSource());
            output = JasperExportManager.exportReportToPdf(jasperPrint);
        } catch (JRException e) {
            LOGGER.error("Error en la generacion del extracto:", e);
        }
        return output;
    }
}
