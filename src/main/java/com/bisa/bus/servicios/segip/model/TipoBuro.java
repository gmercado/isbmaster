package com.bisa.bus.servicios.segip.model;

/**
 * @author rsalvatierra on 04/04/2016.
 */
public enum TipoBuro {
    SEGIP("SEGIP");

    private String descripcion;

    TipoBuro(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return "TipoSolicitud{" +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }

}
