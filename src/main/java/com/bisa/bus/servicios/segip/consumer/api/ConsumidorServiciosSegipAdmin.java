package com.bisa.bus.servicios.segip.consumer.api;

import bus.config.dao.CryptUtils;
import bus.consumoweb.consumer.ClienteServiciosWeb;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import com.bisa.bus.servicios.segip.api.ParametrosSegipService;
import com.bisa.bus.servicios.segip.consumer.model.UsuarioFinalAltaResponse;
import com.bisa.bus.servicios.segip.consumer.model.UsuarioFinalHabilitaInhabilitaResponse;
import com.bisa.bus.servicios.segip.consumer.model.UsuarioFinalModificaResponse;
import com.google.inject.Inject;

import java.net.URL;
import java.util.concurrent.ExecutorService;

import static com.bisa.bus.servicios.segip.api.ConfiguracionSegip.*;

/**
 * @author rsalvatierra on 22/04/2016.
 */
public class ConsumidorServiciosSegipAdmin extends ClienteServiciosWeb {

    public static final String SEGIP_ADMIN = "segip-admin";
    public static final String AUX = "ns1";
    private final CryptUtils cryptUtils;
    private final ParametrosSegipService parametros;

    @Inject
    public ConsumidorServiciosSegipAdmin(@SQLFueraDeLinea ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente, CryptUtils cryptUtils, ParametrosSegipService parametros) {
        super(executor, consumoWebDao, medioAmbiente);
        this.cryptUtils = cryptUtils;
        this.parametros = parametros;
    }

    public int getCodigoInstitucion() {
        return parametros.getValorIntDe(SEGIP_CODIGO_INSTITUCION, SEGIP_CODIGO_INSTITUCION_DEFAULT);
    }

    public String getUsuario() {
        return parametros.getValorDe(SEGIP_USUARIO, SEGIP_USUARIO_DEFAULT);
    }

    public String getClave() {
        return cryptUtils.dec(parametros.getValorDe(SEGIP_CLAVE, SEGIP_CLAVE_DEFAULT));
    }

    @Override
    protected URL newURL() {
        return ConsumidorServiciosSegipAdmin.class.getResource("/com/bisa/bus/servicios/segip/consumer/api/ServicioExternoComun.wsdl");
    }

    @Override
    protected Class getResult() {
        switch (getType()) {
            case 1:
                return UsuarioFinalAltaResponse.class;
            case 2:
                return UsuarioFinalHabilitaInhabilitaResponse.class;
            case 3:
                return UsuarioFinalModificaResponse.class;
        }
        return null;
    }

    @Override
    protected String getPort() {
        return parametros.getValorDe(SEGIP_ADMIN_PORT_NAME, SEGIP_ADMIN_PORT_NAME_DEFAULT);
    }

    @Override
    protected String getEndPoint() {
        return parametros.getValorDe(SEGIP_ADMIN_URL, SEGIP_ADMIN_URL_DEFAULT);
    }

    @Override
    protected String getOperationName() {
        switch (getType()) {
            case 1:
                return parametros.getValorDe(SEGIP_ADMIN_OPERATION_NAME_NEW, SEGIP_ADMIN_OPERATION_NAME_NEW_DEFAULT);
            case 2:
                return parametros.getValorDe(SEGIP_ADMIN_OPERATION_NAME_HAB, SEGIP_ADMIN_OPERATION_NAME_HAB_DEFAULT);
            case 3:
                return parametros.getValorDe(SEGIP_ADMIN_OPERATION_NAME_MOD, SEGIP_ADMIN_OPERATION_NAME_MOD_DEFAULT);
        }
        return null;
    }

    @Override
    protected String getServiceName() {
        return parametros.getValorDe(SEGIP_ADMIN_SERVICE_NAME, SEGIP_ADMIN_SERVICE_NAME_DEFAULT);
    }

    @Override
    protected void setProxy() {
        String host = parametros.getValorDe(PROXY_HOST, PROXY_HOST_DEFAULT);
        int port = parametros.getValorIntDe(PROXY_PORT, PROXY_PORT_DEFAULT);
        if (host != null && port > 0) {
            System.setProperty(PROXY_HOST, host);
            System.setProperty(PROXY_PORT, String.valueOf(port));
        }
    }

    @Override
    protected String getId() {
        return SEGIP_ADMIN;
    }

    @Override
    protected String getNameSpace() {
        return parametros.getValorDe(SEGIP_NAMESPACE, SEGIP_NAMESPACE_DEFAULT);
    }
/*
    @Override
    public OMElement createMessage(Object o) {
        switch (getType()) {
            case 1:
                return getOmElementAlta(o);
            case 2:
                return getOmElementCambioEstado(o);
            case 3:
                return getOmElementModificacion(o);
        }
        return null;
    }

    private OMElement getOmElementAlta(Object o) {
        XmlRootElement xml = o.getClass().getAnnotation(XmlRootElement.class);
        UsuarioFinalAltaRequest request = (UsuarioFinalAltaRequest) o;
        OMFactory fac = OMAbstractFactory.getOMFactory();
        OMNamespace omNs = fac.createOMNamespace(xml.namespace(), AUX);
        OMElement method = fac.createOMElement(xml.name(), omNs);
        OMElement value = fac.createOMElement("pIdInstitucion", omNs);
        value.setText(String.valueOf(request.getpIdInstitucion()));
        method.addChild(value);
        value = fac.createOMElement("pUsuario", omNs);
        value.setText(request.getpUsuario());
        method.addChild(value);
        value = fac.createOMElement("pContrasenia", omNs);
        value.setText(request.getpContrasenia());
        method.addChild(value);
        value = fac.createOMElement("pNumeroCedula", omNs);
        value.setText(request.getpNumeroCedula());
        method.addChild(value);
        if (request.getpComplemento() != null) {
            value = fac.createOMElement("pComplemento", omNs);
            value.setText(request.getpComplemento());
            method.addChild(value);
        }
        if (request.getpNombre() != null) {
            value = fac.createOMElement("pNombre", omNs);
            value.setText(request.getpNombre());
            method.addChild(value);
        }
        if (request.getpPrimerApellido() != null) {
            value = fac.createOMElement("pPrimerApellido", omNs);
            value.setText(request.getpPrimerApellido());
            method.addChild(value);
        }
        if (request.getpSegundoApellido() != null) {
            value = fac.createOMElement("pSegundoApellido", omNs);
            value.setText(request.getpSegundoApellido());
            method.addChild(value);
        }
        if (request.getpFechaNacimiento() != null) {
            value = fac.createOMElement("pFechaNacimiento", omNs);
            value.setText(request.getpFechaNacimiento());
            method.addChild(value);
        }
        if (request.getpNombreInstitucion() != null) {
            value = fac.createOMElement("pNombreInstitucion", omNs);
            value.setText(request.getpNombreInstitucion());
            method.addChild(value);
        }
        if (request.getpNombreCargo() != null) {
            value = fac.createOMElement("pNombreCargo", omNs);
            value.setText(request.getpNombreCargo());
            method.addChild(value);
        }
        if (request.getpDepartamento() != null) {
            value = fac.createOMElement("pDepartamento", omNs);
            value.setText(request.getpDepartamento());
            method.addChild(value);
        }
        if (request.getpProvincia() != null) {
            value = fac.createOMElement("pProvincia", omNs);
            value.setText(request.getpProvincia());
            method.addChild(value);
        }
        if (request.getpSeccionMunicipal() != null) {
            value = fac.createOMElement("pSeccionMunicipal", omNs);
            value.setText(request.getpSeccionMunicipal());
            method.addChild(value);
        }
        if (request.getpLocalidad() != null) {
            value = fac.createOMElement("pLocalidad", omNs);
            value.setText(request.getpLocalidad());
            method.addChild(value);
        }
        if (request.getpOficina() != null) {
            value = fac.createOMElement("pOficina", omNs);
            value.setText(request.getpOficina());
            method.addChild(value);
        }
        if (request.getpCorreoElectronico() != null) {
            value = fac.createOMElement("pCorreoElectronico", omNs);
            value.setText(request.getpCorreoElectronico());
            method.addChild(value);
        }
        if (request.getpTelefono() != null) {
            value = fac.createOMElement("pTelefono", omNs);
            value.setText(request.getpTelefono());
            method.addChild(value);
        }
        if (request.getpTelefonoOficina() != null) {
            value = fac.createOMElement("pTelefonoOficina", omNs);
            value.setText(request.getpTelefonoOficina());
            method.addChild(value);
        }
        return method;
    }

    private OMElement getOmElementCambioEstado(Object o) {
        XmlRootElement xml = o.getClass().getAnnotation(XmlRootElement.class);
        UsuarioFinalHabilitaInhabilitaRequest request = (UsuarioFinalHabilitaInhabilitaRequest) o;
        OMFactory fac = OMAbstractFactory.getOMFactory();
        OMNamespace omNs = fac.createOMNamespace(xml.namespace(), AUX);
        OMElement method = fac.createOMElement(xml.name(), omNs);
        OMElement value = fac.createOMElement("pCodigoInstitucion", omNs);
        value.setText(String.valueOf(request.getpIdInstitucion()));
        method.addChild(value);
        value = fac.createOMElement("pUsuario", omNs);
        value.setText(request.getpUsuario());
        method.addChild(value);
        value = fac.createOMElement("pContrasenia", omNs);
        value.setText(request.getpContrasenia());
        method.addChild(value);
        value = fac.createOMElement("pClaveAcceso", omNs);
        value.setText(request.getpClaveAcceso());
        method.addChild(value);
        value = fac.createOMElement("pEstado", omNs);
        value.setText(String.valueOf(request.ispEstado()));
        method.addChild(value);
        return method;
    }

    private OMElement getOmElementModificacion(Object o) {
        XmlRootElement xml = o.getClass().getAnnotation(XmlRootElement.class);
        UsuarioFinalModificaRequest request = (UsuarioFinalModificaRequest) o;
        OMFactory fac = OMAbstractFactory.getOMFactory();
        OMNamespace omNs = fac.createOMNamespace(xml.namespace(), AUX);
        OMElement method = fac.createOMElement(xml.name(), omNs);
        OMElement value = fac.createOMElement("pCodigoInstitucion", omNs);
        value.setText(String.valueOf(request.getpIdInstitucion()));
        method.addChild(value);
        value = fac.createOMElement("pUsuario", omNs);
        value.setText(request.getpUsuario());
        method.addChild(value);
        value = fac.createOMElement("pContrasenia", omNs);
        value.setText(request.getpContrasenia());
        method.addChild(value);
        value = fac.createOMElement("pClaveAcceso", omNs);
        value.setText(request.getpClaveAcceso());
        method.addChild(value);
        if (request.getpNombreInstitucion() != null) {
            value = fac.createOMElement("pNombreInstitucion", omNs);
            value.setText(request.getpNombreInstitucion());
            method.addChild(value);
        }
        if (request.getpCargo() != null) {
            value = fac.createOMElement("pCargo", omNs);
            value.setText(request.getpCargo());
            method.addChild(value);
        }
        if (request.getpDepartamento() != null) {
            value = fac.createOMElement("pDepartamento", omNs);
            value.setText(request.getpDepartamento());
            method.addChild(value);
        }
        if (request.getpProvincia() != null) {
            value = fac.createOMElement("pProvincia", omNs);
            value.setText(request.getpProvincia());
            method.addChild(value);
        }
        if (request.getpSeccion() != null) {
            value = fac.createOMElement("pSeccion", omNs);
            value.setText(request.getpSeccion());
            method.addChild(value);
        }
        if (request.getpLocalidad() != null) {
            value = fac.createOMElement("pLocalidad", omNs);
            value.setText(request.getpLocalidad());
            method.addChild(value);
        }
        if (request.getpOficina() != null) {
            value = fac.createOMElement("pOficina", omNs);
            value.setText(request.getpOficina());
            method.addChild(value);
        }
        if (request.getpCorreoElectronico() != null) {
            value = fac.createOMElement("pCorreoElectronico", omNs);
            value.setText(request.getpCorreoElectronico());
            method.addChild(value);
        }
        if (request.getpTelefono() != null) {
            value = fac.createOMElement("pTelefono", omNs);
            value.setText(request.getpTelefono());
            method.addChild(value);
        }
        if (request.getpTelefonoOficina() != null) {
            value = fac.createOMElement("pTelefonoOficina", omNs);
            value.setText(request.getpTelefonoOficina());
            method.addChild(value);
        }

        return method;
    }
    */
}
