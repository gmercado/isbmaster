package com.bisa.bus.servicios.segip.entities;

import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;
import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author josanchez on 15/03/2016.
 * @author rsalvatierra modificado on 31/03/2016
 */
@Entity
@Table(name = "SGP020")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S20FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S20USRALT", nullable = false, columnDefinition = "CHAR(255) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S20FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S20USRMOD", columnDefinition = "CHAR(255)"))
})
public class PersonaSegip extends EntityBase implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S20CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "S20NROREV", columnDefinition = "NUMERIC(10)")
    @Version
    private Long version;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "S20FECCAR", columnDefinition = "TIMESTAMP")
    private Date fechaCarga;

    @NotNull
    @Column(name = "S20NRODOC", nullable = false, unique = true, columnDefinition = "CHAR(30)")
    private String numeroDocumento;

    @Column(name = "S20NUMCLI", columnDefinition = "CHAR(10)")
    private String codigoCliente;

    @Column(name = "S20ESVALI", columnDefinition = "CHAR(5)")
    private String consultaValida;

    @Column(name = "S20CODUNI", columnDefinition = "CHAR(50)")
    private String codigoUnico;

    @Column(name = "S20CODRES", columnDefinition = "CHAR(3)")
    private String codigoRespuesta;

    @Column(name = "S20DESRES", columnDefinition = "CHAR(300)")
    private String descripcionRespuesta;

    @Column(name = "S20TIPMEN", columnDefinition = "CHAR(20)")
    private String tipoMensaje;

    @Column(name = "S20MENSJE", columnDefinition = "CHAR(255)")
    private String mensajeTecnico;

    @Column(name = "S20NOMBRE", columnDefinition = "CHAR(60)")
    private String nombres;

    @Column(name = "S20PRIAPE", columnDefinition = "CHAR(30)")
    private String primerApellido;

    @Column(name = "S20SEGAPE", columnDefinition = "CHAR(30)")
    private String segundoApellido;

    @Column(name = "S20FECNAC", columnDefinition = "NUMERIC(8)")
    private Long fechaNacimiento;

    @Column(name = "S20DIRECC", columnDefinition = "CHAR(255)")
    private String direccion;

    @Column(name = "S20OCUPRF", columnDefinition = "CHAR(255)")
    private String ocupacionProfesion;

    @Column(name = "S20ESTCIV", columnDefinition = "CHAR(30)")
    private String estadoCivil;

    @Column(name = "S20NOMCOY", columnDefinition = "CHAR(255)")
    private String nombreConyugue;

    @Column(name = "S20PAISOR", columnDefinition = "CHAR(30)")
    private String pais;

    @Column(name = "S20LUNACD", columnDefinition = "CHAR(100)")
    private String departamento;

    @Column(name = "S20LUNACP", columnDefinition = "CHAR(100)")
    private String provincia;

    @Column(name = "S20LUNACL", columnDefinition = "CHAR(100)")
    private String localidad;

    @Column(name = "S20TIPNAL", columnDefinition = "CHAR(15)")
    private String tipoNacionalidad;

    @Column(name = "S20SYSTEM", columnDefinition = "CHAR(30)")
    private String sistema;

    @Column(name = "S20OBSERS", columnDefinition = "CHAR(255)")
    private String observacion;

    @Basic(fetch = FetchType.EAGER)
    @Column(name = "S20REPOSG", columnDefinition = "BLOB")
    @Lob
    private byte[] reporte;

    @Basic(fetch = FetchType.EAGER)
    @Column(name = "S20FOTOSG", columnDefinition = "BLOB")
    @Lob
    private byte[] fotoPersona;

    public PersonaSegip() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getCodigoCliente() {
        return StringUtils.trimToEmpty(codigoCliente);
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getPrimerApellido() {
        return StringUtils.trimToEmpty(primerApellido);
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return StringUtils.trimToEmpty(segundoApellido);
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getNombres() {
        return StringUtils.trimToEmpty(nombres);
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public Date getFechaNacimiento() {
        return FormatosUtils.fechaYYYYMMDD(this.fechaNacimiento);
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = FormatosUtils.fechaYYYYMMDD(fechaNacimiento);
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getDireccion() {
        return StringUtils.trimToEmpty(direccion);
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getOcupacionProfesion() {
        return StringUtils.trimToEmpty(ocupacionProfesion);
    }

    public void setOcupacionProfesion(String ocupacionProfesion) {
        this.ocupacionProfesion = ocupacionProfesion;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getNombreConyugue() {
        return StringUtils.trimToEmpty(nombreConyugue);
    }

    public void setNombreConyugue(String nombreConyugue) {
        this.nombreConyugue = nombreConyugue;
    }

    public String getPais() {
        return StringUtils.trimToEmpty(pais);
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDepartamento() {
        return StringUtils.trimToEmpty(departamento);
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return StringUtils.trimToEmpty(provincia);
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getLocalidad() {
        return StringUtils.trimToEmpty(localidad);
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getTipoNacionalidad() {
        return StringUtils.trimToEmpty(tipoNacionalidad);
    }

    public void setTipoNacionalidad(String tipoNacionalidad) {
        this.tipoNacionalidad = tipoNacionalidad;
    }

    public String getSistema() {
        return StringUtils.trimToEmpty(sistema);
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public byte[] getReporte() {
        return reporte;
    }

    public void setReporte(byte[] reporte) {
        this.reporte = reporte;
    }

    public byte[] getFotoPersona() {
        return fotoPersona;
    }

    public void setFotoPersona(byte[] fotoPersona) {
        this.fotoPersona = fotoPersona;
    }

    public String getCodigoRespuesta() {
        return codigoRespuesta;
    }

    public void setCodigoRespuesta(String codigoRespuesta) {
        this.codigoRespuesta = codigoRespuesta;
    }

    public String getCodigoUnico() {
        return StringUtils.trimToEmpty(codigoUnico);
    }

    public void setCodigoUnico(String codigoUnico) {
        this.codigoUnico = codigoUnico;
    }

    public String getDescripcionRespuesta() {
        return StringUtils.trimToEmpty(descripcionRespuesta);
    }

    public void setDescripcionRespuesta(String descripcionRespuesta) {
        this.descripcionRespuesta = descripcionRespuesta;
    }

    public String getConsultaValida() {
        return StringUtils.trimToEmpty(consultaValida);
    }

    public void setConsultaValida(String consultaValida) {
        this.consultaValida = consultaValida;
    }

    public String getMensajeTecnico() {
        return StringUtils.trimToEmpty(mensajeTecnico);
    }

    public void setMensajeTecnico(String mensajeTecnico) {
        this.mensajeTecnico = mensajeTecnico;
    }

    public String getTipoMensaje() {
        return StringUtils.trimToEmpty(tipoMensaje);
    }

    public void setTipoMensaje(String tipoMensaje) {
        this.tipoMensaje = tipoMensaje;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(Date fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public String getFechaNacimientoFormato() {
        return FormatosUtils.formatoFecha(FormatosUtils.fechaYYYYMMDD(fechaNacimiento));
    }

    public String getFechaCreacionFormato() {
        return FormatosUtils.formatoFechaHora(fechaCreacion);
    }

    public String getObs() {
        return StringUtils.trimToEmpty(getObservacion()).toUpperCase();
    }

    @Override
    public String toString() {
        return "PersonaSegip{" +
                "id=" + id +
                ", numeroDocumento='" + StringUtils.trimToEmpty(getNumeroDocumento()) + '\'' +
                ", fechaCarga=" + getFechaCarga() +
                ", codigoCliente='" + StringUtils.trimToEmpty(getCodigoCliente()) + '\'' +
                ", primerApellido='" + StringUtils.trimToEmpty(getPrimerApellido()) + '\'' +
                ", segundoApellido='" + StringUtils.trimToEmpty(getSegundoApellido()) + '\'' +
                ", nombres='" + StringUtils.trimToEmpty(getNombres()) + '\'' +
                ", fechaNacimiento=" + getFechaNacimiento() +
                ", observacion='" + StringUtils.trimToEmpty(getObservacion()) + '\'' +
                ", direccion='" + StringUtils.trimToEmpty(getDireccion()) + '\'' +
                ", ocupacionProfesion='" + StringUtils.trimToEmpty(getOcupacionProfesion()) + '\'' +
                ", estadoCivil='" + StringUtils.trimToEmpty(getEstadoCivil()) + '\'' +
                ", nombreConyugue='" + StringUtils.trimToEmpty(getNombreConyugue()) + '\'' +
                ", pais='" + StringUtils.trimToEmpty(getPais()) + '\'' +
                ", departamento='" + StringUtils.trimToEmpty(getDepartamento()) + '\'' +
                ", provincia='" + StringUtils.trimToEmpty(getProvincia()) + '\'' +
                ", localidad='" + StringUtils.trimToEmpty(getLocalidad()) + '\'' +
                ", tipoNacionalidad='" + StringUtils.trimToEmpty(getTipoNacionalidad()) + '\'' +
                ", sistema='" + StringUtils.trimToEmpty(getSistema()) + '\'' +
                ", codigoRespuesta='" + StringUtils.trimToEmpty(getCodigoRespuesta()) + '\'' +
                ", codigoUnico='" + StringUtils.trimToEmpty(getCodigoUnico()) + '\'' +
                ", descripcionRespuesta='" + StringUtils.trimToEmpty(getDescripcionRespuesta()) + '\'' +
                ", consultaValida='" + StringUtils.trimToEmpty(getConsultaValida()) + '\'' +
                ", mensajeTecnico='" + StringUtils.trimToEmpty(getMensajeTecnico()) + '\'' +
                ", tipoMensaje='" + StringUtils.trimToEmpty(getTipoMensaje()) + '\'' +
                ", version=" + getVersion() +
                '}';
    }
}
