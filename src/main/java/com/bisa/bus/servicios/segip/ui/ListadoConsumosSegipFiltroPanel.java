package com.bisa.bus.servicios.segip.ui;

import bus.database.components.FiltroPanel;
import com.bisa.bus.servicios.segip.api.ParametrosSegipService;
import com.bisa.bus.servicios.segip.entities.BitacoraSegip;
import com.bisa.bus.servicios.segip.entities.Oficina;
import com.bisa.bus.servicios.segip.entities.VariableSegip;
import com.bisa.bus.servicios.segip.model.TipoRespuesta;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author rsalvatierra on 23/03/2016.
 */

public class ListadoConsumosSegipFiltroPanel extends FiltroPanel<BitacoraSegip> {
    @Inject
    ParametrosSegipService parametros;

    public ListadoConsumosSegipFiltroPanel(String id, IModel<BitacoraSegip> model1, IModel<BitacoraSegip> model2) {
        super(id, model1, model2);
        final String datePattern = "dd/MM/yyyy";
        DateTextField fechaDesde = new DateTextField("fechaDesde", new PropertyModel<>(model1, "fechaHoraConsulta"), datePattern);
        fechaDesde.setRequired(true);
        fechaDesde.add(new DatePicker());

        DateTextField fechaHasta = new DateTextField("fechaHasta", new PropertyModel<>(model2, "fechaHoraConsulta"), datePattern);
        fechaHasta.setRequired(true);
        fechaHasta.add(new DatePicker());

        add(new TextField<>("usuario", new PropertyModel<>(model1, "codigoUsuario")));
        add(fechaDesde);
        add(fechaHasta);
        add(new DropDownChoice<>("estado", new PropertyModel<>(model1, "tipoRespuestaEnum"),
                new LoadableDetachableModel<List<? extends TipoRespuesta>>() {
                    @Override
                    protected List<? extends TipoRespuesta> load() {
                        return new ArrayList<>(Arrays.asList((TipoRespuesta.values())));
                    }
                }, new IChoiceRenderer<TipoRespuesta>() {
            @Override
            public Object getDisplayValue(TipoRespuesta object) {
                return object.toString();
            }

            @Override
            public String getIdValue(TipoRespuesta object, int index) {
                return Integer.toString(index);
            }

            @Override
            public TipoRespuesta getObject(String id, IModel<? extends List<? extends TipoRespuesta>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));

        add(new DropDownChoice<>("canal", new PropertyModel<>(model1, "tipoCanal"),
                new LoadableDetachableModel<List<? extends VariableSegip>>() {
                    @Override
                    protected List<? extends VariableSegip> load() {
                        return parametros.getCanales();
                    }
                }, new IChoiceRenderer<VariableSegip>() {
            @Override
            public Object getDisplayValue(VariableSegip object) {
                return object.getNombreParte();
            }

            @Override
            public String getIdValue(VariableSegip object, int index) {
                return Integer.toString(index);
            }

            @Override
            public VariableSegip getObject(String id, IModel<? extends List<? extends VariableSegip>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));

        add(new DropDownChoice<>("oficina", new PropertyModel<>(model1, "oficina"),
                new LoadableDetachableModel<List<? extends Oficina>>() {
                    @Override
                    protected List<? extends Oficina> load() {
                        return parametros.getOficinas();
                    }
                }, new IChoiceRenderer<Oficina>() {
            @Override
            public Object getDisplayValue(Oficina object) {
                return object.getDescripcion();
            }

            @Override
            public String getIdValue(Oficina object, int index) {
                return Integer.toString(index);
            }

            @Override
            public Oficina getObject(String id, IModel<? extends List<? extends Oficina>> choices) {
                if (StringUtils.trimToNull(id) != null) return choices.getObject().get(Integer.parseInt(id));
                return null;
            }
        }).setNullValid(true).setOutputMarkupId(true));
    }
}
