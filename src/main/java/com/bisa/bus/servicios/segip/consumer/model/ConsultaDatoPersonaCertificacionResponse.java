package com.bisa.bus.servicios.segip.consumer.model;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author rsalvatierra on 04/05/2016.
 */
@XmlRootElement(name = "ConsultaDatoPersonaCertificacionResponse", namespace = "http://tempuri.org/")
public class ConsultaDatoPersonaCertificacionResponse implements Serializable {

    private RespuestaConsultaCertificacion consultaDatoPersonaCertificacionResult;

    public ConsultaDatoPersonaCertificacionResponse() {
    }

    public ConsultaDatoPersonaCertificacionResponse(RespuestaConsultaCertificacion consultaDatoPersonaCertificacionResult) {
        this.consultaDatoPersonaCertificacionResult = consultaDatoPersonaCertificacionResult;
    }

    @XmlElement(name = "ConsultaDatoPersonaCertificacionResult")
    public RespuestaConsultaCertificacion getConsultaDatoPersonaCertificacionResult() {
        return consultaDatoPersonaCertificacionResult;
    }

    public void setConsultaDatoPersonaCertificacionResult(RespuestaConsultaCertificacion consultaDatoPersonaCertificacionResult) {
        this.consultaDatoPersonaCertificacionResult = consultaDatoPersonaCertificacionResult;
    }

    @Override
    public String toString() {
        return "ConsultaDatoPersonaCertificacionResponse{" + consultaDatoPersonaCertificacionResult + '}';
    }
}
