package com.bisa.bus.servicios.segip.entities;

import org.apache.commons.lang.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author josanchez on 22/03/2016.
 * @author rsalvatierra modificado on 30/03/2016.
 */

@Entity
@Table(name = "CUP00301")
public class Cliente implements Serializable {

    @Id
    @Column(name = "CUNBR", columnDefinition = "CHAR(10)")
    private String id;

    @Column(name = "CUNTID", columnDefinition = "CHAR(20)")
    private String numeroDocumento;

    @Column(name = "CUSOFF", columnDefinition = "CHAR(5)")
    private String tipoDocumento;

    @Column(name = "CUNA1", columnDefinition = "CHAR(40)")
    private String descripCliente;

    @Column(name = "CUBDTE", columnDefinition = "NUMERIC(7)")
    private BigDecimal fechaNacimiento;

    @Column(name = "CUMRTS", columnDefinition = "CHAR(1)")
    private String estadoCivil;

    @Column(name = "CUCPRF", columnDefinition = "CHAR(3)")
    private String profesion;

    @Column(name = "CUCLPH", columnDefinition = "DECIMAL(13)")
    private String celular;

    @Column(name = "CUHMPH", columnDefinition = "DECIMAL(13)")
    private String telefono;

    @Column(name = "CUPERS", columnDefinition = "CHAR(1)")
    private String tipoPersona;

    @Column(name = "CUNA2", columnDefinition = "CHAR(40)")
    private String direccion1;

    @Column(name = "CUNA3", columnDefinition = "CHAR(40)")
    private String direccion2;

    @Column(name = "CUNA4", columnDefinition = "CHAR(40)")
    private String direccion3;

    @Column(name = "CUNA5", columnDefinition = "CHAR(40)")
    private String direccion4;

    @Column(name = "CUNA6", columnDefinition = "CHAR(40)")
    private String direccion5;

    @Column(name = "CUCTXN", columnDefinition = "CHAR(14)")
    private String nit;

    @Column(name = "CUNA80", columnDefinition = "CHAR(80)")
    private String nombreCompleto;

    public Cliente() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public BigDecimal getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(BigDecimal fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getDescripCliente() {
        return descripCliente;
    }

    public void setDescripCliente(String descripCliente) {
        this.descripCliente = descripCliente;
    }

    public String getDireccion1() {
        return direccion1;
    }

    public void setDireccion1(String direccion1) {
        this.direccion1 = direccion1;
    }

    public String getDireccion2() {
        return direccion2;
    }

    public void setDireccion2(String direccion2) {
        this.direccion2 = direccion2;
    }

    public String getDireccion3() {
        return direccion3;
    }

    public void setDireccion3(String direccion3) {
        this.direccion3 = direccion3;
    }

    public String getDireccion4() {
        return direccion4;
    }

    public void setDireccion4(String direccion4) {
        this.direccion4 = direccion4;
    }

    public String getDireccion5() {
        return direccion5;
    }

    public void setDireccion5(String direccion5) {
        this.direccion5 = direccion5;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id='" + getId() + '\'' +
                ", numeroDocumento='" + StringUtils.trimToEmpty(getNumeroDocumento()) + '\'' +
                ", tipoDocumento='" + StringUtils.trimToEmpty(getTipoDocumento()) + '\'' +
                ", descripCliente='" + StringUtils.trimToEmpty(getDescripCliente()) + '\'' +
                ", fechaNacimiento=" + getFechaNacimiento() +
                ", estadoCivil='" + StringUtils.trimToEmpty(getEstadoCivil()) + '\'' +
                ", profesion='" + StringUtils.trimToEmpty(getProfesion()) + '\'' +
                ", celular='" + StringUtils.trimToEmpty(getCelular()) + '\'' +
                ", telefono='" + StringUtils.trimToEmpty(getTelefono()) + '\'' +
                ", tipoPersona='" + StringUtils.trimToEmpty(getTipoPersona()) + '\'' +
                ", direccion1='" + StringUtils.trimToEmpty(getDireccion1()) + '\'' +
                ", direccion2='" + StringUtils.trimToEmpty(getDireccion2()) + '\'' +
                ", direccion3='" + StringUtils.trimToEmpty(getDireccion3()) + '\'' +
                ", direccion4='" + StringUtils.trimToEmpty(getDireccion4()) + '\'' +
                ", direccion5='" + StringUtils.trimToEmpty(getDireccion5()) + '\'' +
                '}';
    }
}
