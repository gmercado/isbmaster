//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.02.01 a las 04:08:40 PM BOT 
//


package com.bisa.bus.servicios.segip.consumer.stub;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.math.BigDecimal;
import java.math.BigInteger;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.bisa.stub.proxy.segip.stub package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _RespuestaConsultaCertificacion_QNAME = new QName("http://schemas.datacontract.org/2004/07/Convenio.Core.Modelo.Dto.Proceso.ServicioExterno", "RespuestaConsultaCertificacion");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _ResultadoExterno_QNAME = new QName("http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado", "ResultadoExterno");
    private final static QName _BaseResultado_QNAME = new QName("http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado", "BaseResultado");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _VerificacionDatoDocumentoCiePNumeroAutorizacion_QNAME = new QName("http://tempuri.org/", "pNumeroAutorizacion");
    private final static QName _VerificacionDatoDocumentoCiePFechaNacimiento_QNAME = new QName("http://tempuri.org/", "pFechaNacimiento");
    private final static QName _VerificacionDatoDocumentoCiePNombreUsuario_QNAME = new QName("http://tempuri.org/", "pNombreUsuario");
    private final static QName _VerificacionDatoDocumentoCiePComplemento_QNAME = new QName("http://tempuri.org/", "pComplemento");
    private final static QName _VerificacionDatoDocumentoCiePSegundoApellido_QNAME = new QName("http://tempuri.org/", "pSegundoApellido");
    private final static QName _VerificacionDatoDocumentoCiePNombres_QNAME = new QName("http://tempuri.org/", "pNombres");
    private final static QName _VerificacionDatoDocumentoCiePPrimerApellido_QNAME = new QName("http://tempuri.org/", "pPrimerApellido");
    private final static QName _VerificacionDatoDocumentoCiePContrasenia_QNAME = new QName("http://tempuri.org/", "pContrasenia");
    private final static QName _VerificacionDatoDocumentoCiePNumeroDocumento_QNAME = new QName("http://tempuri.org/", "pNumeroDocumento");
    private final static QName _VerificacionDatoDocumentoCiePClaveAccesoUsuarioFinal_QNAME = new QName("http://tempuri.org/", "pClaveAccesoUsuarioFinal");
    private final static QName _BaseResultadoMensaje_QNAME = new QName("http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado", "Mensaje");
    private final static QName _BaseResultadoTipoMensaje_QNAME = new QName("http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado", "TipoMensaje");
    private final static QName _ConsultaDatoPersonaCertificacionResponseConsultaDatoPersonaCertificacionResult_QNAME = new QName("http://tempuri.org/", "ConsultaDatoPersonaCertificacionResult");
    private final static QName _RespuestaConsultaCertificacionReporteCertificacion_QNAME = new QName("http://schemas.datacontract.org/2004/07/Convenio.Core.Modelo.Dto.Proceso.ServicioExterno", "ReporteCertificacion");
    private final static QName _ConsultaDatoPersonaContrastacionPUsuario_QNAME = new QName("http://tempuri.org/", "pUsuario");
    private final static QName _ConsultaDatoPersonaEnJsonPNombre_QNAME = new QName("http://tempuri.org/", "pNombre");
    private final static QName _ResultadoExternoCodigoUnico_QNAME = new QName("http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado", "CodigoUnico");
    private final static QName _ResultadoExternoDescripcionRespuesta_QNAME = new QName("http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado", "DescripcionRespuesta");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bisa.stub.proxy.segip.stub
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultaDatoPersonaCertificacionResponse }
     */
    public ConsultaDatoPersonaCertificacionResponse createConsultaDatoPersonaCertificacionResponse() {
        return new ConsultaDatoPersonaCertificacionResponse();
    }

    /**
     * Create an instance of {@link RespuestaConsultaCertificacion }
     */
    public RespuestaConsultaCertificacion createRespuestaConsultaCertificacion() {
        return new RespuestaConsultaCertificacion();
    }

    /**
     * Create an instance of {@link ConsultaDatoPersonaCertificacion }
     */
    public ConsultaDatoPersonaCertificacion createConsultaDatoPersonaCertificacion() {
        return new ConsultaDatoPersonaCertificacion();
    }

    /**
     * Create an instance of {@link BaseResultado }
     */
    public BaseResultado createBaseResultado() {
        return new BaseResultado();
    }

    /**
     * Create an instance of {@link ResultadoExterno }
     */
    public ResultadoExterno createResultadoExterno() {
        return new ResultadoExterno();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaConsultaCertificacion }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Convenio.Core.Modelo.Dto.Proceso.ServicioExterno", name = "RespuestaConsultaCertificacion")
    public JAXBElement<RespuestaConsultaCertificacion> createRespuestaConsultaCertificacion(RespuestaConsultaCertificacion value) {
        return new JAXBElement<RespuestaConsultaCertificacion>(_RespuestaConsultaCertificacion_QNAME, RespuestaConsultaCertificacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultadoExterno }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado", name = "ResultadoExterno")
    public JAXBElement<ResultadoExterno> createResultadoExterno(ResultadoExterno value) {
        return new JAXBElement<ResultadoExterno>(_ResultadoExterno_QNAME, ResultadoExterno.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseResultado }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado", name = "BaseResultado")
    public JAXBElement<BaseResultado> createBaseResultado(BaseResultado value) {
        return new JAXBElement<BaseResultado>(_BaseResultado_QNAME, BaseResultado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }


    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado", name = "Mensaje", scope = BaseResultado.class)
    public JAXBElement<String> createBaseResultadoMensaje(String value) {
        return new JAXBElement<String>(_BaseResultadoMensaje_QNAME, String.class, BaseResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado", name = "TipoMensaje", scope = BaseResultado.class)
    public JAXBElement<String> createBaseResultadoTipoMensaje(String value) {
        return new JAXBElement<String>(_BaseResultadoTipoMensaje_QNAME, String.class, BaseResultado.class, value);
    }


    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaConsultaCertificacion }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ConsultaDatoPersonaCertificacionResult", scope = ConsultaDatoPersonaCertificacionResponse.class)
    public JAXBElement<RespuestaConsultaCertificacion> createConsultaDatoPersonaCertificacionResponseConsultaDatoPersonaCertificacionResult(RespuestaConsultaCertificacion value) {
        return new JAXBElement<RespuestaConsultaCertificacion>(_ConsultaDatoPersonaCertificacionResponseConsultaDatoPersonaCertificacionResult_QNAME, RespuestaConsultaCertificacion.class, ConsultaDatoPersonaCertificacionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Convenio.Core.Modelo.Dto.Proceso.ServicioExterno", name = "ReporteCertificacion", scope = RespuestaConsultaCertificacion.class)
    public JAXBElement<byte[]> createRespuestaConsultaCertificacionReporteCertificacion(byte[] value) {
        return new JAXBElement<byte[]>(_RespuestaConsultaCertificacionReporteCertificacion_QNAME, byte[].class, RespuestaConsultaCertificacion.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado", name = "CodigoUnico", scope = ResultadoExterno.class)
    public JAXBElement<String> createResultadoExternoCodigoUnico(String value) {
        return new JAXBElement<String>(_ResultadoExternoCodigoUnico_QNAME, String.class, ResultadoExterno.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado", name = "DescripcionRespuesta", scope = ResultadoExterno.class)
    public JAXBElement<String> createResultadoExternoDescripcionRespuesta(String value) {
        return new JAXBElement<String>(_ResultadoExternoDescripcionRespuesta_QNAME, String.class, ResultadoExterno.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pNumeroAutorizacion", scope = ConsultaDatoPersonaCertificacion.class)
    public JAXBElement<String> createConsultaDatoPersonaCertificacionPNumeroAutorizacion(String value) {
        return new JAXBElement<String>(_VerificacionDatoDocumentoCiePNumeroAutorizacion_QNAME, String.class, ConsultaDatoPersonaCertificacion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pFechaNacimiento", scope = ConsultaDatoPersonaCertificacion.class)
    public JAXBElement<String> createConsultaDatoPersonaCertificacionPFechaNacimiento(String value) {
        return new JAXBElement<String>(_VerificacionDatoDocumentoCiePFechaNacimiento_QNAME, String.class, ConsultaDatoPersonaCertificacion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pComplemento", scope = ConsultaDatoPersonaCertificacion.class)
    public JAXBElement<String> createConsultaDatoPersonaCertificacionPComplemento(String value) {
        return new JAXBElement<String>(_VerificacionDatoDocumentoCiePComplemento_QNAME, String.class, ConsultaDatoPersonaCertificacion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pSegundoApellido", scope = ConsultaDatoPersonaCertificacion.class)
    public JAXBElement<String> createConsultaDatoPersonaCertificacionPSegundoApellido(String value) {
        return new JAXBElement<String>(_VerificacionDatoDocumentoCiePSegundoApellido_QNAME, String.class, ConsultaDatoPersonaCertificacion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pPrimerApellido", scope = ConsultaDatoPersonaCertificacion.class)
    public JAXBElement<String> createConsultaDatoPersonaCertificacionPPrimerApellido(String value) {
        return new JAXBElement<String>(_VerificacionDatoDocumentoCiePPrimerApellido_QNAME, String.class, ConsultaDatoPersonaCertificacion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pNombre", scope = ConsultaDatoPersonaCertificacion.class)
    public JAXBElement<String> createConsultaDatoPersonaCertificacionPNombre(String value) {
        return new JAXBElement<String>(_ConsultaDatoPersonaEnJsonPNombre_QNAME, String.class, ConsultaDatoPersonaCertificacion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pContrasenia", scope = ConsultaDatoPersonaCertificacion.class)
    public JAXBElement<String> createConsultaDatoPersonaCertificacionPContrasenia(String value) {
        return new JAXBElement<String>(_VerificacionDatoDocumentoCiePContrasenia_QNAME, String.class, ConsultaDatoPersonaCertificacion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pUsuario", scope = ConsultaDatoPersonaCertificacion.class)
    public JAXBElement<String> createConsultaDatoPersonaCertificacionPUsuario(String value) {
        return new JAXBElement<String>(_ConsultaDatoPersonaContrastacionPUsuario_QNAME, String.class, ConsultaDatoPersonaCertificacion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pNumeroDocumento", scope = ConsultaDatoPersonaCertificacion.class)
    public JAXBElement<String> createConsultaDatoPersonaCertificacionPNumeroDocumento(String value) {
        return new JAXBElement<String>(_VerificacionDatoDocumentoCiePNumeroDocumento_QNAME, String.class, ConsultaDatoPersonaCertificacion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pClaveAccesoUsuarioFinal", scope = ConsultaDatoPersonaCertificacion.class)
    public JAXBElement<String> createConsultaDatoPersonaCertificacionPClaveAccesoUsuarioFinal(String value) {
        return new JAXBElement<String>(_VerificacionDatoDocumentoCiePClaveAccesoUsuarioFinal_QNAME, String.class, ConsultaDatoPersonaCertificacion.class, value);
    }

}
