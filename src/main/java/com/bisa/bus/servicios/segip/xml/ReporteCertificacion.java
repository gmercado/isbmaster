package com.bisa.bus.servicios.segip.xml;

import com.bisa.bus.servicios.segip.model.TipoNacionalidad;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rsalvatierra on 09/03/2016.
 */
@XmlRootElement(name = "segip")
public class ReporteCertificacion {

    public static final int TIPO_FOTO = 12;
    private DatosCabecera datosCabecera;
    private DatosCiudadano datosCiudadano;
    private DatosCiudadano datosExtranjero;
    private DatosComplementarios datosComplementarios;
    private DatosFamiliares datosFamiliares;
    private DatosNacimiento datosNacimiento;
    private Imagenes imagenes;

    /**
     * @return the da
     */
    public DatosCabecera getDatosCabecera() {
        return datosCabecera;
    }

    /**
     * @param da the da to set
     */
    @XmlElement(name = "convenio_interinstitucional")
    public void setDatosCabecera(DatosCabecera da) {
        this.datosCabecera = da;
    }

    /**
     * @return the datosCiudadano
     */
    public DatosCiudadano getDatosCiudadano() {
        return datosCiudadano;
    }

    /**
     * @param datosCiudadano the datosCiudadano to set
     */
    @XmlElement(name = "informacion_del_ciudadano_nacional")
    public void setDatosCiudadano(DatosCiudadano datosCiudadano) {
        this.datosCiudadano = datosCiudadano;
        this.datosCiudadano.setTipoCiudadano(TipoNacionalidad.NACIONAL.getDescripcion());
    }

    /**
     * @return the datosCiudadano
     */
    public DatosCiudadano getDatosExtranjero() {
        return datosExtranjero;
    }

    /**
     * @param datosExtranjero the datosCiudadano to set
     */
    @XmlElement(name = "informacion_del_ciudadano_extranjero")
    public void setDatosExtranjero(DatosCiudadano datosExtranjero) {
        this.datosExtranjero = datosExtranjero;
        this.datosCiudadano = datosExtranjero;
        this.datosCiudadano.setTipoCiudadano(TipoNacionalidad.EXTRANJERO.getDescripcion());
    }

    /**
     * @return the datosComplementarios
     */
    public DatosComplementarios getDatosComplementarios() {
        return datosComplementarios;
    }

    /**
     * @param datosComplementarios the datosComplementarios to set
     */
    @XmlElement(name = "informacion_complementaria")
    public void setDatosComplementarios(DatosComplementarios datosComplementarios) {
        this.datosComplementarios = datosComplementarios;
    }

    /**
     * @return the datosFamiliares
     */
    public DatosFamiliares getDatosFamiliares() {
        return datosFamiliares;
    }

    /**
     * @param datosFamiliares the datosFamiliares to set
     */
    @XmlElement(name = "informacion_de_relacion_familiar")
    public void setDatosFamiliares(DatosFamiliares datosFamiliares) {
        this.datosFamiliares = datosFamiliares;
    }

    /**
     * @return the datosNacimiento
     */
    public DatosNacimiento getDatosNacimiento() {
        return datosNacimiento;
    }

    /**
     * @param datosNacimiento the datosNacimiento to set
     */
    @XmlElement(name = "lugar_de_nacimiento")
    public void setDatosNacimiento(DatosNacimiento datosNacimiento) {
        this.datosNacimiento = datosNacimiento;
    }

    /**
     * @return the imagenes
     */
    public Imagenes getImagenes() {
        return imagenes;
    }

    /**
     * @param imagenes the imagenes to set
     */
    @XmlElement(name = "Images")
    public void setImagenes(Imagenes imagenes) {
        this.imagenes = imagenes;
    }

    public byte[] getFoto() {
        byte[] foto = null;
        if (getImagenes() != null && getImagenes().getListImages() != null) {
            for (Imagen imagen : getImagenes().getListImages()) {
                if (imagen.getId() == TIPO_FOTO) {
                    foto = imagen.getImage();
                    break;
                }
            }
        }
        return foto;
    }

    @Override
    public String toString() {
        return "{" +
                "datosCabecera=" + datosCabecera +
                ", datosCiudadano=" + datosCiudadano +
                ", datosExtranjero=" + datosExtranjero +
                ", datosComplementarios=" + datosComplementarios +
                ", datosFamiliares=" + datosFamiliares +
                ", datosNacimiento=" + datosNacimiento +
                ", imagenes=" + imagenes +
                '}';
    }
}
