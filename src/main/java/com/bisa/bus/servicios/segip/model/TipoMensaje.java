package com.bisa.bus.servicios.segip.model;

/**
 * @author josanchez on 17/03/2016.
 * @author rsalvatierra modificado on 01/04/2016
 */
public enum TipoMensaje {
    OK("0", "Correcto"),
    NOK("1", "Incorrecto"),
    WARN("2", "Advertencia"),
    INF("3", "Informativo");

    private String codigo;
    private String descripcion;

    TipoMensaje(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }


}
