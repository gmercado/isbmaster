package com.bisa.bus.servicios.segip.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.segip.entities.Cliente;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.LinkedList;

/**
 * @author josanchez on 23/03/2016.
 * @author rsalvatierra modificado on 30/03/2016.
 */
public class ClienteDao extends DaoImpl<Cliente, Long> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClienteDao.class);

    @Inject
    protected ClienteDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public Cliente getCliente(String pNumeroDocumento) {
        LOGGER.debug("Obteniendo registro CUP003 por Numero Documento:{}.", pNumeroDocumento);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Cliente> q = cb.createQuery(Cliente.class);
                        Root<Cliente> p = q.from(Cliente.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("numeroDocumento"), StringUtils.trimToEmpty(pNumeroDocumento)));

                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Cliente> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                            return query.getResultList().get(0);
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public Cliente getClienteById(String pNumeroCliente) {
        LOGGER.debug("Obteniendo registro CUP003 por Numero Documento:{}.", pNumeroCliente);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Cliente> q = cb.createQuery(Cliente.class);
                        Root<Cliente> p = q.from(Cliente.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("id"), StringUtils.trimToEmpty(pNumeroCliente)));

                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Cliente> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                            return query.getResultList().get(0);
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public Cliente getClienteJuridico(String pNIT) {
        LOGGER.debug("Obteniendo registro CUP003 por NIT:{}.", pNIT);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Cliente> q = cb.createQuery(Cliente.class);
                        Root<Cliente> p = q.from(Cliente.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("nit"), StringUtils.trimToEmpty(pNIT)));

                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Cliente> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                            return query.getResultList().get(0);
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }
}