package com.bisa.bus.servicios.segip.model;

import java.io.Serializable;

/**
 * @author josanchez on 17/03/2016.
 * @author rsalvatierra modificado on 04/04/2016
 */
public class ResultadoExterno extends BaseResultado implements Serializable {

    private Integer codigoRespuestaBean;
    private String codigoUnicoBean;
    private String descripcionRespuestaBean;

    public ResultadoExterno() {
    }

    public Integer getCodigoRespuestaBean() {
        return codigoRespuestaBean;
    }

    public void setCodigoRespuestaBean(Integer codigoRespuestaBean) {
        this.codigoRespuestaBean = codigoRespuestaBean;
    }

    public String getCodigoUnicoBean() {
        return codigoUnicoBean;
    }

    public void setCodigoUnicoBean(String codigoUnicoBean) {
        this.codigoUnicoBean = codigoUnicoBean;
    }

    public String getDescripcionRespuestaBean() {
        return descripcionRespuestaBean;
    }

    public void setDescripcionRespuestaBean(String descripcionRespuestaBean) {
        this.descripcionRespuestaBean = descripcionRespuestaBean;
    }
}
