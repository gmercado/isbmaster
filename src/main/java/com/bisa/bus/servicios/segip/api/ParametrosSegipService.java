package com.bisa.bus.servicios.segip.api;

import com.bisa.bus.servicios.segip.entities.Oficina;
import com.bisa.bus.servicios.segip.entities.VariableSegip;

import java.util.List;

/**
 * @author rsalvatierra on 06/06/2016.
 */
public interface ParametrosSegipService {
    String getValorDe(String nombre, String valorPorDefecto);

    Integer getValorIntDe(String nombre, Integer valorPorDefecto);

    List<VariableSegip> getCanales();

    List<Oficina> getOficinas();
}
