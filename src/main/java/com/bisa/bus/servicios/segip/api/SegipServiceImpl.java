package com.bisa.bus.servicios.segip.api;

import com.bisa.bus.servicios.segip.consumer.api.SegipServicioWeb;
import com.bisa.bus.servicios.segip.consumer.model.ConsultaDatoPersonaCertificacionResponse;
import com.bisa.bus.servicios.segip.consumer.model.RespuestaConsultaCertificacion;
import com.bisa.bus.servicios.segip.entities.ContrastacionSegip;
import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import com.bisa.bus.servicios.segip.model.*;
import com.bisa.bus.servicios.segip.xml.ReporteCertificacion;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.bisa.bus.servicios.segip.api.ConfiguracionSegip.*;

/**
 * @author josanchez on 11/03/2016.
 * @author rsalvatierra modificado on 30/03/2016
 */
public class SegipServiceImpl implements SegipService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SegipServiceImpl.class);

    private final SegipProcessService procesoSegip;
    private final BitacoraSegipService bitacoraSegip;
    private final PersonaSegipService personaSegip;
    private final ContrastacionSegipService contrastacionSegipS;
    private final SegipServicioWeb wsSegip;

    @Inject
    public SegipServiceImpl(SegipProcessService procesoSegip, BitacoraSegipService bitacoraSegip, PersonaSegipService personaSegip, ContrastacionSegipService contrastacionSegipS, SegipServicioWeb wsSegip) {
        this.procesoSegip = procesoSegip;
        this.bitacoraSegip = bitacoraSegip;
        this.personaSegip = personaSegip;
        this.contrastacionSegipS = contrastacionSegipS;
        this.wsSegip = wsSegip;
    }

    @Override
    public ResultadoConsultaSegip consultasCertificacionSegip(final SolicitudConsultaSegip request) {
        PersonaSegip consultaPersonaSegip = null;
        ContrastacionSegip contrastacionSegip = null;
        SolicitudConsultaSegip consulta = null;
        Boolean consumeServicioSegip = true;
        ResultadoConsultaSegip resultadoConsultaSegip = null;
        try {
            LOGGER.info(">>>Inicio");
            consulta = setConsulta(request);
            //validaciones
            if (!debeConsumirServicio()) {
                //Validar consumo servicio
                resultadoConsultaSegip = obtenerError(TipoCodigoError.SERV_NO_HAB, MENSAJE_NO_CONSUMO);
            } else if (!canalHabilitado(consulta)) {
                //Validar canal
                resultadoConsultaSegip = obtenerError(TipoCodigoError.ERR_ORIGEN, MENSAJE_ERROR_CANAL);
            } else if (!tipoDocHabilitado(consulta)) {
                //Validar tipo documento
                resultadoConsultaSegip = obtenerError(TipoCodigoError.ERR_TIPODOC, MENSAJE_ERROR_TIPODOC);
            } else if (!formatoEsValido(consulta)) {
                //Validar formato
                resultadoConsultaSegip = obtenerError(TipoCodigoError.ERR_FORMATO, MENSAJE_ERROR_FORMATO);
            } else {
                //verificar si forza consulta
                if (!debeForzarConsultaSegip(consulta)) {
                    //Consulta SGP20
                    consultaPersonaSegip = existePersonaBDLocal(consulta);
                    if (consultaPersonaSegip != null) {
                        //Ya existe en BD SEGIP
                        //Verificar si es contrastado y esta vigente
                        if (registroContrastado(consultaPersonaSegip)) {
                            //Verifica fecha validez bd contrastacion
                            if (contrastacionVigente() && registroVigente(consulta, consultaPersonaSegip)) {
                                //En FECHA
                                //Traer datos del SGP20
                                consumeServicioSegip = false;
                                resultadoConsultaSegip = obtenerDatosSGP20(consulta, consultaPersonaSegip);
                            }
                        } else {
                            //Registro historico desde Segip
                            if (registroSegipVigente(consulta, consultaPersonaSegip)) {
                                //Traer datos del SGP20
                                consumeServicioSegip = false;
                                resultadoConsultaSegip = obtenerDatosSGP20(consulta, consultaPersonaSegip);
                            }
                        }
                    } else {
                        //No existe BD SEGIP
                        //Verifica fecha validez bd contrastacion y buscar en BD contrastacion
                        contrastacionSegip = existePersonaBDContrastacion(consulta);
                        if (contrastacionVigente() && contrastacionSegip != null) {
                            if (registroContrastadoVigente(consulta, contrastacionSegip)) {
                                //Traer datos contrastacion
                                consumeServicioSegip = false;
                                resultadoConsultaSegip = obtenerDatosContrastacion(consulta, contrastacionSegip);
                            }
                        }
                    }
                }
                //Consumir Servicio SEGIP
                if (consumeServicioSegip) {
                    resultadoConsultaSegip = consumirServicioSEGIP(consulta);
                }
                //Escribir IFS, solo para AS400
                if (escribeReporteIFS(consulta)) {
                    resultadoConsultaSegip = escribirIFS(consulta, resultadoConsultaSegip);
                }
            }
            LOGGER.info(">>>fin");
        } catch (Exception e) {
            LOGGER.error("Problemas en consultas SEGIP: {}", e);
            resultadoConsultaSegip = obtenerError(TipoCodigoError.ERR_INESPERADO, MENSAJE_ERROR_RUNTIME_A);
        } finally {
            try {
                registrarBitacora(consulta, resultadoConsultaSegip, consultaPersonaSegip);
            } catch (Exception e) {
                LOGGER.error("Error al registrar la bitacora del SEGIP: {} ", e);
            }
        }
        return resultadoConsultaSegip;
    }

    private boolean canalHabilitado(SolicitudConsultaSegip consulta) {
        return procesoSegip.canalValido(consulta);
    }

    private boolean tipoDocHabilitado(SolicitudConsultaSegip consulta) {
        return procesoSegip.tipoDocValido(consulta);
    }

    private boolean debeConsumirServicio() {
        return procesoSegip.consumeServicioSegip();
    }

    private SolicitudConsultaSegip setConsulta(final SolicitudConsultaSegip request) {
        String numDoc;
        String[] docs;
        SolicitudConsultaSegip consulta = request;
        LOGGER.debug("request {}", request);
        if (request.getCanal() != null
                && (TipoCanal.AS400.equals(request.getCanal())
                || TipoCanal.PCBISA.equals(request.getCanal())
                || TipoCanal.STI.equals(request.getCanal()))) {
            if (request.getNumeroDocumento() != null
                    && StringUtils.isNotBlank(request.getNumeroDocumento())
                    && StringUtils.isNotEmpty(request.getNumeroDocumento())) {
                numDoc = StringUtils.trimToEmpty(request.getNumeroDocumento().toUpperCase());
                //Nacional
                if (numDoc.contains(SEPARADOR_DOCUMENTO) && procesoSegip.getCarnetNacional().contains(consulta.getTipoDocumento())) {
                    docs = numDoc.split(SEPARADOR_DOCUMENTO);
                    if (docs[0] != null) {
                        consulta.setNumeroDocumento(docs[0]);
                    }
                    if (docs[1] != null) {
                        consulta.setComplemento(docs[1]);
                    }
                } else {
                    //Extranjero
                    if (numDoc.contains(SEPARADOR_DOCUMENTO) && procesoSegip.getCarnetExtranjero().contains(consulta.getTipoDocumento())) {
                        docs = numDoc.split(SEPARADOR_DOCUMENTO);
                        if (docs.length == 3) {
                            if (docs[0] != null) {
                                consulta.setNumeroDocumento(docs[0] + "-" + docs[1]);
                            }
                            if (docs[2] != null) {
                                consulta.setComplemento(docs[2]);
                            }
                        } else {
                            if (docs.length == 2
                                    && docs[0] != null && docs[1] != null
                                    && docs[0].length() > 1 && docs[1].length() < 3
                                    ) {
                                consulta.setNumeroDocumento(docs[0]);
                                consulta.setComplemento(docs[1]);
                            } else {
                                consulta.setNumeroDocumento(numDoc);
                                consulta.setComplemento(null);
                            }
                        }
                        //Otros tipos doc
                    } else {
                        consulta.setNumeroDocumento(numDoc);
                        consulta.setComplemento(null);
                    }
                }
            }
        }
        LOGGER.debug("consulta {}", consulta);
        return consulta;
    }

    private boolean formatoEsValido(SolicitudConsultaSegip consulta) {
        return procesoSegip.formatoValido(consulta);
    }

    private PersonaSegip existePersonaBDLocal(SolicitudConsultaSegip consulta) {
        return personaSegip.getPersona(consulta);
    }

    private boolean contrastacionVigente() {
        return procesoSegip.fechaContrastacionVigente();
    }

    private ContrastacionSegip existePersonaBDContrastacion(SolicitudConsultaSegip consulta) {
        ContrastacionSegip contrastacionSegip = contrastacionSegipS.getPersonaContrastacion(consulta.getNumeroDocumento(), consulta.getComplemento(), consulta.getFechaNacimiento());
        if (contrastacionSegip == null) {
            contrastacionSegip = contrastacionSegipS.getPersonaContrastacion2(consulta.getNumeroDocumento(), consulta.getComplemento(), consulta.getFechaNacimiento());
        }
        return contrastacionSegip;
    }

    private boolean registroSegipVigente(SolicitudConsultaSegip consulta, PersonaSegip consultaPersonaSegip) {
        return StringUtils.trimToNull(consultaPersonaSegip.getCodigoRespuesta()) != null
                && TipoCodigoRespuesta.NO_REALIZO_CONSULTA.getCodigo() != Integer.parseInt(StringUtils.trimToEmpty(consultaPersonaSegip.getCodigoRespuesta()))
                && procesoSegip.fechaBDlocalVigente()
                && registroVigente(consulta, consultaPersonaSegip);
    }

    private boolean registroContrastadoVigente(SolicitudConsultaSegip consulta, ContrastacionSegip contrastacionSegip) {
        return procesoSegip.plazoRetencionVigente(consulta, contrastacionSegip);
    }

    private boolean registroVigente(SolicitudConsultaSegip consulta, PersonaSegip consultaPersonaSegip) {
        return procesoSegip.plazoRetencionVigente(consulta, consultaPersonaSegip);
    }

    private boolean registroContrastado(PersonaSegip consultaPersonaSegip) {
        return SYSTEM_CONTRASTACION.equals(StringUtils.trimToEmpty(consultaPersonaSegip.getSistema()));
    }

    private boolean debeForzarConsultaSegip(SolicitudConsultaSegip consulta) {
        return consulta.isForzarConsulta();
    }

    private ResultadoConsultaSegip obtenerDatosSGP20(SolicitudConsultaSegip consulta, PersonaSegip consultaPersonaSegip) {
        SegipWebModel webModel = procesoSegip.getDatosPersona(consultaPersonaSegip);
        ContrastacionSegip contrastacionSegip = existePersonaBDContrastacion(consulta);
        if (SYSTEM_CONTRASTACION.equals(StringUtils.trimToEmpty(consultaPersonaSegip.getSistema()))
                && contrastacionSegip != null) {
            webModel.setReporte(procesoSegip.getReporteContrastacion(consulta.getNombreOficial(), contrastacionSegip));
        }
        ResultadoConsultaSegip resultadoConsultaSegip = new ResultadoConsultaSegip();
        resultadoConsultaSegip.setIdPersona(consultaPersonaSegip.getId());
        resultadoConsultaSegip.setCodError(procesoSegip.getCodError(consultaPersonaSegip.getCodigoRespuesta()));
        resultadoConsultaSegip.setEsValidoBean(true);
        resultadoConsultaSegip.setCodigoRespuestaBean(Integer.valueOf(StringUtils.trimToEmpty(consultaPersonaSegip.getCodigoRespuesta())));
        resultadoConsultaSegip.setCodigoUnicoBean(consultaPersonaSegip.getCodigoUnico());
        resultadoConsultaSegip.setTipoMensajeBean(consultaPersonaSegip.getTipoMensaje());
        resultadoConsultaSegip.setMensajeBean(consultaPersonaSegip.getMensajeTecnico());
        resultadoConsultaSegip.setDescripcionRespuestaBean(consultaPersonaSegip.getDescripcionRespuesta());
        resultadoConsultaSegip.setSegipWebModel(webModel);
        resultadoConsultaSegip.setTipoRespuesta(TipoRespuesta.CONSULTA);
        return resultadoConsultaSegip;
    }

    private ResultadoConsultaSegip obtenerDatosContrastacion(SolicitudConsultaSegip consulta, ContrastacionSegip contrastacionSegip) {
        ResultadoConsultaSegip resultadoConsultaSegip = new ResultadoConsultaSegip();
        try {
            SegipWebModel webModel = procesoSegip.getDatosPersona(contrastacionSegip);
            if (procesoSegip.generaReporteConstrastacion()) {
                webModel.setReporte(procesoSegip.getReporteContrastacion(consulta.getNombreOficial(), contrastacionSegip));
            }
            resultadoConsultaSegip.setEsValidoBean(true);
            resultadoConsultaSegip.setCodError(TipoCodigoError.OK);
            resultadoConsultaSegip.setCodigoRespuestaBean(TipoCodigoRespuesta.OK.getCodigo());
            resultadoConsultaSegip.setDescripcionRespuestaBean(procesoSegip.getMensaje(MENSAJE_REGISTRO_CONTRASTACION));
            resultadoConsultaSegip.setTipoMensajeBean(TipoMensaje.OK.getDescripcion());
            resultadoConsultaSegip.setMensajeBean(procesoSegip.getMensaje(MENSAJE_REGISTRO_EXISTOSA));
            resultadoConsultaSegip.setSegipWebModel(webModel);
            resultadoConsultaSegip.setTipoRespuesta(TipoRespuesta.CONTRASTADO);
            personaSegip.registrarPersonaBaseContrastacion(resultadoConsultaSegip, contrastacionSegip, consulta);
        } catch (Exception e) {
            LOGGER.error("Error al obtener datos CONTRASTACION : {}", e);
            resultadoConsultaSegip.setEsValidoBean(false);
            resultadoConsultaSegip.setCodError(TipoCodigoError.ERR_BD_CON);
            resultadoConsultaSegip.setCodigoRespuestaBean(TipoCodigoRespuesta.NO_REALIZO_CONSULTA.getCodigo());
            resultadoConsultaSegip.setDescripcionRespuestaBean(TipoCodigoRespuesta.NO_REALIZO_CONSULTA.getDescripcion());
            resultadoConsultaSegip.setTipoMensajeBean(TipoMensaje.WARN.getDescripcion());
            resultadoConsultaSegip.setMensajeBean(procesoSegip.getMensaje(MENSAJE_ERROR_BD_CONTRASTACION));
            resultadoConsultaSegip.setTipoRespuesta(TipoRespuesta.ERROR);
        }
        return resultadoConsultaSegip;
    }

    private ResultadoConsultaSegip consumirServicioSEGIP(SolicitudConsultaSegip consulta) {
        ResultadoConsultaSegip resultadoConsultaSegip;
        RespuestaConsultaCertificacion respuesta;
        ConsultaDatoPersonaCertificacionResponse response;
        ReporteCertificacion reporte = null;
        resultadoConsultaSegip = new ResultadoConsultaSegip();
        try {
            LOGGER.info(">>>Inicio CONSUMO");
            response = wsSegip.consumo(consulta);
            LOGGER.info(">>>fin CONSUMO");
            if (response != null) {
                respuesta = response.getConsultaDatoPersonaCertificacionResult();
            } else {
                resultadoConsultaSegip.setEsValidoBean(false);
                resultadoConsultaSegip.setCodError(TipoCodigoError.ERR_CON_SEGIP);
                resultadoConsultaSegip.setCodigoRespuestaBean(TipoCodigoRespuesta.NO_REALIZO_CONSULTA.getCodigo());
                resultadoConsultaSegip.setDescripcionRespuestaBean(TipoCodigoRespuesta.NO_REALIZO_CONSULTA.getDescripcion());
                resultadoConsultaSegip.setTipoMensajeBean(TipoMensaje.WARN.getDescripcion());
                resultadoConsultaSegip.setMensajeBean(procesoSegip.getMensaje(MENSAJE_ERROR_SEGIP));
                resultadoConsultaSegip.setTipoRespuesta(TipoRespuesta.ERROR);
                return resultadoConsultaSegip;
            }
        } catch (Exception e) {
            LOGGER.error("Error al consumir el servicio SEGIP : {}", e);
            resultadoConsultaSegip.setEsValidoBean(false);
            resultadoConsultaSegip.setCodError(TipoCodigoError.ERR_CON_SEGIP);
            resultadoConsultaSegip.setCodigoRespuestaBean(TipoCodigoRespuesta.NO_REALIZO_CONSULTA.getCodigo());
            resultadoConsultaSegip.setDescripcionRespuestaBean(TipoCodigoRespuesta.NO_REALIZO_CONSULTA.getDescripcion());
            resultadoConsultaSegip.setTipoMensajeBean(TipoMensaje.WARN.getDescripcion());
            resultadoConsultaSegip.setMensajeBean(procesoSegip.getMensaje(MENSAJE_ERROR_SEGIP));
            resultadoConsultaSegip.setTipoRespuesta(TipoRespuesta.ERROR);
            return resultadoConsultaSegip;
        }
        //Verificar si podemos parser reporte SEGIP
        if (procesoSegip.obtenerReporteSegip()) {
            reporte = procesoSegip.getReporteCertificacion(respuesta);
        }
        resultadoConsultaSegip.setCodError(procesoSegip.getCodError(respuesta.getCodigoRespuesta()));
        resultadoConsultaSegip.setEsValidoBean(respuesta.getEsValido());
        resultadoConsultaSegip.setCodigoUnicoBean(respuesta.getCodigoUnico());
        resultadoConsultaSegip.setTipoMensajeBean(respuesta.getTipoMensaje());
        resultadoConsultaSegip.setDescripcionRespuestaBean(respuesta.getDescripcionRespuesta());
        resultadoConsultaSegip.setCodigoRespuestaBean(respuesta.getCodigoRespuesta());
        resultadoConsultaSegip.setMensajeBean(respuesta.getMensaje());
        resultadoConsultaSegip.setTipoRespuesta(TipoRespuesta.CONSUMO);
        personaSegip.registrarPersonaCertificadaSegip(reporte, consulta, respuesta, resultadoConsultaSegip);
        resultadoConsultaSegip.setSegipWebModel(procesoSegip.getDatosPersona(personaSegip.getPersona(consulta)));
        return resultadoConsultaSegip;
    }

    private ResultadoConsultaSegip escribirIFS(SolicitudConsultaSegip consulta, ResultadoConsultaSegip response) {
        return procesoSegip.escribirIFS(consulta, response);
    }

    private boolean escribeReporteIFS(SolicitudConsultaSegip consulta) {
        return procesoSegip.escribeIFS(consulta);
    }

    private void registrarBitacora(SolicitudConsultaSegip consulta, ResultadoConsultaSegip resultadoConsultaSegip, PersonaSegip consultaPersonaSegip) {
        bitacoraSegip.registrar(consulta, resultadoConsultaSegip, consultaPersonaSegip);
    }

    private ResultadoConsultaSegip obtenerError(TipoCodigoError tipoError, String Mensaje) {
        ResultadoConsultaSegip resultadoConsultaSegip;
        resultadoConsultaSegip = new ResultadoConsultaSegip();
        resultadoConsultaSegip.setEsValidoBean(false);
        resultadoConsultaSegip.setCodError(tipoError);
        resultadoConsultaSegip.setCodigoRespuestaBean(TipoCodigoRespuesta.NO_REALIZO_CONSULTA.getCodigo());
        resultadoConsultaSegip.setDescripcionRespuestaBean(TipoCodigoRespuesta.NO_REALIZO_CONSULTA.getDescripcion());
        resultadoConsultaSegip.setTipoMensajeBean(TipoMensaje.WARN.getDescripcion());
        resultadoConsultaSegip.setMensajeBean(procesoSegip.getMensaje(Mensaje));
        resultadoConsultaSegip.setTipoRespuesta(TipoRespuesta.ERROR);
        return resultadoConsultaSegip;
    }
}
