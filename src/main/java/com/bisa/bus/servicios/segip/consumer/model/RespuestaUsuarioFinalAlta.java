package com.bisa.bus.servicios.segip.consumer.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author rsalvatierra on 05/05/2016.
 */
public class RespuestaUsuarioFinalAlta {
    private String claveAcceso;
    private Boolean esValido;
    private String mensaje;
    private String tipoMensaje;

    public RespuestaUsuarioFinalAlta() {
    }

    @XmlElement(name = "ClaveAcceso")
    public String getClaveAcceso() {
        return claveAcceso;
    }

    public void setClaveAcceso(String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }

    @XmlElement(name = "EsValido")
    public Boolean getEsValido() {
        return esValido;
    }

    public void setEsValido(Boolean esValido) {
        this.esValido = esValido;
    }

    @XmlElement(name = "Mensaje")
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @XmlElement(name = "TipoMensaje")
    public String getTipoMensaje() {
        return tipoMensaje;
    }

    public void setTipoMensaje(String tipoMensaje) {
        this.tipoMensaje = tipoMensaje;
    }
}
