package com.bisa.bus.servicios.segip.api;

import bus.monitor.as400.AS400Factory;
import bus.plumbing.file.ArchivoIFS;
import bus.plumbing.utils.Convert;
import bus.plumbing.utils.FechaOperator;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.TransferObject;
import com.bisa.bus.servicios.segip.consumer.model.RespuestaConsultaCertificacion;
import com.bisa.bus.servicios.segip.entities.*;
import com.bisa.bus.servicios.segip.model.*;
import com.bisa.bus.servicios.segip.xml.ReporteCertificacion;
import com.google.inject.Inject;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.ConnectionPoolException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Date;
import java.util.regex.Pattern;

import static com.bisa.bus.servicios.segip.api.ConfiguracionSegip.*;

/**
 * @author rsalvatierra on 01/04/2016.
 */
public class SegipProcessServiceImpl implements SegipProcessService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SegipProcessServiceImpl.class);

    @Inject
    private ParametrosSegipService parametros;

    @Inject
    private ObservacionesSegipService observacionesSegipService;

    @Inject
    private AS400Factory factory;

    @Inject
    private PersonaSegipService persona;

    @Override
    public PersonaSegipHist getDatosHistorico(PersonaSegip consultaPersonaSegip) {
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();
        return transferObject.convert(consultaPersonaSegip, PersonaSegipHist.class);
    }

    @Override
    public String getObservacion(ReporteCertificacion reporte, ResultadoConsultaSegip respuesta) {
        String observacion = null;
        //Observacion del reporte
        if (reporte != null && reporte.getDatosCiudadano() != null) {
            observacion = reporte.getDatosCiudadano().getObservaciones();
        }
        //Observacion general
        if (StringUtils.isEmpty(observacion) || StringUtils.isBlank(observacion)) {
            if (respuesta != null && respuesta.getCodigoRespuestaBean() != null) {
                switch (respuesta.getCodigoRespuestaBean()) {
                    case 0:
                        observacion = TipoCodigoError.NO_REALIZO_CONSULTA.getDescripcion();
                        break;
                    case 1:
                        observacion = TipoCodigoError.NO_EXISTE.getDescripcion();
                        break;
                    case 3:
                        observacion = TipoCodigoError.DUPLICADO.getDescripcion();
                        break;
                    case 4:
                        if (respuesta.getDescripcionRespuestaBean() != null) {
                            observacion = respuesta.getDescripcionRespuestaBean();
                        } else {
                            observacion = TipoCodigoError.OBSERVADO.getDescripcion();
                        }
                        break;
                    default:
                        observacion = TipoCodigoError.NO_REALIZO_CONSULTA.getDescripcion();
                        break;
                }
            }
        }
        observacion = StringUtils.trimToNull(observacion);
        if (observacion != null) {
            observacion = observacion.trim().toUpperCase();
        }
        return observacion;
    }

    @Override
    public SegipWebModel getDatosPersona(ContrastacionSegip contrastacionSegip) {
        SegipWebModel webModel = new SegipWebModel();
        webModel.setNumeroDocumento(contrastacionSegip.getDocumentoIdentidad());
        webModel.setNombres(contrastacionSegip.getNombres());
        webModel.setPrimerApellido(contrastacionSegip.getApellidoPaterno());
        webModel.setSegundoApellido(contrastacionSegip.getApellidoMaterno());
        webModel.setFechaNacimiento(contrastacionSegip.getFechaNacimiento());
        return webModel;
    }

    @Override
    public SegipWebModel getDatosPersona(PersonaSegip consultaPersonaSegip) {
        TransferObject transferObject = new TransferObject();
        transferObject.initializate();
        return transferObject.convert(consultaPersonaSegip, SegipWebModel.class);
    }

    @Override
    public TipoCodigoError getCodError(int codRespuesta) {
        TipoCodigoError codError;
        switch (codRespuesta) {
            case 0:
                codError = TipoCodigoError.NO_REALIZO_CONSULTA;
                break;
            case 1:
                codError = TipoCodigoError.NO_EXISTE;
                break;
            case 2:
                codError = TipoCodigoError.OK;
                break;
            case 3:
                codError = TipoCodigoError.DUPLICADO;
                break;
            case 4:
                codError = TipoCodigoError.OBSERVADO;
                break;
            case 5:
                codError = TipoCodigoError.SIN_NACIONALIDAD;
                break;
            default:
                codError = null;
                break;
        }
        return codError;
    }

    @Override
    public TipoCodigoError getCodError(String codRespuesta) {
        if (codRespuesta != null) {
            return getCodError(Integer.parseInt(StringUtils.trimToEmpty(codRespuesta)));
        }
        return null;
    }

    @Override
    public boolean fechaBDlocalVigente() {
        Date fechaActual = new Date();
        String parametroFechaVigente = parametros.getValorDe(FECHA_VALIDEZ_BASE_SEGIP, FECHA_VALIDEZ_BASE_SEGIP_DEFAULT);
        Date fechaVigente = FormatosUtils.getFechaByFormatoYYYY(parametroFechaVigente);
        return ((fechaVigente != null) && (fechaVigente.compareTo(fechaActual) == 1));
    }

    @Override
    public boolean fechaContrastacionVigente() {
        Date fechaActual = new Date();
        String parametroFechaVigente = parametros.getValorDe(FECHA_VALIDEZ_BASE_CONTRASTACION, FECHA_VALIDEZ_BASE_CONTRASTACION_DEFAULT);
        Date fechaVigente = FormatosUtils.getFechaByFormatoYYYY(parametroFechaVigente);
        return ((fechaVigente != null) && (fechaVigente.compareTo(fechaActual) == 1));
    }

    @Override
    public boolean generaReporteConstrastacion() {
        String parametroGeneraReporte = parametros.getValorDe(GENERA_REPORTE_CONTRASTACION, GENERA_REPORTE_CONTRASTACION_DEFAULT);
        return Boolean.valueOf(parametroGeneraReporte);
    }

    @Override
    public boolean escribeIFS(SolicitudConsultaSegip request) {
        String parametroUtilizaIFS = parametros.getValorDe(UTILIZAR_IFS, UTILIZAR_IFS_DEFAULT);
        return ((Boolean.valueOf(parametroUtilizaIFS)) && (TipoSolicitud.ALT_AS400.equals(request.getTipoSolicitud()) || TipoSolicitud.MOD_AS400.equals(request.getTipoSolicitud())));
    }

    @Override
    public boolean obtenerReporteSegip() {
        String parametroReporte = parametros.getValorDe(OBTENER_REPORTE_SEGIP, OBTENER_REPORTE_SEGIP_DEFAULT);
        return Boolean.valueOf(parametroReporte);
    }

    @Override
    public boolean mostrarReporteDatosSegip() {
        String parametroReporte = parametros.getValorDe(MOSTRAR_REPORTE_DATOS_SEGIP, MOSTRAR_REPORTE_DATOS_SEGIP_DEFAULT);
        return Boolean.valueOf(parametroReporte);
    }

    @Override
    public ReporteCertificacion getReporteCertificacion(RespuestaConsultaCertificacion respuesta) {
        IReporteConvert report = new ReporteDecoded();
        if (respuesta != null && respuesta.getEsValido() && respuesta.getReporteCertificacion() != null && respuesta.getReporteCertificacion().length() > 0) {
            return report.get(Convert.convertStringToBytesBase64(respuesta.getReporteCertificacion()));
        }
        return null;
    }

    @Override
    public boolean plazoRetencionVigente(SolicitudConsultaSegip request, PersonaSegip datosSegip) {
        Date fechaVigente;
        Date fechaActual = new Date();
        Date fechaVencimiento = getFechaVencimiento(request);
        if (fechaVencimiento != null) {
            return (fechaVencimiento.compareTo(fechaActual) == 1);
        } else {
            String parametroTipoValidez = parametros.getValorDe(TIPO_PERIODO_VALIDEZ_BASE_SEGIP, TIPO_PERIODO_VALIDEZ_BASE_SEGIP_DEFAULT);
            int parametroPeriodoValidez = parametros.getValorIntDe(PERIODO_VALIDEZ_BASE_SEGIP, PERIODO_VALIDEZ_BASE_SEGIP_DEFAULT);
            Date fechaConsumo = datosSegip.getFechaCarga();
            if (fechaConsumo == null) {
                fechaConsumo = datosSegip.getFechaCreacion();
            }
            fechaVigente = FechaOperator.sumarRestarFecha(parametroTipoValidez, fechaConsumo, parametroPeriodoValidez);
            return (fechaVigente.compareTo(fechaActual) == 1);
        }
    }

    @Override
    public boolean plazoRetencionVigente(SolicitudConsultaSegip request, ContrastacionSegip contrastacionSegip) {
        Date fechaVigente;
        Date fechaActual = new Date();
        Date fechaVencimiento = getFechaVencimiento(request);
        if (fechaVencimiento != null) {
            return (fechaVencimiento.compareTo(fechaActual) == 1);
        } else {
            String parametroTipoValidez = parametros.getValorDe(TIPO_PERIODO_VALIDEZ_BASE_SEGIP, TIPO_PERIODO_VALIDEZ_BASE_SEGIP_DEFAULT);
            int parametroPeriodoValidez = parametros.getValorIntDe(PERIODO_VALIDEZ_BASE_SEGIP, PERIODO_VALIDEZ_BASE_SEGIP_DEFAULT);
            Date fechaConsumo = contrastacionSegip.getFechaContrastacion();
            fechaVigente = FechaOperator.sumarRestarFecha(parametroTipoValidez, fechaConsumo, parametroPeriodoValidez);
            return (fechaVigente.compareTo(fechaActual) == 1);
        }
    }

    @Override
    public boolean formatoValido(SolicitudConsultaSegip request) {
        Boolean documentoValido;
        //Validar numero extranjero
        String tipodocExtranjero = getCarnetExtranjero();
        if (request.getTipoDocumento() != null && tipodocExtranjero.contains(request.getTipoDocumento())) {
            documentoValido = validaPatron(PATTERN_NUMERO_EXTRANJERO, PATTERN_NUMERO_EXTRANJERO_DEFAULT, request.getNumeroDocumento());
            if (documentoValido) {
                if (request.getComplemento() != null && StringUtils.isNotEmpty(request.getComplemento()) && StringUtils.isNotBlank(request.getComplemento())) {
                    documentoValido = validaPatron(PATTERN_COMPLEMENTO, PATTERN_COMPLEMENTO_DEFAULT, request.getComplemento());
                }
                request.setNumeroDocumento(extraerNumeros(request.getNumeroDocumento()));
            }
        } else {
            documentoValido = StringUtils.isNotEmpty(request.getNumeroDocumento())
                    && StringUtils.isNotBlank(request.getNumeroDocumento())
                    && validaPatron(PATTERN_NUMERO_DOCUMENTO, PATTERN_NUMERO_DOCUMENTO_DEFAULT, request.getNumeroDocumento());
            if (documentoValido) {
                if (request.getComplemento() != null && StringUtils.isNotEmpty(request.getComplemento()) && StringUtils.isNotBlank(request.getComplemento())) {
                    documentoValido = validaPatron(PATTERN_COMPLEMENTO, PATTERN_COMPLEMENTO_DEFAULT, request.getComplemento());
                }
            }
        }
        return documentoValido;
    }

    @Override
    public String getCarnetExtranjero() {
        return parametros.getValorDe(TIPO_DOC_EXTRANJERO, TIPO_DOC_EXTRANJERO_DEFAULT);
    }

    @Override
    public String getCarnetNacional() {
        return parametros.getValorDe(TIPO_DOC_NACIONAL, TIPO_DOC_NACIONAL_DEFAULT);
    }

    @Override
    public boolean canalValido(SolicitudConsultaSegip request) {
        String parametroCanalValido = parametros.getValorDe(SEGIP_CANAL_HABILITADO + "" + request.getCanal().getDescripcion(), SEGIP_CANAL_HABILITADO_DEFAULT);
        return Boolean.valueOf(parametroCanalValido);
    }

    @Override
    public boolean tipoDocValido(SolicitudConsultaSegip request) {
        String parametroDocValido;
        if (request.getTipoDocumento() != null && StringUtils.isNotBlank(request.getTipoDocumento())) {
            parametroDocValido = parametros.getValorDe(SEGIP_TIPODOC_HABILITADO, SEGIP_TIPODOC_HABILITADO_DEFAULT);
            return parametroDocValido.contains(request.getTipoDocumento());
        }
        return true;
    }

    @Override
    public boolean consumeServicioSegip() {
        String parametroConsumeSEGIP = parametros.getValorDe(SEGIP_CONSUME_SERVICIO, SEGIP_CONSUME_SERVICIO_DEFAULT);
        return Boolean.valueOf(parametroConsumeSEGIP);
    }

    @Override
    public String getFormato(int tipo) {
        String parametro;
        switch (tipo) {
            case 1:
                parametro = parametros.getValorDe(PATTERN_NUMERO_DOCUMENTO, PATTERN_NUMERO_DOCUMENTO_DEFAULT);
                break;
            case 2:
                parametro = parametros.getValorDe(PATTERN_COMPLEMENTO, PATTERN_COMPLEMENTO_DEFAULT);
                break;
            default:
                parametro = null;
                break;
        }
        return parametro;
    }

    @Override
    public String getMensaje(String tipo) {
        String mensaje;
        switch (tipo) {
            case MENSAJE_REGISTRO_EXISTOSA:
                mensaje = parametros.getValorDe(MENSAJE_REGISTRO_EXISTOSA, MENSAJE_REGISTRO_EXISTOSA_DEFAULT);
                break;
            case MENSAJE_REGISTRO_CONTRASTACION:
                mensaje = parametros.getValorDe(MENSAJE_REGISTRO_CONTRASTACION, MENSAJE_REGISTRO_CONTRASTACION_DEFAULT);
                break;
            case MENSAJE_NO_CONSUMO:
                mensaje = parametros.getValorDe(MENSAJE_NO_CONSUMO, MENSAJE_NO_CONSUMO_DEFAULT);
                break;
            case MENSAJE_NO_REPORTE:
                mensaje = parametros.getValorDe(MENSAJE_NO_REPORTE, MENSAJE_NO_REPORTE_DEFAULT);
                break;
            case MENSAJE_ERROR_RUNTIME_A:
                mensaje = parametros.getValorDe(MENSAJE_ERROR_RUNTIME_A, MENSAJE_ERROR_RUNTIME_A_DEFAULT);
                break;
            case MENSAJE_ERROR_FORMATO:
                mensaje = parametros.getValorDe(MENSAJE_ERROR_FORMATO, MENSAJE_ERROR_FORMATO_DEFAULT);
                break;
            case MENSAJE_ERROR_SEGIP:
                mensaje = parametros.getValorDe(MENSAJE_ERROR_SEGIP, MENSAJE_ERROR_SEGIP_DEFAULT);
                break;
            case MENSAJE_ERROR_CONSULTASEGIP:
                mensaje = parametros.getValorDe(MENSAJE_ERROR_CONSULTASEGIP, MENSAJE_ERROR_CONSULTASEGIP_DEFAULT);
                break;
            case MENSAJE_ERROR_BD_CONTRASTACION:
                mensaje = parametros.getValorDe(MENSAJE_ERROR_BD_CONTRASTACION, MENSAJE_ERROR_BD_CONTRASTACION_DEFAULT);
                break;
            case MENSAJE_ERROR_CANAL:
                mensaje = parametros.getValorDe(MENSAJE_ERROR_CANAL, MENSAJE_ERROR_CANAL_DEFAULT);
                break;
            case MENSAJE_ERROR_TIPODOC:
                mensaje = parametros.getValorDe(MENSAJE_ERROR_TIPODOC, MENSAJE_ERROR_TIPODOC_DEFAULT);
                break;
            default:
                mensaje = "";
                break;
        }
        return StringUtils.trimToEmpty(mensaje);
    }

    @Override
    public ObservacionesSegip getRespuesta(ResultadoConsultaSegip response) {
        String codigo;
        ObservacionesSegip observacion;
        if (TipoCodigoError.OBSERVADO.equals(response.getCodError())) {
            observacion = observacionesSegipService.getMensajeObs(response.getDescripcionRespuestaBean());
            if (observacion == null) {
                codigo = response.getCodError().getCodigo();
                observacion = new ObservacionesSegip(codigo, response.getDescripcionRespuestaBean());
            }
        } else if (TipoRespuesta.CONTRASTADO.equals(response.getTipoRespuesta())) {
            codigo = MENSAJE_CONTRASTADO_DEFAULT;
            observacion = new ObservacionesSegip(codigo, observacionesSegipService.getMensaje(codigo));
        } else {
            codigo = response.getCodError().getCodigo();
            observacion = new ObservacionesSegip(codigo, observacionesSegipService.getMensaje(codigo));
        }
        return observacion;
    }

    @Override
    public ResultadoConsultaSegip escribirIFS(SolicitudConsultaSegip consulta, ResultadoConsultaSegip response) {
        String path, pathfinal, archivo;
        ResultadoConsultaSegip resultadoConsultaSegip;
        AS400 as400 = null;
        resultadoConsultaSegip = response;
        //Verifica de escribir en IFS AS400
        if (consulta.getCodigo() != null && response != null && response.getSegipWebModel() != null && response.getSegipWebModel().getReporte() != null) {
            path = parametros.getValorDe(PATH_IFS_REPORTE_SEGIP, PATH_IFS_REPORTE_SEGIP_DEFAULT);
            archivo = consulta.getCodigo() + ARCHIVO_PDF;
            try {
                as400 = factory.getAS400(AS400.COMMAND);
                pathfinal = ((new StringBuffer()).append(path).append(archivo)).toString();
                EscribirArchivo(as400, pathfinal, response.getSegipWebModel().getReporte());
                resultadoConsultaSegip.getSegipWebModel().setPath(path);
                resultadoConsultaSegip.getSegipWebModel().setArchivo(archivo);
            } catch (IOException | AS400SecurityException | ConnectionPoolException e) {
                LOGGER.error("Error al escribir el IFS: {}", e);
            } finally {
                factory.ret(as400, AS400.COMMAND);
            }
        }
        return resultadoConsultaSegip;
    }

    private boolean validaPatron(String campoPattern, String valorDefecto, String valor) {
        String patron = parametros.getValorDe(campoPattern, valorDefecto);
        Pattern p = Pattern.compile(patron);
        return p.matcher(StringUtils.trimToEmpty(valor)).matches();
    }

    private void EscribirArchivo(AS400 conexion, String path, byte[] reporte) throws IOException, AS400SecurityException {
        ArchivoIFS.writeFile(conexion, path, reporte);
    }

    private Date getFechaVencimiento(SolicitudConsultaSegip consulta) {
        ClienteInfoAdicional datosCliente = persona.getInfoAdicional(consulta);
        try {
            if (datosCliente != null && StringUtils.isNotBlank(datosCliente.getFechaVencimiento())
                    && StringUtils.isNotEmpty(datosCliente.getFechaVencimiento())) {
                return FormatosUtils.getFechaByFormatoYYYY(StringUtils.trimToNull(datosCliente.getFechaVencimiento()));
            }
        } catch (Exception e) {
            LOGGER.error("Error al convertir fecha vencimiento: {}", e);
        }
        return null;
    }

    private String extraerNumeros(String dato) {
        StringBuilder cadena = new StringBuilder("");
        char[] toCharArray = dato.toCharArray();
        for (int i = 0; i < dato.length(); i++) {
            char caracter = toCharArray[i];
            if (Character.isDigit(caracter)) {
                cadena.append(caracter);
            }
        }
        return cadena.toString();
    }

    public String getMensajeContrastacion() {
        return parametros.getValorDe(MENSAJE_REPORTE_CONTRASTACION, MENSAJE_REPORTE_CONTRASTACION_DEFAULT);
    }

    public byte[] getReporteContrastacion(String usuario, ContrastacionSegip contrastacionSegip) {
        return ReporteContrastacion.get(usuario, contrastacionSegip, getMensajeContrastacion());
    }
}
