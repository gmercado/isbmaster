package com.bisa.bus.servicios.segip.ui;

import bus.database.components.FiltroPanel;
import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

/**
 * @author rsalvatierra on 17/03/2016.
 */
public class ListadoPersonasSegip extends FiltroPanel<PersonaSegip> {

    public ListadoPersonasSegip(String id, IModel<PersonaSegip> model1, IModel<PersonaSegip> model2) {
        super(id, model1, model2);

        add(new TextField<>("numeroDocumento", new PropertyModel<>(model1, "numeroDocumento")));
    }
}
