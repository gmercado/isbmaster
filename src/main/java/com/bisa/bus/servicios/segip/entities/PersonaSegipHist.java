package com.bisa.bus.servicios.segip.entities;

import bus.database.model.EntityBase;
import bus.plumbing.utils.FormatosUtils;
import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author josanchez on 15/03/2016.
 * @author rsalvatierra modificado on 01/04/2016
 */
@Entity
@Table(name = "SGP021")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S21FECALT")),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S21USRALT", columnDefinition = "CHAR(255)")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S21FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S21USRMOD", columnDefinition = "CHAR(255)"))
})
public class PersonaSegipHist extends EntityBase implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S21CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "S21CODPER")
    private PersonaSegip personaSegip;

    @Column(name = "S21NROREV", columnDefinition = "NUMERIC(10)")
    private Long version;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "S21FECCAR", columnDefinition = "TIMESTAMP")
    private Date fechaCarga;

    @Column(name = "S21NUMCLI", columnDefinition = "CHAR(10)")
    private String codigoCliente;

    @Column(name = "S21ESVALI", columnDefinition = "CHAR(5)")
    private String consultaValida;

    @Column(name = "S21CODUNI", columnDefinition = "CHAR(50)")
    private String codigoUnico;

    @Column(name = "S21CODRES", columnDefinition = "CHAR(3)")
    private String codigoRespuesta;

    @Column(name = "S21DESRES", columnDefinition = "CHAR(300)")
    private String descripcionRespuesta;

    @Column(name = "S21TIPMEN", columnDefinition = "CHAR(20)")
    private String tipoMensaje;

    @Column(name = "S21MENSJE", columnDefinition = "CHAR(255)")
    private String mensajeTecnico;

    @Column(name = "S21NOMBRE", columnDefinition = "CHAR(60)")
    private String nombres;

    @Column(name = "S21PRIAPE", columnDefinition = "CHAR(30)")
    private String primerApellido;

    @Column(name = "S21SEGAPE", columnDefinition = "CHAR(30)")
    private String segundoApellido;

    @Column(name = "S21FECNAC", columnDefinition = "NUMERIC(8)")
    private Long fechaNacimiento;

    @Column(name = "S21DIRECC", columnDefinition = "CHAR(255)")
    private String direccion;

    @Column(name = "S21OCUPRF", columnDefinition = "CHAR(255)")
    private String ocupacionProfesion;

    @Column(name = "S21ESTCIV", columnDefinition = "CHAR(30)")
    private String estadoCivil;

    @Column(name = "S21NOMCOY", columnDefinition = "CHAR(255)")
    private String nombreConyugue;

    @Column(name = "S21PAISOR", columnDefinition = "CHAR(30)")
    private String pais;

    @Column(name = "S21LUNACD", columnDefinition = "CHAR(100)")
    private String departamento;

    @Column(name = "S21LUNACP", columnDefinition = "CHAR(100)")
    private String provincia;

    @Column(name = "S21LUNACL", columnDefinition = "CHAR(100)")
    private String localidad;

    @Column(name = "S21TIPNAL", columnDefinition = "CHAR(15)")
    private String tipoNacionalidad;

    @Column(name = "S21SYSTEM", columnDefinition = "CHAR(30)")
    private String sistema;

    @Column(name = "S21OBSERS", columnDefinition = "CHAR(255)")
    private String observacion;

    @Basic(fetch = FetchType.EAGER)
    @Column(name = "S21REPOSG", columnDefinition = "BLOB")
    @Lob
    private byte[] reporte;

    public PersonaSegipHist() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoCliente() {
        return StringUtils.trimToEmpty(codigoCliente);
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getPrimerApellido() {
        return StringUtils.trimToEmpty(primerApellido);
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return StringUtils.trimToEmpty(segundoApellido);
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getNombres() {
        return StringUtils.trimToEmpty(nombres);
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public Date getFechaNacimiento() {
        return FormatosUtils.fechaYYYYMMDD(fechaNacimiento);
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = FormatosUtils.fechaYYYYMMDD(fechaNacimiento);
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getDireccion() {
        return StringUtils.trimToEmpty(direccion);
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getOcupacionProfesion() {
        return StringUtils.trimToEmpty(ocupacionProfesion);
    }

    public void setOcupacionProfesion(String ocupacionProfesion) {
        this.ocupacionProfesion = ocupacionProfesion;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getNombreConyugue() {
        return StringUtils.trimToEmpty(nombreConyugue);
    }

    public void setNombreConyugue(String nombreConyugue) {
        this.nombreConyugue = nombreConyugue;
    }

    public String getPais() {
        return StringUtils.trimToEmpty(pais);
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDepartamento() {
        return StringUtils.trimToEmpty(departamento);
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return StringUtils.trimToEmpty(provincia);
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getLocalidad() {
        return StringUtils.trimToEmpty(localidad);
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getTipoNacionalidad() {
        return StringUtils.trimToEmpty(tipoNacionalidad);
    }

    public void setTipoNacionalidad(String tipoNacionalidad) {
        this.tipoNacionalidad = tipoNacionalidad;
    }

    public String getSistema() {
        return StringUtils.trimToEmpty(sistema);
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public byte[] getReporte() {
        return reporte;
    }

    public void setReporte(byte[] reporte) {
        this.reporte = reporte;
    }

    public String getCodigoRespuesta() {
        return StringUtils.trimToEmpty(codigoRespuesta);
    }

    public void setCodigoRespuesta(String codigoRespuesta) {
        this.codigoRespuesta = codigoRespuesta;
    }

    public String getCodigoUnico() {
        return StringUtils.trimToEmpty(codigoUnico);
    }

    public void setCodigoUnico(String codigoUnico) {
        this.codigoUnico = codigoUnico;
    }

    public String getDescripcionRespuesta() {
        return StringUtils.trimToEmpty(descripcionRespuesta);
    }

    public void setDescripcionRespuesta(String descripcionRespuesta) {
        this.descripcionRespuesta = descripcionRespuesta;
    }

    public String getConsultaValida() {
        return StringUtils.trimToEmpty(consultaValida);
    }

    public void setConsultaValida(String consultaValida) {
        this.consultaValida = consultaValida;
    }

    public String getMensajeTecnico() {
        return StringUtils.trimToEmpty(mensajeTecnico);
    }

    public void setMensajeTecnico(String mensajeTecnico) {
        this.mensajeTecnico = mensajeTecnico;
    }

    public String getTipoMensaje() {
        return StringUtils.trimToEmpty(tipoMensaje);
    }

    public void setTipoMensaje(String tipoMensaje) {
        this.tipoMensaje = tipoMensaje;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(Date fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public String getFechaNacimientoFormato() {
        return FormatosUtils.formatoFecha(FormatosUtils.fechaYYYYMMDD(fechaNacimiento));
    }

    public String getFechaCreacionFormato() {
        return FormatosUtils.formatoFechaHora(fechaCreacion);
    }

    public PersonaSegip getPersonaSegip() {
        return personaSegip;
    }

    public void setPersonaSegip(PersonaSegip personaSegip) {
        this.personaSegip = personaSegip;
    }

    @Override
    public String toString() {
        return "PersonaSegipHist{" +
                "id=" + id +
                ", fechaCarga=" + getFechaCarga() +
                ", codigoCliente='" + StringUtils.trimToEmpty(getCodigoCliente()) + '\'' +
                ", primerApellido='" + StringUtils.trimToEmpty(getPrimerApellido()) + '\'' +
                ", segundoApellido='" + StringUtils.trimToEmpty(getSegundoApellido()) + '\'' +
                ", nombres='" + StringUtils.trimToEmpty(getNombres()) + '\'' +
                ", fechaNacimiento=" + getFechaNacimiento() +
                ", observacion='" + StringUtils.trimToEmpty(getObservacion()) + '\'' +
                ", direccion='" + StringUtils.trimToEmpty(getDireccion()) + '\'' +
                ", ocupacionProfesion='" + StringUtils.trimToEmpty(getOcupacionProfesion()) + '\'' +
                ", estadoCivil='" + StringUtils.trimToEmpty(getEstadoCivil()) + '\'' +
                ", nombreConyugue='" + StringUtils.trimToEmpty(getNombreConyugue()) + '\'' +
                ", pais='" + StringUtils.trimToEmpty(getPais()) + '\'' +
                ", departamento='" + StringUtils.trimToEmpty(getDepartamento()) + '\'' +
                ", provincia='" + StringUtils.trimToEmpty(getProvincia()) + '\'' +
                ", localidad='" + StringUtils.trimToEmpty(getLocalidad()) + '\'' +
                ", tipoNacionalidad='" + StringUtils.trimToEmpty(getTipoNacionalidad()) + '\'' +
                ", sistema='" + StringUtils.trimToEmpty(getSistema()) + '\'' +
                ", codigoRespuesta='" + StringUtils.trimToEmpty(getCodigoRespuesta()) + '\'' +
                ", codigoUnico='" + StringUtils.trimToEmpty(getCodigoUnico()) + '\'' +
                ", descripcionRespuesta='" + StringUtils.trimToEmpty(getDescripcionRespuesta()) + '\'' +
                ", consultaValida='" + StringUtils.trimToEmpty(getConsultaValida()) + '\'' +
                ", mensajeTecnico='" + StringUtils.trimToEmpty(getMensajeTecnico()) + '\'' +
                ", tipoMensaje='" + StringUtils.trimToEmpty(getTipoMensaje()) + '\'' +
                ", version=" + getVersion() +
                '}';
    }
}