package com.bisa.bus.servicios.segip.entities;

import org.apache.commons.lang.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author josanchez on 23/03/2016.
 * @author rsalvatierra modificado on 30/03/2016
 */
@Entity
@Table(name = "CUP00401")
public class ClienteNatural implements Serializable {

    @Id
    @Column(name = "CUCNBR", columnDefinition = "CHAR(10)")
    private String id;

    @Column(name = "CUCFNM", columnDefinition = "CHAR(20)")
    private String nombre;

    @Column(name = "CUCLNM", columnDefinition = "CHAR(20)")
    private String paterno;

    @Column(name = "CUCSNM", columnDefinition = "CHAR(20)")
    private String materno;

    @Column(name = "CUCUNM", columnDefinition = "CHAR(40)")
    private String conyuge;

    @Column(name = "CUCCTY", columnDefinition = "CHAR(30)")
    private String ciudad;

    @Column(name = "CUCTWN", columnDefinition = "CHAR(30)")
    private String provincia;

    @Column(name = "CUCDIS", columnDefinition = "CHAR(30)")
    private String zona;

    @Column(name = "CUCSTR", columnDefinition = "CHAR(30)")
    private String calle;

    @Column(name = "CUCHNO", columnDefinition = "CHAR(10)")
    private String numeroDireccion;

    public ClienteNatural() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getConyuge() {
        return conyuge;
    }

    public void setConyuge(String conyuge) {
        this.conyuge = conyuge;
    }

    public String getNumeroDireccion() {
        return numeroDireccion;
    }

    public void setNumeroDireccion(String numeroDireccion) {
        this.numeroDireccion = numeroDireccion;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDireccionCompleta() {
        return StringUtils.trimToEmpty(getZona()) + " " + StringUtils.trimToEmpty(getNumeroDireccion()) + "" + StringUtils.trimToEmpty(getCalle());
    }

    @Override
    public String toString() {
        return "ClienteNatural{" +
                "id='" + getId() + '\'' +
                ", nombre='" + StringUtils.trimToEmpty(getNombre()) + '\'' +
                ", paterno='" + StringUtils.trimToEmpty(getPaterno()) + '\'' +
                ", materno='" + StringUtils.trimToEmpty(getMaterno()) + '\'' +
                ", conyuge='" + StringUtils.trimToEmpty(getConyuge()) + '\'' +
                ", numeroDireccion='" + StringUtils.trimToEmpty(getNumeroDireccion()) + '\'' +
                ", calle='" + StringUtils.trimToEmpty(getCalle()) + '\'' +
                ", zona='" + StringUtils.trimToEmpty(getZona()) + '\'' +
                ", provincia='" + StringUtils.trimToEmpty(getProvincia()) + '\'' +
                ", ciudad='" + StringUtils.trimToEmpty(getCiudad()) + '\'' +
                '}';
    }
}
