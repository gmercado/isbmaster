package com.bisa.bus.servicios.segip.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.segip.entities.ClienteNatural;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.LinkedList;

/**
 * @author josanchez on 23/03/2016.
 * @author rsalvatierra modificadon on 30/03/2016
 */
public class ClienteNaturalDao extends DaoImpl<ClienteNatural, Long> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClienteNaturalDao.class);

    @Inject
    protected ClienteNaturalDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public ClienteNatural getDatosCliente(String pCodigoCliente) {
        LOGGER.debug("Obteniendo registro Cup004 por Codigo Cliente:{}.", pCodigoCliente);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<ClienteNatural> q = cb.createQuery(ClienteNatural.class);
                        Root<ClienteNatural> p = q.from(ClienteNatural.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("id"), StringUtils.trimToEmpty(pCodigoCliente)));

                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<ClienteNatural> query = entityManager.createQuery(q);
                        return query.getSingleResult();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }
}
