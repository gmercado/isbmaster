package com.bisa.bus.servicios.segip.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author rsalvatierra on 09/03/2016.
 */
@XmlRootElement(name = "Images")
public class Imagenes {
    private List<Imagen> listImages;

    /**
     * @return the listImages
     */
    public List<Imagen> getListImages() {
        return listImages;
    }

    /**
     * @param listImages the listImages to set
     */
    @XmlElement(name = "Image")
    public void setListImages(List<Imagen> listImages) {
        this.listImages = listImages;
    }
}
