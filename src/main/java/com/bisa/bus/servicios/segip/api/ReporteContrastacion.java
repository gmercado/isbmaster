package com.bisa.bus.servicios.segip.api;

import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.segip.entities.ContrastacionSegip;
import net.sf.jasperreports.engine.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author rsalvatierra on 17/03/2016.
 */
public class ReporteContrastacion {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReporteContrastacion.class);

    private static final String LOGO_BANCO = "bancobisalogo.png";
    private static final String REPORTE_CONTRASTACION = "rptDatosContrastacion.jasper";

    public static byte[] get(String usuario, ContrastacionSegip datosContrastacion, String mensaje) {
        byte[] output = null;
        JasperPrint jasperPrint;
        Map<String, Object> paramMap = new HashMap<>();
        String logoPath = (ReporteContrastacion.class.getResource("/images/")).getPath();
        String reportPath = (ReporteContrastacion.class.getResource("/reports/")).getPath();
        logoPath += LOGO_BANCO;
        reportPath += REPORTE_CONTRASTACION;

        paramMap.put("logo", logoPath);
        paramMap.put("codigoCliente", datosContrastacion.getCodigoCliente().toString());
        paramMap.put("fechaContrastacion", FormatosUtils.formatoFecha(datosContrastacion.getFechaContrastacion()));
        paramMap.put("documentoIdentidad", datosContrastacion.getDocumentoIdentidad());
        paramMap.put("complemento", datosContrastacion.getComplemento());
        paramMap.put("nombres", datosContrastacion.getNombres());
        paramMap.put("apellidoPaterno", datosContrastacion.getApellidoPaterno());
        paramMap.put("apellidoMaterno", datosContrastacion.getApellidoMaterno());
        paramMap.put("fechaNacimiento", FormatosUtils.formatoFecha(datosContrastacion.getFechaNacimiento()));
        paramMap.put("usuario", usuario);
        paramMap.put("fechaImpresion", FormatosUtils.formatoFechaHora(new Date()));
        paramMap.put("mensaje", mensaje);
        paramMap.put("estado", datosContrastacion.getEstadoSolicitud());
        try {
            jasperPrint = JasperFillManager.fillReport(reportPath, paramMap, new JREmptyDataSource());
            output = JasperExportManager.exportReportToPdf(jasperPrint);
        } catch (JRException e) {
            LOGGER.error("Error en la generacion del reporte de contrastacion:", e);
        }
        return output;
    }
}
