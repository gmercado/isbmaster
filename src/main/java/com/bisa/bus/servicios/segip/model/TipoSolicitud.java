package com.bisa.bus.servicios.segip.model;

/**
 * @author rsalvatierra on 04/04/2016.
 */
public enum TipoSolicitud {
    ALT_AS400("AA", "Alta Cliente - AS400"),
    MOD_AS400("AM", "Modificacion Cliente - AS400"),
    CON_AQUA2("QC", "Consulta - Aqua2"),
    ALT_STI("SA", "Alta Cliente - STI"),;

    private String codigo;
    private String descripcion;

    TipoSolicitud(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static TipoSolicitud get(String codigo) {
        for (TipoSolicitud valor : TipoSolicitud.values()) {
            if (valor.getCodigo().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return "TipoSolicitud{" +
                "codigo=" + codigo +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }

}
