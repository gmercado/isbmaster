package com.bisa.bus.servicios.segip.ws;

import com.bisa.bus.servicios.segip.api.SegipProcessService;
import com.bisa.bus.servicios.segip.api.SegipService;
import com.bisa.bus.servicios.segip.entities.ObservacionesSegip;
import com.bisa.bus.servicios.segip.model.*;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;

import javax.jws.WebService;

import static com.bisa.bus.servicios.segip.api.ConfiguracionSegip.MENSAJE_ERROR_RUNTIME_A;

/**
 * @author josanchez on 17/02/2016.
 * @author rsalvatierra modificado on 31/03/2016
 */
@WebService(targetNamespace = "bisa.com")
public class WebServiceSegipEndPoint implements WebServiceSegip {

    @Inject
    SegipService segipService;
    @Inject
    SegipProcessService procesoSegip;

    @Override
    public ResultadoConsultaSegip consultarSEGIP(SolicitudConsultaSegip request) {
        //Consumir servicio
        ResultadoConsultaSegip response = segipService.consultasCertificacionSegip(request);
        if (response == null) {
            response = new ResultadoConsultaSegip();
            response.setEsValidoBean(false);
            response.setCodError(TipoCodigoError.ERR_INESPERADO);
            response.setCodigoRespuestaBean(TipoCodigoRespuesta.NO_REALIZO_CONSULTA.getCodigo());
            response.setDescripcionRespuestaBean(TipoCodigoRespuesta.NO_REALIZO_CONSULTA.getDescripcion());
            response.setTipoMensajeBean(TipoMensaje.WARN.getDescripcion());
            response.setMensajeBean(procesoSegip.getMensaje(MENSAJE_ERROR_RUNTIME_A));
            response.setTipoRespuesta(TipoRespuesta.ERROR);
        }
        ObservacionesSegip obs = procesoSegip.getRespuesta(response);
        if (obs != null) {
            if (obs.getDescripcion() != null) {
                response.setDescError(StringUtils.trimToEmpty(obs.getDescripcion().toUpperCase()));
            } else {
                response.setDescError(TipoCodigoError.ERR_INESPERADO.getDescripcion());
            }
        }
        return response;
    }
}
