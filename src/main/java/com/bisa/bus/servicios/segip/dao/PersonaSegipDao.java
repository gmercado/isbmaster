package com.bisa.bus.servicios.segip.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.plumbing.utils.Convert;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.segip.api.PersonaSegipService;
import com.bisa.bus.servicios.segip.api.SegipProcessService;
import com.bisa.bus.servicios.segip.consumer.model.RespuestaConsultaCertificacion;
import com.bisa.bus.servicios.segip.entities.*;
import com.bisa.bus.servicios.segip.model.ResultadoConsultaSegip;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegip;
import com.bisa.bus.servicios.segip.model.TipoCodigoRespuesta;
import com.bisa.bus.servicios.segip.xml.ReporteCertificacion;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static com.bisa.bus.servicios.segip.api.ConfiguracionSegip.*;

/**
 * @author josanchez on 15/03/2016.
 * @author rsalvatierra modificado on 30/03/2016
 */
public class PersonaSegipDao extends DaoImpl<PersonaSegip, Long> implements PersonaSegipService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonaSegipDao.class);

    private final ClienteNaturalDao clienteNaturalDao;
    private final ClienteDao clienteDao;
    private final ClienteInfoAdicionalDao clienteInfoAdicionalDao;
    private final PersonaSegipHistDao personaHistDao;
    private final SegipProcessService segipProcesoService;

    @Inject
    protected PersonaSegipDao(@BasePrincipal EntityManagerFactory entityManagerFactory, ClienteNaturalDao clienteNaturalDao, ClienteDao clienteDao, ClienteInfoAdicionalDao clienteInfoAdicionalDao, PersonaSegipHistDao personaHistDao, SegipProcessService segipProcesoService) {
        super(entityManagerFactory);
        this.clienteNaturalDao = clienteNaturalDao;
        this.clienteDao = clienteDao;
        this.clienteInfoAdicionalDao = clienteInfoAdicionalDao;
        this.personaHistDao = personaHistDao;
        this.segipProcesoService = segipProcesoService;
    }

    @Override
    public PersonaSegip getPersona(SolicitudConsultaSegip request) {
        LOGGER.debug("Obteniendo registro Segip por Numero Documento:{}", request.getNumeroDocumentoComplemento());
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<PersonaSegip> q = cb.createQuery(PersonaSegip.class);
                        Root<PersonaSegip> p = q.from(PersonaSegip.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("numeroDocumento"), StringUtils.trimToEmpty(request.getNumeroDocumentoComplemento())));
                        if (request.getFechaNacimiento() != null && StringUtils.isNotBlank(request.getFechaNacimiento())) {
                            predicatesAnd.add(cb.equal(p.get("fechaNacimiento"), FormatosUtils.fechaYYYYMMDD(FormatosUtils.getFechaByFormatoYYYY(request.getFechaNacimiento()))));
                        }
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<PersonaSegip> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                            return query.getResultList().get(0);
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta: {}", e);
            return null;
        }
    }

    @Override
    public PersonaSegip getPersona(Long id) {
        return find(id);
    }

    @Override
    protected Path<Long> countPath(Root<PersonaSegip> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<PersonaSegip> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.<String>get("numeroDocumento"));
        return paths;
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<PersonaSegip> from, PersonaSegip example, PersonaSegip example2) {
        String nroPNR;
        List<Predicate> predicates = new LinkedList<>();
        if (example != null) {
            nroPNR = StringUtils.trimToNull(example.getNumeroDocumento());
            if (nroPNR != null) {
                LOGGER.debug(">>>>> numeroDocumento=" + nroPNR);
                predicates.add(cb.like(from.get("numeroDocumento"), nroPNR.toUpperCase()));
                predicates.add(cb.qbe(from, example));
            }
        }
        return predicates.toArray(new Predicate[predicates.size()]);
    }

    private Cliente verificaCliente(String pNumeroDocumento) {
        return clienteDao.getCliente(pNumeroDocumento);
    }

    private Cliente obtenerCliente(String pNumeroCliente) {
        return clienteDao.getClienteById(pNumeroCliente);
    }

    private ClienteNatural verificaNombreCliente(String pCodigoCliente) {
        return clienteNaturalDao.getDatosCliente(pCodigoCliente);
    }

    @Override
    public ClienteInfoAdicional getInfoAdicional(SolicitudConsultaSegip request) {
        Cliente datosCliente;
        //Buscar por numero de documento
        if (request.getNumeroDocumentoComplemento() != null) {
            datosCliente = verificaCliente(request.getNumeroDocumentoComplemento());
            if (datosCliente != null) {
                return clienteInfoAdicionalDao.getDatosCliente(datosCliente.getId());
            }
        }
        return null;
    }

    private String obtenerCodigoCliente(SolicitudConsultaSegip request) {
        String codCliente = null;
        Cliente datosCliente = null;
        ClienteNatural datosCompCliente;
        //Buscar por numero de cliente
        if (StringUtils.isNotBlank(request.getNumeroCliente()) && StringUtils.isNotEmpty(request.getNumeroCliente())) {
            datosCliente = obtenerCliente(request.getNumeroCliente());
        }
        //Buscar por numero de documento
        if (datosCliente == null && request.getNumeroDocumentoComplemento() != null) {
            datosCliente = verificaCliente(request.getNumeroDocumentoComplemento());
        }
        if (datosCliente != null) {
            datosCompCliente = verificaNombreCliente(datosCliente.getId());
            if (datosCompCliente != null) {
                codCliente = StringUtils.trimToEmpty(datosCliente.getId());
            }
        }
        return codCliente;
    }

    @Override
    public void registrarPersonaBaseContrastacion(ResultadoConsultaSegip respuesta, ContrastacionSegip contrastacionSegip, SolicitudConsultaSegip request) {
        String codigoCliente;
        PersonaSegip personaSegip = getPersona(request);
        if (personaSegip == null) {
            //Si no existe en BD SEGIP, se registra
            personaSegip = new PersonaSegip();
            personaSegip.setFechaCarga(contrastacionSegip.getFechaContrastacion());
            personaSegip.setSistema(SYSTEM_CONTRASTACION);
            personaSegip.setNumeroDocumento(request.getNumeroDocumentoComplemento());
            //Obtener Datos Cliente CUP003
            codigoCliente = obtenerCodigoCliente(request);
            //Datos Contrastacion
            if (codigoCliente != null) {
                personaSegip.setCodigoCliente(codigoCliente);
            }
            personaSegip.setNumeroDocumento(StringUtils.trimToEmpty(request.getNumeroDocumentoComplemento()));
            personaSegip.setNombres(StringUtils.trimToEmpty(contrastacionSegip.getNombres()));
            personaSegip.setPrimerApellido(StringUtils.trimToEmpty(contrastacionSegip.getApellidoPaterno()));
            personaSegip.setSegundoApellido(StringUtils.trimToEmpty(contrastacionSegip.getApellidoMaterno()));
            personaSegip.setFechaNacimiento(contrastacionSegip.getFechaNacimiento());
            //reporte
            if (respuesta.getSegipWebModel() != null && respuesta.getSegipWebModel().getReporte() != null) {
                personaSegip.setReporte(respuesta.getSegipWebModel().getReporte());
            }
            //datos generales
            personaSegip.setCodigoRespuesta(respuesta.getCodigoRespuestaBean().toString());
            personaSegip.setCodigoUnico(respuesta.getCodigoUnicoBean());
            personaSegip.setDescripcionRespuesta(respuesta.getDescripcionRespuestaBean());
            personaSegip.setConsultaValida(String.valueOf(respuesta.isEsValidoBean()));
            personaSegip.setMensajeTecnico(respuesta.getMensajeBean());
            personaSegip.setTipoMensaje(respuesta.getTipoMensajeBean());

            personaSegip.setFechaCreacion(new Date());
            personaSegip.setUsuarioCreador(request.getNombreOficial());
            personaSegip.setObservacion(CONTRASTADO_OBSERVACION);
            persist(personaSegip);
        }
    }

    @Override
    public void registrarPersonaCertificadaSegip(ReporteCertificacion reporte, SolicitudConsultaSegip request, RespuestaConsultaCertificacion respuestaServicio, ResultadoConsultaSegip respuesta) {
        String codigoCliente;
        PersonaSegip personaSegip = getPersona(request);
        if (personaSegip == null) {
            //Si no existe en BD SEGIP, se registra
            personaSegip = new PersonaSegip();
            personaSegip.setNumeroDocumento(request.getNumeroDocumentoComplemento());
            //Obtener Datos Cliente CUP003
            codigoCliente = obtenerCodigoCliente(request);
            if (codigoCliente != null) {
                personaSegip.setCodigoCliente(codigoCliente);
            }
            personaSegip.setFechaCreacion(new Date());
            personaSegip.setUsuarioCreador(request.getNombreOficial());
        }
        personaSegip.setSistema(SYSTEM_SEGIP);
        //Verificar si tenemos datos en el reporte
        if (reporte != null) {
            personaSegip.setNombres(reporte.getDatosCiudadano().getNombres());
            personaSegip.setPrimerApellido(reporte.getDatosCiudadano().getPrimerApellido());
            personaSegip.setSegundoApellido(reporte.getDatosCiudadano().getSegundoApellido());
            personaSegip.setFechaNacimiento(FormatosUtils.getFechaByFormatoYYYY(reporte.getDatosCiudadano().getFechaNacimiento()));
            personaSegip.setDireccion(reporte.getDatosComplementarios().getDomicilio());
            personaSegip.setOcupacionProfesion(reporte.getDatosComplementarios().getProfesion());
            personaSegip.setEstadoCivil(reporte.getDatosComplementarios().getEstadoCivil());
            personaSegip.setNombreConyugue(reporte.getDatosFamiliares().getConyuge());
            personaSegip.setDepartamento(reporte.getDatosNacimiento().getDepartamento());
            personaSegip.setLocalidad(reporte.getDatosNacimiento().getLocalidad());
            personaSegip.setProvincia(reporte.getDatosNacimiento().getProvinicia());
            personaSegip.setPais(reporte.getDatosNacimiento().getPais());
            personaSegip.setTipoNacionalidad(reporte.getDatosCiudadano().getTipoCiudadano());
            if (respuestaServicio.getReporteCertificacion() != null) {
                personaSegip.setReporte(Convert.convertStringToBytesBase64(respuestaServicio.getReporteCertificacion()));
            }
            personaSegip.setFotoPersona(reporte.getFoto());
        } else {
            personaSegip.setNombres(null);
            personaSegip.setPrimerApellido(null);
            personaSegip.setSegundoApellido(null);
            personaSegip.setFechaNacimiento(null);
            personaSegip.setDireccion(null);
            personaSegip.setOcupacionProfesion(null);
            personaSegip.setEstadoCivil(null);
            personaSegip.setNombreConyugue(null);
            personaSegip.setDepartamento(null);
            personaSegip.setLocalidad(null);
            personaSegip.setProvincia(null);
            personaSegip.setPais(null);
            personaSegip.setTipoNacionalidad(null);
            personaSegip.setReporte(null);
            personaSegip.setFotoPersona(null);
        }
        //datos respuesta
        if (respuesta.getCodigoRespuestaBean() != null) {
            personaSegip.setCodigoRespuesta(respuesta.getCodigoRespuestaBean().toString());
        }
        personaSegip.setObservacion(segipProcesoService.getObservacion(reporte, respuesta));
        personaSegip.setCodigoUnico(respuesta.getCodigoUnicoBean());
        personaSegip.setDescripcionRespuesta(respuesta.getDescripcionRespuestaBean());
        personaSegip.setConsultaValida(String.valueOf(respuesta.isEsValidoBean()));
        personaSegip.setMensajeTecnico(respuesta.getMensajeBean());
        personaSegip.setTipoMensaje(respuesta.getTipoMensajeBean());
        personaSegip.setFechaCarga(new Date());
        if (!String.valueOf(TipoCodigoRespuesta.NO_REALIZO_CONSULTA.getCodigo()).equals(personaSegip.getCodigoRespuesta())
                && !String.valueOf(TipoCodigoRespuesta.NO_EXiSTE.getCodigo()).equals(personaSegip.getCodigoRespuesta())) {
            //Registrar historial si existe cambio
            if (existeModificacion(request, personaSegip)) {
                PersonaSegip personaOrig = getPersona(request);
                PersonaSegipHist personaSegipHist = segipProcesoService.getDatosHistorico(personaOrig);
                personaHistDao.actualizaRegistro(request, personaSegip, personaSegipHist);
                personaSegip.setFechaCarga(new Date());
                personaSegip.setFechaModificacion(new Date());
                personaSegip.setUsuarioModificador(request.getNombreOficial());
            }
            //Actualizar registro si cambia de estado
            merge(personaSegip);
        }
    }

    private boolean existeModificacion(SolicitudConsultaSegip request, PersonaSegip personaSegip) {
        if (personaSegip.getId() == null) {
            return false;
        } else {
            PersonaSegip personaSegipOther = getPersona(request);
            if (personaSegipOther != null) {
                if (!personaSegip.getNombres().equals(personaSegipOther.getNombres())
                        || !StringUtils.trimToEmpty(personaSegip.getPrimerApellido()).equals(StringUtils.trimToEmpty(personaSegipOther.getPrimerApellido()))
                        || !StringUtils.trimToEmpty(personaSegip.getSegundoApellido()).equals(StringUtils.trimToEmpty(personaSegipOther.getSegundoApellido()))
                        || !(personaSegip.getFechaNacimiento() != null && personaSegip.getFechaNacimiento().equals(personaSegipOther.getFechaNacimiento()))
                        || !StringUtils.trimToEmpty(personaSegip.getDireccion()).equals(StringUtils.trimToEmpty(personaSegipOther.getDireccion()))
                        || !StringUtils.trimToEmpty(personaSegip.getOcupacionProfesion()).equals(StringUtils.trimToEmpty(personaSegipOther.getOcupacionProfesion()))
                        || !StringUtils.trimToEmpty(personaSegip.getEstadoCivil()).equals(StringUtils.trimToEmpty(personaSegipOther.getEstadoCivil()))
                        || !StringUtils.trimToEmpty(personaSegip.getNombreConyugue()).equals(StringUtils.trimToEmpty(personaSegipOther.getNombreConyugue()))
                        || !StringUtils.trimToEmpty(personaSegip.getDepartamento()).equals(StringUtils.trimToEmpty(personaSegipOther.getDepartamento()))
                        || !StringUtils.trimToEmpty(personaSegip.getLocalidad()).equals(StringUtils.trimToEmpty(personaSegipOther.getLocalidad()))
                        || !StringUtils.trimToEmpty(personaSegip.getProvincia()).equals(StringUtils.trimToEmpty(personaSegipOther.getProvincia()))
                        || !StringUtils.trimToEmpty(personaSegip.getPais()).equals(StringUtils.trimToEmpty(personaSegipOther.getPais()))
                        || !StringUtils.trimToEmpty(personaSegip.getSistema()).equals(StringUtils.trimToEmpty(personaSegipOther.getSistema()))
                        || !StringUtils.trimToEmpty(personaSegip.getTipoNacionalidad()).equals(StringUtils.trimToEmpty(personaSegipOther.getTipoNacionalidad()))
                        || !StringUtils.trimToEmpty(personaSegip.getObservacion()).equals(StringUtils.trimToEmpty(personaSegipOther.getObservacion()))
                        || !StringUtils.trimToEmpty(personaSegip.getCodigoRespuesta()).equals(StringUtils.trimToEmpty(personaSegipOther.getCodigoRespuesta()))
                        || !StringUtils.trimToEmpty(personaSegip.getCodigoUnico()).equals(StringUtils.trimToEmpty(personaSegipOther.getCodigoUnico()))
                        || !StringUtils.trimToEmpty(personaSegip.getDescripcionRespuesta()).equals(StringUtils.trimToEmpty(personaSegipOther.getDescripcionRespuesta()))
                        || !StringUtils.trimToEmpty(personaSegip.getConsultaValida()).equals(StringUtils.trimToEmpty(personaSegipOther.getConsultaValida()))
                        || !StringUtils.trimToEmpty(personaSegip.getMensajeTecnico()).equals(StringUtils.trimToEmpty(personaSegipOther.getMensajeTecnico()))
                        || !StringUtils.trimToEmpty(personaSegip.getTipoMensaje()).equals(StringUtils.trimToEmpty(personaSegipOther.getTipoMensaje()))
                        ) {
                    return true;
                }
            }
        }
        return false;
    }
}
