package com.bisa.bus.servicios.segip.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rsalvatierra on 09/03/2016.
 */
@XmlRootElement(name = "informacion_complementaria")
public class DatosComplementarios {

    private String domicilio;
    private String profesion;
    private String estadoCivil;

    /**
     * @return the domicilio
     */
    public String getDomicilio() {
        return domicilio;
    }

    /**
     * @param domicilio the domicilio to set
     */
    @XmlElement(name = "domicilio")
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    /**
     * @return the profesion
     */
    public String getProfesion() {
        return profesion;
    }

    /**
     * @param profesion the profesion to set
     */
    @XmlElement(name = "profesion_u_ocupacion")
    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    /**
     * @return the estadoCivil
     */
    public String getEstadoCivil() {
        return estadoCivil;
    }

    /**
     * @param estadoCivil the estadoCivil to set
     */
    @XmlElement(name = "estado_civil")
    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    @Override
    public String toString() {
        return domicilio + "-" + profesion + "-" + estadoCivil;
    }
}
