package com.bisa.bus.servicios.segip.ui;

import bus.database.components.Listado;
import bus.database.components.ListadoDataProvider;
import bus.database.components.ListadoPanel;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;
import bus.plumbing.components.ToolButton;
import com.bisa.bus.servicios.segip.dao.ObservacionesSegipDao;
import com.bisa.bus.servicios.segip.entities.ObservacionesSegip;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.ArrayList;
import java.util.List;

/**
 * @author rsalvatierra on 01/06/2016
 */
@AuthorizeInstantiation({RolesBisa.SEGIP_PARAM})
@Menu(value = "Observaciones SEGIP", subMenu = SubMenu.SEGIP)
public class ListadoObservacionesSegip extends Listado<ObservacionesSegip, Long> {

    @Override
    protected ListadoPanel<ObservacionesSegip, Long> newListadoPanel(String id) {
        return new ListadoPanel<ObservacionesSegip, Long>(id) {
            @Override
            protected List<IColumn<ObservacionesSegip, String>> newColumns(ListadoDataProvider<ObservacionesSegip, Long> dataProvider) {
                ArrayList<IColumn<ObservacionesSegip, String>> iColumns = new ArrayList<>();
                iColumns.add(new LinkPropertyColumn<ObservacionesSegip>(Model.of("C\u00f3digo"), "codigo", "codigo") {
                    @Override
                    protected void onClick(ObservacionesSegip object) {
                        setResponsePage(EditarObservacionSegip.class, new PageParameters().add("codigo", object.getCodigo()));
                    }
                });
                iColumns.add(new PropertyColumn<>(Model.of("Mensaje"), "descripcion"));
                return iColumns;
            }

            @Override
            protected void poblarBotones(RepeatingView rv, String idButton, Form<Void> filtroForm) {
                WebMarkupContainer wmk;
                rv.add(wmk = new WebMarkupContainer(rv.newChildId()));
                wmk.add(new ToolButton(idButton) {

                    @Override
                    public void onSubmit() {
                        setResponsePage(NuevaObservacionSegip.class);
                    }
                }
                        .setClassAttribute("btn btn-info")
                        .setIconAttribute("icon-file icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Crear observaci\u00f3n")));
            }

            @Override
            protected Class<? extends Dao<ObservacionesSegip, Long>> getProviderClazz() {
                return ObservacionesSegipDao.class;
            }
        };
    }
}
