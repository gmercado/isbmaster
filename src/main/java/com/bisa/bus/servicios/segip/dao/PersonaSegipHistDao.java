package com.bisa.bus.servicios.segip.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.segip.api.PersonaSegipHistService;
import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import com.bisa.bus.servicios.segip.entities.PersonaSegipHist;
import com.bisa.bus.servicios.segip.model.HistoricoSegip;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegip;
import com.google.inject.Inject;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author josanchez on 15/03/2016.
 * @author rsalvatierra modificado on 01/04/2016
 */
public class PersonaSegipHistDao extends DaoImpl<PersonaSegipHist, Long> implements Serializable, PersonaSegipHistService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonaSegipHistDao.class);

    @Inject
    protected PersonaSegipHistDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    public List<HistoricoSegip> getHistorico2(PersonaSegip id) {
        LOGGER.debug("Obteniendo historico para {}", id);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {

                        String sql = "select s01nomusr as codUsuario,s01tipusr as canal,s01feccon as fecha,s01coderr as codError,s01codres as codRespuesta,s20codigo as codPersona,s20nrorev as nroRevision,s20esvali as esValido,s20codres as codigoRespuesta,s20coduni as codigoUnico,s20desres as desRespuesta,s20tipmen as tipoMensaje,s20mensje as mensaje,s20nombre as nombre,s20priape as primerApellido,s20segape as segundoApellido,s20fecnac as fechaNacimiento,s20obsers as observaciones" +
                                " from sgp001 b,sgp020 p" +
                                " where p.s20codigo=b.s01codper and p.s20nrorev=b.S01NROREV" +
                                " and p.s20codigo=?" +
                                " union\n" +
                                " select s01nomusr,s01tipusr,s01feccon,s01coderr,s01codres,s20codigo,s21nrorev,s21esvali,s21codres,s21coduni,s21desres,s21tipmen,s21mensje,s21nombre,s21priape,s21segape,s21fecnac,s21obsers" +
                                " from sgp001 b1,sgp020 p1,sgp021 h" +
                                " where p1.s20codigo=h.s21codper " +
                                " and p1.s20codigo=b1.s01codper and h.s21nrorev=b1.S01NROREV" +
                                " and p1.s20codigo=?" +
                                " order by 3 desc";
                        Query q = entityManager.createNativeQuery(sql, HistoricoSegip.class).setParameter(1, id.getId()).setParameter(2, id.getId());
                        return (List<HistoricoSegip>) q.getResultList();
                    });
        } catch (Exception e) {
            LOGGER.error("Error en la consulta: {}", e);
            return null;
        }
    }


    @Override
    protected Path<Long> countPath(Root<PersonaSegipHist> from) {
        return from.get("idHist");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<PersonaSegipHist> p) {
        Path<String> id = p.get("id");
        return Collections.singletonList(id);
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<PersonaSegipHist> from, PersonaSegipHist example, PersonaSegipHist example2) {
        List<Predicate> predicates = new LinkedList<>();
        Long idPersona = example.getId();

        Predicate qbe = cb.qbe(from, example);
        predicates.add(qbe);

        if (idPersona != null) {
            LOGGER.debug(">>>>> idPersona=" + idPersona);
            predicates.add(cb.equal(from.get("id"), idPersona));
        }

        return predicates.toArray(new Predicate[predicates.size()]);
    }

    @Override
    public void actualizaRegistro(SolicitudConsultaSegip request, PersonaSegip personaSegip, PersonaSegipHist personaSegipHist) {
        try {
            personaSegipHist.setId(null);
            personaSegipHist.setPersonaSegip(personaSegip);
            personaSegipHist.setFechaModificacion(new Date());
            personaSegipHist.setUsuarioModificador(request.getNombreOficial());
            merge(personaSegipHist);
        } catch (Exception e) {
            LOGGER.error("Error en el historico SEGIP {}", e);
        }
    }
}
