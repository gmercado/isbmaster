package com.bisa.bus.servicios.segip;

import bus.consumoweb.consumer.ClienteServiciosWeb;
import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.bus.servicios.segip.api.*;
import com.bisa.bus.servicios.segip.consumer.api.ConsumidorSegip;
import com.bisa.bus.servicios.segip.consumer.api.ConsumidorSegipAdmin;
import com.bisa.bus.servicios.segip.consumer.api.ConsumidorServiciosSegip;
import com.bisa.bus.servicios.segip.consumer.api.ConsumidorServiciosSegipAdmin;
import com.bisa.bus.servicios.segip.dao.*;

/**
 * @author josanchez
 * @author rsalvatierra modificado on 04/05/2016
 */
public class SegipModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(SegipProcessService.class).to(SegipProcessServiceImpl.class);
        bind(SegipService.class).to(SegipServiceImpl.class);
        bind(BitacoraSegipService.class).to(BitacoraSegipDao.class);
        bind(ContrastacionSegipService.class).to(ContrastacionSegipDao.class);
        bind(PersonaSegipService.class).to(PersonaSegipDao.class);
        bind(PersonaSegipHistService.class).to(PersonaSegipHistDao.class);
        bind(ObservacionesSegipService.class).to(ObservacionesSegipDao.class);
        bind(ClienteServiciosWeb.class).annotatedWith(ConsumidorSegip.class).to(ConsumidorServiciosSegip.class);
        bind(ClienteServiciosWeb.class).annotatedWith(ConsumidorSegipAdmin.class).to(ConsumidorServiciosSegipAdmin.class);
        bind(ParametrosSegipService.class).to(VariableSegipDao.class);
        bindUI("com/bisa/bus/servicios/segip/ui");
    }
}
