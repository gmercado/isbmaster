package com.bisa.bus.servicios.segip.api;

import com.bisa.bus.servicios.segip.xml.ReporteCertificacion;

/**
 * @author rsalvatierra on 09/03/2016.
 */
public interface IReporteConvert {
    ReporteCertificacion get(byte[] reportEncoded);
}
