package com.bisa.bus.servicios.segip.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.segip.api.ContrastacionSegipService;
import com.bisa.bus.servicios.segip.entities.ContrastacionSegip;
import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import com.bisa.bus.servicios.segip.model.ResultadoConsultaSegip;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegip;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;

/**
 * @author rsalvatierra on 24/03/2016.
 */
@AuthorizeInstantiation({RolesBisa.SEGIP_CONSUMO, RolesBisa.SEGIP_CONSULTAS})
@Titulo("Detalle Contrastaci\u00f3n")
public class DetalleContrastacion extends BisaWebPage {

    public static final String SEPARADOR_DOCUMENTO = "-";
    @Inject
    ContrastacionSegipService segip;

    private SolicitudConsultaSegip solicitud;
    private ResultadoConsultaSegip respuestaSegip;
    private PersonaSegip datosSegip;

    public DetalleContrastacion(SolicitudConsultaSegip solicitud, ResultadoConsultaSegip respuestaSegip) {
        super();
        this.solicitud = solicitud;
        this.respuestaSegip = respuestaSegip;
        inicio(getContrastacionSegip(solicitud));
    }

    public DetalleContrastacion(PersonaSegip datosSegip) {
        super();
        this.datosSegip = datosSegip;
        inicio(getContrastacionSegip(datosSegip));
    }

    public void inicio(ContrastacionSegip datosContrastacion) {

        IModel<ContrastacionSegip> objectModel = new CompoundPropertyModel<>(datosContrastacion);
        //Form
        Form<ContrastacionSegip> form = new Form<>("detalleContrastacion", objectModel);
        add(form);
        //Cuerpo
        form.add(new Label("codigoCliente"));
        form.add(new Label("nombres"));
        form.add(new Label("apellidoPaterno"));
        form.add(new Label("apellidoMaterno"));
        form.add(new Label("fechaNacimientoFormato"));
        form.add(new Label("documentoIdentidad"));
        form.add(new Label("detalleEstadoContrastacion"));
        form.add(new Label("fechaContrastacionFormato"));
        //botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                if (respuestaSegip != null) {
                    setResponsePage(new RespuestaSegip(solicitud, respuestaSegip));
                } else {
                    setResponsePage(new DetallePersonaSegip(datosSegip));
                }
            }
        });
    }

    public ContrastacionSegip getContrastacionSegip(SolicitudConsultaSegip solicitud) {
        ContrastacionSegip contrastacionSegip = segip.getPersonaContrastacion(solicitud.getNumeroDocumento(), solicitud.getComplemento(), solicitud.getFechaNacimiento());
        if (contrastacionSegip == null) {
            contrastacionSegip = segip.getPersonaContrastacion2(solicitud.getNumeroDocumento(), solicitud.getComplemento(), solicitud.getFechaNacimiento());
        }
        return contrastacionSegip;
    }

    public ContrastacionSegip getContrastacionSegip(PersonaSegip datosSegip) {
        String numDoc;
        String compl;
        if (datosSegip.getNumeroDocumento().contains(SEPARADOR_DOCUMENTO)) {
            String[] cadenas = datosSegip.getNumeroDocumento().split(SEPARADOR_DOCUMENTO);
            numDoc = StringUtils.trimToEmpty(cadenas[0]);
            compl = StringUtils.trimToEmpty(cadenas[1]);
        } else {
            numDoc = StringUtils.trimToEmpty(datosSegip.getNumeroDocumento());
            compl = null;
        }
        return segip.getPersonaContrastacion2(numDoc, compl, null);
    }
}
