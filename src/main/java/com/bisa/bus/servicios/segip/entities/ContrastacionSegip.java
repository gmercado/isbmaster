package com.bisa.bus.servicios.segip.entities;

import bus.plumbing.utils.FormatosUtils;
import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author josanchez on 03/03/2016.
 * @author rsalvatierra modificado on 20/04/2016
 */
@Entity
@Table(name = "SGP022")
public class ContrastacionSegip implements Serializable {

    @Id
    @Column(name = "S22CODIGO", columnDefinition = "NUMERIC(10)", nullable = false, unique = true)
    private Long codigo;

    @Column(name = "S22NUMCLI", columnDefinition = "VARCHAR(10)", nullable = false)
    private Long codigoCliente;

    @Column(name = "S22DOCIDE", columnDefinition = "VARCHAR(20)", nullable = false)
    private String documentoIdentidadCore;

    @Column(name = "S22COMPLE", columnDefinition = "VARCHAR(5)")
    private String complementoCore;

    @Column(name = "S22FECNAC", columnDefinition = "NUMERIC(8)")
    private Long fechaNacimientoCore;

    @Column(name = "S22CDOCID", columnDefinition = "VARCHAR(20)", nullable = false)
    private String documentoIdentidad;

    @Column(name = "S22CCOMPL", columnDefinition = "VARCHAR(5)")
    private String complemento;

    @Column(name = "S22CNOMBR", columnDefinition = "VARCHAR(60)", nullable = false)
    private String nombres;

    @Column(name = "S22CPRIAP", columnDefinition = "VARCHAR(30)")
    private String apellidoPaterno;

    @Column(name = "S22CSEGAP", columnDefinition = "VARCHAR(30)")
    private String apellidoMaterno;

    @Column(name = "S22CFECNA", columnDefinition = "NUMERIC(8)")
    private Long fechaNacimiento;

    @Column(name = "S22COBSER", columnDefinition = "VARCHAR(255)")
    private String observaciones;

    @Column(name = "S22ESTCON", columnDefinition = "VARCHAR(1)")
    private String estaContrastado;

    @Column(name = "S22NOMCON", columnDefinition = "VARCHAR(10)")
    private String nombreContrastacion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "S22FECCON")
    private Date fechaContrastacion;

    @Column(name = "S22ESTSOL", columnDefinition = "VARCHAR(1)")
    private String estadoSolicitud;

    public ContrastacionSegip() {
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public Long getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(Long codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getDocumentoIdentidadCore() {
        return documentoIdentidadCore;
    }

    public void setDocumentoIdentidadCore(String documentoIdentidadCore) {
        this.documentoIdentidadCore = documentoIdentidadCore;
    }

    public String getComplementoCore() {
        return complementoCore;
    }

    public void setComplementoCore(String complementoCore) {
        this.complementoCore = complementoCore;
    }

    public String getDocumentoIdentidad() {
        return documentoIdentidad;
    }

    public void setDocumentoIdentidad(String documentoIdentidad) {
        this.documentoIdentidad = documentoIdentidad;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public Date getFechaNacimiento() {
        return FormatosUtils.fechaYYYYMMDD(fechaNacimiento);
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = FormatosUtils.fechaYYYYMMDD(fechaNacimiento);
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getEstaContrastado() {
        return estaContrastado;
    }

    public void setEstaContrastado(String estaContrastado) {
        this.estaContrastado = estaContrastado;
    }

    public String getNombreContrastacion() {
        return nombreContrastacion;
    }

    public void setNombreContrastacion(String nombreContrastacion) {
        this.nombreContrastacion = nombreContrastacion;
    }

    public Date getFechaContrastacion() {
        return fechaContrastacion;
    }

    public void setFechaContrastacion(Date fechaContrastacion) {
        this.fechaContrastacion = fechaContrastacion;
    }

    public Date getFechaNacimientoCore() {
        return FormatosUtils.fechaYYYYMMDD(fechaNacimientoCore);
    }

    public void setFechaNacimientoCore(Date fechaNacimientoCore) {
        this.fechaNacimientoCore = FormatosUtils.fechaYYYYMMDD(fechaNacimientoCore);
    }

    public String getFechaNacimientoFormato() {
        return FormatosUtils.formatoFecha(FormatosUtils.fechaYYYYMMDD(fechaNacimiento));
    }

    public String getFechaContrastacionFormato() {
        return FormatosUtils.formatoFecha(fechaContrastacion);
    }

    public String getDetalleEstadoContrastacion() {
        return ("S".equals(getEstaContrastado()) ? "SI" : "NO");
    }

    public String getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(String estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }

    @Override
    public String toString() {
        return "ContrastacionSegip{" +
                "codigoCliente=" + getCodigoCliente() +
                ", nombres='" + StringUtils.trimToEmpty(getNombres()) + '\'' +
                ", apellidoPaterno='" + StringUtils.trimToEmpty(getApellidoPaterno()) + '\'' +
                ", apellidoMaterno='" + StringUtils.trimToEmpty(getApellidoMaterno()) + '\'' +
                ", fechaNacimiento=" + getFechaNacimiento() +
                ", documentoIdentidad='" + StringUtils.trimToEmpty(getDocumentoIdentidad()) + '\'' +
                ", complemento='" + StringUtils.trimToEmpty(getComplemento()) + '\'' +
                ", estaContrastado='" + StringUtils.trimToEmpty(getEstaContrastado()) + '\'' +
                ", nombrecontrastacion='" + StringUtils.trimToEmpty(getNombreContrastacion()) + '\'' +
                ", fechaContrastacion=" + getFechaContrastacion() +
                '}';
    }
}
