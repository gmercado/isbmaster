package com.bisa.bus.servicios.segip.model;

/**
 * @author josanchez on 17/03/2016.
 * @author rsalvatierra modificado on 01/04/2016
 */
public enum TipoCodigoRespuesta {

    NO_REALIZO_CONSULTA(0, "No se realizo la busqueda"),
    NO_EXiSTE(1, "No se encontro el registro"),
    OK(2, "Se encontro 1 registro"),
    DUPLICADO(3, "Se encontro mas de un registro"),
    OBSERVADO(4, "Registro con observacion");

    private int codigo;
    private String descripcion;

    TipoCodigoRespuesta(int codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
