package com.bisa.bus.servicios.segip.api;

import com.bisa.bus.servicios.segip.entities.BitacoraSegip;
import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import com.bisa.bus.servicios.segip.model.ResultadoConsultaSegip;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegip;

import java.util.List;

/**
 * @author rsalvatierra on 21/04/2016.
 */
public interface BitacoraSegipService {

    List<BitacoraSegip> getConsumos(BitacoraSegip model1, BitacoraSegip model2);

    void registrar(SolicitudConsultaSegip request, ResultadoConsultaSegip response, PersonaSegip personaSegip);

}
