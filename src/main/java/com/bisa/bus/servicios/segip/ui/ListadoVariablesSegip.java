package com.bisa.bus.servicios.segip.ui;

import bus.database.components.Listado;
import bus.database.components.ListadoDataProvider;
import bus.database.components.ListadoPanel;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;
import bus.plumbing.components.ToolButton;
import com.bisa.bus.servicios.segip.dao.VariableSegipDao;
import com.bisa.bus.servicios.segip.entities.VariableSegip;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.ArrayList;
import java.util.List;

/**
 * @author rsalvatierra on 01/06/2016
 */
@AuthorizeInstantiation({RolesBisa.SEGIP_PARAM})
@Menu(value = "Par\u00E1metros SEGIP", subMenu = SubMenu.SEGIP)
public class ListadoVariablesSegip extends Listado<VariableSegip, Long> {

    @Override
    protected ListadoPanel<VariableSegip, Long> newListadoPanel(String id) {
        return new ListadoPanel<VariableSegip, Long>(id) {
            @Override
            protected List<IColumn<VariableSegip, String>> newColumns(ListadoDataProvider<VariableSegip, Long> dataProvider) {
                ArrayList<IColumn<VariableSegip, String>> iColumns = new ArrayList<>();
                iColumns.add(new LinkPropertyColumn<VariableSegip>(Model.of("Nombre"), "nombre", "nombre") {
                    @Override
                    protected void onClick(VariableSegip object) {
                        setResponsePage(EditarVariableSegip.class, new PageParameters().add("id", object.getId()));
                    }
                });
                iColumns.add(new PropertyColumn<>(Model.of("Valor"), "valor"));
                return iColumns;
            }

            @Override
            protected void poblarBotones(RepeatingView rv, String idButton, Form<Void> filtroForm) {
                WebMarkupContainer wmk;
                rv.add(wmk = new WebMarkupContainer(rv.newChildId()));
                wmk.add(new ToolButton(idButton) {

                    @Override
                    public void onSubmit() {
                        setResponsePage(NuevaVariableSegip.class);
                    }
                }
                        .setClassAttribute("btn btn-info")
                        .setIconAttribute("icon-file icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Crear par\u00E1metro")));
            }

            @Override
            protected Class<? extends Dao<VariableSegip, Long>> getProviderClazz() {
                return VariableSegipDao.class;
            }
        };
    }
}
