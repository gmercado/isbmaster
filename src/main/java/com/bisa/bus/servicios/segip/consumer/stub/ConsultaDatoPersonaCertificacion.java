//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.02.01 a las 04:08:40 PM BOT 
//


package com.bisa.bus.servicios.segip.consumer.stub;


/**
 * <p>Clase Java para anonymous complex type.
 * <p/>
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p/>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pCodigoInstitucion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="pUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pContrasenia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pClaveAccesoUsuarioFinal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pNumeroAutorizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pNumeroDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pComplemento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pPrimerApellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pSegundoApellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pFechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
//@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "", propOrder = {
//    "pCodigoInstitucion",
//    "pUsuario",
//    "pContrasenia",
//    "pClaveAccesoUsuarioFinal",
//    "pNumeroAutorizacion",
//    "pNumeroDocumento",
//    "pComplemento",
//    "pNombre",
//    "pPrimerApellido",
//    "pSegundoApellido",
//    "pFechaNacimiento"
//})
//@XmlRootElement(name = "ConsultaDatoPersonaCertificacion")
public class ConsultaDatoPersonaCertificacion {

    private Integer pCodigoInstitucion;
    private String pUsuario;
    private String pContrasenia;
    private String pClaveAccesoUsuarioFinal;
    private String pNumeroAutorizacion;
    private String pNumeroDocumento;
    private String pComplemento;
    private String pNombre;
    private String pPrimerApellido;
    private String pSegundoApellido;
    private String pFechaNacimiento;

    public Integer getpCodigoInstitucion() {
        return pCodigoInstitucion;
    }

    public void setpCodigoInstitucion(Integer pCodigoInstitucion) {
        this.pCodigoInstitucion = pCodigoInstitucion;
    }

    public String getpUsuario() {
        return pUsuario;
    }

    public void setpUsuario(String pUsuario) {
        this.pUsuario = pUsuario;
    }

    public String getpContrasenia() {
        return pContrasenia;
    }

    public void setpContrasenia(String pContrasenia) {
        this.pContrasenia = pContrasenia;
    }

    public String getpClaveAccesoUsuarioFinal() {
        return pClaveAccesoUsuarioFinal;
    }

    public void setpClaveAccesoUsuarioFinal(String pClaveAccesoUsuarioFinal) {
        this.pClaveAccesoUsuarioFinal = pClaveAccesoUsuarioFinal;
    }

    public String getpNumeroAutorizacion() {
        return pNumeroAutorizacion;
    }

    public void setpNumeroAutorizacion(String pNumeroAutorizacion) {
        this.pNumeroAutorizacion = pNumeroAutorizacion;
    }

    public String getpNumeroDocumento() {
        return pNumeroDocumento;
    }

    public void setpNumeroDocumento(String pNumeroDocumento) {
        this.pNumeroDocumento = pNumeroDocumento;
    }

    public String getpComplemento() {
        return pComplemento;
    }

    public void setpComplemento(String pComplemento) {
        this.pComplemento = pComplemento;
    }

    public String getpNombre() {
        return pNombre;
    }

    public void setpNombre(String pNombre) {
        this.pNombre = pNombre;
    }

    public String getpPrimerApellido() {
        return pPrimerApellido;
    }

    public void setpPrimerApellido(String pPrimerApellido) {
        this.pPrimerApellido = pPrimerApellido;
    }

    public String getpSegundoApellido() {
        return pSegundoApellido;
    }

    public void setpSegundoApellido(String pSegundoApellido) {
        this.pSegundoApellido = pSegundoApellido;
    }

    public String getpFechaNacimiento() {
        return pFechaNacimiento;
    }

    public void setpFechaNacimiento(String pFechaNacimiento) {
        this.pFechaNacimiento = pFechaNacimiento;
    }

//    protected Integer pCodigoInstitucion;
//    @XmlElementRef(name = "pUsuario", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
//    protected JAXBElement<String> pUsuario;
//    @XmlElementRef(name = "pContrasenia", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
//    protected JAXBElement<String> pContrasenia;
//    @XmlElementRef(name = "pClaveAccesoUsuarioFinal", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
//    protected JAXBElement<String> pClaveAccesoUsuarioFinal;
//    @XmlElementRef(name = "pNumeroAutorizacion", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
//    protected JAXBElement<String> pNumeroAutorizacion;
//    @XmlElementRef(name = "pNumeroDocumento", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
//    protected JAXBElement<String> pNumeroDocumento;
//    @XmlElementRef(name = "pComplemento", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
//    protected JAXBElement<String> pComplemento;
//    @XmlElementRef(name = "pNombre", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
//    protected JAXBElement<String> pNombre;
//    @XmlElementRef(name = "pPrimerApellido", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
//    protected JAXBElement<String> pPrimerApellido;
//    @XmlElementRef(name = "pSegundoApellido", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
//    protected JAXBElement<String> pSegundoApellido;
//    @XmlElementRef(name = "pFechaNacimiento", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
//    protected JAXBElement<String> pFechaNacimiento;
//
//    /**
//     * Obtiene el valor de la propiedad pCodigoInstitucion.
//     *
//     * @return
//     *     possible object is
//     *     {@link Integer }
//     *
//     */
//    public Integer getPCodigoInstitucion() {
//        return pCodigoInstitucion;
//    }
//
//    /**
//     * Define el valor de la propiedad pCodigoInstitucion.
//     *
//     * @param value
//     *     allowed object is
//     *     {@link Integer }
//     *
//     */
//    public void setPCodigoInstitucion(Integer value) {
//        this.pCodigoInstitucion = value;
//    }
//
//    /**
//     * Obtiene el valor de la propiedad pUsuario.
//     *
//     * @return
//     *     possible object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public JAXBElement<String> getPUsuario() {
//        return pUsuario;
//    }
//
//    /**
//     * Define el valor de la propiedad pUsuario.
//     *
//     * @param value
//     *     allowed object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public void setPUsuario(JAXBElement<String> value) {
//        this.pUsuario = value;
//    }
//
//    /**
//     * Obtiene el valor de la propiedad pContrasenia.
//     *
//     * @return
//     *     possible object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public JAXBElement<String> getPContrasenia() {
//        return pContrasenia;
//    }
//
//    /**
//     * Define el valor de la propiedad pContrasenia.
//     *
//     * @param value
//     *     allowed object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public void setPContrasenia(JAXBElement<String> value) {
//        this.pContrasenia = value;
//    }
//
//    /**
//     * Obtiene el valor de la propiedad pClaveAccesoUsuarioFinal.
//     *
//     * @return
//     *     possible object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public JAXBElement<String> getPClaveAccesoUsuarioFinal() {
//        return pClaveAccesoUsuarioFinal;
//    }
//
//    /**
//     * Define el valor de la propiedad pClaveAccesoUsuarioFinal.
//     *
//     * @param value
//     *     allowed object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public void setPClaveAccesoUsuarioFinal(JAXBElement<String> value) {
//        this.pClaveAccesoUsuarioFinal = value;
//    }
//
//    /**
//     * Obtiene el valor de la propiedad pNumeroAutorizacion.
//     *
//     * @return
//     *     possible object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public JAXBElement<String> getPNumeroAutorizacion() {
//        return pNumeroAutorizacion;
//    }
//
//    /**
//     * Define el valor de la propiedad pNumeroAutorizacion.
//     *
//     * @param value
//     *     allowed object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public void setPNumeroAutorizacion(JAXBElement<String> value) {
//        this.pNumeroAutorizacion = value;
//    }
//
//    /**
//     * Obtiene el valor de la propiedad pNumeroDocumento.
//     *
//     * @return
//     *     possible object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public JAXBElement<String> getPNumeroDocumento() {
//        return pNumeroDocumento;
//    }
//
//    /**
//     * Define el valor de la propiedad pNumeroDocumento.
//     *
//     * @param value
//     *     allowed object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public void setPNumeroDocumento(JAXBElement<String> value) {
//        this.pNumeroDocumento = value;
//    }
//
//    /**
//     * Obtiene el valor de la propiedad pComplemento.
//     *
//     * @return
//     *     possible object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public JAXBElement<String> getPComplemento() {
//        return pComplemento;
//    }
//
//    /**
//     * Define el valor de la propiedad pComplemento.
//     *
//     * @param value
//     *     allowed object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public void setPComplemento(JAXBElement<String> value) {
//        this.pComplemento = value;
//    }
//
//    /**
//     * Obtiene el valor de la propiedad pNombre.
//     *
//     * @return
//     *     possible object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public JAXBElement<String> getPNombre() {
//        return pNombre;
//    }
//
//    /**
//     * Define el valor de la propiedad pNombre.
//     *
//     * @param value
//     *     allowed object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public void setPNombre(JAXBElement<String> value) {
//        this.pNombre = value;
//    }
//
//    /**
//     * Obtiene el valor de la propiedad pPrimerApellido.
//     *
//     * @return
//     *     possible object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public JAXBElement<String> getPPrimerApellido() {
//        return pPrimerApellido;
//    }
//
//    /**
//     * Define el valor de la propiedad pPrimerApellido.
//     *
//     * @param value
//     *     allowed object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public void setPPrimerApellido(JAXBElement<String> value) {
//        this.pPrimerApellido = value;
//    }
//
//    /**
//     * Obtiene el valor de la propiedad pSegundoApellido.
//     *
//     * @return
//     *     possible object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public JAXBElement<String> getPSegundoApellido() {
//        return pSegundoApellido;
//    }
//
//    /**
//     * Define el valor de la propiedad pSegundoApellido.
//     *
//     * @param value
//     *     allowed object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public void setPSegundoApellido(JAXBElement<String> value) {
//        this.pSegundoApellido = value;
//    }
//
//    /**
//     * Obtiene el valor de la propiedad pFechaNacimiento.
//     *
//     * @return
//     *     possible object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public JAXBElement<String> getPFechaNacimiento() {
//        return pFechaNacimiento;
//    }
//
//    /**
//     * Define el valor de la propiedad pFechaNacimiento.
//     *
//     * @param value
//     *     allowed object is
//     *     {@link JAXBElement }{@code <}{@link String }{@code >}
//     *
//     */
//    public void setPFechaNacimiento(JAXBElement<String> value) {
//        this.pFechaNacimiento = value;
//    }

}
