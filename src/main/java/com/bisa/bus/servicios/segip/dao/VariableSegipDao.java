package com.bisa.bus.servicios.segip.dao;

import bus.config.dao.CryptUtils;
import bus.database.dao.DaoImplCached;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.segip.api.ParametrosSegipService;
import com.bisa.bus.servicios.segip.entities.Oficina;
import com.bisa.bus.servicios.segip.entities.VariableSegip;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.apache.commons.configuration.Configuration;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.timer.Timer;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author rsalvatierra on 01/06/2016
 */
public class VariableSegipDao extends DaoImplCached<VariableSegip, Long> implements ParametrosSegipService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VariableSegipDao.class);

    private final CryptUtils cryptUtils;
    private final Configuration configuration;

    @Inject
    public VariableSegipDao(@BasePrincipal EntityManagerFactory entityManagerFactory,
                            Ehcache cache, CryptUtils cryptUtils, Configuration configuration) {
        super(entityManagerFactory, cache);
        this.cryptUtils = cryptUtils;
        this.configuration = configuration;
    }

    @Override
    public VariableSegip merge(VariableSegip data) {
        VariableSegip merged = super.merge(data);
        ehcache.remove(merged.getNombre());
        return merged;
    }

    @Override
    public VariableSegip remove(Long id) {
        VariableSegip removed = super.remove(id);
        ehcache.remove(removed.getNombre());
        return removed;
    }

    @Override
    protected Path<Long> countPath(Root<VariableSegip> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<VariableSegip> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.<String>get("nombre"));
        paths.add(p.<String>get("valor"));
        return paths;
    }

    @Override
    public String getValorDe(String nombreVariable, String valorPorDefecto) {
        Element element = ehcache.get(nombreVariable);
        if (element != null && !element.isExpired() && element.getObjectValue() != null) {
            return (String) element.getObjectValue();
        }
        VariableSegip variable;
        try {
            variable = doWithTransaction((entityManager, t) -> {
                OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
                OpenJPACriteriaQuery<VariableSegip> query = cb.createQuery(VariableSegip.class);
                query.where(cb.equal(query.from(VariableSegip.class).get("nombre"), nombreVariable));

                TypedQuery<VariableSegip> typedQuery = entityManager.createQuery(query);
                List<VariableSegip> resultList = typedQuery.getResultList();
                if (resultList.isEmpty()) {
                    return null;
                }
                if (resultList.size() > 1) {
                    LOGGER.warn("Buscando una variable con mas de un registro, {}, deviolviendo el primero", resultList);
                }
                return resultList.get(0);
            });
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error al obtener el valor de la variable, asumiendo el valor por defecto", e);
            variable = null;
        }

        boolean putInCache = true;

        String valor;
        if (variable == null) {
            putInCache = false;
            valor = configuration.getString(nombreVariable, valorPorDefecto);
        } else {
            valor = variable.getValor();
        }

        if (cryptUtils.esCryptLegado(valor)) {
            valor = cryptUtils.dec(valor);
        } else if (cryptUtils.esOpenssl(valor)) {
            valor = cryptUtils.decssl(valor);
        }
        valor = Joiner.on("\n").join(Iterables.filter(Splitter.on("\n").split(valor), input -> input != null && !input.startsWith("// ")));

        if (putInCache) {
            element = new Element(nombreVariable, valor);
            element.setTimeToLive((int) (Timer.ONE_DAY / 1000L));
            ehcache.put(element);
        }
        LOGGER.debug("valores parametros << {}  >> {}", nombreVariable, valor);
        return valor;
    }

    @Override
    public Integer getValorIntDe(String nombreVariable, Integer valorPorDefecto) {
        try {
            return Integer.parseInt(getValorDe(nombreVariable, ""));
        } catch (NumberFormatException e) {
            LOGGER.warn("Utilizando el valor por defecto para la variable entera {}", nombreVariable);
            return valorPorDefecto;
        }
    }

    @Override
    public List<VariableSegip> getCanales() {
        LOGGER.debug("Obteniendo canales...");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<VariableSegip> q = cb.createQuery(VariableSegip.class);
                        Root<VariableSegip> p = q.from(VariableSegip.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.like(p.get("nombre"), "canal.%"));

                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<VariableSegip> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta: {}", e);
            return null;
        }
    }

    @Override
    public List<Oficina> getOficinas() {
        LOGGER.debug("Obteniendo oficinas...");
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Oficina> q = cb.createQuery(Oficina.class);
                        Root<Oficina> p = q.from(Oficina.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.notEqual(p.get("codigo"), "100"));
                        predicatesAnd.add(cb.notLike(p.get("nombre"), "%CIDRE%"));
                        predicatesAnd.add(cb.equal(p.get("estado"), "0"));

                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        q.orderBy(cb.asc(p.get("codigo")));
                        TypedQuery<Oficina> query = entityManager.createQuery(q);
                        return query.getResultList();
                    });
        } catch (Exception e) {
            LOGGER.error("Error en la consulta: {}", e);
            return null;
        }
    }
}
