package com.bisa.bus.servicios.segip.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.segip.api.PersonaSegipHistService;
import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import com.bisa.bus.servicios.segip.model.HistoricoSegip;
import com.bisa.bus.servicios.segip.model.TipoCodigoError;
import com.google.inject.Inject;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.core.util.lang.PropertyResolver;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.stream.Collectors;

/**
 * @author rsalvatierra on 23/03/2016.
 */
@AuthorizeInstantiation({RolesBisa.SEGIP_CONSUMO, RolesBisa.SEGIP_CONSULTAS})
@Titulo("Hist\u00f3rico Datos Persona - SEGIP")
public class HistoricoPersonaSegip extends BisaWebPage {

    @Inject
    PersonaSegipHistService personaSegipHistService;
    private PersonaSegip datosSegip;

    public HistoricoPersonaSegip() {
        super();
    }

    public HistoricoPersonaSegip(PersonaSegip datosSegip) {
        super();
        inicio(datosSegip);
    }

    private void inicio(PersonaSegip datosSegip) {
        this.datosSegip = datosSegip;
        LinkedList<IColumn<HistoricoSegip, String>> iColumns = new LinkedList<>();
        iColumns.add(new PropertyColumn<HistoricoSegip, String>(Model.of("Fecha"), "fechaConsumo") {
            @Override
            public void populateItem(Item<ICellPopulator<HistoricoSegip>> item, String componentId, IModel<HistoricoSegip> rowModel) {
                HistoricoSegip hist = rowModel.getObject();
                item.add(new Label(componentId, hist.getFechaConsumo()).add(new AttributeAppender("style", Model.of("text-align:center"))));
            }
        });
        iColumns.add(new PropertyColumn<>(Model.of("Usuario"), "codUsuario"));
        iColumns.add(new PropertyColumn<>(Model.of("Origen"), "origenSolicitud"));
        iColumns.add(new PropertyColumn<>(Model.of("Estado"), "estadoConsulta"));
        iColumns.add(new PropertyColumn<HistoricoSegip, String>(Model.of("Cod. Error"), "codError") {
            @Override
            public void populateItem(Item<ICellPopulator<HistoricoSegip>> item, String componentId, IModel<HistoricoSegip> rowModel) {
                HistoricoSegip hist = rowModel.getObject();
                item.add(new Label(componentId, hist.getCodError() + "-" + TipoCodigoError.get(hist.getCodError())));
            }
        });
        iColumns.add(new PropertyColumn<HistoricoSegip, String>(Model.of("Versi\u00f3n"), "nroRevision", "nroRevision") {
            @Override
            public void populateItem(Item<ICellPopulator<HistoricoSegip>> item, String componentId, IModel<HistoricoSegip> rowModel) {
                HistoricoSegip hist = rowModel.getObject();
                item.add(new Label(componentId, hist.getNroRevision()).add(new AttributeAppender("style", Model.of("text-align:right"))));
            }
        });
        iColumns.add(new PropertyColumn<>(Model.of("C\u00f3digo \u00fanico"), "codigoUnico"));
        iColumns.add(new PropertyColumn<>(Model.of("Nombres"), "nombre"));
        iColumns.add(new PropertyColumn<>(Model.of("Apellido Paterno"), "primerApellido"));
        iColumns.add(new PropertyColumn<>(Model.of("Apellido Materno"), "segundoApellido"));
        iColumns.add(new PropertyColumn<>(Model.of("Fecha Nacimiento"), "fechaNacFormato"));
        iColumns.add(new PropertyColumn<>(Model.of("Observaciones"), "observaciones"));

        SortableDataProvider<HistoricoSegip, String> sortableDataProvider = new ColasDataProvider();
        add(new AjaxFallbackDefaultDataTable<>("datosHistoricos", iColumns, sortableDataProvider, 5));

        IModel<PersonaSegip> objectModel = new CompoundPropertyModel<>(datosSegip);
        //Form
        Form<PersonaSegip> form = new Form<>("historico", objectModel);
        add(form);
        //botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(new DetallePersonaSegip(datosSegip));
            }
        });
    }

    private class ColasDataProvider extends SortableDataProvider<HistoricoSegip, String> {

        private final LinkedList<HistoricoSegip> colas;

        {
            colas = new LinkedList<>();
            colas.addAll(personaSegipHistService.getHistorico2(datosSegip).stream().collect(Collectors.toList()));
        }

        @Override
        public Iterator<? extends HistoricoSegip> iterator(long first, long count) {
            SortParam sort = getSort();
            if (sort != null) {
                Collections.sort(colas, (o1, o2) -> {
                    Integer value1 = ((Number) PropertyResolver.getValue(getSort().getProperty(), o1)).intValue();
                    Integer value2 = ((Number) PropertyResolver.getValue(getSort().getProperty(), o2)).intValue();
                    if (getSort().isAscending()) {
                        return value1.compareTo(value2);
                    } else {
                        return value2.compareTo(value1);
                    }
                });
            }
            return colas.subList((int) first, (int) (first + count)).iterator();
        }

        @Override
        public long size() {
            return colas.size();
        }

        @Override
        public IModel<HistoricoSegip> model(HistoricoSegip object) {
            return Model.of(object);
        }
    }
}
