package com.bisa.bus.servicios.segip.model;

/**
 * @author rsalvatierra on 04/04/2016.
 */
public enum TipoCanal {
    AS400("AS400"),
    STI("STI"),
    AQUA2("AQUA2"),
    PCBISA("PCBISA");

    private String descripcion;

    TipoCanal(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return "TipoSolicitud{" +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }

}
