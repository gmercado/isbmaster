package com.bisa.bus.servicios.segip.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author rsalvatierra on 01/06/2016
 */
@Entity
@Table(name = "SGP091")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S91FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S91USRALT", nullable = false, columnDefinition = "CHAR(255) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S91FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S91USRMOD", columnDefinition = "CHAR(255)"))
})
public class ObservacionesSegip extends EntityBase implements Serializable {

    @Id
    @Column(name = "S91CODERR", columnDefinition = "CHAR(4)")
    private String codigo;

    @Column(name = "S91DESMEN", columnDefinition = "CHAR(1000)")
    private String descripcion;

    @Version
    @Column(name = "S91NVER", columnDefinition = "NUMERIC(5)")
    private long version;

    public ObservacionesSegip() {
    }

    public ObservacionesSegip(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }
}
