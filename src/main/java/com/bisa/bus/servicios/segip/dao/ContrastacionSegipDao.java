package com.bisa.bus.servicios.segip.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.segip.api.ContrastacionSegipService;
import com.bisa.bus.servicios.segip.api.ParametrosSegipService;
import com.bisa.bus.servicios.segip.entities.ContrastacionSegip;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.LinkedList;

import static com.bisa.bus.servicios.segip.api.ConfiguracionSegip.*;

/**
 * @author josanchez on 03/03/2016.
 */
public class ContrastacionSegipDao extends DaoImpl<ContrastacionSegip, Long> implements Serializable, ContrastacionSegipService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContrastacionSegipDao.class);

    private final ParametrosSegipService parametrosSegipService;

    @Inject
    protected ContrastacionSegipDao(@BasePrincipal EntityManagerFactory entityManagerFactory, ParametrosSegipService parametrosSegipService) {
        super(entityManagerFactory);
        this.parametrosSegipService = parametrosSegipService;
    }

    @Override
    public ContrastacionSegip getPersonaContrastacion(String pNumeroDocumento, String pComplemento, String pFechaNacimiento) {
        final String pContrastacion = this.parametrosSegipService.getValorDe(NOMBRE_CONTRASTACION_VIGENTE, NOMBRE_CONTRASTACION_VIGENTE_DEFAULT);
        LOGGER.debug("Obteniendo registro Contrastacion por Numero Documento:{} y complemento {}.", pNumeroDocumento, pComplemento);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<ContrastacionSegip> q = cb.createQuery(ContrastacionSegip.class);
                        Root<ContrastacionSegip> p = q.from(ContrastacionSegip.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("documentoIdentidadCore"), StringUtils.trimToEmpty(pNumeroDocumento)));
                        if (!StringUtils.isEmpty(pComplemento) && !StringUtils.isBlank(pComplemento)) {
                            predicatesAnd.add(cb.equal(p.get("complementoCore"), StringUtils.trimToEmpty(pComplemento)));
                        }
                        if (!StringUtils.isEmpty(pFechaNacimiento) && !StringUtils.isBlank(pFechaNacimiento)) {
                            predicatesAnd.add(cb.equal(p.get("fechaNacimientoCore"), FormatosUtils.fechaYYYYMMDD(FormatosUtils.getFechaByFormatoYYYY(pFechaNacimiento))));
                        }
                        predicatesAnd.add(cb.equal(p.get("nombreContrastacion"), StringUtils.trimToEmpty(pContrastacion)));
                        predicatesAnd.add(cb.equal(p.get("estaContrastado"), REGISTRO_CONTRASTADO));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<ContrastacionSegip> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                            return query.getResultList().get(0);
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta: {}", e);
            return null;
        }
    }

    @Override
    public ContrastacionSegip getPersonaContrastacion2(String pNumeroDocumento, String pComplemento, String pFechaNacimiento) {
        final String pContrastacion = this.parametrosSegipService.getValorDe(NOMBRE_CONTRASTACION_VIGENTE, NOMBRE_CONTRASTACION_VIGENTE_DEFAULT);
        LOGGER.debug("Obteniendo registro Contrastacion2 por Numero Documento:{} y complemento {}.", pNumeroDocumento, pComplemento);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<ContrastacionSegip> q = cb.createQuery(ContrastacionSegip.class);
                        Root<ContrastacionSegip> p = q.from(ContrastacionSegip.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("documentoIdentidad"), StringUtils.trimToEmpty(pNumeroDocumento)));
                        if (!StringUtils.isEmpty(pComplemento) && !StringUtils.isBlank(pComplemento)) {
                            predicatesAnd.add(cb.equal(p.get("complemento"), StringUtils.trimToEmpty(pComplemento)));
                        }
                        if (!StringUtils.isEmpty(pFechaNacimiento) && !StringUtils.isBlank(pFechaNacimiento)) {
                            predicatesAnd.add(cb.equal(p.get("fechaNacimiento"), FormatosUtils.fechaYYYYMMDD(FormatosUtils.getFechaByFormatoYYYY(pFechaNacimiento))));
                        }
                        predicatesAnd.add(cb.equal(p.get("nombreContrastacion"), StringUtils.trimToEmpty(pContrastacion)));
                        predicatesAnd.add(cb.equal(p.get("estaContrastado"), REGISTRO_CONTRASTADO));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<ContrastacionSegip> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else if (query.getResultList() != null && query.getResultList().size() > 1) {
                            return query.getResultList().get(0);
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta: {}", e);
            return null;
        }
    }
}
