package com.bisa.bus.servicios.segip.model;

/**
 * @author josanchez on 11/03/2016.
 * @author rsalvatierra modificado on 01/04/2016
 */
public enum EstadoPersona {

    ACTIVO(1, "AC"),
    HISTORICO(2, "HI"),
    ANULADO(3, "AN"),
    BAJA(4, "BA"),
    OBSERVADO(5, "OB"),
    PENDIENTE(6, "PE");

    private int codigo;
    private String descripcion;

    EstadoPersona(int codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return "EnumEstadoPersonaSegip{" +
                "codigo=" + codigo +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
