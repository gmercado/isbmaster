package com.bisa.bus.servicios.segip.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.segip.dao.ObservacionesSegipDao;
import com.bisa.bus.servicios.segip.entities.ObservacionesSegip;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.validation.validator.PatternValidator;
import org.apache.wicket.validation.validator.StringValidator;

import java.util.Date;

import static bus.users.api.Metadatas.USER_META_DATA_KEY;

/**
 * @author rsalvatierra on 01/06/2016
 */
@Titulo("Creaci\u00f3n de observaciones SEGIP")
@AuthorizeInstantiation({RolesBisa.SEGIP_PARAM})
public class NuevaObservacionSegip extends BisaWebPage {

    private final IModel<ObservacionesSegip> observacionesSegipIModel;

    public NuevaObservacionSegip() {
        super();

        observacionesSegipIModel = new CompoundPropertyModel<>(Model.of(new ObservacionesSegip()));

        Form<ObservacionesSegip> crear;
        add(crear = new Form<>("nueva", observacionesSegipIModel));

        TextField<String> nombre;
        crear.add(nombre = new RequiredTextField<>("codigo"));

        nombre.add(StringValidator.lengthBetween(3, 5));
        nombre.add(new PatternValidator("[A-Za-z0-9_.-]*"));
        nombre.setOutputMarkupId(true);

        TextArea<String> valor;
        crear.add(valor = new TextArea<>("descripcion"));

        valor.setRequired(true);
        valor.add(StringValidator.maximumLength(1000));
        valor.setOutputMarkupId(true);

        crear.add(new IndicatingAjaxButton("crear") {

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                try {
                    ObservacionesSegip observacionesSegip = observacionesSegipIModel.getObject();
                    observacionesSegip.setUsuarioCreador(getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase());
                    observacionesSegip.setFechaCreacion(new Date());
                    getInstance(ObservacionesSegipDao.class).persist(observacionesSegip);
                    info("Variable creada");
                    setResponsePage(ListadoObservacionesSegip.class);
                } catch (Exception e) {
                    LOGGER.error("Error inesperado al guardar variable", e);
                    error(e.getMessage());
                    target.add(feedbackPanel);
                }
            }
        });

        crear.add(new Button("cancelar") {
            @Override
            public void onSubmit() {
                setResponsePage(ListadoObservacionesSegip.class);
                getSession().info("Operaci\u00f3n cancelada");
            }
        }.setDefaultFormProcessing(false));
    }
}
