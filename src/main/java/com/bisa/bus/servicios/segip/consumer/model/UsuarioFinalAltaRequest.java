package com.bisa.bus.servicios.segip.consumer.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author rsalvatierra on 05/05/2016.
 */
@XmlRootElement(name = "UsuarioFinalAlta", namespace = "http://tempuri.org/")
public class UsuarioFinalAltaRequest implements Serializable {
    private int pIdInstitucion;
    private String pUsuario;
    private String pContrasenia;
    private String pNumeroCedula;
    private String pComplemento;
    private String pNombre;
    private String pPrimerApellido;
    private String pSegundoApellido;
    private String pFechaNacimiento;
    private String pNombreInstitucion;
    private String pNombreCargo;
    private String pDepartamento;
    private String pProvincia;
    private String pSeccionMunicipal;
    private String pLocalidad;
    private String pOficina;
    private String pCorreoElectronico;
    private String pTelefono;
    private String pTelefonoOficina;

    public UsuarioFinalAltaRequest() {
    }

    public int getpIdInstitucion() {
        return pIdInstitucion;
    }

    public void setpIdInstitucion(int pIdInstitucion) {
        this.pIdInstitucion = pIdInstitucion;
    }

    public String getpUsuario() {
        return pUsuario;
    }

    public void setpUsuario(String pUsuario) {
        this.pUsuario = pUsuario;
    }

    public String getpContrasenia() {
        return pContrasenia;
    }

    public void setpContrasenia(String pContrasenia) {
        this.pContrasenia = pContrasenia;
    }

    public String getpNumeroCedula() {
        return pNumeroCedula;
    }

    public void setpNumeroCedula(String pNumeroCedula) {
        this.pNumeroCedula = pNumeroCedula;
    }

    public String getpComplemento() {
        return pComplemento;
    }

    public void setpComplemento(String pComplemento) {
        this.pComplemento = pComplemento;
    }

    public String getpNombre() {
        return pNombre;
    }

    public void setpNombre(String pNombre) {
        this.pNombre = pNombre;
    }

    public String getpPrimerApellido() {
        return pPrimerApellido;
    }

    public void setpPrimerApellido(String pPrimerApellido) {
        this.pPrimerApellido = pPrimerApellido;
    }

    public String getpSegundoApellido() {
        return pSegundoApellido;
    }

    public void setpSegundoApellido(String pSegundoApellido) {
        this.pSegundoApellido = pSegundoApellido;
    }

    public String getpFechaNacimiento() {
        return pFechaNacimiento;
    }

    public void setpFechaNacimiento(String pFechaNacimiento) {
        this.pFechaNacimiento = pFechaNacimiento;
    }

    public String getpNombreInstitucion() {
        return pNombreInstitucion;
    }

    public void setpNombreInstitucion(String pNombreInstitucion) {
        this.pNombreInstitucion = pNombreInstitucion;
    }

    public String getpNombreCargo() {
        return pNombreCargo;
    }

    public void setpNombreCargo(String pNombreCargo) {
        this.pNombreCargo = pNombreCargo;
    }

    public String getpDepartamento() {
        return pDepartamento;
    }

    public void setpDepartamento(String pDepartamento) {
        this.pDepartamento = pDepartamento;
    }

    public String getpProvincia() {
        return pProvincia;
    }

    public void setpProvincia(String pProvincia) {
        this.pProvincia = pProvincia;
    }

    public String getpSeccionMunicipal() {
        return pSeccionMunicipal;
    }

    public void setpSeccionMunicipal(String pSeccionMunicipal) {
        this.pSeccionMunicipal = pSeccionMunicipal;
    }

    public String getpLocalidad() {
        return pLocalidad;
    }

    public void setpLocalidad(String pLocalidad) {
        this.pLocalidad = pLocalidad;
    }

    public String getpOficina() {
        return pOficina;
    }

    public void setpOficina(String pOficina) {
        this.pOficina = pOficina;
    }

    public String getpCorreoElectronico() {
        return pCorreoElectronico;
    }

    public void setpCorreoElectronico(String pCorreoElectronico) {
        this.pCorreoElectronico = pCorreoElectronico;
    }

    public String getpTelefono() {
        return pTelefono;
    }

    public void setpTelefono(String pTelefono) {
        this.pTelefono = pTelefono;
    }

    public String getpTelefonoOficina() {
        return pTelefonoOficina;
    }

    public void setpTelefonoOficina(String pTelefonoOficina) {
        this.pTelefonoOficina = pTelefonoOficina;
    }
}
