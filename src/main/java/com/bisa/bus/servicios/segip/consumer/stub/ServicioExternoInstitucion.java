package com.bisa.bus.servicios.segip.consumer.stub;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import java.net.URL;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.7-b01-
 * Generated source version: 2.1
 */
@WebServiceClient(name = ServicioExternoInstitucion.SERVICE, targetNamespace = ServicioExternoInstitucion.NAMESPACE, wsdlLocation = ServicioExternoInstitucion.WSDL)
public class ServicioExternoInstitucion
        extends Service {

    public static final String NAMESPACE = "http://tempuri.org/";
    public static final String SERVICE = "ServicioExternoInstitucion";
    public static final String PORT = "httpBasicConfig";
    public static final String WSDL = "/com/bisa/bus/servicios/segip/consumer/wsdl/ServicioExternoInstitucion.wsdl";
    private final static URL SERVICIOEXTERNOINSTITUCION_WSDL_LOCATION;

    static {
        SERVICIOEXTERNOINSTITUCION_WSDL_LOCATION = ServicioExternoInstitucion.class.getResource(WSDL);
    }

    public ServicioExternoInstitucion(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ServicioExternoInstitucion() {
        super(SERVICIOEXTERNOINSTITUCION_WSDL_LOCATION, new QName(NAMESPACE, SERVICE));
    }

    /**
     * @return returns IServicioExternoInstitucion
     */
    @WebEndpoint(name = PORT)
    public IServicioExternoInstitucion getHttpBasicConfig() {
        return super.getPort(new QName(NAMESPACE, PORT), IServicioExternoInstitucion.class);
    }

    /**
     * @param features A list of {@link WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return returns IServicioExternoInstitucion
     */
    @WebEndpoint(name = PORT)
    public IServicioExternoInstitucion getHttpBasicConfig(WebServiceFeature... features) {
        return super.getPort(new QName(NAMESPACE, PORT), IServicioExternoInstitucion.class, features);
    }
}
