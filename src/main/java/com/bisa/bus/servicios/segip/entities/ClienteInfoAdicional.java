package com.bisa.bus.servicios.segip.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author rsalvatierra on 30/05/2016
 */
@Entity
@Table(name = "CUP02701")
public class ClienteInfoAdicional implements Serializable {

    @Id
    @Column(name = "CUNBR", columnDefinition = "CHAR(10)")
    private String id;

    @Column(name = "CUTEN5", columnDefinition = "CHAR(10)")
    private String fechaVencimiento;

    public ClienteInfoAdicional() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    @Override
    public String toString() {
        return "ClienteInfoAdicional{" +
                "id='" + id + '\'' +
                ", fechaVencimiento='" + fechaVencimiento + '\'' +
                '}';
    }
}
