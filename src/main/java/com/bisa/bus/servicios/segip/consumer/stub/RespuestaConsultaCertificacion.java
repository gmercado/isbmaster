//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.02.01 a las 04:08:40 PM BOT 
//


package com.bisa.bus.servicios.segip.consumer.stub;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RespuestaConsultaCertificacion complex type.
 * <p/>
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p/>
 * <pre>
 * &lt;complexType name="RespuestaConsultaCertificacion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Framework.Comun.Resultado}ResultadoExterno">
 *       &lt;sequence>
 *         &lt;element name="ReporteCertificacion" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RespuestaConsultaCertificacion", namespace = "http://schemas.datacontract.org/2004/07/Convenio.Core.Modelo.Dto.Proceso.ServicioExterno", propOrder = {
        "reporteCertificacion"
})
public class RespuestaConsultaCertificacion
        extends ResultadoExterno {

    @XmlElementRef(name = "ReporteCertificacion", namespace = "http://schemas.datacontract.org/2004/07/Convenio.Core.Modelo.Dto.Proceso.ServicioExterno", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> reporteCertificacion;

    public RespuestaConsultaCertificacion() {
    }

    public RespuestaConsultaCertificacion(boolean esValido, String mensaje, String tipoMensaje) {

    }

    /**
     * Obtiene el valor de la propiedad reporteCertificacion.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     */
    public JAXBElement<byte[]> getReporteCertificacion() {
        return reporteCertificacion;
    }

    /**
     * Define el valor de la propiedad reporteCertificacion.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     */
    public void setReporteCertificacion(JAXBElement<byte[]> value) {
        this.reporteCertificacion = value;
    }

    @Override
    public String toString() {
        return "RespuestaConsultaCertificacion{" +
                " codigoRespuesta=" + codigoRespuesta +
                ", codigoUnico=" + codigoUnico.getValue() +
                ", descripcionRespuesta=" + descripcionRespuesta.getValue() +
                ", esValido=" + String.valueOf(esValido) +
                ", mensaje=" + mensaje.getValue() +
                ", tipoMensaje=" + tipoMensaje.getValue() +
                ", reporteCertificacion=" + (getReporteCertificacion().getValue() != null) +
                '}';
    }
}
