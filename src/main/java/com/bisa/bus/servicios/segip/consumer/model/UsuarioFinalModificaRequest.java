package com.bisa.bus.servicios.segip.consumer.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author rsalvatierra on 05/05/2016.
 */
@XmlRootElement(name = "UsuarioFinalModifica", namespace = "http://tempuri.org/")
public class UsuarioFinalModificaRequest implements Serializable {

    private int pIdInstitucion;
    private String pUsuario;
    private String pContrasenia;
    private String pClaveAcceso;
    private String pNombreInstitucion;
    private String pCargo;
    private String pDepartamento;
    private String pProvincia;
    private String pSeccion;
    private String pLocalidad;
    private String pOficina;
    private String pCorreoElectronico;
    private String pTelefono;
    private String pTelefonoOficina;

    public UsuarioFinalModificaRequest() {
    }

    public int getpIdInstitucion() {
        return pIdInstitucion;
    }

    public void setpIdInstitucion(int pIdInstitucion) {
        this.pIdInstitucion = pIdInstitucion;
    }

    public String getpUsuario() {
        return pUsuario;
    }

    public void setpUsuario(String pUsuario) {
        this.pUsuario = pUsuario;
    }

    public String getpContrasenia() {
        return pContrasenia;
    }

    public void setpContrasenia(String pContrasenia) {
        this.pContrasenia = pContrasenia;
    }

    public String getpClaveAcceso() {
        return pClaveAcceso;
    }

    public void setpClaveAcceso(String pClaveAcceso) {
        this.pClaveAcceso = pClaveAcceso;
    }

    public String getpNombreInstitucion() {
        return pNombreInstitucion;
    }

    public void setpNombreInstitucion(String pNombreInstitucion) {
        this.pNombreInstitucion = pNombreInstitucion;
    }

    public String getpCargo() {
        return pCargo;
    }

    public void setpCargo(String pCargo) {
        this.pCargo = pCargo;
    }

    public String getpDepartamento() {
        return pDepartamento;
    }

    public void setpDepartamento(String pDepartamento) {
        this.pDepartamento = pDepartamento;
    }

    public String getpProvincia() {
        return pProvincia;
    }

    public void setpProvincia(String pProvincia) {
        this.pProvincia = pProvincia;
    }

    public String getpSeccion() {
        return pSeccion;
    }

    public void setpSeccion(String pSeccion) {
        this.pSeccion = pSeccion;
    }

    public String getpLocalidad() {
        return pLocalidad;
    }

    public void setpLocalidad(String pLocalidad) {
        this.pLocalidad = pLocalidad;
    }

    public String getpOficina() {
        return pOficina;
    }

    public void setpOficina(String pOficina) {
        this.pOficina = pOficina;
    }

    public String getpCorreoElectronico() {
        return pCorreoElectronico;
    }

    public void setpCorreoElectronico(String pCorreoElectronico) {
        this.pCorreoElectronico = pCorreoElectronico;
    }

    public String getpTelefono() {
        return pTelefono;
    }

    public void setpTelefono(String pTelefono) {
        this.pTelefono = pTelefono;
    }

    public String getpTelefonoOficina() {
        return pTelefonoOficina;
    }

    public void setpTelefonoOficina(String pTelefonoOficina) {
        this.pTelefonoOficina = pTelefonoOficina;
    }
}
