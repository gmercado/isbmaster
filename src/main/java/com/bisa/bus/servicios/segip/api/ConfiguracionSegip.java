package com.bisa.bus.servicios.segip.api;

/**
 * @author josanchez on 09/03/2016.
 * @author rsalvatierra modificado on 04/04/2016
 */
public interface ConfiguracionSegip {

    //Definición del servicio
    String SEGIP_NAMESPACE = "ws.namespace";
    String SEGIP_NAMESPACE_DEFAULT = "http://tempuri.org/";
    //Proxy
    String PROXY_HOST = "http.proxyHost";
    String PROXY_HOST_DEFAULT = "navegar.grupobisa.net";  //"lpzpxy.grupobisa.net";
    String PROXY_PORT = "http.proxyPort";
    int PROXY_PORT_DEFAULT = 8080;
    //Servicio de Consulta Persona Certificacion
    String SEGIP_URL = "ws.url";
    String SEGIP_URL_DEFAULT = "http://200.105.134.227:9030/ServicioExternoInstitucion.svc";
    String SEGIP_SERVICE_NAME = "ws.service.name";
    String SEGIP_SERVICE_NAME_DEFAULT = "ServicioExternoInstitucion";
    String SEGIP_OPERATION_NAME = "ws.service.operation.name";
    String SEGIP_OPERATION_NAME_DEFAULT = "ConsultaDatoPersonaCertificacion";
    String SEGIP_PORT_NAME = "ws.service.port.name";
    String SEGIP_PORT_NAME_DEFAULT = "httpBasicConfig";
    //Servicio de altas/mod/bajas usuarios finales
    String SEGIP_ADMIN_URL = "ws.admin.url";
    String SEGIP_ADMIN_URL_DEFAULT = "http://200.105.134.227:9010/ServicioExternoComun.svc";
    String SEGIP_ADMIN_SERVICE_NAME = "ws.admin.service.name";
    String SEGIP_ADMIN_SERVICE_NAME_DEFAULT = "ServicioExternoInstitucion";
    String SEGIP_ADMIN_PORT_NAME = "ws.admin.service.port.name.admin";
    String SEGIP_ADMIN_PORT_NAME_DEFAULT = "BasicEndpointExternoComun";

    String SEGIP_ADMIN_OPERATION_NAME_NEW = "ws.admin.service.operation.name.new";
    String SEGIP_ADMIN_OPERATION_NAME_NEW_DEFAULT = "UsuarioFinalAlta";
    String SEGIP_ADMIN_OPERATION_NAME_MOD = "ws.admin.service.operation.name.mod";
    String SEGIP_ADMIN_OPERATION_NAME_MOD_DEFAULT = "UsuarioFinalModifica";
    String SEGIP_ADMIN_OPERATION_NAME_HAB = "ws.admin.service.operation.name.hab";
    String SEGIP_ADMIN_OPERATION_NAME_HAB_DEFAULT = "UsuarioFinalHabilitaInhabilita";
    //Valores de conexion
    String SEGIP_CODIGO_INSTITUCION = "ws.conexion.codigo.institucion";
    int SEGIP_CODIGO_INSTITUCION_DEFAULT = 158;
    String SEGIP_USUARIO = "ws.conexion.usuario";
    String SEGIP_USUARIO_DEFAULT = "";
    String SEGIP_CLAVE = "ws.conexion.clave";
    String SEGIP_CLAVE_DEFAULT = "";
    String SEGIP_CODIGO_USUARIO_FINAL = "ws.conexion.codigo.usuario";
    String SEGIP_CODIGO_USUARIO_FINAL_DEFAULT = "";
    //Parametros de sistema
    String SEGIP_CONSUME_SERVICIO = "ws.consumir.servicio";
    String SEGIP_CONSUME_SERVICIO_DEFAULT = "false";
    String SEGIP_CANAL_HABILITADO = "canal.";
    String SEGIP_CANAL_HABILITADO_DEFAULT = "false";
    String SEGIP_TIPODOC_HABILITADO = "tipo.documento";
    String SEGIP_TIPODOC_HABILITADO_DEFAULT = "CLP";
    String OBTENER_REPORTE_SEGIP = "ws.reporte.obtener.data";
    String OBTENER_REPORTE_SEGIP_DEFAULT = "false";
    String MOSTRAR_REPORTE_DATOS_SEGIP = "ws.reporte.mostrar.data";
    String MOSTRAR_REPORTE_DATOS_SEGIP_DEFAULT = "false";
    String FECHA_VALIDEZ_BASE_SEGIP = "local.obtener.data";
    String FECHA_VALIDEZ_BASE_SEGIP_DEFAULT = "01/01/2027";
    String PERIODO_VALIDEZ_BASE_SEGIP = "local.periodo.tiempo.validez";
    int PERIODO_VALIDEZ_BASE_SEGIP_DEFAULT = 24;
    String TIPO_PERIODO_VALIDEZ_BASE_SEGIP = "local.periodo.tipo.validez";
    String TIPO_PERIODO_VALIDEZ_BASE_SEGIP_DEFAULT = "MES";
    //Contrastacion
    String FECHA_VALIDEZ_BASE_CONTRASTACION = "contrastacion.fecha.validez";
    String FECHA_VALIDEZ_BASE_CONTRASTACION_DEFAULT = "31/12/2026";
    String GENERA_REPORTE_CONTRASTACION = "contrastacion.genera.reporte";
    String GENERA_REPORTE_CONTRASTACION_DEFAULT = "false";
    String NOMBRE_CONTRASTACION_VIGENTE = "contrastacion.nombre";
    String NOMBRE_CONTRASTACION_VIGENTE_DEFAULT = "CON1";
    //As400
    String UTILIZAR_IFS = "as400.utiliza.ifs";
    String UTILIZAR_IFS_DEFAULT = "true";
    String PATH_IFS_REPORTE_SEGIP = "as400.path.ifs";
    String PATH_IFS_REPORTE_SEGIP_DEFAULT = "/Bisa/SEGIP/";
    //Validacion
    String PATTERN_NUMERO_DOCUMENTO = "pattern.numero.documento";
    String PATTERN_NUMERO_DOCUMENTO_DEFAULT = "[0-9]+(-?[0-9]+)?";//(E-)?[0-9]+(-?[0-9]+)?
    String PATTERN_COMPLEMENTO = "pattern.complemento";
    String PATTERN_COMPLEMENTO_DEFAULT = "[0-9][A-Za-z0-9]";
    String PATTERN_NUMERO_EXTRANJERO = "pattern.numero.extranjero";
    String PATTERN_NUMERO_EXTRANJERO_DEFAULT = "[E]?[-]?[0-9]+(-?[0-9]+)?";

    String TIPO_DOC_EXTRANJERO = "tipo.documento.extranjero";
    String TIPO_DOC_EXTRANJERO_DEFAULT = "CEX";

    String TIPO_DOC_NACIONAL = "tipo.documento.nacional";
    String TIPO_DOC_NACIONAL_DEFAULT = "CLP, CCB, CBE, CCH, COR, CPA, CPO, CTJ, CSC";

    String SYSTEM_CONTRASTACION = "CONTRASTACION";
    String CONTRASTADO_OBSERVACION = "CONTRASTADO";
    String MENSAJE_CONTRASTADO_DEFAULT = "505";
    String SYSTEM_SEGIP = "SEGIP";
    String SIN_REPORTE_INSTITUCION = "BANCO BISA";
    String SIN_OBSERVACION = "SIN OBSERVACION";
    String REGISTRO_CONTRASTADO = "A";

    String ARCHIVO_PDF = ".pdf";
    String ARCHIVO_CSV = ".csv";
    String SEPARADOR_DOCUMENTO = "-";

    //Mensajes
    String MENSAJE_REGISTRO_EXISTOSA = "mensaje.registro.exitoso";
    String MENSAJE_REGISTRO_EXISTOSA_DEFAULT = "La consulta se realiz\u00f3 satisfactoriamente.";
    String MENSAJE_REGISTRO_CONTRASTACION = "mensaje.contrastacion.exitosa";
    String MENSAJE_REGISTRO_CONTRASTACION_DEFAULT = "Se encontr\u00f3 un registro, fuente Base Contrastaci\u00f3n Banco BISA.";

    String MENSAJE_NO_CONSUMO = "mensaje.no.consumo";
    String MENSAJE_NO_CONSUMO_DEFAULT = "El servicio del SEGIP no esta habilitado para su consumo.";
    String MENSAJE_NO_REPORTE = "mensaje.no.reporte";
    String MENSAJE_NO_REPORTE_DEFAULT = "No se tiene el reporte de certificaci\u00f3n.";

    String MENSAJE_ERROR_RUNTIME_A = "mensaje.error.runtime";
    String MENSAJE_ERROR_RUNTIME_A_DEFAULT = "Ocurrio un error al realizar el proceso, por favor vuelva a intentar mas tarde.";
    String MENSAJE_ERROR_FORMATO = "mensaje.error.formato";
    String MENSAJE_ERROR_FORMATO_DEFAULT = "El formato de entrada no es válido.";
    String MENSAJE_ERROR_TIPODOC = "mensaje.error.tipo.doc";
    String MENSAJE_ERROR_TIPODOC_DEFAULT = "El tipo documento no es válido.";
    String MENSAJE_ERROR_CANAL = "mensaje.error.canal";
    String MENSAJE_ERROR_CANAL_DEFAULT = "El origen de la solicitud no es válido.";
    String MENSAJE_ERROR_SEGIP = "mensaje.error.segip";
    String MENSAJE_ERROR_SEGIP_DEFAULT = "Ocurrio un error al realizar la consulta al SEGIP, por favor vuelva a intentar mas tarde.";
    String MENSAJE_ERROR_CONSULTASEGIP = "mensaje.error.consulta.segip";
    String MENSAJE_ERROR_CONSULTASEGIP_DEFAULT = "Hubo un error al consultar la informaci\u00f3n del SEGIP,por favor vuelva a intentar m\u00e1s tarde.";
    String MENSAJE_ERROR_BD_CONTRASTACION = "mensaje.error.contrastacion.bd";
    String MENSAJE_ERROR_BD_CONTRASTACION_DEFAULT = "Se tienen problemas con BD.";
    String MENSAJE_REPORTE_CONTRASTACION = "mensaje.reporte.contrastacion";
    String MENSAJE_REPORTE_CONTRASTACION_DEFAULT = "Registro observado en comparación con información del SEGIP";

}

