package com.bisa.bus.servicios.segip.consumer.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rsalvatierra on 05/05/2016.
 */
@XmlRootElement(name = "UsuarioFinalHabilitaInhabilitaResponse", namespace = "http://tempuri.org/")
public class UsuarioFinalHabilitaInhabilitaResponse {
    private RespuestaUsuarioFinalAlta UsuarioFinalHabilitaInhabilitaResult;

    public UsuarioFinalHabilitaInhabilitaResponse() {
    }

    @XmlElement(name = "UsuarioFinalHabilitaInhabilitaResult")
    public RespuestaUsuarioFinalAlta getUsuarioFinalHabilitaInhabilitaResult() {
        return UsuarioFinalHabilitaInhabilitaResult;
    }

    public void setUsuarioFinalHabilitaInhabilitaResult(RespuestaUsuarioFinalAlta usuarioFinalHabilitaInhabilitaResult) {
        UsuarioFinalHabilitaInhabilitaResult = usuarioFinalHabilitaInhabilitaResult;
    }
}
