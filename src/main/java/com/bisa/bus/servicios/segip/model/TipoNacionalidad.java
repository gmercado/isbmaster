package com.bisa.bus.servicios.segip.model;

/**
 * @author rsalvatierra on 11/04/2016.
 */
public enum TipoNacionalidad {

    NACIONAL("NACIONAL"),
    EXTRANJERO("EXTRANJERO");

    private String descripcion;

    TipoNacionalidad(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
