package com.bisa.bus.servicios.segip.model;

import java.io.Serializable;

/**
 * @author josanchez on 17/03/2016.
 * @author rsalvatierra modificado on 04/04/2016
 */
public class BaseResultado implements Serializable {
    private Boolean esValidoBean;
    private String mensajeBean;
    private String tipoMensajeBean;

    public BaseResultado() {
    }

    public Boolean isEsValidoBean() {
        return esValidoBean;
    }

    public void setEsValidoBean(Boolean esValidoBean) {
        this.esValidoBean = esValidoBean;
    }

    public String getMensajeBean() {
        return mensajeBean;
    }

    public void setMensajeBean(String mensajeBean) {
        this.mensajeBean = mensajeBean;
    }

    public String getTipoMensajeBean() {
        return tipoMensajeBean;
    }

    public void setTipoMensajeBean(String tipoMensajeBean) {
        this.tipoMensajeBean = tipoMensajeBean;
    }
}
