package com.bisa.bus.servicios.segip.api;

import com.bisa.bus.servicios.segip.model.ResultadoConsultaSegip;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegip;

/**
 * @author rsalvatierra on 21/04/2016.
 */
public interface SegipService {

    ResultadoConsultaSegip consultasCertificacionSegip(final SolicitudConsultaSegip request);

}
