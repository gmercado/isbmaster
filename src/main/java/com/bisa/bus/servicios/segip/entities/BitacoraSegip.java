package com.bisa.bus.servicios.segip.entities;

import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.segip.model.TipoRespuesta;
import com.bisa.bus.servicios.segip.model.TipoSolicitud;
import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author josanchez on 09/03/2016.
 * @author rsalvatierra modificado on 01/04/2016
 */
@Entity
@Table(name = "SGP001")
public class BitacoraSegip implements Serializable {

    public static final String SEPARADOR_CONSULTA = "-";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S01CODIGO", columnDefinition = "NUMERIC(10)")
    private Long id;

    @Column(name = "S01NOMUSR", columnDefinition = "CHAR(255) default user", nullable = false)
    private String codigoUsuario;

    @Column(name = "S01OFIUSR", columnDefinition = "CHAR(3)")
    private String oficinaUsuario;

    @Column(name = "S01CANUSR", columnDefinition = "CHAR(10)")
    private String canalUsuario;

    @Column(name = "S01TIPUSR", columnDefinition = "CHAR(2)", nullable = false)
    private String tipoUsuario;

    @Column(name = "S01TIPBUR", columnDefinition = "CHAR(20)")
    private String tipoBuro;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "S01FECCON", nullable = false)
    private Date fechaHoraConsulta;

    @Column(name = "S01CODERR", columnDefinition = "CHAR(3)", nullable = false)
    private String codError;

    @Column(name = "S01CRIBUS", columnDefinition = "CLOB")
    private String criterioBusqueda;

    @Column(name = "S01RESBUS", columnDefinition = "CLOB")
    private String resultadoBusqueda;

    @Column(name = "S01CODRES", columnDefinition = "CHAR(3)", nullable = false)
    private String tipoRespuesta;

    @Column(name = "S01NROREV", columnDefinition = "NUMERIC(10)", nullable = false)
    private Long nroRevision;

    @Transient
    private TipoRespuesta tipoRespuestaEnum;

    @Transient
    private VariableSegip tipoCanal;

    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "S01OFIUSR")
    private Oficina oficina;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "S01CODPER")
    private PersonaSegip personaSegip;

    public BitacoraSegip() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCriterioBusqueda() {
        return criterioBusqueda;
    }

    public void setCriterioBusqueda(String criterioBusqueda) {
        this.criterioBusqueda = criterioBusqueda;
    }

    public String getResultadoBusqueda() {
        return resultadoBusqueda;
    }

    public void setResultadoBusqueda(String resultadoBusqueda) {
        this.resultadoBusqueda = resultadoBusqueda;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public Date getFechaHoraConsulta() {
        return fechaHoraConsulta;
    }

    public void setFechaHoraConsulta(Date fechaHoraConsulta) {
        this.fechaHoraConsulta = fechaHoraConsulta;
    }

    public String getCodError() {
        return codError;
    }

    public void setCodError(String codError) {
        this.codError = codError;
    }

    public PersonaSegip getPersonaSegip() {
        return personaSegip;
    }

    public void setPersonaSegip(PersonaSegip personaSegip) {
        this.personaSegip = personaSegip;
    }

    public String getTipoRespuesta() {
        return tipoRespuesta;
    }

    public void setTipoRespuesta(String tipoRespuesta) {
        this.tipoRespuesta = tipoRespuesta;
    }

    public Long getNroRevision() {
        return nroRevision;
    }

    public void setNroRevision(Long nroRevision) {
        this.nroRevision = nroRevision;
    }

    public String getOficinaUsuario() {
        return oficinaUsuario;
    }

    public void setOficinaUsuario(String oficinaUsuario) {
        this.oficinaUsuario = oficinaUsuario;
    }

    public String getCanalUsuario() {
        return canalUsuario;
    }

    public void setCanalUsuario(String canalUsuario) {
        this.canalUsuario = canalUsuario;
    }

    public String getTipoBuro() {
        return tipoBuro;
    }

    public void setTipoBuro(String tipoBuro) {
        this.tipoBuro = tipoBuro;
    }

    public String getFechaConsumo() {
        return FormatosUtils.formatoFechaHora(getFechaHoraConsulta());
    }

    public String getConsultaDocumento() {
        String cadena = getCriterioBusqueda();
        if (cadena != null && cadena.contains(",")) {
            String[] cadenas = cadena.split(",");
            if (cadenas.length > 0) {
                cadena = getDetalleConsulta(cadenas);
            }
        }
        return cadena;
    }

    private String getDetalleConsulta(String[] cadenas) {
        StringBuilder sb = new StringBuilder();
        sb.append(cadenas[1]);
        sb.append(SEPARADOR_CONSULTA);
        sb.append(cadenas[6]);
        String[] subcadenas = cadenas[13].split("=");
        if (!subcadenas[1].trim().equals("''")) {
            sb.append(SEPARADOR_CONSULTA);
            sb.append(cadenas[13]);
        }
        return sb.toString();
    }

    public String getEstadoConsulta() {
        String tipo = "-";
        TipoRespuesta tipoR;
        if (StringUtils.isNotBlank(getTipoRespuesta())) {
            tipoR = TipoRespuesta.get(StringUtils.trimToEmpty(getTipoRespuesta()));
            if (tipoR != null) {
                tipo = tipoR.toString();
            }
        }
        return tipo;
    }

    public String getOrigenSolicitud() {
        String tipo = "-";
        TipoSolicitud tipoR;
        if (StringUtils.isNotBlank(getTipoUsuario())) {
            tipoR = TipoSolicitud.get(StringUtils.trimToEmpty(getTipoUsuario()));
            if (tipoR != null) {
                tipo = tipoR.getDescripcion();
            }
        }
        return tipo;
    }

    public TipoRespuesta getTipoRespuestaEnum() {
        return tipoRespuestaEnum;
    }

    public void setTipoRespuestaEnum(TipoRespuesta tipoRespuestaEnum) {
        this.tipoRespuestaEnum = tipoRespuestaEnum;
    }

    public VariableSegip getTipoCanal() {
        return tipoCanal;
    }

    public void setTipoCanal(VariableSegip tipoCanal) {
        this.tipoCanal = tipoCanal;
    }

    public Oficina getOficina() {
        return oficina;
    }

    public void setOficina(Oficina oficina) {
        this.oficina = oficina;
    }

    @Override
    public String toString() {
        return "BitacoraSegip{" +
                "id=" + getId() +
                ", codError='" + StringUtils.trimToEmpty(getCodError()) + '\'' +
                ", tipoRespuesta='" + StringUtils.trimToEmpty(getTipoRespuesta()) + '\'' +
                ", codigoUsuario='" + StringUtils.trimToEmpty(getCodigoUsuario()) + '\'' +
                ", oficina='" + StringUtils.trimToEmpty(getOficinaUsuario()) + '\'' +
                ", canal='" + StringUtils.trimToEmpty(getCanalUsuario()) + '\'' +
                ", tipoBuro='" + StringUtils.trimToEmpty(getTipoBuro()) + '\'' +
                ", tipoUsuario='" + StringUtils.trimToEmpty(getTipoUsuario()) + '\'' +
                ", fechaHoraConsulta=" + getFechaHoraConsulta() +
                ", criterioBusqueda='" + StringUtils.trimToEmpty(getCriterioBusqueda()) + '\'' +
                ", resultadoBusqueda='" + StringUtils.trimToEmpty(getResultadoBusqueda()) + '\'' +
                ", nroRev=" + getNroRevision() + '\'' +
                ", personaSegip=" + getPersonaSegip() +
                '}';
    }
}
