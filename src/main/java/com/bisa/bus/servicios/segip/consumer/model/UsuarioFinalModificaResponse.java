package com.bisa.bus.servicios.segip.consumer.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rsalvatierra on 05/05/2016.
 */
@XmlRootElement(name = "UsuarioFinalModificaResponse", namespace = "http://tempuri.org/")
public class UsuarioFinalModificaResponse {
    private RespuestaUsuarioFinalAlta UsuarioFinalModificaResult;

    public UsuarioFinalModificaResponse() {
    }

    @XmlElement(name = "UsuarioFinalModificaResult")
    public RespuestaUsuarioFinalAlta getUsuarioFinalModificaResult() {
        return UsuarioFinalModificaResult;
    }

    public void setUsuarioFinalModificaResult(RespuestaUsuarioFinalAlta usuarioFinalModificaResult) {
        UsuarioFinalModificaResult = usuarioFinalModificaResult;
    }
}
