/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.bisa.bus.servicios.segip.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.users.api.Metadatas;
import com.bisa.bus.servicios.segip.dao.ObservacionesSegipDao;
import com.bisa.bus.servicios.segip.entities.ObservacionesSegip;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.validator.StringValidator;

import java.util.Date;

import static bus.users.api.Metadatas.USER_META_DATA_KEY;

/**
 * @author rsalvatierra modificado on 09/05/2016
 */
@Titulo("Edici\u00f3n de observaciones SEGIP")
@AuthorizeInstantiation({RolesBisa.SEGIP_PARAM})
public class EditarObservacionSegip extends BisaWebPage {

    /**
     * @param parameters necesito el "codigo" de la variable.
     */
    public EditarObservacionSegip(PageParameters parameters) {
        super(parameters);
        IModel<ObservacionesSegip> model = new LoadableDetachableModel<ObservacionesSegip>() {
            @Override
            protected ObservacionesSegip load() {
                return new ObservacionesSegip(getPageParameters().get("codigo").toString(), getInstance(ObservacionesSegipDao.class).getMensaje(getPageParameters().get("codigo").toString()));
            }
        };

        Form<ObservacionesSegip> editar;
        add(editar = new Form<>("editar", new CompoundPropertyModel<>(model)));

        editar.add(new Label("codigo"));

        TextArea<String> valor;
        editar.add(valor = new TextArea<>("descripcion"));
        valor.setRequired(true);
        valor.add(StringValidator.maximumLength(1000));
        valor.setOutputMarkupId(true);

        editar.add(new IndicatingAjaxButton("editar", editar) {

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                try {
                    ObservacionesSegipDao observacionesSegipDao = getInstance(ObservacionesSegipDao.class);
                    ObservacionesSegip v = (ObservacionesSegip) form.getModelObject();
                    v.setUsuarioModificador(getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase());
                    v.setFechaModificacion(new Date());
                    observacionesSegipDao.merge(v);
                    LOGGER.info("El usuario {} ha actualizado el valor del mensaje {}",
                            getSession().getMetaData(Metadatas.USER_META_DATA_KEY).getUserlogon(),
                            v.getCodigo());
                    getSession().info("Operaci\u00f3n ejecutada satisfactoriamente");
                    setResponsePage(ListadoObservacionesSegip.class);
                } catch (Exception e) {
                    LOGGER.error("Ha ocurrido un error inesperado al editar variable", e);
                    getSession().error("Ha ocurrido un error inesperado, comun\u00edquese con soporte t\u00e9cnico");
                    target.add(feedbackPanel);
                }
            }
        });

        editar.add(new IndicatingAjaxButton("eliminar", editar) {

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                try {
                    ObservacionesSegipDao variableDao = getInstance(ObservacionesSegipDao.class);
                    ObservacionesSegip v = (ObservacionesSegip) form.getModelObject();
                    variableDao.remove(v.getCodigo());
                    LOGGER.info("El usuario {} ha eliminado el valor del mensaje {}",
                            getSession().getMetaData(Metadatas.USER_META_DATA_KEY).getUserlogon(),
                            v.getCodigo());
                    getSession().info("Operaci\u00f3n ejecutada satisfactoriamente");
                    setResponsePage(ListadoObservacionesSegip.class);
                } catch (Exception e) {
                    LOGGER.error("Ha ocurrido un error inesperado al eliminar variable", e);
                    getSession().error("Ha ocurrido un error inesperado, comun\u00edquese con soporte t\u00e9cnico");
                    target.add(feedbackPanel);
                }
            }

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }
        }.setDefaultFormProcessing(false));

        editar.add(new Button("cancelar") {

            @Override
            public void onSubmit() {
                getSession().info("Operaci\u00f3n cancelada");
                setResponsePage(ListadoObservacionesSegip.class);
            }
        }.setDefaultFormProcessing(false));
    }
}
