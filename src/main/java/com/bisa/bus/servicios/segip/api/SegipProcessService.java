package com.bisa.bus.servicios.segip.api;

import com.bisa.bus.servicios.segip.consumer.model.RespuestaConsultaCertificacion;
import com.bisa.bus.servicios.segip.entities.ContrastacionSegip;
import com.bisa.bus.servicios.segip.entities.ObservacionesSegip;
import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import com.bisa.bus.servicios.segip.entities.PersonaSegipHist;
import com.bisa.bus.servicios.segip.model.ResultadoConsultaSegip;
import com.bisa.bus.servicios.segip.model.SegipWebModel;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegip;
import com.bisa.bus.servicios.segip.model.TipoCodigoError;
import com.bisa.bus.servicios.segip.xml.ReporteCertificacion;

/**
 * @author rsalvatierra on 21/04/2016.
 */
public interface SegipProcessService {
    boolean fechaBDlocalVigente();

    boolean fechaContrastacionVigente();

    boolean generaReporteConstrastacion();

    boolean escribeIFS(SolicitudConsultaSegip request);

    boolean obtenerReporteSegip();

    boolean mostrarReporteDatosSegip();

    boolean plazoRetencionVigente(SolicitudConsultaSegip request, PersonaSegip datosSegip);

    boolean plazoRetencionVigente(SolicitudConsultaSegip request, ContrastacionSegip contrastacionSegip);

    ResultadoConsultaSegip escribirIFS(SolicitudConsultaSegip consulta, ResultadoConsultaSegip response);

    SegipWebModel getDatosPersona(ContrastacionSegip contrastacionSegip);

    SegipWebModel getDatosPersona(PersonaSegip consultaPersonaSegip);

    TipoCodigoError getCodError(String codRespuesta);

    TipoCodigoError getCodError(int codRespuesta);

    String getObservacion(ReporteCertificacion reporte, ResultadoConsultaSegip respuesta);

    ReporteCertificacion getReporteCertificacion(RespuestaConsultaCertificacion respuesta);

    PersonaSegipHist getDatosHistorico(PersonaSegip consultaPersonaSegip);

    boolean formatoValido(SolicitudConsultaSegip request);

    boolean canalValido(SolicitudConsultaSegip request);

    boolean tipoDocValido(SolicitudConsultaSegip request);

    boolean consumeServicioSegip();

    String getFormato(int tipo);

    String getMensaje(String tipo);

    ObservacionesSegip getRespuesta(ResultadoConsultaSegip response);

    byte[] getReporteContrastacion(String usuario, ContrastacionSegip contrastacionSegip);

    String getCarnetExtranjero();

    String getCarnetNacional();
}
