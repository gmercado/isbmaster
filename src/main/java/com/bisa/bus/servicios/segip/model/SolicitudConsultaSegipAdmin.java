package com.bisa.bus.servicios.segip.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author rsalvatierra on 06/05/2016
 */
public class SolicitudConsultaSegipAdmin implements Serializable {

    private static final long serialVersionUID = 1L;

    private String codigo;
    private String numeroDocumento;
    private String complemento;
    private String codigoAgencia;
    private String nombreOficial;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String fechaNacimiento;
    private Boolean estado;
    private int operacion;
    private String cargo;
    private String oficina;
    private String correoElectronico;
    private String telefono;
    private String telefonoOficina;

    public SolicitudConsultaSegipAdmin() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigoAgencia() {
        return codigoAgencia;
    }

    public void setCodigoAgencia(String codigoAgencia) {
        this.codigoAgencia = codigoAgencia;
    }

    public String getNombreOficial() {
        return nombreOficial;
    }

    public void setNombreOficial(String nombreOficial) {
        this.nombreOficial = nombreOficial;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Boolean isEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public int getOperacion() {
        return operacion;
    }

    public void setOperacion(int operacion) {
        this.operacion = operacion;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getOficina() {
        return oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefonoOficina() {
        return telefonoOficina;
    }

    public void setTelefonoOficina(String telefonoOficina) {
        this.telefonoOficina = telefonoOficina;
    }

    public String getNumeroDocumentoComplemento() {
        if (StringUtils.isNotBlank(getComplemento()) && StringUtils.isNotEmpty(getComplemento())) {
            return getNumeroDocumento() + "-" + getComplemento();
        }
        return getNumeroDocumento();
    }

    @Override
    public String toString() {
        return "SolicitudConsultaSegip{" +
                "codigo='" + StringUtils.trimToEmpty(getCodigo()) + '\'' +
                ", numeroDocumento='" + StringUtils.trimToEmpty(getNumeroDocumento()) + '\'' +
                ", codigoAgencia='" + StringUtils.trimToEmpty(getCodigoAgencia()) + '\'' +
                ", nombreOficial='" + StringUtils.trimToEmpty(getNombreOficial()) + '\'' +
                ", complemento='" + StringUtils.trimToEmpty(getComplemento()) + '\'' +
                ", nombre='" + StringUtils.trimToEmpty(getNombre()) + '\'' +
                ", primerApellido='" + StringUtils.trimToEmpty(getPrimerApellido()) + '\'' +
                ", segundoApellido='" + StringUtils.trimToEmpty(getSegundoApellido()) + '\'' +
                ", fechaNacimiento='" + StringUtils.trimToEmpty(getFechaNacimiento()) + '\'' +
                '}';
    }
}
