package com.bisa.bus.servicios.segip.dao;

import bus.database.dao.DaoImplCached;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.segip.api.ObservacionesSegipService;
import com.bisa.bus.servicios.segip.entities.ObservacionesSegip;
import com.google.inject.Inject;
import net.sf.ehcache.Ehcache;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author rsalvatierra on 01/06/2016
 */
public class ObservacionesSegipDao extends DaoImplCached<ObservacionesSegip, Long> implements Serializable, ObservacionesSegipService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ObservacionesSegipDao.class);

    @Inject
    public ObservacionesSegipDao(@BasePrincipal EntityManagerFactory entityManagerFactory,
                                 Ehcache cache) {
        super(entityManagerFactory, cache);
    }

    @Override
    public ObservacionesSegip merge(ObservacionesSegip data) {
        return super.merge(data);
    }

    public ObservacionesSegip remove(String id) {
        LOGGER.debug("Remove {} con id {}", getClazz(), id);
        return doWithTransaction((entityManager, t) -> {
            ObservacionesSegip entity = entityManager.find(getClazz(), id);
            entityManager.remove(entity);
            return entity;
        });
    }

    @Override
    protected Path<Long> countPath(Root<ObservacionesSegip> from) {
        return from.get("codigo");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<ObservacionesSegip> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.<String>get("codigo"));
        paths.add(p.<String>get("descripcion"));
        return paths;
    }

    @Override
    public String getMensaje(String codigo) {
        ObservacionesSegip variable;
        try {
            variable = doWithTransaction((entityManager, t) -> {
                OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
                OpenJPACriteriaQuery<ObservacionesSegip> query = cb.createQuery(ObservacionesSegip.class);
                query.where(cb.equal(query.from(ObservacionesSegip.class).get("codigo"), codigo));

                TypedQuery<ObservacionesSegip> typedQuery = entityManager.createQuery(query);
                List<ObservacionesSegip> resultList = typedQuery.getResultList();
                if (resultList.isEmpty()) {
                    return null;
                }
                if (resultList.size() > 1) {
                    LOGGER.warn("Buscando una variable con mas de un registro, {}, deviolviendo el primero", resultList);
                }
                return resultList.get(0);
            });
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error al obtener el valor de la variable, asumiendo el valor por defecto", e);
            variable = null;
        }
        return (variable != null) ? variable.getDescripcion() : null;
    }

    @Override
    public ObservacionesSegip getMensajeObs(String mensaje) {
        ObservacionesSegip variable;
        try {
            variable = doWithTransaction((entityManager, t) -> {
                OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
                OpenJPACriteriaQuery<ObservacionesSegip> query = cb.createQuery(ObservacionesSegip.class);
                query.where(cb.like(query.from(ObservacionesSegip.class).get("codigo"), "5%"));
                query.where(cb.like(query.from(ObservacionesSegip.class).get("descripcion"), "%" + mensaje.toUpperCase() + "%"));

                TypedQuery<ObservacionesSegip> typedQuery = entityManager.createQuery(query);
                List<ObservacionesSegip> resultList = typedQuery.getResultList();
                if (resultList.isEmpty()) {
                    return null;
                }
                if (resultList.size() > 1) {
                    LOGGER.warn("Buscando una variable con mas de un registro, {}, deviolviendo el primero", resultList);
                }
                return resultList.get(0);
            });
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error al obtener el valor de la variable, asumiendo el valor por defecto", e);
            variable = null;
        }
        return variable;
    }
}
