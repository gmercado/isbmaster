package com.bisa.bus.servicios.segip.model;

import bus.plumbing.utils.FormatosUtils;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * @author rsalvatierra on 11/04/2016.
 */
public class HistoricoSegip implements Serializable {

    private String codUsuario;
    private String canal;
    private Date fecha;
    private String codError;
    private String codRespuesta;
    private Long codPersona;
    private Long nroRevision;
    private String esValido;
    private String codigoRespuesta;
    private String codigoUnico;
    private String desRespuesta;
    private String tipoMensaje;
    private String mensaje;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private Long fechaNacimiento;
    private String observaciones;
    private String forzado;

    public HistoricoSegip() {
    }

    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getCodError() {
        return codError;
    }

    public void setCodError(String codError) {
        this.codError = codError;
    }

    public String getCodRespuesta() {
        return codRespuesta;
    }

    public void setCodRespuesta(String codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    public Long getCodPersona() {
        return codPersona;
    }

    public void setCodPersona(Long codPersona) {
        this.codPersona = codPersona;
    }

    public Long getNroRevision() {
        return nroRevision;
    }

    public void setNroRevision(Long nroRevision) {
        this.nroRevision = nroRevision;
    }

    public String getEsValido() {
        return esValido;
    }

    public void setEsValido(String esValido) {
        this.esValido = esValido;
    }

    public String getCodigoRespuesta() {
        return codigoRespuesta;
    }

    public void setCodigoRespuesta(String codigoRespuesta) {
        this.codigoRespuesta = codigoRespuesta;
    }

    public String getCodigoUnico() {
        return codigoUnico;
    }

    public void setCodigoUnico(String codigoUnico) {
        this.codigoUnico = codigoUnico;
    }

    public String getDesRespuesta() {
        return desRespuesta;
    }

    public void setDesRespuesta(String desRespuesta) {
        this.desRespuesta = desRespuesta;
    }

    public String getTipoMensaje() {
        return tipoMensaje;
    }

    public void setTipoMensaje(String tipoMensaje) {
        this.tipoMensaje = tipoMensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public Long getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Long fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getForzado() {
        return forzado;
    }

    public void setForzado(String forzado) {
        this.forzado = forzado;
    }

    public String getFechaConsumo() {
        return FormatosUtils.formatoFechaHoraLarga(getFecha());
    }

    public String getFechaNacFormato() {
        return FormatosUtils.formatoFecha(FormatosUtils.fechaYYYYMMDD(getFechaNacimiento()));
    }

    public String getEstadoConsulta() {
        String tipo = "-";
        TipoRespuesta tipoR;
        if (StringUtils.isNotBlank(getCodRespuesta())) {
            tipoR = TipoRespuesta.get(StringUtils.trimToEmpty(getCodRespuesta()));
            if (tipoR != null) {
                tipo = tipoR.toString();
            }
        }
        return tipo;
    }

    public String getOrigenSolicitud() {
        String tipo = "-";
        TipoSolicitud tipoR;
        if (StringUtils.isNotBlank(getCanal())) {
            tipoR = TipoSolicitud.get(StringUtils.trimToEmpty(getCanal()));
            if (tipoR != null) {
                tipo = tipoR.getDescripcion();
            }
        }
        return tipo;
    }
}
