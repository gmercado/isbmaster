package com.bisa.bus.servicios.segip.entities;

import bus.database.model.EntityBase;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author rsalvatierra on 01/06/2016
 */
@Entity
@Table(name = "SGP099")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S99FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S99USRALT", nullable = false, columnDefinition = "CHAR(255) default user")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S99FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S99USRMOD", columnDefinition = "CHAR(255)"))
})
public class VariableSegip extends EntityBase implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S99NIDVM", columnDefinition = "NUMERIC(5)")
    private Long id;

    @Version
    @Column(name = "S99NVER", columnDefinition = "NUMERIC(5)")
    private long version;

    @Column(name = "S99VNOM", columnDefinition = "CHAR(100)")
    private String nombre;

    @Column(name = "S99VVAL", columnDefinition = "CHAR(1000)")
    private String valor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getNombreParte() {
        return getNombre().substring(6, getNombre().length());
    }
}
