package com.bisa.bus.servicios.segip.api;

import com.bisa.bus.servicios.segip.entities.ObservacionesSegip;

/**
 * @author rsalvatierra on 10/06/2016.
 */
public interface ObservacionesSegipService {

    String getMensaje(String codigo);

    ObservacionesSegip getMensajeObs(String mensaje);
}
