package com.bisa.bus.servicios.segip.consumer.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author rsalvatierra on 04/05/2016.
 */
@XmlRootElement(name = "ConsultaDatoPersonaCertificacion", namespace = "http://tempuri.org/")
public class ConsultaDatoPersonaCertificacionRequest implements Serializable {
    private int pCodigoInstitucion;
    private String pUsuario;
    private String pContrasenia;
    private String pClaveAccesoUsuarioFinal;
    private String pNumeroAutorizacion;
    private String pNumeroDocumento;
    private String pComplemento;
    private String pNombre;
    private String pPrimerApellido;
    private String pSegundoApellido;
    private String pFechaNacimiento;

    public ConsultaDatoPersonaCertificacionRequest() {
    }

    public int getpCodigoInstitucion() {
        return pCodigoInstitucion;
    }

    public void setpCodigoInstitucion(int pCodigoInstitucion) {
        this.pCodigoInstitucion = pCodigoInstitucion;
    }

    public String getpUsuario() {
        return pUsuario;
    }

    public void setpUsuario(String pUsuario) {
        this.pUsuario = pUsuario;
    }

    public String getpContrasenia() {
        return pContrasenia;
    }

    public void setpContrasenia(String pContrasenia) {
        this.pContrasenia = pContrasenia;
    }

    public String getpClaveAccesoUsuarioFinal() {
        return pClaveAccesoUsuarioFinal;
    }

    public void setpClaveAccesoUsuarioFinal(String pClaveAccesoUsuarioFinal) {
        this.pClaveAccesoUsuarioFinal = pClaveAccesoUsuarioFinal;
    }

    public String getpNumeroAutorizacion() {
        return pNumeroAutorizacion;
    }

    public void setpNumeroAutorizacion(String pNumeroAutorizacion) {
        this.pNumeroAutorizacion = pNumeroAutorizacion;
    }

    public String getpNumeroDocumento() {
        return pNumeroDocumento;
    }

    public void setpNumeroDocumento(String pNumeroDocumento) {
        this.pNumeroDocumento = pNumeroDocumento;
    }

    public String getpComplemento() {
        return pComplemento;
    }

    public void setpComplemento(String pComplemento) {
        this.pComplemento = pComplemento;
    }

    public String getpNombre() {
        return pNombre;
    }

    public void setpNombre(String pNombre) {
        this.pNombre = pNombre;
    }

    public String getpPrimerApellido() {
        return pPrimerApellido;
    }

    public void setpPrimerApellido(String pPrimerApellido) {
        this.pPrimerApellido = pPrimerApellido;
    }

    public String getpSegundoApellido() {
        return pSegundoApellido;
    }

    public void setpSegundoApellido(String pSegundoApellido) {
        this.pSegundoApellido = pSegundoApellido;
    }

    public String getpFechaNacimiento() {
        return pFechaNacimiento;
    }

    public void setpFechaNacimiento(String pFechaNacimiento) {
        this.pFechaNacimiento = pFechaNacimiento;
    }
}
