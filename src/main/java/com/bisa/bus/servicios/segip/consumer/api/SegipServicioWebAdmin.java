package com.bisa.bus.servicios.segip.consumer.api;

import bus.consumoweb.consumer.ClienteServiciosWeb;
import com.bisa.bus.servicios.segip.api.ConfiguracionSegip;
import com.bisa.bus.servicios.segip.consumer.model.*;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegipAdmin;
import com.google.inject.Inject;

/**
 * @author rsalvatierra on 22/04/2016.
 */
public class SegipServicioWebAdmin {

    private final ClienteServiciosWeb clienteServicio;
    private final ConsumidorServiciosSegipAdmin consumidorSegip;


    @Inject
    public SegipServicioWebAdmin(@ConsumidorSegipAdmin ClienteServiciosWeb clienteServicio, ConsumidorServiciosSegipAdmin consumidorSegip) {
        this.clienteServicio = clienteServicio;
        this.consumidorSegip = consumidorSegip;
    }

    public UsuarioFinalAltaResponse consumoAlta(SolicitudConsultaSegipAdmin solicitud) {
        UsuarioFinalAltaRequest request = new UsuarioFinalAltaRequest();
        request.setpIdInstitucion(consumidorSegip.getCodigoInstitucion());
        request.setpUsuario(consumidorSegip.getUsuario());
        request.setpContrasenia(consumidorSegip.getClave());
        request.setpNombreInstitucion(ConfiguracionSegip.SIN_REPORTE_INSTITUCION);
        if (solicitud.getNumeroDocumento() != null) {
            request.setpNumeroCedula(solicitud.getNumeroDocumento());
        }
        if (solicitud.getComplemento() != null) {
            request.setpComplemento(solicitud.getComplemento());
        }
        if (solicitud.getPrimerApellido() != null) {
            request.setpPrimerApellido(solicitud.getPrimerApellido());
        }
        if (solicitud.getSegundoApellido() != null) {
            request.setpSegundoApellido(solicitud.getSegundoApellido());
        }
        if (solicitud.getNombre() != null) {
            request.setpNombre(solicitud.getNombre());
        }
        if (solicitud.getFechaNacimiento() != null) {
            request.setpFechaNacimiento(solicitud.getFechaNacimiento());
        }
        if (solicitud.getCargo() != null) {
            request.setpNombreCargo(solicitud.getCargo());
        }
        if (solicitud.getOficina() != null) {
            request.setpOficina(solicitud.getOficina());
        }
        if (solicitud.getCorreoElectronico() != null) {
            request.setpCorreoElectronico(solicitud.getCorreoElectronico());
        }
        if (solicitud.getTelefono() != null) {
            request.setpTelefono(solicitud.getTelefono());
        }
        if (solicitud.getTelefonoOficina() != null) {
            request.setpTelefonoOficina(solicitud.getTelefonoOficina());
        }
        return (UsuarioFinalAltaResponse) clienteServicio.consumir(request);
    }

    public UsuarioFinalHabilitaInhabilitaResponse consumoHabilita(SolicitudConsultaSegipAdmin solicitud) {
        UsuarioFinalHabilitaInhabilitaRequest request = new UsuarioFinalHabilitaInhabilitaRequest();
        request.setpIdInstitucion(consumidorSegip.getCodigoInstitucion());
        request.setpUsuario(consumidorSegip.getUsuario());
        request.setpContrasenia(consumidorSegip.getClave());
        request.setpClaveAcceso(solicitud.getCodigo());
        request.setpEstado(solicitud.isEstado());
        return (UsuarioFinalHabilitaInhabilitaResponse) clienteServicio.consumir(request);
    }

    public UsuarioFinalModificaResponse consumoModifica(SolicitudConsultaSegipAdmin solicitud) {
        UsuarioFinalModificaRequest request = new UsuarioFinalModificaRequest();
        request.setpIdInstitucion(consumidorSegip.getCodigoInstitucion());
        request.setpUsuario(consumidorSegip.getUsuario());
        request.setpContrasenia(consumidorSegip.getClave());
        request.setpClaveAcceso(solicitud.getCodigo());
        request.setpNombreInstitucion(ConfiguracionSegip.SIN_REPORTE_INSTITUCION);
        if (solicitud.getCargo() != null) {
            request.setpCargo(solicitud.getCargo());
        }
        if (solicitud.getOficina() != null) {
            request.setpOficina(solicitud.getOficina());
        }
        if (solicitud.getCorreoElectronico() != null) {
            request.setpCorreoElectronico(solicitud.getCorreoElectronico());
        }
        if (solicitud.getTelefono() != null) {
            request.setpTelefono(solicitud.getTelefono());
        }
        if (solicitud.getTelefonoOficina() != null) {
            request.setpTelefonoOficina(solicitud.getTelefonoOficina());
        }
        return (UsuarioFinalModificaResponse) clienteServicio.consumir(request);
    }

}
