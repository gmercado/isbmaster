package com.bisa.bus.servicios.segip.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rsalvatierra on 09/03/2016.
 */
@XmlRootElement(name = "lugar_de_nacimiento")
public class DatosNacimiento {

    private String departamento;
    private String provinicia;
    private String localidad;
    private String pais;

    /**
     * @return the departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * @param departamento the departamento to set
     */
    @XmlElement(name = "departamento")
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * @return the provinicia
     */
    public String getProvinicia() {
        return provinicia;
    }

    /**
     * @param provinicia the provinicia to set
     */
    @XmlElement(name = "provincia")
    public void setProvinicia(String provinicia) {
        this.provinicia = provinicia;
    }

    /**
     * @return the localidad
     */
    public String getLocalidad() {
        return localidad;
    }

    /**
     * @param localidad the localidad to set
     */
    @XmlElement(name = "localidad")
    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    /**
     * @return the pais
     */
    public String getPais() {
        return pais;
    }

    /**
     * @param pais to set
     */
    @XmlElement(name = "pais")
    public void setPais(String pais) {
        this.pais = pais;
    }

    @Override
    public String toString() {
        return pais + "-" + departamento + "-" + provinicia + "-" + localidad;
    }
}
