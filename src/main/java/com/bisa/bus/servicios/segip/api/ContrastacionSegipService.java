package com.bisa.bus.servicios.segip.api;

import com.bisa.bus.servicios.segip.entities.ContrastacionSegip;

/**
 * @author rsalvatierra on 21/04/2016.
 */
public interface ContrastacionSegipService {

    ContrastacionSegip getPersonaContrastacion(String pNumeroDocumento, String pComplemento, String pFechaNacimiento);

    ContrastacionSegip getPersonaContrastacion2(String pNumeroDocumento, String pComplemento, String pFechaNacimiento);
}
