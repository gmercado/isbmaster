package com.bisa.bus.servicios.segip.xml;

import bus.plumbing.utils.Convert;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rsalvatierra on 09/03/2016.
 */
@XmlRootElement(name = "Image")
public class Imagen {

    private int id;
    private String type;
    private String im;
    private byte[] image;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    @XmlAttribute(name = "id")
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    @XmlElement(name = "type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the im
     */
    public String getIm() {
        return im;
    }

    /**
     * @param im the im to set
     */
    @XmlElement(name = "image")
    public void setIm(String im) {
        this.im = im;
        this.image = Convert.convertStringToBytesBase64(im);
    }

    /**
     * @return the image
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(byte[] image) {
        this.image = image;
    }
}
