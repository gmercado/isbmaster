package com.bisa.bus.servicios.segip.api;

import com.bisa.bus.servicios.segip.entities.BitacoraSegip;
import org.apache.commons.lang.StringUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import static bus.plumbing.utils.Convert.FileToBytes;

/**
 * @author rsalvatierra on 13/04/2016.
 */
public class ReporteConsumos {
    public static final String NOMBRE_REPORTE = "consumos-SEGIP";
    private static final String separador = ",";

    public static byte[] get(List<BitacoraSegip> consumos) {
        File file;
        BufferedWriter bw;
        try {
            file = File.createTempFile(NOMBRE_REPORTE, ConfiguracionSegip.ARCHIVO_CSV);
            bw = new BufferedWriter(new FileWriter(file));
            bw.append("FECHA").append(separador);
            bw.append("AGENCIA").append(separador);
            bw.append("USUARIO").append(separador);
            bw.append("CANAL").append(separador);
            bw.append("CONSULTA").append(separador);
            bw.append("RESPUESTA").append(separador);
            bw.append("ESTADO").append(separador);

            for (BitacoraSegip orden : consumos) {
                bw.append('\n');
                bw.append(StringUtils.trimToEmpty(orden.getFechaConsumo())).append(separador);
                bw.append(StringUtils.trimToEmpty(orden.getOficina().getDescripcion())).append(separador);
                bw.append(StringUtils.trimToEmpty(orden.getCodigoUsuario())).append(separador);
                bw.append(StringUtils.trimToEmpty(orden.getCanalUsuario())).append(separador);
                bw.append(StringUtils.trimToEmpty(format(orden.getConsultaDocumento()))).append(separador);
                bw.append(StringUtils.trimToEmpty(orden.getCodError())).append(separador);
                bw.append(StringUtils.trimToEmpty(orden.getEstadoConsulta())).append(separador);
            }
            bw.flush();
            bw.close();
        } catch (IOException e) {
            return null;
        }
        return FileToBytes(file);
    }

    private static String format(String cadena) {
        return cadena.replace(",", ";").replace("\n", "").replace("\r", "");
    }
}
