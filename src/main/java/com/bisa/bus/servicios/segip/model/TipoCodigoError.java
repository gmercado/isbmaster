package com.bisa.bus.servicios.segip.model;

/**
 * @author josanchez on 17/03/2016.
 * @author rsalvatierra modificado on 01/04/2016
 */
public enum TipoCodigoError {
    OK("000", "Correcto"),
    //1-100 Errores en contrastacion
    ERR_BD_CON("001", "Error BD contrastacion"),
    //101-200 Errores en WS SEGIP
    ERR_CON_SEGIP("101", "Error consumo SEGIP"),
    NO_EXISTE("102", "No se encontro el registro"),
    DUPLICADO("103", "Se encontro mas de un registro"),
    OBSERVADO("104", "Registro con observacion"),
    SIN_NACIONALIDAD("105", "Registro sin nacionalidad"),
    NO_REALIZO_CONSULTA("200", "No se realizo la busqueda"),
    //900-999 Errores internos
    ERR_ORIGEN("901", "El origen de la solicitud no es valido"),
    ERR_FORMATO("902", "El formato de entrada no es valido"),
    ERR_TIPODOC("903", "El tipo documento no es valido"),
    SERV_NO_HAB("904", "El servicio del SEGIP no esta habilitado para su consumo"),
    ERR_INESPERADO("999", "Error inesperado");

    private String codigo;
    private String descripcion;

    TipoCodigoError(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static TipoCodigoError get(String codigo) {
        for (TipoCodigoError valor : TipoCodigoError.values()) {
            if (valor.getCodigo().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
