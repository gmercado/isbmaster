package com.bisa.bus.servicios.segip.consumer.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author rsalvatierra on 05/05/2016.
 */
@XmlRootElement(name = "UsuarioFinalHabilitaInhabilita", namespace = "http://tempuri.org/")
public class UsuarioFinalHabilitaInhabilitaRequest implements Serializable {
    private int pIdInstitucion;
    private String pUsuario;
    private String pContrasenia;
    private String pClaveAcceso;
    private Boolean pEstado;

    public UsuarioFinalHabilitaInhabilitaRequest() {
    }

    public int getpIdInstitucion() {
        return pIdInstitucion;
    }

    public void setpIdInstitucion(int pIdInstitucion) {
        this.pIdInstitucion = pIdInstitucion;
    }

    public String getpUsuario() {
        return pUsuario;
    }

    public void setpUsuario(String pUsuario) {
        this.pUsuario = pUsuario;
    }

    public String getpContrasenia() {
        return pContrasenia;
    }

    public void setpContrasenia(String pContrasenia) {
        this.pContrasenia = pContrasenia;
    }

    public String getpClaveAcceso() {
        return pClaveAcceso;
    }

    public void setpClaveAcceso(String pClaveAcceso) {
        this.pClaveAcceso = pClaveAcceso;
    }

    public Boolean ispEstado() {
        return pEstado;
    }

    public void setpEstado(Boolean pEstado) {
        this.pEstado = pEstado;
    }
}
