package com.bisa.bus.servicios.segip.queue;

import bus.dataqueue.api.common.DataQueueReader;
import bus.dataqueue.api.common.DataQueueTwoWay;
import bus.dataqueue.api.common.Queue;
import bus.dataqueue.api.extend.DataQueueDelegate;
import bus.dataqueue.api.extend.DataQueueMethod;
import bus.dataqueue.api.extend.IDataQueueData;
import bus.dataqueue.api.extend.ParamCuerpo;
import bus.dataqueue.task.Task;
import com.bisa.bus.servicios.segip.api.SegipProcessService;
import com.bisa.bus.servicios.segip.api.SegipService;
import com.bisa.bus.servicios.segip.entities.ObservacionesSegip;
import com.bisa.bus.servicios.segip.model.*;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * @author rsalvatierra on 07/03/2016.
 */
@Queue
@DataQueueDelegate("segip")
public class LectorSegip implements DataQueueReader {

    public static final String COLA_ENTRADA = "SGP10";
    public static final String COLA_RESPUESTA = "SGP11";
    public static final String NAME = "SEGIP";
    private static final Logger LOGGER = LoggerFactory.getLogger(LectorSegip.class);
    private final Task cola;

    @Inject
    SegipService segip;
    @Inject
    SegipProcessService processService;

    @Inject
    public LectorSegip(@DataQueueTwoWay Task cola, @Named(NAME) String bean) {
        this.cola = cola;
        this.cola.setBean(bean);
        this.cola.setClass(this);
    }

    public Task get() {
        return this.cola;
    }

    /**
     * @param numeroOperacion Codigo de operacion
     * @param numeroDocumento Carnet de Identidad
     * @param tipoDocumento   Extencion CI
     * @param codigoCliente   Codigo Cliente
     * @param usuarioAS       Usuario de AS/400
     * @param sucursal        Sucursal Usuario
     */
    @DataQueueMethod(
            formatoCuerpoLectura = COLA_ENTRADA, formatosEscritura = {COLA_RESPUESTA})
    public void procesarSegip(IDataQueueData iDataQueueData,
                              @ParamCuerpo("SCUOPER") BigDecimal numeroOperacion,
                              @ParamCuerpo("SCUNTID") String numeroDocumento,
                              @ParamCuerpo("SCUSOFF") String tipoDocumento,
                              @ParamCuerpo("SCUNBR") String codigoCliente,
                              @ParamCuerpo("SSCUSER") String usuarioAS,
                              @ParamCuerpo("SSCFREQ") BigDecimal sucursal,
                              @ParamCuerpo("SSPROCE") String tipoSolicitud,
                              @ParamCuerpo("SSFECNA") String fechaNacimiento,
                              @ParamCuerpo("SSBUROS") String buro,
                              @ParamCuerpo("SSFORZA") String forzarConsulta,
                              @ParamCuerpo("SSUSERH") String usuario,
                              @ParamCuerpo("SSDISPO") String terminal,
                              @ParamCuerpo("SSFECHA") String fechaHora,
                              @ParamCuerpo("SSIPTER") String IP) {
        // Contenemos los datos en una estructura completa para el caso
        DatosEscritura datosEscritura = new DatosEscritura();
        datosEscritura.setiDataQueueData(iDataQueueData);
        datosEscritura.setCodigoOperacion(numeroOperacion);
        final String operacion = StringUtils.trimToEmpty(numeroOperacion.toString());
        LOGGER.info("[SEGIP] Recibiendo solicitud de consumo al SEGIP desde el AS400, numero Operacion='{}'", operacion);
        //
        if (operacion.length() > 0) {
            try {
                LOGGER.info("[SEGIP] - Procesamos consulta ...");
                LOGGER.info("[AS/400] - {}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|...", numeroDocumento, tipoDocumento, codigoCliente, usuarioAS, sucursal, tipoSolicitud, fechaNacimiento, buro, forzarConsulta, usuario, terminal, fechaHora, IP);
                //Consumir servicio
                SolicitudConsultaSegip request = new SolicitudConsultaSegip();
                request.setNumeroDocumento(StringUtils.trimToEmpty(numeroDocumento.toUpperCase()));
                request.setTipoDocumento(StringUtils.trimToEmpty(tipoDocumento));
                request.setNumeroAutorizacion(null);
                request.setNumeroCliente(StringUtils.trimToEmpty(codigoCliente));
                request.setTipoSolicitud(TipoSolicitud.get(tipoSolicitud));
                request.setCodigoAgencia(sucursal.toString());
                request.setNombreOficial(StringUtils.trimToEmpty(usuarioAS));
                request.setCodigo(StringUtils.trimToEmpty(operacion));
                request.setCanal(TipoCanal.AS400);
                if (forzarConsulta != null && StringUtils.isNotBlank(forzarConsulta)) {
                    request.setForzarConsulta(Boolean.valueOf(StringUtils.trimToEmpty(forzarConsulta)));
                }
                if (buro != null && StringUtils.isNotBlank(buro)) {
                    request.setTipoBuro(TipoBuro.valueOf(buro));
                } else {
                    request.setTipoBuro(TipoBuro.SEGIP);
                }
                if (fechaNacimiento != null && StringUtils.isNotBlank(fechaNacimiento)) {
                    request.setFechaNacimiento(StringUtils.trimToEmpty(fechaNacimiento));
                }
                ResultadoConsultaSegip response = segip.consultasCertificacionSegip(request);
                if (response != null) {
                    if (response.getSegipWebModel() != null) {
                        datosEscritura.setIdReporte(String.valueOf(response.getSegipWebModel().getId()));
                        datosEscritura.setRutaIFS(response.getSegipWebModel().getPath());
                        datosEscritura.setArchivoIFS(response.getSegipWebModel().getArchivo());
                    }
                    ObservacionesSegip obs = processService.getRespuesta(response);
                    if (obs != null) {
                        datosEscritura.setCodigoRespuesta(obs.getCodigo());
                        if (obs.getDescripcion() != null) {
                            if (StringUtils.trimToEmpty(obs.getDescripcion()).length() > 49) {
                                datosEscritura.setDescripcionRespuesta(StringUtils.trimToEmpty(obs.getDescripcion().toUpperCase()).substring(0, 49));
                            } else {
                                datosEscritura.setDescripcionRespuesta(StringUtils.trimToEmpty(obs.getDescripcion().toUpperCase()));
                            }
                        } else {
                            datosEscritura.setDescripcionRespuesta(TipoCodigoError.ERR_INESPERADO.getDescripcion());
                        }
                    }
                }
                escribirRespuesta(datosEscritura);
            } catch (Exception e) {
                LOGGER.error("Error en el Lector SEGIP: {}", e);
            }
        }
    }

    private void escribirRespuesta(DatosEscritura datosEscritura) {
        datosEscritura.getiDataQueueData().put(COLA_RESPUESTA, "SCUOPER2", datosEscritura.getCodigoOperacion());
        datosEscritura.getiDataQueueData().put(COLA_RESPUESTA, "SIDREPO2", datosEscritura.getIdReporte());
        datosEscritura.getiDataQueueData().put(COLA_RESPUESTA, "SCODRES2", datosEscritura.getCodigoRespuesta());
        datosEscritura.getiDataQueueData().put(COLA_RESPUESTA, "SDESRES2", datosEscritura.getDescripcionRespuesta());
        datosEscritura.getiDataQueueData().put(COLA_RESPUESTA, "SRUTAIFS", datosEscritura.getRutaIFS());
        datosEscritura.getiDataQueueData().put(COLA_RESPUESTA, "SARCHIFS", datosEscritura.getArchivoIFS());
    }

    private static class DatosEscritura {

        private IDataQueueData iDataQueueData;
        private BigDecimal codigoOperacion;
        private String idReporte;
        private String codigoRespuesta;
        private String descripcionRespuesta;
        private String rutaIFS;
        private String archivoIFS;

        private DatosEscritura() {
        }

        public IDataQueueData getiDataQueueData() {
            return iDataQueueData;
        }

        public void setiDataQueueData(IDataQueueData iDataQueueData) {
            this.iDataQueueData = iDataQueueData;
        }


        public BigDecimal getCodigoOperacion() {
            return codigoOperacion;
        }

        public void setCodigoOperacion(BigDecimal codigoOperacion) {
            this.codigoOperacion = codigoOperacion;
        }

        public String getIdReporte() {
            return idReporte;
        }

        public void setIdReporte(String idReporte) {
            this.idReporte = idReporte;
        }

        public String getCodigoRespuesta() {
            return codigoRespuesta;
        }

        public void setCodigoRespuesta(String codigoRespuesta) {
            this.codigoRespuesta = codigoRespuesta;
        }

        public String getDescripcionRespuesta() {
            return descripcionRespuesta;
        }

        public void setDescripcionRespuesta(String descripcionRespuesta) {
            this.descripcionRespuesta = descripcionRespuesta;
        }

        public String getRutaIFS() {
            return rutaIFS;
        }

        public void setRutaIFS(String rutaIFS) {
            this.rutaIFS = rutaIFS;
        }

        public String getArchivoIFS() {
            return archivoIFS;
        }

        public void setArchivoIFS(String archivoIFS) {
            this.archivoIFS = archivoIFS;
        }

        @Override
        public String toString() {
            return "DatosEscritura{" +
                    ", codigoOperacion=" + codigoOperacion +
                    ", idReporte='" + idReporte + '\'' +
                    ", codigoRespuesta='" + codigoRespuesta + '\'' +
                    ", descripcionRespuesta='" + descripcionRespuesta + '\'' +
                    ", rutaIFS='" + rutaIFS + '\'' +
                    ", archivoIFS='" + archivoIFS + '\'' +
                    '}';
        }
    }
}
