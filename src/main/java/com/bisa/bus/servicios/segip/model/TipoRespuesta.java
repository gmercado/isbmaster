package com.bisa.bus.servicios.segip.model;

/**
 * @author rsalvatierra on 05/04/2016.
 */
public enum TipoRespuesta {

    CONSUMO("CM"),
    CONTRASTADO("CT"),
    CONSULTA("CN"),
    ERROR("ER");

    private String descripcion;

    TipoRespuesta(String descripcion) {
        this.descripcion = descripcion;
    }

    public static TipoRespuesta get(String codigo) {
        for (TipoRespuesta valor : TipoRespuesta.values()) {
            if (valor.getDescripcion().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }

    public static TipoRespuesta getId(String valor) {
        if (valor.equals(TipoRespuesta.CONSUMO.toString())) {
            return TipoRespuesta.CONSUMO;
        } else if (valor.equals(TipoRespuesta.CONTRASTADO.toString())) {
            return TipoRespuesta.CONTRASTADO;
        } else if (valor.equals(TipoRespuesta.CONSULTA.toString())) {
            return TipoRespuesta.CONSULTA;
        } else if (valor.equals(TipoRespuesta.ERROR.toString())) {
            return TipoRespuesta.ERROR;
        }
        return null;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
