package com.bisa.bus.servicios.segip.xml;

import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rsalvatierra on 09/03/2016.
 */
@XmlRootElement(name = "informacion_del_ciudadano_nacional")
public class DatosCiudadano {

    private String primerApellido;
    private String segundoApellido;
    private String nombres;
    private String fechaNacimiento;
    private String observaciones;
    private String ci;
    private String tipoCiudadano;

    /**
     * @return the primerApellido
     */
    public String getPrimerApellido() {
        return primerApellido;
    }

    /**
     * @param primerApellido the primerApellido to set
     */
    @XmlElement(name = "primer_apellido")
    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    /**
     * @return the segundoApellido
     */
    public String getSegundoApellido() {
        return segundoApellido;
    }

    /**
     * @param segundoApellido the segundoApellido to set
     */
    @XmlElement(name = "segundo_apellido")
    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    /**
     * @return the nombres
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * @param nombres the nombres to set
     */
    @XmlElement(name = "nombres")
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * @return the FechaNacimiento
     */
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * @param fechaNacimiento the fechaNacimiento to set
     */
    @XmlElement(name = "fecha_de_nacimiento")
    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * @return the observaciones
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * @param observaciones the observaciones to set
     */
    @XmlElement(name = "observacion")
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    /**
     * @return the ci
     */
    public String getCi() {
        return ci;
    }

    /**
     * @param ci the ci to set
     */
    @XmlElement(name = "ci")
    public void setCi(String ci) {
        this.ci = ci;
    }

    /**
     * @return the tipoCiudadano
     */
    public String getTipoCiudadano() {
        return tipoCiudadano;
    }

    /**
     * @param tipoCiudadano to set
     */
    public void setTipoCiudadano(String tipoCiudadano) {
        this.tipoCiudadano = tipoCiudadano;
    }

    @Override
    public String toString() {
        return primerApellido + "-" + segundoApellido + "-" + nombres + "-" + fechaNacimiento + "-" + observaciones + "-" + ci + "-" + tipoCiudadano;
    }

    /***
     *
     */
    public String getNombreCompletoPersonaSegip() {
        return StringUtils.trimToEmpty(getNombres()) + " " + StringUtils.trimToEmpty(getPrimerApellido()) + " " + StringUtils.trimToEmpty(getSegundoApellido());
    }
}
