package com.bisa.bus.servicios.segip.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rsalvatierra on 09/03/2016.
 */
@XmlRootElement(name = "informacion_de_relacion_familiar")
public class DatosFamiliares {

    private String conyuge;

    /**
     * @return the conyuge
     */
    public String getConyuge() {
        return conyuge;
    }

    /**
     * @param conyuge the conyuge to set
     */
    @XmlElement(name = "nombre_del_conyuge")
    public void setConyuge(String conyuge) {
        this.conyuge = conyuge;
    }

    public String toString() {
        return conyuge;
    }
}
