package com.bisa.bus.servicios.segip.api;

import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import com.bisa.bus.servicios.segip.entities.PersonaSegipHist;
import com.bisa.bus.servicios.segip.model.HistoricoSegip;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegip;

import java.util.List;

/**
 * @author rsalvatierra on 21/04/2016.
 */
public interface PersonaSegipHistService {

    List<HistoricoSegip> getHistorico2(PersonaSegip id);

    void actualizaRegistro(SolicitudConsultaSegip request, PersonaSegip personaSegip, PersonaSegipHist personaSegipHist);
}
