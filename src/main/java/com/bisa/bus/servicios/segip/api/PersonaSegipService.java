package com.bisa.bus.servicios.segip.api;

import com.bisa.bus.servicios.segip.consumer.model.RespuestaConsultaCertificacion;
import com.bisa.bus.servicios.segip.entities.ClienteInfoAdicional;
import com.bisa.bus.servicios.segip.entities.ContrastacionSegip;
import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import com.bisa.bus.servicios.segip.model.ResultadoConsultaSegip;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegip;
import com.bisa.bus.servicios.segip.xml.ReporteCertificacion;

//import com.bisa.bus.servicios.segip.ws.stub.RespuestaConsultaCertificacion;

/**
 * @author rsalvatierra on 21/04/2016.
 */
public interface PersonaSegipService {

    PersonaSegip getPersona(SolicitudConsultaSegip request);

    PersonaSegip getPersona(Long id);

    ClienteInfoAdicional getInfoAdicional(SolicitudConsultaSegip request);

    void registrarPersonaBaseContrastacion(ResultadoConsultaSegip respuesta, ContrastacionSegip contrastacionSegip, SolicitudConsultaSegip request);

    void registrarPersonaCertificadaSegip(ReporteCertificacion reporte, SolicitudConsultaSegip request, RespuestaConsultaCertificacion respuestaServicio, ResultadoConsultaSegip respuesta);
}
