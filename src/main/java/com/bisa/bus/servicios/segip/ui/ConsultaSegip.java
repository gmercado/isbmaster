package com.bisa.bus.servicios.segip.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.ControlGroupBorder;
import bus.plumbing.utils.FormatosUtils;
import bus.users.api.Metadatas;
import com.bisa.bus.servicios.segip.api.SegipProcessService;
import com.bisa.bus.servicios.segip.api.SegipService;
import com.bisa.bus.servicios.segip.model.*;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.validator.DateValidator;
import org.apache.wicket.validation.validator.PatternValidator;
import org.apache.wicket.validation.validator.StringValidator;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.Set;

import static bus.users.api.Metadatas.OFICINA_META_DATA_KEY;
import static bus.users.api.Metadatas.USER_META_DATA_KEY;
import static com.bisa.bus.servicios.segip.api.ConfiguracionSegip.MENSAJE_ERROR_CONSULTASEGIP;

/**
 * @author rsalvatierra on 18/03/2016.
 */
@AuthorizeInstantiation({RolesBisa.SEGIP_CONSUMO, RolesBisa.SEGIP_CONSULTAS})
@Menu(value = "Consulta A SEGIP", subMenu = SubMenu.SEGIP)
public class ConsultaSegip extends BisaWebPage {

    @Inject
    SegipService segipService;
    @Inject
    SegipProcessService processService;
    Model<String> css = new Model<>(HIDE);
    private IModel<SolicitudConsultaSegipForm> objectModel;

    public ConsultaSegip() {
        inicio(new SolicitudConsultaSegipForm());
    }

    public ConsultaSegip(SolicitudConsultaSegip solicitud) {
        SolicitudConsultaSegipForm f = new SolicitudConsultaSegipForm();
        f.setNumeroDocumento(solicitud.getNumeroDocumento());
        f.setComplemento(solicitud.getComplemento());
        if (solicitud.getFechaNacimiento() != null && StringUtils.isNotEmpty(solicitud.getFechaNacimiento())
                && StringUtils.isNotBlank(solicitud.getFechaNacimiento())) {
            f.setInfoAdicional(true);
            f.setFechaNacimiento(FormatosUtils.getFechaByFormatoYYYY(solicitud.getFechaNacimiento()));
            css.setObject(SHOW);
        }
        inicio(f);
    }

    private void inicio(SolicitudConsultaSegipForm solicitud) {
        RequiredTextField<String> numeroDocumento;
        TextField<String> complemento;
        Component infoAdicional;
        DateTextField fechaNacimiento;
        ControlGroupBorder ctrlFecha;
        WebMarkupContainer containerInfoAdicional;
        this.objectModel = new CompoundPropertyModel<>(solicitud);
        //Form
        Form<SolicitudConsultaSegipForm> form = new Form<>("buscarSegip", objectModel);
        add(form);
        //Número Documento
        numeroDocumento = new RequiredTextField<>("numeroDocumento");
        numeroDocumento.add(StringValidator.minimumLength(4));
        numeroDocumento.add(StringValidator.maximumLength(15));
        numeroDocumento.add(new PatternValidator(processService.getFormato(1)));
        numeroDocumento.setOutputMarkupId(true);
        form.add(new ControlGroupBorder("numeroDocumento", numeroDocumento, feedbackPanel, Model.of("N\u00famero Documento:")).add(numeroDocumento));
        //Complemento
        complemento = new TextField<>("complemento");
        complemento.add(StringValidator.maximumLength(2));
        complemento.add(new PatternValidator(processService.getFormato(2)));
        form.add(new ControlGroupBorder("complemento", complemento, feedbackPanel, Model.of("Complemento:")).add(complemento));

        //Fecha Nacimiento
        final String datePattern = "dd/MM/yyyy";
        fechaNacimiento = new DateTextField("fechaNacimiento", datePattern);
        fechaNacimiento.setRequired(false).add(DateValidator.maximum(new Date()))
                .add(DateValidator.minimum(new DateTime().minusYears(90).toDate()));
        fechaNacimiento.add(new DatePicker());
        fechaNacimiento.setOutputMarkupId(true);
        fechaNacimiento.setOutputMarkupPlaceholderTag(true);
        ctrlFecha = new ControlGroupBorder("fechaNacimiento", complemento, feedbackPanel, Model.of("Fecha Nacimiento:"));
        ctrlFecha.add(fechaNacimiento);
        ctrlFecha.setOutputMarkupId(true);
        ctrlFecha.setOutputMarkupPlaceholderTag(true);
        form.add(ctrlFecha);
        containerInfoAdicional = new WebMarkupContainer("panelAdicional");
        containerInfoAdicional.setOutputMarkupPlaceholderTag(true);
        containerInfoAdicional.add(ctrlFecha);
        containerInfoAdicional.add(AttributeModifier.append("class", css));
        form.add(containerInfoAdicional);
        //Información adicional
        infoAdicional = new AjaxCheckBox("infoAdicional", new PropertyModel<>(objectModel, "infoAdicional")) {
            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                //containerInfoAdicional.setVisible(getObjetoModel().isInfoAdicional());
                if (getObjetoModel().isInfoAdicional()) {
                    css.setObject(SHOW);
                    fechaNacimiento.setRequired(true);
                } else {
                    css.setObject(HIDE);
                    fechaNacimiento.setRequired(false);
                }
                target.add(containerInfoAdicional);
            }
        }.setLabel(Model.of("Informaci\u00f3n Adicional")).setOutputMarkupId(true);
        form.add(new ControlGroupBorder("infoAdicional", infoAdicional, feedbackPanel, Model.of("Informaci\u00f3n Adicional:")).add(infoAdicional));
        Set<String> roles = getSession().getMetaData(Metadatas.USER_META_DATA_KEY).getRoles();
        boolean TieneRolSegip = roles.contains(RolesBisa.SEGIP_CONSUMO);
        final Component forzarConsulta = new CheckBox("forzarConsulta", new PropertyModel<>(objectModel, "forzarConsulta"))
                .setLabel(Model.of("Consulta directa")).setOutputMarkupId(true).setVisible(TieneRolSegip);
        form.add(new ControlGroupBorder("forzarConsulta", forzarConsulta, feedbackPanel, Model.of("Consulta directa:")).add(forzarConsulta).setVisible(TieneRolSegip));

        //Boton consulta
        form.add(new Button("consulta") {
            @Override
            public void onSubmit() {
                //Obtender datos
                String numeroDocumento = null, complemento = null;
                if (getObjetoModel() != null) {
                    if (StringUtils.isNotBlank(getObjetoModel().getNumeroDocumento()) && StringUtils.isNotEmpty(getObjetoModel().getNumeroDocumento())) {
                        numeroDocumento = StringUtils.trimToNull(getObjetoModel().getNumeroDocumento().toUpperCase());
                    }
                    if (StringUtils.isNotBlank(getObjetoModel().getComplemento()) && StringUtils.isNotEmpty(getObjetoModel().getComplemento())) {
                        complemento = StringUtils.trimToNull(getObjetoModel().getComplemento().toUpperCase());
                    }
                    //Consumir servicio
                    SolicitudConsultaSegip request = new SolicitudConsultaSegip();
                    request.setNumeroDocumento(numeroDocumento);
                    request.setComplemento(complemento);
                    request.setNombreOficial(getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase());
                    request.setCodigoAgencia(getSession().getMetaData(OFICINA_META_DATA_KEY));
                    request.setTipoSolicitud(TipoSolicitud.CON_AQUA2);
                    request.setCanal(TipoCanal.AQUA2);
                    request.setTipoBuro(TipoBuro.SEGIP);
                    request.setForzarConsulta(getObjetoModel().isForzarConsulta());
                    if (getObjetoModel().isInfoAdicional() && getObjetoModel().getFechaNacimiento() != null) {
                        request.setFechaNacimiento(FormatosUtils.fechaFormateadaConYYYY(getObjetoModel().getFechaNacimiento()));
                    }
                    ResultadoConsultaSegip response = segipService.consultasCertificacionSegip(request);
                    if (response != null) {
                        setResponsePage(new RespuestaSegip(request, response));
                    } else {
                        getSession().error(processService.getMensaje(MENSAJE_ERROR_CONSULTASEGIP));
                    }
                }
            }
        });
        form.add(new Button("limpiar") {
            @Override
            public void onSubmit() {
                if ((StringUtils.isNotBlank(getObjetoModel().getNumeroDocumento()) && StringUtils.isNotEmpty(getObjetoModel().getNumeroDocumento()))
                        || (StringUtils.isNotBlank(getObjetoModel().getComplemento()) && StringUtils.isNotEmpty(getObjetoModel().getComplemento()))) {
                    setResponsePage(new ConsultaSegip());
                } else {
                    form.clearInput();
                    getObjetoModel().setInfoAdicional(false);
                    fechaNacimiento.setRequired(false);
                    getObjetoModel().setFechaNacimiento(null);
                    css.setObject(HIDE);
                }
            }
        }.setDefaultFormProcessing(false));
    }

    private SolicitudConsultaSegipForm getObjetoModel() {
        return this.objectModel.getObject();
    }
}
