package com.bisa.bus.servicios.segip.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.ButtonAuthorize;
import bus.plumbing.file.FileTemp;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.segip.api.ContrastacionSegipService;
import com.bisa.bus.servicios.segip.api.PersonaSegipService;
import com.bisa.bus.servicios.segip.api.SegipProcessService;
import com.bisa.bus.servicios.segip.api.SegipService;
import com.bisa.bus.servicios.segip.entities.ContrastacionSegip;
import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import com.bisa.bus.servicios.segip.model.*;
import com.google.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.PopupSettings;
import org.apache.wicket.markup.html.link.ResourceLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.IRequestCycle;
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler;
import org.apache.wicket.request.resource.ContentDisposition;
import org.apache.wicket.request.resource.ResourceStreamResource;
import org.apache.wicket.util.resource.FileResourceStream;
import org.apache.wicket.util.resource.IResourceStream;

import java.io.File;

import static bus.users.api.Metadatas.OFICINA_META_DATA_KEY;
import static bus.users.api.Metadatas.USER_META_DATA_KEY;
import static com.bisa.bus.servicios.segip.api.ConfiguracionSegip.*;

/**
 * @author rsalvatierra on 24/03/2016.
 */
@AuthorizeInstantiation({RolesBisa.SEGIP_CONSUMO, RolesBisa.SEGIP_CONSULTAS})
@Titulo("Detalle Persona - SEGIP")
public class DetallePersonaSegip extends BisaWebPage {

    @Inject
    PersonaSegipService personaSegipService;
    @Inject
    SegipService segipService;
    @Inject
    SegipProcessService processService;
    @Inject
    ContrastacionSegipService contrastacionSegipService;
    private ContrastacionSegip contrastacionSegip;
    private PersonaSegip personaSegip;

    public DetallePersonaSegip() {
        inicio(new PersonaSegip());
    }

    public DetallePersonaSegip(PersonaSegip datosSegip) {
        inicio(datosSegip);
    }

    private void inicio(PersonaSegip datosSegip) {
        personaSegip = datosSegip;
        setDefaultModel(new CompoundPropertyModel<>(datosSegip));

        IModel<PersonaSegip> objectModel = new CompoundPropertyModel<>(datosSegip);
        //Form
        Form<PersonaSegip> form = new Form<>("detallePersonaSegip", objectModel);
        add(form);

        form.add(new Label("codigoCliente"));
        form.add(new Label("numeroDocumento"));
        form.add(new Label("primerApellido"));
        form.add(new Label("segundoApellido"));
        form.add(new Label("nombres"));
        form.add(new Label("fechaNacimientoFormato"));
        form.add(new Label("codigoUnico"));
        form.add(new Label("codigoRespuesta"));
        form.add(new Label("descripcionRespuesta"));
        form.add(new Label("tipoMensaje"));
        form.add(new Label("mensajeTecnico"));
        form.add(new Label("observacion"));
        form.add(new Label("version"));
        form.add(new Label("fechaCreacionFormato"));
        form.add(new Label("usuarioCreador"));

        //botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(ConsultaPersonaSegip.class);
            }
        }.setDefaultFormProcessing(false));
        form.add(new Button("historico") {
            @Override
            public void onSubmit() {
                setResponsePage(new HistoricoPersonaSegip(datosSegip));
            }
        }.setDefaultFormProcessing(false));
        //Verificar si es contrastado
        if (SYSTEM_CONTRASTACION.equals(StringUtils.trimToEmpty(datosSegip.getSistema()))
                && existePersonaBDContrastacion()) {
            String oficial = getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase();
            datosSegip.setReporte(processService.getReporteContrastacion(oficial, contrastacionSegip));
        }
        //Verificar si genera reporte
        if (datosSegip.getReporte() != null) {
            form.add(new ResourceLink("reporte", new ResourceStreamResource() {
                public IResourceStream getResourceStream() {
                    final File file = FileTemp.create(datosSegip.getNumeroDocumento().trim(), ARCHIVO_PDF, datosSegip.getReporte());
                    if (file != null) {
                        final IResourceStream firs = new FileResourceStream(file);
                        getRequestCycle().scheduleRequestHandlerAfterCurrent(
                                new ResourceStreamRequestHandler(firs) {
                                    @Override
                                    public void respond(IRequestCycle requestCycle) {
                                        super.respond(requestCycle);
                                        FileUtils.deleteQuietly(file);
                                    }
                                }.setFileName(datosSegip.getNumeroDocumento().trim()).setContentDisposition(ContentDisposition.ATTACHMENT));
                        return firs;
                    }
                    return null;
                }
            }).setPopupSettings(new PopupSettings("Reporte SEGIP", PopupSettings.RESIZABLE | PopupSettings.SCROLLBARS).setHeight(500).setWidth(700)));
        } else {
            form.add(new Button("reporte") {
                @Override
                public void onSubmit() {
                    getSession().error(processService.getMensaje(MENSAJE_NO_REPORTE));
                }
            });
        }
        form.add(new ButtonAuthorize("actualizar", RolesBisa.SEGIP_CONSUMO) {
            @Override
            public void onSubmit() {
                //Obtender datos
                Long idPersona = datosSegip.getId();
                String numeroDocumento = null, complemento = null;
                if (datosSegip.getNumeroDocumento() != null && datosSegip.getNumeroDocumento().contains(SEPARADOR_DOCUMENTO)) {
                    String[] docs = datosSegip.getNumeroDocumento().split(SEPARADOR_DOCUMENTO);
                    if (docs[0] != null) {
                        numeroDocumento = docs[0];
                    }
                    if (docs[1] != null) {
                        complemento = docs[1];
                    }
                } else {
                    numeroDocumento = datosSegip.getNumeroDocumento();
                    complemento = null;
                }
                //Consumir servicio
                SolicitudConsultaSegip request = new SolicitudConsultaSegip();
                request.setNumeroDocumento(numeroDocumento);
                request.setComplemento(complemento);
                request.setNombreOficial(getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase());
                request.setCodigoAgencia(getSession().getMetaData(OFICINA_META_DATA_KEY));
                request.setTipoSolicitud(TipoSolicitud.CON_AQUA2);
                request.setForzarConsulta(true);
                request.setCanal(TipoCanal.AQUA2);
                request.setTipoBuro(TipoBuro.SEGIP);
                ResultadoConsultaSegip response = segipService.consultasCertificacionSegip(request);
                if (response != null && response.getCodError() != null) {
                    if (TipoCodigoError.OK.equals(response.getCodError())) {
                        setResponsePage(new DetallePersonaSegip(personaSegipService.getPersona(idPersona)));
                    } else {
                        getSession().error(response.getMensajeBean());
                    }
                } else {
                    getSession().error(processService.getMensaje(MENSAJE_ERROR_CONSULTASEGIP));
                }
            }
        }.setDefaultFormProcessing(false));
        Button datos;
        form.add(datos = new Button("datos") {
            @Override
            public void onSubmit() {
                if (datosSegip.getReporte() != null && (datosSegip.getCodigoRespuesta() != null && !StringUtils.isEmpty(datosSegip.getCodigoRespuesta()))) {
                    if (TipoCodigoRespuesta.OK.getCodigo() == Integer.parseInt(StringUtils.trimToEmpty(datosSegip.getCodigoRespuesta()))) {
                        if (SYSTEM_CONTRASTACION.equals(StringUtils.trimToEmpty(datosSegip.getSistema()))) {
                            setResponsePage(new DetalleContrastacion(datosSegip));
                        } else if (datosSegip.getReporte() != null && datosSegip.getReporte().length > 0) {
                            setResponsePage(new DetalleSegip(datosSegip));
                        } else {
                            getSession().error(processService.getMensaje(MENSAJE_NO_REPORTE));
                        }
                    } else {
                        getSession().error(processService.getMensaje(MENSAJE_NO_REPORTE));
                    }
                } else {
                    getSession().error(processService.getMensaje(MENSAJE_NO_REPORTE));
                }
            }
        }.setDefaultFormProcessing(false));
        datos.setVisible(processService.mostrarReporteDatosSegip());
    }

    private boolean existePersonaBDContrastacion() {

        String numeroDocumento = null, complemento = null;
        if (personaSegip.getNumeroDocumento() != null && personaSegip.getNumeroDocumento().contains(SEPARADOR_DOCUMENTO)) {
            String[] docs = personaSegip.getNumeroDocumento().split(SEPARADOR_DOCUMENTO);
            if (docs[0] != null) {
                numeroDocumento = docs[0];
            }
            if (docs[1] != null) {
                complemento = docs[1];
            }
        } else {
            numeroDocumento = personaSegip.getNumeroDocumento();
            complemento = null;
        }
        String fecha = null;
        if (personaSegip.getFechaNacimiento() != null) {
            fecha = FormatosUtils.fechaFormateadaConYYYY(personaSegip.getFechaNacimiento());
        }
        //Contrastacion
        contrastacionSegip = contrastacionSegipService.getPersonaContrastacion(numeroDocumento, complemento, fecha);
        if (contrastacionSegip == null) {
            contrastacionSegip = contrastacionSegipService.getPersonaContrastacion2(numeroDocumento, complemento, fecha);
        }
        return (contrastacionSegip != null);
    }
}
