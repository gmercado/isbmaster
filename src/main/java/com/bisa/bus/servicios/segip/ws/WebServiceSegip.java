package com.bisa.bus.servicios.segip.ws;

import com.bisa.bus.servicios.segip.model.ResultadoConsultaSegip;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegip;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 * @author josanchez on 17/02/2016.
 * @author rsalvatierra modificado on 13/04/2016
 */
@WebService(
        targetNamespace = WebServiceSegip.NAMESPACE,
        name = "WebServiceSegip")
public interface WebServiceSegip {
    String NAMESPACE = "http://www.bisa.com/ebanking/services/soap";

    /**
     * *
     * Descripcion: Consulta Certificacion Datos PersonaSegip
     */
    @WebMethod(operationName = "consultarDatosSEGIP", exclude = false)
    @WebResult(name = "ConsultaDatoPersonaCertificacionResult", targetNamespace = NAMESPACE)
    ResultadoConsultaSegip consultarSEGIP(@WebParam(name = "requestPersonaCertificacion") SolicitudConsultaSegip request);
}
