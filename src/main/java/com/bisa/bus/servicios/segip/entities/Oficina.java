package com.bisa.bus.servicios.segip.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author rsalvatierra on 08/06/2016.
 */
@Entity
@Table(name = "CFP10201")
public class Oficina implements Serializable {

    @Id
    @Column(name = "cfbrch", columnDefinition = "CHAR(3)")
    String codigo;

    @Column(name = "cfbrnm", columnDefinition = "CHAR(100)")
    String nombre;

    @Column(name = "cfbrst", columnDefinition = "CHAR(100)")
    String estado;

    public Oficina() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return getCodigo() + " - " + getNombre().replace("AGENCIA", "");
    }

}
