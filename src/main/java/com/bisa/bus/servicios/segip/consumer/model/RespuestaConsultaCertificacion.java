package com.bisa.bus.servicios.segip.consumer.model;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

/**
 * @author rsalvatierra on 04/05/2016.
 */
public class RespuestaConsultaCertificacion implements Serializable {
    private String reporteCertificacion;
    private Integer codigoRespuesta;
    private String codigoUnico;
    private String descripcionRespuesta;
    private Boolean esValido;
    private String mensaje;
    private String tipoMensaje;

    public RespuestaConsultaCertificacion() {
    }

    @XmlElement(name = "ReporteCertificacion")
    public String getReporteCertificacion() {
        return reporteCertificacion;
    }

    public void setReporteCertificacion(String reporteCertificacion) {
        this.reporteCertificacion = reporteCertificacion;
    }

    @XmlElement(name = "CodigoRespuesta")
    public Integer getCodigoRespuesta() {
        return codigoRespuesta;
    }

    public void setCodigoRespuesta(Integer codigoRespuesta) {
        this.codigoRespuesta = codigoRespuesta;
    }

    @XmlElement(name = "CodigoUnico")
    public String getCodigoUnico() {
        return codigoUnico;
    }

    public void setCodigoUnico(String codigoUnico) {
        this.codigoUnico = codigoUnico;
    }

    @XmlElement(name = "DescripcionRespuesta")
    public String getDescripcionRespuesta() {
        return descripcionRespuesta;
    }

    public void setDescripcionRespuesta(String descripcionRespuesta) {
        this.descripcionRespuesta = descripcionRespuesta;
    }

    @XmlElement(name = "EsValido")
    public Boolean getEsValido() {
        return esValido;
    }

    public void setEsValido(Boolean esValido) {
        this.esValido = esValido;
    }

    @XmlElement(name = "Mensaje")
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @XmlElement(name = "TipoMensaje")
    public String getTipoMensaje() {
        return tipoMensaje;
    }

    public void setTipoMensaje(String tipoMensaje) {
        this.tipoMensaje = tipoMensaje;
    }

    @Override
    public String toString() {
        return "RespuestaConsultaCertificacion{" + "reporteCertificacion=" + reporteCertificacion.length() + '}' + "\n"
                + "getEsValido=" + getEsValido() + "\n"
                + "getCodigoRespuesta=" + getCodigoRespuesta() + "\n"
                + "getDescripcionRespuesta=" + getDescripcionRespuesta() + "\n"
                + "getTipoMensaje=" + getTipoMensaje() + "\n"
                + "getMensaje=" + getMensaje() + "\n"
                + "getCodigoUnico=" + getCodigoUnico() + "\n";
    }
}

