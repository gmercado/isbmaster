package com.bisa.bus.servicios.segip.api;

import bus.plumbing.utils.Convert;
import bus.plumbing.utils.FormatosUtils;
import bus.plumbing.utils.PdfExtractor;
import com.bisa.bus.servicios.segip.xml.ReporteCertificacion;
import com.itextpdf.text.pdf.parser.*;
import nu.xom.Attribute;
import nu.xom.Element;
import nu.xom.Serializer;
import nu.xom.converters.DOMConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author rsalvatierra on 09/03/2016.
 */
public class ReporteDecoded implements IReporteConvert {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private PdfExtractor pdfExtractor;

    /**
     * @param reportEncoded Reporte en formato bytes
     * @return ReporteCertificacion
     */
    public ReporteCertificacion get(byte[] reportEncoded) {
        if (reportEncoded == null) {
            return null;
        }
        //Convetir pdf
        pdfExtractor = new PdfExtractor(reportEncoded);
        //Convertir bytes -> XML
        nu.xom.Document doc = new nu.xom.Document(extractToXML());
        //Convertir XMl -> Bytes
        org.w3c.dom.Document docTemp = getDocument(doc);
        ByteArrayOutputStream baos = getXmlToBytes(docTemp);
        //Convertir Bytes -> Clase
        return extractToClass(baos);
    }

    /**
     * @return Element
     */
    private Element extractToXML() {
        String result, linea = null;
        Element root, rootImage, elementPrimary = null, elementSecondary = null, data;
        Attribute attr;
        String lineas[], campo[] = null;
        List<ImagePdf> listImag;
        //PDF reporte
        result = pdfExtractor.getTextDecode();
        root = new Element("segip");
        if (result != null) {
            lineas = result.split("\\n");
            for (String lineaAux : lineas) {
                try {
                    linea = lineaAux;
                    if (linea.contains("final")) {
                        continue;
                    } else if (linea.contains("verificar")) {
                        if (elementSecondary != null) {
                            root.appendChild(elementPrimary);
                        }
                        break;
                    } else if (linea.contains("Nombre de usuario ")) {
                        linea = linea.replace("Nombre de usuario ", "Nombre de usuario final:");
                    }
                    if (linea.indexOf(":") > 0) {
                        try {
                            campo = linea.split(":");
                            if (campo.length == 2) {
                                elementSecondary = new Element(StringFormat(campo[0], 1));
                                elementSecondary.appendChild(StringFormat(campo[1], 2));
                                if (elementPrimary != null) {
                                    elementPrimary.appendChild(elementSecondary);
                                }
                            } else {
                                elementSecondary = new Element(StringFormat(campo[0], 1));
                                elementSecondary.appendChild(StringFormat(linea.substring(linea.indexOf(":") + 1), 2));
                                if (elementPrimary != null) {
                                    elementPrimary.appendChild(elementSecondary);
                                }
                            }
                        } catch (Exception e) {
                            if (campo != null) {
                                for (String aCampo : campo) {
                                    LOGGER.error("Error en el proceso de conversion a XML {}, {}", aCampo, e);
                                }
                            }
                        }
                    } else {
                        if (elementPrimary == null) {
                            elementPrimary = new Element(StringFormat(linea, 1));
                        } else {
                            if (elementSecondary == null) {
                                elementPrimary.appendChild(new Element(StringFormat(linea, 1)));
                            } else {
                                root.appendChild(elementPrimary);
                                elementPrimary = new Element(StringFormat(linea, 1));
                                elementSecondary = null;
                            }
                        }
                    }
                } catch (Exception e) {
                    LOGGER.error("Error al generar xml, linea {} error {}", linea, e);
                }
            }
            //Imagenes
            listImag = getImagePdfs();
            if (listImag != null) {
                rootImage = new Element("Images");
                for (ImagePdf image : listImag) {
                    Element mage = new Element("Image");
                    attr = new Attribute("id", String.valueOf(image.getRef()));
                    mage.addAttribute(attr);
                    data = new Element("type");
                    data.appendChild(image.getType());
                    mage.appendChild(data);
                    data = new Element("image");
                    data.appendChild(Convert.convertBytesToStringBase64(image.getImage()));
                    mage.appendChild(data);
                    rootImage.appendChild(mage);
                }
                root.appendChild(rootImage);
            }
        }
        return root;
    }

    /**
     * @param cadena texto a formateo
     * @param tipo   tipo de formato
     * @return String
     */
    private String StringFormat(String cadena, int tipo) {
        String response;
        switch (tipo) {
            case 1:
                response = cadena.replace(" ", "_").replace("(", "").replace(")", "").replace("<", "").replace(">", "").replace(" ", "_").replace("-", "").replace(".", "").replace(",", "").replace("/", "").toLowerCase().trim();
                response = FormatosUtils.convertirCaracteresEspeciales(response);
                break;
            case 2:
                response = cadena.replace("<", "").replace(">", "").trim();
                break;
            default:
                response = "";
                break;
        }
        return response;
    }

    /**
     * @param doc Documento formato XML
     * @return Document
     */
    private Document getDocument(nu.xom.Document doc) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        Document document = null;
        DOMImplementation impl = null;
        try {
            builder = factory.newDocumentBuilder();
            impl = builder.getDOMImplementation();
        } catch (ParserConfigurationException e) {
            LOGGER.error("Error en la transformacion a document estandar :" + e);
        }
        if (impl != null) {
            document = DOMConverter.convert(doc, impl);
        }
        return document;
    }

    /**
     * @return Lista de imagenes
     */
    private List<ImagePdf> getImagePdfs() {
        PdfReaderContentParser pdfParser;
        ImageRenderListener imgListener;
        List<ImagePdf> listImag;
        pdfParser = new PdfReaderContentParser(pdfExtractor.getPdfReader());
        imgListener = new ImageRenderListener();
        try {
            imgListener = pdfParser.processContent(1, new ImageRenderListener());
        } catch (IOException e) {
            LOGGER.error("Error en la obtencion de imagenes :" + e);
        }
        listImag = imgListener.getImageData();
        return listImag;
    }

    /**
     * @param path    ruta XML
     * @param reporte pdf extractor
     */
    public void createXML(String path, PdfExtractor reporte) {
        File xmlFile;
        nu.xom.Document doc;
        ByteArrayOutputStream baos;
        try {
            pdfExtractor = reporte;
            doc = new nu.xom.Document(extractToXML());
            baos = new ByteArrayOutputStream();
            Serializer serializer = new Serializer(baos);
            serializer.setIndent(4);
            serializer.write(doc);
            xmlFile = new File(path);
            PrintWriter writer = new PrintWriter(new FileOutputStream(xmlFile));
            writer.println(baos.toString());
            writer.flush();
            writer.close();
        } catch (Exception e) {
            LOGGER.error("Error al generar el XML :", e);
        }
    }

    /**
     * @param docTemp Documento XML
     * @return ByteArrayOutputStream
     */
    private ByteArrayOutputStream getXmlToBytes(Document docTemp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Source xmlSource = new DOMSource(docTemp);
        Result outputTarget = new StreamResult(baos);
        try {
            TransformerFactory.newInstance().newTransformer().transform(xmlSource, outputTarget);
        } catch (TransformerException e) {
            LOGGER.error("Error en la transformacion de xml a bytes :" + e);
        }
        return baos;
    }

    /**
     * @param baos Ingresar Bytes
     * @return ReporteCertificacion
     */
    private ReporteCertificacion extractToClass(ByteArrayOutputStream baos) {
        JAXBContext jaxbContext;
        Unmarshaller jaxbUnmarshaller;
        ReporteCertificacion reporte = null;
        InputStream inputStream;
        try {
            jaxbContext = JAXBContext.newInstance(ReporteCertificacion.class);
            jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            inputStream = new ByteArrayInputStream(baos.toByteArray());
            reporte = (ReporteCertificacion) jaxbUnmarshaller.unmarshal(inputStream);
        } catch (JAXBException e) {
            LOGGER.error("Error al convertir XML a Object :", e);
        }
        return reporte;
    }

    private static class ImagePdf {

        private int ref;
        private byte[] image;
        private String type;

        public ImagePdf(int ref, byte[] image, String type) {
            this.ref = ref;
            this.image = image;
            this.type = type;
        }

        /**
         * @return the ref
         */
        public int getRef() {
            return ref;
        }

        /**
         * @param ref the ref to set
         */
        public void setRef(int ref) {
            this.ref = ref;
        }

        /**
         * @return the image
         */
        public byte[] getImage() {
            return image;
        }

        /**
         * @param image the image to set
         */
        public void setImage(byte[] image) {
            this.image = image;
        }

        /**
         * @return the type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type the type to set
         */
        public void setType(String type) {
            this.type = type;
        }

    }

    private static class ImageRenderListener implements RenderListener {

        private final List<ImagePdf> elements;

        public ImageRenderListener() {
            elements = new ArrayList<>();
        }

        @Override
        public void beginTextBlock() {
        }

        @Override
        public void endTextBlock() {
        }

        @Override
        public void renderImage(ImageRenderInfo renderInfo) {
            try {
                PdfImageObject image = renderInfo.getImage();
                if (image == null) {
                    return;
                }
                ImagePdf p = new ImagePdf(renderInfo.getRef().getNumber(), image.getImageAsBytes(), image.getFileType());
                elements.add(p);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public List<ImagePdf> getImageData() {
            return elements;
        }

        @Override
        public void renderText(TextRenderInfo renderInfo) {

        }

    }
}
