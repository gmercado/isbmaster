package com.bisa.bus.servicios.segip.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.bisa.bus.servicios.segip.api.BitacoraSegipService;
import com.bisa.bus.servicios.segip.entities.BitacoraSegip;
import com.bisa.bus.servicios.segip.entities.Oficina;
import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import com.bisa.bus.servicios.segip.entities.VariableSegip;
import com.bisa.bus.servicios.segip.model.ResultadoConsultaSegip;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegip;
import com.bisa.bus.servicios.segip.model.TipoRespuesta;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.*;

/**
 * @author josanchez on 09/03/2016.
 * @author rsalvatierra modificado on 01/04/2016
 */
public class BitacoraSegipDao extends DaoImpl<BitacoraSegip, Long> implements Serializable, BitacoraSegipService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BitacoraSegipDao.class);
    private final PersonaSegipDao personaSegipDao;

    @Inject
    protected BitacoraSegipDao(@BasePrincipal EntityManagerFactory entityManagerFactory, PersonaSegipDao personaSegipDao) {
        super(entityManagerFactory);
        this.personaSegipDao = personaSegipDao;
    }

    @Override
    public void registrar(SolicitudConsultaSegip request, ResultadoConsultaSegip response, PersonaSegip personaSegip) {
        BitacoraSegip bitacoraSegip = new BitacoraSegip();
        bitacoraSegip.setFechaHoraConsulta(new Date());
        bitacoraSegip.setCodigoUsuario(request.getNombreOficial());
        bitacoraSegip.setCriterioBusqueda(request.toString());
        bitacoraSegip.setOficinaUsuario(request.getCodigoAgencia());
        bitacoraSegip.setCanalUsuario(request.getCanal().getDescripcion());
        bitacoraSegip.setTipoBuro(request.getTipoBuro().getDescripcion());
        if (request.getTipoSolicitud() != null) {
            bitacoraSegip.setTipoUsuario(request.getTipoSolicitud().getCodigo());
        }
        if (personaSegip != null) {
            bitacoraSegip.setPersonaSegip(personaSegipDao.find(personaSegip.getId()));
        } else {
            bitacoraSegip.setPersonaSegip(personaSegipDao.getPersona(request));
        }
        if (bitacoraSegip.getPersonaSegip() != null && bitacoraSegip.getPersonaSegip().getVersion() != null) {
            bitacoraSegip.setNroRevision(bitacoraSegip.getPersonaSegip().getVersion());
        } else {
            bitacoraSegip.setNroRevision(0L);
        }
        if (response != null) {
            bitacoraSegip.setTipoRespuesta(response.getTipoRespuesta().getDescripcion());
            bitacoraSegip.setCodError(response.getCodError().getCodigo());
            bitacoraSegip.setResultadoBusqueda(response.toString());
        }
        persist(bitacoraSegip);
    }

    @Override
    public List<BitacoraSegip> getConsumos(BitacoraSegip model1, BitacoraSegip model2) {
        LOGGER.debug("Obteniendo consumos Segip:{},{}", model1, model2);
        try {
            String codigoUsuario = model1.getCodigoUsuario();
            String tipoRespuesta = (model1.getTipoRespuestaEnum() != null) ? model1.getTipoRespuestaEnum().getDescripcion() : null;
            Date desde = model1.getFechaHoraConsulta();
            Date hasta = model2.getFechaHoraConsulta();
            String tipoCanal = (model1.getTipoCanal() != null) ? model1.getTipoCanal().getNombreParte() : null;
            String tipoOficina = (model1.getOficina() != null) ? model1.getOficina().getCodigo() : null;
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<BitacoraSegip> q = cb.createQuery(BitacoraSegip.class);
                        Root<BitacoraSegip> p = q.from(BitacoraSegip.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        if (StringUtils.trimToNull(codigoUsuario) != null) {
                            predicatesAnd.add(cb.equal(p.get("codigoUsuario"), StringUtils.trimToEmpty(codigoUsuario)));
                        }

                        if (desde != null && hasta != null) {
                            Date inicio = DateUtils.truncate(desde, Calendar.DAY_OF_MONTH);
                            Date inicio2 = DateUtils.truncate(hasta, Calendar.DAY_OF_MONTH);
                            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
                            predicatesAnd.add(cb.between(p.get("fechaHoraConsulta"), inicio, inicio2));
                        }

                        if (StringUtils.trimToNull(tipoRespuesta) != null) {
                            predicatesAnd.add(cb.equal(p.get("tipoRespuesta"), StringUtils.trimToEmpty(tipoRespuesta)));
                        }
                        if (tipoCanal != null) {
                            predicatesAnd.add(cb.equal(p.get("canalUsuario"), tipoCanal));
                        }
                        if (tipoOficina != null) {
                            predicatesAnd.add(cb.equal(p.get("oficinaUsuario"), tipoOficina));
                        }

                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<BitacoraSegip> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta: {}", e);
        }
        return null;
    }

    @Override
    protected Path<Long> countPath(Root<BitacoraSegip> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<BitacoraSegip> p) {
        Path<String> codigoUsuario = p.get("codigoUsuario");
        return Collections.singletonList(codigoUsuario);
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<BitacoraSegip> from, BitacoraSegip example, BitacoraSegip example2) {

        List<Predicate> predicates = new LinkedList<>();
        Date fecha = example.getFechaHoraConsulta();
        String usuario = StringUtils.trimToNull(example.getCodigoUsuario());
        TipoRespuesta tipoRespuesta = example.getTipoRespuestaEnum();
        VariableSegip tipoCanal = example.getTipoCanal();
        Oficina oficina = example.getOficina();
        if (fecha != null && example2.getFechaHoraConsulta() != null) {
            Date inicio = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH);
            Date inicio2 = DateUtils.truncate(example2.getFechaHoraConsulta(), Calendar.DAY_OF_MONTH);
            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
            LOGGER.debug("   FECHAS ENTRE [{}] A [{}]", inicio, inicio2);
            predicates.add(cb.between(from.get("fechaHoraConsulta"), inicio, inicio2));
        }

        if (usuario != null) {
            final String s = StringUtils.trimToEmpty(usuario);
            LOGGER.debug(">>>>> usuario=" + s);
            predicates.add(cb.equal(from.get("codigoUsuario"), s));
        }
        if (tipoRespuesta != null) {
            LOGGER.debug(">>>>> tipoRespuesta =" + tipoRespuesta);
            predicates.add(cb.equal(from.get("tipoRespuesta"), tipoRespuesta.getDescripcion()));
        }
        if (tipoCanal != null) {
            LOGGER.debug(">>>>> tipoCanal =" + tipoCanal);
            predicates.add(cb.equal(from.get("canalUsuario"), tipoCanal.getNombreParte()));
        }
        if (oficina != null) {
            LOGGER.debug(">>>>> tipoOficina =" + oficina);
            predicates.add(cb.equal(from.get("oficinaUsuario"), oficina.getCodigo()));
        }
        /*OpenJPACriteriaQuery<BitacoraSegip> query = cb.createQuery(BitacoraSegip.class);
        Root<BitacoraSegip> root = query.from(BitacoraSegip.class);
        query.where(predicates.toArray(new Predicate[predicates.size()]));
        LOGGER.info("CQL--->" + query.toCQL());*/
        return predicates.toArray(new Predicate[predicates.size()]);
    }
}
