package com.bisa.bus.servicios.segip.consumer.api;

import bus.config.dao.CryptUtils;
import bus.consumoweb.consumer.ClienteServiciosWeb;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import com.bisa.bus.servicios.segip.api.ParametrosSegipService;
import com.bisa.bus.servicios.segip.consumer.model.ConsultaDatoPersonaCertificacionResponse;
import com.google.inject.Inject;

import java.net.URL;
import java.util.concurrent.ExecutorService;

import static com.bisa.bus.servicios.segip.api.ConfiguracionSegip.*;

/**
 * @author rsalvatierra on 22/04/2016.
 */
public class ConsumidorServiciosSegip extends ClienteServiciosWeb {

    public static final String SEGIP = "segip";
    public static final String WSDL_SEGIP = "/com/bisa/bus/servicios/segip/consumer/api/ServicioExternoInstitucion.wsdl";
    private final CryptUtils cryptUtils;
    private final ParametrosSegipService parametros;

    @Inject
    public ConsumidorServiciosSegip(@SQLFueraDeLinea ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente, CryptUtils cryptUtils, ParametrosSegipService parametros) {
        super(executor, consumoWebDao, medioAmbiente);
        this.cryptUtils = cryptUtils;
        this.parametros = parametros;
    }

    public int getCodigoInstitucion() {
        return parametros.getValorIntDe(SEGIP_CODIGO_INSTITUCION, SEGIP_CODIGO_INSTITUCION_DEFAULT);
    }

    public String getUsuario() {
        return parametros.getValorDe(SEGIP_USUARIO, SEGIP_USUARIO_DEFAULT);
    }

    public String getClave() {
        return cryptUtils.dec(parametros.getValorDe(SEGIP_CLAVE, SEGIP_CLAVE_DEFAULT));
    }

    public String getClaveUsuario() {
        return cryptUtils.dec(parametros.getValorDe(SEGIP_CODIGO_USUARIO_FINAL, SEGIP_CODIGO_USUARIO_FINAL_DEFAULT));
    }

    @Override
    protected URL newURL() {
        return ConsumidorServiciosSegip.class.getResource(WSDL_SEGIP);
    }

    @Override
    protected Class getResult() {
        return ConsultaDatoPersonaCertificacionResponse.class;
    }

    @Override
    protected String getPort() {
        return parametros.getValorDe(SEGIP_PORT_NAME, SEGIP_PORT_NAME_DEFAULT);
    }

    @Override
    protected String getEndPoint() {
        return parametros.getValorDe(SEGIP_URL, SEGIP_URL_DEFAULT);
    }

    @Override
    protected String getOperationName() {
        return parametros.getValorDe(SEGIP_OPERATION_NAME, SEGIP_OPERATION_NAME_DEFAULT);
    }

    @Override
    protected String getServiceName() {
        return parametros.getValorDe(SEGIP_SERVICE_NAME, SEGIP_SERVICE_NAME_DEFAULT);
    }

    @Override
    protected void setProxy() {
        String host = parametros.getValorDe(PROXY_HOST, PROXY_HOST_DEFAULT);
        int port = parametros.getValorIntDe(PROXY_PORT, PROXY_PORT_DEFAULT);
        if (host != null && port > 0) {
            System.setProperty(PROXY_HOST, host);
            System.setProperty(PROXY_PORT, String.valueOf(port));
        }
    }

    @Override
    protected String getId() {
        return SEGIP;
    }

    @Override
    protected String getNameSpace() {
        return parametros.getValorDe(SEGIP_NAMESPACE, SEGIP_NAMESPACE_DEFAULT);
    }

}
