package com.bisa.bus.servicios.segip.model;

import java.io.Serializable;

/**
 * @author josanchez on 18/03/2016.
 * @author rsalvatierra modificado on 04/04/2016
 */
public class ResultadoConsultaSegip extends ResultadoExterno implements Serializable {

    private Long IdPersona;
    private TipoCodigoError codError;
    private String descError;
    private TipoRespuesta tipoRespuesta;
    private SegipWebModel segipWebModel;

    public ResultadoConsultaSegip() {
    }

    public SegipWebModel getSegipWebModel() {
        return segipWebModel;
    }

    public void setSegipWebModel(SegipWebModel segipWebModel) {
        this.segipWebModel = segipWebModel;
    }

    public TipoCodigoError getCodError() {
        return codError;
    }

    public void setCodError(TipoCodigoError codError) {
        this.codError = codError;
    }

    public Long getIdPersona() {
        return IdPersona;
    }

    public void setIdPersona(Long idPersona) {
        IdPersona = idPersona;
    }

    public TipoRespuesta getTipoRespuesta() {
        return tipoRespuesta;
    }

    public void setTipoRespuesta(TipoRespuesta tipoRespuesta) {
        this.tipoRespuesta = tipoRespuesta;
    }

    public String getDescError() {
        return descError;
    }

    public void setDescError(String descError) {
        this.descError = descError;
    }

    @Override
    public String toString() {
        return "ResultadoConsultaSegip{" + '\'' +
                ",codigoError=" + getCodError() + '\'' +
                ",tipoRespuesta=" + getTipoRespuesta() + '\'' +
                ",IdPersona=" + getIdPersona() + '\'' +
                ",codigoRespuestaBean=" + getCodigoRespuestaBean() + '\'' +
                ",codigoUnicoBean=" + getCodigoUnicoBean() + '\'' +
                ",descripcionRespuestaBean=" + getDescripcionRespuestaBean() + '\'' +
                ",esValidoBean=" + isEsValidoBean() + '\'' +
                ",tipoMensajeBean='" + getTipoMensajeBean() + '\'' +
                ",mensajeBean='" + getMensajeBean() + '\'' +
                ",segipWebModel=" + segipWebModel + '\'' +
                '}';
    }
}
