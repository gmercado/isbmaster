package com.bisa.bus.servicios.segip.model;

import bus.plumbing.utils.FormatosUtils;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * @author josanchez on 18/03/2016.
 * @author rsalvatierra modificado on 04/04/2016
 */
public class SegipWebModel implements Serializable {

    private Long id;

    private String numeroDocumento;

    private String tipoDocumento;

    private Date fechaCarga;

    private String codigoCliente;

    private String primerApellido;

    private String segundoApellido;

    private String nombres;

    private Date fechaNacimiento;

    private String direccion;

    private String ocupacionProfesion;

    private String estadoCivil;

    private String nombreConyugue;

    private String pais;

    private String departamento;

    private String provincia;

    private String localidad;

    private String tipoNacionalidad;

    private String sistema;

    private byte[] reporte;

    private byte[] fotoPersona;

    private String path;

    private String archivo;

    public SegipWebModel() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Date getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(Date fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getOcupacionProfesion() {
        return ocupacionProfesion;
    }

    public void setOcupacionProfesion(String ocupacionProfesion) {
        this.ocupacionProfesion = ocupacionProfesion;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getNombreConyugue() {
        return nombreConyugue;
    }

    public void setNombreConyugue(String nombreConyugue) {
        this.nombreConyugue = nombreConyugue;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getTipoNacionalidad() {
        return tipoNacionalidad;
    }

    public void setTipoNacionalidad(String tipoNacionalidad) {
        this.tipoNacionalidad = tipoNacionalidad;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public byte[] getReporte() {
        return reporte;
    }

    public void setReporte(byte[] reporte) {
        this.reporte = reporte;
    }

    public byte[] getFotoPersona() {
        return fotoPersona;
    }

    public void setFotoPersona(byte[] fotoPersona) {
        this.fotoPersona = fotoPersona;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", numeroDocumento='" + StringUtils.trimToEmpty(numeroDocumento) + '\'' +
                ", tipoDocumento='" + StringUtils.trimToEmpty(tipoDocumento) + '\'' +
                ", fechaCarga=" + FormatosUtils.formatoFechaHora(fechaCarga) +
                ", codigoCliente='" + StringUtils.trimToEmpty(codigoCliente) + '\'' +
                ", primerApellido='" + StringUtils.trimToEmpty(primerApellido) + '\'' +
                ", segundoApellido='" + StringUtils.trimToEmpty(segundoApellido) + '\'' +
                ", nombres='" + StringUtils.trimToEmpty(nombres) + '\'' +
                ", sistema='" + StringUtils.trimToEmpty(sistema) + '\'' +
                ", path='" + StringUtils.trimToEmpty(path) + '\'' +
                ", archivo='" + StringUtils.trimToEmpty(archivo) + '\'' +
                '}';
    }
}
