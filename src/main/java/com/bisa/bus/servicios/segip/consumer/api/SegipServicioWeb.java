package com.bisa.bus.servicios.segip.consumer.api;

import bus.consumoweb.consumer.ClienteServiciosWeb;
import bus.consumoweb.entities.ConsumoWeb;
import bus.plumbing.utils.Convert;
import bus.plumbing.utils.Watch;
import com.bisa.bus.servicios.segip.consumer.model.ConsultaDatoPersonaCertificacionResponse;
import com.bisa.bus.servicios.segip.consumer.stub.IServicioExternoInstitucion;
import com.bisa.bus.servicios.segip.consumer.stub.RespuestaConsultaCertificacion;
import com.bisa.bus.servicios.segip.consumer.stub.ServicioExternoInstitucion;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegip;
import com.bisa.isb.ws.security.WebServiceHandler;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.ws.BindingProvider;
import java.util.List;
import java.util.concurrent.Future;

import static bus.plumbing.utils.Watches.CONSUMO_WS;

/**
 * @author rsalvatierra on 22/04/2016.
 */
public class SegipServicioWeb {

    protected final Logger LOGGER = LoggerFactory.getLogger(SegipServicioWeb.class);
    private final ConsumidorServiciosSegip consumidorSegip;
    private final ClienteServiciosWeb clienteServicio;

    @Inject
    public SegipServicioWeb(@ConsumidorSegip ClienteServiciosWeb clienteServicio, ConsumidorServiciosSegip consumidorSegip) {
        this.consumidorSegip = consumidorSegip;
        this.clienteServicio = clienteServicio;
    }

    public ConsultaDatoPersonaCertificacionResponse consumo(SolicitudConsultaSegip solicitud) {
        RespuestaConsultaCertificacion respuesta = null;
        Future<ConsumoWeb> consumoWebFuture = null;
        Watch watch = new Watch(CONSUMO_WS);
        int responseCode = 0;
        String cadena = "";
        try {
            watch.start();
            consumoWebFuture = clienteServicio.registrarConsumo(solicitud.toString());
            consumidorSegip.setProxy();
            Integer pCodigoInstitucion = consumidorSegip.getCodigoInstitucion();
            String pUsuario = consumidorSegip.getUsuario();
            String pContrasenia = consumidorSegip.getClave();
            String pClaveAccesoUsuarioFinal = consumidorSegip.getClaveUsuario();
            String pNumeroAutorizacion = solicitud.getNumeroAutorizacion();
            String pNumeroDocumento = solicitud.getNumeroDocumento();
            String pComplemento = solicitud.getComplemento();
            String pNombre = solicitud.getNombre();
            String pPrimerApellido = solicitud.getPrimerApellido();
            String pSegundoApellido = solicitud.getSegundoApellido();
            String pFechaNacimiento = solicitud.getFechaNacimiento();
            ServicioExternoInstitucion servicio = new ServicioExternoInstitucion();
            IServicioExternoInstitucion port = servicio.getHttpBasicConfig();
            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, consumidorSegip.getEndPoint());
            /*Trace Service */
            BindingProvider bindingProvider = ((BindingProvider) port);
            List handlerChain = bindingProvider.getBinding().getHandlerChain();
            handlerChain.add(new WebServiceHandler());
            bindingProvider.getBinding().setHandlerChain(handlerChain);
            /* Run Service */
            respuesta = port.consultaDatoPersonaCertificacion(pCodigoInstitucion, pUsuario, pContrasenia, pClaveAccesoUsuarioFinal, pNumeroAutorizacion, pNumeroDocumento, pComplemento, pNombre, pPrimerApellido, pSegundoApellido, pFechaNacimiento);
            cadena = (respuesta != null) ? respuesta.toString() : "";
            responseCode = 200;
        } catch (Exception e) {
            LOGGER.error("Hubo un error al comunicarse con el SEGIP ", e);
            cadena = e.getMessage();
            responseCode = 400;
        } finally {
            final int milis = (int) watch.stop();
            clienteServicio.actualizaConsumo(cadena, responseCode, milis, consumoWebFuture);
        }
        return (respuesta != null) ? new ConsultaDatoPersonaCertificacionResponse(getRespuestaConsultaCertificacion(respuesta)) : null;
    }

    private com.bisa.bus.servicios.segip.consumer.model.RespuestaConsultaCertificacion getRespuestaConsultaCertificacion(RespuestaConsultaCertificacion respuesta) {
        com.bisa.bus.servicios.segip.consumer.model.RespuestaConsultaCertificacion res = new com.bisa.bus.servicios.segip.consumer.model.RespuestaConsultaCertificacion();
        res.setCodigoRespuesta(respuesta.getCodigoRespuesta());
        res.setCodigoUnico(respuesta.getCodigoUnico().getValue());
        res.setDescripcionRespuesta(respuesta.getDescripcionRespuesta().getValue());
        res.setEsValido(respuesta.isEsValido());
        res.setMensaje(respuesta.getMensaje().getValue());
        if (respuesta.getReporteCertificacion().getValue() != null) {
            res.setReporteCertificacion(Convert.convertBytesToStringBase64(respuesta.getReporteCertificacion().getValue()));
        }
        res.setTipoMensaje(respuesta.getTipoMensaje().getValue());
        return res;
    }
}
