package com.bisa.bus.servicios.segip.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;
import com.bisa.bus.servicios.segip.api.ConfiguracionSegip;
import com.bisa.bus.servicios.segip.dao.PersonaSegipDao;
import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import com.bisa.bus.servicios.segip.model.TipoCodigoRespuesta;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.LinkedList;
import java.util.List;

/**
 * @author rsalvatierra on 03/03/2016.
 */
@AuthorizeInstantiation({RolesBisa.SEGIP_CONSUMO, RolesBisa.SEGIP_CONSULTAS})
@Menu(value = "Consulta Datos SEGIP", subMenu = SubMenu.SEGIP)
public class ConsultaPersonaSegip extends Listado<PersonaSegip, Long> {

    @Override
    protected ListadoPanel<PersonaSegip, Long> newListadoPanel(String id) {

        return new ListadoPanel<PersonaSegip, Long>(id) {
            @Override
            protected List<IColumn<PersonaSegip, String>> newColumns(ListadoDataProvider<PersonaSegip, Long> dataProvider) {
                List<IColumn<PersonaSegip, String>> columns = new LinkedList<>();
                columns.add(new LinkPropertyColumn<PersonaSegip>(Model.of("N\u00famero Documento"), "numeroDocumento", "numeroDocumento") {
                    @Override
                    protected void onClick(PersonaSegip object) {
                        setResponsePage(new DetallePersonaSegip(object));
                    }
                });
                columns.add(new PropertyColumn<>(Model.of("Nombres"), "nombres", "nombres"));
                columns.add(new PropertyColumn<>(Model.of("Apellido Paterno"), "primerApellido", "primerApellido"));
                columns.add(new PropertyColumn<>(Model.of("Apellido Materno"), "segundoApellido", "segundoApellido"));
                columns.add(new PropertyColumn<>(Model.of("Fecha Nacimiento"), "fechaNacimiento", "fechaNacimientoFormato"));
                columns.add(new PropertyColumn<PersonaSegip, String>(Model.of("Observaciones"), "obs") {
                    @Override
                    public void populateItem(Item<ICellPopulator<PersonaSegip>> item, String componentId, final IModel<PersonaSegip> rowModel) {
                        item.add(new Label(componentId, getDataModel(rowModel)).
                                add(new AttributeModifier("class", new AbstractReadOnlyModel<String>() {
                                    @Override
                                    public String getObject() {
                                        if (rowModel.getObject() != null && rowModel.getObject().getObservacion() != null) {
                                            String obs = rowModel.getObject().getObservacion().trim();
                                            if (!StringUtils.isBlank(obs) && !StringUtils.isEmpty(obs)) {
                                                if (ConfiguracionSegip.CONTRASTADO_OBSERVACION.equalsIgnoreCase(obs)) {
                                                    return "btn btn-small disabled btn-success";
                                                } else if (TipoCodigoRespuesta.NO_EXiSTE.getDescripcion().equalsIgnoreCase(obs)) {
                                                    return "btn btn-small disabled btn-info";
                                                } else if (!ConfiguracionSegip.SIN_OBSERVACION.equalsIgnoreCase(obs)) {
                                                    return "btn btn-small disabled btn-danger";
                                                }
                                            }
                                        }
                                        return "mutted";
                                    }
                                })));
                    }
                });
                return columns;
            }

            @Override
            protected Class<? extends Dao<PersonaSegip, Long>> getProviderClazz() {
                return PersonaSegipDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<PersonaSegip> newFiltroPanel(String id, IModel<PersonaSegip> model1, IModel<PersonaSegip> model2) {
                return new ListadoPersonasSegip(id, model1, model2);
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("numeroDocumento", true);
            }

            @Override
            protected PersonaSegip newObject1() {
                return new PersonaSegip();
            }

            @Override
            protected boolean isVisibleData() {
                PersonaSegip cliente = getModel1().getObject();
                if (getModel1().getObject().getNumeroDocumento() != null) {
                    getModel1().getObject().setNumeroDocumento(getModel1().getObject().getNumeroDocumento().toUpperCase());
                }
                return !((cliente.getNumeroDocumento() == null));
            }
        };
    }
}
