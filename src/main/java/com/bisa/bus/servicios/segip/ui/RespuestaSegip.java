package com.bisa.bus.servicios.segip.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.file.FileTemp;
import com.bisa.bus.servicios.segip.api.SegipProcessService;
import com.bisa.bus.servicios.segip.model.ResultadoConsultaSegip;
import com.bisa.bus.servicios.segip.model.SegipWebModel;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegip;
import com.bisa.bus.servicios.segip.model.TipoCodigoRespuesta;
import com.google.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.PopupSettings;
import org.apache.wicket.markup.html.link.ResourceLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.IRequestCycle;
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler;
import org.apache.wicket.request.resource.ContentDisposition;
import org.apache.wicket.request.resource.ResourceStreamResource;
import org.apache.wicket.util.resource.FileResourceStream;
import org.apache.wicket.util.resource.IResourceStream;

import java.io.File;

import static com.bisa.bus.servicios.segip.api.ConfiguracionSegip.*;

/**
 * @author rsalvatierra on 18/03/2016.
 */
@AuthorizeInstantiation({RolesBisa.SEGIP_CONSUMO, RolesBisa.SEGIP_CONSULTAS})
@Titulo("Respuesta SEGIP")
public class RespuestaSegip extends BisaWebPage {

    private final SolicitudConsultaSegip solicitud;
    private final ResultadoConsultaSegip respuestaSegip;
    @Inject
    SegipProcessService processService;

    public RespuestaSegip(SolicitudConsultaSegip solicitud, ResultadoConsultaSegip respuestaSegip) {
        super();
        this.solicitud = solicitud;
        this.respuestaSegip = respuestaSegip;
        inicio();
    }

    public void inicio() {
        //Model
        IModel<ResultadoConsultaSegip> objectModel = new CompoundPropertyModel<>(respuestaSegip);
        //Form
        Form<ResultadoConsultaSegip> form = new Form<>("respuestaSegip", objectModel);
        add(form);
        //Cuerpo
        form.add(new Label("numeroDocumento", Model.of(solicitud.getNumeroDocumento())));
        form.add(new Label("complemento", Model.of(solicitud.getComplemento())));
        form.add(new Label("codigoUnicoBean"));
        form.add(new Label("codigoRespuestaBean"));
        form.add(new Label("descripcionRespuestaBean"));
        form.add(new Label("mensajeBean"));
        form.add(new Label("tipoMensajeBean"));
        form.add(new Label("esValidoBean"));
        //botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(new ConsultaSegip(solicitud));
            }
        });
        if (respuestaSegip.getSegipWebModel() != null && respuestaSegip.getSegipWebModel().getReporte() != null
                && TipoCodigoRespuesta.OK.getCodigo() == respuestaSegip.getCodigoRespuestaBean()) {
            form.add(new ResourceLink("reporte", new ResourceStreamResource() {
                public IResourceStream getResourceStream() {
                    SegipWebModel datosSegip = respuestaSegip.getSegipWebModel();
                    if (datosSegip != null) {
                        final File file = FileTemp.create(datosSegip.getNumeroDocumento().trim(), ARCHIVO_PDF, datosSegip.getReporte());
                        if (file != null) {
                            final IResourceStream firs = new FileResourceStream(file);
                            getRequestCycle().scheduleRequestHandlerAfterCurrent(
                                    new ResourceStreamRequestHandler(firs) {
                                        @Override
                                        public void respond(IRequestCycle requestCycle) {
                                            super.respond(requestCycle);
                                            FileUtils.deleteQuietly(file);
                                        }
                                    }.setFileName(datosSegip.getNumeroDocumento().trim()).setContentDisposition(ContentDisposition.ATTACHMENT));
                            return firs;
                        }
                    }
                    return null;
                }
            }).setPopupSettings(new PopupSettings("Reporte SEGIP", PopupSettings.RESIZABLE | PopupSettings.SCROLLBARS).setHeight(500).setWidth(700)));
        } else {
            form.add(new Button("reporte") {
                @Override
                public void onSubmit() {
                    getSession().error(processService.getMensaje(MENSAJE_NO_REPORTE));
                }
            }.setDefaultFormProcessing(false));
        }
        Button datos;
        form.add(datos = new Button("datos") {
            @Override
            public void onSubmit() {
                if (respuestaSegip.getSegipWebModel() != null && respuestaSegip.getSegipWebModel().getReporte() != null
                        && TipoCodigoRespuesta.OK.getCodigo() == respuestaSegip.getCodigoRespuestaBean()) {
                    if (SYSTEM_CONTRASTACION.equals(StringUtils.trimToEmpty(respuestaSegip.getSegipWebModel().getSistema())) ||
                            StringUtils.trimToNull(respuestaSegip.getCodigoUnicoBean()) == null) {
                        setResponsePage(new DetalleContrastacion(solicitud, respuestaSegip));
                    } else if (respuestaSegip.getSegipWebModel() != null && respuestaSegip.getSegipWebModel().getReporte() != null && respuestaSegip.getSegipWebModel().getReporte().length > 0) {
                        setResponsePage(new DetalleSegip(solicitud, respuestaSegip));
                    } else {
                        getSession().error(processService.getMensaje(MENSAJE_NO_REPORTE));
                    }
                } else {
                    getSession().error(processService.getMensaje(MENSAJE_NO_REPORTE));
                }
            }
        }.setDefaultFormProcessing(false));
        datos.setVisible(processService.mostrarReporteDatosSegip());
    }
}
