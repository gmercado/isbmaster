package com.bisa.bus.servicios.segip.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rsalvatierra on 09/03/2016.
 */
@XmlRootElement(name = "convenio_interinstitucional")
public class DatosCabecera {

    private String sistema;
    private String institucion;
    private String usuario;
    private String usuarioFinal;
    private String fechaImpresion;

    /**
     * @return the sistema
     */
    public String getSistema() {
        return sistema;
    }

    /**
     * @param sistema the sistema to set
     */
    @XmlElement(name = "sistema")
    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    /**
     * @return the institucion
     */
    public String getInstitucion() {
        return institucion;
    }

    /**
     * @param institucion the institucion to set
     */
    @XmlElement(name = "institucion")
    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    @XmlElement(name = "nombre_de_usuario")
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the usuarioFinal
     */
    public String getUsuarioFinal() {
        return usuarioFinal;
    }

    /**
     * @param usuarioFinal the usuarioFinal to set
     */
    @XmlElement(name = "nombre_de_usuario_final")
    public void setUsuarioFinal(String usuarioFinal) {
        this.usuarioFinal = usuarioFinal;
    }

    /**
     * @return the fechaImpresion
     */
    public String getFechaImpresion() {
        return fechaImpresion;
    }

    /**
     * @param fechaImpresion the fechaImpresion to set
     */
    @XmlElement(name = "fecha_de_impresion")
    public void setFechaImpresion(String fechaImpresion) {
        this.fechaImpresion = fechaImpresion;
    }

    @Override
    public String toString() {
        return sistema + "-" + fechaImpresion + "-" + institucion + "-" + usuario + "-" + usuarioFinal;
    }
}
