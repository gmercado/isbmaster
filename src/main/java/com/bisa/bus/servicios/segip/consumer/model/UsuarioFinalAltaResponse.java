package com.bisa.bus.servicios.segip.consumer.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rsalvatierra on 05/05/2016.
 */
@XmlRootElement(name = "UsuarioFinalAltaResponse", namespace = "http://tempuri.org/")
public class UsuarioFinalAltaResponse {
    private RespuestaUsuarioFinalAlta UsuarioFinalAltaResult;

    public UsuarioFinalAltaResponse() {
    }

    @XmlElement(name = "UsuarioFinalAltaResult")
    public RespuestaUsuarioFinalAlta getUsuarioFinalAltaResult() {
        return UsuarioFinalAltaResult;
    }

    public void setUsuarioFinalAltaResult(RespuestaUsuarioFinalAlta usuarioFinalAltaResult) {
        UsuarioFinalAltaResult = usuarioFinalAltaResult;
    }
}
