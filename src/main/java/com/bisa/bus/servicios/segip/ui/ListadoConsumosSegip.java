package com.bisa.bus.servicios.segip.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.segip.api.BitacoraSegipService;
import com.bisa.bus.servicios.segip.api.ConfiguracionSegip;
import com.bisa.bus.servicios.segip.api.ReporteConsumos;
import com.bisa.bus.servicios.segip.dao.BitacoraSegipDao;
import com.bisa.bus.servicios.segip.entities.BitacoraSegip;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler;
import org.apache.wicket.util.resource.AbstractResourceStreamWriter;

import javax.inject.Inject;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * @author rsalvatierra on 23/03/2016.
 */
@AuthorizeInstantiation({RolesBisa.SEGIP_REPORTE})
@Menu(value = "Reporte Consumos SEGIP", subMenu = SubMenu.SEGIP)
public class ListadoConsumosSegip extends Listado<BitacoraSegip, Long> {

    @Inject
    BitacoraSegipService bitacoraSegipService;

    public ListadoConsumosSegip() {
        super();
    }

    @Override
    protected ListadoPanel<BitacoraSegip, Long> newListadoPanel(String id) {

        return new ListadoPanel<BitacoraSegip, Long>(id) {
            @Override
            protected List<IColumn<BitacoraSegip, String>> newColumns(ListadoDataProvider<BitacoraSegip, Long> dataProvider) {
                List<IColumn<BitacoraSegip, String>> columns = new LinkedList<>();
                columns.add(new PropertyColumn<>(Model.of("ID"), "id", "id"));
                columns.add(new PropertyColumn<>(Model.of("Fecha"), "fechaConsumo", "fechaConsumo"));
                columns.add(new PropertyColumn<>(Model.of("Agencia"), "oficina.descripcion", "oficina.descripcion"));
                columns.add(new PropertyColumn<>(Model.of("Usuario"), "codigoUsuario", "codigoUsuario"));
                columns.add(new PropertyColumn<>(Model.of("Canal"), "canalUsuario", "canalUsuario"));
                columns.add(new PropertyColumn<>(Model.of("Consulta"), "consultaDocumento", "consultaDocumento"));
                columns.add(new PropertyColumn<>(Model.of("Respuesta"), "codError", "codError"));
                columns.add(new PropertyColumn<>(Model.of("Estado"), "estadoConsulta", "estadoConsulta"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<BitacoraSegip, Long>> getProviderClazz() {
                return BitacoraSegipDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<BitacoraSegip> newFiltroPanel(String id, IModel<BitacoraSegip> model1, IModel<BitacoraSegip> model2) {
                ListadoConsumosSegipFiltroPanel filtro = new ListadoConsumosSegipFiltroPanel(id, model1, model2);
                filtro.add(new Link("saveExcel") {
                    @Override
                    public void onClick() {
                        if (getModel1() == null || getModel1().getObject() == null) {
                            getSession().error("Hubo un error al exportar el archivo.");
                            return;
                        }
                        AbstractResourceStreamWriter resourceStreamWriter = new AbstractResourceStreamWriter() {
                            @Override
                            public void write(final OutputStream output) throws IOException {
                                List<BitacoraSegip> consumos = bitacoraSegipService.getConsumos(getModel1().getObject(), getModel2().getObject());
                                byte[] reporte = ReporteConsumos.get(consumos);
                                if (reporte != null) {
                                    output.write(reporte);
                                    output.flush();
                                    output.close();
                                }
                            }
                        };

                        getRequestCycle().scheduleRequestHandlerAfterCurrent(new ResourceStreamRequestHandler(resourceStreamWriter)
                                .setFileName(ReporteConsumos.NOMBRE_REPORTE + ConfiguracionSegip.ARCHIVO_CSV));
                    }
                });
                return filtro;
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("fechaHoraConsulta", false);
            }


            @Override
            protected BitacoraSegip newObject2() {
                return new BitacoraSegip();
            }

            @Override
            protected BitacoraSegip newObject1() {
                return new BitacoraSegip();
            }

            @Override
            protected boolean isVisibleData() {
                BitacoraSegip bitacora = getModel1().getObject();
                BitacoraSegip bitacora2 = getModel2().getObject();
                return !((bitacora.getFechaHoraConsulta() == null || bitacora2.getFechaHoraConsulta() == null));
            }
        };
    }

}
