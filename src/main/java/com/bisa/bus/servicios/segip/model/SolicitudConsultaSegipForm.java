package com.bisa.bus.servicios.segip.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * @author josanchez on 04/03/2016.
 * @author rsalvatierra modificado on 30/03/2016
 */
public class SolicitudConsultaSegipForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String numeroDocumento;

    private String tipoDocumento;

    private String complemento;

    private String nombre;

    private String primerApellido;

    private String segundoApellido;

    private Date fechaNacimiento;

    private boolean infoAdicional;

    private boolean forzarConsulta;

    public SolicitudConsultaSegipForm() {
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNumeroDocumentoComplemento() {
        if (StringUtils.isNotBlank(getComplemento()) && StringUtils.isNotEmpty(getComplemento())) {
            return getNumeroDocumento() + "-" + getComplemento();
        }
        return getNumeroDocumento();
    }

    public boolean isInfoAdicional() {
        return infoAdicional;
    }

    public void setInfoAdicional(boolean infoAdicional) {
        this.infoAdicional = infoAdicional;
    }

    public boolean isForzarConsulta() {
        return forzarConsulta;
    }

    public void setForzarConsulta(boolean forzarConsulta) {
        this.forzarConsulta = forzarConsulta;
    }

    @Override
    public String toString() {
        return "SolicitudConsultaSegip{" +
                ", numeroDocumento='" + StringUtils.trimToEmpty(getNumeroDocumento()) + '\'' +
                ", tipoDocumento='" + StringUtils.trimToEmpty(getTipoDocumento()) + '\'' +
                ", complemento='" + StringUtils.trimToEmpty(getComplemento()) + '\'' +
                ", nombre='" + StringUtils.trimToEmpty(getNombre()) + '\'' +
                ", primerApellido='" + StringUtils.trimToEmpty(getPrimerApellido()) + '\'' +
                ", segundoApellido='" + StringUtils.trimToEmpty(getSegundoApellido()) + '\'' +
                ", fechaNacimiento='" + getFechaNacimiento() + '\'' +
                '}';
    }
}
