package com.bisa.bus.servicios.segip.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.utils.Imagen;
import com.bisa.bus.servicios.segip.entities.PersonaSegip;
import com.bisa.bus.servicios.segip.model.ResultadoConsultaSegip;
import com.bisa.bus.servicios.segip.model.SegipWebModel;
import com.bisa.bus.servicios.segip.model.SolicitudConsultaSegip;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;

/**
 * @author rsalvatierra on 18/03/2016.
 */
@AuthorizeInstantiation({RolesBisa.SEGIP_CONSUMO, RolesBisa.SEGIP_CONSULTAS})
@Titulo("Detalle SEGIP")
public class DetalleSegip extends BisaWebPage {

    private SolicitudConsultaSegip solicitud;
    private ResultadoConsultaSegip respuestaSegip;

    public DetalleSegip(SolicitudConsultaSegip solicitud, ResultadoConsultaSegip respuestaSegip) {
        super();
        this.solicitud = solicitud;
        this.respuestaSegip = respuestaSegip;
        inicio(respuestaSegip.getSegipWebModel());
    }

    public DetalleSegip(PersonaSegip datosSegip) {
        super();
        inicio(datosSegip);
    }

    public void inicio(SegipWebModel datosSegip) {

        IModel<SegipWebModel> objectModel = new CompoundPropertyModel<>(datosSegip);
        //Form
        Form<SegipWebModel> form = new Form<>("detalleSegip", objectModel);
        add(form);
        //Cuerpo
        form.add(new Label("numeroDocumento"));
        form.add(new Label("nombres"));
        form.add(new Label("primerApellido"));
        form.add(new Label("segundoApellido"));
        form.add(new Label("fechaNacimiento"));
        form.add(new Label("estadoCivil"));
        form.add(new Label("ocupacionProfesion"));
        form.add(new Label("departamento"));
        form.add(new Label("provincia"));
        form.add(new Label("localidad"));
        form.add(new Label("direccion"));
        //imagen
        if (datosSegip != null && datosSegip.getFotoPersona() != null) {
            form.add(new Image("foto", Imagen.convertToResource(datosSegip.getFotoPersona())));
        }
        //botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(new RespuestaSegip(solicitud, respuestaSegip));
            }
        });
    }

    public void inicio(PersonaSegip datosSegip) {

        IModel<PersonaSegip> objectModel = new CompoundPropertyModel<>(datosSegip);
        //Form
        Form<PersonaSegip> form = new Form<>("detalleSegip", objectModel);
        add(form);
        //Cuerpo
        form.add(new Label("numeroDocumento"));
        form.add(new Label("nombres"));
        form.add(new Label("primerApellido"));
        form.add(new Label("segundoApellido"));
        form.add(new Label("fechaNacimiento"));
        form.add(new Label("estadoCivil"));
        form.add(new Label("ocupacionProfesion"));
        form.add(new Label("departamento"));
        form.add(new Label("provincia"));
        form.add(new Label("localidad"));
        form.add(new Label("direccion"));
        //imagen
        if (datosSegip != null && datosSegip.getFotoPersona() != null) {
            form.add(new Image("foto", Imagen.convertToResource(datosSegip.getFotoPersona())));
        }
        //botones
        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(new DetallePersonaSegip(datosSegip));
            }
        });
    }
}
