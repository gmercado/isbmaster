package com.bisa.bus.servicios.segip.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import com.bisa.bus.servicios.segip.dao.VariableSegipDao;
import com.bisa.bus.servicios.segip.entities.VariableSegip;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.validation.validator.PatternValidator;
import org.apache.wicket.validation.validator.StringValidator;

import java.util.Date;

import static bus.users.api.Metadatas.USER_META_DATA_KEY;

/**
 * @author rsalvatierra on 01/06/2016
 */
@Titulo("Creaci\u00f3n de par\u00E1metro SEGIP")
@AuthorizeInstantiation({RolesBisa.SEGIP_PARAM})
public class NuevaVariableSegip extends BisaWebPage {

    private final IModel<VariableSegip> variableIModel;

    public NuevaVariableSegip() {
        super();

        variableIModel = new CompoundPropertyModel<>(Model.of(new VariableSegip()));

        Form<VariableSegip> crear;
        add(crear = new Form<>("nueva", variableIModel));

        TextField<String> nombre;
        crear.add(nombre = new RequiredTextField<>("nombre"));

        nombre.add(StringValidator.lengthBetween(5, 100));
        nombre.add(new PatternValidator("[A-Za-z0-9_.-]*"));
        nombre.setOutputMarkupId(true);

        TextArea<String> valor;
        crear.add(valor = new TextArea<>("valor"));

        valor.setRequired(true);
        valor.add(StringValidator.maximumLength(1000));
        valor.setOutputMarkupId(true);

        crear.add(new IndicatingAjaxButton("crear") {

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                try {
                    VariableSegip variable = variableIModel.getObject();
                    variable.setUsuarioCreador(getSession().getMetaData(USER_META_DATA_KEY).getUserlogon().toUpperCase());
                    variable.setFechaCreacion(new Date());
                    getInstance(VariableSegipDao.class).persist(variable);
                    info("Variable creada");
                    setResponsePage(ListadoVariablesSegip.class);
                } catch (Exception e) {
                    LOGGER.error("Error inesperado al guardar variable", e);
                    error(e.getMessage());
                    target.add(feedbackPanel);
                }
            }
        });

        crear.add(new Button("cancelar") {

            private static final long serialVersionUID = 62944255371806955L;

            @Override
            public void onSubmit() {
                setResponsePage(ListadoVariablesSegip.class);
                getSession().info("Operaci\u00f3n cancelada");
            }
        }.setDefaultFormProcessing(false));
    }
}
