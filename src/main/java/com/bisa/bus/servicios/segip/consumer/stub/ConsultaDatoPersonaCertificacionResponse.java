//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.02.01 a las 04:08:40 PM BOT 
//


package com.bisa.bus.servicios.segip.consumer.stub;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Clase Java para anonymous complex type.
 * <p/>
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p/>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaDatoPersonaCertificacionResult" type="{http://schemas.datacontract.org/2004/07/Convenio.Core.Modelo.Dto.Proceso.ServicioExterno}RespuestaConsultaCertificacion" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "consultaDatoPersonaCertificacionResult"
})
@XmlRootElement(name = "ConsultaDatoPersonaCertificacionResponse")
public class ConsultaDatoPersonaCertificacionResponse {

    @XmlElementRef(name = "ConsultaDatoPersonaCertificacionResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<RespuestaConsultaCertificacion> consultaDatoPersonaCertificacionResult;

    /**
     * Obtiene el valor de la propiedad consultaDatoPersonaCertificacionResult.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link RespuestaConsultaCertificacion }{@code >}
     */
    public JAXBElement<RespuestaConsultaCertificacion> getConsultaDatoPersonaCertificacionResult() {
        return consultaDatoPersonaCertificacionResult;
    }

    /**
     * Define el valor de la propiedad consultaDatoPersonaCertificacionResult.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link RespuestaConsultaCertificacion }{@code >}
     */
    public void setConsultaDatoPersonaCertificacionResult(JAXBElement<RespuestaConsultaCertificacion> value) {
        this.consultaDatoPersonaCertificacionResult = value;
    }

}
