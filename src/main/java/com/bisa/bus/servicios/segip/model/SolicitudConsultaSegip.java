package com.bisa.bus.servicios.segip.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author josanchez on 04/03/2016.
 * @author rsalvatierra modificado on 30/03/2016
 */
public class SolicitudConsultaSegip implements Serializable {

    private static final long serialVersionUID = 1L;

    private String codigo;

    private String numeroDocumento;

    private String tipoDocumento;

    private String numeroCliente;

    private String codigoAgencia;

    private String nombreOficial;

    private String complemento;

    private TipoSolicitud tipoSolicitud;

    private String numeroAutorizacion;

    private String nombre;

    private String primerApellido;

    private String segundoApellido;

    private String fechaNacimiento;

    private TipoCanal canal;

    private TipoBuro tipoBuro;

    private boolean forzarConsulta;

    public SolicitudConsultaSegip() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNumeroCliente() {
        return numeroCliente;
    }

    public void setNumeroCliente(String numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    public String getCodigoAgencia() {
        return codigoAgencia;
    }

    public void setCodigoAgencia(String codigoAgencia) {
        this.codigoAgencia = codigoAgencia;
    }

    public String getNombreOficial() {
        return nombreOficial;
    }

    public void setNombreOficial(String nombreOficial) {
        this.nombreOficial = nombreOficial;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public TipoSolicitud getTipoSolicitud() {
        return tipoSolicitud;
    }

    public void setTipoSolicitud(TipoSolicitud tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNumeroDocumentoComplemento() {
        if (StringUtils.isNotBlank(getComplemento()) && StringUtils.isNotEmpty(getComplemento())) {
            return getNumeroDocumento() + "-" + getComplemento();
        }
        return getNumeroDocumento();
    }

    public boolean isForzarConsulta() {
        return forzarConsulta;
    }

    public void setForzarConsulta(boolean forzarConsulta) {
        this.forzarConsulta = forzarConsulta;
    }

    public TipoCanal getCanal() {
        return canal;
    }

    public void setCanal(TipoCanal canal) {
        this.canal = canal;
    }

    public TipoBuro getTipoBuro() {
        return tipoBuro;
    }

    public void setTipoBuro(TipoBuro tipoBuro) {
        this.tipoBuro = tipoBuro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SolicitudConsultaSegip)) return false;
        SolicitudConsultaSegip that = (SolicitudConsultaSegip) o;
        return numeroDocumento.equals(that.numeroDocumento) && numeroCliente.equals(that.numeroCliente) && codigoAgencia.equals(that.codigoAgencia) && nombreOficial.equals(that.nombreOficial);
    }

    @Override
    public int hashCode() {
        int result = numeroDocumento.hashCode();
        result = 31 * result + numeroCliente.hashCode();
        result = 31 * result + codigoAgencia.hashCode();
        result = 31 * result + nombreOficial.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SolicitudConsultaSegip{" +
                "codigo='" + StringUtils.trimToEmpty(getCodigo()) + '\'' +
                ", numeroDocumento='" + StringUtils.trimToEmpty(getNumeroDocumento()) + '\'' +
                ", tipoDocumento='" + StringUtils.trimToEmpty(getTipoDocumento()) + '\'' +
                ", numeroCliente='" + StringUtils.trimToEmpty(getNumeroCliente()) + '\'' +
                ", codigoAgencia='" + StringUtils.trimToEmpty(getCodigoAgencia()) + '\'' +
                ", nombreOficial='" + StringUtils.trimToEmpty(getNombreOficial()) + '\'' +
                ", complemento='" + StringUtils.trimToEmpty(getComplemento()) + '\'' +
                ", tipoSolicitud=" + getTipoSolicitud() +
                ", numeroAutorizacion='" + StringUtils.trimToEmpty(getNumeroAutorizacion()) + '\'' +
                ", nombre='" + StringUtils.trimToEmpty(getNombre()) + '\'' +
                ", primerApellido='" + StringUtils.trimToEmpty(getPrimerApellido()) + '\'' +
                ", segundoApellido='" + StringUtils.trimToEmpty(getSegundoApellido()) + '\'' +
                ", fechaNacimiento='" + StringUtils.trimToEmpty(getFechaNacimiento()) + '\'' +
                ", canal='" + getCanal() + '\'' +
                ", forzarConsulta='" + String.valueOf(isForzarConsulta()) + '\'' +
                '}';
    }
}
