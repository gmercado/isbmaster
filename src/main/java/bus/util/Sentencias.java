package bus.util;

/**
 * Created by jjusto on 04/12/2017.
 */
public class Sentencias {

    public static final String OBTENER_ORDENES_CON_SECUENCIA_CERO_POR_FECHA
            = "SELECT a.* FROM achp01 a  WHERE achfeccam = ? AND achnumseq = ? AND achestado = 4 " +
            "AND ACHTIPCNTD = 'CCAD'";


    public static final String OBTENER_SECUENCIA_DE_LA_ORDEN
            = "SELECT XTNUMSEQ FROM PSP802 a WHERE XTFECSIS = ? AND XTCODTRN = ? " +
            "AND (XTCUENTA2 = ? OR XTCUENTA = ? ) AND XTNUMUSR = ? AND XTIMPTOT = ? " +
            "AND XTREVE=' ' AND XTREIMP =' ' AND XTTPODTA <> 20 AND XTPOST='P'";

    public static final String VERIFICAR_SI_SECUENCIA_ESTA_ASIGNADA
            = "SELECT COUNT(a.achnumord) existen FROM achp01 a WHERE ACHFECCAM = ? AND ACHNUMSEQ = ? " +
            "AND ACHNUMCAJA = ?";

    public static final String ACTUALIZAR_NRO_SECUENCIA
            = "UPDATE ACHP01 SET ACHNUMSEQ = ? WHERE ACHNUMORD = ? AND ACHNUMSEQ = 0 AND ACHESTADO = 4";

    public static final String GUARDA_REGISTROS_AMBIENTE_TMP_PROD =
            "INSERT INTO ACHP01R (A1RNUMORD, A1RNUMCAJA, A1RNUMSEQ, A1RTIPMEN, A1RESTADO, A1RTMEPROC, A1RCODRESP," +
                    " A1RDESRESP, A1RESTREV, A1RTMEREV, A1RNUMSEQR, A1RTMECAJA) " +
                    " SELECT ACHNUMORD, ACHNUMCAJA, ACHNUMSEQ, ACHTIPMEN, " +
                    " ACHESTADO, ACHTMEPROC, ACHCODRESP, ACHDESRESP, ACHESTREV, ACHTMEREV, ACHNUMSEQR, ACHTMECAJA FROM ZTBNKPRD01.ACHP01";

    public static final String OBTENER_ORDENES_CON_SECUENCIA_AMBIENTE_TEMP
            = "SELECT A.ACHNUMORD, A.ACHNROLOT, A.ACHCNTORIG, A.ACHCNTDEST, A.ACHCODTRN, A.ACHNUMCAJA, A.ACHIMPORTE, A.ACHTMECAJA, A.ACHORIGEN " +
             " FROM ZBNKPRD01.ACHP01 A WHERE A.achnumord in "+
             "( SELECT B.achnumord FROM ZTBNKPRD01.ACHP01 B WHERE B.ACHESTADO = 4 ) ";

    public static final String ACTUALIZAR_NRO_SECUENCIA2
            = "UPDATE ACHP01 SET ACHNUMSEQ = ? WHERE ACHNUMORD = ? AND ACHESTADO = 4";

    /*** Domiciliacion ***/
    public static String CUENTA_EXISTE = "SELECT 1 FROM tap002 WHERE DMACCT = ?";

    public static String CAMBIO_ESTADO_FUD = "UPDATE ACHP12 SET A12ESTADO = ?, A12OBSERV = ? WHERE A12ID = ?";

    public static String NRO_DEBITOS = "SELECT COUNT(a.A20ACHP12) AS PAGOS FROM ACHP20 a WHERE a.A20ACHP12 = ?";

    public static String BUSCAR_FUD = "SELECT a.*, b.A15IMPMAX, b.A15CODSER, b.A15MONEDA " +
            "FROM ACHP12 a " +
            "JOIN achp15 b ON a.A12ID = b.A15ACHP12 " +
            "WHERE a.A12NUMCTA = ? AND a.A12ACHP14 = ? AND a.A12ESTADO = 'A' ORDER BY a.A12FEC DESC";

    public static String TIPO_DE_CAMBIO = "SELECT GBCOTVENB " +
            "  FROM GLC802 " +
            "    WHERE gbfecef = ( " +
            "      SELECT max(gbfecef) " +
            "        FROM  GLC802 " +
            "    WHERE gbbanco = 1 AND gbcodmon = (SELECT TMCMCO FROM ZBANKTRAD.BTTABCAM WHERE TMCMBT = 'USD')) " +
            "AND gbcodmon = (SELECT TMCMCO FROM ZBANKTRAD.BTTABCAM WHERE TMCMBT = 'USD') " +
            "ORDER BY GBHOREF DESC ";
}
