/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.database.components;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * @author Marcelo Morales
 *         Date: 6/21/11
 *         Time: 5:09 AM
 */
public class BuscarPorTerminosPanel extends Panel {

    private static final long serialVersionUID = -9110065142282340708L;

    public BuscarPorTerminosPanel(String id, IModel<String> model, Component component) {
        super(id, model);

        TextField<String> terminos;
        add(terminos = new TextField<String>("terminos", model, String.class));
        terminos.setOutputMarkupId(true);
        terminos.add(new UpdateComponentOnChangeAjaxBehavior(component));
    }

    private static class UpdateComponentOnChangeAjaxBehavior extends OnChangeAjaxBehavior {

        private static final long serialVersionUID = 8032354481012335432L;

        private final Component component;

        public UpdateComponentOnChangeAjaxBehavior(Component component) {
            this.component = component;
        }

        @Override
        protected void onUpdate(AjaxRequestTarget target) {
            if (target != null) {
                target.add(component);
            }
        }
    }
}
