package bus.database.components;

import bus.consumoweb.infocred.objetos.DatosTitular;
import bus.consumoweb.infocred.objetos.DatosTitularLista;
import bus.consumoweb.infocred.utilitarios.XmlUtil;
import bus.database.dao.Dao;
import bus.database.dao.OrderBySingle;
import bus.plumbing.wicket.BisaWebApplication;
import com.bisa.bus.servicios.asfi.infocred.api.ClienteWebServiceInfocred;
import com.bisa.bus.servicios.asfi.infocred.consumer.stub.Titular;
import com.bisa.bus.servicios.asfi.infocred.consumer.stub.Usuario;
import com.bisa.bus.servicios.asfi.infocred.entities.Titulares;
import com.google.common.base.Strings;
import org.apache.wicket.Application;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.xml.bind.JAXBException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Marcelo Morales
 * @author Roger Chura, modificado 19/04/2013
 */
public class ListadoDataProvider<T extends Serializable, PK extends Serializable> extends SortableDataProvider<T, String> {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ListadoDataProvider.class);
    private static final long serialVersionUID = -5843120266570333159L;
    private final Class<? extends Dao<T, PK>> providerClazz;

    private final EstiloFiltro estiloFiltro;

    private final IModel<T> model1;

    private final IModel<T> model2;

    private final IModel<String> buscar;

    public ListadoDataProvider(SortParam<String> sortParam, Class<? extends Dao<T, PK>> providerClazz,
                               EstiloFiltro estiloFiltro, IModel<T> model1, IModel<T> model2, IModel<String> buscar) {
        super();
        this.providerClazz = providerClazz;
        this.estiloFiltro = estiloFiltro;
        this.model1 = model1;
        this.model2 = model2;
        this.buscar = buscar;
        setSort(sortParam);
    }

    @Override
    public Iterator<? extends T> iterator(long first, long count) {
        LOGGER.debug("ListadoDataProvider.size() ->1 " + Application.get().getClass().getName());
        final Dao<T, PK> dao;
        dao = BisaWebApplication.get().getInstance(providerClazz);

        switch (estiloFiltro) {
            case VACIO:
                if (getFg() == null) {
                    return dao.getTodos(sorteo(getSort()), first, count).iterator();
                } else {
                    return dao.getTodos(null, null, sorteo(getSort()), first, count, getFg()).iterator();
                }

            case BUSQUEDA:
                String terminos = buscar.getObject();

                if (Strings.isNullOrEmpty(terminos)) {
                    if (getFg() == null) {
                        return dao.getTodos(sorteo(getSort()), first, count).iterator();
                    } else {
                        return dao.getTodos(null, null, sorteo(getSort()), first, count, getFg()).iterator();
                    }
                }

                if (getFg() == null) {
                    return dao.buscarPorString(terminos, sorteo(getSort()), first, count).iterator();
                } else {
                    return dao.buscarPorString(terminos, sorteo(getSort()), first, count, getFg()).iterator();
                }

//            case BUSQUEDA_SUBMIT:
//                terminos = buscar.getObject();
//
//                if (getFg() == null) {
//                    Iterator i = dao.buscarPorString(terminos, sorteo(getSort()), first, count).iterator();
//                    if (i != null && i.hasNext()) {
//                        return i;
//                    }
//                    return obtenerTitularesPorWS(terminos);
//                } else {
//                    Iterator i = dao.buscarPorString(terminos, sorteo(getSort()), first, count, getFg()).iterator();
//                    if (i != null && i.hasNext()) {
//                        return i;
//                    }
//                    return obtenerTitularesPorWS(terminos);
//                }

            case FORMULARIO:
                return dao.getTodos(model1.getObject(), model2.getObject(), sorteo(getSort()), first, count, getFg()).iterator();
        }

        throw new IllegalStateException();
    }

    @Override
    public long size() {
        LOGGER.debug("ListadoDataProvider.size() ->2 " + Application.get().getClass().getName());
        final Dao<T, PK> dao;
        dao = BisaWebApplication.get().getInstance(providerClazz);

        switch (estiloFiltro) {
            case VACIO:
                return dao.contar();

            case BUSQUEDA:
                String terminos = buscar.getObject();
                if (Strings.isNullOrEmpty(terminos)) {
                    return dao.contar();
                }
                return dao.contarPorString(terminos);

//            case BUSQUEDA_SUBMIT:
//                terminos = buscar.getObject();
//                int count = dao.contarPorString(terminos);
//                if (count > 0) {
//                    return count;
//                }
//                return contarTitularesPorWS(terminos);

            case FORMULARIO:
                return dao.contar(model1.getObject(), model2.getObject());
        }

        throw new IllegalStateException();
    }

    @Override
    public IModel<T> model(T object) {
        return Model.of(object);
    }

    protected String getFg() {
        return null;
    }

    protected Dao.OrderBy<T> sorteo(final SortParam<String> sort) {
        if (sort == null) {
            return null;
        }

        return new OrderBySingle<T>() {

            @Override
            protected boolean asc() {
                return sort.isAscending();
            }

            @Override
            protected Path<?> getOrderBy(Root<T> tRoot) {
                return orderBy(tRoot, sort.getProperty());
            }
        };
    }

    /**
     * Por favor no devuelva null
     *
     * @param tRoot el "from"
     * @param sort  la propiedad de sort, que no es type-safe
     * @return un path no nulo
     */
    protected Path<?> orderBy(Root<T> tRoot, String sort) {
        if (sort == null) {
            return null;
        }
        return tRoot.get(sort);
    }

    /**
     * INFOCRED hay que hacer algo mas limpio
     */
    // Buscamos por WS
//    public Iterator obtenerTitularesPorWS(String termino) {
//        List<Titulares> list = new ArrayList<>();
//        ClienteWebServiceInfocred cliente = new ClienteWebServiceInfocred();
//        Usuario usuario = new Usuario("6780170","gmercado");
//        Titular titular = new Titular(0, "", termino);
//        String response = cliente.consumo(titular, usuario, ClienteWebServiceInfocred.MetodoWebservice.TITULAR_LIST_BY_NAME);
//        try {
//            DatosTitularLista d = XmlUtil.xmlToObject(response, DatosTitularLista.class);
//            for (DatosTitular datosTitular : d.getTitulares()) {
//                Titulares titulares = new Titulares(datosTitular, "gmercado");
//                list.add(titulares);
//            }
//        } catch (JAXBException e) {
//            LOGGER.error("Error Marshall al obtener los titulares por WS ", e);
//        } catch (Exception e) {
//            LOGGER.error("Error inesperado al obtener los titulares por WS ", e);
//        }
//        return list.iterator();
//    }

//    public int contarTitularesPorWS(String termino) {
//        if (Strings.isNullOrEmpty(termino)) {
//            return 0;
//        }
//        List<Titulares> list = new ArrayList<>();
//        ClienteWebServiceInfocred cliente = new ClienteWebServiceInfocred();
//        Usuario usuario = new Usuario("6780170","gmercado");
//        Titular titular = new Titular(0, "", termino);
//        String response = cliente.consumo(titular, usuario, ClienteWebServiceInfocred.MetodoWebservice.TITULAR_LIST_BY_NAME);
//        try {
//            DatosTitularLista d = XmlUtil.xmlToObject(response, DatosTitularLista.class);
//            for (DatosTitular datosTitular : d.getTitulares()) {
//                Titulares titulares = new Titulares(datosTitular, "gmercado");
//                list.add(titulares);
//            }
//        } catch (JAXBException e) {
//            LOGGER.error("Error Marshall al obtener los titulares por WS ", e);
//        } catch (Exception e) {
//            LOGGER.error("Error inesperado al obtener los titulares por WS ", e);
//        }
//        return list != null ? list.size() : 0;
//    }
}
