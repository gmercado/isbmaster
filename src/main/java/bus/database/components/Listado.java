/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.database.components;

import bus.inicio.ui.BisaWebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.io.Serializable;

/**
 * @author Marcelo Morales
 *         Date: 6/21/11
 *         Time: 4:03 AM
 */
public abstract class Listado<T extends Serializable, PK extends Serializable> extends BisaWebPage {

    protected Listado() {
    }

    protected Listado(PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(newListadoPanel("listado"));
    }

    protected abstract ListadoPanel<T, PK> newListadoPanel(String id);
}
