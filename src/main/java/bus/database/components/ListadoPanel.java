/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.database.components;

import bus.database.dao.Dao;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RadioGroup;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.io.Serializable;
import java.util.List;

import static bus.database.components.EstiloFiltro.BUSQUEDA;

/**
 * @author Marcelo Morales
 */
public abstract class ListadoPanel<T extends Serializable, PK extends Serializable> extends Panel {

    private static final long serialVersionUID = 3480591883919496466L;
    protected DataTable<T, String> table;
    private IModel<T> model1;
    private IModel<T> model2;
    private IModel<String> buscar;

    public ListadoPanel(String id) {
        super(id);
        setOutputMarkupId(true);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        Form<Void> listform;
        add(listform = new Form<Void>("listform"));

        switch (getEstiloFiltro()) {
            case BUSQUEDA:
                buscar = Model.of("");
                break;
            case VACIO:
                buscar = null;
                break;
            case FORMULARIO:
                break;
        }

        ListadoDataProvider<T, PK> dataProvider = newListadoDataProvider(sorteoInicial(), getProviderClazz(), getEstiloFiltro(), getModel1(), getModel2(), getBuscarModel());
        List<IColumn<T, String>> iColumns = newColumns(dataProvider);
        table = newDataTable("datos", iColumns, dataProvider);
        listform.add(new RadioGroup<T>("radiogroup", getSeleccionModel()) {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onConfigure() {
                super.onConfigure();
                setVisible(isVisibleData());
            }

            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }
        }.add(table));

        switch (getEstiloFiltro()) {
            case BUSQUEDA:
                listform.add(newBusquedaPanel("filtro"));
                break;

            case VACIO:
                listform.add(new EmptyPanel("filtro"));
                break;

            case FORMULARIO:
                listform.add(newFiltroPanel("filtro", getModel1(), getModel2()));
                break;
        }

        RepeatingView repeatingView = new RepeatingView("botones de operacion");
        listform.add(repeatingView);
        poblarBotones(repeatingView, "boton", listform);
    }

    protected DataTable<T, String> newDataTable(String id, List<IColumn<T, String>> iColumns, ListadoDataProvider<T, PK> dataProvider) {
        return new AjaxFallbackDefaultDataTable<T, String>(id, iColumns, dataProvider, getRowCount());
    }

    protected boolean isVisibleData() {
        return true;
    }

    protected IModel<T> getSeleccionModel() {
        return Model.of();
    }

    protected ListadoDataProvider<T, PK> newListadoDataProvider(SortParam<String> sortParam,
                                                                Class<? extends Dao<T, PK>> providerClazz,
                                                                EstiloFiltro estiloFiltro,
                                                                IModel<T> model11,
                                                                IModel<T> model21,
                                                                IModel<String> buscarModel) {
        return new ListadoDataProvider<T, PK>(sortParam, providerClazz, estiloFiltro,
                model11, model21, buscarModel);
    }

    /**
     * @param repeatingView andadir Button o AjaxButton o IndicatingAjaxButton
     * @param listform      el formulario contenedor
     */
    protected void poblarBotones(RepeatingView repeatingView, String idButton, Form<Void> listform) {
        repeatingView.setVisible(false);
    }


    protected Panel newBusquedaPanel(String id) {
        return new BuscarPorTerminosPanel(id, getBuscarModel(), table);
    }

    protected Panel newBusquedaSubmitPanel(String id) {
        return new BuscarPorTerminoSubmitPanel(id, getBuscarModel(), table);
    }

    /**
     * Sobre-escribir esto para colocar datos por defecto
     *
     * @return los datos por defecto del primer modelo.
     */
    protected T newObject1() {
        return null;
    }

    protected T newObject2() {
        return null;
    }

    protected FiltroPanel<T> newFiltroPanel(String id, IModel<T> model1, IModel<T> model2) {
        if (EstiloFiltro.FORMULARIO.equals(getEstiloFiltro())) {
            throw new IllegalStateException("Debes sobre-escribir newFiltroPanel");
        }
        throw new IllegalStateException();
    }

    protected int getRowCount() {
        return 15;
    }

    protected abstract List<IColumn<T, String>> newColumns(ListadoDataProvider<T, PK> dataProvider);

    protected abstract Class<? extends Dao<T, PK>> getProviderClazz();

    protected SortParam<String> sorteoInicial() {
        return null;
    }

    protected IModel<T> getModel1() {
        if (model1 == null) {
            model1 = Model.of(newObject1());
        }
        return model1;
    }

    protected IModel<T> getModel2() {
        if (model2 == null) {
            model2 = Model.of(newObject2());
        }
        return model2;
    }

    protected final IModel<String> getBuscarModel() {
        return buscar;
    }

    protected EstiloFiltro getEstiloFiltro() {
        return BUSQUEDA;
    }

    public DataTable<T, String> getTable() {
        return table;
    }

}
