package bus.database.components;

import bus.inicio.ui.BisaWebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.io.Serializable;

/**
 * Created by gmercado on 09/01/2017.
 */
public abstract class ListadoInfocred<T extends Serializable, PK extends Serializable> extends BisaWebPage {
    protected ListadoInfocred() {
    }

    protected ListadoInfocred(PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(newListadoPanel("listado"));
    }

    protected abstract ListadoPanel<T, PK> newListadoPanel(String id);
}
