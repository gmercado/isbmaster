package bus.database.components;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class BuscarPorTerminoSubmitPanel extends Panel {

    public BuscarPorTerminoSubmitPanel(String id, IModel<String> model, Component component) {
        super(id, model);

        TextField<String> terminos;
        add(terminos = new TextField<String>("terminos", model, String.class));
        terminos.setOutputMarkupId(true);

        add(new AjaxButton("buscar") {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                super.onSubmit(target, form);
                target.add(component);
            }
        });
    }
}
