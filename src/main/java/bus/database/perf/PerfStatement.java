/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.perf;

import bus.plumbing.utils.PerfStat;
import bus.plumbing.utils.Watch;

import java.sql.*;
import java.util.Map;

/**
 * @author Marcelo Morales
 * @since 12/23/10 11:53 AM
 */
public class PerfStatement implements Statement {
    protected final Watch counter;
    protected final Watch specificSentence;
    private final Statement delegate;
    private final ThreadLocal<Long> specific;
    private final Map<String, PerfStat> lruMap;
    protected String consulta;

    public PerfStatement(Statement delegate, Watch counter, Map<String, PerfStat> lruMap) {
        this(delegate, counter, lruMap, 0L, null);
    }

    public PerfStatement(Statement delegate, Watch counter, Map<String, PerfStat> lruMap, long timeSoFar, String consulta) {
        this.delegate = delegate;
        this.counter = counter;
        this.lruMap = lruMap;
        specific = new ThreadLocal<>();
        specific.set(timeSoFar);
        specificSentence = new Watch(specific);
        this.consulta = consulta;
    }

    @Override
    public ResultSet executeQuery(String s) throws SQLException {
        this.consulta = s;
        counter.start();
        specificSentence.start();
        ResultSet delegaredExecutor = delegate.executeQuery(s);
        specificSentence.stop();

        if (delegaredExecutor == null) {
            counter.stop();
            return null;
        }
        ResultSet resultSet = new PerfResultSet(delegaredExecutor, counter, specificSentence);
        counter.stop();
        return resultSet;
    }

    @Override
    public int executeUpdate(String s) throws SQLException {
        this.consulta = s;
        counter.start();
        specificSentence.start();
        int i = delegate.executeUpdate(s);
        specificSentence.stop();
        counter.stop();
        return i;
    }

    @Override
    public void close() throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.close();
        specificSentence.stop();
        counter.stop();

        PerfStat perfStat = lruMap.get(consulta);
        if (perfStat == null) {
            perfStat = new PerfStat();
        }
        lruMap.put(consulta, perfStat.add(specific.get()));
        specific.remove();
    }

    @Override
    public int getMaxFieldSize() throws SQLException {
        counter.start();
        int maxFieldSize = delegate.getMaxFieldSize();
        counter.stop();
        return maxFieldSize;
    }

    @Override
    public void setMaxFieldSize(int i) throws SQLException {
        counter.start();
        delegate.setMaxFieldSize(i);
        counter.stop();
    }

    @Override
    public int getMaxRows() throws SQLException {
        counter.start();
        int maxRows = delegate.getMaxRows();
        counter.stop();
        return maxRows;
    }

    @Override
    public void setMaxRows(int i) throws SQLException {
        counter.start();
        delegate.setMaxRows(i);
        counter.stop();
    }

    @Override
    public void setEscapeProcessing(boolean b) throws SQLException {
        counter.start();
        delegate.setEscapeProcessing(b);
        counter.stop();
    }

    @Override
    public int getQueryTimeout() throws SQLException {
        counter.start();
        int queryTimeout = delegate.getQueryTimeout();
        counter.stop();
        return queryTimeout;
    }

    @Override
    public void setQueryTimeout(int i) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.setQueryTimeout(i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void cancel() throws SQLException {
        counter.start();
        delegate.cancel();
        counter.stop();
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        counter.start();
        SQLWarning warnings = delegate.getWarnings();
        counter.stop();
        return warnings;
    }

    @Override
    public void clearWarnings() throws SQLException {
        counter.start();
        delegate.clearWarnings();
        counter.stop();
    }

    @Override
    public void setCursorName(String s) throws SQLException {
        counter.start();
        delegate.setCursorName(s);
        counter.stop();
    }

    @Override
    public boolean execute(String s) throws SQLException {
        this.consulta = s;
        counter.start();
        specificSentence.start();
        boolean execute = delegate.execute(s);
        specificSentence.stop();
        counter.stop();
        return execute;
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        counter.start();
        specificSentence.start();
        ResultSet delegateResultSet = delegate.getResultSet();
        specificSentence.stop();
        if (delegateResultSet == null) {
            counter.stop();
            return null;
        }
        ResultSet resultSet = new PerfResultSet(delegateResultSet, counter, specificSentence);
        counter.stop();
        return resultSet;
    }

    @Override
    public int getUpdateCount() throws SQLException {
        counter.start();
        int updateCount = delegate.getUpdateCount();
        counter.stop();
        return updateCount;
    }

    @Override
    public boolean getMoreResults() throws SQLException {
        counter.start();
        specificSentence.start();
        boolean moreResults = delegate.getMoreResults();
        specificSentence.stop();
        counter.stop();
        return moreResults;
    }

    @Override
    public int getFetchDirection() throws SQLException {
        counter.start();
        int fetchDirection = delegate.getFetchDirection();
        counter.stop();
        return fetchDirection;
    }

    @Override
    public void setFetchDirection(int i) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.setFetchDirection(i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public int getFetchSize() throws SQLException {
        counter.start();
        int fetchSize = delegate.getFetchSize();
        counter.stop();
        return fetchSize;
    }

    @Override
    public void setFetchSize(int i) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.setFetchSize(i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public int getResultSetConcurrency() throws SQLException {
        counter.start();
        int resultSetConcurrency = delegate.getResultSetConcurrency();
        counter.stop();
        return resultSetConcurrency;
    }

    @Override
    public int getResultSetType() throws SQLException {
        counter.start();
        int resultSetType = delegate.getResultSetType();
        counter.stop();
        return resultSetType;
    }

    @Override
    public void addBatch(String s) throws SQLException {
        this.consulta = s;
        counter.start();
        specificSentence.start();
        delegate.addBatch(s);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void clearBatch() throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.clearBatch();
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public int[] executeBatch() throws SQLException {
        counter.start();
        specificSentence.start();
        int[] ints = delegate.executeBatch();
        specificSentence.stop();
        counter.stop();
        return ints;
    }

    @Override
    public Connection getConnection() throws SQLException {
        counter.start();
        Connection connection = new PerfConnection(delegate.getConnection(), counter, lruMap);
        counter.stop();
        return connection;
    }

    @Override
    public boolean getMoreResults(int i) throws SQLException {
        counter.start();
        specificSentence.start();
        boolean moreResults = delegate.getMoreResults(i);
        specificSentence.stop();
        counter.stop();
        return moreResults;
    }

    @Override
    public ResultSet getGeneratedKeys() throws SQLException {
        counter.start();
        specificSentence.start();
        ResultSet delegateGeneratedKeys = delegate.getGeneratedKeys();
        specificSentence.stop();
        ResultSet generatedKeys = new PerfResultSet(delegateGeneratedKeys, counter, specificSentence);
        counter.stop();
        return generatedKeys;
    }

    @Override
    public int executeUpdate(String s, int i) throws SQLException {
        this.consulta = s;
        counter.start();
        specificSentence.start();
        int i1 = delegate.executeUpdate(s, i);
        specificSentence.stop();
        counter.stop();
        return i1;
    }

    @Override
    public int executeUpdate(String s, int[] ints) throws SQLException {
        this.consulta = s;
        counter.start();
        specificSentence.start();
        int i = delegate.executeUpdate(s, ints);
        specificSentence.stop();
        counter.stop();
        return i;
    }

    @Override
    public int executeUpdate(String s, String[] strings) throws SQLException {
        this.consulta = s;
        counter.start();
        specificSentence.start();
        int i = delegate.executeUpdate(s, strings);
        specificSentence.stop();
        counter.stop();
        return i;
    }

    @Override
    public boolean execute(String s, int i) throws SQLException {
        this.consulta = s;
        counter.start();
        specificSentence.start();
        boolean execute = delegate.execute(s, i);
        specificSentence.stop();
        counter.stop();
        return execute;
    }

    @Override
    public boolean execute(String s, int[] ints) throws SQLException {
        this.consulta = s;
        counter.start();
        specificSentence.start();
        boolean execute = delegate.execute(s, ints);
        specificSentence.stop();
        counter.stop();
        return execute;
    }

    @Override
    public boolean execute(String s, String[] strings) throws SQLException {
        this.consulta = s;
        counter.start();
        specificSentence.start();
        boolean execute = delegate.execute(s, strings);
        specificSentence.stop();
        counter.stop();
        return execute;
    }

    @Override
    public int getResultSetHoldability() throws SQLException {
        counter.start();
        int resultSetHoldability = delegate.getResultSetHoldability();
        counter.stop();
        return resultSetHoldability;
    }

    @Override
    public boolean isClosed() throws SQLException {
        counter.start();
        boolean closed = delegate.isClosed();
        counter.stop();
        return closed;
    }

    @Override
    public boolean isPoolable() throws SQLException {
        counter.start();
        boolean poolable = delegate.isPoolable();
        counter.stop();
        return poolable;
    }

    @Override
    public void setPoolable(boolean b) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.setPoolable(b);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public <T> T unwrap(Class<T> tClass) throws SQLException {
        return delegate.unwrap(tClass);
    }

    @Override
    public boolean isWrapperFor(Class<?> aClass) throws SQLException {
        return delegate.isWrapperFor(aClass);
    }

    @Override
    public void closeOnCompletion() throws SQLException {
        delegate.closeOnCompletion();
    }

    @Override
    public boolean isCloseOnCompletion() throws SQLException {
        return delegate.isCloseOnCompletion();
    }
}