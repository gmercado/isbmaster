/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.perf;

import bus.plumbing.utils.Watch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.Map;

/**
 * @author Marcelo Morales
 * @since 12/23/10 12:02 PM
 */
public class PerfResultSet implements ResultSet {

    private static final Logger LOGGER = LoggerFactory.getLogger(PerfResultSet.class);
    protected final Watch counter;
    protected final Watch specificSentence;
    private final ResultSet delegate;

    public PerfResultSet(ResultSet delegate, Watch counter, Watch specificSentence) {
        this.delegate = delegate;
        this.counter = counter;
        this.specificSentence = specificSentence;
    }

    @Override
    public boolean next() throws SQLException {
        counter.start();
        specificSentence.start();
        boolean next = delegate.next();
        specificSentence.stop();
        counter.stop();
        return next;
    }

    @Override
    public void close() throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.close();
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public boolean wasNull() throws SQLException {
        counter.start();
        boolean b = delegate.wasNull();
        counter.stop();
        return b;
    }

    @Override
    public String getString(int i) throws SQLException {
        counter.start();
        String string = delegate.getString(i);
        counter.stop();
        return string;
    }

    @Override
    public boolean getBoolean(int i) throws SQLException {
        counter.start();
        boolean aBoolean = delegate.getBoolean(i);
        counter.stop();
        return aBoolean;
    }

    @Override
    public byte getByte(int i) throws SQLException {
        counter.start();
        byte aByte = delegate.getByte(i);
        counter.stop();
        return aByte;
    }

    @Override
    public short getShort(int i) throws SQLException {
        counter.start();
        short aShort = delegate.getShort(i);
        counter.stop();
        return aShort;
    }

    @Override
    public int getInt(int i) throws SQLException {
        counter.start();
        int anInt = delegate.getInt(i);
        counter.stop();
        return anInt;
    }

    @Override
    public long getLong(int i) throws SQLException {
        counter.start();
        long aLong = delegate.getLong(i);
        counter.stop();
        return aLong;
    }

    @Override
    public float getFloat(int i) throws SQLException {
        counter.start();
        float aFloat = delegate.getFloat(i);
        counter.stop();
        return aFloat;
    }

    @Override
    public double getDouble(int i) throws SQLException {
        counter.start();
        double aDouble = delegate.getDouble(i);
        counter.stop();
        return aDouble;
    }

    @SuppressWarnings({"deprecation"})
    @Override
    @Deprecated
    public BigDecimal getBigDecimal(int i, int i1) throws SQLException {
        counter.start();
        BigDecimal bigDecimal = delegate.getBigDecimal(i, i1);
        counter.stop();
        return bigDecimal;
    }

    @Override
    public byte[] getBytes(int i) throws SQLException {
        counter.start();
        byte[] bytes = delegate.getBytes(i);
        counter.stop();
        return bytes;
    }

    @Override
    public Date getDate(int i) throws SQLException {
        counter.start();
        Date date = delegate.getDate(i);
        counter.stop();
        return date;
    }

    @Override
    public Time getTime(int i) throws SQLException {
        counter.start();
        Time time = delegate.getTime(i);
        counter.stop();
        return time;
    }

    @Override
    public Timestamp getTimestamp(int i) throws SQLException {
        counter.start();
        Timestamp timestamp = delegate.getTimestamp(i);
        counter.stop();
        return timestamp;
    }

    @Override
    public InputStream getAsciiStream(int i) throws SQLException {
        LOGGER.warn("Se ha llamado a getAsciiStream. Los datos del delegado InputStream no se cuentan en las stats",
                new Throwable());
        counter.start();
        InputStream asciiStream = delegate.getAsciiStream(i);
        counter.stop();
        return asciiStream;
    }

    @Override
    @Deprecated
    @SuppressWarnings({"deprecation"})
    public InputStream getUnicodeStream(int i) throws SQLException {
        counter.start();
        InputStream unicodeStream = delegate.getUnicodeStream(i);
        counter.stop();
        return unicodeStream;
    }

    @Override
    public InputStream getBinaryStream(int i) throws SQLException {
        counter.start();
        InputStream binaryStream = delegate.getBinaryStream(i);
        counter.stop();
        return binaryStream;
    }

    @Override
    public String getString(String s) throws SQLException {
        counter.start();
        String string = delegate.getString(s);
        counter.stop();
        return string;
    }

    @Override
    public boolean getBoolean(String s) throws SQLException {
        counter.start();
        boolean aBoolean = delegate.getBoolean(s);
        counter.stop();
        return aBoolean;
    }

    @Override
    public byte getByte(String s) throws SQLException {
        counter.start();
        byte aByte = delegate.getByte(s);
        counter.stop();
        return aByte;
    }

    @Override
    public short getShort(String s) throws SQLException {
        counter.start();
        short aShort = delegate.getShort(s);
        counter.stop();
        return aShort;
    }

    @Override
    public int getInt(String s) throws SQLException {
        counter.start();
        int anInt = delegate.getInt(s);
        counter.stop();
        return anInt;
    }

    @Override
    public long getLong(String s) throws SQLException {
        counter.start();
        long aLong = delegate.getLong(s);
        counter.stop();
        return aLong;
    }

    @Override
    public float getFloat(String s) throws SQLException {
        counter.start();
        float aFloat = delegate.getFloat(s);
        counter.stop();
        return aFloat;
    }

    @Override
    public double getDouble(String s) throws SQLException {
        counter.start();
        double aDouble = delegate.getDouble(s);
        counter.stop();
        return aDouble;
    }

    @Override
    @Deprecated
    @SuppressWarnings({"deprecation"})
    public BigDecimal getBigDecimal(String s, int i) throws SQLException {
        counter.start();
        BigDecimal bigDecimal = delegate.getBigDecimal(s, i);
        counter.stop();
        return bigDecimal;
    }

    @Override
    public byte[] getBytes(String s) throws SQLException {
        counter.start();
        byte[] bytes = delegate.getBytes(s);
        counter.stop();
        return bytes;
    }

    @Override
    public Date getDate(String s) throws SQLException {
        counter.start();
        Date date = delegate.getDate(s);
        counter.stop();
        return date;
    }

    @Override
    public Time getTime(String s) throws SQLException {
        counter.start();
        Time time = delegate.getTime(s);
        counter.stop();
        return time;
    }

    @Override
    public Timestamp getTimestamp(String s) throws SQLException {
        counter.start();
        Timestamp timestamp = delegate.getTimestamp(s);
        counter.stop();
        return timestamp;
    }

    @Override
    public InputStream getAsciiStream(String s) throws SQLException {
        counter.start();
        InputStream asciiStream = delegate.getAsciiStream(s);
        counter.stop();
        return asciiStream;
    }

    @Override
    @Deprecated
    @SuppressWarnings({"deprecation"})
    public InputStream getUnicodeStream(String s) throws SQLException {
        counter.start();
        InputStream unicodeStream = delegate.getUnicodeStream(s);
        counter.stop();
        return unicodeStream;
    }

    @Override
    public InputStream getBinaryStream(String s) throws SQLException {
        counter.start();
        InputStream binaryStream = delegate.getBinaryStream(s);
        counter.stop();
        return binaryStream;
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        counter.start();
        SQLWarning warnings = delegate.getWarnings();
        counter.stop();
        return warnings;
    }

    @Override
    public void clearWarnings() throws SQLException {
        counter.start();
        delegate.clearWarnings();
        counter.stop();
    }

    @Override
    public String getCursorName() throws SQLException {
        counter.start();
        String cursorName = delegate.getCursorName();
        counter.stop();
        return cursorName;
    }

    @Override
    public ResultSetMetaData getMetaData() throws SQLException {
        counter.start();
        ResultSetMetaData metaData = delegate.getMetaData();
        counter.stop();
        return metaData;
    }

    @Override
    public Object getObject(int i) throws SQLException {
        counter.start();
        Object object = delegate.getObject(i);
        counter.stop();
        return object;
    }

    @Override
    public Object getObject(String s) throws SQLException {
        counter.start();
        Object object = delegate.getObject(s);
        counter.stop();
        return object;
    }

    @Override
    public int findColumn(String s) throws SQLException {
        counter.start();
        int column = delegate.findColumn(s);
        counter.stop();
        return column;
    }

    @Override
    public Reader getCharacterStream(int i) throws SQLException {
        LOGGER.warn("Se ha llamado a getCharacterStream. Los datos del delegado Reader no se cuentan en las stats",
                new Throwable());
        counter.start();
        Reader characterStream = delegate.getCharacterStream(i);
        counter.stop();
        return characterStream;
    }

    @Override
    public Reader getCharacterStream(String s) throws SQLException {
        LOGGER.warn("Se ha llamado a getCharacterStream. Los datos del delegado Reader no se cuentan en las stats",
                new Throwable());
        counter.start();
        Reader characterStream = delegate.getCharacterStream(s);
        counter.stop();
        return characterStream;
    }

    @Override
    public BigDecimal getBigDecimal(int i) throws SQLException {
        counter.start();
        BigDecimal bigDecimal = delegate.getBigDecimal(i);
        counter.stop();
        return bigDecimal;
    }

    @Override
    public BigDecimal getBigDecimal(String s) throws SQLException {
        counter.start();
        BigDecimal bigDecimal = delegate.getBigDecimal(s);
        counter.stop();
        return bigDecimal;
    }

    @Override
    public boolean isBeforeFirst() throws SQLException {
        counter.start();
        boolean beforeFirst = delegate.isBeforeFirst();
        counter.stop();
        return beforeFirst;
    }

    @Override
    public boolean isAfterLast() throws SQLException {
        counter.start();
        boolean afterLast = delegate.isAfterLast();
        counter.stop();
        return afterLast;
    }

    @Override
    public boolean isFirst() throws SQLException {
        counter.start();
        boolean first = delegate.isFirst();
        counter.stop();
        return first;
    }

    @Override
    public boolean isLast() throws SQLException {
        counter.start();
        boolean last = delegate.isLast();
        counter.stop();
        return last;
    }

    @Override
    public void beforeFirst() throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.beforeFirst();
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void afterLast() throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.afterLast();
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public boolean first() throws SQLException {
        counter.start();
        specificSentence.start();
        boolean first = delegate.first();
        specificSentence.stop();
        counter.stop();
        return first;
    }

    @Override
    public boolean last() throws SQLException {
        counter.start();
        specificSentence.start();
        boolean last = delegate.last();
        specificSentence.stop();
        counter.stop();
        return last;
    }

    @Override
    public int getRow() throws SQLException {
        counter.start();
        int row = delegate.getRow();
        counter.stop();
        return row;
    }

    @Override
    public boolean absolute(int i) throws SQLException {
        counter.start();
        specificSentence.start();
        boolean absolute = delegate.absolute(i);
        specificSentence.stop();
        counter.stop();
        return absolute;
    }

    @Override
    public boolean relative(int i) throws SQLException {
        counter.start();
        specificSentence.start();
        boolean relative = delegate.relative(i);
        specificSentence.stop();
        counter.stop();
        return relative;
    }

    @Override
    public boolean previous() throws SQLException {
        counter.start();
        specificSentence.start();
        boolean previous = delegate.previous();
        specificSentence.stop();
        counter.stop();
        return previous;
    }

    @Override
    public int getFetchDirection() throws SQLException {
        counter.start();
        int fetchDirection = delegate.getFetchDirection();
        counter.stop();
        return fetchDirection;
    }

    @Override
    public void setFetchDirection(int i) throws SQLException {
        counter.start();
        delegate.setFetchDirection(i);
        counter.stop();
    }

    @Override
    public int getFetchSize() throws SQLException {
        counter.start();
        int fetchSize = delegate.getFetchSize();
        counter.stop();
        return fetchSize;
    }

    @Override
    public void setFetchSize(int i) throws SQLException {
        counter.start();
        delegate.setFetchSize(i);
        counter.stop();
    }

    @Override
    public int getType() throws SQLException {
        counter.start();
        int type = delegate.getType();
        counter.stop();
        return type;
    }

    @Override
    public int getConcurrency() throws SQLException {
        counter.start();
        int concurrency = delegate.getConcurrency();
        counter.stop();
        return concurrency;
    }

    @Override
    public boolean rowUpdated() throws SQLException {
        counter.start();
        boolean b = delegate.rowUpdated();
        counter.stop();
        return b;
    }

    @Override
    public boolean rowInserted() throws SQLException {
        counter.start();
        boolean b = delegate.rowInserted();
        counter.stop();
        return b;
    }

    @Override
    public boolean rowDeleted() throws SQLException {
        counter.start();
        boolean b = delegate.rowDeleted();
        counter.stop();
        return b;
    }

    @Override
    public void updateNull(int i) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNull(i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBoolean(int i, boolean b) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBoolean(i, b);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateByte(int i, byte b) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateByte(i, b);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateShort(int i, short j) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateShort(i, j);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateInt(int i, int i1) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateInt(i, i1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateLong(int i, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateLong(i, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateFloat(int i, float v) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateFloat(i, v);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateDouble(int i, double v) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateDouble(i, v);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBigDecimal(int i, BigDecimal bigDecimal) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBigDecimal(i, bigDecimal);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateString(int i, String s) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateString(i, s);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBytes(int i, byte[] bytes) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBytes(i, bytes);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateDate(int i, Date date) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateDate(i, date);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateTime(int i, Time time) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateTime(i, time);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateTimestamp(int i, Timestamp timestamp) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateTimestamp(i, timestamp);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateAsciiStream(int i, InputStream inputStream, int i1) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateAsciiStream(i, inputStream, i1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBinaryStream(int i, InputStream inputStream, int i1) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBinaryStream(i, inputStream, i1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateCharacterStream(int i, Reader reader, int i1) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateCharacterStream(i, reader, i1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateObject(int i, Object o, int i1) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateObject(i, o, i1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateObject(int i, Object o) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateObject(i, o);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateNull(String s) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNull(s);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBoolean(String s, boolean b) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBoolean(s, b);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateByte(String s, byte b) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateByte(s, b);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateShort(String s, short i) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateShort(s, i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateInt(String s, int i) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateInt(s, i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateLong(String s, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateLong(s, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateFloat(String s, float v) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateFloat(s, v);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateDouble(String s, double v) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateDouble(s, v);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBigDecimal(String s, BigDecimal bigDecimal) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBigDecimal(s, bigDecimal);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateString(String s, String s1) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateString(s, s1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBytes(String s, byte[] bytes) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBytes(s, bytes);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateDate(String s, Date date) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateDate(s, date);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateTime(String s, Time time) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateTime(s, time);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateTimestamp(String s, Timestamp timestamp) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateTimestamp(s, timestamp);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateAsciiStream(String s, InputStream inputStream, int i) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateAsciiStream(s, inputStream, i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBinaryStream(String s, InputStream inputStream, int i) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBinaryStream(s, inputStream, i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateCharacterStream(String s, Reader reader, int i) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateCharacterStream(s, reader, i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateObject(String s, Object o, int i) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateObject(s, o, i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateObject(String s, Object o) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateObject(s, o);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void insertRow() throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.insertRow();
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateRow() throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateRow();
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void deleteRow() throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.deleteRow();
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void refreshRow() throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.refreshRow();
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void cancelRowUpdates() throws SQLException {
        counter.start();
        delegate.cancelRowUpdates();
        counter.stop();
    }

    @Override
    public void moveToInsertRow() throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.moveToInsertRow();
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void moveToCurrentRow() throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.moveToCurrentRow();
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public Statement getStatement() throws SQLException {
        counter.start();
        Statement statement = delegate.getStatement();
        counter.stop();
        return statement;
    }

    @Override
    public Object getObject(int i, Map<String, Class<?>> stringClassMap) throws SQLException {
        counter.start();
        Object object = delegate.getObject(i, stringClassMap);
        counter.stop();
        return object;
    }

    @Override
    public Ref getRef(int i) throws SQLException {
        LOGGER.warn("Se ha llamado a getRef. Los datos del delegado Ref no se cuentan en las stats",
                new Throwable());
        counter.start();
        Ref ref = delegate.getRef(i);
        counter.stop();
        return ref;
    }

    @Override
    public Blob getBlob(int i) throws SQLException {
        counter.start();
        Blob blob = new PerfBlob(delegate.getBlob(i), counter);
        counter.stop();
        return blob;
    }

    @Override
    public Clob getClob(int i) throws SQLException {
        counter.start();
        Clob clob = new PerfClob(delegate.getClob(i), counter);
        counter.stop();
        return clob;
    }

    @Override
    public Array getArray(int i) throws SQLException {
        LOGGER.warn("Se ha llamado a getArray. Los datos del delegado Array no se cuentan en las stats",
                new Throwable());
        counter.start();
        Array array = delegate.getArray(i);
        counter.stop();
        return array;
    }

    @Override
    public Object getObject(String s, Map<String, Class<?>> stringClassMap) throws SQLException {
        counter.start();
        Object object = delegate.getObject(s, stringClassMap);
        counter.stop();
        return object;
    }

    @Override
    public Ref getRef(String s) throws SQLException {
        counter.start();
        Ref ref = delegate.getRef(s);
        counter.stop();
        return ref;
    }

    @Override
    public Blob getBlob(String s) throws SQLException {
        counter.start();
        Blob blob = new PerfBlob(delegate.getBlob(s), counter);
        counter.stop();
        return blob;
    }

    @Override
    public Clob getClob(String s) throws SQLException {
        counter.start();
        Clob clob = new PerfClob(delegate.getClob(s), counter);
        counter.stop();
        return clob;
    }

    @Override
    public Array getArray(String s) throws SQLException {
        counter.start();
        Array array = delegate.getArray(s);
        counter.stop();
        return array;
    }

    @Override
    public Date getDate(int i, Calendar calendar) throws SQLException {
        counter.start();
        Date date = delegate.getDate(i, calendar);
        counter.stop();
        return date;
    }

    @Override
    public Date getDate(String s, Calendar calendar) throws SQLException {
        counter.start();
        Date date = delegate.getDate(s, calendar);
        counter.stop();
        return date;
    }

    @Override
    public Time getTime(int i, Calendar calendar) throws SQLException {
        counter.start();
        Time time = delegate.getTime(i, calendar);
        counter.stop();
        return time;
    }

    @Override
    public Time getTime(String s, Calendar calendar) throws SQLException {
        counter.start();
        Time time = delegate.getTime(s, calendar);
        counter.stop();
        return time;
    }

    @Override
    public Timestamp getTimestamp(int i, Calendar calendar) throws SQLException {
        counter.start();
        Timestamp timestamp = delegate.getTimestamp(i, calendar);
        counter.stop();
        return timestamp;
    }

    @Override
    public Timestamp getTimestamp(String s, Calendar calendar) throws SQLException {
        counter.start();
        Timestamp timestamp = delegate.getTimestamp(s, calendar);
        counter.stop();
        return timestamp;
    }

    @Override
    public URL getURL(int i) throws SQLException {
        counter.start();
        URL url = delegate.getURL(i);
        counter.stop();
        return url;
    }

    @Override
    public URL getURL(String s) throws SQLException {
        counter.start();
        URL url = delegate.getURL(s);
        counter.stop();
        return url;
    }

    @Override
    public void updateRef(int i, Ref ref) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateRef(i, ref);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateRef(String s, Ref ref) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateRef(s, ref);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBlob(int i, Blob blob) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBlob(i, blob);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBlob(String s, Blob blob) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBlob(s, blob);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateClob(int i, Clob clob) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateClob(i, clob);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateClob(String s, Clob clob) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateClob(s, clob);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateArray(int i, Array array) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateArray(i, array);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateArray(String s, Array array) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateArray(s, array);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public RowId getRowId(int i) throws SQLException {
        counter.start();
        RowId rowId = delegate.getRowId(i);
        counter.stop();
        return rowId;
    }

    @Override
    public RowId getRowId(String s) throws SQLException {
        LOGGER.warn("Se ha llamado a getRowId. Los datos del delegado RowId no se cuentan en las stats",
                new Throwable());
        counter.start();
        RowId rowId = delegate.getRowId(s);
        counter.stop();
        return rowId;
    }

    @Override
    public void updateRowId(int i, RowId rowId) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateRowId(i, rowId);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateRowId(String s, RowId rowId) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateRowId(s, rowId);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public int getHoldability() throws SQLException {
        counter.start();
        int holdability = delegate.getHoldability();
        counter.stop();
        return holdability;
    }

    @Override
    public boolean isClosed() throws SQLException {
        counter.start();
        boolean closed = delegate.isClosed();
        counter.stop();
        return closed;
    }

    @Override
    public void updateNString(int i, String s) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNString(i, s);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateNString(String s, String s1) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNString(s, s1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateNClob(int i, NClob nClob) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNClob(i, nClob);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateNClob(String s, NClob nClob) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNClob(s, nClob);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public NClob getNClob(int i) throws SQLException {
        LOGGER.warn("Se ha llamado a getNClob. Los datos del delegado NClob no se cuentan en las stats",
                new Throwable());
        counter.start();
        NClob nClob = delegate.getNClob(i);
        counter.stop();
        return nClob;
    }

    @Override
    public NClob getNClob(String s) throws SQLException {
        counter.start();
        NClob nClob = delegate.getNClob(s);
        counter.stop();
        return nClob;
    }

    @Override
    public SQLXML getSQLXML(int i) throws SQLException {
        LOGGER.warn("Se ha llamado a getSQLXML. Los datos del delegado SQLXML no se cuentan en las stats",
                new Throwable());
        counter.start();
        SQLXML sqlxml = delegate.getSQLXML(i);
        counter.stop();
        return sqlxml;
    }

    @Override
    public SQLXML getSQLXML(String s) throws SQLException {
        counter.start();
        SQLXML sqlxml = delegate.getSQLXML(s);
        counter.stop();
        return sqlxml;
    }

    @Override
    public void updateSQLXML(int i, SQLXML sqlxml) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateSQLXML(i, sqlxml);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateSQLXML(String s, SQLXML sqlxml) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateSQLXML(s, sqlxml);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public String getNString(int i) throws SQLException {
        counter.start();
        String nString = delegate.getNString(i);
        counter.stop();
        return nString;
    }

    @Override
    public String getNString(String s) throws SQLException {
        counter.start();
        String nString = delegate.getNString(s);
        counter.stop();
        return nString;
    }

    @Override
    public Reader getNCharacterStream(int i) throws SQLException {
        counter.start();
        Reader nCharacterStream = delegate.getNCharacterStream(i);
        counter.stop();
        return nCharacterStream;
    }

    @Override
    public Reader getNCharacterStream(String s) throws SQLException {
        counter.start();
        Reader nCharacterStream = delegate.getNCharacterStream(s);
        counter.stop();
        return nCharacterStream;
    }

    @Override
    public void updateNCharacterStream(int i, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNCharacterStream(i, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateNCharacterStream(String s, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNCharacterStream(s, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateAsciiStream(int i, InputStream inputStream, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateAsciiStream(i, inputStream, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBinaryStream(int i, InputStream inputStream, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBinaryStream(i, inputStream, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateCharacterStream(int i, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateCharacterStream(i, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateAsciiStream(String s, InputStream inputStream, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateAsciiStream(s, inputStream, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBinaryStream(String s, InputStream inputStream, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBinaryStream(s, inputStream, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateCharacterStream(String s, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateCharacterStream(s, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBlob(int i, InputStream inputStream, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBlob(i, inputStream, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBlob(String s, InputStream inputStream, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBlob(s, inputStream, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateClob(int i, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateClob(i, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateClob(String s, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateClob(s, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateNClob(int i, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNClob(i, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateNClob(String s, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNClob(s, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateNCharacterStream(int i, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNCharacterStream(i, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateNCharacterStream(String s, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNCharacterStream(s, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateAsciiStream(int i, InputStream inputStream) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateAsciiStream(i, inputStream);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBinaryStream(int i, InputStream inputStream) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBinaryStream(i, inputStream);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateCharacterStream(int i, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateCharacterStream(i, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateAsciiStream(String s, InputStream inputStream) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateAsciiStream(s, inputStream);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBinaryStream(String s, InputStream inputStream) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBinaryStream(s, inputStream);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateCharacterStream(String s, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateCharacterStream(s, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBlob(int i, InputStream inputStream) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBlob(i, inputStream);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateBlob(String s, InputStream inputStream) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateBlob(s, inputStream);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateClob(int i, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateClob(i, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateClob(String s, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateClob(s, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateNClob(int i, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNClob(i, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void updateNClob(String s, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        delegate.updateNClob(s, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public <T> T unwrap(Class<T> tClass) throws SQLException {
        return delegate.unwrap(tClass);
    }

    @Override
    public boolean isWrapperFor(Class<?> aClass) throws SQLException {
        return delegate.isWrapperFor(aClass);
    }

    @Override
    public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
        return delegate.getObject(columnIndex, type);
    }

    @Override
    public <T> T getObject(String columnLabel, Class<T> type) throws SQLException {
        return delegate.getObject(columnLabel, type);
    }
}