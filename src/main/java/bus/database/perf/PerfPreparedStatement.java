/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.perf;

import bus.plumbing.utils.PerfStat;
import bus.plumbing.utils.Watch;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.Map;

/**
 * @author Marcelo Morales
 * @since 12/23/10 12:34 PM
 */
public class PerfPreparedStatement extends PerfStatement implements PreparedStatement {
    private final PreparedStatement statement;

    public PerfPreparedStatement(PreparedStatement statement, Watch counter, Map<String, PerfStat> lruMap) {
        super(statement, counter, lruMap);
        this.statement = statement;
    }

    public PerfPreparedStatement(PreparedStatement statement, Watch counter, Map<String, PerfStat> lruMap, long timeSoFar, String consulta) {
        super(statement, counter, lruMap, timeSoFar, consulta);
        this.statement = statement;
    }

    @Override
    public ResultSet executeQuery() throws SQLException {
        counter.start();
        specificSentence.start();
        ResultSet resDelegated = statement.executeQuery();
        specificSentence.stop();
        ResultSet resultSet = new PerfResultSet(resDelegated, counter, specificSentence);
        counter.stop();
        return resultSet;
    }

    @Override
    public int executeUpdate() throws SQLException {
        counter.start();
        specificSentence.start();
        int i = statement.executeUpdate();
        specificSentence.stop();
        counter.stop();
        return i;
    }

    @Override
    public void setNull(int i, int i1) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setNull(i, i1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBoolean(int i, boolean b) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setBoolean(i, b);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setByte(int i, byte b) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setByte(i, b);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setShort(int i, short j) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setShort(i, j);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setInt(int i, int i1) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setInt(i, i1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setLong(int i, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setLong(i, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setFloat(int i, float v) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setFloat(i, v);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setDouble(int i, double v) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setDouble(i, v);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBigDecimal(int i, BigDecimal bigDecimal) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setBigDecimal(i, bigDecimal);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setString(int i, String s) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setString(i, s);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBytes(int i, byte[] bytes) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setBytes(i, bytes);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setDate(int i, Date date) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setDate(i, date);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setTime(int i, Time time) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setTime(i, time);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setTimestamp(int i, Timestamp timestamp) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setTimestamp(i, timestamp);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setAsciiStream(int i, InputStream inputStream, int i1) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setAsciiStream(i, inputStream, i1);
        specificSentence.stop();
        counter.stop();
    }

    @SuppressWarnings({"deprecation"})
    @Override
    @Deprecated
    public void setUnicodeStream(int i, InputStream inputStream, int i1) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setUnicodeStream(i, inputStream, i1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBinaryStream(int i, InputStream inputStream, int i1) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setBinaryStream(i, inputStream, i1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void clearParameters() throws SQLException {
        counter.start();
        specificSentence.start();
        statement.clearParameters();
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setObject(int i, Object o, int i1) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setObject(i, o, i1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setObject(int i, Object o) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setObject(i, o);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public boolean execute() throws SQLException {
        counter.start();
        specificSentence.start();
        boolean execute = statement.execute();
        specificSentence.stop();
        counter.stop();
        return execute;
    }

    @Override
    public void addBatch() throws SQLException {
        counter.start();
        specificSentence.start();
        statement.addBatch();
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setCharacterStream(int i, Reader reader, int i1) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setCharacterStream(i, reader, i1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setRef(int i, Ref ref) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setRef(i, ref);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBlob(int i, Blob blob) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setBlob(i, blob);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setClob(int i, Clob clob) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setClob(i, clob);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setArray(int i, Array array) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setArray(i, array);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public ResultSetMetaData getMetaData() throws SQLException {
        counter.start();
        ResultSetMetaData metaData = statement.getMetaData();
        counter.stop();
        return metaData;
    }

    @Override
    public void setDate(int i, Date date, Calendar calendar) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setDate(i, date, calendar);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setTime(int i, Time time, Calendar calendar) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setTime(i, time, calendar);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setTimestamp(int i, Timestamp timestamp, Calendar calendar) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setTimestamp(i, timestamp, calendar);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNull(int i, int i1, String s) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setNull(i, i1, s);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setURL(int i, URL url) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setURL(i, url);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public ParameterMetaData getParameterMetaData() throws SQLException {
        counter.start();
        ParameterMetaData parameterMetaData = statement.getParameterMetaData();
        counter.stop();
        return parameterMetaData;
    }

    @Override
    public void setRowId(int i, RowId rowId) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setRowId(i, rowId);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNString(int i, String s) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setNString(i, s);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNCharacterStream(int i, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setNCharacterStream(i, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNClob(int i, NClob nClob) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setNClob(i, nClob);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setClob(int i, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setClob(i, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBlob(int i, InputStream inputStream, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setBlob(i, inputStream, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNClob(int i, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setNClob(i, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setSQLXML(int i, SQLXML sqlxml) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setSQLXML(i, sqlxml);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setObject(int i, Object o, int i1, int i2) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setObject(i, o, i1, i2);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setAsciiStream(int i, InputStream inputStream, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setAsciiStream(i, inputStream, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBinaryStream(int i, InputStream inputStream, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setBinaryStream(i, inputStream, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setCharacterStream(int i, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setCharacterStream(i, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setAsciiStream(int i, InputStream inputStream) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setAsciiStream(i, inputStream);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBinaryStream(int i, InputStream inputStream) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setBinaryStream(i, inputStream);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setCharacterStream(int i, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setCharacterStream(i, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNCharacterStream(int i, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setNCharacterStream(i, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setClob(int i, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setClob(i, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBlob(int i, InputStream inputStream) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setBlob(i, inputStream);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNClob(int i, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        statement.setNClob(i, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void closeOnCompletion() throws SQLException {
        statement.closeOnCompletion();
    }

    @Override
    public boolean isCloseOnCompletion() throws SQLException {
        return statement.isCloseOnCompletion();
    }
}
