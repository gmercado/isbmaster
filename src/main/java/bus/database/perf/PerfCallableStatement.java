/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.perf;

import bus.plumbing.utils.PerfStat;
import bus.plumbing.utils.Watch;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.Map;

/**
 * @author Marcelo Morales
 * @since 12/23/10 12:41 PM
 */
public class PerfCallableStatement extends PerfPreparedStatement implements CallableStatement {
    private final CallableStatement callableStatement;

    public PerfCallableStatement(CallableStatement callableStatement, Watch counter, Map<String, PerfStat> lruMap) {
        super(callableStatement, counter, lruMap);
        this.callableStatement = callableStatement;
    }

    public PerfCallableStatement(CallableStatement callableStatement, Watch counter, Map<String, PerfStat> lruMap,
                                 long timeSoFar, String consulta) {
        super(callableStatement, counter, lruMap, timeSoFar, consulta);
        this.callableStatement = callableStatement;
    }

    @Override
    public void registerOutParameter(int i, int i1) throws SQLException {
        counter.start();
        callableStatement.registerOutParameter(i, i1);
        counter.stop();
    }

    @Override
    public void registerOutParameter(int i, int i1, int i2) throws SQLException {
        counter.start();
        callableStatement.registerOutParameter(i, i1, i2);
        counter.stop();
    }

    @Override
    public boolean wasNull() throws SQLException {
        counter.start();
        boolean b = callableStatement.wasNull();
        counter.stop();
        return b;
    }

    @Override
    public String getString(int i) throws SQLException {
        counter.start();
        String string = callableStatement.getString(i);
        counter.stop();
        return string;
    }

    @Override
    public boolean getBoolean(int i) throws SQLException {
        counter.start();
        boolean aBoolean = callableStatement.getBoolean(i);
        counter.stop();
        return aBoolean;
    }

    @Override
    public byte getByte(int i) throws SQLException {
        counter.start();
        byte aByte = callableStatement.getByte(i);
        counter.stop();
        return aByte;
    }

    @Override
    public short getShort(int i) throws SQLException {
        counter.start();
        short aShort = callableStatement.getShort(i);
        counter.stop();
        return aShort;
    }

    @Override
    public int getInt(int i) throws SQLException {
        counter.start();
        int anInt = callableStatement.getInt(i);
        counter.stop();
        return anInt;
    }

    @Override
    public long getLong(int i) throws SQLException {
        counter.start();
        long aLong = callableStatement.getLong(i);
        counter.stop();
        return aLong;
    }

    @Override
    public float getFloat(int i) throws SQLException {
        counter.start();
        float aFloat = callableStatement.getFloat(i);
        counter.stop();
        return aFloat;
    }

    @Override
    public double getDouble(int i) throws SQLException {
        counter.start();
        double aDouble = callableStatement.getDouble(i);
        counter.stop();
        return aDouble;
    }

    @SuppressWarnings({"deprecation"})
    @Override
    @Deprecated
    public BigDecimal getBigDecimal(int i, int i1) throws SQLException {
        counter.start();
        BigDecimal bigDecimal = callableStatement.getBigDecimal(i, i1);
        counter.stop();
        return bigDecimal;
    }

    @Override
    public byte[] getBytes(int i) throws SQLException {
        counter.start();
        byte[] bytes = callableStatement.getBytes(i);
        counter.stop();
        return bytes;
    }

    @Override
    public Date getDate(int i) throws SQLException {
        counter.start();
        Date date = callableStatement.getDate(i);
        counter.stop();
        return date;
    }

    @Override
    public Time getTime(int i) throws SQLException {
        counter.start();
        Time time = callableStatement.getTime(i);
        counter.stop();
        return time;
    }

    @Override
    public Timestamp getTimestamp(int i) throws SQLException {
        counter.start();
        Timestamp timestamp = callableStatement.getTimestamp(i);
        counter.stop();
        return timestamp;
    }

    @Override
    public Object getObject(int i) throws SQLException {
        counter.start();
        Object object = callableStatement.getObject(i);
        counter.stop();
        return object;
    }

    @Override
    public BigDecimal getBigDecimal(int i) throws SQLException {
        counter.start();
        BigDecimal bigDecimal = callableStatement.getBigDecimal(i);
        counter.stop();
        return bigDecimal;
    }

    @Override
    public Object getObject(int i, Map<String, Class<?>> stringClassMap) throws SQLException {
        counter.start();
        Object object = callableStatement.getObject(i, stringClassMap);
        counter.stop();
        return object;
    }

    @Override
    public Ref getRef(int i) throws SQLException {
        counter.start();
        Ref ref = callableStatement.getRef(i);
        counter.stop();
        return ref;
    }

    @Override
    public Blob getBlob(int i) throws SQLException {
        counter.start();
        Blob blob = new PerfBlob(callableStatement.getBlob(i), counter);
        counter.stop();
        return blob;
    }

    @Override
    public Clob getClob(int i) throws SQLException {
        counter.start();
        Clob clob = callableStatement.getClob(i);
        counter.stop();
        return clob;
    }

    @Override
    public Array getArray(int i) throws SQLException {
        counter.start();
        Array array = callableStatement.getArray(i);
        counter.stop();
        return array;
    }

    @Override
    public Date getDate(int i, Calendar calendar) throws SQLException {
        counter.start();
        Date date = callableStatement.getDate(i, calendar);
        counter.stop();
        return date;
    }

    @Override
    public Time getTime(int i, Calendar calendar) throws SQLException {
        counter.start();
        Time time = callableStatement.getTime(i, calendar);
        counter.stop();
        return time;
    }

    @Override
    public Timestamp getTimestamp(int i, Calendar calendar) throws SQLException {
        counter.start();
        Timestamp timestamp = callableStatement.getTimestamp(i, calendar);
        counter.stop();
        return timestamp;
    }

    @Override
    public void registerOutParameter(int i, int i1, String s) throws SQLException {
        counter.start();
        callableStatement.registerOutParameter(i, i1, s);
        counter.stop();
    }

    @Override
    public void registerOutParameter(String s, int i) throws SQLException {
        counter.start();
        callableStatement.registerOutParameter(s, i);
        counter.stop();
    }

    @Override
    public void registerOutParameter(String s, int i, int i1) throws SQLException {
        counter.start();
        callableStatement.registerOutParameter(s, i, i1);
        counter.stop();
    }

    @Override
    public void registerOutParameter(String s, int i, String s1) throws SQLException {
        counter.start();
        callableStatement.registerOutParameter(s, i, s1);
        counter.stop();
    }

    @Override
    public URL getURL(int i) throws SQLException {
        counter.start();
        URL url = callableStatement.getURL(i);
        counter.stop();
        return url;
    }

    @Override
    public void setURL(String s, URL url) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setURL(s, url);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNull(String s, int i) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setNull(s, i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBoolean(String s, boolean b) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setBoolean(s, b);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setByte(String s, byte b) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setByte(s, b);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setShort(String s, short i) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setShort(s, i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setInt(String s, int i) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setInt(s, i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setLong(String s, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setLong(s, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setFloat(String s, float v) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setFloat(s, v);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setDouble(String s, double v) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setDouble(s, v);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBigDecimal(String s, BigDecimal bigDecimal) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setBigDecimal(s, bigDecimal);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setString(String s, String s1) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setString(s, s1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBytes(String s, byte[] bytes) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setBytes(s, bytes);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setDate(String s, Date date) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setDate(s, date);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setTime(String s, Time time) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setTime(s, time);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setTimestamp(String s, Timestamp timestamp) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setTimestamp(s, timestamp);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setAsciiStream(String s, InputStream inputStream, int i) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setAsciiStream(s, inputStream, i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBinaryStream(String s, InputStream inputStream, int i) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setBinaryStream(s, inputStream, i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setObject(String s, Object o, int i, int i1) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setObject(s, o, i, i1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setObject(String s, Object o, int i) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setObject(s, o, i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setObject(String s, Object o) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setObject(s, o);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setCharacterStream(String s, Reader reader, int i) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setCharacterStream(s, reader, i);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setDate(String s, Date date, Calendar calendar) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setDate(s, date, calendar);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setTime(String s, Time time, Calendar calendar) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setTime(s, time, calendar);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setTimestamp(String s, Timestamp timestamp, Calendar calendar) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setTimestamp(s, timestamp, calendar);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNull(String s, int i, String s1) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setNull(s, i, s1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public String getString(String s) throws SQLException {
        counter.start();
        String string = callableStatement.getString(s);
        counter.stop();
        return string;
    }

    @Override
    public boolean getBoolean(String s) throws SQLException {
        counter.start();
        boolean aBoolean = callableStatement.getBoolean(s);
        counter.stop();
        return aBoolean;
    }

    @Override
    public byte getByte(String s) throws SQLException {
        counter.start();
        byte aByte = callableStatement.getByte(s);
        counter.stop();
        return aByte;
    }

    @Override
    public short getShort(String s) throws SQLException {
        counter.start();
        short aShort = callableStatement.getShort(s);
        counter.stop();
        return aShort;
    }

    @Override
    public int getInt(String s) throws SQLException {
        counter.start();
        int anInt = callableStatement.getInt(s);
        counter.stop();
        return anInt;
    }

    @Override
    public long getLong(String s) throws SQLException {
        counter.start();
        long aLong = callableStatement.getLong(s);
        counter.stop();
        return aLong;
    }

    @Override
    public float getFloat(String s) throws SQLException {
        counter.start();
        float aFloat = callableStatement.getFloat(s);
        counter.stop();
        return aFloat;
    }

    @Override
    public double getDouble(String s) throws SQLException {
        counter.start();
        double aDouble = callableStatement.getDouble(s);
        counter.stop();
        return aDouble;
    }

    @Override
    public byte[] getBytes(String s) throws SQLException {
        counter.start();
        byte[] bytes = callableStatement.getBytes(s);
        counter.stop();
        return bytes;
    }

    @Override
    public Date getDate(String s) throws SQLException {
        counter.start();
        Date date = callableStatement.getDate(s);
        counter.stop();
        return date;
    }

    @Override
    public Time getTime(String s) throws SQLException {
        counter.start();
        Time time = callableStatement.getTime(s);
        counter.stop();
        return time;
    }

    @Override
    public Timestamp getTimestamp(String s) throws SQLException {
        counter.start();
        Timestamp timestamp = callableStatement.getTimestamp(s);
        counter.stop();
        return timestamp;
    }

    @Override
    public Object getObject(String s) throws SQLException {
        counter.start();
        Object object = callableStatement.getObject(s);
        counter.stop();
        return object;
    }

    @Override
    public BigDecimal getBigDecimal(String s) throws SQLException {
        counter.start();
        BigDecimal bigDecimal = callableStatement.getBigDecimal(s);
        counter.stop();
        return bigDecimal;
    }

    @Override
    public Object getObject(String s, Map<String, Class<?>> stringClassMap) throws SQLException {
        counter.start();
        Object object = callableStatement.getObject(s, stringClassMap);
        counter.stop();
        return object;
    }

    @Override
    public Ref getRef(String s) throws SQLException {
        counter.start();
        Ref ref = callableStatement.getRef(s);
        counter.stop();
        return ref;
    }

    @Override
    public Blob getBlob(String s) throws SQLException {
        counter.start();
        Blob blob = new PerfBlob(callableStatement.getBlob(s), counter);
        counter.stop();
        return blob;
    }

    @Override
    public Clob getClob(String s) throws SQLException {
        counter.start();
        Clob clob = callableStatement.getClob(s);
        counter.stop();
        return clob;
    }

    @Override
    public Array getArray(String s) throws SQLException {
        counter.start();
        Array array = callableStatement.getArray(s);
        counter.stop();
        return array;
    }

    @Override
    public Date getDate(String s, Calendar calendar) throws SQLException {
        counter.start();
        Date date = callableStatement.getDate(s, calendar);
        counter.stop();
        return date;
    }

    @Override
    public Time getTime(String s, Calendar calendar) throws SQLException {
        counter.start();
        Time time = callableStatement.getTime(s, calendar);
        counter.stop();
        return time;
    }

    @Override
    public Timestamp getTimestamp(String s, Calendar calendar) throws SQLException {
        counter.start();
        Timestamp timestamp = callableStatement.getTimestamp(s, calendar);
        counter.stop();
        return timestamp;
    }

    @Override
    public URL getURL(String s) throws SQLException {
        counter.start();
        URL url = callableStatement.getURL(s);
        counter.stop();
        return url;
    }

    @Override
    public RowId getRowId(int i) throws SQLException {
        counter.start();
        RowId rowId = callableStatement.getRowId(i);
        counter.stop();
        return rowId;
    }

    @Override
    public RowId getRowId(String s) throws SQLException {
        counter.start();
        RowId rowId = callableStatement.getRowId(s);
        counter.stop();
        return rowId;
    }

    @Override
    public void setRowId(String s, RowId rowId) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setRowId(s, rowId);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNString(String s, String s1) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setNString(s, s1);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNCharacterStream(String s, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setNCharacterStream(s, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNClob(String s, NClob nClob) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setNClob(s, nClob);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setClob(String s, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setClob(s, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBlob(String s, InputStream inputStream, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setBlob(s, inputStream, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNClob(String s, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setNClob(s, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public NClob getNClob(int i) throws SQLException {
        counter.start();
        NClob nClob = callableStatement.getNClob(i);
        counter.stop();
        return nClob;
    }

    @Override
    public NClob getNClob(String s) throws SQLException {
        counter.start();
        NClob nClob = callableStatement.getNClob(s);
        counter.stop();
        return nClob;
    }

    @Override
    public void setSQLXML(String s, SQLXML sqlxml) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setSQLXML(s, sqlxml);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public SQLXML getSQLXML(int i) throws SQLException {
        counter.start();
        SQLXML sqlxml = callableStatement.getSQLXML(i);
        counter.stop();
        return sqlxml;
    }

    @Override
    public SQLXML getSQLXML(String s) throws SQLException {
        counter.start();
        SQLXML sqlxml = callableStatement.getSQLXML(s);
        counter.stop();
        return sqlxml;
    }

    @Override
    public String getNString(int i) throws SQLException {
        counter.start();
        String nString = callableStatement.getNString(i);
        counter.stop();
        return nString;
    }

    @Override
    public String getNString(String s) throws SQLException {
        counter.start();
        String nString = callableStatement.getNString(s);
        counter.stop();
        return nString;
    }

    @Override
    public Reader getNCharacterStream(int i) throws SQLException {
        counter.start();
        Reader nCharacterStream = callableStatement.getNCharacterStream(i);
        counter.stop();
        return nCharacterStream;
    }

    @Override
    public Reader getNCharacterStream(String s) throws SQLException {
        counter.start();
        Reader nCharacterStream = callableStatement.getNCharacterStream(s);
        counter.stop();
        return nCharacterStream;
    }

    @Override
    public Reader getCharacterStream(int i) throws SQLException {
        counter.start();
        Reader characterStream = callableStatement.getCharacterStream(i);
        counter.stop();
        return characterStream;
    }

    @Override
    public Reader getCharacterStream(String s) throws SQLException {
        counter.start();
        Reader characterStream = callableStatement.getCharacterStream(s);
        counter.stop();
        return characterStream;
    }

    @Override
    public void setBlob(String s, Blob blob) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setBlob(s, blob);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setClob(String s, Clob clob) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setClob(s, clob);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setAsciiStream(String s, InputStream inputStream, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setAsciiStream(s, inputStream, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBinaryStream(String s, InputStream inputStream, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setBinaryStream(s, inputStream, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setCharacterStream(String s, Reader reader, long l) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setCharacterStream(s, reader, l);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setAsciiStream(String s, InputStream inputStream) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setAsciiStream(s, inputStream);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBinaryStream(String s, InputStream inputStream) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setBinaryStream(s, inputStream);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setCharacterStream(String s, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setCharacterStream(s, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNCharacterStream(String s, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setNCharacterStream(s, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setClob(String s, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setClob(s, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setBlob(String s, InputStream inputStream) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setBlob(s, inputStream);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public void setNClob(String s, Reader reader) throws SQLException {
        counter.start();
        specificSentence.start();
        callableStatement.setNClob(s, reader);
        specificSentence.stop();
        counter.stop();
    }

    @Override
    public <T> T getObject(int parameterIndex, Class<T> type) throws SQLException {
        return callableStatement.getObject(parameterIndex, type);
    }

    @Override
    public <T> T getObject(String parameterName, Class<T> type) throws SQLException {
        return callableStatement.getObject(parameterName, type);
    }
}