/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.database.perf;

import bus.plumbing.utils.Watch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;

/**
 * @author Marcelo Morales
 */
public class PerfBlob implements Blob {
    private static final Logger LOGGER = LoggerFactory.getLogger(PerfBlob.class);

    private final Blob blob;
    private final Watch counter;

    public PerfBlob(Blob blob, Watch counter) {
        this.blob = blob;
        this.counter = counter;
    }

    @Override
    public long length() throws SQLException {
        counter.start();
        long length = (blob == null) ? 0 : blob.length();
        counter.stop();
        return length;
    }

    @Override
    public byte[] getBytes(long pos, int length) throws SQLException {
        counter.start();
        byte[] bytes = blob.getBytes(pos, length);
        counter.stop();
        return bytes;
    }

    @Override
    public InputStream getBinaryStream() throws SQLException {
        LOGGER.warn("getBinaryStream puede fallar en estadisticas en PerfBlob");
        counter.start();
        InputStream binaryStream = blob.getBinaryStream();
        counter.stop();
        return binaryStream;
    }

    @Override
    public long position(byte[] pattern, long start) throws SQLException {
        counter.start();
        long position = blob.position(pattern, start);
        counter.stop();
        return position;
    }

    @Override
    public long position(Blob pattern, long start) throws SQLException {
        counter.start();
        long position = blob.position(pattern, start);
        counter.stop();
        return position;
    }

    @Override
    public int setBytes(long pos, byte[] bytes) throws SQLException {
        counter.start();
        int i = blob.setBytes(pos, bytes);
        counter.stop();
        return i;
    }

    @Override
    public int setBytes(long pos, byte[] bytes, int offset, int len) throws SQLException {
        counter.start();
        int i = blob.setBytes(pos, bytes, offset, len);
        counter.stop();
        return i;
    }

    @Override
    public OutputStream setBinaryStream(long pos) throws SQLException {
        counter.start();
        OutputStream outputStream = blob.setBinaryStream(pos);
        counter.stop();
        return outputStream;
    }

    @Override
    public void truncate(long len) throws SQLException {
        counter.start();
        blob.truncate(len);
        counter.stop();
    }

    @Override
    public void free() throws SQLException {
        counter.start();
        blob.free();
        counter.stop();
    }

    @Override
    public InputStream getBinaryStream(long pos, long length) throws SQLException {
        LOGGER.warn("getBinaryStream puede fallar en estadisticas en PerfBlob");
        counter.start();
        InputStream binaryStream = blob.getBinaryStream(pos, length);
        counter.stop();
        return binaryStream;
    }
}
