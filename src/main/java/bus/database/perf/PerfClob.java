package bus.database.perf;

import bus.plumbing.utils.Watch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.sql.Clob;
import java.sql.SQLException;

/**
 * La clase <code>PerfClob.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 24/04/2013, 12:28:16
 */
public class PerfClob implements Clob {
    private static final Logger LOGGER = LoggerFactory.getLogger(PerfClob.class);

    private final Clob clob;
    private final Watch counter;

    public PerfClob(Clob clob, Watch counter) {
        this.clob = clob;
        this.counter = counter;
    }


    @Override
    public long length() throws SQLException {
        counter.start();
        long length = (clob == null) ? 0 : clob.length();
        counter.stop();
        return length;
    }

    @Override
    public String getSubString(long pos, int length) throws SQLException {
        counter.start();
        String subString = clob.getSubString(pos, length);
        counter.stop();
        return subString;
    }

    @Override
    public Reader getCharacterStream() throws SQLException {
        LOGGER.warn("getCharacterStream puede fallar en estadisticas en PerfBlob");
        counter.start();
        Reader reader = clob.getCharacterStream();
        counter.stop();
        return reader;
    }

    @Override
    public InputStream getAsciiStream() throws SQLException {
        counter.start();
        InputStream inputStream = clob.getAsciiStream();
        counter.stop();
        return inputStream;
    }

    @Override
    public long position(String searchstr, long start) throws SQLException {
        counter.start();
        long position = clob.position(searchstr, start);
        counter.stop();
        return position;
    }

    @Override
    public long position(Clob searchstr, long start) throws SQLException {
        counter.start();
        long position = clob.position(searchstr, start);
        counter.stop();
        return position;
    }

    @Override
    public int setString(long pos, String str) throws SQLException {
        counter.start();
        int i = clob.setString(pos, str);
        counter.stop();
        return i;
    }

    @Override
    public int setString(long pos, String str, int offset, int len)
            throws SQLException {
        counter.start();
        int i = clob.setString(pos, str, offset, len);
        counter.stop();
        return i;
    }

    @Override
    public OutputStream setAsciiStream(long pos) throws SQLException {
        counter.start();
        OutputStream outputStream = clob.setAsciiStream(pos);
        counter.stop();
        return outputStream;
    }

    @Override
    public Writer setCharacterStream(long pos) throws SQLException {
        counter.start();
        Writer writer = clob.setCharacterStream(pos);
        counter.stop();
        return writer;
    }

    @Override
    public void truncate(long len) throws SQLException {
        counter.start();
        clob.truncate(len);
        counter.stop();
    }

    @Override
    public void free() throws SQLException {
        counter.start();
        clob.free();
        counter.stop();
    }

    @Override
    public Reader getCharacterStream(long pos, long length) throws SQLException {
        LOGGER.warn("getCharacterStream puede fallar en estadisticas en PerfBlob");
        counter.start();
        Reader reader = clob.getCharacterStream(pos, length);
        counter.stop();
        return reader;
    }

}
