/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.perf;

import bus.plumbing.utils.PerfStat;
import bus.plumbing.utils.Watch;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * @author Marcelo Morales
 * @since 12/23/10 11:39 AM
 */
public class PerfConnection implements Connection {

    private final Connection delegated;

    private final Watch counter;

    private final Map<String, PerfStat> lruMap;

    public PerfConnection(Connection delegated, Watch counter, Map<String, PerfStat> lruMap) {
        this.delegated = delegated;
        this.counter = counter;
        this.lruMap = lruMap;
    }

    @Override
    public Statement createStatement() throws SQLException {
        counter.start();
        Statement statement = new PerfStatement(delegated.createStatement(), counter, lruMap);
        counter.stop();
        return statement;
    }

    @Override
    public PreparedStatement prepareStatement(String s) throws SQLException {
        counter.start();
        Long t = System.currentTimeMillis();
        PreparedStatement ps = delegated.prepareStatement(s);
        ps = new PerfPreparedStatement(ps, counter, lruMap, System.currentTimeMillis() - t, s);
        counter.stop();
        return ps;
    }

    @Override
    public CallableStatement prepareCall(String s) throws SQLException {
        counter.start();
        Long t = System.currentTimeMillis();
        CallableStatement st = delegated.prepareCall(s);
        st = new PerfCallableStatement(st, counter, lruMap, System.currentTimeMillis() - t, s);
        counter.stop();
        return st;
    }

    @Override
    public String nativeSQL(String s) throws SQLException {
        counter.start();
        String s1 = delegated.nativeSQL(s);
        counter.stop();
        return s1;
    }

    @Override
    public boolean getAutoCommit() throws SQLException {
        counter.start();
        boolean autoCommit = delegated.getAutoCommit();
        counter.stop();
        return autoCommit;
    }

    @Override
    public void setAutoCommit(boolean b) throws SQLException {
        counter.start();
        delegated.setAutoCommit(b);
        counter.stop();
    }

    @Override
    public void commit() throws SQLException {
        counter.start();
        delegated.commit();
        counter.stop();
    }

    @Override
    public void rollback() throws SQLException {
        counter.start();
        delegated.rollback();
        counter.stop();
    }

    @Override
    public void close() throws SQLException {
        counter.start();
        delegated.close();
        counter.stop();
    }

    @Override
    public boolean isClosed() throws SQLException {
        counter.start();
        boolean closed = delegated.isClosed();
        counter.stop();
        return closed;
    }

    @Override
    public DatabaseMetaData getMetaData() throws SQLException {
        counter.start();
        DatabaseMetaData metaData = delegated.getMetaData();
        counter.stop();
        return metaData;
    }

    @Override
    public boolean isReadOnly() throws SQLException {
        counter.start();
        boolean readOnly = delegated.isReadOnly();
        counter.stop();
        return readOnly;
    }

    @Override
    public void setReadOnly(boolean b) throws SQLException {
        counter.start();
        delegated.setReadOnly(b);
        counter.stop();
    }

    @Override
    public String getCatalog() throws SQLException {
        counter.start();
        String catalog = delegated.getCatalog();
        counter.stop();
        return catalog;
    }

    @Override
    public void setCatalog(String s) throws SQLException {
        counter.start();
        delegated.setCatalog(s);
        counter.stop();
    }

    @Override
    public int getTransactionIsolation() throws SQLException {
        counter.start();
        int transactionIsolation = delegated.getTransactionIsolation();
        counter.stop();
        return transactionIsolation;
    }

    @Override
    public void setTransactionIsolation(int i) throws SQLException {
        counter.start();
        delegated.setTransactionIsolation(i);
        counter.stop();
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        counter.start();
        SQLWarning warnings = delegated.getWarnings();
        counter.stop();
        return warnings;
    }

    @Override
    public void clearWarnings() throws SQLException {
        counter.start();
        delegated.clearWarnings();
        counter.stop();
    }

    @Override
    public Statement createStatement(int i, int i1) throws SQLException {
        counter.start();
        Statement statement = new PerfStatement(delegated.createStatement(i, i1), counter, lruMap);
        counter.stop();
        return statement;
    }

    @Override
    public PreparedStatement prepareStatement(String s, int i, int i1) throws SQLException {
        counter.start();
        long t = System.currentTimeMillis();
        PreparedStatement ps = delegated.prepareStatement(s, i, i1);
        ps = new PerfPreparedStatement(ps, counter, lruMap, System.currentTimeMillis() - t, s);
        counter.stop();
        return ps;
    }

    @Override
    public CallableStatement prepareCall(String s, int i, int i1) throws SQLException {
        counter.start();
        long t = System.currentTimeMillis();
        CallableStatement cs = delegated.prepareCall(s, i, i1);
        cs = new PerfCallableStatement(cs, counter, lruMap, System.currentTimeMillis() - t, s);
        counter.stop();
        return cs;
    }

    @Override
    public Map<String, Class<?>> getTypeMap() throws SQLException {
        counter.start();
        Map<String, Class<?>> typeMap = delegated.getTypeMap();
        counter.stop();
        return typeMap;
    }

    @Override
    public void setTypeMap(Map<String, Class<?>> stringClassMap) throws SQLException {
        counter.start();
        delegated.setTypeMap(stringClassMap);
        counter.stop();
    }

    @Override
    public int getHoldability() throws SQLException {
        counter.start();
        int holdability = delegated.getHoldability();
        counter.stop();
        return holdability;
    }

    @Override
    public void setHoldability(int i) throws SQLException {
        counter.start();
        delegated.setHoldability(i);
        counter.stop();
    }

    @Override
    public Savepoint setSavepoint() throws SQLException {
        counter.start();
        Savepoint savepoint = delegated.setSavepoint();
        counter.stop();
        return savepoint;
    }

    @Override
    public Savepoint setSavepoint(String s) throws SQLException {
        counter.start();
        Savepoint savepoint = delegated.setSavepoint(s);
        counter.stop();
        return savepoint;
    }

    @Override
    public void rollback(Savepoint savepoint) throws SQLException {
        counter.start();
        delegated.rollback(savepoint);
        counter.stop();
    }

    @Override
    public void releaseSavepoint(Savepoint savepoint) throws SQLException {
        counter.start();
        delegated.releaseSavepoint(savepoint);
        counter.stop();
    }

    @Override
    public Statement createStatement(int i, int i1, int i2) throws SQLException {
        counter.start();
        Statement statement = new PerfStatement(delegated.createStatement(i, i1, i2), counter, lruMap);
        counter.stop();
        return statement;
    }

    @Override
    public PreparedStatement prepareStatement(String s, int i, int i1, int i2) throws SQLException {
        counter.start();
        long t = System.currentTimeMillis();
        PreparedStatement ps = delegated.prepareStatement(s, i, i1, i2);
        ps = new PerfPreparedStatement(ps, counter, lruMap, System.currentTimeMillis() - t, s);
        counter.stop();
        return ps;
    }

    @Override
    public CallableStatement prepareCall(String s, int i, int i1, int i2) throws SQLException {
        counter.start();
        long t = System.currentTimeMillis();
        CallableStatement cs = delegated.prepareCall(s, i, i1, i2);
        cs = new PerfCallableStatement(cs, counter, lruMap, System.currentTimeMillis() - t, s);
        counter.stop();
        return cs;
    }

    @Override
    public PreparedStatement prepareStatement(String s, int i) throws SQLException {
        counter.start();
        long t = System.currentTimeMillis();
        PreparedStatement ps = delegated.prepareStatement(s, i);
        ps = new PerfPreparedStatement(ps, counter, lruMap, System.currentTimeMillis() - t, s);
        counter.stop();
        return ps;
    }

    @Override
    public PreparedStatement prepareStatement(String s, int[] ints) throws SQLException {
        counter.start();
        long t = System.currentTimeMillis();
        PreparedStatement ps = delegated.prepareStatement(s, ints);
        ps = new PerfPreparedStatement(ps, counter, lruMap, System.currentTimeMillis() - t, s);
        counter.stop();
        return ps;
    }

    @Override
    public PreparedStatement prepareStatement(String s, String[] strings) throws SQLException {
        counter.start();
        long t = System.currentTimeMillis();
        PreparedStatement ps = delegated.prepareStatement(s, strings);
        ps = new PerfPreparedStatement(ps, counter, lruMap, System.currentTimeMillis() - t, s);
        counter.stop();
        return ps;
    }

    @Override
    public Clob createClob() throws SQLException {
        counter.start();
        Clob clob = delegated.createClob();
        counter.stop();
        return clob;
    }

    @Override
    public Blob createBlob() throws SQLException {
        counter.start();
        Blob blob = delegated.createBlob();
        counter.stop();
        return blob;
    }

    @Override
    public NClob createNClob() throws SQLException {
        counter.start();
        NClob nClob = delegated.createNClob();
        counter.stop();
        return nClob;
    }

    @Override
    public SQLXML createSQLXML() throws SQLException {
        counter.start();
        SQLXML sqlxml = delegated.createSQLXML();
        counter.stop();
        return sqlxml;
    }

    @Override
    public boolean isValid(int i) throws SQLException {
        counter.start();
        boolean valid = delegated.isValid(i);
        counter.stop();
        return valid;
    }

    @Override
    public void setClientInfo(String s, String s1) throws SQLClientInfoException {
        counter.start();
        delegated.setClientInfo(s, s1);
        counter.stop();
    }

    @Override
    public String getClientInfo(String s) throws SQLException {
        counter.start();
        String clientInfo = delegated.getClientInfo(s);
        counter.stop();
        return clientInfo;
    }

    @Override
    public Properties getClientInfo() throws SQLException {
        counter.start();
        Properties clientInfo = delegated.getClientInfo();
        counter.stop();
        return clientInfo;
    }

    @Override
    public void setClientInfo(Properties properties) throws SQLClientInfoException {
        counter.start();
        delegated.setClientInfo(properties);
        counter.stop();
    }

    @Override
    public Array createArrayOf(String s, Object[] objects) throws SQLException {
        counter.start();
        Array arrayOf = delegated.createArrayOf(s, objects);
        counter.stop();
        return arrayOf;
    }

    @Override
    public Struct createStruct(String s, Object[] objects) throws SQLException {
        counter.start();
        Struct struct = delegated.createStruct(s, objects);
        counter.stop();
        return struct;
    }

    @Override
    public <T> T unwrap(Class<T> tClass) throws SQLException {
        return delegated.unwrap(tClass);
    }

    @Override
    public boolean isWrapperFor(Class<?> aClass) throws SQLException {
        return delegated.isWrapperFor(aClass);
    }

    @Override
    public String getSchema() throws SQLException {
        return delegated.getSchema();
    }

    @Override
    public void setSchema(String schema) throws SQLException {
        delegated.setSchema(schema);
    }

    @Override
    public void abort(Executor executor) throws SQLException {
        delegated.abort(executor);
    }

    @Override
    public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
        delegated.setNetworkTimeout(executor, milliseconds);
    }

    @Override
    public int getNetworkTimeout() throws SQLException {
        return delegated.getNetworkTimeout();
    }
}