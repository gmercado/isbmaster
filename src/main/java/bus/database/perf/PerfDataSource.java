/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.perf;

import bus.plumbing.utils.PerfStat;
import bus.plumbing.utils.Watch;
import bus.plumbing.utils.Watches;
import org.apache.commons.dbcp.BasicDataSource;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Logger;


/**
 * OJO: en general, la implementacion proxeada puede devolver null a algunas llamadas, controlar el caso.
 *
 * @author Marcelo Morales
 * @since 12/14/10 10:19 AM
 */
public class PerfDataSource implements DataSource {

    private final BasicDataSource targetDataSource;
    private final ThreadLocal<Long> dbMilis;
    private final Map<String, PerfStat> lruMap;

    public BasicDataSource getTargetDataSource() {
        return targetDataSource;
    }

    @SuppressWarnings({"unchecked"})
    public PerfDataSource(BasicDataSource targetDataSource, ThreadLocal<Long> dbMilis, int maxSize) {
        this.targetDataSource = targetDataSource;
        this.dbMilis = dbMilis;
        lruMap = Collections.synchronizedMap(new org.apache.commons.collections.map.LRUMap(maxSize));
    }

    public Map<String, PerfStat> getPerfStats() {
        return Collections.unmodifiableMap(lruMap);
    }

    @Override
    public Connection getConnection() throws SQLException {
        long t = System.currentTimeMillis();
        Connection connection = targetDataSource.getConnection();
        Long sofar = Watches.DBPOOL_MILIS.get();
        if (sofar != null) {
            Watches.DBPOOL_MILIS.set(System.currentTimeMillis() - t + sofar);
        }
        return new PerfConnection(connection, new Watch(dbMilis), lruMap);
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        long t = System.currentTimeMillis();
        Connection connection = targetDataSource.getConnection(username, password);
        Long sofar = Watches.DBPOOL_MILIS.get();
        if (sofar != null) {
            Watches.DBPOOL_MILIS.set(System.currentTimeMillis() - t + sofar);
        }
        return new PerfConnection(connection, new Watch(dbMilis), lruMap);
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return targetDataSource.getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter printWriter) throws SQLException {
        targetDataSource.setLogWriter(printWriter);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return targetDataSource.getLoginTimeout();
    }

    @Override
    public void setLoginTimeout(int i) throws SQLException {
        targetDataSource.setLoginTimeout(i);
    }

    @Override
    public <T> T unwrap(Class<T> tClass) throws SQLException {
        return targetDataSource.unwrap(tClass);
    }

    @Override
    public boolean isWrapperFor(Class<?> aClass) throws SQLException {
        return targetDataSource.isWrapperFor(aClass);
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new SQLFeatureNotSupportedException("not yet support");
    }
}
