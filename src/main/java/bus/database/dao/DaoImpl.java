/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.database.dao;

import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.sql.Connection;
import java.util.*;

/**
 * @author Marcelo Morales
 * @since 5/22/11
 */
public abstract class DaoImpl<T extends Serializable, PK extends Serializable> extends DaoImplBase<T> implements Dao<T, PK> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Dao.class);

    protected DaoImpl(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    private static void firstCount(TypedQuery<?> query, long first, long count) {
        if (first > 0) {
            query.setFirstResult((int) first);
        }
        if (count > 0) {
            query.setMaxResults((int) count);
        }
    }

    @Override
    public T find(final PK id, final String... fg) {
        LOGGER.debug("Buscando {} con id {}", getClazz(), id);
        return doWithTransaction((entityManager, t) -> {
            if (fg != null) {
                for (String s : fg) {
                    if (!StringUtils.isEmpty(s) && !StringUtils.isWhitespace(s)) {
                        entityManager.getFetchPlan().addFetchGroup(s);
                    }

                }
            }
            return entityManager.find(getClazz(), id);
        });
    }

    @Override
    public T find(final PK id) {
        LOGGER.debug("Buscando {} con id {}", getClazz(), id);
        return doWithTransaction((entityManager, t) -> entityManager.find(getClazz(), id));
    }

    @Override
    public T merge(final T data) {
        OBJETO_PARA_LOGUEAR.set(data);
        LOGGER.debug("Merge {} con data {}", getClazz(), data);
        return doWithTransaction((entityManager, t) -> entityManager.merge(data));
    }

    @Override
    public T persist(final T data) {
        OBJETO_PARA_LOGUEAR.set(data);
        LOGGER.debug("Persist {} con data {}", getClazz(), data);
        return doWithTransaction((entityManager, t) -> {
            entityManager.persist(data);
            return data;
        });
    }

    @Override
    public T remove(final PK id) {
        LOGGER.debug("Remove {} con id {}", getClazz(), id);
        return doWithTransaction((entityManager, t) -> {
            T entity = entityManager.find(getClazz(), id);
            entityManager.remove(entity);
            return entity;
        });
    }

    /**
     * El nombre de la propiedad que tiene la llave primaria.
     *
     * @param from root del jpa criteria.
     * @return el nombre de la propiedad que tiene la llave privada.
     */
    protected Path<PK> countPath(Root<T> from) {
        return null;
    }

    /**
     * Retorna una lista de los atributos que van a ser tratados como listas de palabras (LIKEs en SQL en lugar de =).
     *
     * @param p el root
     * @return una lista de propiedades (puede ser null)
     */
    protected Iterable<Path<String>> getFullTexts(Root<T> p) {
        LOGGER.debug("No usamos fulltexts para clase {}", getClazz());
        return null;
    }

    @Override
    public int contarPorString(final String terminos) {
        if (terminos == null || "".equals(terminos.trim())) {
            return 0;
        }

        return doWithTransaction((entityManager, t) -> {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Long> q = cb.createQuery(Long.class);

            Root<T> p = q.from(getClazz());
            final Expression<Long> count = selectCount(cb, p);
            q.select(count);
            terminos(cb, q, p, terminos);

            try {
                return entityManager.createQuery(q).getSingleResult().intValue();
            } catch (NoResultException e) {
                return 0;
            }
        });
    }

    protected Expression<Long> selectCount(CriteriaBuilder cb, Root<T> p) {
        final Path<PK> countPath = countPath(p);
        if (countPath == null) {
            throw new IllegalStateException("Debes implementar countPath en " + getClazz() +
                    " o bien selectCount en " + getClass());
        }
        return cb.count(countPath);
    }

    @Override
    public List<T> buscarPorString(final String terminos, final OrderBy<T> orders, final long first, final long count, final String... fg) {
        if (terminos == null || "".equals(terminos.trim())) {
            return Collections.emptyList();
        }

        return doWithTransaction((entityManager, t) -> {
            if (fg != null) {
                for (String s : fg) {
                    if (!StringUtils.isEmpty(s) && !StringUtils.isWhitespace(s)) {
                        entityManager.getFetchPlan().addFetchGroup(s);
                    }
                }
            }
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<T> q = cb.createQuery(getClazz());
            Root<T> p = q.from(getClazz());
            terminos(cb, q, p, terminos);

            if (orders != null) {
                List<Order> orderList = orders.orders(cb, p);
                if (orderList != null && !orderList.isEmpty()) {
                    q.orderBy(orderList);
                }
            }

            TypedQuery<T> query = entityManager.createQuery(q);

            firstCount(query, first, count);
            return query.getResultList();
        });
    }

    @Override
    public List<T> buscarPorString(final String terminos) {
        if (terminos == null || "".equals(terminos.trim())) {
            return Collections.emptyList();
        }
        return doWithTransaction((entityManager, t) -> {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<T> q = cb.createQuery(getClazz());
            Root<T> p = q.from(getClazz());
            terminos(cb, q, p, terminos);
            TypedQuery<T> query = entityManager.createQuery(q);
            return query.getResultList();
        });
    }

    private void terminos(CriteriaBuilder cb, CriteriaQuery<?> q, Root<T> p, String terminos) {
        LinkedList<Predicate> predicatesOr = new LinkedList<>();
        Iterable<Path<String>> fullTexts = getFullTexts(p);
        if (fullTexts == null) {
            throw new IllegalStateException("Tienes que implementar el método getFullTexts de " +
                    this.getClass().getName() + " para realizar operaciones de busqueda de terminos.");
        }
        for (Path<String> x : fullTexts) {
            StringTokenizer tokenizer = new StringTokenizer(terminos, " \t\n\r\f,.;:/");
            LinkedList<Predicate> predicatesAnd = new LinkedList<>();
            while (tokenizer.hasMoreTokens()) {
                String token = "%" + tokenizer.nextToken() + "%";

                predicatesAnd.add(cb.like(cb.lower(x), StringUtils.lowerCase(token)));
            }
            predicatesOr.add(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
        }
        q.where(cb.or(predicatesOr.toArray(new Predicate[predicatesOr.size()])));
    }

    @Override
    public List<T> getTodos(final T example, final T example2,
                            final OrderBy<T> sortParam, final long first, final long count, final String... fg) {
        return doWithTransaction((entityManager, t) -> {

            if (fg != null) {
                for (String s : fg) {
                    if (!StringUtils.isEmpty(s) && !StringUtils.isWhitespace(s)) {
                        entityManager.getFetchPlan().addFetchGroup(s);
                    }
                }
            }

            OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<T> q = cb.createQuery(getClazz());
            Root<T> from = q.from(getClazz());
            byExample(cb, q, from, example, example2);
            if (sortParam != null) {
                List<Order> orderList = sortParam.orders(cb, from);
                if (orderList != null && !orderList.isEmpty()) {
                    q.orderBy(orderList);
                }
            }
            TypedQuery<T> query = entityManager.createQuery(q);
            firstCount(query, first, count);
            return query.getResultList();
        });
    }

    @Override
    public int contar(final T example, final T example2) {

        return doWithTransaction((entityManager, t) -> {
            OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Long> q = cb.createQuery(Long.class);
            Root<T> from = q.from(getClazz());
            final Expression<Long> count = selectCount(cb, from);
            q.select(count);
            byExample(cb, q, from, example, example2);
            TypedQuery<Long> query = entityManager.createQuery(q);
            try {
                return query.getSingleResult().intValue();
            } catch (NoResultException e) {
                return 0;
            }
        });
    }

    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    private void byExample(OpenJPACriteriaBuilder cb, CriteriaQuery<?> q, Root<T> from, T example, T example2) {
        if (example == null) {
            LOGGER.trace("el example debe tener valor");
        } else if (example2 == null) {
            q.where(cb.qbe(from, example));
        } else {
            q.where(createQBE(cb, from, example, example2));
        }
    }

    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<T> from, T example, T example2) {
        throw new IllegalStateException("Tienes que implementar createQBE en " + getClass() + " si usas betweens");
    }

    @Override
    public List<T> getTodos(OrderBy<T> sortParam, long first, long count) {
        return getTodos(null, sortParam, first, count);
    }

    @Override
    public List<T> getTodos(T example, OrderBy<T> sortParam, long first, long count) {
        return getTodos(example, null, sortParam, first, count);
    }

    @Override
    public int contar() {
        return contar(null);
    }

    @Override
    public int contar(T example) {
        return contar(example, null);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<T> listByNamedQuery(String namedQuery, Map<String, Object> parameters) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createNamedQuery(namedQuery);
        for (String parameter : parameters.keySet()) {
            query.setParameter(parameter, parameters.get(parameter));
        }
        query.setHint("hibernate.cache.use_query_cache", true);
        return (List<T>) query.getResultList();
    }

    @Override
    public Connection getConnection() {
        try {
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            return entityManager.unwrap(Connection.class);
        } catch (Exception e) {
            LOGGER.error("Error al obtener la conexion, ", e);
        }
        return null;
    }
}
