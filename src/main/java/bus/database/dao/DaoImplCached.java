/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.dao;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

/**
 * @author Marcelo Morales
 *         Created: 1/4/12 3:48 PM
 */
public abstract class DaoImplCached<T extends Serializable, PK extends Serializable> extends DaoImpl<T, PK> {

    protected final Ehcache ehcache;

    protected DaoImplCached(EntityManagerFactory entityManagerFactory, Ehcache ehcache) {
        super(entityManagerFactory);
        this.ehcache = ehcache;
    }

    @SuppressWarnings({"unchecked"})
    public final T findCached(PK id, Integer secondsToLive) {
        final String name = getClazz().getName();
        final String key = name + "-" + id;
        Element element = ehcache.get(key);
        if (element != null && !element.isExpired() && element.getObjectValue() != null) {
            //noinspection unchecked
            return (T) element.getObjectValue();
        }

        final T t = find(id);
        if (t != null) {
            Element elementToDatabase = new Element(key, t);
            if (secondsToLive != null && secondsToLive > 1) {
                elementToDatabase.setTimeToLive(secondsToLive);
            }
            ehcache.put(elementToDatabase);
        }

        return t;
    }
}
