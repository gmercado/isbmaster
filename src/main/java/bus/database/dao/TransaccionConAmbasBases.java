/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.dao;

import bus.database.model.BaseAuxiliar;
import bus.database.model.BasePrincipal;
import com.google.common.base.Function;
import com.google.inject.Inject;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;

/**
 * @author Marcelo Morales
 *         Created: 4/2/12 4:58 PM
 */
public final class TransaccionConAmbasBases {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransaccionConAmbasBases.class);

    private final DaoImplBase<Void> void400;

    private final DaoImplBase<Void> voidSql;

    @Inject
    public TransaccionConAmbasBases(@BasePrincipal EntityManagerFactory em400, @BaseAuxiliar EntityManagerFactory emsql) {
        void400 = new DaoImplBase<Void>(em400);
        voidSql = new DaoImplBase<Void>(emsql);
    }

    public <T> T doAmbos(final WithAmbasBases<T> withAmbasBases, Function<Exception, T> function) {
        try {
            return void400.doWithTransaction(new DaoImplBase.WithTransaction<T>() {

                @Override
                public T call(final OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                    return voidSql.doWithTransaction(new DaoImplBase.WithTransaction<T>() {

                        @Override
                        public T call(OpenJPAEntityManager emsql, OpenJPAEntityTransaction t) {
                            return withAmbasBases.hacer(entityManager, emsql);
                        }
                    });
                }
            });
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error con ambas bases", e);
            return function.apply(e);
        }
    }

    public interface WithAmbasBases<T> {
        T hacer(OpenJPAEntityManager entityManager400, OpenJPAEntityManager entityManagerSql);
    }
}
