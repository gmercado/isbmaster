/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.dao;

import bus.database.model.*;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

import javax.sql.DataSource;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import org.apache.commons.configuration.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import static bus.config.api.Configurations.CACHE_AMBIENTE_ACTUAL;
import static bus.config.api.Configurations.CACHE_AMBIENTE_ACTUAL_DEFAULT;
import static bus.config.api.Configurations.CONVERTIR_CCSID;
import static bus.config.api.Configurations.CONVERTIR_CCSID_DEFAULT;


/**
 * @author Marcelo Morales
 * @since 6/28/11
 */
public class AmbienteActualDao implements AmbienteActual, Serializable {

    private static final long serialVersionUID = 674905932132613523L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AmbienteActual.class);

    private static final String LLAVE_AMBIENTE_TEMPORAL = "___llave_ambiente_temporal____";

    private static final String LLAVE_OFFLINE = "___llave_offline____";

    private final Configuration configuration;

    private final Ehcache ehcache;

    private final DataSource dataSource;


    @Inject
    public AmbienteActualDao(@AmbienteDataSource DataSource dataSource,
                             Configuration configuration,
                             Ehcache cache) {
        this.configuration = configuration;
        this.ehcache = cache;
        this.dataSource = dataSource;
    }


//    @Inject
//    public AmbienteActualDao(@BasePrincipal DataSource dataSource,
//                             Configuration configuration,
//                             Ehcache cache) {
//        this.configuration = configuration;
//        this.ehcache = cache;
//        this.dataSource = dataSource;
//    }




    @Override
    public boolean isOffline() {
        Element element = ehcache.get(LLAVE_OFFLINE);
        if (element != null && !element.isExpired()) {
            return (Boolean) element.getObjectValue();
        }

        AmbienteActualBean ambienteActualBean = getAmbienteActual();
        boolean offline = ambienteActualBean != null && ambienteActualBean.isOffline();
        Element elementToStore = new Element(LLAVE_OFFLINE, offline);
        elementToStore.setTimeToLive(configuration.getInt(CACHE_AMBIENTE_ACTUAL, CACHE_AMBIENTE_ACTUAL_DEFAULT));
        ehcache.put(elementToStore);
        return offline;
    }

    @Override
    public boolean isAmbienteTemporal() {
        Element element = ehcache.get(LLAVE_AMBIENTE_TEMPORAL);
        if (element != null && !element.isExpired()) {
            return (Boolean) element.getObjectValue();
        }

        AmbienteActualBean ambienteActualBean = getAmbienteActual();
        boolean temporal = ambienteActualBean != null && ambienteActualBean.isTemporal();

        Element elementToStore = new Element(LLAVE_AMBIENTE_TEMPORAL, temporal);
        elementToStore.setTimeToLive(configuration.getInt(CACHE_AMBIENTE_ACTUAL, CACHE_AMBIENTE_ACTUAL_DEFAULT));
        ehcache.put(elementToStore);

        return temporal;
    }

    @Override
    public boolean convertirCCSID() {
        String string = configuration.getString(CONVERTIR_CCSID, CONVERTIR_CCSID_DEFAULT);
        return Arrays.asList("si", "SI", "1", "Si", "yes", "YES", "Yes", "True", "true", "TRUE").contains(string);
    }

    public void setProduccion(String username) {
        LOGGER.info("Cambiando aplicacion a ambiente de produccion por orden del usuario {}", username);
        cambiar(false, false, username);
    }

    public void setTemporal(String username) {
        LOGGER.info("Cambiando aplicacion a ambiente temporal por orden del usuario {}", username);
        cambiar(true, false, username);
    }

    public void setOffline(String username) {
        LOGGER.info("Cambiando aplicacion a offline por orden del usuario {}", username);
        cambiar(false, true, username);
    }

    private void cambiar(final boolean temporal, final boolean offline, String username) {
        ehcache.remove(LLAVE_OFFLINE);
        ehcache.remove(LLAVE_AMBIENTE_TEMPORAL);

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        PreparedStatement actualizacionPs = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("Select I90OFFLNE, I90AMTEMP, I90VER from ISBP90");

            if (resultSet.next()) {
                boolean anteriorTemporal = resultSet.getBoolean("I90AMTEMP");
                if (anteriorTemporal != temporal) {
                    LOGGER.info("Usuario {} ha cambiado el estado temporal de {} a {}",
                            new Object[]{username, anteriorTemporal, temporal});
                }

                boolean anteriorOffline = resultSet.getBoolean("E20OFFLNE");
                if (anteriorOffline != offline) {
                    LOGGER.info("Usuario {} ha cambiado el estado offile de {} a {}",
                            new Object[]{username, anteriorOffline, offline});
                }

                long version = resultSet.getLong("E20VER");

                actualizacionPs = connection.prepareStatement(
                        "UPDATE ISBP90 SET I90OFFLNE=?, I90AMTEMP=?, I90VER=? WHERE I90VER=?");
                actualizacionPs.setBoolean(1, offline);
                actualizacionPs.setBoolean(2, temporal);
                actualizacionPs.setLong(3, version + 1);
                actualizacionPs.setLong(4, version);
            } else {
                actualizacionPs = connection.prepareStatement(
                        "INSERT INTO ISBP90(I90OFFLNE, I90AMTEMP, I90USER, I90FECHA, I90VER)VALUES(?, ?, user, current_timestamp , 0)");
                actualizacionPs.setBoolean(1, offline);
                actualizacionPs.setBoolean(2, temporal);
            }

            int cuantos = actualizacionPs.executeUpdate();

            if (cuantos != 1) {
                throw new IllegalStateException("No se pudo cambiar el estado del ambiente actual");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            if (actualizacionPs != null) {
                try {
                    actualizacionPs.close();
                } catch (SQLException e) {
                    LOGGER.error("Error al cerrar", e);
                }
            }

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error("Error al cerrar", e);
                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error("Error al cerrar", e);
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Error Al cerrar", e);
                }
            }
        }
    }

    public AmbienteActualBean getAmbienteActual() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        AmbienteActualBean ambienteActualBean = new AmbienteActualBean();
        try {

            LOGGER.debug("CONSULTANDO REGISTRO DE AMBIENTE ACTUAL....");
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT I90OFFLNE, I90AMTEMP, I90USER, I90FECHA, I90VER FROM ISBP90");

            if (!resultSet.next()) {
                ambienteActualBean.setVersion(0L);
                ambienteActualBean.setOffline(false);
                ambienteActualBean.setTemporal(false);
                return ambienteActualBean;
            }

            ambienteActualBean.setVersion(resultSet.getLong("I90VER"));
            ambienteActualBean.setTemporal(resultSet.getBoolean("I90AMTEMP"));
            ambienteActualBean.setOffline(resultSet.getBoolean("I90OFFLNE"));
            ambienteActualBean.setUsuario(resultSet.getString("I90USER"));
            ambienteActualBean.setFecha(resultSet.getDate("I90FECHA"));

            LOGGER.debug("DATOS AMBIENTE ACTUAL...."+ambienteActualBean.toString());
            return ambienteActualBean;

        } catch (SQLException e) {
            LOGGER.error("No tengo ISBP90, asumiendo como viene", e);
            ambienteActualBean.setVersion(0L);
            ambienteActualBean.setOffline(false);
            ambienteActualBean.setTemporal(false);
            return ambienteActualBean;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error("Error al cerrar", e);
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOGGER.error("Error al cerrar", e);
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Error al cerrar ds", e);
                }
            }
        }
    }

}
