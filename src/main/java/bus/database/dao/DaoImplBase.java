/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.dao;

import org.apache.openjpa.persistence.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

/**
 * @author Marcelo Morales
 *         Date: Jul 13, 2010
 */
public class DaoImplBase<T> {

    protected static final ThreadLocal<Serializable> OBJETO_PARA_LOGUEAR = new ThreadLocal<Serializable>();
    private static final Logger LOGGER = LoggerFactory.getLogger(Dao.class);
    protected final OpenJPAEntityManagerFactory entityManagerFactory;
    private Class<T> persistentClass;

    public DaoImplBase(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = OpenJPAPersistence.cast(entityManagerFactory);
    }

    public final <Q> Q doWithTransaction(WithTransaction<Q> s) {
        OpenJPAEntityManager entityManager = null;
        OpenJPAEntityTransaction transaction = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            transaction = entityManager.getTransaction();
            transaction.begin();
            Q q = s.call(entityManager, transaction);
            transaction.commit();
            return q;

            // TODO: Rollbackexception cuando al commit, la base responde rollback

        } catch (Exception e) {
            try {
                final Serializable serializable = OBJETO_PARA_LOGUEAR.get();
                if (serializable != null) {
                    // TODO: colocar una definicion completa del entity que tiene esta clase, seguramente
                    LOGGER.info("El objeto que se intent� loguear es {}, la definici�n del entity es {}",
                            serializable, getClazz());
                }
            } catch (Throwable eee) {
                LOGGER.error("Ha ocurrido un error al mostrar el Objeto de la transaccion", eee);
            }
            if (transaction != null) {
                try {
                    transaction.rollback();
                } catch (InvalidStateException e1) {
                    LOGGER.info("Rollback programado no ha sido necesario: {}", e1.getMessage());
                } catch (Exception e1) {
                    LOGGER.warn("Ha fallado el rollback de " + e, e1);
                }
            }
            throw new RuntimeException("Se ha realizado rollback a una transaccion: el mensaje es " + e, e);
        } finally {
            try {
                if (entityManager != null) {
                    entityManager.close();
                }
            } catch (Exception e) {
                LOGGER.error("Error al cerrar la sesion", e);
            }
            OBJETO_PARA_LOGUEAR.remove();
        }
    }

    @SuppressWarnings("unchecked")
    protected synchronized Class<T> getClazz() {
        if (persistentClass == null) {
            this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).
                    getActualTypeArguments()[0];
        }
        return persistentClass;
    }

    public interface WithTransaction<S> {
        S call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t);
    }
}
