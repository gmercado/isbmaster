/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.dao;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

/**
 * @author Marcelo Morales
 * @since 1/9/11 12:52 PM
 */
public interface
        Dao<T extends Serializable, PK extends Serializable> {

    /**
     * Obtiene un objeto dada una llave primaria.
     *
     * @param id la llave primaria del objeto.
     * @return el elemento de la base de datos o <code>null</code> si no existiese.
     */
    T find(PK id);

    T find(final PK id, final String... fg);

    /**
     * Encapsula <code>merge</code> como se especifica con Hibernate.
     *
     * @param data Para persistir.
     * @return el elemento persistido.
     */
    T merge(T data);

    T persist(T data);

    T remove(PK id);

    int contarPorString(String terminos);

    List<T> buscarPorString(String terminos, OrderBy<T> orders, long first, long count, String... fg);

    List<T> buscarPorString(String terminos);

    List<T> getTodos(OrderBy<T> sortParam, long first, long count);

    List<T> getTodos(T example, OrderBy<T> sortParam, long first, long count);

    List<T> getTodos(T example, T example2, OrderBy<T> sortParam, long first, long count, String... fg);

    int contar();

    int contar(T example);

    int contar(T example, T example2);

    EntityManagerFactory getEntityManagerFactory();

    interface OrderBy<T extends Serializable> {

        List<Order> orders(CriteriaBuilder cb, Root<T> root);
    }

    List<T> listByNamedQuery(String namedQuery, Map<String, Object> parameters);

    Connection getConnection ();
}
