/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.dao;

import bus.plumbing.utils.Shutdowner;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author Marcelo Morales
 * @since 1/10/11 10:21 AM
 */
public class ExecutorServiceProvider implements Provider<ExecutorService> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutorServiceProvider.class);

    private volatile ExecutorService executorService;

    @Inject
    public ExecutorServiceProvider(Shutdowner shutdowner) {
        shutdowner.register(new Runnable() {
            @Override
            public void run() {
                stop();
                try {
                    executorService.awaitTermination(1, TimeUnit.HOURS);
                } catch (InterruptedException e) {
                    LOGGER.warn("Nos han interrumpido", e);
                }
            }
        });
    }

    @Override
    public synchronized ExecutorService get() {
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
        return executorService;
    }

    public void stop() {
        if (executorService != null && !executorService.isShutdown()) {
            executorService.shutdown();
        }
    }
}
