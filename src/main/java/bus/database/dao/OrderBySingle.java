package bus.database.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * @author Marcelo Morales
 *         Since: 2/6/13
 */
public abstract class OrderBySingle<T extends Serializable> implements Dao.OrderBy<T> {

    @Override
    public final List<Order> orders(CriteriaBuilder cb, Root<T> tRoot) {
        if (asc()) {
            return Collections.singletonList(cb.asc(getOrderBy(tRoot)));
        }
        return Collections.singletonList(cb.desc(getOrderBy(tRoot)));
    }

    protected abstract boolean asc();

    protected abstract Path<?> getOrderBy(Root<T> tRoot);
}
