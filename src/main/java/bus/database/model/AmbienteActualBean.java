/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Marcelo Morales
 * @since 6/28/11
 */
public final class AmbienteActualBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private long version;

    private boolean offline;

    private boolean temporal;

    private String usuario;

    private Date fecha;

    public long getVersion() {
        return this.version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public boolean isOffline() {
        return offline;
    }

    public void setOffline(boolean offline) {
        this.offline = offline;
    }

    public boolean isTemporal() {
        return temporal;
    }

    public void setTemporal(boolean temporal) {
        this.temporal = temporal;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof AmbienteActualBean))
            return false;
        AmbienteActualBean that = (AmbienteActualBean) o;
        return offline == that.offline && temporal == that.temporal && version == that.version;
    }

    @Override
    public int hashCode() {
        int result = (int) (version ^ (version >>> 32));
        result = 31 * result + (offline ? 1 : 0);
        result = 31 * result + (temporal ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AmbienteActualBean{");
        sb.append("version=").append(version);
        sb.append(", offline=").append(offline);
        sb.append(", temporal=").append(temporal);
        sb.append(", usuario='").append(usuario).append('\'');
        sb.append(", fecha=").append(fecha);
        sb.append('}');
        return sb.toString();
    }
}
