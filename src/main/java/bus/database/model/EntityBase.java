/*
 *  Copyright 2009 Banco Bisa S.A.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.database.model;

import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Roger Chura
 * @author Miguel vega
 */
@MappedSuperclass
public class EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.TIMESTAMP)
    protected Date fechaCreacion;

    protected String usuarioCreador;

    @Temporal(TemporalType.TIMESTAMP)
    protected Date fechaModificacion;

    protected String usuarioModificador;

    public Date getFechaCreacion() {
        if (fechaCreacion == null) {
            return null;
        }
        return new Date(fechaCreacion.getTime());
    }

    public void setFechaCreacion(Date fechaCreacion) {
        if (fechaCreacion == null) {
            this.fechaCreacion = null;
        } else {
            this.fechaCreacion = new Date(fechaCreacion.getTime());
        }
    }

    public Date getFechaModificacion() {
        if (fechaModificacion == null) {
            return null;
        }
        return new Date(fechaModificacion.getTime());
    }

    public void setFechaModificacion(Date fechaModificacion) {
        if (fechaModificacion == null) {
            this.fechaModificacion = null;
        } else {
            this.fechaModificacion = new Date(fechaModificacion.getTime());
        }
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @Override
    public String toString() {
        return "EntityBase{" +
                "fechaCreacion=" + fechaCreacion +
                ", usuarioCreador='" + usuarioCreador + '\'' +
                ", fechaModificacion=" + fechaModificacion +
                ", usuarioModificador='" + usuarioModificador + '\'' +
                '}';
    }
}
