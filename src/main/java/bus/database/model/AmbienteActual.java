/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.model;

/**
 * Interfaz para detectar el estado de la aplicaci�n, si est� fuera de l�nea, o en ambiente temporal.
 *
 * @author Marcelo Morales
 * @since 6/9/11
 */
public interface AmbienteActual {

    /**
     * Aplicaci�n cerrada.
     *
     * @return true si la aplicaci�n est� cerrada.
     */
    boolean isOffline();

    /**
     * Ambiente temporal
     *
     * @return true si estamos en ambiente temporal.
     */
    boolean isAmbienteTemporal();

    /**
     * Convertir CCSID
     *
     * @return true si es necesario convertir la codificaci�n de las consultas/datos de monitor.
     */
    boolean convertirCCSID();
}
