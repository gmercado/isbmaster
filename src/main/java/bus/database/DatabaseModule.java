package bus.database;

import bus.database.dao.AmbienteActualDao;
import bus.database.dao.ExecutorServiceProvider;
import bus.database.datasource.CambioAmbienteDataSource;
import bus.database.datasource.DataSourcePrincipalProvider;
import bus.database.datasource.DataSourceTemporalProvider;
import bus.database.jpa.EntityManagerFactoryServicePrincipal;
import bus.database.model.*;
import bus.plumbing.api.AbstractModuleBisa;
import com.google.inject.Scopes;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.concurrent.ExecutorService;

/**
 * @author Marcelo Morales
 * @since 4/30/12
 */
public class DatabaseModule extends AbstractModuleBisa {

    @Override
    protected void configure() {

        //a datasource only for AmbienteDao
        bind(DataSource.class).annotatedWith(AmbienteDataSource.class).
                toProvider(DataSourcePrincipalProvider.class).in(Scopes.SINGLETON);

        bind(AmbienteActual.class).to(AmbienteActualDao.class);

        bind(DataSource.class).annotatedWith(BaseSinCambioAmbiente.class).
                toProvider(DataSourcePrincipalProvider.class).in(Scopes.SINGLETON);

        bind(DataSource.class).annotatedWith(BaseTemporal.class).
                toProvider(DataSourceTemporalProvider.class).in(Scopes.SINGLETON);

        //base eprincipal, basada en CambioAmbienteDataSource
        bind(DataSource.class).annotatedWith(BasePrincipal.class).
                to(CambioAmbienteDataSource.class).in(Scopes.SINGLETON);

//        bind(DataSource.class).annotatedWith(BasePrincipal.class).
//                toProvider(DataSourceProviderAS400.class).in(Scopes.SINGLETON);

        bind(String.class).annotatedWith(BasePrincipal.class).toInstance("principal");

        bind(EntityManagerFactory.class).annotatedWith(BasePrincipal.class).
                toProvider(EntityManagerFactoryServicePrincipal.class).in(Scopes.SINGLETON);

        bind(ExecutorService.class).annotatedWith(SQLFueraDeLinea.class).toProvider(ExecutorServiceProvider.class).
                in(Scopes.SINGLETON);

        bindUI("bus/database/ui");
    }
}
