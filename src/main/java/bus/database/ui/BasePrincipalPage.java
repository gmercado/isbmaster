/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.database.ui;

import bus.database.model.*;
import bus.database.perf.PerfDataSource;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.utils.PerfStat;
import com.google.inject.Key;
import org.apache.openjpa.jdbc.conf.JDBCConfiguration;
import org.apache.openjpa.jdbc.meta.MappingTool;
import org.apache.openjpa.jdbc.schema.SchemaTool;
import org.apache.openjpa.meta.MetaDataRepository;
import org.apache.openjpa.persistence.EntityManagerFactoryImpl;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Map;

/**
 * @author Marcelo Morales
 *         Date: 6/24/11
 *         Time: 5:38 PM
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Menu(value = "Estad\u00edsticas de base de datos", subMenu = SubMenu.ADMINISTRACION)
public class BasePrincipalPage extends BisaWebPage {

    public BasePrincipalPage() {
        super();

        add(new DetalleConsultasPanel("consultas400", new LoadableDetachableModel<Map<String, PerfStat>>() {
            @Override
            protected Map<String, PerfStat> load() {
                return ((PerfDataSource) getInstance(Key.get(DataSource.class, BaseSinCambioAmbiente.class))).getPerfStats();
            }
        }));

        add(new DetalleConsultasPanel("consultas400t", new LoadableDetachableModel<Map<String, PerfStat>>() {
            @Override
            protected Map<String, PerfStat> load() {
                return ((PerfDataSource) getInstance(Key.get(DataSource.class, BaseTemporal.class))).getPerfStats();
            }
        }));

        add(new DetalleConsultasPanel("consultasSql", new LoadableDetachableModel<Map<String, PerfStat>>() {
            @Override
            protected Map<String, PerfStat> load() {
                return ((PerfDataSource) getInstance(Key.get(DataSource.class, BaseAuxiliar.class))).getPerfStats();
            }
        }));

        add(new DetalleConsultasPanel("consultasAux2", new LoadableDetachableModel<Map<String, PerfStat>>() {
            @Override
            protected Map<String, PerfStat> load() {
                return ((PerfDataSource) getInstance(Key.get(DataSource.class, BaseAuxiliar2.class))).getPerfStats();
            }
        }));

        add(new DetalleConsultasPanel("consultasAux3", new LoadableDetachableModel<Map<String, PerfStat>>() {

            private static final long serialVersionUID = 2737271127727243890L;

            @Override
            protected Map<String, PerfStat> load() {
                return ((PerfDataSource) getInstance(Key.get(DataSource.class, BaseAuxiliar3.class))).getPerfStats();
            }
        }));

        add(new DetalleConsultasPanel("consultasStadistic", new LoadableDetachableModel<Map<String, PerfStat>>() {

            private static final long serialVersionUID = 2737271127727243890L;

            @Override
            protected Map<String, PerfStat> load() {
                return ((PerfDataSource) getInstance(Key.get(DataSource.class, BaseStadistic.class))).getPerfStats();
            }
        }));

        IModel<String> sqlModel = Model.of("");

        add(new Label("script", sqlModel));

        add(new Link<String>("colocar script", sqlModel) {

            private static final long serialVersionUID = 1L;

            @Override
            public void onClick() {

                try {
                    EntityManagerFactoryImpl emf =
                            (EntityManagerFactoryImpl) getInstance(Key.get(EntityManagerFactory.class, BasePrincipal.class));

                    JDBCConfiguration conf = (JDBCConfiguration) emf.getBrokerFactory().getConfiguration();

                    MetaDataRepository mr = conf.getMappingRepositoryInstance();

                    Collection<Class<?>> classes = mr.loadPersistentTypes(true, getClass().getClassLoader());

                    StringWriter mappingWriter = new StringWriter();

                    MappingTool.Flags flags = new MappingTool.Flags();
                    flags.action = MappingTool.ACTION_BUILD_SCHEMA;
                    flags.sqlWriter = mappingWriter;
                    flags.readSchema = false;
                    flags.ignoreErrors = true;
                    flags.schemaAction = SchemaTool.ACTION_BUILD;

                    MappingTool tool = new MappingTool(conf, flags.action, false);
                    tool.setIgnoreErrors(flags.ignoreErrors);
                    tool.setMetaDataFile(flags.metaDataFile);
                    tool.setMappingWriter(flags.mappingWriter);
                    tool.setSchemaAction(flags.schemaAction);
                    tool.setSchemaWriter(flags.schemaWriter);
                    tool.setReadSchema(flags.readSchema);
                    tool.setPrimaryKeys(flags.primaryKeys);
                    tool.setForeignKeys(flags.foreignKeys);
                    tool.setIndexes(flags.indexes);
                    tool.setSequences(flags.sequences || flags.dropSequences);
                    SchemaTool schemaTool = new SchemaTool(conf);
                    schemaTool.setWriter(mappingWriter);
                    tool.setSchemaTool(schemaTool);

                    classes.forEach(tool::run);
                    tool.record(flags);

                    getModel().setObject(mappingWriter.toString());

                } catch (Exception e) {
                    getSession().error(e.getMessage());
                    LOGGER.warn("Error", e);
                }
            }
        });
    }
}
