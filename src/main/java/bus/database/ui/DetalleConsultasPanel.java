/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.database.ui;

import bus.plumbing.components.DPropertyColumn;
import bus.plumbing.utils.PerfStat;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;

import java.util.*;

/**
 * @author Marcelo Morales
 *         Date: 7/9/11
 *         Time: 6:31 AM
 */
class DetalleConsultasPanel extends Panel {

    private static final String SORT_C = "c";
    private static final String SORT_SUM = "sum";
    private static final String SORT_COUNT = "count";
    private static final String SORT_MIN = "min";
    private static final String SORT_MAX = "max";
    private static final String SORT_MEAN = "mean";
    private static final String SORT_VAR = "var";

    DetalleConsultasPanel(String id, IModel<Map<String, PerfStat>> model) {
        super(id, model);

        add(new AjaxFallbackDefaultDataTable<>(
                "perfs",
                new ArrayList<IColumn<Map.Entry<String, PerfStat>, String>>() {

                    {
                        add(new AbstractColumn<Map.Entry<String, PerfStat>, String>(Model.of("Consulta"), SORT_C) {

                            @Override
                            public void populateItem(Item<ICellPopulator<Map.Entry<String, PerfStat>>> cellItem,
                                                     String componentId, IModel<Map.Entry<String, PerfStat>> rowModel) {
                                cellItem.add(new Label(componentId, rowModel.getObject().getKey()));
                            }
                        });
                        add(new DPropertyColumn<>(Model.of("Cantidad"), SORT_COUNT, "value.count"));
                        add(new DPropertyColumn<>(Model.of("M\u00ednimos milis"), SORT_MIN, "value.min"));
                        add(new DPropertyColumn<>(Model.of("Media milis"), SORT_MEAN, "value.mean"));
                        add(new DPropertyColumn<>(Model.of("M\u00e1ximos milis"), SORT_MAX, "value.max"));
                        add(new DPropertyColumn<>(Model.of("Varianza"), SORT_VAR, "value.variance"));
                        add(new DPropertyColumn<>(Model.of("Milis totales"), SORT_SUM, "value.sum"));
                    }
                },
                new SortableDataProvider<Map.Entry<String, PerfStat>, String>() {

                    @Override
                    public Iterator<? extends Map.Entry<String, PerfStat>> iterator(long first, long count) {
                        @SuppressWarnings({"unchecked"})
                        Map<String, PerfStat> map = (Map<String, PerfStat>) getDefaultModelObject();

                        if (getSort() == null && first == 0) {
                            return map.entrySet().iterator();
                        }

                        ArrayList<Map.Entry<String, PerfStat>> entries =
                                new ArrayList<>(map.entrySet());

                        if (getSort() == null) {
                            return entries.subList((int) first, (int) (first + count)).iterator();
                        }

                        Collections.sort(entries, (o1, o2) -> {
                            if (SORT_SUM.equals(getSort().getProperty())) {
                                Long sum1 = o1.getValue().getSum();
                                Long sum2 = o2.getValue().getSum();
                                if (getSort().isAscending()) {
                                    return sum1.compareTo(sum2);
                                } else {
                                    return sum2.compareTo(sum1);
                                }
                            }

                            if (SORT_C.equals(getSort().getProperty())) {
                                if (getSort().isAscending()) {
                                    return o1.getKey().compareTo(o2.getKey());
                                } else {
                                    return o2.getKey().compareTo(o1.getKey());
                                }
                            }

                            if (SORT_COUNT.equals(getSort().getProperty())) {
                                Integer count1 = o1.getValue().getCount();
                                Integer count2 = o2.getValue().getCount();
                                if (getSort().isAscending()) {
                                    return count1.compareTo(count2);
                                } else {
                                    return count2.compareTo(count1);
                                }
                            }

                            if (SORT_MIN.equals(getSort().getProperty())) {
                                Long count1 = o1.getValue().getMin();
                                Long count2 = o2.getValue().getMin();
                                if (getSort().isAscending()) {
                                    return count1.compareTo(count2);
                                } else {
                                    return count2.compareTo(count1);
                                }
                            }

                            if (SORT_MAX.equals(getSort().getProperty())) {
                                Long count1 = o1.getValue().getMax();
                                Long count2 = o2.getValue().getMax();
                                if (getSort().isAscending()) {
                                    return count1.compareTo(count2);
                                } else {
                                    return count2.compareTo(count1);
                                }
                            }

                            if (SORT_MEAN.equals(getSort().getProperty())) {
                                Double count1 = o1.getValue().getMean();
                                Double count2 = o2.getValue().getMean();
                                if (getSort().isAscending()) {
                                    return count1.compareTo(count2);
                                } else {
                                    return count2.compareTo(count1);
                                }
                            }

                            if (SORT_VAR.equals(getSort().getProperty())) {
                                Double count1 = o1.getValue().getVariance();
                                Double count2 = o2.getValue().getVariance();
                                if (getSort().isAscending()) {
                                    return count1.compareTo(count2);
                                } else {
                                    return count2.compareTo(count1);
                                }
                            }

                            return 0;
                        });

                        return entries.subList((int) first, (int) (first + count)).iterator();
                    }

                    @Override
                    public long size() {
                        @SuppressWarnings({"unchecked"})
                        Map<String, PerfStat> map = (Map<String, PerfStat>) getDefaultModelObject();
                        return map != null ? map.size() : 0;
                    }

                    @Override
                    public IModel<Map.Entry<String, PerfStat>> model(Map.Entry<String, PerfStat> object) {
                        final String s = object.getKey();
                        final PerfStat p = object.getValue();
                        return new LoadableDetachableModel<Map.Entry<String, PerfStat>>() {
                            @Override
                            protected Map.Entry<String, PerfStat> load() {
                                return new AbstractMap.SimpleImmutableEntry<>(s, p);
                            }
                        };
                    }
                },
                1000
        ));
    }
}
