/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
package bus.database.jpa;

import bus.database.model.BaseAuxiliar3;
import bus.plumbing.utils.Shutdowner;
import com.google.inject.Inject;

import javax.sql.DataSource;

public class EntityManagerFactoryServiceAuxiliar3 extends EntityManagerFactoryProvider {

    @Inject
    public EntityManagerFactoryServiceAuxiliar3(@BaseAuxiliar3 DataSource dataSource, @BaseAuxiliar3 String pu, Shutdowner shutdowner) {
        super(dataSource, pu, shutdowner, BaseAuxiliar3.class);
    }
}
