/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.jpa;

import org.apache.openjpa.jdbc.sql.DB2Dictionary;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Diccionario para AS/400 con el otro sistema de nombres.
 *
 * @author Marcelo Morales
 */
public class BisaDictionary extends DB2Dictionary {

    @Override
    public void connectedConfiguration(Connection conn) throws SQLException {
        super.connectedConfiguration(conn);

        // la 400 del banco es mas o menos pre-diluviana
        supportsGetGeneratedKeys = false;

        // naming=system hace que necesitemos cambiar el . por un /
        lastGeneratedKeyQuery = "select identity_val_local() from sysibm/sysdummy1";
    }
}
