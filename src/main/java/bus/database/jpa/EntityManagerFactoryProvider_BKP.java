/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.jpa;

import bus.plumbing.utils.Shutdowner;
import com.google.inject.Provider;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Para construir el esquema, utiliza variables de env o pasarlas con a properties de java.
 *
 * @author Marcelo Morales
 * @since 1/10/11 10:10 AM
 */
abstract class EntityManagerFactoryProvider_BKP implements Provider<EntityManagerFactory> {
    private static final Logger LOGGER = LoggerFactory.getLogger(EntityManagerFactoryProvider_BKP.class);
    private volatile EntityManagerFactory emf;
    private final String pu;
    private final DataSource dataSource;

    EntityManagerFactoryProvider_BKP(DataSource dataSource,
                                     String pu,
                                     Shutdowner shutdowner) {
        super();
        this.dataSource = dataSource;
        this.pu = pu;
        shutdowner.register(new Runnable() {

            @Override
            public void run() {
                stop();
            }
        });
    }

    @Override
    public synchronized EntityManagerFactory get() {
        if (emf == null) {
            start();
        }
        return emf;
    }

    public void start() {
        if (emf != null) {
            emf.close();
        }
        LOGGER.info("Abriendo OpenJPA para el provider {}", getClass().getName());

        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("openjpa.Log", "log4j");
        properties.put("openjpa.ConnectionFactory", dataSource);
        properties.put("openjpa.LockTimeout", 10000);
        properties.put("openjpa.jdbc.TransactionIsolation", "read-committed");
        properties.put("openjpa.BrokerImpl", "non-finalizing");

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            String databaseProductName = connection.getMetaData().getDatabaseProductName();
            LOGGER.info("La base a la que me conecto para el Persistence unit '{}' responde '{}'", pu, databaseProductName);
            if (StringUtils.contains(databaseProductName, "DB2") && StringUtils.contains(databaseProductName, "AS")) {
                properties.put("openjpa.jdbc.DBDictionary", BisaDictionary.class.getName());
                LOGGER.info("Utilizando el dialecto BisaDictionary (AS/400 modificado) para la conexion {}",
                        getClass().getName());
            }
        } catch (SQLException e) {
            LOGGER.error("Error conectar la base de datos referida en el persistence unit " + pu, e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                LOGGER.warn("Error al cerrar la conexion a la base de datos", e);
            }
        }

        properties.put("openjpa.jdbc.QuerySQLCache", "false");
        properties.put("openjpa.DataCache", "false");
        properties.put("openjpa.QueryCache", "false");
        properties.put("openjpa.RemoteCommitProvider", "sjvm");

        emf = Persistence.createEntityManagerFactory(pu, properties);
    }

    public void stop() {
        if (emf != null) {
            LOGGER.info("Cerrando OpenJPA para el provider {}", getClass().getName());
            emf.close();
            emf = null;
        }
    }


    public DataSource getDataSource() {
        return dataSource;
    }
}
