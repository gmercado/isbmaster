/*
/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.database.datasource;

import com.google.inject.Provider;

import javax.sql.DataSource;

/**
 * @author Marcelo Morales
 * @since 1/10/11 10:07 AM
 */
public abstract class DataSourceServiceProvider implements Provider<DataSource> {

    private volatile DataSource delegateDataSource;

    public synchronized DataSource get() {
        if (delegateDataSource == null) {
            delegateDataSource = newDataSourceProxy();
        }
        return delegateDataSource;
    }

    protected abstract DataSource newDataSourceProxy();

    public DataSource getDelegateDataSource() {
        return delegateDataSource;
    }
}
