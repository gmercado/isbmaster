/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package bus.database.datasource;

import bus.database.model.AmbienteActual;
import bus.database.model.BasePrincipal;
import bus.database.model.BaseSinCambioAmbiente;
import bus.database.model.BaseTemporal;
import bus.database.perf.PerfDataSource;
import com.google.inject.Inject;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

/**
 * @author Marcelo Morales
 * @since 2/9/11 11:28 AM
 */
public class CambioAmbienteDataSource implements DataSource {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(CambioAmbienteDataSource.class);

    private final DataSource dataSource;

    private final DataSource dataSourceTemporal;

    private final AmbienteActual ambienteActual;

    @Inject
    public CambioAmbienteDataSource(@BaseSinCambioAmbiente DataSource dataSource,
                                    @BaseTemporal DataSource dataSourceTemporal, AmbienteActual ambienteActual) {
        this.dataSource = dataSource;
        this.dataSourceTemporal = dataSourceTemporal;
        this.ambienteActual = ambienteActual;
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (ambienteActual.isAmbienteTemporal()) {
            LOG.debug("Estamos en ambiente TEMPORAL. URL: {}", ((PerfDataSource)dataSourceTemporal).getTargetDataSource().getUrl());
            return dataSourceTemporal.getConnection();
        }
        LOG.debug("Estamos en ambiente PRINCIPAl. URL: {}", ((PerfDataSource)dataSource).getTargetDataSource().getUrl());
        return dataSource.getConnection();
    }

    @Override
    public Connection getConnection(String s, String s1) throws SQLException {
        if (ambienteActual.isAmbienteTemporal()) {
            LOG.debug("Estamos en ambiente TEMPORAL. URL: {}", ((PerfDataSource)dataSourceTemporal).getTargetDataSource().getUrl());
            return dataSourceTemporal.getConnection(s, s1);
        }
        LOG.debug("Estamos en ambiente PRINCIPAl. URL: {}", ((PerfDataSource)dataSource).getTargetDataSource().getUrl());
        return dataSource.getConnection(s, s1);
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        if (ambienteActual.isAmbienteTemporal()) {
            return dataSourceTemporal.getLogWriter();
        }
        return dataSource.getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter printWriter) throws SQLException {
        dataSource.setLogWriter(printWriter);
        dataSourceTemporal.setLogWriter(printWriter);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        if (ambienteActual.isAmbienteTemporal()) {
            return dataSourceTemporal.getLoginTimeout();
        }
        return dataSource.getLoginTimeout();
    }

    @Override
    public void setLoginTimeout(int i) throws SQLException {
        dataSource.setLoginTimeout(i);
        dataSourceTemporal.setLoginTimeout(i);
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new UnsupportedOperationException("Mothod not supported");
    }

    public <T> T unwrap(Class<T> tClass) throws SQLException {
        if (ambienteActual.isAmbienteTemporal()) {
            return dataSourceTemporal.unwrap(tClass);
        }
        return dataSource.unwrap(tClass);
    }

    public boolean isWrapperFor(Class<?> aClass) throws SQLException {
        if (ambienteActual.isAmbienteTemporal()) {
            return dataSourceTemporal.isWrapperFor(aClass);
        }
        return dataSource.isWrapperFor(aClass);
    }
}