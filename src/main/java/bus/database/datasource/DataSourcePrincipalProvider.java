/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
package bus.database.datasource;

import bus.database.model.BasePrincipal;
import bus.database.perf.PerfDataSource;
import bus.plumbing.utils.Shutdowner;
import bus.plumbing.utils.Watches;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.SQLException;

import static bus.config.api.Configurations.*;

/**
 * @author Marcelo Morales
 */
public class DataSourcePrincipalProvider extends DataSourceServiceProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataSourcePrincipalProvider.class);

    private final Configuration configuration;

    private final Shutdowner shutdowner;


    @Inject
    public DataSourcePrincipalProvider(Configuration configuration, Shutdowner shutdowner) {
        this.shutdowner = shutdowner;
        this.configuration = configuration;
    }

    @Override
    protected DataSource newDataSourceProxy() {
        final BasicDataSource bds = new BasicDataSource();
        final String string = configuration.getString(BASEPRIN_URI, BASEPRIN_URI_DEFAULT);

        bds.setDriverClassName(configuration.getString(BASEPRIN_DRIVER, BASEPRIN_DRIVER_DEFAULT));
        bds.setUrl(string);
        bds.setUsername(configuration.getString(BASEPRIN_USERNAME, BASEPRIN_USERNAME_DEFAULT));
        bds.setPassword(configuration.getString(BASEPRIN_PASSWORD, BASEPRIN_PASSWORD_DEFAULT));
        bds.setInitialSize(0);
        bds.setValidationQuery(configuration.getString(BASEPRIN_VALIDATION_QUERY, BASEPRIN_VALIDATION_QUERY_DEFAULT));
        bds.setMaxActive(4);

        bds.setPoolPreparedStatements(true);
        bds.setTestOnBorrow(false);
        bds.setTestOnReturn(false);
        bds.setTestWhileIdle(true);
        bds.setTimeBetweenEvictionRunsMillis(30000);

        LOGGER.info("PRINCIPAL: Inicializando el dataSource a {}, Utilizando el URL {}.", bds.getUrl(), string);


        shutdowner.register(() -> {
            try {
                bds.close();
            } catch (SQLException e) {
                LOGGER.error("Error al cerrar base " + string, e);
            }
        });

        return new PerfDataSource(bds, Watches.DB_MILIS_PRINCIPAL, 500);
    }
}
