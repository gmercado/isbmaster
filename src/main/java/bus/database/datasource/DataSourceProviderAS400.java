/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 */
package bus.database.datasource;

import bus.database.model.AmbienteActual;
import bus.database.model.BasePrincipal;
import bus.database.model.BaseSinCambioAmbiente;
import bus.database.model.BaseTemporal;
import bus.database.perf.PerfDataSource;
import bus.plumbing.utils.Shutdowner;
import bus.plumbing.utils.Watches;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.SQLException;

import static bus.config.api.Configurations.*;

/**
 * @author Marcelo Morales
 */
public class DataSourceProviderAS400 extends DataSourceServiceProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataSourceProviderAS400.class);

    private final DataSource dataSourcePrincipal;

    private final DataSource dataSourceTemporal;

    private final AmbienteActual ambienteActual;

    @Inject
    public DataSourceProviderAS400(@BaseSinCambioAmbiente DataSource dataSourcePrincipal,
                                   @BaseTemporal DataSource dataSourceTemporal,
                                   AmbienteActual ambienteActual) {
        this.dataSourcePrincipal = dataSourcePrincipal;
        this.dataSourceTemporal = dataSourceTemporal;
        this.ambienteActual = ambienteActual;
    }

    @Override
    protected DataSource newDataSourceProxy() {
        return new CambioAmbienteDataSource(dataSourcePrincipal,
                dataSourceTemporal,
                ambienteActual);
    }
}
