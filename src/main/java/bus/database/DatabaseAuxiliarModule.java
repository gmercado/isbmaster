package bus.database;

import com.google.inject.AbstractModule;
import com.google.inject.Provider;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.lang.annotation.Annotation;

import static com.google.inject.Scopes.SINGLETON;

/**
 * @author Marcelo Morales
 *         Since: 7/12/13
 */
public class DatabaseAuxiliarModule extends AbstractModule {

    private final Class<? extends Annotation> annotationType;
    private final String pu;
    private final Class<? extends Provider<DataSource>> dataSourceProvider;
    private final Class<? extends Provider<EntityManagerFactory>> emfProvider;

    public DatabaseAuxiliarModule(Class<? extends Annotation> annotationType,
                                  Class<? extends Provider<DataSource>> dataSourceProvider,
                                  String pu,
                                  Class<? extends Provider<EntityManagerFactory>> emfProvider) {
        this.annotationType = annotationType;
        this.pu = pu;
        this.dataSourceProvider = dataSourceProvider;
        this.emfProvider = emfProvider;
    }

    @Override
    protected void configure() {
        bind(DataSource.class).annotatedWith(annotationType).toProvider(dataSourceProvider).in(SINGLETON);
        bind(String.class).annotatedWith(annotationType).toInstance(pu);
        bind(EntityManagerFactory.class).annotatedWith(annotationType).toProvider(emfProvider).in(SINGLETON);
    }
}
