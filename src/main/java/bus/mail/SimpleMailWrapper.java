package bus.mail;

import bus.env.api.MedioAmbiente;
import com.google.inject.Inject;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import static bus.env.api.Variables.*;

/**
 * @author Marcelo Morales
 *         Created: 5/11/12 12:27 PM
 */
public class SimpleMailWrapper extends SimpleEmail{

    //private final SimpleEmail simpleEmail;
    private final MedioAmbiente medioAmbiente;

    @Inject
    public SimpleMailWrapper(MedioAmbiente medioAmbiente1) throws EmailException {
        super();
        this.medioAmbiente = medioAmbiente1;
//        simpleEmail = new SimpleEmail();
//        simpleEmail.setHostName(medioAmbiente.getValorDe(MAIL_HOST, MAIL_HOST_DEFAULT));
//        simpleEmail.setFrom(medioAmbiente.getValorDe(MAIL_FROM, MAIL_FROM_DEFAULT));

        setHostName(medioAmbiente.getValorDe(MAIL_HOST, MAIL_HOST_DEFAULT));
        setFrom(medioAmbiente.getValorDe(MAIL_FROM, MAIL_FROM_DEFAULT));
        setSmtpPort(medioAmbiente.getValorIntDe(MAIL_PORT, MAIL_PORT_DEFAULT));

    }

    public String sendMsg(String subject, String msg, String... to) throws EmailException {
//        for (String s : to) {
//            simpleEmail.addTo(s);
//        }
//        return simpleEmail.setSubject(subject).setMsg(msg).send();

        for (String s : to) {
            addTo(s);
        }
        return setSubject(subject).setMsg(msg).send();
    }
}
