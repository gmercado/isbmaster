package bus.mail.api;

import bus.env.api.MedioAmbiente;
import bus.mail.dao.CorreoListaDao;
import bus.mail.model.Mailer;
import bus.mail.model.MailerAttachments;
import bus.mail.model.MailerLista;
import bus.mail.model.ObtenerTo;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static bus.env.api.Variables.*;

/**
 * Created by rchura on 29-01-16.
 */
public class MailerFactory implements DisposableBean, InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailerFactory.class);

    private static final int MANDAR_MAIL_THREADS = 10;

    private static final int ESPERA_PARA_TERMINAR = 15;

    private ExecutorService executor;

    private final MedioAmbiente medioAmbiente;
    private final CorreoListaDao correoListaDao;
    private final MailerAttachments mailerAttachments;

    @Inject
    public MailerFactory(MedioAmbiente medioAmbiente, CorreoListaDao correoListaDao, MailerAttachments mailerAttachments) {
        this.medioAmbiente = medioAmbiente;
        this.correoListaDao = correoListaDao;
        this.mailerAttachments = mailerAttachments;
    }


    public Mailer getMailer(final String email) {

        String from = medioAmbiente.getValorDe(MAIL_FROM, MAIL_FROM_DEFAULT);
        return new Mailer(mailerAttachments, from, executor, new ObtenerTo() {

            @Override
            public String[] getTo() {
                return new String[]{email};
            }

            @Override
            public String getPrefix() {
                return "";
            }

        });
    }

    public Mailer getMailer(final String[] emails) {

        String from = medioAmbiente.getValorDe(MAIL_FROM, MAIL_FROM_DEFAULT);
        return new Mailer(mailerAttachments, from, executor, new ObtenerTo() {

            @Override
            public String[] getTo() {
                return emails;
            }

            @Override
            public String getPrefix() {
                return "";
            }

        });
    }

    public Mailer getMailer(final MailerLista mailerLista) {
        String from = medioAmbiente.getValorDe(MAIL_FROM, MAIL_FROM_DEFAULT);
        return new Mailer(mailerAttachments, from, executor, new ObtenerTo() {

            @Override
            public String[] getTo() {
                List<String> todos = correoListaDao.getTodosActivos(mailerLista);
                LOGGER.debug("Envio de correo de notificacion a correos {}.", todos);
                return todos.toArray(new String[todos.size()]);
            }

            @Override
            public String getPrefix() {
                return mailerLista.getPrefijo();
            }
        });
    }


    @Override
    public void destroy() {

        executor.shutdown();
        LOGGER.info("Terminando el servicio de ejecucion de actualizacion de web service {}. " +
                "Esperando {} a que los trabajos terminen", executor, ESPERA_PARA_TERMINAR);
        try {
            executor.awaitTermination(ESPERA_PARA_TERMINAR, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
            LOGGER.warn("Nos han interrumpido", ex);
        }
        LOGGER.info("Ejecutor {} terminado", executor);
    }

    @Override
    public void afterPropertiesSet() {
        executor = Executors.newFixedThreadPool(MANDAR_MAIL_THREADS);
    }

    private Properties propertiesMailer() {
        String host = medioAmbiente.getValorDe(MAIL_HOST, MAIL_HOST_DEFAULT);
        String from = medioAmbiente.getValorDe(MAIL_FROM, MAIL_FROM_DEFAULT);
        int port = medioAmbiente.getValorIntDe(MAIL_PORT, MAIL_PORT_DEFAULT);

        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.setProperty("mail.smtp.starttls.enable", "fasle"); //true
        props.setProperty("mail.smtp.port", "" + port);
        props.setProperty("mail.smtp.user", from); //correo
        props.setProperty("mail.smtp.auth", "false"); //true

        return props;
    }
}
