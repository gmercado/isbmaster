/*
 *  Copyright 2009 Banco Bisa S.A.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.mail.entities;

import bus.mail.model.MailerLista;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author marcelo
 */
@Entity
@Table(name = "ISBP07")
public class CorreoLista implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I07NID")
    private Long id;

    @SuppressWarnings("unused")
    @Version
    @Column(name = "I07NVERSN")
    private long version;

    @Enumerated(EnumType.STRING)
    @Column(name = "I07VLISTA", length = 50, nullable = false)
    private MailerLista mailerLista;

    @Column(name = "I07VEMAIL")
    private String email;

    @Column(name = "I07NACTIVO")
    private Boolean activo;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MailerLista getMailerLista() {
        return mailerLista;
    }

    public void setMailerLista(MailerLista mailerLista) {
        this.mailerLista = mailerLista;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getActivoAsString() {
        if (activo != null && activo.booleanValue()) {
            return "Activo";
        }
        return "Desactivado";
    }

    @Override
    public String toString() {
        return "CorreoLista{" +
                "id=" + id +
                ", version=" + version +
                ", mailerLista=" + mailerLista +
                ", email='" + email + '\'' +
                ", activo=" + activo +
                '}';
    }
}
