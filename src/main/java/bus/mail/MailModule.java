package bus.mail;

import bus.mail.api.MailerFactory;
import bus.plumbing.api.AbstractModuleBisa;
import com.google.inject.Scopes;

/**
 * @author Marcelo Morales
 *         Created: 5/15/12 7:23 PM
 */
public class MailModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(SimpleMailWrapper.class).in(Scopes.SINGLETON);
        bind(MailerFactory.class).in(Scopes.SINGLETON);
        bindUI("bus/mail/ui");
    }
}
