package bus.mail.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.mail.entities.CorreoLista;
import bus.mail.model.MailerLista;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by rchura on 29-01-16.
 */
public class CorreoListaDao extends DaoImpl<CorreoLista, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CorreoListaDao.class);

    @Inject
    public CorreoListaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);

    }

    @Override
    protected Path<Long> countPath(Root<CorreoLista> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<CorreoLista> p) {
        Path<String> email = p.get("email");
        return Lists.newArrayList(email);
    }


    public CorreoLista getById(Long id) {
        return find(id);
    }

    public List<CorreoLista> getTodos(CorreoLista ejemplo, String order, boolean asc, int first, int count) {
        return getTodos(ejemplo, null, first, count);
        //List<T> getTodos(T example, OrderBy<T> sortParam, long first, long count)+;
    }


    public List<String> getTodosActivos(MailerLista mailerLista) {

        return doWithTransaction(new WithTransaction<List<String>>() {
            @Override
            public List<String> call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {

                OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
                OpenJPACriteriaQuery<CorreoLista> q = cb.createQuery(CorreoLista.class);
                Root<CorreoLista> from = q.from(CorreoLista.class);
                //q.select(from.get("email"));
                q.where(cb.equal(from.get("mailerLista"), mailerLista),
                        cb.equal(from.get("activo"), Boolean.TRUE));

                TypedQuery<CorreoLista> query = entityManager.createQuery(q);
                List<CorreoLista> lista = null;
                try {
                    lista = query.getResultList();
                } catch (NoResultException e) {
                    return Lists.newArrayList();
                }

                List<String> mails = new ArrayList<String>();
                for (CorreoLista c : lista) {
                    mails.add(c.getEmail());
                }
                return mails;
            }
        });

    }


    public void insertarCorreo(MailerLista mailerLista, String email) {

        List<CorreoLista> lista = doWithTransaction(new WithTransaction<List<CorreoLista>>() {
            @Override
            public List<CorreoLista> call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
                OpenJPACriteriaQuery<CorreoLista> q = cb.createQuery(CorreoLista.class);
                Root<CorreoLista> from = q.from(CorreoLista.class);
                q.where(cb.equal(from.get("email"), email),
                        cb.equal(from.get("mailerLista"), mailerLista));
                TypedQuery<CorreoLista> query = entityManager.createQuery(q);
                return query.getResultList();
            }
        });

        if (lista != null && !lista.isEmpty()) {
            LOGGER.warn("Se ha intentado colocar el correo {}  en la lista {} por segunda vez, ignorando", email,
                    mailerLista.getDescripcion());
            return;
        }

        CorreoLista nuevo = new CorreoLista();
        nuevo.setEmail(email);
        nuevo.setMailerLista(mailerLista);
        nuevo.setActivo(Boolean.TRUE);
        merge(nuevo);
        LOGGER.debug("Insertando el correo {} en la lista {}", email, mailerLista.getDescripcion());
    }


    public void eliminarCorreo(MailerLista mailerLista, String email) {

        List<CorreoLista> lista = doWithTransaction(new WithTransaction<List<CorreoLista>>() {
            @Override
            public List<CorreoLista> call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
                OpenJPACriteriaQuery<CorreoLista> q = cb.createQuery(CorreoLista.class);
                Root<CorreoLista> from = q.from(CorreoLista.class);
                q.where(cb.equal(from.get("email"), email),
                        cb.equal(from.get("mailerLista"), mailerLista));
                TypedQuery<CorreoLista> query = entityManager.createQuery(q);
                return query.getResultList();
            }
        });
        if (lista == null || lista.isEmpty()) {
            LOGGER.warn("Se ha intentado eliminar el correo inexistente {} de la lista {}, ignorando", email,
                    mailerLista.getDescripcion());
            return;
        }
        remove(lista.get(0).getId());
        LOGGER.debug("Eliminando el correo {} de la lista {}", email, mailerLista.getDescripcion());
    }


    public void desactivar(Long[] aLong) {
        for (Long id : aLong) {
            CorreoLista get = find(id);
            get.setActivo(Boolean.FALSE);
            merge(get);
        }
    }


    public void activar(Long[] aLong) {
        for (Long id : aLong) {
            CorreoLista get = find(id);
            get.setActivo(Boolean.TRUE);
            merge(get);
        }
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<CorreoLista> from, CorreoLista example, CorreoLista example2) {
        List<Predicate> predicates = new LinkedList<Predicate>();
        MailerLista tipo = example.getMailerLista();
        String correo = example.getEmail();
        if (tipo != null) {
            predicates.add(cb.equal(from.get("mailerLista"), tipo));
        }
        if (StringUtils.isNotEmpty(correo)) {
            predicates.add(cb.like(from.get("email"), correo));
        }
        return predicates.toArray(new Predicate[predicates.size()]); //super.createQBE(cb, from, example, example2);
    }

}
