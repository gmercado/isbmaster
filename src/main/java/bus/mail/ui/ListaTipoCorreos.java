package bus.mail.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.mail.model.MailerLista;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;
import com.bisa.bus.servicios.asfi.infocred.ui.SortableDataProviderSame;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by atenorio on 06/07/2017.
 */
@Menu(value = "Lista Correos", subMenu = SubMenu.ADMINISTRACION)
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN, RolesBisa.SOLO_LECTURA})
public class ListaTipoCorreos extends BisaWebPage {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ListaCorreos.class);

    public ListaTipoCorreos(){
        List<IColumn<MailerLista, String>> columns = new LinkedList<IColumn<MailerLista, String>>();
        columns.add(new LinkPropertyColumn<MailerLista>(new Model<String>("Descripci\u00F3n"), "descripcion") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onClick(MailerLista object) {
                setResponsePage(new ListaCorreos(object));
            }

            //@Override
            public boolean isActivo(IModel<MailerLista> model) {
                return true; // model.getObject().puedo(getUsuarioActual().getRol()) ||
                        //getUsuarioActual().getRol().contains("OPERADOR");
            }
        });
        columns.add(new PropertyColumn<MailerLista, String>(new Model<String>("Prefijo en el asunto"), "prefijo"));
        add(new DefaultDataTable("listas", columns, new MailersListaDataProvider(), 10));
    }

    private static class MailersListaDataProvider extends SortableDataProviderSame<MailerLista> {

        private static final long serialVersionUID = 1L;


        @Override
        public Iterator<? extends MailerLista> iterator(long first, long count) {
            return Arrays.asList(MailerLista.values()).iterator();
        }

        @Override
        public long size() {
            return MailerLista.values().length;
        }
    }
}
