package bus.mail.ui;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.inicio.utils.Titulo;
import bus.mail.dao.CorreoListaDao;
import bus.mail.entities.CorreoLista;
import bus.mail.model.MailerLista;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.AjaxCheckBoxColumn;
import bus.plumbing.components.IndicatingAjaxToolButton;
import bus.plumbing.components.ToolButton;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by atenorio on 06/07/2017.
 */
//@Menu(value = "Lista correos", subMenu = SubMenu.ADMINISTRACION)
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Titulo("Listado de correos")
public class ListaCorreos extends Listado<CorreoLista, Long> {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ListaCorreos.class);

    private final IModel<HashSet<CorreoLista>> seleccionados;

    MailerLista tipo;

    public ListaCorreos(MailerLista tipo) {
        super();
        seleccionados = new Model<>(new HashSet<>());
        this.tipo = tipo;
    }

    @Override
    protected ListadoPanel<CorreoLista, Long> newListadoPanel(String id) {
        return new ListadoPanel<CorreoLista, Long>(id) {
            @Override
            protected List<IColumn<CorreoLista, String>> newColumns(ListadoDataProvider<CorreoLista, Long> dataProvider) {
                List<IColumn<CorreoLista, String>> columns = new LinkedList<>();

                columns.add(new AjaxCheckBoxColumn<>(seleccionados, dataProvider));

                columns.add(new PropertyColumn<>(Model.of("ID"), "id", "id"));
                columns.add(new PropertyColumn<>(Model.of("Correo"), "email", "email"));

                columns.add(new PropertyColumn<CorreoLista, String>(Model.of("Tipo"), "mailerLista", "mailerLista") {
                    @Override
                    public void populateItem(Item<ICellPopulator<CorreoLista>> item, String componentId, IModel<CorreoLista> rowModel) {
                        CorreoLista correo = rowModel.getObject();
                        item.add(new Label(componentId, correo.getMailerLista()).add(new AttributeAppender("style", Model.of("text-align:center"))));
                    }
                });

                columns.add(new PropertyColumn<>(Model.of("Estado"), "activo", "activo"));
                return columns;
            }

            @Override
            protected Class<? extends Dao<CorreoLista, Long>> getProviderClazz() {
                return CorreoListaDao.class;
            }

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }

            @Override
            protected FiltroPanel<CorreoLista> newFiltroPanel(String id, IModel<CorreoLista> model1, IModel<CorreoLista> model2) {
                return new ListaCorreoFiltroPanel(id, model1, model2);
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("activo", false);
            }

            @Override
            protected CorreoLista newObject2() {
                CorreoLista correo = new CorreoLista();
                correo.setMailerLista(tipo);
                return correo;
            }

            @Override
            protected CorreoLista newObject1() {
                CorreoLista correo = new CorreoLista();
                correo.setMailerLista(tipo);
                return correo;
            }

            @Override
            protected boolean isVisibleData() {
                CorreoLista ftp1 = getModel1().getObject();
                CorreoLista ftp2 = getModel2().getObject();
                return true; //!((ftp1.getFechaCreacion() == null || ftp2.getFechaCreacion() == null));
            }
            @Override
            protected void poblarBotones(RepeatingView repeatingView, String idButton, Form<Void> listform) {
                WebMarkupContainer wmk;

                repeatingView.add(wmk = new WebMarkupContainer(repeatingView.newChildId()));
                wmk.add(new ToolButton(idButton) {
                    @Override
                    public void onSubmit() {
                        setResponsePage(ListaTipoCorreos.class);
                    }
                }.setClassAttribute("btn btn-primary")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Volver")));

                repeatingView.add(wmk = new WebMarkupContainer(repeatingView.newChildId()));
                wmk.add(new ToolButton(idButton) {
                    @Override
                    public void onSubmit() {
                        setResponsePage(new CrearCorreo(tipo));
                    }
                }.setClassAttribute("btn btn-info")
                        .setIconAttribute("icon-file icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Crear correo")));

                repeatingView.add(wmk = new WebMarkupContainer(repeatingView.newChildId()));
                wmk.add(new IndicatingAjaxToolButton(idButton, listform) {

                    @Override
                    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                        target.add(feedbackPanel);
                        target.add(table);
                        Set<CorreoLista> lista = seleccionados.getObject();
                        if (lista == null || lista.isEmpty()) {
                            getSession().warn("Nada por hacer");
                            return;
                        }
                        CorreoListaDao dao = getInstance(CorreoListaDao.class);
                        int eliminados = 0;
                        int errados = 0;
                        for (CorreoLista correo: lista) {
                            try {
                                dao.remove(correo.getId());
                                eliminados++;
                            } catch (Exception e) {
                                LOGGER.error("Ha ocurrido un error al eliminar a un correo" + correo, e);
                                errados++;
                            }
                        }
                        if (errados != 0) {
                            getSession().error("Algunos (" + errados + " de " +
                                    (eliminados + errados) +
                                    ") registros de corroe no pudieron eliminarse. Consule a soporte t\u00e9cnico.");
                        } else {
                            getSession().info("Operaci\u00f3n realizada correctamente");
                        }
                        seleccionados.setObject(new HashSet<>());
                    }

                    @Override
                    protected void onError(AjaxRequestTarget target, Form<?> form) {
                        target.add(feedbackPanel);
                    }
                }.setClassAttribute("btn btn-danger")
                        .setIconAttribute("icon-remove icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Eliminar correo seleccionados")));
            }

        };
    }

}
