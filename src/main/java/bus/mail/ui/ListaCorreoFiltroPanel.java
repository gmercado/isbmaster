package bus.mail.ui;

import bus.database.components.FiltroPanel;
import bus.mail.entities.CorreoLista;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

/**
 * Created by atenorio on 08/05/2017.
 */
public class ListaCorreoFiltroPanel extends FiltroPanel<CorreoLista> {

    protected ListaCorreoFiltroPanel(String id, IModel<CorreoLista> model1, IModel<CorreoLista> model2) {
        super(id, model1, model2);


        add(new TextField<>("email", new PropertyModel<>(model1, "email")));

    }
}
