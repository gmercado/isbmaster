package bus.mail.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.mail.dao.CorreoListaDao;
import bus.mail.model.MailerLista;
import bus.plumbing.api.RolesBisa;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.validator.EmailAddressValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by atenorio on 06/07/2017.
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN, RolesBisa.SOLO_LECTURA})
@Titulo("Crear correo")
public class CrearCorreo extends BisaWebPage {

    protected static final Logger LOGGER = LoggerFactory.getLogger(CrearCorreo.class);


    private String correo;


    public CrearCorreo(final MailerLista lista) {
        Form<?> form;
        add(form = new Form<Void>("crear") {

            private static final long serialVersionUID = 1L;

            @Override
            protected void onSubmit() {
                try {
                    CorreoListaDao correoListaDao = getInstance(CorreoListaDao.class);
                    correoListaDao.insertarCorreo(lista, correo);
                    setResponsePage(new ListaCorreos(lista));
                    info("Dirección adicionada correctamente");
                } catch (Exception e) {
                    LOGGER.error("Error al insertar en una lista", e);
                    error("Ha ocurrido un error inesperado, comuníquese con soporte técnico");
                }
            }
        });

        TextField<String> tf;
        form.add(tf = new RequiredTextField<String>("email", new PropertyModel<String>(this, "correo")));

        tf.add(EmailAddressValidator.getInstance());
        //tf.add(EmailAddressValidator.maximumLength(255));

        form.add(new Button("cancelar", Model.of("Cancelar")) {

            private static final long serialVersionUID = 1242248008350765173L;

            @Override
            public void onSubmit() {
                setResponsePage(new ListaCorreos(lista));
                getSession().info("Operaci\u00f3n cancelada, no se ha creado un nuevo registro de usuario");
            }
        }.setDefaultFormProcessing(false));
    }
}
