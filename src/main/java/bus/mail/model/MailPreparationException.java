package bus.mail.model;

/**
 * Created by rchura on 29-01-16.
 */
public class MailPreparationException  extends Exception {
    /**
     * Constructor for MailPreparationException.
     * @param msg the detail message
     */
    public MailPreparationException(String msg) {
        super(msg);
    }

    /**
     * Constructor for MailPreparationException.
     * @param msg the detail message
     * @param cause the root cause from the mail API in use
     */
    public MailPreparationException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public MailPreparationException(Throwable cause) {
        super("Could not prepare mail", cause);
    }
}
