/*
 *  Copyright 2009 Banco Bisa S.A.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.mail.model;

import org.apache.commons.mail.EmailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.Executor;

/**
 * @author Marcelo Morales
 * @author Ricardo Salvatierra on 21/12/2017
 */
public final class Mailer {

    private static final Logger LOGGER = LoggerFactory.getLogger(Mailer.class);
    private static final String SMTP = "smtp";
    private static final String UTF_8 = "UTF-8";
    private MailerAttachments mailSender;
    private String from;
    private Executor executor;
    private ObtenerTo obtenerTo;

    public Mailer(MailerAttachments mailSender, String from, Executor executor, ObtenerTo obtenerTo) {
        this.mailSender = mailSender;
        this.from = from;
        this.executor = executor;
        this.obtenerTo = obtenerTo;
    }

    private MimeMessage createMimeMessage() throws MailPreparationException {
        try {
            String[] to = obtenerTo.getTo();
            if (to.length == 0) {
                LOGGER.error("Alguien esta enviando correos usando Mailer pero no usa destinatarios", new Throwable());
                return mailSender.createMimeMessage();
            }
            MimeMessage createMimeMessage = mailSender.createMimeMessage();
            createMimeMessage.setFrom(new InternetAddress(from, from, UTF_8));
            InternetAddress[] addresses = new InternetAddress[to.length];
            for (int i = 0; i < to.length; i++) {
                String add = to[i];
                addresses[i] = new InternetAddress(add, add, UTF_8);
            }
            createMimeMessage.addRecipients(Message.RecipientType.TO, addresses);
            return createMimeMessage;
        } catch (MessagingException | UnsupportedEncodingException ex) {
            throw new MailPreparationException(ex);
        }
    }

    public void mailAndForgetTempFileExample(final String subject, final String textoCuerpo,
                                             final File archivo1, final File archivo2) {
        LOGGER.debug("Ingresando 1");
        // Se compone la parte del texto
        BodyPart texto = new MimeBodyPart();
        try {
            texto.setText(textoCuerpo);
        } catch (MessagingException e) {
            LOGGER.error("error ", e);
        }

        LOGGER.debug("Creando adjunto del archivo");
        BodyPart adjunto1 = getBodyPart(archivo1);
        BodyPart adjunto2 = null;
        if (archivo2 != null) {
            adjunto2 = getBodyPart(archivo2);
        }
        LOGGER.debug("creando  MimeMultipart");
        // Una MultiParte para agrupar texto e imagen.
        MimeMultipart multiParte = new MimeMultipart();
        try {
            multiParte.addBodyPart(texto);
            multiParte.addBodyPart(adjunto1);
            if (adjunto2 != null) {
                multiParte.addBodyPart(adjunto2);
            }
        } catch (MessagingException e) {
            LOGGER.error("error ", e);
        }

        LOGGER.debug("creando  MimeMessage");
        // Se compone el correo, dando to, from, subject y el
        // contenido.
        MimeMessage message = null;
        try {
            message = createMimeMessage();
            message.setSubject(subject);
            message.setContent(multiParte);
        } catch (MessagingException | MailPreparationException e) {
            LOGGER.error("error ", e);
        }

        LOGGER.debug("creando  Transport");

        // Se envia el correo.
        Transport t;
        try {
            t = mailSender.getMailSession().getTransport(SMTP);
            t.connect(from, null);
            if (message != null) {
                t.sendMessage(message, message.getAllRecipients());
            }
            t.close();

        } catch (EmailException | MessagingException e) {
            LOGGER.error("error ", e);
        }
        LOGGER.debug("Correo enviado correctamente....");
    }

    private BodyPart getBodyPart(File archivo) {
        BodyPart adjunto1 = new MimeBodyPart();
        try {
            if (archivo != null) {
                adjunto1.setDataHandler(new DataHandler(new FileDataSource(archivo)));
                adjunto1.setFileName(archivo.getName());
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return adjunto1;
    }


    public void mailAndForget(String subject, String template, Object[] datos) {
        // TODO: hacer esto!
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void mail(String subject, String template, Object[] datos) {
        // TODO: hacer
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void mailAndForget(final String subject, final String textoCuerpo) {

        LOGGER.debug("Ingresando 1");
        // Se compone la parte del texto
        BodyPart texto = new MimeBodyPart();
        try {
            texto.setText(textoCuerpo);
        } catch (MessagingException e) {
            LOGGER.error("MessagingException:", e);
        }

        LOGGER.debug("creando  MimeMultipart");
        // Una MultiParte para agrupar texto e imagen.
        MimeMultipart multiParte = new MimeMultipart();
        try {
            multiParte.addBodyPart(texto);
        } catch (MessagingException e) {
            LOGGER.error("MessagingException:", e);
        }

        LOGGER.debug("creando  MimeMessage");
        // Se compone el correo, dando to, from, subject y el
        // contenido.
        MimeMessage message = null;
        try {
            message = createMimeMessage();
            message.setSubject(subject);
            message.setContent(multiParte);
        } catch (MailPreparationException e) {
            LOGGER.error("MailPreparationException:", e);
        } catch (MessagingException ex) {
            LOGGER.error("MessagingException:", ex);
        }

        LOGGER.debug("creando  Transport");
        // Se envia el correo.
        Transport t;
        try {
            t = mailSender.getMailSession().getTransport(SMTP);
            t.connect(from, null);
            if (message != null) {
                t.sendMessage(message, message.getAllRecipients());
            }
            t.close();

        } catch (NoSuchProviderException ex) {
            LOGGER.error("NoSuchProviderException:", ex);
        } catch (EmailException ex) {
            LOGGER.error("EmailException:", ex);
        } catch (MessagingException ex) {
            LOGGER.error("MessagingException:", ex);
        }
        LOGGER.debug("Correo enviado correctamente....");
    }


    public void mailAndForget(final MimeMessage mimeMessage) {
        executor.execute(() -> {
            try {
                mailSender.send();
            } catch (Exception e) {
                LOGGER.warn("No se pudo enviar un correo MIME", e);
            }
        });
    }
}
