package bus.mail.model;

import bus.env.api.MedioAmbiente;
import com.google.inject.Inject;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.Properties;

import static bus.env.api.Variables.*;


/**
 * Created by rchura on 29-01-16.
 */
public class MailerAttachments extends Email {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailerAttachments.class);

    private final MedioAmbiente medioAmbiente;

    @Inject
    public MailerAttachments(MedioAmbiente medioAmbiente1) throws EmailException {
        //super();
        this.medioAmbiente = medioAmbiente1;
        setHostName(medioAmbiente.getValorDe(MAIL_HOST, MAIL_HOST_DEFAULT));
        setFrom(medioAmbiente.getValorDe(MAIL_FROM, MAIL_FROM_DEFAULT));
        setSmtpPort(medioAmbiente.getValorIntDe(MAIL_PORT, MAIL_PORT_DEFAULT));

        Session session = Session.getDefaultInstance(propertiesMailer());
        setMailSession(session);
    }

    @Override
    public Email setMsg(String msg) throws EmailException {
        if (isEmpty(msg))
        {
            throw new EmailException("Invalid message supplied");
        }

        setContent(msg, ATTACHMENTS);
        return this;
    }

    private static boolean isEmpty(String str)
    {
        return (str == null) || (str.length() == 0);
    }

    public MimeMessage createMimeMessage()
    {
        Session aSession = null;
        try {
            aSession = getMailSession();
        } catch (EmailException e) {
            e.printStackTrace();
        }

        return super.createMimeMessage(aSession);
    }

    public MimeMessage getMimeMessage(final String subject, final String textoCuerpo, final File archivo){

        // Se compone la parte del texto
        BodyPart texto = new MimeBodyPart();
        try {
            texto.setText("Texto del mensaje");
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        // Se compone el adjunto con la imagen
        BodyPart adjunto = new MimeBodyPart();
        try {
            adjunto.setDataHandler(new DataHandler(new FileDataSource(archivo)));
            adjunto.setFileName(archivo.getName());
        } catch (MessagingException e) {
            e.printStackTrace();
        }


        // Una MultiParte para agrupar texto e imagen.
        MimeMultipart multiParte = new MimeMultipart();
        try {
            multiParte.addBodyPart(texto);
            multiParte.addBodyPart(adjunto);
        } catch (MessagingException e) {
            e.printStackTrace();
        }


        // Se compone el correo, dando to, from, subject y el
        // contenido.
        MimeMessage message = createMimeMessage();
        try {
            message.setFrom(new InternetAddress("yo@yo.com"));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress("chuidiang@gmail.com"));
            message.setSubject("Hola");
            message.setContent(multiParte);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return message;

    }



    private Properties propertiesMailer() {

        String host = medioAmbiente.getValorDe(MAIL_HOST, MAIL_HOST_DEFAULT);
        String from = medioAmbiente.getValorDe(MAIL_FROM, MAIL_FROM_DEFAULT);
        int port = medioAmbiente.getValorIntDe(MAIL_PORT, MAIL_PORT_DEFAULT);

        Properties props = new Properties();
        props.put("mail.smtp.host",  host);
        props.setProperty("mail.smtp.starttls.enable", "fasle"); //true
        props.setProperty("mail.smtp.port", ""+port);
        props.setProperty("mail.smtp.user", from); //correo
        props.setProperty("mail.smtp.auth", "false"); //true

        return props;
    }


}
