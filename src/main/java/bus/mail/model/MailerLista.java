/*
 *  Copyright 2009 Banco Bisa S.A.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.mail.model;

import bus.plumbing.api.Roles;

import java.util.Arrays;
import java.util.Collection;

/**
 *
 * @author Marcelo Morales 
 */
public enum MailerLista {

    ADMINISTRADORES("Todos los clientes de servicios web", new Roles[]{Roles.ADMCLIEN}, "[Admin]"),
    BOA_RESPONSABLES("Notificaci\u00F3n a responsables de BoA", new Roles[] {Roles.OPERADOR}, "[BoA]"),
    COMUNICACIONES("Errores de comunicaciones", new Roles[] {Roles.ENVIRON, Roles.SCHED}, "[Comunicaciones]"),
    SOPORTE("Soporte t\u00E9cnico", new Roles[] {Roles.ENVIRON, Roles.SCHED}, "[Soporte]"),
    OPERADORES("Operadores", new Roles[] {Roles.ENVIRON, Roles.SCHED}, "[Operador]"),
    TARJETA_CREDITO("Responsables Tarjetas de Cr\u00E9dito", new Roles[] {Roles.ENVIRON, Roles.SCHED}, "[TarjetaCredito]")
    ;

    private String descripcion;

    private Roles[] rolesAceptados;

    private String prefijo;

    private MailerLista(String descripcion, Roles[] rolesAceptados, String prefijo) {
        this.descripcion = descripcion;
        Arrays.sort(rolesAceptados);
        this.rolesAceptados = rolesAceptados;
        this.prefijo = prefijo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public boolean puedo(Collection<String> roles) {
        for (String rol : roles) {
            if (puedo(rol)) {
                return true;
            }
        }
        return false;
    }

    public boolean puedo(Roles[] roles) {
        for (Roles ractual : roles) {
            if (puedo(ractual)) {
                return true;
            }
        }
        return false;
    }

    public boolean puedo(Roles roles) {
        return Arrays.binarySearch(rolesAceptados, roles) != -1;
    }

    public boolean puedo(String role) {
        return puedo(Roles.valueOf(role));
    }

    public String getPrefijo() {
        return prefijo;
    }

}
