package bus;

import bus.cache.CacheModule;
import bus.config.ConfigFileModule;
import bus.consumoweb.ConsumidorModule;
import bus.database.DatabaseAuxiliarModule;
import bus.database.DatabaseModule;
import bus.database.datasource.*;
import bus.database.jpa.*;
import bus.database.model.*;
import bus.dataqueue.DataqueueModule;
import bus.env.EnvModule;
import bus.inicio.InicioModule;
import bus.logging.LoggingModule;
import bus.mail.MailModule;
import bus.monitor.MonitorModule;
import bus.plumbing.wicket.WicketModule;
import bus.sched.SchedModule;
import com.bisa.bus.servicios.tc.sftp.SftpModule;
import bus.sign.SignModule;
import bus.users.UsersModule;
import com.bisa.bus.servicios.ach.AchModule;
import com.bisa.bus.servicios.asfi.infocred.InfocredModule;
import com.bisa.bus.servicios.chatbot.ChatBotModule;
import com.bisa.bus.servicios.bisa.ui.MesaAyudaModule;
import com.bisa.bus.servicios.mojix.MojixModule;
import com.bisa.bus.servicios.segip.SegipModule;
import com.bisa.bus.servicios.sin.ciclos.FacturacionCiclosModule;
import com.bisa.bus.servicios.skyb.boa.BoAModule;
import com.bisa.bus.servicios.tc.sftp.SftpModule;
import com.bisa.indicadoresbcb.IndicadoresBCBModule;
import com.bisa.isb.ws.WebServiceModule;
import com.bisa.isb.ws.security.WSSecurityModule;
import com.bisa.monitoreo.JobMonitorModule;
import com.bisa.movil.SincAutorizacionModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

/**
 * @author Marcelo Morales
 * @since 4/27/12
 */
public class GuiceServletConfig extends GuiceServletContextListener {

    private static final String CONFIGURATION_FILE = "isb.properties";

    @Override
    protected Injector getInjector() {
        return Guice.createInjector(
                new ConfigFileModule(CONFIGURATION_FILE),
                new CacheModule(),
                new DatabaseModule(),
                new DatabaseAuxiliarModule(BaseAuxiliar.class, DataSourceAuxiliarProvider.class, "auxiliar", EntityManagerFactoryServiceAuxiliar.class),
                new DatabaseAuxiliarModule(BaseAuxiliar2.class, DataSourceAuxiliar2Provider.class, "auxiliar2", EntityManagerFactoryServiceAuxiliar2.class),
                new DatabaseAuxiliarModule(BaseAuxiliar3.class, DataSourceAuxiliar3Provider.class, "auxiliar3", EntityManagerFactoryServiceAuxiliar3.class),
                new DatabaseAuxiliarModule(BaseStadistic.class, DataSourceSqlStadisticProvider.class, "stadistic", EntityManagerFactoryServiceStadistic.class),
                new DatabaseAuxiliarModule(BaseContract.class, DataSourceSqlContractProvider.class, "contract", EntityManagerFactoryServiceContract.class),
                new EnvModule(),
                new ConsumidorModule(),
                new SchedModule(),
                new WicketModule(),
                new InicioModule(),
                new UsersModule(),
                new LoggingModule(),
                new MonitorModule(),
                //new EjemploModule(),
                //new HallazgoModule(),
                new WebServiceModule(),
                new MailModule(),
                //new InterceptorsModule(),
                new WSSecurityModule(),
                new BoAModule(),
                new DataqueueModule(),
                new SegipModule(),
                new SignModule(),
                new FacturacionCiclosModule(),
                new IndicadoresBCBModule(),
                new InfocredModule(),
                new MojixModule(),
                new SftpModule(),
                new JobMonitorModule(),
                new ChatBotModule(),
                new MesaAyudaModule(),
                new SincAutorizacionModule(),
                new AchModule()
        );
    }

}
