package bus.plumbing.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by rchura on 27-01-16.
 */
public class ArchivoProfile {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArchivoProfile.class);

    /**
     * Archivo
     */
    private File archivo;
    /**
     * cantidad total de caracteres
     */
    private long sumaChar;
    /**
     * numero de filas
     */
    private long nroFilas;
    /**
     * MD5
     */
    private String md5;
    /**
     * archivo con o sin contenido
     */
    private boolean tieneContenido;


    public ArchivoProfile(File archivo, long sumaChar, long nroFilas) {
        this.archivo = archivo;
        this.sumaChar = sumaChar;
        this.nroFilas = nroFilas;
        this.tieneContenido = nroFilas > 0;

        if (tieneContenido ) {
            String valorMd5 = sumaChar + "BISA" + nroFilas;
            this.md5 = obtenerMD5(valorMd5);
            Object[] obj = {archivo.getName(), valorMd5, this.md5};
            LOGGER.debug("Archivo[{}],val[{}],md5[{}]", obj);
        } else {
            LOGGER.debug("Archivo sin contenido[{}]", archivo.getName());
        }
    }


    private String obtenerMD5(String cadena) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(cadena.getBytes());
            byte result[] = m.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < result.length; i++) {
                String s = Integer.toHexString(result[i]);
                int length = s.length();
                if (length >= 2) {
                    sb.append(s.substring(length - 2, length));
                } else {
                    sb.append("0");
                    sb.append(s);
                }
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            LOGGER.warn("No encontramos el algoritmo MD5.", e);
        } catch (Exception e) {
            LOGGER.warn("Error general.", e);
        }
        return "-ERROR-";
    }

    public File getArchivo() {
        return archivo;
    }

    public long getSumaChar() {
        return sumaChar;
    }

    public long getNroFilas() {
        return nroFilas;
    }

    public String getMd5() {
        return md5;
    }

    public boolean isTieneContenido() {
        return tieneContenido;
    }
}
