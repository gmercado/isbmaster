package bus.plumbing.file;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.IFSFileOutputStream;

import java.io.IOException;

/**
 * @author rsalvatierra on 04/04/2016.
 * @see http://www.ibm.com/developerworks/ibmi/library/i-ifs/
 */
public class ArchivoIFS {

    /**
     Writes an integrated file system file.
     @param system The system that contains the file.
     @param path The absolute path name of the file.
     @param bytes The bytes to be written.
     @throws IOException
     @throws AS400SecurityException
     **/
    public static void writeFile(AS400 system, String path, byte[] bytes) throws
            AS400SecurityException, IOException {
        IFSFile file = new IFSFile(system, path);
        /** creates a file output stream */
        IFSFileOutputStream fos = new IFSFileOutputStream (file);
        /** Writes bytes to file */
        fos.write(bytes);
        fos.close();
    }
}
