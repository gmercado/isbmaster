package bus.plumbing.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author rsalvatierra on 18/03/2016.
 */
public class FileTemp {

    public static File create(String nombreArchivo, String extension, byte[] stream) {
        File file = null;
        FileOutputStream fileOuputStream = null;
        try {
            file = File.createTempFile(nombreArchivo, extension);
            fileOuputStream = new FileOutputStream(file);
            fileOuputStream.write(stream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileOuputStream != null) {
                    fileOuputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }
}
