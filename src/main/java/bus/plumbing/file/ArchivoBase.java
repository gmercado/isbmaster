package bus.plumbing.file;

import bus.env.api.MedioAmbiente;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

import static bus.env.api.Variables.TEMPORAL_FILE;
import static bus.env.api.Variables.TEMPORAL_FILE_DEFAULT;

/**
 * Created by rchura on 26-01-16.
 */
public class ArchivoBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArchivoBase.class);

    private static final DateTimeFormatterBuilder dtfb = new DateTimeFormatterBuilder().appendYear(4, 4).
            appendMonthOfYear(2).
            appendLiteral("-semana-del-").
            appendDayOfMonth(2);

    private final MedioAmbiente medioAmbiente;

    private String pathHome;

    @Inject
    public ArchivoBase(MedioAmbiente medioAmbiente) {
        this.medioAmbiente = medioAmbiente;
    }

    /**
     * El parametro filename incluye nombre y extension
     * @param filename
     * @return
     * @throws IOException
     */
    public final File crearArchivoPorSemana(String filename) throws IOException {

        String path = getPathHome();
        File pathFile = new File(path);
        if (!pathFile.exists()) {
            if (!pathFile.mkdirs()) {
                LOGGER.warn("No se puede crear el directorio base [{}].", pathFile);
            }
        }
        if (!pathFile.isDirectory()) {
            throw new IllegalStateException("Path " + path + " no es un directorio.");
        }
        DateTime dt = new DateTime().withDayOfWeek(1);
        pathFile = new File(pathFile, dtfb.toFormatter().print(dt));
        if (!pathFile.exists()) {
            boolean created = pathFile.mkdirs();
            if (!created) {
                throw new IllegalStateException("No se ha podido crear el directorio " + pathFile.getAbsolutePath());
            }
        }
        if (!pathFile.isDirectory()) {
            throw new IllegalStateException("Path " + pathFile.getAbsolutePath() + " no es un directorio.");
        }
        if (StringUtils.isEmpty(StringUtils.trimToEmpty(filename))){
            throw new IllegalStateException("FileName [" + filename + "] no es un nombre valido.");
        }
        LOGGER.debug("Creando un nuevo archivo en la ruta {} para el filename {}.", path, filename);
        LOGGER.trace("Trace de la creacion del nuevo archivo.", new Throwable());
        return new File(pathFile, filename);
    }


    public final File crearArchivoSimple(String filename) throws IOException {
        String path = getPathHome();
        File pathFile = new File(path);
        if (!pathFile.exists()) {
            if (!pathFile.mkdirs()) {
                LOGGER.warn("No se puede crear el directorio base [{}].", pathFile);
            }
        }
        if (!pathFile.isDirectory()) {
            throw new IllegalStateException("Path " + path + " no es un directorio.");
        }
        DateTime dt = new DateTime().withDayOfWeek(1);
        pathFile = new File(pathFile, dtfb.toFormatter().print(dt));
        if (!pathFile.exists()) {
            boolean created = pathFile.mkdirs();
            if (!created) {
                throw new IllegalStateException("No se ha podido crear el directorio " + pathFile.getAbsolutePath());
            }
        }
        if (!pathFile.isDirectory()) {
            throw new IllegalStateException("Path " + pathFile.getAbsolutePath() + " no es un directorio.");
        }
        if (StringUtils.isEmpty(StringUtils.trimToEmpty(filename))){
            throw new IllegalStateException("FileName [" + filename + "] no es un nombre valido.");
        }
        LOGGER.debug("Creando un nuevo archivo en la ruta {} para el filename {}.", path, filename);
        LOGGER.trace("Trace de la creacion del nuevo archivo.", new Throwable());
        return new File(pathFile, filename);
    }


    public String getPathHome() {
        if(StringUtils.trimToNull(pathHome)==null){
            // Directorio por defecto
            pathHome = medioAmbiente.getValorDe(TEMPORAL_FILE, TEMPORAL_FILE_DEFAULT);
        }
        return pathHome;
    }

    public void setPathHome(String pathHome) {
        this.pathHome = pathHome;
    }

}
