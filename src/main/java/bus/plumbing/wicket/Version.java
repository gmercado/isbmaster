/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.plumbing.wicket;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.wicket.protocol.http.WicketFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

/**
 * @author Marcelo Morales
 *         Created: 5/15/12 9:10 AM
 */
@Singleton
public class Version {

    public static final String MANIFEST = "/META-INF/MANIFEST.MF";
    public static final String IMPLEMENTATION_VERSION = "Implementation-Version";
    public static final String BUILT_WHEN = "Built-When";
    public static final String BUILT_BY = "Built-By";
    public static final String IMPLEMENTATION_BUILD = "Implementation-Build";
    public static final String BUILD_JDK = "Build-Jdk";
    public static final String IMPLEMENTATION_TITLE = "Implementation-Title";
    private static final Logger LOGGER = LoggerFactory.getLogger(Version.class);
    private String versionPom;
    private String built;
    private String construidoPor;
    private String versionMercurial;
    private String jdk;
    private String producto;

    @Inject
    public Version(WicketFilter wicketFilter) {
        final Attributes attributes = getAttributes(wicketFilter);
        if (attributes != null) {
            versionPom = attributes.getValue(IMPLEMENTATION_VERSION);
            built = attributes.getValue(BUILT_WHEN);
            construidoPor = attributes.getValue(BUILT_BY);
            versionMercurial = attributes.getValue(IMPLEMENTATION_BUILD);
            jdk = attributes.getValue(BUILD_JDK);
            producto = attributes.getValue(IMPLEMENTATION_TITLE);
        }
    }

    private Attributes getAttributes(WicketFilter wicketFilter) {
        Attributes attributes = null;
        try {
            ServletContext servletContext = wicketFilter.getFilterConfig().getServletContext();
            InputStream inputStream = servletContext.getResourceAsStream(MANIFEST);
            if (inputStream != null) {
                Manifest manifest = new Manifest(inputStream);
                attributes = manifest.getMainAttributes();
            }
        } catch (Exception e) {
            LOGGER.error("No se puede encontrar la version", e);
        }
        return attributes;
    }

    public String getVersionPom() {
        return versionPom;
    }

    public String getBuilt() {
        return built;
    }

    public String getConstruidoPor() {
        return construidoPor;
    }

    public String getVersionMercurial() {
        return versionMercurial;
    }

    public String getJdk() {
        return jdk;
    }

    public String getProducto() {
        return producto;
    }
}
