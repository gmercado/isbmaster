package bus.plumbing.wicket;

import bus.plumbing.utils.PerfFilter;
import com.google.inject.Injector;
import com.google.inject.servlet.ServletModule;
import org.apache.wicket.Application;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.guice.GuiceWebApplicationFactory;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.protocol.http.WicketFilter;

import java.util.HashMap;
import java.util.Map;

import static com.google.inject.Scopes.SINGLETON;

/**
* @author Marcelo Morales
* @since 4/30/12
*/
public final class WicketModule extends ServletModule {

    @Override
    protected void configureServlets() {
        bind(PerfFilter.class).asEagerSingleton();
        filter("/*").through(PerfFilter.class);

        Map<String, String> initParams = new HashMap<>();

        bind(WicketFilter.class).in(SINGLETON);
        bind(WebApplication.class).to(BisaWebApplication.class).asEagerSingleton();
        initParams.put(WicketFilter.APP_FACT_PARAM, GuiceWebApplicationFactory.class.getName());
        initParams.put(WicketFilter.FILTER_MAPPING_PARAM, "/1/*");
        initParams.put(Application.CONFIGURATION, RuntimeConfigurationType.DEPLOYMENT.name());
        initParams.put("injectorContextAttribute", Injector.class.getName());

        filter("/1/*").through(WicketFilter.class, initParams);
    }
}
