/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.plumbing.wicket;

import com.google.inject.*;
import com.google.inject.spi.TypeConverterBinding;
import org.apache.wicket.*;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.core.request.mapper.CryptoMapper;
import org.apache.wicket.core.util.crypt.KeyInSessionSunJceCryptFactory;
import org.apache.wicket.guice.GuiceComponentInjector;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.util.convert.converter.AbstractConverter;
import org.apache.wicket.util.lang.Bytes;
import org.apache.wicket.util.string.Strings;
import org.mvp.labs.HomePage;

import java.lang.annotation.Annotation;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParsePosition;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * @author Marcelo Morales
 * @since 1/4/11
 */
public class BisaWebApplication extends AuthenticatedWebApplication implements Injector {

    private final Injector injector;

    private final Class<? extends WebPage> ingresoClass;

    private final Class<? extends WebPage> homePageClass;

    private final Class<? extends AuthenticatedWebSession> sessionClass;

    @SuppressWarnings("unchecked")
    @Inject
    public BisaWebApplication(Injector injector,
                              @PaginaIngresoClass Class ingresoClass,
                              @PaginaPrincipalClass Class homePageClass,
                              Class<? extends AuthenticatedWebSession> sessionClass) {
        this.injector = injector;
        this.ingresoClass = ingresoClass;
        this.homePageClass = homePageClass;
        this.sessionClass = sessionClass;
    }

    @Override
    protected void init() {
        super.init();

        getApplicationSettings().setAccessDeniedPage(ingresoClass);
        getApplicationSettings().setPageExpiredErrorPage(ingresoClass);
        // TODO: getApplicationSettings().setInternalErrorPage(ErrorInternoPage.class);
        getApplicationSettings().setDefaultMaximumUploadSize(Bytes.bytes(100)); // no hacemos upload

        getMarkupSettings().setStripWicketTags(true);
        getMarkupSettings().setStripComments(true);
        getMarkupSettings().setCompressWhitespace(true);

        getSecuritySettings().setCryptFactory(new KeyInSessionSunJceCryptFactory());
        setRootRequestMapper(new CryptoMapper(new SystemMapper(this), this));

        getComponentInstantiationListeners().add(new GuiceComponentInjector(this, injector));

        mountPage("/wicket", HomePage.class);
    }

    public static BisaWebApplication get() {
        return (BisaWebApplication) Application.get();
    }

    @Override
    protected Class<? extends AuthenticatedWebSession> getWebSessionClass() {
        return sessionClass;
    }

    @Override
    public Class<? extends WebPage> getSignInPageClass() {
        return ingresoClass;
    }

    @Override
    public Class<? extends Page> getHomePage() {
        return homePageClass;
    }

    @Override
    public <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> typeLiteral) {
        return injector.getMembersInjector(typeLiteral);
    }

    @Override
    public <T> MembersInjector<T> getMembersInjector(Class<T> type) {
        return injector.getMembersInjector(type);
    }

    @Override
    public Map<Key<?>, Binding<?>> getBindings() {
        return injector.getBindings();
    }

    @Override
    public Map<Key<?>, Binding<?>> getAllBindings() {
        return injector.getAllBindings();
    }

    @Override
    public <T> Binding<T> getBinding(Key<T> key) {
        return injector.getBinding(key);
    }

    @Override
    public <T> Binding<T> getBinding(Class<T> type) {
        return injector.getBinding(type);
    }

    @Override
    public <T> Binding<T> getExistingBinding(Key<T> key) {
        return injector.getExistingBinding(key);
    }

    @Override
    public <T> List<Binding<T>> findBindingsByType(TypeLiteral<T> type) {
        return injector.findBindingsByType(type);
    }

    @Override
    public <T> Provider<T> getProvider(Key<T> key) {
        return injector.getProvider(key);
    }

    @Override
    public <T> Provider<T> getProvider(Class<T> type) {
        return injector.getProvider(type);
    }

    @Override
    public <T> T getInstance(Key<T> key) {
        return injector.getInstance(key);
    }

    @Override
    public <T> T getInstance(Class<T> type) {
        return injector.getInstance(type);
    }

    @Override
    public Injector getParent() {
        return injector.getParent();
    }

    @Override
    public Injector createChildInjector(Iterable<? extends Module> modules) {
        return injector.createChildInjector(modules);
    }

    @Override
    public Injector createChildInjector(Module... modules) {
        return injector.createChildInjector(modules);
    }

    @Override
    public Map<Class<? extends Annotation>, Scope> getScopeBindings() {
        return injector.getScopeBindings();
    }

    @Override
    public Set<TypeConverterBinding> getTypeConverterBindings() {
        return injector.getTypeConverterBindings();
    }

    @Override
    public void injectMembers(Object instance) {
        injector.injectMembers(instance);
    }


    @Override
    protected IConverterLocator newConverterLocator() {
        ConverterLocator iConverterLocator = new ConverterLocator();
        iConverterLocator.set(BigDecimal.class, new AbstractConverter<BigDecimal>() {

            private static final long serialVersionUID = -4728350803987407823L;


            @Override
            public BigDecimal convertToObject(String value, Locale locale) {
                if (Strings.isEmpty(value)) {
                    return null;
                }

                final DecimalFormat numberFormat = n();

                if (!value.matches("^(((\\d{1,3})(,\\d{3})*)|(\\d+))(\\.\\d{1,2})?$")) {
                    throw newConversionException("Cannot parse '" + value + "' using format " + numberFormat,
                            value, locale).setFormat(numberFormat);
                }

                final ParsePosition position = new ParsePosition(0);

                final BigDecimal number = (BigDecimal) numberFormat.parse(value, position);

                if (position.getIndex() != value.length()) {
                    throw newConversionException("Cannot parse '" + value + "' using format " + numberFormat,
                            value, locale).setFormat(numberFormat);
                }

                if (number.scale() > numberFormat.getMaximumFractionDigits()) {
                    throw newConversionException("Cannot parse '" + value + "' using format " + numberFormat,
                            value, locale).setFormat(numberFormat);
                }

                return number;
            }

            private DecimalFormat n() {
                final DecimalFormat numberFormat = new DecimalFormat();
                numberFormat.setMaximumFractionDigits(2);
                DecimalFormatSymbols newSymbols = new DecimalFormatSymbols();
                newSymbols.setDecimalSeparator('.');
                newSymbols.setGroupingSeparator(',');
                numberFormat.setDecimalFormatSymbols(newSymbols);
                numberFormat.setParseBigDecimal(true);
                return numberFormat;
            }

            @Override
            public String convertToString(BigDecimal value, Locale locale) {
                if (value == null) {
                    return null;
                }
                return n().format(value);
            }

            @Override
            protected Class<BigDecimal> getTargetType() {
                return BigDecimal.class;
            }
        });
        return iConverterLocator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Session newSession(final Request request, final Response response) {
        try {
            return sessionClass.getDeclaredConstructor(Request.class).newInstance(request);
        } catch (Exception e) {
            throw new WicketRuntimeException("No puedo usar" + sessionClass, e);
        }
    }
}
