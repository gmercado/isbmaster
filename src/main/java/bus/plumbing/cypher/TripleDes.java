package bus.plumbing.cypher;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;

/**
 * @author by rchura on 11-12-15.
 * @author by rsalvatierra on 27-09-17.
 */
public class TripleDes {

    private static final Logger LOGGER = LoggerFactory.getLogger(TripleDes.class);
    private static final int PADDING_BLOCK = 8;
    private final String ALGO_NAME;
    private final String SHARED_KEY;


    public TripleDes(String nombreAlgoritmo, String llaveEncriptacion) {
        this.ALGO_NAME = nombreAlgoritmo;
        this.SHARED_KEY = llaveEncriptacion;
        LOGGER.debug("Nombre Algoritmo [{}], Llave [{}]", nombreAlgoritmo, llaveEncriptacion);
    }

    public String encrypt(String input) {
        String encryptedString = null;
        try {

            byte[] key = SHARED_KEY.getBytes("UTF-8");

            String desAlgoName = getDESAlgorithmName(ALGO_NAME);

            Cipher cipher = Cipher.getInstance(ALGO_NAME);
            //Actual DES algo needs 56 bits key, which is equivalent to 1byte (0 at 0th position)  Get 8*3 byets key
            LOGGER.debug("DES Algorithm  shared key size in bytes >> " + key.length);

            SecretKeySpec secretKeySpec = new SecretKeySpec(key, desAlgoName);

            byte[] encryptedBytes = encryptIntoBytes(cipher, secretKeySpec, input.getBytes(), 0, input.getBytes().length);

            encryptedString = new String(Base64.encodeBase64(encryptedBytes));
            LOGGER.debug("CADENA ENCRIPTADA: " + encryptedString);

        } catch (Exception e) {
            LOGGER.error("Error:<Exception> al encriptar el valor [" + input + "]. ", e);
        }

        return encryptedString;
    }


    public String decrypt(String input) {
        String decryptedString = null;

        try {
            String desAlgoName = getDESAlgorithmName(ALGO_NAME);
            Cipher cipher = Cipher.getInstance(ALGO_NAME);
            byte[] key = SHARED_KEY.getBytes("UTF-8");

            LOGGER.debug("DES Algorithm  shared key size in bytes >> " + key.length);
            SecretKeySpec secretKeySpec = new SecretKeySpec(key, desAlgoName);

            byte[] ciphertext = Base64.decodeBase64(input.getBytes("UTF-8"));

            byte[] encryptedBytes = decryptIntoBytes(cipher, secretKeySpec, ciphertext, 0, ciphertext.length);

            decryptedString = new String(encryptedBytes, "UTF-8");

            LOGGER.debug("CADENA DESENCRIPTADA: " + decryptedString);

        } catch (Exception e) {
            LOGGER.error("Error:<Exception> al encriptar el valor [" + input + "]. ", e);
        }

        return decryptedString;
    }

    private static byte[] encryptIntoBytes(Cipher cipher, SecretKeySpec secretKeySpec, byte[] dct, int offset, int len) throws GeneralSecurityException {
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        return cipher.doFinal(addPadding(dct, offset, len));
    }

    private static byte[] decryptIntoBytes(Cipher cipher, SecretKeySpec secretKeySpec, byte[] dct, int offset, int len) throws GeneralSecurityException {
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        return cipher.doFinal(dct);
    }

    private static String getDESAlgorithmName(String algoName) {
        LOGGER.debug("getDESAlgorithmName algoName >> " + algoName);
        String desAlgoName;
        int i = algoName.indexOf("/");
        if (i != -1)
            desAlgoName = algoName.substring(0, i);
        else
            desAlgoName = algoName;

        LOGGER.debug("getDESAlgorithmName algoName final >> " + desAlgoName);
        return desAlgoName;
    }

    /**
     * Adds padding characters to the data to be encrypted. Also adds random
     * Initial Value to the beginning of the encrypted data when using Triple
     * DES in CBC mode (DES-EDE3/CBC).
     *
     * @param inData Array of bytes to be padded
     * @param offset Offset to starting point within array
     * @param len    Number of bytes to be encrypted
     * @return Padded array of bytes
     */
    private static byte[] addPadding(byte[] inData, int offset, int len) {
        LOGGER.debug("addPadding offset >> " + offset + ", len >> " + len);
        byte[] bp;
        int padChars = PADDING_BLOCK; // start with max padding value
        //int partial = (len + 1) % padChars; // calculate how many extra bytes
        int partial = (len) % padChars; // calculate how many extra bytes
        // exist
        if (partial == 0) {
            //padChars = 1; // if none, set to only pad with length byte
            padChars = 0; // if none, set to only pad with length byte
        } else {
            padChars = padChars - partial; // calculate padding size to
            //padChars = padChars - partial + 1; // calculate padding size to
            // include length
        }
        LOGGER.debug("addPadding >> Add padding of " + padChars);
        /*
         * Create a byte array large enough to hold data plus padding bytes The
         * count of padding bytes is placed in the first byte of the data to be
         * encrypted. That byte is included in the count.
         */
        bp = new byte[len + padChars];
        //bp[0] = Byte.parseByte(Integer.toString(padChars));
        System.arraycopy(inData, offset, bp, 0, len);
        return bp;
    }

    public static byte[] hexFromString(String hex) {
        int len = hex.length();
        byte[] buf = new byte[((len + 1) / 2)];

        int i = 0, j = 0;
        if ((len % 2) == 1)
            buf[j++] = (byte) fromDigit(hex.charAt(i++));

        while (i < len) {
            buf[j++] = (byte) ((fromDigit(hex.charAt(i++)) << 4) | fromDigit(hex.charAt(i++)));
        }
        return buf;
    }

    private static int fromDigit(char ch) {
        if (ch >= '0' && ch <= '9')
            return ch - '0';
        if (ch >= 'A' && ch <= 'F')
            return ch - 'A' + 10;
        if (ch >= 'a' && ch <= 'f')
            return ch - 'a' + 10;

        throw new IllegalArgumentException("invalid hex digit '" + ch + "'");
    }

    public static String hexToString(byte[] ba) {
        return hexToString(ba, 0, ba.length);
    }

    private static final char[] hexDigits = {'0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    private static String hexToString(byte[] ba, int offset, int length) {
        char[] buf = new char[length * 2];
        int j = 0;
        int k;

        for (int i = offset; i < offset + length; i++) {
            k = ba[i];
            buf[j++] = hexDigits[(k >>> 4) & 0x0F];
            buf[j++] = hexDigits[k & 0x0F];
        }
        return new String(buf);
    }

}