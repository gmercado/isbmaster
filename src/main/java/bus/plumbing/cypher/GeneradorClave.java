/*
 * Copyright 2013 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.plumbing.cypher;

import bus.env.api.MedioAmbiente;
import org.apache.commons.lang.StringUtils;
import org.jasypt.digest.config.SimpleDigesterConfig;
import org.jasypt.salt.RandomSaltGenerator;
import org.jasypt.util.password.ConfigurablePasswordEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import static bus.env.api.Variables.*;

/**
 * Generador de clave o C\u00f3digo de verificación bajo un método de encriptación no reversible SHA-256
 *
 * @author Roger Chura
 * @since 1.0
 */
public class GeneradorClave {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneradorClave.class);
    private static final int ITERATIONS = 1000;
    private static final int SALT = 8;

    private final SecureRandom secureRandom;

    private final ConfigurablePasswordEncryptor encryptor;

    private final MedioAmbiente medioAmbiente;

    public GeneradorClave(MedioAmbiente medioAmbiente) throws NoSuchAlgorithmException {
        this.medioAmbiente = medioAmbiente;

        secureRandom = new SecureRandom();
        encryptor = new ConfigurablePasswordEncryptor();
        String algoritmo = StringUtils.trimToEmpty(this.medioAmbiente.getValorDe(NOMBRE_ALGORITMO_CLAVE, NOMBRE_ALGORITMO_CLAVE_DEFAULT));
        encryptor.setAlgorithm(algoritmo);

        SimpleDigesterConfig dc = new SimpleDigesterConfig();
        dc.setIterations(ITERATIONS);
        dc.setSaltGenerator(new RandomSaltGenerator());
        dc.setSaltSizeBytes(SALT);
        dc.setAlgorithm(algoritmo);
        encryptor.setConfig(dc);
    }

    /**
     * Genera una nueva clave de una longitud de N digitos
     *
     * @param digitos, longitud de la clave
     * @return ParClaves, clave y clave hasheada
     */
    public ParClaves nuevaClave(int digitos) {
        int i = secureRandom.nextInt(
                Integer.parseInt("1" + StringUtils.leftPad("", digitos, '0')));
        String pinCrudo = StringUtils.leftPad(String.valueOf(i), digitos, '0');
        String pinHasheado = encryptor.encryptPassword(pinCrudo);
        final ParClaves nuevaClave = new ParClaves(pinCrudo, pinHasheado);

        // OJOOO BORRAR LA SIGUIENTE LINEA.
        LOGGER.info("CLAVE: La nueva clave generada es: {} y la hasheada es: {}", nuevaClave.getClave(), nuevaClave.getClaveHasheado());

        if (medioAmbiente != null) {
            String valor = medioAmbiente.getValorDe(MOSTRAR_CLAVE, MOSTRAR_CLAVE_DEFAULT);
            if ("true".equalsIgnoreCase(valor)) {
                LOGGER.error("La aplicacion esta en modo debug (la variable {} existe): la CLAVE generada es {}.",
                        MOSTRAR_CLAVE, nuevaClave.getClave());
            }
        } else {
            LOGGER.error("El objeto <medioAmbiente> tiene valor nulo.");
        }

        return nuevaClave;
    }


    /**
     * Genera una nueva clave de una longitud de N digitos
     *
     * @param password, longitud de la clave
     * @return ParClaves, clave y clave hasheada
     */
    public ParClaves secret(String password) {
        return new ParClaves(password, encryptor.encryptPassword(password));
    }

    /**
     * Devuelve true si el digitado verifica contra alguno de los hashes.
     *
     * @param digitado el dato digitado
     * @param hashed   los datos en hash
     * @return true si alguno verifica.
     */
    public boolean verificar(String digitado, String... hashed) {
        if (StringUtils.trimToNull(digitado) == null) {
            return false;
        }
        for (String s : hashed) {
            if (s == null) {
                continue;
            }
            if (encryptor.checkPassword(digitado, s)) {
                return true;
            }
        }
        return false;
    }
}
