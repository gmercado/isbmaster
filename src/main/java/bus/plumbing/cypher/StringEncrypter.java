package bus.plumbing.cypher;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.*;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

/**
 * Created by rchura on 07-12-15.
 */
public class StringEncrypter implements CryptoSupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(StringEncrypter.class);

    private Cipher ecipher;
    private Cipher dcipher;

    private SecretKey key;
    private String encryptKey = "b21617ca69b03871354aa8586934e493";
    private int keysize = 24; //TripleDes

    public StringEncrypter() {
        try {
            byte[] keyAsBytes = encryptKey.getBytes("UTF-8");
            KeySpec keySpec = new DESedeKeySpec(keyAsBytes);
            //SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
            //SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede/ECB/NoPadding");
            //byte[] valuebytes = value.getBytes();
            key = new SecretKeySpec(Arrays.copyOf(keyAsBytes, keysize), "DESede");
            //key = keyFactory.generateSecret(keySpec);
        } catch (UnsupportedEncodingException | InvalidKeyException e) {
            e.printStackTrace();
        }

    }

    public StringEncrypter(SecretKey key, String algorithm) {
        try {
            ecipher = Cipher.getInstance(algorithm);
            dcipher = Cipher.getInstance(algorithm);
            ecipher.init(Cipher.ENCRYPT_MODE, key);
            dcipher.init(Cipher.DECRYPT_MODE, key);
        } catch (NoSuchPaddingException e) {
            LOGGER.error("EXCEPTION: NoSuchPaddingException", e);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("EXCEPTION: NoSuchAlgorithmException", e);
        } catch (InvalidKeyException e) {
            LOGGER.error("EXCEPTION: InvalidKeyException", e);
        }
    }


    public StringEncrypter(String passPhrase) {
        // 8-bytes Salt
        byte[] salt = {
                (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
                (byte) 0x56, (byte) 0x34, (byte) 0xE3, (byte) 0x03
        };

        // Iteration count
        int iterationCount = 19;
        try {

            KeySpec keySpec = new DESedeKeySpec(passPhrase.getBytes());
            //KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);
            SecretKey key = SecretKeyFactory.getInstance("DESede").generateSecret(keySpec);
            //SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndTripleDES").generateSecret(keySpec);
            //SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
            ecipher = Cipher.getInstance("DESede");
            dcipher = Cipher.getInstance("DESede");
            // Prepare the parameters to the cipthers
            ecipher.init(Cipher.ENCRYPT_MODE, key);
            dcipher.init(Cipher.DECRYPT_MODE, key);

//            AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);
//            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
//            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

//        } catch (InvalidAlgorithmParameterException e) {
//            LOGGER.error("EXCEPTION: InvalidAlgorithmParameterException", e);
        } catch (InvalidKeySpecException e) {
            LOGGER.error("EXCEPTION: InvalidKeySpecException", e);
        } catch (NoSuchPaddingException e) {
            LOGGER.error("EXCEPTION: NoSuchPaddingException", e);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("EXCEPTION: NoSuchAlgorithmException", e);
        } catch (InvalidKeyException e) {
            LOGGER.error("EXCEPTION: InvalidKeyException", e);
        }
    }


    public String encrypt(String str) {
        try {
            // Encode the string into bytes using utf-8
            byte[] utf8 = str.getBytes("UTF8");
            // Encrypt
            byte[] enc = ecipher.doFinal(utf8);
            // Encode bytes to base64 to get a string
            return Base64.encodeBase64String(enc);

        } catch (BadPaddingException e) {
            LOGGER.error("EXCEPTION: BadPaddingException", e);
        } catch (IllegalBlockSizeException e) {
            LOGGER.error("EXCEPTION: IllegalBlockSizeException", e);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("EXCEPTION: UnsupportedEncodingException", e);
        }
        return null;
    }


    public String decrypt(String str) {
        try {
            // Decode base64 to get bytes
            byte[] dec = Base64.decodeBase64(str);
            // Decrypt
            byte[] utf8 = dcipher.doFinal(dec);
            // Decode using utf-8
            return new String(utf8, "UTF8");
        } catch (BadPaddingException e) {
            LOGGER.error("EXCEPTION: BadPaddingException", e);
        } catch (IllegalBlockSizeException e) {
            LOGGER.error("EXCEPTION: IllegalBlockSizeException", e);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("EXCEPTION: UnsupportedEncodingException", e);
        }
        return null;
    }

    @Override
    public final String dec(String retValue) {
        try {
            //Cipher cipher = Cipher.getInstance("DESede/NoPadding");
            Cipher cipher = Cipher.getInstance("DESede/ECB/NoPadding");
            //Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] ciphertext = Base64.decodeBase64(retValue.getBytes("UTF-8"));
            byte[] clear = cipher.doFinal(ciphertext);
            return new String(clear, "UTF-8");
        } catch (UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            LOGGER.error("Error al des-encriptar la cadena '" + retValue + "'", e);
        }
        return retValue;
    }


    @Override
    public final String enc(String value) {
        if (value == null) {
            return null;
        }
        try {
            //Cipher cipher = Cipher.getInstance("DESede/NoPadding");
            Cipher cipher = Cipher.getInstance("DESede/ECB/NoPadding");
            //Cipher cipher = Cipher.getInstance("DESede");
            try {
                cipher.init(Cipher.ENCRYPT_MODE, key);
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }
            byte[] cleartext = value.getBytes("UTF-8");
            byte[] ciphertext = cipher.doFinal(cleartext);

            return new String(Base64.encodeBase64(ciphertext));
        } catch (IllegalBlockSizeException e) {
            LOGGER.error("Error <IllegalBlockSizeException> al encriptar la cadena '" + value + "'", e);
        } catch (BadPaddingException e) {
            LOGGER.error("Error <BadPaddingException> al encriptar la cadena '" + value + "'", e);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Error <NoSuchAlgorithmException> al encriptar la cadena '" + value + "'", e);
        } catch (NoSuchPaddingException e) {
            LOGGER.error("Error <NoSuchPaddingException> al encriptar la cadena '" + value + "'", e);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Error <UnsupportedEncodingException> al encriptar la cadena '" + value + "'", e);
        }
        return value;
    }


}
