package bus.plumbing.cypher;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.Assert;

import javax.crypto.*;
import javax.crypto.spec.DESedeKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;
import java.util.Properties;

/**
 * Created by rchura on 07-12-15.
 */
public class EncryptedPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer implements InitializingBean,
        CryptoSupport {

    private static final Logger LOGGER = LoggerFactory.getLogger(EncryptedPropertyPlaceholderConfigurer.class);

    public void afterPropertiesSet() throws Exception {

        String alternativeLocation = System.getProperty(altLocationVar);
        if (alternativeLocation != null) {
            LOGGER.info("Se ha encontrado la variable {}, de modo que se intenta cambiar el location a {}.",
                    altLocationVar, alternativeLocation);
            Resource resource = new UrlResource(alternativeLocation);
            Assert.isTrue(resource.exists(), alternativeLocation + " no existe");
            setLocation(resource);
        }

        byte[] keyAsBytes = encryptKey.getBytes("UTF-8");
        KeySpec keySpec = new DESedeKeySpec(keyAsBytes);

        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
        key = keyFactory.generateSecret(keySpec);

        largo = prefix.length();
    }

    private String altLocationVar;

    private int largo;

    private SecretKey key;

    protected String resolvePlaceholder(String placeholder, Properties props) {
        String retValue;
        retValue = super.resolvePlaceholder(placeholder, props);
        if (retValue != null && retValue.startsWith(prefix)) {
            return dec(retValue);
        }
        return retValue;
    }

    @Override
    public final String dec(String retValue) {
        try {
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] ciphertext = Base64.decodeBase64(retValue.substring(largo).getBytes("UTF-8"));
            byte[] clear = cipher.doFinal(ciphertext);
            return new String(clear, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Error al des-encriptar la cadena '" + retValue + "'", e);
        } catch (IllegalBlockSizeException e) {
            LOGGER.error("Error al des-encriptar la cadena '" + retValue + "'", e);
        } catch (InvalidKeyException e) {
            LOGGER.error("Error al des-encriptar la cadena '" + retValue + "'", e);
        } catch (BadPaddingException e) {
            LOGGER.error("Error al des-encriptar la cadena '" + retValue + "'", e);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Error al des-encriptar la cadena '" + retValue + "'", e);
        } catch (NoSuchPaddingException e) {
            LOGGER.error("Error al des-encriptar la cadena '" + retValue + "'", e);
        }
        return retValue;
    }

    @Override
    public final String enc(String value) {
        if (value == null) {
            return null;
        }
        try {
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] cleartext = value.getBytes("UTF-8");
            byte[] ciphertext  = cipher.doFinal(cleartext);
            return "crypt:" + new String(Base64.encodeBase64(ciphertext));
        } catch (IllegalBlockSizeException e) {
            LOGGER.error("Error al encriptar la cadena '" + value + "'", e);
        } catch (BadPaddingException e) {
            LOGGER.error("Error al encriptar la cadena '" + value + "'", e);
        } catch (InvalidKeyException e) {
            LOGGER.error("Error al encriptar la cadena '" + value + "'", e);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Error al encriptar la cadena '" + value + "'", e);
        } catch (NoSuchPaddingException e) {
            LOGGER.error("Error al encriptar la cadena '" + value + "'", e);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Error al encriptar la cadena '" + value + "'", e);
        }
        return value;
    }

    /**
     * Holds value of property encryptKey.
     */
    private String encryptKey = "b21617ca69b03871354aa8586934e493";

    /**
     * Setter for property encryptKey.
     *
     * @param encryptKey
     *            New value of property encryptKey.
     */
    public void setEncryptKey(String encryptKey) {
        this.encryptKey = encryptKey;
    }

    private String prefix = "crypt:";

    /**
     * Setter for property prefix.
     *
     * @param prefix
     *            the new prefix.
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getAltLocationVar() {
        return altLocationVar;
    }

    public void setAltLocationVar(String altLocationVar) {
        this.altLocationVar = altLocationVar;
    }


    private Resource lastLocation;

    public Resource getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(Resource lastLocation) {
        this.lastLocation = lastLocation;
    }

    @Override
    public void setLocation(Resource location) {
        setLastLocation(location);
        super.setLocation(location);
    }

}

