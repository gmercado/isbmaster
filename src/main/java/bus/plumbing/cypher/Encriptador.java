/*
 * Copyright 2013 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.plumbing.cypher;

import bus.env.api.MedioAmbiente;
import org.apache.commons.lang.StringUtils;
import org.jasypt.digest.config.SimpleDigesterConfig;
import org.jasypt.salt.RandomSaltGenerator;
import org.jasypt.util.password.ConfigurablePasswordEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.NoSuchAlgorithmException;

import static bus.env.api.Variables.MOSTRAR_PWD;
import static bus.env.api.Variables.MOSTRAR_PWD_DEFAULT;


/**
 * Generador de clave o C\u00f3digo de verificaci�n bajo un m�todo de encriptaci�n no reversible SHA-256
 * @author Roger Chura
 * @since  1.0
 */
/**
 * Generador de clave o código de verificación bajo un método de encriptación no reversible SHA-256
 * @author Roger Chura
 * @since  1.0
 */
public class Encriptador {

    private static final Logger LOGGER = LoggerFactory.getLogger(Encriptador.class);

    private final ConfigurablePasswordEncryptor encryptor;

    private final MedioAmbiente medioAmbiente;

    private final String nombreAlgoritmo;

    private final String algoritmoDefault;

    public Encriptador(MedioAmbiente medioAmbiente, String nombreAlgoritmo, String algoritmoDefault)throws NoSuchAlgorithmException {
        this.medioAmbiente = medioAmbiente;
        this.nombreAlgoritmo = nombreAlgoritmo;
        this.algoritmoDefault = algoritmoDefault;

        String algoritmo = StringUtils.trimToEmpty(this.medioAmbiente.getValorDe(nombreAlgoritmo, algoritmoDefault));
        LOGGER.info("Nombre del algoritmo para Encriptar:"+algoritmo);

        encryptor = new ConfigurablePasswordEncryptor();
        encryptor.setAlgorithm(algoritmo);
        SimpleDigesterConfig dc = new SimpleDigesterConfig();
        dc.setIterations(1000);
        dc.setSaltGenerator(new RandomSaltGenerator("SHA1PRNG"));
        dc.setSaltSizeBytes(8);
        dc.setAlgorithm(algoritmo);
        encryptor.setConfig(dc);
    }

    /**
     * Genera un nuevo hash para el password porporcionado
     * @param passwordPlano
     * @return ParClaves, password y password hasheada
     */
    public ParClaves getPassword(String passwordPlano) {
        if(StringUtils.trimToNull(passwordPlano)==null){
            return null;
        }

        LOGGER.debug("Iniciando generacion de HASH.");

        String pwdHasheado = encryptor.encryptPassword(passwordPlano);
        final ParClaves nuevaClave = new ParClaves(passwordPlano, pwdHasheado);

        if(medioAmbiente!=null){
            String valor = medioAmbiente.getValorDe(MOSTRAR_PWD, MOSTRAR_PWD_DEFAULT);
            if("true".equalsIgnoreCase(valor)){
                LOGGER.error("La aplicacion esta en modo debug (la variable {} existe): la CLAVE generada es {}.",
                        MOSTRAR_PWD, nuevaClave.getClave());
            }
        }else{
            LOGGER.error("El objeto <medioAmbiente> tiene valor nulo.");
        }

        return nuevaClave;
    }

    /**
     * Devuelve true si el texto digitado coincide contra alguno de los hashes.
     * @param digitado el dato digitado
     * @param hashed los datos en hash
     * @return true si alguno verifica.
     */
    public boolean checkPassword(String digitado, String... hashed) {
        if(StringUtils.trimToNull(digitado)==null){
            return false;
        }
        for (String s : hashed) {
            if (s == null) {
                continue;
            }
            if (encryptor.checkPassword(digitado, s)) {
                return true;
            }
        }
        return false;
    }
}

