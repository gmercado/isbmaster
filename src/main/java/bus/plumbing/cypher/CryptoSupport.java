package bus.plumbing.cypher;

/**
 * Created by rchura on 07-12-15.
 */
public interface CryptoSupport {
    String dec(String retValue);

    String enc(String value);
}
