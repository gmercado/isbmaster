package bus.plumbing.compress;

/**
 * @author by rsalvatierra on 08/03/2017.
 */
public class Item {
    private String path;
    private byte[] content;

    public Item(String path, String content) {
        this(path, content.getBytes());
    }

    Item(String path, byte[] content) {
        this.path = path;
        this.content = content;
    }

    String getPath() {
        return path;
    }

    byte[] getContent() {
        return content;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}
