package bus.plumbing.compress;

import java.io.File;
import java.util.Random;

/**
 * @author by rsalvatierra on 08/03/2017.
 */
public class CompressArchiveStructure {
    public static Item[] create() {
        //Example Compress
        //     <root>
        //     |
        //     +- info.txt
        //     +- random-100-bytes.dump
        //     +- dir1
        //     |  +- file-in-a-directory1.txt
        //     +- dir2
        //        +- file-in-a-directory2.txt

        Item[] items = new Item[3];

        items[0] = new Item("info.txt", "This is the info");

        byte[] content = new byte[100];
        new Random().nextBytes(content);
        items[1] = new Item("random-100-bytes.dump", content);

        // dir1 doesn't have separate archive item
        items[2] = new Item("var" + File.separator +"log"+File.separator+"isbfile"+File.separator+ "170216sin.txt",
                "This file located in a directory 'dir'");
            /*
        // dir2 does have separate archive item
        items[3] = new Item("dir2" + File.separator, (byte[]) null);
        items[4] = new Item("dir2" + File.separator + "file2.txt",
                "This file located in a directory 'dir'");*/
        return items;
    }

}
