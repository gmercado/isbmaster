package bus.plumbing.compress;

import net.sf.sevenzipjbinding.*;
import net.sf.sevenzipjbinding.impl.OutItemFactory;
import net.sf.sevenzipjbinding.impl.RandomAccessFileOutStream;
import net.sf.sevenzipjbinding.util.ByteArrayStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * @author by rsalvatierra on 08/03/2017.
 */
public class Compress7z {
    private static final Logger LOGGER = LoggerFactory.getLogger(Compress7z.class);
    private static final String MODE_READ_WRITE = "rw";
    private static final int LEVEL_NORMAL_DEFAULT = 5;
    private static final boolean SOLID_STATE_DEFAULT = true;
    private static final int THREAD_COUNT_DEFAULT = 2;

    /**
     * The callback provides information about archive items.
     */
    private final class MyCreateCallback
            implements IOutCreateCallback<IOutItem7z> {

        public void setOperationResult(boolean operationResultOk)
                throws SevenZipException {
            // Track each operation result here
        }

        public void setTotal(long total) throws SevenZipException {
            // Track operation progress here
        }

        public void setCompleted(long complete) throws SevenZipException {
            // Track operation progress here
        }

        public IOutItem7z getItemInformation(int index,
                                             OutItemFactory<IOutItem7z> outItemFactory) {
            IOutItem7z item = outItemFactory.createOutItem();

            if (items[index].getContent() == null) {
                // Directory
                item.setPropertyIsDir(true);
            } else {
                // File
                item.setDataSize((long) items[index].getContent().length);
            }

            item.setPropertyPath(items[index].getPath());

            return item;
        }

        public ISequentialInStream getStream(int i) throws SevenZipException {
            if (items[i].getContent() == null) {
                return null;
            }
            return new ByteArrayStream(items[i].getContent(), true);
        }
    }

    private Item[] items;

    public void compress(String filename, Item[] itemPath) {
        if (itemPath == null) {
            items = CompressArchiveStructure.create();
        } else {
            items = itemPath;
        }
        boolean success = false;
        RandomAccessFile raf = null;
        IOutCreateArchive7z outArchive = null;
        try {
            raf = new RandomAccessFile(filename, MODE_READ_WRITE);

            // Open out-archive object
            outArchive = SevenZip.openOutArchive7z();

            // Configure archive
            outArchive.setLevel(LEVEL_NORMAL_DEFAULT);
            outArchive.setSolid(SOLID_STATE_DEFAULT);
            outArchive.setThreadCount(THREAD_COUNT_DEFAULT);

            // Create archive
            outArchive.createArchive(new RandomAccessFileOutStream(raf),
                    items.length, new MyCreateCallback());

            success = true;
        } catch (SevenZipException e) {
            LOGGER.error("7z-Error occurs:", e);
        } catch (Exception e) {
            LOGGER.error("Error occurs: ", e);
        } finally {
            if (outArchive != null) {
                try {
                    outArchive.close();
                } catch (IOException e) {
                    LOGGER.error("Error closing file: ", e);
                    success = false;
                }
            }
            if (raf != null) {
                try {
                    raf.close();
                } catch (IOException e) {
                    LOGGER.error("Error closing file: ", e);
                    success = false;
                }
            }
        }
        if (success) {
            LOGGER.info("Compression operation succeeded");
        }
    }

    public void compress(File filename, Item[] itemPath) {
        if (itemPath == null) {
            items = CompressArchiveStructure.create();
        } else {
            items = itemPath;
        }
        boolean success = false;
        RandomAccessFile raf = null;
        IOutCreateArchive7z outArchive = null;
        try {
            raf = new RandomAccessFile(filename, MODE_READ_WRITE);

            // Open out-archive object
            outArchive = SevenZip.openOutArchive7z();

            // Configure archive
            outArchive.setLevel(LEVEL_NORMAL_DEFAULT);
            outArchive.setSolid(SOLID_STATE_DEFAULT);
            outArchive.setThreadCount(THREAD_COUNT_DEFAULT);

            // Create archive
            outArchive.createArchive(new RandomAccessFileOutStream(raf),
                    items.length, new MyCreateCallback());

            success = true;
        } catch (SevenZipException e) {
            LOGGER.error("7z-Error occurs:", e);
        } catch (Exception e) {
            LOGGER.error("Error occurs: ", e);
        } finally {
            if (outArchive != null) {
                try {
                    outArchive.close();
                } catch (IOException e) {
                    LOGGER.error("Error closing file: ", e);
                    success = false;
                }
            }
            if (raf != null) {
                try {
                    raf.close();
                } catch (IOException e) {
                    LOGGER.error("Error closing file: ", e);
                    success = false;
                }
            }
        }
        if (success) {
            LOGGER.debug("Compression operation succeeded");
        }
    }

}
