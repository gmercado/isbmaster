package bus.plumbing.sftp;

/**
 * Created by atenorio on 03/05/2017.
 */
import com.jcraft.jsch.*;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author Marcelo Morales
 *
 */
public class SftpOperations {

    private static final Logger LOGGER = LoggerFactory.getLogger(SftpOperations.class);

    private long reconnectDelay;

    private int maximumReconnectAttempts;

    private String localWorkDirectory;

    private Session session;

    private ChannelSftp channel;

    private boolean aceptarSiNoEstaEnKnownHosts;

    public SftpOperations(long reconnectDelay, int maximumReconnectAttempts, String localWorkDirectory) {
        this.reconnectDelay = reconnectDelay;
        this.maximumReconnectAttempts = maximumReconnectAttempts;
        this.localWorkDirectory = localWorkDirectory;
    }

    public boolean isConnected() {
        return session != null && session.isConnected() && channel != null && channel.isConnected();
    }

    /**
     * Conectar a un sftp.
     * @param host
     * @param port
     * @param username
     * @param password
     * @param knownHostsFile
     * @param privateKeyFile
     * @param privateKeyFilePassphrase
     * @return
     * @throws JSchException cuando NO se puede conectar al cabo de todos los intentos.
     */
    public boolean connect(String host, int port, String username, String password, String knownHostsFile,
                           String privateKeyFile, String privateKeyFilePassphrase, boolean aceptarSiNoEnKnown) throws JSchException {
        if (isConnected()) {
            LOGGER.debug("ya estamos conectados a {}, no hacemos nada para conectarnos nuevamente", host);
            // already connected
            return true;
        }
        LOGGER.info("Connect to {}", host);
        this.aceptarSiNoEstaEnKnownHosts = aceptarSiNoEnKnown;

        boolean connected = false;
        int attempt = 0;

        while (!connected) {
            try {
                if (LOGGER.isDebugEnabled() && attempt > 0) {
                    LOGGER.debug("Reconnect attempt #{} connecting to {}", attempt, host);
                }

                if (channel == null || !channel.isConnected()) {
                    if (session == null || !session.isConnected()) {
                        LOGGER.debug("Session isn't connected, trying to recreate and connect.");
                        session = createSession(privateKeyFile, privateKeyFilePassphrase, knownHostsFile, username, host,
                                port, password);
                        session.connect();
                    }
                    LOGGER.debug("Channel isn't connected, trying to recreate and connect.");
                    channel = (ChannelSftp) session.openChannel("sftp");
                    channel.connect();
                    LOGGER.info("Connected to {}", host);
                }

                // yes we could connect
                connected = true;
            } catch (JSchException e) {
                attempt++;
                if (attempt > getMaximumReconnectAttempts()) {
                    throw e;
                }
                LOGGER.debug("Ha ocurrido un error en la conexion, reintentamos...", e);
                if (getReconnectDelay() > 0) {
                    try {
                        Thread.sleep(getReconnectDelay());
                    } catch (InterruptedException e1) {
                        LOGGER.warn("Interumpido", e1);
                    }
                }
            }
        }
        return true;
    }

    protected Session createSession(String privateKeyFile, String privateKeyFilePassphrase, String knownHostsFile,
                                    String username, String host, int port, final String password) throws JSchException {
        final JSch jsch = new JSch();

        if (isNotEmpty(privateKeyFile)) {
            LOGGER.debug("Using private keyfile: {}", privateKeyFile);
            if (isNotEmpty(privateKeyFilePassphrase)) {
                jsch.addIdentity(privateKeyFile, privateKeyFilePassphrase);
            } else {
                jsch.addIdentity(privateKeyFile);
            }
        }

        if (isNotEmpty(knownHostsFile)) {
            LOGGER.debug("Using knownhosts file: {}", knownHostsFile);
            jsch.setKnownHosts(knownHostsFile);
        }

        final Session mySession = jsch.getSession(username, host, port);
        mySession.setUserInfo(new UserInfo() {

            @Override
            public String getPassphrase() {
                return null;
            }

            @Override
            public String getPassword() {
                return password;
            }

            @Override
            public boolean promptPassword(String s) {
                LOGGER.warn("Escuchando password, msg={}", s);
                return true;
            }

            @Override
            public boolean promptPassphrase(String s) {
                LOGGER.warn("Escuchando passphrase, msg={}", s);
                return true;
            }

            @Override
            public boolean promptYesNo(String s) {
                LOGGER.error("El host al que nos hemos conectado por ssh no esta en nuestro known_hosts, msg={}", s);
                return aceptarSiNoEstaEnKnownHosts;
            }

            @Override
            public void showMessage(String s) {
                LOGGER.info("El sistema ssh ha mandado un mensaje msg={}", s);
            }
        });
        return mySession;
    }

    private static boolean isNotEmpty(Object value) {
        if (value == null) {
            return false;
        } else if (value instanceof String) {
            String text = (String) value;
            return text.trim().length() > 0;
        } else {
            return true;
        }
    }

    public long getReconnectDelay() {
        return reconnectDelay;
    }

    public int getMaximumReconnectAttempts() {
        return maximumReconnectAttempts;
    }

    public void disconnect() {
        if (session != null && session.isConnected()) {
            session.disconnect();
        }
        if (channel != null && channel.isConnected()) {
            channel.disconnect();
        }
        LOGGER.debug("Disconnected");
    }

    public boolean deleteFile(String name) throws SftpException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Deleteing file: {}", name);
        }
        channel.rm(name);
        return true;
    }

    public String getCurrentDirectory() throws SftpException {
        return channel.pwd();
    }

    public void changeCurrentDirectory(String path) throws SftpException {
        LOGGER.debug("Indentando cambiar el directorio a '{}'", path);
        channel.cd(path);
    }

    public List<ChannelSftp.LsEntry> listFiles() throws SftpException {
        return listFiles(".");
    }

    @SuppressWarnings("unchecked")
    public List<ChannelSftp.LsEntry> listFiles(String path) throws SftpException {
        if (!isNotEmpty(path)) {
            // list current dirctory if file path is not given
            path = ".";
        }
        return channel.ls(path);
    }

    public File retrieveFileToFileInLocalWorkDirectory(String name, String relativeName) throws SftpException {
        File local = new File(getLocalWorkDirectory());
        File temp = new File(local, relativeName + ".inprogress");
        local = new File(local, relativeName);

        OutputStream os;

        try {
            if (local.mkdirs()) { // create directory to local work file
                LOGGER.debug("Se ha creado el directorio satisfactoriamente {}", local);
            } else {
                LOGGER.debug("El directorio inicial no se ha creado o ya existe {}", local);
            }

            if (temp.exists()) { // delete any existing files
                if (!temp.delete()) {
                    throw new IllegalStateException("No puedo eliminar el archivo {}" + temp);
                }
            }
            if (local.exists()) {
                if (!local.delete()) {
                    throw new IllegalStateException("No puedo eliminar el archivo {}" + local);
                }
            }

            // create new temp local work file
            if (!temp.createNewFile()) {
                throw new IllegalStateException("No puedo crear el archivo {}" + temp);
            }

            os = new FileOutputStream(temp);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        try {
            // store the java.io.File handle as the body
            channel.get(name, os);
        } finally {
            IOUtils.closeQuietly(os);
        }

        // rename temp to local after we have retrieved the data
        if (!temp.renameTo(local)) {
            throw new IllegalStateException("No puedo renombrar: " + temp + " a: " + local);
        }

        return local;
    }

    public String getLocalWorkDirectory() {
        return localWorkDirectory;
    }

    private static String onlyPath(String name) {
        if (name == null) {
            return null;
        }
        int pos = name.lastIndexOf('/');
        if (pos == -1) {
            pos = name.lastIndexOf(File.separator);
        }
        if (pos != -1) {
            return name.substring(0, pos);
        }
        // no path
        return null;
    }

    private static String stripPath(String name) {
        if (name == null) {
            return null;
        }
        int pos = name.lastIndexOf('/');
        if (pos == -1) {
            pos = name.lastIndexOf(File.separator);
        }
        if (pos != -1) {
            return name.substring(pos + 1);
        }
        return name;
    }

    public boolean existsFile(String name) throws SftpException {
        String directory = onlyPath(name);
        String onlyName;
        if (directory == null) {
            directory = ".";
            onlyName = name;
        } else {
            onlyName = stripPath(name);
        }
        Vector<?> files = channel.ls(directory);
        for (Object file : files) {
            ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) file;
            if (entry.getFilename().equals(onlyName)) {
                return true;
            }
        }
        return false;
    }

    public boolean storeFile(String name, InputStream in) throws SftpException {
        boolean existFile = existsFile(name);
        if (existFile) {
            throw new IllegalStateException("File already exist: " + name + ". Cannot write new file.");
        }
        channel.put(in, name);
        return true;
    }

    public boolean storeFileOverTheExistingOne(String name, InputStream in) throws SftpException {
        channel.put(in, name);
        return true;
    }

    public void moveFiles(String sourcePath, String destPath, List<String> archivos) throws SftpException {
        channel.cd(sourcePath);
        SftpATTRS attrs = null;
        for (String oListItem : archivos) {
            try {
                attrs = channel.stat(destPath + "/" + oListItem);

            } catch (SftpException e) {
                LOGGER.debug("There is no file named {}" + oListItem, e);
            } finally {
                long inicio = System.currentTimeMillis();
                if (attrs != null) {
                    channel.rm(destPath + "/" + oListItem);
                }
                String destino = destPath + "/" + oListItem;
                channel.rename(oListItem, destino);
                LOGGER.info("Archivo movido: {} en {} milisegundos ", oListItem, (System.currentTimeMillis() - inicio));
            }
        }
    }

    public void renameFile(String sourcePath, String  archivo, String  archivoRenombrado) throws SftpException {
        channel.cd(sourcePath);
        SftpATTRS attrs = null;
            try {
                attrs = channel.stat(sourcePath + "/" + archivo);
            } catch (SftpException e) {
                LOGGER.debug("There is no file named {}" + archivo, e);
            } finally {
                long inicio = System.currentTimeMillis();
                if (attrs != null) {
                    //channel.rm(destPath + "/" + oListItem);
                }
                channel.rename(archivo, archivoRenombrado);
                LOGGER.info("Archivo movido: {} en {} milisegundos ", archivo, (System.currentTimeMillis() - inicio));
            }
    }
}
