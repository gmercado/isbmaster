package bus.plumbing.csv;

import bus.plumbing.csv.annotation.FormatCsv;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by atenorio on 18/05/2017.
 */
public class GeneradorCSV implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(GeneradorCSV.class);

    public static boolean adicionarLinea(BufferedWriter bw, String object) throws IOException {
        bw.write(object);
        bw.write(new String("\r\n"));
        return true;
    }
    /**
     * Generación de lineas de csv
     **/
    public static boolean poblarLinea(BufferedWriter bw, Object object) throws IOException {
        Field[] fieldsL = object.getClass().getDeclaredFields();
        StringBuilder sb = new StringBuilder();
        for (Field field : fieldsL) {
            FormatCsv format = field.getAnnotation(FormatCsv.class);
            String propertyName = field.getName();
            //propertyName = objDescriptor.getName();
            if (format == null) {
                continue;
            }
            // Recuperar tipo de dato
            try {
                Object propType = PropertyUtils.getPropertyType(object, propertyName);
                Object propValue = PropertyUtils.getProperty(object, propertyName);
                String value = "";
                if (propValue != null) {
                    if (propType.equals(BigDecimal.class)) {
                        BigDecimal dec = (BigDecimal) propValue;
                        DecimalFormat decimalFormat = new DecimalFormat(format.pattern());
                        value = decimalFormat.format(dec);
                        value = value.replace(".", "");
                        value = value.replace(",", "");
                    } else if (propType.equals(Integer.class)) {
                        Integer entero = (Integer) propValue;
                        value = Integer.toString(entero);
                    } else if (propType.equals(String.class)) {
                        value = (String) propValue;
                    } else if (propType.equals(String.class)) {
                        Date fecha = (Date) propValue;
                        if (StringUtils.isNotEmpty(format.pattern())) {
                            SimpleDateFormat sdf = new SimpleDateFormat(format.pattern());
                            value = sdf.format(fecha);
                        }
                    } else {
                        value = propValue.toString();
                    }
                }
                if (format.align().equals(FormatCsv.alignType.RIGTH)) {
                    sb.append(StringUtils.leftPad(value, format.length(), format.fillSpace()));
                } else {
                    sb.append(StringUtils.rightPad(value, format.length(), format.fillSpace()));
                }
            } catch (IllegalAccessException e) {
                LOGGER.error("IllegalAccessException: {}", e);
                return false;
            } catch (InvocationTargetException e) {
                LOGGER.error("InvocationTargetException: {}", e);
                return false;
            } catch (NoSuchMethodException e) {
                LOGGER.error("NoSuchMethodException: {}", e);
                return false;
            } catch (Exception e) {
                LOGGER.error("Exception: {}", e);
                return false;
            }
        }
        bw.write(sb.toString()); //, StandardCharsets.ISO_8859_1 US_ASCII "Windows-1252");
        bw.write(new String("\r\n")); //newLine();
        return true;
    }

    public static ByteBuffer str_to_bb(String msg, Charset charset){
        return ByteBuffer.wrap(msg.getBytes(charset));
    }

    public static String bb_to_str(ByteBuffer buffer, Charset charset){
        byte[] bytes;
        if(buffer.hasArray()) {
            bytes = buffer.array();
        } else {
            bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
        }
        return new String(bytes, charset);
    }

    public static String generarCsv(Object object){
        Field[] fieldsL = object.getClass().getDeclaredFields();
        StringBuilder sb = new StringBuilder();
        for (Field field : fieldsL) {
            FormatCsv format = field.getAnnotation(FormatCsv.class);
            String propertyName = field.getName();
            //propertyName = objDescriptor.getName();
            if (format == null) {
                continue;
            }
            // Recuperar tipo de dato
            try {
                Object propType = PropertyUtils.getPropertyType(object, propertyName);
                Object propValue = PropertyUtils.getProperty(object, propertyName);
                String value = "";
                if (propValue != null) {
                    if (propType.equals(BigDecimal.class)) {
                        BigDecimal dec = (BigDecimal) propValue;
                        DecimalFormat decimalFormat = new DecimalFormat(format.pattern());
                        value = decimalFormat.format(dec);
                        value = value.replace(".", "");
                        value = value.replace(",", "");
                    } else if (propType.equals(Integer.class)) {
                        Integer entero = (Integer) propValue;
                        value = Integer.toString(entero);
                    } else if (propType.equals(String.class)) {
                        value = (String) propValue;
                    } else if (propType.equals(String.class)) {
                        Date fecha = (Date) propValue;
                        if (StringUtils.isNotEmpty(format.pattern())) {
                            SimpleDateFormat sdf = new SimpleDateFormat(format.pattern());
                            value = sdf.format(fecha);
                        }
                    } else {
                        value = propValue.toString();
                    }
                }
                if (format.align().equals(FormatCsv.alignType.RIGTH)) {
                    sb.append(StringUtils.leftPad(value, format.length(), format.fillSpace()));
                } else {
                    sb.append(StringUtils.rightPad(value, format.length(), format.fillSpace()));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return "";
            } catch (InvocationTargetException e) {
                e.printStackTrace();
                return "";
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return "";
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
        return sb.toString();
    }

}
