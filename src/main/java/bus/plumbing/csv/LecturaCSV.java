package bus.plumbing.csv;

import bus.plumbing.csv.annotation.FormatCsv;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by atenorio on 24/05/2017.
 */
public class LecturaCSV {
    private static final Logger LOGGER = LoggerFactory.getLogger(LecturaCSV.class);

    public LecturaCSV() {
    }

    public static <T> T procesar(Class<T> classz, String linea) {
        if (linea != null && linea.isEmpty()) {
            return null;
        }
        T objeto = null;
        try {
            objeto = classz.newInstance();
        } catch (InstantiationException e) {
            LOGGER.error("Error {}.", e);
            return null;
        } catch (IllegalAccessException e) {
            LOGGER.error("Error {}.", e);
            return null;
        }
        Field[] fieldsL = classz.getDeclaredFields();

        for (Field field : fieldsL) {
            FormatCsv format = field.getAnnotation(FormatCsv.class);
            String propertyName = field.getName();
            if (format == null) {
                continue;
            }

            try {
                Object propType = PropertyUtils.getPropertyType(objeto, propertyName);
                // Verificar si existe problemas de lectura en un campo
                if (linea.length() < format.length()) {
                    return null;
                }
                // Recuperar valor
                String value = linea.substring(0, format.length());
                // Actualizar linea
                linea = linea.substring(format.length());

                //Cargar valor a atributo
                if (propType.equals(BigDecimal.class)) {
                    BigDecimal b = new BigDecimal(value);
                    Double fraccion = Math.pow(0.1, format.scale());
                    BigDecimal propValue = new BigDecimal(b.doubleValue() * fraccion.doubleValue());
                    propValue = propValue.setScale(format.scale(), RoundingMode.HALF_UP);
                    PropertyUtils.setProperty(objeto, propertyName, propValue);
                } else if (propType.equals(BigInteger.class)) {
                    if (StringUtils.trimToEmpty(value).isEmpty()) {
                        value = "0";
                    }
                    BigInteger propValue = new BigInteger(value);
                    PropertyUtils.setProperty(objeto, propertyName, propValue);
                } else if (propType.equals(Integer.class)) {
                    if (StringUtils.trimToEmpty(value).isEmpty()) {
                        value = "0";
                    }
                    Integer propValue = new Integer(value);
                    PropertyUtils.setProperty(objeto, propertyName, propValue);
                } else if (propType.equals(String.class)) {
                    String propValue = value.trim();
                    PropertyUtils.setProperty(objeto, propertyName, propValue);
                } else if (propType.equals(Date.class)) {
                    if (StringUtils.isNotEmpty(format.pattern())) {
                        SimpleDateFormat sdf = new SimpleDateFormat(format.pattern());
                        Date propValue = sdf.parse(value);
                        PropertyUtils.setProperty(objeto, propertyName, propValue);
                    }
                } else if (propType.equals(Character.class)) {
                    if (StringUtils.isNotEmpty(value)) {
                        Character propValue = value.charAt(0);
                        PropertyUtils.setProperty(objeto, propertyName, propValue);
                    } else {
                        PropertyUtils.setProperty(objeto, propertyName, " ".charAt(0));
                    }
                } else if (field.getType().isPrimitive()) {
                    switch (propType.toString()) {
                        case "short":
                            if (StringUtils.trimToEmpty(value).isEmpty()) {
                                PropertyUtils.setSimpleProperty(objeto, propertyName, (new Integer(0)).shortValue());
                            } else {
                                PropertyUtils.setSimpleProperty(objeto, propertyName, (new Integer(value.trim())).shortValue());
                            }
                            break;
                        case "int":
                            if (StringUtils.trimToEmpty(value).isEmpty()) {
                                PropertyUtils.setSimpleProperty(objeto, propertyName, (new Integer(0)).intValue());
                            } else {
                                PropertyUtils.setSimpleProperty(objeto, propertyName, (new Integer(value.trim())).intValue());
                            }
                            break;
                        case "long":
                            if (StringUtils.trimToEmpty(value).isEmpty()) {
                                PropertyUtils.setSimpleProperty(objeto, propertyName, (new Long(0)).longValue());
                            } else {
                                PropertyUtils.setSimpleProperty(objeto, propertyName, (new Long(value)).longValue());
                            }
                            break;
                        case "boolean":
                            PropertyUtils.setSimpleProperty(objeto, propertyName, (new Boolean(value)).booleanValue());
                            break;
                        default:
                            LOGGER.info("Valor por defecto tipo primitivo " + propType);
                            break;
                    }
                } else {
                    LOGGER.error("Error no se puede asignar a campo {} de tipo {} el valor {}.", propertyName, propType, value);
                    return null;
                }
            } catch (IllegalAccessException e) {
                LOGGER.error("IllegalAccessException {}", e);
                return null;
            } catch (InvocationTargetException e) {
                LOGGER.error("InvocationTargetException {}", e);
                return null;
            } catch (NoSuchMethodException e) {
                LOGGER.error("NoSuchMethodException {}", e);
                return null;
            } catch (Exception e) {
                LOGGER.error("Error Exception {}", e);
                return null;
            }
        }
        return objeto;
    }

    public static <T> T procesar(Class<T> classz, String linea, String separador) {
        if (linea != null && linea.isEmpty()) {
            return null;
        }
        T objeto = null;
        try {
            objeto = classz.newInstance();

        } catch (InstantiationException e) {
            LOGGER.error("Error {}.", e);
            return null;
        } catch (IllegalAccessException e) {
            LOGGER.error("Error {}.", e);
            return null;
        }

        // Validar consistencia de numero de campos en csv y atributos
        Field[] fieldsL = classz.getDeclaredFields();
        int campos = 0;
        for (Field field : fieldsL) {
            FormatCsv format = field.getAnnotation(FormatCsv.class);
            if (format != null) {
                campos++;
            }
        }

        // Separar linea
        Splitter splitter = Splitter.on(CharMatcher.anyOf(separador));
        List<String> lista = splitter.trimResults().splitToList(linea);
        if(lista.size() != campos){
            LOGGER.error("Error inconsistencia en mapeo de csv a objeto {}.", classz.getName());
            return null;
        }
        // Inicar recuperacion de datos
        campos = 0;
        for (Field field : fieldsL) {
            FormatCsv format = field.getAnnotation(FormatCsv.class);
            String propertyName = field.getName();

            if (format == null) {
                continue;
            }
            String value = lista.get(campos);
            try {
                Object propType = PropertyUtils.getPropertyType(objeto, propertyName);

                //Cargar valor a atributo
                if (propType.equals(BigDecimal.class)) {
                    value= value.replace("+", "");
                    Number numero = NumberFormat.getInstance().parse(value);

                    Double fraccion = Math.pow(0.1, format.scale());
                    BigDecimal propValue = new BigDecimal(numero.doubleValue() * fraccion.doubleValue());
                    propValue = propValue.setScale(format.scale(), RoundingMode.HALF_UP);
                    PropertyUtils.setProperty(objeto, propertyName, propValue);
                } else if (propType.equals(BigInteger.class)) {
                    if (StringUtils.trimToEmpty(value).isEmpty()) {
                        value = "0";
                    }
                    value= value.replace("+", "");
                    Number numero = NumberFormat.getInstance().parse(value);
                    BigInteger propValue = BigInteger.valueOf(numero.longValue());
                    PropertyUtils.setProperty(objeto, propertyName, propValue);
                } else if (propType.equals(Integer.class)) {
                    if (StringUtils.trimToEmpty(value).isEmpty()) {
                        value = "0";
                    }
                    Integer propValue = new Integer(value);
                    PropertyUtils.setProperty(objeto, propertyName, propValue);
                }  else if (propType.equals(Short.class)) {
                    if (StringUtils.trimToEmpty(value).isEmpty()) {
                        value = "0";
                    }
                    Short propValue = new Short(value);
                    PropertyUtils.setProperty(objeto, propertyName, propValue);
                } else if (propType.equals(String.class)) {
                    String propValue = value.trim();
                    PropertyUtils.setProperty(objeto, propertyName, propValue);
                } else if (propType.equals(Date.class)) {
                    if (StringUtils.isNotEmpty(format.pattern())) {
                        SimpleDateFormat sdf = new SimpleDateFormat(format.pattern());
                        Date propValue = sdf.parse(value);
                        PropertyUtils.setProperty(objeto, propertyName, propValue);
                    }
                } else if (propType.equals(Character.class)) {
                    if (StringUtils.isNotEmpty(value)) {
                        Character propValue = value.charAt(0);
                        PropertyUtils.setProperty(objeto, propertyName, propValue);
                    } else {
                        PropertyUtils.setProperty(objeto, propertyName, " ".charAt(0));
                    }
                } else if (field.getType().isPrimitive()) {
                    switch (propType.toString()) {
                        case "short":
                            if (StringUtils.trimToEmpty(value).isEmpty()) {
                                PropertyUtils.setSimpleProperty(objeto, propertyName, (new Integer(0)).shortValue());
                            } else {
                                value= value.replace("+", "");
                                PropertyUtils.setSimpleProperty(objeto, propertyName, (new Integer(value.trim())).shortValue());
                            }
                            break;
                        case "int":
                            if (StringUtils.trimToEmpty(value).isEmpty()) {
                                PropertyUtils.setSimpleProperty(objeto, propertyName, (new Integer(0)).intValue());
                            } else {
                                value= value.replace("+", "");
                                PropertyUtils.setSimpleProperty(objeto, propertyName, (new Integer(value.trim())).intValue());
                            }
                            break;
                        case "long":
                            if (StringUtils.trimToEmpty(value).isEmpty()) {
                                PropertyUtils.setSimpleProperty(objeto, propertyName, (new Long(0)).longValue());
                            } else {
                                value= value.replace("+", "");
                                PropertyUtils.setSimpleProperty(objeto, propertyName, (new Long(value)).longValue());
                            }
                            break;
                        case "boolean":
                            PropertyUtils.setSimpleProperty(objeto, propertyName, (new Boolean(value)).booleanValue());
                            break;
                        default:
                            LOGGER.info("Valor por defecto tipo primitivo " + propType);
                            break;
                    }
                } else {
                    LOGGER.error("Error no se puede asignar a campo {} de tipo {} el valor {}.", propertyName, propType, value);
                    return null;
                }
            } catch (IllegalAccessException e) {
                LOGGER.error("IllegalAccessException {}", e);
                e.printStackTrace();
                return null;
            } catch (InvocationTargetException e) {
                LOGGER.error("InvocationTargetException {}", e);
                e.printStackTrace();
                return null;
            } catch (NoSuchMethodException e) {
                LOGGER.error("NoSuchMethodException {}", e);
                e.printStackTrace();
                return null;
            } catch (Exception e) {
                LOGGER.error("Error Exception {}", e);
                e.printStackTrace();
                return null;
            }
            campos++;
        }
        return objeto;
    }
}
