package bus.plumbing.csv.annotation;

/**
 * Created by atenorio on 18/05/2017.
 */
public class FieldWithComment {
    public FieldWithComment(){
    }
    public FieldWithComment(String name, int lenght, String pattern, FormatCsv.formatType  formatType, FormatCsv.alignType  alignType){
        this.name = name;
        this.lenght = lenght;
        this.pattern = pattern;
        this.align = alignType;
        this.format = formatType;
    }

    private String name;
    private int lenght = 50;
    private boolean nullable = true;
    private FormatCsv.alignType align;
    private boolean showInForm = false;//mostrar en la tabla de datos
    private Class<?> type;
    private String pattern;
    //private int validatorType = Validator.STRING;
    private double max;
    private double min;
    private FormatCsv.formatType format;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLenght() {
        return lenght;
    }

    public void setLenght(int lenght) {
        this.lenght = lenght;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public boolean isShowInForm() {
        return showInForm;
    }

    public void setShowInForm(boolean showInForm) {
        this.showInForm = showInForm;
    }

    public Class<?> getType() {
        return type;
    }

    public void setType(Class<?> type) {
        this.type = type;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }



    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }


    public FormatCsv.alignType getAlign() {
        return align;
    }

    public void setAlign(FormatCsv.alignType align) {
        this.align = align;
    }

    public FormatCsv.formatType getFormat() {
        return format;
    }

    public void setFormat(FormatCsv.formatType format) {
        this.format = format;
    }
}
