package bus.plumbing.csv.annotation;

import java.lang.annotation.*;

/**
 * Created by atenorio on 18/05/2017.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface FormatCsv {

    static enum formatType{DECIMAL,INTEGER,TEXT,BOOLEAN}
    static enum alignType{LEFT,RIGTH}
    public alignType align() default alignType.RIGTH;
    public formatType type() default formatType.TEXT;

    public String name() default "";
    public String pattern() default "";
    public int length() default 20;
    public int precision() default 5;
    public int scale() default 2;
    public boolean nullable();
    public String fillSpace() default "";
}