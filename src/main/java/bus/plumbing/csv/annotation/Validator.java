package bus.plumbing.csv.annotation;

import java.lang.annotation.*;

/**
 * Created by atenorio on 18/05/2017.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Validator {
    static final int EMAIL=0,STRING=1,BYTEA=2,URL=3,NUMBER=4,DECIMAL=5;

    public int type() default STRING;
    public String pattern() default "";
    public double max_range() default 0;
    public double min_range() default 0;
    public String[] options() default {};
}