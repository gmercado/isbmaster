/*
 *  Copyright 2009 Banco Bisa S.A.
 * 
 *  Licensed under the Apache License, Version 1.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-1.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.plumbing.components;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

/**
 *
 * @author Marcelo Morales
 */
public class TelefonoCelularValidator extends Behavior implements IValidator<String> {

    private static final long serialVersionUID = 766666L;

    @Override
    public void validate(IValidatable<String> validatable) {
        if (isValid(validatable.getValue())) {
            return;
        }
        validatable.error(new ValidationError("Tel�fono en formato incorrecto"));
    }


    public static boolean isValid(String numeroCelular){
    	if(StringUtils.trimToNull(numeroCelular)==null){
    		return false;
    	}
        if (!StringUtils.isNumeric(numeroCelular)) {
            return false;
        }
        if (numeroCelular.startsWith("5917") && numeroCelular.length() == 11) {
            return true;
        }
        if (numeroCelular.startsWith("5916") && numeroCelular.length() == 11) {
            return true;
        }
        if (numeroCelular.startsWith("7") && numeroCelular.length() == 8) {
            return true;
        }
        if (numeroCelular.startsWith("6") && numeroCelular.length() == 8) {
            return true;
        }    	
    	return false;
    }

}
