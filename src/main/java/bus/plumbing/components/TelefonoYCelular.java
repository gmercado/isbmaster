package bus.plumbing.components;

import org.apache.wicket.model.IModel;

import java.util.HashMap;
import java.util.Map;

public class TelefonoYCelular extends ListAndTextPanel {

    private static final long serialVersionUID = 1L;

    public TelefonoYCelular(String id, IModel<String> primaryModel, IModel<String> primaryLabelModel) {
        super(id, primaryModel, primaryLabelModel);
    }

    @Override
    public Map<String, String> cargaLista(IModel<String> primaryModel) {
        Map<String, String> lista = new HashMap<String, String>();
        lista.put("2", "La Paz, Potos� y Oruro");
        lista.put("3", "Santa Cruz, Trinidad y Cobija");
        lista.put("4", "(Cochabamba, Chuquisaca y Tarija");
        return lista;
    }

}
