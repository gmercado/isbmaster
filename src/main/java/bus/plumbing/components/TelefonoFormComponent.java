package bus.plumbing.components;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.ILabelProvider;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.validation.INullAcceptingValidator;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.ValidationError;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Marcelo Morales
 * @author David Cuevas
 *         Since: 12/31/13
 */
public class TelefonoFormComponent extends Panel implements Serializable, ILabelProvider<String>{

    private static final long serialVersionUID = 1L;

    private IModel<String> modelFono = null;
    private IModel<String> modelFonoLocal = Model.of("");

    // lista de areas
    private DropDownChoice<String> dropDownChoiceTf;
    private IModel<String> selectedModel=Model.of("");
    private IModel<? extends Map<String, String>> validacionesModel;
    
    // texto o numero de celular
    private TextField<String> fonoTf;
    private Model<String> modelSufijo = Model.of("");
    
    // control de listas
    public enum Tipo{        FIJO,CELULAR,AMBOS    }
    private final String prefijosCelulares="[67]";
    private final IModel<? extends Map<String, String>> AMBOS= Model.of(ImmutableMap.of(
            "Celular", prefijosCelulares,
            "Fijo Lpz / Oru / Pts", "2", 
            "Fijo Scz / Ben / Pnd", "3", 
            "Fijo Cba / Suc / Tar", "4" 
            ));
    private final IModel<? extends Map<String, String>> CELULAR= Model.of(ImmutableMap.of(
            "Celular", prefijosCelulares));
    private final IModel<? extends Map<String, String>> FIJO= Model.of(ImmutableMap.of(
            "Fijo Lpz / Oru / Pts", "2", 
            "Fijo Scz / Ben / Pnd", "3", 
            "Fijo Cba / Suc / Tar", "4" 
            ));

    private int anchoComponente = 12; //tamanio ideal para ancho en pixeles ->280;
    private ControlGroupBorder.TipoAncho tipoAnchoComponente=ControlGroupBorder.TipoAncho.SPAN;
    private boolean isRequired=false;
    
    private final TelefonoFormComponent.Tipo tipoListaDeTelefono;

    public TelefonoFormComponent(String id){
        this(id, null, Tipo.AMBOS);
    }

    public TelefonoFormComponent(String id, TelefonoFormComponent.Tipo tipo){
        this(id, null, tipo);
    }
    public TelefonoFormComponent(String id, IModel<String> model, TelefonoFormComponent.Tipo tipo){
        this(id, model, tipo, 12, ControlGroupBorder.TipoAncho.SPAN);
    }
    public TelefonoFormComponent(String id, IModel<String> model, TelefonoFormComponent.Tipo tipo, int ancho, ControlGroupBorder.TipoAncho tipoAncho){
        super(id, model);
        this.anchoComponente=ancho;
        this.tipoAnchoComponente=tipoAncho;
        if (Tipo.FIJO.equals(tipo)){
            this.validacionesModel = FIJO;
        } else if (Tipo.CELULAR.equals(tipo)){
            this.validacionesModel = CELULAR;
        } else if (Tipo.AMBOS.equals(tipo)){
            this.validacionesModel = AMBOS;
        } else {
            this.validacionesModel = null;
        }
        this.tipoListaDeTelefono = tipo;
        this.modelFono = model;
        if (model!=null){
            this.modelFonoLocal.setObject(model.getObject());
        }
    }

    public TelefonoFormComponent(String id, IModel<String> model,
            IModel<? extends Map<String, String>> valsModel){
        this(id, model, valsModel, 12, ControlGroupBorder.TipoAncho.SPAN);
    }
    public TelefonoFormComponent(String id, IModel<String> model,
                                 IModel<? extends Map<String, String>> valsModel, int ancho, ControlGroupBorder.TipoAncho tipoAncho){
        super(id, model);
        this.anchoComponente=ancho;
        this.tipoAnchoComponente=tipoAncho;
        this.validacionesModel = valsModel;
        this.modelFono = model;
        if (model!=null){
            this.modelFonoLocal.setObject(model.getObject());
        }

        this.tipoListaDeTelefono = null;
    }

    protected AlertFeedbackPanel getAlertFeedbackPanel(){
        return null;
    }

    @Override
    public IModel<String> getLabel() {
        return Model.of("");
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onInitialize() {
        super.onInitialize();
        if (modelFono==null){
            modelFono = (IModel<String>) getDefaultModel();
            modelFonoLocal.setObject(modelFono.getObject());
        }
        // Para inicializar los componentes con los valores del modelo
        String prefijo;
        if (modelFono!=null && modelFono.getObject()!=null && validacionesModel!=null && validacionesModel.getObject()!=null){
            prefijo = StringUtils.substring(modelFono.getObject(), 0, 1);
            if (!StringUtils.isEmpty(prefijo)){
                for (String elemento: validacionesModel.getObject().keySet()){
                    if ( validacionesModel.getObject().get(elemento).contains(prefijo) ){
                        selectedModel.setObject(elemento);
                        break;
                    }
                }
                if ( prefijosCelulares.contains(prefijo)){
                    modelSufijo.setObject(modelFono.getObject());
                } else {
                    modelSufijo.setObject(StringUtils.substring(modelFono.getObject(), 1));
                }
            } else {
                selectedModel.setObject(null);
                modelSufijo.setObject(null);
            }
        }

        // Agregando los componentes
        IModel<List<String>> choices = new AbstractReadOnlyModel<List<String>>() {
            private static final long serialVersionUID = -4398018539373403874L;

            @Override
            public List<String> getObject() {
                final Map<String, String> object = validacionesModel.getObject();
                if (object == null) {
                    return null;
                }
                return new LinkedList<String>(object.keySet());
            }
        };

        dropDownChoiceTf = new DropDownChoice<String>("__opt", selectedModel, choices) {
            private static final long serialVersionUID = -8980575242127816063L;

            {
                add(new OnChangeAjaxBehavior() {
                    private static final long serialVersionUID = -4259049379150772676L;

                    @Override
                    protected void onUpdate(AjaxRequestTarget target) {
                        //fonoTf.setConvertedInput("");
                        //fonoTf.setModelObject("");
                        target.add(fonoTf);
                    }
                });
            }
        };
        dropDownChoiceTf.setNullValid(true).setRequired(false);

        fonoTf = newTelComponent("_telefono", modelSufijo);
        fonoTf.setRequired(isRequired());

        add(new ControlGroupBorder("borde", fonoTf, getAlertFeedbackPanel(), new ResourceModel(fonoTf.getId()), anchoComponente, tipoAnchoComponente).
                add(fonoTf).
                add(dropDownChoiceTf));

    }

    protected TextField<String> newTelComponent(final String id, final IModel<String> model) {
        return new TextField<String>(id, model) {
            private static final long serialVersionUID = 1L;
            {
                setOutputMarkupId(true);

                add(new OnChangeAjaxBehavior() {
                    private static final long serialVersionUID = 1L;

                    @Override
                    protected void onUpdate(AjaxRequestTarget target) {
                        // con esto garantizamos que el modelo sea limpiado si no existe fono
                        actualizarModeloFono();
                        //target.add(fonoTf);
                    }
                });

                /*
                add(new AttributeModifier("placeholder", new AbstractReadOnlyModel<Object>() {
                    private static final long serialVersionUID = -9164161406574230572L;

                    @Override
                    public Object getObject() {
                        final String sel = selectedModel.getObject();
                        if (sel != null) {
                            final String s = validacionesModel.getObject().get(sel);
                            if (s!=null && prefijosCelulares.contains(s)){
                                return "NNNNNNNN";
                            }
                            return "NNNNNNN";
                        }
                        return "NNNNNNNN";
                    }
                }));
                */

                add(new INullAcceptingValidator<String>() {

                    private static final long serialVersionUID = 2415665533024852579L;

                    @Override
                    public void validate(IValidatable<String> validatable) {
                        final String value = validatable.getValue();
                        if (StringUtils.trimToNull(value) == null) {
                            return;
                        }
                        actualizarModeloFono();
                        String mensaje = mensajeTelefonoInvalido();
                        if (mensaje!=null){
                            validatable.error(new ValidationError(mensaje));
                        }
                    }
                });
            }
            private void actualizarModeloFono() {
                // si no hay telefono, limpiamos el modelo
                String fono = StringUtils.isEmpty(fonoTf.getInput())?fonoTf.getValue():fonoTf.getInput();
                if (StringUtils.isEmpty(fono)){
                    modelFono.detach();
                    modelFono.setObject(null);
                    modelFonoLocal.setObject("");
                    return;
                }
                // armamos el telefono con el prefijo
                String aux="";
                String sel = selectedModel.getObject();
                if (!StringUtils.isEmpty(sel)) {
                    String prefijo = validacionesModel.getObject().get(sel);
                    aux += StringUtils.isNumeric(prefijo)?prefijo:"";
                }
                aux += fonoTf.getInput();
                modelFono.setObject(aux);
                modelFonoLocal.setObject(aux);
            }
            private String mensajeTelefonoInvalido(){
                String fono = modelFono.getObject();
                if (fono == null){
                    return null;
                }

                String sel = selectedModel.getObject();
                if (StringUtils.isEmpty(sel)) {
                    if (TelefonoFormComponent.Tipo.AMBOS.equals(tipoListaDeTelefono)){
                        return "El tipo o �rea para el tel�fono debe ser especificado.";
                    }
                    if (TelefonoFormComponent.Tipo.CELULAR.equals(tipoListaDeTelefono)){
                        return "El tipo celular debe ser especificado.";
                    }
                    if (TelefonoFormComponent.Tipo.FIJO.equals(tipoListaDeTelefono)){
                        return "El �rea del tel�fono debe ser especificado.";
                    }
                    return "Debe especificar el tipo de tel�fono.";
                }

                String prefijo = validacionesModel.getObject().get(sel);
                String digito = StringUtils.substring(fono, 0, 1);
                if (StringUtils.isNumeric(prefijo)){
                    // es fijo
                    if (fono.length() != 8){
                        return "El tel�fono fijo ingresado debe tener 7 digitos";
                    }
                    if (!(StringUtils.contains(prefijo, digito) && StringUtils.isNumeric(fono))) {
                        return "El tel�fono fijo ingresado es incorrecto";
                    }
                    String digito2 = StringUtils.substring(fono, 1, 2);
                    if (!StringUtils.contains(prefijo, digito2)){
                        return "El tel�fono fijo ingresado debe iniciar en "+prefijo;
                    }
                } else {
                    if (fono.length() != 8){
                        return "El tel�fono celular ingresado debe tener 8 digitos";
                    }
                    // es celular
                    if (!(StringUtils.contains(prefijo, digito) && StringUtils.isNumeric(fono))) {
                        return "El tel�fono celular ingresado es incorrecto";
                    }
                }
                return null;
            }
        };
    }

    public TelefonoFormComponent setAnchoComponente(int anchoComponente) {
        this.anchoComponente = anchoComponente;
        return this;
    }

    public TelefonoFormComponent setTipoAnchoComponente(
            ControlGroupBorder.TipoAncho tipoAnchoComponente) {
        this.tipoAnchoComponente = tipoAnchoComponente;
        return this;
    }

    public TelefonoFormComponent setRequired(boolean isRequired) {
        this.isRequired = isRequired;
        return this;
    }

    public int getAnchoComponente() {
        return anchoComponente;
    }

    public ControlGroupBorder.TipoAncho getTipoAnchoComponente() {
        return tipoAnchoComponente;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public IModel<String> getModelFono() {
        return modelFono;
    }

    public IModel<String> getModelFonoLocal() {
        return modelFonoLocal;
    }

    @Override
    protected void onModelChanged() {
        // TODO Auto-generated method stub
        super.onModelChanged();
    }

    public TextField<String> getComponenteTexto() {
        return fonoTf;
    }

}
