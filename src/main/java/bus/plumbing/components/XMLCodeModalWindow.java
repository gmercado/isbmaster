/*
 *  Copyright 2009 Banco Bisa S.A.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.plumbing.components;

import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.MarkupStream;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.util.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;

/**
 * @author Marcelo Morales
 */
public class XMLCodeModalWindow extends Panel {

    private static final long serialVersionUID = 2398230948L;

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLCodeModalWindow.class);
    
    private XMLCodeModalWindow(String id) {
        super(id);
    }

    public XMLCodeModalWindow(String id, final IModel<String> model) {
        super(id, model);
        add(new MarkupContainer("code") {

            private static final long serialVersionUID = 1L;

            @Override
            public void onComponentTagBody(MarkupStream markupStream, ComponentTag openTag) {
                String object = model.getObject();
                replaceComponentTagBody(markupStream, openTag, getFontificadoXMLString(object));
                //replaceComponentTagBody(markupStream, openTag, getFontificadoXMLfromFileName(object));
            }

        });
    }

    public static XMLCodeModalWindow fromXML(String id, final IModel<String> xmlModel) {
        XMLCodeModalWindow panel = new XMLCodeModalWindow(id);
        panel.add(new MarkupContainer("code") {

            private static final long serialVersionUID = 1L;

            @Override
            public void onComponentTagBody(MarkupStream markupStream, ComponentTag openTag) {
                String xmlReal = xmlModel.getObject();
                replaceComponentTagBody(markupStream, openTag, getFontificadoXMLString(xmlReal));
            }

        });
        return panel;
    }
    
    public static CharSequence getFontificadoXMLString(String string) {
        if (string == null) {
            return "<b>No hay texto para mostrar.</b>";
        }
        Reader reader = null;
        try {
            reader = new StringReader(string);
            InputStream fallback = null;
            try {
                fallback = new ByteArrayInputStream(string.getBytes("UTF-8"));
                return fontificarRealmente(reader, fallback);
            } catch (UnsupportedEncodingException e) {
                LOGGER.error("Ha ocurrido un error inesperado al mostrar XML a partir del string ["+string+"].", e);
                return "<b>Ha ocurrido un error al mostrar el archivo</b>";
            } finally { 
                IOUtils.closeQuietly(fallback);
            }
        } finally {
            IOUtils.closeQuietly(reader);
        }
    }
    
    public static CharSequence getFontificadoXMLfromFileName(String filename) {
        if (filename == null) {
            return "<b>Registro del archivo no se encuentra</b>";
        }
        File file = new File(filename);
        if (!file.exists()) {
            return "<b>No se puede encontrar el archivo llamado '" + filename + "'</b>";
        }
        if (!file.canRead()) {
            return "<b>El archivo '" + filename + "'no se puede leer</b>";
        }
        Reader fileReader = null;
        try {
            fileReader = new InputStreamReader(new FileInputStream(file), "UTF-8");
            InputStream fallback = null;
            try {
                return fontificarRealmente(fileReader, fallback);
            } finally {
                IOUtils.closeQuietly(fallback);
            }
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Ha ocurrido un error inesperado al mostrar XML", e);
            return "<b>Ha ocurrido un error al mostrar el archivo '" + filename + "'</b>";
        } catch (FileNotFoundException e) {
            LOGGER.error("Se ha borrado el archivo", e);
            return "<b>No se puede encontrar el archivo '" + filename + "'</b>";
        } finally {
            IOUtils.closeQuietly(fileReader);
        }
    }
    
    private static CharSequence fontificarRealmente(Reader reader, InputStream fallback) {
        SAXParser parser = newSaxParser();
        if (parser == null) {
            return "<b>Ha ocurrido un error al mostrar el archivo</b>";
        }
        try {
            try {
                StringBuilder appendable = new StringBuilder();
                parser.parse(new InputSource(reader), new FontificarHandler(appendable));
                return appendable;
            } catch (SAXException e) {
                LOGGER.error("Ha ocurrido un error al fontificar archivo XML: mostrandolo CRUDO", e);
                return IOUtils.toString(fallback, "UTF-8");
            }
        } catch (IOException e) {
            LOGGER.error("Ha ocurrido un error de IO al fontificar un archivo", e);
            return "<b>Ha ocurrido un error al mostrar el archivo</b>";
        }
    }

    private static SAXParser newSaxParser() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setValidating(false);
        try {
            factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            factory.setFeature("http://apache.org/xml/features/continue-after-fatal-error", true);
            factory.setFeature("http://xml.org/sax/features/use-entity-resolver2", false);
            return factory.newSAXParser();
        } catch (SAXNotRecognizedException e1) {
            LOGGER.error("Error al mostrar archivo", e1);
            return null;
        } catch (SAXNotSupportedException e1) {
            LOGGER.error("Error al mostrar archivo", e1);
            return null;
        } catch (ParserConfigurationException e1) {
            LOGGER.error("Error al mostrar archivo", e1);
            return null;
        } catch (SAXException e) {
            LOGGER.error("Error al mostrar archivo", e);
            return null;
        }
    }
}
