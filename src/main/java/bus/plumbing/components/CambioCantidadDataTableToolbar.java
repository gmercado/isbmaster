/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package bus.plumbing.components;

import com.google.common.collect.Lists;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.model.IModel;

/**
 * @author Marcelo Morales
 */
public class CambioCantidadDataTableToolbar extends AbstractToolbar {

    private static final long serialVersionUID = 1L;

    public CambioCantidadDataTableToolbar(final DataTable<?, ?> table) {
        super(table);

        WebMarkupContainer td = new WebMarkupContainer("td");
        add(td);

        td.add(AttributeModifier.replace("colspan", String.valueOf(table.getColumns().size())));
        td.add(new DropDownChoice<Integer>("cantidad", new IModel<Integer>() {

            @Override
            public Integer getObject() {
                return (int) table.getItemsPerPage();
            }

            @Override
            public void setObject(Integer object) {
                table.setItemsPerPage(object);
            }

            @Override
            public void detach() {
            }
        }, Lists.newArrayList((int) table.getItemsPerPage(), 10, 20, 50, 100, 200)).
                add(new OnChangeAjaxBehavior() {

                    @Override
                    protected void onUpdate(AjaxRequestTarget target) {
                        target.add(table);
                    }
                }));
    }
}
