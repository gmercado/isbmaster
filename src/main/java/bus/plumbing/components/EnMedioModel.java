/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.plumbing.components;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;

/**
 * @author Marcelo Morales
 * @since 9/4/11
 */
public class EnMedioModel extends AbstractReadOnlyModel<String> {

    private static final long serialVersionUID = -7751145863245522116L;

    private final IModel<String> model;
    private final IModel<String> busquedaModel;

    public EnMedioModel(IModel<String> model, IModel<String> busquedaModel) {
        this.model = model;
        this.busquedaModel = busquedaModel;
    }

    @Override
    public String getObject() {
        String pajar = model.getObject();
        String aguja = busquedaModel.getObject();
        if (pajar == null || "".equals(pajar.trim())) {
            return pajar;
        }
        if (aguja == null || "".equals(aguja.trim())) {
            return StringUtils.abbreviate(pajar, 50);
        }

        int i = pajar.indexOf(aguja);
        if (i < 20) {
            return StringUtils.abbreviate(pajar, 50);
        }

        return StringUtils.abbreviate(pajar, i - 20, 100);
    }
}
