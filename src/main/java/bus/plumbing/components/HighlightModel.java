package bus.plumbing.components;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import org.apache.wicket.model.IModel;

import static com.google.common.base.Strings.isNullOrEmpty;
import static org.apache.wicket.util.string.Strings.escapeMarkup;

/**
 * @author Marcelo Morales
 *         Since: 9/2/13
 */
public class HighlightModel implements IModel<String> {

    private final IModel<String> innerModel;

    private final IModel<String> searchModel;

    private String startMarkup;
    private String endMarkup;

    public HighlightModel(IModel<String> innerModel, IModel<String> searchModel) {
        this.innerModel = innerModel;
        this.searchModel = searchModel;
        startMarkup = "<b>";
        endMarkup = "</b>";
    }

    @Override
    public String getObject() {
        final String search = searchModel.getObject();
        if (isNullOrEmpty(search)) {
            return innerModel.getObject();
        }

        final String content = innerModel.getObject();
        if (isNullOrEmpty(content)) {
            return null;
        }

        Iterable<String> split = Splitter.on(search).split(content);
        return Joiner.on(startMarkup + escapeMarkup(search) + endMarkup).join(split);
    }

    @Override
    public void setObject(String object) {
    }

    @Override
    public void detach() {
        innerModel.detach();
        searchModel.detach();
    }

    public String getStartMarkup() {
        return startMarkup;
    }

    public String getEndMarkup() {
        return endMarkup;
    }

    public HighlightModel setEndMarkup(String endMarkup) {
        this.endMarkup = endMarkup;
        return this;
    }

    public HighlightModel setStartMarkup(String startMarkup) {
        this.startMarkup = startMarkup;
        return this;
    }
}
