/*
 *  Copyright 2009 Banco Bisa S.A.
 * 
 *  Licensed under the Apache License, Version 1.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-1.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.plumbing.components;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.validation.IValidatable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Roger Chura
 * @since   1.0
 */
public class EMailValidator extends AbstractValidator<String> {

    private static final long serialVersionUID = 766666L;

    @Override
    protected void onValidate(IValidatable<String> validatable) {
        if (StringUtils.isEmpty(StringUtils.trimToEmpty(validatable.getValue()))){
            error(validatable);
            return;        	
        }

        if (isValid(validatable.getValue())){
        	return;
        }

        error(validatable);
    }
    
    public static boolean isValid(String email){
    	if (StringUtils.isEmpty(StringUtils.trimToEmpty(email))){
    		return false;
    	}else{
        	String regex = "^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$";
        	Pattern pattern = Pattern.compile(regex);
        	Matcher matcher = pattern.matcher(StringUtils.trimToEmpty(email));
        	return matcher.matches();
    	}
    }
}
