package bus.plumbing.components;

import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.panel.FeedbackPanel;

/**
 * 
 * La clase <code>FeedBackPanelLine.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 08/05/2013, 17:12:36
 *
 */
public class FeedBackPanelLine extends FeedbackPanel {

    private static final long serialVersionUID = 1L;

    public FeedBackPanelLine(String id, IFeedbackMessageFilter filter) {
        super(id, filter);
    }

}
