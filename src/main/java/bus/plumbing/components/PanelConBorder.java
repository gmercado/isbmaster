package bus.plumbing.components;

import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;

import java.io.Serializable;

public abstract class PanelConBorder<T>  extends Panel implements Serializable {
	private static final long serialVersionUID = 2954561877674249345L;
	
	// alertFeedbackPanel para evitar que se muestre el mensaje en el AlertFeedbackPanel de WebBisaWebPage, excluye el mensaje
	public final AlertFeedbackPanel feedbackPanel;

	public PanelConBorder(String id, IModel<T> model, AlertFeedbackPanel alertFeedbackPanel) {
		super(id, model);
		
        setDefaultModel(model);
        setDefaultModelObject(model.getObject());
        this.feedbackPanel = alertFeedbackPanel;
	}

    @Override
    protected void onInitialize() {
        super.onInitialize();
     
    }
    
    protected abstract IModel<T> getTypedModel();
    
    public final void addComponent(Component component){
        add(component);
    }
    
    public final MarkupContainer addComponentConBordeSinResaltado(String idBorde, boolean mostrarNotificacionesEnLaAlertaPrincipal, Component component, int tamanioSpan){
    	
    	AlertFeedbackPanel feedbackPanelLocal = null;
        if (!mostrarNotificacionesEnLaAlertaPrincipal) {
            feedbackPanelLocal = feedbackPanel;
        }
        
        ControlGroupBorder control = new ControlGroupBorder(idBorde, component, feedbackPanelLocal, new ResourceModel(component.getId()), tamanioSpan);
        control.add(component);
        add(control);
        return control.getWmc();
        
    }

    public final MarkupContainer addComponentConBordeSinResaltado(Component component, int tamanioSpan){
        return addComponentConBordeSinResaltado(component.getId()+"_BORDER", false, component, tamanioSpan);
    }
    
    public final MarkupContainer addComponentConBordeSinResaltado(Component component){
        return addComponentConBordeSinResaltado(component, 4);
    }
	
	
}
