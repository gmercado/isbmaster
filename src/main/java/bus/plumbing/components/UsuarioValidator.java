package bus.plumbing.components;

import org.apache.wicket.validation.CompoundValidator;
import org.apache.wicket.validation.validator.PatternValidator;
import org.apache.wicket.validation.validator.StringValidator;

public class UsuarioValidator extends CompoundValidator<String> {

    private static final long serialVersionUID = 1L;

    public UsuarioValidator() {
        add(StringValidator.minimumLength(6));
        add(StringValidator.maximumLength(25));
        add(new PatternValidator("[a-z0-9_]+"));
    }

}
