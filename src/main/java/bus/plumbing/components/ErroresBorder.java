/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.plumbing.components;

import org.apache.wicket.Component;
import org.apache.wicket.feedback.ContainerFeedbackMessageFilter;
import org.apache.wicket.markup.html.border.Border;

/**
 * 
 * La clase <code>ErroresBorder.java</code> tiene la funcion de 
 * mostrar el texto de la validacion a lado del componente
 *
 * @author David Cuevas
 * @version 1.0, 08/05/2013, 13:13:28
 *
 */
public class ErroresBorder extends Border {

    private static final long serialVersionUID = 1L;

    private ContainerFeedbackMessageFilter filter; 
    public ErroresBorder(Component componente) {
        super(componente.getId());
        filter = new ContainerFeedbackMessageFilter(this);
        addToBorder(new AlertFeedbackPanel("feedback", filter));
        //addToBorder(new FeedBackPanelLine("feedback", filter));

        add(componente);
    }
    
    public ErroresBorder(String idBorder, Component componente) {
        super(idBorder);
        filter = new ContainerFeedbackMessageFilter(this);
        addToBorder(new AlertFeedbackPanel("feedback", filter));
        //addToBorder(new FeedBackPanelLine("feedback", filter)); 

        add(componente);
    }

    public ContainerFeedbackMessageFilter getFilter() {
        return filter;
    }
    
}
