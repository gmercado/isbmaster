package bus.plumbing.components;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AbstractAutoCompleteRenderer;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteBehavior;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteSettings;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.Response;
import org.apache.wicket.util.string.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Marcelo Morales
 *         Since: 8/16/13
 */
public class BuscarClienteTextField extends TextField<String> {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(BuscarClienteTextField.class);

    public BuscarClienteTextField(String id) {
        super(id);
    }

    public BuscarClienteTextField(String id, Class<String> type) {
        super(id, type);
    }

    public BuscarClienteTextField(String id, IModel<String> model) {
        super(id, model);
    }

    public BuscarClienteTextField(String id, IModel<String> model, Class<String> type) {
        super(id, model, type);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        setOutputMarkupId(true);

        final AutoCompleteSettings settings = new AutoCompleteSettings();
        settings.setShowListOnEmptyInput(false);

        add(new AutoCompleteBehavior<Map<String, Object>>(new AbstractAutoCompleteRenderer<Map<String, Object>>() {
            private static final long serialVersionUID = 1L;

            @Override
            public void renderHeader(Response response) {
                response.write("<ul class=\"unstyled\">");
            }

            @Override
            public void render(Map<String, Object> object, Response response, String criteria) {
                super.render(object, response, criteria);    //To change body of overridden methods use File | Settings | File Templates.
            }

            @Override
            protected void renderChoice(Map<String, Object> object, Response response, String criteria) {
                String textValue = Strings.escapeMarkup(object.get("CUNA1").toString()).toString();
                response.write(textValue);
            }

            @Override
            protected String getTextValue(Map<String, Object> object) {
                return object.get("CUNBR").toString();
            }

        }, settings) {
            private static final long serialVersionUID = 1L;

            @Override
            protected Iterator<Map<String, Object>> getChoices(String input) {
                if (input.length() < 4) {
                    return Collections.emptyIterator();
                }

                String nombre = StringUtils.upperCase(input);
                try {
                    //Todo: revisar injeccion de Datasource
                    return new QueryRunner().
                            query("SELECT CUNBR, CUNA1 FROM CUP00301 WHERE CUBK=1 AND CUNA1 LIKE ?",
                                    new MapListHandler(), nombre + "%").iterator();
                } catch (SQLException e) {
                    LOGGER.error("Error de sql", e);
                    return Collections.emptyIterator();
                }

            }
        });
    }
}
