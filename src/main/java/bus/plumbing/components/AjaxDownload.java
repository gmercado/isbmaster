package bus.plumbing.components;


import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.AbstractAjaxBehavior;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.resource.IResource;

/**
 * @author Marcelo Morales
 *         Date: 7/24/12
 */
public class AjaxDownload extends AbstractAjaxBehavior {

    private static final long serialVersionUID = 4305196655395128361L;

    private final IResource iResource;

    protected AjaxDownload(IResource iResource) {
        this.iResource = iResource;
    }

    @Override
    public void onRequest() {
        Request request = RequestCycle.get().getRequest();
        Response response = RequestCycle.get().getResponse();
        IResource.Attributes a = new IResource.Attributes(request, response, null);
        iResource.respond(a);
    }

    public void initiate(AjaxRequestTarget target) {
        String url = getCallbackUrl().toString();

        url = url + (url.contains("?") ? "&" : "?");
        url = url + "ac=" + System.currentTimeMillis();

        // the timeout is needed to let Wicket release the channel
        target.appendJavaScript("setTimeout(\"window.location.href='" + url + "'\", 100);");
    }
}
