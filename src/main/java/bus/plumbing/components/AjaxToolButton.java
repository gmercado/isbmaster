package bus.plumbing.components;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.IModel;

import java.io.Serializable;

/**
 * 
 * La clase <code>AjaxToolButton.java</code> contiene metodos 
 * para configurar comodamente la apariencia de un boton
 * <p>&lt;button type="submit" wicket:id="[id]"></p>
 * <p>  &lt;span wicket:id="[id]_icon">&lt;/span></p>
 * <p>&lt;/button></p>
 * 
 *
 * @author David Cuevas
 * @version 1.0, 28/08/2013, 13:59:53
 *
 */
public class AjaxToolButton extends AjaxButton implements Serializable {
    private static final long serialVersionUID = 1L;
    private boolean noIcon=false; 
    private boolean iconLeft=false; 
    private String iconAttribute="";

    public AjaxToolButton(String id, IModel<String> model) {
        super(id, model);
    }

    public AjaxToolButton(String id, IModel<String> model, Form<?> form) {
        super(id, model, form);
    }

    public AjaxToolButton(String id, Form<?> form) {
        super(id, form);
        // TODO Auto-generated constructor stub
    }

    public AjaxToolButton(String id) {
        super(id);
    }

    public boolean isIconLeft() {
        return iconLeft;
    }

    public AjaxToolButton setIconLeft(boolean iconLeft) {
        this.iconLeft = iconLeft;
        return this;
    }
    
    public boolean isNoIcon() {
        return noIcon;
    }

    public AjaxToolButton setNoIcon(boolean noIcon) {
        this.noIcon = noIcon;
        return this;
    }

    public AjaxToolButton setClassAttribute(String att) {
        add(new AttributeAppender("class", att));
        return this;
    }

    public AjaxToolButton setIconAttribute(String att) {
        this.iconAttribute = att;
        return this;
    }

    @Override
    protected void onBeforeRender() {
        Component component = get(getId() + "_icon");
        if (component != null){
            remove(component);
        }
        add(new Component(getId() + "_icon", getDefaultModel()) {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onRender() {
                if (isNoIcon()){
                    getResponse().write(getLabel().getObject());
                    return;
                }
                if (isIconLeft()){
                    getResponse().write("<i class=\""+iconAttribute+"\"></i> "+getLabel().getObject());
                } else {
                    getResponse().write(getLabel().getObject()+" <i class=\""+iconAttribute+"\"></i> ");
                }
            }
        });
        super.onBeforeRender();
    }
 
}