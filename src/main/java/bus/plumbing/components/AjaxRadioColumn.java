/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package bus.plumbing.components;

import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.markup.html.form.RadioGroup;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;

/**
 * @author Marcelo Morales
 */
public class AjaxRadioColumn<T> extends AbstractColumn<T, String> {

    public AjaxRadioColumn(IModel<String> displayModel, String sortProperty) {
        super(displayModel, sortProperty);
    }

    public AjaxRadioColumn(IModel<String> displayModel) {
        super(displayModel);
    }

    protected RadioGroup<T> getRadioGroup() {
        return null;
    }

    @Override
    public void populateItem(Item<ICellPopulator<T>> cellItem, String componentId, IModel<T> rowModel) {
        cellItem.add(new AjaxRadioPanel<T>(componentId, rowModel, getRadioGroup()));
    }
}
