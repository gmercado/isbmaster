/*
 *
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.plumbing.components;

import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.IHeaderContributor;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IFormSubmittingComponent;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * 
 * La clase <code>LinkPanelSubmitter.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 27/02/2013, 19:03:11
 *
 * @param <T>
 */
public abstract class LinkPanelSubmitter<T> extends Panel implements IHeaderContributor, IFormSubmittingComponent {

    private static final long serialVersionUID = 3745595786583746714L;
    private IModel<T> objectModel;

    private boolean defaultFormProcessing = true;

    public LinkPanelSubmitter(String id, IModel<?> labelModel, IModel<T> objectModel) {
        super(id, labelModel);
        add(newLink("link", this.objectModel = objectModel).add(new Label("label", labelModel)));
    }

    protected MarkupContainer newLink(String linkId, IModel<T> objectModel) {
        //noinspection WicketForgeJavaIdInspection
        return new Link<T>(linkId, objectModel) {

            private static final long serialVersionUID = 2184653430920841212L;

            @Override
            public void onClick() {
                LinkPanelSubmitter.this.onSubmit();
            }
        };
    }

    protected abstract void onClick(T object);

    @Override
    public void onSubmit() {
        onClick(objectModel.getObject());
    }

    @Override
    public Form<?> getForm() {
        return Form.findForm(this);
    }

    @Override
    public boolean getDefaultFormProcessing() {
        return defaultFormProcessing;
    }

    @Override
    public void onError() {
        
    }

    @Override
    public Component setDefaultFormProcessing(boolean defaultFormProcessing) {
        if (this.defaultFormProcessing != defaultFormProcessing)
        {
                addStateChange();
        }

        this.defaultFormProcessing = defaultFormProcessing;
        return this;
    }

    @Override
    public String getInputName() {
        String inputName = Form.getRootFormRelativeId(this);
        Form<?> form = findParent(Form.class);

        if (form != null)
        {
                return "" + inputName;
        }
        else
        {
                return inputName;
        }
    }

}
