package bus.plumbing.components;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;

public abstract class AjaxLinkColumn<T> extends AbstractColumn<T, String> {

	private static final long serialVersionUID = -1007615817092948637L;
	public AjaxLinkColumn(IModel<String> displayModel, String sortProperty) {
		super(displayModel, sortProperty);
	}
	public AjaxLinkColumn(IModel<String> displayModel) {
		super(displayModel);
	}

	@Override
	public void populateItem(Item<ICellPopulator<T>> cellItem, String componentId, IModel<T> rowModel) {
		cellItem.add(newAjaxLinkPanel(componentId, rowModel));
	}

	protected AjaxLinkPanel<T> newAjaxLinkPanel(final String componentId, final IModel<T> rowModel) {
		return new AjaxLinkPanel<T>(componentId, createLabelModel(rowModel), rowModel) {

			private static final long serialVersionUID = -8592173780499543512L;

			@Override
			protected void onClick(AjaxRequestTarget target) {
				AjaxLinkColumn.this.onClick(target, getModelObject());
			}

		};
	}

	protected abstract IModel<?> createLabelModel(IModel<T> rowModel);

	protected abstract void onClick(AjaxRequestTarget target, T object);
}
