package bus.plumbing.components;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;

import java.io.Serializable;

/**
 * 
 * La clase <code>AjaxToolButton.java</code> contiene metodos 
 * para configurar comodamente la apariencia de un boton
 * <p>&lt;button type="submit" wicket:id="[id]"></p>
 * <p>  &lt;span wicket:id="[id]_icon">&lt;/span></p>
 * <p>&lt;/button></p>
 * 
 *
 * @author David Cuevas
 * @version 1.0, 23/01/2014, 13:59:53
 *
 */
public abstract class ToolLink<T> extends Link<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private boolean noIcon=false; 
    private boolean iconLeft=false; 
    private String iconAttribute="";

    public ToolLink(String id, IModel<T> model) {
        super(id, model);
    }

    public boolean isIconLeft() {
        return iconLeft;
    }

    public ToolLink<T> setIconLeft(boolean iconLeft) {
        this.iconLeft = iconLeft;
        return this;
    }
    
    public boolean isNoIcon() {
        return noIcon;
    }

    public ToolLink<T> setNoIcon(boolean noIcon) {
        this.noIcon = noIcon;
        return this;
    }

    public ToolLink<T> setClassAttribute(String att) {
        add(new AttributeAppender("class", att));
        return this;
    }

    public ToolLink<T> setIconAttribute(String att) {
        this.iconAttribute = att;
        return this;
    }

    @Override
    protected void onBeforeRender() {
        Component component = get(getId() + "_icon");
        if (component != null){
            remove(component);
        }
        add(new Component(getId() + "_icon", getDefaultModel()) {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onRender() {
                if (isNoIcon()){
                    getResponse().write(getModel().getObject().toString());
                    return;
                }
                if (isIconLeft()){
                    getResponse().write("<i class=\""+iconAttribute+"\"></i> "+getModel().getObject().toString());
                } else {
                    getResponse().write(getModel().getObject().toString()+" <i class=\""+iconAttribute+"\"></i> ");
                }
            }
        });
        super.onBeforeRender();
    }
 
}