package bus.plumbing.components;

import org.apache.wicket.authorization.Action;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeAction;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.model.IModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author rsalvatierra on 23/03/2016.
 */
@AuthorizeAction(action = Action.ENABLE)
public class ButtonAuthorize extends Button {

    private final Set<String> rolesUsuario = AuthenticatedWebSession.get().getRoles();
    private final List<String> rolesAuth;

    public ButtonAuthorize(String id, String rol) {
        super(id);
        this.rolesAuth = new ArrayList<>();
        this.rolesAuth.add(rol);
    }

    public ButtonAuthorize(String id, List<String> rolesAuth) {
        super(id);
        this.rolesAuth = rolesAuth;
    }

    public ButtonAuthorize(String id, IModel<String> model, List<String> rolesAuth) {
        super(id, model);
        this.rolesAuth = rolesAuth;
    }

    @Override
    protected void onBeforeRender() {
        super.onBeforeRender();
        for (String rol : rolesAuth) {
            if (rolesUsuario.contains(rol)) {
                setEnabled(true);
                break;
            }
        }
    }
}
