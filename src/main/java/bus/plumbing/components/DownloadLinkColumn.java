package bus.plumbing.components;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.link.PopupSettings;
import org.apache.wicket.markup.html.link.ResourceLink;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.resource.ResourceStreamResource;
import org.apache.wicket.util.resource.IResourceStream;

public abstract class DownloadLinkColumn<T> extends AbstractColumn<T,String> {

	private static final long serialVersionUID = -1007615817092948637L;

	private String tooltip;

	public DownloadLinkColumn(IModel<String> displayModel, String sortProperty, String tooltip) {
		super(displayModel, sortProperty);
		this.tooltip=tooltip;
	}

	public DownloadLinkColumn(IModel<String> displayModel, String sortProperty) {
		super(displayModel, sortProperty);
	}

	public DownloadLinkColumn(IModel<String> displayModel) {
		super(displayModel);
	}

	@Override
	public void populateItem(Item<ICellPopulator<T>> cellItem, String componentId, IModel<T> rowModel) {
		cellItem.add(newLinkPanel(componentId, rowModel));
	}

	protected LinkPanel<T> newLinkPanel(final String componentId, final IModel<T> rowModel) {
		return new LinkPanel<T>(componentId, createLabelModel(rowModel), rowModel) {

			private static final long serialVersionUID = -8592173780499543512L;

			@Override
			protected AbstractLink newLink(String linkId, IModel<T> objectModel) {
				return DownloadLinkColumn.this.newLink(linkId, objectModel);
			}
		};
	}

	protected AbstractLink newLink(String linkId, IModel<T> model) {
		PopupSettings popupSettings =new PopupSettings("Reporte SEGIP",PopupSettings.RESIZABLE | PopupSettings.SCROLLBARS).setHeight(500).setWidth(700);
		ResourceLink<T> link1=new ResourceLink<>(linkId,new ResourceStreamResource() {
			public IResourceStream getResourceStream() {
				return DownloadLinkColumn.this.getResource(model.getObject());
			}
		});
		link1.add(AttributeModifier.append("title", this.tooltip));
		link1.setPopupSettings(popupSettings);
		return link1;
	}

	protected abstract IModel<?> createLabelModel(IModel<T> rowModel);

	protected abstract IResourceStream getResource(T object);
}
