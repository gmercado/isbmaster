/*
 * Copyright 2012 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.plumbing.components;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

import java.io.Serializable;
import java.util.List;

/**
 * @author Marcelo Morales
 *         Date: 7/14/11
 *         Time: 10:12 AM
 */
public class DropDownPanel<T extends Serializable> extends Panel {

    private static final long serialVersionUID = 161674099504673042L;

    public DropDownPanel(String id, IModel<T> model, List<T> choices) {
        super(id, model);

        add(newDropDown("dropdown", model, choices));
    }

    protected DropDownChoice<T> newDropDown(String id, IModel<T> model, List<T> choices) {
        return new DropDownChoice<T>(id, model, choices);
    }
}
