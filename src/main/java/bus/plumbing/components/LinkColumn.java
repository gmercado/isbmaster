/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.plumbing.components;

import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;

/**
 * @author Marcelo Morales
 * @since 8/30/11
 */
public abstract class LinkColumn<T> extends AbstractColumn<T, String> {

    private static final long serialVersionUID = -1007615817092948637L;

    public LinkColumn(IModel<String> displayModel, String sortProperty) {
        super(displayModel, sortProperty);
    }

    public LinkColumn(IModel<String> displayModel) {
        super(displayModel);
    }

    @Override
    public void populateItem(Item<ICellPopulator<T>> cellItem, String componentId, IModel<T> rowModel) {
        cellItem.add(newLinkPanel(componentId, rowModel));
    }

    protected LinkPanel<T> newLinkPanel(final String componentId, final IModel<T> rowModel) {
        return new LinkPanel<T>(componentId, createLabelModel(rowModel), rowModel) {

            private static final long serialVersionUID = -8592173780499543512L;

            @Override
            protected AbstractLink newLink(String linkId, IModel<T> objectModel) {
                return LinkColumn.this.newLink(linkId, objectModel);
            }

            @Override
            protected void onClick() {
                LinkColumn.this.onClick(getModelObject());
            }
            
        };
    }

    protected AbstractLink newLink(String linkId, IModel<T> model) {
        return new Link<T>(linkId, model) {

            @Override
            public void onClick() {
                LinkColumn.this.onClick(getModelObject());
            }
        };
    }

    protected abstract IModel<?> createLabelModel(IModel<T> rowModel);

    protected abstract void onClick(T object);
}
