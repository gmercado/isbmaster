package bus.plumbing.components;
/*
 *  Copyright 2008 Banco Bisa S.A.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */

import org.apache.wicket.model.IModel;

/**
 *
 * @param <T> x
 * @author David Cuevas
 */
public class TimeStampPropertyColumn<T> extends DateTimePropertyColumn<T> {

    static final long serialVersionUID = 928374289347L;

    public TimeStampPropertyColumn(IModel<String> displayModel, String propertyExpression) {
        super(displayModel, propertyExpression);
    }

    public TimeStampPropertyColumn(IModel<String> displayModel, String sortProperty, String propertyExpression) {
        super(displayModel, sortProperty, propertyExpression);
    }

    public TimeStampPropertyColumn(IModel<String> displayModel, String propertyExpression, int dateStyle) {
        super(displayModel, propertyExpression);
    }

    public TimeStampPropertyColumn(IModel<String> displayModel, String sortProperty, String propertyExpression, int dateStyle) {
        super(displayModel, sortProperty, propertyExpression);
    }

    public TimeStampPropertyColumn(IModel<String> displayModel, String sortProperty, String propertyExpression, int dateStyle,
                                   int timeStyle) {
        super(displayModel, sortProperty, propertyExpression);
    }

    /*@Override
    protected IModel<?> createLabelModel(IModel<T> itemModel) {
        Object value = PropertyResolver.getValue(getPropertyExpression(), itemModel.getObject());
        if (value == null) {
            return new Model<String>("-");
        }
        Date date;
        if (value instanceof Timestamp) {
            date = new Date(((Timestamp) value).getTime());
        } else {
            date = (Date) value;
        }
        return new Model<String>(getDateFormat().format(date));
    }*/
}