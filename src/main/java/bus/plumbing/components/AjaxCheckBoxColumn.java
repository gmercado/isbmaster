/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package bus.plumbing.components;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.ISortableDataProvider;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IStyledColumn;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author Marcelo Morales
 */
public class AjaxCheckBoxColumn<T> implements IStyledColumn<T, String> {

    private static final long serialVersionUID = 1L;

    private final IModel<? extends Collection<T>> collectionModel;

    private final IModel<Boolean> bm;

    private final ISortableDataProvider<T, String> sdp;

    public AjaxCheckBoxColumn(IModel<? extends Collection<T>> collectionModel, ISortableDataProvider<T, String> sdp) {
        this.collectionModel = collectionModel;
        this.sdp = sdp;
        bm = Model.of(false);
    }

    @Override
    public String getCssClass() {
        return null;
    }

    @Override
    public Component getHeader(String componentId) {
        return new CheckBoxPanel(componentId, bm) {

            private static final long serialVersionUID = 8656629546006386060L;

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                DataTable parent = findParent(DataTable.class);
                Collection<T> collection = collectionModel.getObject();
                long first = parent.getCurrentPage() * parent.getItemsPerPage();
                Iterator<? extends T> iterator = sdp.iterator(first, parent.getItemsPerPage());
                while (iterator.hasNext()) {
                    if (bm.getObject()) {
                        collection.add(iterator.next());
                    } else {
                        collection.remove(iterator.next());
                    }
                }
                target.add(parent);
            }
        };
    }

    @Override
    public final String getSortProperty() {
        return null;
    }

    @Override
    public final boolean isSortable() {
        return false;
    }

    @Override
    public void detach() {
        if (collectionModel != null) {
            collectionModel.detach();
        }
    }

    @Override
    public final void populateItem(Item<ICellPopulator<T>> cellItem, String componentId, IModel<T> rowModel) {
        cellItem.add(new AjaxCheckBoxPanel<T>(componentId, rowModel, collectionModel) {

            private static final long serialVersionUID = -3707511236545113456L;

            @Override
            protected void onUpdate(AjaxRequestTarget target, T object) {
                AjaxCheckBoxColumn.this.onUpdate(target, object);
            }
        });
    }

    protected void onUpdate(AjaxRequestTarget target, T objeto) {
        // NOP
    }
}
