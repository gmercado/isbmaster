/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.plumbing.components;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

/**
 * @author Marcelo Morales
 * @since 8/30/11
 */
public abstract class LinkPropertyColumn<T> extends LinkColumn<T> {

    private static final long serialVersionUID = 2843815485921084998L;

    private final String property;

    public LinkPropertyColumn(IModel<String> displayModel, String sortProperty, String property) {
        super(displayModel, sortProperty);
        this.property = property;
    }

    public LinkPropertyColumn(IModel<String> displayModel, String property) {
        super(displayModel);
        this.property = property;
    }

    @Override
    protected IModel<?> createLabelModel(IModel<T> rowModel) {
        return new PropertyModel<Object>(rowModel, property);
    }
}
