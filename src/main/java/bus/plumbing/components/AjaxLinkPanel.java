/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.plumbing.components;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.panel.GenericPanel;
import org.apache.wicket.model.IModel;

/**
 * @author Marcelo Morales
 *         Created: 1/30/12 3:48 PM
 */
public class AjaxLinkPanel<T> extends GenericPanel<T> {

    private static final long serialVersionUID = 1776069400967229486L;

    public AjaxLinkPanel(String id, IModel<?> labelModel, IModel<T> objectModel) {
        super(id, objectModel);
        add(newLink("link", objectModel).setBody(labelModel));
    }

    protected AbstractLink newLink(String linkId, IModel<T> objectModel) {
        return new AjaxLink<T>(linkId, objectModel) {

            private static final long serialVersionUID = -2540701206613184525L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                AjaxLinkPanel.this.onClick(target);
            }
        };
    }

    protected void onClick(AjaxRequestTarget target) {}

}
