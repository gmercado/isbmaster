package bus.plumbing.components;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.feedback.ComponentFeedbackMessageFilter;
import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.FeedbackMessagesModel;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.util.string.Strings;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Marcelo Morales
 * @author David Cuevas
 *         Created: 5/10/12 4:50 PM
 *         Modificado 9/01/14
 */
public class ControlGroupBorder extends Border {

    private static final long serialVersionUID = 1L;

    private final MarkupContainer wmc;
    
    public enum TipoAncho{
        SPAN, WIDTH
    }

        
    public ControlGroupBorder(String id, Component component, AlertFeedbackPanel feedbackPanel, IModel<?> label, int tamanio) {
        this(id, component, feedbackPanel, label, tamanio, TipoAncho.SPAN);
    }
    
    public ControlGroupBorder(String id, Component component, AlertFeedbackPanel feedbackPanel, IModel<?> label, int tamanio, TipoAncho tipoAncho) {
        super(id);
        component.setOutputMarkupId(true);
        ComponentFeedbackMessageFilter cff = new ComponentFeedbackMessageFilter(component);

        if (feedbackPanel != null) {
            feedbackPanel.addFiltroExcluido(cff);
        }
        
        final FeedbackMessagesModel messagesModel;
        setDefaultModel(messagesModel = new FeedbackMessagesModel(this));
        messagesModel.setFilter(cff);

        final String span = tamanio>0 && TipoAncho.SPAN.equals(tipoAncho) ?"span"+tamanio:"";
        final String width = tamanio>0 && TipoAncho.WIDTH.equals(tipoAncho) ?"width:"+tamanio+"px":"";

        wmc = new WebMarkupContainer("__control") {

            private static final long serialVersionUID = 1L;

            @Override
            protected void onComponentTag(ComponentTag tag) {
                super.onComponentTag(tag);

                final FeedbackMessagesModel messagesModel = (FeedbackMessagesModel) ControlGroupBorder.this.getDefaultModel();
                final List<FeedbackMessage> feedbackMessages = messagesModel.getObject();

                if (feedbackMessages.isEmpty()) {
                    tag.put("class", "control-group " + span);
                } else if (Iterables.any(feedbackMessages, new LevelPredicate(FeedbackMessage.ERROR))) {
                    tag.put("class", "mark-alert-error control-group error alert alert-error "+span);
                } else if (Iterables.any(feedbackMessages, new LevelPredicate(FeedbackMessage.WARNING))) {
                    tag.put("class", "mark-alert-warning control-group warning alert alert-warning "+span);
                } else if (Iterables.any(feedbackMessages, new LevelPredicate(FeedbackMessage.SUCCESS))) {
                    tag.put("class", "mark-alert-success control-group success alert alert-success "+span);
                } else if (Iterables.any(feedbackMessages, new LevelPredicate(FeedbackMessage.INFO))) {
                    tag.put("class", "mark-alert-info control-group info alert alert-info "+span);
                } else {
                    tag.put("class", "control-group "+span);
                }
                if (!"".equals(width)){
                    tag.put("style", width);
                }
            }
        };

        wmc.add(new Label("___label", label).add(new AttributeAppender("for", component.getMarkupId())));

        wmc.add(new ListView<FeedbackMessage>("__mensaje", messagesModel) {

            private static final long serialVersionUID = 1L;

            @Override
            protected void populateItem(ListItem<FeedbackMessage> item) {
                item.add(new Component("___mensaje", item.getModel()) {

                    private static final long serialVersionUID = 293847L;

                    @Override
                    protected void onRender() {
                        Object modelObject = getDefaultModelObject();
                        final String label;
                        if (modelObject == null) {
                            label = "";
                        } else {
                            Serializable message = ((FeedbackMessage) modelObject).getMessage();
                            if (message == null) {
                                label = "";
                            } else {
                                label = message.toString();
                            }
                        }
                        getResponse().write(Strings.escapeMarkup(label));
                    }
                    
                });
            }
        });

        addToBorder(wmc);
        wmc.setOutputMarkupId(true);
    }

    public ControlGroupBorder(String id, Component component, AlertFeedbackPanel feedbackPanel, IModel<?> label) {
        this(id, component, feedbackPanel, label, -1);
    }

    public MarkupContainer getWmc() {
        return wmc;
    }

    private static class LevelPredicate implements Predicate<FeedbackMessage> {

        private final int level;

        public LevelPredicate(int level) {
            this.level = level;
        }

        @Override
        public boolean apply(@Nullable FeedbackMessage input) {
            return input != null && input.getLevel() == level;
        }
    }

    
}
