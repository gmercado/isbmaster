/*
 *  Copyright 2009 Banco Bisa S.A.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.plumbing.components;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.validation.IValidatable;

import java.math.BigInteger;
import java.net.InetAddress;
import java.util.Collections;


/**
 *
 * @author Marcelo Morales
 */
public class IPAddressValidator extends AbstractValidator<String> {

    static final long serialVersionUID = 3984238L;

    @Override
    protected void onValidate(IValidatable<String> validatable) {
        String ip = validatable.getValue();
        String[] bytes = StringUtils.split(ip, '.');
        if (bytes.length != 4) {
            error(validatable, "validar.ip", Collections.<String, Object>singletonMap("0", ip));
            return;
        }
        try {
            int o0 = Integer.parseInt(bytes[0]);
            int o1 = Integer.parseInt(bytes[1]);
            int o2 = Integer.parseInt(bytes[2]);
            int o3 = Integer.parseInt(bytes[3]);
            if (o0 < 0 || o0 > 255) {
                error(validatable, "validar.ip", Collections.<String, Object>singletonMap("0", ip));
                return;
            }
            if (o1 < 0 || o1 > 255) {
                error(validatable, "validar.ip", Collections.<String, Object>singletonMap("0", ip));
                return;
            }
            if (o2 < 0 || o2 > 255) {
                error(validatable, "validar.ip", Collections.<String, Object>singletonMap("0", ip));
                return;
            }
            if (o3 < 0 || o3 > 255) {
                error(validatable, "validar.ip", Collections.<String, Object>singletonMap("0", ip));
                return;
            }
            BigInteger bi = BigInteger.valueOf(o0).shiftLeft(8).add(BigInteger.valueOf(o1)).shiftLeft(8).add(
                    BigInteger.valueOf(o2)).shiftLeft(8).add(BigInteger.valueOf(o3));
            byte[] ipreal = bi.toByteArray();
            if (ipreal.length == 5) {
                byte[] nip = new byte[4];
                System.arraycopy(ipreal, 1, nip, 0, 4);
                ipreal = nip;
            }
            InetAddress.getByAddress(ipreal).toString();
        } catch (Exception e) {
            error(validatable, "validar.ip", Collections.<String, Object>singletonMap("0", ip));
        }
    }
}
