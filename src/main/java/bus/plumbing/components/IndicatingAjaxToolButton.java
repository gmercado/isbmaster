package bus.plumbing.components;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.IModel;

import java.io.Serializable;

/**
 * 
 * La clase <code>IndicatingAjaxToolButton.java</code> contiene metodos 
 * para configurar comodamente la apariencia de un boton
 * <p>&lt;button type="submit" wicket:id="[id]"></p>
 * <p>  &lt;span wicket:id="[id]_icon">&lt;/span></p>
 * <p>&lt;/button></p>
 * 
 * @author David Cuevas
 * @version 1.0, 26/06/2013, 10:41:46
 *
 */
public class IndicatingAjaxToolButton extends IndicatingAjaxButton implements Serializable {
    private static final long serialVersionUID = 1L;
    private boolean noIcon=false; 
    private boolean iconLeft=false; 
    private String iconAttribute="";

    public IndicatingAjaxToolButton(String id, Form<?> form) {
        super(id, form);
    }

    public IndicatingAjaxToolButton(String id, IModel<String> model,
            Form<?> form) {
        super(id, model, form);
    }

    public IndicatingAjaxToolButton(String id, IModel<String> model) {
        super(id, model);
    }

    public IndicatingAjaxToolButton(String id) {
        super(id);
    }

    public boolean isIconLeft() {
        return iconLeft;
    }

    public IndicatingAjaxToolButton setIconLeft(boolean iconLeft) {
        this.iconLeft = iconLeft;
        return this;
    }
    
    public boolean isNoIcon() {
        return noIcon;
    }

    public IndicatingAjaxToolButton setNoIcon(boolean noIcon) {
        this.noIcon = noIcon;
        return this;
    }

    public IndicatingAjaxToolButton setClassAttribute(String att) {
        add(new AttributeAppender("class", att));
        return this;
    }

    public IndicatingAjaxToolButton setIconAttribute(String att) {
        this.iconAttribute = att;
        return this;
    }

    @Override
    protected void onBeforeRender() {
        Component component = get(getId() + "_icon");
        if (component != null){
            remove(component);
        }
        add(new Component(getId() + "_icon", getDefaultModel()) {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onRender() {
                if (isNoIcon()){
                    getResponse().write(getLabel().getObject());
                    return;
                }
                if (isIconLeft()){
                    getResponse().write("<i class=\""+iconAttribute+"\"></i> "+getLabel().getObject());
                } else {
                    getResponse().write(getLabel().getObject()+" <i class=\""+iconAttribute+"\"></i> ");
                }
            }
        });
        super.onBeforeRender();
    }
 
}