/*
 *
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.plumbing.components;

import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.link.DownloadLink;
import org.apache.wicket.markup.html.panel.GenericPanel;
import org.apache.wicket.model.IModel;

import java.io.File;

/**
 * Panel que contiene un link, muy �til para columnas de tablas y otros componentes.
 *
 * @author Marcelo Morales
 * @since Wed Aug 28 09:45:17 BOT 2013
 */
public class DownloadLinkPanel<T> extends GenericPanel<T> {

    private static final long serialVersionUID = 1776069400967229486L;

    public DownloadLinkPanel(String id, IModel<?> labelModel, IModel<T> objectModel) {
        super(id, objectModel);
        add(newLink("link", objectModel).setBody(labelModel));
    }

    protected AbstractLink newLink(String linkId, IModel<T> objectModel) {
        File f = new File("reporte.pdf");
        return new DownloadLink(linkId, f, "reporte.pdf") {
            private static final long serialVersionUID = 2184653430920841212L;

            @Override
            public void onClick() {
                DownloadLinkPanel.this.onClick();
            }
        };
    }

    protected void onClick(){}

}