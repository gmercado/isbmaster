package bus.plumbing.components;

import com.google.common.collect.Lists;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * 
 * La clase <code>ListAndTextPanel.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 19/12/2013, 17:07:06
 *
 */
public abstract class ListAndTextPanel extends Panel {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(ListAndTextPanel.class);
    
    private final IModel<String> primaryModel;
    
    private IModel<String> codigoListaModel;
    private IModel<String> textoModel;
    
    private RequiredTextField<String> textField;
    private DropDownChoice<String> listadoField; 
    
    public RequiredTextField<String> getTextField() {
        return textField;
    }

    public DropDownChoice<String> getListadoField() {
        return listadoField;
    }
    
    public ListAndTextPanel(String id, IModel<String> primaryModel, IModel<String> primaryLabelModel) {
        super(id, primaryModel);
        this.primaryModel = primaryModel;
        
        add(newLista("lista", primaryModel ));
        add(newTexto("texto", primaryModel ));
        getTextField().add(new IValidator<String>() {

            private static final long serialVersionUID = 1L;

            @Override
            public void validate(IValidatable<String> validatable) {
                String sufijo = textField.getValue();
                String prefijo = listadoField.getValue();
                LOGGER.debug("Sufijo '{}' + prefijo '{}' ", sufijo, prefijo);
                //validar(textField, listadoField.getValue());
                //TODO : incluir como validar
                ListAndTextPanel.this.primaryModel.setObject(sufijo + prefijo);
            }
        });
    }

    /*public void validar(IValidatable<String> textField, String listadoField) {
        
    }*/

    private RequiredTextField<String> newTexto(String id, final IModel<String> primaryModel) {
        return textField = new RequiredTextField<String>(id, textoModel);
    }
    
    public abstract Map<String, String> cargaLista(IModel<String> primaryModel);

    private DropDownChoice<String> newLista(String id, final IModel<String> primaryModel) {
        
        final LoadableDetachableModel<Map<String, String>> listadoChoicesModel = new LoadableDetachableModel<Map<String, String>>() {
            private static final long serialVersionUID = 1L;

            @Override
            protected Map<String, String> load() {
                return cargaLista(primaryModel);
            }
        };
        return listadoField = new DropDownChoice<String>(id, codigoListaModel,
                new AbstractReadOnlyModel<List<? extends String>>() {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public List<? extends String> getObject() {
                        return Lists.newArrayList(listadoChoicesModel.getObject().keySet());
                    }
                },
                new MapChoiceRenderer<String>(listadoChoicesModel));
    }

    
}
