/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package bus.plumbing.components;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
* @author Marcelo Morales
*         Date: 7/5/12
*/
public class SeleccionColumn<T> extends AbstractColumn<T, String> {

    private static final long serialVersionUID = 1L;

    private final IModel<T> seleccionModel;
    private final Component[] otrosComponentes;
    private final String prefijoUnico;
    private final String tituloBoton;

    public SeleccionColumn(IModel<T> seleccionModel, String prefijoUnico, Component otroComponente) {
        this("", seleccionModel, prefijoUnico, otroComponente);
    }

    public SeleccionColumn(String name, IModel<T> seleccionModel, String prefijoUnico, Component otroComponente) {
        this(name, "seleccionar", seleccionModel, prefijoUnico, otroComponente);
    }

    public SeleccionColumn(String name, String titulo, IModel<T> seleccionModel, String prefijoUnico, Component... otrosComponentes) {
        super(Model.of(name));
        this.tituloBoton = titulo;
        this.seleccionModel = seleccionModel;
        this.otrosComponentes = otrosComponentes;
        this.prefijoUnico = prefijoUnico;
    }

    @Override
    public void populateItem(Item<ICellPopulator<T>> cellItem, String componentId, IModel<T> rowModel) {
        cellItem.add(new LinkPanel<T>(componentId, Model.of(tituloBoton), rowModel) {

            private static final long serialVersionUID = 1L;

            @Override
            protected AbstractLink newLink(String linkId, IModel<T> objectModel) {
                return new AjaxFallbackLink<T>(linkId, objectModel) {

                    private static final long serialVersionUID = 1L;

                    {
                        setOutputMarkupId(true);
                    }

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        seleccionModel.setObject(getModelObject());
                        if (target != null) {
                            target.add(otrosComponentes);
                            target.add(this);
                            target.appendJavaScript("$(\"a." + prefijoUnico + "\").click(function() {\n" +
                                            "     $(\"a.btn-success\").removeClass(\"btn-success\");\n" +
                                            "   });");
                        }
                    }

                    @Override
                    protected void onComponentTag(ComponentTag tag) {
                        super.onComponentTag(tag);
                        if (seleccionModel != null && seleccionModel.getObject() != null &&
                                seleccionModel.getObject().equals(getModelObject())) {
                            tag.put("class", prefijoUnico + " btn btn-success");
                        } else {
                            tag.put("class", prefijoUnico + " btn");
                        }
                    }
                };
            }

            @Override
            protected void onClick() {
                throw new IllegalStateException("ESTO NO DEBERIA CORRER");
            }
        });
    }
}
