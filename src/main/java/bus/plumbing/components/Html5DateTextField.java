/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.plumbing.components;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.model.IModel;

import java.util.Date;

/**
 * @author Marcelo Morales
 */
public class Html5DateTextField extends DateTextField {

    private static final long serialVersionUID = 1113830432584658199L;

    public Html5DateTextField(String id) {
        super(id, "yyyy-MM-dd");
        this.add(new AttributeModifier("style", "width:120px"));
    }

    public Html5DateTextField(String id, IModel<Date> model) {
        super(id, model, "yyyy-MM-dd");
        this.add(new AttributeModifier("style", "width:120px"));
    }

    public Html5DateTextField(String id, IModel<Date> model, String formato) {
        super(id, model, formato);
        this.add(new AttributeModifier("style", "width:120px"));
    }

    /*@Override
    protected String getInputType() {
        return "date";
    }*/
}