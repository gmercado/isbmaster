package bus.plumbing.components;

import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.MarkupStream;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.SubmitLink;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.io.Serializable;

/**
 * @author Marcelo Morales Since: 5/2/13
 */
public abstract class DropDownButton<T> extends Panel {

    private final RepeatingView acciones;

    private final IModel<T> primaryModel;

    public DropDownButton(String id, IModel<T> primaryModel, IModel<String> primaryLabelModel) {
        super(id, primaryModel);
        this.primaryModel = primaryModel;

        add(new AttributeAppender("class", "btn-group"));

        add(newPrimaryButton("accion", primaryLabelModel));

        acciones = new RepeatingView("acciones");
        add(acciones);

        acciones.add(new LinkPanel<T>(acciones.newChildId(), primaryLabelModel, primaryModel) {

            private static final long serialVersionUID = -6999586985312721127L;

            @Override
            protected AbstractLink newLink(String linkId, IModel<T> objectModel) {
                return new SubmitLink(linkId) {
                    private static final long serialVersionUID = -4154621876906472539L;

                    @Override
                    public void onSubmit() {
                        submitPrimary(DropDownButton.this.primaryModel);
                    }
                };
            }

            @Override
            protected void onClick() {
                throw new IllegalStateException();
            }
        });
    }

    protected Button newPrimaryButton(String id, IModel<String> labelModel) {
        Button components = new Button(id) {

            @Override
            public void onSubmit() {
                submitPrimary(primaryModel);
            }

            @Override
            public void onComponentTagBody(MarkupStream markupStream,
                    ComponentTag openTag) {
                super.onComponentTagBody(markupStream, openTag); //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        components.setLabel(labelModel);
        components.add(new Label("Action", labelModel));
        return components;
    }

    protected abstract void submitPrimary(IModel<T> primaryModel);

    public final DropDownButton addSeparator() {
        acciones.add(new WebMarkupContainer(acciones.newChildId())
                .add(new AttributeAppender("class", "divider")));
        return this;
    }

    public final DropDownButton addAction(IModel<?> labelModel, final SerializableRunnable c) {
        acciones.add(new LinkPanel<Serializable>(acciones.newChildId(), labelModel, Model.of()) {

            private static final long serialVersionUID = -1413250928536185963L;

            @Override
            protected void onClick() {
                c.run();
            }
        });
        return this;
    }
    
    
}
