/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package bus.plumbing.components;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.extensions.model.AbstractCheckBoxModel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

import java.util.Collection;

/**
 * @author Marcelo Morales
 */
public abstract class AjaxCheckBoxPanel<S> extends Panel {

    private static final long serialVersionUID = 1L;

    public AjaxCheckBoxPanel(String id, final IModel<S> model, final IModel<? extends Collection<S>> collectionModel) {
        super(id);

        add(new AjaxCheckBox("checkbox", new AbstractCheckBoxModel() {

            private static final long serialVersionUID = 1L;

            @Override
            public boolean isSelected() {
                Collection<S> collection = collectionModel.getObject();
                if (collection == null) {
                    return false;
                }
                S element = model.getObject();
                return element != null && collection.contains(element);
            }

            @Override
            public void select() {
                collectionModel.getObject().add(model.getObject());
            }

            @Override
            public void unselect() {
                collectionModel.getObject().remove(model.getObject());
            }
        }) {
            private static final long serialVersionUID = -3536498834463701041L;

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                AjaxCheckBoxPanel.this.onUpdate(target, model.getObject());
            }
        });
    }

    protected abstract void onUpdate(AjaxRequestTarget target, S object);
}
