/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.plumbing.components;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import org.apache.wicket.Component;
import org.apache.wicket.markup.transformer.AbstractTransformerBehavior;
import org.apache.wicket.model.IModel;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.StringReader;

/**
 * @author Marcelo Morales
 * @since 9/3/11
 */
public class HighlightBehaviour extends AbstractTransformerBehavior {

    private static final long serialVersionUID = -2859105153799939291L;

    private final IModel<String> searchHighlighted;

    public HighlightBehaviour(IModel<String> searchHighlighted) {
        this.searchHighlighted = searchHighlighted;
    }

    @Override
    public CharSequence transform(Component component, CharSequence output) throws Exception {
        String srch = searchHighlighted.getObject();
        if (srch == null || "".equals(srch.trim())) {
            return output;
        }
        StringBuilder stringBuilder = new StringBuilder();

        SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
        saxParser.parse(new InputSource(new StringReader(output.toString())), new HighlightHandler(stringBuilder, srch));

        return stringBuilder;
    }


    /**
     * Este handler es case-sensitive.
     */
    private static class HighlightHandler extends DefaultHandler {

        private final StringBuilder stringBuilder;

        private final String search;

        private final String replace;

        public HighlightHandler(StringBuilder stringBuilder, String search) {
            this.stringBuilder = stringBuilder;
            this.search = search;
            this.replace = "<b>" + search + "</b>";
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String s = String.valueOf(ch, start, length);
            Iterable<String> split = Splitter.on(search).split(s);
            Joiner.on(replace).appendTo(stringBuilder, split);
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            stringBuilder.append("<").append(qName).append(">");
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            stringBuilder.append("</").append(qName).append(">");
        }
    }
}
