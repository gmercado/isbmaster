/*
 *  Copyright 2009 Banco Bisa S.A.
 * 
 *  Licensed under the Apache License, Version 1.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-1.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.plumbing.components;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validacion de password
 * @author Roger Chura
 * @since  1.0
 */
public class PasswordValidator extends Behavior implements IValidator<String> {

    private static final long serialVersionUID = 766666L;

    @Override
    public void validate(IValidatable<String> validatable) {
    	if(logitudAceptable(validatable.getValue())){
            if (contieneLetra(validatable.getValue())) {
                if (contieneNumero(validatable.getValue())) {
                    return;
                }else{
                	validatable.error(new ValidationError("La contraseña debe contener al menos un número."));
                }
            }else{
            	validatable.error(new ValidationError("La contraseña debe contener al menos una letra."));
            }
    	}else{
    		validatable.error(new ValidationError("La contraseña debe tener al menos 6 caracteres."));
    	}
    }

    public static boolean contieneLetra(String password){
    	Pattern p = Pattern.compile("[A-Z|a-z]+");
        Matcher m = p.matcher(password);
        return m.find();
    }
    public static boolean contieneNumero(String password){
    	Pattern p = Pattern.compile("[0-9]+");
        Matcher m = p.matcher(password);
        return m.find();
    }
    public static boolean logitudAceptable(String password){
    	return StringUtils.length(password)>=6; 
    }

}
