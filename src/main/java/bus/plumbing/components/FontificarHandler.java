/*
 *  Copyright 2009 Banco Bisa S.A.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.plumbing.components;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.util.string.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

class FontificarHandler extends DefaultHandler {

    private static final int INDENT = 2;

    private static final Logger LOGGER = LoggerFactory.getLogger(FontificarHandler.class);

    private Map<String, String> nss = new HashMap<String, String>();

    private List<SAXParseException> errores = new LinkedList<SAXParseException>();
    
    private StringBuilder textos = new StringBuilder();

    private boolean brs = false;

    private boolean tagAbierto = false;

    private Appendable appendable;

    private int indent = 0;

    private FontificarHandler(Appendable appendable, int indent) {
        this(appendable);
        this.indent = indent;
    }

    public FontificarHandler(final Appendable appendable) {
        super();
        this.appendable = new Appendable() {

            @Override
            public Appendable append(CharSequence csq, int start, int end) throws IOException {
                appendable.append(csq, start, end);
                brs = false;
                return this;
            }

            @Override
            public Appendable append(char c) throws IOException {
                appendable.append(c);
                brs = false;
                return this;
            }

            @Override
            public Appendable append(CharSequence csq) throws IOException {
                appendable.append(csq);
                brs = false;
                return this;
            }
        };
    }

    @Override
    public void startDocument() throws SAXException {
        try {
            sp();
            appendable.append("<i>");
            appendable.append(Strings.escapeMarkup("<?xml version=\"1.0\"?>"));
            appendable.append("</i>");
        } catch (IOException e) {
            throw new SAXException(e);
        }
    }

    @Override
    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        nss.put(uri, prefix);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        textos = new StringBuilder();
        try {
            if (tagAbierto) {
                appendable.append("<b>");
                appendable.append("&gt;");
                appendable.append("</b>");
            }
            br();
            sp();
            inc();
            appendable.append("<b>");
            appendable.append("&lt;");
            appendable.append("</b>");
            if (!StringUtils.isBlank(uri)) {
                if (nss.containsKey(uri)) {
                    if (!StringUtils.isBlank(nss.get(uri))) {
                        appendable.append("<i style=\"color: #999999\" title=\"");
                        appendable.append(Strings.escapeMarkup(uri));
                        appendable.append("\">");
                        appendable.append(Strings.escapeMarkup(nss.get(uri)));
                        appendable.append("</i>");
                        appendable.append(':');
                    }
                } else {
                    appendable.append("<i style=\"color: #999999\">");
                    appendable.append(Strings.escapeMarkup(uri));
                    appendable.append("</i>");
                    appendable.append(':');
                }
            }
            appendable.append("<i style=\"color: #666666\">");
            appendable.append(Strings.escapeMarkup(localName));
            appendable.append("</i>");

            for (int i = 0; i < attributes.getLength(); i++) {
                if (!StringUtils.isBlank(attributes.getURI(i))) {
                    if (nss.containsKey(attributes.getURI(i))) {
                        if (!StringUtils.isBlank(nss.get(attributes.getURI(i)))) {
                            appendable.append(" <i style=\"color: #999999\" title=\"");
                            appendable.append(Strings.escapeMarkup(attributes.getURI(i)));
                            appendable.append("\">");
                            appendable.append(Strings.escapeMarkup(nss.get(attributes.getURI(i))));
                            appendable.append("</i>");
                            appendable.append(":<i style=\"color: #121212\">");
                        } else {
                            appendable.append(" <i style=\"color: #121212\">");
                        }
                    } else {
                        appendable.append(" <i style=\"color: #999999\">");
                        appendable.append(Strings.escapeMarkup(attributes.getURI(i)));
                        appendable.append("</i>");
                        appendable.append(":<i style=\"color: #121212\">");
                    }
                } else {
                    appendable.append(" <i style=\"color: #121212\">");
                }
                appendable.append(Strings.escapeMarkup(attributes.getLocalName(i)));
                appendable.append("</i>=&quot;");
                appendable.append("<i style=\"color: #999999\">");
                appendable.append(Strings.escapeMarkup(attributes.getValue(i)));
                appendable.append("</i>&quot;");
            }
            tagAbierto = true;
        } catch (IOException e) {
            throw new SAXException(e);
        }
    }

    private void br() throws IOException {
        if (brs) {
            return;
        }
        appendable.append("<br>");
        brs = true;
    }
    
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        textos.append(String.valueOf(ch, start, length));
    }
    
    private void chars() throws SAXException {
        try {
            String texto = StringUtils.trim(textos.toString());
            textos = new StringBuilder();
            if (StringUtils.isEmpty(texto)) {
                return;
            }
            if (tagAbierto) {
                appendable.append("<b>");
                appendable.append("&gt;");
                appendable.append("</b>");
                tagAbierto = false;
            }
            if (StringUtils.startsWith(texto, "<")) {
                LOGGER.debug("Detectando XML en {}", texto);
                SAXParserFactory factory = SAXParserFactory.newInstance();
                factory.setNamespaceAware(true);
                factory.setValidating(false);
                factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
                factory.setFeature("http://apache.org/xml/features/continue-after-fatal-error", true);
                factory.setFeature("http://xml.org/sax/features/use-entity-resolver2", false);
                SAXParser parser;
                parser = factory.newSAXParser();
                appendable.append("<div style=\"background-color: #ffeeee\">");
                try {
                    StringBuilder sb = new StringBuilder();
                    parser.parse(new InputSource(new StringReader(texto)),
                            new FontificarHandler(sb, indent + INDENT));
                    appendable.append(sb);
                } catch (Throwable e) {
                    LOGGER.warn("Error de Parse", e);
                    appendable.append("<b>");
                    appendable.append(Strings.escapeMarkup(texto));
                    appendable.append("</b>");
                }
                appendable.append("</div>");
                sp();
            } else {
                appendable.append("<b>");
                appendable.append(Strings.escapeMarkup(texto));
                appendable.append("</b>");
            }
        } catch (IOException e) {
            throw new SAXException(e);
        } catch (ParserConfigurationException e) {
            throw new SAXException(e);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        dec();
        chars();
        try {
            if (tagAbierto) {
                appendable.append("<b>");
                appendable.append("/&gt;");
                appendable.append("</b>");
                br();
                tagAbierto = false;
                return;
            }
            if (brs) {
              sp();  
            }
            appendable.append("<b>");
            appendable.append("&lt;");
            appendable.append("</b>");
            if (!StringUtils.isEmpty(uri)) {
                if (nss.containsKey(uri)) {
                    if (!StringUtils.isBlank(nss.get(uri))) {
                        appendable.append("<i style=\"color: #999999\" title=\"");
                        appendable.append(Strings.escapeMarkup(uri));
                        appendable.append("\">/");
                        appendable.append(Strings.escapeMarkup(nss.get(uri)));
                        appendable.append("</i>");
                        appendable.append(':');
                    } else {
                        appendable.append("/");
                    }
                } else {
                    appendable.append("<i style=\"color: #999999\">/");
                    appendable.append(Strings.escapeMarkup(uri));
                    appendable.append("</i>");
                    appendable.append(':');
                }
                appendable.append("<i style=\"color: #666666\">");
            } else {
                appendable.append("<i style=\"color: #666666\">/");
            }
            appendable.append(Strings.escapeMarkup(localName));
            appendable.append("</i>");
            appendable.append("<b>");
            appendable.append("&gt;");
            appendable.append("</b>");
            br();
        } catch (IOException e) {
            throw new SAXException(e);
        }
    }

    @Override
    public void endDocument() throws SAXException {
        try {
            if (!errores.isEmpty()) {
                br();
                appendable.append("<b>Errores:</b><ol>");
                for (SAXParseException exception : errores) {
                    appendable.append("<li>[");
                    appendable.append(String.valueOf(exception.getLineNumber()));
                    appendable.append(", ");
                    appendable.append(String.valueOf(exception.getColumnNumber()));
                    appendable.append("]: ");
                    appendable.append(Strings.escapeMarkup(exception.getMessage()));
                    appendable.append("</li>");
                }
                appendable.append("</ol>");
                br();
            }
        } catch (IOException e) {
            throw new SAXException(e);
        }
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        errores.add(e);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        errores.add(e);
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        errores.add(e);
    }

    private void inc() {
        indent += INDENT;
    }

    private void dec() {
        indent -= INDENT;
    }

    private void sp() throws IOException {
        for (int i = 0; i < indent; i++) {
            appendable.append("&nbsp;");
        }
    }

    @Override
    public void notationDecl(String name, String publicId, String systemId) throws SAXException {
        LOGGER.error("Fontificacion de Notacion no implementada {}, {}, {}", new Object[] { name, publicId, systemId });
    }

    @Override
    public void processingInstruction(String target, String data) throws SAXException {
        LOGGER.error("Fontificacion de Proccesing instruction no implementada: {} {}", target, data);
    }

    @Override
    public void unparsedEntityDecl(String name, String publicId, String systemId, String notationName)
            throws SAXException {
        LOGGER.error("Fontificacion de Unparsed NO implementado: {} {} {} {}", new Object[] { name, publicId, systemId,
                notationName });
    }
}
