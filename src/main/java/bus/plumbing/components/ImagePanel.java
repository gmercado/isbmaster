package bus.plumbing.components;

import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.resource.IResource;

public class ImagePanel extends Panel {

    private static final long serialVersionUID = 1L;
    
    private boolean personalizado;
    private int width;
    private int height;
    
    public ImagePanel(String id, IResource iResource) {
        super(id);
        add(newImage("image", iResource));
    }

    protected Image newImage(String id, IResource iResource) {
        return new Image(id, iResource) { 
            
            private static final long serialVersionUID = 1L;

            @Override
            protected void onComponentTag(ComponentTag tag) {
                super.onComponentTag(tag);
                if (width>0) {
                    tag.put("width", String.valueOf(width));
                }
                if (height>0) {
                    tag.put("height", String.valueOf(height));
                }
            }
        };
    }

    public boolean isPersonalizado() {
        return personalizado;
    }

    public void setPersonalizado(boolean personalizado) {
        this.personalizado = personalizado;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    
}
