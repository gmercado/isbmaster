package bus.plumbing.components;

import java.io.Serializable;

/**
 * @author Marcelo Morales
 *         Since: 5/2/13
 */
public abstract class SerializableRunnable implements Serializable, Runnable {
}
