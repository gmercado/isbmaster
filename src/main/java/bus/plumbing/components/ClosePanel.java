package bus.plumbing.components;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;

import java.io.Serializable;

/**
 * @author Marcelo Morales
 *         Since: 9/12/13
 */
public class ClosePanel extends Panel {

    public ClosePanel(String id, Serializable label) {
        super(id);
        add(new Label("body", label));
    }
}
