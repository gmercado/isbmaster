package bus.plumbing.components;

import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.IModel;

import java.util.List;
import java.util.Map;

public class MapChoiceRenderer<T> implements IChoiceRenderer<T> {

    private final IModel<Map<T, String>> valores;

    public MapChoiceRenderer(IModel<Map<T, String>> valores) {
        this.valores = valores;
    }

    @Override
    public Object getDisplayValue(T object) {
        return valores.getObject().get(object);
    }

    @Override
    public String getIdValue(T object, int index) {
        return Integer.toString(index);
    }

    @Override
    public T getObject(String id, IModel<? extends List<? extends T>> choices) {
        throw new IllegalStateException("Method not supported");
        //return valores.getObject().get(id);
    }
}