package bus.plumbing.components;
/*
 *  Copyright 2008 Banco Bisa S.A.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */

import org.apache.wicket.core.util.lang.PropertyResolver;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.joda.time.DateTime;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @param <T> x
 * @author Marcelo Morales modificado por David Cuervas
 */
public class DateTimePropertyColumn<T> extends PropertyColumn<T, String> {

    private DateFormat df;

    static final long serialVersionUID = 928374289347L;

    public DateTimePropertyColumn(IModel<String> displayModel, String propertyExpression) {
        super(displayModel, propertyExpression);
        df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM, new Locale("es", "AR"));
    }

    public DateTimePropertyColumn(IModel<String> displayModel, String sortProperty, String propertyExpression) {
        super(displayModel, sortProperty, propertyExpression);
        df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM, new Locale("es", "AR"));
    }

    public DateTimePropertyColumn(IModel<String> displayModel, String propertyExpression, int dateStyle) {
        super(displayModel, propertyExpression);
        df = DateFormat.getDateInstance(dateStyle);
    }

    public DateTimePropertyColumn(IModel<String> displayModel, String sortProperty, String propertyExpression, int dateStyle) {
        super(displayModel, sortProperty, propertyExpression);
        df = DateFormat.getDateInstance(dateStyle, new Locale("es", "AR"));
    }

    public DateTimePropertyColumn(IModel<String> displayModel, String sortProperty, String propertyExpression, int dateStyle,
            int timeStyle) {
        super(displayModel, sortProperty, propertyExpression);
        df = DateFormat.getDateTimeInstance(dateStyle, timeStyle, new Locale("es", "AR"));
    }

    public final DateFormat getDateFormat() {
        return df;
    }
    
    @Override
    public IModel getDataModel(IModel<T> itemModel) {
        Object value = PropertyResolver.getValue(getPropertyExpression(), itemModel.getObject());
        if (value == null) {
            return Model.of("-");
        }
        Date date;
        if (value instanceof DateTime) {
            date = ((DateTime) value).toDate();
        } else {
            date = (Date) value;
        }
        return new Model<String>(df.format(date));
    }
}
