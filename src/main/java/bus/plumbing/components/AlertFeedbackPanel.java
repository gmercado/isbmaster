/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package bus.plumbing.components;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.apache.wicket.Component;
import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.panel.FeedbackPanel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Marcelo Morales  Created: 5/10/12 5:26 PM
 * @author David Cuevas     Modified 09/05/2013, 18:24:47
 */
public class AlertFeedbackPanel extends FeedbackPanel {

    private final List<IFeedbackMessageFilter> listFilterExcluded = new ArrayList<IFeedbackMessageFilter>();

    public AlertFeedbackPanel(String id) {
        super(id);
        setFilter(new IFeedbackMessageFilter() {

            public boolean accept(final FeedbackMessage message) {
                return !Iterables.any(listFilterExcluded, new IFeedbackMessageFilterPredicate(message));
            }
        });
    }

    public AlertFeedbackPanel(String id, final IFeedbackMessageFilter filter) {
        super(id);
        setFilter(new IFeedbackMessageFilter() {

            @Override
            public boolean accept(FeedbackMessage message) {
                return filter.accept(message) &&
                        !Iterables.any(listFilterExcluded, new IFeedbackMessageFilterPredicate(message));
            }
        });
    }

    boolean porDefecto = true;

    /**
     * Sirve para colocar una clase adicional que muestra "Error", 'info", etc.
     * @param id
     * @param b
     */
    public AlertFeedbackPanel(String id, boolean b) {
        this(id);
        porDefecto = b;
    }

    public AlertFeedbackPanel addFiltroExcluido(IFeedbackMessageFilter filtro) {
        listFilterExcluded.add(filtro);
        return this;
    }

    @Override
    protected String getCSSClass(FeedbackMessage message) {
        if (porDefecto) {
            return getCssClassNormal(message);
        } else {
            return getCssClassAlternativo(message);
        }
    }

    private String getCssClassNormal(FeedbackMessage message) {
        if (message.isError()) {
            return "alert alert-error";
        }

        if (message.isWarning()) {
            return "alert alert-warning";
        }

        if (message.isSuccess()) {
            return "alert alert-success";
        }

        if (message.isInfo()) {
            return "alert alert-info";
        }

        return "alert";
    }

    private String getCssClassAlternativo(FeedbackMessage message) {
        if (message.isError()) {
            return "show-alert-error alert alert-error";
        }

        if (message.isWarning()) {
            return "show-alert-warning alert alert-warning";
        }

        if (message.isSuccess()) {
            return "show-alert-success alert alert-success";
        }

        if (message.isInfo()) {
            return "show-alert-info alert alert-info";
        }

        return "alert";
    }

    private static class IFeedbackMessageFilterPredicate implements Predicate<IFeedbackMessageFilter>, Serializable {


        private final FeedbackMessage message;

        public IFeedbackMessageFilterPredicate(FeedbackMessage message) {
            this.message = message;
        }

        @Override
        public boolean apply(IFeedbackMessageFilter input) {
            return input.accept(message);
        }
    }

    protected Component newMessageDisplayComponent(String id, FeedbackMessage message) {
        Serializable serializable = message.getMessage();
        final ClosePanel components = new ClosePanel(id, (serializable == null) ? "" : serializable.toString());
        components.setEscapeModelStrings(AlertFeedbackPanel.this.getEscapeModelStrings());
        return components;
    }
}
