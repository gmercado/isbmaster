/*
 *  Copyright 2008 Banco Bisa S.A.
 *
 *  Licensed under the Apache License, Version 1.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.plumbing.utils;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Implementacion de par, equivalente a Map.Entry.
 *
 * @param <P> un tipo cualquiera.
 * @param <Q> un tipo cualquiera.
 * @author Marcelo Morales
 */
public class Par<P extends Serializable, Q extends Serializable> implements Serializable {

    static final long serialVersionUID = 9L;

    private P car;

    private Q cdr;

    public static <X extends Serializable, Y extends Serializable> Par<X, Y> de(X x, Y y) {
        return new Par<>(x, y);
    }

    public Par(P car, Q cdr) {
        this.car = car;
        this.cdr = cdr;
    }

    public P getCar() {
        return car;
    }

    public Q getCdr() {
        return cdr;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked") final Par<P, Q> other = (Par<P, Q>) obj;
        if (this.car != other.car && (this.car == null || !this.car.equals(other.car))) {
            return false;
        }
        if (this.cdr != other.cdr && (this.cdr == null || !this.cdr.equals(other.cdr))) {
            return false;
        }
        return true;
    }

    public static <P extends Serializable, Q extends Serializable> Map<P, Q> toMap(Iterable<Par<P, Q>> pares) {
        if (pares == null) {
            return null;
        }
        LinkedHashMap<P, Q> hashMap = new LinkedHashMap<>();
        for (Par<P, Q> par : pares) {
            hashMap.put(par.car, par.cdr);
        }
        return hashMap;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash += 97 * hash + (this.car != null ? this.car.hashCode() : 0);
        hash += 97 * hash + (this.cdr != null ? this.cdr.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(car);
        sb.append(" : ");
        sb.append(cdr);
        return sb.toString();
    }
}
