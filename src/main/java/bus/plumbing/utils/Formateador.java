package bus.plumbing.utils;

import org.apache.commons.lang.StringUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author David Cuevas
 */
public class Formateador {

    private static final NumberFormat FORMATO_DECIMAL;
    private static final NumberFormat FORMATO_DECIMAL_LARGO;
    private static final NumberFormat FORMATO_ENTERO;

    private static final DateFormat FORMATO_DATE_ddMMyyyy;
    private static final DateFormat FORMATO_DATE_ddMMyy;
    private static final DateFormat FORMATO_DATE_ddMMyyhhmm;
    private static final DateFormat FORMATO_DATE_ddMMMMyyyy_hhmm_AM;

    private static final DateFormat FORMATO_FECHA_DECIMAL;
    private static final DateFormat FORMATO_FECHAHORA_DECIMAL;

    private static final DateFormat FORMATO_PERIODO;

    static {
        FORMATO_DECIMAL = NumberFormat.getNumberInstance(new Locale("es"));
        FORMATO_DECIMAL.setMaximumFractionDigits(2);
        FORMATO_DECIMAL.setMinimumFractionDigits(2);
        FORMATO_DECIMAL.setMinimumIntegerDigits(1);

        FORMATO_DECIMAL_LARGO = NumberFormat.getNumberInstance(new Locale("es"));
        FORMATO_DECIMAL_LARGO.setMaximumFractionDigits(7);
        FORMATO_DECIMAL_LARGO.setMinimumFractionDigits(7);
        FORMATO_DECIMAL_LARGO.setMinimumIntegerDigits(1);

        FORMATO_ENTERO = NumberFormat.getNumberInstance(new Locale("es"));
        FORMATO_ENTERO.setMaximumFractionDigits(0);
        FORMATO_ENTERO.setMinimumFractionDigits(0);
        FORMATO_ENTERO.setMinimumIntegerDigits(1);
        FORMATO_ENTERO.setGroupingUsed(false);

        FORMATO_DATE_ddMMyyyy = new SimpleDateFormat("dd/MM/yyyy");
        FORMATO_DATE_ddMMyy = new SimpleDateFormat("dd/MM/yy");
        FORMATO_DATE_ddMMyyhhmm = new SimpleDateFormat("dd/MM/yy HH:mm");
        FORMATO_DATE_ddMMMMyyyy_hhmm_AM = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy HH:mm:ss a", new Locale("es", "ES"));

        FORMATO_FECHA_DECIMAL = new SimpleDateFormat("yyyyMMdd");
        FORMATO_FECHAHORA_DECIMAL = new SimpleDateFormat("yyyyMMdd HHmmss");

        FORMATO_PERIODO = new SimpleDateFormat("yyyyMM");

    }

    public static String getCuentaFormateada(String numeroCuenta) {
        return StringUtils.substring(numeroCuenta, 0, -4) + '-'
                + StringUtils.substring(StringUtils.substring(numeroCuenta, -4), 0, -1) + '-'
                + StringUtils.substring(numeroCuenta, -1);
    }

    public static NumberFormat formatoDecimal() {
        return FORMATO_DECIMAL;
    }

    public static String decimalFormateado(BigDecimal dato) {
        if (dato == null) {
            return FORMATO_DECIMAL.format(BigDecimal.ZERO);
        }
        return FORMATO_DECIMAL.format(dato);
    }

    public static String decimalLargoFormateado(BigDecimal dato) {
        if (dato == null) {
            return FORMATO_DECIMAL_LARGO.format(BigDecimal.ZERO);
        }
        return FORMATO_DECIMAL_LARGO.format(dato);
    }

    public static String enteroFormateado(BigDecimal dato) {
        if (dato == null) {
            return FORMATO_ENTERO.format(BigDecimal.ZERO);
        }
        return FORMATO_ENTERO.format(dato);
    }

    public static String fechaFormateadaConYYYY(Date dato) {
        if (dato == null) {
            return "";
        }
        return FORMATO_DATE_ddMMyyyy.format(dato);
    }

    public static String fechaFormateadaConYYYY(Timestamp dato) {
        if (dato == null) {
            return "";
        }
        return FORMATO_DATE_ddMMyyyy.format(new Date(dato.getTime()));
    }

    public static String fechaFormateadaConYY(Date dato) {
        if (dato == null) {
            return "";
        }
        return FORMATO_DATE_ddMMyy.format(dato);
    }

    public static String fechaHoraFormateadaConYY(Date dato) {
        if (dato == null) {
            return "";
        }
        return FORMATO_DATE_ddMMyyhhmm.format(dato);
    }

    public static String fechaHoraFormateadaConYY(Timestamp dato) {
        if (dato == null) {
            return "";
        }
        return FORMATO_DATE_ddMMyyhhmm.format(new Date(dato.getTime()));
    }

    public static String fechaHoraFormateadaCompleta(Date dato) {
        if (dato == null) {
            return "";
        }
        return FORMATO_DATE_ddMMMMyyyy_hhmm_AM.format(dato);
    }

    public static String fechaHoraFormateadaCompleta(Timestamp dato) {
        if (dato == null) {
            return "";
        }
        return FORMATO_DATE_ddMMMMyyyy_hhmm_AM.format(new Date(dato.getTime()));
    }

    public static Date parsearFechaHora(String cad) {
        try {
            return FORMATO_FECHAHORA_DECIMAL.parse(cad);
        } catch (ParseException e) {
        }
        return null;
    }

    public static Date parsearFecha(String cad) {
        try {
            return FORMATO_FECHA_DECIMAL.parse(cad);
        } catch (ParseException e) {
        }
        return null;
    }

    public static String periodoFormateado(Date dato) {
        if (dato == null) {
            return "";
        }
        return FORMATO_PERIODO.format(dato);
    }

    public static String tarjetaCreditoFormateado(String dato, char separador) {
        if (dato == null) {
            return "";
        }
        String tarjeta = dato;
        if (tarjeta.length() != 16) {
            return tarjeta;
        }
        return StringUtils.substring(tarjeta, 0, -12) + separador
                + StringUtils.substring(tarjeta, 4, -8) + separador
                + StringUtils.substring(tarjeta, 8, -4) + separador
                + StringUtils.substring(tarjeta, -4)
                ;
    }

    public static String tarjetaDebitoFormateado(String dato, char separador) {
        if (dato == null) {
            return "";
        }
        String tarjeta = dato;
        if (tarjeta.length() != 16) {
            return tarjeta;
        }
        return StringUtils.substring(tarjeta, 0, -12) + separador
                + StringUtils.substring(tarjeta, 4, -8) + separador
                + StringUtils.substring(tarjeta, 8, -4) + separador
                + StringUtils.substring(tarjeta, -4)
                ;
    }

    public static String tarjetaDebitoOfuscado(String dato) {
        if (dato == null) {
            return "";
        }
        return ofuscarMedio(dato, 4, 4);
    }

    public static String tarjetaCreditoOfuscado(String dato) {
        if (dato == null) {
            return "";
        }
        return ofuscarMedio(dato, 6, 4);
    }

    public static String tarjetaCreditoOfuscado(String dato, char conCaracter) {
        if (dato == null) {
            return "";
        }
        return ofuscarMedio(dato, 6, 4, conCaracter);
    }

    public static String tarjetaCreditoFormateadoOfuscado(String dato) {
        return tarjetaCreditoFormateado(tarjetaCreditoOfuscado(dato), ' ');
    }

    public static String tarjetaCreditoFormateadoOfuscado(String dato, char conCaracter) {
        return tarjetaCreditoFormateado(tarjetaCreditoOfuscado(dato, conCaracter), ' ');
    }

    public static String tarjetaDebitoFormateadoOfuscado(String dato) {
        return tarjetaDebitoFormateado(tarjetaDebitoOfuscado(dato), '-');
    }

    public static String tarjetaCreditoFormateadoOfuscado(String dato, int desde1, int hasta1, int desde2, int hasta2, char separ) {
        return tarjetaCreditoFormateado(ofuscarDosPosiciones(dato, desde1, hasta1, desde2, hasta2), separ);
    }

    public static String ofuscarMedio(String dato, int izq, int der) {
        if (dato == null) {
            return null;
        }
        StringBuilder str = new StringBuilder();
        return str.append(StringUtils.substring(dato, 0, izq)).append(StringUtils.leftPad(
                StringUtils.substring(dato, -der),
                StringUtils.length(dato) - izq,
                'X')).toString();
    }

    public static String ofuscarMedio(String dato, int izq, int der, char conCaracter) {
        if (dato == null) {
            return null;
        }
        StringBuilder str = new StringBuilder();
        return str.append(StringUtils.substring(dato, 0, izq)).append(StringUtils.leftPad(
                StringUtils.substring(dato, -der),
                StringUtils.length(dato) - izq,
                conCaracter)).toString();
    }

    public static String ofuscarDosPosiciones(String dato, int desde1, int hasta1, int desde2, int hasta2) {
        if (dato == null) {
            return null;
        }
        StringBuilder str = new StringBuilder();
        str.append(StringUtils.leftPad("", StringUtils.length(StringUtils.substring(dato, desde1, hasta1)), '*'));
        str.append(StringUtils.substring(dato, hasta1, desde2 - 1));
        str.append(StringUtils.leftPad("", StringUtils.length(StringUtils.substring(dato, desde2 - 1, hasta2)), '*'));
        str.append(StringUtils.substring(dato, hasta2));

        return str.toString();
    }


    public static String errorFormateadoAStackTrace(Throwable throwable) {
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        throwable.printStackTrace(printWriter);
        return writer.toString();
    }


}
