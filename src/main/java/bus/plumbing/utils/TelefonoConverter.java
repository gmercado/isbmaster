package bus.plumbing.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.util.convert.converter.LongConverter;

import java.text.DecimalFormat;
import java.util.Locale;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class TelefonoConverter extends LongConverter {
    private static final long serialVersionUID = 6171744582333137000L;
    private static final String PREFIX_BO = "591";
    private static final String PREFIX_CEL7 = "7";
    private static final String PREFIX_CEL6 = "6";
    private static final String PATTERN = "########";

    @Override
    public String convertToString(Long value, Locale locale) {
        DecimalFormat fmt = new DecimalFormat(PATTERN);
        return fmt.format(value);
    }

    // Obtiene el celular adicionanado el codigo internacional.
    public static String getCodigoInternacional(String numeroCelular) {

        if (StringUtils.trimToNull(numeroCelular) == null) {
            return "";
        }
        if (numeroCelular.startsWith(PREFIX_BO) && numeroCelular.length() == 11) {
            return numeroCelular;
        } else if ((numeroCelular.startsWith(PREFIX_CEL7) || numeroCelular.startsWith(PREFIX_CEL6))
                && numeroCelular.length() == 8) {
            return PREFIX_BO + numeroCelular;
        } else {
            return numeroCelular;
        }
    }

    //Obtiene el numero de celular ofuscado por el medio, quitando previamente el codigo internacional.
    public static String getOfuscarMedio(String numeroCelular, int izq, int der, char conCaracter) {
        if (numeroCelular == null) {
            return null;
        }

        String dato;
        if (numeroCelular.startsWith(PREFIX_BO) && numeroCelular.length() == 11) {
            dato = StringUtils.substring(numeroCelular, 3);
        } else {
            dato = StringUtils.trimToEmpty(numeroCelular);
        }

        return StringUtils.substring(dato, 0, izq) +
                StringUtils.leftPad(
                        StringUtils.substring(dato, -der),
                        StringUtils.length(dato) - izq,
                        conCaracter);
    }

    public static String getCodigoInternacional(Long celularBeneficiario) {
        return getCodigoInternacional(String.valueOf(celularBeneficiario));
    }
}
