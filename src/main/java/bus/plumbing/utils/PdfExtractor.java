package bus.plumbing.utils;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author rsalvatierra on 09/03/2016.
 */
public class PdfExtractor {

    private static final Logger LOGGER = LoggerFactory.getLogger(PdfExtractor.class);
    private PdfReader pdfReader;
    private byte[] pdfBytes;
    private String textDecode;
    private String textEncode;

    public PdfExtractor(byte[] bytesDecode) {
        setPdfBytes(bytesDecode);
        extract();
    }

    public PdfReader getPdfReader() {
        return pdfReader;
    }

    public void setPdfReader(PdfReader pdfReader) {
        this.pdfReader = pdfReader;
    }

    public byte[] getPdfBytes() {
        return pdfBytes;
    }

    public void setPdfBytes(byte[] pdfBytes) {
        this.pdfBytes = pdfBytes;
    }

    public String getTextDecode() {
        return textDecode;
    }

    public void setTextDecode(String textDecode) {
        this.textDecode = textDecode;
    }

    public String getTextEncode() {
        return textEncode;
    }

    public void setTextEncode(String textEncode) {
        this.textEncode = textEncode;
    }

    public void extract() {
        //this.pdfBytes = Convert.convertStringToBytesBase64(getTextEncode());
        try {
            this.pdfReader = new PdfReader(this.pdfBytes);
            this.textDecode = PdfTextExtractor.getTextFromPage(this.pdfReader, 1);
        } catch (IOException e) {
            LOGGER.error("Error en el proceso de extraccion " + e);
        }
    }

    /**
     * @param path ruta del pdf
     * @throws IOException excepcion en caso de falla del archivo
     */
    public void createPDF(String path) throws IOException {
        OutputStream out = new FileOutputStream(path);
        out.write(this.pdfBytes);
        out.close();
    }
}
