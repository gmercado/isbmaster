/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.plumbing.utils;

/**
 * @author Marcelo Morales
 * @since 12/23/10 11:42 AM
 */
public class Watch {
    private long inicio;
    private boolean iniciado;
    private final ThreadLocal<Long> total;

    public Watch(ThreadLocal<Long> total) {
        this.total = total;
        inicio = 0;
        iniciado = false;
    }

    public void start() {
        inicio = System.currentTimeMillis();
        iniciado = true;
    }

    /**
     * Podemos llamar a stop varias veces, pero solo la primera vez va a tener algun sentido.
     *
     * @return el tiempo total que ha transcurrido desde la ultima vez.
     */
    public long stop() {
        if (!iniciado) {
            return 0;
        }

        Long aLong = total.get();
        if (aLong == null) {
            return 0L;
        }

        long tiempoPasado = System.currentTimeMillis() - inicio;
        synchronized (total) {
            total.set(total.get() + tiempoPasado);
        }
        return tiempoPasado;
    }
}
