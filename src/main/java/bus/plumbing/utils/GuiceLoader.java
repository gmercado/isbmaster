package bus.plumbing.utils;

import bus.plumbing.wicket.BisaWebApplication;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.Application;

/**
 * @author by rsalvatierra on 06/06/2017.
 */
public class GuiceLoader {
    private static final String BISA_WEB_APPLICATION = "BisaWebApplication";

    public static <T> T get(Class<T> clazz) {
        if (StringUtils.contains(Application.get().getClass().getName(), BISA_WEB_APPLICATION)) {
            return BisaWebApplication.get().getInstance(clazz);
        } else {
            throw new IllegalStateException("Imposible obtener la instancia de la clase <" + clazz.getName() + ">.");
        }
    }
}
