package bus.plumbing.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * @author rsalvatierra on 11/04/2016.
 */
public class FechaOperator {

    public static Date sumarRestarFecha(int tipo, Date fecha, int cantidad) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.add(tipo, cantidad);  // numero de días,meses,anios a añadir, o restar en caso de valores < 0
        return calendar.getTime(); // Devuelve el objeto Date con los nuevos días,meses,anios añadidos
    }

    public static Date sumarRestarFecha(String tipo, Date fecha, int cantidad) {
        return sumarRestarFecha(getField(tipo), fecha, cantidad);
    }

    private static int getField(String tipo) {
        switch (TipoFecha.valueOf(tipo)) {
            case DIA:
                return Calendar.DAY_OF_YEAR;
            case MES:
                return Calendar.MONTH;
            case ANIO:
                return Calendar.YEAR;
            default:
                return Calendar.ERA;
        }
    }

    private enum TipoFecha {
        DIA, MES, ANIO
    }

    public static Date getZeroTimeDate(Date fecha) {
        Date res;
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(fecha);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        res = calendar.getTime();

        return res;
    }
}
