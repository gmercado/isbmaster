/*
 *  Copyright 2009 Banco Bisa S.A.
 *
 *  Licensed under the Apache License, Version 1.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.plumbing.utils;

import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * La clase Monedas provee metodos necesario para el manejo de las monedas en
 * format est&aacute;ndar "<b>ISO</b>" o en formato de uso interno del
 * "<b>Banco BISA S.A.</b>"
 *
 * @author "Ricardo Primintela"
 */
public class Monedas {

    private static final Logger LOG = LoggerFactory.getLogger(Monedas.class);

    private Monedas() {
    }

    /**
     * El c&oacute;digo <b>ISO</b> para la moneda Bolivianos
     *
     * @since 1.0
     */
    public static final String ISO_BOLIVIANOS = "BOB";

    /**
     * El c&oacute;digo <b>ISO</b> para la moneda D&oacute;lares
     *
     * @since 1.0
     */
    public static final String ISO_DOLARES = "USD";
    public static final String ISO_EUROS = "EUR";
    public static final String ISO_UFV = "UFV";

    /**
     * El c&oacute;digo de uso interno del <b>Banco BISA S.A.</b> para la moneda
     * Bolivianos
     *
     * @since 1.0
     */
    public static final short BISA_BOLIVIANOS = 0;

    /**
     * El c&oacute;digo de uso interno del <b>Banco BISA S.A.</b> para la moneda
     * D&oacute;lares
     *
     * @since 1.0
     */
    public static final short BISA_DOLARES = 2;

    /**
     * El c&oacute;digo de uso interno del <b>Banco BISA S.A.</b> para la moneda
     * UFVs
     *
     * @since 1.0
     */
    public static final short BISA_UFV = 4;

    /**
     * El c&oacute;digo de uso interno del <b>Banco BISA S.A.</b> para la moneda
     * Euros
     *
     * @since 1.0
     */
    public static final short BISA_EUROS = 11;

    /**
     * Convierte el c&oacute;digo de la moneda <i>DEL</i> formato <b>ISO</b>,
     * <i>AL</i> c&oacute;digo de moneda de uso interno del <b>Banco BISA
     * S.A.</b>
     *
     * @param iso C&oacute;digo de la moneda en formato <b>ISO</b>
     * @return C&oacute;digo de la moneda en el formato interno del <b>Banco
     * BISA S.A.</b>
     */
    public static Short getBisaByISO(String iso) {
        if (iso == null) {
            throw new IllegalArgumentException("No aceptamos la moneda NULL");
        }

        if (ISO_BOLIVIANOS.equalsIgnoreCase(iso.trim())) {
            return BISA_BOLIVIANOS;
        } else if (ISO_DOLARES.equalsIgnoreCase(iso.trim())) {
            return BISA_DOLARES;
        } else {
            LOG.warn("Se intento utilizar una moneda que no es aceptada >> " + iso);
            throw new IllegalArgumentException("No aceptamos la moneda '" + iso + "'");
        }
    }

    public static String getBisaMonedasByISO(String iso) {
        if (iso == null) {
            throw new IllegalArgumentException("No aceptamos la moneda NULL");
        }

        if (ISO_BOLIVIANOS.equalsIgnoreCase(iso.trim())) {
            return "Bolivianos";
        } else if (ISO_DOLARES.equalsIgnoreCase(iso.trim())) {
            return "D\u00f3lares";
        } else {
            LOG.warn("Se intento utilizar una moneda que no es aceptada >> " + iso);
            throw new IllegalArgumentException("No aceptamos la moneda '" + iso + "'");
        }
    }

    /**
     * Devuelve la descripci&oacute;n de la moneda en el formato del
     * c&oacute;digo <b>BISA</b>.
     *
     * @param moneda C&oacute;digo de la moneda en formato <b>BISA</b>
     * @return Descrici&oacute;digo de la moneda en el formato interno del
     * <b>Banco BISA S.A.</b>
     */
    public static String getDescripcionByBisa(short moneda) {
        switch (moneda) {
            case BISA_BOLIVIANOS:
                return "Bolivianos";
            case BISA_DOLARES:
                return "D\u00f3lares";
            case BISA_UFV:
                return "UFVs";
            case BISA_EUROS:
                return "Euros";
            default: {
                LOG.warn("Se intento utilizar una moneda que no es aceptada >> " + moneda);
                throw new IllegalArgumentException("No aceptamos la moneda '" + moneda + "'");
            }
        }
    }

    /**
     * Devuelve la descripci&oacute;n de la moneda en el formato del
     * c&oacute;digo <b>BISA</b>.
     *
     * @param moneda C&oacute;digo de la moneda en formato <b>BISA</b>
     * @return Descrici&oacute;digo de la moneda en el formato interno del
     * <b>Banco BISA S.A.</b>
     */
    public static String getDescripcionISOByBisa(short moneda) {
        switch (moneda) {
            case BISA_BOLIVIANOS:
                return ISO_BOLIVIANOS;
            case BISA_DOLARES:
                return ISO_DOLARES;
            case BISA_UFV:
                return ISO_UFV;
            case BISA_EUROS:
                return ISO_EUROS;
            default: {
                LOG.warn("Se intento utilizar una moneda que no es aceptada >> " + moneda);
                throw new IllegalArgumentException("No aceptamos la moneda '" + moneda + "'");
            }
        }
    }

    /**
     * Devuelve el simbolo de la moneda
     *
     * @param moneda C&oacute;digo de la moneda en formato <b>BISA</b>
     * @return Descrici&oacute;digo de la moneda en el formato interno del
     * <b>Banco BISA S.A.</b>
     */
    public static String getSimbolo(short moneda) {
        switch (moneda) {
            case BISA_BOLIVIANOS:
                return "BOB";
            case BISA_DOLARES:
                return "$US";
            case BISA_UFV:
                return "UFVs";
            case BISA_EUROS:
                return "€";
            default: {
                LOG.warn("Se intento utilizar una moneda que no es aceptada >> " + moneda);
                throw new IllegalArgumentException("No aceptamos la moneda '" + moneda + "'");
            }
        }
    }

    /**
     * Devuelve la descripci&oacute;n de la moneda en el formato del
     * c&oacute;digo <b>BISA</b>.
     *
     * @param moneda C&oacute;digo de la moneda en formato <b>BISA</b>
     * @return Descrici&oacute;digo de la moneda en el formato interno del
     * <b>Banco BISA S.A.</b>
     */
    public static String getDescripcionHumanaByBisa(short moneda) {
        switch (moneda) {
            case BISA_BOLIVIANOS:
                return "Bs.";
            case BISA_DOLARES:
                return "$us.";
            case BISA_UFV:
                return "Ufv.";
            case BISA_EUROS:
                return "Eur.";
            default:
                LOG.warn("Se intento utilizar una moneda que no es aceptada >> " + moneda);
                throw new IllegalArgumentException("No aceptamos la moneda '" + moneda + "'");
        }
    }

    /**
     * Devuelve la descripci&oacute;n de la moneda en el formato del
     * c&oacute;digo <b>BISA</b>.
     *
     * @param codMoneda C&oacute;digo de la moneda en formato <b>BISA</b>
     * @return Descrici&oacute;digo de la moneda en el formato interno del
     * <b>Banco BISA S.A.</b>
     */
    public static String getDescripcionByBisa(String codMoneda) {
        if (codMoneda == null) {
            throw new IllegalArgumentException("No aceptamos la moneda NULL");
        }
        short moneda;
        try {
            moneda = Short.parseShort(codMoneda);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Formato de la moneda incorrecto");
        }
        return getDescripcionByBisa(moneda);
    }

    /**
     * Devuelve la descripci&oacute;n corta de la moneda en el formato del
     * c&oacute;digo <b>BISA</b>.
     *
     * @param moneda C&oacute;digo de la moneda en formato <b>BISA</b>
     * @return Descrici&oacute;digo de la moneda en el formato interno del
     * <b>Banco BISA S.A.</b>
     */
    public static String getDescripcionByBisaCorta(short moneda) {
        switch (moneda) {
            case BISA_BOLIVIANOS:
                return "Bs";
            case BISA_DOLARES:
                return "USD";
            case BISA_UFV:
                return "UFV";
            case BISA_EUROS:
                return "EUR";
            default: {
                LOG.warn("Se intento utilizar una moneda que no es aceptada >> " + moneda);
                throw new IllegalArgumentException("No aceptamos la moneda '" + moneda + "'");
            }
        }
    }

    /**
     * Devuelve la descripci&oacute;n corta de la moneda en el formato del
     * c&oacute;digo <b>BISA</b>.
     *
     * @param codMoneda C&oacute;digo de la moneda en formato <b>BISA</b>
     * @return Descrici&oacute;digo de la moneda en el formato interno del
     * <b>Banco BISA S.A.</b>
     */
    public static String getDescripcionByBisaCorta(String codMoneda) {
        if (codMoneda == null) {
            throw new IllegalArgumentException("No aceptamos la moneda NULL");
        }
        short moneda;
        try {
            moneda = Short.parseShort(codMoneda);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Formato de la moneda incorrecto");
        }
        return getDescripcionByBisaCorta(moneda);
    }

    public static boolean esBolivianos(Short monedaTransaccion) {
        return monedaTransaccion != null && BISA_BOLIVIANOS == monedaTransaccion;
    }

    public static String geDescripcionCodigoMoneda(int moneda) {
        String ret = "Desconocido";
        switch (moneda) {
            case 0: {
                ret = "Boliviano";
                break;
            }
            case 2: {
                ret = "Dolar";
                break;
            }
            case 4: {
                ret = "UFV";
                break;
            }
            case 11: {
                ret = "Euro";
                break;
            }
        }
        return ret;
    }


    public static IModel<? extends List<Short>> todasModel() {
        ArrayList<Short> ar = new ArrayList<>();
        ar.add(BISA_BOLIVIANOS);
        ar.add(BISA_DOLARES);
        ar.add(BISA_EUROS);
        ar.add(BISA_UFV);
        return new Model<>(ar);
    }

    public static IChoiceRenderer<Short> choiceRenderer() {
        return new IChoiceRenderer<Short>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Object getDisplayValue(Short object) {
                return getDescripcionByBisa(object);
            }

            @Override
            public String getIdValue(Short object, int index) {
                return Integer.toString(index);
            }

            @Override
            public Short getObject(String id, IModel<? extends List<? extends Short>> choices) {
                throw new UnsupportedOperationException("Mothod not supported");
            }
        };
    }

    public static boolean esValida(String moneda) {
        return moneda != null && (BISA_BOLIVIANOS == new Short(moneda) || BISA_DOLARES == new Short(moneda));
    }
}
