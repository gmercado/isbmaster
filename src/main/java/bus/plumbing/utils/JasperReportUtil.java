package bus.plumbing.utils;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Map;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class JasperReportUtil implements Serializable {

    private static final String LOGO = "infocred.png";
    private static final String REPORTE = "repPrincipalInfocred";
    private static final Logger log = LoggerFactory.getLogger(JasperReportUtil.class);

    public static byte[] generarReporteStream(final Map<String, Object> parametros, final String reportName, Connection conn) {

        SimpleOutputStreamExporterOutput simpleOutputStreamExporterOutput = null;

        ByteArrayOutputStream byteArrayOutputStream = null;
        String logoPath = (JasperReportUtil.class.getResource("/images/")).getPath();
        String reportPath = (JasperReportUtil.class.getResource("/reports/infocred/")).getPath();
        logoPath += LOGO;
        //reportPath += REPORTE;

        //Seteando parametros por defecto
        parametros.put("logo", logoPath);
        parametros.put("reportes", reportPath);

        //final String jrxmlFileName = reportPath + REPORTE + ".jrxml";
        final String jasperFileName = reportPath + REPORTE + ".jasper";
        //Definiendo el objeto de retorno
        parametros.put("logo", logoPath);
        parametros.put("SUBREPORT_DIR", reportPath);
        try {
            //JasperCompileManager.compileReportToFile(jrxmlFileName, jasperFileName);
            JasperPrint jprint = JasperFillManager.fillReport(jasperFileName, parametros, conn);
            //Definiendo el outputstream
            byteArrayOutputStream = new ByteArrayOutputStream();
            simpleOutputStreamExporterOutput = new SimpleOutputStreamExporterOutput(byteArrayOutputStream);
            //Definiendo el exporter
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(new SimpleExporterInput(jprint));
            exporter.setExporterOutput(simpleOutputStreamExporterOutput);
            exporter.exportReport();
            //Exportando
            return byteArrayOutputStream.toByteArray();
        } catch (JRException e) {
            log.error("Error al generar el reporte Pdf:", e);
        } catch (Exception e) {
            log.error("Error inesperado al generar reporte Pdf:", e);
        } finally {
            try {
                if (byteArrayOutputStream != null) {
                    byteArrayOutputStream.close();
                }
                if (simpleOutputStreamExporterOutput != null) {
                    simpleOutputStreamExporterOutput.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                log.error("Error al tratar de cerrar streams del reporte PDF", e);
            }
        }
        return null;
    }
}
