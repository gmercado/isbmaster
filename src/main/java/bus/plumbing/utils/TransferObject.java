package bus.plumbing.utils;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author josanchez on 16/03/2016.
 * @author rsalvatierra modificado on 30/03/2016
 */
public class TransferObject {

    private Mapper mapper;

    public TransferObject() {
    }

    /**
     * Inicializa los recursos de la clase
     */
    @PostConstruct
    public void initializate() {
        mapper = new DozerBeanMapper();
    }

    /**
     * @param source  origen
     * @param toClass destino
     * @return conversion
     */

    public <F, T> T convert(final F source, final Class<T> toClass) {
        if (source == null)
            return null;
        return mapper.map(source, toClass);
    }

    /**
     * @param source  origen
     * @param toClass destino
     * @return conversion
     */
    public <T, U> List<U> convert(final List<T> source, final Class<U> toClass) {

        if (source == null) {
            return null;
        }
        final List<U> dest = new ArrayList<>();

        for (T element : source) {
            dest.add(mapper.map(element, toClass));
        }

        return dest;
    }
}
