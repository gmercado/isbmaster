package bus.plumbing.utils;

import org.apache.wicket.request.resource.DynamicImageResource;
import org.apache.wicket.request.resource.IResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * @author rsalvatierra on 21/03/2016.
 */
public class Imagen {

    private static final Logger LOGGER = LoggerFactory.getLogger(Imagen.class);

    public static IResource convertToResource(final byte[] imagen) {
        IResource imageResource = null;
        Boolean isImage;
        try {
            isImage = ImageIO.read(new ByteArrayInputStream(imagen)) != null;
            if (isImage) {
                imageResource = new DynamicImageResource() {
                    @Override
                    protected byte[] getImageData(Attributes attributes) {
                        return imagen;
                    }
                };
            }
        } catch (IOException e) {
            LOGGER.error("Error al convertir la imagen {}", e);
        }
        return imageResource;
    }
}
