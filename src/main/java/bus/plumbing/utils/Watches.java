/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.plumbing.utils;

/**
 * @author Marcelo Morales
 * @since 1/4/11 6:48 AM
 */
public interface Watches {

    ThreadLocal<Long> MONITOR_MILIS = new ThreadLocal<Long>();

    ThreadLocal<Long> MONITOR_400 = new ThreadLocal<Long>();

    ThreadLocal<Long> FORMATOS = new ThreadLocal<Long>();

    ThreadLocal<Long> DB_MILIS_PRINCIPAL = new ThreadLocal<Long>();

    ThreadLocal<Long> DB_MILIS_AUXILIAR = new ThreadLocal<Long>();

    ThreadLocal<Long> DB_MILIS_AUXILIAR2 = new ThreadLocal<Long>();

    ThreadLocal<Long> DB_MILIS_AUXILIAR3 = new ThreadLocal<Long>();

    ThreadLocal<Long> DBPOOL_MILIS = new ThreadLocal<Long>();

    ThreadLocal<Long> CONSUMO_WS = new ThreadLocal<Long>();
}
