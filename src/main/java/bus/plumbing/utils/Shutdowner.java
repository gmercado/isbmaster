/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.plumbing.utils;

import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Stack;

/**
 * @author Marcelo Morales
 * @since Oct 28, 2010 9:31:57 PM
 */
@Singleton
public class Shutdowner {

    private static final Logger LOGGER = LoggerFactory.getLogger(Shutdowner.class);

    private final Stack<Runnable> stack = new Stack<Runnable>();

    public void register(Runnable call) {
        stack.add(call);
    }

    public void shutdown() {
        while (!stack.isEmpty()) {
            Runnable stringCallable = stack.pop();
            try {
                stringCallable.run();
            } catch (Exception e) {
                LOGGER.error("Error al cerrar", e);
            }
        }
    }
}
