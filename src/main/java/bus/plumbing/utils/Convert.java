package bus.plumbing.utils;

import com.itextpdf.text.pdf.codec.Base64;
import org.apache.wicket.util.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author rsalvatierra on 09/03/2016.
 */
public class Convert {
    private static final String DD_MM_YYYY = "dd/MM/yyyy";
    private static final String YYYY_DDD = "yyyyDDD";

    public static String convertBytesToStringBase64(byte[] bytes) {
        return Base64.encodeBytes(bytes);
    }

    public static byte[] convertStringToBytesBase64(String text) {
        return Base64.decode(text);
    }

    public static Date fromJulian(String injulian) {
        try {
            return new SimpleDateFormat("yyyyD").parse(injulian);
        } catch (ParseException e) {
            return null;
        }
    }

    public static BigDecimal convertDateToJulian(String pFechaNacimiento) {
        SimpleDateFormat sdf = new SimpleDateFormat(DD_MM_YYYY);
        SimpleDateFormat sdfIso = new SimpleDateFormat(YYYY_DDD);
        BigDecimal bdJulian;
        try {
            Date dt = sdf.parse(pFechaNacimiento);
            String julianDt = sdfIso.format(dt);
            bdJulian = new BigDecimal(julianDt);
        } catch (ParseException e) {
            bdJulian = BigDecimal.ZERO;
        }
        return bdJulian;
    }

    public static byte[] FileToBytes(File file) {
        byte[] bfile = null;
        try {
            if (file != null) {
                bfile = new byte[(int) file.length()];
                FileInputStream fileInputStream = new FileInputStream(file);
                fileInputStream.read(bfile);
                fileInputStream.close();
            }
        } catch (Exception e) {
            return null;
        }
        return bfile;
    }

    public static byte[] FileToBytes2(File file) {
        FileReader fileReader = null;
        byte[] bfile = null;
        try {
            if (file != null) {
                fileReader = new FileReader(file);
                bfile = IOUtils.toByteArray(fileReader);
            }
        } catch (Exception e) {
            return null;
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bfile;
    }
}
