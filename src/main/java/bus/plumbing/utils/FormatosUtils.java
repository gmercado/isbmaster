/*
 *  Copyright 2009 Banco Bisa S.A.
 *
 *  Licensed under the Apache License, Version 1.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.plumbing.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.apache.wicket.util.string.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * @author Marcelo Morales
 * @author Ricardo Primintela
 * @author rsalvatierra on 20/10/2017
 */
public class FormatosUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(FormatosUtils.class);

    private static final String FILE_SEPARATOR = System.getProperty("file.separator");
    private static final long unDia = 86400000L;
    private static final NumberFormat FORMATO_DECIMAL;
    private static final NumberFormat FORMATO_DECIMAL_MEDIANO;
    private static final NumberFormat FORMATO_DECIMAL_LARGO;
    private static final NumberFormat FORMATO_ENTERO;
    private static final DateFormat FORMATO_DATE_ddMMyyyy;
    private static final DateFormat FORMATO_DATE_ddMMyy;
    private static final DateFormat FORMATO_DATE_ddMMyyhhmm;
    private static final DateFormat FORMATO_DATE_ddMMyyyyhhmmss;
    private static final DateFormat FORMATO_PERIODO;
    private static final DateFormat FORMATO_DATE_yyyyMMdd;
    private static final DateFormat FORMATO_DATE_yyMMdd;
    private static final DateFormat FORMATO_HORA_HHmmss;
    private static final DateFormat FORMATO_HORA_HH_mm_ss;
    private static final SimpleDateFormat FORMATO_LARGO_PERIODO;
    private static final SimpleDateFormat FORMATO_CORTO_PERIODO;
    private static final SimpleDateFormat FORMATO_FECHA_HORA_LARGA;
    private static final SimpleDateFormat FORMATO_FECHA_LARGA;
    private static final SimpleDateFormat FORMATO_LITERAL;
    private static final int[] DIAS_FIN_DE_SEMANA = {1, 7};

    private static final String LANGUAGE_EN = "en";

    static {
        FORMATO_DECIMAL = NumberFormat.getNumberInstance(new Locale(LANGUAGE_EN));
        // FORMATO_DECIMAL = NumberFormat.getNumberInstance(new Locale("es"));
        FORMATO_DECIMAL.setMaximumFractionDigits(2);
        FORMATO_DECIMAL.setMinimumFractionDigits(2);
        FORMATO_DECIMAL.setMinimumIntegerDigits(1);
        FORMATO_DECIMAL.setGroupingUsed(false);

        FORMATO_DECIMAL_MEDIANO = NumberFormat.getNumberInstance(new Locale(LANGUAGE_EN));
        // FORMATO_DECIMAL_MEDIANO = NumberFormat.getNumberInstance(new
        // Locale("es"));
        FORMATO_DECIMAL_MEDIANO.setMaximumFractionDigits(4);
        FORMATO_DECIMAL_MEDIANO.setMinimumFractionDigits(4);
        FORMATO_DECIMAL_MEDIANO.setMinimumIntegerDigits(1);
        FORMATO_DECIMAL.setGroupingUsed(false);

        FORMATO_DECIMAL_LARGO = NumberFormat.getNumberInstance(new Locale(LANGUAGE_EN));
        // FORMATO_DECIMAL_LARGO = NumberFormat.getNumberInstance(new
        // Locale("es"));
        FORMATO_DECIMAL_LARGO.setMaximumFractionDigits(7);
        FORMATO_DECIMAL_LARGO.setMinimumFractionDigits(7);
        FORMATO_DECIMAL_LARGO.setMinimumIntegerDigits(1);
        FORMATO_DECIMAL.setGroupingUsed(false);

        FORMATO_ENTERO = NumberFormat.getNumberInstance(new Locale(LANGUAGE_EN));
        // FORMATO_ENTERO = NumberFormat.getNumberInstance(new Locale("es"));
        FORMATO_ENTERO.setMaximumFractionDigits(0);
        FORMATO_ENTERO.setMinimumFractionDigits(0);
        FORMATO_ENTERO.setMinimumIntegerDigits(1);
        FORMATO_ENTERO.setGroupingUsed(false);

        FORMATO_DATE_yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
        FORMATO_DATE_yyMMdd = new SimpleDateFormat("yyMMdd");
        FORMATO_DATE_ddMMyyyy = new SimpleDateFormat("dd/MM/yyyy");
        FORMATO_DATE_ddMMyy = new SimpleDateFormat("dd/MM/yy");
        FORMATO_DATE_ddMMyyhhmm = new SimpleDateFormat("dd/MM/yy HH:mm");

        FORMATO_PERIODO = new SimpleDateFormat("yyyyMM");
        FORMATO_HORA_HHmmss = new SimpleDateFormat("HHmmss");
        FORMATO_HORA_HH_mm_ss = new SimpleDateFormat("HH:mm:ss");

        FORMATO_LARGO_PERIODO = new SimpleDateFormat("MMMM/yyyy");
        FORMATO_CORTO_PERIODO = new SimpleDateFormat("MM/yyyy");
        FORMATO_FECHA_LARGA = new SimpleDateFormat("yyyy-MM-dd");
        FORMATO_FECHA_HORA_LARGA = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        FORMATO_DATE_ddMMyyyyhhmmss = new SimpleDateFormat("ddMMyyyyHHmmss");

        FORMATO_LITERAL = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", new Locale("es", "ES"));

    }


    private FormatosUtils() {
    }

    public static String formatoCuentaBisa(String cuenta) {
        BigInteger cta = BigInteger.ZERO;
        cta = getCtaInteger(cuenta, cta);
        return formatoCuentaBisa(cta);
    }

    private static String formatoCuentaBisa(BigInteger cta) {
        String cuenta = "0000000000";
        cuenta = getLeftPadCuenta(cta, cuenta);

        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.substring(cuenta, 0, 6));
        sb.append("-");
        sb.append(StringUtils.substring(cuenta, 6, 9));
        sb.append("-");
        sb.append(StringUtils.substring(cuenta, 9, 10));

        return sb.toString();
    }

    private static String getLeftPadCuenta(BigInteger cta, String cuenta) {
        if (cta != null) {
            if (cta.toString().length() <= 10) {
                cuenta = StringUtils.leftPad(cta.toString(), 10, '0');
            } else {
                LOGGER.warn("Cuenta '{}' invalida. No se puede parsear. Parseando valor por defecto '0'", cta);
            }
        } else {
            LOGGER.warn("Cuenta invalida. No se puede parsear. Parseando valor por defecto '0'");
        }
        return cuenta;
    }

    public static String formatoCuentaBisaMask(String cuenta) {
        BigInteger cta = BigInteger.ZERO;
        cta = getCtaInteger(cuenta, cta);
        return formatoCuentaBisaMask(cta);
    }

    private static BigInteger getCtaInteger(String cuenta, BigInteger cta) {
        if (StringUtils.trimToNull(cuenta) != null) {
            if (StringUtils.isNumeric(cuenta)) {
                cta = new BigInteger(StringUtils.trim(cuenta));
            } else {
                LOGGER.warn("Cuenta '{}' invalida. No se puede parsear. Parseando valor por defecto '0'", cuenta);
            }
        } else {
            LOGGER.warn("Cuenta invalida. No se puede parsear. Parseando valor por defecto '0'");
        }
        return cta;
    }

    private static String formatoCuentaBisaMask(BigInteger cta) {
        String cuenta = "0000000000";
        cuenta = getLeftPadCuenta(cta, cuenta);

        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.leftPad("", 6, '*'));
        sb.append(StringUtils.substring(cuenta, 6, 10));
        return sb.toString();
    }

    public static String formatoCuentaExterna(String cuenta) {
        if (cuenta.length() <= 4) {
            return cuenta;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.leftPad("", cuenta.length() - 4, '*'));
        sb.append(StringUtils.right(cuenta, 4));
        return sb.toString();
    }

    public static String formatoCuentaContableBisa(String cuenta) {
        if ("0".equalsIgnoreCase(cuenta)) {
            return "0";
        }
        String parse = "";
        String aux1 = "";
        String aux2 = "";

        aux1 = cuenta.substring(cuenta.length() - 4, cuenta.length());
        aux2 = cuenta.substring(0, cuenta.length() - 4);
        parse = aux2 + "-" + aux1;

        aux1 = parse.substring(parse.length() - 6, parse.length());
        aux2 = parse.substring(0, parse.length() - 6);
        cuenta = aux2 + "-" + aux1;

        aux1 = cuenta.substring(cuenta.length() - 9, cuenta.length());
        aux2 = cuenta.substring(0, cuenta.length() - 9);
        parse = aux2 + "-" + aux1;
        cuenta = parse;

        aux1 = cuenta.substring(cuenta.length() - 6, cuenta.length());
        aux2 = cuenta.substring(0, cuenta.length() - 6);

        String finalStr = "";
        StringBuilder sb = new StringBuilder("");
        int largo = aux2.length();
        if (largo < 3) {
            for (int j = 0; j < (3 - largo); j++) {
                sb.append("0");
            }
        }
        finalStr = sb.toString();
        parse = finalStr + aux2 + aux1;
        return parse;
    }

    public static String formatoFecha(Date fecha) {
        if (fecha == null) {
            return "-";
        }
        String ret = "-";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            ret = sdf.format(fecha);
        } catch (Exception e) {
            LOGGER.error("No se pudo parsear fecha >> " + fecha.toString(), e);
        }
        return ret;
    }

    public static String formatoFechaHora(Date fecha) {
        if (fecha == null) {
            return "-";
        }
        String ret = "-";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            ret = sdf.format(fecha);
        } catch (Exception e) {
            LOGGER.error("No se pudo parsear fecha >> " + fecha.toString(), e);
        }
        return ret;
    }

    public static String formatoFechaHoraLarga(Date fecha) {
        if (fecha == null) {
            return "-";
        }
        String ret = "-";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            ret = sdf.format(fecha);
        } catch (Exception e) {
            LOGGER.error("No se pudo parsear fecha >> " + fecha.toString(), e);
        }
        return ret;
    }

    public static String formatoFechaISO(Date fecha) {
        if (fecha == null) {
            return "-";
        }
        String ret = "-";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            ret = sdf.format(fecha);
        } catch (Exception e) {
            LOGGER.error("No se pudo parsear fecha >> " + fecha.toString(), e);
        }
        return ret;
    }

    public static String formatoImporte(BigDecimal monto) {
        if (monto == null) {
            return "-";
        }
        DecimalFormat dec = new DecimalFormat("#,##0.00".trim());
        DecimalFormatSymbols decSim = new DecimalFormatSymbols();
        decSim.setDecimalSeparator(',');
        decSim.setGroupingSeparator('.');
        dec.setDecimalFormatSymbols(decSim);
        return dec.format(monto);
    }

    public static String numeroClienteFormateado(BigDecimal numeroCliente) {
        return numeroClienteFormateado(numeroCliente.toString());
    }

    public static String numeroClienteFormateado(String numeroCliente) {
        return StringUtils.leftPad(numeroCliente, 10, '0');
    }

    public static String cleanPath(String path) {
        String mAppPath = path;
        if (!right$(mAppPath, 1L).equals(FILE_SEPARATOR)) {
            mAppPath += FILE_SEPARATOR;
        }
        return mAppPath;
    }

    public static String limpiaLetras(String texto) {
        if ("".equalsIgnoreCase(StringUtils.trimToEmpty(texto))) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < texto.length(); i++) {
            String c = "" + texto.charAt(i);
            if (StringUtils.isNumeric(c)) {
                sb.append(c);
            }
        }

        return sb.toString();
    }

    private static String right$(String s, long l) {
        int index = s.length() - (int) l;
        return s.substring(index);
    }

    public static Date getFechaXMenosYDias(Date xFecha, int yDias) {
        LOGGER.debug("Obteniendo la fecha {}sistema menos {} dias", xFecha.getTime(), yDias);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdfHuman = new SimpleDateFormat("dd/MM/yyyy");

        long fecha = xFecha.getTime();
        long dias = yDias * unDia;
        Date fechaXMenosYDias = new Date(fecha - dias);
        try {
            fechaXMenosYDias = sdf.parse(sdf.format(fechaXMenosYDias));
        } catch (ParseException e) {
            LOGGER.error("El formato de la fecha no es el esperado " + fechaXMenosYDias, e);
        }
        LOGGER.debug("Fecha actual del sistema menos un dia " + sdfHuman.format(fechaXMenosYDias));

        return fechaXMenosYDias;
    }

    public static Date getFechaByString(String fecha) {
        String[] formato = {"yyyyMMdd"};
        Date fechaParseada = null;
        if (fecha.length() > 0) {
            try {
                fechaParseada = DateUtils.parseDate(fecha, formato);
            } catch (ParseException e) {
                LOGGER.error("Error al parsear fecha String al tipo de dato Date. Fecha " + fecha + ". " + e.getMessage(), e);
                fechaParseada = null;
            }
        }
        return fechaParseada;
    }

    public static Date getFechaByFormatoYYYY(String fecha) {
        Date fechaParseada = null;
        if (fecha.length() > 0) {
            try {
                fechaParseada = FORMATO_DATE_ddMMyyyy.parse(fecha);
            } catch (ParseException e) {
                LOGGER.error("Error al parsear fecha String al tipo de dato Date. Fecha " + fecha + ". " + e.getMessage(), e);
                fechaParseada = null;
            }
        }
        return fechaParseada;
    }

    public static String getFechaByFormatoDDMMYYYYhhmmss(Date fecha) {
        try {
            return FORMATO_DATE_ddMMyyyyhhmmss.format(fecha);
        } catch (Exception e) {
            LOGGER.error("Error al obtener la fecha al formato " + fecha + ". " + e.getMessage(), e);
        }
        return null;
    }

    public static NumberFormat formatoDecimal() {
        return FORMATO_DECIMAL;
    }

    public static String decimalFormateado(BigDecimal dato) {
        if (dato == null) {
            return FORMATO_DECIMAL.format(BigDecimal.ZERO);
        }
        return FORMATO_DECIMAL.format(dato);
    }

    public static String decimalMedianoFormateado(BigDecimal dato) {
        if (dato == null) {
            return FORMATO_DECIMAL_MEDIANO.format(BigDecimal.ZERO);
        }
        return FORMATO_DECIMAL_MEDIANO.format(dato);
    }

    public static String decimalLargoFormateado(BigDecimal dato) {
        if (dato == null) {
            return FORMATO_DECIMAL_LARGO.format(BigDecimal.ZERO);
        }
        return FORMATO_DECIMAL_LARGO.format(dato);
    }

    public static String enteroFormateado(BigDecimal dato) {
        if (dato == null) {
            return FORMATO_ENTERO.format(BigDecimal.ZERO);
        }
        return FORMATO_ENTERO.format(dato);
    }

    public static String enteroFormateado(Integer dato) {
        if (dato == null) {
            return FORMATO_ENTERO.format(0);
        }
        return FORMATO_ENTERO.format(dato);
    }

    public static String fechaFormateadaConYYYYMMDD(Date dato) {
        if (dato == null) {
            return null;
        }
        return FORMATO_DATE_yyyyMMdd.format(dato);
    }

    public static String fechaFormateadaConYYMMDD(Date dato) {
        if (dato == null) {
            return null;
        }
        return FORMATO_DATE_yyMMdd.format(dato);
    }

    public static String fechaFormateadaConYYYY(Date dato) {
        if (dato == null) {
            return null;
        }
        return FORMATO_DATE_ddMMyyyy.format(dato);
    }

    public static String fechaFormateadaConYY(Date dato) {
        if (dato == null) {
            return null;
        }
        return FORMATO_DATE_ddMMyy.format(dato);
    }

    public static String fechaHoraFormateadaConYY(Date dato) {
        if (dato == null) {
            return null;
        }
        return FORMATO_DATE_ddMMyyhhmm.format(dato);
    }

    public static String periodoFormateado(Date dato) {
        if (dato == null) {
            return null;
        }
        return FORMATO_PERIODO.format(dato);
    }

    public static String horaFormateadaConHHmmss(Date dato) {
        if (dato == null) {
            return null;
        }
        return FORMATO_HORA_HHmmss.format(dato);
    }

    public static String horaFormateadaConHH_mm_ss(Date dato) {
        if (dato == null) {
            return null;
        }
        return FORMATO_HORA_HH_mm_ss.format(dato);
    }

    public static String horaFormateadaConHHmmss(BigDecimal dato) {
        if (dato == null) {
            return null;
        }
        return FORMATO_HORA_HHmmss.format(dato.toString());
    }

    public static String fechaLargaFormateada(Date dato) {
        if (dato == null) {
            return null;
        }
        return FORMATO_FECHA_LARGA.format(dato);
    }

    public static String fechaHoraLargaFormateada(Date dato) {
        if (dato == null) {
            return null;
        }
        return FORMATO_FECHA_HORA_LARGA.format(dato);
    }

    public static String errorFormateadoAStackTrace(Throwable throwable) {
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        throwable.printStackTrace(printWriter);
        return writer.toString();
    }

    public static Date deDDMMYYYYaFecha(String fecha) {
        String[] formato = {"dd/MM/yyyy", "dd/MM/yyyy HH:mm:ss"};
        Date fechaParseada = null;
        if (!com.google.common.base.Strings.isNullOrEmpty(fecha)) {
            try {
                fechaParseada = DateUtils.parseDate(fecha.trim(), formato);
            } catch (ParseException e) {
                LOGGER.error("Error al parsear la fecha [" + fecha
                        + "] String en formato dd/MM/yyyy a Date. "
                        + e.getMessage(), e);
                fechaParseada = null;
            }
        }
        return fechaParseada;
    }

    public static Date deYYYYMMDDaFecha(String fecha) {
        String[] formato = {"yyyyMMdd"};
        Date fechaParseada = null;
        if (fecha != null && fecha.length() > 0) {
            try {
                fechaParseada = DateUtils.parseDate(fecha, formato);
            } catch (ParseException e) {
                LOGGER.error("Error al parsear la fecha [" + fecha
                        + "] String en formato yyyyMMdd a Date. "
                        + e.getMessage(), e);
                fechaParseada = null;
            }
        }
        return fechaParseada;
    }

    public static String getPeriodoCorto(String periodo) {
        if (StringUtils.trimToNull(periodo) == null) {
            return null;
        }
        String periodoCorto = null;
        if (periodo.length() >= 6) {
            try {
                Date fechaPeriodo = FORMATO_PERIODO.parse(periodo);
                periodoCorto = FORMATO_CORTO_PERIODO.format(fechaPeriodo);
            } catch (Exception e) {
                LOGGER.warn("No se puede formatear el periodo [" + periodo + "] a periodo largo [mes/yyyy].", e);
            }
        }
        return periodoCorto;
    }

    public static String getPeriodoLargo(String periodo) {
        if (StringUtils.trimToNull(periodo) == null) {
            return null;
        }
        String periodoLargo = null;
        if (periodo.length() >= 6) {
            try {
                Date fechaPeriodo = FORMATO_PERIODO.parse(periodo);
                periodoLargo = FORMATO_LARGO_PERIODO.format(fechaPeriodo);
            } catch (Exception e) {
                LOGGER.warn("No se puede formatear el periodo [" + periodo + "] a periodo largo [mes/yyyy].", e);
            }
        }
        return periodoLargo;
    }

    public static Date getFechaHoraLarga(String fecha) {
        if (StringUtils.trimToNull(fecha) == null) {
            return null;
        }
        Date nuevaFecha = null;
        if (fecha.length() >= 6) {
            try {
                nuevaFecha = FORMATO_FECHA_HORA_LARGA.parse(fecha);
            } catch (Exception e) {
                LOGGER.warn("No se puede formatear la fecha [" + fecha + "] a fecha larga [yyyy-MM-dd HH:mm:ss].", e);
            }
        }
        return nuevaFecha;
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++
    public static boolean estaEnHorario(Date fechaHoy, long horaIni, long horaFin) {
        if (fechaHoy == null) return false;
        GregorianCalendar fechaCalendario = new GregorianCalendar();
        fechaCalendario.setTime(fechaHoy);
        int hora = fechaCalendario.get(Calendar.HOUR_OF_DAY);
        int minuto = fechaCalendario.get(Calendar.MINUTE);
        String cadena = "" + StringUtils.leftPad(String.valueOf(hora), 2, "0") + StringUtils.leftPad(String.valueOf(minuto), 2, "0");
        long horaMinuto = Long.parseLong(cadena);
        LOGGER.info("La hora y minuto obtenido a partir de la fecha {} es [{}]", new Object[]{fechaHoraFormateadaConYY(fechaHoy), horaMinuto});
        return horaMinuto >= horaIni && horaMinuto <= horaFin;
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++

    public static String wrap(String str, int wrapLength) {
        return wrap(str, wrapLength, null, false);
    }

    private static int getPosicionSiguienteRetornoCarro(String str, int offset) {
        String retornoCarro = "\n";
        String finLinea = SystemUtils.LINE_SEPARATOR;
        int siguienteRetornoCarro = str.indexOf(retornoCarro, offset);
        int siguienteFinLinea = str.indexOf(finLinea, offset);
        if (siguienteFinLinea < 0 || siguienteRetornoCarro < 0)
            siguienteRetornoCarro = (siguienteRetornoCarro > siguienteFinLinea) ? siguienteRetornoCarro
                    : siguienteFinLinea;
        else
            siguienteRetornoCarro = (siguienteRetornoCarro > siguienteFinLinea) ? siguienteFinLinea
                    : siguienteRetornoCarro;
        return siguienteRetornoCarro;
    }

    public static String wrap(String str, int wrapLength, String newLineStr,
                              boolean wrapLongWords) {
        if (str == null) {
            return null;
        }
        if (newLineStr == null) {
            newLineStr = SystemUtils.LINE_SEPARATOR;
        }
        if (wrapLength < 1) {
            wrapLength = 1;
        }
        int inputLineLength = str.length();
        int offset = 0;
        StringBuffer wrappedLine = new StringBuffer(inputLineLength + 32);

        while ((inputLineLength - offset) > wrapLength) {
            if (str.charAt(offset) == ' ') {
                offset++;
                continue;
            }

            int spaceToWrapAt = str.lastIndexOf(' ', wrapLength + offset);

            int siguienteRetornoCarro = getPosicionSiguienteRetornoCarro(str,
                    offset);
            if (spaceToWrapAt > siguienteRetornoCarro
                    && siguienteRetornoCarro > 0) {
                spaceToWrapAt = siguienteRetornoCarro;
            }

            if (spaceToWrapAt >= offset) {
                // normal case
                wrappedLine.append(str.substring(offset, spaceToWrapAt));
                wrappedLine.append(newLineStr);
                offset = spaceToWrapAt + 1;
            } else {
                // really long word or URL
                if (wrapLongWords) {
                    // wrap really long word one line at a time
                    wrappedLine.append(str.substring(offset, wrapLength
                            + offset));
                    wrappedLine.append(newLineStr);
                    offset += wrapLength;
                } else {
                    // do not wrap really long word, just extend beyond limit
                    spaceToWrapAt = str.indexOf(' ', wrapLength + offset);

                    siguienteRetornoCarro = getPosicionSiguienteRetornoCarro(
                            str, offset);
                    if (spaceToWrapAt > siguienteRetornoCarro
                            && siguienteRetornoCarro > 0) {
                        spaceToWrapAt = siguienteRetornoCarro;
                    }

                    if (spaceToWrapAt >= 0) {
                        wrappedLine
                                .append(str.substring(offset, spaceToWrapAt));
                        wrappedLine.append(newLineStr);
                        offset = spaceToWrapAt + 1;
                    } else {
                        wrappedLine.append(str.substring(offset));
                        offset = inputLineLength;
                    }
                }
            }
        }

        // Whatever is left in line is short enough to just pass through
        wrappedLine.append(str.substring(offset));

        return wrappedLine.toString();
    }

    public static String capitalizar(String valor) {
        if (valor == null)
            return null;
        StrBuilder resultado = new StrBuilder();
        String[] palabras = StringUtils.split(valor, ' ');
        for (String palabra : palabras) {
            String palabraCapitalizada = StringUtils.capitalize(StringUtils
                    .lowerCase(StringUtils.trimToEmpty(palabra)));
            if (palabraCapitalizada != null && !palabraCapitalizada.equals("")
                    && !palabraCapitalizada.equals(" ")) {
                resultado.append(palabraCapitalizada + " ");
            }
        }
        return resultado.toString();
    }

    public synchronized static String deJulianoAFechaddmmyyyy(String juliano) {
        SimpleDateFormat dfJuliano = new SimpleDateFormat("yyyyDDD");
        SimpleDateFormat dfEstandar = new SimpleDateFormat("dd/MM/yyyy");
        Date aux1;
        try {
            aux1 = dfJuliano.parse(juliano);
        } catch (ParseException e) {
            return "";
        }
        return dfEstandar.format(aux1);
    }

    public synchronized static String de_ddmmyy_a_ddmmyyyy(String ddmmyy) {
        SimpleDateFormat dfmini = new SimpleDateFormat("ddMMyy");
        SimpleDateFormat dfEstandar = new SimpleDateFormat("dd/MM/yyyy");
        Date aux1;
        try {
            aux1 = dfmini.parse(StringUtils.leftPad(ddmmyy, 6, "0"));
        } catch (ParseException e) {
            return "";
        }
        return dfEstandar.format(aux1);
    }

    public synchronized static String ponerSepearadoresAddmmyyyy(String ddmmyyyy) {
        SimpleDateFormat dfmini = new SimpleDateFormat("ddMMyyyy");
        SimpleDateFormat dfEstandar = new SimpleDateFormat("dd/MM/yyyy");
        Date aux1;
        try {
            aux1 = dfmini.parse(StringUtils.leftPad(ddmmyyyy, 8, "0"));
        } catch (ParseException e) {
            return "";
        }
        return dfEstandar.format(aux1);
    }

    public static String convertirCaracteresEspeciales(String string) {

        CharSequence t = Strings.toEscapedUnicode(string);
        String cadena = t.toString();

        // Reemplazando caracteres especiales, acentos y tildes
        cadena = StringUtils.replace(cadena, "\\u00E1", "a"); //á
        cadena = StringUtils.replace(cadena, "\\u00E9", "e"); //é
        cadena = StringUtils.replace(cadena, "\\u00FA", "u"); //ú
        cadena = StringUtils.replace(cadena, "\\u00F3", "o"); //ó
        cadena = StringUtils.replace(cadena, "\\u00ED", "i"); //í
        cadena = StringUtils.replace(cadena, "\\u00F1", "n"); //ñ

        return Strings.fromEscapedUnicode(cadena);
    }


    public static Date modificarMinutosDate(Date fechaActual, int minutos) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaActual);
        cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE) + minutos); // minutos (+, -)
        return cal.getTime();
    }

    public static Date modificarDiasDate(Date fechaActual, int dias) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaActual);
        cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + dias); // minutos (+, -)
        return cal.getTime();
    }

    public static Long fechaYYYYMMDD(Date dato) {
        if (dato == null) {
            return null;
        }
        try {
            return Long.parseLong(fechaFormateadaConYYYYMMDD(dato));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date fechaYYYYMMDD(Long dato) {
        if (dato == null) {
            return null;
        }
        try {
            return FORMATO_DATE_yyyyMMdd.parse(String.valueOf(dato));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String fechaYYYYMMDD(BigDecimal dato) {
        if (dato == null || BigDecimal.ZERO.equals(dato)) {
            return null;
        }
        try {
            Date fecha = FORMATO_DATE_yyyyMMdd.parse(String.valueOf(dato));
            return FORMATO_DATE_ddMMyyyy.format(fecha);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date dateYYYYMMDD(BigDecimal dato) {
        if (dato == null || BigDecimal.ZERO.equals(dato)) {
            return null;
        }
        try {
            return FORMATO_DATE_yyyyMMdd.parse(String.valueOf(dato));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String fechaLiteral(BigDecimal dato) {
        if (dato == null || BigDecimal.ZERO.equals(dato)) {
            return null;
        }
        try {
            Date fecha = FORMATO_DATE_yyyyMMdd.parse(String.valueOf(dato));
            return FORMATO_LITERAL.format(fecha);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static long diferenciaDiasEntreFechas(Date fecha1, Date fecha2) {
        try {
            final long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000;

            return (fecha1.getTime() - fecha2.getTime()) / MILLSECS_PER_DAY;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static BigDecimal stringToBigDecimal(String valor) {
        BigDecimal b = BigDecimal.ZERO;
        try {
            if (com.google.common.base.Strings.isNullOrEmpty(valor)) {
                return b;
            }
            if (!com.google.common.base.Strings.isNullOrEmpty(valor)) {
                b = new BigDecimal(valor.trim().replace(",", ""));
            }
            return b;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }

    public static BigInteger stringToBigInteger(String valor) {
        BigInteger b = BigInteger.ZERO;
        try {
            if (!com.google.common.base.Strings.isNullOrEmpty(valor)) {
                b = new BigInteger(valor.trim().replaceAll(",", ""));
            }
            return b;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BigInteger.ZERO;
    }
}
