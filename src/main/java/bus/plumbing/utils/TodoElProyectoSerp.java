/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.plumbing.utils;

import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import serp.bytecode.Project;
import serp.bytecode.visitor.BCVisitor;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

/**
 * Esta clase visita todos los .class del proyecto y responde preguntas.
 *
 * @author Marcelo Morales
 * @since 6/20/11
 */
@Singleton
public final class TodoElProyectoSerp {

    private static final Logger LOGGER = LoggerFactory.getLogger(TodoElProyectoSerp.class);

    private final List<Project> projectList;

    public TodoElProyectoSerp() {
        projectList = new LinkedList<>();
    }

    public void add(String paq) {
        Project project = new Project(paq);
        projectList.add(project);
        try {
            URLClassLoader classLoader = (URLClassLoader) TodoElProyectoSerp.class.getClassLoader();
            Enumeration<URL> resources = classLoader.findResources(paq);
            while (resources.hasMoreElements()) {
                URL url = resources.nextElement();
                URI uri = url.toURI();
                File file = new File(uri);
                LOGGER.debug("Utilizando {} como base del proyecto", file);
                process(project, file);
            }
        } catch (IOException | URISyntaxException e) {
            LOGGER.error("Ha ocurrido un error al procesar", e);
        }
    }

    private void process(Project project, File f) {
        if (f.isFile() && f.getName().endsWith(".class")) {
            project.loadClass(f);
        }

        if (f.isDirectory()) {
            for (String aChildren : f.list()) {
                process(project, new File(f, aChildren));
            }
        }
    }

    public void acceptVisit(BCVisitor bcVisitor) {
        for (Project project : projectList) {
            project.acceptVisit(bcVisitor);
        }
    }
}
