/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.plumbing.utils;

import java.io.Serializable;

/**
 * @author Marcelo Morales
 *         Date: 7/8/11
 *         Time: 7:52 PM
 */
public class PerfStat implements Serializable {

    private static final long serialVersionUID = -8349472113113683215L;

    private final long min;

    private final long sum;

    private final long sumSquares;

    private final long max;

    private final int count;

    public PerfStat() {
        this.min = Long.MAX_VALUE;
        this.max = Long.MIN_VALUE;
        this.sumSquares = sum =  count = 0;
    }

    private PerfStat(long min, long sum, long max, int count, long sumSquares) {
        this.min = min;
        this.sum = sum;
        this.max = max;
        this.count = count;
        this.sumSquares = sumSquares;
    }

    public PerfStat add(long tiempo) {
        return new PerfStat(
                Math.min(tiempo, min),
                sum + tiempo,
                Math.max(tiempo, max),
                count + 1,
                sumSquares + tiempo * tiempo);
    }

    public long getMin() {
        return min;
    }

    public long getSum() {
        return sum;
    }

    public long getMax() {
        return max;
    }

    public int getCount() {
        return count;
    }

    public double getMean() {
        if (count == 0) {
            return Double.NaN;
        }
        double mean = sum / (double) count;
        return Math.round(mean);
    }

    public double getVariance() {
        if (count == 0) {
            return Double.NaN;
        }
        double mediaDeCuadrados = sumSquares / (double) count;
        double mean = getMean();
        return Math.round(mediaDeCuadrados - mean * mean);
    }
}
