package bus.plumbing.utils;

import com.google.common.base.Splitter;

import java.util.HashMap;
import java.util.Iterator;

/**
 * @author by rsalvatierra on 01/06/2017.
 */
public class RestUtils {

    private static final String SEPARADOR_BASIC = ":";
    private static final String BASIC = "BASIC";
    private static final int POSITION_USER = 6;

    public static HashMap<String, String> getUsernameHeaders(String keyHeader, String usrName) {
        HashMap<String, String> authorizationMap = new HashMap<>();
        if (keyHeader != null && keyHeader.toUpperCase().startsWith(BASIC)) {
            String userpassEncoded = keyHeader.substring(POSITION_USER);
            String userpassDecoded = new String(org.apache.commons.codec.binary.Base64.decodeBase64(userpassEncoded));
            Iterator<String> splitted = Splitter.on(SEPARADOR_BASIC).split(userpassDecoded).iterator();
            final String name = splitted.next();
            authorizationMap.put(usrName, name);
        }
        return authorizationMap;
    }

}
