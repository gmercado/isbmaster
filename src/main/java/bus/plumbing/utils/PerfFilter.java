/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.plumbing.utils;

import com.google.inject.Inject;

import javax.servlet.*;
import java.io.IOException;

import static bus.plumbing.utils.Watches.*;

/**
 * @author Marcelo Morales
 * @since 1/4/11 6:21 AM
 */
public class PerfFilter implements Filter {

    @Inject
    public PerfFilter() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        DB_MILIS_PRINCIPAL.set(0L);
        DB_MILIS_AUXILIAR.set(0L);
        DB_MILIS_AUXILIAR2.set(0L);
        DB_MILIS_AUXILIAR3.set(0L);
        DBPOOL_MILIS.set(0L);
        FORMATOS.set(0L);
        MONITOR_400.set(0L);
        MONITOR_MILIS.set(0L);
        CONSUMO_WS.set(0L);
        try {
            chain.doFilter(request, response);
        } finally {
            DB_MILIS_PRINCIPAL.remove();
            DB_MILIS_AUXILIAR.remove();
            DB_MILIS_AUXILIAR2.remove();
            DB_MILIS_AUXILIAR3.remove();
            DBPOOL_MILIS.remove();
            FORMATOS.remove();
            MONITOR_400.remove();
            MONITOR_MILIS.remove();
            CONSUMO_WS.remove();
        }
    }

    @Override
    public void destroy() {

    }
}
