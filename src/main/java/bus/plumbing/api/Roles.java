/*
 *  Copyright 2008 Banco Bisa S.A.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.plumbing.api;

/**
 *
 * @author Marcelo Morales
 */
public enum Roles {
 
    /**
     * Usuario sólo consultas
     */
    OPERADOR("Usuario sólo consultas", true),
    /**
     * Administración de servicios web
     */
    ADMCLIEN("Administración de servicios web", true),
    /**
     * Administrar usuarios
     */
    USUARIOS("Administrar usuarios", true),
    /**
     * Administrar trabajos asíncronos
     */
    SCHED("Administrar trabajos asíncronos", true),
    /**
     * Administrar sistema de Log
     */
    LOGGING("Administrar sistema de Log", true),
    /**
     * Cambiar ambiente
     */
    LIBRERIA("Cambiar ambiente", true),
    /**
     * Administrar variables de medio ambiente
     */
    ENVIRON("Administrar variables de medio ambiente", true);
    
    /*
     * Existe un rol llamado "CLIENTE", que no se configura sino se entrega al usuario que se autentica como cliente.
     */

    private final String descripcion;

    private final boolean administracion;

    Roles(String desc, boolean administracion) {
        descripcion = desc;
        this.administracion = administracion;
    }

    public static String[] todosLosRoles() {
        String[] roles = new String[Roles.values().length];
        for (int i = 0; i < roles.length; i++) {
            roles[i] = Roles.values()[i].name();
        }
        return roles;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public boolean isAdministracion() {
        return administracion;
    }

}
