/*
 * Copyright (c) 2010-2013. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.plumbing.api;

import bus.plumbing.utils.TodoElProyectoSerp;
import com.google.inject.*;
import com.google.inject.binder.AnnotatedBindingBuilder;
import com.google.inject.binder.AnnotatedConstantBindingBuilder;
import com.google.inject.binder.LinkedBindingBuilder;
import com.google.inject.matcher.Matcher;
import com.google.inject.spi.Message;
import com.google.inject.spi.TypeConverter;
import com.google.inject.spi.TypeListener;
import org.quartz.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * @author Marcelo Morales
 *         Created: 5/1/12 6:28 AM
 */
public abstract class AbstractModuleBisa implements Module {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractModuleBisa.class);

    private Binder binder;

    @Override
    public final void configure(Binder binder) {
        this.binder = binder;
        try {
            configure();
        } finally {
            this.binder = null;
        }
    }

    protected abstract void configure();

    protected final Binder binder() {
        return binder;
    }

    protected final void bindScope(Class<? extends Annotation> scopeAnnotation,
                             Scope scope) {
        binder.bindScope(scopeAnnotation, scope);
    }

    protected final <T> LinkedBindingBuilder<T> bind(Key<T> key) {
        return binder.bind(key);
    }

    protected final <T> AnnotatedBindingBuilder<T> bind(TypeLiteral<T> typeLiteral) {
        return binder.bind(typeLiteral);
    }

    protected final <T> AnnotatedBindingBuilder<T> bind(Class<T> clazz) {
        return binder.bind(clazz);
    }

    protected final AnnotatedConstantBindingBuilder bindConstant() {
        return binder.bindConstant();
    }

    protected final void install(Module module) {
        binder.install(module);
    }

    protected final void addError(String message, Object... arguments) {
        binder.addError(message, arguments);
    }

    protected final void addError(Throwable t) {
        binder.addError(t);
    }

    protected final void addError(Message message) {
        binder.addError(message);
    }

    protected final void requestInjection(Object instance) {
        binder.requestInjection(instance);
    }

    protected final void requestStaticInjection(Class<?>... types) {
        binder.requestStaticInjection(types);
    }

    protected final void bindInterceptor(Matcher<? super Class<?>> classMatcher,
                                   Matcher<? super Method> methodMatcher,
                                   org.aopalliance.intercept.MethodInterceptor... interceptors) {
        binder.bindInterceptor(classMatcher, methodMatcher, interceptors);
    }

    protected final void requireBinding(Key<?> key) {
        binder.getProvider(key);
    }

    protected final void requireBinding(Class<?> type) {
        binder.getProvider(type);
    }

    protected final <T> Provider<T> getProvider(Key<T> key) {
        return binder.getProvider(key);
    }

    protected final <T> Provider<T> getProvider(Class<T> type) {
        return binder.getProvider(type);
    }

    protected final void convertToTypes(Matcher<? super TypeLiteral<?>> typeMatcher,
                                  TypeConverter converter) {
        binder.convertToTypes(typeMatcher, converter);
    }

    protected final Stage currentStage() {
        return binder.currentStage();
    }

    protected final <T> MembersInjector<T> getMembersInjector(Class<T> type) {
        return binder.getMembersInjector(type);
    }

    protected final <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> type) {
        return binder.getMembersInjector(type);
    }

    protected final void bindListener(Matcher<? super TypeLiteral<?>> typeMatcher,
                                TypeListener listener) {
        binder.bindListener(typeMatcher, listener);
    }

    protected final void bindSched(String name, String group, String cron, Class<? extends Job> clazz) {
        binder.requestInjection(new JobActivator(name, group, cron, clazz));
    }

    protected final void bindSchedConHistoria(String name, String group, String cron, Class<? extends Job> clazz) {
        binder.requestInjection(new JobActivator(name, group, cron, clazz, true));
    }

    protected final void bindUI(String paq) {
        binder.requestInjection(new UIActivator(paq));
    }

    private static final class JobActivator {

        private final String nombre;
        private final String grupo;
        private final String cron;
        private final Class<? extends Job> job;
        private final boolean conHistoria;

        private JobActivator(String nombre, String grupo, String cron, Class<? extends Job> job) {
            this(nombre, grupo, cron, job, false);
        }

        private JobActivator(String nombre, String grupo, String cron, Class<? extends Job> job, boolean ch) {
            this.nombre = nombre;
            this.grupo = grupo;
            this.cron = cron;
            this.job = job;
            this.conHistoria = ch;
        }

        @Inject
        public void setQuartz(ProgramadorTareas programadorTareas) {
            try {
                programadorTareas.programar(nombre, grupo, cron, job, conHistoria);
            } catch (Exception e) {
                LOGGER.error("No se ha podido programar " + job + " en " + getClass(), e);
            }
        }
    }

    private static final class UIActivator {
        private final String paq;

        private UIActivator(String paq) {
            this.paq = paq;
        }

        @Inject
        public void setTodoElProyectoSerp(TodoElProyectoSerp todoElProyectoSerp) {
            todoElProyectoSerp.add(paq);
        }
    }

}
