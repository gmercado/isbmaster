package bus.plumbing.api;

import org.apache.wicket.util.time.Duration;
import org.quartz.InterruptableJob;
import org.quartz.Job;

import java.util.Map;

/**
 * @author Marcelo Morales
 *         Created: 5/1/12 6:09 AM
 */
public interface ProgramadorTareas {

    void programar(String nombreTrabajo, String grupo, String cronPorDefecto, Class<? extends Job> jc, boolean conHistoria);

    void programarTrabajoTemporal(Class<? extends InterruptableJob> jobClass, String name,
                                  Duration esperaInicial, Map<?, ?> dataMap);
}
