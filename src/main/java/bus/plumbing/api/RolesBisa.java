/*
 * Copyright (c) 2010-2013. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.plumbing.api;

import com.google.common.base.Function;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.IModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Marcelo Morales
 * @since 4/30/12
 */
public class RolesBisa {

    public static final String ADMIN = "ADM";

    public static final String ISB_ADMIN = "ISB-ADM";

    public static final String SERVICIOS = "SERV";

    public static final String SOLO_LECTURA = "RDO";

    public static final String ISB_BOA = "ISB-BOA";

    public static final String SEGIP_CONSULTAS = "SGP-CON";

    public static final String SEGIP_CONSUMO = "SGP-WS";

    public static final String SEGIP_REPORTE = "SGP-RPC";

    public static final String SEGIP_PARAM = "SGP-PRM";

    public static final String SIN_DOSIFICACION = "SIN_DOS";

    public static final String SIN_ADM = "SIN_ADM";

    public static final String BCB_COT = "BCB-COT";

    public static final String BCB_TRE = "BCB-TRE";

    public static final String INFOCRED_ADMIN = "INF-AD";

    public static final String INFOCRED_USUARIO = "INF-US";

    public static final String FTP_TRANSFERENCIA = "FTP-TRA";

    public static final String JOVEN_MOVIMIENTOS = "JOV_MOV";

    public static final String JOB_MONITOR_POR_IP = "JOB-MON";

    public static final String LINKSER_CONSULTAS = "LNK-CON";

    public static final String LINKSER_CONSUMO = "LNK-WS";

    public static final String MESA_AYUDA = "MES-AYU";

    public static List<String> getTodos() {
        return new ArrayList<>(Arrays.<String>asList(ADMIN, SOLO_LECTURA, ISB_ADMIN, SERVICIOS, ISB_BOA, SEGIP_CONSULTAS, SEGIP_CONSUMO, SEGIP_REPORTE, SEGIP_PARAM, SIN_DOSIFICACION, SIN_ADM, INFOCRED_ADMIN, INFOCRED_USUARIO, BCB_COT,BCB_TRE, FTP_TRANSFERENCIA,JOVEN_MOVIMIENTOS,JOB_MONITOR_POR_IP, LINKSER_CONSULTAS, LINKSER_CONSUMO, MESA_AYUDA));
    }

    public static IChoiceRenderer<String> getCR() {
        return new StringIChoiceRenderer();
    }

    public static Function<String, String> getF() {
        return new StringIChoiceRenderer();
    }

    private static class StringIChoiceRenderer implements IChoiceRenderer<String>, Function<String, String> {

        private static final long serialVersionUID = -3471110578426491250L;

        @Override
        public Object getDisplayValue(String object) {
            if (ADMIN.equals(object)) {
                return "Super Usuario del sistema";
            } else if (SOLO_LECTURA.equals(object)) {
                return "Usuario s\u00f3lo lectura";
            } else if (ISB_ADMIN.equals(object)) {
                return "Administrador Sistema ISB";
            } else if (ISB_BOA.equals(object)) {
                return "Usuario consultas BOA";
            } else if (SEGIP_CONSULTAS.equals(object)) {
                return "Usuario consultas datos SEGIP";
            } else if (SEGIP_CONSUMO.equals(object)) {
                return "Usuario consumo servicio SEGIP";
            } else if (SEGIP_REPORTE.equals(object)) {
                return "Usuario reporte conciliacion SEGIP";
            } else if (SEGIP_PARAM.equals(object)) {
                return "Usuario parametros SEGIP";
            } else if (SIN_DOSIFICACION.equals(object)) {
                return "Usuario consultas SIN";
            } else if (SIN_ADM.equals(object)) {
                return "Usuario Administrador SIN";
            } else if (SERVICIOS.equals(object)) {
                return "Servicios OnLine";
            } else if (BCB_COT.equals(object)) {
                return "Servicios BCB para Indicadores";
            } else if (BCB_TRE.equals(object)) {
                return "Servicios BCB para tasa TRE";
            } else if (INFOCRED_ADMIN.equals(object)) {
                return "Usuario admin servicio INFOCRED";
            } else if (INFOCRED_USUARIO.equals(object)) {
                return "Usuario consulta servicio INFOCRED";
            } else if (JOVEN_MOVIMIENTOS.equals(object)) {
                return "Usuario consulta BISA NEO";
            }  else if (FTP_TRANSFERENCIA.equals(object)) {
                return "Transferencia FTP";
            }else  if (JOB_MONITOR_POR_IP.equals(object)) {
                return "Jobs por IP";
            }else  if (MESA_AYUDA.equals(object)) {
                return "Usuario consulta Mesa de Ayuda";
            }
            return object;
        }

        @Override
        public String getIdValue(String object, int index) {
            return Integer.toString(index);
        }

        @Override
        public String getObject(String id, IModel<? extends List<? extends String>> choices) {
            throw new UnsupportedOperationException("Method not supported");
        }

        @Override
        public String apply(String input) {
            return (String) getDisplayValue(input);
        }
    }
}
