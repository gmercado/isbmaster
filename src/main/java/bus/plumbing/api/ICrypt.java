package bus.plumbing.api;

/**
 * @author  by rsalvatierra on 27/06/2017.
 */
public interface ICrypt {
    /**
     * Des-encripta un valor que se ha encriptado con 3DES al estilo legado que empieza con "crypt:".
     *
     * <table>
     *     <tr>
     *         <th>Entrada</th>
     *         <th>Salida</th>
     *     </tr>
     *     <tr>
     *         <td>null</td>
     *         <td>null</td>
     *     </tr>
     *     <tr>
     *         <td>ejemplo</td>
     *         <td>ejemplo</td>
     *     </tr>
     *     <tr>
     *         <td>crypt:SD8923osid23==</td>
     *         <td>(plaintext)</td>
     *     </tr>
     * </table>
     *
     * @param retValue el valor que puede estar encriptado con la encriptación legada.
     * @return el valor des-encriptado.
     */
    String dec(String retValue);

    /**
     * Encripta un valor con la encriptación legada.
     * @param value cualquier valor que se desea encriptar.
     * @return el valor encriptado, listo para colocarse en medio ambiente o en un archivo de configuración.
     */
    String enc(String value);

    /**
     * Des-encripta un valor que se ha encriptado con AES al estilo openssl:.
     *
     * <p>Esta forma de encriptación guarda compatibilidad con openssl, que debe ejecutarse del siguiente modo</p>
     *
     * <p><code>$ openssl enc -d -aes-128-cbc -a -A </code>. La contraseña debe ser la misma que la utilizada por
     * la aplicación. Véáse el manual del programador en la sección "uso de la interfaz de cifrado" para más
     * información.</p>
     *
     * <table>
     *     <tr>
     *         <th>Entrada</th>
     *         <th>Salida</th>
     *     </tr>
     *     <tr>
     *         <td>null</td>
     *         <td>null</td>
     *     </tr>
     *     <tr>
     *         <td>ejemplo</td>
     *         <td>ejemplo</td>
     *     </tr>
     *     <tr>
     *         <td>openssl:U2FsdGVkX1+SPMJrmpOuv59QW2GxpVMAqReoiWEUBwY=</td>
     *         <td>qweqweqwe</td>
     *     </tr>
     * </table>
     *
     * @param value el valor, que posiblemente esté encriptado con el esquema openssl:
     * @return el valor de entrada, o bien el des-encriptado.
     */
    String decssl(String value);

    /**
     * Encripta un valor con el esquema nuevo de openssl:.
     *
     * <p>Esta forma de encriptación guarda compatibilidad con openssl, que debe ejecutarse del siguiente modo</p>
     *
     * <p><code>$ openssl enc -aes-128-cbc -a -A </code>. La contraseña debe ser la misma que la utilizada por
     * la aplicación. Véáse el manual del programador en la sección "uso de la interfaz de cifrado" para más
     * información.</p>
     *
     * @param value el valor a encriptar.
     * @return el valor encriptado.
     */
    String encssl(String value);
}
