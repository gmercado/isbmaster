package bus.users.tipos;

public enum AdmUserEstado {

    UACT("Activo"),
    UBLK("Bloqueado");
    
    private final String descripcion;
    
    AdmUserEstado(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
    
}
