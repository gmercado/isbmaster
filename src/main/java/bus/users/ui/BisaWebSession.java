package bus.users.ui;

import bus.users.api.AutenticadorUsuarios;
import bus.users.entities.AdmUser;
import com.google.inject.Injector;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.request.Request;

import java.util.Locale;
import java.util.Set;

import static bus.users.api.Metadatas.USER_META_DATA_KEY;

/**
* @author Marcelo Morales
*         Since: 2/6/13
*/
public class BisaWebSession extends AuthenticatedWebSession {

    private static final long serialVersionUID = 8533052589444270578L;

    public BisaWebSession(Request request) {
        super(request);
        setLocale(new Locale("es", "BO"));
    }

    @Override
    public boolean authenticate(String username, String password) {
        AutenticadorUsuarios au = ((Injector) getApplication()).getInstance(AutenticadorUsuarios.class);
        return au.autenticar(this, username, password);
    }

    @Override
    public Roles getRoles() {
        AdmUser user = getMetaData(USER_META_DATA_KEY);
        if (user == null) {
            return new Roles();
        }
        Set<String> roles = user.getRoles();
        if (roles == null) {
            return new Roles();
        }
        return new Roles(roles.toArray(new String[roles.size()]));
    }
}
