/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.users.ui;

import bus.database.components.Listado;
import bus.database.components.ListadoDataProvider;
import bus.database.components.ListadoPanel;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.*;
import bus.users.dao.AdmUserDao;
import bus.users.entities.AdmUser;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author Marcelo Morales
 * @since 7/19/11
 */
@AuthorizeInstantiation({RolesBisa.ADMIN})
@Menu(value = "Administraci\u00f3n de usuarios", subMenu = SubMenu.PARAMETROS)
public class ListadoAdminUsers extends Listado<AdmUser, Long> {

    private final IModel<HashSet<AdmUser>> seleccionados;

    public ListadoAdminUsers() {
        super();
        seleccionados = new Model<>(new HashSet<>());
    }

    @Override
    protected ListadoPanel<AdmUser, Long> newListadoPanel(String id) {
        return new ListadoPanel<AdmUser, Long>(id) {
            @Override
            protected List<IColumn<AdmUser, String>> newColumns(ListadoDataProvider<AdmUser, Long> dataProvider) {
                List<IColumn<AdmUser, String>> columnList = new LinkedList<>();

                columnList.add(new AjaxCheckBoxColumn<>(seleccionados, dataProvider));
                columnList.add(new LinkPropertyColumn<AdmUser>(Model.of("Nombre de usuario"), "userlogon", "userlogon") {
                    @Override
                    protected void onClick(AdmUser object) {
                        setResponsePage(EditAdmUser.class, new PageParameters().add("id", object.getId()));
                    }
                });
                columnList.add(new PropertyColumn<>(Model.of("Roles"), "rolAsString"));
                columnList.add(new LinkColumn<AdmUser>(Model.of("Operaciones")) {
                    @Override
                    protected IModel<?> createLabelModel(IModel<AdmUser> rowModel) {
                        return Model.of("copiar");
                    }

                    @Override
                    protected void onClick(AdmUser object) {
                        setResponsePage(NuevoAdmUserPage.class, new PageParameters().add("id", object.getId()));
                    }
                });
                return columnList;
            }

            @Override
            protected Class<? extends Dao<AdmUser, Long>> getProviderClazz() {
                return AdmUserDao.class;
            }

            @Override
            protected void poblarBotones(RepeatingView repeatingView, String idButton, Form<Void> listform) {
                WebMarkupContainer wmk;
                repeatingView.add(wmk = new WebMarkupContainer(repeatingView.newChildId()));
                wmk.add(new ToolButton(idButton) {
                    @Override
                    public void onSubmit() {
                        setResponsePage(NuevoAdmUserPage.class);
                    }
                }.setClassAttribute("btn btn-info")
                        .setIconAttribute("icon-file icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Crear nuevo usuario")));

                repeatingView.add(wmk = new WebMarkupContainer(repeatingView.newChildId()));
                wmk.add(new IndicatingAjaxToolButton(idButton, listform) {

                    @Override
                    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                        target.add(feedbackPanel);
                        target.add(table);
                        Set<AdmUser> usuariosSeleccionados = seleccionados.getObject();
                        if (usuariosSeleccionados == null || usuariosSeleccionados.isEmpty()) {
                            getSession().warn("Nada por hacer");
                            return;
                        }
                        AdmUserDao userDao = getInstance(AdmUserDao.class);
                        int eliminados = 0;
                        int errados = 0;
                        for (AdmUser admUser : usuariosSeleccionados) {
                            try {
                                userDao.remove(admUser.getId());
                                eliminados++;
                            } catch (Exception e) {
                                LOGGER.error("Ha ocurrido un error al eliminar a un usuario " + admUser, e);
                                errados++;
                            }
                        }
                        if (errados != 0) {
                            getSession().error("Algunos (" + errados + " de " +
                                    (eliminados + errados) +
                                    ") registros de usuarios no pudieron eliminarse. Consule a soporte t\u00e9cnico.");
                        } else {
                            getSession().info("Operaci\u00f3n realizada correctamente");
                        }
                        seleccionados.setObject(new HashSet<>());
                    }

                    @Override
                    protected void onError(AjaxRequestTarget target, Form<?> form) {
                        target.add(feedbackPanel);
                    }
                }.setClassAttribute("btn btn-danger")
                        .setIconAttribute("icon-remove icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Eliminar usuarios seleccionados")));
            }

        };
    }


}
