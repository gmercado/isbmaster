/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.users.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.users.dao.AdmUserDao;
import bus.users.entities.AdmUser;
import bus.users.tipos.AdmUserEstado;
import com.google.common.collect.Sets;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.validator.PatternValidator;
import org.apache.wicket.validation.validator.StringValidator;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Marcelo Morales
 *         Date: 7/27/11
 *         Time: 9:08 AM
 */
@Titulo("Crear nuevo usuario")
@AuthorizeInstantiation({RolesBisa.ADMIN})
public class NuevoAdmUserPage extends BisaWebPage {

    private static final long serialVersionUID = 2571783665563685486L;

    /**
     * @param parameters puede contener un id del cual copiar.
     */
    public NuevoAdmUserPage(PageParameters parameters) {
        super(parameters);

        AdmUser admuser = new AdmUser();
        if (!parameters.get("id").isEmpty()) {
            AdmUserDao dao = getInstance(AdmUserDao.class);
            AdmUser fromDb = dao.find(parameters.get("id").toLong());
            admuser.setRoles(Sets.newHashSet(fromDb.getRoles()));
        }
        IModel<AdmUser> userModel = Model.of(admuser);

        Form<AdmUser> nuevoUsuario;
        add(nuevoUsuario = new Form<>("nuevo usuario", new CompoundPropertyModel<>(userModel)));

        nuevoUsuario.add(new RequiredTextField<String>("userlogon").
                add(new PatternValidator("[A-Za-z][A-Za-z0-9]+")).
                add(StringValidator.maximumLength(25)).
                setOutputMarkupId(true));

        nuevoUsuario.add(new CheckBoxMultipleChoice<>("roles",
                RolesBisa.getTodos(), RolesBisa.getCR()).
                setRequired(true).
                setOutputMarkupId(true));

        nuevoUsuario.add(new Button("ok", Model.of("Guardar usuario")) {

            private static final long serialVersionUID = -2202984086710425518L;

            @Override
            public void onSubmit() {
                try {
                    AdmUserDao admUserDao = getInstance(AdmUserDao.class);
                    AdmUser modelObject = (AdmUser) getForm().getModelObject();
                    modelObject.setUserlogon(modelObject.getUserlogon().toLowerCase());
                    modelObject.setEstado(AdmUserEstado.UACT);
                    modelObject.setFechaActivacion(new Timestamp(new Date().getTime()));
                    admUserDao.persist(modelObject);
                    getSession().info("Usuario creado satisfactoriamente");
                    setResponsePage(ListadoAdminUsers.class);
                } catch (Exception e) {
                    getSession().error("Ha ocurrido un error inesperado, comun\u00edquese con soporte t\u00e9cnico");
                    LOGGER.error("Ha ocurrido un error inesperado al crear el registro de nuevo usuario", e);
                }
            }
        });

        nuevoUsuario.add(new Button("cancelar", Model.of("Cancelar")) {

            private static final long serialVersionUID = 1242248008350765173L;

            @Override
            public void onSubmit() {
                setResponsePage(ListadoAdminUsers.class);
                getSession().info("Operaci\u00f3n cancelada, no se ha creado un nuevo registro de usuario");
            }
        }.setDefaultFormProcessing(false));

    }
}
