/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.users.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.users.api.Metadatas;
import bus.users.dao.AdmUserDao;
import bus.users.entities.AdmUser;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * @author Marcelo Morales
 * @since 8/30/11
 */
@Titulo("Crear nuevo usuario")
@AuthorizeInstantiation({RolesBisa.ADMIN})
public class EditAdmUser extends BisaWebPage {

    private static final long serialVersionUID = 5337507179156131158L;

    /**
     * @param parameters necesito el "id" del usuario
     */
    public EditAdmUser(PageParameters parameters) {
        super(parameters);

        IModel<AdmUser> admUserIModel = new LoadableDetachableModel<AdmUser>() {

            private static final long serialVersionUID = -8209868355275986857L;

            @Override
            protected AdmUser load() {
                Long id = getPageParameters().get("id").toLong();
                AdmUserDao userDao = getInstance(AdmUserDao.class);
                return userDao.find(id);
            }
        };

        Form<AdmUser> editarUsuarioForm;
        add(editarUsuarioForm = new Form<>("editar usuario", new CompoundPropertyModel<>(admUserIModel)));

        editarUsuarioForm.add(new Label("userlogon"));

        editarUsuarioForm.add(new CheckBoxMultipleChoice<>("roles",
                RolesBisa.getTodos(), RolesBisa.getCR()).
                setRequired(true).
                setOutputMarkupId(true));

        editarUsuarioForm.add(new Button("ok", Model.of("Guardar usuario")) {

            private static final long serialVersionUID = -7498462827371758455L;

            @Override
            public void onSubmit() {
                try {
                    AdmUserDao admUserDao = getInstance(AdmUserDao.class);
                    AdmUser user = admUserDao.merge((AdmUser) getForm().getModelObject());
                    getSession().info("Usuario actualizado satisfactoriamente");
                    setResponsePage(ListadoAdminUsers.class);
                    LOGGER.info("El usuario '{}' ha actualizado el registro del usuario '{}'",
                            getSession().getMetaData(Metadatas.USER_META_DATA_KEY), user.getUserlogon());
                } catch (Exception e) {
                    getSession().error("Ha ocurrido un error inesperado, comun\u00edquese con soporte t\u00e9cnico");
                    LOGGER.error("Ha ocurrido un error inesperado al crear el registro de nuevo usuario", e);
                }
            }
        });

        editarUsuarioForm.add(new Button("cancelar", Model.of("Cancelar")) {

            private static final long serialVersionUID = 3870915976218491449L;

            @Override
            public void onSubmit() {
                setResponsePage(ListadoAdminUsers.class);
                getSession().info("Operaci\u00f3n cancelada, no se ha creado un nuevo registro de usuario");
            }
        }.setDefaultFormProcessing(false));
    }
}
