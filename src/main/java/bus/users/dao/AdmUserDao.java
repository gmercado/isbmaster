package bus.users.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.env.api.MedioAmbiente;
import bus.monitor.as400.AS400Factory;
import bus.plumbing.api.RolesBisa;
import bus.users.api.As400ParaAutenticar;
import bus.users.api.AutenticadorUsuarios;
import bus.users.api.Metadatas;
import bus.users.entities.AdmUser;
import com.google.inject.Inject;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400SecurityException;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.OpenJPAQuery;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaQuery;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.concurrent.locks.ReentrantLock;

import static bus.env.api.Variables.*;

/**
 * @author Marcelo Morales @since 7/19/11
 * @author rsalvatierra modificado on 08/06/2016
 */
public class AdmUserDao extends DaoImpl<AdmUser, Long> implements AutenticadorUsuarios {

    private static final Logger LOG = LoggerFactory.getLogger(AdmUserDao.class);

    private static final ReentrantLock authenLock = new ReentrantLock(true);

    private final MedioAmbiente medioAmbiente;

    private final AS400 as400;

    @Inject
    public AdmUserDao(@BasePrincipal EntityManagerFactory entityManagerFactory, MedioAmbiente medioAmbiente,
                      @As400ParaAutenticar AS400 as400) {
        super(entityManagerFactory);
        this.medioAmbiente = medioAmbiente;
        this.as400 = as400;
    }


    public AdmUser getByUsername(final String username) {
        if (username == null) {
            return null;
        }
        return doWithTransaction((entityManager, t) -> {
            OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
            OpenJPACriteriaQuery<AdmUser> query = cb.createQuery(AdmUser.class);

            Root<AdmUser> root = query.from(AdmUser.class);
            query.where(cb.equal(cb.lower(root.<String>get("userlogon")), username.trim().toLowerCase()));

            try {
                return entityManager.createQuery(query).getSingleResult();
            } catch (NoResultException e) {
                return null;
            }
        });
    }

    @Override
    protected Path<Long> countPath(Root<AdmUser> from) {
        return from.get("id");
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Iterable<Path<String>> getFullTexts(Root<AdmUser> p) {
        Path<String> userlogon = p.get("userlogon");
        return Collections.singletonList(userlogon);
    }

    @Override
    public boolean autenticar(Session session, String username, String password) {

        AdmUser admUser;

        if (!medioAmbiente.existe(ADMIN_NO_PERMITIR_ADMIN)) {
            String admin = medioAmbiente.getValorDe(ADMIN_USUARIO, ADMIN_USUARIO_DEFAULT);
            String claveadmin = medioAmbiente.getValorDe(ADMIN_CLAVE, ADMIN_CLAVE_DEFAULT);
            if (admin.equals(username) && claveadmin.equals(password)) {
                LOG.warn("Usuario <{}> es superusuario configurado por en medio ambiente: permitiendo el acceso sin" +
                        " consultar con el servicio de autenticacion as/400.", username);

                admUser = new AdmUser();
                admUser.setRoles(new HashSet<>(RolesBisa.getTodos()));
                admUser.setUserlogon("[usuario inicial]");

                session.setMetaData(Metadatas.USER_META_DATA_KEY, admUser);

                return true;
            }

            admUser = getByUsername(username);

            if (admUser == null) {
                LOG.info("Usuario {} no configurado en la aplicacion", username);
                session.setMetaData(Metadatas.USER_META_DATA_KEY, null);
                return false;
            }

            // Se autentica a un usuario con un password maestro en el caso de trabajar en modo de desarrollo.
            if (RuntimeConfigurationType.DEVELOPMENT.equals(session.getApplication().getConfigurationType())) {
                if (claveadmin.equals(password)) {
                    LOG.warn("Aceptando la contrasena del administrador al usuario '{}' " +
                                    "porque la aplicacion esta en modo desarrollo",
                            username);

                    session.setMetaData(Metadatas.USER_META_DATA_KEY, admUser);

                    return true;
                }
            }
        } else {
            admUser = getByUsername(username);

            if (admUser == null) {
                LOG.info("Usuario {} no configurado en la aplicacion", username);
                session.setMetaData(Metadatas.USER_META_DATA_KEY, null);
                return false;
            }
        }


        String sistemaAuth = medioAmbiente.getValorDe(ADMIN_AUTENTICACION_SISTEMA, ADMIN_AUTENTICACION_SISTEMA_DEFAULT);
        try {
            authenLock.lock();
            try {
                as400.setSystemName(sistemaAuth);
                /*
                    * OJO: authenticate llama a su propio "close", de modo que esto es suficiente. No quedan conexiones
                    * adicionales. �el separar el host de la autenticaci�n parece una buena pr�ctica?.
                    */
                boolean authenticate = as400.authenticate(username, password);
                if (authenticate) {
                    LOG.info("Acceso autorizado por la AS/400 al usuario '{}'", username);
                    session.setMetaData(Metadatas.USER_META_DATA_KEY, admUser);
                    session.setMetaData(Metadatas.OFICINA_META_DATA_KEY, getOficina(username));
                    return true;
                }
            } finally {
                try {
                    as400.disconnectAllServices();
                } catch (Throwable e) {
                    LOG.error("Error al desconectarse de los servicios de autenticacion de la as/400", e);
                }
            }
        } catch (AS400SecurityException e) {
            LOG.warn(AS400Factory.getDescripcionSecurityException(e, "(autenticacion)", "autenticacion"), e);
        } catch (IOException e) {
            LOG.warn("error i/o", e);
        } catch (PropertyVetoException e) {
            LOG.error("Aparentemente el nombre de sistema '" + sistemaAuth + "' no tiene el formato correcto", e);
        } finally {
            authenLock.unlock();
        }
        session.setMetaData(Metadatas.USER_META_DATA_KEY, null);
        return false;
    }

    private String getOficina(final String userAS) {
        return doWithTransaction((entityManager, t) -> {
            String agencia = null;
            String user = StringUtils.upperCase(StringUtils.rightPad(StringUtils.trimToEmpty(userAS), 10, ' '));
            OpenJPAQuery<Integer> sqlAgencia = entityManager.createNativeQuery("SELECT scfreq FROM scp001L1 WHERE scuser = ?", Integer.class);
            try {
                agencia = sqlAgencia.setParameter(1, user).getSingleResult().toString();
            } catch (NoResultException e) {
                LOG.warn("NO existe el usuario [{}] en AS400", user);
            }
            return agencia;
        });
    }
}
