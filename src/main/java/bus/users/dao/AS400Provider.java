/*
 * Copyright 2012 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package bus.users.dao;

import bus.env.api.MedioAmbiente;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.SecureAS400;

import static bus.env.api.Variables.ADMIN_AUTENTICACION_SISTEMA_SSL;
import static bus.env.api.Variables.ADMIN_AUTENTICACION_SISTEMA_SSL_DEFAULT;

/**
 * @author Marcelo Morales
 */
public class AS400Provider implements Provider<AS400> {

    private final MedioAmbiente medioAmbiente;

    @Inject
    public AS400Provider(MedioAmbiente medioAmbiente) {
        this.medioAmbiente = medioAmbiente;
    }

    @Override
    public AS400 get() {
        String s = medioAmbiente.getValorDe(ADMIN_AUTENTICACION_SISTEMA_SSL, ADMIN_AUTENTICACION_SISTEMA_SSL_DEFAULT);
        if ("no".equalsIgnoreCase(s)) {
            return new AS400();
        } else {
            return new SecureAS400();
        }
    }
}
