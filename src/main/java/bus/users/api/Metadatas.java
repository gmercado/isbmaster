/*
 * Copyright (c) 2010-2013. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.users.api;

import bus.users.entities.AdmUser;
import org.apache.wicket.MetaDataKey;

/**
 * @author Marcelo Morales @since 4/30/12
 * @author rsalvatierra modificado on 08/06/2016
 */
public interface Metadatas {

    MetaDataKey<AdmUser> USER_META_DATA_KEY = new MetaDataKey<AdmUser>() {
        private static final long serialVersionUID = -5179242971145023664L;
    };

    MetaDataKey<String> OFICINA_META_DATA_KEY = new MetaDataKey<String>() {};
}
