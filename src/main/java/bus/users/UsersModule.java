package bus.users;

import bus.plumbing.api.AbstractModuleBisa;
import bus.users.api.As400ParaAutenticar;
import bus.users.api.AutenticadorUsuarios;
import bus.users.dao.AS400Provider;
import bus.users.dao.AdmUserDao;
import bus.users.ui.BisaWebSession;
import com.google.inject.TypeLiteral;
import com.ibm.as400.access.AS400;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;

/**
 * @author Marcelo Morales
 * @since 4/30/12
 */
public class UsersModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(AutenticadorUsuarios.class).to(AdmUserDao.class);
        bind(AS400.class).annotatedWith(As400ParaAutenticar.class).toProvider(AS400Provider.class);
        bindUI("bus/users/ui");
        bind(new TypeLiteral<Class<? extends AuthenticatedWebSession>>(){}).
                toInstance(BisaWebSession.class);
    }
}
