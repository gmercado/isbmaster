/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.users.entities;

import bus.plumbing.api.RolesBisa;
import bus.users.tipos.AdmUserEstado;
import com.google.common.base.Joiner;
import com.google.common.collect.Collections2;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Marcelo Morales
 * @since 7/19/11
 * Modificado: 16/10/2012
 */
public class AdmUser implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private AdmUserEstado estado;
    private Timestamp fechaActivacion;
    private String userlogon;
    private Set<String> roles;
    private long version;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdmUser)) return false;

        AdmUser admUser = (AdmUser) o;

        return !(getId() != null ? !getId().equals(admUser.getId()) : admUser.getId() != null);
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

    public AdmUserEstado getEstado() {
        return estado;
    }

    public void setEstado(AdmUserEstado estado) {
        this.estado = estado;
    }

    public Timestamp getFechaActivacion() {
        return fechaActivacion;
    }

    public void setFechaActivacion(Timestamp fechaActivacion) {
        this.fechaActivacion = fechaActivacion;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserlogon() {
        return userlogon;
    }

    public void setUserlogon(String userlogon) {
        this.userlogon = userlogon;
    }

    public Set<String> getRoles() {
        if (roles == null) {
            roles = new HashSet<String>();
        }
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public String getRolAsString() {
        if (roles == null || roles.isEmpty()) {
            return "(sin roles)";
        }
        return Joiner.on(", ").skipNulls().join(Collections2.transform(roles, RolesBisa.getF()));
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Usuario [id=");
        builder.append(id);
        builder.append(", roles=");
        builder.append(roles);
        builder.append(", userlogon=");
        builder.append(userlogon);
        builder.append(", version=");
        builder.append(version);
        builder.append("]");
        return builder.toString();
    }
}
