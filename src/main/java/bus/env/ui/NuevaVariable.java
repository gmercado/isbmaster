/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.env.ui;

import bus.env.dao.VariableDao;
import bus.env.dao.VariableHistoricoDao;
import bus.env.entities.TipoHistorico;
import bus.env.entities.Variable;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.users.api.Metadatas;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.validation.validator.PatternValidator;
import org.apache.wicket.validation.validator.StringValidator;

/**
 * @author Marcelo Morales Date: 6/21/11 Time: 6:09 AM
 * @author rsalvatierra modificado on 09/05/2016
 */
@Titulo("Creaci\u00f3n de nueva variable")
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
public class NuevaVariable extends BisaWebPage {

    private final IModel<Variable> variableIModel;

    public NuevaVariable() {
        super();

        variableIModel = new CompoundPropertyModel<>(Model.of(new Variable()));

        Form<Variable> crear;
        add(crear = new Form<>("nueva", variableIModel));

        TextField<String> nombre;
        crear.add(nombre = new RequiredTextField<>("nombre"));

        nombre.add(StringValidator.lengthBetween(5, 100));
        nombre.add(new PatternValidator("[A-Za-z0-9_.-]*"));
        nombre.setOutputMarkupId(true);

        TextArea<String> valor;
        crear.add(valor = new TextArea<>("valor"));

        valor.setRequired(true);
        valor.add(StringValidator.maximumLength(1000));
        valor.setOutputMarkupId(true);

        crear.add(new IndicatingAjaxButton("crear") {

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                try {
                    getInstance(VariableDao.class).persist(variableIModel.getObject(),getSession().getMetaData(Metadatas.USER_META_DATA_KEY).getUserlogon(),getIPAcceso(getRequest()));

                    info("Variable creada");
                    setResponsePage(ListadoVariables.class);
                } catch (Exception e) {
                    LOGGER.error("Error inesperado al guardar variable", e);
                    error(e.getMessage());
                    target.add(feedbackPanel);
                }
            }
        });

        crear.add(new Button("cancelar") {

            private static final long serialVersionUID = 62944255371806955L;

            @Override
            public void onSubmit() {
                setResponsePage(ListadoVariables.class);
                getSession().info("Operaci\u00f3n cancelada");
            }
        }.setDefaultFormProcessing(false));
    }
}
