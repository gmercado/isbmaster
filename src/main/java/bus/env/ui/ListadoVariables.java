/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.env.ui;

import bus.database.components.Listado;
import bus.database.components.ListadoDataProvider;
import bus.database.components.ListadoPanel;
import bus.database.dao.Dao;
import bus.env.dao.VariableDao;
import bus.env.entities.Variable;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.*;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marcelo Morales @since 6/6/11
 * @author rsalvatierra modificado on 09/05/2016
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Menu(value = "Medio ambiente", subMenu = SubMenu.ADMINISTRACION)
public class ListadoVariables extends Listado<Variable, Long> {

    @Override
    protected ListadoPanel<Variable, Long> newListadoPanel(String id) {
        return new ListadoPanel<Variable, Long>(id) {
            @Override
            protected List<IColumn<Variable, String>> newColumns(ListadoDataProvider<Variable, Long> dataProvider) {
                ArrayList<IColumn<Variable, String>> iColumns = new ArrayList<>();
                iColumns.add(new LinkPropertyColumn<Variable>(Model.of("Nombre"), "nombre", "nombre") {
                    @Override
                    protected void onClick(Variable object) {
                        setResponsePage(EditarVariable.class, new PageParameters().add("id", object.getId()));
                    }
                });
                iColumns.add(new AbstractColumn<Variable, String>(Model.of("Valor")) {
                    @Override
                    public void populateItem(Item<ICellPopulator<Variable>> item, String componentId, IModel<Variable> model) {
                        item.add(new Label(componentId,
                                new EnMedioModel(new PropertyModel<>(model, "valor"), getBuscarModel())).
                                add(new HighlightBehaviour(getBuscarModel())));
                    }
                });
                return iColumns;
            }

            @Override
            protected void poblarBotones(RepeatingView rv, String idButton, Form<Void> filtroForm) {
                WebMarkupContainer wmk;
                rv.add(wmk = new WebMarkupContainer(rv.newChildId()));
                wmk.add(new ToolButton(idButton) {

                    @Override
                    public void onSubmit() {
                        setResponsePage(NuevaVariable.class);
                    }
                }.setClassAttribute("btn btn-info")
                        .setIconAttribute("icon-file icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Crear nueva variable")));

                rv.add(wmk = new WebMarkupContainer(rv.newChildId()));
                wmk.add(new IndicatingAjaxToolButton(idButton, filtroForm) {

                    @Override
                    protected void onError(AjaxRequestTarget target, Form<?> form) {
                        target.add(feedbackPanel);
                    }

                    @Override
                    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                        target.add(table);
                        target.add(feedbackPanel);
                        try {
                            VariableDao instance = getInstance(VariableDao.class);
                            int i = instance.poblarValoresPorDefecto();
                            getSession().info("La operaci\u00f3n se ha realizado satisfactoriamente, con " + i + " insersiones");
                        } catch (Exception e) {
                            getSession().error("Ha ocurrido un error inesperado, comun\u00edquese con soporte t\u00e9cnico");
                            LOGGER.error("Error inesperado", e);
                        }
                    }
                }.setClassAttribute("btn btn-primary")
                        .setIconAttribute("icon-wrench icon-white")
                        .setDefaultFormProcessing(false).setLabel(Model.of("Poblar con valores por defecto")));
            }

            @Override
            protected Class<? extends Dao<Variable, Long>> getProviderClazz() {
                return VariableDao.class;
            }
        };
    }
}
