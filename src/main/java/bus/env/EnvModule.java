package bus.env;

import bus.env.api.MedioAmbiente;
import bus.env.dao.VariableDao;
import bus.plumbing.api.AbstractModuleBisa;

/**
 * @author Marcelo Morales
 * @since 4/30/12
 */
public class EnvModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(MedioAmbiente.class).to(VariableDao.class);
        bindUI("bus/env/ui");
    }
}
