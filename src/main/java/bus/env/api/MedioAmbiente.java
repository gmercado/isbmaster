/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.env.api;

import bus.env.entities.Variable;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Marcelo Morales
 * @since 1/4/11 3:49 PM
 */
public interface MedioAmbiente {

    String getValorDe(String nombre, String valorPorDefecto);

    Long getValorLongDe(String nombre, Long valorPorDefecto);

    Integer getValorIntDe(String nombre, Integer valorPorDefecto);

    BigDecimal getValorBigDecimalDe(String variable, BigDecimal valorPorDefecto);

    boolean existe(String variable);

    String valor(String nombre, Object... param);

    List<Variable> getValorBulkDe(final String nombreVariable);
}
