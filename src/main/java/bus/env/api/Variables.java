/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.env.api;

import java.io.File;
import java.math.BigDecimal;

/**
 * Contiene los valores de las variables.
 * <p/>
 * <p>Esta aplicación no contiene auto-descubrimiento de variables,
 * de modo que por convención se espera que se utilize esta clase
 * para definir los valores iniciales.</p>
 *
 * @author Marcelo Morales
 * Date: 9/2/11
 * Time: 7:58 AM
 */
public interface Variables {

    String LIBRERIA_FORMATOS_MONITOR = "libreria.formatos.monitor";
    String LIBRERIA_FORMATOS_MONITOR_DEFAULT = "ZICBSBISA";

    String LIBRERIA_COLAS_MONITOR = "libreria.colas.monitor";
    String LIBRERIA_COLAS_MONITOR_DEFAULT = "ZBNKPRD01";

    String LIBRERIA_COLAS_MONITOR_TEMPORAL = "libreria.colas.monitor.temporal";
    String LIBRERIA_COLAS_MONITOR_TEMPORAL_DEFAULT = "ZTBNKPRD01";

    String LIBRERIA_COLA_IN = "libreria.cola.in";
    String LIBRERIA_COLA_IN_DEFAULT = "PSXMONIIQ";
    String LIBRERIA_COLA_OUT = "libreria.cola.out";
    String LIBRERIA_COLA_OUT_DEFAULT = "PSXMONIOQ";

    // ************************************************************
    String MONITOR_SISTEMA = "monitor.sistema";
    String MONITOR_SISTEMA_DEFAULT = "bisades.grupobisa.net";

    String MONITOR_USUARIO = "monitor.usuario";
    String MONITOR_USUARIO_DEFAULT = "EBANK411";

    String MONITOR_PASSWORD = "monitor.password";
    String MONITOR_PASSWORD_DEFAULT = "bisaebank";

    String MONITOR_CCSID = "monitor.ccsid";
    int MONITOR_CCSID_DEFAULT = 284;

    String MONITOR_TIMEOUT = "monitor.timeout";
    int MONITOR_TIMEOUT_DEFAULT = 10;

    String MONITOR_REVERSION_TIMEOUT = "monitor.reversion.timeout";
    int MONITOR_REVERSION_TIMEOUT_DEFAULT = 1800;

    String MONITOR_INTENTOS_ESCRITURA = "monitor.intentos.escritura.dq";
    int MONITOR_INTENTOS_ESCRITURA_DEFAULT = 2;

    String MONITOR_INTENTOS_LECTURA = "monitor.intentos.lectura.dq";
    int MONITOR_INTENTOS_LECTURA_DEFAULT = 1;

    String MONITOR_CANAL = "monitor.canal";
    String MONITOR_CANAL_DEFAULT = "5";

    String CONSULTA_ERRORES_CACHE = "consulta.400.errores.secs.cache.ttl";
    int CONSULTA_ERRORES_CACHE_DEFAULT = 115200;

    String MONITOR_DEVICE_PREFIX = "monitor.device.prefix";
    String MONITOR_DEVICE_PREFIX_DEFAULT = "ISB"; // dependiendo de la aplicacion

    String RPG_INTENTOS_INVOCACION = "rpg.intentos.invocacion.programa";
    int RPG_INTENTOS_INVOCACION_DEFAULT = 2;

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    String MONITOR_VERSION = "monitor.version.wver";
    int MONITOR_VERSION_DEFAULT = 118;

    String MONITOR_CODIGO_SINFONDO = "monitor.codigo.sinfondo";
    String MONITOR_CODIGO_SINFONDO_DEFUALT = "050:081:200";

    String MONITOR_CODIGO_PCC01 = "monitor.codigo.pcc01";
    String MONITOR_CODIGO_PCC01_DEFUALT = "284";

    String USUARIO_MONITOR = "transaccion.monitor.usuario";
    int USUARIO_MONITOR_DEFAULT = 5102;

    String TRANSACCION_COTIZACION = "transaccion.monitor.cotizacion";
    String TRANSACCION_COTIZACION_DEFAULT = "IC46";

    String TRANSACCION_BLOQUEO_TARJETA_DEBITO = "transaccion.monitor.bloqueo.tarjeta.debito";
    String TRANSACCION_BLOQUEO_TARJETA_DEBITO_DEFAULT = "IS41";

    String TRANSACCION_CONSULTA_CUENTA = "transaccion.monitor.consulta.cliente";
    String TRANSACCION_CONSULTA_CUENTA_DEFAULT = "IC93";

    String TRANSACCION_CONSULTA_MOV_CUENTA = "transaccion.monitor.consulta.movimiento.cuenta";
    String TRANSACCION_CONSULTA_MOV_CUENTA_DEFAULT = "IC02";

    String TRANSACCION_CONSULTA_TARJETA_CREDITO = "transaccion.monitor.consulta.tarjeta.credito";
    String TRANSACCION_CONSULTA_TARJETA_CREDITO_DEFAULT = "IC61";

    String TRANSACCION_TRANSFERENCIAS_ACH_CHAT = "transaccion.monitor.transferencia.ach.chat";
    String TRANSACCION_TRANSFERENCIAS_ACH_CHAT_DEFAULT = "CT86";//IT30

    String TRANSACCION_TRANSFERENCIAS_TERCEROS_CHAT = "transaccion.monitor.transferencia.terceros.chat";
    String TRANSACCION_TRANSFERENCIAS_TERCEROS_CHAT_DEFAULT = "CT84";//IT05

    String TRANSACCION_TRANSFERENCIAS_PROPIAS_CHAT = "transaccion.monitor.transferencia.propias.chat";
    String TRANSACCION_TRANSFERENCIAS_PROPIAS_CHAT_DEFAULT = "CT85";//IT01

    String TRANSACCION_PAGO_TARJETA_CREDITO_CHAT = "transaccion.monitor.pago.tarjeta.credito.chat";
    String TRANSACCION_PAGO_TARJETA_CREDITO_CHAT_DEFAULT = "CT87";//IT61

    String TRANSACCION_CONSULTA_PRESTAMO = "transaccion.monitor.consulta.prestamo";
    String TRANSACCION_CONSULTA_PRESTAMO_DEFAULT = "IC51";

    // ************************************************************

    String ADMIN_NO_PERMITIR_ADMIN = "no.permitir.admin";
    String ADMIN_NO_PERMITIR_ADMIN_DEFAULT = "true";

    String ADMIN_USUARIO = "admin.usuario";
    String ADMIN_USUARIO_DEFAULT = "admin";

    String ADMIN_CLAVE = "admin.clave";
    String ADMIN_CLAVE_DEFAULT = "bisabisa";

    String ADMIN_AUTENTICACION_SISTEMA = "admin.autenticacion.sistema";
    String ADMIN_AUTENTICACION_SISTEMA_DEFAULT = "bisades.grupobisa.net";

    String ADMIN_AUTENTICACION_SISTEMA_SSL = "admin.autenticacion.sistema.seguro";
    String ADMIN_AUTENTICACION_SISTEMA_SSL_DEFAULT = "no";

    String AQUA_HOST = "aqua.host";
    String AQUA_HOST_DEFAULT = "1.10.3.106";
    String AQUA_PORT = "aqua.port";
    int AQUA_PORT_DEFAULT = 9010;
    String AQUA_PATH = "aqua.path";
    String AQUA_PATH_DEFAULT = "/AquaWar/soap/";
    String AQUA_USERNAME = "aqua.username";
    String AQUA_USERNAME_DEFAULT = "ebisamovil";
    String AQUA_PASSWORD = "aqua.password";
    String AQUA_PASSWORD_DEFAULT = "ebisa123456";

    String CODIGOBISA_HOST = "codigobisa.host";
    String CODIGOBISA_HOST_DEFAULT = "1.10.3.82";
    String CODIGOBISA_PORT = "codigobisa.port";
    int CODIGOBISA_PORT_DEFAULT = 8511;
    String CODIGOBISA_PATH = "codigobisa.path";
    String CODIGOBISA_PATH_DEFAULT = "/codigobisa/";
    String CODIGOBISA_USERNAME = "codigobisa.username";
    String CODIGOBISA_USERNAME_DEFAULT = "MOVIL411S";
    String CODIGOBISA_PASSWORD = "codigobisa.password";
    String CODIGOBISA_PASSWORD_DEFAULT = "crypt:YE9nQtqRjih7A93VZwXovg==";

    String YELLOWPEPPER_HOST = "yellowpepper.host";
    String YELLOWPEPPER_HOST_DEFAULT = "190.60.21.57";
    String YELLOWPEPPER_PORT = "yellowpepper.port";
    int YELLOWPEPPER_PORT_DEFAULT = 80;
    String YELLOWPEPPER_PATH = "yellowpepper.path";
    String YELLOWPEPPER_PATH_DEFAULT = "/bisa/webservice/";
    String YELLOWPEPPER_SERIAL = "yellowpepper.serial";
    String YELLOWPEPPER_SERIAL_DEFAULT = "Bisa";
    String YELLOWPEPPER_PIN = "yellowpepper.pin";
    String YELLOWPEPPER_PIN_DEFAULT = "Bisa";
    String YELLOWPEPPER_SENDER = "yellowpepper.sender";
    String YELLOWPEPPER_SENDER_DEFAULT = "2472";

    String YELLOWPEPPER_PRIORIDAD = "yellowpepper.prioridad";
    String YELLOWPEPPER_PRIORIDAD_DEFAULT = "4";
    String YELLOWPEPPER_PRIORIDAD_DOMINIO = "yellowpepper.prioridad.dominio";
    String YELLOWPEPPER_PRIORIDAD_DOMINIO_DEFAULT = "1,2,3,4";
    String YELLOWPEPPER_SISTEMAS_PERMITIDOS = "yellowpepper.sistemas.permitidos";
    String YELLOWPEPPER_SISTEMAS_PERMITIDOS_DEFAULT = "ebisa;coderoad;bcbcot";
    // -----------------------------------------------------------

    String MAIL_HOST = "mail.host";
    String MAIL_HOST_DEFAULT = "smtpinterno2.grupobisa.net"; //"localhost";
    String MAIL_FROM = "mail.from";
    String MAIL_FROM_DEFAULT = "isb@grupobisa.com"; //"root@localhost.localdomain";
    String MAIL_PORT = "mail.port";
    int MAIL_PORT_DEFAULT = 25;

    String MAIL_FROM_NAME = "mail.from.name";
    String MAIL_FROM_NAME_DEFAULT = "eBISA.Servicio";

    String WC_TIMEOUT = "consumos.web.milis.connect.timeout";
    int WC_TIMEOUT_DEFAULT = 20000;

    String WC_SO_TIMEOUT = "consumos.web.milis.socket.timeout";
    int WC_SO_TIMEOUT_DEFAULT = 20000;

    // =============================================================
    String NOMBRE_ALGORITMO_CLAVE = "cypher.nombre.algoritmo.para.clave";
    String NOMBRE_ALGORITMO_CLAVE_DEFAULT = "SHA-256";

    String MOSTRAR_CLAVE = "cypher.mostrar.clave.generada";
    String MOSTRAR_CLAVE_DEFAULT = "false";

    String MOSTRAR_PWD = "cypher.mostrar.password.generada";
    String MOSTRAR_PWD_DEFAULT = "false";

    // =============================================================
    String PROXY_HOST = "proxy.host";
    String PROXY_HOST_DEFAULT = "navegar.grupobisa.net";  //"lpzpxy.grupobisa.net";
    String PROXY_PORT = "proxy.port";
    int PROXY_PORT_DEFAULT = 8080;

    //=========================================================================
    //======================= BOA ====================================

    // =============================================================
    String WEBSERVICE_USERPWD = "web.service.user.pwd";
    String WEBSERVICE_USERPWD_DEFAULT = "changeitchangeit";

    //===========================================================
    String SKYB_BOA_URL_TOKEN = "skyb.boa.url.token";
    String SKYB_BOA_URL_TOKEN_DEFAULT = "https://ef1.obairlines.com.bo/ServicesVPN/Token.svc";

    String SKYB_BOA_METODO_TOKEN = "skyb.boa.metodo.token";
    String SKYB_BOA_METODO_TOKEN_DEFAULT = "/GetToken";

    String SKYB_BOA_URL_RESERVA = "skyb.boa.url.reserva";
    String SKYB_BOA_URL_RESERVA_DEFAULT = "https://ef1.obairlines.com.bo/ServicesVPN/InternalReservation.svc";

    String SKYB_BOA_METODO_BOOKING = "skyb.boa.metodo.booking";
    String SKYB_BOA_METODO_BOOKING_DEFAULT = "/GetBooking";

    String SKYB_BOA_METODO_AUTHORIZATION = "skyb.boa.metodo.authorization";
    String SKYB_BOA_METODO_AUTHORIZATION_DEFAULT = "/SetAuthorization";

    String SKYB_BOA_METODO_TICKETPNR = "skyb.boa.metodo.ticketPNR";
    String SKYB_BOA_METODO_TICKETPNR_DEFAULT = "/GetTicketPNR";

    String SKYB_BOA_METODO_INVOICEMAIL = "skyb.boa.metodo.invoice.mail";
    String SKYB_BOA_METODO_INVOICEMAIL_DEFAULT = "/GetInvoicePNREmail";

    String SKYB_BOA_METODO_INVOICEPDF = "skyb.boa.metodo.invoice.pdf";
    String SKYB_BOA_METODO_INVOICEPDF_DEFAULT = "/GetInvoicePNRPDF";

    String SKYB_BOA_URL_CONCILIACION = "skyb.boa.url.conciliacion";
    String SKYB_BOA_URL_CONCILIACION_DEFAULT = "https://ef1.obairlines.com.bo/ServicesVPN/Accounting.svc";

    String SKYB_BOA_METODO_CONCILIACION_DIARIA = "skyb.boa.metodo.conciliacion.diaria";
    String SKYB_BOA_METODO_CONCILIACION_DIARIA_DEFAULT = "/DailyReconciliation";

    String SKYB_BOA_METODO_CONCILIACION_MENSUAL = "skyb.boa.metodo.conciliacion.diaria";
    String SKYB_BOA_METODO_CONCILIACION_MNESUAL_DEFAULT = "/MonthlyReconciliation";

    String SKYB_BOA_CREDENTIALS = "skyb.boa.credenciales";
    String SKYB_BOA_CREDENTIALS_DEFAULT = "{1A8E65A3-B316-4A75-8BC1-60E0CDC95448}{02033F6A-37B2-4F65-B2A3-FEEB400EC6C0}";

    String SKYB_BOA_LENGUAJE = "skyb.boa.lenguaje";
    String SKYB_BOA_LENGUAJE_DEFAULT = "ES";

    String SKYB_BOA_IP = "skyb.boa.ip";
    String SKYB_BOA_IP_DEFAULT = "186.121.212.154";

    String SKYB_BOA_PERMITIR_PAGO = "skyb.boa.permitir.pago";
    String SKYB_BOA_PERMITIR_PAGO_DEFAULT = "true";

    String SKYB_BOA_TRANSACCION_MONITOR = "skyb.boa.transaccion.monitor";
    String SKYB_BOA_TRANSACCION_MONITOR_DEFAULT = "IT82";

    String SKYB_BOA_NOTIFICACION_PAGO_GRUPO = "skyb.boa.notificacion.pago.grupo";
    String SKYB_BOA_NOTIFICACION_PAGO_GRUPO_DEFAULT = "Banco Bisa: Los boletos del pago de su reserva ya fueron procesados por BoA, por favor revise ingresando a su e-Bisa.";

    String SKYB_BOA_NOTIFICACION_PAGO_SIMPLE = "skyb.boa.notificacion.pago.simple";
    String SKYB_BOA_NOTIFICACION_PAGO_SIMPLE_DEFAULT = "Banco Bisa: El boleto del pago de su reserva ya fue procesado por BoA, por favor revise ingresando a su e-Bisa.";

    String SKYB_BOA_ALGORITMO_ENCRIPTACION = "skyb.boa.algoritmo.encriptacion";
    String SKYB_BOA_ALGORITMO_ENCRIPTACION_DEFAULT = "DESede/ECB/NoPadding";

    String SKYB_BOA_LLAVE_ENCRIPTACION = "skyb.boa.llave.encriptacion";
    String SKYB_BOA_LLAVE_ENCRIPTACION_DEFAULT = "3257E4C94B83E828EA682C25";

    String SKYB_BOA_ARCHIVO_BASE = "skyb.boa.archivo.nombre.base";
    String SKYB_BOA_ARCHIVO_BASE_DEFAULT = "_BISA_BOA";

    String SKYB_BOA_SMSCONFIRMACION = "skyb.boa.enviar.sms.confirmacion";
    String SKYB_BOA_SMSCONFIRMACION_DEFAULT = "false";

    String SKYB_BOA_SERVICIO_MENSAJE_PREFIJO = "skyb.boa.servicio.mensaje.prefijo";
    String SKYB_BOA_SERVICIO_MENSAJE_PREFIJO_DEFAULT = "Ocurri\u00F3 un problema. Mensaje BOA: {0}";

    // Boletos solicitud de token por BOA
    String SKYB_BOA_MENSAJE_NO_PROCESA_TOKEN = "skyb.boa.mensaje.no.procesa.emision";
    String SKYB_BOA_MENSAJE_NO_PROCESA_TOKEN_DEFAULT = "El Pago de la Reserva no puede ser procesado. Mensaje de BOA: {0}";

    // Boletos en proceso de emision por BOA
    String SKYB_BOA_MENSAJE_NO_PROCESA_EMISION = "skyb.boa.mensaje.no.procesa.emision";
    String SKYB_BOA_MENSAJE_NO_PROCESA_EMISION_DEFAULT = "El Pago de la Reserva no puede ser procesado. Mensaje de BOA: {0}";
    //La reserva tiene una solicitud de pendiente de emisión en BOA
    //El Pago de la Reserva no puede ser procesada.
    //Problema al solicitar la emisión del boleto a BOA.
    //Pago NO autoriado por BoA. No se puede solicitar la emisión del boleto.

    // Inicio de proceso de emisión
    String SKYB_BOA_MENSAJE_PROCESO_EMISION = "skyb.boa.mensaje.proceso.emision.boa";
    String SKYB_BOA_MENSAJE_PROCESO_EMISION_DEFAULT = "Estamos solicitando a BoA la generaci\u00F3n de su(s) boleto(s) electr\u00F3nico(s). En minutos, verifique la emisi\u00f3n de los mismos para su impresi\u00f3n en Operaciones de Pagos: Boliviana de Aviaci\u00f3n.";

    // Completado proceso de emision
    String SKYB_BOA_MENSAJE_COMPLETADO_PROCESO_EMISION = "skyb.boa.mensaje.completada.emision.boa";
    String SKYB_BOA_MENSAJE_COMPLETADO_PROCESO_EMISION_DEFAULT = "BoA esta generando su(s) boleto(s) electr\u00F3nico(s). En minutos, verifique la emisi\u00f3n de los mismos para su impresi\u00f3n en Operaciones de Pagos: Boliviana de Aviaci\u00f3n.";

    // Emision de ticket de boletos confirmada
    String SKYB_BOA_MENSAJE_PAGO_COMPLETADO = "skyb.boa.mensaje.pago.completado";
    String SKYB_BOA_MENSAJE_PAGO_COMPLETADO_DEFAULT = "Pago completado correctamente. Los tickets fueron emitidos para los titulares de la reserva.";

    // Emision de ticket de boletos rechazada
    String SKYB_BOA_MENSAJE_ERROR_EMISION_TICKETS = "skyb.boa.mensaje.error.emision.ticket";
    String SKYB_BOA_MENSAJE_ERROR_EMISION_TICKETS_DEFAULT = "No se puede obtener los Tickets para confirmar el Pago. Mensaje de BOA: {0}";

    // Mensaje de servicio no disponible
    String SKYB_BOA_MENSAJE_SERVICIO_NO_DISPONIBLE = "skyb.boa.mensaje.servicio.no.diponible";
    String SKYB_BOA_MENSAJE_SERVICIO_NO_DISPONIBLE_DEFAULT = "El pago de reservas a BoA no est\u00e3 disponible. Por favor intenta m\u00e3s tarde.";

    //===========================================================
    String SKYB_BOA_CONCILIACION_FECHA = "skyb.boa.conciliacion.diaria.fecha";
    String SKYB_BOA_CONCILIACION_FECHA_DEFAULT = "YYYYMMDD";

    String SKYB_BOA_CONCILIACION_MESANIO = "skyb.boa.conciliacion.mensual.mes.anio";
    String SKYB_BOA_CONCILIACION_MESANIO_DEFAULT = "MM:YYYY";

    String SKYB_BOA_CONCILIACION_MANUAL = "skyb.boa.conciliacion.manual";
    String SKYB_BOA_CONCILIACION_MANUAL_DEFAULT = "false";

    String SKYB_BOA_CONCILIACION_EMAIL_ENVIO = "skyb.boa.conciliacion.email.destino";
    String SKYB_BOA_CONCILIACION_EMAIL_ENVIO_DEFAULT = "rchura@grupopbisa.com, LPenaranda@grupobisa.com";

    String SKYB_BOA_RECAUDACION_EMAIL_ENVIO = "skyb.boa.recaudacion.email.destino";
    String SKYB_BOA_RECAUDACION_EMAIL_ENVIO_DEFAULT = "rchura@grupopbisa.com, LPenaranda@grupobisa.com";

    String SKYB_BOA_RECAUDACION_SUBJECT_MAIL = "skyb.boa.recaudacion.subject.mail";
    String SKYB_BOA_RECAUDACION_SUBJECT_MAIL_DEFAULT = "Pago reservas BoA - Recaudaciones";

    String SKYB_BOA_EMISION_BOLETO_MINUTOS_ATRAS = "skyb.boa.emision.boleto.minutos.atras";
    int SKYB_BOA_EMISION_BOLETO_MINUTOS_ATRAS_DEFAULT = -1;

    String SKYB_BOA_VERIFICACION_DIAS_ATRAS = "skyb.boa.verifica.pago.dias.atras";
    int SKYB_BOA_VERIFICACION_DIAS_ATRAS_DEFAULT = -2;

    String SKYB_BOA_MINUTOS_REINTENTO_PAGO = "skyb.boa.minutos.reintento.pago";
    int SKYB_BOA_MINUTOS_REINTENTO_PAGO_DEFAULT = 0;

    String SKYB_BOA_ESTADOS_PENDIENTE = "skyb.boa.estados.pendientes";
    String SKYB_BOA_ESTADOS_PENDIENTE_DEFAULT = "7"; //"1,2,3,4,5,6,7,8,9";

    String SKYB_BOA_MENSAJE_PENDIENTE = "skyb.boa.mensaje.pendiente";
    String SKYB_BOA_MENSAJE_PENDIENTE_DEFAULT = "El ticket esta en proceso de confirmaci\u00f3n con BOA, por favor espere esta confirmaci\u00f3n";

    String SKYB_BOA_MENSAJE_COMPLETO = "skyb.boa.mensaje.completo";
    String SKYB_BOA_MENSAJE_COMPLETO_DEFAULT = "El ticket ya fue comprado.";

    String SKYB_BOA_MENSAJE_REVERTIDO = "skyb.boa.mensaje.revertido";
    String SKYB_BOA_MENSAJE_REVERTIDO_DEFAULT = "El ticket fue revertido.";

    String SKYB_BOA_MENSAJE_RECHAZADO = "skyb.boa.mensaje.rechazado";
    String SKYB_BOA_MENSAJE_RECHAZADO_DEFAULT = "El ticket fue rechazado.";

    String SKYB_BOA_MENSAJE_AUTORIZACION = "skyb.boa.mensaje.autorizacion";
    String SKYB_BOA_MENSAJE_AUTORIZACION_DEFAULT = "El ticket necesita autorizaci\u00f3n.";

    //==========================================================
    String TEMPORAL_FILE = "var.log.file";
    String TEMPORAL_FILE_DEFAULT = "/var/log/isbfile";

    //=========================================================================
    //=======================SEGMENTO JOVEN====================================
    //CONFIGURACION

    String MOJIX_CANAL = "mojix.canal";
    String MOJIX_CANAL_DEFAULT = "bisaNeo";

    String MOJIX_TAMANIO_TRANSACTION_ID = "mojix.transaction.id";
    int MOJIX_TAMANIO_TRANSACTION_ID_DEFAULT = 40;

    String MOJIX_TAMANIO_MOTIVO = "mojix.motivo.id";
    int MOJIX_TAMANIO_MOTIVO_DEFAULT = 6;

    String MOJIX_USUARIO_MONITOR = "mojix.monitor.usuario";
    int MOJIX_USUARIO_MONITOR_DEFAULT = 5101;

    String MOJIX_CONSULTA_CUENTA_TRANSACCION_MONITOR = "mojix.consulta.cuenta.transaccion.monitor";
    String MOJIX_CONSULTA_CUENTA_TRANSACCION_MONITOR_DEFAULT = "IC03";

    String MOJIX_TRANSFERENCIA_OTROS_BANCOS_TRANSACCION_MONITOR = "mojix.transferencia.otro.bancos.transaccion.monitor";
    String MOJIX_TRANSFERENCIA_OTROS_BANCOS_TRANSACCION_MONITOR_DEFAULT = "IT84";

    String MOJIX_VALIDA_TIEMPO_SOLICITUD_TOKEN_SMS = "mojix.valida.tiempo.solicitud.sms";
    String MOJIX_VALIDA_TIEMPO_SOLICITUD_TOKEN_SMS_DEFAULT = "disabled";

    String TIEMPO_ENTRE_SOLICITUD_CLAVES = "isb.aqua2.retiro.segundos.entre.solicitud.claves";
    int TIEMPO_ENTRE_SOLICITUD_CLAVES_DEFAULT = 60;

    String CANTIDAD_DE_DIGITOS = "cantidad.de.digitos";
    int CANTIDAD_DE_DIGITOS_DEFAULT = 4;

    String HORAS_ENTRE_PEDIDOS_DE_CLAVE_QUE_NO_CADUCAN_LAS_OTRAS = "isb.aqua2.horas.pedido.clave.anula.otras";
    String HORAS_ENTRE_PEDIDOS_DE_CLAVE_QUE_NO_CADUCAN_LAS_OTRAS_DEFAULT = "3";

    String MOJIX_APLICACIONES_PERMITIDAS_ENVIO_SMS = "mojix.aplicaciones.permitidas.envio.sms";
    String MOJIX_APLICACIONES_PERMITIDAS_ENVIO_SMS_DEFAULT = "coderoad";

    String MOJIX_MENSAJE_TIEMPO_DE_VIDA_TOKEN_HORAS = "mojix.tiempo.vida.horas";
    String MOJIX_MENSAJE_TIEMPO_DE_VIDA_TOKEN_HORAS_DEFAULT = "1";

    String MOJIX_TIEMPO_SOLICITUD_TOKEN = "mojix.tiempo.solicitud.token";
    String MOJIX_TIEMPO_SOLICITUD_TOKEN_DEFAULT = "30";

    String HORAS_ENTRE_PEDIDOS_QUE_ANULAN = "isb.aqua2.retiro.horas.pedido.clave.anula.otras";
    String HORAS_ENTRE_PEDIDOS_QUE_ANULAN_DEFAULT = "3";

    String MOJIX_TELEFONOS_PERMITIDOS = "mojix.telefonos.permitidos";
    String MOJIX_TELEFONOS_PERMITIDOS_DEFAULT = "*";

    String MOJIX_MENSAJE_EVENTO = "mojix.evento.mensaje";
    String MOJIX_MENSAJE_EVENTO_DEFAULT = "";

    //MENSAJES
    String MOJIX_MENSAJE_SIN_RESPUESTA = "----";

    String MOJIX_MENSAJE_REGISTRO_EXITOSO = "mojix.mensaje.registro.exitoso.0";
    String MOJIX_MENSAJE_REGISTRO_EXITOSO_DEFAULT = "La Transacci\u00f3n fue procesada exitosamente.";

    String MOJIX_MENSAJE_FORMATO_NO_VALIDO = "mojix.mensaje.formato.no.valido.100";
    String MOJIX_MENSAJE_FORMATO_NO_VALIDO_DEFAULT = "Formato no valido, para la fecha de nacimiento.";

    String MOJIX_MENSAJE_ADVERTENCIA_MISMA_CUENTA = "mojix.mensaje.advertencia.misma.cuenta.101";
    String MOJIX_MENSAJE_ADVERTENCIA_MISMA_CUENTA_DEFAULT = "El n\u00famero de cuenta Origen $CORIGEN debe ser distinto al de la cuenta Destino $CDESTINO.";

    String MOJIX_MENSAJE_VALIDACION_CODIGO_TOKEN_SMS = "mojix.mensaje.validacion.codigo.token.sms.102";
    String MOJIX_MENSAJE_VALIDACION_CODIGO_TOKEN_SMS_DEFAULT = "El c\u00f3digo introducido no coincide con el c\u00f3digo enviado a tu celular, verifica el c\u00f3digo registrado o solicita un nuevo c\u00f3digo.";

    String MOJIX_MENSAJE_SIN_RESULTADO = "mojix.mensaje.sinresultado.103";
    String MOJIX_MENSAJE_SIN_RESULTADO_DEFAULT = "No se encontraron resultados para la busqueda.";

    String MOJIX_MENSAJE_ADVERTENCIA_CUENTA_INEXISTENTE = "mojix.mensaje.advertencia.cuenta.inexistente.104";
    String MOJIX_MENSAJE_ADVERTENCIA_CUENTA_INEXISTENTE_DEFAULT = "La Cuenta $CUENTAINVALIDA no es v\u00e1lida. Por favor verifica la misma.";

    String MOJIX_MENSAJE_ADVERTENCIA_TAMANIO_TRANSACTIONID = "mojix.mensaje.advertencia.tamanio.transactionid.105";
    String MOJIX_MENSAJE_ADVERTENCIA_TAMANIO_TRANSACTIONID_DEFAULT = "Se excedi\u00f3 la cantidad de caracteres permitidos en el campo 'transactionID'.";

    String MOJIX_MENSAJE_INFORMATIVO_CLIENTE_INEXISTENTE = "mojix.mensaje.informativo.cliente.inexistente.106";
    String MOJIX_MENSAJE_INFORMATIVO_CLIENTE_INEXISTENTE_DEFAULT = "No se encontraron registros con  el n\u00famero de documento $DOCID y fecha de nacimiento $FECNAC.";

    String MOJIX_MENSAJE_INFORMATIVO_CLIENTE_CODIGO_INVALIDO = "mojix.mensaje.informativo.codigo.invalido.107";
    String MOJIX_MENSAJE_INFORMATIVO_CLIENTE_CODIGO_INVALIDO_DEFAULT = "No se encontraron registros  para el c\u00f3digo de cliente $CODCLIENTE.";

    String MOJIX_MENSAJE_ERROR_PROCESO = "mojix.mensaje.error.proceso.108";
    String MOJIX_MENSAJE_ERROR_PROCESO_DEFAULT = "Hubo un error al procesar la operaci\u00f3n. Por favor vuelve a intentar.";

    String MOJIX_MENSAJE_ESTADO_TRANSACCION = "mojix.mensaje.estado.transaccion.109";
    String MOJIX_MENSAJE_ESTADO_TRANSACCION_DEFAULT = "La transacci\u00f3n n\u00famero $TRANSID se encuentra en estado $ESTADOTRANS";

    String MOJIX_MENSAJE_TIEMPO_SOLICITUD_TOKEN_SMS = "mojix.mensaje.tiempo.solicitud.token.sms.110";
    String MOJIX_MENSAJE_TIEMPO_SOLICITUD_TOKEN_SMS_DEFAULT = "Debes esperar al menos $SOLICITUDTOKEN segundos antes de intentar solicitar otra token SMS.";

    String MOJIX_MENSAJE_VALIDA_CAMPO_MONTO_NO_VACIO = "mojix.mensaje.valida.campo.monto.no.vacio.111";
    String MOJIX_MENSAJE_VALIDA_CAMPO_MONTO_NO_VACIO_DEFAULT = "El monto de la transferencia no debe ser vacio.";

    String MOJIX_VALIDA_TRANSACCION_MONTO = "mojix.valida.transaccion.monto.112";
    String MOJIX_VALIDA_TRANSACCION_MONTO_DEFAULT = "El monto de la transferencia debe ser mayor a cero.";

    String MOJIX_MENSAJE_SALDO_INSUFICIENTE = "mojix.mensaje.saldo.insuficiente.113";
    String MOJIX_MENSAJE_SALDO_INSUFICIENTE_DEFAULT = "Saldo insuficiente en tu cuenta.";

    String MOJIX_MENSAJE_NO_EFECTUAR_DEBITO = "mojix.mensaje.no.efectuar.debito.114";
    String MOJIX_MENSAJE_NO_EFECTUAR_DEBITO_DEFAULT = "No se puede efectuar el debito de tu cuenta. Por favor verifica los datos de tu transferencia.";

    String MOJIX_MENSAJE_VALIDA_CAMPO_MOTIVO = "mojix.mensaje.valida.motivo.115";
    String MOJIX_MENSAJE_VALIDA_CAMPO_MOTIVO_DEFAULT = "El texto en el campo 'Motivo' debe ser mayor a 6 caracteres.";

    String MOJIX_MENSAJE_VALIDACION_TOKEN_CODIGO_CONF_REGISTROS = "mojix.mensaje.validacion.codigo.conf.registros.116";
    String MOJIX_MENSAJE_VALIDACION_TOKEN_CODIGO_CONF_REGISTROS_DEFAULT = "Se encontr\u00f3 m\u00e1s de una transacci\u00f3n en estado $ESTADOTRANS para la b\u00fasqueda del Token SMS $TOKENSMS y c\u00f3digo de confirmaci\u00f3n $CODCONFIRMACION.";

    String MOJIX_MENSAJE_VALIDACION_TOKEN_CODIGO_CONFIRMACION = "mojix.mensaje.validacion.codigo.confirmacion.117";
    String MOJIX_MENSAJE_VALIDACION_TOKEN_CODIGO_CONFIRMACION_DEFAULT = "No se encontr\u00f3 registros para el Token SMS $TOKENSMS y c\u00f3digo de confirmaci\u00f3n $CODCONFIRMACION.";

    String MOJIX_MENSAJE_TOKEN_EXPIRADO_UTILIZADO = "mojix.mensaje.token.expirado.utilizado.118";
    String MOJIX_MENSAJE_TOKEN_EXPIRADO_UTILIZADO_DEFAULT = "El c\u00f3digo expir\u00f3 o ya fue utilizado. Por favor solicita un nuevo c\u00f3digo.";

    String MOJIX_MENSAJE_VALIDA_CAMPOS = "mojix.mensaje.valida.campos.119";
    String MOJIX_MENSAJE_VALIDA_CAMPOS_DEFAULT = "El campo $NOMBRECAMPO es requerido.";

    String MOJIX_MENSAJE_REGISTRO_ERRONEO = "mojix.mensaje.registro.exitoso.120";
    String MOJIX_MENSAJE_REGISTRO_ERRONEO_DEFAULT = "Existe un error al procesar la transacci\u00f3n, por favor intenta de nuevo.";

    String MOJIX_MENSAJE_ERROR_REVERTIR_DEBITO = "mojix.mensaje.error.revertir.debito.121";
    String MOJIX_MENSAJE_ERROR_REVERTIR_DEBITO_DEFAULT = "Ocurri\u00f3 un error inesperado al revertir el d\u00e9bito. Por favor comun\u00edquese con Bisa Responde.";

    String MOJIX_MENSAJE_ADVERTENCIA_TRANSACTIONID = "mojix.mensaje.advertencia.transactionid.122";
    String MOJIX_MENSAJE_ADVERTENCIA_TRANSACTIONID_DEFAULT = "La Transacci\u00f3n ya se encuentra procesada.";

    String MOJIX_MENSAJE_CAMPO_TRANSACTIONID_VACIO = "mojix.mensaje.campo.transactionid.123";
    String MOJIX_MENSAJE_CAMPO_TRANSACTIONID_VACIO_DEFAULT = "El campo 'transacionId' de la transferencia no debe ser vacio.";

    String MOJIX_MENSAJE_CUENTA_VALIDA_TRANSFERENCIA = "mojix.mensaje.cuenta.valida.transferencia.124";
    String MOJIX_MENSAJE_CUENTA_VALIDA_TRANSFERENCIA_DEFAULT = "Para procesar la transferencia debe existir una cuenta valida donde acreditar el monto de la transferencia.";

    String MOJIX_MENSAJE_VALIDA_MOTIVO_OBLIGATORIO = "mojix.mensaje.valida.motivo.obligatorio.125";
    String MOJIX_MENSAJE_VALIDA_MOTIVO_OBLIGATORIO_DEFAULT = "El campo motivo es obligatorio.";

    String MOJIX_MENSAJE_TRANSACCION_ANULADA = "mojix.mensaje.transaccion.anulada.126";
    String MOJIX_MENSAJE_TRANSACCION_ANULADA_DEFAULT = "Tenemos problemas al procesar la transaccion $TRANSID, vuelva a generar otra. ";

    String MOJIX_MENSAJE_CLIENTE_TOO_MANY = "mojix.mensaje.cliente_too.many.127";
    String MOJIX_MENSAJE_CLIENTE_TOO_MANY_DEFAULT = "Se encontr\u00f3 m\u00e1s de un registro para el c\u00f3digo de cliente $CODCLIENTE.";

    String MOJIX_MENSAJE_INFORMATIVO_CODIGO_CONFIRMACION_INVALIDO = "mojix.mensaje.informativo.codigo.confirmacion.invalido.128";
    String MOJIX_MENSAJE_INFORMATIVO_CODIGO_CONFIRMACION_INVALIDO_DEFAULT = "No se encontraron registros  para el c\u00f3digo de confirmaci\u00f3 $CODCONFIRMACION.";

    String MOJIX_MENSAJE_INFORMATIVO_TRANSACTIONID_INVALIDO = "mojix.mensaje.informativo.codigo.invalido.129";
    String MOJIX_MENSAJE_INFORMATIVO_TRANSACTIONID_INVALIDO_DEFAULT = "No se encontraron registros para la transacci\u00f3n $TRANSID";

    String MOJIX_MENSAJE_NO_EXISTE_DATA = "mojix.mensaje.noexiste.data.130";
    String MOJIX_MENSAJE_NO_EXISTE_DATA_DEFAULT = "No se encontraron registros";

    String MOJIX_MENSAJE_SIN_CONTRATO = "mojix.mensaje.sin.contrato.131";
    String MOJIX_MENSAJE_SIN_CONTRATO_DEFAULT = "Apersónate a cualquier agencia del Banco a nivel nacional para firmar la Adenda y acceder al servicio 'BISA Neo'. Posteriormente, podrás realizar la afiliación a través de este canal.";

    String MOJIX_MENSAJE_ERROR_CLAVE_MOVIL = "mojix.mensaje.error.clave.movil.132";
    String MOJIX_MENSAJE_ERROR_CLAVE_MOVIL_DEFAULT = "Hubo un error al generar la clave movil.";

    String MOJIX_MENSAJE_ERROR_GIRO_MOVIL = "mojix.mensaje.error.giro.movil.133";
    String MOJIX_MENSAJE_ERROR_GIRO_MOVIL_DEFAULT = "Hubo un error al emitir el giro movil.";

    String MOJIX_MENSAJE_ERROR_RECARGA = "mojix.mensaje.error.recarga.134";
    String MOJIX_MENSAJE_ERROR_RECARGA_DEFAULT = "Hubo un error al recargar el telefono.";

    String MOJIX_MENSAJE_PPC01 = "mojix.mensaje.campo.transactionid.135";
    String MOJIX_MENSAJE_PPC01_DEFAULT = "Esta operación es igual o supera los $us10,000, por lo " +
            "tanto se requiere llenar el formulario PCC-01 de " +
            "acuerdo a la normativa vigente. " +
            "Por el momento este formulario no esta disponible " +
            "para esta transacción, por lo que debes apersonarte " +
            "por las oficinas del Banco para realizar esta " +
            "transacción. ";

    String MOJIX_MENSAJE_TIPO_MONEDAS_VALIDAS = "mojix.mensaje.campo.transactionid.136";
    String MOJIX_MENSAJE_TIPO_MONEDAS_VALIDAS_DEFAULT = "Se intento utilizar una moneda que no es aceptada.";

    //MENSAJE CODIGO TOKEN SMS*****************************************************************************************/

    String MOJIX_NOTIFICACION_TOKEN = "mojix.notificacion.token";
    String MOJIX_NOTIFICACION_TOKEN_DEFAULT = "Banco Bisa: Tu Token SMS es: $TOKENSMS y es valido para una sola transaccion hasta $TOKENTIEMPOVIDA.";

    //=========================================================================
    //=======================DATA QUEUE====================================

    String DATAQUEUE_FACTORY = ".dataqueue.factory";
    String DATAQUEUE_FACTORY_DEFAULT = "as400Factory";

    String DATAQUEUE_LIBRERIACOLAS = ".dataqueue.libreriaColas";
    String DATAQUEUE_LIBRERIACOLAS_DEFAULT = "SBISASQL";

    String DATAQUEUE_LIBRERIAFORMATOS = ".dataqueue.libreriaFormatos";
    String DATAQUEUE_LIBRERIAFORMATOS_DEFAULT = "DICBSBISA";

    String DATAQUEUE_COLAENTRADA = ".dataqueue.colaEntrada";
    String DATAQUEUE_COLAENTRADA_DEFAULT = "DTAQPRB";

    String DATAQUEUE_COLASALIDA = ".dataqueue.colaSalida";
    String DATAQUEUE_COLASALIDA_DEFAULT = "DTAQPRB";

    String DATAQUEUE_BEAN = ".dataqueue.bean";
    String DATAQUEUE_BEAN_DEFAULT = "bean";

    String DATAQUEUE_ELIMINARCONTENIDOPRIMERAVEZ = ".dataqueue.eliminarContenidoDataqueueLaPrimeraVez";
    String DATAQUEUE_ELIMINARCONTENIDOPRIMERAVEZ_DEFAULT = "bean";
    //==========================================================
    String BISA_NIT = "sin.ciclos.banco.nit";
    BigDecimal BISA_NIT_DEFAULT = new BigDecimal(1020149020);

    String BISA_RAZON_SOCIAL = "sin.ciclos.banco.razon.social";
    String BISA_RAZON_SOCIAL_DEFAULT = "BANCO BISA S.A.";

    String BISA_CASA_MATRIZ = "sin.ciclos.banco.casa.matriz";
    String BISA_CASA_MATRIZ_DEFAULT = "CASA MATRIZ, 16 DE JULIO 1628, BANCO BISA 11 0, CENTRAL, LA PAZ";

    String BISA_NACIONAL = "sin.ciclos.banco.nacional";
    String BISA_NACIONAL_DEFAULT = "LA PAZ";

    String BISA_SIN_USUARIO = "sin.ciclos.usuario";
    String BISA_SIN_USUARIO_DEFAULT = "BISASIN4";

    String BISA_CODIGO_DOSIFICACION_PARALELA = "sin.ciclos.dosificacion.paralela";
    Integer BISA_CODIGO_DOSIFICACION_PARALELA_DEFAULT = 0;

    String BISA_CODIGO_SUCURSAL_DOSIFICACION = "sin.ciclos.dosificacion.sucursal";
    Integer BISA_CODIGO_SUCURSAL_DOSIFICACION_DEFAULT = 0;

    String BISA_CODIGO_ACTIVIDAD_ECONOMICA = "sin.ciclos.banco.codigo.actividad.economica";
    Long BISA_CODIGO_ACTIVIDAD_ECONOMICA_DEFAULT = (long) 72803;

    String SIN_DOSIFICACIONES_PENDIENTES = "sin.ciclos.dosificaciones.pendientes";
    int SIN_DOSIFICACIONES_PENDIENTES_DEFAULT = 1;

    String BISA_LEYENDA = "sin.ciclos.banco.leyenda.default";
    String BISA_LEYENDA_DEFAULT = "Ley No 453 La entidad financiera tiene la obligacion de promover la educacion financiera";

    String BISA_SIN_FACTURA_DESDE = "sin.ciclos.factura.desde";
    String BISA_SIN_FACTURA_DESDE_DEFAULT = "1";

    String BISA_SIN_FACTURA_HASTA = "sin.ciclos.factura.hasta";
    String BISA_SIN_FACTURA_HASTA_DEFAULT = "999999999";

    String BISA_SIN_FACTURA_ACTUAL = "sin.ciclos.factura.actual";
    String BISA_SIN_FACTURA_ACTUAL_DEFAULT = "0";

    //========================    MENSAJES     ==================================

    String BISA_SIN_MENSAJE_TIPO_PERSONA = "sin.ciclos.mensaje.req.tipopersona";
    String BISA_SIN_MENSAJE_TIPO_PERSONA_DEFAULT = "El Tipo Persona es requerido.";

    String BISA_SIN_MENSAJE_NUMERO_DOCUMENTO = "sin.ciclos.mensaje.req.numerodocumento";
    String BISA_SIN_MENSAJE_NUMERO_DOCUMENTO_DEFAULT = "El Número Documento es requerido.";

    String BISA_SIN_MENSAJE_INFO_ADICIONAL = "sin.ciclos.mensaje.req.infoadicional";
    String BISA_SIN_MENSAJE_INFO_ADICIONAL_DEFAULT = "La Información Adicional es requerida.";

    String BISA_SIN_MENSAJE_ERROR_INFO_ADICIONAL = "sin.ciclos.mensaje.error.infoadicional";
    String BISA_SIN_MENSAJE_ERROR_INFO_ADICIONAL_DEFAULT = "La Informaci\u00f3n Adicional es incorrecta.";

    String BISA_SIN_MENSAJE_SIN_FACTURAS = "sin.ciclos.mensaje.sinfacturas";
    String BISA_SIN_MENSAJE_SIN_FACTURAS_DEFAULT = "Sin Facturas";

    String BISA_SIN_MENSAJE_SIN_ERROR = "sin.ciclos.mensaje.sinerror";
    String BISA_SIN_MENSAJE_SIN_ERROR_DEFAULT = "OK";

    String BISA_SIN_MENSAJE_BROWSER = "sin.ciclos.mensaje.req.browser";
    String BISA_SIN_MENSAJE_BROWSER_DEFAULT = "El navegador browser es requerido.";

    String BISA_SIN_MENSAJE_IP = "sin.ciclos.mensaje.req.ip";
    String BISA_SIN_MENSAJE_IP_DEFAULT = "El n\u00famero IP es requerido.";

    String BISA_SIN_MENSAJE_TERMINAL = "sin.ciclos.mensaje.req.terminal";
    String BISA_SIN_MENSAJE_TERMINAL_DEFAULT = "La terminal de consulta es requerida.";

    String BISA_SIN_MENSAJE_NUM_AUTORIZACION = "sin.ciclos.mensaje.req.numautorizacion";
    String BISA_SIN_MENSAJE_NUM_AUTORIZACION_DEFAULT = "El n\u00famero de autorizaci\u00f3n es requerido.";

    String BISA_SIN_MENSAJE_NUM_FACTURA = "sin.ciclos.mensaje.req.numfactura";
    String BISA_SIN_MENSAJE_NUM_FACTURA_DEFAULT = "El n\u00famero de factura es requerido.";

    String BISA_SIN_MENSAJE_IDENTIFICADOR = "sin.ciclos.mensaje.req.identificador";
    String BISA_SIN_MENSAJE_IDENTIFICADOR_DEFAULT = "El identificador es requerido.";

    String BISA_SIN_MENSAJE_RESPUESTA = "sin.ciclos.mensaje.req.respuesta";
    String BISA_SIN_MENSAJE_RESPUESTA_DEFAULT = "El Tipo Persona es requerido.";

    String BISA_SIN_MENSAJE_EVENTO = "sin.ciclos.mensaje.req.evento";
    String BISA_SIN_MENSAJE_EVENTO_DEFAULT = "El evento es requerido.";

    String SIN_PORT_NAME = "";
    String SIN_PORT_NAME_DEFAULT = "";

    String SIN_URL = "sin.ciclos.endpoint";
    String SIN_URL_DEFAULT = "http://localhost:51335/SinFacCiclosService.svc";

    String SIN_MENSAJE_SIN_INICIO = "sin.ciclos.dosificacion.mensaje.sininicio";
    String SIN_MENSAJE_EXISTE = "sin.ciclos.dosificacion.mensaje.existe";
    String SIN_MENSAJE_EXISTE_DOSIF_PENDIENTE = "sin.ciclos.dosificacion.mensaje.existe.dosif.pendiente";
    String SIN_MENSAJE_NO_EXISTE = "sin.ciclos.dosificacion.mensaje.noexiste";
    String SIN_MENSAJE_ERROR_SERVICIO = "sin.ciclos.dosificacion.mensaje.error.servicio";
    String SIN_MENSAJE_ERROR_INESPERADO = "sin.ciclos.dosificacion.mensaje.error.inseperado";

    //========================
    String BISA_SIN_USUARIO_JOB = "sin.ciclos.usuario.job";
    String BISA_SIN_USUARIO_JOB_DEFAULT = "BISASIN4";

    String BISA_SIN_CARACTERISTICAS_JOB = "sin.ciclos.caracteristicas.job";
    String BISA_SIN_CARACTERISTICAS_JOB_DEFAULT = "CSCF,";

    String BISA_SIN_REINTENTOS_JOB = "sin.ciclos.reintentos.job";
    int BISA_SIN_REINTENTOS_JOB_DEFAULT = 10;

    //========================

    String SIN_NOMBRE_ARCHIVO = "sin.ciclos.nombre.archivo";
    String SIN_NOMBRE_ARCHIVO_DEFAULT = "sin";

    String SIN_SEPARADOR_ARCHIVO = "sin.ciclos.separador.archivo";
    String SIN_SEPARADOR_ARCHIVO_DEFAULT = "|";

    String SIN_EMAIL_ENVIO = "sin.ciclos.email.destino";
    String SIN_EMAIL_ENVIO_DEFAULT = "rsalvatierra@grupopbisa.com";

    String SIN_SUBJECT_MAIL = "sin.ciclos.subject.mail";
    String SIN_SUBJECT_MAIL_DEFAULT = "Proceso Cierre Facturacion por Ciclos";

    String SIN_CARACTERES_ESPECIALES = "sin.ciclos.caracteres.especiales";
    String SIN_CARACTERES_ESPECIALES_DEFAULT = "#";

    String SIN_CARACTERES_ESPECIALES_REEMPLAZO = "sin.ciclos.caracteres.especiales.reemplazo";
    String SIN_CARACTERES_ESPECIALES_REEMPLAZO_DEFAULT = "\u00d1";

    //========================

    String SIN_NUM_CONTROL_DIGITOS = "sin.ciclos.dosificaciones.num.control";
    String SIN_NUM_CONTROL_DIGITOS_DEFAULT = "3:9";

    String SIN_NUM_AUTORIZACION_DIGITOS = "sin.ciclos.dosificaciones.num.autorizacion";
    String SIN_NUM_AUTORIZACION_DIGITOS_DEFAULT = "6:15";

    String LIBRERIA_VALIDACION_NIT = "rpg.libreria.validacion.nit";
    String LIBRERIA_VALIDACION_NIT_DEFAULT = "SICBSBISA";

    String PROGRAMA_VALIDACION_NIT = "rpg.programa.validacion.nit";
    String PROGRAMA_VALIDACION_NIT_DEFAULT = "NT0004";

    String LIBRERIA_SGTE_DIA_HABIL = "rpg.libreria.sgte.dia.habil";
    String LIBRERIA_SGTE_DIA_HABIL_DEFAULT = "ZICBSBISA";

    String PROGRAMA_SGTE_DIA_HABIL = "rpg.programa.sgte.dia.habil";
    String PROGRAMA_SGTE_DIA_HABIL_DEFAULT = "MSVA35";

    String BCB_INDICADORES = "bcb.";
    String BCB_COTIZACIONES = "bcb.cotizaciones.";
    String BCB_TRE = "bcb.tre.";
    String BCB_COTIZACIONES_PENDIENTES_DE_POSTEO = "bcb.cotizaciones.pendientes.de.posteo";
    String BCB_COTIZACIONES_PENDIENTES_DE_POSTEO_DEFAULT = "No puede ingresar cotizaciones, existen pendientes de " +
            "posteo.";
    String BCB_COTIZACIONES_EMAIL_ENVIO = "bcb.cotizaciones.email.envio";
    String BCB_COTIZACIONES_EMAIL_ENVIO_DEFAULT = "fvelarde@grupobisa.com;mvargas@grupobisa.com;mtorrico@grupobisa.com";

    String BCB_COTIZACIONES_SMS_ENVIO = "bcb.cotizaciones.sms.envio";
    String BCB_COTIZACIONES_SMS_ENVIO_DEFAULT = "71536144;71557194;70170999;75141698";

    String BCB_COTIZACIONES_SMS_PRIORIDAD = "bcb.cotizaciones.sms.prioridad";
    int BCB_COTIZACIONES_SMS_PRIORIDAD_DEFAULT = 1;

    String BCB_COTIZACIONES_UMBRAL_HORA_WARNING = "bcb.cotizaciones.umbral.hora.warning";
    String BCB_COTIZACIONES_UMBRAL_HORA_WARNING_DEFAULT = "21:00:00";//"21:00:00";

    String BCB_COTIZACIONES_UMBRAL_HORA_TOPE = "bcb.cotizaciones.umbral.hora.tope";
    String BCB_COTIZACIONES_UMBRAL_HORA_TOPE_DEFAULT = "23:00:00";

    String BCB_COTIZACIONES_NOTIFICAR_REGISTRO_SIEMPRE = "bcb.cotizaciones.notificar.registro.siempre";
    String BCB_COTIZACIONES_NOTIFICAR_REGISTRO_SIEMPRE_DEFAULT = "true";

    //GRAVE
    String BCB_COTIZACIONES_NOTIFICACION_GRAVE_EMAIL = "bcb.cotizaciones.notificacion.grave.email";
    String BCB_COTIZACIONES_NOTIFICACION_GRAVE_EMAIL_DEFAULT = "Estimados/das.\n\nHasta este momento, que son las HORA_ACTUAL, no se logr\u00f3 obtener las cotizaciones de fecha FECHA_CARGA del Banco Central de Bolivia. Se sugiere tomar los recaudos correspondientes ya que no se intentar\u00e1 m\u00e1s.";

    String BCB_COTIZACIONES_NOTIFICACION_GRAVE_SMS = "bcb.cotizaciones.notificacion.grave.sms";
    String BCB_COTIZACIONES_NOTIFICACION_GRAVE_SMS_DEFAULT = "ISB-Alerta grave:Hasta las HORA_ACTUAL, no se obtuvo " +
            "las cotizaciones del FECHA_CARGA del BCB. NO SE INTENTARA MAS.";

    //ALERTA
    String BCB_COTIZACIONES_NOTIFICACION_ALERTA_EMAIL = "bcb.cotizaciones.notificacion.alerta.email";
    String BCB_COTIZACIONES_NOTIFICACION_ALERTA_EMAIL_DEFAULT = "Estimados/das.\n\nHasta este momento, que son las HORA_ACTUAL, no se logr\u00f3 obtener las cotizaciones de fecha FECHA_CARGA del Banco Central de Bolivia. Se seguir\u00e1 intentando hasta las HORA_TOPE pero se sugiere tomar los recaudos correspondientes.\n\n";

    String BCB_COTIZACIONES_NOTIFICACION_ALERTA_SMS = "bcb.cotizaciones.notificacion.alerta.sms";
    String BCB_COTIZACIONES_NOTIFICACION_ALERTA_SMS_DEFAULT = "ISB-Alerta: Hasta las HORA_ACTUAL, no se obtuvo las cotizaciones de FECHA_CARGA del BCB, se seguira intentando hasta las HORA_TOPE";

    //DESALARMA
    String BCB_COTIZACIONES_NOTIFICACION_DESALARMA_EMAIL = "bcb.cotizaciones.notificacion.desalarma.email";
    String BCB_COTIZACIONES_NOTIFICACION_DESALARMA_EMAIL_DEFAULT = "Estimados/das.\n\nLas cotizaciones de fecha FECHA_CARGA han sido registradas hoy a las HORA_ACTUAL y ser\u00e1n aplicadas a media noche.\n\n";

    String BCB_COTIZACIONES_NOTIFICACION_DESALARMA_SMS = "bcb.cotizaciones.notificacion.desalarma.sms";
    String BCB_COTIZACIONES_NOTIFICACION_DESALARMA_SMS_DEFAULT = "ISB-Desalarma: Las cotizaciones de FECHA_CARGA han sido registradas a las HORA_ACTUAL";

    String BCB_TRE_SMS_PRIORIDAD = "bcb.tre.sms.prioridad";
    int BCB_TRE_SMS_PRIORIDAD_DEFAULT = 1;

    String BCB_TRE_CARGA_EMAIL_ENVIO = "bcb.tre.carga.email.envio";
    String BCB_TRE_CARGA_EMAIL_ENVIO_DEFAULT = "psandoval@grupobisa.com;cpozo@grupobisa.com;iiriarte@grupobisa.com";
    String BCB_TRE_CARGA_SMS_ENVIO = "bcb.tre.carga.sms.envio";
    String BCB_TRE_CARGA_SMS_ENVIO_DEFAULT = "72533008;70164372;70663204";

    String BCB_TRE_CARGA_ESCALADA_EMAIL_ENVIO = "bcb.tre.carga.escalada.email.envio";
    String BCB_TRE_CARGA_ESCALADA_EMAIL_ENVIO_DEFAULT = "ASiles@grupobisa.com;fmonroy@grupobisa.com";
    String BCB_TRE_CARGA_ESCALADA_SMS_ENVIO = "bcb.tre.carga.escalada.sms.envio";
    String BCB_TRE_CARGA_ESCALADA_SMS_ENVIO_DEFAULT = "72514741;72011171";

    //VERIFICACION ENCARGADOS DE CONTABILIDAD PARA LA CARGA TASA TRE
    String BCB_TRE_CARGA_VERIFICACION_HORA_INICIO = "bcb.tre.carga.verificacion.hora.inicio";
    String BCB_TRE_CARGA_VERIFICACION_HORA_INICIO_DEFAULT = "09:00:00";

    String BCB_TRE_CARGA_VERIFICACION_HORA_FIN = "bcb.tre.carga.verificacion.hora.fin";
    String BCB_TRE_CARGA_VERIFICACION_HORA_FIN_DEFAULT = "11:00:00";

    String BCB_TRE_CARGA_VERIFICACION_EMAIL = "bcb.tre.carga.verificacion.email";
    String BCB_TRE_CARGA_VERIFICACION_EMAIL_DEFAULT = "Estimados/das.\n\nHasta este momento, que son las HORA_ACTUAL, no se encontr\u00f3 el registro de la tasa TRE de esta semana. Se sugiere usar la funci\u00f3n 503020 en el Fiserv para realizar el registro respectivo.\n\n";

    String BCB_TRE_CARGA_VERIFICACION_SMS = "bcb.tre.carga.verificacion.sms";
    String BCB_TRE_CARGA_VERIFICACION_SMS_DEFAULT = "ISB-Alerta: Hasta las HORA_ACTUAL, no se encontro el registro de la tasa TRE. Se sugiere hacer el registro";


    //VERIFICACION JEFES DE CONTABILIDAD
    String BCB_TRE_CARGA_VERIFICACION_ESCALADA_HORA_INICIO = "bcb.tre.carga.verificacion.escalada.hora.inicio";
    String BCB_TRE_CARGA_VERIFICACION_ESCALADA_HORA_INICIO_DEFAULT = "12:00:00";

    String BCB_TRE_CARGA_VERIFICACION_ESCALADA_HORA_FIN = "bcb.tre.carga.verificacion.escalada.hora.fin";
    String BCB_TRE_CARGA_VERIFICACION_ESCALADA_HORA_FIN_DEFAULT = "23:00:00";

    String BCB_TRE_CARGA_VERIFICACION_ESCALADA_EMAIL = "bcb.tre.carga.verificacion.escalada.email";
    String BCB_TRE_CARGA_VERIFICACION_ESCALADA_EMAIL_DEFAULT = "Estimados/das.\n\nHasta este momento, que son las HORA_ACTUAL, no se encontr\u00f3 el registro de la tasa TRE de esta semana. Se sugiere tomar los recaudos que correspondan.\n\n";

    String BCB_TRE_CARGA_VERIFICACION_ESCALADA_SMS = "bcb.tre.carga.verificacion.escalada.email";
    String BCB_TRE_CARGA_VERIFICACION_ESCALADA_SMS_DEFAULT = "ISB-Alerta: Hasta las HORA_ACTUAL, no se encontro el registro de la tasa TRE. Tome los recaudos";

    //============== Variables INFOCRED ============== //
    String NUMERO_DIAS_VALIDO_REGISTRO_INFOCRED = "infocred.numero.dias.valido.registro";
    long NUMERO_DIAS_VALIDO_REGISTRO_INFOCRED_DEFAULT = 19;

    String ES_HABILITADO_CONSUMO_INFOCRED = "infocred.esta.habilitado.consumo";
    String ES_HABILITADO_CONSUMO_INFOCRED_DEFAULT = "NO";

    String INFOCRED_END_POINT = "infocred.end.point";
    String INFOCRED_END_POINT_DEFAULT = "https://www.infocred.com.bo/BICService/BICService.svc";

    String INFOCRED_USUARIO_WEBSERVICE = "infocred.usuario.webservice";
    String INFOCRED_USUARIO_WEBSERVICE_DEFAULT = "BISWEBSERVICE";

    String INFOCRED_PASSWORD_WEBSERVICE = "infocred.password.webservice";
    String INFOCRED_PASSWORD_WEBSERVICE_DEFAULT = "openssl:U2FsdGVkX1+INMB7QOOGgEpEw1Ffnj+6365KWtiPTUo=";

    String INFOCRED_HOST_PROXY = "infocred.host.proxy";
    String INFOCRED_HOST_PROXY_DEFAULT = "";

    String INFOCRED_PORT_PROXY = "infocred.port.proxy";
    String INFOCRED_PORT_PROXY_DEFAULT = "";

    String INFOCRED_USUARIO_PROXY = "infocred.usuario.proxy";
    String INFOCRED_USUARIO_PROXY_DEFAULT = "";

    String INFOCRED_PASSWORD_PROXY = "infocred.password.proxy";
    String INFOCRED_PASSWORD_PROXY_DEFAULT = "";

    String INFOCRED_LIBRERIA_REPORTES = "infocred.libreria.reportes";
    String INFOCRED_LIBRERIA_REPORTES_DEFAULT = "SBISASQL";

    String INFOCRED_REPORTES_MES_1 = "infocred.mes.01";
    String INFOCRED_REPORTES_MES_1_DEFAULT = "ENE";

    String INFOCRED_REPORTES_MES_2 = "infocred.mes.02";
    String INFOCRED_REPORTES_MES_2_DEFAULT = "FEB";

    String INFOCRED_REPORTES_MES_3 = "infocred.mes.03";
    String INFOCRED_REPORTES_MES_3_DEFAULT = "MAR";

    String INFOCRED_REPORTES_MES_4 = "infocred.mes.04";
    String INFOCRED_REPORTES_MES_4_DEFAULT = "ABR";

    String INFOCRED_REPORTES_MES_5 = "infocred.mes.05";
    String INFOCRED_REPORTES_MES_5_DEFAULT = "MAY";

    String INFOCRED_REPORTES_MES_6 = "infocred.mes.06";
    String INFOCRED_REPORTES_MES_6_DEFAULT = "JUN";

    String INFOCRED_REPORTES_MES_7 = "infocred.mes.07";
    String INFOCRED_REPORTES_MES_7_DEFAULT = "JUL";

    String INFOCRED_REPORTES_MES_8 = "infocred.mes.08";
    String INFOCRED_REPORTES_MES_8_DEFAULT = "AGO";

    String INFOCRED_REPORTES_MES_9 = "infocred.mes.09";
    String INFOCRED_REPORTES_MES_9_DEFAULT = "SEP";

    String INFOCRED_REPORTES_MES_10 = "infocred.mes.10";
    String INFOCRED_REPORTES_MES_10_DEFAULT = "OCT";

    String INFOCRED_REPORTES_MES_11 = "infocred.mes.11";
    String INFOCRED_REPORTES_MES_11_DEFAULT = "NOV";

    String INFOCRED_REPORTES_MES_12 = "infocred.mes.12";
    String INFOCRED_REPORTES_MES_12_DEFAULT = "DIC";

    //============== Variables trabajos asincronos ============== //
    String DIRECTORIO_BASE_JOBS_DEFAULT = File.separator + "opt" + File.separator + "INFOCRED"; //valor fijo

    String INFOCRED_ALERTA_INICIO_CUERPO_CONSULTA_AJENA = "infocred.alerta.inicio.cuerpo.consulta.ajena";
    int INFOCRED_ALERTA_INICIO_CUERPO_CONSULTA_AJENA_DEFAULT = 11;

    String INFOCRED_ALERTA_DATOS_CONSULTA_AJENA = "infocred.alerta.datos.consulta.ajena";
    String INFOCRED_ALERTA_DATOS_CONSULTA_AJENA_DEFAULT = "1|2|3|4|5|x|7|8|x";

    String INFOCRED_ALERTA_COLUMNA_INSERTAR_CONSULTA_AJENA = "infocred.alerta.columna.insertar.consulta.ajena";
    String INFOCRED_ALERTA_COLUMNA_INSERTAR_CONSULTA_AJENA_DEFAULT = "1|2|3|4|5|6|7";

    String INFOCRED_ALERTA_INICIO_CUERPO_CAMBIO_ESTADO = "infocred.alerta.inicio.cuerpo.cambio.estado";
    int INFOCRED_ALERTA_INICIO_CUERPO_CAMBIO_ESTADO_DEFAULT = 12;

    String INFOCRED_ALERTA_DATOS_CAMBIO_ESTADO = "infocred.alerta.datos.cambio.estado";
    String INFOCRED_ALERTA_DATOS_CAMBIO_ESTADO_DEFAULT = "1|2|3|4|5|x|7|8|9|10|11|12|13";

    String INFOCRED_ALERTA_COLUMNA_INSERTAR_CAMBIO_ESTADO = "infocred.alerta.columna.insertar.cambio.estado";
    String INFOCRED_ALERTA_COLUMNA_INSERTAR_CAMBIO_ESTADO_DEFAULT = "1|2|3|4|5|6|8|9|10|11|12|13";

    String INFOCRED_ALERTA_INICIO_CUERPO_NUEVA_DEUDA = "infocred.alerta.inicio.cuerpo.nueva.deuda";
    int INFOCRED_ALERTA_INICIO_CUERPO_NUEVA_DEUDA_DEFAULT = 12;

    String INFOCRED_ALERTA_DATOS_NUEVA_DEUDA = "infocred.alerta.datos.nueva.deuda";
    String INFOCRED_ALERTA_DATOS_NUEVA_DEUDA_DEFAULT = "1|2|3|4|5|x|7|8|9|10|11";

    String INFOCRED_ALERTA_COLUMNA_INSERTAR_NUEVA_DEUDA = "infocred.alerta.columna.insertar.nueva.deuda";
    String INFOCRED_ALERTA_COLUMNA_INSERTAR_NUEVA_DEUDA_DEFAULT = "1|2|3|4|5|6|8|9|10|11";

    String INFOCRED_ALERTA_INICIO_CUERPO_VARIACION_SALDO = "infocred.alerta.inicio.cuerpo.variacion.saldo";
    int INFOCRED_ALERTA_INICIO_CUERPO_VARIACION_SALDO_DEFAULT = 12;

    String INFOCRED_ALERTA_DATOS_VARIACION_SALDO = "infocred.alerta.datos.variacion.saldo";
    String INFOCRED_ALERTA_DATOS_VARIACION_SALDO_DEFAULT = "1|2|3|4|5|x|7|8|9|10|11|12";

    String INFOCRED_ALERTA_COLUMNA_INSERTAR_VARIACION_SALDO = "infocred.alerta.columna.insertar.variacion.saldo";
    String INFOCRED_ALERTA_COLUMNA_INSERTAR_VARIACION_SALDO_DEFAULT = "1|2|3|4|5|6|8|9|10|11|13";

    String INFOCRED_ALERTA_INICIO_CUERPO_NUEVA_CONSULTA_AJENA = "infocred.alerta.inicio.cuerpo.nueva.consulta.ajena";
    int INFOCRED_ALERTA_INICIO_CUERPO_NUEVA_CONSULTA_AJENA_DEFAULT = 9;

    String INFOCRED_ALERTA_DATOS_NUEVA_CONSULTA_AJENA = "infocred.alerta.datos.nueva.consulta.ajena";
    String INFOCRED_ALERTA_DATOS_NUEVA_CONSULTA_AJENA_DEFAULT = "1|2|3|4|5|6|7|8|9|10";

    String INFOCRED_ALERTA_COLUMNA_INSERTAR_NUEVA_CONSULTA_AJENA = "infocred.alerta.columna.insertar.nueva.consulta.ajena";
    String INFOCRED_ALERTA_COLUMNA_INSERTAR_NUEVA_CONSULTA_AJENA_DEFAULT = "1|2|3|4|6|14|7|15|16|17";

    String INFOCRED_ALERTA_INICIO_CUERPO_RESUMEN = "infocred.alerta.inicio.cuerpo.resumen";
    int INFOCRED_ALERTA_INICIO_CUERPO_RESUMEN_DEFAULT = 11;

    String INFOCRED_ALERTA_DATOS_RESUMEN = "infocred.alerta.datos.resumen";
    String INFOCRED_ALERTA_DATOS_RESUMEN_DEFAULT = "1|2";

    String INFOCRED_ALERTA_COLUMNA_INSERTAR_RESUMEN = "infocred.alerta.columna.insertar.resumen";
    String INFOCRED_ALERTA_COLUMNA_INSERTAR_RESUMEN_DEFAULT = "1|2";

    String INFOCRED_ALERTA_INICIO_CUERPO_NUEVO_RESUMEN = "infocred.alerta.inicio.cuerpo.nuevo.resumen";
    int INFOCRED_ALERTA_INICIO_CUERPO_NUEVO_RESUMEN_DEFAULT = 9;

    String INFOCRED_ALERTA_DATOS_NUEVO_RESUMEN = "infocred.alerta.datos.nuevo.resumen";
    String INFOCRED_ALERTA_DATOS_NUEVO_RESUMEN_DEFAULT = "1|2|3|4|5|6";

    String INFOCRED_ALERTA_COLUMNA_INSERTAR_NUEVO_RESUMEN = "infocred.alerta.columna.insertar.nuevo.resumen";
    String INFOCRED_ALERTA_COLUMNA_INSERTAR_NUEVO_RESUMEN_DEFAULT = "1|3|2|4|5|6";

    String SEPARADOR_CARTERA_COMA = "infocred.separador.cartera.coma";
    String SEPARADOR_CARTERA_COMA_DEFAULT = ",";

    String SEPARADOR_CARTERA_PIPE = "infocred.separador.cartera.pipe";
    String SEPARADOR_CARTERA_PIPE_DEFAULT = "|";

    String SEPARADOR_CARTERA_ZIP_PIPE = "infocred.separador.cartera.zip.pipe";
    String SEPARADOR_CARTERA_ZIP_PIPE_DEFAULT = "|";

    String INFOCRED_LISTADO_EMAILS = "infocred.listado.emails";
    String INFOCRED_LISTADO_EMAILS_DEFAULT = "gmercado@grupobisa.com";

    String INFOCRED_USUARIO_ALTA_JOBS = "SISTEMA";

    String INFOCRED_LISTA_TIPO_DOCUMENTOS = "infocred.lista.tipo.documento";
    String INFOCRED_LISTA_TIPO_DOCUMENTOS_DEFAULT =
            "Persona Extrangera|2|CI|1001|Libreta Militar|1002|Pasaporte|1003|RUN|1004" +
                    "|Otro|1005|RUC|3040|NIT|4050|Correlativo Persona Natural|1006|Empresa Extranjera|1007" +
                    "|Correlativo Persona Juridica|1008|Por resolucion|1009|CI Duplicado|1010";

    // Conexion al FTP de INFOCRED
    String INFOCRED_URL_FTP = "infocred.url.ftp";
    String INFOCRED_URL_FTP_DEFAULT = "1.10.3.82";

    String INFOCRED_PUERTO_FTP = "infocred.puerto.ftp";
    int INFOCRED_PUERTO_FTP_DEFAULT = 21;

    String INFOCRED_USUARIO_FTP = "infocred.usuario.ftp";
    String INFOCRED_USUARIO_FTP_DEFAULT = "root";

    String INFOCRED_PASSWORD_FTP = "infocred.password.ftp";
    String INFOCRED_PASSWORD_FTP_DEFAULT = "desa";

    String INFOCRED_TIPO_ARCHIVO_FTP = "infocred.tipo.archivo.ftp";
    Integer INFOCRED_TIPO_ARCHIVO_FTP_DEFAULT = 2;

    String INFOCRED_ARCHIVO_ALERTA_1 = "infocred.archivo.alerta.1";
    String INFOCRED_ARCHIVO_ALERTA_1_DEFAULT = "1_ConsultaAjena_217324033";

    String INFOCRED_ARCHIVO_ALERTA_2 = "infocred.archivo.alerta.2";
    String INFOCRED_ARCHIVO_ALERTA_2_DEFAULT = "2_CambioEstado_217324033";

    String INFOCRED_ARCHIVO_ALERTA_3 = "infocred.archivo.alerta.3";
    String INFOCRED_ARCHIVO_ALERTA_3_DEFAULT = "3_NuevaDeuda_217324033";

    String INFOCRED_ARCHIVO_ALERTA_4 = "infocred.archivo.alerta.4";
    String INFOCRED_ARCHIVO_ALERTA_4_DEFAULT = "4_VariacionSaldo_217324033";

    String INFOCRED_ARCHIVO_ALERTA_5 = "infocred.archivo.alerta.5";
    String INFOCRED_ARCHIVO_ALERTA_5_DEFAULT = "13_ResumendeConsultasAjenas_217324033";

    String INFOCRED_ARCHIVO_ALERTA_6 = "infocred.archivo.alerta.6";
    String INFOCRED_ARCHIVO_ALERTA_6_DEFAULT = "19_NuevaConsultaAjena_217324033";

    String INFOCRED_ARCHIVO_ALERTA_7 = "infocred.archivo.alerta.7";
    String INFOCRED_ARCHIVO_ALERTA_7_DEFAULT = "21_NuevoResumendeConsultasAjenas_217324033";

    String INFOCRED_ARCHIVO_COMA = "infocred.archivo.coma";
    String INFOCRED_ARCHIVO_COMA_DEFAULT = "BIS #fecha.csv|yyyyMM";

    String INFOCRED_ARCHIVO_PIPE = "infocred.archivo.pipe";
    String INFOCRED_ARCHIVO_PIPE_DEFAULT = "BIS #fecha_CarteraPropia.zip|yyyyMM";

    String INFOCRED_ARCHIVO_ZIP_PIPE = "infocred.archivo.zip.pipe";
    String INFOCRED_ARCHIVO_ZIP_PIPE_DEFAULT = "BIS #fecha.zip|yyyyMM";

    String INFOCRED_SSL = "infocred.ssl";
    String INFOCRED_SSL_DEFAULT = "false";

    String INFOCRED_RUTA_CACERTS_SSL = "infocred.ruta.cacerts.ssl";
    String INFOCRED_RUTA_CACERTS_SSL_DEFAULT = "/usr/java/jdk1.8.0_102/jre/lib/security/cacerts";

    String INFOCRED_CLAVE_CACERTS_SSL = "infocred.clave.cacerts.ssl";
    String INFOCRED_CLAVE_CACERTS_SSL_DEFAULT = "changeit";

    //============== Variables Clave Movil ============== //
    String WC_CLAVEMOVIL_TIMEOUT = "consumos.web.clavemovil.timeout.segundos";
    int WC_CLAVEMOVIL_TIMEOUT_DEFAULT = 5;

    //============== Variables Giro Movil ============== //
    String WC_GIROMOVIL_TIMEOUT = "consumos.web.giro.movil.timeout.segundos";
    int WC_GIROMOVIL_TIMEOUT_DEFAULT = 10;

    //============== Variables Facturacion Electronica ============== //
    String WC_FACTURACION_TIMEOUT = "consumos.web.facturacion.timeout.segundos";
    int WC_FACTURACION_TIMEOUT_DEFAULT = 10;

    String DESACTIVAR_FACTURACION_ELECTRONICA = "desactivar.facturacion.electronica";
    String DESACTIVAR_FACTURACION_ELECTRONICA_DEFAULT = "no";

    //============== Variables Pago de Servicios ============== //
    String PAGO_SERVICIOS_CANAL = "pago.servicios.canal";
    String PAGO_SERVICIOS_CANAL_DEFAULT = "EBMOVIL";

    String PAGO_SERVICIOS_CONSULTA_SERVICIOS_TIMEOUT = "pago.servicios.consulta.srvs.timeout.secs";
    int PAGO_SERVICIOS_CONSULTA_SERVICIOS_TIMEOUT_DEFAULT = 20;

    String PAGO_SERVICIOS_CONSULTA_DEUDAS_TIMEOUT = "pago.servicios.consulta.deuds.timeout.secs";
    int PAGO_SERVICIOS_CONSULTA_DEUDAS_TIMEOUT_DEFAULT = 60;

    String PAGO_SERVICIOS_PAGO_DEUDAS_TIMEOUT = "pago.servicios.pago.deuds.timeout.milis";
    int PAGO_SERVICIOS_PAGO_DEUDAS_TIMEOUT_DEFAULT = 600000;

    String PAGO_SERVICIOS_EMPRESAS_QUE_PREFACTURAN = "pago.servicios.empresas.con.prefactura";
    String PAGO_SERVICIOS_EMPRESAS_QUE_PREFACTURAN_DEFAULT = "ELECTROPAZ";

    String PAGO_SERVICIOS_FACTURAR_ACTIVAR = "pago.servicios.activar.factura.por.envio";
    String PAGO_SERVICIOS_FACTURAR_ACTIVAR_DEFAULT = "NO";

    //============== Variables PCC01 ============== //
    String LIMITE_FORMULARO_PCC01 = "limite.formulario.pcc01.dolares";
    String LIMITE_FORMULARO_PCC01_DEFAULT = "10000";

    String MENSAJE_RESTRICCION_FORMULARIO_PCC01 =
            "Esta operación es igual o supera los $us10,000, por lo " +
                    "tanto se requiere llenar el formulario PCC-01 de " +
                    "acuerdo a la normativa vigente. " +
                    "Por el momento este formulario no esta disponible " +
                    "para esta transacción, por lo que debes apersonarte " +
                    "por las oficinas del Banco para realizar esta " +
                    "transacción. " +
                    "Estamos trabajando para incluir este formulario a esta " +
                    "transacción..";


    //CONEXION FTP ATC
    String FTP_PASSWORD_ATC = "ftp.password.atc";
    //String FTP_PASSWORD_ATC_DEFAULT="desa";
    String FTP_PASSWORD_ATC_DEFAULT = "BISA123.";

    String FTP_USER_ATC = "ftp.usuario.atc";
    //String FTP_USER_ATC_DEFAULT= "root"; //"BISA123."
    String FTP_USER_ATC_DEFAULT = "usrBISA";

    String FTP_URL_ATC = "ftp.url.atc";
    String FTP_URL_ATC_DEFAULT = "sftp://usrBISA@172.16.190.109/FILES?known_hosts=/root/.ssh/known_hosts";
    //String FTP_URL_ATC_DEFAULT = "sftp://root@1.10.3.82/FILES?known_hosts=/root/.ssh/known_hosts";

    String FTP_LOCAL_TEMP_ATC = "ftp.local.temp.atc";
    String FTP_LOCAL_TEMP_ATC_DEFAULT = "/TEMP";

    String FTP_MD5_SUBIDA_ATC = "ftp.md5.subida.atc";
    String FTP_MD5_SUBIDA_ATC_DEFAULT = "false";

    String FTP_MD5_BAJADA_ATC = "ftp.md5.bajada.atc";
    String FTP_MD5_BAJADA_ATC_DEFAULT = "false";

    String FTP_RUTA_SOLICITUD_ALTAS_ATC = "ftp.ruta.tc.atc.envio.solicitud.altas";
    String FTP_RUTA_SOLICITUD_ALTAS_ATC_DEFAULT = "/FILES/IN/TOPROCESS/ADD_ACCOUNTS/ISSUER_7";

    String FTP_RUTA_TEMPORAL_ALTA_ATC = "ftp.ruta.tc.atc.temporal.solicitud.altas";
    String FTP_RUTA_TEMPORAL_ALTA_ATC_DEFAULT = "NULL"; // /TEMP/ADD_ACCOUNT";

    String FTP_RUTA_RECEPCION_ALTAS_ATC = "ftp.ruta.tc.atc.recepcion.solicitud.altas";
    String FTP_RUTA_RECEPCION_ALTAS_ATC_DEFAULT = "/FILES/OUT/TODETOKENIZE/ADD_ACCOUNTS/ISSUER_7";

    String FTP_RUTA_DESCARTE_ALTAS_ATC = "ftp.ruta.tc.atc.descarte.solicitud.altas";
    String FTP_RUTA_DESCARTE_ALTAS_ATC_DEFAULT = "/FILES/OUT/DISCARDED/ADD_ACCOUNTS/ISSUER_7";

    String FTP_RUTA_ENVIO_PAGOS_ATC = "ftp.ruta.tc.atc.envio.pagos";
    String FTP_RUTA_ENVIO_PAGOS_ATC_DEFAULT = "/FILES/IN/TOPROCESS/ACCOUNT_PAYMENT/ISSUER_7";

    String FTP_RUTA_ENVIO_TEMPORAL_ATC = "ftp.ruta.tc.atc.envio.termporal.pagos";
    String FTP_RUTA_ENVIO_TEMPORAL_ATC_DEFAULT = "NULL";  // "/TEMP/PAYMENT_ACCOUNT";

    String FTP_RUTA_RECEPCION_PAGOS_ATC = "ftp.ruta.tc.atc.recepcion.pagos";
    String FTP_RUTA_RECEPCION_PAGOS_ATC_DEFAULT = "/FILES/OUT/PROCESSED/ACCOUNT_PAYMENT/ISSUER_7";

    String FTP_RUTA_RECEPCION_FACTURAS_ATC = "ftp.ruta.tc.atc.recepcion.facturas";
    String FTP_RUTA_RECEPCION_FACTURAS_ATC_DEFAULT = "/FILES/OUT/PROCESSED/BILLING_LEDGER/ISSUER_7";

    String FTP_RUTA_RECEPCION_EXTRACTO_ATC = "ftp.ruta.tc.atc.recepcion.extracto";
    String FTP_RUTA_RECEPCION_EXTRACTO_ATC_DEFAULT = "/FILES/OUT/PROCESSED/ATCUC076_EXTRACTS_GENERATION_FILE/ISSUER_7";

    String FTP_RUTA_DESCARTE_PAGOS_ATC = "ftp.ruta.tc.atc.descarte.pagos";
    String FTP_RUTA_DESCARTE_PAGOS_ATC_DEFAULT = "/FILES/OUT/DISCARDED/ACCOUNT_PAYMENT/ISSUER_7";

    String FTP_RUTA_RECEPCION_BALANCE_ATC = "ftp.ruta.tc.atc.balance";
    String FTP_RUTA_RECEPCION_BALANCE_ATC_DEFAULT = "/FILES/OUT/PROCESSED/ATCUC077_BALANCE_FILE/ISSUER_7";

    String FTP_RUTA_RECEPCION_SALDO_MENSUAL_ATC = "ftp.ruta.tc.atc.saldo.mensual";
    String FTP_RUTA_RECEPCION_SALDO_MENSUAL_ATC_DEFAULT = "/FILES/OUT/PROCESSED/ATCUC079_BALANCE_REPORT_FILE/ISSUER_7";

    String FTP_RUTA_RECEPCION_MOVIMIENTOS_ATC = "ftp.ruta.tc.atc.movimiento.diario";
    String FTP_RUTA_RECEPCION_MOVIMIENTOS_ATC_DEFAULT = "/FILES/OUT/PROCESSED/ATCUC086_ISSUER_MOVEMENTS_FILE/ISSUER_7";

    String FTP_RUTA_RECEPCION_MOVIMIENTOS_MENSUAL_ATC = "ftp.ruta.tc.atc.movimiento.mensual";
    String FTP_RUTA_RECEPCION_MOVIMIENTOS_MENSUAL_ATC_DEFAULT = "/FILES/OUT/PROCESSED/ATCUC080_MONTHLY_MOVS_FILE/ISSUER_7";

    String FTP_RUTA_RECEPCION_MOVIMIENTOS_MENSUAL_EMISOR_ATC = "ftp.ruta.tc.atc.movimiento.mensual.emisor";
    String FTP_RUTA_RECEPCION_MOVIMIENTOS_MENSUAL_EMISOR_ATC_DEFAULT = "/FILES/OUT/PROCESSED/ATCUC111_ISSUER_MONTHLY_MOVS_FILE/ISSUER_7";

    String FTP_FILENAME_ATC_SOLITITUD_ALTAS = "ftp.nombre.archivo.atc.solicitud.altas";
    String FTP_FILENAME_ATC_SOLITITUD_ALTAS_DEFAULT = "BIS"; //{YYYYDDMM}.TXT

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_ATC_FORZADO = "ftp.nombre.archivo.atc.recepcion.alta.forzado";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_ATC_FORZADO_DEFAULT = "NULL";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_ATC = "ftp.nombre.archivo.atc.recepcion.alta";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_ATC_DEFAULT = "BIS#fecha.out|yyyyMMdd";

    String FTP_NOMBRE_ARCHIVO_PAGOS_ATC = "ftp.nombre.archivo.atc.pagos";
    String FTP_NOMBRE_ARCHIVO_PAGOS_ATC_DEFAULT = "BISPAGO"; //{YYYYDDMM}.TXT

    String FTP_RECEPCION_NOMBRE_ARCHIVO_PAGOS_ATC = "ftp.nombre.archivo.atc.recepcion.pagos";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_PAGOS_ATC_DEFAULT = "BISPAGO#fecha.out|yyyyMMdd";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_PAGOS_ATC_FORZADO = "ftp.nombre.archivo.atc.recepcion.pagos.forzado";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_PAGOS_ATC_FORZADO_DEFAULT = "NULL";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_BALANCE_077 = "ftp.nombre.archivo.atc.recepcion.balance.077";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_BALANCE_077_DEFAULT = "Balance_#fecha\\d{6}.dat|yyyyMMdd";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_BALANCE_077_FORZADO = "ftp.nombre.archivo.atc.recepcion.balance.077.forzado";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_BALANCE_077_FORZADO_DEFAULT = "NULL";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_SALDO_MENSUAL_079 = "ftp.nombre.archivo.atc.recepcion.saldo.mensual.079";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_SALDO_MENSUAL_079_DEFAULT = "BISU\\d{4}V.dat";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_MOV_MENSUAL_080 = "ftp.nombre.archivo.atc.recepcion.movimiento.mensual.080";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_MOV_MENSUAL_080_DEFAULT = "BISM\\d{4}V.dat";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_MOV_MENSUAL_EMISOR_111 = "ftp.nombre.archivo.atc.recepcion.mov.mensual.080";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_MOV_MENSUAL_EMISOR_111_DEFAULT = "EMIABC012017.dat";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_MOVIMIETOS_086 = "ftp.nombre.archivo.atc.recepcion.movimientos.086";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_MOVIMIETOS_086_DEFAULT = "EMIBIS#fecha.dat|yyyyMMdd";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_ATC = "ftp.nombre.archivo.atc.recepcion.facturas";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_ATC_DEFAULT = "Ventas_#fecha\\d{10}.dat|ddMMyyyy";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_ATC_FORZADO = "ftp.nombre.archivo.atc.recepcion.facturas.forzado";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_ATC_FORZADO_DEFAULT = "NULL";


    String FTP_RECEPCION_NOMBRE_ARCHIVO_EXTRACTO_MENSUAL_ATC = "ftp.nombre.archivo.atc.recepcion.extracto";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_EXTRACTO_MENSUAL_ATC_DEFAULT = "EXTRACT_FILE_#fecha\\d{6}.dat|yyyyMMdd";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_EXTRACTO_MENSUAL_ATC_FORZADO = "ftp.nombre.archivo.atc.recepcion.extracto.forzado";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_EXTRACTO_MENSUAL_ATC_FORZADO_DEFAULT = "NULL";

    String FECHA_DE_PROCESO_ATC = "fecha.proceso.atc";
    String FECHA_DE_PROCESO_ATC_DEFAULT = "0";

    String LIBRERIA_TC_ATC = "rpg.libreria.tc.atc";
    String LIBRERIA_TC_ATC_DEFAULT = "zicbsbisa";

    String PROGRAMA_TC_SOLICITUD_ATC = "rpg.programa.tc.atc.solicitud.alta";
    String PROGRAMA_TC_SOLICITUD_ATC_DEFAULT = "TCRAN7005";

    String PROGRAMA_TC_RECEPCION_ALTA_ATC_HABILITADO = "rpg.programa.tc.atc.recepcion.solicitud.alta.habilitado";
    String PROGRAMA_TC_RECEPCION_ALTA_ATC_HABILITADO_DEFAULT = "false";

    String PROGRAMA_TC_RECEPCION_ALTA_ATC = "rpg.programa.tc.atc.recepcion.solicitud.alta";
    String PROGRAMA_TC_RECEPCION_ALTA_ATC_DEFAULT = "TCRAN7006";

    String PROGRAMA_TC_PAGOS_ATC = "rpg.programa.tc.atc.pagos.envio";
    String PROGRAMA_TC_PAGOS_ATC_DEFAULT = "TCRAN7007";

    String PROGRAMA_TC_DG_ATC = "rpg.programa.tc.atc.datos.generales";
    String PROGRAMA_TC_DG_ATC_DEFAULT = "TCRAN7008";

    String PROGRAMA_TC_MOVIMIENTOS_ATC = "rpg.programa.tc.atc.movimientos";
    String PROGRAMA_TC_MOVIMIENTOS_ATC_DEFAULT = "TCRAN7009";

    String PROGRAMA_TC_CIERRE_MENSUAL_ATC = "rpg.programa.tc.atc.cierre.mensual";
    String PROGRAMA_TC_CIERRE_MENSUAL_ATC_DEFAULT = "TCRAN7010";

    String TC_NOTIFICAR_EMAIL_ATC = "notificar.email.tc.atc";
    String TC_NOTIFICAR_EMAIL_ATC_DEFAULT = "atenorio@grupobisa.com";

    String TC_NOTIFICAR_EMAIL_TITULO = "tc.notificar.email.titulo";
    String TC_NOTIFICAR_EMAIL_TITULO_DEFAULT = "Notificaci\u00F3n intercambio de archivo ";

    String NOTIFICAR_SOPORTE_EMAIL = "notificar.email.soporte";
    String NOTIFICAR_SOPORTE_EMAIL_DEFAULT = "atenorio@grupobisa.com";

    String NOTIFICAR_OPERADOR_EMAIL = "notificar.email.operador";
    String NOTIFICAR_OPERADOR_EMAIL_DEFAULT = "atenorio@grupobisa.com";

    String FTP_MENSAJE_EMAIL_PROCESO_DIARIO_OPERADOR = "notificar.email.mensaje.alta.operador.atc";
    String FTP_MENSAJE_EMAIL_PROCESO_DIARIO_OPERADOR_DEFAULT = "Favor proceder con la Recepci\u00F3n de N\u00FAmeros de Tarjeta y Cuentas desde el sistema FISERV.";

    String FTP_MENSAJE_EMAIL_PROCESO_DIARIO_ERROR_OPERADOR = "notificar.email.mensaje.alta.error.operador.atc";
    String FTP_MENSAJE_EMAIL_PROCESO_DIARIO_ERROR_OPERADOR_DEFAULT = "Por favor no proceder con la Recepci\u00F3n de N\u00FAmeros de Tarjeta y Cuentas desde el sistema FISERV. \nDebido a que no hay archivos para procesar.";

    String FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_OPERADOR = "notificar.email.mensaje.movimiento.operador.atc";
    String FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_OPERADOR_DEFAULT = "Favor proceder con la Recepci\u00F3n de Datos Generales y Movimientos desde el sistema FISERV.";

    String FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_ERROR_OPERADOR = "notificar.email.mensaje.movimiento.error.operador.atc";
    String FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_ERROR_OPERADOR_DEFAULT = "Por favor no proceder con la Recepci\u00F3n de Datos Generales y Movimientos desde el sistema FISERV. \nDebido a problemas con la carga de archivos a procesar.";

    String FTP_MENSAJE_EMAIL_CIERRE_MENSUAL_OPERADOR = "notificar.email.mensaje.cierre.mensual.operador.atc";
    String FTP_MENSAJE_EMAIL_CIERRE_MENSUAL_OPERADOR_DEFAULT = "Favor proceder con el Proceso de Cierre Mensual desde el sistema FISERV.";

    String FTP_MENSAJE_EMAIL_CIERRE_MENSUAL_VALIDAR_OPERADOR = "notificar.email.mensaje.cierre.mensual.validar.operador.atc";
    String FTP_MENSAJE_EMAIL_CIERRE_MENSUAL_VALIDAR_OPERADOR_DEFAULT = "Por favor, en comunicar a Mesa de Servicios (Ronald Castro) que el proceso de carga de Saldos Mensuales tiene problemas de validaci\u00F3n. ";

    String FTP_MENSAJE_EMAIL_VALIDAR_TABLA_FECHA_CORTE_ATC = "notificar.email.mensaje.error.validar.tabla.fecha.corte.atc";
    String FTP_MENSAJE_EMAIL_VALIDAR_TABLA_FECHA_CORTE_ATC_DEFAULT = "No se tiene configurado registro en TCPFEVEN para el periodo #variable.";

    String FTP_MENSAJE_EMAIL_VALIDAR_FECHA_CORTE_ATC = "notificar.email.mensaje.error.validar.fecha.corte.atc";
    String FTP_MENSAJE_EMAIL_VALIDAR_FECHA_CORTE_ATC_DEFAULT = "Fecha de cierre de archivo de saldos #variable no corresponde al periodo.";

    String FTP_MENSAJE_EMAIL_VALIDAR_FECHA_VENCIMIENTO_ATC = "notificar.email.mensaje.error.validar.fecha.vencimiento.atc";
    String FTP_MENSAJE_EMAIL_VALIDAR_FECHA_VENCIMIENTO_ATC_DEFAULT = "Fecha de vencimiento de pago de archivo de saldos #variable no corresponde al periodo.";

    String FTP_MENSAJE_EMAIL_VALIDAR_ALTAS_ATC = "notificar.email.mensaje.error.validar.alta.atc";
    String FTP_MENSAJE_EMAIL_VALIDAR_ALTAS_ATC_DEFAULT = "Cuentas ATC #variable de archivo de saldo no tienen cuenta de alta en el Banco.";

    String FTP_MENSAJE_EMAIL_CIERRE_MANSUAL_ERROR_OPERADOR = "notificar.email.mensaje.cierre.mensual.error.operador.atc";
    String FTP_MENSAJE_EMAIL_CIERRE_MANSUAL_ERROR_OPERADOR_DEFAULT = "Por favor no ejecutar el Proceso de Cierre Mensual desde el sistema FISERV. \nDebido a problemas con la carga de archivos a procesar.";

    String FTP_MENSAJE_EMAIL_FACTURACION_OPERADOR = "notificar.email.mensaje.facturacion.operador.atc";
    String FTP_MENSAJE_EMAIL_FACTURACION_OPERADOR_DEFAULT = "Favor proceder con el Proceso de Facturaci\u00F3n desde el sistema FISERV.";

    String FTP_MENSAJE_EMAIL_FACTURACION_ERROR_OPERADOR = "notificar.email.mensaje.facturacion.error.operador.atc";
    String FTP_MENSAJE_EMAIL_FACTURACION_ERROR_OPERADOR_DEFAULT = "Por favor no ejecutar el Proceso de Facturaci\u00F3n desde el sistema FISERV. \nDebido a problemas con la carga de archivos a procesar.";

    String FTP_VALIDAR_RECEPCION_ARCHIVO_ATC = "ftp.validar.recepcion.archivo.atc";
    String FTP_VALIDAR_RECEPCION_ARCHIVO_ATC_DEFAULT = "true";

    String FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC = "ftp.recepcion.archivo.codificacion.atc";
    String FTP_RECEPCION_ARCHIVO_CODIFICACION_ATC_DEFAULT = "ISO-8859-1";

    String JOBS_MONITOR_USERS = "jobs.monitor.users";
    String JOBS_MONITOR_USERS_DEFAULT = "'EBANKING','EBISASVC'";
    String JOBS_MONITOR_IP = "jobs.monitor.ip";
    String JOBS_MONITOR_IP_DEFAULT = "'172.20.100.17','172.20.100.35','172.20.100.36'";

    //CONEXION FTP LINKSER
    String FTP_USER_LINKSER = "ftp.usuario.linkser";
    String FTP_USER_LINKSER_DEFAULT = "root";

    String FTP_PASSWORD_LINKSER = "ftp.password.linkser";
    String FTP_PASSWORD_LINKSER_DEFAULT = "desa";

    String FTP_URL_LINKSER = "ftp.url.linkser";
    String FTP_URL_LINKSER_DEFAULT = "sftp://root@1.10.3.82/Linkser-TC?known_hosts=/root/.ssh/known_hosts";

    String FTP_LOCAL_TEMP_LINKSER = "ftp.local.temp.linkser";
    String FTP_LOCAL_TEMP_LINKSER_DEFAULT = "/TEMP/Linkser";

    String FTP_MD5_SUBIDA_LINKSER = "ftp.md5.subida.linkser";
    String FTP_MD5_SUBIDA_LINKSER_DEFAULT = "false";

    String FTP_MD5_BAJADA_LINKSER = "ftp.md5.bajada.linkser";
    String FTP_MD5_BAJADA_LINKSER_DEFAULT = "false";

    String FECHA_DE_PROCESO_LINKSER = "fecha.proceso.linkser";
    String FECHA_DE_PROCESO_LINKSER_DEFAULT = "0";

    String FTP_RUTA_SOLICITUD_ALTAS_LINKSER = "ftp.ruta.tc.linkser.envio.solicitud.altas";
    String FTP_RUTA_SOLICITUD_ALTAS_LINKSER_DEFAULT = "/Linkser-TC/solicitudes";

    String FTP_RUTA_TEMPORAL_ALTA_LINKSER = "ftp.ruta.tc.linkser.temporal.solicitud.altas";
    String FTP_RUTA_TEMPORAL_ALTA_LINKSER_DEFAULT = "NULL";

    String FTP_RUTA_RECEPCION_ALTAS_LINKSER = "ftp.ruta.tc.linkser.recepcion.solicitud.altas";
    String FTP_RUTA_RECEPCION_ALTAS_LINKSER_DEFAULT = "/Linkser-TC/tarjetas";

    String FTP_RUTA_RECEPCION_RENOVACIONES_LINKSER = "ftp.ruta.tc.linkser.recepcion.renovaciones";
    String FTP_RUTA_RECEPCION_RENOVACIONES_LINKSER_DEFAULT = "/Linkser-TC/Detalle Tarjetas";

    String FTP_RUTA_ENVIO_PAGOS_LINKSER = "ftp.ruta.tc.linkser.envio.pagos";
    String FTP_RUTA_ENVIO_PAGOS_LINKSER_DEFAULT = "/Linkser-TC/pagos";

    String FTP_RUTA_ENVIO_ANTICIPOS_LINKSER = "ftp.ruta.tc.linkser.envio.anticipos";
    String FTP_RUTA_ENVIO_ANTICIPOS_LINKSER_DEFAULT = "/Linkser-TC/AnticiposTC";

    // Rutas de intercambio de archivos
    String FTP_RUTA_ENVIO_ATC_LINKSER = "ftp.ruta.tc.linkser.recepcion.atc";
    String FTP_RUTA_ENVIO_ATC_LINKSER_DEFAULT = "/Linkser-TC/BIS";

    String FTP_RUTA_RECEPCION_ATC_LINKSER = "ftp.ruta.tc.linkser.envio.atc";
    String FTP_RUTA_RECEPCION_ATC_LINKSER_DEFAULT = "/ATC_Netdrive/recepciones";

    String FTP_RUTA_ENVIO_TEMPORAL_LINKSER = "ftp.ruta.tc.linkser.envio.termporal.pagos";
    String FTP_RUTA_ENVIO_TEMPORAL_LINKSER_DEFAULT = "NULL";

    String FTP_RUTA_RECEPCION_DG_LINKSER = "ftp.ruta.tc.linkser.recepcion.datos.generales";
    String FTP_RUTA_RECEPCION_DG_LINKSER_DEFAULT = "/Linkser-TC/movimientosTC";

    String FTP_RUTA_RECEPCION_MOVIMIENTOS_LINKSER = "ftp.ruta.tc.linkser.recepcion.movimiento.diario";
    String FTP_RUTA_RECEPCION_MOVIMIENTOS_LINKSER_DEFAULT = "/Linkser-TC/movimientosTC";

    String FTP_RUTA_RECEPCION_PAGO_ESTABLECIMIENTO_LINKSER = "ftp.ruta.tc.linkser.recepcion.pago.establecimiento";
    String FTP_RUTA_RECEPCION_PAGO_ESTABLECIMIENTO_LINKSER_DEFAULT = "/Linkser-TC/etab";

    String FTP_RUTA_RECEPCION_FACTURAS_LINKSER = "ftp.ruta.tc.linkser.recepcion.facturas";
    String FTP_RUTA_RECEPCION_FACTURAS_LINKSER_DEFAULT = "/Linkser-TC/ARCHIVOS CMS/Cierre_Gestion#anio/#mes#anio|yyyy|MMMM";

    String FTP_RUTA_RECEPCION_CIERRE_LINKSER = "ftp.ruta.tc.linkser.cierre";
    String FTP_RUTA_RECEPCION_CIERRE_LINKSER_DEFAULT = "/Linkser-TC/ARCHIVOS CMS/Cierre_Gestion#anio/#mes#anio|yyyy|MMMM";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_LINKSER_FORZADO = "ftp.nombre.archivo.linkser.recepcion.alta.forzado";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_LINKSER_FORZADO_DEFAULT = "NULL";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_LINKSER = "ftp.nombre.archivo.linkser.recepcion.alta";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_LINKSER_DEFAULT = "Archivo de Alta";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ERROR_ALTA_LINKSER = "ftp.nombre.archivo.linkser.recepcion.error.alta";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ERROR_ALTA_LINKSER_DEFAULT = "ISS00012ERROOI7#numero.OUT";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_TARJETA_LINKSER = "ftp.nombre.archivo.linkser.recepcion.alta.tarjeta";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_TARJETA_LINKSER_DEFAULT = "A7#fechanT.Cms|MMdd";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_RENOVACION_TARJETA_LINKSER = "ftp.nombre.archivo.linkser.recepcion.renovacion.tarjeta";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_RENOVACION_TARJETA_LINKSER_DEFAULT = "TARI.#fecha|yyMMdd";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_BAJA_TARJETA_LINKSER = "ftp.nombre.archivo.linkser.baja.renovacion.tarjeta";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_BAJA_TARJETA_LINKSER_DEFAULT = "SDOI.#fecha|yyMMdd";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_RENOVACION_LINKSER = "ftp.nombre.archivo.linkser.recepcion.renovacion";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_RENOVACION_LINKSER_DEFAULT = "TARI.\\d{6}.dat";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_BAJAS_LINKSER = "ftp.nombre.archivo.linkser.recepcion.bajas";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_BAJAS_LINKSER_DEFAULT = "SDOI.\\d{6}.dat";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_TARJETA_LINKSER_FORZADO = "ftp.nombre.archivo.linkser.recepcion.alta.tarjeta.forzado";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ALTA_TARJETA_LINKSER_FORZADO_DEFAULT = "NULL";

    String FTP_NOMBRE_ARCHIVO_PAGOS_LINKSER = "ftp.nombre.archivo.linkser.pagos";
    String FTP_NOMBRE_ARCHIVO_PAGOS_LINKSER_DEFAULT = "pbisa#fecha.dat|ddMMyy"; //{YYYYDDMM}.TXT

    String FTP_NOMBRE_ARCHIVO_ANTICIPO_LINKSER = "ftp.nombre.archivo.linkser.anticipo";
    String FTP_NOMBRE_ARCHIVO_ANTICIPO_LINKSER_DEFAULT = "BIS#fecha.dat|ddMM";

    String FTP_NOMBRE_ARCHIVO_PAGO_ESTABLECIMIENTO_BS_LINKSER = "ftp.nombre.archivo.linkser.pago.establecimiento.bs";
    String FTP_NOMBRE_ARCHIVO_PAGO_ESTABLECIMIENTO_BS_LINKSER_DEFAULT = "BISA#fecha.BOL|ddMM";

    String FTP_NOMBRE_ARCHIVO_PAGO_ESTABLECIMIENTO_DS_LINKSER = "ftp.nombre.archivo.linkser.pago.establecimiento.ds";
    String FTP_NOMBRE_ARCHIVO_PAGO_ESTABLECIMIENTO_DS_LINKSER_DEFAULT = "BISA#fecha.DOL|ddMM";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_LINKSER_CIERRE_OPERA = "ftp.nombre.archivo.linkser.recepcion.operacion.mensual";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_LINKSER_CIERRE_OPERA_DEFAULT = "OPERA_BSA#fecha|MMyyyy";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_LINKSER_CIERRE_CONSUMOS_ORIGINAL = "ftp.nombre.archivo.linkser.recepcion.consumos.mensual.original";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_LINKSER_CIERRE_CONSUMOS_ORIGINAL_DEFAULT = "CONSUMOS .DAT";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_LINKSER_CIERRE_CONSUMOS = "ftp.nombre.archivo.linkser.recepcion.consumos.mensual";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_LINKSER_CIERRE_CONSUMOS_DEFAULT = "CONSUMOS#fecha.DAT|MMyyyy";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_LINKSER = "ftp.nombre.archivo.linkser.transferencia.desde.atc";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_ATC_LINKSER_DEFAULT = "BIS#fecha.dat|ddMM";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_LINKSER_FORZADO = "ftp.nombre.archivo.linkser.recepcion.facturas.forzado";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_LINKSER_FORZADO_DEFAULT = "NULL";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_CUENTA_LINKSER = "ftp.nombre.archivo.linkser.recepcion.factura.cuenta";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_CUENTA_LINKSER_DEFAULT = "FACACCT-CONTABLE.#fecha|MMyyyy";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_CUENTA_LINKSER_FORZADO = "ftp.nombre.archivo.linkser.recepcion.factura.cuenta.forzado";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_CUENTA_LINKSER_FORZADO_DEFAULT = "NULL";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_DG_LINKSER = "ftp.nombre.archivo.linkser.recepcion.datos.generales";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_DG_LINKSER_DEFAULT = "RS#fecha|ddMMyy";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_MOVIMIENTOS_LINKSER = "ftp.nombre.archivo.linkser.recepcion.MOVIMIENTOS";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_MOVIMIENTOS_LINKSER_DEFAULT = "TX#fecha|ddMMyy";

    String FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_LINKSER = "ftp.nombre.archivo.linkser.recepcion.facturas";
    String FTP_RECEPCION_NOMBRE_ARCHIVO_FACTURA_LINKSER_DEFAULT = "FACACCT.#fecha|MMyyyy";

    String LIBRERIA_TC_LINKSER = "rpg.libreria.tc.linkser";
    String LIBRERIA_TC_LINKSER_DEFAULT = "zicbsbisa";

    String PROGRAMA_TC_SOLICITUD_LINKSER = "rpg.programa.tc.linkser.solicitud.alta";
    String PROGRAMA_TC_SOLICITUD_LINKSER_DEFAULT = "TCCL7005";

    String FTP_VALIDAR_RECEPCION_ARCHIVO_LINKSER = "ftp.validar.recepcion.archivo.linkser";
    String FTP_VALIDAR_RECEPCION_ARCHIVO_LINKSER_DEFAULT = "true";

    String FTP_RECEPCION_ARCHIVO_CODIFICACION_LINKSER = "ftp.recepcion.archivo.codificacion.linkser";
    String FTP_RECEPCION_ARCHIVO_CODIFICACION_LINKSER_DEFAULT = "ISO-8859-1";

    String FTP_ENVIO_ARCHIVO_CODIFICACION_LINKSER = "ftp.envio.archivo.codificacion.linkser";
    String FTP_ENVIO_ARCHIVO_CODIFICACION_LINKSER_DEFAULT = "UTF-8";

    String FTP_MENSAJE_EMAIL_PROCESO_DIARIO_OPERADOR_LINKSER = "notificar.email.mensaje.alta.operador.linkser";
    String FTP_MENSAJE_EMAIL_PROCESO_DIARIO_OPERADOR_LINKSER_DEFAULT = "Favor proceder con la Recepci\u00F3n de N\u00FAmeros de Tarjeta y Cuentas LINKSER desde el sistema FISERV.";

    String FTP_MENSAJE_EMAIL_PROCESO_DIARIO_ERROR_OPERADOR_LINKSER = "notificar.email.mensaje.alta.error.operador.linkser";
    String FTP_MENSAJE_EMAIL_PROCESO_DIARIO_ERROR_OPERADOR_LINKSER_DEFAULT = "Por favor no proceder con la Recepci\u00F3n de N\u00FAmeros de Tarjeta y Cuentas LINKSER desde el sistema FISERV. \nDebido a que ha problemas en carga de solicitud pendiente a procesar.";

    String FTP_MENSAJE_EMAIL_PROCESO_RENOVACION_OPERADOR_LINKSER = "notificar.email.mensaje.renovacion.operador.linkser";
    String FTP_MENSAJE_EMAIL_PROCESO_RENOVACION_OPERADOR_LINKSER_DEFAULT = "Favor proceder con la Recepci\u00F3n de N\u00FAmeros de Tarjeta y Cuentas LINKSER desde el sistema FISERV. Debido a que se cargo archivo de renovaciones.";

    String FTP_MENSAJE_EMAIL_PROCESO_RENOVACION_SIN_DATOS_OPERADOR_LINKSER = "notificar.email.mensaje.renovacion.sin.datos.operador.linkser";
    String FTP_MENSAJE_EMAIL_PROCESO_RENOVACION_SIN_DATOS_OPERADOR_LINKSER_DEFAULT = "Por favor no proceder con la Recepci\u00F3n de N\u00FAmeros de Tarjeta y Cuentas LINKSER desde el sistema FISERV. \nDebido a que el archivo de renovaciones esta vacio.";

    String FTP_MENSAJE_EMAIL_PROCESO_RENOVACION_ERROR_OPERADOR_LINKSER = "notificar.email.mensaje.renovacion.error.operador.linkser";
    String FTP_MENSAJE_EMAIL_PROCESO_RENOVACION_ERROR_OPERADOR_LINKSER_DEFAULT = "Por favor no proceder con la Recepci\u00F3n de N\u00FAmeros de Tarjeta y Cuentas LINKSER desde el sistema FISERV. \nDebido a que no hay archivos de renovaciones para procesar.";

    String FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_OPERADOR_LINKSER = "notificar.email.mensaje.movimiento.operador.linkser";
    String FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_OPERADOR_LINKSER_DEFAULT = "Favor proceder con la Recepci\u00F3n de Datos Generales y Movimientos desde el sistema FISERV.";

    String FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_ERROR_OPERADOR_LINKSER = "notificar.email.mensaje.movimiento.error.operador.linkser";
    String FTP_MENSAJE_EMAIL_MOVIMIENTOS_DIARIO_ERROR_OPERADOR_LINKSER_DEFAULT = "Por favor no proceder con la Recepci\u00F3n de Datos Generales y Movimientos desde el sistema FISERV. \nDebido a problemas con la carga de archivos a procesar.";

    String FTP_MENSAJE_EMAIL_CIERRE_MENSUAL_OPERADOR_LINKSER = "notificar.email.mensaje.cierre.mensual.operador.linkser";
    String FTP_MENSAJE_EMAIL_CIERRE_MENSUAL_OPERADOR_LINKSER_DEFAULT = "Favor proceder con el Proceso de Cierre Mensual de LINKSER desde el sistema FISERV.";

    String FTP_MENSAJE_EMAIL_CIERRE_MANSUAL_ERROR_OPERADOR_LINKSER = "notificar.email.mensaje.cierre.mensual.error.operador.linkser";
    String FTP_MENSAJE_EMAIL_CIERRE_MANSUAL_ERROR_OPERADOR_LINKSER_DEFAULT = "Por favor no ejecutar el Proceso de Cierre Mensual de LINKSER desde el sistema FISERV. \nDebido a problemas con la carga de archivos a procesar.";

    String FTP_MENSAJE_EMAIL_FACTURACION_OPERADOR_LINKSER = "notificar.email.mensaje.facturacion.operador.linkser";
    String FTP_MENSAJE_EMAIL_FACTURACION_OPERADOR_LINKSER_DEFAULT = "Favor proceder con el Proceso de Facturaci\u00F3n de LINKSER desde el sistema FISERV.";

    String FTP_MENSAJE_EMAIL_FACTURACION_ERROR_OPERADOR_LINKSER = "notificar.email.mensaje.facturacion.error.operador.linkser";
    String FTP_MENSAJE_EMAIL_FACTURACION_ERROR_OPERADOR_LINKSER_DEFAULT = "Por favor no ejecutar el Proceso de Facturaci\u00F3n de LINKSER desde el sistema FISERV. \nDebido a problemas con la carga de archivos a procesar.";

    String FTP_MENSAJE_EMAIL_PAGO_ESTABLECIMIENTO_LINKSER = "notificar.email.mensaje.pago.establecimiento.linkser";
    String FTP_MENSAJE_EMAIL_PAGO_ESTABLECIMIENTO_LINKSER_DEFAULT = "Favor proceder con el Proceso de Pago a establecimientos desde el sistema FISERV.";

    String FTP_MENSAJE_EMAIL_PAGO_ESTABLECIMIENTO_LINKSER_ERROR_LINKSER = "notificar.email.mensaje.pago.establecimiento.error.linkser";
    String FTP_MENSAJE_EMAIL_PAGO_ESTABLECIMIENTO_LINKSER_ERROR_LINKSER_DEFAULT = "Por favor no ejecutar opción de pago a establecimientos desde el sistema FISERV. \nDebido a problemas con la carga de archivos a procesar.";

    String FTP_MENSAJE_EMAIL_TRANSFERENCIA_ATC_LINKSER = "notificar.email.mensaje.transferencia.atc.linkser";
    String FTP_MENSAJE_EMAIL_TRANSFERENCIA_ATC_LINKSER_DEFAULT = "Se realizo correctamente la transferencia de archivos de ATC a LINKSER.";

    String FTP_MENSAJE_EMAIL_TRANSFERENCIA_ATC_LINKSER_ERROR = "notificar.email.mensaje.transferencia.atc.linkser.error";
    String FTP_MENSAJE_EMAIL_TRANSFERENCIA_ATC_LINKSER_ERROR_DEFAULT = "No se completo la transferencia de archivos de ATC a LINKSER. \nDebido a problemas con la carga de archivos a procesar.";


    //============== Variables CHAT BOT============== //

    String CB_CANAL = "chatbot.canal";
    String CB_CANAL_DEFAULT = "chatbot";

    String CONEXION_SQLITE = "conexion.sqlite.local";
    String CONEXION_SQLITE_DEFAULT = "d://cajeros.sqlite";

    String IR_CONEXION_SQLITE = "habilitar.conexion.sqlite";
    String IR_CONEXION_SQLITE_DEFAULT = "false";

    String USUARIOS_PILOTO_EBISA = "chatbot.usuarios.ebisa.piloto";
    String USUARIOS_PILOTO_EBISA_DEFAULT = "*";

    String CONEXION_DB2_TICKETING = "conexion.db2.ticketing.principal";
    String CONEXION_DB2_TICKETING_DEFAULT = "//xdb201:50000/DBSQ1";

    String USER_CONEXION_DB2_TICKETING = "conexion.user.db2.ticketing.principal";
    String USER_CONEXION_DB2_TICKETING_DEFAULT = "db2svc";

    String PASS_CONEXION_DB2_TICKETING = "conexion.pass.db2.ticketing.principal";
    String PASS_CONEXION_DB2_TICKETING_DEFAULT = "db2final";

    String CONEXION_DB2_TICKETING_LOCAL = "conexion.db2.ticketing.local";
    String CONEXION_DB2_TICKETING_LOCAL_DEFAULT = "//LOCAL:50000/DBSQ1";

    String USER_CONEXION_DB2_TICKETING_LOCAL = "conexion.user.db2.ticketing.local";
    String USER_CONEXION_DB2_TICKETING_LOCAL_DEFAULT = "db2svc";

    String PASS_CONEXION_DB2_TICKETING_LOCAL = "conexion.pass.db2.ticketing.local";
    String PASS_CONEXION_DB2_TICKETING_LOCAL_DEFAULT = "db2final";

    String ESTADOS_TICKETING_LOCAL = "estado.db2.ticketing.local";
    String ESTADOS_TICKETING_LOCAL_DEFAULT = "1,2,3,20";

    String CB_CANTIDAD_MOVIMIENTOS = "chatbot.cantidad.movimientos";
    int CB_CANTIDAD_MOVIMIENTOS_DEFAULT = 5;

    String CB_CANTIDAD_MOVIMIENTOS_EXTRACTO = "chatbot.cantidad.movimientos.extracto";
    int CB_CANTIDAD_MOVIMIENTOS_EXTRACTO_DEFAULT = 30;

    String CB_MENSAJE_REQUERIDO = "chatbot.mensaje.requerido";
    String CB_MENSAJE_REQUERIDO_DEFAULT = "El campo $NOMBRECAMPO es requerido.";

    String CB_MENSAJE_FORMATO = "chatbot.mensaje.formato";
    String CB_MENSAJE_FORMATO_DEFAULT = "El formato no es valido para el campo $NOMBRECAMPO.";

    String CB_MENSAJE_ERROR_PROCESO = "chatbot.mensaje.error.proceso";
    String CB_MENSAJE_ERROR_PROCESO_DEFAULT = "Problemas al procesar la solicitud, favor vuelva a intentarlo mas tarde.";

    String CB_MENSAJE_ERROR_RECARGA = "chatbot.mensaje.error.recarga";
    String CB_MENSAJE_ERROR_RECARGA_DEFAULT = "Hubo un error al recargar el telefono.";

    String CB_MENSAJE_ERROR_MONTO_RECARGA = "chatbot.mensaje.error.recarga.monto";
    String CB_MENSAJE_ERROR_MONTO_RECARGA_DEFAULT = "El monto para la recarga no es permitido.";

    String CB_MENSAJE_CLIENTE_INEXISTENTE2 = "chatbot.mensaje.cliente.inexistente2";
    String CB_MENSAJE_CLIENTE_INEXISTENTE2_DEFAULT = "Cliente no registrado.";

    String CB_MENSAJE_CLIENTE_AFILIACION = "chatbot.mensaje.cliente.afiliacion";
    String CB_MENSAJE_CLIENTE_AFILIACION_DEFAULT = "Para hacer uso de la aplicación, por favor ingresa a la Banca por Internet e-Bisa , en la opción Servicios Bancarios > Aplicación BISA Chat y afíliate !.";

    String CB_MENSAJE_CLIENTE_INACTIVO = "chatbot.mensaje.cliente.inactivo";
    String CB_MENSAJE_CLIENTE_INACTIVO_DEFAULT = "Cliente inactivo";

    String CB_MENSAJE_CLIENTE_BLOQUEADO = "chatbot.mensaje.cliente.bloqueado";
    String CB_MENSAJE_CLIENTE_BLOQUEADO_DEFAULT = "Cliente bloqueado";

    String CB_MENSAJE_LIMITE_EXCEDIDO = "chatbot.mensaje.limite.excedido";
    String CB_MENSAJE_LIMITE_EXCEDIDO_DEFAULT = "Su limite del servicio ha sido sobrepasado. Vuelva a intentar mas tarde.";

    String CB_MENSAJE_CLIENTE_DESCONECTADO = "chatbot.mensaje.cliente.desconectado";
    String CB_MENSAJE_CLIENTE_DESCONECTADO_DEFAULT = "Su tiempo de sesión expiró, favor vuelva a registrarse al área privada.";

    String CB_MENSAJE_CLIENTE_SIN_CORREO = "chatbot.mensaje.cliente.sin.correo";
    String CB_MENSAJE_CLIENTE_SIN_CORREO_DEFAULT = "Cliente no tiene correo valido";

    String CB_MENSAJE_SIN_CONTRATO = "chatbot.mensaje.sin.contrato";
    String CB_MENSAJE_SIN_CONTRATO_DEFAULT = "Apersónate a cualquier agencia del Banco a nivel nacional para firmar la Adenda y acceder al servicio 'CHATBOT'. Posteriormente, podrás realizar la afiliación a través de este canal.";

    String CB_MENSAJE_PILOTO = "chatbot.mensaje.sin.piloto";
    String CB_MENSAJE_PILOTO_DEFAULT = "No estas registrado para realizar acciones en este sistema.";

    String CB_MENSAJE_HAB_TD_EXTERIOR = "chatbot.mensaje.error.hab.td.exterior";
    String CB_MENSAJE_HAB_TD_EXTERIOR_DEFAULT = "Hubo un error al tratar de habilitar la Tarjeta de debito para el exterior.";

    String CB_MENSAJE_VENCIMIENTOS = "chatbot.mensaje.error.vencimientos";
    String CB_MENSAJE_VENCIMIENTOS_DEFAULT = "No se tienen vencimientos.";

    String CB_MENSAJE_BENEFICIARIOS = "chatbot.mensaje.error.beneficiarios";
    String CB_MENSAJE_BENEFICIARIOS_DEFAULT = "No se tienen beneficiarios con esa coincidencia.";

    String CB_MENSAJE_SALDO_INSUFICIENTE = "chatbot.mensaje.saldo.insuficiente";
    String CB_MENSAJE_SALDO_INSUFICIENTE_DEFAULT = "Saldo insuficiente en la cuenta.";

    String CB_MENSAJE_ADVERTENCIA_CUENTA_INEXISTENTE = "chatbot.mensaje.cuenta.inexistente";
    String CB_MENSAJE_ADVERTENCIA_CUENTA_INEXISTENTE_DEFAULT = "La Cuenta no es v\u00e1lida. Por favor verifica la misma.";

    String CB_MENSAJE_VALIDACION_CODIGO_TOKEN_SMS = "mojix.mensaje.validacion.codigo.token.sms.102";
    String CB_MENSAJE_VALIDACION_CODIGO_TOKEN_SMS_DEFAULT = "El c\u00f3digo introducido no coincide con el c\u00f3digo enviado a tu celular, verifica el c\u00f3digo registrado o solicita un nuevo c\u00f3digo.";

    String CB_MENSAJE_ERROR_GIRO_MOVIL = "chatbot.mensaje.error.giro.movil";
    String CB_MENSAJE_ERROR_GIRO_MOVIL_DEFAULT = "Hubo un error al emitir el giro movil.";

    String CB_MENSAJE_ERROR_SESION_INACTIVA = "chatbot.mensaje.error.tiempo.inactivo";
    String CB_MENSAJE_ERROR_SESION_INACTIVA_DEFAULT = "Termino su tiempo de sesion.";

    String CB_MENSAJE_PPC01 = "chatbot.mensaje.error.pcc01";
    String CB_MENSAJE_PPC01_DEFAULT = "Esta operación es igual o supera los $us10,000, por lo " +
            "tanto se requiere llenar el formulario PCC-01 de " +
            "acuerdo a la normativa vigente. " +
            "Por el momento este formulario no esta disponible " +
            "para esta transacción, por lo que debes apersonarte " +
            "por las oficinas del Banco para realizar esta " +
            "transacción. ";

    String CB_MENSAJE_DEBE_PPC01 = "chatbot.mensaje.warning.pcc01";
    String CB_MENSAJE_DEBE_PPC01_DEFAULT = "Debes solicitar origen/destino de fondos";

    String CB_MENSAJE_ERROR_BLOQUEO = "chatbot.mensaje.error.bloqueo.td";
    String CB_MENSAJE_ERROR_BLOQUEO_DEFAULT = "Hubo un error al procesar el bloqueo.";

    String CB_MENSAJE_ERROR_EXTRACTO = "chatbot.mensaje.error.extracto";
    String CB_MENSAJE_ERROR_EXTRACTO_DEFAULT = "Hubo un error al generar el extracto.";

    String CB_MENSAJE_ERROR_SIN_EXTRACTO = "chatbot.mensaje.error.sin.extracto";
    String CB_MENSAJE_ERROR_SIN_EXTRACTO_DEFAULT = "No tiene extracto, verifique";

    String CB_MENSAJE_NO_EFECTUAR_DEBITO = "chatbot.mensaje.no.efectuar.debito";
    String CB_MENSAJE_NO_EFECTUAR_DEBITO_DEFAULT = "No se puede efectuar el debito en cuenta, favor comunicarse con el Banco.";

    String CB_MENSAJE_EXITOSO = "chatbot.mensaje.registro.exitoso.0";
    String CB_MENSAJE_EXITOSO_DEFAULT = "La Transacci\u00f3n fue procesada exitosamente.";

    String CB_NOTIFICACION_TOKEN_SMS = "chatbot.notificacion.token.sms";
    String CB_NOTIFICACION_TOKEN_SMS_DEFAULT = "Banco Bisa: Tu Token para el chatbot es: $TOKENSMS y es valido para una sola transaccion hasta $TOKENTIEMPOVIDA.";

    String CB_NOTIFICACION_TOKEN_EMAIL = "chatbot.notificacion.token.email";
    String CB_NOTIFICACION_TOKEN_EMAIL_DEFAULT = "Estimado Cliente: \n\n" +
            "Tu Token para el chatbot es: $TOKENSMS y es v\u00e1lido para una sola transacci\u00f3n hasta $TOKENTIEMPOVIDA.\n\n" +
            "Atentamente,\n" +
            "Servicio BISA Chat\n" +
            "\n" +
            "NOTA DE CONFIDENCIALIDAD: " +
            "La informaci\u00f3n contenida en este correo electr\u00f3nico y sus anexos solo puede ser utilizada por el individuo o la compa\u00f1\u00eda a la cual esta dirigido. Sin expresa autorizaci\u00f3n del remitente, su difusi\u00f3n, distribuci\u00f3n o copia esta prohibida y sancionada por Ley. Si por error recibe este mensaje, por favor reenv\u00edelo al banco y luego b\u00f3rrelo.";

    String CB_PROMOCIONES = "chatbot.promociones.nombre.0";
    String CB_PROMOCIONES_DEFAULT = "POS SIN TARJETA";

    String CB_PROMOCIONES_URL = "chatbot.promociones.url.0";
    String CB_PROMOCIONES_URL_DEFAULT = "http://www.bisa.com/images/banner-pos-sin-tarjeta-006.jpg";

    String CB_CANTIDAD_DE_DIGITOS = "chatbot.cantidad.de.digitos";
    int CB_CANTIDAD_DE_DIGITOS_DEFAULT = 4;

    String CB_HORAS_ENTRE_PEDIDOS_QUE_ANULAN = "chatbot.horas.pedido.clave.anula.otras";
    String CB_HORAS_ENTRE_PEDIDOS_QUE_ANULAN_DEFAULT = "3";

    String CB_MENSAJE_TIEMPO_DE_VIDA_TOKEN_MINUTOS = "chatbot.tiempo.vida.minutos";
    int CB_MENSAJE_TIEMPO_DE_VIDA_TOKEN_MINUTOS_DEFAULT = 15;

    String CB_ALGORITMO_ENCRIPTACION = "chatbot.algoritmo.encriptacion";
    String CB_ALGORITMO_ENCRIPTACION_DEFAULT = "DESede/ECB/NoPadding";

    String CB_LLAVE_ENCRIPTACION = "chatbot.llave.encriptacion";
    String CB_LLAVE_ENCRIPTACION_DEFAULT = "3257E4C94B83E828EA682C25";

    String CB_NOMBRE_ARCHIVO_CORREO_EXTRACTO = "chatbot.mail.mensaje.extracto.nombre.archivo";
    String CB_NOMBRE_ARCHIVO_CORREO_EXTRACTO_DEFAULT = "extracto";

    String CB_EXTENSION_ARCHIVO_CORREO_EXTRACTO = "chatbot.mail.mensaje.extracto.extension.archivo";
    String CB_EXTENSION_ARCHIVO_CORREO_EXTRACTO_DEFAULT = ".pdf";

    String CB_ASUNTO_CORREO_EXTRACTO = "chatbot.mail.mensaje.extracto.asunto";
    String CB_ASUNTO_CORREO_EXTRACTO_DEFAULT = "BISA Chat: Estado de cuenta";

    String CB_CONTENIDO_CORREO_EXTRACTO = "chatbot.mail.mensaje.extracto.contenido";
    String CB_CONTENIDO_CORREO_EXTRACTO_DEFAULT = "Estimado cliente:\n" +
            "\n" +
            "Adjunto encontrar\u00e1 el archivo que contiene la informaci\u00f3n del estado de cuenta que solicit\u00f3 por BISA Chat.\n" +
            "\n" +
            "Atentamente,\n" +
            "Servicio BISA Chat\n" +
            "\n" +
            "NOTA DE CONFIDENCIALIDAD: " +
            "La informaci\u00f3n contenida en este correo electr\u00f3nico y sus anexos solo puede ser utilizada por el individuo o la compa\u00f1\u00eda a la cual esta dirigido. Sin expresa autorizaci\u00f3n del remitente, su difusi\u00f3n, distribuci\u00f3n o copia esta prohibida y sancionada por Ley. Si por error recibe este mensaje, por favor reenv\u00edelo al banco y luego b\u00f3rrelo.";

    String LLAVE_TOKEN_WEB = "llave.token.web";
    String LLAVE_TOKEN_WEB_DEFAULT = "/7JZSfM6IOudYMYinvQqqkLjSrg3gux/ETiS0t380XM=";

    String TIEMPO_VIGENCIA_TOKEN_WEB = "tiempo.token.web.minutos";
    int TIEMPO_VIGENCIA_TOKEN_WEB_DEFAULT = 5;

    String RECARGA_RANGO = "recarga.monto.rango.";

    String RECARGA_RANGO_ENTEL = "recarga.monto.rango.entel";
    String RECARGA_RANGO_ENTEL_DEFAULT = "5-100";

    String RECARGA_RANGO_VIVA = "recarga.monto.rango.viva";
    String RECARGA_RANGO_VIVA_DEFAULT = "5-100";

    String RECARGA_RANGO_TIGO = "recarga.monto.rango.tigo";
    String RECARGA_RANGO_TIGO_DEFAULT = "10,20,25,30,50,100";

    String CB_ASUNTO_CORREO_TOKEN = "chatbot.mail.mensaje.token.asunto";
    String CB_ASUNTO_CORREO_TOKEN_DEFAULT = "BISA Chat: Token";

    String CB_EMAIL_CODIGOBISA = "chatbot.mail.codigo.bisa";
    String CB_EMAIL_CODIGOBISA_DEFAULT = "false";

    String CB_TIEMPO_VIGENCIA_SESION = "chatbot.tiempo.vigencia.session";
    int CB_TIEMPO_VIGENCIA_SESION_DEFAULT = 15;

    // TARJETA DE CREDITO
    String PROGRAMA_TC_OBTENER_NUMERO_TARJETA_CREDITO = "rpg.programa.tc.obtener.numero.tarjeta.credito";
    String PROGRAMA_TC_OBTENER_NUMERO_TARJETA_CREDITO_DEFAULT = "TCVA007A";

    String PROGRAMA_TC_DESENCRIPTAR_NUMERO_TARJETA_CREDITO = "rpg.programa.tc.desencriptar.numero.tarjeta.credito";
    String PROGRAMA_TC_DESENCRIPTAR_NUMERO_TARJETA_CREDITO_DEFAULT = "TCVA007Z";

    String PROGRAMA_TC_DESENCRIPTAR_TARJETA_CREDITO_CCSID = "rpg.programa.tc.desencriptar.numero.tarjeta.credito.ccsid";
    int PROGRAMA_TC_DESENCRIPTAR_TARJETA_CREDITO_CCSID_DEFAULT = 37;

    /** Nuevo Cliente ACH **/
    String ACH_IMPORTE_ENVIO_SMS = "ach.importe.envio.sms";
    BigDecimal ACH_IMPORTE_ENVIO_SMS_DEFAULT = new BigDecimal("10");

    String ACH_CAMARA_COMPENSACION = "ach.camara.compensacion";
    String ACH_CAMARA_COMPENSACION_DEFAULT = "ACCL";

    String ACH_CICLO_UMBRAL = "ach.ciclo.umbral";
    int ACH_CICLO_UMBRAL_DEFAULT = 15;

    String ACH_USUARIOS_A_NOTIFICAR = "ach.usuarios.a.notificar";
    String ACH_USUARIOS_A_NOTIFICAR_DEFAULT = "gmercado@grupobisa.com,jjusto@grupobisa.com,rmunozs@grupobisa.com,respinoza@grupobisa.com";

    String ACH_DESCRIPCION_CODIGOS = "ach.descripcion.codigos";
    String ACH_DESCRIPCION_CODIGOS_DEFAULT =
             "RA01|Cuenta no existe pago banco destino" +
            "|RA02|Error tipo de cuenta pago en banco destino" +
            "|RA03|Error tipo de moneda pago en banco destino" +
            "|RA04|Error en el titular de cuenta destino" +
            "|RA05|Estado de la cuenta invalido en banco destino" +
            "|RA06|Pago excede el monto adeudado" +
            "|RA07|Amortización extraordinaria no permitida o excede limites" +
            "|RA08|Problema interno en banco destino" +
            "|RA09|El pago de prestamos y tarjetas no habilitado" +
            "|RC01|Cuenta no existe debito" +
            "|RC02|Cuenta sin fondos debito" +
            "|RC03|Error tipo cuenta debito" +
            "|RC04|Error tipo de moneda debito" +
            "|RC05|No existe preacuerdo o datos incorrectos" +
            "|RC06|Estado de la cuenta invalido" +
            "|RC07|Monto excede al maximo al permitido" +
            "|RC08|Periodo de afiliacion vencido" +
            "|RC09|Preacuerdo se dio de bajan" +
            "|0000|Orden Aceptada" +
            "|D10|Orden no pudo enviarse al destinatario"+
            "|D11|Respuesta del destinatario no recibida";
}