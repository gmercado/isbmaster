package bus.env.entities;

/**
 * Created by ccalle on 28/04/2017.
 */
public enum TipoHistorico {
    I("INSERT"),
    U("UPDATE"),
    D("DELETE");

    private final String descripcion;

    private TipoHistorico(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }


}
