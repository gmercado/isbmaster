/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.env.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "ISBP99H")
public class VariableHistorico implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "I99NID")
    private Long id;

    @Column(name = "I99NIDVM")
    private long idVariable;

    @Column(name = "I99VNOM")
    private String nombre;

    @Column(name = "I99VVAL")
    private String valor;


    @Column(name = "I99NVER")
    private long version;

    @Column(name = "I99DFECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaOperacion;

    @Column(name = "I99VUSR")
    private String usrOperacion;

    @Column(name = "I99VIP")
    private String ip;

    @Column(name = "I99CTPOOP")
    @Enumerated(EnumType.STRING)
    private TipoHistorico tipoHistorico;

    public VariableHistorico() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getIdVariable() {
        return idVariable;
    }

    public void setIdVariable(long idVariable) {
        this.idVariable = idVariable;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public Date getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(Date fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public String getUsrOperacion() {
        return usrOperacion;
    }

    public void setUsrOperacion(String usrOperacion) {
        this.usrOperacion = usrOperacion;
    }

    public TipoHistorico getTipoHistorico() {
        return tipoHistorico;
    }

    public void setTipoHistorico(TipoHistorico tipoHistorico) {
        this.tipoHistorico = tipoHistorico;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
