/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.env.dao;

import bus.config.dao.CryptUtils;
import bus.database.dao.DaoImpl;
import bus.database.dao.DaoImplCached;
import bus.database.model.BasePrincipal;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.env.entities.TipoHistorico;
import bus.env.entities.Variable;
import bus.env.entities.VariableHistorico;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.apache.commons.configuration.Configuration;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.timer.Timer;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Marcelo Morales
 * @since 5/22/11
 */
public class VariableHistoricoDao extends DaoImpl<VariableHistorico, Long> {
    private static final Logger LOGGER = LoggerFactory.getLogger(VariableHistoricoDao.class);

    @Inject
    public VariableHistoricoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }


    public void guardar(Variable variable, TipoHistorico tipoHistorico, String usuario,String ip){
        try{
            VariableHistorico variableHistorico= new VariableHistorico();
            variableHistorico.setIdVariable(variable.getId());
            variableHistorico.setNombre(variable.getNombre());
            variableHistorico.setValor(variable.getValor());
            variableHistorico.setVersion(variable.getVersion());
            variableHistorico.setUsrOperacion(usuario);
            variableHistorico.setTipoHistorico(tipoHistorico);
            variableHistorico.setFechaOperacion(new Date());
            variableHistorico.setIp(ip);
            super.persist(variableHistorico);
        } catch (Throwable e) {
            LOGGER.error("Ha ocurrido un error al guardar historico(" + tipoHistorico.getDescripcion() + ") del parametro " + variable.getId(),e);
        }
    }

}
