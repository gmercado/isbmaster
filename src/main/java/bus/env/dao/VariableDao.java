/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.env.dao;

import bus.config.dao.CryptUtils;
import bus.database.dao.DaoImplCached;
import bus.database.model.BasePrincipal;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.env.entities.TipoHistorico;
import bus.env.entities.Variable;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.apache.commons.configuration.Configuration;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.timer.Timer;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Marcelo Morales
 * @since 5/22/11
 */
public class VariableDao extends DaoImplCached<Variable, Long> implements MedioAmbiente, Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(VariableDao.class);

    private final CryptUtils cryptUtils;

    private final Configuration configuration;
    private VariableHistoricoDao variableHistoricoDao;

    @Inject
    public VariableDao(@BasePrincipal EntityManagerFactory entityManagerFactory,
                       CryptUtils cryptUtils,
                       Configuration configuration,
                       Ehcache cache, VariableHistoricoDao variableHistoricoDao) {
        super(entityManagerFactory, cache);
        this.cryptUtils = cryptUtils;
        this.configuration = configuration;
        this.variableHistoricoDao = variableHistoricoDao;

    }

    @Override
    public Variable merge(Variable data) {
        Variable merged = super.merge(data);
        ehcache.remove(merged.getNombre());
        return merged;
    }

    public Variable merge(Variable data, String usuario, String ip) {
        Variable merged = this.merge(data);
        variableHistoricoDao.guardar(merged, TipoHistorico.U, usuario, ip);
        return merged;
    }

    @Override
    public Variable remove(Long id) {
        Variable removed = super.remove(id);
        ehcache.remove(removed.getNombre());
        return removed;
    }

    public Variable remove(Long id, String usuario, String ip) {
        Variable removed = this.remove(id);
        variableHistoricoDao.guardar(removed, TipoHistorico.D, usuario, ip);
        return removed;
    }

    @Override
    public Variable persist(Variable data) {
        return super.persist(data);
    }

    public Variable persist(Variable data, String usuario, String ip) {
        Variable var = this.persist(data);
        variableHistoricoDao.guardar(var, TipoHistorico.I, usuario, ip);
        return var;
    }

    @Override
    protected Path<Long> countPath(Root<Variable> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<Variable> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.<String>get("nombre"));
        paths.add(p.<String>get("valor"));
        return paths;
    }

    @Override
    public boolean existe(final String variable) {

        Element element = ehcache.get(variable);
        if (element != null && !element.isExpired() && element.getObjectValue() != null) {
            return true;
        }

        try {
            return doWithTransaction((entityManager, t) -> {
                OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
                OpenJPACriteriaQuery<Variable> query = cb.createQuery(Variable.class);
                query.where(cb.equal(query.from(Variable.class).get("nombre"), variable));
                TypedQuery<Variable> typedQuery = entityManager.createQuery(query);

                List<Variable> resultList = typedQuery.getResultList();
                return resultList != null && resultList.size() > 0;
            });
        } catch (Exception e) {
            LOGGER.error("No pude encontrar una variable, asumo que no existe", e);
            return false;
        }
    }

    @Override
    public String getValorDe(final String nombreVariable, String valorPorDefecto) {
        Element element = ehcache.get(nombreVariable);
        if (element != null && !element.isExpired() && element.getObjectValue() != null) {
            return (String) element.getObjectValue();
        }
        Variable variable;
        try {
            variable = doWithTransaction((entityManager, t) -> {
                OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
                OpenJPACriteriaQuery<Variable> query = cb.createQuery(Variable.class);
                query.where(cb.equal(query.from(Variable.class).get("nombre"), nombreVariable));

                TypedQuery<Variable> typedQuery = entityManager.createQuery(query);
                List<Variable> resultList = typedQuery.getResultList();
                if (resultList.isEmpty()) {
                    return null;
                }
                if (resultList.size() > 1) {
                    LOGGER.warn("Buscando una variable con mas de un registro, {}, deviolviendo el primero", resultList);
                }
                return resultList.get(0);
            });
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error al obtener el valor de la variable, asumiendo el valor por defecto", e);
            variable = null;
        }

        boolean putInCache = true;

        String valor;
        if (variable == null) {
            putInCache = false;
            valor = configuration.getString(nombreVariable, valorPorDefecto);
        } else {
            valor = variable.getValor();
        }

        if (cryptUtils.esCryptLegado(valor)) {
            valor = cryptUtils.dec(valor);
        } else if (cryptUtils.esOpenssl(valor)) {
            valor = cryptUtils.decssl(valor);
        }
        valor = Joiner.on("\n").join(Iterables.filter(Splitter.on("\n").split(valor), input -> input != null && !input.startsWith("// ")));

        if (putInCache) {
            element = new Element(nombreVariable, valor);
            element.setTimeToLive((int) (Timer.ONE_DAY / 1000L));
            ehcache.put(element);
        }

        return valor;
    }

    @Override
    public Long getValorLongDe(String nombreVariable, Long valorPorDefecto) {
        try {
            return Long.parseLong(getValorDe(nombreVariable, ""));
        } catch (NumberFormatException e) {
            LOGGER.warn("Utilizando el valor por defecto para la variable entera {}", nombreVariable);
            return valorPorDefecto;
        }
    }

    @Override
    public Integer getValorIntDe(String nombreVariable, Integer valorPorDefecto) {
        try {
            return Integer.parseInt(getValorDe(nombreVariable, ""));
        } catch (NumberFormatException e) {
            LOGGER.warn("Utilizando el valor por defecto para la variable entera {}", nombreVariable);
            return valorPorDefecto;
        }
    }

    @Override
    public BigDecimal getValorBigDecimalDe(String variable, BigDecimal valorPorDefecto) {
        try {
            return new BigDecimal(getValorDe(variable, ""));
        } catch (NumberFormatException e) {
            LOGGER.warn("Utilizando el valor por defecto para la variable decimal {}", variable);
            return valorPorDefecto;
        }
    }

    public int poblarValoresPorDefecto() {
        return doWithTransaction((entityManager, t) -> {
            OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();

            int insersiones = 0;
            Field[] fields = Variables.class.getFields();
            for (Field field : fields) {
                if (field.getName().endsWith("DEFAULT")) {
                    continue;
                }

                Object nombrefld;
                try {
                    nombrefld = field.get(null);
                } catch (IllegalAccessException e) {
                    // NOP
                    continue;
                }

                OpenJPACriteriaQuery<Long> query = cb.createQuery(Long.class);
                Root<Variable> from = query.from(Variable.class);
                query.select(cb.count(from.get("id")));
                query.where(cb.equal(from.get("nombre"), nombrefld));
                Long singleResult;
                try {
                    singleResult = entityManager.createQuery(query).getSingleResult();
                } catch (NoResultException e) {
                    singleResult = 0L;
                }

                if (singleResult == null || singleResult == 0) {

                    try {
                        Field valorDefault = Variables.class.getField(field.getName() + "_DEFAULT");
                        Object valor = valorDefault.get(null);
                        if (valor == null) {
                            continue;
                        }

                        Variable variable = new Variable();
                        variable.setNombre(nombrefld.toString());
                        variable.setValor(valor.toString());
                        entityManager.persist(variable);

                        insersiones++;
                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        // NOP
                    }
                }
            }

            return insersiones;
        });

    }

    public String valor(String nombre, Object... param) {
        //String valorPrimitivo = valorDe(nombre);
        try {
            StringBuilder sb = new StringBuilder();
            Formatter formatter = new Formatter(sb, new Locale("es", "ES"));
            formatter.format(nombre, param);
            return sb.toString();
        } catch (IllegalFormatException e) {
            LOGGER.error("Ha ocurrido un error de formato al intentar interpolar la variable "
                    + nombre + ". Devolviendo el valor crudo.", e);
            return nombre;
        }
    }

    @Override
    public List<Variable> getValorBulkDe(final String nombreVariable) {
        List<Variable> variables;
        try {
            variables = doWithTransaction((entityManager, t) -> {
                String nombreVar = nombreVariable.substring(0, nombreVariable.lastIndexOf(".")) + "%";
                OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
                OpenJPACriteriaQuery<Variable> query = cb.createQuery(Variable.class);
                Root<Variable> p = query.from(Variable.class);
                query.where(cb.like(p.get("nombre"), nombreVar));
                query.orderBy(cb.asc(p.get("nombre")));
                TypedQuery<Variable> typedQuery = entityManager.createQuery(query);
                return typedQuery.getResultList();
            });
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error al obtener el valor de la variable, asumiendo el valor por defecto", e);
            variables = null;
        }
        return variables;
    }

}
