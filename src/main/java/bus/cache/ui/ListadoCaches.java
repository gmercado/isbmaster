/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.cache.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import com.google.inject.Inject;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Statistics;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.model.AbstractReadOnlyModel;


/**
 * @author Marcelo Morales
 * @since 9/7/11
 */
@Menu(value = "Detalle de caches", subMenu = SubMenu.ADMINISTRACION)
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
public class ListadoCaches extends BisaWebPage {

    @Inject
    private Ehcache ehcache;

    public ListadoCaches() {
        add(new CacheStatsPanel("variables", new AbstractReadOnlyModel<Statistics>() {
            @Override
            public Statistics getObject() {
                return ehcache.getStatistics();
            }
        }));
    }
}
