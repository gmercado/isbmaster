/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.cache.ui;

import bus.inicio.ui.BisaWebPage;
import net.sf.ehcache.Statistics;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxFallbackLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 * @author Marcelo Morales
 * @since 9/10/11
 */
public class CacheStatsPanel extends Panel {

    public CacheStatsPanel(String id, final IModel<Statistics> model) {
        super(id);

        IModel<Statistics> iModel = new CompoundPropertyModel<>(model);

        Form<Statistics> form;
        add(form = new Form<>("cacheForm", iModel));
        form.setOutputMarkupId(true);

        form.add(new Label("associatedCacheName"));
        form.add(new Label("averageGetTime"));
        form.add(new Label("cacheHits"));
        form.add(new Label("cacheMisses"));
        form.add(new AjaxLink<Statistics>("show", iModel) {

            @Override
            public void onClick(AjaxRequestTarget target) {
                target.appendJavaScript("alert('" +
                        getModelObject().getAssociatedCache().getKeys() +
                        "');");
            }
        }.add(new Label("objectCount")));
        form.add(new Label("averageSearchTime"));
        form.add(new Label("memoryStoreObjectCount"));
        form.add(new Label("diskStoreObjectCount"));
        form.add(new Label("evictionCount"));
        form.add(new Label("inMemoryHits"));
        form.add(new Label("inMemoryMisses"));
        form.add(new Label("offHeapHits"));
        form.add(new Label("offHeapMisses"));
        form.add(new Label("offHeapStoreObjectCount"));
        form.add(new Label("onDiskHits"));
        form.add(new Label("onDiskMisses"));
        form.add(new Label("eficiencia", new LoadableDetachableModel<Object>() {

            @Override
            protected Object load() {
                Statistics statistics = model.getObject();
                final long cacheHits = statistics.getCacheHits();
                final long cacheMisses = statistics.getCacheMisses();

                final long denom = cacheHits + cacheMisses;

                if (denom == 0) {
                    return "-";
                }

                final double numer = cacheHits - cacheMisses;

                return (numer / denom) * 100;
            }
        }));

        form.add(new IndicatingAjaxFallbackLink<Statistics>("limpiar", iModel) {

            @Override
            public void onClick(AjaxRequestTarget target) {
                Statistics st = getModelObject();
                st.clearStatistics();
                getSession().info("Estad\u00edsticas de cache re-seteadas");
                if (target != null) {
                    target.add(findParent(Form.class));
                    target.add(((BisaWebPage) getPage()).feedbackPanel);
                }
            }
        });

        form.add(new IndicatingAjaxFallbackLink<Statistics>("eliminar", iModel) {

            @Override
            public void onClick(AjaxRequestTarget target) {
                Statistics st = getModelObject();
                st.getAssociatedCache().removeAll();
                getSession().warn("Estad\u00edsticas eliminadas");
                if (target != null) {
                    target.add(findParent(Form.class));
                    target.add(((BisaWebPage) getPage()).feedbackPanel);
                }
            }
        });
    }
}
