package bus.cache;

import bus.config.api.Configurations;
import bus.plumbing.api.AbstractModuleBisa;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Scopes;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.config.CacheConfiguration;
import org.apache.commons.configuration.Configuration;


/**
 * @author Marcelo Morales
 * @since 4/30/12
 */
public class CacheModule extends AbstractModuleBisa {

    public CacheModule() {
    }

    @Override
    protected void configure() {
        bind(Ehcache.class).toProvider(CacheProvider.class).in(Scopes.SINGLETON);
        bindUI("bus/cache/ui");
    }

    public static class CacheProvider implements Provider<Ehcache> {

        private final Configuration configuration;

        @Inject
        public CacheProvider(Configuration configuration) {
            this.configuration = configuration;
        }

        @Override
        public Ehcache get() {
            CacheConfiguration cacheConfiguration = new CacheConfiguration();
            cacheConfiguration.setName("cache por defecto");
            cacheConfiguration.setStatistics(true);
            cacheConfiguration.setMaxEntriesLocalHeap(
                    configuration.getInt(Configurations.CACHE_MAX, Integer.parseInt(Configurations.CACHE_MAX_DEFAULT)));
            Cache cache = new Cache(cacheConfiguration);
            cache.setCacheManager(new CacheManager());
            cache.initialise();
            return cache;
        }
    }
}
