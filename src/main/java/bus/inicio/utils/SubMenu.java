/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.inicio.utils;

/**
 * @author Marcelo Morales
 *         Created: 3/19/12 5:16 PM
 */
public enum SubMenu {

    NINGUNO(null),

    ADMINISTRACION("Administraci\u00f3n"),

    PARAMETROS("Par\u00e1metros"),

    SEGIP("SEGIP"),

    SIN("SIN"),

    LINKSER("LINKSER"),

    SERVICIOS_BOA("Servicios BoA"),

    SERVICIO_WEB("Servicios Web"),

    INDICADORES_BCB("Indicadores BCB"),

    JOVEN("BISA Neo"),

    INFOCRED("Infocred"),

    FTP("FTP"),

    JOBS_IP("Jobs AS400"),

    MESA_AYUDA("Contratos");

    private final String descripcion;

    SubMenu(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
