package bus.inicio.ui;

import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.wicket.Version;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * @author Marcelo Morales
 * @since 4/30/12
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.SOLO_LECTURA, RolesBisa.ISB_ADMIN, RolesBisa.SERVICIOS,
        RolesBisa.ISB_BOA,
        RolesBisa.SEGIP_CONSULTAS, RolesBisa.SEGIP_CONSUMO, RolesBisa.SEGIP_PARAM, RolesBisa.SEGIP_REPORTE,
        RolesBisa.SIN_DOSIFICACION, RolesBisa.SIN_ADM, RolesBisa.BCB_COT,RolesBisa.BCB_TRE, RolesBisa.FTP_TRANSFERENCIA,
        RolesBisa.JOVEN_MOVIMIENTOS, RolesBisa.JOB_MONITOR_POR_IP,
        RolesBisa.LINKSER_CONSULTAS, RolesBisa.LINKSER_CONSUMO })
@Titulo("Sistema ISB - Integration Service Bus")
public class Inicio extends BisaWebPage {

    private static final long serialVersionUID = 8014447377136381004L;

    public Inicio(PageParameters parameters) {
        super(parameters);

        setDefaultModel(new CompoundPropertyModel<Version>(new AbstractReadOnlyModel<Version>() {

            private static final long serialVersionUID = -150401039956617180L;

            @Override
            public Version getObject() {
                return getInstance(Version.class);
            }
        }));

        add(new Label("versionPom"));
        add((new Label("built")).setVisible(false));
        add((new Label("construidoPor")).setVisible(false));
        add((new Label("versionMercurial")).setVisible(false));
        add((new Label("jdk")).setVisible(false));
        add(new Label("producto"));
    }
}
