/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.inicio.ui;

import bus.inicio.ui.ArmadorMenu.Entrada;
import bus.plumbing.components.AlertFeedbackPanel;
import bus.plumbing.utils.IPClienteWeb;
import bus.plumbing.wicket.BisaWebApplication;
import bus.users.api.Metadatas;
import bus.users.entities.AdmUser;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import com.google.inject.*;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.IHeaderContributor;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.protocol.http.servlet.ServletWebRequest;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.PackageResourceReference;
import org.apache.wicket.resource.JQueryResourceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author Marcelo Morales
 * @since 1/10/11
 */
public class BisaWebPage extends WebPage implements IHeaderContributor {

    private static final long serialVersionUID = -6434824898899114808L;
    public static final String HIDE = "hide invisible";
    public static final String SHOW = "control-group";

    protected static final Logger LOGGER = LoggerFactory.getLogger(BisaWebPage.class);

    public final AlertFeedbackPanel feedbackPanel = new AlertFeedbackPanel("feedback", false);

    public BisaWebPage() {
        super();
    }

    public BisaWebPage(PageParameters parameters) {
        super(parameters);
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        // TODO: cambiar la referencia del jquery del wicket para que use el que estamos mostrando
        final Request request = getRequest();
        final String contextPath = request.getContextPath();
        //response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/jquery-1.8.3.js"));
        final JQueryResourceReference jQueryResourceReference = JQueryResourceReference.get();
        response.render(JavaScriptHeaderItem.forReference(jQueryResourceReference));

        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/bootstrap-transition.js"));
        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/bootstrap-alert.js"));
        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/bootstrap-modal.js"));
        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/bootstrap-dropdown.js"));
        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/bootstrap-scrollspy.js"));
        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/bootstrap-tab.js"));
        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/bootstrap-tooltip.js"));
        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/bootstrap-popover.js"));
        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/bootstrap-button.js"));
        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/bootstrap-collapse.js"));
        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/bootstrap-carousel.js"));
        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/bootstrap-typeahead.js"));
        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/modernizr.custom.63721.js"));
        response.render(JavaScriptHeaderItem.forUrl(contextPath + "/1/js/jquery-ui-1.9.2.custom.min.js"));

        // TODO: Como resolver el problema!!!!!.
        // 1) Si los generamos en el webapp directamente, NO aparecen en jetty:run, solo en jetty:run-war
        // 2) Si Corremos jetty:run-war, el cambio de templates en l�nea y la recarga de clases no funciona
        // 3) Si los dejamos aqui, no pasan por el cache del browser.
        response.render(CssHeaderItem.forReference(new PackageResourceReference(BisaWebPage.class, "bootstrap.css")));
        response.render(CssHeaderItem.forReference(new PackageResourceReference(BisaWebPage.class, "responsive.css")));

        response.render(CssHeaderItem.forUrl(contextPath + "1/js/jquery-ui-1.9.2.custom.min.css"));
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        feedbackPanel.setOutputMarkupId(true);
        add(feedbackPanel);

        add(new Label("_titulo", ArmadorMenu.obtenerTitulo(getClass())));
        add(new Label("_subtitulo", ArmadorMenu.obtenerSubTitulo(getClass())));

        add(new LogoLink("_logo"));

        AdmUser metaData = getSession().getMetaData(Metadatas.USER_META_DATA_KEY);
        String userlogon;
        if (metaData == null) {
            userlogon = "[desconectado]";
        } else {
            userlogon = metaData.getUserlogon();
        }

        //add(new Label("_usuario", userlogon).setVisible(AuthenticatedWebSession.get().isSignedIn()));
        add(new Link<Void>("_inicio") {
            private static final long serialVersionUID = 1L;

            @Override
            public void onClick() {
                setResponsePage(Inicio.class);
            }
        });

        Link<String> usuarioLabel = new Link<String>("_usuario", Model.of(userlogon)) {
            private static final long serialVersionUID = 1L;

            @Override
            public void onClick() {
            }

            @Override
            protected void onBeforeRender() {
                Component component = get(getId() + "_icon");
                if (component != null) {
                    remove(component);
                }
                add(new Component(getId() + "_icon", getDefaultModel()) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    protected void onRender() {
                        getResponse().write(" <i class=\"icon-user icon-white\"></i>  " + getDefaultModel().getObject());
                    }
                });
                super.onBeforeRender();
            }
        };

        add(usuarioLabel.setVisible(AuthenticatedWebSession.get().isSignedIn()));

        AbstractReadOnlyModel<List<Class<? extends BisaWebPage>>> menu =
                new AbstractReadOnlyModel<List<Class<? extends BisaWebPage>>>() {

                    private static final long serialVersionUID = 5659215940591025275L;

                    @Override
                    public List<Class<? extends BisaWebPage>> getObject() {
                        return getInstance(ArmadorMenu.class).getMenu();
                    }
                };

        AbstractReadOnlyModel<List<ArmadorMenu.Entrada>> menuAdmin =
                new AbstractReadOnlyModel<List<ArmadorMenu.Entrada>>() {

                    private static final long serialVersionUID = 5518296082932291951L;

                    @Override
                    public List<ArmadorMenu.Entrada> getObject() {
                        return getInstance(ArmadorMenu.class).getMenusDropDowns();
                    }
                };

        add(new MenuListView("_menu de usuario", menu));

        add(new EntradaListView("_menus adminstrativos", menuAdmin).setOutputMarkupId(true));

        add(new Link<Void>("_desconectar") {

            private static final long serialVersionUID = -6221093437276759223L;

            @Override
            public void onClick() {
                AuthenticatedWebSession authenticatedWebSession = AuthenticatedWebSession.get();
                authenticatedWebSession.signOut();
                authenticatedWebSession.invalidate();
                setResponsePage(Ingreso.class);
            }
        });
    }


    protected BisaWebApplication getAdminApplication() {
        return BisaWebApplication.get();
    }

    protected <T> MembersInjector<T> getMembersInjector(TypeLiteral<T> typeLiteral) {
        return getAdminApplication().getMembersInjector(typeLiteral);
    }

    protected <T> MembersInjector<T> getMembersInjector(Class<T> type) {
        return getAdminApplication().getMembersInjector(type);
    }

    protected Map<Key<?>, Binding<?>> getBindings() {
        return getAdminApplication().getBindings();
    }

    protected Map<Key<?>, Binding<?>> getAllBindings() {
        return getAdminApplication().getAllBindings();
    }

    protected <T> Binding<T> getBinding(Key<T> key) {
        return getAdminApplication().getBinding(key);
    }

    protected <T> Binding<T> getBinding(Class<T> type) {
        return getAdminApplication().getBinding(type);
    }

    protected <T> Binding<T> getExistingBinding(Key<T> key) {
        return getAdminApplication().getExistingBinding(key);
    }

    protected <T> List<Binding<T>> findBindingsByType(TypeLiteral<T> type) {
        return getAdminApplication().findBindingsByType(type);
    }

    protected <T> Provider<T> getProvider(Key<T> key) {
        return getAdminApplication().getProvider(key);
    }

    protected <T> Provider<T> getProvider(Class<T> type) {
        return getAdminApplication().getProvider(type);
    }

    protected <T> T getInstance(Key<T> key) {
        return getAdminApplication().getInstance(key);
    }

    protected <T> T getInstance(Class<T> type) {
        return getAdminApplication().getInstance(type);
    }

    private class MenuListView extends ListView<Class<? extends BisaWebPage>> {

        private static final long serialVersionUID = 9038660396718883001L;

        public MenuListView(String id, IModel<List<Class<? extends BisaWebPage>>> menu) {
            super(id, menu);
        }

        @Override
        protected void populateItem(ListItem<Class<? extends BisaWebPage>> classListItem) {
            Class<? extends BisaWebPage> cls = classListItem.getModelObject();

            BookmarkablePageLink<BisaWebPage> bpl;
            classListItem.add(bpl = new BookmarkablePageLink<BisaWebPage>("enlace", cls) {

                private static final long serialVersionUID = -245466202761954314L;

                protected void disableLink(final ComponentTag tag) {
                    // if the tag is an anchor proper
                    if (tag.getName().equalsIgnoreCase("a")) {
                        // Remove any href from the old link
                        tag.remove("href");

                        tag.remove("onclick");
                    }
                    // if the tag is a button or input
                }
            });

            bpl.setEnabled(!cls.equals(BisaWebPage.this.getClass()) &&
                    getApplication().getSecuritySettings().getAuthorizationStrategy().isInstantiationAuthorized(cls));

            if (cls.equals(BisaWebPage.this.getClass())) {
                bpl.add(new AttributeAppender("class", Model.of("active"), " "));
            }

            bpl.add(new Label("label", ArmadorMenu.obtenerTitulo(cls)));
        }
    }

    private class EntradaListView extends ListView<ArmadorMenu.Entrada> {

        private static final long serialVersionUID = -2504209340208930312L;

        public EntradaListView(final String id, IModel<List<ArmadorMenu.Entrada>> menuAdmin) {
            super(id, menuAdmin);
        }

        @Override
        protected void populateItem(ListItem<ArmadorMenu.Entrada> objectListItem) {

            final IModel<ArmadorMenu.Entrada> mdl = objectListItem.getModel();
            WebMarkupContainer wmk;
            objectListItem.add(wmk = new WebMarkupContainer("dropdown"));
            wmk.add(new Label("titulo", new PropertyModel<Object>(mdl, "p.descripcion")));
            wmk.add(new AttributeAppender("href", "#" + EntradaListView.this.getMarkupId()));
            wmk.add(new BehaviorComponent(objectListItem));

            MenuListView menuListView = new MenuListView("_menu administrativo",
                    new PropertyModel<List<Class<? extends BisaWebPage>>>(mdl, "q"));
            menuListView.add(new BehaviorComponent(objectListItem));

            objectListItem.add(menuListView);
        }
    }


    class BehaviorComponent extends Behavior {
        private static final long serialVersionUID = 1L;
        ListItem<Entrada> componentLocal;

        public BehaviorComponent(ListItem<Entrada> component) {
            this.componentLocal = component;
        }

        @Override
        public void onConfigure(Component component) {
            super.onConfigure(componentLocal);
            List<Class<? extends BisaWebPage>> modelObject = componentLocal.getModelObject().getQ();
            component.setVisible(Iterators.any(modelObject.iterator(), new Predicate<Class<? extends BisaWebPage>>() {

                @Override
                public boolean apply(Class<? extends BisaWebPage> cls) {
                    return !cls.equals(BisaWebPage.this.getClass()) &&
                            getApplication().getSecuritySettings().getAuthorizationStrategy().isInstantiationAuthorized(cls);
                }
            }));
        }
    }


    class LogoLink extends AjaxFallbackLink<Void> {
        private static final long serialVersionUID = 271100524610019978L;

        public LogoLink(String id) {
            super(id);
        }

        @Override
        public void onClick(AjaxRequestTarget target) {
            // TODO incorporar un mensaje al  hacer click en la imagen
            /*if (target != null) {
                target.add(findParent(DataTable.class));
            }*/
        }

        @Override
        protected void onInitialize() {
            super.onInitialize();
        }
    }

    public static String getIPAcceso(Request request) {
        ServletWebRequest servletWebRequest = (ServletWebRequest) request;
        HttpServletRequest httpServletRequest = servletWebRequest.getContainerRequest();
        return IPClienteWeb.getClientIpAddr(httpServletRequest);
    }
}