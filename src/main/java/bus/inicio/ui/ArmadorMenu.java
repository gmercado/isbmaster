/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.inicio.ui;

import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.inicio.utils.Titulo;
import bus.plumbing.utils.TodoElProyectoSerp;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.WebPage;
import serp.bytecode.Annotation;
import serp.bytecode.Annotations;
import serp.bytecode.Attributes;
import serp.bytecode.BCClass;
import serp.bytecode.visitor.BCVisitor;

import java.util.*;

/**
 * @author Marcelo Morales
 * @since 6/20/11
 */
@Singleton
public class ArmadorMenu {

    /**
     * Contiene las paginas que deben aparecer en el menu
     */
    private List<Class<? extends BisaWebPage>> menu;

    /**
     * Contiene las paginas que deben aparecer en el menu de administracion
     */
    private Map<SubMenu, List<Class<? extends BisaWebPage>>> menusDropDowns;

    /**
     * Contiene las paginas que pueden llegarse directamete (creaciones, etc)
     */
    private List<Class<? extends BisaWebPage>> paginasDirectas;

    private final TodoElProyectoSerp todoElProyectoSerp;

    @Inject
    public ArmadorMenu(TodoElProyectoSerp todoElProyectoSerp) {
        this.todoElProyectoSerp = todoElProyectoSerp;
    }

    public static String obtenerTitulo(Class<? extends WebPage> modelObject) {
        String t;
        Menu annotation = modelObject.getAnnotation(Menu.class);
        if (annotation == null) {
            Titulo titulo = modelObject.getAnnotation(Titulo.class);
            if (titulo == null) {
                t = modelObject.getName();
            } else {
                t = titulo.value();
            }
        } else {
            t = annotation.value();
        }
        return t;
    }

    public static String obtenerSubTitulo(Class<? extends WebPage> modelObject) {
        String t;
        Menu annotation = modelObject.getAnnotation(Menu.class);
        if (annotation == null) {
            Titulo titulo = modelObject.getAnnotation(Titulo.class);
            if (titulo == null) {
                t = "";
            } else {
                t = titulo.subtitulo();
            }
        } else {
            t = annotation.subtitulo();
        }
        return t;
    }

    public synchronized List<Class<? extends BisaWebPage>> getMenu() {
        if (menu == null) {
            init();
        }
        return menu;
    }

    public synchronized List<Entrada> getMenusDropDowns() {
        if (menu == null) {
            init();
        }
        List<Entrada> pars = new LinkedList<>();
        for (SubMenu subMenu : menusDropDowns.keySet()) {
            final List<Class<? extends BisaWebPage>> classes = menusDropDowns.get(subMenu);
            pars.add(new Entrada(subMenu, classes));
        }

        Collections.sort(pars, (o1, o2) -> o1.p.compareTo(o2.p));

        return pars;
    }

    private void init() {
        menu = new ArrayList<>();
        menusDropDowns = new TreeMap<>();
        paginasDirectas = new ArrayList<>();
        todoElProyectoSerp.acceptVisit(new MenuVisitor());
        Comparator<Class<? extends BisaWebPage>> comparator = (o1, o2) -> o1.getAnnotation(Menu.class).value().compareTo(o2.getAnnotation(Menu.class).value());

        Collections.sort(menu, comparator);

        for (List<Class<? extends BisaWebPage>> classList : menusDropDowns.values()) {
            Collections.sort(classList, comparator);
        }

    }

    @SuppressWarnings({"unchecked"})
    private class MenuVisitor extends BCVisitor {

        @Override
        public void enterAnnotations(Annotations obj) {
            Attributes owner1 = obj.getOwner();

            if (!(owner1 instanceof BCClass)) {
                return;
            }

            Annotation annotation = obj.getAnnotation(Menu.class);
            if (annotation == null) {

                annotation = obj.getAnnotation(Titulo.class);
                if (annotation == null) {
                    return;
                }

                annotation = obj.getAnnotation(AuthorizeInstantiation.class);
                if (annotation == null) {
                    return;
                }

                BCClass owner = (BCClass) owner1;
                Class type = owner.getType();
                if (BisaWebPage.class.isAssignableFrom(type)) {
                    paginasDirectas.add(type);
                }

                return;
            }

            Annotation annotationInstantiation = obj.getAnnotation(AuthorizeInstantiation.class);
            if (annotationInstantiation == null) {
                return;
            }

            BCClass owner = (BCClass) owner1;
            Class type = owner.getType();
            if (BisaWebPage.class.isAssignableFrom(type)) {
                final SubMenu subMenu = ((Menu) type.getAnnotation(Menu.class)).subMenu();
                if (SubMenu.NINGUNO.equals(subMenu)) {
                    menu.add(type);
                } else {
                    List<Class<? extends BisaWebPage>> classList = menusDropDowns.get(subMenu);
                    if (classList == null) {
                        menusDropDowns.put(subMenu, classList = new ArrayList<>());
                    }
                    classList.add(type);
                }
                paginasDirectas.add(type);
            }
        }
    }

    public static final class Entrada {
        private final SubMenu p;
        private final List<Class<? extends BisaWebPage>> q;

        private Entrada(SubMenu p, List<Class<? extends BisaWebPage>> q) {
            this.p = p;
            this.q = q;
        }

        public SubMenu getP() {
            return p;
        }

        public List<Class<? extends BisaWebPage>> getQ() {
            return q;
        }
    }
}
