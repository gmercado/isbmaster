/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.inicio.ui;

import bus.inicio.utils.Titulo;
import bus.plumbing.components.ControlGroupBorder;
import org.apache.wicket.Component;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.validation.validator.StringValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marcelo Morales
 * @since 6/6/11
 */
@Titulo(value="Ingreso al Sistema ISB",subtitulo="Integration Service Bus")
public class Ingreso extends BisaWebPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(Ingreso.class);

    private static final long serialVersionUID = 1282616188039695965L;

    private final IModel<String> usernameModel = Model.of("");
    private final IModel<String> passwordModel = Model.of("");
    private final Component usernameTF;
    private final Component passwordtf;

    public Ingreso() {
        super();

        StatelessForm<Void> form;
        add(form = new StatelessForm<Void>("ingreso") {

            private static final long serialVersionUID = 824626961720077349L;

            @Override
            public final void onSubmit() {
                if (signIn(usernameModel.getObject(), passwordModel.getObject())) {
                    onSignInSucceeded();
                } else {
                    onSignInFailed();
                }
            }

        });

        usernameTF = new RequiredTextField<String>("username", usernameModel, String.class).
                add(StringValidator.maximumLength(40)).
                setLabel(Model.of("Nombre de usuario")).
                setOutputMarkupId(true);

        form.add(new ControlGroupBorder("username", usernameTF, feedbackPanel, Model.of("Nombre de usuario")).add(usernameTF));

        passwordtf = new PasswordTextField("password", passwordModel).
                setLabel(Model.of("contraseña")).
                setType(String.class).
                setOutputMarkupId(true);

        form.add(new ControlGroupBorder("password", passwordtf, feedbackPanel, Model.of("Contrase\u00f1a")).add(passwordtf));

        final Button ingreso = new Button("ingreso");
        form.add(ingreso);
        form.setDefaultButton(ingreso);

    }

    public boolean signIn(String username, String password) {
        try {
            return AuthenticatedWebSession.get().signIn(username, password);
        } catch (Exception e) {
            String error = e.getMessage();
            if (error == null) {
                LOGGER.error("Ha ocurrido un error inesperado y sin mensaje", e);
                passwordtf.error("Ha ocurrido un error inesperado");
            } else {
                passwordtf.error(error);
            }
        }
        return false;
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        response.render(OnDomReadyHeaderItem.forScript("$('#" + usernameTF.getMarkupId() + "').focus();"));
    }

    protected void onSignInFailed() {
        passwordtf.error(getLocalizer().getString("signInFailed", this, "Sign in failed"));
    }

    protected void onSignInSucceeded() {
        continueToOriginalDestination();
        setResponsePage(getApplication().getHomePage());
    }

}
