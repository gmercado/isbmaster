package bus.inicio;

import bus.inicio.ui.Ingreso;
import bus.inicio.ui.Inicio;
import bus.plumbing.api.AbstractModuleBisa;
import bus.plumbing.wicket.PaginaIngresoClass;
import bus.plumbing.wicket.PaginaPrincipalClass;

/**
 * @author Marcelo Morales
 * @since 4/30/12
 */
public class InicioModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(Class.class).annotatedWith(PaginaIngresoClass.class).toInstance(Ingreso.class);
        bind(Class.class).annotatedWith(PaginaPrincipalClass.class).toInstance(Inicio.class);

        bindUI("bus/inicio/ui");
    }
}
