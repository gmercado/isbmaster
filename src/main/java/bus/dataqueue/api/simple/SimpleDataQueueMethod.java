package bus.dataqueue.api.simple;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * La clase <code>SimpleDataQueueMethod.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 27/05/2014, 15:56:57
 */
@BindingAnnotation
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpleDataQueueMethod {
}
