package bus.dataqueue.api.simple;

import bus.dataqueue.api.common.DataQueueClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

/**
 * La clase <code>SimpleDataQueueInvoke.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 28/05/2014, 10:27:24
 */
public class SimpleDataQueueInvoke implements Runnable, Member, AnnotatedElement {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleDataQueueInvoke.class);

    public final Method method;
    public final Object target;
    public final Object[] parameters;

    public SimpleDataQueueInvoke(DataQueueClass clazz) {
        this.target = clazz.getTarget();
        this.method = clazz.getMethod();
        this.parameters = clazz.getParameters();
    }

    @Override
    public void run() {
        try {
            invoke();
        } catch (InvocationTargetException ex) {
            LOG.error(getInvocationFailureMessage(), ex.getTargetException());
        } catch (IllegalAccessException ex) {
            LOG.error(getInvocationFailureMessage(), ex);
        } catch (Throwable e) {
            LOG.error("Ha ocurrido un error inesperado", e);
        }
    }

    public Object invoke() throws InvocationTargetException, IllegalAccessException {
        return method.invoke(target, parameters);
    }

    protected String getInvocationFailureMessage() {
        return "Invocation of method '" + method.getName() + "' on target class [" + target + "] failed";
    }

    @Override
    public Class<?> getDeclaringClass() {
        return method.getDeclaringClass();
    }

    @Override
    public String getName() {
        return method.getName();
    }

    @Override
    public int getModifiers() {
        return method.getModifiers();
    }

    @Override
    public boolean isSynthetic() {
        return method.isSynthetic();
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass) {
        return true;
    }

    @Override
    public <T extends Annotation> T getAnnotation(Class<T> annotationClass) {
        return method.getAnnotation(annotationClass);
    }

    @Override
    public Annotation[] getAnnotations() {
        return method.getAnnotations();
    }

    @Override
    public Annotation[] getDeclaredAnnotations() {
        return method.getDeclaredAnnotations();
    }
}
