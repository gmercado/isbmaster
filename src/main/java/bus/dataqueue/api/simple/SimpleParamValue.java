package bus.dataqueue.api.simple;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * La clase <code>SimpleParamValue.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 28/05/2014, 10:17:31
 */
@BindingAnnotation
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpleParamValue {
}
