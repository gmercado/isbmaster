package bus.dataqueue.api.simple;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * La clase <code>SimpleDataQueueDelegate.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 27/05/2014, 15:54:07
 */
@BindingAnnotation
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpleDataQueueDelegate {
    String value();
}
