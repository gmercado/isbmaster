package bus.dataqueue.api.simple;

import bus.dataqueue.api.common.DataQueueBase;
import bus.dataqueue.api.common.DataQueueClass;
import bus.dataqueue.task.Task;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.monitor.as400.AS400Factory;
import com.google.inject.Inject;
import com.ibm.as400.access.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * La clase <code>SimpleDataQueueTask.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 27/05/2014, 15:47:13
 */
public class SimpleDataQueueTask extends DataQueueBase implements Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleDataQueueTask.class);
    private final MedioAmbiente medioAmbiente;
    private final AS400Factory factory;

    @Inject
    public SimpleDataQueueTask(MedioAmbiente medioAmbiente, AS400Factory as400) {
        this.medioAmbiente = medioAmbiente;
        this.factory = as400;
    }

    @Override
    public boolean isCorriendo() {
        return currentThread != null && currentThread.isAlive();
    }

    @Override
    public void start() throws Exception {
        //Inicio
        ExecutorService executorService;
        String colaEntradaCompleta;
        //Ambiente
        /*if (esDesarrollo()) {
            if (isPrimeraEjecucionEnDesarrollo()) {
                LOGGER.warn("NO iniciamos el lector para el metodo '{}' porque es la primera ejecucion en DESARROLLO.", getClazz().getMethodName());
                setPrimeraEjecucionEnDesarrollo(false);
                return;
            }
            LOGGER.info("Iniciamos el lector simple para el metodo '{}'. Se asume que se inicio manualmente la ejecucion en DESARROLLO., ", getClazz().getMethodName());
        } else {
            LOGGER.info("Iniciamos el lector simple para el metodo '{}'. Ejecucion en PRODUCCION.", getClazz().getMethodName());
        }*/
        //verificar parametros de entrada
        validarParametros();

        executorService = Executors.newFixedThreadPool(3);
        colaEntradaCompleta = new QSYSObjectPathName(getLibreriaColas(), getColaEntrada(), DATAQUEUE).getPath();
        //Proceso
        currentThread = new Thread(() -> {
            DataQueue dataQueue;
            DataQueueEntry dqData;
            String dataString;
            AS400 as400 = null;
            int cantidaDeLecturas = 0;
            try {
                as400 = factory.getAS400(AS400.DATAQUEUE);
                started = true;
                while (started && as400 != null) {
                    try {
                        dataQueue = new DataQueue(as400, colaEntradaCompleta);
                        if (cantidaDeLecturas == 0) {
                            if (isEliminarContenidoDataqueueLaPrimeraVez()) {
                                LOGGER.info("Elimina contenido del DQ {} para {} (1ra vez)", colaEntradaCompleta, getBeanName());
                                dataQueue.clear();
                                cantidaDeLecturas++;
                            } else {
                                LOGGER.info("No elimina contenido del DQ {} para {} (1ra vez)", colaEntradaCompleta, getBeanName());
                            }
                        }
                        LOGGER.debug("Inicio del lector dataqueue '{}' en el sistema AS/400 '{}'", colaEntradaCompleta, as400.getSystemName());
                        //Leer dataqueue
                        dqData = dataQueue.read(SEGUNDOS_TIMEOUT);
                        if (dqData == null) {
                            LOGGER.debug("La lectura del dataqueue '{}' en el sistema AS/400 '{}' no ha devuelto " +
                                            "resultados en '{}' segundos, realizando la lectura nuevamente, componente='{}'",
                                    colaEntradaCompleta, as400.getSystemName(), SEGUNDOS_TIMEOUT, getBeanName());
                            continue;
                        }
                        dataString = new String(dqData.getData(), FORMAT_IBM_284);
                        LOGGER.debug("Recibiendo la siguiente cadena (IBM284) '{}'", dataString);
                        //asignar valor de cola a parametro
                        getClazz().setValues(dataString);
                        //ejecutar metodo de clase
                        executorService.submit(new SimpleDataQueueInvoke(getClazz()));
                        //Incrementar lector
                        cantidaDeLecturas++;
                        LOGGER.debug("Se ha iniciado el trabajo '{}'", cantidaDeLecturas);
                    } catch (InterruptedException ie) {
                        LOGGER.info("Interrumpiendo lectura cola transacciones AS/400 '{}'. Se han lanzado {}" +
                                " ejecuciones de trabajos.", ie.toString(), cantidaDeLecturas);
                        break;
                    } catch (Exception e) {
                        LOGGER.error("Error en la cola " + getBeanName() + ": re-instanciando el sistema AS/400 en " +
                                (MILISEGUNDOS_REINSTANCIAR_POR_ERROR / 1000) + " segundos", e);
                        try {
                            Thread.sleep(MILISEGUNDOS_REINSTANCIAR_POR_ERROR);
                        } catch (InterruptedException ex) {
                            LOGGER.info("Interrumpiendo lectura cola transacciones AS/400 '{}'. Se han lanzado '{}'" +
                                            " ejecuciones de trabajos bean='{}'",
                                    ex.toString(), cantidaDeLecturas, getBeanName());
                            break;
                        }
                    }
                }
                LOGGER.info("Terminando la ejecucion de la lectura de cola {} despues de  transacciones. El " +
                        "numero de transacciones, {} puede no ser perfecto, sino variar por la cantidad de errores de" +
                        " ejecucion.", colaEntradaCompleta, cantidaDeLecturas);
            } catch (ConnectionPoolException e) {
                LOGGER.error("Error al conectar con as/400. Deteniendo el lector " + getBeanName(), e);
            } finally {
                started = false;
                factory.ret(as400, AS400.DATAQUEUE);
            }
        }, "Ejecutor400-" + getBeanName() + "-" + getColaEntrada());
        currentThread.start();
        try {
            Thread.sleep(UN_SEGUNDO);
        } catch (InterruptedException ex) {
            LOGGER.warn("Proceso interrumpido al intentar correr el componente " + getBeanName(), ex);
        }
        if (!currentThread.isAlive() || !started) {
            LOGGER.error("El hilo de lectura AS/400 '{}' no esta corriendo. Debe iniciarlo nuevamente de forma manual.",
                    getBeanName());
        }
    }

    @Override
    public void stop() throws Exception {
        started = false;
        Thread.yield();
        currentThread.interrupt();
        currentThread = null;
        LOGGER.info("Parando el lector para el metodo [{}] con nombre [{}] ", getClazz().getMethodName(), getBeanName());
    }

    @Override
    public String getFlujo() {
        return "De " + getLibreriaColas() + "/" + getColaEntrada();
    }

    @Override
    public void setClass(Object o) {
        setClazz(new DataQueueClass(o, getBeanName()));
    }

    @Override
    public void setBean(String bean) {
        setBeanName(medioAmbiente.getValorDe(bean + Variables.DATAQUEUE_BEAN, Variables.DATAQUEUE_BEAN_DEFAULT));
        setLibreriaColas(medioAmbiente.getValorDe(bean + Variables.DATAQUEUE_LIBRERIACOLAS, Variables.DATAQUEUE_LIBRERIACOLAS_DEFAULT));
        setColaEntrada(medioAmbiente.getValorDe(bean + Variables.DATAQUEUE_COLAENTRADA, Variables.DATAQUEUE_COLAENTRADA_DEFAULT));
        setEliminarContenidoDataqueueLaPrimeraVez(Boolean.valueOf(medioAmbiente.getValorDe(bean + Variables.DATAQUEUE_ELIMINARCONTENIDOPRIMERAVEZ, Variables.DATAQUEUE_ELIMINARCONTENIDOPRIMERAVEZ_DEFAULT)));
    }
}
