package bus.dataqueue.api.common;

import bus.dataqueue.api.extend.DataQueueData;
import bus.dataqueue.api.extend.DataQueueWriter;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.as400.AS400Factory;
import com.ibm.as400.access.*;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author rsalvatierra on 28/03/2016.
 */
public class DataQueueRunner implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataQueueRunner.class);

    private static final long MILISEGUNDOS_REINSTANCIAR_POR_ERROR = 20000L;
    private static final int SEGUNDOS_TIMEOUT = 30;
    private static final String DATAQUEUE = "DTAQ";
    private static final String FORMAT_IBM_284 = "IBM284";
    private static final String PREFIJO_FORMATO_CUERPO_AS400 = "PSR9";
    private final AS400Factory factory;
    private final String colaEntradaCompleta;
    private final String colaSalidaCompleta;
    private final DataQueueParam parametros;
    private DataQueueData dataQueueData;
    private ExecutorService executorService;
    private byte[] cabecera;
    private String cabeceraHex;
    private RecordFormat formatoCabecera, formatoCuerpo, formatoCuerpo2;
    private AS400 as400;
    private volatile Set<String> consultas = Collections.synchronizedSet(new HashSet<>());

    public DataQueueRunner(AS400Factory factory, DataQueueParam parametros) {
        this.executorService = Executors.newFixedThreadPool(3);
        this.parametros = parametros;
        this.factory = factory;
        String csc = "";
        this.colaEntradaCompleta = new QSYSObjectPathName(parametros.getLibreriaColas(), parametros.getColaEntrada(), DATAQUEUE).getPath();
        if (!StringUtils.isEmpty(parametros.getColaSalida())) {
            csc = new QSYSObjectPathName(parametros.getLibreriaColas(), parametros.getColaSalida(), DATAQUEUE).getPath();
        }
        this.colaSalidaCompleta = csc;
        //Conexion
        try {
            as400 = factory.getAS400(AS400.DATAQUEUE);
            //Formatos cabecera
            if (tieneCabecera()) {
                formatoCabecera = factory.buscarFormato(parametros.getLibreriaFormatos(), parametros.getClazz().getFormat().getFormatoCabeceraLectura());
            } else {
                formatoCuerpo = factory.buscarFormato(parametros.getLibreriaFormatos(), parametros.getClazz().getFormat().getFormatoCuerpoLectura());
            }
            //Formato Salida
            if (tieneRespuesta()) {
                dataQueueData = new DataQueueData(parametros.getClazz().getFormat().getFormatosEscritura(), getSalidaRecord(factory, parametros));
            }
        } catch (ConnectionPoolException e) {
            LOGGER.error("Error al conectarme con as/400. Deteniendo el lector " + parametros.getBeanName(), e);
        } catch (SistemaCerradoException | IOException e) {
            LOGGER.error("Error en los formatos as/400. Deteniendo el lector " + parametros.getBeanName(), e);
        } finally {
            factory.ret(as400, AS400.DATAQUEUE);
        }
    }

    @Override
    public void run() {
        DataQueueEntry dqData;
        int cantidaDeLecturas = 0;
        byte[] data;
        DataQueue dataQueue;
        Record cabeceraLeida = null, cuerpoLeido, cuerpoLeido2 = null;
        while (true) {
            try {
                as400 = factory.getAS400(AS400.DATAQUEUE);
                dataQueue = new DataQueue(as400, colaEntradaCompleta);
                if (cantidaDeLecturas == 0) {
                    if (parametros.isEliminarContenidoDataqueueLaPrimeraVez()) {
                        LOGGER.debug("Elimina contenido del DQ {} para {} (1ra vez)", colaEntradaCompleta, parametros.getBeanName());
                        dataQueue.clear();
                        cantidaDeLecturas++;
                    } else {
                        LOGGER.debug("No elimina contenido del DQ {} para {} (1ra vez)", colaEntradaCompleta, parametros.getBeanName());
                    }
                }
                LOGGER.debug("Inicio del lector dataqueue '{}' en el sistema AS/400 '{}'", colaEntradaCompleta, as400.getSystemName());
                //Leer dataqueue
                dqData = dataQueue.read(SEGUNDOS_TIMEOUT);
                if (dqData == null) {
                    LOGGER.debug("La lectura del dataqueue '{}' en el sistema AS/400 '{}' no ha devuelto " +
                                    "resultados en '{}' segundos, realizando la lectura nuevamente, componente='{}'",
                            colaEntradaCompleta, as400.getSystemName(), SEGUNDOS_TIMEOUT,
                            parametros.getBeanName());
                    continue;
                }
                //Lector
                data = dqData.getData();
                LOGGER.debug("Recibiendo la siguiente cadena (IBM284) '{}'", new String(data, FORMAT_IBM_284));
                if (data == null || data.length < 12) {
                    LOGGER.error("Largo insuficiente para la cadena '{}'", new String(data, FORMAT_IBM_284));
                    continue;
                }
                if (tieneCabecera()) {
                    //Cabecera
                    cabeceraLeida = getCabecera(data);
                    if (consultas.contains(cabeceraHex)) {
                        LOGGER.warn("La peticion para el codigo '{}' se ha ignorado porque se encuentra en " +
                                        "proceso de ejecucion. Aproximadamente es literalmente igual a ['{}'], " +
                                        " data completa=[{}]",
                                cabeceraHex, StringUtils.substring(new String(data, FORMAT_IBM_284), 0, 11), consultas);
                        continue;
                    }
                    consultas.add(cabeceraHex);
                    //Cuerpo
                    cuerpoLeido = getCuerpo(data, cabeceraLeida);
                    //Cuerpo2
                    cuerpoLeido2 = getCuerpo2(data, cabeceraLeida, cuerpoLeido);
                    //cargar bean
                } else {
                    //Cuerpo para SEGIP
                    cuerpoLeido = getCuerpo(data);
                    BigDecimal cabecera2 = (BigDecimal) cuerpoLeido.getField("SCUOPER");
                    cabecera = (new AS400Text(15)).toBytes(StringUtils.leftPad(cabecera2.toString(), 15, '0'));
                }
                parametros.getClazz().setValues(cabeceraLeida, cuerpoLeido, cuerpoLeido2, dataQueueData);
                if (tieneRespuesta()) {
                    DataQueueWriter dqw = new DataQueueWriter(dataQueueData,
                            colaSalidaCompleta, factory, cabecera, () -> {
                        try {
                            if (cabeceraHex != null) {
                                consultas.remove(cabeceraHex);
                            }
                        } catch (Exception e) {
                            LOGGER.warn("Transaccion terminada sin quitar el seguro bean=" + parametros.getBeanName(), e);
                        }
                    }, parametros.getClazz());

                    executorService.submit(dqw);
                }
                cantidaDeLecturas++;

                LOGGER.debug("Se ha iniciado el trabajo '{}'", cantidaDeLecturas);

                if (LOGGER.isDebugEnabled()) {
                    if (formatoCabecera != null && cabeceraLeida != null) {
                        for (FieldDescription fd : formatoCabecera.getFieldDescriptions()) {
                            LOGGER.debug("Cabecera: {} : <{}>", fd.getFieldName(), cabeceraLeida.getField(fd.getFieldName()));
                        }
                    }
                    if (formatoCuerpo != null && cuerpoLeido != null) {
                        for (FieldDescription fd : formatoCuerpo.getFieldDescriptions()) {
                            LOGGER.debug("Cuerpo 1: {} : <{}>", fd.getFieldName(), cuerpoLeido.getField(fd.getFieldName()));
                        }
                    }
                    if (formatoCuerpo2 != null && cuerpoLeido2 != null) {
                        for (FieldDescription fd : formatoCuerpo2.getFieldDescriptions()) {
                            LOGGER.debug("Cuerpo 2: {} : <{}>", fd.getFieldName(), cuerpoLeido2.getField(fd.getFieldName()));
                        }
                    }
                }

            } catch (InterruptedException ie) {
                LOGGER.info("Interrumpiendo lectura cola transacciones AS/400 '{}'. Se han lanzado {}" +
                        " ejecuciones de trabajos.", ie.toString(), cantidaDeLecturas);
                break;
            } catch (ClassCastException e) {
                LOGGER.error("Error al castear el tip de dato --> " + parametros.getClazz().getFormat().getCampoDondeSeDefineFormatoCuerpo(), e);
                break;
            } catch (Exception e) {
                LOGGER.error("Error en la cola " + parametros.getBeanName() + ": re-instanciando el sistema AS/400 en " +
                        (MILISEGUNDOS_REINSTANCIAR_POR_ERROR / 1000) + " segundos", e);
                try {
                    Thread.sleep(MILISEGUNDOS_REINSTANCIAR_POR_ERROR);
                } catch (InterruptedException ex) {
                    LOGGER.info("Interrumpiendo lectura cola transacciones AS/400 '{}'. Se han lanzado '{}'" +
                                    " ejecuciones de trabajos bean='{}'",
                            ex.toString(), cantidaDeLecturas, parametros.getBeanName());
                    break;
                }
            } finally {
                factory.ret(as400, AS400.DATAQUEUE);
            }
        }
        LOGGER.info("Terminando la ejecucion de la lectura de cola {} despues de  transacciones. El " +
                "numero de transacciones, {} puede no ser perfecto, sino variar por la cantidad de errores de" +
                " ejecucion.", colaEntradaCompleta, cantidaDeLecturas);
    }

    private LinkedHashMap<String, Record> getSalidaRecord(AS400Factory factory, DataQueueParam parametros) throws IOException, SistemaCerradoException {
        LinkedHashMap<String, Record> salidaRecord = new LinkedHashMap<>();
        for (String formatoEscritura : parametros.getClazz().getFormat().getFormatosEscritura()) {
            salidaRecord.put(formatoEscritura, factory.buscarFormato(parametros.getLibreriaFormatos(), formatoEscritura).getNewRecord());
        }
        return salidaRecord;
    }

    private Record getCuerpo2(byte[] data, Record cabecera, Record cuerpo) throws IOException, SistemaCerradoException {
        String formatoCuerpoNombre2;
        byte[] datacuerpo2;
        if (!"".equals(parametros.getClazz().getFormat().getCampoDondeSeDefineFormatoCuerpo2())) {
            // HAY CUERPO2
            formatoCuerpoNombre2 = StringUtils.leftPad(
                    cabecera.getField(parametros.getClazz().getFormat().getCampoDondeSeDefineFormatoCuerpo2()).toString(), 3, '0');
            formatoCuerpo2 = factory.buscarFormato(parametros.getLibreriaFormatos(), PREFIJO_FORMATO_CUERPO_AS400 + formatoCuerpoNombre2);
            int recordLength2 = cabecera.getRecordLength() + cuerpo.getRecordLength();
            LOGGER.debug("Tamanio de la cabecera + cuerpo1 {}", recordLength2);
            datacuerpo2 = ArrayUtils.subarray(data, recordLength2, data.length);
            return formatoCuerpo2.getNewRecord(datacuerpo2);
        }
        return null;
    }

    private Record getCuerpo(byte[] data, Record cabecera) throws IOException, SistemaCerradoException {
        byte[] datacuerpo;
        String formatoCuerpoNombre = StringUtils.leftPad(
                cabecera.getField(parametros.getClazz().getFormat().getCampoDondeSeDefineFormatoCuerpo()).toString(), 3, '0');
        formatoCuerpo = factory.buscarFormato(parametros.getLibreriaFormatos(), PREFIJO_FORMATO_CUERPO_AS400 + formatoCuerpoNombre);
        datacuerpo = ArrayUtils.subarray(data, cabecera.getRecordLength(), data.length);
        LOGGER.debug("Data cuerpo (IBM284) '{}', tamanto formato {} tamanya datacuerpo {}",
                new String(datacuerpo, FORMAT_IBM_284), formatoCuerpo.getNewRecord().getRecordLength(), datacuerpo.length);
        return formatoCuerpo.getNewRecord(datacuerpo);
    }

    private Record getCuerpo(byte[] data) throws IOException, SistemaCerradoException {
        return formatoCuerpo.getNewRecord(data);
    }


    private Record getCabecera(byte[] data) throws IOException, SistemaCerradoException {
        cabecera = new byte[11];
        System.arraycopy(data, 0, cabecera, 0, cabecera.length);
        cabeceraHex = String.valueOf(Hex.encodeHex(cabecera));
        //recordLength = cabeceraLeida.getRecordLength();
        //LOGGER.debug("Tamanio de la cabecera {}", recordLength);
        return formatoCabecera.getNewRecord(data);
    }

    private boolean tieneCabecera() {
        return (StringUtils.isNotEmpty(parametros.getClazz().getFormat().getFormatoCabeceraLectura()) && StringUtils.isNotBlank(parametros.getClazz().getFormat().getFormatoCabeceraLectura()));
    }

    private boolean tieneRespuesta() {
        return (parametros.getClazz().getFormat().getFormatosEscritura() != null && parametros.getClazz().getFormat().getFormatosEscritura().length > 0);
    }
}