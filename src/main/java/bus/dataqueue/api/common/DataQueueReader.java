package bus.dataqueue.api.common;

import bus.dataqueue.task.Task;

/**
 * @author rsalvatierra on 04/03/2016.
 */
public interface DataQueueReader {
    Task get();
}
