package bus.dataqueue.api.common;

import bus.dataqueue.api.extend.*;
import bus.dataqueue.api.simple.SimpleDataQueueDelegate;
import bus.dataqueue.api.simple.SimpleDataQueueMethod;
import bus.dataqueue.api.simple.SimpleParamValue;
import com.ibm.as400.access.Record;

import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author rsalvatierra on 07/03/2016.
 */
public class DataQueueClass {

    private Object target;
    private String methodName;
    private Method method;
    private Object[] parameters;
    private List<Object> ListArguments;
    private DataQueueFormat format;

    public DataQueueClass(Object o, String name) {
        Class<?> aClass;
        if (o != null && name != null) {
            aClass = o.getClass();
            if (aClass != null) {
                if (aClass.isAnnotationPresent(SimpleDataQueueDelegate.class)) {
                    this.Simple(o, name);
                } else if (aClass.isAnnotationPresent(DataQueueDelegate.class)) {
                    this.Full(o, name);
                } else {
                    throw new UnsupportedOperationException("Not supported yet.");
                }
            }
        }
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }

    public List<Object> getArgumentos() {
        return ListArguments;
    }

    public Object[] getParameters() {
        return parameters;
    }

    public void setValues(String dataString) {
        Object o;
        if (this.ListArguments != null && dataString != null) {
            this.parameters = new Object[this.ListArguments.size()];
            for (int i = 0; i < this.ListArguments.size(); i++) {
                o = this.ListArguments.get(i);
                if (o instanceof SimpleParamValue) {
                    this.getParameters()[i] = dataString;
                }
            }
        }
    }

    public void setValues(Record cabeceraLeida, Record cuerpoLeido, Record cuerpoLeido2, DataQueueData dataQueueData) throws UnsupportedEncodingException {
        Object o;
        if (this.ListArguments != null) {
            this.parameters = new Object[this.ListArguments.size()];
            for (int i = 0; i < this.ListArguments.size(); i++) {
                o = this.ListArguments.get(i);
                if (o instanceof ParamCabecera && cabeceraLeida != null) {
                    this.getParameters()[i] = cabeceraLeida.getField(((ParamCabecera) o).value());
                } else if (o instanceof ParamCuerpo) {
                    if (((ParamCuerpo) o).numCuerpo() == 0) {
                        if (cuerpoLeido != null) {
                            this.getParameters()[i] = cuerpoLeido.getField(((ParamCuerpo) o).value());
                        }
                    } else {
                        if (cuerpoLeido2 != null) {
                            this.getParameters()[i] = cuerpoLeido2.getField(((ParamCuerpo) o).value());
                        }
                    }
                } else {
                    this.getParameters()[i] = dataQueueData;
                }
            }
        }
    }

    private void Simple(Object o, String name) {
        //Inicio
        SimpleDataQueueDelegate sdqd;
        Annotation[][] parameterAnnotations;
        Annotation[] parameterAnnotation;
        Class<?> parameterType;
        Class<?>[] parameterTypes;

        //Proceso
        this.target = o;
        sdqd = this.target.getClass().getAnnotation(SimpleDataQueueDelegate.class);
        if (name == null || !name.equals(sdqd.value())) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        //Metodos
        for (Method methodAux : this.target.getClass().getMethods()) {
            if (methodAux.isAnnotationPresent(SimpleDataQueueMethod.class)) {
                setMethod(methodAux);
                setMethodName(methodAux.getName());
                //Parametros
                parameterTypes = this.method.getParameterTypes();
                parameterAnnotations = this.method.getParameterAnnotations();
                this.ListArguments = new ArrayList<>();
                for (int j = 0; j < parameterTypes.length; j++) {
                    parameterType = parameterTypes[j];
                    if (parameterType.equals(String.class)) {
                        parameterAnnotation = parameterAnnotations[j];
                        for (Annotation annotation : parameterAnnotation) {
                            if (annotation.annotationType().equals(SimpleParamValue.class)) {
                                this.ListArguments.add(annotation);
                            }
                        }
                    }
                }
            }
        }
    }

    private void Full(Object o, String name) {
        //Inicio
        DataQueueDelegate sdqd;
        Class<?> parameterType;
        Class<?>[] parameterTypes;
        Annotation[][] parameterAnnotations;
        Annotation[] parameterAnnotation;

        //Proceso
        target = o;
        sdqd = o.getClass().getAnnotation(DataQueueDelegate.class);
        if (name == null || !name.equals(sdqd.value())) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        //Metodos
        for (Method methodAux : o.getClass().getMethods()) {
            if (methodAux.isAnnotationPresent(DataQueueMethod.class)) {
                setMethod(methodAux);
                setMethodName(methodAux.getName());
                //Formatos
                setFormat(new DataQueueFormat(method.getAnnotation(DataQueueMethod.class)));
                //Parametros
                parameterTypes = this.method.getParameterTypes();
                parameterAnnotations = this.method.getParameterAnnotations();
                this.ListArguments = new ArrayList<>();
                for (int j = 0; j < parameterTypes.length; j++) {
                    parameterType = parameterTypes[j];
                    if (parameterType.equals(IDataQueueData.class)) {
                        this.ListArguments.add(IDataQueueData.class);
                        continue;
                    }
                    if (parameterType.equals(String.class) || parameterType.equals(BigDecimal.class)) {
                        parameterAnnotation = parameterAnnotations[j];
                        for (Annotation annotation : parameterAnnotation) {
                            if (annotation.annotationType().equals(ParamCabecera.class) ||
                                    annotation.annotationType().equals(ParamCuerpo.class)) {
                                this.ListArguments.add(annotation);
                            }
                        }
                    }
                }
            }
        }
    }

    public DataQueueFormat getFormat() {
        return format;
    }

    public void setFormat(DataQueueFormat format) {
        this.format = format;
    }
}
