package bus.dataqueue.api.common;

import bus.dataqueue.api.extend.DataQueueMethod;

/**
 * @author rsalvatierra on 07/03/2016.
 */
public class DataQueueFormat {

    private String formatoCabeceraLectura;
    private String formatoCuerpoLectura;
    private String campoDondeSeDefineFormatoCuerpo;
    private String campoDondeSeDefineFormatoCuerpo2;
    private String[] formatosEscritura;

    public DataQueueFormat(String formatoCabeceraLectura, String campoDondeSeDefineFormatoCuerpo, String campoDondeSeDefineFormatoCuerpo2, String[] formatosEscritura, String formatoCuerpoLectura) {
        setFormatoCabeceraLectura(formatoCabeceraLectura);
        setCampoDondeSeDefineFormatoCuerpo(campoDondeSeDefineFormatoCuerpo);
        setCampoDondeSeDefineFormatoCuerpo2(campoDondeSeDefineFormatoCuerpo2);
        setFormatosEscritura(formatosEscritura);
        setFormatoCuerpoLectura(formatoCuerpoLectura);
    }

    public DataQueueFormat(DataQueueMethod dataQueueMethod) {
        setFormatoCabeceraLectura(dataQueueMethod.formatoCabeceraLectura());
        setCampoDondeSeDefineFormatoCuerpo(dataQueueMethod.campoDondeSeDefineFormatoCuerpo());
        setCampoDondeSeDefineFormatoCuerpo2(dataQueueMethod.campoDondeSeDefineFormatoCuerpo2());
        setFormatosEscritura(dataQueueMethod.formatosEscritura());
        setFormatoCuerpoLectura(dataQueueMethod.formatoCuerpoLectura());
    }

    public String getFormatoCabeceraLectura() {
        return formatoCabeceraLectura;
    }

    public void setFormatoCabeceraLectura(String formatoCabeceraLectura) {
        this.formatoCabeceraLectura = formatoCabeceraLectura;
    }

    public String getCampoDondeSeDefineFormatoCuerpo() {
        return campoDondeSeDefineFormatoCuerpo;
    }

    public void setCampoDondeSeDefineFormatoCuerpo(String campoDondeSeDefineFormatoCuerpo) {
        this.campoDondeSeDefineFormatoCuerpo = campoDondeSeDefineFormatoCuerpo;
    }

    public String getCampoDondeSeDefineFormatoCuerpo2() {
        return campoDondeSeDefineFormatoCuerpo2;
    }

    public void setCampoDondeSeDefineFormatoCuerpo2(String campoDondeSeDefineFormatoCuerpo2) {
        this.campoDondeSeDefineFormatoCuerpo2 = campoDondeSeDefineFormatoCuerpo2;
    }

    public String[] getFormatosEscritura() {
        return formatosEscritura;
    }

    public void setFormatosEscritura(String[] formatosEscritura) {
        this.formatosEscritura = formatosEscritura;
    }

    public String getFormatoCuerpoLectura() {
        return formatoCuerpoLectura;
    }

    public void setFormatoCuerpoLectura(String formatoCuerpoLectura) {
        this.formatoCuerpoLectura = formatoCuerpoLectura;
    }
}
