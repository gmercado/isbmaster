package bus.dataqueue.api.common;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.RuntimeConfigurationType;
import org.castor.core.util.Assert;

import java.util.Properties;

/**
 * @author Marcelo Morales
 */
public abstract class DataQueueBase {

    public static final long UN_SEGUNDO = 1000L;
    public static final long MILISEGUNDOS_REINSTANCIAR_POR_ERROR = 20000L;
    public static final int SEGUNDOS_TIMEOUT = 290;
    public static final String DATAQUEUE = "DTAQ";
    public static final String FORMAT_IBM_284 = "IBM284";
    public volatile boolean started;
    public volatile Thread currentThread;
    private String libreriaColas;
    private String libreriaFormatos;
    private String colaEntrada;
    private String colaSalida;
    private boolean eliminarContenidoDataqueueLaPrimeraVez = true;
    private boolean primeraEjecucionEnDesarrollo = true;
    private String beanName;
    private DataQueueClass clazz;

    public DataQueueBase() {
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String name) {
        this.beanName = name;
    }

    public String getColaEntrada() {
        return colaEntrada;
    }

    public void setColaEntrada(String colaEntrada) {
        this.colaEntrada = colaEntrada;
    }

    public String getColaSalida() {
        return StringUtils.trimToEmpty(colaSalida);
    }

    public void setColaSalida(String colaSalida) {
        this.colaSalida = colaSalida;
    }

    public String getLibreriaColas() {
        return libreriaColas;
    }

    public void setLibreriaColas(String libreriaColas) {
        this.libreriaColas = libreriaColas;
    }

    public String getLibreriaFormatos() {
        return libreriaFormatos;
    }

    public void setLibreriaFormatos(String libreriaFormatos) {
        this.libreriaFormatos = libreriaFormatos;
    }

    public boolean isEliminarContenidoDataqueueLaPrimeraVez() {
        return eliminarContenidoDataqueueLaPrimeraVez;
    }

    public void setEliminarContenidoDataqueueLaPrimeraVez(boolean eliminarContenidoDataqueueLaPrimeraVez) {
        this.eliminarContenidoDataqueueLaPrimeraVez = eliminarContenidoDataqueueLaPrimeraVez;
    }

    public boolean isPrimeraEjecucionEnDesarrollo() {
        return primeraEjecucionEnDesarrollo;
    }

    public void setPrimeraEjecucionEnDesarrollo(boolean primeraEjecucionEnDesarrollo) {
        this.primeraEjecucionEnDesarrollo = primeraEjecucionEnDesarrollo;
    }

    public DataQueueClass getClazz() {
        return clazz;
    }

    public void setClazz(DataQueueClass clazz) {
        this.clazz = clazz;
    }

    @Deprecated
    protected boolean esDesarrollo() {
        Properties properties = System.getProperties();
        String valor;
        for (Object obj : properties.keySet()) {
            if (obj != null && obj.toString().equalsIgnoreCase("wicket.configuration")) {
                valor = properties.getProperty(obj.toString());
                if (RuntimeConfigurationType.DEVELOPMENT.name().equalsIgnoreCase(valor)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected void validarParametros() {
        //validar parametros
        Assert.notNull(getLibreriaColas(), "Necesito la propiedad 'libreria'");
        //Assert.notNull(getLibreriaFormatos(), "Necesito la propiedad 'libreriaFormatos'");
        Assert.notNull(getColaEntrada(), "Necesito la propiedad 'colaEntrada'");
        // si no tenemos cola de salida, entonces no escribimos en la cola
        //Assert.notNull(getColaSalida(), "Necesito la propiedad 'colaSalida'");
        //Assert.isTrue(currentThread == null || !currentThread.isAlive(), "El trabajo ya se encuentra en ejecucion");
        Assert.notNull(getClazz().getTarget(), "No se ha encontrado un target");
        Assert.notNull(getClazz().getMethodName(), "No se ha definido un metodo");
        //Assert.notNull(getClazz().getFormat().getCampoDondeSeDefineFormatoCuerpo(), "No se han encontrado el campo donde se");
        //Assert.notNull(getClazz().getFormat().getFormatoCabeceraLectura(), "nombreFormatoCabeceraLectura no encontrado");
        Assert.notNull(getClazz().getArgumentos(), "No se han encontrado argumentos en el metodo");
    }
}
