package bus.dataqueue.api.common;

import bus.dataqueue.api.extend.DataQueueDelegate;
import bus.dataqueue.api.simple.SimpleDataQueueDelegate;
import bus.dataqueue.task.Task;
import bus.dataqueue.task.TaskSnapshot;
import bus.plumbing.wicket.BisaWebApplication;
import com.google.inject.Binding;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Scopes;

import java.util.LinkedList;
import java.util.Map;

/**
 * @author by rsalvatierra on 24/04/2017.
 */
public class ListQueue {

    public static LinkedList<TaskSnapshot> getQueues() {
        Key key;
        Binding binding;
        LinkedList<TaskSnapshot> colas = new LinkedList<>();
        for (Map.Entry<Key<?>, Binding<?>> entrySet : BisaWebApplication.get().getAllBindings().entrySet()) {
            key = entrySet.getKey();
            binding = entrySet.getValue();
            if (Scopes.isSingleton(binding)) {
                if (key.getTypeLiteral().getRawType().getAnnotation(Queue.class) != null) {
                    Object o = BisaWebApplication.get().getInstance(Key.get(key.getTypeLiteral()));
                    if (o instanceof DataQueueReader) {
                        colas.add(new TaskSnapshot(((DataQueueReader) o).get()));
                    }
                }
            }
        }
        return colas;
    }

    public static Task getTaskQueues(String name) {
        Key key;
        Binding binding;
        for (Map.Entry<Key<?>, Binding<?>> entrySet : BisaWebApplication.get().getAllBindings().entrySet()) {
            key = entrySet.getKey();
            binding = entrySet.getValue();
            if (Scopes.isSingleton(binding)) {
                if (key.getTypeLiteral().getRawType().getAnnotation(Queue.class) != null) {
                    Object o = BisaWebApplication.get().getInstance(Key.get(key.getTypeLiteral()));
                    if (o instanceof DataQueueReader) {
                        SimpleDataQueueDelegate sdqd;
                        DataQueueDelegate dqd;
                        Class<?> aClass = o.getClass();
                        if (aClass.isAnnotationPresent(SimpleDataQueueDelegate.class)) {
                            sdqd = aClass.getAnnotation(SimpleDataQueueDelegate.class);
                            if (sdqd.value().equals(name)) {
                                return ((DataQueueReader) o).get();
                            }
                        }
                        if (aClass.isAnnotationPresent(DataQueueDelegate.class)) {
                            dqd = aClass.getAnnotation(DataQueueDelegate.class);
                            if (dqd.value().equals(name)) {
                                return ((DataQueueReader) o).get();
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public static void setTasks(Injector injector) throws Exception {
        Key key;
        Binding binding;
        Task task;
        for (Map.Entry<Key<?>, Binding<?>> entrySet : injector.getAllBindings().entrySet()) {
            key = entrySet.getKey();
            binding = entrySet.getValue();
            if (Scopes.isSingleton(binding)) {
                if (key.getTypeLiteral().getRawType().getAnnotation(Queue.class) != null) {
                    Object o = injector.getInstance(Key.get(key.getTypeLiteral()));
                    if (o instanceof DataQueueReader) {
                        Class<?> aClass = o.getClass();
                        if (aClass.isAnnotationPresent(SimpleDataQueueDelegate.class)) {
                            task = ((DataQueueReader) o).get();
                            if (!task.isCorriendo()) {
                                task.start();
                            }
                        }
                        if (aClass.isAnnotationPresent(DataQueueDelegate.class)) {
                            task = ((DataQueueReader) o).get();
                            if (!task.isCorriendo()) {
                                task.start();
                            }
                        }
                    }
                }
            }
        }
    }
}
