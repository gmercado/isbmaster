package bus.dataqueue.api.common;

/**
 * @author rsalvatierra on 28/03/2016.
 */
public class DataQueueParam {

    private String libreriaColas;
    private String libreriaFormatos;
    private String colaEntrada;
    private String colaSalida;
    private boolean eliminarContenidoDataqueueLaPrimeraVez = true;
    private String beanName;
    private DataQueueClass clazz;

    public String getLibreriaColas() {
        return libreriaColas;
    }

    public void setLibreriaColas(String libreriaColas) {
        this.libreriaColas = libreriaColas;
    }

    public String getLibreriaFormatos() {
        return libreriaFormatos;
    }

    public void setLibreriaFormatos(String libreriaFormatos) {
        this.libreriaFormatos = libreriaFormatos;
    }

    public String getColaEntrada() {
        return colaEntrada;
    }

    public void setColaEntrada(String colaEntrada) {
        this.colaEntrada = colaEntrada;
    }

    public String getColaSalida() {
        return colaSalida;
    }

    public void setColaSalida(String colaSalida) {
        this.colaSalida = colaSalida;
    }

    public boolean isEliminarContenidoDataqueueLaPrimeraVez() {
        return eliminarContenidoDataqueueLaPrimeraVez;
    }

    public void setEliminarContenidoDataqueueLaPrimeraVez(boolean eliminarContenidoDataqueueLaPrimeraVez) {
        this.eliminarContenidoDataqueueLaPrimeraVez = eliminarContenidoDataqueueLaPrimeraVez;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public DataQueueClass getClazz() {
        return clazz;
    }

    public void setClazz(DataQueueClass clazz) {
        this.clazz = clazz;
    }
}
