package bus.dataqueue.api.common;

/**
 * @author rsalvatierra on 04/03/2016.
 */
public enum DataQueueStatus {
    /**
     * Detenido
     */
    STOP("Detenido"),

    /**
     * En ejecucion
     */
    RUN("Corriendo");

    private final String descripcion;

    DataQueueStatus(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
