package bus.dataqueue.api.extend;

/**
 * @author Marcelo Morales
 */
public interface IDataQueueData {
    Object put(String formato, String campo, Object valor);
}
