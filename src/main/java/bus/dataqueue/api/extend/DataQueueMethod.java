package bus.dataqueue.api.extend;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Marcelo Morales
 */
@BindingAnnotation
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DataQueueMethod {

    String formatoCabeceraLectura() default "";

    String formatoCuerpoLectura() default "";

    String campoDondeSeDefineFormatoCuerpo() default "";

    String campoDondeSeDefineFormatoCuerpo2() default "";

    String[] formatosEscritura() default {};
}
