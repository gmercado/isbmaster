package bus.dataqueue.api.extend;

import bus.dataqueue.api.common.DataQueueBase;
import bus.dataqueue.api.common.DataQueueClass;
import bus.dataqueue.api.common.DataQueueParam;
import bus.dataqueue.api.common.DataQueueRunner;
import bus.dataqueue.task.Task;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.monitor.as400.AS400Factory;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author rsalvatierra on 07/03/2016.
 */
public class DataQueueTask extends DataQueueBase implements Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataQueueTask.class);
    private final MedioAmbiente medioAmbiente;
    private final AS400Factory factory;
    private DataQueueParam parametros;

    @Inject
    public DataQueueTask(MedioAmbiente medioAmbiente, AS400Factory as400) {
        this.medioAmbiente = medioAmbiente;
        this.factory = as400;
        this.parametros = new DataQueueParam();
    }

    @Override
    public boolean isCorriendo() {
        return currentThread != null && currentThread.isAlive();
    }

    @Override
    public void start() throws Exception {
        //verificar ambiente
        //if (verificarAmbiente()) return;
        //verificar parametros de entrada
        validarParametros();
        //setear parameters para ejecutor
        setParameters();
        //Running
        currentThread = new Thread(new DataQueueRunner(factory, parametros), "Ejecutor400-" + getBeanName() + "-" + getColaEntrada());
        currentThread.start();
        try {
            Thread.sleep(UN_SEGUNDO);
        } catch (InterruptedException ex) {
            LOGGER.warn("Proceso interrumpido al intentar correr el componente " + getBeanName(), ex);
        }
        if (!currentThread.isAlive()) {
            LOGGER.error("El hilo de lectura AS/400 '{}' no esta corriendo. Debe iniciarlo nuevamente de forma manual.",
                    getBeanName());
        }
    }

    @Override
    public void stop() throws Exception {
        Thread.yield();
        currentThread.interrupt();
        currentThread = null;
        LOGGER.info("Parando el lector para el metodo [{}] con nombre [{}] ", getClazz().getMethodName(), getBeanName());
    }

    @Override
    public String getFlujo() {
        StringBuilder sb = new StringBuilder();
        sb.append("De ").append(getLibreriaColas()).append("/").append(getColaEntrada());
        if (!StringUtils.isEmpty(getColaSalida())) {
            sb.append(" a ").append(getLibreriaColas()).append("/").append(getColaSalida());
        }
        return sb.toString();
    }

    @Override
    public void setClass(Object o) {
        setClazz(new DataQueueClass(o, getBeanName()));
    }

    @Override
    public void setBean(String bean) {
        setBeanName(medioAmbiente.getValorDe(bean + Variables.DATAQUEUE_BEAN, Variables.DATAQUEUE_BEAN_DEFAULT));
        setLibreriaColas(medioAmbiente.getValorDe(bean + Variables.DATAQUEUE_LIBRERIACOLAS, Variables.DATAQUEUE_LIBRERIACOLAS_DEFAULT));
        setLibreriaFormatos(medioAmbiente.getValorDe(bean + Variables.DATAQUEUE_LIBRERIAFORMATOS, Variables.DATAQUEUE_LIBRERIAFORMATOS_DEFAULT));
        setColaEntrada(medioAmbiente.getValorDe(bean + Variables.DATAQUEUE_COLAENTRADA, Variables.DATAQUEUE_COLAENTRADA_DEFAULT));
        setColaSalida(medioAmbiente.getValorDe(bean + Variables.DATAQUEUE_COLASALIDA, Variables.DATAQUEUE_COLASALIDA_DEFAULT));
        setEliminarContenidoDataqueueLaPrimeraVez(Boolean.valueOf(medioAmbiente.getValorDe(bean + Variables.DATAQUEUE_ELIMINARCONTENIDOPRIMERAVEZ, Variables.DATAQUEUE_ELIMINARCONTENIDOPRIMERAVEZ_DEFAULT)));
    }

    private void setParameters() {
        parametros.setBeanName(getBeanName());
        parametros.setLibreriaColas(getLibreriaColas());
        parametros.setLibreriaFormatos(getLibreriaFormatos());
        parametros.setColaEntrada(getColaEntrada());
        parametros.setColaSalida(getColaSalida());
        parametros.setEliminarContenidoDataqueueLaPrimeraVez(isEliminarContenidoDataqueueLaPrimeraVez());
        parametros.setClazz(getClazz());
    }

    @Deprecated
    private boolean verificarAmbiente() {
        //if (esDesarrollo()) {
        if (isPrimeraEjecucionEnDesarrollo()) {
            LOGGER.warn("NO iniciamos el lector para el metodo '{}' porque es la primera ejecucion en DESARROLLO.", getClazz().getMethodName());
            setPrimeraEjecucionEnDesarrollo(false);
            try {
                if (currentThread != null) {
                    stop();
                }
            } catch (Exception e) {
                LOGGER.error("NO pudimos parar el lector " + getClazz().getMethodName() + " para la primera ejecución en DESARROLLO.", e);
            }
            return true;
        }
        LOGGER.info("Iniciamos el lector para el metodo '{}'. Se asume que se inicio manualmente la ejecucion en DESARROLLO., ", getClazz().getMethodName());
        /*} else {
            LOGGER.info("Iniciamos el lector para el metodo '{}'. Ejecucion en PRODUCCION.", getClazz().getMethodName());
        }*/
        return false;
    }
}
