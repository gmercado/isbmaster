package bus.dataqueue.api.extend;

import com.ibm.as400.access.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Marcelo Morales
 */
public class DataQueueData implements IDataQueueData {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataQueueData.class);

    private final HashMap<String, HashMap<String, Object>> map;

    private final LinkedHashMap<String, Record> records;

    public DataQueueData(String[] formatos, LinkedHashMap<String, Record> records) {
        this.records = records;
        this.map = new HashMap<>();
        for (String formato : formatos) {
            this.map.put(formato, new HashMap<>());
        }
    }

    @Override
    public Object put(String formato, String campo, Object valor) {
        return map.get(formato).put(campo, valor);
    }

    byte[] obtenerResultado() throws IOException {
        LOGGER.debug("Escribiendo en la COLAAAA->[{}]", map.entrySet());

        for (Map.Entry<String, HashMap<String, Object>> entry : map.entrySet()) {
            final Record record = records.get(entry.getKey());
            final HashMap<String, Object> value = entry.getValue();
            LOGGER.info("Escribiendo Valores->[{}]", value.entrySet());
            for (Map.Entry<String, Object> llaveValor : value.entrySet()) {
                LOGGER.debug("Escribiendo {}='{}'", llaveValor.getKey(), llaveValor.getValue());
                record.setField(llaveValor.getKey(), llaveValor.getValue());
            }
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        for (Record record : records.values()) {
            record.getContents(baos);
        }

        return baos.toByteArray();
    }
}
