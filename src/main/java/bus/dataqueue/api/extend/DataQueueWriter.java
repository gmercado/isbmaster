package bus.dataqueue.api.extend;

import bus.dataqueue.api.common.DataQueueClass;
import bus.monitor.as400.AS400Factory;
import com.ibm.as400.access.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @author Marcelo Morales
 */
public class DataQueueWriter implements Runnable, Member, AnnotatedElement {

    private static final Logger LOG = LoggerFactory.getLogger(DataQueueWriter.class);
    public static final String IBM284 = "IBM284";
    public final Method method;
    public final Object target;
    public final Object[] parameters;
    private final DataQueueData salida;
    private final String colaSalidaCompleta;
    private final AS400Factory factory;
    private Runnable afterInvokeCallback;
    private byte[] key;

    public DataQueueWriter(DataQueueData dataQueueData, String colaSalidaCompleta, AS400Factory factory, byte[] cabecera, Runnable runnable, DataQueueClass clazz) {
        this.afterInvokeCallback = runnable;
        this.target = clazz.getTarget();
        this.method = clazz.getMethod();
        this.parameters = clazz.getParameters();
        this.key = Arrays.copyOf(cabecera, cabecera.length);
        this.salida = dataQueueData;
        this.colaSalidaCompleta = colaSalidaCompleta;
        this.factory = factory;
    }

    @Override
    public void run() {
        try {
            invoke();
            AS400 as400 = null;
            try {
                if (!StringUtils.isEmpty(this.colaSalidaCompleta)) {
                    as400 = factory.getAS400(AS400.DATAQUEUE);
                    byte[] byteData = salida.obtenerResultado();
                    String llaveStr = new String(key, IBM284);
                    KeyedDataQueue keyedDataQueue = new KeyedDataQueue(as400, colaSalidaCompleta);
                    LOG.debug("Intentando escribir en el data queue [{}] con la llave [{}]", colaSalidaCompleta, llaveStr);
                    keyedDataQueue.write(key, byteData);
                    LOG.info("Se ha escrito (en IBM284) con la llave {} la siguiente data: {}", llaveStr, new String(byteData, IBM284));
                } else {
                    LOG.debug("No escribimos porque no se especifico la COLA (en IBM284)");
                }
            } catch (UnsupportedEncodingException ex) {
                LOG.error("Encoding erroneo", ex);
            } catch (AS400SecurityException ex) {
                LOG.error("El usuario especificado no tiene privilegios", ex);
            } catch (ErrorCompletingRequestException ex) {
                LOG.error("Ocurrio un error en la conexion antes de que se termine la peticion", ex);
            } catch (IOException ex) {
                LOG.error("Hubo un error con la comunicacion con el sistema", ex);
            } catch (IllegalObjectTypeException ex) {
                LOG.error("El objeto en el sistema no es el tipo requerido", ex);
            } catch (InterruptedException ex) {
                LOG.error("El hilo de trabajo fue interrumpido", ex);
            } catch (ObjectDoesNotExistException ex) {
                LOG.error("El Keyed Data Queue " + colaSalidaCompleta + " no existe!!", ex);
            } finally {
                factory.ret(as400, AS400.DATAQUEUE);
            }
        } catch (Throwable e) {
            LOG.error("Ha ocurrido un error inesperado", e);
        } finally {
            afterInvokeCallback.run();
        }
    }

    public Object invoke() throws InvocationTargetException, IllegalAccessException {
        return method.invoke(target, parameters);
    }

    @Override
    public Class<?> getDeclaringClass() {
        return method.getDeclaringClass();
    }

    @Override
    public String getName() {
        return method.getName();
    }

    @Override
    public int getModifiers() {
        return method.getModifiers();
    }

    @Override
    public boolean isSynthetic() {
        return method.isSynthetic();
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass) {
        return true;
    }

    @Override
    public <T extends Annotation> T getAnnotation(Class<T> annotationClass) {
        return method.getAnnotation(annotationClass);
    }

    @Override
    public Annotation[] getAnnotations() {
        return method.getAnnotations();
    }

    @Override
    public Annotation[] getDeclaredAnnotations() {
        return method.getDeclaredAnnotations();
    }
}
