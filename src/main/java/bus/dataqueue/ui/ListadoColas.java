package bus.dataqueue.ui;

import bus.dataqueue.api.common.DataQueueStatus;
import bus.dataqueue.api.common.ListQueue;
import bus.dataqueue.task.TaskSnapshot;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.LinkPropertyColumn;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.core.util.lang.PropertyResolver;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author rsalvatierra on 02/03/2016.
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Menu(value = "Lectores Colas", subMenu = SubMenu.ADMINISTRACION)
public class ListadoColas extends BisaWebPage {

    public ListadoColas() {
        LinkedList<IColumn<TaskSnapshot, String>> iColumns = new LinkedList<>();
        iColumns.add(new LinkPropertyColumn<TaskSnapshot>(Model.of("Nombre"), "beanName") {
            @Override
            protected void onClick(TaskSnapshot object) {
                setResponsePage(ColaDetalle.class, new PageParameters().add("beanName", object.getBeanName()));
            }
        });
        iColumns.add(new PropertyColumn<>(Model.of("Flujo"), "flujo"));
        iColumns.add(new PropertyColumn<TaskSnapshot, String>(Model.of("Estado"), "estado.descripcion") {
            @Override
            public void populateItem(Item<ICellPopulator<TaskSnapshot>> item, String componentId, final IModel<TaskSnapshot> rowModel) {
                item.add(new Label(componentId, getDataModel(rowModel)).
                        add(new AttributeModifier("class", new AbstractReadOnlyModel<String>() {
                            @Override
                            public String getObject() {
                                DataQueueStatus estado = rowModel.getObject().getEstado();
                                switch (estado) {
                                    case STOP:
                                        return "muted";
                                    case RUN:
                                        return "btn btn-small disabled btn-danger";
                                }
                                return "";
                            }
                        })));
            }
        });
        iColumns.add(new PropertyColumn<>(Model.of("Accion Inicial"), "inicial"));
        SortableDataProvider<TaskSnapshot, String> sortableDataProvider = new ColasDataProvider();
        add(new AjaxFallbackDefaultDataTable<>("LectoresDQ", iColumns, sortableDataProvider, 5));
    }

    private class ColasDataProvider extends SortableDataProvider<TaskSnapshot, String> {

        private final LinkedList<TaskSnapshot> colas;

        {
            colas = ListQueue.getQueues();
        }

        @Override
        public Iterator<? extends TaskSnapshot> iterator(long first, long count) {
            SortParam sort = getSort();
            if (sort != null) {
                Collections.sort(colas, (o1, o2) -> {
                    Integer value1 = ((Number) PropertyResolver.getValue(getSort().getProperty(), o1)).intValue();
                    Integer value2 = ((Number) PropertyResolver.getValue(getSort().getProperty(), o2)).intValue();
                    if (getSort().isAscending()) {
                        return value1.compareTo(value2);
                    } else {
                        return value2.compareTo(value1);
                    }
                });
            }
            return colas.subList((int) first, (int) (first + count)).iterator();
        }

        @Override
        public long size() {
            return colas.size();
        }

        @Override
        public IModel<TaskSnapshot> model(TaskSnapshot object) {
            return Model.of(object);
        }
    }
}
