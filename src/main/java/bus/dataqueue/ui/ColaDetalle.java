package bus.dataqueue.ui;

import bus.dataqueue.api.common.ListQueue;
import bus.dataqueue.task.Task;
import bus.dataqueue.task.TaskSnapshot;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * @author rsalvatierra on 02/03/2016.
 */
@AuthorizeInstantiation(RolesBisa.ADMIN)
@Titulo("Detalle del lector")
public class ColaDetalle extends BisaWebPage {

    private transient Task dqp;

    public ColaDetalle(PageParameters parameters) {
        super(parameters);

        String name = getPageParameters().get("beanName").toString();
        //Model
        setDefaultModel(new CompoundPropertyModel<>(new LoadableDetachableModel<TaskSnapshot>() {
            @Override
            protected TaskSnapshot load() {
                dqp = ListQueue.getTaskQueues(name);
                return new TaskSnapshot(dqp);
            }
        }));

        //Form
        Form<TaskSnapshot> form = new Form<>("cambiarestado");
        add(form);

        form.add(new Label("beanName"));
        form.add(new Label("estado.descripcion"));
        form.add(new Label("flujo"));
        form.add(new Label("inicial"));

        form.add(new Button("volver") {
            @Override
            public void onSubmit() {
                setResponsePage(ListadoColas.class);
            }
        });

        form.add(new Button("iniciar") {
            @Override
            public void onSubmit() {
                TaskSnapshot cola = (TaskSnapshot) ColaDetalle.this.getDefaultModelObject();
                if (!cola.isCorriendo()) {
                    try {
                        dqp.start();
                        getSession().info("Iniciado");
                    } catch (Exception e) {
                        getSession().error("Error al iniciar " + e);
                    }
                }
            }
        });

        form.add(new Button("detener") {
            @Override
            public void onSubmit() {
                TaskSnapshot cola = (TaskSnapshot) ColaDetalle.this.getDefaultModelObject();
                if (cola.isCorriendo()) {
                    try {
                        dqp.stop();
                        getSession().info("Trabajo detenido");
                    } catch (Exception ex) {
                        getSession().error("Error inesperado" + ex);
                    }
                }
            }
        });
    }

}
