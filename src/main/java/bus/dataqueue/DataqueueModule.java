package bus.dataqueue;

import bus.dataqueue.api.common.DataQueueOneWay;
import bus.dataqueue.api.common.DataQueueTwoWay;
import bus.dataqueue.api.extend.DataQueueTask;
import bus.dataqueue.api.simple.SimpleDataQueueTask;
import bus.dataqueue.task.Task;
import bus.dataqueue.task.TaskJob;
import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.bus.servicios.segip.queue.LectorSegip;
import com.google.inject.Singleton;
import com.google.inject.name.Names;

/**
 * @author rsalvatierra on 05/02/2016.
 */
public class DataqueueModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(Task.class).annotatedWith(DataQueueOneWay.class).to(SimpleDataQueueTask.class);
        bind(Task.class).annotatedWith(DataQueueTwoWay.class).to(DataQueueTask.class);
        bind(LectorSegip.class).in(Singleton.class);
        bindConstant().annotatedWith(Names.named("SEGIP")).to("segip");
        bindSched("VerificarLectorColas", "DATAQUEUE", "0 0 8-16 ? * MON-FRI", TaskJob.class);
        bindUI("bus/dataqueue/ui");
    }

}
