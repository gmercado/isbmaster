package bus.dataqueue.task;

import bus.dataqueue.api.common.ListQueue;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author by rsalvatierra on 25/04/2017.
 */
public class TaskJob implements Job {

    private final Injector injector;

    @Inject
    public TaskJob(Injector injector) {
        this.injector = injector;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            ListQueue.setTasks(injector);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
