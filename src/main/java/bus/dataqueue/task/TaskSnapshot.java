package bus.dataqueue.task;

import bus.dataqueue.api.common.DataQueueBase;
import bus.dataqueue.api.common.DataQueueStatus;

import java.io.Serializable;

/**
 * @author Marcelo Morales
 */
public class TaskSnapshot implements Serializable {

    private boolean corriendo;

    private String flujo;

    private String inicial;

    private String beanName;

    public TaskSnapshot(Task task) {
        this.corriendo = task.isCorriendo();
        this.flujo = task.getFlujo();
        this.beanName = task.getBeanName();
        if (task instanceof DataQueueBase && ((DataQueueBase) task).isEliminarContenidoDataqueueLaPrimeraVez()) {
            inicial = "Elimina el contenido de la cola de entrada";
        } else {
            inicial = "Ninguno";
        }
    }

    public boolean isCorriendo() {
        return corriendo;
    }

    public DataQueueStatus getEstado() {
        if (corriendo) {
            return DataQueueStatus.RUN;
        }
        return DataQueueStatus.STOP;
    }

    public String getFlujo() {
        return flujo;
    }

    public String getInicial() {
        return inicial;
    }

    public String getBeanName() {
        return beanName;
    }

}
