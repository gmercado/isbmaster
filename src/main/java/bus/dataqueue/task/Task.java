package bus.dataqueue.task;

/**
 * @author Marcelo Morales
 */
public interface Task {

    boolean isCorriendo();

    void start() throws Exception;

    void stop() throws Exception;

    String getFlujo();

    String getBeanName();

    void setClass(Object o);

    void setBean(String bean);
}
