package bus.interfaces.ticketing.dao;

import bus.env.api.MedioAmbiente;
import bus.interfaces.ticketing.entities.AgenciaTicket;
import com.google.inject.Inject;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.openlogics.gears.jdbc.DataStoreFactory;
import org.openlogics.gears.jdbc.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static bus.env.api.Variables.*;

/**
 * @author by rsalvatierra on 17/01/2018.
 */
public class TicketDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(TicketDao.class);

    private static final String COM_IBM_DB2_JCC_DB2_DRIVER = "com.ibm.db2.jcc.DB2Driver";
    private static final String JDBC_DB2 = "jdbc:db2:";
    private final MedioAmbiente medioAmbiente;

    @Inject
    public TicketDao(MedioAmbiente medioAmbiente) {
        this.medioAmbiente = medioAmbiente;
    }

    static {
        try {
            Class.forName(COM_IBM_DB2_JCC_DB2_DRIVER);
        } catch (ClassNotFoundException e) {
            LOGGER.error("error al cargar DB2", e);
        }
    }

    public int getCantidadTickets(AgenciaTicket agenciaTicket, Date fecha) {
        int cantidadTicket = 0;
        org.openlogics.gears.jdbc.ObjectDataStore ds;
        try {
            String URI = medioAmbiente.getValorDe(CONEXION_DB2_TICKETING_LOCAL, CONEXION_DB2_TICKETING_LOCAL_DEFAULT);
            String user = medioAmbiente.getValorDe(USER_CONEXION_DB2_TICKETING_LOCAL, USER_CONEXION_DB2_TICKETING_LOCAL_DEFAULT);
            String pass = medioAmbiente.getValorDe(PASS_CONEXION_DB2_TICKETING_LOCAL, PASS_CONEXION_DB2_TICKETING_LOCAL_DEFAULT);

            URI = URI.replaceAll("LOCAL", agenciaTicket.getServDB());
            String estados = medioAmbiente.getValorDe(ESTADOS_TICKETING_LOCAL, ESTADOS_TICKETING_LOCAL_DEFAULT);
            ds = DataStoreFactory.createObjectDataStore(DriverManager.getConnection(JDBC_DB2 + URI, user, pass));
            ds.setAutoClose(true);
            List<Map<String, Object>> map = ds.select(Query.of("SELECT count(1) cantidad FROM QS.QTICKET WHERE date(ts1)=? AND estado IN (" + estados + ")", fecha), new MapListHandler());
            for (Map<String, Object> stringObjectMap : map) {
                cantidadTicket = (int) stringObjectMap.get("cantidad");
            }
        } catch (SQLException ex) {
            LOGGER.error("Error al conectarse al DB2 ticketing", ex);
        }
        return cantidadTicket;
    }
}

