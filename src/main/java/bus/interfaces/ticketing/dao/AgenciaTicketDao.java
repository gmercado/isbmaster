package bus.interfaces.ticketing.dao;

import bus.env.api.MedioAmbiente;
import bus.interfaces.ticketing.entities.AgenciaTicket;
import com.google.inject.Inject;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.openlogics.gears.jdbc.DataStoreFactory;
import org.openlogics.gears.jdbc.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static bus.env.api.Variables.*;

/**
 * @author by rsalvatierra on 17/01/2018.
 */
public class AgenciaTicketDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(AgenciaTicketDao.class);

    private static final String COM_IBM_DB2_JCC_DB2_DRIVER = "com.ibm.db2.jcc.DB2Driver";
    private static final String JDBC_DB2 = "jdbc:db2:";
    private final MedioAmbiente medioAmbiente;

    @Inject
    public AgenciaTicketDao(MedioAmbiente medioAmbiente) {
        this.medioAmbiente = medioAmbiente;
    }

    static {
        try {
            Class.forName(COM_IBM_DB2_JCC_DB2_DRIVER);
        } catch (ClassNotFoundException e) {
            LOGGER.error("error al cargar DB2", e);
        }
    }

    public AgenciaTicket getURLAgencia(String codigo) {
        AgenciaTicket agenciaTicket = null;
        org.openlogics.gears.jdbc.ObjectDataStore ds;
        try {

            String URI = medioAmbiente.getValorDe(CONEXION_DB2_TICKETING, CONEXION_DB2_TICKETING_DEFAULT);
            String user = medioAmbiente.getValorDe(USER_CONEXION_DB2_TICKETING, USER_CONEXION_DB2_TICKETING_DEFAULT);
            String pass = medioAmbiente.getValorDe(PASS_CONEXION_DB2_TICKETING, PASS_CONEXION_DB2_TICKETING_DEFAULT);

            ds = DataStoreFactory.createObjectDataStore(DriverManager.getConnection(JDBC_DB2 + URI, user, pass));
            ds.setAutoClose(true);
            List<Map<String, Object>> map = ds.select(Query.of("SELECT * FROM QS.QAGENCIA where AGID=?", codigo), new MapListHandler());
            for (Map<String, Object> stringObjectMap : map) {
                agenciaTicket = new AgenciaTicket();
                agenciaTicket.setId((String) stringObjectMap.get("AGID"));
                agenciaTicket.setDesc((String) stringObjectMap.get("AGDESC"));
                agenciaTicket.setServ((String) stringObjectMap.get("AGSERV"));
                agenciaTicket.setServDB((String) stringObjectMap.get("AGSERVDB"));
            }
            LOGGER.debug("conexion DB2 Ticketing");
        } catch (SQLException ex) {
            LOGGER.error("Error al conectarse al DB2 ticketing", ex);
        }
        return agenciaTicket;
    }

}
