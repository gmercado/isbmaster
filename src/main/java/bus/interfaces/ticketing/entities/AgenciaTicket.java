package bus.interfaces.ticketing.entities;

/**
 * @author by rsalvatierra on 17/01/2018.
 */
public class AgenciaTicket {
    private String id;
    private String desc;
    private String serv;
    private String servDB;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getServ() {
        return serv;
    }

    public void setServ(String serv) {
        this.serv = serv;
    }

    public String getServDB() {
        return servDB;
    }

    public void setServDB(String servDB) {
        this.servDB = servDB;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", desc='" + desc + '\'' +
                ", serv='" + serv + '\'' +
                ", servDB='" + servDB + '\'' +
                '}';
    }
}
