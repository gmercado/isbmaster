package bus.interfaces.as400.dao;

import bus.database.dao.DaoImpl;
import bus.interfaces.as400.entities.ClienteCrediticio;
import bus.interfaces.as400.entities.ClienteCrediticioPK;
import com.google.inject.Inject;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.List;

public class ClienteCrediticioDao extends DaoImpl<ClienteCrediticio, ClienteCrediticioPK> {
    
    private static final Logger LOG = LoggerFactory.getLogger(ClienteCrediticioDao.class);

    @Inject
    protected ClienteCrediticioDao(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }
 
    public List<ClienteCrediticio> getListaCreditosByNumeroCliente(final BigDecimal nroCliente){
         if( nroCliente == null){ 
            LOG.error("Error no se puede consultar un numero de cliente nulo en la BD.");
            return null;
        }
        return doWithTransaction(new WithTransaction<List<ClienteCrediticio>>(){
            @Override
            public List<ClienteCrediticio> call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
                OpenJPACriteriaQuery<ClienteCrediticio> query = cb.createQuery(ClienteCrediticio.class);
                Root<ClienteCrediticio> root = query.from(ClienteCrediticio.class);
                
                Predicate p1 = cb.equal(root.<String>get("clienteCrediticioPK.numeroCliente"), nroCliente);
                query.where(p1); 
                LOG.debug(" LA CONSULTA ES: "+query.toCQL());
                
                try {
                    return entityManager.createQuery(query).getResultList();
                } catch (NoResultException e) {
                    LOG.info("El numero de cliente [{}] no se encuentra registrado como Cliente Crediticio. ", nroCliente, e);
                    return null;
                } catch (Exception e) {
                    LOG.error("Error: No se puede determinar si el numero de cliente [{}] esta o no registrado como Cliente Crediticio. ", nroCliente, e);
                    return null;
                }
            }
        }); 
    }
    
    public boolean esClienteCrediticio(BigDecimal nroCliente){
        if( nroCliente == null){ 
            LOG.error("Error no se puede consultar un numero de cliente nulo en la BD.");
            return false;
        }
        
        List<ClienteCrediticio> lista = getListaCreditosByNumeroCliente(nroCliente);
        if(lista != null && lista.size()>0){
            return true;
        }
        return false;
    }
    
}
