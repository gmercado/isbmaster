package bus.interfaces.as400.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.interfaces.as400.entities.ClienteCuenta;
import bus.interfaces.as400.entities.ClienteCuentaDetalle;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author by josanchez on 03/08/2016.
 * @author by rsalvatierra on 03/11/2016.
 */
public class ClienteCuentaDetalleDao extends DaoImpl<ClienteCuentaDetalle, ClienteCuentaDetalle> {

    private static final Logger LOG = LoggerFactory.getLogger(ClienteCuenta.class);

    @Inject
    protected ClienteCuentaDetalleDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public ClienteCuentaDetalle getByCuentaCliente(final Long cuenta, final String codigoCliente) {
        if (cuenta == null) {
            LOG.error("Error no se puede consultar un n\u00famero de cuenta vacio.");
            return null;
        }
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<ClienteCuentaDetalle> q = cb.createQuery(ClienteCuentaDetalle.class);
                    Root<ClienteCuentaDetalle> p = q.from(ClienteCuentaDetalle.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("codigoCliente"), codigoCliente));
                    predicatesAnd.add(cb.equal(p.get("cuenta"), cuenta));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<ClienteCuentaDetalle> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else {
                        return null;
                    }
                }
        );
    }

    public List<String> getByCliente(final String codigoCliente) {
        List<String> numeroCuentas = new ArrayList<>();
        if (codigoCliente == null) {
            LOG.error("Error no se puede consultar un n\u00famero de cliente vacio.");
            return null;
        }
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<ClienteCuentaDetalle> q = cb.createQuery(ClienteCuentaDetalle.class);
                    Root<ClienteCuentaDetalle> p = q.from(ClienteCuentaDetalle.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("codigoCliente"), codigoCliente));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<ClienteCuentaDetalle> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() > 0) {
                        numeroCuentas.addAll(query.getResultList().stream().map(clienteCuenta -> String.valueOf(clienteCuenta.getCuenta())).collect(Collectors.toList()));
                        return numeroCuentas;
                    } else {
                        return null;
                    }
                }
        );
    }

    public ClienteCuentaDetalle getByCuenta(final Long cuenta) {
        if (cuenta == null) {
            LOG.error("Error no se puede consultar un n\u00famero de cuenta vacio.");
            return null;
        }
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<ClienteCuentaDetalle> q = cb.createQuery(ClienteCuentaDetalle.class);
                    Root<ClienteCuentaDetalle> p = q.from(ClienteCuentaDetalle.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("cuenta"), cuenta));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<ClienteCuentaDetalle> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else {
                        return null;
                    }
                }
        );
    }
}
