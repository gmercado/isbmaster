package bus.interfaces.as400.dao;

import bus.database.dao.DaoImplBase;
import bus.database.model.BasePrincipal;
import bus.interfaces.as400.entities.TarjetaDebitoAutorizacion;
import bus.interfaces.tipos.EstadoTarjetaDebito;
import bus.interfaces.tipos.Pais;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.OpenJPAQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author by rsalvatierra on 28/09/2017.
 */
public class TarjetaDebitoDao {

    private static final Logger LOG = LoggerFactory.getLogger(TarjetaDebitoDao.class);
    private final DaoImplBase<Void> daoAS400;

    @Inject
    public TarjetaDebitoDao(@BasePrincipal EntityManagerFactory entityManagerFactoryAS400) {
        this.daoAS400 = new DaoImplBase<>(entityManagerFactoryAS400);
    }

    /**
     * Recuperacion de listado de paises
     */
    @SuppressWarnings("unchecked")
    public List<Pais> getPaises() {
        return daoAS400.doWithTransaction((entityManager, t) -> {
            String sql = "SELECT ATPAI6 AS codigoPais, ATDES6 AS nombrePais FROM ATP01006 WHERE ATEST6 = 'A' ORDER BY codigoPais DESC";
            OpenJPAQuery<Pais> sqlPaises = entityManager.createNativeQuery(sql, Pais.class);
            List<Pais> paisList = null;
            try {
                paisList = sqlPaises.getResultList();
            } catch (NoResultException e) {
                LOG.warn("NO existe el pais ");
            }
            return paisList;
        });
    }

    @SuppressWarnings("unchecked")
    public List<TarjetaDebitoAutorizacion> getActivaciones(final String numeroCliente) {
        LOG.debug("Obteniendo listado de tarjetas de debito habilitadas para el cliente {}", numeroCliente);
        Integer fechaActual = TarjetaDebitoAutorizacion.formatoFecha(new Date(System.currentTimeMillis()));
        String sql = "SELECT ATNTA5 as nroTarjeta, ATPA15 as codPais, ATATM5 as trnATM,ATPOS5 as trnPOS, ATIME5 as monto,  ATCLI5 as codCliente, ATEST5 as estado, ATCOD5 as codRespuesta, ATFEA5 as fechaActivacion, ATFEE5 as fechaExpiracion, ATHOR5 as horaAlta  " +
                ", ATP01006.atdes6 as pais  FROM ATP01005 LEFT JOIN ATP01006 ON ATPA15 = ATPAI6 " +
                " WHERE ATEST5='A' AND ATFEE5 >= ? AND ATCLI5 IN ? ORDER BY estado";
        return daoAS400.doWithTransaction((entityManager, t) -> {
            OpenJPAQuery<TarjetaDebitoAutorizacion> tarjetasSQL = entityManager.createNativeQuery(sql, TarjetaDebitoAutorizacion.class);
            List<TarjetaDebitoAutorizacion> tarjetaDebitoAutorizacions = null;
            try {
                tarjetasSQL.setParameters(1, fechaActual.toString());
                tarjetasSQL.setParameters(2, numeroCliente);
                tarjetaDebitoAutorizacions = tarjetasSQL.getResultList();
            } catch (NoResultException e) {
                LOG.warn("NO existe el pais ");
            }
            return tarjetaDebitoAutorizacions;
        });
    }

    @SuppressWarnings("unchecked")
    public List<TarjetaDebitoAutorizacion> getActivacionesCompleto(String numeroCliente, List<String> tarjetasActivas) {
        if (tarjetasActivas.size() == 0) {
            return new ArrayList<>();
        }
        LOG.debug("Obteniendo listado de tarjetas de debito habilitadas para el cliente {}", numeroCliente);
        Integer fechaActual = TarjetaDebitoAutorizacion.formatoFecha(new Date(System.currentTimeMillis()));
        if (StringUtils.isNotEmpty(numeroCliente) && numeroCliente.length() > 6) {
            numeroCliente = numeroCliente.substring(4);
        }
        Map<String, List<String>> parametros = new HashMap<>();
        parametros.put("fechaActual", Collections.singletonList(fechaActual.toString()));
        parametros.put("nroCliente", Collections.singletonList(numeroCliente));
        parametros.put("tarjetas", tarjetasActivas);
        List<TarjetaDebitoAutorizacion> lista = daoAS400.doWithTransaction((entityManager, t) -> {
            OpenJPAQuery<TarjetaDebitoAutorizacion> tarjetasSQL = entityManager.createNativeQuery(getSQL_TransaccionesExterior(), TarjetaDebitoAutorizacion.class);
            List<TarjetaDebitoAutorizacion> tarjetaDebitoAutorizacions = null;
            try {
                tarjetasSQL.setParameters(parametros);
                tarjetaDebitoAutorizacions = tarjetasSQL.getResultList();
            } catch (NoResultException e) {
                LOG.warn("NO existe el pais ");
            }
            return tarjetaDebitoAutorizacions;
        });

        List<TarjetaDebitoAutorizacion> listaSinRegla = new ArrayList<>();
        List<String> existentes = lista.stream().map(obj -> obj.getNroTarjeta().trim()).collect(Collectors.toList());

        tarjetasActivas.removeAll(existentes);
        for (String nroTarjeta : tarjetasActivas) {
            // Definir registro por defecto
            TarjetaDebitoAutorizacion tarjeta = new TarjetaDebitoAutorizacion(nroTarjeta);
            tarjeta.setEstado(EstadoTarjetaDebito.ACTIVO.getEstado());
            tarjeta.setCodCliente(numeroCliente);
            tarjeta.setTipoTransaccion(TarjetaDebitoAutorizacion.TRANS_EXTERIOR);
            tarjeta.setFechaActivacion(TarjetaDebitoAutorizacion.FECHA_DEFECTO);
            tarjeta.setFechaExpiracion(TarjetaDebitoAutorizacion.FECHA_DEFECTO);
            tarjeta.setTrnATM(TarjetaDebitoAutorizacion.TRANS_ACTIVA);
            tarjeta.setTrnPOS(TarjetaDebitoAutorizacion.TRANS_ACTIVA);
            // Cargado de limites por defecto
            tarjeta.setMonto(TarjetaDebitoAutorizacion.MONTO_POR_DEFECTO);

            listaSinRegla.add(tarjeta);
        }
        lista.addAll(listaSinRegla);
        return lista;
    }


    /**
     * Registro de autorización de operaciones en el exterior
     */
    @SuppressWarnings("unchecked")
    public boolean guardarAutorizacion(TarjetaDebitoAutorizacion tarjeta, String canal) {
        try {
            int nro = daoAS400.doWithTransaction((entityManager, t) -> {
                OpenJPAQuery<Integer> sqlcantidad = entityManager.createNativeQuery(getSQL_ValidarRegistro(), Integer.class);
                sqlcantidad.setParameter(1, tarjeta.getNroTarjeta());
                sqlcantidad.setParameter(2, tarjeta.getCodPais());
                sqlcantidad.setParameter(3, tarjeta.getTipoTransaccion());
                return sqlcantidad.getSingleResult();
            });
            tarjeta.setFechaAlta(Integer.parseInt(TarjetaDebitoAutorizacion.FECHA_FORMATO.format(new Date(System.currentTimeMillis()))));
            tarjeta.setHoraAlta(Integer.parseInt(TarjetaDebitoAutorizacion.HORA_FORMATO.format(new Date(System.currentTimeMillis()))));
            tarjeta.setFechaSistema(new Date(System.currentTimeMillis()));
            tarjeta.setPrograma(canal);
            tarjeta.setDispositivo(canal);
            tarjeta.setMonto(0d);
            tarjeta.setEvento("W");
            if (nro == 0) {
                LOG.debug("Creando nuevo registro");
                setTarjeta(getSQL_Insert(), 1, tarjeta);
            } else {
                LOG.debug("actualizando registro");
                tarjeta.setEvento("U");
                setTarjeta(getSQL_Update(), 3, tarjeta);
            }
            setTarjeta(getSQL_InsertHist(), 2, tarjeta);
            return true;
        } catch (Exception e) {
            LOG.error("No se pudo guardar autorización de transacciones exterior de tarjeta de debito=[{}]", tarjeta.getNroTarjeta(), e);
            return false;
        }
    }

    private void setTarjeta(String sql, int tipo, TarjetaDebitoAutorizacion tarjeta) {
        daoAS400.doWithTransaction((entityManager, t) -> {
            OpenJPAQuery sqlcantidad = entityManager.createNativeQuery(sql);
            switch (tipo) {
                case 1:
                    sqlcantidad.setParameter(1, tarjeta.getNroTarjeta());
                    sqlcantidad.setParameter(2, tarjeta.getCodPais());
                    sqlcantidad.setParameter(3, tarjeta.getTrnATM());
                    sqlcantidad.setParameter(4, tarjeta.getTrnPOS());
                    sqlcantidad.setParameter(5, tarjeta.getCodCliente());
                    sqlcantidad.setParameter(6, tarjeta.getEstado());
                    sqlcantidad.setParameter(7, tarjeta.getCodRespuesta());
                    sqlcantidad.setParameter(8, tarjeta.getFechaActivacion());
                    sqlcantidad.setParameter(9, tarjeta.getFechaExpiracion());
                    sqlcantidad.setParameter(10, tarjeta.getMonto());
                    sqlcantidad.setParameter(11, tarjeta.getTipoTransaccion());
                    sqlcantidad.setParameter(12, tarjeta.getHoraAlta());
                    sqlcantidad.setParameter(13, tarjeta.getUsuario());
                    sqlcantidad.setParameter(14, tarjeta.getDispositivo());
                    sqlcantidad.setParameter(15, tarjeta.getFechaSistema());
                    sqlcantidad.setParameter(16, tarjeta.getIp());
                    sqlcantidad.setParameter(17, tarjeta.getPrograma());
                    sqlcantidad.setParameter(18, tarjeta.getEvento());
                    break;
                case 2:
                    sqlcantidad.setParameter(1, tarjeta.getNroTarjeta());
                    sqlcantidad.setParameter(2, tarjeta.getCodPais());
                    sqlcantidad.setParameter(3, tarjeta.getTrnATM());
                    sqlcantidad.setParameter(4, tarjeta.getTrnPOS());
                    sqlcantidad.setParameter(5, tarjeta.getCodCliente());
                    sqlcantidad.setParameter(6, tarjeta.getEstado());
                    sqlcantidad.setParameter(7, tarjeta.getCodRespuesta());
                    sqlcantidad.setParameter(8, tarjeta.getFechaActivacion());
                    sqlcantidad.setParameter(9, tarjeta.getFechaExpiracion());
                    sqlcantidad.setParameter(10, tarjeta.getMonto());
                    sqlcantidad.setParameter(11, tarjeta.getTipoTransaccion());
                    sqlcantidad.setParameter(12, tarjeta.getFechaAlta());
                    sqlcantidad.setParameter(13, tarjeta.getHoraAlta());
                    sqlcantidad.setParameter(14, tarjeta.getUsuario());
                    sqlcantidad.setParameter(15, tarjeta.getDispositivo());
                    sqlcantidad.setParameter(16, tarjeta.getFechaSistema());
                    sqlcantidad.setParameter(17, tarjeta.getIp());
                    sqlcantidad.setParameter(18, tarjeta.getPrograma());
                    sqlcantidad.setParameter(19, tarjeta.getEvento());
                    break;
                case 3:
                    sqlcantidad.setParameter(1, tarjeta.getCodCliente());
                    sqlcantidad.setParameter(2, tarjeta.getTrnATM());
                    sqlcantidad.setParameter(3, tarjeta.getTrnPOS());
                    sqlcantidad.setParameter(4, tarjeta.getEstado());
                    sqlcantidad.setParameter(5, tarjeta.getCodRespuesta());
                    sqlcantidad.setParameter(6, tarjeta.getFechaActivacion());
                    sqlcantidad.setParameter(7, tarjeta.getFechaExpiracion());
                    sqlcantidad.setParameter(8, tarjeta.getMonto());
                    sqlcantidad.setParameter(9, tarjeta.getHoraAlta());
                    sqlcantidad.setParameter(10, tarjeta.getUsuario());
                    sqlcantidad.setParameter(11, tarjeta.getDispositivo());
                    sqlcantidad.setParameter(12, tarjeta.getFechaSistema());
                    sqlcantidad.setParameter(13, tarjeta.getIp());
                    sqlcantidad.setParameter(14, tarjeta.getPrograma());
                    sqlcantidad.setParameter(15, tarjeta.getEvento());
                    sqlcantidad.setParameter(16, tarjeta.getMontoAcumulado());
                    sqlcantidad.setParameter(17, tarjeta.getTipoTransaccion());
                    sqlcantidad.setParameter(18, tarjeta.getNroTarjeta());
                    sqlcantidad.setParameter(19, tarjeta.getCodPais());
                    sqlcantidad.setParameter(20, tarjeta.getTipoTransaccion());
                    break;
                default:
                    sqlcantidad.setParameter(1, tarjeta.getNroTarjeta());
                    break;

            }

            return sqlcantidad.executeUpdate();
        });
    }

    /**
     * Registro de autorización de operaciones por internet
     */
    @SuppressWarnings("unchecked")
    public boolean guardarAutorizacionInternet(TarjetaDebitoAutorizacion tarjeta) {
        try {
            int nro = daoAS400.doWithTransaction((entityManager, t) -> {
                OpenJPAQuery<Integer> sqlcantidad = entityManager.createNativeQuery(getSQL_ValidarRegistroInternet(), Integer.class);
                sqlcantidad.setParameter(1, tarjeta.getNroTarjeta());
                sqlcantidad.setParameter(2, tarjeta.getCodPais());
                return sqlcantidad.getSingleResult();
            });
            tarjeta.setFechaAlta(Integer.parseInt(TarjetaDebitoAutorizacion.FECHA_FORMATO.format(new Date(System.currentTimeMillis()))));
            tarjeta.setHoraAlta(Integer.parseInt(TarjetaDebitoAutorizacion.HORA_FORMATO.format(new Date(System.currentTimeMillis()))));
            tarjeta.setFechaSistema(new Date(System.currentTimeMillis()));
            tarjeta.setPrograma("EBISA");
            tarjeta.setDispositivo("EBISA");
            tarjeta.setEvento("W");
            // Aplicar regla de bloqueo para registro antiguos
            tarjeta.setMontoAcumulado(0d);
            if (nro == 0) {
                LOG.debug("Creando nuevo registro");
                setTarjeta(getSQL_Insert(), 1, tarjeta);
            } else {
                LOG.debug("actualizando registro");
                tarjeta.setEvento("U");
                tarjeta.setTipoTransaccion(TarjetaDebitoAutorizacion.TRANS_COMPRA_INTERTNET);
                setTarjeta(getSQL_UpdateInternet(), 3, tarjeta);
            }
            tarjeta.setTipoTransaccion(TarjetaDebitoAutorizacion.TRANS_COMPRA_INTERTNET);
            setTarjeta(getSQL_InsertHist(), 2, tarjeta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
            LOG.warn("No se pudo guardar autorización de transacciones internet de tarjeta de debito=[{}]", tarjeta.getNroTarjeta(), e);
            return false;
        }
    }

    /**
     * Recuperacion de fecha de vencimiento de tarjeta plastica
     **/
    @SuppressWarnings("unchecked")
    public Date getFechaExpiracion(String tarjeta) {
        try {
            List<Map<String, Object>> lista = daoAS400.doWithTransaction((entityManager, t) -> {
                OpenJPAQuery<Map<String, Object>> sqlcantidad = entityManager.createNativeQuery(getSQL_FechaVencimiento());
                sqlcantidad.setParameter(1, tarjeta);
                return sqlcantidad.getResultList();
            });
            if (lista != null && lista.size() > 0) {
                for (Map<String, Object> obj : lista) {
                    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
                    String fechaCadena = (String) (obj.get("FECHA"));
                    fechaCadena = fechaCadena.substring(1);
                    return sdf2.parse("20" + fechaCadena);
                }
            }
        } catch (Exception e) {
            LOG.warn("No se pudo validar fecha de vencimiento de tarjeta de debito=[{}]", tarjeta, e);
            return null;
        }
        return null;
    }

    /**
     * Recuperacion de monto maximo
     **/
    @SuppressWarnings("unchecked")
    public Double getMontoLimite(String sql, String tarjeta) {
        if (StringUtils.isEmpty(sql)) {
            return 0d;
        }

        try {
            List<Map<String, Object>> lista = daoAS400.doWithTransaction((entityManager, t) -> {
                OpenJPAQuery<Map<String, Object>> sqlcantidad = entityManager.createNativeQuery(getSQL_FechaVencimiento());
                sqlcantidad.setParameter(1, tarjeta);
                return sqlcantidad.getResultList();
            });
            if (lista != null && lista.size() > 0) {
                for (Map<String, Object> obj : lista) {
                    BigDecimal value = (BigDecimal) (obj.get("MONTO"));
                    Double max = value.doubleValue();
                    max = max * 0.01;
                    return max;
                }
            }
        } catch (Exception e) {
            LOG.warn("No se pudo validar monto limite de tarjeta de debito=[{}]", tarjeta, e);
            return 0d;
        }
        return 0d;
    }

    /**
     * Listado de tarjetas con operaciones activas para compras por internet
     */
    @SuppressWarnings("unchecked")
    public List<TarjetaDebitoAutorizacion> getActivacionesComprasInternet(String numeroCliente, List<String> tarjetasActivas) {
        if (tarjetasActivas.size() == 0) {
            return new ArrayList<>();
        }
        LOG.debug("Obteniendo listado de tarjetas de debito habilitadas para el cliente {}", numeroCliente);
        Integer fechaActual = TarjetaDebitoAutorizacion.formatoFecha(new Date(System.currentTimeMillis()));
        if (StringUtils.isNotEmpty(numeroCliente) && numeroCliente.length() > 6) {
            numeroCliente = numeroCliente.substring(4);
        }
        Map<String, List<String>> parametros = new HashMap<>();
        parametros.put("fechaActual", Collections.singletonList(fechaActual.toString()));
        parametros.put("nroCliente", Collections.singletonList(numeroCliente));
        parametros.put("tarjetas", tarjetasActivas);
        List<TarjetaDebitoAutorizacion> lista = daoAS400.doWithTransaction((entityManager, t) -> {
            OpenJPAQuery<TarjetaDebitoAutorizacion> sqlcantidad = entityManager.createNativeQuery(getSQL_ComprasInternet(), TarjetaDebitoAutorizacion.class);
            sqlcantidad.setParameters(parametros);
            return sqlcantidad.getResultList();
        });

        List<TarjetaDebitoAutorizacion> listaSinRegla = new ArrayList<>();
        List<String> existentes = lista.stream().map(obj -> obj.getNroTarjeta().trim()).collect(Collectors.toList());

        // Regla por defecto
        TarjetaDebitoAutorizacion defecto = getAutorizacion(TarjetaDebitoAutorizacion.CLIENTE_POR_DEFECTO,
                TarjetaDebitoAutorizacion.BIN_POR_DEFECTO);

        tarjetasActivas.removeAll(existentes);
        for (String nroTarjeta : tarjetasActivas) {
            // Definir registro por defecto
            TarjetaDebitoAutorizacion tarjeta = new TarjetaDebitoAutorizacion(nroTarjeta);
            tarjeta.setEstado(null); //EstadoTarjetaDebito.ACTIVO.getEstado()
            tarjeta.setCodCliente(numeroCliente);
            tarjeta.setTipoTransaccion(TarjetaDebitoAutorizacion.TRANS_COMPRA_INTERTNET);
            tarjeta.setFechaActivacion(TarjetaDebitoAutorizacion.FECHA_DEFECTO);
            tarjeta.setFechaExpiracion(TarjetaDebitoAutorizacion.FECHA_DEFECTO);
            tarjeta.setTrnATM(TarjetaDebitoAutorizacion.TRANS_DEFECTO);
            tarjeta.setTrnPOS(TarjetaDebitoAutorizacion.TRANS_DEFECTO);
            // Cargado de limites por defecto
            tarjeta.setMonto(0d);//defecto.getMonto()

            listaSinRegla.add(tarjeta);
        }
        lista.addAll(listaSinRegla);
        return lista;
    }

    @SuppressWarnings("unchecked")
    public TarjetaDebitoAutorizacion getAutorizacion(String numeroCliente, String tarjeta) {
        List<TarjetaDebitoAutorizacion> lista = daoAS400.doWithTransaction((entityManager, t) -> {
            OpenJPAQuery<TarjetaDebitoAutorizacion> sqlcantidad = entityManager.createNativeQuery(getSQL_ComprasInternet(), TarjetaDebitoAutorizacion.class);
            sqlcantidad.setParameter(1, numeroCliente);
            sqlcantidad.setParameter(2, tarjeta);
            return sqlcantidad.getResultList();
        });

        if (lista.size() == 1) {
            return lista.get(0);
        }
        return new TarjetaDebitoAutorizacion();
    }

    private String getSQL_FechaVencimiento() {
        return "SELECT CAST(CHEXDT  AS  CHAR(7)) as FECHA  FROM ASID113001/ZCMSCH0P WHERE CHCARD = ? AND CHPART = '001' AND CHMBRN = '1'";
    }

    private String getSQL_ComprasInternet() { //AND ATEST5 in ('A', 'I')
        return "SELECT tabla.* FROM (SELECT ROW_NUMBER()OVER() AS id, ATNTA5 as nroTarjeta, ATPA15 as codPais, ATATM5 as trnATM, ATPOS5 as trnPOS, " +
                " ATITR5 as tipoTransaccion, ATIME5 as monto, ATIAC5 as montoAcumulado, " +
                " ATCLI5 as codCliente, ATEST5 as estado, ATCOD5 as codRespuesta, ATFEA5 as fechaActivacion, ATFEE5 as fechaExpiracion, ATHOR5 as horaAlta  " +
                " FROM ATP01005  WHERE (ATITR5='C' OR ATITR5='A') AND ATCLI5 IN (:nroCliente) AND ATNTA5 IN (:tarjetas)) as tabla ORDER BY estado DESC ";
    }

    private String getSQL_TransaccionesExterior() {  //ATEST5='A' AND AND ATFEE5 >= (:fechaActual)
        return "SELECT tabla.*, ATP01006.atdes6 as pais FROM (SELECT ROW_NUMBER()OVER() AS id, ATNTA5 as nroTarjeta, ATPA15 as codPais, ATATM5 as trnATM, " +
                " ATITR5 as tipoTransaccion, ATIME5 as monto, " +
                " ATPOS5 as trnPOS, ATCLI5 as codCliente, ATEST5 as estado, ATCOD5 as codRespuesta, ATFEA5 as fechaActivacion, ATFEE5 as fechaExpiracion, ATHOR5 as horaAlta  " +
                " FROM ATP01005  WHERE ATITR5='T' AND ATCLI5 IN (:nroCliente) AND ATNTA5 IN (:tarjetas)) as tabla LEFT JOIN ATP01006 ON tabla.codPais = ATPAI6 ORDER BY estado, pais DESC ";
    }

    private String getSQL_ValidarRegistro() {
        return "SELECT count(*) FROM ATP01005  WHERE  ATNTA5 = ? AND ATPA15=? AND ATITR5=?";
    }

    private String getSQL_ValidarRegistroInternet() {
        return "SELECT count(*) FROM ATP01005  WHERE  ATNTA5 = ? AND ATPA15=? AND (ATITR5='C' OR ATITR5='A')";
    }

    private String getSQL_InsertHist() {
        return "INSERT INTO ATP01007 (ATNTA7,ATPA17,ATATM7,ATPOS7,ATCLI7," +
                "ATEST7,ATCOD7,ATFEA7,ATFEE7,ATIME7," +
                "ATITR7,ATFMO7,ATHMO7,ATUMO7,ATTMO7," +
                "ATFEC7,ATIPAD7,ATPGMN7,ATEVE7) VALUES (" +
                "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    }

    private String getSQL_Update() {
        return "UPDATE ATP01005  SET ATCLI5=?, ATATM5=?, ATPOS5=?, ATEST5=?, ATCOD5=?, ATFEA5=?, ATFEE5=?, " +
                " ATIME5=?, ATHOR5=?, ATUSE5=?, ATDEV5=?, ATFEC5=?, " +  //ATUSE5=user
                " ATIPAD5=?, ATPGMN5=?, ATEVE5=?, ATIAC5=?, ATITR5=?  " +
                " WHERE ATNTA5=? AND ATPA15=? AND ATITR5=? ";
    }

    private String getSQL_UpdateInternet() {
        return "UPDATE ATP01005  SET ATCLI5=?, ATATM5=?, ATPOS5=?, ATEST5=?, ATCOD5=?, ATFEA5=?, ATFEE5=?, " +
                " ATIME5=?, ATHOR5=?, ATUSE5=?, ATDEV5=?, ATFEC5=?, " +  //ATUSE5=user
                " ATIPAD5=?, ATPGMN5=?, ATEVE5=?, ATIAC5=?, ATITR5=?  " +
                " WHERE ATNTA5=? AND ATPA15=? AND (ATITR5='C' OR ATITR5='A') ";
    }

    private String getSQL_Insert() {
        return "INSERT INTO ATP01005 (ATNTA5,ATPA15,ATATM5,ATPOS5,ATCLI5," +
                "ATEST5,ATCOD5,ATFEA5,ATFEE5,ATIME5,ATITR5,ATHOR5,ATUSE5,ATDEV5,ATFEC5,ATIPAD5,ATPGMN5,ATEVE5) VALUES (" +
                "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    }
}
