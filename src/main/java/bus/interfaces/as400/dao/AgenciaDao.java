package bus.interfaces.as400.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.interfaces.as400.entities.Agencia;
import bus.interfaces.as400.entities.AgenciaPK;
import com.google.inject.Inject;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class AgenciaDao extends DaoImpl<Agencia, AgenciaPK> {

    @Inject
    protected AgenciaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<AgenciaPK> countPath(Root<Agencia> from) {
        return from.get("cfbrnm");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<Agencia> p) {
        ArrayList<Path<String>> arrayList = new ArrayList<>();
        arrayList.add(p.get("cfbrnm"));
        return arrayList;
    }

    public List<Agencia> getAgenciasActivas() {
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<Agencia> q = cb.createQuery(Agencia.class);
                    Root<Agencia> p = q.from(Agencia.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("cfbrst"), 0));
                    predicatesAnd.add(cb.notEqual(p.get("cfbrch"), 100));
                    predicatesAnd.add(cb.notEqual(p.get("cfbrch"), 900));
                    predicatesAnd.add(cb.notLike(p.get("cfbrnm"), "% CIDRE%"));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    q.orderBy(cb.asc(p.get("cfbrnm")));
                    TypedQuery<Agencia> query = entityManager.createQuery(q);
                    return query.getResultList();
                }
        );
    }
}
