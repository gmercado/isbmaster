package bus.interfaces.as400.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.interfaces.as400.entities.AgenciaCajero;
import bus.interfaces.tipos.TipoCajero;
import com.google.inject.Inject;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class AgenciaCajeroDao extends DaoImpl<AgenciaCajero, Long> {

    @Inject
    protected AgenciaCajeroDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }


    public List<AgenciaCajero> getAgenciasActivas(TipoCajero tipoCajero) {
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<AgenciaCajero> q = cb.createQuery(AgenciaCajero.class);
                    Root<AgenciaCajero> p = q.from(AgenciaCajero.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("tipo"), tipoCajero));
                    predicatesAnd.add(cb.equal(p.get("estado"), 1));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<AgenciaCajero> query = entityManager.createQuery(q);
                    return query.getResultList();
                }
        );
    }

    public void registrar(AgenciaCajero agenciaCajero, String usuario) {
        agenciaCajero.setUsuarioCreador(usuario);
        agenciaCajero.setFechaCreacion(new Date());
        agenciaCajero.setEstado(1);
        persist(agenciaCajero);
    }
}

