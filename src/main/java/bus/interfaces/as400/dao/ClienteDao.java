package bus.interfaces.as400.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.interfaces.as400.entities.Cliente;
import bus.interfaces.as400.entities.ClientePK;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author David Cuevas
 * @author Roger Chura
 */
public class ClienteDao extends DaoImpl<Cliente, ClientePK> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClienteDao.class);
    private static final String PERSONA_NATURAL = "P";
    private static final String NOMBRE_DEPURADO = "NULO%";

    @Inject
    protected ClienteDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<ClientePK> countPath(Root<Cliente> from) {
        return from.get("codCliente");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<Cliente> p) {
        ArrayList<Path<String>> arrayList = new ArrayList<>();
        arrayList.add(p.get("documento"));
        arrayList.add(p.get("codCliente"));
        return arrayList;
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<Cliente> from, Cliente example, Cliente example2) {
        List<Predicate> list = new LinkedList<>();
        if (example != null) {
            // Las llaves primarias son excluidas 
            if (!(example.getBanco() == null || StringUtils.isEmpty(example.getBanco().toPlainString()))) {
                list.add(cb.equal(from.<String>get("banco"), example.getBanco()));
            }
            if (!StringUtils.isEmpty(example.getCodCliente())) {
                list.add(cb.equal(from.<String>get("codCliente"), example.getCodClientePaddeado()));
            }
            if (StringUtils.isEmpty(example.getDocumento())) {
                example.setTipoDoc(null);
            }
            list.add(cb.qbe(from, example));
        }
        return list.toArray(new Predicate[list.size()]);
    }

    /**
     * Lista de clientes dado un documento de identidad.
     *
     * @param docIdentidad    identificacion
     * @param fechaNacimiento fecha nacimiento
     * @return cliente
     */
    public Cliente getClienteByDocumentoAndFecNacimiento(final String docIdentidad, BigDecimal fechaNacimiento) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Cliente> q = cb.createQuery(Cliente.class);
                        Root<Cliente> p = q.from(Cliente.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("documento"), StringUtils.trimToEmpty(docIdentidad)));
                        predicatesAnd.add(cb.equal(p.get("fechaNac"), fechaNacimiento));
                        predicatesAnd.add(cb.equal(p.get("tipoCliente"), PERSONA_NATURAL));
                        predicatesAnd.add(cb.notLike(p.get("nombreCompleto"), NOMBRE_DEPURADO));

                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Cliente> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1)
                            return query.getSingleResult();
                        else
                            return null;
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    /**
     * Lista de clientes dado un documento de identidad.
     *
     * @param docIdentidad    identificacion
     * @param fechaNacimiento fecha nacimiento
     * @return cliente
     */
    public List<Cliente> getListaClientesByDocumentoAndFecNacimiento(final String docIdentidad, final BigDecimal fechaNacimiento) {
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Cliente> q = cb.createQuery(Cliente.class);
                        Root<Cliente> p = q.from(Cliente.class);

                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("documento"), StringUtils.trimToEmpty(docIdentidad)));
                        predicatesAnd.add(cb.equal(p.get("fechaNac"), fechaNacimiento));
                        predicatesAnd.add(cb.equal(p.get("tipoCliente"), PERSONA_NATURAL));
                        predicatesAnd.add(cb.notLike(p.get("nombreCompleto"), NOMBRE_DEPURADO));

                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Cliente> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public Cliente getByCodigoCliente(final String codigoCliente) {
        if (StringUtils.trimToNull(codigoCliente) == null) {
            LOGGER.error("Error no se puede consultar un Codigo Cliente NULO en la BD.");
            return null;
        }
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<Cliente> q = cb.createQuery(Cliente.class);
                    Root<Cliente> p = q.from(Cliente.class);
                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("codCliente"), StringUtils.trimToEmpty(codigoCliente)));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<Cliente> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else {
                        return null;
                    }
                }
        );
    }


}
