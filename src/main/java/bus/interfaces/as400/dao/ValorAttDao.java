package bus.interfaces.as400.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.interfaces.as400.entities.ValorAtt;
import bus.interfaces.as400.entities.ValorAttPK;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

/**
 * La clase <code>ListaDeValorDao.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 20/05/2013, 16:59:31
 */
public class ValorAttDao extends DaoImpl<ValorAtt, ValorAttPK> {

    @Inject
    protected ValorAttDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public List<ValorAtt> getListaDe(final TipoAtt tipoAtt) {
        return doWithTransaction(new WithTransaction<List<ValorAtt>>() {
            @Override
            public List<ValorAtt> call(OpenJPAEntityManager entityManager,
                                       OpenJPAEntityTransaction t) {
                TypedQuery<ValorAtt> q =
                        entityManager.createQuery("select v from ValorAtt v " +
                                        " where v.campo = :campo",
                                ValorAtt.class)
                                .setParameter("campo", tipoAtt.getCampo());
                return q.getResultList();
            }
        });

    }

    public List<ValorAtt> getListaDeOrdenadoPorDescripcion(final TipoAtt tipoAtt) {
        return doWithTransaction(new WithTransaction<List<ValorAtt>>() {
            @Override
            public List<ValorAtt> call(OpenJPAEntityManager entityManager,
                                       OpenJPAEntityTransaction t) {
                TypedQuery<ValorAtt> q =
                        entityManager.createQuery("select v from ValorAtt v " +
                                        " where v.campo = :campo " +
                                        " order by v.descripcion",
                                ValorAtt.class)
                                .setParameter("campo", tipoAtt.getCampo());
                return q.getResultList();
            }
        });

    }

    public List<ValorAtt> getListaDeConValor(final TipoAtt tipoAtt, final String valor) {
        return doWithTransaction(new WithTransaction<List<ValorAtt>>() {
            @Override
            public List<ValorAtt> call(OpenJPAEntityManager entityManager,
                                       OpenJPAEntityTransaction t) {
                TypedQuery<ValorAtt> q =
                        entityManager.createQuery("select v from ValorAtt v " +
                                        " where v.campo = :campo " +
                                        "   and v.valor = :valor",
                                ValorAtt.class)
                                .setParameter("campo", tipoAtt.getCampo())
                                .setParameter("valor", valor);
                return q.getResultList();
            }
        });
    }

    public List<ValorAtt> getListaDeConValorDiferenteA(final TipoAtt tipoAtt, final String valor) {
        return doWithTransaction(new WithTransaction<List<ValorAtt>>() {
            @Override
            public List<ValorAtt> call(OpenJPAEntityManager entityManager,
                                       OpenJPAEntityTransaction t) {
                TypedQuery<ValorAtt> q =
                        entityManager.createQuery("select v from ValorAtt v " +
                                        " where v.campo = :campo " +
                                        "   and v.valor <> :valor",
                                ValorAtt.class)
                                .setParameter("campo", tipoAtt.getCampo())
                                .setParameter("valor", valor);
                return q.getResultList();
            }
        });
    }

    public List<ValorAtt> getListaDeNacionalidades() {
        List<ValorAtt> list = getListaDe(TipoAtt.NACIONALIDAD);
        if (list == null || list.isEmpty()) {
            return list;
        }
        String[] descripciones = new String[2];
        for (ValorAtt att : list) {
            descripciones[0] = StringUtils.substring(att.getDescripcion(), 0, 20);
            descripciones[1] = StringUtils.substring(att.getDescripcion(), 20);
            att.setDescripcion(descripciones[0]);
            att.setDescripcion2(descripciones[1]);
        }
        return list;
    }

    public List<ValorAtt> getListaDePaises() {
        return getListaDePaisesFormateado(getListaDe(TipoAtt.PAIS));
    }

    public List<ValorAtt> getPaisBolivia() {
        return getListaDePaisesFormateado(getListaDeConValor(TipoAtt.PAIS, "004            "));
    }

    private List<ValorAtt> getListaDePaisesFormateado(List<ValorAtt> list) {
        if (list == null || list.isEmpty()) {
            return list;
        }
        String[] descripciones = new String[2];
        for (ValorAtt att : list) {
            descripciones[0] = StringUtils.substring(att.getDescripcion(), 0, att.getDescripcion().length() - 2);
            descripciones[1] = StringUtils.substring(att.getDescripcion(), -2);
            att.setDescripcion(descripciones[0]);
            att.setDescripcion2(descripciones[1]);
        }
        return list;
    }

    public List<ValorAtt> getListaDeDepartamentos() {
        List<ValorAtt> list = getListaDeConValorDiferenteA(TipoAtt.DEPARTAMENTO, "10             ");
        if (list == null || list.isEmpty()) {
            return list;
        }
        String[] descripciones = new String[2];
        for (ValorAtt att : list) {
            descripciones[0] = StringUtils.substring(att.getDescripcion(), 0, att.getDescripcion().length() - 3);
            descripciones[1] = StringUtils.substring(att.getDescripcion(), -3);
            att.setDescripcion(descripciones[0]);
            att.setDescripcion2(descripciones[1]);
        }
        return list;
    }

    public List<ValorAtt> getListaDeCiudades() {
        return getListaDeConValorDiferenteA(TipoAtt.CIUDAD, "10             ");
    }

    public List<ValorAtt> getListaDeTiposDeVivienda() {
        return getListaDe(TipoAtt.VIVIENDA);
    }

    public List<ValorAtt> getListaDeNivelDeEstudios() {
        return getListaDe(TipoAtt.NIVEL_ESTUDIOS);
    }

    public List<ValorAtt> getListaDeProfesiones() {
        return getListaDeOrdenadoPorDescripcion(TipoAtt.PROFESION);
    }

    public ValorAtt getValor(final TipoAtt tipoAtt, String Valor) {
        return doWithTransaction((entityManager, t) -> {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<ValorAtt> q = cb.createQuery(ValorAtt.class);
            Root<ValorAtt> p = q.from(ValorAtt.class);

            LinkedList<Predicate> predicatesAnd = new LinkedList<>();
            predicatesAnd.add(cb.equal(p.get("campo"), StringUtils.trimToEmpty(tipoAtt.getCampo())));
            predicatesAnd.add(cb.equal(p.get("valor"), StringUtils.trimToEmpty(Valor)));
            q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
            TypedQuery<ValorAtt> query = entityManager.createQuery(q);
            if (query.getResultList()!=null && query.getResultList().size()==1) {
                return query.getSingleResult();
            } else {
                return null;
            }
        });
    }
}
