package bus.interfaces.as400.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.interfaces.as400.entities.Cuenta;
import bus.interfaces.as400.entities.CuentaPK;
import com.google.inject.Inject;
import org.apache.openjpa.persistence.OpenJPAQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;

/**
 * @author by josanchez on 28/07/2016.
 * @author by rsalvatierra on 20/07/2017.
 */
public class CuentaDao extends DaoImpl<Cuenta, CuentaPK> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CuentaDao.class);

    @Inject
    protected CuentaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public Cuenta getByNumero(final Long numeroCuenta) {
        if (numeroCuenta == null || numeroCuenta == 0) {
            LOGGER.error("Error no se puede consultar un documento NULO en la BD.");
            return null;
        }
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<Cuenta> q = cb.createQuery(Cuenta.class);
                    Root<Cuenta> p = q.from(Cuenta.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("numeroCuenta"), numeroCuenta));
                    predicatesAnd.add(cb.notEqual(p.get("estadoCuenta"), "4"));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<Cuenta> query = entityManager.createQuery(q);
                    if (query.getResultList() != null && query.getResultList().size() == 1) {
                        return query.getSingleResult();
                    } else {
                        return null;
                    }
                }
        );
    }

    public String getProducto(final Long tipoCTA, final Long tipoPro) {
        return doWithTransaction((entityManager, t) -> {
            @SuppressWarnings("unchecked")
            OpenJPAQuery<String> sqlTipoProducto =
                    entityManager.createNativeQuery("SELECT CFPRNM FROM CFP21001 WHERE cfbk = ? AND cfadsg=? AND cftnbr=? AND cfrec=?",
                            String.class);
            String descripcionProducto = null;
            try {
                sqlTipoProducto.setParameter(1, 1);
                sqlTipoProducto.setParameter(2, tipoCTA);
                sqlTipoProducto.setParameter(3, tipoPro);
                sqlTipoProducto.setParameter(4, 210);
                descripcionProducto = sqlTipoProducto.getSingleResult();
            } catch (NoResultException e) {
                LOGGER.warn("NO existe descripcion del producto {} {} en AS400", tipoPro, tipoCTA);
            }
            return descripcionProducto;
        });
    }
}
