package bus.interfaces.as400.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.interfaces.as400.entities.ClienteCuenta;
import bus.interfaces.as400.entities.ClienteCuentaPK;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author by josanchez on 03/08/2016.
 */
public class ClienteCuentaDao extends DaoImpl<ClienteCuenta, ClienteCuentaPK> {

    private static final Logger LOG = LoggerFactory.getLogger(ClienteCuenta.class);

    @Inject
    protected ClienteCuentaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public List<String> getCuentaByNumCuenta(final String codigoCliente) {
        List<String> numeroCuentas = new ArrayList<>();
        if (codigoCliente == null) {
            LOG.error("Error no se puede consultar un documento NULO en la BD.");
            return null;
        }
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<ClienteCuenta> q = cb.createQuery(ClienteCuenta.class);
                    Root<ClienteCuenta> p = q.from(ClienteCuenta.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("codigoCliente"), codigoCliente));
                    predicatesAnd.add(cb.equal(p.get("numeroAplicacion"), 20));
                    predicatesAnd.add(p.get("relacion").in("SOW", "JOF", "JAF"));

                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));

                    TypedQuery<ClienteCuenta> query = entityManager.createQuery(q);
                    if (query.getResultList() == null && query.getResultList().size() > 1) {
                        return null;
                    }
                    for (ClienteCuenta clienteCuenta : query.getResultList()) {
                        numeroCuentas.add(clienteCuenta.getNumeroCuenta().substring(1, clienteCuenta.getNumeroCuenta().length()));
                    }
                    return numeroCuentas;
                }
        );
    }

}
