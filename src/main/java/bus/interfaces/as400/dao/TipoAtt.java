package bus.interfaces.as400.dao;

public enum TipoAtt {

    TIPO_DOCUMENTO("Tipo de Documento", "CUSOFF    "),
    ESTADO_CIVIL("Estado Civil", "CUMRTS    "),
    NACIONALIDAD("Nacionalidad", "CUCCC1    "),
    PAIS("Pais", "CUCNTY    "),
    DEPARTAMENTO("Departamento", "CUCSTA    "),
    CIUDAD("Ciudad", "CUSOCI    "),
    VIVIENDA("Vivienda", "CUOWN     "),
    //
    NIVEL_ESTUDIOS("Nivel de Estudios", "CULFSF    "),
    PROFESION("Profesion", "CUCPRF    "),
    SITUACION_LABORAL("Situaci\u00F3n Laboral", "CUEBPA    "),
    //
    /**
     * Codigo banco de tarjeta de credito en sistema NAZIR, utilizado para validar en la recepcion de archivos de retroalimentacion.
     * se confirmo via correo que se tiene un codigo de banco de envio (0900) y un codigo de banco de recepcion (0009)
     * la causa de la diferencia esta en revision por el proveedor paystudio. Para el pase se adiciona el nuevo codigo.
     **/
    TARJETA_CREDITO_EMISOR("Codigo emisor de tarjeta de credito recepcion", "TCCEMIR"); // 0009

    private final String descripcion;
    private final String campo;
    
    TipoAtt(String descripcion, String campo) {
        this.descripcion = descripcion;
        this.campo = campo;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public String getCampo() {
        return campo;
    }
}
