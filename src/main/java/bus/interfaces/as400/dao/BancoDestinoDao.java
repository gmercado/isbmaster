package bus.interfaces.as400.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.interfaces.as400.entities.BancoDestino;
import com.google.inject.Inject;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

/**
 * @author by rsalvatierra on 28/06/2017.
 */
public class BancoDestinoDao extends DaoImpl<BancoDestino, Long> {

    @Inject
    protected BancoDestinoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    public List<BancoDestino> getBancosActivos() {
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<BancoDestino> q = cb.createQuery(BancoDestino.class);
                    Root<BancoDestino> p = q.from(BancoDestino.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("incluirBanco"), "S"));
                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    q.orderBy(cb.asc(p.get("nombreCorto")));
                    TypedQuery<BancoDestino> query = entityManager.createQuery(q);
                    return query.getResultList();
                }
        );
    }

}
