package bus.interfaces.as400.entities;

import java.io.Serializable;

/**
 * Created by josanchez on 03/08/2016.
 */
public class ClienteCuentaDetallePK implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long cuenta;

    private String codigoCliente;

    public ClienteCuentaDetallePK() {
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ClientePK)) {
            return false;
        }
        ClienteCuentaDetallePK castOther = (ClienteCuentaDetallePK) obj;
        return (this.cuenta.longValue() == castOther.cuenta.longValue())
                && (this.getCodigoCliente().equals(castOther.getCodigoCliente()))
                ;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + ((int) (this.cuenta.longValue() ^ (this.cuenta.longValue() >>> 32)));
        hash = hash * prime + this.getCodigoCliente().hashCode();
        return hash;
    }

    public Long getCuenta() {
        return cuenta;
    }

    public void setCuenta(Long cuenta) {
        this.cuenta = cuenta;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }
}
