package bus.interfaces.as400.entities;

import bus.database.model.EntityBase;
import bus.interfaces.sqlite.entities.Cajero;
import bus.interfaces.tipos.TipoCajero;

import javax.persistence.*;

/**
 * @author by rsalvatierra on 05/09/2017.
 */
@Entity
@Table(name = "GEN01")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "G01FECALT", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "G01USRALT", nullable = false)),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "G01FECMOD")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "G01USRMOD"))
})
public class AgenciaCajero extends EntityBase {

    @Id
    @Column(name = "G01ID", nullable = false, length = 10)
    private long id;

    @Column(name = "G01TIPAGE")
    @Enumerated(EnumType.STRING)
    private TipoCajero tipo;

    @Column(name = "G01DEPART")
    private String departamento;

    @Column(name = "G01CIUDAD")
    private String ciudad;

    @Column(name = "G01NOMBRE")
    private String nombre;

    @Column(name = "G01TELEFO")
    private String telefonos;

    @Column(name = "G01HORARI")
    private String horarios;

    @Column(name = "G01DIRECC")
    private String direccion;

    @Column(name = "G01LATITU")
    private double latitud;

    @Column(name = "G01LONGIT")
    private double longitud;

    @Column(name = "G01DOLARE")
    private String dolares;

    @Column(name = "G01DEPOBI")
    private String depobisa;

    @Column(name = "G01CODIGO")
    private Long codigoAgencia;

    @Column(name = "G01ESTADO")
    private int estado;

    public AgenciaCajero() {
    }

    public AgenciaCajero(Cajero cajero) {
        setId(cajero.get_id());
        setTipo(cajero.getTipo());
        setCiudad(cajero.getCiudad());
        setDepartamento(cajero.getDepa());
        setNombre(cajero.getDescripcion());
        setLatitud(cajero.getLatitud());
        setLongitud(cajero.getLongitud());
        setHorarios(cajero.getHorarios());
        setTelefonos(cajero.getTelefonos());
        setDireccion(cajero.getDireccion());
        setDepobisa(cajero.getDepobisa());
        setDolares(cajero.getDolares());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TipoCajero getTipo() {
        return tipo;
    }

    public void setTipo(TipoCajero tipo) {
        this.tipo = tipo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(String telefonos) {
        this.telefonos = telefonos;
    }

    public String getHorarios() {
        return horarios;
    }

    public void setHorarios(String horarios) {
        this.horarios = horarios;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getDolares() {
        return dolares;
    }

    public void setDolares(String dolares) {
        this.dolares = dolares;
    }

    public String getDepobisa() {
        return depobisa;
    }

    public void setDepobisa(String depobisa) {
        this.depobisa = depobisa;
    }

    public Long getCodigoAgencia() {
        return codigoAgencia;
    }

    public void setCodigoAgencia(Long codigoAgencia) {
        this.codigoAgencia = codigoAgencia;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
}
