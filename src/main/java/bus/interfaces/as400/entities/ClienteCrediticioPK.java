package bus.interfaces.as400.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.math.BigDecimal;

@Embeddable
public class ClienteCrediticioPK implements Serializable {
    
    private static final long serialVersionUID = -9086363645062684707L;

    @Column(name="OPOPSB", length=16, nullable=false )
    private String numeroOperacion;
    
    @Column(name="OPCLIE", precision=10, scale=0, nullable=false )
    private BigDecimal numeroCliente;

    public String getNumeroOperacion() {
        return numeroOperacion;
    }

    public BigDecimal getNumeroCliente() {
        return numeroCliente;
    }

    public void setNumeroOperacion(String numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }

    public void setNumeroCliente(BigDecimal numeroCliente) {
        this.numeroCliente = numeroCliente;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ClienteCrediticioPK) {
            ClienteCrediticioPK pk = (ClienteCrediticioPK)obj;
            return numeroOperacion.equals(pk.numeroOperacion) && numeroCliente.equals(pk.numeroCliente);
        } else {
            return false;
        }       
    }

    @Override
    public int hashCode() {
        return numeroOperacion.hashCode()+numeroCliente.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ClienteCrediticioPK [numeroCliente=");
        builder.append(numeroCliente);
        builder.append(", numeroOperacion=");
        builder.append(numeroOperacion);
        builder.append("]");
        return builder.toString();
    }
    
}
