package bus.interfaces.as400.entities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Res�men completo de operaciones (central retorno)
 * @author Roger Chura
 * @since  1.0, 31/05/2013
 */
@Entity
@Table(name="CROPERACD")
public class ClienteCrediticio implements Serializable {

    private static final long serialVersionUID = -8892489370694784608L; 

    @EmbeddedId
    private ClienteCrediticioPK clienteCrediticioPK;
    
    @Column(name="OPTIPO", length=5)
    private String tipoOperacion;
    
    @Column(name="OPSUBT", length=25)
    private String subtipoPrestamo;

    @Column(name="OPDECR", length=6)
    private String destinoCredito;

    @Column(name="OPDECRD", length=40)
    private String descripcionDestino;

    @Column(name="OPTTEC", length=20)
    private String tipoGrupo;

    @Column(name="OPESTA", length=15)
    private String estadoOperacion;

    @Column(name="OPNOCL", length=40)
    private String nombreCliente;

    @Column(name="OPIDCL", length=13)
    private String numeroIdentificacion;

    @Column(name="OPIDCI", length=13)
    private String documentoIdentidad;

    @Column(name="OPIDNIT", length=13)
    private String documentoNIT;

    @Column(name="OPACCL", length=6)
    private String actividad;

    @Column(name="OPACCLD", length=40)
    private String descripcionActividad;
    
    @Column(name="OPCALI", length=2)
    private String calificacion;

    @Column(name="OPFEDE", scale=8, precision=0)
    private BigDecimal fechaDesembolso;

    @Column(name="OPFEVE", scale=8, precision=0)
    private BigDecimal fechaVencimiento;

    @Column(name="OPFEES", scale=8, precision=0)
    private BigDecimal fechaIngreso;

    @Column(name="OPFEMO", scale=8, precision=0)
    private BigDecimal fechaIncumplimiento;

    public ClienteCrediticioPK getClienteCrediticioPK() {
        return clienteCrediticioPK;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public String getSubtipoPrestamo() {
        return subtipoPrestamo;
    }

    public String getDestinoCredito() {
        return destinoCredito;
    }

    public String getDescripcionDestino() {
        return descripcionDestino;
    }

    public String getTipoGrupo() {
        return tipoGrupo;
    }

    public String getEstadoOperacion() {
        return estadoOperacion;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public String getDocumentoIdentidad() {
        return documentoIdentidad;
    }

    public String getDocumentoNIT() {
        return documentoNIT;
    }

    public String getActividad() {
        return actividad;
    }

    public String getDescripcionActividad() {
        return descripcionActividad;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public BigDecimal getFechaDesembolso() {
        return fechaDesembolso;
    }

    public BigDecimal getFechaVencimiento() {
        return fechaVencimiento;
    }

    public BigDecimal getFechaIngreso() {
        return fechaIngreso;
    }

    public BigDecimal getFechaIncumplimiento() {
        return fechaIncumplimiento;
    }

    public void setClienteCrediticioPK(ClienteCrediticioPK clienteCrediticioPK) {
        this.clienteCrediticioPK = clienteCrediticioPK;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public void setSubtipoPrestamo(String subtipoPrestamo) {
        this.subtipoPrestamo = subtipoPrestamo;
    }

    public void setDestinoCredito(String destinoCredito) {
        this.destinoCredito = destinoCredito;
    }

    public void setDescripcionDestino(String descripcionDestino) {
        this.descripcionDestino = descripcionDestino;
    }

    public void setTipoGrupo(String tipoGrupo) {
        this.tipoGrupo = tipoGrupo;
    }

    public void setEstadoOperacion(String estadoOperacion) {
        this.estadoOperacion = estadoOperacion;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public void setDocumentoIdentidad(String documentoIdentidad) {
        this.documentoIdentidad = documentoIdentidad;
    }

    public void setDocumentoNIT(String documentoNIT) {
        this.documentoNIT = documentoNIT;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public void setDescripcionActividad(String descripcionActividad) {
        this.descripcionActividad = descripcionActividad;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public void setFechaDesembolso(BigDecimal fechaDesembolso) {
        this.fechaDesembolso = fechaDesembolso;
    }

    public void setFechaVencimiento(BigDecimal fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public void setFechaIngreso(BigDecimal fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public void setFechaIncumplimiento(BigDecimal fechaIncumplimiento) {
        this.fechaIncumplimiento = fechaIncumplimiento;
    }
 

    
}




/*

CREATE TABLE CROPERACD
(
   OPRESP    CHAR(1)        NOT NULL DEFAULT ' ',
   OPANAP    CHAR(4)        NOT NULL DEFAULT ' ',
   OPOPSB    CHAR(16)       NOT NULL DEFAULT ' ',
   OPSUCU    CHAR(2)        NOT NULL DEFAULT ' ',
      NOT NULL DEFAULT ' ',
        NOT NULL DEFAULT ' ',
   OPMONE    CHAR(3)        NOT NULL DEFAULT ' ',
   OPMONO    NUMERIC(3)     NOT NULL DEFAULT 0,
   OPNROP    DECIMAL(13)    NOT NULL DEFAULT 0,
   OPNRRE    NUMERIC(2)     NOT NULL DEFAULT 0,
      NOT NULL DEFAULT ' ',
   OPFPGO    CHAR(1)        NOT NULL DEFAULT ' ',
   OPFOPA    CHAR(1)        NOT NULL DEFAULT ' ',
   OPLIPR    CHAR(30)       NOT NULL DEFAULT ' ',
   OPFFNN    CHAR(2)        NOT NULL DEFAULT ' ',
   OPTCRE    CHAR(2)        NOT NULL DEFAULT ' ',
   OPDEST    CHAR(30)       NOT NULL DEFAULT ' ',
   OPBALC    CHAR(5)        NOT NULL DEFAULT ' ',
   OPAALC    CHAR(4)        NOT NULL DEFAULT ' ',
   OPLICA    CHAR(16)       NOT NULL DEFAULT ' ',
   OPCOOF    CHAR(3)        NOT NULL DEFAULT ' ',
   OPNOOF    CHAR(20)       NOT NULL DEFAULT ' ',
   OPPOPR    CHAR(1)        NOT NULL DEFAULT ' ',
   OPOPCL    NUMERIC(5)     NOT NULL DEFAULT 0,
   OPDEGA    CHAR(80)       NOT NULL DEFAULT ' ',
   OPCPON    CHAR(25)       NOT NULL DEFAULT ' ',
   OPVGR0    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPVGR1    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPVGR8    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPDPLZ    NUMERIC(5)     NOT NULL DEFAULT 0,
   OPDIAM    DECIMAL(5)     NOT NULL DEFAULT 0,
   OPDIAMC   DECIMAL(5)     NOT NULL DEFAULT 0,
   OPDGRC    NUMERIC(5)     NOT NULL DEFAULT 0,
   OPDGRI    NUMERIC(5)     NOT NULL DEFAULT 0,
   OPNROC    DECIMAL(4)     NOT NULL DEFAULT 0,
   OPPPCA    DECIMAL(4)     NOT NULL DEFAULT 0,
   OPPPIN    DECIMAL(4)     NOT NULL DEFAULT 0,
   OPREPA    DECIMAL(4)     NOT NULL DEFAULT 0,
    NOT NULL DEFAULT ' ',
   OPCOTI    NUMERIC(11,7)  NOT NULL DEFAULT 0,
     NOT NULL DEFAULT ' ',
   OPSUCL    CHAR(2)        NOT NULL DEFAULT ' ',
   OPDESU    CHAR(40)       NOT NULL DEFAULT ' ',
   OPTIPE    CHAR(3)        NOT NULL DEFAULT ' ',
    NOT NULL DEFAULT ' ',
   OPFCAL    CHAR(1)        NOT NULL DEFAULT ' ',
   OPCALA    CHAR(2)        NOT NULL DEFAULT ' ',
   OPNIVA    CHAR(30)       NOT NULL DEFAULT ' ',
   OPIDCO    CHAR(13)       NOT NULL DEFAULT ' ',
   OPNOCO    CHAR(40)       NOT NULL DEFAULT ' ',
   OPNCOD    DECIMAL(2)     NOT NULL DEFAULT 0,
 NOT NULL DEFAULT ' ',
   OPFEPV    DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFEPVE   CHAR(10)       NOT NULL DEFAULT ' ',
   OPFEPI    DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFEPIE   CHAR(10)       NOT NULL DEFAULT ' ',
   OPFEPC    DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFEPCE   CHAR(10)       NOT NULL DEFAULT ' ',
   OPFEUI    DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFEUIE   CHAR(10)       NOT NULL DEFAULT ' ',
   OPFEUC    DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFEUCE   CHAR(10)       NOT NULL DEFAULT ' ',
   OPFPRC    DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFPRCE   CHAR(10)       NOT NULL DEFAULT ' ',
   OPFPRI    DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFPRIE   CHAR(10)       NOT NULL DEFAULT ' ',
   OPFERE    DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFEREE   CHAR(10)       NOT NULL DEFAULT ' ',
   OPFEPR    DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFEPRE   CHAR(10)       NOT NULL DEFAULT ' ',
   OPTASA    NUMERIC(6,4)   NOT NULL DEFAULT 0,
   OPMONT    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPMOUS    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPMOOR    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPVALC    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPCUK    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPCUI    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPMORE    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSACO    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAVI    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAAT    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAVE    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAEJ    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSACA    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAUS    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAOR    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPINVI    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPINAT    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPINVE    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPINEJ    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPINSP    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPINTU    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPINTO    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPINSU    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPRRQ    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPRCOCA  NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPRCOCO  NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPRESAD  NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPRUS    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPROR    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPRADUS  NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPRADOR  NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAGRT   NUMERIC(17,2)  NOT NULL DEFAULT 0,
   OPSAGRV   NUMERIC(17,2)  NOT NULL DEFAULT 0,
   OPSAGRM   NUMERIC(17,2)  NOT NULL DEFAULT 0,
   OPTOGA    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPGAOP    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPGAUS    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSACU    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSANC    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPCACO    NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPTAPOOL  NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPCTAO    NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCCCO    NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCCVI    NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCCAT    NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCCVE    NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCCEJ    NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCCCA    NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCIVI    NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCIAT    NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCIVE    NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCIEJ    NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCISP    NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCPRECA  NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCPRECO  NUMERIC(6)     NOT NULL DEFAULT 0,
   OPCPREAD  NUMERIC(8)     NOT NULL DEFAULT 0,
   OPFIL1    DECIMAL(1)     NOT NULL DEFAULT 0,
   OPFIL2    CHAR(1)        NOT NULL DEFAULT ' ',
   OPFIL3    DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFIL4    CHAR(20)       NOT NULL DEFAULT ' ',
   OPTPLI    CHAR(1)        NOT NULL DEFAULT ' ',
   OPFDELI   DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFDELIE  CHAR(10)       NOT NULL DEFAULT ' ',
   OPFCALI   DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFCALIE  CHAR(10)       NOT NULL DEFAULT ' ',
   OPMCOLI   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPMOLIC   NUMERIC(3)     NOT NULL DEFAULT 0,
   OPPATR    NUMERIC(15)    NOT NULL DEFAULT 0,
   OPINVS    NUMERIC(15)    NOT NULL DEFAULT 0,
   OPPEOC    NUMERIC(15)    NOT NULL DEFAULT 0,
   OPINDI    NUMERIC(7,4)   NOT NULL DEFAULT 0,
   OPTIID    CHAR(4)        NOT NULL DEFAULT ' ',
   OPTCDE    DECIMAL(7,5)   NOT NULL DEFAULT 0,
   OPGRDCR   CHAR(1)        NOT NULL DEFAULT ' ',
   OPNRCRA   DECIMAL(2)     NOT NULL DEFAULT 0,
   OPCOCRE   CHAR(3)        NOT NULL DEFAULT ' ',
   OPSUCUC   DECIMAL(3)     NOT NULL DEFAULT 0,
   OPTPINT   CHAR(2)        NOT NULL DEFAULT ' ',
   OPTPPLA   CHAR(1)        NOT NULL DEFAULT ' ',
   OPTOPR    CHAR(2)        NOT NULL DEFAULT ' ',
   OPCOBL    NUMERIC(5,2)   NOT NULL DEFAULT 0,
   OPFEIA    DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFEIAE   CHAR(10)       NOT NULL DEFAULT ' ',
   OPFECA    DECIMAL(8)     NOT NULL DEFAULT 0,
   OPFECAE   CHAR(10)       NOT NULL DEFAULT ' ',
   OPINCA    CHAR(20)       NOT NULL DEFAULT ' ',
   OPSACOD   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAVID   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAATD   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAVED   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAEJD   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSACAD   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPCACOD   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPRCOCAD NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPRCOCOD NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSACOO   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAVIO   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAATO   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAVEO   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSAEJO   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPSACAO   NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPRCOCAO NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPRCOCOO NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPPORPRCA NUMERIC(5,2)   NOT NULL DEFAULT 0,
   OPPORPRCO NUMERIC(5,2)   NOT NULL DEFAULT 0,
   OPPORPRCI NUMERIC(5,2)   NOT NULL DEFAULT 0,
   OPPRECICO NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPVGPRVEH NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPVGPRMER NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPVGPRMAQ NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPVGWARRA NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPVGSTABY NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPVGAUTOL NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPVGHIPOT NUMERIC(15,2)  NOT NULL DEFAULT 0,
   OPEMPR    CHAR(3)        NOT NULL DEFAULT ' ',
   OPSADM    CHAR(15)       NOT NULL DEFAULT ' '
);

*/