package bus.interfaces.as400.entities;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * @author David Cuevas
 *
 */
public class ClientePK implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private BigDecimal banco;
    
    private String codCliente;

    public ClientePK() {
    }

    public ClientePK(BigDecimal banco, String codCliente) {
        this.banco = banco;
        this.codCliente = codCliente;
    }

    public BigDecimal getBanco() {
        return banco;
    }

    public void setBanco(BigDecimal banco) {
        this.banco = banco;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ClientePK)) {
            return false;
        }
        ClientePK castOther = (ClientePK) obj;
        return (this.banco.longValue() == castOther.banco.longValue()) 
                && (this.codCliente.equals( castOther.codCliente ))
                ;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + ((int) (this.banco.longValue() ^ (this.banco.longValue() >>> 32)));
        hash = hash * prime + this.codCliente.hashCode();
        return hash;
    }


}
