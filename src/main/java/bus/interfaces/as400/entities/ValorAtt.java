package bus.interfaces.as400.entities;

import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * La clase <code>ListaDeValor.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 20/05/2013, 16:59:24
 *
 */
public class ValorAtt implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private BigDecimal banco = new BigDecimal(1);
    private BigDecimal aplicacion = new BigDecimal(90);
    private String campo;
    private String valor;

    private String descripcion;
    @Transient
    private String descripcion2;
    private String seguridad1;
    private String seguridad2;
    private String campoCaracterAsociadoAlValor;
    private BigDecimal campoNumericoAsociadoAlValor;
    
    public BigDecimal getBanco() {
        return banco;
    }
    public void setBanco(BigDecimal banco) {
        this.banco = banco;
    }
    public BigDecimal getAplicacion() {
        return aplicacion;
    }
    public void setAplicacion(BigDecimal aplicacion) {
        this.aplicacion = aplicacion;
    }
    public String getCampo() {
        return campo;
    }
    public void setCampo(String campo) {
        this.campo = campo;
    }
    public String getValor() {
        return valor;
    }
    public void setValor(String valor) {
        this.valor = valor;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String getDescripcion2() {
        return descripcion2;
    }
    public void setDescripcion2(String descripcion2) {
        this.descripcion2 = descripcion2;
    }
    public String getSeguridad1() {
        return seguridad1;
    }
    public void setSeguridad1(String seguridad1) {
        this.seguridad1 = seguridad1;
    }
    public String getSeguridad2() {
        return seguridad2;
    }
    public void setSeguridad2(String seguridad2) {
        this.seguridad2 = seguridad2;
    }
    public String getCampoCaracterAsociadoAlValor() {
        return campoCaracterAsociadoAlValor;
    }
    public void setCampoCaracterAsociadoAlValor(String campoCaracterAsociadoAlValor) {
        this.campoCaracterAsociadoAlValor = campoCaracterAsociadoAlValor;
    }
    public BigDecimal getCampoNumericoAsociadoAlValor() {
        return campoNumericoAsociadoAlValor;
    }
    public void setCampoNumericoAsociadoAlValor(
            BigDecimal campoNumericoAsociadoAlValor) {
        this.campoNumericoAsociadoAlValor = campoNumericoAsociadoAlValor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ValorAtt)) return false;

        ValorAtt valorAtt = (ValorAtt) o;

        if (aplicacion != null ? !aplicacion.equals(valorAtt.aplicacion) : valorAtt.aplicacion != null) return false;
        if (banco != null ? !banco.equals(valorAtt.banco) : valorAtt.banco != null) return false;
        if (campo != null ? !campo.equals(valorAtt.campo) : valorAtt.campo != null) return false;
        if (valor != null ? !valor.equals(valorAtt.valor) : valorAtt.valor != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = banco != null ? banco.hashCode() : 0;
        result = 31 * result + (aplicacion != null ? aplicacion.hashCode() : 0);
        result = 31 * result + (campo != null ? campo.hashCode() : 0);
        result = 31 * result + (valor != null ? valor.hashCode() : 0);
        return result;
    }

    /**
     * con esto obtenemos el codigo del atributo en los combos
     */
    @Override
    final public String toString() {
        return getValor();
    }
}
