package bus.interfaces.as400.entities;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by josanchez on 03/08/2016.
 */
public class ClienteCuentaPK implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long numeroBanco;

    private String codigoCliente;

    public ClienteCuentaPK() {
    }

    public ClienteCuentaPK(Long numeroBanco, String codigoCliente) {
        this.numeroBanco = numeroBanco;
        this.codigoCliente = codigoCliente;
    }

    public Long getNumeroBanco() {
        return numeroBanco;
    }

    public void setNumeroBanco(Long numeroBanco) {
        this.numeroBanco = numeroBanco;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ClientePK)) {
            return false;
        }
        ClienteCuentaPK castOther = (ClienteCuentaPK) obj;
        return (this.numeroBanco.longValue() == castOther.numeroBanco.longValue())
                && (this.codigoCliente.equals( castOther.codigoCliente ))
                ;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + ((int) (this.numeroBanco.longValue() ^ (this.numeroBanco.longValue() >>> 32)));
        hash = hash * prime + this.codigoCliente.hashCode();
        return hash;
    }
}
