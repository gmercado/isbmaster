package bus.interfaces.as400.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import bus.interfaces.tipos.EstadoCivilDao;

/**
 * 
 * @author David Cuevas
 *
 */
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private BigDecimal banco;
    
    private String codCliente;
    private String tipoDoc;
    private String documento;
    private String nombreCompleto;
    private String telefono;
    private String estadoCivil;
    private BigDecimal fechaNac;
    private BigDecimal agencia;
    private String mail;
    private String tipoCliente;
    private String sexo;

    @Transient
    private String tituloBoton = "Registrar e-Bisa";
    
    @Transient
    private String tipoTramite;
    
    public BigDecimal getBanco() {
        return banco;
    }
    public void setBanco(BigDecimal banco) {
        this.banco = banco;
    }
    public String getCodCliente() {
        return codCliente;
    }
    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }
    public String getNombreCompleto() {
        return nombreCompleto;
    }
    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }
    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getTipoDoc() {
        return tipoDoc;
    }
    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }
    public String getDocumento() {
        return documento;
    }
    public void setDocumento(String documento) {
        this.documento = documento;
    }
    public String getEstadoCivil() {
        return estadoCivil;
    }
    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }
    public BigDecimal getFechaNac() {
        return fechaNac;
    }
    public void setFechaNac(BigDecimal fechaNac) {
        this.fechaNac = fechaNac;
    }
    public BigDecimal getAgencia() {
        return agencia;
    }
    public void setAgencia(BigDecimal agencia) {
        this.agencia = agencia;
    }
    public String getMail() {
        return mail;
    }
    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFechaDeNacimiento() {
        Date fechaParseada=null;
        DateFormat df = null;
        try {
            fechaParseada = DateUtils.parseDate(fechaNac.toPlainString(), new String[]{ "yyyyD" });
            df = new SimpleDateFormat("dd/MM/yyyy");
        } catch (Exception e) {
            return "";
        }
        return df.format(fechaParseada);
    }

    public String getEstadoCivilDescripcion() {
        return EstadoCivilDao.getTipos().get(estadoCivil);
    }
    
    public String getDocumentoCompleto() {
        return tipoDoc+" "+documento; 
    }
    
    public String getTituloBoton() {
        return tituloBoton;
    }
    
    public void setTituloBoton(String tituloBoton) {
        this.tituloBoton = tituloBoton;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }
    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }
    public String getCodClientePaddeado() {
        return StringUtils.leftPad(codCliente, 10, '0');
    }

    public String getCodClienteSinCeros() {
        // incorporamos una excepcion para el codigo de cliente de ebisa
        // para eliminar los ceros por izquierda
        if (!StringUtils.isEmpty(this.getCodCliente())) {
            return this.getCodCliente().replaceFirst("^0*", "") ;
        }
        return this.codCliente;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }


    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

}
