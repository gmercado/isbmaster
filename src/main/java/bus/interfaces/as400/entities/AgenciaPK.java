package bus.interfaces.as400.entities;

import java.io.Serializable;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class AgenciaPK implements Serializable {

    private static final long serialVersionUID = -7317777494303065083L;

    /**
     * Bank Number
     */
    private Long cfbank;

    /**
     * Branch Number
     */
    private Long cfbrch;

    public AgenciaPK() {
    }

    public AgenciaPK(Long cfbank, Long cfbrch) {
        this.cfbank = cfbank;
        this.cfbrch = cfbrch;
    }

    public Long getCfbank() {
        return cfbank;
    }

    public void setCfbank(Long cfbank) {
        this.cfbank = cfbank;
    }

    public Long getCfbrch() {
        return cfbrch;
    }

    public void setCfbrch(Long cfbrch) {
        this.cfbrch = cfbrch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AgenciaPK)) return false;

        AgenciaPK cfp102Id = (AgenciaPK) o;

        if (!cfbank.equals(cfp102Id.cfbank)) return false;
        if (!cfbrch.equals(cfp102Id.cfbrch)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cfbank.hashCode();
        result = 31 * result + cfbrch.hashCode();
        return result;
    }
}

