package bus.interfaces.as400.entities;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * La clase <code>ListaDeValorPK.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 20/05/2013, 16:59:16
 *
 */
public class ValorAttPK implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private BigDecimal banco;
    private BigDecimal aplicacion;
    private String campo;
    private String valor;

    public BigDecimal getBanco() {
        return banco;
    }
    public void setBanco(BigDecimal banco) {
        this.banco = banco;
    }
    public BigDecimal getAplicacion() {
        return aplicacion;
    }
    public void setAplicacion(BigDecimal aplicacion) {
        this.aplicacion = aplicacion;
    }
    public String getCampo() {
        return campo;
    }
    public void setCampo(String campo) {
        this.campo = campo;
    }
    
    public String getValor() {
        return valor;
    }
    public void setValor(String valor) {
        this.valor = valor;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ValorAttPK)) {
            return false;
        }
        ValorAttPK castOther = (ValorAttPK) obj;
        return (this.banco.longValue() == castOther.banco.longValue()) && 
               (this.aplicacion.longValue() == castOther.aplicacion.longValue()) &&
               (this.campo.equals( castOther.campo )) &&
               (this.valor.equals( castOther.valor ))
                ;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + ((int) (this.banco.longValue() ^ (this.banco.longValue() >>> 32)));
        hash = hash * prime + ((int) (this.aplicacion.longValue() ^ (this.aplicacion.longValue() >>> 32)));
        hash = hash * prime + this.campo.hashCode();
        hash = hash * prime + this.valor.hashCode();
        return hash;
    }


    

}
