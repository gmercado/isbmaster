package bus.interfaces.as400.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author by rsalvatierra on 28/06/2017.
 */
@Entity
@Table(name = "psp879")
public class BancoDestino implements Serializable {

    private static final long serialVersionUID = -3632246623627518433L;

    @Id
    @Column(name = "BNCBAN")
    private Long id;

    @Column(name = "BNNOMC", length = 30)
    private String nombreCorto;

    @Column(name = "BNNOML", length = 60)
    private String nombreLargo;

    @Column(name = "BNACTACH", length = 1)
    private String incluirBanco;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public String getNombreLargo() {
        return nombreLargo;
    }

    public void setNombreLargo(String nombreLargo) {
        this.nombreLargo = nombreLargo;
    }

    public String getIncluirBanco() {
        return incluirBanco;
    }

    public void setIncluirBanco(String incluirBanco) {
        this.incluirBanco = incluirBanco;
    }
}
