package bus.interfaces.as400.entities;

import java.io.Serializable;

/**
 * @author by josanchez on 28/07/2016.
 */
public class CuentaPK implements Serializable {

    private Long tipoCuenta;
    private Long numeroCuenta;

    public CuentaPK() {
    }

    public CuentaPK(Long tipoCuenta, Long numeroCuenta) {
        this.tipoCuenta = tipoCuenta;
        this.numeroCuenta = numeroCuenta;
    }

    private static final long serialVersionUID = 1L;

    public Long getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(Long tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public Long getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(Long numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CuentaPK)) {
            return false;
        }
        CuentaPK castOther = (CuentaPK) obj;
        return (this.tipoCuenta.longValue() == castOther.tipoCuenta.longValue())
                && (this.numeroCuenta.equals(castOther.numeroCuenta))
                ;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + ((int) (this.tipoCuenta.longValue() ^ (this.tipoCuenta.longValue() >>> 32)));
        hash = hash * prime + this.numeroCuenta.hashCode();
        return hash;
    }


}
