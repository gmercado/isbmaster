package bus.interfaces.as400.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by josanchez on 03/08/2016.
 */
@Entity
@Table(name = "CUP00901")
@IdClass(ClienteCuentaPK.class)
public class ClienteCuenta implements Serializable {

    @Id
    @Column(name = "CUXBK", columnDefinition = "NUMERIC(3)")
    private Long numeroBanco;

    @Column(name = "CUXREC", columnDefinition = "VARCHAR(1)")
    private String tipoRegistro;

    @Id
    @Column(name = "CUX1CS", columnDefinition = "VARCHAR(10)")
    private String codigoCliente;

    @Column(name = "CUX1AP", columnDefinition = "NUMERIC(2)")
    private BigDecimal numeroAplicacion;

    @Column(name = "CUX1TY", columnDefinition = "NUMERIC(1)")
    private BigDecimal tipoRelacion;

    @Column(name = "CUX1AC", columnDefinition = "NUMERIC(12)")
    private String numeroCuenta;

    @Column(name = "CUXREL", columnDefinition = "CHAR(3)")
    private String relacion;

    public ClienteCuenta() {
    }

    public ClienteCuenta(Long numeroBanco, String tipoRegistro, String codigoCliente, BigDecimal numeroAplicacion, BigDecimal tipoRelacion, String numeroCuenta, String relacion) {
        this.numeroBanco = numeroBanco;
        this.tipoRegistro = tipoRegistro;
        this.codigoCliente = codigoCliente;
        this.numeroAplicacion = numeroAplicacion;
        this.tipoRelacion = tipoRelacion;
        this.numeroCuenta = numeroCuenta;
        this.relacion = relacion;
    }

    public Long getNumeroBanco() {
        return numeroBanco;
    }

    public void setNumeroBanco(Long numeroBanco) {
        this.numeroBanco = numeroBanco;
    }

    public String getTipoRegistro() {
        return tipoRegistro;
    }

    public void setTipoRegistro(String tipoRegistro) {
        this.tipoRegistro = tipoRegistro;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public BigDecimal getNumeroAplicacion() {
        return numeroAplicacion;
    }

    public void setNumeroAplicacion(BigDecimal numeroAplicacion) {
        this.numeroAplicacion = numeroAplicacion;
    }

    public BigDecimal getTipoRelacion() {
        return tipoRelacion;
    }

    public void setTipoRelacion(BigDecimal tipoRelacion) {
        this.tipoRelacion = tipoRelacion;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getRelacion() {
        return relacion;
    }

    public void setRelacion(String relacion) {
        this.relacion = relacion;
    }

    @Override
    public String toString() {
        return "ClienteCuenta{" +
                "numeroBanco=" + numeroBanco +
                ", tipoRegistro='" + tipoRegistro + '\'' +
                ", codigoCliente='" + codigoCliente + '\'' +
                ", numeroAplicacion=" + numeroAplicacion +
                ", tipoRelacion=" + tipoRelacion +
                ", numeroCuenta='" + numeroCuenta + '\'' +
                ", relacion='" + relacion + '\'' +
                '}';
    }
}
