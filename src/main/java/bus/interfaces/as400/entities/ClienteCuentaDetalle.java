package bus.interfaces.as400.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by josanchez on 29/08/2016.
 * @author by rsalvatierra on 29/04/2017.
 */
@Entity
@Table(name = "TAP80201")
@IdClass(ClienteCuentaDetallePK.class)
public class ClienteCuentaDetalle implements Serializable {

    @Id
    @Column(name = "T02CUENTA", columnDefinition = "NUMERIC(10)")
    private Long cuenta;

    @Id
    @Column(name = "T02CLIEN", columnDefinition = "VARCHAR(10)")
    private String codigoCliente;

    @Column(name = "T02NOMBR", columnDefinition = "VARCHAR(10)")
    private String nombreTitular;

    @Column(name = "T02TPOCTA", columnDefinition = "NUMERIC(1)")
    private String tipoCuenta;

    @Column(name = "T02BANCO", columnDefinition = "NUMERIC(3)")
    private BigDecimal codigobanco;

    public ClienteCuentaDetalle() {
    }

    public Long getCuenta() {
        return cuenta;
    }

    public void setCuenta(Long cuenta) {
        this.cuenta = cuenta;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getNombreTitular() {
        return nombreTitular;
    }

    public void setNombreTitular(String nombreTitular) {
        this.nombreTitular = nombreTitular;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public BigDecimal getCodigobanco() {
        return codigobanco;
    }

    public void setCodigobanco(BigDecimal codigobanco) {
        this.codigobanco = codigobanco;
    }
}
