package bus.interfaces.as400.entities;

import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
@Entity
@Table(name = "CFP102")
@IdClass(AgenciaPK.class)
public class Agencia implements Serializable {

    private static final long serialVersionUID = -8838885375284735228L;

    /**
     * Bank Number
     */
    @Id
    @Column(name = "CFBANK")
    private Long cfbank;

    /**
     * Branch Number
     */
    @Id
    @Column(name = "CFBRCH")
    private Long cfbrch;

    /**
     * Branch Name
     */
    @Column(name = "CFBRNM", nullable = false, length = 40)
    private String cfbrnm;

    /**
     * Branch Address 1
     */
    @Column(name = "CFBRA1", nullable = false, length = 30)
    private String cfbra1;
    /**
     * Branch Address 2
     */
    @Column(name = "CFBRA2", nullable = false, length = 30)
    private String cfbra2;
    /**
     * Zip Code
     */
    @Column(name = "CFBZIP", nullable = false)
    private Long cfbzip;
    /**
     * Postal Code
     */
    @Column(name = "CFBRPC", nullable = false, length = 10)
    private String cfbrpc;
    /**
     * State Code for Branches
     */
    @Column(name = "CFBRST", nullable = false)
    private Long cfbrst;
    /**
     * Region Number
     */
    @Column(name = "CFBRRG", nullable = false)
    private BigDecimal cfbrrg;
    /**
     * Bank State Branch
     */
    @Column(name = "CFBSB", nullable = false)
    private Long cfbsb;
    /**
     * Branch Manager Name
     */
    @Column(name = "CFBMGN", nullable = false, length = 30)
    private String cfbmgn;
    /**
     * Holiday 1
     */
    @Column(name = "CFHB01", nullable = false)
    private BigDecimal cfhb01;
    /**
     * Holiday 2
     */
    @Column(name = "CFHB02", nullable = false)
    private BigDecimal cfhb02;
    /**
     * Holiday 3
     */
    @Column(name = "CFHB03", nullable = false)
    private BigDecimal cfhb03;
    /**
     * Holiday 4
     */
    @Column(name = "CFHB04", nullable = false)
    private BigDecimal cfhb04;
    /**
     * Holiday 5
     */
    @Column(name = "CFHB05", nullable = false)
    private BigDecimal cfhb05;
    /**
     * Holiday 6
     */
    @Column(name = "CFHB06", nullable = false)
    private BigDecimal cfhb06;
    /**
     * Holiday 7
     */
    @Column(name = "CFHB07", nullable = false)
    private BigDecimal cfhb07;
    /**
     * Holiday 8
     */
    @Column(name = "CFHB08", nullable = false)
    private BigDecimal cfhb08;
    /**
     * Holiday 9
     */
    @Column(name = "CFHB09", nullable = false)
    private BigDecimal cfhb09;
    /**
     * Holiday 10
     */
    @Column(name = "CFHB10", nullable = false)
    private BigDecimal cfhb10;
    /**
     * Holiday 11
     */
    @Column(name = "CFHB11", nullable = false)
    private BigDecimal cfhb11;
    /**
     * Holiday 12
     */
    @Column(name = "CFHB12", nullable = false)
    private BigDecimal cfhb12;
    /**
     * Holiday 13
     */
    @Column(name = "CFHB13", nullable = false)
    private BigDecimal cfhb13;
    /**
     * Holiday 14
     */
    @Column(name = "CFHB14", nullable = false)
    private BigDecimal cfhb14;
    /**
     * Holiday 15
     */
    @Column(name = "CFHB15", nullable = false)
    private BigDecimal cfhb15;
    /**
     * Holiday 16
     */
    @Column(name = "CFHB16", nullable = false)
    private BigDecimal cfhb16;
    /**
     * Holiday 17
     */
    @Column(name = "CFHB17", nullable = false)
    private BigDecimal cfhb17;
    /**
     * Holiday 18
     */
    @Column(name = "CFHB18", nullable = false)
    private BigDecimal cfhb18;
    /**
     * Holiday 19
     */
    @Column(name = "CFHB19", nullable = false)
    private BigDecimal cfhb19;
    /**
     * Holiday 20
     */
    @Column(name = "CFHB20", nullable = false)
    private BigDecimal cfhb20;
    /**
     * Holiday 21
     */
    @Column(name = "CFHB21", nullable = false)
    private BigDecimal cfhb21;
    /**
     * Holiday 22
     */
    @Column(name = "CFHB22", nullable = false)
    private BigDecimal cfhb22;
    /**
     * Holiday 23
     */
    @Column(name = "CFHB23", nullable = false)
    private BigDecimal cfhb23;
    /**
     * Holiday 24
     */
    @Column(name = "CFHB24", nullable = false)
    private BigDecimal cfhb24;
    /**
     * Branch Week Definition
     */
    @Column(name = "CFBWK", nullable = false, length = 7)
    private String cfbwk;
    /**
     * Time of funds release
     */
    @Column(name = "CFBRTM", nullable = false)
    private BigDecimal cfbrtm;

    public Long getCfbank() {
        return cfbank;
    }

    public void setCfbank(Long cfbank) {
        this.cfbank = cfbank;
    }

    public Long getCfbrch() {
        return cfbrch;
    }

    public void setCfbrch(Long cfbrch) {
        this.cfbrch = cfbrch;
    }

    public String getCfbrnm() {
        return cfbrnm;
    }

    public void setCfbrnm(String cfbrnm) {
        this.cfbrnm = cfbrnm;
    }

    public String getCfbra1() {
        return cfbra1;
    }

    public void setCfbra1(String cfbra1) {
        this.cfbra1 = cfbra1;
    }

    public String getCfbra2() {
        return cfbra2;
    }

    public void setCfbra2(String cfbra2) {
        this.cfbra2 = cfbra2;
    }

    public Long getCfbzip() {
        return cfbzip;
    }

    public void setCfbzip(Long cfbzip) {
        this.cfbzip = cfbzip;
    }

    public String getCfbrpc() {
        return cfbrpc;
    }

    public void setCfbrpc(String cfbrpc) {
        this.cfbrpc = cfbrpc;
    }

    public Long getCfbrst() {
        return cfbrst;
    }

    public void setCfbrst(Long cfbrst) {
        this.cfbrst = cfbrst;
    }

    public BigDecimal getCfbrrg() {
        return cfbrrg;
    }

    public void setCfbrrg(BigDecimal cfbrrg) {
        this.cfbrrg = cfbrrg;
    }

    public Long getCfbsb() {
        return cfbsb;
    }

    public void setCfbsb(Long cfbsb) {
        this.cfbsb = cfbsb;
    }

    public String getCfbmgn() {
        return cfbmgn;
    }

    public void setCfbmgn(String cfbmgn) {
        this.cfbmgn = cfbmgn;
    }

    public BigDecimal getCfhb01() {
        return cfhb01;
    }

    public void setCfhb01(BigDecimal cfhb01) {
        this.cfhb01 = cfhb01;
    }

    public BigDecimal getCfhb02() {
        return cfhb02;
    }

    public void setCfhb02(BigDecimal cfhb02) {
        this.cfhb02 = cfhb02;
    }

    public BigDecimal getCfhb03() {
        return cfhb03;
    }

    public void setCfhb03(BigDecimal cfhb03) {
        this.cfhb03 = cfhb03;
    }

    public BigDecimal getCfhb04() {
        return cfhb04;
    }

    public void setCfhb04(BigDecimal cfhb04) {
        this.cfhb04 = cfhb04;
    }

    public BigDecimal getCfhb05() {
        return cfhb05;
    }

    public void setCfhb05(BigDecimal cfhb05) {
        this.cfhb05 = cfhb05;
    }

    public BigDecimal getCfhb06() {
        return cfhb06;
    }

    public void setCfhb06(BigDecimal cfhb06) {
        this.cfhb06 = cfhb06;
    }

    public BigDecimal getCfhb07() {
        return cfhb07;
    }

    public void setCfhb07(BigDecimal cfhb07) {
        this.cfhb07 = cfhb07;
    }

    public BigDecimal getCfhb08() {
        return cfhb08;
    }

    public void setCfhb08(BigDecimal cfhb08) {
        this.cfhb08 = cfhb08;
    }

    public BigDecimal getCfhb09() {
        return cfhb09;
    }

    public void setCfhb09(BigDecimal cfhb09) {
        this.cfhb09 = cfhb09;
    }

    public BigDecimal getCfhb10() {
        return cfhb10;
    }

    public void setCfhb10(BigDecimal cfhb10) {
        this.cfhb10 = cfhb10;
    }

    public BigDecimal getCfhb11() {
        return cfhb11;
    }

    public void setCfhb11(BigDecimal cfhb11) {
        this.cfhb11 = cfhb11;
    }

    public BigDecimal getCfhb12() {
        return cfhb12;
    }

    public void setCfhb12(BigDecimal cfhb12) {
        this.cfhb12 = cfhb12;
    }

    public BigDecimal getCfhb13() {
        return cfhb13;
    }

    public void setCfhb13(BigDecimal cfhb13) {
        this.cfhb13 = cfhb13;
    }

    public BigDecimal getCfhb14() {
        return cfhb14;
    }

    public void setCfhb14(BigDecimal cfhb14) {
        this.cfhb14 = cfhb14;
    }

    public BigDecimal getCfhb15() {
        return cfhb15;
    }

    public void setCfhb15(BigDecimal cfhb15) {
        this.cfhb15 = cfhb15;
    }

    public BigDecimal getCfhb16() {
        return cfhb16;
    }

    public void setCfhb16(BigDecimal cfhb16) {
        this.cfhb16 = cfhb16;
    }

    public BigDecimal getCfhb17() {
        return cfhb17;
    }

    public void setCfhb17(BigDecimal cfhb17) {
        this.cfhb17 = cfhb17;
    }

    public BigDecimal getCfhb18() {
        return cfhb18;
    }

    public void setCfhb18(BigDecimal cfhb18) {
        this.cfhb18 = cfhb18;
    }

    public BigDecimal getCfhb19() {
        return cfhb19;
    }

    public void setCfhb19(BigDecimal cfhb19) {
        this.cfhb19 = cfhb19;
    }

    public BigDecimal getCfhb20() {
        return cfhb20;
    }

    public void setCfhb20(BigDecimal cfhb20) {
        this.cfhb20 = cfhb20;
    }

    public BigDecimal getCfhb21() {
        return cfhb21;
    }

    public void setCfhb21(BigDecimal cfhb21) {
        this.cfhb21 = cfhb21;
    }

    public BigDecimal getCfhb22() {
        return cfhb22;
    }

    public void setCfhb22(BigDecimal cfhb22) {
        this.cfhb22 = cfhb22;
    }

    public BigDecimal getCfhb23() {
        return cfhb23;
    }

    public void setCfhb23(BigDecimal cfhb23) {
        this.cfhb23 = cfhb23;
    }

    public BigDecimal getCfhb24() {
        return cfhb24;
    }

    public void setCfhb24(BigDecimal cfhb24) {
        this.cfhb24 = cfhb24;
    }

    public String getCfbwk() {
        return cfbwk;
    }

    public void setCfbwk(String cfbwk) {
        this.cfbwk = cfbwk;
    }

    public BigDecimal getCfbrtm() {
        return cfbrtm;
    }

    public void setCfbrtm(BigDecimal cfbrtm) {
        this.cfbrtm = cfbrtm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Agencia)) return false;

        Agencia cfp102 = (Agencia) o;

        if (cfbank != null ? !cfbank.equals(cfp102.cfbank) : cfp102.cfbank != null) return false;
        if (cfbrch != null ? !cfbrch.equals(cfp102.cfbrch) : cfp102.cfbrch != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cfbank != null ? cfbank.hashCode() : 0;
        result = 31 * result + (cfbrch != null ? cfbrch.hashCode() : 0);
        return result;
    }

    public String getNombreAgenciaSinAGENCIA() {
        return StringUtils.remove(this.cfbrnm, "AGENCIA");
    }

    public String getNombreAgenciaSinCiudad() {
        return StringUtils.substring(this.cfbrnm, 6);
    }

    public String getCiudad() {
        return StringUtils.substring(this.cfbrnm, 0, 3);
    }
}
