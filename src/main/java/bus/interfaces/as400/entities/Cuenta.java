package bus.interfaces.as400.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by josanchez on 28/07/2016.
 * @author by rsalvatierra on 28/04/2017.
 */
@Entity
@Table(name = "TAP00201")
@IdClass(CuentaPK.class)
public class Cuenta implements Serializable {

    @Id
    @Column(name = "DMTYP", columnDefinition = "NUMERIC(1)")
    private Long tipoCuenta;

    @Id
    @Column(name = "DMACCT", columnDefinition = "NUMERIC(10)")
    private Long numeroCuenta;

    @Column(name = "DMPERS", columnDefinition = "VARCHAR(1)")
    private String tipoPersona;

    @Column(name = "DMBRCH", columnDefinition = "NUMERIC(3)")
    private BigDecimal agencia;

    @Column(name = "DMTYPE", columnDefinition = "NUMERIC(3)")
    private BigDecimal producto;

    @Column(name = "DMSTAT", columnDefinition = "CHAR(1)")
    private String estadoCuenta;

    @Column(name = "DMDOPN", columnDefinition = "decimal(7)")
    private BigDecimal fechaCreacion;

    @Column(name = "DMCMCN", columnDefinition = "NUMERIC(3)")
    private Short codigoMoneda;

    public Cuenta(Long tipoCuenta, Long numeroCuenta, String tipoPersona, BigDecimal agencia, BigDecimal producto,
                  String estadoCuenta, Short codigoMoneda, BigDecimal fechaCreacion) {
        this.tipoCuenta = tipoCuenta;
        this.numeroCuenta = numeroCuenta;
        this.tipoPersona = tipoPersona;
        this.agencia = agencia;
        this.producto = producto;
        this.estadoCuenta = estadoCuenta;
        this.codigoMoneda = codigoMoneda;
        this.fechaCreacion = fechaCreacion;
    }

    public Cuenta() {
    }

    public Long getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(Long tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public Long getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(Long numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public BigDecimal getAgencia() {
        return agencia;
    }

    public void setAgencia(BigDecimal agencia) {
        this.agencia = agencia;
    }

    public BigDecimal getProducto() {
        return producto;
    }

    public void setProducto(BigDecimal producto) {
        this.producto = producto;
    }

    public String getEstadoCuenta() {
        return estadoCuenta;
    }

    public void setEstadoCuenta(String estadoCuenta) {
        this.estadoCuenta = estadoCuenta;
    }

    public Short getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(Short codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public BigDecimal getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(BigDecimal fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getTipoCta() {
        String ret = "K";
        int c = getTipoCuenta().intValue();
        switch (c) {
            case 1:
                ret = "A";
                break;
            case 6:
                ret = "C";
                break;
        }
        return ret;
    }

    @Override
    public String toString() {
        return "Cuenta{" +
                "tipoCuenta=" + tipoCuenta +
                ", numeroCuenta=" + numeroCuenta +
                ", tipoPersona='" + tipoPersona + '\'' +
                ", agencia=" + agencia +
                ", producto=" + producto +
                ", estadoCuenta='" + estadoCuenta + '\'' +
                ", fechaCreacion=" + fechaCreacion +
                ", codigoMoneda=" + codigoMoneda +
                '}';
    }
}
