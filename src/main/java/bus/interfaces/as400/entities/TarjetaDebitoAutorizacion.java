package bus.interfaces.as400.entities;

import bus.interfaces.tipos.EstadoTarjetaDebito;
import bus.interfaces.tipos.Pais;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author by rsalvatierra on 27/09/2017.
 */
public class TarjetaDebitoAutorizacion {

    public static String BIN_POR_DEFECTO = "4787860000000000";
    public static String CLIENTE_POR_DEFECTO = "000000";
    public static String PAIS_POR_DEFECTO = Pais.DEFAULT_CODIGO;
    public static String TRANS_DEFECTO = " ";
    public static String TRANS_ACTIVA = "S";
    public static String TRANS_INACTIVA = "N";
    public static String TRANS_COMPRA_INTERTNET = "C";
    public static String TRANS_EXTERIOR = "T";
    public static String FECHA_POR_DEFECTO_TEXTO = "";
    public static String MONTO_POR_DEFECTO_TEXTO = "";

    public static int FECHA_DEFECTO = 0;
    public static double MONTO_POR_DEFECTO = 0;

    //ATNTA5
    private String nroTarjeta;
    //ATPA15
    private String codPais;
    //ATATM5
    private String trnATM = "S";
    //ATINT5
    //private String trnINT="N";
    //ATPOS5
    private String trnPOS = "S";
    //ATCLI5
    private String codCliente;
    //ATEST5
    private String estado = EstadoTarjetaDebito.ACTIVO.getEstado();
    //ATCOD5
    private String codRespuesta = "01"; //Respuesta por defecto para transacciones
    //ATFEA5
    private Integer fechaActivacion;
    //ATFEE5
    private Integer fechaExpiracion;
    //ATITR5
    private String tipoTransaccion;
    //ATIME5
    private Double monto = 0d;

    //ATIAC5
    private Double montoAcumulado = 0d;
    //ATFEAI5
    //private Integer fechaActivacionInternet;
    //ATFEEI5
    //private Integer fechaExpiracionInternet;

    private Integer fechaAlta;
    //ATHOR5
    private Integer horaAlta;
    //ATUSE5
    private String usuario;

    //ATDEV5	CHAR(10)
    private String dispositivo;
    //ATFEC5	TIMESTAMP
    private Date fechaSistema;
    //ATIPAD5	CHAR(15)
    private String ip;
    //ATPGMN5	CHAR(10)
    private String programa;
    //ATJOBN5
    private String trabajo;  //CHAR(6)
    //ATEVE5
    private String evento;  //CHAR(1)

    private String pais;


    private Date fechaDesde = new Date();
    private Date fechaHasta = new Date();

    private Date fechaDesdeInternet = new Date();
    private Date fechaHastaInternet = new Date();

    public TarjetaDebitoAutorizacion() {

    }

    public TarjetaDebitoAutorizacion(String nroTarjeta) {
        this.nroTarjeta = nroTarjeta;
        this.codPais = Pais.DEFAULT_CODIGO;
        this.pais = Pais.DEFAULT_NOMBRE_PAIS;
        this.monto = MONTO_POR_DEFECTO;
    }

    public String getNroTarjeta() {
        return nroTarjeta;
    }

    public void setNroTarjeta(String nroTarjeta) {
        this.nroTarjeta = nroTarjeta;
    }

    public String getCodPais() {
        return codPais;
    }

    public void setCodPais(String codPais) {
        this.codPais = codPais;
    }

    public String getTrnATM() {
        return trnATM;
    }

    public void setTrnATM(String trnATM) {
        this.trnATM = trnATM;
    }

    public String getTrnPOS() {
        return trnPOS;
    }

    public void setTrnPOS(String trnPOS) {
        this.trnPOS = trnPOS;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getEstadoCompraInternet() {
        // Verificar estado transacciones basado en estado de regla, estado de transaccion y intervalo de fechas
        EstadoTarjetaDebito val = EstadoTarjetaDebito.valorEnum(estado);
        if (val == null) return "";
        if (val.equals(EstadoTarjetaDebito.ACTIVO)) {
            Calendar fechaActual = GregorianCalendar.getInstance();
            fechaActual.set(Calendar.HOUR_OF_DAY, 0);
            fechaActual.set(Calendar.MINUTE, 0);
            fechaActual.set(Calendar.SECOND, 0);
            fechaActual.set(Calendar.MILLISECOND, 0);
            if (getFechaHasta() != null && (fechaActual).after(getFechaHasta())) {
                val = EstadoTarjetaDebito.INACTIVO;
            }
        }

        if (EstadoTarjetaDebito.ACTIVO.getEstado().equals(val.getEstado())) {
            return "Tarjeta de d\u00E9bito habilitada";
        } else {
            return "Tarjeta de d\u00E9bito deshabilitada";
        }

    }

    public String getEstadoTransaccionExterior() {
        // Verificar estado transacciones basado en estado de regla, estado de transaccion y intervalo de fechas
        EstadoTarjetaDebito val = EstadoTarjetaDebito.valorEnum(estado);
        if (val == null) return "";
        if (val.equals(EstadoTarjetaDebito.ACTIVO)) {
            Calendar fechaActual = GregorianCalendar.getInstance();
            fechaActual.set(Calendar.HOUR_OF_DAY, 0);
            fechaActual.set(Calendar.MINUTE, 0);
            fechaActual.set(Calendar.SECOND, 0);
            fechaActual.set(Calendar.MILLISECOND, 0);
            if (getFechaHasta() != null && (fechaActual).after(getFechaHasta())) {
                val = EstadoTarjetaDebito.INACTIVO;
            }
        }

        if (EstadoTarjetaDebito.ACTIVO.getEstado().equals(val.getEstado())) {
            return "Tarjeta de d\u00E9bito habilitada";
        } else {
            return "Tarjeta de d\u00E9bito deshabilitada";
        }
    }

    private String montoLimite;

    private String montoLiteral;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodRespuesta() {
        return codRespuesta;
    }

    public void setCodRespuesta(String codRespuesta) {
        this.codRespuesta = codRespuesta;
    }

    public Integer getFechaActivacion() {
        return fechaActivacion;
    }

    public void setFechaActivacion(Integer fechaActivacion) {
        this.fechaActivacion = fechaActivacion;
    }

    public Integer getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Integer fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public Integer getHoraAlta() {
        return horaAlta;
    }

    public void setHoraAlta(Integer horaAlta) {
        this.horaAlta = horaAlta;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Date getFechaDesde() {
        fechaDesde = formatoFecha(fechaActivacion);
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
        this.fechaActivacion = formatoFecha(fechaDesde);
    }

    public Date getFechaHasta() {
        fechaHasta = formatoFecha(fechaExpiracion);
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
        this.fechaExpiracion = formatoFecha(fechaHasta);
    }

    private static final Logger LOG = LoggerFactory.getLogger(TarjetaDebitoAutorizacion.class);
    public static final SimpleDateFormat FECHA_FORMATO = new SimpleDateFormat("yyyyMMdd");
    public static final SimpleDateFormat HORA_FORMATO = new SimpleDateFormat("HHmmss");

    public static final Date formatoFecha(Integer fecha) {
        if (fecha == null || fecha == 0 || fecha == 19000101) {
            return null;
        }
        try {
            return FECHA_FORMATO.parse(fecha.toString());
        } catch (ParseException e) {
            LOG.error("La fecha <{}> (que deberia estar en formato yyyyMMdd) no es valida", fecha.toString());
        }
        return null;
    }

    public static final Integer formatoFecha(Date fecha) {
        if (fecha == null) {
            return null;
        }
        return Integer.parseInt(FECHA_FORMATO.format(fecha));
    }

    public Date getFechaSistema() {
        return fechaSistema;
    }

    public void setFechaSistema(Date fechaSistema) {
        this.fechaSistema = fechaSistema;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public String getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(String trabajo) {
        this.trabajo = trabajo;
    }

    public String getEvento() {
        return evento;
    }

    public void setEvento(String evento) {
        this.evento = evento;
    }

    public String getDispositivo() {
        return dispositivo;
    }

    public void setDispositivo(String dispositivo) {
        this.dispositivo = dispositivo;
    }

    public Integer getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Integer fechaAlta) {
        this.fechaAlta = fechaAlta;
    }


    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Override
    public String toString() {
        StringBuffer str = new StringBuffer();
        str.append("tarjeta.getNroTarjeta()" + getNroTarjeta() + "\n");
        str.append("tarjeta.getCodPais()" + getCodPais() + "\n");
        str.append("tarjeta.getTrnATM()" + getTrnATM() + "\n");
        str.append("tarjeta.getTrnPOS()" + getTrnPOS() + "\n");
        str.append("tarjeta.getCodCliente()" + getCodCliente() + "\n");
        str.append("tarjeta.getEstado()" + getEstado() + "\n");
        str.append("tarjeta.getCodRespuesta()" + getCodRespuesta() + "\n");
        str.append("tarjeta.getFechaActivacion()" + getFechaActivacion() + "\n");
        str.append("tarjeta.getFechaExpiracion()" + getFechaExpiracion() + "\n");
        str.append("ttarjeta.getHoraAlta()" + getHoraAlta() + "\n");
        str.append("tarjeta.getUsuario()" + getUsuario() + "\n");
        str.append("tarjeta.getDispositivo()" + getDispositivo() + "\n");
        str.append("tarjeta.getFechaSistema()" + getFechaSistema() + "\n");
        str.append("tarjeta.getIp()" + getIp() + "\n");
        str.append("tarjeta.getPrograma()" + getPrograma() + "\n");
        str.append("tarjeta.getEvento()" + getEvento() + "\n");
        return str.toString();
    }

    public String getTipoTransaccion() {
        return tipoTransaccion;
    }

    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public String getMontoLimite() {
        if (monto == 0) {
            return "";
        }
        DecimalFormat dec = new DecimalFormat("###0");
        DecimalFormatSymbols decSim = new DecimalFormatSymbols();
        decSim.setDecimalSeparator(',');
        decSim.setGroupingSeparator('.');
        dec.setDecimalFormatSymbols(decSim);
        return dec.format(monto);
    }

    public void setMontoLimite(String montoLimite) {
        this.montoLimite = montoLimite;
    }

    public Double getMontoAcumulado() {
        return montoAcumulado;
    }

    public void setMontoAcumulado(Double montoAcumulado) {
        this.montoAcumulado = montoAcumulado;
    }

}
