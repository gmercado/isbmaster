package bus.interfaces.tipos;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author by rsalvatierra on 28/09/2017.
 */
public enum EstadoTarjetaDebito {
    ACTIVO("A", "Operaciones permitidas"), // "Activo"),
    INACTIVO("I", "Operaciones bloquedas"); // "Inactivo");

    private String estado;
    private String estadoDesc;

    EstadoTarjetaDebito(String estado, String estadoDesc) {
        this.estado = estado;
        this.estadoDesc = estadoDesc;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(String estadoDesc) {
        this.estadoDesc = estadoDesc;
    }


    public static EstadoTarjetaDebito valorEnum(String valor) {
        if (StringUtils.trimToNull(valor) == null) {
            return null;
        }
        List<EstadoTarjetaDebito> estados = new ArrayList<>(Arrays.asList((EstadoTarjetaDebito.values())));
        for (EstadoTarjetaDebito estado : estados) {
            if (StringUtils.trimToEmpty(valor).equalsIgnoreCase(estado.getEstado())) {
                return estado;
            }
        }
        return null;
    }

    public String getDisplayValue() {
        return estadoDesc;
    }


}
