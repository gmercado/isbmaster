package bus.interfaces.tipos;

import java.util.HashMap;

/**
 * 
 * La clase <code>EstadoCivilDao.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 14/05/2013, 18:39:21
 *
 */
public class EstadoCivilDao {

    private static final HashMap<String, String> tipos = new HashMap<String, String>();

    static {
        tipos.put("1", "SOLTERO");
        tipos.put("2", "CASADO");
        tipos.put("3", "DIVORCIADO");
        tipos.put("4", "VIUDO");
        tipos.put("5", "CONCUBINO");
    }
            
    public static HashMap<String, String> getTipos(){
        return tipos;
    }
}
