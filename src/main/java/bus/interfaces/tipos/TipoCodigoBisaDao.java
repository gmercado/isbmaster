package bus.interfaces.tipos;

import java.util.HashMap;

public class TipoCodigoBisaDao {

    private static final HashMap<String, String> tipos = new HashMap<String, String>();

    static {
        tipos.put("MTX10", "Tarjeta de Coordenadas");
        tipos.put("DPGO3", "Token");
        }
            
    public static HashMap<String, String> getTipos(){
        return tipos;
    }
    
    public static boolean isToken(String tipo){
        return "DPGO3".equals(tipo);
    }
    public static boolean isTarjetaCoordenadas(String tipo){
        return "MTX10".equals(tipo);
    }
}
