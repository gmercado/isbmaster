package bus.interfaces.tipos;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class TipoSexoDao {

    private static final Map<String, String> tipos = 
            ImmutableMap.<String, String>of("M", "Masculino", "F", "Femenino");
    
    public static Map<String, String> getTipos(){
        return tipos;
    }
}
