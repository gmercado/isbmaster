package bus.interfaces.tipos;

import java.util.HashMap;

/**
 * 
 * La clase <code>TipoProductosDao.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 15/05/2013, 17:52:13
 *
 */
public class TipoProductosDao {

    private static final HashMap<String, String> tipos = new HashMap<String, String>();

    static {
        tipos.put("1", "Cuenta Libreta Inversi�n en Bolivianos");
        tipos.put("2", "Cuenta Libreta Inversi�n en D�lares");
    }
            
    public static HashMap<String, String> getTipos(){
        return tipos;
    }
}
