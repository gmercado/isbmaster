package bus.interfaces.tipos;

/**
 * @author by rsalvatierra on 05/09/2017.
 */
public enum TipoCotizacion {
    Oficial,
    Compra,
    Venta;
}
