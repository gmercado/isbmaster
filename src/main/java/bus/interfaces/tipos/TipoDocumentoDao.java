package bus.interfaces.tipos;

public enum TipoDocumentoDao {
    CLP("C\u00E9dula La Paz", "LP", "CLP", 0),

    CSC("C\u00E9dula Santa Cruz", "SC", "CSC", 1),

    CCB("C\u00E9dula Cochabamba", "CB", "CCB", 2),

    CCH("C\u00E9dula Chuquisaca", "CH", "CCH", 3),

    CTJ("C\u00E9dula Tarija", "TJ", "CTJ", 4),

    COR("C\u00E9dula Oruro", "OR", "COR", 5),

    CPO("C\u00E9dula Potos\u00ED", "PO", "CPO", 6),

    CPA("C\u00E9dula Pando", "PA", "CPA", 7),

    CBE("C\u00E9dula Beni", "BE", "CBE", 8),

    NIT("N\u00FAmero Id. Tributaria", "NIT", "NIT", 9),

    CEX("Carnet Extranjero", "CEX", "CEX", 10),

    PAS("Pasaporte", "PAS", "PAS", 11);

    private final String descripcion;
    private final String abreviacion;
    private final String codigo;
    private final int secuencia;

    TipoDocumentoDao(String descripcion, String abreviacion, String codigo, int secuencia) {
        this.descripcion = descripcion;
        this.abreviacion = abreviacion;
        this.codigo = codigo;
        this.secuencia = secuencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getAbreviacion() {
        return abreviacion;
    }

    public String getCodigo() {
        return codigo;
    }

    public Integer getSecuencia() {
        return secuencia;
    }

}
