package bus.interfaces.tipos;

/**
 * @author by rsalvatierra on 28/09/2017.
 */
public class Pais {

    public static String DEFAULT_CODIGO = "000";
    public static String DEFAULT_NOMBRE_PAIS = "Todos los paises";

    private String codigoPais;
    private String nombrePais;

    public Pais() {
        this.codigoPais = DEFAULT_CODIGO;
        this.nombrePais = DEFAULT_NOMBRE_PAIS;
    }

    public Pais(String codigo, String descripcion) {
        this.codigoPais = codigo;
        this.nombrePais = descripcion;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public String getNombrePais() {
        return nombrePais;
    }

    public void setNombrePais(String nombrePais) {
        this.nombrePais = nombrePais;
    }


    public String getDisplayValue() {
        return nombrePais;
    }

}
