package bus.interfaces.tipos;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public enum TipoDepartamento {
    LP("La Paz", "LP", "La Paz"),

    SC("Santa Cruz", "SC", "Santa Cruz"),

    CB("Cochabamba", "CB", "Cochabamba"),

    CH("Chuquisaca", "CH", "Chuquisaca"),

    TJ("Tarija", "TJ", "Tarija"),

    OR("Oruro", "OR", "Oruro"),

    PO("Potos\u00ED", "PT", "Potosi"),

    PA("Pando", "PA", "Pando"),

    BE("Beni", "BN", "Beni");

    private final String descripcion;
    private final String codigo;
    private final String match;

    TipoDepartamento(String descripcion, String codigo, String match) {
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.match = match;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getMatch() {
        return match;
    }

    public static TipoDepartamento get(String codigo) {
        for (TipoDepartamento valor : TipoDepartamento.values()) {
            if (valor.getMatch().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }
}
