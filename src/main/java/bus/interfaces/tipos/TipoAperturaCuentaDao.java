package bus.interfaces.tipos;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class TipoAperturaCuentaDao {

	
    private static final Map<String, String> tipos =
        ImmutableMap.<String, String>of(
                "N", "Apertura normal de una cuenta para uso personal.",
                "P", "La empresa donde trabajo me ha pedido abrir una cuenta para poder utilizar el servicio de Pago de Planillas del BISA."
        );

public static Map<String, String> getTipos(){
    return tipos;
}
	
}


