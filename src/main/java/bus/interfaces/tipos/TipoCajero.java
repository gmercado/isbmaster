package bus.interfaces.tipos;

/**
 * @author by rsalvatierra on 05/09/2017.
 */
public enum TipoCajero {
    Sucursal,
    Cajero;

    public static TipoCajero get(String codigo) {
        for (TipoCajero valor : TipoCajero.values()) {
            if (valor.name().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }
}
