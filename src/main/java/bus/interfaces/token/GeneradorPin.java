/*
 *  Copyright 2010 Banco Bisa S.A.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.interfaces.token;

import org.apache.commons.lang.StringUtils;
import org.jasypt.digest.config.SimpleDigesterConfig;
import org.jasypt.salt.RandomSaltGenerator;
import org.jasypt.util.password.ConfigurablePasswordEncryptor;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * @author Marcelo Morales
 * @author by rsalvatierra
 */
public class GeneradorPin {

    private static final String SHA_256 = "SHA-256";
    private static final String SHA1_PRNG = "SHA1PRNG";
    private SecureRandom secureRandom;
    private ConfigurablePasswordEncryptor encryptor;

    public GeneradorPin() throws NoSuchAlgorithmException {
        secureRandom = SecureRandom.getInstance(SHA1_PRNG);
        encryptor = new ConfigurablePasswordEncryptor();
        encryptor.setAlgorithm(SHA_256);
        SimpleDigesterConfig dc = new SimpleDigesterConfig();
        dc.setIterations(1000);
        dc.setSaltGenerator(new RandomSaltGenerator(SHA1_PRNG));
        dc.setSaltSizeBytes(4);
        dc.setAlgorithm(SHA_256);
        encryptor.setConfig(dc);
    }

    public ParPines nuevoPin(int digitos) {
        int i = secureRandom.nextInt(
                Integer.parseInt("1" + StringUtils.leftPad("", digitos, '0')));
        String pinCrudo = StringUtils.leftPad(String.valueOf(i), digitos, '0');
        String pinHasheado = encryptor.encryptPassword(pinCrudo);
        return new ParPines(pinCrudo, pinHasheado);
    }

    /**
     * Devuelve true si el digitado verifica contra alguno de los hashes.
     *
     * @param digitado el dato digitado
     * @param hashed   los datos en hash
     * @return true si alguno verifica.
     */
    public boolean verificar(String digitado, String... hashed) {
        for (String s : hashed) {
            if (s == null) {
                continue;
            }
            if (encryptor.checkPassword(digitado, s)) {
                return true;
            }
        }
        return false;
    }
}
