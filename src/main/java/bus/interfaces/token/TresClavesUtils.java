/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package bus.interfaces.token;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.NoSuchAlgorithmException;
import java.util.Date;

import static java.lang.Integer.parseInt;

/**
 * @author Jheyson Sanchez @since 22/08/16
 * @author by rsalvatierra
 */
public final class TresClavesUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(TresClavesUtils.class);

    private TresClavesUtils() {
    }

    public static ParPines actualizarClaveMovil(TresClaves tc,
                                                int cantidadDeDigitos,
                                                String variableHorasEntrePedidos) throws NoSuchAlgorithmException {
        GeneradorPin generadorPin = new GeneradorPin();
        ParPines nuevoPin = generadorPin.nuevoPin(cantidadDeDigitos);

        if (tc.getFechaSolicitudToken() == null) {
            resetClave(tc, nuevoPin.getPinHasheado());
            tc.setFechaSolicitudToken(new Date());
        } else {
            int horas;
            try {
                horas = parseInt(variableHorasEntrePedidos);
            } catch (NumberFormatException e) {
                LOGGER.error("La variable {} no esta en formato correcto, debe ser un entero", variableHorasEntrePedidos);
                horas = 3;
            }

            if (new DateTime().isBefore(new DateTime(tc.getFechaSolicitudToken()).plusHours(horas))) {
                LOGGER.debug("No han pasado {} horas de peticion de clave al giro {}, anadiendo", horas, tc);
                addClave(tc, nuevoPin.getPinHasheado());
            } else {
                resetClave(tc, nuevoPin.getPinHasheado());
                tc.setFechaSolicitudToken(new Date());
            }
        }
        return nuevoPin;
    }

    private static void addClave(TresClaves tc, String nc) {
        if (tc.getTokenSMS() == null) {
            tc.setTokenSMS(nc);
        }
    }

    private static void resetClave(TresClaves tc, String nc) {
        tc.setTokenSMS(nc);
    }

}
