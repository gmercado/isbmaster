package bus.interfaces.sqlite.dao;

import bus.env.api.MedioAmbiente;
import bus.interfaces.sqlite.entities.Cajero;
import bus.interfaces.sqlite.entities.Contacto;
import bus.interfaces.tipos.TipoCajero;
import com.google.inject.Inject;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.openlogics.gears.jdbc.DataStoreFactory;
import org.openlogics.gears.jdbc.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static bus.env.api.Variables.CONEXION_SQLITE;
import static bus.env.api.Variables.CONEXION_SQLITE_DEFAULT;

/**
 * @author by rsalvatierra on 04/09/2017.
 */
public class CajeroDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(CajeroDao.class);

    private static final String ORG_SQLITE_JDBC = "org.sqlite.JDBC";
    private static final String JDBC_SQLITE = "jdbc:sqlite:";
    private final MedioAmbiente medioAmbiente;

    @Inject
    public CajeroDao(MedioAmbiente medioAmbiente) {
        this.medioAmbiente = medioAmbiente;
    }

    static {
        try {
            Class.forName(ORG_SQLITE_JDBC);
        } catch (ClassNotFoundException e) {
            LOGGER.error("error al cargar SQLite", e);
        }
    }

    public List<Cajero> getCajeros(TipoCajero tipoCajero) {
        org.openlogics.gears.jdbc.ObjectDataStore ds;
        List<Cajero> cajeros = new ArrayList<>();
        try {
            String URI = medioAmbiente.getValorDe(CONEXION_SQLITE, CONEXION_SQLITE_DEFAULT);
            LOGGER.debug("conexion SQLITE");
            ds = DataStoreFactory.createObjectDataStore(DriverManager.getConnection(JDBC_SQLITE + URI));
            ds.setAutoClose(true);
            List<Map<String, Object>> map = ds.select(Query.of("SELECT * FROM CAJEROS WHERE tipo=?", tipoCajero.name()), new MapListHandler());
            for (Map<String, Object> stringObjectMap : map) {
                Cajero cajero = new Cajero();
                cajero.set_id((int) stringObjectMap.get("_id"));
                cajero.setLatitud((double) stringObjectMap.get("lat"));
                cajero.setLongitud((double) stringObjectMap.get("long"));
                cajero.setDepa((String) stringObjectMap.get("departamento"));
                cajero.setDepartamento((String) stringObjectMap.get("departamento"));
                cajero.setCiudad((String) stringObjectMap.get("ciudad"));
                cajero.setDireccion((String) stringObjectMap.get("direccion"));
                cajero.setTelefonos((String) stringObjectMap.get("telf"));
                cajero.setHorarios((String) stringObjectMap.get("horarios"));
                cajero.setDepobisa((String) stringObjectMap.get("depobisa"));
                cajero.setDolares((String) stringObjectMap.get("dolares"));
                cajero.setTipo((String) stringObjectMap.get("tipo"));
                cajero.setDescripcion((String) stringObjectMap.get("descripcion"));
                cajeros.add(cajero);
                LOGGER.info("{} {}", cajero.get_id(), cajero.getDescripcion());
            }
        } catch (SQLException ex) {
            LOGGER.error("Error al conectarse al SQLITE ", ex);
        }
        return cajeros;
    }

    public List<Contacto> getContactos() {
        org.openlogics.gears.jdbc.ObjectDataStore ds;
        List<Contacto> contactos = new ArrayList<>();
        try {
            String URI = medioAmbiente.getValorDe(CONEXION_SQLITE, CONEXION_SQLITE_DEFAULT);
            LOGGER.info("conexion SQLITE");
            ds = DataStoreFactory.createObjectDataStore(DriverManager.getConnection(JDBC_SQLITE + URI));
            ds.setAutoClose(true);
            List<Map<String, Object>> map = ds.select(Query.of("SELECT * FROM EMERGENCIAS"), new MapListHandler());
            for (Map<String, Object> stringObjectMap : map) {
                Contacto contacto = new Contacto();
                contacto.set_id((int) stringObjectMap.get("_id"));
                contacto.setReferencia((String) stringObjectMap.get("problema"));
                contacto.setTelefono((String) stringObjectMap.get("numero"));
                contactos.add(contacto);
            }
        } catch (SQLException ex) {
            LOGGER.error("Error al conectarse al SQLITE ", ex);
        }
        return contactos;
    }
}