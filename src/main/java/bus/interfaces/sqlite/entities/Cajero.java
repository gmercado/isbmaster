package bus.interfaces.sqlite.entities;

import bus.interfaces.as400.entities.AgenciaCajero;
import bus.interfaces.tipos.TipoCajero;
import bus.interfaces.tipos.TipoDepartamento;

/**
 * @author by rsalvatierra on 05/09/2017.
 */
public class Cajero {
    private int _id;
    private double latitud;
    private double longitud;
    private TipoDepartamento departamento;
    private String ciudad;
    private String telefonos;
    private TipoCajero tipo;
    private String horarios;
    private String direccion;
    private String dolares;
    private String depobisa;
    private String descripcion;
    private String depa;

    public Cajero() {
    }

    public Cajero(AgenciaCajero agenciaCajero) {
        if (TipoCajero.Sucursal.equals(agenciaCajero.getTipo())) {
            set_id(Math.toIntExact(agenciaCajero.getCodigoAgencia()));
        } else {
            set_id(Math.toIntExact(agenciaCajero.getId()));
        }
        setLatitud(agenciaCajero.getLatitud());
        setLongitud(agenciaCajero.getLongitud());
        setDepartamento(agenciaCajero.getDepartamento());
        setCiudad(agenciaCajero.getCiudad());
        setTelefonos(agenciaCajero.getTelefonos());
        setHorarios(agenciaCajero.getHorarios());
        setTipo(agenciaCajero.getTipo().name());
        setDireccion(agenciaCajero.getDireccion());
        setDolares(agenciaCajero.getDolares());
        setDepobisa(agenciaCajero.getDepobisa());
        setDescripcion(agenciaCajero.getNombre());
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public TipoDepartamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = TipoDepartamento.get(departamento);
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(String telefonos) {
        this.telefonos = telefonos;
    }

    public TipoCajero getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = TipoCajero.get(tipo);
    }

    public String getHorarios() {
        return horarios;
    }

    public void setHorarios(String horarios) {
        this.horarios = horarios;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDolares() {
        return dolares;
    }

    public void setDolares(String dolares) {
        this.dolares = dolares;
    }

    public String getDepobisa() {
        return depobisa;
    }

    public void setDepobisa(String depobisa) {
        this.depobisa = depobisa;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDepa() {
        return depa;
    }

    public void setDepa(String depa) {
        this.depa = depa;
    }

    @Override
    public String toString() {
        return "Cajero{" +
                "_id=" + _id +
                ", latitud=" + latitud +
                ", longitud=" + longitud +
                ", departamento='" + departamento + '\'' +
                ", ciudad='" + ciudad + '\'' +
                ", telefonos='" + telefonos + '\'' +
                ", tipo='" + tipo + '\'' +
                ", horarios='" + horarios + '\'' +
                ", direccion='" + direccion + '\'' +
                ", dolares='" + dolares + '\'' +
                ", depobisa='" + depobisa + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
