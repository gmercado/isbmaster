package bus.interfaces.sqlite.entities;

/**
 * @author by rsalvatierra on 05/09/2017.
 */
public class Contacto {
    private int _id;
    private String telefono;
    private String referencia;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
}
