package bus.interfaces.codigobisa.dao;

public class Dispositivo{
    private String tarjeta;
    private Integer estado;
    private String tipo;
    private Integer agencia;
    private String nombreAgencia;
    
    public Dispositivo() {
    }
    
    public Dispositivo(String tarjeta, Integer estado, String tipo, Integer agencia, String nombreAgencia) {
        this.tarjeta = tarjeta;
        this.estado = estado;
        this.tipo = tipo;
        this.agencia = agencia;
        this.nombreAgencia = nombreAgencia;
    }
    public String getTarjeta() {
        return tarjeta;
    }
    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }
    public Integer getEstado() {
        return estado;
    }
    public void setEstado(Integer estado) {
        this.estado = estado;
    }
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public Integer getAgencia() {
        return agencia;
    }
    public void setAgencia(Integer agencia) {
        this.agencia = agencia;
    }
    public String getNombreAgencia() {
        return nombreAgencia;
    }
    public void setNombreAgencia(String nombreAgencia) {
        this.nombreAgencia = nombreAgencia;
    }
    
}
