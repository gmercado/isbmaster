package bus.interfaces.codigobisa.dao;

import bus.consumoweb.consumer.ConsumidorServiciosWeb;
import bus.consumoweb.consumer.DefaultHandlerRespuesta;
import bus.consumoweb.model.ConsumidorCodigoBisa;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Resources;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static com.google.common.base.Charsets.US_ASCII;


/**
 * Modificacion David Cuevas
 * 17/04/2013
 * 
 * @author Marcelo Morales
 *         Date: 12/3/12
 *     
 */
public class CodigoBisaServDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(CodigoBisaServDao.class);

    private final ConsumidorServiciosWeb consumidorServiciosWeb;

    @Inject
    public CodigoBisaServDao(@ConsumidorCodigoBisa ConsumidorServiciosWeb consumidorServiciosWeb) {
        this.consumidorServiciosWeb = consumidorServiciosWeb;
    }

    public String generaCodigoBisa(String aNombreDe, Integer nroCliente, String celular, String tipoDispositivo,
                                   Integer ebisaId, String ebisaLogin, BigInteger codigoBisa, Integer custodioId)
            throws IOException, SAXException, TimeoutException, ExecutionException {

        Map<String, String> interpolar = ImmutableMap.<String, String>builder().
                put("path", "s/indos/generaCodigoBisa/" + aNombreDe).
                put("content-type", "application/xml").
                put("nroCliente", nroCliente == null ? "" : nroCliente.toString()).
                put("celular", celular).
                put("tipoDispositivo", tipoDispositivo).
                put("ebisaId", ebisaId == null ? "" : ebisaId.toString()).
                put("ebisaLogin", ebisaLogin).
                put("codigoBisa", codigoBisa == null ? "" : ("<codigoBisa>" + codigoBisa.toString() + "</codigoBisa>")).
                put("custodioId", custodioId == null ? "" : ("<custodioId>" + custodioId.toString() + "</custodioId>")).
                build();
        String s = Resources.toString(Resources.getResource(CodigoBisaServDao.class, "generaCodigoBisa.xml"), US_ASCII);
        Object consumir = consumidorServiciosWeb.consumir(s, interpolar, new DefaultHandlerRespuesta() {

            @Override
            public String getRespuesta(Object o) {
                return o.toString();
            }
        });

        return consumir.toString();
    }

    /**
     * Retornara una excepcion que debe ser atrapada en caso de error
     * 
     * @param codCliente
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws ErrorCodigobisa
     */
    public LinkedList<LinkedHashMap<String, String>> consultaDispositivo(Long codCliente) throws IOException, SAXException, ErrorCodigobisa, TimeoutException, ExecutionException {
        Map<String, String> interpolar = ImmutableMap.of("path", "s/indos/consultaDispositivo/" + codCliente);
        Object consumir = consumidorServiciosWeb.consumir(null, interpolar, new DefaultHandlerRespuesta() {

            @Override
            public String getRespuesta(Object o) {
                if (o instanceof String) {
                    return (String) o;
                }

                if (o instanceof Document) {
                    return "OK";
                }
                return "desconocido " + o;
            }
        });

        if (consumir instanceof Document) {
            return toMap((Document) consumir);
        }

        throw new ErrorCodigobisa(consumir.toString());
    }


    /**
     * 
     * @param ci
     * @param exp
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws ErrorCodigobisa
     */
    public LinkedList<LinkedHashMap<String, String>> consultaDispositivo(String ci, String exp) throws IOException, SAXException, ErrorCodigobisa, TimeoutException, ExecutionException {
        Map<String, String> interpolar = ImmutableMap.of("path", "s/indos/consultaDispositivo/" + ci + "/" + exp);
        Object consumir = consumidorServiciosWeb.consumir(null, interpolar, new DefaultHandlerRespuesta() {

            @Override
            public String getRespuesta(Object o) {
                if (o instanceof String) {
                    return (String) o;
                }

                if (o instanceof Document) {
                    return "OK";
                }
                return "desconocido " + o;
            }
        });

        if (consumir instanceof Document) {
            return toMap((Document) consumir);
        }

        throw new ErrorCodigobisa(consumir.toString());
    }
    
    public LinkedList<LinkedHashMap<String, String>> consultaCustodio(Long codigoAgencia) throws IOException, SAXException, ErrorCodigobisa, TimeoutException, ExecutionException {
        Map<String, String> interpolar = ImmutableMap.of("path", "s/indos/consultaCustodio/" + codigoAgencia);
        Object consumir = consumidorServiciosWeb.consumir(null, interpolar, new DefaultHandlerRespuesta() {

            @Override
            public String getRespuesta(Object o) {
                if (o instanceof String) {
                    return (String) o;
                }

                if (o instanceof Document) {
                    return "OK";
                }

                return "desconocido " + o;
            }
        });

        if (consumir instanceof Document) {
            return toMap((Document) consumir);
        }

        throw new ErrorCodigobisa(consumir.toString());
    }

    private LinkedList<LinkedHashMap<String, String>> toMap(Document consumir) {
        final LinkedList<LinkedHashMap<String, String>> maps = new LinkedList<LinkedHashMap<String, String>>();
        NodeList childNodes = ((Document) consumir).getDocumentElement().getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node item = childNodes.item(i);
            LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
            NodeList childNodes1 = item.getChildNodes();
            for (int j = 0; j < childNodes1.getLength(); j++) {
                Node internal = childNodes1.item(j);
                map.put(((Element) internal).getTagName(), internal.getTextContent());
            }

            maps.add(map);
        }
        return maps;
    }
}
