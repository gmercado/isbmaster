package bus.interfaces.codigobisa.dao;

/**
 * @author Marcelo Morales
 *         Date: 12/3/12
 */
public class ErrorCodigobisa extends Exception {

    public ErrorCodigobisa(String message) {
        super(message);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
