package bus.interfaces.ebisa.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author by rsalvatierra on 21/08/2017.
 */
public class ExchangeRateId implements Serializable {

    private static final long serialVersionUID = 7400576549842071404L;

    private String currCode;

    private Date fecha;

    public ExchangeRateId() {
    }

    public ExchangeRateId(String currCode, Date fecha) {
        this.currCode = currCode;
        this.fecha = fecha;
    }

    public String getCurrCode() {
        return currCode;
    }

    public void setCurrCode(String currCode) {
        this.currCode = currCode;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof ExchangeRateId))
            return false;

        ExchangeRateId that = (ExchangeRateId) o;

        if (currCode != null ? !currCode.equals(that.currCode) : that.currCode != null)
            return false;
        if (fecha != null ? !fecha.equals(that.fecha) : that.fecha != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = currCode != null ? currCode.hashCode() : 0;
        result = 31 * result + (fecha != null ? fecha.hashCode() : 0);
        return result;
    }
}
