package bus.interfaces.ebisa.tipos;

/**
 * @author by rsalvatierra on 28/06/2017.
 */
public enum TipoCuentaBeneficiario {
    OTRO_BANCO,
    CAJA_AHORRO,
    NO_USADO2,
    NO_USADO3,
    NO_USADO4,
    NO_USADO5,
    CUENTA_CORRIENTE

}
