package bus.interfaces.ebisa.tipos;

/**
 * @author by rsalvatierra on 21/08/2017.
 */
public enum TransaccionEstado {
    /**
     * Rechazada por usuario
     */
    REU("Rechazada por un usuario", "Rechazada", 5L),

    /**
     * Rechazada por sistema
     */
    RES("Rechazada por el sistema", "Rechazada", 6L),

    /**
     * Procesada
     */
    PRO("Procesada exitosamente", "Procesada", 4L),

    /**
     * Pendiente
     */
    PEN("Pendiente de aprobación", "Pendiente aprobacion", 2L),

    /**
     * Anulada
     */
    ANU("Anulada", "Anulada", 9L),

    /**
     * Aprobada pero en proceso
     */
    APR("Aprobada y enviada a proceso", "Aprobada y enviada a proceso", 3L);

    private final String descripcion;
    private final String descripcionCorta;
    private final Long codigoFormularioPcc01;

    TransaccionEstado(String descripcion, String descripcionCorta, Long codigoFormularioPcc01) {
        this.descripcion = descripcion;
        this.descripcionCorta = descripcionCorta;
        this.codigoFormularioPcc01 = codigoFormularioPcc01;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getDescripcionCorta() {
        return descripcionCorta;
    }

    public Long getCodigoFormularioPcc01() {
        return codigoFormularioPcc01;
    }
}
