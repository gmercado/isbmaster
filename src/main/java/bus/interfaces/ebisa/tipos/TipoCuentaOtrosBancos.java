package bus.interfaces.ebisa.tipos;

/**
 * @author by rsalvatierra on 05/07/2017.
 */
public enum TipoCuentaOtrosBancos {
    C("CUENTA"),
    P("PRESTAMO"),
    T("TARJETA");
    private String descripcion;

    TipoCuentaOtrosBancos(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
