package bus.interfaces.ebisa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * The persistent class for the tbl_Function database table.
 */
@Entity
@Table(name = "tbl_Function")
public class Tbl_Function implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "FunctionID")
    private String functionID;

    @Column(name = "Authorizeable")
    private boolean authorizeable;

    @Column(name = "Description")
    private String description;

    @Column(name = "MoneyUse")
    private boolean moneyUse;

    @Column(name = "TramPermiso")
    private boolean tramPermiso;

    @Column(name = "TramAutorizacion")
    private boolean tramAutorizacion;

    public Tbl_Function() {
    }

    public String getFunctionID() {
        return this.functionID;
    }

    public void setFunctionID(String functionID) {
        this.functionID = functionID;
    }

    public boolean getAuthorizeable() {
        return this.authorizeable;
    }

    public void setAuthorizeable(boolean authorizeable) {
        this.authorizeable = authorizeable;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getMoneyUse() {
        return this.moneyUse;
    }

    public void setMoneyUse(boolean moneyUse) {
        this.moneyUse = moneyUse;
    }

    public boolean isTramPermiso() {
        return tramPermiso;
    }

    public void setTramPermiso(boolean tramPermiso) {
        this.tramPermiso = tramPermiso;
    }

    public boolean isTramAutorizacion() {
        return tramAutorizacion;
    }

    public void setTramAutorizacion(boolean tramAutorizacion) {
        this.tramAutorizacion = tramAutorizacion;
    }


}