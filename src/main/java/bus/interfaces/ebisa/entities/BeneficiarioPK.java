package bus.interfaces.ebisa.entities;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 28/06/2017.
 */
public class BeneficiarioPK implements Serializable {

    private static final long serialVersionUID = -5057900838858082094L;

    private BigDecimal codigoCliente;

    private String codigoBeneficiario;

    public BigDecimal getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(BigDecimal codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getCodigoBeneficiario() {
        return codigoBeneficiario;
    }

    public void setCodigoBeneficiario(String codigoBeneficiario) {
        this.codigoBeneficiario = codigoBeneficiario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof BeneficiarioPK))
            return false;

        BeneficiarioPK that = (BeneficiarioPK) o;

        if (codigoBeneficiario != null ? !codigoBeneficiario.equals(that.codigoBeneficiario) : that.codigoBeneficiario != null)
            return false;
        if (codigoCliente != null ? !codigoCliente.equals(that.codigoCliente) : that.codigoCliente != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codigoCliente != null ? codigoCliente.hashCode() : 0;
        result = 31 * result + (codigoBeneficiario != null ? codigoBeneficiario.hashCode() : 0);
        return result;
    }
}
