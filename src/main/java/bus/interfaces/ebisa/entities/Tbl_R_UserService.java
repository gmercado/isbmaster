package bus.interfaces.ebisa.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;


/**
 * The persistent class for the tbl_R_UserService database table.
 * 
 */
@Entity
@Table(name="tbl_R_UserService")
public class Tbl_R_UserService implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ServiceID")
	private long serviceID;

	@Column(name="CreationDate")
	private Timestamp creationDate;

	@Column(name="Deleted")
	private boolean deleted;

	//bi-directional many-to-one association to Tbl_User
    @ManyToOne
	@JoinColumn(name="UserID")
	private Tbl_User tblUser;

	//bi-directional many-to-one association to Tbl_Service
	@OneToMany(mappedBy="tblRUserService")
	private Set<Tbl_Service> tblServices;

    public Tbl_R_UserService() {
    }

	public long getServiceID() {
		return this.serviceID;
	}

	public void setServiceID(long serviceID) {
		this.serviceID = serviceID;
	}

	public Timestamp getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Tbl_User getTblUser() {
		return this.tblUser;
	}

	public void setTblUser(Tbl_User tblUser) {
		this.tblUser = tblUser;
	}
	
	public Set<Tbl_Service> getTblServices() {
		return this.tblServices;
	}

	public void setTblServices(Set<Tbl_Service> tblServices) {
		this.tblServices = tblServices;
	}
	
}