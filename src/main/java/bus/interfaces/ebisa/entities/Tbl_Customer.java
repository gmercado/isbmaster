package bus.interfaces.ebisa.entities;

import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Set;

/**
 * The persistent class for the tbl_Customer database table.
 */
@Entity
@Table(name = "tbl_Customer")
public class Tbl_Customer implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CustomerID")
    private long customerID;

    @Column(name = "ActiveAdmin")
    private boolean activeAdmin;

    @Column(name = "CardCreationDate")
    private Timestamp cardCreationDate;

    @Column(name = "CreationDate")
    private Timestamp creationDate;

    @Column(name = "Customer")
    private String customer;

    @Column(name = "CustomerTypeID")
    private BigDecimal customerTypeID;

    @Column(name = "Deleted")
    private boolean deleted;

    @Column(name = "DeletionDate")
    private Timestamp deletionDate;

    @Column(name = "Disabled")
    private boolean disabled;

    @Column(name = "HabilitationDate")
    private Timestamp habilitationDate;

    @Column(name = "ModificationDate")
    private Timestamp modificationDate;

    @Column(name = "Name")
    private String name;

    @Column(name = "NroTarjeta")
    private String nroTarjeta;

    @Column(name = "UserHabilitation")
    private String userHabilitation;

    @Column(name = "UserIDAdmin")
    private BigDecimal userIDAdmin;

    @Column(name = "UserModification")
    private String userModification;

    // bi-directional many-to-one association to Tbl_User
    @OneToMany(mappedBy = "tblCustomer")
    private Set<Tbl_User> tblUsers;

    public Tbl_Customer() {
    }

    public long getCustomerID() {
        return this.customerID;
    }

    public void setCustomerID(long customerID) {
        this.customerID = customerID;
    }

    public boolean getActiveAdmin() {
        return this.activeAdmin;
    }

    public void setActiveAdmin(boolean activeAdmin) {
        this.activeAdmin = activeAdmin;
    }

    public Timestamp getCardCreationDate() {
        return this.cardCreationDate;
    }

    public void setCardCreationDate(Timestamp cardCreationDate) {
        this.cardCreationDate = cardCreationDate;
    }

    public Timestamp getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public String getCustomer() {
        return this.customer;
    }
    
    public String getCustomerSinCeros() {
        // incorporamos una excepcion para el codigo de cliente de ebisa
        // para eliminar los ceros por izquierda
        if (!StringUtils.isEmpty(this.getCustomer())) {
            return this.getCustomer().replaceFirst("^0*", "") ;
        }
        return this.customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public BigDecimal getCustomerTypeID() {
        return this.customerTypeID;
    }

    public void setCustomerTypeID(BigDecimal customerTypeID) {
        this.customerTypeID = customerTypeID;
    }

    public boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Timestamp getDeletionDate() {
        return this.deletionDate;
    }

    public void setDeletionDate(Timestamp deletionDate) {
        this.deletionDate = deletionDate;
    }

    public boolean getDisabled() {
        return this.disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public Timestamp getHabilitationDate() {
        return this.habilitationDate;
    }

    public void setHabilitationDate(Timestamp habilitationDate) {
        this.habilitationDate = habilitationDate;
    }

    public Timestamp getModificationDate() {
        return this.modificationDate;
    }

    public void setModificationDate(Timestamp modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNroTarjeta() {
        return this.nroTarjeta;
    }

    public void setNroTarjeta(String nroTarjeta) {
        this.nroTarjeta = nroTarjeta;
    }

    public String getUserHabilitation() {
        return this.userHabilitation;
    }

    public void setUserHabilitation(String userHabilitation) {
        this.userHabilitation = userHabilitation;
    }

    public BigDecimal getUserIDAdmin() {
        return this.userIDAdmin;
    }

    public void setUserIDAdmin(BigDecimal userIDAdmin) {
        this.userIDAdmin = userIDAdmin;
    }

    public String getUserModification() {
        return this.userModification;
    }

    public void setUserModification(String userModification) {
        this.userModification = userModification;
    }

    public Set<Tbl_User> getTblUsers() {
        return this.tblUsers;
    }

    public void setTblUsers(Set<Tbl_User> tblUsers) {
        this.tblUsers = tblUsers;
    }
    


}