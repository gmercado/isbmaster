package bus.interfaces.ebisa.entities;

import bus.interfaces.as400.entities.BancoDestino;
import bus.interfaces.ebisa.tipos.TipoCuentaBeneficiario;
import bus.interfaces.ebisa.tipos.TipoCuentaOtrosBancos;
import org.apache.openjpa.persistence.FetchAttribute;
import org.apache.openjpa.persistence.FetchGroup;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author by rsalvatierra on 28/06/2017.
 */
@Entity
@Table(name = "LPP03")
@IdClass(BeneficiarioPK.class)
@FetchGroup(name = Beneficiario.INCLUIR_TIPO_PAGO,
        attributes = {@FetchAttribute(name = "tipoPago", recursionDepth = 2),
                @FetchAttribute(name = "estado", recursionDepth = 2)})
public class Beneficiario implements Serializable {

    static final String INCLUIR_TIPO_PAGO = "incluir tipo pago";
    private static final long serialVersionUID = -3480417860299372946L;

    @Id
    @Column(name = "L03CODCLI")
    private BigDecimal codigoCliente;

    @Id
    @Column(name = "L03CODBEN", length = 20)
    private String codigoBeneficiario;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "L03BNKBEN")
    private BancoDestino bancoBeneficiario;

    @Column(name = "L03CTABEN", length = 20)
    private String cuentaBeneficiario;

    @Column(name = "L03TPOCTA")
    @Enumerated(EnumType.ORDINAL)
    private TipoCuentaBeneficiario tipoCuentaBeneficiario;

    @Column(name = "L03MONCTA")
    private Short moneda;

    @Column(name = "L03NOMCORT", length = 60)
    private String nombreCorto;

    @Column(name = "L03NOMCOMP", length = 60)
    private String nombreCompleto;

    @Column(name = "L03NOMBRES", length = 60)
    private String nombres;

    @Column(name = "L03APPPAT", length = 60)
    private String paterno;

    @Column(name = "L03APPMAT", length = 60)
    private String materno;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "L03CODEST")
    private EstadoLoteHistorico estado;

    @Column(name = "L03SUCENT", precision = 3, nullable = false, scale = 0)
    private Integer sucursal;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "L03TPOPAGO")
    private TipoPagoLoteHistorico tipoPago;

    @Column(name = "L03NRODOC", length = 1, nullable = false)
    private String nroDocumentoIdentidad;

    @Column(name = "L03TPODOC")
    private String tipoDocumentoIdentidad;

    @Enumerated(EnumType.STRING)
    @Column(name = "L03TPOCTAP")
    private TipoCuentaOtrosBancos tipoCuentaOtrosBancos;

    @Column(name = "L03TIPPER")
    private char tipoPersona;

    @Column(name = "L03FECMOD", length = 26, precision = 26, nullable = true, scale = 6)
    private Date fechaModificacion;

    @Column(name = "L03USRMOD", length = 25, precision = 25, nullable = true)
    private String usuarioModificador;

    @Column(name = "L03FECCREA", length = 26, precision = 26, nullable = true, scale = 6)
    private Date fechaCreacion;

    @Column(name = "L03USRCREA", length = 25, precision = 25, nullable = true)
    private String usuarioCreador;

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public EstadoLoteHistorico getEstado() {
        return estado;
    }

    public void setEstado(EstadoLoteHistorico estado) {
        this.estado = estado;
    }

    public char getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(char tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public BigDecimal getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(BigDecimal codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getCodigoBeneficiario() {
        return codigoBeneficiario;
    }

    public void setCodigoBeneficiario(String codigoBeneficiario) {
        this.codigoBeneficiario = codigoBeneficiario;
    }

    public BancoDestino getBancoBeneficiario() {
        return bancoBeneficiario;
    }

    public void setBancoBeneficiario(BancoDestino bancoBeneficiario) {
        this.bancoBeneficiario = bancoBeneficiario;
    }

    public String getCuentaBeneficiario() {
        return cuentaBeneficiario;
    }

    public void setCuentaBeneficiario(String cuentaBeneficiario) {
        this.cuentaBeneficiario = cuentaBeneficiario;
    }

    public TipoCuentaBeneficiario getTipoCuentaBeneficiario() {
        return tipoCuentaBeneficiario;
    }

    public void setTipoCuentaBeneficiario(TipoCuentaBeneficiario tipoCuentaBeneficiario) {
        this.tipoCuentaBeneficiario = tipoCuentaBeneficiario;
    }

    public Short getMoneda() {
        return moneda;
    }

    public void setMoneda(Short moneda) {
        this.moneda = moneda;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Integer getSucursal() {
        return sucursal;
    }

    public void setSucursal(Integer sucursal) {
        this.sucursal = sucursal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Beneficiario))
            return false;

        Beneficiario that = (Beneficiario) o;

        return !(codigoBeneficiario != null ?
                !codigoBeneficiario.equals(that.codigoBeneficiario) : that.codigoBeneficiario != null) &&
                !(codigoCliente != null ?
                        !codigoCliente.equals(that.codigoCliente) : that.codigoCliente != null);

    }

    @Override
    public int hashCode() {
        int result = codigoCliente != null ? codigoCliente.hashCode() : 0;
        result = 31 * result + (codigoBeneficiario != null ? codigoBeneficiario.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("titular=").append(nombreCompleto);
        sb.append(", banco=").append(bancoBeneficiario.getNombreCorto());
        sb.append(", cuenta=").append(cuentaBeneficiario);
        sb.append(", estado=").append(estado);
        sb.append(", sucursal=").append(sucursal);
        sb.append(", codigo cliente=").append(codigoCliente);
        sb.append(", codigo beneficiario=").append(codigoBeneficiario);
        sb.append(", nroDocumentoIdentidad=").append(nroDocumentoIdentidad);
        sb.append(", tipoDocumentoIdentidad=").append(tipoDocumentoIdentidad);

        return sb.toString();
    }

    public String getNroDocumentoIdentidad() {
        return nroDocumentoIdentidad;
    }

    public void setNroDocumentoIdentidad(String nroDocumentoIdentidad) {
        this.nroDocumentoIdentidad = nroDocumentoIdentidad;
    }

    public String getTipoDocumentoIdentidad() {
        return tipoDocumentoIdentidad;
    }

    public void setTipoDocumentoIdentidad(String tipoDocumentoIdentidad) {
        this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
    }

    public TipoCuentaOtrosBancos getTipoCuentaOtrosBancos() {
        return tipoCuentaOtrosBancos;
    }

    public void setTipoCuentaOtrosBancos(TipoCuentaOtrosBancos tipoCuentaOtrosBancos) {
        this.tipoCuentaOtrosBancos = tipoCuentaOtrosBancos;
    }

    public TipoPagoLoteHistorico getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(TipoPagoLoteHistorico tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getNombreCompletoFinal() {
        if (nombreCompleto == null || nombreCompleto.trim().equals("")) {
            return nombreCorto;
        } else {
            return nombreCompleto;
        }
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }
}
