package bus.interfaces.ebisa.entities;


import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.bisa.model.TipoCargaContrato;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "tint_log_web")
public class EstadisticasEbisa implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "RqUID", length = 20)
    private String id;

    @Column(name = "OperationID", length = 1000)
    private String  operacionID;

    @Column(name = "AccountFrom", length = 200)
    private String numeroCuenta;

    @Column(name = "CustPermID", length = 20)
    private String  codigoCliente;

    @Column(name = "RqDate")
    private Date fechaRegistro;

    @Column(name = "UserName")
    private
    String nombreUSuario;

    @Lob
    @Column(name = "EndUserPage", length = 1337)
    private String detalleHtml;

    @Column(name = "ClientAppIPAddr")
    private String registroIP;

    @Column(name = "ActionData")
    private String pagina;

    @Transient
    private Boolean activo;

    @Transient
    private TipoCargaContrato tipoCargaContrato;


    @Transient
    private Date fechaCreacion;

    @Transient
    private String usuarioCreador;

    @Transient
    private Date fechaModificacion;

    @Transient
    private String usuarioModificador;

    public String getDescripcionTipoContrato(){
        return TipoCargaContrato.valueOf(tipoCargaContrato.name()).getDescripcion();
    }
public String getFecha(){
        return FormatosUtils.fechaHoraLargaFormateada(fechaRegistro);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOperacionID() {
        return operacionID;
    }

    public void setOperacionID(String operacionID) {
        this.operacionID = operacionID;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getNombreUSuario() {
        return nombreUSuario;
    }

    public void setNombreUSuario(String nombreUSuario) {
        this.nombreUSuario = nombreUSuario;
    }

    public String getDetalleHtml() {
        return detalleHtml;
    }

    public void setDetalleHtml(String detalleHtml) {
        this.detalleHtml = detalleHtml;
    }

    public String getRegistroIP() {
        return registroIP;
    }

    public void setRegistroIP(String registroIP) {
        this.registroIP = registroIP;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public TipoCargaContrato getTipoCargaContrato() {
        return tipoCargaContrato;
    }

    public void setTipoCargaContrato(TipoCargaContrato tipoCargaContrato) {
        this.tipoCargaContrato = tipoCargaContrato;
    }

    public String getPagina() {
        return pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }
}
