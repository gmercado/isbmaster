package bus.interfaces.ebisa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author by rsalvatierra on 28/06/2017.
 */
@Entity
@Table(name = "LPP06")
public class EstadoLoteHistorico implements Serializable {

    private static final long serialVersionUID = 2370136052547538625L;

    public final static String L13 = "L13";     // En espera de mas aprobaciones
    public final static String L02 = "L02";     // En espera de mas aprobaciones
    public final static String L04 = "L04";     // enviado a proceso
    public final static String L05 = "L05";     // en proceso
    public final static String L06 = "L06";     // procesado con pendientes
    public final static String L07 = "L07";     // procesado parcialmente
    public final static String L08 = "L08";     // completamente procesado
    public final static String L11 = "L11";     // programado
    public final static String P02 = "P02";     // pago procesado
    public final static String P03 = "P03";     // pago procesado pendiente de confirmacion
    public final static String B03 = "B03";     // En espera de mas aprobaciones

    @Id
    @Column(name = "L06CODEST")
    private String codEstado;

    @Column(name = "L06DESCRIP")
    private String descripcion;

    @Column(name = "L06RUBRO")
    private String rubro;

    public EstadoLoteHistorico() {
    }

    public EstadoLoteHistorico(String codEstado) {
        this.codEstado = codEstado;
    }

    public String getCodEstado() {
        return codEstado;
    }

    public void setCodEstado(String codEstado) {
        this.codEstado = codEstado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRubro() {
        return rubro;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof EstadoLoteHistorico))
            return false;

        EstadoLoteHistorico lote = (EstadoLoteHistorico) obj;

        return lote.getCodEstado().equals(this.getCodEstado());
    }

    @Override
    public String toString() {
        return "CodEstado=" + codEstado +
                ", Descripcion=" + descripcion;
    }

    @Override
    public int hashCode() {
        return codEstado.hashCode();
    }

}
