package bus.interfaces.ebisa.entities;

import javax.persistence.Column;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class Tbl_ServicePK implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "AcctID")
    private String acctID;

    @Column(name = "CreationDate")
    private Timestamp creationDate;

    @Column(name = "FunctionID")
    private String functionID;

    @Column(name = "ServiceID")
    private BigDecimal serviceID;

    public String getAcctID() {
        return acctID;
    }

    public void setAcctID(String acctID) {
        this.acctID = acctID;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public String getFunctionID() {
        return functionID;
    }

    public void setFunctionID(String functionID) {
        this.functionID = functionID;
    }

    public BigDecimal getServiceID() {
        return serviceID;
    }

    public void setServiceID(BigDecimal serviceID) {
        this.serviceID = serviceID;
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Tbl_ServicePK)) {
            return false;
        }
        Tbl_ServicePK castOther = (Tbl_ServicePK) other;
        return this.acctID.equals(castOther.acctID) 
                && this.functionID.equals(castOther.functionID)
                && this.creationDate.equals(castOther.creationDate)
                && this.serviceID.longValue() == castOther.serviceID.longValue()
                ;

    }

    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + this.acctID.hashCode();
        hash = hash * prime + this.functionID.hashCode();
        hash = hash * prime + ((int) (this.serviceID.longValue() ^ (this.serviceID.longValue() >>> 32)));
        hash = hash * prime + this.creationDate.hashCode();

        return hash;
    }
    
}