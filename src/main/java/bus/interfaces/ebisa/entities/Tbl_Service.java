package bus.interfaces.ebisa.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The persistent class for the tbl_Service database table.
 */
@Entity
@Table(name = "tbl_Service")
@IdClass(Tbl_ServicePK.class)
public class Tbl_Service implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "AcctID")
    private String acctID;

    @Id
    @Column(name = "CreationDate")
    private Timestamp creationDate;

    @Column(name = "Deleted")
    private boolean deleted;

    @Id
    @Column(name = "FunctionID")
    private String functionID;

    @Id
    @Column(name = "ServiceID")
    private BigDecimal serviceID;

    // bi-directional many-to-one association to Tbl_R_UserService
    @ManyToOne
    @JoinColumn(name = "ServiceID")
    private Tbl_R_UserService tblRUserService;

    public Tbl_Service() {
    }

    public String getAcctID() {
        return this.acctID;
    }

    public void setAcctID(String acctID) {
        this.acctID = acctID;
    }

    public Timestamp getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getFunctionID() {
        return this.functionID;
    }

    public void setFunctionID(String functionID) {
        this.functionID = functionID;
    }

    public Tbl_R_UserService getTblRUserService() {
        return this.tblRUserService;
    }

    public void setTblRUserService(Tbl_R_UserService tblRUserService) {
        this.tblRUserService = tblRUserService;
    }

    public BigDecimal getServiceID() {
        return serviceID;
    }

    public void setServiceID(BigDecimal serviceID) {
        this.serviceID = serviceID;
    }


}