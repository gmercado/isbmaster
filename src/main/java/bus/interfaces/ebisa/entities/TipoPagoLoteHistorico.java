package bus.interfaces.ebisa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author by rsalvatierra on 28/06/2017.
 */
@Entity
@Table(name = "LPP07")
public class TipoPagoLoteHistorico implements Serializable {

    private static final long serialVersionUID = -626201620858228305L;

    @Id
    @Column(name = "L07TPOPAGO")
    private int codTipoPago;

    @Column(name = "L07DESCRIP")
    private String descripcion;

    public TipoPagoLoteHistorico() {

    }

    public TipoPagoLoteHistorico(int codTipoPago) {
        this.codTipoPago = codTipoPago;
    }

    public int getCodTipoPago() {
        return codTipoPago;
    }

    public void setCodTipoPago(int codTipoPago) {
        this.codTipoPago = codTipoPago;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TipoPagoLoteHistorico))
            return false;

        TipoPagoLoteHistorico beneficiarioLoteHistorico = (TipoPagoLoteHistorico) obj;

        return beneficiarioLoteHistorico.getCodTipoPago() == this.getCodTipoPago();
    }

    @Override
    public int hashCode() {
        return this.getCodTipoPago();
    }

    @Override
    public String toString() {
        return "codigo=" + this.getCodTipoPago() +
                ", descripcion=" + this.getDescripcion();
    }


}

