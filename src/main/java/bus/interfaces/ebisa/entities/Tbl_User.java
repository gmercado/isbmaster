package bus.interfaces.ebisa.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Set;

/**
 * The persistent class for the tbl_User database table.
 */
@Entity
@Table(name = "tbl_User")
public class Tbl_User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "UserID")
    private long userID;

    @Column(name = "AccessType")
    private String accessType;

    @Column(name = "AddressChangesCount")
    private BigDecimal addressChangesCount;

    @Column(name = "Branch")
    private String branch;

    @Column(name = "ChangePassword")
    private boolean changePassword;

    @Column(name = "CodigoBisa")
    private String codigoBisa;

    @Column(name = "CreationDate")
    private Timestamp creationDate;

    @Column(name = "Creator")
    private BigDecimal creator;

    @Column(name = "DateChangePassword")
    private Timestamp dateChangePassword;

    @Column(name = "Deleted")
    private boolean deleted;

    @Column(name = "DeletionDate")
    private Timestamp deletionDate;

    @Column(name = "Disabled")
    private boolean disabled;

    private String eMail;

    @Column(name = "Envelope")
    private String envelope;

    private String eSignature;

    private String grupo;

    @Column(name = "ImpLoteCompl")
    private boolean impLoteCompl;

    @Column(name = "LastDateActualization")
    private Timestamp lastDateActualization;

    @Column(name = "MaxDebit")
    private BigDecimal maxDebit;

    @Column(name = "Name")
    private String name;

    @Column(name = "NextDateActualization")
    private Timestamp nextDateActualization;

    @Column(name = "NroCelular")
    private String nroCelular;

    @Column(name = "OperationID")
    private BigDecimal operationID;

    @Column(name = "Password")
    private String password;

    @Column(name = "PasswordActiveDays")
    private BigDecimal passwordActiveDays;

    @Column(name = "PerfilImport")
    private boolean perfilImport;

    @Column(name = "UserLogon")
    private String userLogon;

    @Column(name = "UserTypeID")
    private BigDecimal userTypeID;

    @Column(name = "TramUserCrea")
    private String tramUserCrea;

    // bi-directional many-to-one association to Tbl_R_UserService
    @OneToMany(mappedBy = "tblUser")
    private Set<Tbl_R_UserService> tblRUserServices;

    // bi-directional many-to-one association to Tbl_Section
    @OneToMany(mappedBy = "tblUser")
    private Set<Tbl_Section> tblSections;

    // bi-directional many-to-one association to Tbl_Customer
    @ManyToOne
    @JoinColumn(name = "CustomerID")
    private Tbl_Customer tblCustomer;

    public Tbl_User() {
    }

    public long getUserID() {
        return this.userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public String getAccessType() {
        return this.accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public BigDecimal getAddressChangesCount() {
        return this.addressChangesCount;
    }

    public void setAddressChangesCount(BigDecimal addressChangesCount) {
        this.addressChangesCount = addressChangesCount;
    }

    public String getBranch() {
        return this.branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public boolean getChangePassword() {
        return this.changePassword;
    }

    public void setChangePassword(boolean changePassword) {
        this.changePassword = changePassword;
    }

    public String getCodigoBisa() {
        return this.codigoBisa;
    }

    public void setCodigoBisa(String codigoBisa) {
        this.codigoBisa = codigoBisa;
    }

    public Timestamp getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public BigDecimal getCreator() {
        return this.creator;
    }

    public void setCreator(BigDecimal creator) {
        this.creator = creator;
    }

    public Timestamp getDateChangePassword() {
        return this.dateChangePassword;
    }

    public void setDateChangePassword(Timestamp dateChangePassword) {
        this.dateChangePassword = dateChangePassword;
    }

    public boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Timestamp getDeletionDate() {
        return this.deletionDate;
    }

    public void setDeletionDate(Timestamp deletionDate) {
        this.deletionDate = deletionDate;
    }

    public boolean getDisabled() {
        return this.disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getEMail() {
        return this.eMail;
    }

    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    public String getEnvelope() {
        return this.envelope;
    }

    public void setEnvelope(String envelope) {
        this.envelope = envelope;
    }

    public String getESignature() {
        return this.eSignature;
    }

    public void setESignature(String eSignature) {
        this.eSignature = eSignature;
    }

    public String getGrupo() {
        return this.grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public boolean getImpLoteCompl() {
        return this.impLoteCompl;
    }

    public void setImpLoteCompl(boolean impLoteCompl) {
        this.impLoteCompl = impLoteCompl;
    }

    public Timestamp getLastDateActualization() {
        return this.lastDateActualization;
    }

    public void setLastDateActualization(Timestamp lastDateActualization) {
        this.lastDateActualization = lastDateActualization;
    }

    public BigDecimal getMaxDebit() {
        return this.maxDebit;
    }

    public void setMaxDebit(BigDecimal maxDebit) {
        this.maxDebit = maxDebit;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getNextDateActualization() {
        return this.nextDateActualization;
    }

    public void setNextDateActualization(Timestamp nextDateActualization) {
        this.nextDateActualization = nextDateActualization;
    }

    public String getNroCelular() {
        return this.nroCelular;
    }

    public void setNroCelular(String nroCelular) {
        this.nroCelular = nroCelular;
    }

    public BigDecimal getOperationID() {
        return this.operationID;
    }

    public void setOperationID(BigDecimal operationID) {
        this.operationID = operationID;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BigDecimal getPasswordActiveDays() {
        return this.passwordActiveDays;
    }

    public void setPasswordActiveDays(BigDecimal passwordActiveDays) {
        this.passwordActiveDays = passwordActiveDays;
    }

    public boolean getPerfilImport() {
        return this.perfilImport;
    }

    public void setPerfilImport(boolean perfilImport) {
        this.perfilImport = perfilImport;
    }

    public String getUserLogon() {
        return this.userLogon;
    }

    public void setUserLogon(String userLogon) {
        this.userLogon = userLogon;
    }

    public BigDecimal getUserTypeID() {
        return this.userTypeID;
    }

    public void setUserTypeID(BigDecimal userTypeID) {
        this.userTypeID = userTypeID;
    }

    public Set<Tbl_R_UserService> getTblRUserServices() {
        return this.tblRUserServices;
    }

    public void setTblRUserServices(Set<Tbl_R_UserService> tblRUserServices) {
        this.tblRUserServices = tblRUserServices;
    }

    public Set<Tbl_Section> getTblSections() {
        return this.tblSections;
    }

    public void setTblSections(Set<Tbl_Section> tblSections) {
        this.tblSections = tblSections;
    }

    public Tbl_Customer getTblCustomer() {
        return this.tblCustomer;
    }

    public void setTblCustomer(Tbl_Customer tblCustomer) {
        this.tblCustomer = tblCustomer;
    }

    public String getTramUserCrea() {
        return tramUserCrea;
    }

    public void setTramUserCrea(String tramUserCrea) {
        this.tramUserCrea = tramUserCrea;
    }

    
}