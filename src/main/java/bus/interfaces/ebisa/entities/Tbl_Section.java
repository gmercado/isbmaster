package bus.interfaces.ebisa.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * The persistent class for the tbl_Section database table.
 * 
 */
@Entity
@Table(name="tbl_Section")
public class Tbl_Section implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SectionID")
	private long sectionID;

	@Column(name="Deleted")
	private boolean deleted;

	@Column(name="FunctionID")
	private String functionID;

	@Column(name="Limit")
	private BigDecimal limit;

	@Column(name="Percentage")
	private short percentage;

	//bi-directional many-to-one association to Tbl_User
    @ManyToOne
	@JoinColumn(name="UserID")
	private Tbl_User tblUser;

    public Tbl_Section() {
    }

	public long getSectionID() {
		return this.sectionID;
	}

	public void setSectionID(long sectionID) {
		this.sectionID = sectionID;
	}

	public boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getFunctionID() {
		return this.functionID;
	}

	public void setFunctionID(String functionID) {
		this.functionID = functionID;
	}

	public BigDecimal getLimit() {
		return this.limit;
	}

	public void setLimit(BigDecimal limit) {
		this.limit = limit;
	}

	public short getPercentage() {
		return this.percentage;
	}

	public void setPercentage(short percentage) {
		this.percentage = percentage;
	}

	public Tbl_User getTblUser() {
		return this.tblUser;
	}

	public void setTblUser(Tbl_User tblUser) {
		this.tblUser = tblUser;
	}
	
}