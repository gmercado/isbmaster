package bus.interfaces.ebisa.entities;

import bus.interfaces.ebisa.model.ExchangeRateId;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author by rsalvatierra on 21/08/2017.
 */
@Entity
@Table(name = "tbl_CurExchRate")
@IdClass(ExchangeRateId.class)
public class ExchangeRate implements Serializable {

    private static final long serialVersionUID = 5825949375688993340L;

    @Id
    @Column(name = "CurCode", length = 10)
    private String currCode;

    @Id
    @Column(name = "DateRate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "CurLocalValue", nullable = false)
    private BigDecimal rate;

    public String getCurrCode() {
        return currCode;
    }

    public void setCurrCode(String currCode) {
        this.currCode = currCode;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
}
