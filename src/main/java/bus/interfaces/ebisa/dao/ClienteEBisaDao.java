package bus.interfaces.ebisa.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BaseAuxiliar;
import bus.interfaces.ebisa.entities.Tbl_Customer;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ClienteEBisaDao extends DaoImpl<Tbl_Customer, Long> {

    @Inject
    protected ClienteEBisaDao(@BaseAuxiliar EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Long> countPath(Root<Tbl_Customer> from) {
        return from.get("customerID");
    }
     
    @Override
    protected Iterable<Path<String>> getFullTexts(Root<Tbl_Customer> p) {
        ArrayList<Path<String>> arrayList = new ArrayList<Path<String>>();
        arrayList.add( p.<String>get("customerID") );
        return arrayList;
    }
    
    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<Tbl_Customer> from, Tbl_Customer example, Tbl_Customer example2) {
        List<Predicate> list = new LinkedList<Predicate>();
        if (example != null ) {
            // Las llaves primarias son excluidas 
            if (! (example.getCustomerID() == 0  )) {
                list.add(cb.equal(from.<String>get("customerID"), example.getCustomerID()));
            }
            
            // incorporamos una excepcion para el codigo de cliente de ebisa
            // para eliminar los ceros por izquierda
            if (!StringUtils.isEmpty(example.getCustomer())) {
                example.setCustomer(example.getCustomerSinCeros() );
            }
            
            list.add(cb.qbe(from, example));
            
        }
        return list.toArray(new Predicate[list.size()]);
    }
    
    
}
