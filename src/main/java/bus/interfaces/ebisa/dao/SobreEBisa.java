package bus.interfaces.ebisa.dao;

import java.math.BigDecimal;

public class SobreEBisa {

    private BigDecimal numSobre;
    private BigDecimal sucursal;
    private BigDecimal estado;

    public SobreEBisa() {
        
    }
    
    public SobreEBisa(BigDecimal numSobre, BigDecimal sucursal, BigDecimal estado) {
        this.numSobre = numSobre;
        this.sucursal = sucursal;
        this.estado = estado;
    }

    public BigDecimal getNumSobre() {
        return numSobre;
    }
    
    public void setNumSobre(BigDecimal numSobre) {
        this.numSobre = numSobre;
    }
    public BigDecimal getSucursal() {
        return sucursal;
    }
    public void setSucursal(BigDecimal sucursal) {
        this.sucursal = sucursal;
    }
    public BigDecimal getEstado() {
        return estado;
    }
    public void setEstado(BigDecimal estado) {
        this.estado = estado;
    }
    
    
}
