package bus.interfaces.ebisa.dao;

import bus.database.dao.DaoImplBase;
import bus.interfaces.ebisa.entities.ExchangeRate;
import bus.plumbing.utils.Monedas;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaQuery;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 21/08/2017.
 */
class ConvertirABolivianosTransaction implements DaoImplBase.WithTransaction<BigDecimal> {

    private final short moneda;
    private final BigDecimal monto;

    ConvertirABolivianosTransaction(short moneda, BigDecimal monto) {
        this.moneda = moneda;
        this.monto = monto;
    }

    @Override
    public BigDecimal call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {

        if (moneda == Monedas.BISA_BOLIVIANOS) { // ya esta bolivianizado... no hacer nada
            return monto;
        }

        final OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
        final OpenJPACriteriaQuery<BigDecimal> criteriaQuery = cb.createQuery(BigDecimal.class);

        final Root<ExchangeRate> from = criteriaQuery.from(ExchangeRate.class);

        criteriaQuery.select(from.get("rate")).
                where(cb.equal(from.get("currCode"), StringUtils.leftPad(Short.toString(moneda), 3, '0'))).
                orderBy(cb.desc(from.get("fecha")));

        final TypedQuery<BigDecimal> query = entityManager.createQuery(criteriaQuery);
        query.setMaxResults(1);
        final BigDecimal singleResult = query.getSingleResult();
        return monto.multiply(singleResult).setScale(2, BigDecimal.ROUND_HALF_UP);
    }
}
