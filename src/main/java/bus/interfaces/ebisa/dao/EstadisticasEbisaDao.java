package bus.interfaces.ebisa.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BaseStadistic;
import bus.interfaces.ebisa.entities.EstadisticasEbisa;
import com.bisa.bus.servicios.bisa.model.TipoCargaContrato;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.*;
import java.util.*;

public class EstadisticasEbisaDao extends DaoImpl<EstadisticasEbisa, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(EstadisticasEbisaDao.class);

    @Inject
    protected EstadisticasEbisaDao(@BaseStadistic EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<EstadisticasEbisa> p) {
        ArrayList<Path<String>> paths = new ArrayList<>();
        paths.add(p.get("id"));
        return paths;
    }

    @Override
    protected Path<String> countPath(Root<EstadisticasEbisa> from) {
        return from.get("codigoCliente");
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<EstadisticasEbisa> from, EstadisticasEbisa example, EstadisticasEbisa example2) {

        List<Predicate> predicates = new LinkedList<>();
        Date fecha = example.getFechaRegistro();
        String codigoCliente = (example.getCodigoCliente() != null) ? StringUtils.trimToNull(example.getCodigoCliente()) : null;
        String numeroCuenta = example.getNumeroCuenta();
        TipoCargaContrato tipoCargaContrato = example.getTipoCargaContrato();
        String bFile="F";
        if (fecha != null && example2.getFechaRegistro() != null) {
            Date inicio = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH);
            Date inicio2 = DateUtils.truncate(example2.getFechaRegistro(), Calendar.DAY_OF_MONTH);
            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
            LOGGER.debug("   FECHAS ENTRE [{}] A [{}]", inicio, inicio2);
            predicates.add(cb.between(from.get("fechaRegistro"), inicio, inicio2));
        }

        if (codigoCliente != null) {
            final String s = StringUtils.trimToEmpty(codigoCliente);
            Expression<String> columna = cb.trim(CriteriaBuilder.Trimspec.LEADING, from.get("codigoCliente"));
            LOGGER.debug(">>>>> codigoCliente=" + s);
            predicates.add(cb.equal(columna, s));
        }

        if (tipoCargaContrato != null) {
            LOGGER.debug(">>>>> tipoCargaContrato =" + tipoCargaContrato);
            Expression<String> columna = cb.trim(CriteriaBuilder.Trimspec.LEADING, from.get("pagina"));
            predicates.add(cb.equal(columna, tipoCargaContrato.getFiltro()));
//            predicates.add(cb.equal(cb.trim(from.get("pagina"), "%"+tipoCargaContrato.getFiltro()+"%"));
        }
        /*else{
            bFile="T";
        }*/


        if (numeroCuenta!= null) {
            LOGGER.debug(">>>>> numeroCuenta =" + numeroCuenta);
            Expression<String> columna = cb.trim(CriteriaBuilder.Trimspec.LEADING, from.get("numeroCuenta"));
            predicates.add(cb.equal(columna, numeroCuenta));
//            predicates.add(cb.like(from.get("numeroCuenta"), numeroCuenta));
        }
        /*if("T".equalsIgnoreCase(bFile)){*/
            Expression<String> columna = cb.trim(CriteriaBuilder.Trimspec.LEADING, from.get("pagina"));
            predicates.add(columna.in("bg-aperturaConfirmacion01.htm","lvdesem02.htm"));
        /*}*/
//        Expression<String> columna = cb.trim(CriteriaBuilder.Trimspec.LEADING, from.get("pagina"));
//        String data="bg-aperturaConfirmacion01.htm,";
        //predicates.add(cb.equal(columna,cb.in(data));
        return predicates.toArray(new Predicate[predicates.size()]);
    }
}
