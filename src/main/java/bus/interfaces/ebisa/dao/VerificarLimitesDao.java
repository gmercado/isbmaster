package bus.interfaces.ebisa.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BaseAuxiliar;
import bus.env.api.MedioAmbiente;
import bus.interfaces.ebisa.entities.Tbl_User;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 20/02/2018.
 */
@Singleton
public class VerificarLimitesDao extends DaoImpl<Tbl_User, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(VerificarLimitesDao.class);

    private final MedioAmbiente medioAmbiente;

    public VerificarLimitesDao() {
        super(null);
        medioAmbiente = null;
    }

    @Inject
    public VerificarLimitesDao(@BaseAuxiliar EntityManagerFactory entityManagerFactory, MedioAmbiente medioAmbiente) {

        super(entityManagerFactory);
        this.medioAmbiente = medioAmbiente;
    }

    public BigDecimal montoBolivianos(BigDecimal monto, Short moneda) {
        return doWithTransaction(
                (entityManager, t) -> new ConvertirABolivianosTransaction(moneda, monto).call(entityManager, t));
    }
}

