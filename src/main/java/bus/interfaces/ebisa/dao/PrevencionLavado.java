package bus.interfaces.ebisa.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BaseAuxiliar;
import bus.env.api.MedioAmbiente;
import bus.interfaces.ebisa.entities.Tbl_User;
import bus.interfaces.ebisa.tipos.TransaccionEstado;
import bus.plumbing.utils.Monedas;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import static bus.env.api.Variables.LIMITE_FORMULARO_PCC01;
import static bus.env.api.Variables.LIMITE_FORMULARO_PCC01_DEFAULT;

/**
 * @author by rsalvatierra on 21/08/2017.
 */
@Singleton
public class PrevencionLavado extends DaoImpl<Tbl_User, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PrevencionLavado.class);

    private final MedioAmbiente medioAmbiente;

    public PrevencionLavado() {
        super(null);
        medioAmbiente = null;
    }

    @Inject
    public PrevencionLavado(@BaseAuxiliar EntityManagerFactory entityManagerFactory, MedioAmbiente medioAmbiente) {

        super(entityManagerFactory);
        this.medioAmbiente = medioAmbiente;
    }

    public boolean necesitaFormularioPCC01(final BigDecimal importe, final short moneda) {
        final BigDecimal importeTransaccionBs = doWithTransaction(
                (entityManager, t) -> {
                    BigDecimal importeBs = new ConvertirABolivianosTransaction(moneda, importe).call(entityManager, t);
                    if (importeBs == null) return BigDecimal.ZERO;

                    return importeBs;
                });

        final BigDecimal LIMITE_PCC01 = new BigDecimal(medioAmbiente.getValorDe(LIMITE_FORMULARO_PCC01,
                LIMITE_FORMULARO_PCC01_DEFAULT));

        final BigDecimal importeLimiteBs = doWithTransaction(
                (entityManager, t) -> new ConvertirABolivianosTransaction(Monedas.BISA_DOLARES,
                        LIMITE_PCC01).call(entityManager, t));

        boolean resultado = importeTransaccionBs.compareTo(importeLimiteBs) >= 0;

        LOGGER.debug("ImporteBs={}, LimiteBs={}, Resultado={}", importeTransaccionBs, importeLimiteBs, resultado);

        return resultado;
    }

    public void actualizarInformacionPCC01(OpenJPAEntityManager entityManager, final Long operationId,
                                           final TransaccionEstado estado, final String user, final BigDecimal usuario, final BigDecimal secuencia) {

        try {
            Query query1 = entityManager.createNativeQuery(
                    "UPDATE EBPCCFRM01 SET " +
                            "XTESTADO = ?, " +
                            "XTUSRPRO = ?, " +
                            "XTFECPRO = ?, " +
                            "XTPCTIME = ?, " +
                            "XTNUMUSR = ?, " +
                            "XTNUMSEQ = ? " +
                            "WHERE XTNROOPE = ? ");

            BigDecimal usuario2 = usuario;
            BigDecimal secuencia2 = secuencia;

            if (usuario == null || usuario.equals(BigDecimal.ZERO)) {
                usuario2 = new BigDecimal(5000);
            }

            if (secuencia == null) {
                secuencia2 = BigDecimal.ZERO;
            }

            query1.setParameter(1, estado.getCodigoFormularioPcc01());
            query1.setParameter(2, user);
            query1.setParameter(3, new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())));
            query1.setParameter(4, new SimpleDateFormat("HHmmss").format(new Date(System.currentTimeMillis())));
            query1.setParameter(5, usuario2);
            query1.setParameter(6, secuencia2);
            query1.setParameter(7, operationId);

            int resultado = query1.executeUpdate();

            LOGGER.debug("Cantidad registros actualizados={}, operationId={}", resultado, operationId);

        } catch (Exception e) {
            LOGGER.error("Error al actualizar formulario pcc01, la operacion se proceso, posiblemente no se actualizó" +
                    " el estado del formulario PCC01 en la tabla EBPCCFRM01, operacionId=" + operationId, e);
        }
    }


}
