package bus.interfaces.ebisa.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.interfaces.ebisa.entities.TipoPagoLoteHistorico;
import com.google.inject.Inject;

import javax.persistence.EntityManagerFactory;

/**
 * @author by rsalvatierra on 03/07/2017.
 */
public class TipoPagoLoteHistoricoDao extends DaoImpl<TipoPagoLoteHistorico, Integer> {
    @Inject
    protected TipoPagoLoteHistoricoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

}
