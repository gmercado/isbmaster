package bus.interfaces.ebisa.dao;


import bus.database.dao.DaoImplBase;
import bus.database.dao.DaoImplBase.WithTransaction;
import bus.database.model.BaseAuxiliar;
import bus.database.model.BasePrincipal;
import bus.interfaces.ebisa.entities.Tbl_Customer;
import bus.interfaces.ebisa.entities.Tbl_Function;
import bus.interfaces.ebisa.entities.Tbl_R_UserService;
import bus.interfaces.ebisa.entities.Tbl_User;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;
import org.apache.openjpa.persistence.OpenJPAQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.util.List;

public class EbisaDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(EbisaDao.class);

    private final DaoImplBase<Void> daoAS400;

    private final DaoImplBase<Void> daoSQLServer;

    @Inject
    public EbisaDao(@BasePrincipal EntityManagerFactory entityManagerFactoryAS400, @BaseAuxiliar EntityManagerFactory entityManagerFactory) {
        daoAS400 = new DaoImplBase<>(entityManagerFactoryAS400);
        daoSQLServer = new DaoImplBase<>(entityManagerFactory);
    }

    public boolean existeClienteEnEBisaConAlMenos1UsuarioActivo(final String codigoCliente) {
        return daoSQLServer.doWithTransaction((entityManager, t) -> {
            String codigoClienteSinCeros = codigoCliente;
            if (!StringUtils.isEmpty(codigoClienteSinCeros)) {
                codigoClienteSinCeros = codigoClienteSinCeros.replaceFirst("^0*", "");
            }

            @SuppressWarnings("unchecked") OpenJPAQuery<BigDecimal> sqlNumero =
                    entityManager.createNativeQuery("SELECT count(*) FROM tbl_customer c, tbl_user u WHERE c.customer=? AND u.customerid = c.customerid AND u.deleted = 'false'",
                            BigDecimal.class);
            BigDecimal numero;
            try {
                numero = sqlNumero.setParameter(1, codigoClienteSinCeros).getSingleResult();
            } catch (NoResultException e) {
                numero = new BigDecimal(-1);
                LOGGER.debug("Sin resultados");
            }
            return numero.longValue() > 0;
        });
    }

    public boolean existeClienteEnEBisa(final String codigoCliente) {
        return daoSQLServer.doWithTransaction(new WithTransaction<Boolean>() {
            @Override
            public Boolean call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                String codigoClienteSinCeros = codigoCliente;
                if (!StringUtils.isEmpty(codigoClienteSinCeros)) {
                    codigoClienteSinCeros = codigoClienteSinCeros.replaceFirst("^0*", "");
                }

                @SuppressWarnings("unchecked") OpenJPAQuery<BigDecimal> sqlNumero =
                        entityManager.createNativeQuery("SELECT count(*) FROM tbl_customer WHERE customer= ? ",
                                BigDecimal.class);
                BigDecimal numero = BigDecimal.ZERO;
                try {
                    numero = sqlNumero.setParameter(1, codigoClienteSinCeros).getSingleResult();
                } catch (NoResultException e) {
                    numero = new BigDecimal(-1);
                    LOGGER.debug("Sin resultados");
                }
                return numero.longValue() > 0;
            }
        });
    }

    public boolean existeUsuarioActivoEnEBisa(final String usuario) {
        return daoSQLServer.doWithTransaction(new WithTransaction<Boolean>() {
            @Override
            public Boolean call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                @SuppressWarnings("unchecked") OpenJPAQuery<BigDecimal> sqlNumero =
                        entityManager.createNativeQuery("SELECT count(*) FROM tbl_user WHERE userlogon = ? AND deleted = 'false'",
                                BigDecimal.class);
                BigDecimal numero = BigDecimal.ZERO;
                try {
                    numero = sqlNumero.setParameter(1, usuario).getSingleResult();
                } catch (NoResultException e) {
                    numero = new BigDecimal(-1);
                    LOGGER.debug("Sin resultados");
                }
                return numero.longValue() > 0;
            }
        });
    }

    public List<Tbl_Customer> buscarClientesEBisaPorCodigoCliente(final String codigoCliente) {
        return daoSQLServer.doWithTransaction(new WithTransaction<List<Tbl_Customer>>() {
            @Override
            public List<Tbl_Customer> call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                return buscarClientesEBisaPorCodigoCliente(entityManager, codigoCliente);
            }
        });
    }

    public static List<Tbl_Customer> buscarClientesEBisaPorCodigoCliente(OpenJPAEntityManager entityManager, String codigoCliente) {
        TypedQuery<Tbl_Customer> q =
                entityManager.createQuery("select c from Tbl_Customer c " +
                        " where c.customer = :customer",
                        Tbl_Customer.class)
                        .setParameter("customer", codigoCliente);
        return q.getResultList();
    }

    public List<Tbl_User> buscarUsuariosNoBorradosEBisaPorCustomerID(final long customerID) {
        return daoSQLServer.doWithTransaction(new WithTransaction<List<Tbl_User>>() {
            @Override
            public List<Tbl_User> call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                return buscarUsuariosNoBorradosEBisaPorCustomerID(entityManager, customerID);
            }
        });
    }

    public static List<Tbl_User> buscarUsuariosNoBorradosEBisaPorCustomerID(OpenJPAEntityManager entityManager, long customerID) {
        TypedQuery<Tbl_User> q =
                entityManager.createQuery("select u from Tbl_User u " +
                        " where u.tblCustomer.customerID = :customerID and u.deleted = 'false'",
                        Tbl_User.class)
                        .setParameter("customerID", customerID);
        return q.getResultList();
    }

    public List<Tbl_R_UserService> buscarUserServicePorID(final long serviceID) {
        return daoSQLServer.doWithTransaction((entityManager, t) -> buscarUserServicePorID(entityManager, serviceID));
    }

    public static List<Tbl_R_UserService> buscarUserServicePorID(OpenJPAEntityManager entityManager, final long serviceID) {
        TypedQuery<Tbl_R_UserService> q =
                entityManager.createQuery("select c from Tbl_R_UserService c " +
                        " where c.serviceID = :serviceID",
                        Tbl_R_UserService.class)
                        .setParameter("serviceID", serviceID);
        return q.getResultList();
    }


    public Tbl_Customer registrarClienteEBisa(final String codigoCliente, final String nombreCliente, final int tipoClienteEBisa) {
        return daoSQLServer.doWithTransaction((entityManager, t) -> {

            // buscando un cliente en la misma transaccion por si ya existe
            List<Tbl_Customer> resultList = buscarClientesEBisaPorCodigoCliente(entityManager, codigoCliente);
            if (resultList != null && !resultList.isEmpty()) {
                return resultList.get(0);
            }

            // llamando al procedimiento almacenado
            // en caso de que no exista el cliente
            List<String> obj = null;
            try {
                obj = entityManager.createNativeQuery("{call dbo.pfl_Customer_Create(?,?,?)}", String.class)
                        .setParameter(1, codigoCliente)
                        .setParameter(2, nombreCliente)
                        .setParameter(3, tipoClienteEBisa)
                        .getResultList();
            } catch (Exception e) {
                //TODO: David completar error
                return null;
            }

            if (!"00000".equals(obj.get(0))) {
                return null;
            }

            // buscando el objeto creado en la misma transaccion
            resultList = buscarClientesEBisaPorCodigoCliente(entityManager, codigoCliente);

            if (resultList == null || resultList.isEmpty()) {
                return null;
            }

            return resultList.get(0);
        });
    }

    public List<Tbl_Function> listaDeFuncionesPermitidasParaServicio() {
        return daoSQLServer.doWithTransaction((entityManager, t) -> {
            TypedQuery<Tbl_Function> q =
                    entityManager.createQuery("select f from Tbl_Function f " +
                            " where f.tramPermiso = true",
                            Tbl_Function.class);
            return q.getResultList();
        });
    }

    public boolean registrarServiciosDelUsuarioEBisa(final Tbl_R_UserService userService, final List<Tbl_Function> listFunctions) {
        return daoSQLServer.doWithTransaction(new WithTransaction<Boolean>() {
            @SuppressWarnings("unchecked")
            @Override
            public Boolean call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                boolean error = false;
                for (Tbl_Function function : listFunctions) {
                    // llamando al procedimiento almacenado
                    List<String> obj = null;
                    try {
                        obj = entityManager.createNativeQuery("{call dbo.pfl_Service_Create(?,?,?)}", String.class)
                                .setParameter(1, userService.getServiceID())
                                .setParameter(2, function.getFunctionID())
                                .setParameter(3, "GHOST                                             ")
                                .getResultList();
                    } catch (Exception e) {
                        error = true;
                    }
                    if (obj == null || obj.isEmpty()) {
                        error = true;
                    }
                }
                return !error;
            }
        });
    }

    public List<Tbl_Function> listaDeFuncionesPermitidasParaSeccion() {
        return daoSQLServer.doWithTransaction(new WithTransaction<List<Tbl_Function>>() {
            @Override
            public List<Tbl_Function> call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                //String sectionIDs = medioAmbiente.getValorDe(EBISA_FUNCTION_TO_REGISTER_USER_SECTION, EBISA_FUNCTION_TO_REGISTER_USER_SECTION_DEFAULT);
                //sectionIDs = "'" + StringUtils.replace(StringUtils.trimToEmpty(StringUtils.replaceChars(sectionIDs," ","")), ",", "','") + "'"; 
                TypedQuery<Tbl_Function> q =
                        entityManager.createQuery("select f from Tbl_Function f " +
                                " where f.tramAutorizacion = true",
                                Tbl_Function.class);
                List<Tbl_Function> resultList = q.getResultList();
                return resultList;
            }
        });
    }

    public boolean registrarSeccionesDelUsuarioEBisa(final Tbl_User user, final BigDecimal limitediario, final List<Tbl_Function> listFunctions) {
        return daoSQLServer.doWithTransaction((entityManager, t) -> {
            boolean error = false;
            for (Tbl_Function function : listFunctions) {
                // llamando al procedimiento almacenado
                List<String> obj = null;
                try {
                    obj = entityManager.createNativeQuery("{call dbo.aut_Section_Create(?,?,?,?)}", String.class)
                            .setParameter(1, user.getUserID())
                            .setParameter(2, function.getFunctionID())
                            .setParameter(3, limitediario) // siempre sera 1000000
                            .setParameter(4, 100)// siempre sera 100%
                            .getResultList();
                } catch (Exception e) {
                    error = true;
                }
                if (obj == null || obj.isEmpty()) {
                    error = true;
                }
            }
            return !error;
        });
    }

    public SobreEBisa getDatosSobre(final BigDecimal sobre) {
        return daoAS400.doWithTransaction(new WithTransaction<SobreEBisa>() {
            @Override
            public SobreEBisa call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                @SuppressWarnings("unchecked") OpenJPAQuery<SobreEBisa> sqlSobreEBisa =
                        entityManager.createNativeQuery("SELECT sknumsobre AS numSobre, sksucursal AS sucursal, skestado AS estado" +
                                "  FROM psp711L1 " +
                                " WHERE sknumsobre = ?", SobreEBisa.class);
                SobreEBisa sobreEBisa = null;
                try {
                    sobreEBisa = sqlSobreEBisa.setParameter(1, sobre.intValue()).getSingleResult();
                } catch (NoResultException e) {
                    LOGGER.warn("NO existe el sobre para E-Bisa con el numero [{}] ", sobre.toPlainString());
                }
                return sobreEBisa;
            }
        });
    }

}
