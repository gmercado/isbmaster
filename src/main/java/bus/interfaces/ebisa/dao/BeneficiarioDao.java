package bus.interfaces.ebisa.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.interfaces.as400.entities.BancoDestino;
import bus.interfaces.ebisa.entities.Beneficiario;
import bus.interfaces.ebisa.entities.BeneficiarioPK;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author by rsalvatierra on 28/06/2017.
 */
public class BeneficiarioDao extends DaoImpl<Beneficiario, BeneficiarioPK> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BeneficiarioDao.class);

    @Inject
    protected BeneficiarioDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    /**
     * Retorna Beneficiario a partir del codigo
     *
     * @param pCodigoCliente      Codigo cliente
     * @param pCodigoBeneficiario Codigo beneficiario
     * @return Beneficiario
     */
    public Beneficiario getByCodigo(BigDecimal pCodigoCliente, String pCodigoBeneficiario) {
        LOGGER.debug("Obteniendo registro por codigo:{}.", pCodigoBeneficiario);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Beneficiario> q = cb.createQuery(Beneficiario.class);
                        Root<Beneficiario> p = q.from(Beneficiario.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("codigoCliente"), pCodigoCliente));
                        predicatesAnd.add(cb.equal(p.get("codigoBeneficiario"), pCodigoBeneficiario));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Beneficiario> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    /**
     * Retorna Beneficiario a partir de la cuenta
     *
     * @param pCodigoCliente      Codigo cliente
     * @param pCuentaBeneficiario Cuenta beneficiario
     * @param pBancoBeneficiario  Banco beneficiario
     * @return Beneficiario
     */
    public Beneficiario getByCuenta(BigDecimal pCodigoCliente, String pCuentaBeneficiario, BancoDestino pBancoBeneficiario) {
        LOGGER.debug("Obteniendo registro por cuenta:{}.", pCuentaBeneficiario);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Beneficiario> q = cb.createQuery(Beneficiario.class);
                        Root<Beneficiario> p = q.from(Beneficiario.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("codigoCliente"), pCodigoCliente));
                        predicatesAnd.add(cb.equal(p.get("cuentaBeneficiario"), pCuentaBeneficiario));
                        predicatesAnd.add(cb.equal(p.get("bancoBeneficiario"), pBancoBeneficiario));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Beneficiario> query = entityManager.createQuery(q);
                        if (query.getResultList() != null && query.getResultList().size() == 1) {
                            return query.getSingleResult();
                        } else {
                            return null;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }

    public Beneficiario registrar(Beneficiario beneficiario, String usuario) {
        beneficiario.setUsuarioCreador(usuario);
        beneficiario.setFechaCreacion(new Date());
        return persist(beneficiario);
    }

    public Beneficiario actualizar(Beneficiario beneficiario, String usuario) {
        beneficiario.setUsuarioModificador(usuario);
        beneficiario.setFechaModificacion(new Date());
        return merge(beneficiario);
    }

    /**
     * Retorna Beneficiario a partir del nombre
     *
     * @param pNombreBeneficiario Nombre Beneficiario
     * @return Beneficiario
     */
    public List<Beneficiario> getByNombre(BigDecimal pCodigoCliente, String pNombreBeneficiario) {
        LOGGER.debug("Obteniendo registro {} por nombre:{}.", pCodigoCliente, pNombreBeneficiario);
        try {
            return doWithTransaction(
                    (entityManager, t) -> {
                        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                        CriteriaQuery<Beneficiario> q = cb.createQuery(Beneficiario.class);
                        Root<Beneficiario> p = q.from(Beneficiario.class);
                        LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                        predicatesAnd.add(cb.equal(p.get("codigoCliente"), pCodigoCliente));
                        predicatesAnd.add(cb.like(cb.upper(p.get("nombreCompleto")), "%" + pNombreBeneficiario.toUpperCase() + "%"));
                        q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                        TypedQuery<Beneficiario> query = entityManager.createQuery(q);
                        return query.getResultList();
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error en la consulta:{}", e);
            return null;
        }
    }
}
