package bus.interfaces.ebisa.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashEBisa {

    public static String createHash(String strInText) throws NoSuchAlgorithmException {
        String strOutText = "";
        String LKey = "llave1";
        String RKey = "llave2";
        String Key = "caracteres de la llave";
        MessageDigest md = MessageDigest.getInstance("MD5");
        strInText = LKey + strInText + RKey;
        md.update(strInText.getBytes());
        byte b[] = md.digest();
        for (int i = 0; i < b.length; i++) {
            int j = (b[i] >= 0 ? b[i] : 256 + b[i]) % 32;
            strOutText = strOutText + (j < Key.length() ? Key.substring(j, j + 1) : "");
        }

        return strOutText;
    }

}
