package bus.consumoweb.facturacion.model;

import java.io.Serializable;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class FacturaRespuesta implements Serializable {

    private Integer codigoError;
    private String mensaje;
    private String tranListNoProcesada;

    public FacturaRespuesta(Integer codigoError, String mensaje, String tranListNoProcesada) {
        this.codigoError = codigoError;
        this.mensaje = mensaje;
        this.tranListNoProcesada = tranListNoProcesada;
    }

    public Integer getCodigoError() {
        if (codigoError == null) {
            codigoError = Integer.valueOf(0);
        }
        return codigoError;
    }

    public void setCodigoError(Integer codigoError) {
        this.codigoError = codigoError;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getTranListNoProcesada() {
        return tranListNoProcesada;
    }

    public void setTranListNoProcesada(String tranListNoProcesada) {
        this.tranListNoProcesada = tranListNoProcesada;
    }

    public String toString() {
        return "codigoError=" + codigoError +
                ", mensaje=" + mensaje +
                ", tranListNoProcesada=" + tranListNoProcesada;
    }
}

