package bus.consumoweb.facturacion.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class FacturaSolicitud implements Serializable {

    private static final long serialVersionUID = -1685971611949920810L;
    private static final Logger LOGGER = LoggerFactory.getLogger(FacturaSolicitud.class);

    private String nombreFactura;
    private String nitciFactura;
    private BigDecimal numeroCliente;
    private String nombreUsuario;
    private HashMap<BigDecimal, BigDecimal> cajasecuencia;

    public FacturaSolicitud() {
    }

    public FacturaSolicitud(String nombreFactura, String nitciFactura, BigDecimal numeroCliente, String nombreUsuario,
                            Map<BigDecimal, BigDecimal> cajasecuencia) {
        this.nombreFactura = nombreFactura;
        this.nitciFactura = nitciFactura;
        this.numeroCliente = numeroCliente;
        this.nombreUsuario = nombreUsuario;
        this.cajasecuencia = new HashMap<>(cajasecuencia);
    }


    public String getNombreFactura() {
        return nombreFactura;
    }

    public void setNombreFactura(String nombreFactura) {
        this.nombreFactura = nombreFactura;
    }

    public String getNitciFactura() {
        return nitciFactura;
    }

    public void setNitciFactura(String nitciFactura) {
        this.nitciFactura = nitciFactura;
    }

    public BigDecimal getNumeroCliente() {
        return numeroCliente;
    }

    public void setNumeroCliente(BigDecimal numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public Map<BigDecimal, BigDecimal> getCajasecuencia() {
        return cajasecuencia;
    }

    public void setCajasecuencia(Map<BigDecimal, BigDecimal> cajasecuencia) {
        this.cajasecuencia = new HashMap<>(cajasecuencia);
    }

    public String toString() {
        return "nombreFactura=" + nombreFactura +
                ", nitciFactura=" + nitciFactura +
                ", numeroCliente=" + numeroCliente +
                ", nombreUsuario=" + nombreUsuario +
                ", cajasecuencia=" + cajasecuencia;
    }
}
