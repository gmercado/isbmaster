package bus.consumoweb.facturacion;

import bus.consumoweb.consumer.ConsumidorServiciosWeb;
import bus.consumoweb.consumer.DefaultHandlerRespuesta;
import bus.consumoweb.consumer.ExtraerDatoHandler;
import bus.consumoweb.facturacion.model.FacturaRespuesta;
import bus.consumoweb.facturacion.model.FacturaSolicitud;
import bus.consumoweb.model.ConsumidorAqua;
import bus.env.api.MedioAmbiente;
import bus.monitor.api.ImposibleLeerRespuestaException;
import com.google.inject.Inject;
import org.apache.wicket.util.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static bus.env.api.Variables.*;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class FacturaDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(FacturaDao.class);
    private final ConsumidorServiciosWeb consumidorServiciosWeb;
    private final MedioAmbiente medioAmbiente;

    @Inject
    public FacturaDao(@ConsumidorAqua ConsumidorServiciosWeb consumidorServiciosWeb, MedioAmbiente medioAmbiente) {
        this.consumidorServiciosWeb = consumidorServiciosWeb;
        this.medioAmbiente = medioAmbiente;
    }

    public String facturacionElectronica(FacturaSolicitud solicitud) {
        String resultadoFacturacion = null;
        // Verifica la habilitacion de facturacion electronica
        String db = medioAmbiente.getValorDe(DESACTIVAR_FACTURACION_ELECTRONICA, DESACTIVAR_FACTURACION_ELECTRONICA_DEFAULT);
        if ("no".equalsIgnoreCase(db)) {
            // Facturación electronica
            // Aunque de error, la operación continúa, no afecta al manejo de la operacion
            try {
                FacturaRespuesta resultado = facturar(solicitud);
                if (resultado.getCodigoError().equals(0)) {
                    LOGGER.info("Facturacion electronica exitosa datos={}", solicitud);
                } else {
                    if (resultado.getCodigoError().equals(101)) {
                        LOGGER.debug("El cliente no desea facturacion electronica datos={}, resultado={}", solicitud, resultado);
                    } else {
                        LOGGER.error("Facturacion electronica con error datos={}, resultado={}", solicitud, resultado);
                    }
                }
                resultadoFacturacion = resultado.getCodigoError().toString();
                LOGGER.debug("Resultado facturacion: '{}'", resultadoFacturacion);

            } catch (Exception e) {
                LOGGER.error("Error al facturar operacion: " + solicitud, e);

                resultadoFacturacion = "1";
                // No se toman mas acciones si da error en el aqua
                // La factura se quedara como pendiente en la bandeja de facturacion del e-BISA
            }
            // Guarda los datos en la base de datos
        } else {
            LOGGER.info("Facturacion electronica deshabilitada, parametro {}=si, no se facturo operacion {}",
                    DESACTIVAR_FACTURACION_ELECTRONICA, solicitud);
        }

        return resultadoFacturacion;
    }

    private FacturaRespuesta facturar(FacturaSolicitud solicitud)
            throws IOException, SAXException, ImposibleLeerRespuestaException, TimeoutException, ExecutionException {

        LOGGER.debug("Intentando facturar una operacion");

        //Detalle de las transacciones
        StringBuilder sb = new StringBuilder();
        LOGGER.debug("Datos transaccion: {}", solicitud.getCajasecuencia());
        for (Map.Entry<BigDecimal, BigDecimal> entry : solicitud.getCajasecuencia().entrySet()) {
            LOGGER.debug("Elemento caja={}, secuencia={}", entry.getKey(), entry.getValue());
            sb.append("<ws:ListaDatosTransaccion>");
            sb.append("<ws:numusr>").
                    append(entry.getValue()).
                    append("</ws:numusr><ws:numseq>").
                    append(entry.getKey()).
                    append("</ws:numseq>");
            sb.append("</ws:ListaDatosTransaccion>");
        }

        HashMap<String, String> interpolar = new HashMap<>(15);
        interpolar.put("validaConciliacion", "S");
        interpolar.put("nombre", solicitud.getNombreFactura());
        interpolar.put("nitci", solicitud.getNitciFactura());
        interpolar.put("cliente", solicitud.getNumeroCliente().toPlainString());
        interpolar.put("login", solicitud.getNombreUsuario());
        interpolar.put("datosTransaccion", sb.toString());

        LOGGER.debug("XML Facturacion: {}", sb.toString());

        LOGGER.debug("Solicitud Aqua: {}", interpolar);
        final InputStream resourceAsStream = getClass().getResourceAsStream("solicitar.xml");
        final String template = IOUtils.toString(resourceAsStream);
        final ArrayList<String> codError = new ArrayList<>();
        final ArrayList<String> desError = new ArrayList<>();
        final ArrayList<String> tranList = new ArrayList<>();
        final ArrayList[] arrayLists = {codError, desError, tranList};

        @SuppressWarnings({"unchecked"})
        final DefaultHandlerRespuesta handler = new ExtraerDatoHandler(arrayLists,
                () -> "status=" + codError.toString(), "codError", "desError", "tranList");

        consumidorServiciosWeb.consumir(template, interpolar, handler,
                medioAmbiente.getValorIntDe(WC_FACTURACION_TIMEOUT, WC_FACTURACION_TIMEOUT_DEFAULT)
        );

        if (codError.isEmpty() || codError.get(0).trim().isEmpty()) {
            throw new ImposibleLeerRespuestaException("Error en facturacion, no se encontraron los valores de la respuesta aqua",
                    ImposibleLeerRespuestaException.CODIGO_RESPUESTA_VACIA);
        }

        LOGGER.debug("Respuesta Aqua: codError={}, desError={}, tranList{}",
                codError.get(0), desError.get(0), tranList.get(0));

        return new FacturaRespuesta(Integer.valueOf(codError.get(0)), desError.get(0), tranList.get(0));
    }

}
