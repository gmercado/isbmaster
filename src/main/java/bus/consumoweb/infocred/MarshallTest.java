package bus.consumoweb.infocred;

import bus.consumoweb.infocred.objetos.TitularInfoCred;
import bus.consumoweb.infocred.utilitarios.XmlUtil;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by gmercado on 10/11/2016.
 */
public class MarshallTest {

    public static void main(String[] args) {
        try {
            String content = new String(Files.readAllBytes(Paths.get("D:\\InfoCred_Docs\\InfoCred\\Webservice_consumo\\response_getIndividualReport4.xml")));
            TitularInfoCred t = XmlUtil.xmlToObject(content, TitularInfoCred.class);
            System.out.println(t);

//            content = new String(Files.readAllBytes(Paths.get("C:\\Users\\gmercado\\Desktop\\ptitular.xml")));
//            DatosTitularLista d = XmlUtil.xmlToObject(content, DatosTitularLista.class);
//            System.out.println(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
