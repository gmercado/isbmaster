package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.*;

/**
 * @Author Gary Mercado
 * Yrag Knup 16/02/2017.
 */
@XmlRootElement(name = "tipoDeCambio")
@XmlAccessorType(XmlAccessType.FIELD)
public class CabeceraTipoCambio {

    @XmlAttribute(name = "Titulo")
    private String titulo;

    @XmlAttribute(name = "Fecha")
    private String fecha;

    @XmlAttribute(name = "MonedaOrigen")
    private String monedaOrigen;

    @XmlAttribute(name = "MonedaDestino")
    private String monedaDestino;

    @XmlAttribute(name = "Compra")
    private String compra;

    @XmlAttribute(name = "Venta")
    private String venta;

    @XmlTransient
    private String dataConsolidada = "";

    public CabeceraTipoCambio() {
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMonedaOrigen() {
        return monedaOrigen;
    }

    public void setMonedaOrigen(String monedaOrigen) {
        this.monedaOrigen = monedaOrigen;
    }

    public String getMonedaDestino() {
        return monedaDestino;
    }

    public void setMonedaDestino(String monedaDestino) {
        this.monedaDestino = monedaDestino;
    }

    public String getCompra() {
        return compra;
    }

    public void setCompra(String compra) {
        this.compra = compra;
    }

    public String getVenta() {
        return venta;
    }

    public void setVenta(String venta) {
        this.venta = venta;
    }

    public String getDataConsolidada() {
        dataConsolidada = dataConsolidada.concat(" ")
                .concat(fecha)
                .concat(" ")
                .concat(monedaOrigen)
                .concat("/")
                .concat(monedaDestino)
                .concat(" compra:")
                .concat(compra)
                .concat(" venta:")
                .concat(venta);
        return dataConsolidada;
    }

    public void setDataConsolidada(String dataConsolidada) {
        this.dataConsolidada = dataConsolidada;
    }

    @Override
    public String toString() {
        return "CabeceraTipoCambio{" +
                "titulo='" + titulo + '\'' +
                ", fecha='" + fecha + '\'' +
                ", monedaOrigen='" + monedaOrigen + '\'' +
                ", monedaDestino='" + monedaDestino + '\'' +
                ", compra='" + compra + '\'' +
                ", venta='" + venta + '\'' +
                '}';
    }
}
