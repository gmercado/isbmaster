package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "tOTALSISTEMAFINANCIERO")
@XmlAccessorType(XmlAccessType.FIELD)
public class TotalSistemaFinanciero implements XmlSerial {

    @XmlElement(name = "fila")
    private List<TotalSistemaFinancieroFila> filas;

    public TotalSistemaFinanciero() {
    }

    public List<TotalSistemaFinancieroFila> getFilas() {
        return filas;
    }

    public void setFilas(List<TotalSistemaFinancieroFila> filas) {
        this.filas = filas;
    }

    @Override
    public String toString() {
        return "TotalSistemaFinanciero{" +
                "filas=" + filas +
                '}';
    }
}
