package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "formatoHistorial")
@XmlAccessorType(XmlAccessType.FIELD)
public class FormatoHistorial implements XmlSerial {

    @XmlElement(name = "vwSaldosPersonaDeudaDirecta")
    private SaldosPersonaDeudaDirecta saldosPersonaDeudaDirecta;

    @XmlElement(name = "vwSaldosPersonaDeudaIndirecta")
    private SaldosPersonaDeudaIndirecta saldosPersonaDeudaIndirecta;

    public FormatoHistorial() {
    }

    public SaldosPersonaDeudaDirecta getSaldosPersonaDeudaDirecta() {
        return saldosPersonaDeudaDirecta;
    }

    public void setSaldosPersonaDeudaDirecta(SaldosPersonaDeudaDirecta saldosPersonaDeudaDirecta) {
        this.saldosPersonaDeudaDirecta = saldosPersonaDeudaDirecta;
    }

    public SaldosPersonaDeudaIndirecta getSaldosPersonaDeudaIndirecta() {
        return saldosPersonaDeudaIndirecta;
    }

    public void setSaldosPersonaDeudaIndirecta(SaldosPersonaDeudaIndirecta saldosPersonaDeudaIndirecta) {
        this.saldosPersonaDeudaIndirecta = saldosPersonaDeudaIndirecta;
    }

    @Override
    public String toString() {
        return "FormatoHistorial{" +
                "saldosPersonaDeudaDirecta=" + saldosPersonaDeudaDirecta +
                ", saldosPersonaDeudaIndirecta=" + saldosPersonaDeudaIndirecta +
                '}';
    }
}
