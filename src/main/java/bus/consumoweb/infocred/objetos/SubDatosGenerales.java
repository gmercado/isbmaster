package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "datosGenerales")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubDatosGenerales implements XmlSerial {

    @XmlElement(name = "nombreCompleto")
    private String nombreCompleto;

    @XmlElement(name = "tipoDocumento")
    private String tipoDocumento;

    @XmlElement(name = "nroDocumento")
    private String nroDocumento;

    @XmlElement(name = "ext")
    private String ext;

    @XmlElement(name = "tipoPersona")
    private String tipoPersona;

    @XmlElement(name = "estadoCivil")
    private String estadoCivil;

    @XmlElement(name = "fechaNacFechaDef")
    private String fechaNacFechaDef;

    @XmlElement(name = "datosValidadosSEGIP")
    private String datosValidadosSEGIP;

    @XmlElement(name = "fechaValidacionSEGIP")
    private String fechaValidacionSEGIP;

    public SubDatosGenerales() {
    }

    public SubDatosGenerales(String nombreCompleto, String tipoDocumento, String nroDocumento, String ext,
                             String tipoPersona, String estadoCivil, String fechaNacFechaDef, String datosValidadosSEGIP,
                             String fechaValidacionSEGIP) {
        this.nombreCompleto = nombreCompleto;
        this.tipoDocumento = tipoDocumento;
        this.nroDocumento = nroDocumento;
        this.ext = ext;
        this.tipoPersona = tipoPersona;
        this.estadoCivil = estadoCivil;
        this.fechaNacFechaDef = fechaNacFechaDef;
        this.datosValidadosSEGIP = datosValidadosSEGIP;
        this.fechaValidacionSEGIP = fechaValidacionSEGIP;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getFechaNacFechaDef() {
        return fechaNacFechaDef;
    }

    public void setFechaNacFechaDef(String fechaNacFechaDef) {
        this.fechaNacFechaDef = fechaNacFechaDef;
    }

    public String getDatosValidadosSEGIP() {
        return datosValidadosSEGIP;
    }

    public void setDatosValidadosSEGIP(String datosValidadosSEGIP) {
        this.datosValidadosSEGIP = datosValidadosSEGIP;
    }

    public String getFechaValidacionSEGIP() {
        return fechaValidacionSEGIP;
    }

    public void setFechaValidacionSEGIP(String fechaValidacionSEGIP) {
        this.fechaValidacionSEGIP = fechaValidacionSEGIP;
    }

    @Override
    public String toString() {
        return "\nDatosGenerales{" +
                "nombreCompleto='" + nombreCompleto + '\'' +
                ", tipoDocumento='" + tipoDocumento + '\'' +
                ", nroDocumento='" + nroDocumento + '\'' +
                ", ext='" + ext + '\'' +
                ", tipoPersona='" + tipoPersona + '\'' +
                ", estadoCivil='" + estadoCivil + '\'' +
                ", fechaNacFechaDef='" + fechaNacFechaDef + '\'' +
                ", datosValidadosSEGIP='" + datosValidadosSEGIP + '\'' +
                ", fechaValidacionSEGIP='" + fechaValidacionSEGIP + '\'' +
                '}';
    }
}
