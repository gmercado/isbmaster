package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by gmercado on 10/11/2016.
 */
@XmlRootElement(name = "vwSaldosPersonaDeudaDirecta")
@XmlAccessorType(XmlAccessType.FIELD)
public class SaldosPersonaDeudaDirecta implements XmlSerial {

    @XmlElement(name = "fila")
    private List<SaldosPersonaDeudaDirectaFila> filas;

    public SaldosPersonaDeudaDirecta() {
    }

    public List<SaldosPersonaDeudaDirectaFila> getFilas() {
        return filas;
    }

    public void setFilas(List<SaldosPersonaDeudaDirectaFila> filas) {
        this.filas = filas;
    }

    @Override
    public String toString() {
        return "SaldosPersonaDeudaDirecta{" +
                "filas=" + filas +
                '}';
    }
}
