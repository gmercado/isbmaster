package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "cabecera")
@XmlAccessorType(XmlAccessType.FIELD)
public class Cabecera {

    @XmlElement(name = "fechaHora")
    private String fechaHora;

    @XmlElement(name = "reporteNumero")
    private String reporteNumero;

    @XmlElement(name = "usuario")
    private String usuario;

    @XmlElement(name = "entidad")
    private String entidad;

    @XmlElement(name = "nroSolicitud")
    private String nroSolicitud;

    @XmlElement(name = "tipoDeCambio")
    private CabeceraTipoCambio tipoDeCambio;

    public Cabecera() {
    }

//    public Cabecera(String fechaHora, String reporteNumero, String usuario, String entidad, String nroSolicitud, String tipoDeCambio) {
//        this.fechaHora = fechaHora;
//        this.reporteNumero = reporteNumero;
//        this.usuario = usuario;
//        this.entidad = entidad;
//        this.nroSolicitud = nroSolicitud;
//        this.tipoDeCambio = tipoDeCambio;
//    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getReporteNumero() {
        return reporteNumero;
    }

    public void setReporteNumero(String reporteNumero) {
        this.reporteNumero = reporteNumero;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getNroSolicitud() {
        return nroSolicitud;
    }

    public void setNroSolicitud(String nroSolicitud) {
        this.nroSolicitud = nroSolicitud;
    }

    public CabeceraTipoCambio getTipoDeCambio() {
        return tipoDeCambio;
    }

    public void setTipoDeCambio(CabeceraTipoCambio tipoDeCambio) {
        this.tipoDeCambio = tipoDeCambio;
    }

    @Override
    public String toString() {
        return "Cabecera{" +
                "fechaHora='" + fechaHora + '\'' +
                ", reporteNumero='" + reporteNumero + '\'' +
                ", usuario='" + usuario + '\'' +
                ", entidad='" + entidad + '\'' +
                ", nroSolicitud='" + nroSolicitud + '\'' +
                ", tipoDeCambio=" + tipoDeCambio +
                '}';
    }
}
