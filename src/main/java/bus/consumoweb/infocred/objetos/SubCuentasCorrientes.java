package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "cUENTASCORRIENTES")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubCuentasCorrientes implements XmlSerial {

    @XmlElement(name = "fila")
    private List<CuentasCorrientesFila> filas;

    public SubCuentasCorrientes() {
    }

    public List<CuentasCorrientesFila> getFilas() {
        return filas;
    }

    public void setFilas(List<CuentasCorrientesFila> filas) {
        this.filas = filas;
    }

    @Override
    public String toString() {
        return "SubCuentasCorrientes{" +
                "filas=" + filas +
                '}';
    }
}
