package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "rECTIFICACIONES")
@XmlAccessorType(XmlAccessType.FIELD)
public class Rectificaciones {
    @XmlElement(name = "rECTIFICACION")
    private SubRectificaciones subRectificaciones;

    public Rectificaciones() {
    }

    public SubRectificaciones getSubRectificaciones() {
        return subRectificaciones;
    }

    public void setSubRectificaciones(SubRectificaciones subRectificaciones) {
        this.subRectificaciones = subRectificaciones;
    }
}
