package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "datosFinancieros")
@XmlAccessorType(XmlAccessType.FIELD)
public class DatosFinancieros implements XmlSerial {

    @XmlElement(name = "sISTEMAFINANCIERO")
    private SistemaFinanciero sistemaFinanciero;

    @XmlElement(name = "cUENTASCORRIENTES")
    private CuentasCorrientes cuentasCorrientes;

    @XmlElement(name = "aFP")
    private Afp afp;

    @XmlElement(name = "cASASCOMERCIALES")
    private CasasComerciales casasComerciales;

    @XmlElement(name = "rECTIFICACIONES")
    private Rectificaciones rectificaciones;

    @XmlElement(name = "formatoHistorial")
    private FormatoHistorial formatoHistorial;

    public DatosFinancieros() {
    }

    public SistemaFinanciero getSistemaFinanciero() {
        return sistemaFinanciero;
    }

    public void setSistemaFinanciero(SistemaFinanciero sistemaFinanciero) {
        this.sistemaFinanciero = sistemaFinanciero;
    }

    public FormatoHistorial getFormatoHistorial() {
        return formatoHistorial;
    }

    public void setFormatoHistorial(FormatoHistorial formatoHistorial) {
        this.formatoHistorial = formatoHistorial;
    }

    public CuentasCorrientes getCuentasCorrientes() {
        return cuentasCorrientes;
    }

    public void setCuentasCorrientes(CuentasCorrientes cuentasCorrientes) {
        this.cuentasCorrientes = cuentasCorrientes;
    }

    public Afp getAfp() {
        return afp;
    }

    public void setAfp(Afp afp) {
        this.afp = afp;
    }

    public CasasComerciales getCasasComerciales() {
        return casasComerciales;
    }

    public void setCasasComerciales(CasasComerciales casasComerciales) {
        this.casasComerciales = casasComerciales;
    }

    public Rectificaciones getRectificaciones() {
        return rectificaciones;
    }

    public void setRectificaciones(Rectificaciones rectificaciones) {
        this.rectificaciones = rectificaciones;
    }

    @Override
    public String toString() {
        return "DatosFinancieros{" +
                "sistemaFinanciero=" + sistemaFinanciero +
                ", cuentasCorrientes=" + cuentasCorrientes +
                ", afp=" + afp +
                ", formatoHistorial=" + formatoHistorial +
                '}';
    }
}
