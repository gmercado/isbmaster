package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "domicilio")
@XmlAccessorType(XmlAccessType.FIELD)
public class Domicilio {

    @XmlElement(name = "domicilio")
    private List<SubDomicilio> subDomicilio;// = new ArrayList<SubDomicilio>();

    public Domicilio() {
    }

    public List<SubDomicilio> getSubDomicilio() {
        return subDomicilio;
    }

    public void setSubDomicilio(List<SubDomicilio> subDomicilio) {
        this.subDomicilio = subDomicilio;
    }

    @Override
    public String toString() {
        return "\nDomicilio{" +
                "subDomicilio=" + subDomicilio +
                '}';
    }
}
