package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.*;

/**
 * @Author Gary Mercado
 * Yrag Knup 06/12/2016.
 */
@XmlRootElement(name = "fila")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubAfpFila implements XmlSerial {

    @XmlAttribute(name = "numero")
    private String fila;

    @XmlElement(name = "eNTIDADACREEDORA")
    private String entidadAcreedora;

    @XmlElement(name = "mONTO")
    private String monto;

    @XmlElement(name = "pERIODOSENMORA")
    private String periodosMora;

    @XmlElement(name = "fECHAACTUALIZACION")
    private String fechaActualizacion;

    public SubAfpFila() {
    }

    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    public String getEntidadAcreedora() {
        return entidadAcreedora;
    }

    public void setEntidadAcreedora(String entidadAcreedora) {
        this.entidadAcreedora = entidadAcreedora;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getPeriodosMora() {
        return periodosMora;
    }

    public void setPeriodosMora(String periodosMora) {
        this.periodosMora = periodosMora;
    }

    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    @Override
    public String toString() {
        return "SubAfpFila{" +
                "fila='" + fila + '\'' +
                ", entidadAcreedora='" + entidadAcreedora + '\'' +
                ", monto='" + monto + '\'' +
                ", periodosMora='" + periodosMora + '\'' +
                ", fechaActualizacion='" + fechaActualizacion + '\'' +
                '}';
    }
}
