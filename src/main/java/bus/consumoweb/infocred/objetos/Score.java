package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "score")
@XmlAccessorType(XmlAccessType.FIELD)
public class Score {

    @XmlElement(name = "score")
    private List<SubScore> subScores;

    public Score() {
    }

    public List<SubScore> getSubScores() {
        return subScores;
    }

    public void setSubScores(List<SubScore> subScores) {
        this.subScores = subScores;
    }

    @Override
    public String toString() {
        return "Score{" +
                "subScores=" + subScores +
                '}';
    }
}
