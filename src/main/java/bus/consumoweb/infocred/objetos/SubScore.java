package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "score")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubScore {

    @XmlElement(name = "score")
    private String score;

    @XmlElement(name = "descripcion")
    private String descripcion;

    public SubScore() {
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "SubScore{" +
                "score='" + score + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
