package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "sISTEMAFINANCIERO")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubSistemaFinanciero {

    @XmlElement(name = "fila")
    private List<SubSistemaFinancieroFila> filas;

    public SubSistemaFinanciero() {
    }

    public List<SubSistemaFinancieroFila> getFilas() {
        return filas;
    }

    public void setFilas(List<SubSistemaFinancieroFila> filas) {
        this.filas = filas;
    }

    @Override
    public String toString() {
        return "SubSistemaFinanciero{" +
                "filas=" + filas +
                '}';
    }
}
