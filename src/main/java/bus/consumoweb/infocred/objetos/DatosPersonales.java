package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "datosPersonales")
@XmlAccessorType(XmlAccessType.FIELD)
public class DatosPersonales implements XmlSerial {

    @XmlElement(name = "datosGenerales")
    private DatosGenerales datosGenerales;

    @XmlElement(name = "domicilio")
    private Domicilio domicilio;

    @XmlElement(name = "score")
    private Score score;

    public DatosPersonales() {
    }

    public DatosGenerales getDatosGenerales() {
        return datosGenerales;
    }

    public void setDatosGenerales(DatosGenerales datosGenerales) {
        this.datosGenerales = datosGenerales;
    }

    public Domicilio getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "\nDatosPersonales{" +
                "datosGenerales=" + datosGenerales +
                ", domicilio=" + domicilio +
                ", score=" + score +
                '}';
    }
}
