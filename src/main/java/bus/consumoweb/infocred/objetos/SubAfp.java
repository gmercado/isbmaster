package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup 06/12/2016.
 */
@XmlRootElement(name = "dEUDASAFPS")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubAfp implements XmlSerial {

    @XmlElement(name = "fila")
    private List<SubAfpFila> filas;

    public SubAfp() {
    }

    public List<SubAfpFila> getFilas() {
        return filas;
    }

    public void setFilas(List<SubAfpFila> filas) {
        this.filas = filas;
    }

    @Override
    public String toString() {
        return "SubAfp{" +
                "filas=" + filas +
                '}';
    }
}
