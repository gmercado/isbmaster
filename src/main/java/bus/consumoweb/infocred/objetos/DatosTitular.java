package bus.consumoweb.infocred.objetos;

import com.bisa.bus.servicios.asfi.infocred.entities.Titulares;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 11/11/2016.
 */
@XmlRootElement(name = "titular")
@XmlAccessorType(XmlAccessType.FIELD)
public class DatosTitular implements XmlSerial {

    @XmlAttribute(name = "documentoIdentidad")
    private String documentoIdentidad;

    @XmlAttribute(name = "extension")
    private String extension;

    @XmlAttribute(name = "tipoDocumentoIdentidad")
    private String tipoDocumentoIdentidad;

    @XmlAttribute(name = "nombreCompleto")
    private String nombreCompleto;

    @XmlAttribute(name = "fechaNacimiento")
    private String fechaNacimiento;

    public DatosTitular() {
    }

    public DatosTitular(Titulares titular) {
        this.documentoIdentidad = titular.getDocumento();
        this.extension = titular.getExtension();
        this.tipoDocumentoIdentidad = titular.getTipoDocumento();
        this.nombreCompleto = titular.getNombreCompleto();
        this.fechaNacimiento = titular.getFechaNacimientoFormato();
    }

    public String getDocumentoIdentidad() {
        return documentoIdentidad;
    }

    public void setDocumentoIdentidad(String documentoIdentidad) {
        this.documentoIdentidad = documentoIdentidad;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getTipoDocumentoIdentidad() {
        return tipoDocumentoIdentidad;
    }

    public void setTipoDocumentoIdentidad(String tipoDocumentoIdentidad) {
        this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        return "\nDatosTitular{" +
                "documentoIdentidad='" + documentoIdentidad + '\'' +
                ", extension='" + extension + '\'' +
                ", tipoDocumentoIdentidad='" + tipoDocumentoIdentidad + '\'' +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                ", fechaNacimiento='" + fechaNacimiento + '\'' +
                '}';
    }
}
