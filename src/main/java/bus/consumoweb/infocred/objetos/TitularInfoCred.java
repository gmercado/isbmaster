package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "titular")
@XmlAccessorType(XmlAccessType.FIELD)
public class TitularInfoCred implements XmlSerial {

    @XmlElement(name = "cabecera")
    private Cabecera cabecera;

    @XmlElement(name = "datosPersonales")
    private DatosPersonales datosPersonales;

    @XmlElement(name = "datosFinancieros")
    private DatosFinancieros datosFinancieros;

    @XmlElement(name = "cONSULTASREALIZADAS")
    private ConsultasRealizadas consultasRealizadas;

    @XmlElement(name = "notas")
    private Notas notas;

    public TitularInfoCred() {
    }

    public TitularInfoCred(Cabecera cabecera) {
        this.cabecera = cabecera;
    }

    public Cabecera getCabecera() {
        return cabecera;
    }

    public void setCabecera(Cabecera cabecera) {
        this.cabecera = cabecera;
    }

    public DatosPersonales getDatosPersonales() {
        return datosPersonales;
    }

    public void setDatosPersonales(DatosPersonales datosPersonales) {
        this.datosPersonales = datosPersonales;
    }

    public DatosFinancieros getDatosFinancieros() {
        return datosFinancieros;
    }

    public void setDatosFinancieros(DatosFinancieros datosFinancieros) {
        this.datosFinancieros = datosFinancieros;
    }

    public ConsultasRealizadas getConsultasRealizadas() {
        return consultasRealizadas;
    }

    public void setConsultasRealizadas(ConsultasRealizadas consultasRealizadas) {
        this.consultasRealizadas = consultasRealizadas;
    }

    public Notas getNotas() {
        return notas;
    }

    public void setNotas(Notas notas) {
        this.notas = notas;
    }

    @Override
    public String toString() {
        return "TitularInfoCred{" +
                "cabecera=" + cabecera +
                ", datosPersonales=" + datosPersonales +
                ", datosFinancieros=" + datosFinancieros +
                ", consultasRealizadas=" + consultasRealizadas +
                ", notas=" + notas +
                '}';
    }
}
