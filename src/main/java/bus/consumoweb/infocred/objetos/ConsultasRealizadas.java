package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "cONSULTASREALIZADAS")
@XmlAccessorType(XmlAccessType.FIELD)
public class ConsultasRealizadas {

    @XmlElement(name = "cONSULTASREALIZADAS")
    private SubConsultasRealizadas subConsultasRealizadas;

    public ConsultasRealizadas() {
    }

    public SubConsultasRealizadas getSubConsultasRealizadas() {
        return subConsultasRealizadas;
    }

    public void setSubConsultasRealizadas(SubConsultasRealizadas subConsultasRealizadas) {
        this.subConsultasRealizadas = subConsultasRealizadas;
    }

    @Override
    public String toString() {
        return "ConsultasRealizadas{" +
                "subConsultasRealizadas=" + subConsultasRealizadas +
                '}';
    }
}
