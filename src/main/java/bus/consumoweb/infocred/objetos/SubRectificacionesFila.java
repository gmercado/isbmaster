package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.*;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "fila")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubRectificacionesFila implements XmlSerial {

    @XmlAttribute(name = "numero")
    private String fila;

    @XmlElement(name = "eNTIDAD")
    private String entidad;

    @XmlElement(name = "fECHARESPALDO")
    private String fechaRespaldo;

    @XmlElement(name = "rESPALDO")
    private String respaldo;

    @XmlElement(name = "nUMEROOPERACION")
    private String numeroOperacion;

    @XmlElement(name = "fECHADESDE")
    private String fechaDesde;

    @XmlElement(name = "fECHAHASTA")
    private String fechaHasta;

    @XmlElement(name = "mOTIVO")
    private String motivo;

    @XmlElement(name = "dETALLENOTARECTIFICATORIA")
    private String detalleNotaRectificatoria;

    public SubRectificacionesFila() {
    }

    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getFechaRespaldo() {
        return fechaRespaldo;
    }

    public void setFechaRespaldo(String fechaRespaldo) {
        this.fechaRespaldo = fechaRespaldo;
    }

    public String getRespaldo() {
        return respaldo;
    }

    public void setRespaldo(String respaldo) {
        this.respaldo = respaldo;
    }

    public String getNumeroOperacion() {
        return numeroOperacion;
    }

    public void setNumeroOperacion(String numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }

    public String getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(String fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public String getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(String fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getDetalleNotaRectificatoria() {
        return detalleNotaRectificatoria;
    }

    public void setDetalleNotaRectificatoria(String detalleNotaRectificatoria) {
        this.detalleNotaRectificatoria = detalleNotaRectificatoria;
    }

    @Override
    public String toString() {
        return "SubRectificacionesFila{" +
                "fila='" + fila + '\'' +
                ", entidad='" + entidad + '\'' +
                ", fechaRespadol='" + fechaRespaldo + '\'' +
                ", respaldo='" + respaldo + '\'' +
                ", numeroOperacion='" + numeroOperacion + '\'' +
                ", fechaDesde='" + fechaDesde + '\'' +
                ", fechaHasta='" + fechaHasta + '\'' +
                ", motivo='" + motivo + '\'' +
                ", detalleNotaRectificatoria='" + detalleNotaRectificatoria + '\'' +
                '}';
    }
}
