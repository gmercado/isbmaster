package bus.consumoweb.infocred.objetos;

import com.google.common.base.Strings;

import javax.xml.bind.annotation.*;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "eSTADO1")
@XmlAccessorType(XmlAccessType.FIELD)
public class SaldoPersonaDeudaGestion implements XmlSerial {

    @XmlAttribute(name = "titulo")
    private String gestion;

    @XmlValue
    private String estado;

    public String getGestion() {
        if (!Strings.isNullOrEmpty(gestion) && gestion.length() > 5) {
            String[] a = gestion.trim().split(" ");
            if (a.length > 0)
                return a[1];
        }
        return "";
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado1) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "SaldoPersonaDeudaGestion{" +
                "gestion='" + gestion + '\'' +
                ", estado1='" + estado + '\'' +
                '}';
    }
}
