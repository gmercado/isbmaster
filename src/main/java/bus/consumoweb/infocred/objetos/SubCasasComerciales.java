package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by gmercado on 07/05/2017.
 */
@XmlRootElement(name = "cACOMER")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubCasasComerciales implements XmlSerial {

    @XmlElement(name = "fila")
    private List<SubCasasComercialesFila> filas;

    public List<SubCasasComercialesFila> getFilas() {
        return filas;
    }

    public void setFilas(List<SubCasasComercialesFila> filas) {
        this.filas = filas;
    }

    @Override
    public String toString() {
        return "SubCasasComerciales{" +
                "filas=" + filas +
                '}';
    }
}
