package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "sISTEMAFINANCIERO")
@XmlAccessorType(XmlAccessType.FIELD)
public class SistemaFinanciero implements XmlSerial {

    @XmlElement(name = "sISTEMAFINANCIERO")
    private SubSistemaFinanciero subSistemaFinanciero;

    @XmlElement(name = "dESCRIPCIONCAEDECACTECONO")
    private DescripcionCaedecactecono descripcionCaedecactecono;

    // TODO: falta agregar estos
//    <dESCRIPCIONTIPCAN Titulo="" />
//    <dESCRIPCIONTIPINT Titulo="" />
//    <dESCRIPCIONTIPOPE Titulo="" />
//    <dESCRIPCIONTIPING Titulo="" />

    @XmlElement(name = "tOTALSISTEMAFINANCIERO")
    private TotalSistemaFinanciero totalSistemaFinanciero;

    public SistemaFinanciero() {
    }

    public SubSistemaFinanciero getSubSistemaFinanciero() {
        return subSistemaFinanciero;
    }

    public void setSubSistemaFinanciero(SubSistemaFinanciero subSistemaFinanciero) {
        this.subSistemaFinanciero = subSistemaFinanciero;
    }

    public DescripcionCaedecactecono getDescripcionCaedecactecono() {
        return descripcionCaedecactecono;
    }

    public void setDescripcionCaedecactecono(DescripcionCaedecactecono descripcionCaedecactecono) {
        this.descripcionCaedecactecono = descripcionCaedecactecono;
    }

    public TotalSistemaFinanciero getTotalSistemaFinanciero() {
        return totalSistemaFinanciero;
    }

    public void setTotalSistemaFinanciero(TotalSistemaFinanciero totalSistemaFinanciero) {
        this.totalSistemaFinanciero = totalSistemaFinanciero;
    }

    @Override
    public String toString() {
        return "SistemaFinanciero{" +
                "subSistemaFinanciero=" + subSistemaFinanciero +
                ", descripcionCaedecactecono=" + descripcionCaedecactecono +
                ", totalSistemaFinanciero=" + totalSistemaFinanciero +
                '}';
    }
}
