package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "domicilio")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubDomicilio {

    @XmlElement(name = "direccion")
    private String direccion;

    @XmlElement(name = "localidad")
    private String localidad;

    @XmlElement(name = "provincia")
    private String provincia;

    @XmlElement(name = "departamento")
    private String departamento;

    @XmlElement(name = "fechaDeclaracion")
    private String fechaDeclaracion;

    public SubDomicilio() {
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getFechaDeclaracion() {
        return fechaDeclaracion;
    }

    public void setFechaDeclaracion(String fechaDeclaracion) {
        this.fechaDeclaracion = fechaDeclaracion;
    }

    @Override
    public String toString() {
        return "\nSubDomicilio{" +
                "direccion='" + direccion + '\'' +
                ", localidad='" + localidad + '\'' +
                ", provincia='" + provincia + '\'' +
                ", departamento='" + departamento + '\'' +
                ", fechaDeclaracion='" + fechaDeclaracion + '\'' +
                '}';
    }
}
