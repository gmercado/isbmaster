package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "cASASCOMERCIALES")
@XmlAccessorType(XmlAccessType.FIELD)
public class CasasComerciales implements XmlSerial {
    @XmlElement(name = "cACOMER")
    private SubCasasComerciales subCasasComerciales;

    public CasasComerciales() {
    }

    public SubCasasComerciales getSubCasasComerciales() {
        return subCasasComerciales;
    }

    public void setSubCasasComerciales(SubCasasComerciales subCasasComerciales) {
        this.subCasasComerciales = subCasasComerciales;
    }

    @Override
    public String toString() {
        return "CasasComerciales{" +
                "subCasasComerciales=" + subCasasComerciales +
                '}';
    }
}

