package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.*;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "fila")
@XmlAccessorType(XmlAccessType.FIELD)
public class SaldosPersonaDeudaIndirectaFila implements XmlSerial {

    @XmlAttribute(name = "numero")
    private String fila;

    @XmlElement(name = "aBREVIATURA")
    private String abreviatura;

    @XmlElement(name = "eSTADO1")
    private SaldoPersonaDeudaGestion estado1;

    @XmlElement(name = "sALDO1")
    private String saldo1;

    @XmlElement(name = "eSTADO2")
    private SaldoPersonaDeudaGestion estado2;

    @XmlElement(name = "sALDO2")
    private String saldo2;

    @XmlElement(name = "eSTADO3")
    private SaldoPersonaDeudaGestion estado3;

    @XmlElement(name = "sALDO3")
    private String saldo3;

    @XmlElement(name = "eSTADO4")
    private SaldoPersonaDeudaGestion estado4;

    @XmlElement(name = "sALDO4")
    private String saldo4;

    @XmlElement(name = "eSTADO5")
    private SaldoPersonaDeudaGestion estado5;

    @XmlElement(name = "sALDO5")
    private String saldo5;

    public SaldosPersonaDeudaIndirectaFila() {
    }


    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public SaldoPersonaDeudaGestion getEstado1() {
        return estado1;
    }

    public void setEstado1(SaldoPersonaDeudaGestion estado1) {
        this.estado1 = estado1;
    }

    public String getSaldo1() {
        return saldo1;
    }

    public void setSaldo1(String saldo1) {
        this.saldo1 = saldo1;
    }

    public SaldoPersonaDeudaGestion getEstado2() {
        return estado2;
    }

    public void setEstado2(SaldoPersonaDeudaGestion estado2) {
        this.estado2 = estado2;
    }

    public String getSaldo2() {
        return saldo2;
    }

    public void setSaldo2(String saldo2) {
        this.saldo2 = saldo2;
    }

    public SaldoPersonaDeudaGestion getEstado3() {
        return estado3;
    }

    public void setEstado3(SaldoPersonaDeudaGestion estado3) {
        this.estado3 = estado3;
    }

    public String getSaldo3() {
        return saldo3;
    }

    public void setSaldo3(String saldo3) {
        this.saldo3 = saldo3;
    }

    public SaldoPersonaDeudaGestion getEstado4() {
        return estado4;
    }

    public void setEstado4(SaldoPersonaDeudaGestion estado4) {
        this.estado4 = estado4;
    }

    public String getSaldo4() {
        return saldo4;
    }

    public void setSaldo4(String saldo4) {
        this.saldo4 = saldo4;
    }

    public SaldoPersonaDeudaGestion getEstado5() {
        return estado5;
    }

    public void setEstado5(SaldoPersonaDeudaGestion estado5) {
        this.estado5 = estado5;
    }

    public String getSaldo5() {
        return saldo5;
    }

    public void setSaldo5(String saldo5) {
        this.saldo5 = saldo5;
    }

    @Override
    public String toString() {
        return "SaldosPersonaDeudaIndirectaFila{" +
                "fila='" + fila + '\'' +
                ", abreviatura='" + abreviatura + '\'' +
                ", estado1=" + estado1 +
                ", saldo1='" + saldo1 + '\'' +
                ", estado2=" + estado2 +
                ", saldo2='" + saldo2 + '\'' +
                ", estado3=" + estado3 +
                ", saldo3='" + saldo3 + '\'' +
                ", estado4=" + estado4 +
                ", saldo4='" + saldo4 + '\'' +
                ", estado5=" + estado5 +
                ", saldo5='" + saldo5 + '\'' +
                '}';
    }
}
