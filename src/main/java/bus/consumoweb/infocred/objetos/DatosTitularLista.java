package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup 11/11/2016.
 */
@XmlRootElement(name = "titulares")
@XmlAccessorType(XmlAccessType.FIELD)
public class DatosTitularLista implements XmlSerial {

    @XmlElement(name = "titular")
    private List<DatosTitular> titulares = new ArrayList<>();

    public DatosTitularLista() {
    }

    public List<DatosTitular> getTitulares() {
        return titulares;
    }

    public void setTitulares(List<DatosTitular> titulares) {
        this.titulares = titulares;
    }

    @Override
    public String toString() {
        return "DatosTitularLista{" +
                "titulares=" + titulares +
                '}';
    }
}
