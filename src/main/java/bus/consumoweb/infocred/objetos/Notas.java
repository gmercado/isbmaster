package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "notas")
@XmlAccessorType(XmlAccessType.FIELD)
public class Notas {

    @XmlElement(name = "dATOSVALIDADOSPORELSEGIP")
    private String datosValidosPorSegip;

    @XmlElement(name = "fECHAACTUALIZACION")
    private String fechaActualizacion;

    @XmlElement(name = "sISTEMA")
    private String sistema;

    @XmlElement(name = "hISTORICO")
    private String historico;

    @XmlElement(name = "cAEDEC")
    private String caedec;

    @XmlElement(name = "rEHABILITACIONYCLAUSURADECUENTASCORRIENTES")
    private String rehabilitacionClausuraCuentaCorriente;

    @XmlElement(name = "rECTIFICACIONES")
    private String rectificaciones;

    @XmlElement(name = "mONTO")
    private String monto;

    @XmlElement(name = "mONTOORIGINAL")
    private String montoOriginal;

    @XmlElement(name = "aCLARACION")
    private String aclaracion;

    public Notas() {
    }

    public String getDatosValidosPorSegip() {
        return datosValidosPorSegip;
    }

    public void setDatosValidosPorSegip(String datosValidosPorSegip) {
        this.datosValidosPorSegip = datosValidosPorSegip;
    }

    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public String getCaedec() {
        return caedec;
    }

    public void setCaedec(String caedec) {
        this.caedec = caedec;
    }

    public String getRehabilitacionClausuraCuentaCorriente() {
        return rehabilitacionClausuraCuentaCorriente;
    }

    public void setRehabilitacionClausuraCuentaCorriente(String rehabilitacionClausuraCuentaCorriente) {
        this.rehabilitacionClausuraCuentaCorriente = rehabilitacionClausuraCuentaCorriente;
    }

    public String getRectificaciones() {
        return rectificaciones;
    }

    public void setRectificaciones(String rectificaciones) {
        this.rectificaciones = rectificaciones;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getMontoOriginal() {
        return montoOriginal;
    }

    public void setMontoOriginal(String montoOriginal) {
        this.montoOriginal = montoOriginal;
    }

    public String getAclaracion() {
        return aclaracion;
    }

    public void setAclaracion(String aclaracion) {
        this.aclaracion = aclaracion;
    }

    @Override
    public String toString() {
        return "\nNotas{" +
                "datosValidosPorSegip='" + datosValidosPorSegip + '\'' +
                ", fechaActualizacion='" + fechaActualizacion + '\'' +
                ", sistema='" + sistema + '\'' +
                ", historico='" + historico + '\'' +
                ", caedec='" + caedec + '\'' +
                ", rehabilitacionClausuraCuentaCorriente='" + rehabilitacionClausuraCuentaCorriente + '\'' +
                ", rectificaciones='" + rectificaciones + '\'' +
                ", monto='" + monto + '\'' +
                ", montoOriginal='" + montoOriginal + '\'' +
                ", aclaracion='" + aclaracion + '\'' +
                '}';
    }
}
