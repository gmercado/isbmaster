package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.*;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "fila")
@XmlAccessorType(XmlAccessType.FIELD)
public class TotalSistemaFinancieroFila implements XmlSerial {

    @XmlAttribute(name = "numero")
    private String fila;

    @XmlElement(name = "oRDEN")
    private String orden;

    @XmlElement(name = "dESCRIPCION")
    private String descripcion;

    @XmlElement(name = "sALDO")
    private String saldo;

    public TotalSistemaFinancieroFila() {
    }

    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "TotalSistemaFinancieroFila{" +
                "fila='" + fila + '\'' +
                ", orden='" + orden + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", saldo='" + saldo + '\'' +
                '}';
    }
}
