package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.*;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "fila")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubSistemaFinancieroFila implements XmlSerial {

    @XmlAttribute(name = "numero")
    private String fila;

    @XmlElement(name = "sISTEMA")
    private String sistema;

    @XmlElement(name = "eNTIDAD")
    private String entidad;

    @XmlElement(name = "fECHAACTUALIZACION")
    private String fechaActualizacion;

    @XmlElement(name = "fECHAPROCESAMIENTO")
    private String fechaProcesamiento;

    @XmlElement(name = "tIPOCREDITO")
    private String tipoCredito;

    @XmlElement(name = "tIPOOBLIGADO")
    private String tipoObligado;

    @XmlElement(name = "fECHAINICIO")
    private String fechaInicio;

    @XmlElement(name = "mONEDA")
    private String moneda;

    @XmlElement(name = "fECHAULTIMACUOTA")
    private String fechaUltimaCuota;

    @XmlElement(name = "mONTOORIGINAL")
    private String montoOriginal;

    @XmlElement(name = "vALORCUOTA")
    private String valorCuota;

    @XmlElement(name = "cALIFICACION")
    private String calificacion;

    @XmlElement(name = "eSTADO")
    private String estado;

    @XmlElement(name = "sALDO")
    private String saldo;

    @XmlElement(name = "cONTINGENTE")
    private String contingente;

    @XmlElement(name = "dIASMORA")
    private String diasMora;

    @XmlElement(name = "hISTORICO")
    private String historico;

    @XmlElement(name = "pERIODODEPAGO")
    private String periodoDePago;

    @XmlElement(name = "tIPCAN")
    private String tipCan;

    @XmlElement(name = "tIPINT")
    private String tipInt;

    @XmlElement(name = "tIPOPE")
    private String tipOpe;

    @XmlElement(name = "tIPING")
    private String tipIng;

    @XmlElement(name = "cAEDEC")
    private String caedec;

    @XmlElement(name = "aCTECONO")
    private String actecono;

    public SubSistemaFinancieroFila() {
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public String getTipoObligado() {
        return tipoObligado;
    }

    public void setTipoObligado(String tipoObligado) {
        this.tipoObligado = tipoObligado;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaUltimaCuota() {
        return fechaUltimaCuota;
    }

    public void setFechaUltimaCuota(String fechaUltimaCuota) {
        this.fechaUltimaCuota = fechaUltimaCuota;
    }

    public String getMontoOriginal() {
        return montoOriginal;
    }

    public void setMontoOriginal(String montoOriginal) {
        this.montoOriginal = montoOriginal;
    }

    public String getValorCuota() {
        return valorCuota;
    }

    public void setValorCuota(String valorCuota) {
        this.valorCuota = valorCuota;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getContingente() {
        return contingente;
    }

    public void setContingente(String contingente) {
        this.contingente = contingente;
    }

    public String getDiasMora() {
        return diasMora;
    }

    public void setDiasMora(String diasMora) {
        this.diasMora = diasMora;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public String getPeriodoDePago() {
        return periodoDePago;
    }

    public void setPeriodoDePago(String periodoDePago) {
        this.periodoDePago = periodoDePago;
    }

    public String getCaedec() {
        return caedec;
    }

    public void setCaedec(String caedec) {
        this.caedec = caedec;
    }

    public String getFechaProcesamiento() {
        return fechaProcesamiento;
    }

    public void setFechaProcesamiento(String fechaProcesamiento) {
        this.fechaProcesamiento = fechaProcesamiento;
    }

    public String getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getTipCan() {
        return tipCan;
    }

    public void setTipCan(String tipCan) {
        this.tipCan = tipCan;
    }

    public String getTipInt() {
        return tipInt;
    }

    public void setTipInt(String tipInt) {
        this.tipInt = tipInt;
    }

    public String getTipOpe() {
        return tipOpe;
    }

    public void setTipOpe(String tipOpe) {
        this.tipOpe = tipOpe;
    }

    public String getTipIng() {
        return tipIng;
    }

    public void setTipIng(String tipIng) {
        this.tipIng = tipIng;
    }

    public String getActecono() {
        return actecono;
    }

    public void setActecono(String actecono) {
        this.actecono = actecono;
    }

    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    @Override
    public String toString() {
        return "SubSistemaFinancieroFila{" +
                "fila='" + fila + '\'' +
                ", sistema='" + sistema + '\'' +
                ", entidad='" + entidad + '\'' +
                ", fechaActualizacion='" + fechaActualizacion + '\'' +
                ", fechaProcesamiento='" + fechaProcesamiento + '\'' +
                ", tipoCredito='" + tipoCredito + '\'' +
                ", tipoObligado='" + tipoObligado + '\'' +
                ", fechaInicio='" + fechaInicio + '\'' +
                ", moneda='" + moneda + '\'' +
                ", fechaUltimaCuota='" + fechaUltimaCuota + '\'' +
                ", montoOriginal='" + montoOriginal + '\'' +
                ", valorCuota='" + valorCuota + '\'' +
                ", calificacion='" + calificacion + '\'' +
                ", estado='" + estado + '\'' +
                ", saldo='" + saldo + '\'' +
                ", contingente='" + contingente + '\'' +
                ", diasMora='" + diasMora + '\'' +
                ", historico='" + historico + '\'' +
                ", periodoDePago='" + periodoDePago + '\'' +
                ", tipCan='" + tipCan + '\'' +
                ", tipInt='" + tipInt + '\'' +
                ", tipOpe='" + tipOpe + '\'' +
                ", tipIng='" + tipIng + '\'' +
                ", caedec='" + caedec + '\'' +
                ", actecono='" + actecono + '\'' +
                '}';
    }
}
