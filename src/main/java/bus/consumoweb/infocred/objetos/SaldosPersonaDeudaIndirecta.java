package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "vwSaldosPersonaDeudaIndirecta")
@XmlAccessorType(XmlAccessType.FIELD)
public class SaldosPersonaDeudaIndirecta implements XmlSerial {

    @XmlElement(name = "fila")
    private List<SaldosPersonaDeudaIndirectaFila> filas;

    public SaldosPersonaDeudaIndirecta() {
    }

    public List<SaldosPersonaDeudaIndirectaFila> getFilas() {
        return filas;
    }

    public void setFilas(List<SaldosPersonaDeudaIndirectaFila> filas) {
        this.filas = filas;
    }

    @Override
    public String toString() {
        return "SaldosPersonaDeudaIndirecta{" +
                "filas=" + filas +
                '}';
    }
}
