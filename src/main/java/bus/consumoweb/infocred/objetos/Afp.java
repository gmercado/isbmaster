package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 06/12/2016.
 */
@XmlRootElement(name = "aFP")
@XmlAccessorType(XmlAccessType.FIELD)
public class Afp {
    @XmlElement(name = "dEUDASAFPS")
    private SubAfp subAfp;

    public Afp() {
    }

    public SubAfp getSubAfp() {
        return subAfp;
    }

    public void setSubAfp(SubAfp subAfp) {
        this.subAfp = subAfp;
    }

    @Override
    public String toString() {
        return "Afp{" +
                "subAfp=" + subAfp +
                '}';
    }
}
