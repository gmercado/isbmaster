package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "rECTIFICACION")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubRectificaciones implements XmlSerial {

    @XmlElement(name = "fila")
    private List<SubRectificacionesFila> filas;

    public SubRectificaciones() {
    }

    public List<SubRectificacionesFila> getFilas() {
        return filas;
    }

    public void setFilas(List<SubRectificacionesFila> filas) {
        this.filas = filas;
    }
}
