package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "datosGenerales")
@XmlAccessorType(XmlAccessType.FIELD)
public class DatosGenerales implements XmlSerial {

    @XmlElement(name = "datosGenerales")
    private SubDatosGenerales subDatosGenerales;

    public DatosGenerales() {
    }

    public SubDatosGenerales getSubDatosGenerales() {
        return subDatosGenerales;
    }

    public void setSubDatosGenerales(SubDatosGenerales subDatosGenerales) {
        this.subDatosGenerales = subDatosGenerales;
    }

    @Override
    public String toString() {
        return "\nDatosGenerales{" +
                "subDatosGenerales=" + subDatosGenerales +
                '}';
    }
}
