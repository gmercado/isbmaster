package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.*;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "fila")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubCasasComercialesFila implements XmlSerial {

    @XmlAttribute(name = "numero")
    private String fila;

    @XmlElement(name = "eNTIDAD")
    private String entidad;

    @XmlElement(name = "fECHAINGRESO")
    private String fechaIngreso;

    @XmlElement(name = "mONTO")
    private String monto;

    @XmlElement(name = "tIPOOBLIGADO")
    private String tipoObligado;

    @XmlElement(name = "eSTADODEUDA")
    private String estadoDeuda;

    @XmlElement(name = "rEFERENCIA")
    private String referencia;

    @XmlElement(name = "fECHAACTUALIZACION")
    private String fechaActualizacon;

    @XmlElement(name = "fECHAINICIOOPERACION")
    private String fechaInicioOperacion;

    public SubCasasComercialesFila() {
    }

    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getTipoObligado() {
        return tipoObligado;
    }

    public void setTipoObligado(String tipoObligado) {
        this.tipoObligado = tipoObligado;
    }

    public String getEstadoDeuda() {
        return estadoDeuda;
    }

    public void setEstadoDeuda(String estadoDeuda) {
        this.estadoDeuda = estadoDeuda;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getFechaActualizacon() {
        return fechaActualizacon;
    }

    public void setFechaActualizacon(String fechaActualizacon) {
        this.fechaActualizacon = fechaActualizacon;
    }

    public String getFechaInicioOperacion() {
        return fechaInicioOperacion;
    }

    public void setFechaInicioOperacion(String fechaInicioOperacion) {
        this.fechaInicioOperacion = fechaInicioOperacion;
    }

    @Override
    public String toString() {
        return "SubCasasComercialesFila{" +
                "fila='" + fila + '\'' +
                ", entidad='" + entidad + '\'' +
                ", fechaIngreso='" + fechaIngreso + '\'' +
                ", monto='" + monto + '\'' +
                ", tipoObligado='" + tipoObligado + '\'' +
                ", estadoDeuda='" + estadoDeuda + '\'' +
                ", referencia='" + referencia + '\'' +
                ", fechaActualizacon='" + fechaActualizacon + '\'' +
                ", fechaInicioOperacion='" + fechaInicioOperacion + '\'' +
                '}';
    }
}
