package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "cONSULTASREALIZADAS")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubConsultasRealizadas {

    @XmlElement(name = "iNSTITUCION")
    private String institucion;

    @XmlElement(name = "fECHA")
    private String fecha;

    @XmlElement(name = "cONSULTA")
    private String consulta;

    public SubConsultasRealizadas() {
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }

    @Override
    public String toString() {
        return "\nSubConsultasRealizadas{" +
                "institucion='" + institucion + '\'' +
                ", fecha='" + fecha + '\'' +
                ", consulta='" + consulta + '\'' +
                '}';
    }
}
