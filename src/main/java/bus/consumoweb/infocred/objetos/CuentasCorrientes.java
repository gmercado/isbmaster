package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "cUENTASCORRIENTES")
@XmlAccessorType(XmlAccessType.FIELD)
public class CuentasCorrientes implements XmlSerial {

    @XmlElement(name = "cUENTASCORRIENTES")
    private SubCuentasCorrientes subCuentasCorrientes;

    public CuentasCorrientes() {
    }

    public SubCuentasCorrientes getSubCuentasCorrientes() {
        return subCuentasCorrientes;
    }

    public void setSubCuentasCorrientes(SubCuentasCorrientes subCuentasCorrientes) {
        this.subCuentasCorrientes = subCuentasCorrientes;
    }

    @Override
    public String toString() {
        return "CuentasCorrientes{" +
                "subCuentasCorrientes=" + subCuentasCorrientes +
                '}';
    }
}
