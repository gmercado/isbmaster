package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.*;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
@XmlRootElement(name = "fila")
@XmlAccessorType(XmlAccessType.FIELD)
public class DescripcionCaedecacteconoFila implements XmlSerial {

    @XmlAttribute(name = "numero")
    private String fila;

    @XmlElement(name = "cAEDECACTECO")
    private String caedecacteco;

    @XmlElement(name = "cODIGODESTINOCREDITOACTIVIDADECONOMICA")
    private String coddestinocreditoactividadeconomica;

    @XmlElement(name = "dESCRIPCIONCAEDECACTECO")
    private String descaedecacteco;

    @XmlElement(name = "dESCRIPCIONDESTINOCREDITOACTIVIDADECONOMICA")
    private String desdestinocreditoactividadeconomica;

    @XmlElement(name = "dESCRIPCIONREPORTECAEDECACTECO")
    private String desreportecaedecacteco;

    public DescripcionCaedecacteconoFila() {
    }

    public DescripcionCaedecacteconoFila(String fila, String caedecacteco, String coddestinocreditoactividadeconomica, String descaedecacteco, String desdestinocreditoactividadeconomica, String desreportecaedecacteco) {
        this.fila = fila;
        this.caedecacteco = caedecacteco;
        this.coddestinocreditoactividadeconomica = coddestinocreditoactividadeconomica;
        this.descaedecacteco = descaedecacteco;
        this.desdestinocreditoactividadeconomica = desdestinocreditoactividadeconomica;
        this.desreportecaedecacteco = desreportecaedecacteco;
    }

    public String getFila() {
        return fila;
    }

    public void setFila(String fila) {
        this.fila = fila;
    }

    public String getCaedecacteco() {
        return caedecacteco;
    }

    public void setCaedecacteco(String caedecacteco) {
        this.caedecacteco = caedecacteco;
    }

    public String getCoddestinocreditoactividadeconomica() {
        return coddestinocreditoactividadeconomica;
    }

    public void setCoddestinocreditoactividadeconomica(String coddestinocreditoactividadeconomica) {
        this.coddestinocreditoactividadeconomica = coddestinocreditoactividadeconomica;
    }

    public String getDescaedecacteco() {
        return descaedecacteco;
    }

    public void setDescaedecacteco(String descaedecacteco) {
        this.descaedecacteco = descaedecacteco;
    }

    public String getDesdestinocreditoactividadeconomica() {
        return desdestinocreditoactividadeconomica;
    }

    public void setDesdestinocreditoactividadeconomica(String desdestinocreditoactividadeconomica) {
        this.desdestinocreditoactividadeconomica = desdestinocreditoactividadeconomica;
    }

    public String getDesreportecaedecacteco() {
        return desreportecaedecacteco;
    }

    public void setDesreportecaedecacteco(String desreportecaedecacteco) {
        this.desreportecaedecacteco = desreportecaedecacteco;
    }
}
