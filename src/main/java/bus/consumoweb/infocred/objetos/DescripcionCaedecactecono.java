package bus.consumoweb.infocred.objetos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup 06/12/2016.
 */
@XmlRootElement(name = "dESCRIPCIONCAEDECACTECONO")
@XmlAccessorType(XmlAccessType.FIELD)
public class DescripcionCaedecactecono implements XmlSerial {

    @XmlElement(name = "fila")
    private List<DescripcionCaedecacteconoFila> filas;

    public DescripcionCaedecactecono() {
    }

    public List<DescripcionCaedecacteconoFila> getFilas() {
        return filas;
    }

    public void setFilas(List<DescripcionCaedecacteconoFila> filas) {
        this.filas = filas;
    }

    @Override
    public String toString() {
        return "DescripcionCaedecactecono{" +
                "filas=" + filas +
                '}';
    }
}
