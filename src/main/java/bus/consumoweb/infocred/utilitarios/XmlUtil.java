package bus.consumoweb.infocred.utilitarios;

import bus.consumoweb.infocred.objetos.XmlSerial;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * @Author Gary Mercado
 * Yrag Knup 10/11/2016.
 */
public class XmlUtil {

    public static <G> String objectToXml(XmlSerial object) throws JAXBException, IOException {
        try (StringWriter sw = new StringWriter()) {
            JAXBContext context = JAXBContext.newInstance(object.getClass());
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            m.marshal(object, sw);
            return sw.toString();
        }
    }

    public static <G> G xmlToObject(String xml, Class<G> clazz) throws JAXBException {
        StringReader st = null;
        try {
            JAXBContext context = JAXBContext.newInstance(clazz);
            Unmarshaller um = context.createUnmarshaller();
            st = new StringReader(xml);
            G object = (G) um.unmarshal(st);
            return object;
        } finally {
            if (st != null) {
                st.close();
            }
        }
    }
}
