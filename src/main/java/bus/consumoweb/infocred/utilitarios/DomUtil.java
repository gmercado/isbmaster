package bus.consumoweb.infocred.utilitarios;

import com.google.common.base.Strings;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class DomUtil {

    public final static String SEPARADOR = "|";
    private final static String FECHA_PROCESO = "FECHA DE PROCESO";

    public static Document stringToDocument(String xmlString) throws ParserConfigurationException, IOException, SAXException {
        StringReader sr = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource();
            sr = new StringReader(xmlString);
            is.setCharacterStream(sr);
            return builder.parse(is);
        } catch (Exception e) {
            throw new IllegalArgumentException("Ocurrio un error al crear el fichero Dom, " + e.getMessage());
        } finally {
            if (sr != null) {
                sr.close();
            }
        }
    }

    public static String obtenerCabeceras(String path) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(path);

        document.getDocumentElement().normalize();
        NodeList nList = document.getElementsByTagName("tr");
        if (nList.getLength() == 0) {
            nList = document.getElementsByTagName("Row");
        }
        boolean finCabecera = false;
        int i = 0;
        String cabecera = "";
        while (true || i > nList.getLength()) {
            Node nNode = nList.item(i);
            for (int j = 0; j < nNode.getChildNodes().getLength(); j++) {
                Element eElement = (Element) nNode.getChildNodes().item(j);
                if (!Strings.isNullOrEmpty(eElement.getTextContent())) {
                    cabecera = cabecera.concat(eElement.getTextContent().trim().replaceAll("\\n", "")).concat(SEPARADOR);
                    if (finCabecera) {
                        return cabecera;
                    }
                    if (!Strings.isNullOrEmpty(eElement.getTextContent()) && eElement.getTextContent().contains(FECHA_PROCESO)) {
                        finCabecera = true;
                    }
                }
            }
            i++;
        }
        return null;
    }

    public static List<Map<Integer, String>> obtenerCuerpo(String path, int inicioCuerpo) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(path);

        document.getDocumentElement().normalize();
        NodeList nList = document.getElementsByTagName("tr");
        if (nList.getLength() == 0) {
            nList = document.getElementsByTagName("Row");
        }

        int i = inicioCuerpo;
        List<Map<Integer, String>> listMap = new LinkedList<>();
        Map<Integer, String> map;
        while (nList.getLength() > i) {
            Node nNode = nList.item(i);
            try {
                for (int j = 0; j < nNode.getChildNodes().getLength(); j++) {
                    map = new HashMap<>();
                    Element eElement = (Element) nNode.getChildNodes().item(j);
                    map.put(j + 1, eElement.getTextContent().trim().replaceAll("\\n", ""));
                    listMap.add(map);
                }
            } catch (NullPointerException ign) {
                continue;
            }
            i++;
        }
        return listMap;
    }
}