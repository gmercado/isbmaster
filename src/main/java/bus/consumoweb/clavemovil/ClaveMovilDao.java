package bus.consumoweb.clavemovil;

import bus.consumoweb.consumer.ConsumidorServiciosWeb;
import bus.consumoweb.consumer.DefaultHandlerRespuesta;
import bus.consumoweb.consumer.ExtraerDatoHandler;
import bus.consumoweb.model.ConsumidorAqua;
import bus.env.api.MedioAmbiente;
import bus.monitor.api.ImposibleLeerRespuestaException;
import com.google.inject.Inject;
import org.apache.wicket.util.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static bus.env.api.Variables.WC_CLAVEMOVIL_TIMEOUT;
import static bus.env.api.Variables.WC_CLAVEMOVIL_TIMEOUT_DEFAULT;

/**
 * @author by rsalvatierra on 23/06/2017.
 */
public class ClaveMovilDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClaveMovilDao.class);
    private final ConsumidorServiciosWeb consumidorServiciosWeb;
    private final MedioAmbiente medioAmbiente;

    @Inject
    public ClaveMovilDao(@ConsumidorAqua ConsumidorServiciosWeb consumidorServiciosWeb, MedioAmbiente medioAmbiente) {
        this.consumidorServiciosWeb = consumidorServiciosWeb;
        this.medioAmbiente = medioAmbiente;
    }

    public RetornarClaveMovil claveMovil(String fono)
            throws ImposibleLeerRespuestaException, TimeoutException, ExecutionException, IOException, SAXException {

        LOGGER.debug("Intentando obtener una clave movil para el fono {}", fono);
        final Map<String, String> interpolar = Collections.singletonMap("fono", "C" + fono);
        final InputStream resourceAsStream = getClass().getResourceAsStream("retornarClaveMovilRequest.xml");
        final String template;
        try {
            template = IOUtils.toString(resourceAsStream);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        final ArrayList<String> estado = new ArrayList<>();
        final ArrayList<String> descripcion = new ArrayList<>();
        final ArrayList<String> clave = new ArrayList<>();
        final ArrayList[] arrayLists = {estado, descripcion, clave};

        @SuppressWarnings({"unchecked"})
        final DefaultHandlerRespuesta handler = new ExtraerDatoHandler(arrayLists,
                () -> "status=" + estado.toString(), "estado", "descripcion", "clave");

        consumidorServiciosWeb.consumir(template, interpolar, handler,
                medioAmbiente.getValorIntDe(WC_CLAVEMOVIL_TIMEOUT, WC_CLAVEMOVIL_TIMEOUT_DEFAULT));
        LOGGER.info("datos clave movil {} , {} , {}", estado, descripcion, clave);
        RetornarClaveMovil retomarClaveMovil = new RetornarClaveMovil(estado, descripcion, clave);
        if (!clave.isEmpty()) {
            return retomarClaveMovil;
        }
        if (!descripcion.isEmpty()) {
            return retomarClaveMovil;
        }
        throw new ImposibleLeerRespuestaException("no hay ni clave ni descripcion", ImposibleLeerRespuestaException.CODIGO_RESPUESTA_VACIA);
    }
}
