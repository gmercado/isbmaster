package bus.consumoweb.clavemovil;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author by rsalvatierra on 05/07/2017.
 */
public class RetornarClaveMovil implements Serializable {
    private String estado;
    private String mensaje;
    private String clave;

    RetornarClaveMovil(ArrayList<String> estado, ArrayList<String> descripcion, ArrayList<String> clave) {
        if (estado != null && !estado.isEmpty() && !estado.get(0).isEmpty()) {
            this.estado = estado.get(0);
        }
        if (descripcion != null && !descripcion.isEmpty() && !descripcion.get(0).isEmpty()) {
            this.mensaje = descripcion.get(0);
        }
        if (clave != null && !clave.isEmpty() && !clave.get(0).isEmpty()) {
            this.clave = clave.get(0);
        }
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}
