package bus.consumoweb.giromovil.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class GiroMovil implements Serializable {

    private static final long serialVersionUID = -6858664344978295500L;

    private Long cuenta;
    private Short moneda;
    private String tipoCuenta;
    private Short monedaCuenta;
    private BigDecimal importe;
    private String motivo;
    //Remitente
    private String usuarioEbisa;
    private Long userId;
    private String remitente;
    private String celularRemitente;
    private String correoRemitente;
    //Beneficiario
    private String nombreCompletoBeneficiario;
    private String docIdentidadBeneficiario;
    private String tipoIdentidadBeneficiario;
    private String celularBeneficiario;
    private String canal;

    public Short getMonedaCuenta() {
        return monedaCuenta;
    }

    public void setMonedaCuenta(Short monedaCuenta) {
        this.monedaCuenta = monedaCuenta;
    }

    public String getUsuarioEbisa() {
        return usuarioEbisa;
    }

    public void setUsuarioEbisa(String usuarioEbisa) {
        this.usuarioEbisa = usuarioEbisa;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCelularRemitente() {
        return celularRemitente;
    }

    public void setCelularRemitente(String celularRemitente) {
        this.celularRemitente = celularRemitente;
    }

    public String getCorreoRemitente() {
        return correoRemitente;
    }

    public void setCorreoRemitente(String correoRemitente) {
        this.correoRemitente = correoRemitente;
    }

    public String getRemitente() {
        return remitente;
    }

    public void setRemitente(String remitente) {
        this.remitente = remitente;
    }

    public Long getCuenta() {
        return cuenta;
    }

    public void setCuenta(Long cuentas) {
        this.cuenta = cuentas;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public Short getMoneda() {
        return moneda;
    }

    public void setMoneda(Short moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getNombreCompletoBeneficiario() {
        return nombreCompletoBeneficiario;
    }

    public void setNombreCompletoBeneficiario(String nombreCompletoBeneficiario) {
        this.nombreCompletoBeneficiario = nombreCompletoBeneficiario;
    }

    public String getDocIdentidadBeneficiario() {
        return docIdentidadBeneficiario;
    }

    public void setDocIdentidadBeneficiario(String docIdentidadBeneficiario) {
        this.docIdentidadBeneficiario = docIdentidadBeneficiario;
    }

    public String getTipoIdentidadBeneficiario() {
        return tipoIdentidadBeneficiario;
    }

    public void setTipoIdentidadBeneficiario(String tipoIdentidadBeneficiario) {
        this.tipoIdentidadBeneficiario = tipoIdentidadBeneficiario;
    }

    public String getCelularBeneficiario() {
        return celularBeneficiario;
    }

    public void setCelularBeneficiario(String celular) {
        this.celularBeneficiario = celular;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }
}
