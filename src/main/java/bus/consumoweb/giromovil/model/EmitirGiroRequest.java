package bus.consumoweb.giromovil.model;

import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class EmitirGiroRequest {
    private String canal;
    private BigDecimal importe;
    private Short moneda;
    private Long cuentaNumero;
    private Short cuentaMoneda;
    private String cuentaTipo;
    private String usuarioEbisa;
    private String fono;
    private String beneficiario;
    private String remitente;
    private String nota;
    private String tipoDocBeneficiario;
    private String docBeneficiario;
    private String fonoRemitente;
    private String correoRemitente;
    private Long idUsuario;

    public EmitirGiroRequest(GiroMovil datosGiro) {
        this.canal = datosGiro.getCanal();
        this.importe = datosGiro.getImporte();
        this.moneda = datosGiro.getMoneda();
        this.cuentaNumero = datosGiro.getCuenta();
        this.cuentaMoneda = datosGiro.getMonedaCuenta();
        this.cuentaTipo = datosGiro.getTipoCuenta();
        this.usuarioEbisa = datosGiro.getUsuarioEbisa();
        this.fono = datosGiro.getCelularBeneficiario();
        this.beneficiario = datosGiro.getNombreCompletoBeneficiario();
        this.remitente = datosGiro.getRemitente();
        this.nota = datosGiro.getMotivo();
        this.tipoDocBeneficiario = datosGiro.getTipoIdentidadBeneficiario();
        this.docBeneficiario = datosGiro.getDocIdentidadBeneficiario();
        this.correoRemitente = datosGiro.getCorreoRemitente();
        this.fonoRemitente = datosGiro.getCelularRemitente();
        this.idUsuario = datosGiro.getUserId();

    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public Short getMoneda() {
        return moneda;
    }

    public void setMoneda(Short moneda) {
        this.moneda = moneda;
    }

    public Long getCuentaNumero() {
        return cuentaNumero;
    }

    public void setCuentaNumero(Long cuentaNumero) {
        this.cuentaNumero = cuentaNumero;
    }

    public Short getCuentaMoneda() {
        return cuentaMoneda;
    }

    public void setCuentaMoneda(Short cuentaMoneda) {
        this.cuentaMoneda = cuentaMoneda;
    }

    public String getCuentaTipo() {
        return cuentaTipo;
    }

    public void setCuentaTipo(String cuentaTipo) {
        this.cuentaTipo = cuentaTipo;
    }

    public String getUsuarioEbisa() {
        return usuarioEbisa;
    }

    public void setUsuarioEbisa(String usuarioEbisa) {
        this.usuarioEbisa = usuarioEbisa;
    }

    public String getFono() {
        return fono;
    }

    public void setFono(String fono) {
        this.fono = fono;
    }

    public String getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(String beneficiario) {
        this.beneficiario = beneficiario;
    }

    public String getRemitente() {
        return remitente;
    }

    public void setRemitente(String remitente) {
        this.remitente = remitente;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getTipoDocBeneficiario() {
        return tipoDocBeneficiario;
    }

    public void setTipoDocBeneficiario(String tipoDocBeneficiario) {
        this.tipoDocBeneficiario = tipoDocBeneficiario;
    }

    public String getDocBeneficiario() {
        return docBeneficiario;
    }

    public void setDocBeneficiario(String docBeneficiario) {
        this.docBeneficiario = docBeneficiario;
    }

    public String getFonoRemitente() {
        return fonoRemitente;
    }

    public void setFonoRemitente(String fonoRemitente) {
        this.fonoRemitente = fonoRemitente;
    }

    public String getCorreoRemitente() {
        return correoRemitente;
    }

    public void setCorreoRemitente(String correoRemitente) {
        this.correoRemitente = correoRemitente;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String toString() {
        return "canal=" + canal +
                ", importe=" + importe +
                ", moneda=" + moneda +
                ", cuenatNumero=" + cuentaNumero +
                ", cuentaMoneda=" + cuentaMoneda +
                ", cuentaTipo=" + cuentaTipo +
                ", usuarioEbisa=" + usuarioEbisa +
                ", fono=" + fono +
                ", beneficiario=" + beneficiario +
                ", remitente=" + remitente +
                ", nota=" + nota +
                ", tipoDocBeneficiario=" + tipoDocBeneficiario +
                ", docBeneficiario=" + docBeneficiario;
    }

}
