package bus.consumoweb.giromovil.model;

import bus.config.ConfigFileModule;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class EmitirGiroResponse implements Serializable {

    private static final long serialVersionUID = 5790113065617878263L;

    private String estado;
    private String descripcion;
    private String numeroGiro;
    private String wnumusr;
    private String wnumseq;
    private String wcodseg;
    private String wfecsis;
    private String whora;
    private BigDecimal wcargo;
    private BigDecimal wimptot;
    private String wnombre;
    private String wnombrea;
    private Short wcodmon;
    private BigDecimal wimporte;
    private Short wcodmon2;
    private BigDecimal wimporte2;
    private BigDecimal wcotact;
    private String wcomven;
    private String wpost;
    private String wrej1;
    private String wcodtrn;
    private String ref;

    public EmitirGiroResponse() {
    }

    public EmitirGiroResponse(ArrayList<String> estado, ArrayList<String> descripcion, ArrayList<String> numeroGiro, ArrayList<String> wnumusr,
                              ArrayList<String> wnumseq, ArrayList<String> wcodseg, ArrayList<String> wfecsis, ArrayList<String> whora,
                              ArrayList<String> wcargo, ArrayList<String> wimptot, ArrayList<String> wnombre, ArrayList<String> wnombrea,
                              ArrayList<String> wcodmon, ArrayList<String> wimporte, ArrayList<String> wcodmon2, ArrayList<String> wimporte2,
                              ArrayList<String> wcotact, ArrayList<String> wcomven, ArrayList<String> wpost, ArrayList<String> wrej1,
                              ArrayList<String> wcodtrn, ArrayList<String> ref) throws ParseException {

        if (estado != null && !estado.isEmpty() && !estado.get(0).isEmpty()) {
            this.estado = estado.get(0);
        }

        if (descripcion != null && !descripcion.isEmpty() && !descripcion.get(0).isEmpty()) {
            this.descripcion = descripcion.get(0);
        }

        if (numeroGiro != null && !numeroGiro.isEmpty() && !numeroGiro.get(0).isEmpty()) {
            this.numeroGiro = numeroGiro.get(0).trim();
        }

        if (wnumusr != null && !wnumusr.isEmpty() && !wnumusr.get(0).isEmpty()) {
            this.wnumusr = wnumusr.get(0);
        }

        if (wnumseq != null && !wnumseq.isEmpty() && !wnumseq.get(0).isEmpty()) {
            this.wnumseq = wnumseq.get(0);
        }

        if (wcodseg != null && !wcodseg.isEmpty() && !wcodseg.get(0).isEmpty()) {
            this.wcodseg = wcodseg.get(0);
        }

        if (wfecsis != null && !wfecsis.isEmpty() && !wfecsis.get(0).isEmpty()) {
            this.wfecsis = wfecsis.get(0);
        }

        if (whora != null && !whora.isEmpty() && !whora.get(0).isEmpty()) {
            this.whora = whora.get(0);
        }

        if (wcargo != null && !wcargo.isEmpty() && !wcargo.get(0).isEmpty()) {
            this.wcargo = new BigDecimal(wcargo.get(0).toString());
        }

        if (wimptot != null && !wimptot.isEmpty() && !wimptot.get(0).isEmpty()) {
            this.wimptot = new BigDecimal(wimptot.get(0).toString());
        }

        if (wnombre != null && !wnombre.isEmpty() && !wnombre.get(0).isEmpty()) {
            this.wnombre = wnombre.get(0).trim();
        }

        if (wnombrea != null && !wnombrea.isEmpty() && !wnombrea.get(0).isEmpty()) {
            this.wnombrea = wnombrea.get(0).trim();
        }

        if (wcodmon != null && !wcodmon.isEmpty() && !wcodmon.get(0).isEmpty()) {
            this.wcodmon = Short.valueOf(wcodmon.get(0));
        }

        if (wimporte != null && !wimporte.isEmpty() && !wimporte.get(0).isEmpty()) {
            this.wimporte = new BigDecimal(wimporte.get(0).toString());
        }

        if (wcodmon2 != null && !wcodmon2.isEmpty() && !wcodmon2.get(0).isEmpty()) {
            this.wcodmon2 = Short.valueOf(wcodmon2.get(0));
        }

        if (wimporte2 != null && !wimporte2.isEmpty() && !wimporte2.get(0).isEmpty()) {
            this.wimporte2 = new BigDecimal(wimporte2.get(0).toString());
        }

        if (wcotact != null && !wcotact.isEmpty() && !wcotact.get(0).isEmpty()) {
            this.wcotact = new BigDecimal(wcotact.get(0).toString());
        }

        if (wcomven != null && !wcomven.isEmpty() && !wcomven.get(0).isEmpty()) {
            this.wcomven = wcomven.get(0).trim();
        }

        if (wpost != null && !wpost.isEmpty() && !wpost.get(0).isEmpty()) {
            this.wpost = wpost.get(0);
        }

        if (wrej1 != null && !wrej1.isEmpty() && !wrej1.get(0).isEmpty()) {
            this.wrej1 = wrej1.get(0).trim();
        }

        if (wcodtrn != null && !wcodtrn.isEmpty() && !wcodtrn.get(0).isEmpty()) {
            this.wcodtrn = wcodtrn.get(0).trim();
        }

        if (ref != null && !ref.isEmpty() && !ref.get(0).isEmpty()) {
            this.ref = ref.get(0).trim();
        }

    }

    public EmitirGiroResponse(String estado, String descripcion, String numeroGiro, String wnumusr, String wnumseq, String wcodseg,
                              String wfecsis, String whora, BigDecimal wcargo, BigDecimal wimptot, String wnombre, String wnombrea, Short wcodmon,
                              BigDecimal wimporte, Short wcodmon2, BigDecimal wimporte2, BigDecimal wcotact, String wcomven, String wpost, String wrej1,
                              String wcodtrn, String ref) {

        this.estado = estado;
        this.descripcion = descripcion;
        this.numeroGiro = numeroGiro;
        this.wnumusr = wnumusr;
        this.wnumseq = wnumseq;
        this.wcodseg = wcodseg;
        this.wfecsis = wfecsis;
        this.whora = whora;
        this.wcargo = wcargo;
        this.wimptot = wimptot;
        this.wnombre = wnombre;
        this.wnombrea = wnombrea;
        this.wcodmon = wcodmon;
        this.wimporte = wimporte;
        this.wcodmon2 = wcodmon2;
        this.wimporte2 = wimporte2;
        this.wcotact = wcotact;
        this.wcomven = wcomven;
        this.wpost = wpost;
        this.wrej1 = wrej1;
        this.wcodtrn = wcodtrn;
        this.ref = ref;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNumeroGiro() {
        return numeroGiro;
    }

    public String getNumeroGiroDesencriptado() {
        return ConfigFileModule.getCryptUtils().dec(this.numeroGiro);
    }

    public void setNumeroGiro(String numeroGiro) {
        this.numeroGiro = numeroGiro;
    }

    public String getWnumusr() {
        return wnumusr;
    }

    public void setWnumusr(String wnumusr) {
        this.wnumusr = wnumusr;
    }

    public String getWnumseq() {
        return wnumseq;
    }

    public void setWnumseq(String wnumseq) {
        this.wnumseq = wnumseq;
    }

    public String getWcodseg() {
        return wcodseg;
    }

    public void setWcodseg(String wcodseg) {
        this.wcodseg = wcodseg;
    }

    public String getWfecsis() {
        return wfecsis;
    }

    public void setWfecsis(String wfecsis) {
        this.wfecsis = wfecsis;
    }

    public String getWhora() {
        return whora;
    }

    public void setWhora(String whora) {
        this.whora = whora;
    }

    public BigDecimal getWcargo() {
        return wcargo;
    }

    public void setWcargo(BigDecimal wcargo) {
        this.wcargo = wcargo;
    }

    public BigDecimal getWimptot() {
        return wimptot;
    }

    public void setWimptot(BigDecimal wimptot) {
        this.wimptot = wimptot;
    }

    public String getWnombre() {
        return wnombre;
    }

    public void setWnombre(String wnombre) {
        this.wnombre = wnombre;
    }

    public String getWnombrea() {
        return wnombrea;
    }

    public void setWnombrea(String wnombrea) {
        this.wnombrea = wnombrea;
    }

    public Short getWcodmon() {
        return wcodmon;
    }

    public void setWcodmon(Short wcodmon) {
        this.wcodmon = wcodmon;
    }

    public BigDecimal getWimporte() {
        return wimporte;
    }

    public void setWimporte(BigDecimal wimporte) {
        this.wimporte = wimporte;
    }

    public Short getWcodmon2() {
        return wcodmon2;
    }

    public void setWcodmon2(Short wcodmon2) {
        this.wcodmon2 = wcodmon2;
    }

    public BigDecimal getWimporte2() {
        return wimporte2;
    }

    public void setWimporte2(BigDecimal wimporte2) {
        this.wimporte2 = wimporte2;
    }

    public BigDecimal getWcotact() {
        return wcotact;
    }

    public void setWcotact(BigDecimal wcotact) {
        this.wcotact = wcotact;
    }

    public String getWcomven() {
        return wcomven;
    }

    public void setWcomven(String wcomven) {
        this.wcomven = wcomven;
    }

    public String getWpost() {
        return wpost;
    }

    public void setWpost(String wpost) {
        this.wpost = wpost;
    }

    public String getWrej1() {
        return wrej1;
    }

    public void setWrej1(String wrej1) {
        this.wrej1 = wrej1;
    }

    public String getWcodtrn() {
        return wcodtrn;
    }

    public void setWcodtrn(String wcodtrn) {
        this.wcodtrn = wcodtrn;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String toString() {
        return "estado=" + estado +
                ", descripcion=" + descripcion +
                ", numeroGiro=" + numeroGiro +
                ", wnumusr=" + wnumusr +
                ", wnumseq=" + wnumseq +
                ", wcodseg=" + wcodseg +
                ", wfecsis=" + wfecsis +
                ", whora=" + whora +
                ", wcargo=" + wcargo +
                ", wimptot=" + wimptot +
                ", wnombre=" + wnombre +
                ", wnombrea=" + wnombrea +
                ", wcodmon=" + wcodmon +
                ", wimporte=" + wimporte +
                ", wcodmon2=" + wcodmon2 +
                ", wimporte2=" + wimporte2 +
                ", wcotact=" + wcotact +
                ", wcomven=" + wcomven +
                ", wpost=" + wpost +
                ", wrej1=" + wrej1 +
                ", wcodtrn=" + wcodtrn +
                ", ref=" + ref;
    }

}
