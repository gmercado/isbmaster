package bus.consumoweb.giromovil;

import bus.consumoweb.consumer.ConsumidorServiciosWeb;
import bus.consumoweb.consumer.DefaultHandlerRespuesta;
import bus.consumoweb.consumer.ExtraerDatoHandler;
import bus.consumoweb.giromovil.model.EmitirGiroRequest;
import bus.consumoweb.giromovil.model.EmitirGiroResponse;
import bus.consumoweb.giromovil.model.GiroMovil;
import bus.consumoweb.model.ConsumidorAqua;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.monitor.api.ImposibleLeerRespuestaException;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.util.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class GiroMovilDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(GiroMovilDao.class);

    private final ConsumidorServiciosWeb consumidorServiciosWeb;
    private final MedioAmbiente medioAmbiente;

    @Inject
    public GiroMovilDao(@ConsumidorAqua ConsumidorServiciosWeb consumidorServiciosWeb, MedioAmbiente medioAmbiente) {
        this.consumidorServiciosWeb = consumidorServiciosWeb;
        this.medioAmbiente = medioAmbiente;
    }

    public EmitirGiroResponse procesarGiroMovil(final GiroMovil datosGiro) {
        //ENVIAR GIRO AL AQUA
        final EmitirGiroRequest request = new EmitirGiroRequest(datosGiro);
        EmitirGiroResponse responseTemp = new EmitirGiroResponse();

        // Si no existen errores de validación envía al Aqua
        try {
            responseTemp = emitirGiro(request);
            LOGGER.info("giro -> {}", responseTemp);
        } catch (Exception e) {
            LOGGER.error("Error al enviar giro movil al aqua", e);
            responseTemp.setEstado("999");  // Error de comunicacion
            responseTemp.setDescripcion("Error de comunicacion. Por favor vuelve a intentar, si el problema persiste llama al 800-10-5555");
            responseTemp.setWpost("P");
        }
        return responseTemp;
    }

    private EmitirGiroResponse emitirGiro(EmitirGiroRequest giroRequest)
            throws ImposibleLeerRespuestaException, ParseException, TimeoutException, ExecutionException {
        LOGGER.debug("Iniciando envio giroMovil al aqua");

        HashMap<String, String> interpolar = new HashMap<>();
        interpolar.put("canal", giroRequest.getCanal());
        interpolar.put("importe", giroRequest.getImporte().toString());
        interpolar.put("moneda", giroRequest.getMoneda().toString());
        interpolar.put("cuentaNumero", giroRequest.getCuentaNumero().toString());
        interpolar.put("cuentaMoneda", giroRequest.getCuentaMoneda().toString());
        interpolar.put("cuentaTipo", giroRequest.getCuentaTipo());
        interpolar.put("usuarioEbisa", StringUtils.trimToEmpty(giroRequest.getUsuarioEbisa()));
        interpolar.put("fono", giroRequest.getFono());
        interpolar.put("beneficiario", StringUtils.trimToEmpty(reemplazarCaracteresRaros(giroRequest.getBeneficiario()).toUpperCase()));
        interpolar.put("remitente", StringUtils.trimToEmpty(reemplazarCaracteresRaros(giroRequest.getRemitente())));
        interpolar.put("nota", StringUtils.trimToEmpty(reemplazarCaracteresRaros(giroRequest.getNota())));
        interpolar.put("tipoDocBeneficiario", giroRequest.getTipoDocBeneficiario());
        interpolar.put("docBeneficiario", giroRequest.getDocBeneficiario());
        interpolar.put("fonoRemitente", giroRequest.getFonoRemitente());
        interpolar.put("correoRemitente", StringUtils.trimToEmpty(reemplazarCaracteresRaros(giroRequest.getCorreoRemitente())));

        LOGGER.debug("Solicitud Aqua: {}", interpolar);
        final InputStream resourceAsStream = getClass().getResourceAsStream("emitirGiroRequest.xml");
        final String template;
        try {
            template = IOUtils.toString(resourceAsStream);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        final ArrayList<String> estado = new ArrayList<>();
        final ArrayList<String> descripcion = new ArrayList<>();
        final ArrayList<String> numeroGiro = new ArrayList<>();
        final ArrayList<String> wnumusr = new ArrayList<>();
        final ArrayList<String> wnumseq = new ArrayList<>();
        final ArrayList<String> wcodseg = new ArrayList<>();
        final ArrayList<String> wfecsis = new ArrayList<>();
        final ArrayList<String> whora = new ArrayList<>();
        final ArrayList<String> wcargo = new ArrayList<>();
        final ArrayList<String> wimptot = new ArrayList<>();
        final ArrayList<String> wnombre = new ArrayList<>();
        final ArrayList<String> wnombrea = new ArrayList<>();
        final ArrayList<String> wcodmon = new ArrayList<>();
        final ArrayList<String> wimporte = new ArrayList<>();
        final ArrayList<String> wcodmon2 = new ArrayList<>();
        final ArrayList<String> wimporte2 = new ArrayList<>();
        final ArrayList<String> wcotact = new ArrayList<>();
        final ArrayList<String> wcomven = new ArrayList<>();
        final ArrayList<String> wpost = new ArrayList<>();
        final ArrayList<String> wrej1 = new ArrayList<>();
        final ArrayList<String> wcodtrn = new ArrayList<>();
        final ArrayList<String> ref = new ArrayList<>();

        final ArrayList[] arrayLists = {estado, descripcion, numeroGiro, wnumusr, wnumseq, wcodseg,
                wfecsis, whora, wcargo, wimptot, wnombre, wnombrea, wcodmon, wimporte,
                wcodmon2, wimporte2, wcotact, wcomven, wpost, wrej1, wcodtrn, ref};

        @SuppressWarnings({"unchecked"})
        final DefaultHandlerRespuesta handler = new ExtraerDatoHandler(arrayLists,
                () -> "status=" + estado.toString(), "estado", "descripcion", "numeroDeGiro", "wnumusr", "wnumseq", "wcodseg",
                "wfecsis", "whora", "wcargo", "wimptot", "wnombre", "wnombrea", "wcodmon", "wimporte",
                "wcodmon2", "wimporte2", "wcotact", "wcomven", "wpost", "wrej1", "wcodtrn", "ref");

        LOGGER.debug("Enviando Emision de Giro Movil {} al Aqua ...", giroRequest);

        consumidorServiciosWeb.consumir(template, interpolar, handler,
                medioAmbiente.getValorIntDe(Variables.WC_GIROMOVIL_TIMEOUT, Variables.WC_GIROMOVIL_TIMEOUT_DEFAULT)
        );

        EmitirGiroResponse giroResponse =
                new EmitirGiroResponse(estado, descripcion, numeroGiro, wnumusr, wnumseq, wcodseg,
                        wfecsis, whora, wcargo, wimptot, wnombre, wnombrea, wcodmon, wimporte,
                        wcodmon2, wimporte2, wcotact, wcomven, wpost, wrej1, wcodtrn, ref);

        LOGGER.debug("Respuesta Giro Movil del Aqua {}", giroResponse);
        LOGGER.debug("Numero de Giro: {}", giroResponse.getNumeroGiroDesencriptado());

        if (giroResponse.getEstado() == null) {
            throw new ImposibleLeerRespuestaException("Error en emitirGiro, no se encontraron los valores de la respuesta aqua",
                    ImposibleLeerRespuestaException.CODIGO_RESPUESTA_VACIA);
        }

        return giroResponse;
    }

    public EmitirGiroResponse emitirGiroPreproceso(EmitirGiroRequest giroRequest)
            throws ImposibleLeerRespuestaException, ParseException, TimeoutException, ExecutionException {
        LOGGER.debug("Iniciando envio giroMovilPreproceso al aqua");

        HashMap<String, String> interpolar = new HashMap<>();
        interpolar.put("canal", giroRequest.getCanal());
        interpolar.put("importe", giroRequest.getImporte().toString());
        interpolar.put("moneda", giroRequest.getMoneda().toString());
        interpolar.put("cuentaNumero", giroRequest.getCuentaNumero().toString());
        interpolar.put("cuentaMoneda", giroRequest.getCuentaMoneda().toString());
        interpolar.put("cuentaTipo", giroRequest.getCuentaTipo());
        interpolar.put("usuarioEbisa", giroRequest.getUsuarioEbisa());
        interpolar.put("fono", giroRequest.getFono());
        interpolar.put("fonoRemitente", giroRequest.getFonoRemitente());
        interpolar.put("correoRemitente", giroRequest.getCorreoRemitente());

        LOGGER.debug("Solicitud Aqua: {}", interpolar);
        final InputStream resourceAsStream = getClass().getResourceAsStream("emitirGiroPreprocesoRequest.xml");
        final String template;
        try {
            template = IOUtils.toString(resourceAsStream);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        final ArrayList<String> estado = new ArrayList<>();
        final ArrayList<String> descripcion = new ArrayList<>();
        final ArrayList<String> wfecsis = new ArrayList<>();
        final ArrayList<String> whora = new ArrayList<>();
        final ArrayList<String> wcargo = new ArrayList<>();
        final ArrayList<String> wimptot = new ArrayList<>();
        final ArrayList<String> wcodmon = new ArrayList<>();
        final ArrayList<String> wimporte = new ArrayList<>();
        final ArrayList<String> wcodmon2 = new ArrayList<>();
        final ArrayList<String> wimporte2 = new ArrayList<>();
        final ArrayList<String> wcotact = new ArrayList<>();
        final ArrayList<String> wcomven = new ArrayList<>();

        final ArrayList[] arrayLists = {estado, descripcion,
                wfecsis, whora, wcargo, wimptot, wcodmon, wimporte,
                wcodmon2, wimporte2, wcotact, wcomven};

        @SuppressWarnings({"unchecked"})
        final DefaultHandlerRespuesta handler = new ExtraerDatoHandler(arrayLists,
                () -> "status=" + estado.toString(), "estado", "descripcion",
                "wfecsis", "whora", "wcargo", "wimptot", "wcodmon", "wimporte",
                "wcodmon2", "wimporte2", "wcotact", "wcomven");

        LOGGER.debug("Enviando Emision de Giro Movil Preproceso {} al Aqua ...", giroRequest);

        consumidorServiciosWeb.consumir(template, interpolar, handler,
                medioAmbiente.getValorIntDe(Variables.WC_GIROMOVIL_TIMEOUT, Variables.WC_GIROMOVIL_TIMEOUT_DEFAULT)
        );

        EmitirGiroResponse giroResponse =
                new EmitirGiroResponse(estado, descripcion, null, null, null, null,
                        wfecsis, whora, wcargo, wimptot, null, null, wcodmon, wimporte,
                        wcodmon2, wimporte2, wcotact, wcomven, null, null, null, null);

        LOGGER.debug("Respuesta Giro Movil Preproceso{}", giroResponse);

        if (giroResponse.getEstado() == null) {
            throw new ImposibleLeerRespuestaException("Error en emitirGiroPreproceso, no se encontraron los valores de la respuesta aqua",
                    ImposibleLeerRespuestaException.CODIGO_RESPUESTA_VACIA);
        }

        return giroResponse;
    }

    private String reemplazarCaracteresRaros(String input) {
        if (StringUtils.trimToNull(input) != null) {
            return input.replaceAll("[^\\x00-\\x7F]", "");
        }
        return input;
    }
}
