package bus.consumoweb.pagoservicios;

import bus.consumoweb.consumer.ConsumidorServiciosWeb;
import bus.consumoweb.consumer.DefaultHandlerRespuesta;
import bus.consumoweb.model.ConsumidorAqua;
import bus.consumoweb.pagoservicios.model.*;
import bus.env.api.MedioAmbiente;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.io.Resources;
import com.google.inject.Inject;
import org.apache.wicket.util.string.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static bus.env.api.Variables.*;
import static com.google.common.base.CharMatcher.WHITESPACE;
import static com.google.common.base.Charsets.UTF_8;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.io.Resources.getResource;
import static org.apache.wicket.util.string.Strings.escapeMarkup;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class PagoServiciosDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(PagoServiciosDao.class);
    private final ConsumidorServiciosWeb consumidorServiciosWeb;
    private final MedioAmbiente medioAmbiente;
    private final JAXBContext jaxbContext;

    @Inject
    public PagoServiciosDao(@ConsumidorAqua ConsumidorServiciosWeb consumidorServiciosWeb, MedioAmbiente medioAmbiente) throws JAXBException {
        this.consumidorServiciosWeb = consumidorServiciosWeb;
        this.medioAmbiente = medioAmbiente;
        this.jaxbContext = JAXBContext.newInstance(ListadoDeServiciosResponse.class, Servicio.class, ElementoBusqueda.class,
                ConsultaCuentasDeServiciosResponse.class, Deuda.class, ConsultaDeudasDeServiciosResponse.class,
                Psr9101.class, Psr9120.class, PagoDeudaDeServiciosResponse.class, CuentaDato.class);
    }

    public ListadoDeServiciosResponse consultarServicios()
            throws TimeoutException, ExecutionException, IOException, JAXBException {
        ListadoDeServiciosResponse value = null;
        final URL resource = getResource(PagoServiciosDao.class, "listadoDeServicios-template.xml");
        final String template = Resources.toString(resource, UTF_8);
        final String canal = medioAmbiente.getValorDe(PAGO_SERVICIOS_CANAL, PAGO_SERVICIOS_CANAL_DEFAULT);
        final int timeout = medioAmbiente.getValorIntDe(PAGO_SERVICIOS_CONSULTA_SERVICIOS_TIMEOUT,
                PAGO_SERVICIOS_CONSULTA_SERVICIOS_TIMEOUT_DEFAULT);
        final Object consumir = consumidorServiciosWeb.consumir(template,
                ImmutableMap.of("canal", canal, "cuales", "TODOS"),
                new DefaultHandlerRespuesta(jaxbContext, ListadoDeServiciosResponse.class),
                timeout);
        //noinspection unchecked
        final JAXBElement<ListadoDeServiciosResponse> lds = (JAXBElement<ListadoDeServiciosResponse>) consumir;
        if (lds != null) {
            value = lds.getValue();
        }
        return value;
    }

    public ConsultaCuentasResponse consultarCuentas(Servicio srv, Busqueda busqueda,
                                                    ElementoBusqueda elementoBusqueda,
                                                    BigDecimal nroCliente,
                                                    FavoritoPagoServicios fav)
            throws TimeoutException, ExecutionException, IOException, JAXBException {

        final URL resource = getResource(PagoServiciosDao.class, "consultaCuentas-template.xml");
        final String template = Resources.toString(resource, UTF_8);
        final String canal = medioAmbiente.getValorDe(PAGO_SERVICIOS_CANAL, PAGO_SERVICIOS_CANAL_DEFAULT);
        final int timeout = medioAmbiente.getValorIntDe(PAGO_SERVICIOS_CONSULTA_DEUDAS_TIMEOUT,
                PAGO_SERVICIOS_CONSULTA_DEUDAS_TIMEOUT_DEFAULT);

        ImmutableMap.Builder<String, String> put = ImmutableMap.<String, String>builder().
                put("canal", canal).
                put("empresaCodigo", srv.getEmpresaCodigo()).
                put("tipoCodigo", srv.getCodigo()).
                put("tipoBusqueda", busqueda.getTipo()).
                put("valorBusqueda", busqueda.getValor()).
                put("cobranza", obtenerCobranza(busqueda)).
                put("nroCliente", nroCliente == null ? "0" : nroCliente.toPlainString()).
                put("moneda", busqueda.getMoneda() == null ? "0" : busqueda.getMoneda().toString());

        if (isNullOrEmpty(busqueda.getValor2())) {
            put.put("valorBusqueda2", busqueda.getValor());
        } else {
            put.put("valorBusqueda2", busqueda.getValor2());
        }

        final ImmutableMap<String, String> mapa = put.build();

        final DefaultHandlerRespuesta handler = new DefaultHandlerRespuesta(jaxbContext, ConsultaCuentasDeServiciosResponse.class);
        final Object consumir = consumidorServiciosWeb.consumir(template, mapa, handler, timeout);

        //noinspection unchecked
        final JAXBElement<ConsultaCuentasDeServiciosResponse> lds =
                (JAXBElement<ConsultaCuentasDeServiciosResponse>) consumir;
        if (lds != null) {
            return new ConsultaCuentasResponse(lds.getValue(), srv, elementoBusqueda, busqueda, fav);
        }
        return null;
    }

    private static String obtenerCobranza(Busqueda busqueda) {
        final String cobranza;
        if (busqueda.getNit() != null && busqueda.getRazonSocial() != null && busqueda.getImporte() == null) {
            cobranza = "<ws:cobranza razonSocial=\"" + Strings.escapeMarkup(busqueda.getRazonSocial()) +
                    "\" nit=\"" + Strings.escapeMarkup(busqueda.getNit()) +
                    "\"/>";
        } else if (busqueda.getNit() == null || busqueda.getRazonSocial() == null || busqueda.getImporte() == null) {
            cobranza = "";
        } else {
            cobranza = "<ws:cobranza razonSocial=\"" + Strings.escapeMarkup(busqueda.getRazonSocial()) +
                    "\" nit=\"" + Strings.escapeMarkup(busqueda.getNit()) +
                    "\" monto=\"" + busqueda.getImporte() + "\"/>";
        }
        return cobranza;
    }

    public ConsultaDeudasResponse consultarDeudas(Cuenta cuenta,
                                                  Servicio servicio,
                                                  Busqueda busqueda,
                                                  ElementoBusqueda elementoBusqueda,
                                                  FavoritoPagoServicios fav)
            throws IOException, JAXBException, TimeoutException, ExecutionException {

        final URL resource = getResource(PagoServiciosDao.class, "consultaDeudas-template.xml");

        final String template = Resources.toString(resource, UTF_8);

        final String canal = medioAmbiente.getValorDe(PAGO_SERVICIOS_CANAL, PAGO_SERVICIOS_CANAL_DEFAULT);

        final StringBuilder datoCuenta = new StringBuilder(50 * cuenta.getDato().size());
        for (CuentaDato dato : cuenta.getDato()) {
            datoCuenta.
                    append("<ws:datoCuenta key=\"").
                    append(Strings.escapeMarkup(dato.getKey())).
                    append("\" value=\"").
                    append(Strings.escapeMarkup(dato.getValue())).
                    append("\"/>");
        }

        final int timeout = medioAmbiente.getValorIntDe(PAGO_SERVICIOS_CONSULTA_DEUDAS_TIMEOUT,
                PAGO_SERVICIOS_CONSULTA_DEUDAS_TIMEOUT_DEFAULT);

        ImmutableMap.Builder<String, String> put = ImmutableMap.<String, String>builder().
                put("codCuenta", cuenta.getCodCuenta()).
                put("canal", canal).
                put("empresaCodigo", servicio.getEmpresaCodigo()).
                put("tipoCodigo", servicio.getCodigo()).
                put("tipoBusqueda", busqueda.getTipo()).
                put("valorBusqueda", busqueda.getValor()).
                put("cobranza", obtenerCobranza(busqueda)).
                put("datoCuenta", datoCuenta.toString());

        if (isNullOrEmpty(busqueda.getValor2())) {
            put.put("valorBusqueda2", busqueda.getValor());
        } else {
            put.put("valorBusqueda2", busqueda.getValor2());
        }

        final ImmutableMap<String, String> mapa = put.build();
        final DefaultHandlerRespuesta handler = new DefaultHandlerRespuesta(jaxbContext, ConsultaDeudasDeServiciosResponse.class);
        final Object consumir = consumidorServiciosWeb.consumir(template, mapa, handler, timeout);

        //noinspection unchecked
        final JAXBElement<ConsultaDeudasDeServiciosResponse> lds =
                (JAXBElement<ConsultaDeudasDeServiciosResponse>) consumir;

        if (lds != null) {
            return new ConsultaDeudasResponse(lds.getValue(), cuenta, servicio, elementoBusqueda, busqueda, fav);
        }
        return null;
    }

    public PagoDeudaDeServiciosResponse pagarServicioConIntermediario(final PagoDeudaServicio pagoDeudaServicio) {
        // 2. enviar el pago de servicios al aqua
        PagoDeudaDeServiciosResponse psr = null;
        try {
            final URL resource = getResource(PagoServiciosDao.class, "pagoDeuda-template.xml");
            final String template = Resources.toString(resource, UTF_8);

            String canal = medioAmbiente.getValorDe(PAGO_SERVICIOS_CANAL, PAGO_SERVICIOS_CANAL_DEFAULT);
            int timeout = medioAmbiente.getValorIntDe(PAGO_SERVICIOS_PAGO_DEUDAS_TIMEOUT, PAGO_SERVICIOS_PAGO_DEUDAS_TIMEOUT_DEFAULT);

            final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
            builder.put("canal", canal).
                    put("codCuenta", pagoDeudaServicio.getCuenta().getCodCuenta()).
                    put("importe", pagoDeudaServicio.getFactura().getMonto().toPlainString()).
                    put("nroFactura", pagoDeudaServicio.getFactura().getNro() == null ? "" : pagoDeudaServicio.getFactura().getNro()).
                    put("periodoFactura", pagoDeudaServicio.getFactura().getPeriodo()).
                    put("numeroOperacion", "0").
                    put("codigoBisa", "SI").
                    put("empresaCodigo", pagoDeudaServicio.getServicio().getEmpresaCodigo()).
                    put("tipoCodigo", pagoDeudaServicio.getServicio().getCodigo()).
                    put("tipoBusqueda", pagoDeudaServicio.getBusqueda().getTipo()).
                    put("valorBusqueda", pagoDeudaServicio.getBusqueda().getValor()).
                    put("nroCliente", pagoDeudaServicio.getNroCliente()).
                    put("nroCuenta", pagoDeudaServicio.getCuentaDebito()).
                    put("monedaCta", pagoDeudaServicio.getMonedaDebito()).
                    put("moneda", pagoDeudaServicio.getMonedaServicio()).
                    put("tipoCta", pagoDeudaServicio.getTipoCuentaDebito()).
                    put("idUsuario", pagoDeudaServicio.getIdUsuario()).
                    put("username", pagoDeudaServicio.getUserName()).
                    put("fullname", pagoDeudaServicio.getFullName());

            if (isNullOrEmpty(pagoDeudaServicio.getBusqueda().getValor2())) {
                builder.put("valorBusqueda2", pagoDeudaServicio.getBusqueda().getValor());
            } else {
                builder.put("valorBusqueda2", pagoDeudaServicio.getBusqueda().getValor2());
            }

            if (Iterables.contains(Splitter.on(WHITESPACE).trimResults().omitEmptyStrings().split(
                    medioAmbiente.getValorDe(PAGO_SERVICIOS_EMPRESAS_QUE_PREFACTURAN, PAGO_SERVICIOS_EMPRESAS_QUE_PREFACTURAN_DEFAULT)),
                    pagoDeudaServicio.getServicio().getEmpresaCodigo())) {
                builder.put("direccionEnvio", "");
                builder.put("facturarElectronicamente", "NO_FACTURAR");
            } else if (pagoDeudaServicio.getTipoEntrega() == TipoEntrega.DIRECCION) {
                builder.put("direccionEnvio", "<ws:envioFactura><ws:direccion direccion=\"" +
                        escapeMarkup(pagoDeudaServicio.getDireccion()) +
                        "\" ciudad=\"" +
                        pagoDeudaServicio.getCiudad() +
                        "\" departamento=\"" +
                        pagoDeudaServicio.getDepartamento() +
                        "\"/></ws:envioFactura>");
                if ("SI".equalsIgnoreCase(medioAmbiente.getValorDe(PAGO_SERVICIOS_FACTURAR_ACTIVAR, PAGO_SERVICIOS_FACTURAR_ACTIVAR_DEFAULT))) {
                    builder.put("facturarElectronicamente", "SI_FACTURAR");
                } else {
                    builder.put("facturarElectronicamente", "NO_FACTURAR");
                }
            } else {
                builder.put("direccionEnvio", "<ws:envioFactura><ws:sucursal>" +
                        pagoDeudaServicio.getAgencia() +
                        "</ws:sucursal></ws:envioFactura>");
                builder.put("facturarElectronicamente", "NO_FACTURAR");
            }

            if (pagoDeudaServicio.getBusqueda().getNit() == null ||
                    pagoDeudaServicio.getBusqueda().getRazonSocial() == null ||
                    pagoDeudaServicio.getBusqueda().getImporte() == null) {
                builder.put("cobranza", "");
            } else {
                builder.put("cobranza",
                        "<ws:cobranza razonSocial=\"" + Strings.escapeMarkup(pagoDeudaServicio.getBusqueda().getRazonSocial()) +
                                "\" nit=\"" + Strings.escapeMarkup(pagoDeudaServicio.getBusqueda().getNit()) +
                                "\" monto=\"" + pagoDeudaServicio.getBusqueda().getImporte() + "\"/>");
            }

            final StringBuilder datoCuenta = new StringBuilder(50 * pagoDeudaServicio.getCuenta().getDato().size());
            for (CuentaDato dato : pagoDeudaServicio.getCuenta().getDato()) {
                datoCuenta.
                        append("<ws:datoCuenta key=\"").
                        append(Strings.escapeMarkup(dato.getKey())).
                        append("\" value=\"").
                        append(Strings.escapeMarkup(dato.getValue())).
                        append("\"/>");
            }
            builder.put("datoCuenta", datoCuenta.toString());

            final StringBuilder datoFactura = new StringBuilder(50 * pagoDeudaServicio.getCuenta().getDato().size());
            for (Deuda.Factura.Dato dato : pagoDeudaServicio.getFactura().getDato()) {
                datoFactura.
                        append("<ws:datoFactura key=\"").
                        append(Strings.escapeMarkup(dato.getKey())).
                        append("\" value=\"").
                        append(Strings.escapeMarkup(dato.getValue())).
                        append("\"/>");
            }
            builder.put("datoFactura", datoFactura.toString());

            // El timeout es en segundos
            final Object consumir = consumidorServiciosWeb.consumir(template, builder.build(),
                    new DefaultHandlerRespuesta(jaxbContext, PagoDeudaDeServiciosResponse.class),
                    2 * (timeout / 1000));

            @SuppressWarnings("unchecked")
            final JAXBElement<PagoDeudaDeServiciosResponse> lds = (JAXBElement<PagoDeudaDeServiciosResponse>) consumir;
            if (lds != null) {
                psr = lds.getValue();
            }
            //facturaDao.facturacionElectronica(pagoDeudaServicio.getFacturaSolicitud());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } catch (ExecutionException e) {
            // Este es timeout de socket.... es el unico que deberia ocurrir, porque el timeout de socket es menor al
            // de aplicacion.... por la mitad. Hacemos como cuando ocurre un error de timeout en el monitor!

        } catch (TimeoutException e) {
            LOGGER.error("Error de timeout de aplicacion al pagar un servicio. ESTO NO DEBERIA OCURRIR y es una falla de parametros o programacion", e);
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al consumir un servicio web", e);
        }
        return psr;
    }
}
