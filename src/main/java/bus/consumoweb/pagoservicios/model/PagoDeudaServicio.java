package bus.consumoweb.pagoservicios.model;

import bus.consumoweb.facturacion.model.FacturaSolicitud;

import java.io.Serializable;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class PagoDeudaServicio implements Serializable {
    private static final long serialVersionUID = -2598759792246290272L;

    private Servicio servicio;
    private Busqueda busqueda;
    private Cuenta cuenta;
    private Deuda.Factura factura;
    private String monedaServicio;
    private String cuentaDebito;
    private String monedaDebito;
    private String tipoCuentaDebito;
    private String userName;
    private String idUsuario;
    private String fullName;
    private String nroCliente;
    private TipoEntrega tipoEntrega;
    private String ciudad;
    private String departamento;
    private String direccion;
    private String agencia;
    private FacturaSolicitud facturaSolicitud;

    public String getMonedaServicio() {
        return monedaServicio;
    }

    public void setMonedaServicio(String monedaServicio) {
        this.monedaServicio = monedaServicio;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getMonedaDebito() {
        return monedaDebito;
    }

    public void setMonedaDebito(String monedaDebito) {
        this.monedaDebito = monedaDebito;
    }

    public String getTipoCuentaDebito() {
        return tipoCuentaDebito;
    }

    public void setTipoCuentaDebito(String tipoCuentaDebito) {
        this.tipoCuentaDebito = tipoCuentaDebito;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNroCliente() {
        return nroCliente;
    }

    public void setNroCliente(String nroCliente) {
        this.nroCliente = nroCliente;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setBusqueda(Busqueda busqueda) {
        this.busqueda = busqueda;
    }

    public Busqueda getBusqueda() {
        return busqueda;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setFactura(Deuda.Factura factura) {
        this.factura = factura;
    }

    public Deuda.Factura getFactura() {
        return factura;
    }

    public void setCuentaDebito(String cuentaDebito) {
        this.cuentaDebito = cuentaDebito;
    }

    public String getCuentaDebito() {
        return cuentaDebito;
    }

    public TipoEntrega getTipoEntrega() {
        return tipoEntrega;
    }

    public void setTipoEntrega(TipoEntrega tipoEntrega) {
        this.tipoEntrega = tipoEntrega;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public FacturaSolicitud getFacturaSolicitud() {
        return facturaSolicitud;
    }

    public void setFacturaSolicitud(FacturaSolicitud facturaSolicitud) {
        this.facturaSolicitud = facturaSolicitud;
    }

    @Override
    public String toString() {
        return "PagoDeudaServicio{" +
                "tipoEntrega=" + tipoEntrega +
                ", servicio=" + servicio +
                ", busqueda=" + busqueda +
                ", cuenta=" + cuenta +
                ", factura=" + factura +
                '}';
    }
}
