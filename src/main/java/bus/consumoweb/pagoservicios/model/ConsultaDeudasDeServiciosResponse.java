package bus.consumoweb.pagoservicios.model;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * <p>Java class for anonymous complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="deuda" type="{http://aqua.bisa.com/servicios/pagoservicios/ws}deuda"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "deuda"
})
@XmlRootElement(name = "consultaDeudasDeServiciosResponse")
public class ConsultaDeudasDeServiciosResponse implements Serializable {

    private static final long serialVersionUID = -158972669561480466L;

    @XmlElement(required = true)
    protected Deuda deuda;

    /**
     * Gets the value of the deuda property.
     *
     * @return possible object is
     * {@link Deuda }
     */
    public Deuda getDeuda() {
        return deuda;
    }

    /**
     * Sets the value of the deuda property.
     *
     * @param value allowed object is
     *              {@link Deuda }
     */
    public void setDeuda(Deuda value) {
        this.deuda = value;
    }

    @Override
    public String toString() {
        return deuda.toString();
    }
}
