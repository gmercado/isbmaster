package bus.consumoweb.pagoservicios.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for anonymous complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="servicio" type="{http://aqua.bisa.com/servicios/pagoservicios/ws}servicio" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "servicio"
})
@XmlRootElement(name = "listadoDeServiciosResponse")
public class ListadoDeServiciosResponse implements Serializable {

    private static final long serialVersionUID = 1777730360015153997L;

    protected List<Servicio> servicio;

    /**
     * Gets the value of the servicio property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servicio property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicio().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Servicio }
     */
    public List<Servicio> getServicio() {
        if (servicio == null) {
            servicio = new ArrayList<>();
        }
        return this.servicio;
    }

    @Override
    public String toString() {
        return getServicio().toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ListadoDeServiciosResponse)) return false;
        ListadoDeServiciosResponse response = (ListadoDeServiciosResponse) o;
        return servicio != null ? servicio.equals(response.servicio) : response.servicio == null;
    }

    @Override
    public int hashCode() {
        return servicio != null ? servicio.hashCode() : 0;
    }
}
