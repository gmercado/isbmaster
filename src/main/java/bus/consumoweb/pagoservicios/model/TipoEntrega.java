package bus.consumoweb.pagoservicios.model;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public enum TipoEntrega {
    DIRECCION,
    AGENCIA
}
