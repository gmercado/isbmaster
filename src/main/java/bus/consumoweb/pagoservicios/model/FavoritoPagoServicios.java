package bus.consumoweb.pagoservicios.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class FavoritoPagoServicios implements Serializable {

    private static final long serialVersionUID = -8624907219512835628L;

    private BigDecimal importe;

    private Short moneda;

    private Date fechaIngreso;

    private String pagoServicioCodigoEmpresa;

    private String pagoServicioCodigoServicio;

    private String pagoServicioCodigoTipoBusqueda;

    private String pagoServicioValorBusqueda;

    private String pagoServicioValorBusqueda2;

    private String pagoServicioNitciFactura;

    private String pagoServicioNombreFactura;

    private String pagoServicioEmpresaNombre;

    private String pagoServicioDescripcionServicio;

    private String pagoServicioDescripcionBusqueda;

    private String pagoServicioDireccion;

    private Number codCiu;

    private String sucursalDestino;

    private BigDecimal cuentaOrigen;

    private String nombreAlumno;

    private String alias;

    public FavoritoPagoServicios() {
    }

    public FavoritoPagoServicios(BigDecimal importe, Short moneda, Date fechaIngreso,
                                 String pagoServicioCodigoEmpresa, String pagoServicioCodigoServicio,
                                 String pagoServicioCodigoTipoBusqueda, String pagoServicioValorBusqueda,
                                 String pagoServicioNitciFactura, String pagoServicioNombreFactura,
                                 String pagoServicioEmpresaNombre, String pagoServicioDescripcionServicio,
                                 String pagoServicioDescripcionBusqueda, String pagoServicioDireccion,
                                 Number codCiu, String sucursalDestino, BigDecimal cuentaOrigen, String nombreAlumno,
                                 String alias, String pagoServicioValorBusqueda2) {
        this.importe = importe;
        this.moneda = moneda;
        this.fechaIngreso = fechaIngreso;
        this.pagoServicioCodigoEmpresa = pagoServicioCodigoEmpresa;
        this.pagoServicioCodigoServicio = pagoServicioCodigoServicio;
        this.pagoServicioCodigoTipoBusqueda = pagoServicioCodigoTipoBusqueda;
        this.pagoServicioValorBusqueda = pagoServicioValorBusqueda;
        this.pagoServicioNitciFactura = pagoServicioNitciFactura;
        this.pagoServicioNombreFactura = pagoServicioNombreFactura;
        this.pagoServicioEmpresaNombre = pagoServicioEmpresaNombre;
        this.pagoServicioDescripcionServicio = pagoServicioDescripcionServicio;
        this.pagoServicioDescripcionBusqueda = pagoServicioDescripcionBusqueda;
        this.pagoServicioDireccion = pagoServicioDireccion;
        this.codCiu = codCiu;
        this.sucursalDestino = sucursalDestino;
        this.cuentaOrigen = cuentaOrigen;
        this.nombreAlumno = nombreAlumno;
        this.alias = alias;
        this.pagoServicioValorBusqueda2 = pagoServicioValorBusqueda2;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public Short getMoneda() {
        return moneda;
    }

    public void setMoneda(Short moneda) {
        this.moneda = moneda;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getPagoServicioCodigoEmpresa() {
        return pagoServicioCodigoEmpresa;
    }

    public void setPagoServicioCodigoEmpresa(String pagoServicioCodigoEmpresa) {
        this.pagoServicioCodigoEmpresa = pagoServicioCodigoEmpresa;
    }

    public String getPagoServicioCodigoServicio() {
        return pagoServicioCodigoServicio;
    }

    public void setPagoServicioCodigoServicio(String pagoServicioCodigoServicio) {
        this.pagoServicioCodigoServicio = pagoServicioCodigoServicio;
    }

    public String getPagoServicioCodigoTipoBusqueda() {
        return pagoServicioCodigoTipoBusqueda;
    }

    public void setPagoServicioCodigoTipoBusqueda(String pagoServicioCodigoTipoBusqueda) {
        this.pagoServicioCodigoTipoBusqueda = pagoServicioCodigoTipoBusqueda;
    }

    public String getPagoServicioValorBusqueda() {
        return pagoServicioValorBusqueda;
    }

    public void setPagoServicioValorBusqueda(String pagoServicioValorBusqueda) {
        this.pagoServicioValorBusqueda = pagoServicioValorBusqueda;
    }

    public String getPagoServicioNitciFactura() {
        return pagoServicioNitciFactura;
    }

    public void setPagoServicioNitciFactura(String pagoServicioNitciFactura) {
        this.pagoServicioNitciFactura = pagoServicioNitciFactura;
    }

    public String getPagoServicioNombreFactura() {
        return pagoServicioNombreFactura;
    }

    public void setPagoServicioNombreFactura(String pagoServicioNombreFactura) {
        this.pagoServicioNombreFactura = pagoServicioNombreFactura;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FavoritoPagoServicios)) return false;

        FavoritoPagoServicios that = (FavoritoPagoServicios) o;
        if (moneda != null ? !moneda.equals(that.moneda) : that.moneda != null) return false;
        if (pagoServicioCodigoEmpresa != null ? !pagoServicioCodigoEmpresa.equals(that.pagoServicioCodigoEmpresa) : that.pagoServicioCodigoEmpresa != null)
            return false;
        if (pagoServicioCodigoServicio != null ? !pagoServicioCodigoServicio.equals(that.pagoServicioCodigoServicio) : that.pagoServicioCodigoServicio != null)
            return false;
        if (pagoServicioCodigoTipoBusqueda != null ? !pagoServicioCodigoTipoBusqueda.equals(that.pagoServicioCodigoTipoBusqueda) : that.pagoServicioCodigoTipoBusqueda != null)
            return false;
        if (pagoServicioValorBusqueda != null ? !pagoServicioValorBusqueda.equals(that.pagoServicioValorBusqueda) : that.pagoServicioValorBusqueda != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = moneda != null ? moneda.hashCode() : 0;
        result = 31 * result + (pagoServicioCodigoEmpresa != null ? pagoServicioCodigoEmpresa.hashCode() : 0);
        result = 41 * result + (pagoServicioCodigoServicio != null ? pagoServicioCodigoServicio.hashCode() : 0);
        result = 43 * result + (pagoServicioCodigoTipoBusqueda != null ? pagoServicioCodigoTipoBusqueda.hashCode() : 0);
        result = 127 * result + (pagoServicioValorBusqueda != null ? pagoServicioValorBusqueda.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FavoritoPagoServicios{" +
                "importe=" + importe +
                ", moneda=" + moneda +
                ", pagoServicioCodigoEmpresa=" + pagoServicioCodigoEmpresa +
                ", pagoServicioCodigoServicio=" + pagoServicioCodigoServicio +
                ", pagoServicioCodigoTipoBusqueda=" + pagoServicioCodigoTipoBusqueda +
                ", pagoServicioValorBusqueda=" + pagoServicioValorBusqueda +
                ", pagoServicioNitciFactura=" + pagoServicioNitciFactura +
                ", pagoServicioNombreFactura=" + pagoServicioNombreFactura +
                '}';
    }

    public String getPagoServicioEmpresaNombre() {
        return pagoServicioEmpresaNombre;
    }

    public void setPagoServicioEmpresaNombre(String pagoServicioEmpresaNombre) {
        this.pagoServicioEmpresaNombre = pagoServicioEmpresaNombre;
    }

    public String getPagoServicioDescripcionServicio() {
        return pagoServicioDescripcionServicio;
    }

    public void setPagoServicioDescripcionServicio(String pagoServicioDescripcionServicio) {
        this.pagoServicioDescripcionServicio = pagoServicioDescripcionServicio;
    }

    public String getPagoServicioDescripcionBusqueda() {
        return pagoServicioDescripcionBusqueda;
    }

    public void setPagoServicioDescripcionBusqueda(String pagoServicioDescripcionBusqueda) {
        this.pagoServicioDescripcionBusqueda = pagoServicioDescripcionBusqueda;
    }

    public String getPagoServicioDireccion() {
        return pagoServicioDireccion;
    }

    public void setPagoServicioDireccion(String pagoServicioDireccion) {
        this.pagoServicioDireccion = pagoServicioDireccion;
    }

    public Number getCodCiu() {
        return codCiu;
    }

    public void setCodCiu(Number codCiu) {
        this.codCiu = codCiu;
    }

    public String getSucursalDestino() {
        return sucursalDestino;
    }

    public void setSucursalDestino(String sucursalDestino) {
        this.sucursalDestino = sucursalDestino;
    }

    public BigDecimal getCuentaOrigen() {
        return cuentaOrigen;
    }

    public void setCuentaOrigen(BigDecimal cuentaOrigen) {
        this.cuentaOrigen = cuentaOrigen;
    }

    public String getNombreAlumno() {
        return nombreAlumno;
    }

    public void setNombreAlumno(String nombreAlumno) {
        this.nombreAlumno = nombreAlumno;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPagoServicioValorBusqueda2() {
        return pagoServicioValorBusqueda2;
    }

    public void setPagoServicioValorBusqueda2(String pagoServicioValorBusqueda2) {
        this.pagoServicioValorBusqueda2 = pagoServicioValorBusqueda2;
    }
}
