package bus.consumoweb.pagoservicios.model;


import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for anonymous complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cuenta" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="dato" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="key" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *                           &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="deuda" type="{http://aqua.bisa.com/servicios/pagoservicios/ws}deuda" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="codCuenta" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *                 &lt;attribute name="descripcion" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *                 &lt;attribute name="deudasConsultadas" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="msg" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="deudasConsultadas" use="required" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "cuenta"
})
@XmlRootElement(name = "consultaCuentasDeServiciosResponse")
public class ConsultaCuentasDeServiciosResponse implements Serializable {

    private static final long serialVersionUID = -7556703084390228942L;
    protected List<Cuenta> cuenta;
    @XmlAttribute(name = "msg")
    protected String msg;
    @XmlAttribute(name = "msgDeuda")
    protected String msgDeuda;
    @XmlAttribute(name = "deudasConsultadas", required = true)
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger deudasConsultadas;
    @XmlAttribute(name = "moneda")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger moneda;

    /**
     * Gets the value of the cuenta property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cuenta property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCuenta().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Cuenta }
     */
    public List<Cuenta> getCuenta() {
        if (cuenta == null) {
            cuenta = new ArrayList<Cuenta>();
        }
        return this.cuenta;
    }

    /**
     * Gets the value of the msg property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMsg() {
        return msg;
    }

    /**
     * Sets the value of the msg property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMsg(String value) {
        this.msg = value;
    }

    /**
     * Gets the value of the deudasConsultadas property.
     *
     * @return possible object is
     * {@link java.math.BigInteger }
     */
    public BigInteger getDeudasConsultadas() {
        return deudasConsultadas;
    }

    /**
     * Sets the value of the deudasConsultadas property.
     *
     * @param value allowed object is
     *              {@link java.math.BigInteger }
     */
    public void setDeudasConsultadas(BigInteger value) {
        this.deudasConsultadas = value;
    }


    @Override
    public String toString() {
        return "Cuentas{" +
                "msg=" + msg +
                ", deudasConsultadas=" + deudasConsultadas +
                '}';
    }

    public String getMsgDeuda() {
        return msgDeuda;
    }

    public void setMsgDeuda(String msgDeuda) {
        this.msgDeuda = msgDeuda;
    }

    public BigInteger getMoneda() {
        return moneda;
    }

    public void setMoneda(BigInteger moneda) {
        this.moneda = moneda;
    }
}

