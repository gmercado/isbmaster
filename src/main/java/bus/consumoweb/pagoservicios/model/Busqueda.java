package bus.consumoweb.pagoservicios.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class Busqueda implements Serializable {

    private static final long serialVersionUID = -4590876731451558070L;

    private String tipo;

    private String valor;

    private String valor2;

    private BigDecimal importe;

    private String razonSocial;

    private String nit;

    private Short moneda;

    private String ci;

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        if (valor != null) {
            valor = valor.toUpperCase();
        }
        this.valor = valor;
    }

    public Short getMoneda() {
        return moneda;
    }

    public void setMoneda(Short moneda) {
        this.moneda = moneda;
    }

    @Override
    public String toString() {
        return "Busqueda{" +
                "tipo=" + tipo +
                ", valor=" + valor +
                ", importe=" + importe +
                ", razonSocial=" + razonSocial +
                ", nit=" + nit +
                ", moneda=" + moneda +
                ", ci=" + ci +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Busqueda)) return false;

        Busqueda busqueda = (Busqueda) o;

        if (tipo != null ? !tipo.equals(busqueda.tipo) : busqueda.tipo != null)
            return false;
        if (importe != null ? !importe.equals(busqueda.importe) : busqueda.importe != null) return false;
        if (nit != null ? !nit.equals(busqueda.nit) : busqueda.nit != null) return false;
        if (razonSocial != null ? !razonSocial.equals(busqueda.razonSocial) : busqueda.razonSocial != null)
            return false;
        if (valor != null ? !valor.equals(busqueda.valor) : busqueda.valor != null)
            return false;
        if (ci != null ? !ci.equals(busqueda.ci) : busqueda.ci != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tipo != null ? tipo.hashCode() : 0;
        result = 31 * result + (valor != null ? valor.hashCode() : 0);
        result = 31 * result + (importe != null ? importe.hashCode() : 0);
        result = 31 * result + (razonSocial != null ? razonSocial.hashCode() : 0);
        result = 31 * result + (nit != null ? nit.hashCode() : 0);
        result = 31 * result + (ci != null ? ci.hashCode() : 0);
        return result;
    }

    public String getValor2() {
        return valor2;
    }

    public void setValor2(String valor2) {
        if (valor2 != null) {
            valor2 = valor2.toUpperCase();
        }
        this.valor2 = valor2;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }
}

