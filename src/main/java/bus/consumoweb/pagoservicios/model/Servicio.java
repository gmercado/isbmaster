package bus.consumoweb.pagoservicios.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for servicio complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType name="servicio">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="busqueda" type="{http://aqua.bisa.com/servicios/pagoservicios/ws}elementoBusqueda" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="tipo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="empresaCodigo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="empresaNombre" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "servicio", propOrder = {
        "busqueda"
})
public class Servicio implements Serializable, Comparable<Servicio> {

    private static final long serialVersionUID = -8999354213099945688L;

    protected List<ElementoBusqueda> busqueda;
    @XmlAttribute(name = "tipo")
    protected String tipo;
    @XmlAttribute(name = "descripcion")
    protected String descripcion;
    @XmlAttribute(name = "codigo")
    protected String codigo;
    @XmlAttribute(name = "empresaCodigo")
    protected String empresaCodigo;
    @XmlAttribute(name = "empresaNombre")
    protected String empresaNombre;

    public Servicio() {
    }

    public Servicio(String codigoServicio, String codigoEmpresa) {
        this.empresaCodigo = codigoEmpresa;
        this.codigo = codigoServicio;
    }

    /**
     * Gets the value of the busqueda property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the busqueda property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBusqueda().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ElementoBusqueda }
     */
    public List<ElementoBusqueda> getBusqueda() {
        if (busqueda == null) {
            busqueda = new ArrayList<ElementoBusqueda>();
        }
        return this.busqueda;
    }

    /**
     * Gets the value of the tipo property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Sets the value of the tipo property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTipo(String value) {
        this.tipo = value;
    }

    /**
     * Gets the value of the descripcion property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Sets the value of the descripcion property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Gets the value of the codigo property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the empresaCodigo property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getEmpresaCodigo() {
        return empresaCodigo;
    }

    /**
     * Sets the value of the empresaCodigo property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setEmpresaCodigo(String value) {
        this.empresaCodigo = value;
    }

    /**
     * Gets the value of the empresaNombre property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getEmpresaNombre() {
        return empresaNombre;
    }

    /**
     * Sets the value of the empresaNombre property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setEmpresaNombre(String value) {
        this.empresaNombre = value;
    }


    @Override
    public String toString() {
        return "Servicio{" +
                "busqueda=" + busqueda +
                ", tipo='" + tipo + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", codigo='" + codigo + '\'' +
                ", empresaCodigo='" + empresaCodigo + '\'' +
                ", empresaNombre='" + empresaNombre + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Servicio)) return false;

        Servicio servicio = (Servicio) o;

        if (codigo != null ? !codigo.equals(servicio.codigo) : servicio.codigo != null) return false;
        if (empresaCodigo != null ? !empresaCodigo.equals(servicio.empresaCodigo) : servicio.empresaCodigo != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codigo != null ? codigo.hashCode() : 0;
        result = 31 * result + (empresaCodigo != null ? empresaCodigo.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Servicio o) {
        final int i1 = this.tipo.compareTo(o.tipo);
        if (i1 != 0) {
            return i1;
        }
        final int i = this.empresaNombre.compareTo(o.empresaNombre);
        if (i != 0) {
            return i;
        }
        return this.descripcion.compareTo(o.descripcion);
    }
}

