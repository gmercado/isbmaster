package bus.consumoweb.pagoservicios.model;

import java.io.Serializable;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class ConsultaCuentasResponse implements Serializable {

    private static final long serialVersionUID = 8548220015391281960L;

    private ConsultaCuentasDeServiciosResponse response;

    private Servicio servicio;

    private ElementoBusqueda elementoBusqueda;

    private Busqueda busqueda;

    private FavoritoPagoServicios fav;

    public ConsultaCuentasResponse() {
    }

    public ConsultaCuentasResponse(ConsultaCuentasDeServiciosResponse response, Servicio servicio,
                                   ElementoBusqueda elementoBusqueda, Busqueda busqueda, FavoritoPagoServicios fav) {
        this.response = response;
        this.servicio = servicio;
        this.elementoBusqueda = elementoBusqueda;
        this.busqueda = busqueda;
        this.fav = fav;
    }

    public ConsultaCuentasDeServiciosResponse getResponse() {
        return response;
    }

    public void setResponse(ConsultaCuentasDeServiciosResponse response) {
        this.response = response;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Busqueda getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(Busqueda busqueda) {
        this.busqueda = busqueda;
    }

    public ElementoBusqueda getElementoBusqueda() {
        return elementoBusqueda;
    }

    public FavoritoPagoServicios getFav() {
        return fav;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(60);
        sb.append("Cuentas{").append(response)
                .append(", servicio=").append(servicio)
                .append(", elementoBusqueda=").append(elementoBusqueda)
                .append(", busqueda=").append(busqueda)
                .append(", fav=").append(fav);

        return sb.toString();
    }
}
