package bus.consumoweb.pagoservicios.model;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for anonymous complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dato" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="key" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *                 &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="deuda" type="{http://aqua.bisa.com/servicios/pagoservicios/ws}deuda" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="codCuenta" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="descripcion" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="deudasConsultadas" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cuenta", propOrder = {
        "dato",
        "deuda"
})
public class Cuenta implements Serializable {

    private static final long serialVersionUID = -7396647893260609762L;
    protected ArrayList<CuentaDato> dato;
    protected Deuda deuda;
    @XmlAttribute(name = "codCuenta")
    @XmlSchemaType(name = "anySimpleType")
    protected String codCuenta;
    @XmlAttribute(name = "descripcion")
    @XmlSchemaType(name = "anySimpleType")
    protected String descripcion;
    @XmlAttribute(name = "deudasConsultadas")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger deudasConsultadas;

    /**
     * Gets the value of the dato property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dato property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDato().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CuentaDato }
     */
    public List<CuentaDato> getDato() {
        if (dato == null) {
            dato = new ArrayList<CuentaDato>();
        }
        return this.dato;
    }

    /**
     * Gets the value of the deuda property.
     *
     * @return possible object is
     */
    public Deuda getDeuda() {
        return deuda;
    }

    /**
     * Sets the value of the deuda property.
     *
     * @param value allowed object is
     */
    public void setDeuda(Deuda value) {
        this.deuda = value;
    }

    /**
     * Gets the value of the codCuenta property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCodCuenta() {
        return codCuenta;
    }

    /**
     * Sets the value of the codCuenta property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCodCuenta(String value) {
        this.codCuenta = value;
    }

    /**
     * Gets the value of the descripcion property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Sets the value of the descripcion property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Gets the value of the deudasConsultadas property.
     *
     * @return possible object is
     * {@link java.math.BigInteger }
     */
    public BigInteger getDeudasConsultadas() {
        return deudasConsultadas;
    }

    /**
     * Sets the value of the deudasConsultadas property.
     *
     * @param value allowed object is
     *              {@link java.math.BigInteger }
     */
    public void setDeudasConsultadas(BigInteger value) {
        this.deudasConsultadas = value;
    }


    @Override
    public String toString() {
        return "Cuenta{" +
                "codCuenta=" + codCuenta +
                ", descripcion=" + descripcion +
                '}';
    }
}

