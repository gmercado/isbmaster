package bus.consumoweb.pagoservicios.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * <p>Java class for elementoBusqueda complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType name="elementoBusqueda">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="tipo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "elementoBusqueda")
public class ElementoBusqueda implements Serializable {

    private static final long serialVersionUID = -72480748477628885L;

    @XmlAttribute(name = "tipo")
    protected String tipo;
    @XmlAttribute(name = "descripcion")
    protected String descripcion;

    /**
     * Gets the value of the tipo property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Sets the value of the tipo property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTipo(String value) {
        this.tipo = value;
    }

    /**
     * Gets the value of the descripcion property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Sets the value of the descripcion property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    @Override
    public String toString() {
        return "ElementoBusqueda{" +
                "tipo=" + tipo +
                ", descripcion=" + descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ElementoBusqueda)) return false;

        ElementoBusqueda that = (ElementoBusqueda) o;

        if (!descripcion.equals(that.descripcion)) return false;
        if (!tipo.equals(that.tipo)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tipo.hashCode();
        result = 31 * result + descripcion.hashCode();
        return result;
    }
}
