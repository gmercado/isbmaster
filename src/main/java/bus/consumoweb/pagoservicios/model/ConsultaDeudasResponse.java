package bus.consumoweb.pagoservicios.model;

import java.io.Serializable;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class ConsultaDeudasResponse implements Serializable {

    private static final long serialVersionUID = 2163942218157465549L;
    private ConsultaDeudasDeServiciosResponse response;
    private ElementoBusqueda elementoBusqueda;
    private Cuenta cuenta;
    private Servicio servicio;
    private Busqueda busqueda;
    private FavoritoPagoServicios fav;

    public ConsultaDeudasResponse() {
    }

    public ConsultaDeudasResponse(ConsultaDeudasDeServiciosResponse response, Cuenta cuenta, Servicio servicio,
                                  ElementoBusqueda elementoBusqueda, Busqueda busqueda, FavoritoPagoServicios fav) {
        this.response = response;
        this.cuenta = cuenta;
        this.servicio = servicio;
        this.elementoBusqueda = elementoBusqueda;
        this.busqueda = busqueda;
        this.fav = fav;
    }

    public ConsultaDeudasDeServiciosResponse getResponse() {
        return response;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public Busqueda getBusqueda() {
        return busqueda;
    }

    public ElementoBusqueda getElementoBusqueda() {
        return elementoBusqueda;
    }

    public FavoritoPagoServicios getFav() {
        return fav;
    }

    public void setFav(FavoritoPagoServicios fav) {
        this.fav = fav;
    }
}
