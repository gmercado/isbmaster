package bus.consumoweb.pagoservicios.model;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for deuda complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType name="deuda">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="factura" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="dato" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="key" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *                           &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="nro" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *                 &lt;attribute name="periodo" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *                 &lt;attribute name="monto" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="msg" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deuda", propOrder = {
        "factura"
})
public class Deuda implements Serializable {

    private static final long serialVersionUID = 199830104481945407L;
    protected List<Factura> factura;
    @XmlAttribute(name = "msg")
    protected String msg;

    /**
     * Gets the value of the factura property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the factura property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFactura().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Deuda.Factura }
     */
    public List<Factura> getFactura() {
        if (factura == null) {
            factura = new ArrayList<Factura>();
        }
        return this.factura;
    }

    /**
     * Gets the value of the msg property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMsg() {
        return msg;
    }

    /**
     * Sets the value of the msg property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMsg(String value) {
        this.msg = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * <p>
     * <p>The following schema fragment specifies the expected content contained within this class.
     * <p>
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="dato" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;attribute name="key" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
     *                 &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="nro" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
     *       &lt;attribute name="periodo" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
     *       &lt;attribute name="monto" type="{http://www.w3.org/2001/XMLSchema}decimal" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "dato"
    })
    public static class Factura implements Serializable {

        private static final long serialVersionUID = 7339212694033121502L;
        protected List<Dato> dato;
        @XmlAttribute(name = "nro")
        @XmlSchemaType(name = "anySimpleType")
        protected String nro;
        @XmlAttribute(name = "periodo")
        @XmlSchemaType(name = "anySimpleType")
        protected String periodo;
        @XmlAttribute(name = "monto")
        protected BigDecimal monto;

        /**
         * Gets the value of the dato property.
         * <p>
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the dato property.
         * <p>
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDato().add(newItem);
         * </pre>
         * <p>
         * <p>
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Deuda.Factura.Dato }
         */
        public List<Dato> getDato() {
            if (dato == null) {
                dato = new ArrayList<Dato>();
            }
            return this.dato;
        }

        /**
         * Gets the value of the nro property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getNro() {
            return nro;
        }

        /**
         * Sets the value of the nro property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setNro(String value) {
            this.nro = value;
        }

        /**
         * Gets the value of the periodo property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getPeriodo() {
            return periodo;
        }

        /**
         * Sets the value of the periodo property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setPeriodo(String value) {
            this.periodo = value;
        }

        /**
         * Gets the value of the monto property.
         *
         * @return possible object is
         * {@link java.math.BigDecimal }
         */
        public BigDecimal getMonto() {
            return monto;
        }

        /**
         * Sets the value of the monto property.
         *
         * @param value allowed object is
         *              {@link java.math.BigDecimal }
         */
        public void setMonto(BigDecimal value) {
            this.monto = value;
        }

        @Override
        public String toString() {
            return "Factura{" +
                    "dato=" + dato +
                    ", nro=" + nro +
                    ", periodo=" + periodo +
                    ", monto=" + monto +
                    '}';
        }

        /**
         * <p>Java class for anonymous complex type.
         * <p>
         * <p>The following schema fragment specifies the expected content contained within this class.
         * <p>
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;attribute name="key" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
         *       &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Dato implements Serializable {

            private static final long serialVersionUID = -5214619465592613628L;
            @XmlAttribute(name = "key")
            @XmlSchemaType(name = "anySimpleType")
            protected String key;
            @XmlAttribute(name = "value")
            @XmlSchemaType(name = "anySimpleType")
            protected String value;

            /**
             * Gets the value of the key property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getKey() {
                return key;
            }

            /**
             * Sets the value of the key property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setKey(String value) {
                this.key = value;
            }

            /**
             * Gets the value of the value property.
             *
             * @return possible object is
             * {@link String }
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             *
             * @param value allowed object is
             *              {@link String }
             */
            public void setValue(String value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return "Dato{" +
                        "key=" + key +
                        ", value=" + value +
                        '}';
            }
        }

    }

    @Override
    public String toString() {
        return "Deuda{  msg=" + msg + '}';
    }
}

