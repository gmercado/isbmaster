package bus.consumoweb.pagoservicios.model;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>Java class for psr9101 complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType name="psr9101">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="WTPODTA" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WNUMCTL" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WNUMUSR" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WNUMSEQ" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WPOST" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WFLGAPR" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WTPOAPR" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WTRNPEN" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WFECPRO" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WFECSIS" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WHORA" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WCODSEG" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WFMT1" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WFMT2" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WFMT3" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WLRG1" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WLRG2" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WLRG3" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WOCU1" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WOCU2" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WOCU3" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WCON1" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WCON2" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WCON3" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WREGFACB" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WREGFACO" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WREGLV" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WREGDEP" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WREGMEM" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WREJ1" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WREJ2" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WREJ3" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WREJ4" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WREJ5" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WFLG1" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WFLG2" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WFLG3" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WFLG4" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WVAR1" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "psr9101")
public class Psr9101 implements Serializable {

    @XmlAttribute(name = "WTPODTA")
    protected BigDecimal wtpodta;
    @XmlAttribute(name = "WNUMCTL")
    protected BigDecimal wnumctl;
    @XmlAttribute(name = "WNUMUSR")
    protected BigDecimal wnumusr;
    @XmlAttribute(name = "WNUMSEQ")
    protected BigDecimal wnumseq;
    @XmlAttribute(name = "WPOST")
    @XmlSchemaType(name = "anySimpleType")
    protected String wpost;
    @XmlAttribute(name = "WFLGAPR")
    @XmlSchemaType(name = "anySimpleType")
    protected String wflgapr;
    @XmlAttribute(name = "WTPOAPR")
    @XmlSchemaType(name = "anySimpleType")
    protected String wtpoapr;
    @XmlAttribute(name = "WTRNPEN")
    @XmlSchemaType(name = "anySimpleType")
    protected String wtrnpen;
    @XmlAttribute(name = "WFECPRO")
    protected BigDecimal wfecpro;
    @XmlAttribute(name = "WFECSIS")
    protected BigDecimal wfecsis;
    @XmlAttribute(name = "WHORA")
    protected BigDecimal whora;
    @XmlAttribute(name = "WCODSEG")
    protected BigDecimal wcodseg;
    @XmlAttribute(name = "WFMT1")
    protected BigDecimal wfmt1;
    @XmlAttribute(name = "WFMT2")
    protected BigDecimal wfmt2;
    @XmlAttribute(name = "WFMT3")
    protected BigDecimal wfmt3;
    @XmlAttribute(name = "WLRG1")
    protected BigDecimal wlrg1;
    @XmlAttribute(name = "WLRG2")
    protected BigDecimal wlrg2;
    @XmlAttribute(name = "WLRG3")
    protected BigDecimal wlrg3;
    @XmlAttribute(name = "WOCU1")
    protected BigDecimal wocu1;
    @XmlAttribute(name = "WOCU2")
    protected BigDecimal wocu2;
    @XmlAttribute(name = "WOCU3")
    protected BigDecimal wocu3;
    @XmlAttribute(name = "WCON1")
    @XmlSchemaType(name = "anySimpleType")
    protected String wcon1;
    @XmlAttribute(name = "WCON2")
    @XmlSchemaType(name = "anySimpleType")
    protected String wcon2;
    @XmlAttribute(name = "WCON3")
    @XmlSchemaType(name = "anySimpleType")
    protected String wcon3;
    @XmlAttribute(name = "WREGFACB")
    protected BigDecimal wregfacb;
    @XmlAttribute(name = "WREGFACO")
    protected BigDecimal wregfaco;
    @XmlAttribute(name = "WREGLV")
    @XmlSchemaType(name = "anySimpleType")
    protected String wreglv;
    @XmlAttribute(name = "WREGDEP")
    @XmlSchemaType(name = "anySimpleType")
    protected String wregdep;
    @XmlAttribute(name = "WREGMEM")
    @XmlSchemaType(name = "anySimpleType")
    protected String wregmem;
    @XmlAttribute(name = "WREJ1")
    @XmlSchemaType(name = "anySimpleType")
    protected String wrej1;
    @XmlAttribute(name = "WREJ2")
    @XmlSchemaType(name = "anySimpleType")
    protected String wrej2;
    @XmlAttribute(name = "WREJ3")
    @XmlSchemaType(name = "anySimpleType")
    protected String wrej3;
    @XmlAttribute(name = "WREJ4")
    @XmlSchemaType(name = "anySimpleType")
    protected String wrej4;
    @XmlAttribute(name = "WREJ5")
    @XmlSchemaType(name = "anySimpleType")
    protected String wrej5;
    @XmlAttribute(name = "WFLG1")
    @XmlSchemaType(name = "anySimpleType")
    protected String wflg1;
    @XmlAttribute(name = "WFLG2")
    @XmlSchemaType(name = "anySimpleType")
    protected String wflg2;
    @XmlAttribute(name = "WFLG3")
    @XmlSchemaType(name = "anySimpleType")
    protected String wflg3;
    @XmlAttribute(name = "WFLG4")
    @XmlSchemaType(name = "anySimpleType")
    protected String wflg4;
    @XmlAttribute(name = "WVAR1")
    @XmlSchemaType(name = "anySimpleType")
    protected String wvar1;

    /**
     * Gets the value of the wtpodta property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWTPODTA() {
        return wtpodta;
    }

    /**
     * Sets the value of the wtpodta property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWTPODTA(BigDecimal value) {
        this.wtpodta = value;
    }

    /**
     * Gets the value of the wnumctl property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWNUMCTL() {
        return wnumctl;
    }

    /**
     * Sets the value of the wnumctl property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWNUMCTL(BigDecimal value) {
        this.wnumctl = value;
    }

    /**
     * Gets the value of the wnumusr property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWNUMUSR() {
        return wnumusr;
    }

    /**
     * Sets the value of the wnumusr property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWNUMUSR(BigDecimal value) {
        this.wnumusr = value;
    }

    /**
     * Gets the value of the wnumseq property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWNUMSEQ() {
        return wnumseq;
    }

    /**
     * Sets the value of the wnumseq property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWNUMSEQ(BigDecimal value) {
        this.wnumseq = value;
    }

    /**
     * Gets the value of the wpost property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWPOST() {
        return wpost;
    }

    /**
     * Sets the value of the wpost property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWPOST(String value) {
        this.wpost = value;
    }

    /**
     * Gets the value of the wflgapr property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWFLGAPR() {
        return wflgapr;
    }

    /**
     * Sets the value of the wflgapr property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWFLGAPR(String value) {
        this.wflgapr = value;
    }

    /**
     * Gets the value of the wtpoapr property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWTPOAPR() {
        return wtpoapr;
    }

    /**
     * Sets the value of the wtpoapr property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWTPOAPR(String value) {
        this.wtpoapr = value;
    }

    /**
     * Gets the value of the wtrnpen property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWTRNPEN() {
        return wtrnpen;
    }

    /**
     * Sets the value of the wtrnpen property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWTRNPEN(String value) {
        this.wtrnpen = value;
    }

    /**
     * Gets the value of the wfecpro property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWFECPRO() {
        return wfecpro;
    }

    /**
     * Sets the value of the wfecpro property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWFECPRO(BigDecimal value) {
        this.wfecpro = value;
    }

    /**
     * Gets the value of the wfecsis property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWFECSIS() {
        return wfecsis;
    }

    /**
     * Sets the value of the wfecsis property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWFECSIS(BigDecimal value) {
        this.wfecsis = value;
    }

    /**
     * Gets the value of the whora property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWHORA() {
        return whora;
    }

    /**
     * Sets the value of the whora property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWHORA(BigDecimal value) {
        this.whora = value;
    }

    /**
     * Gets the value of the wcodseg property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWCODSEG() {
        return wcodseg;
    }

    /**
     * Sets the value of the wcodseg property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWCODSEG(BigDecimal value) {
        this.wcodseg = value;
    }

    /**
     * Gets the value of the wfmt1 property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWFMT1() {
        return wfmt1;
    }

    /**
     * Sets the value of the wfmt1 property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWFMT1(BigDecimal value) {
        this.wfmt1 = value;
    }

    /**
     * Gets the value of the wfmt2 property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWFMT2() {
        return wfmt2;
    }

    /**
     * Sets the value of the wfmt2 property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWFMT2(BigDecimal value) {
        this.wfmt2 = value;
    }

    /**
     * Gets the value of the wfmt3 property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWFMT3() {
        return wfmt3;
    }

    /**
     * Sets the value of the wfmt3 property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWFMT3(BigDecimal value) {
        this.wfmt3 = value;
    }

    /**
     * Gets the value of the wlrg1 property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWLRG1() {
        return wlrg1;
    }

    /**
     * Sets the value of the wlrg1 property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWLRG1(BigDecimal value) {
        this.wlrg1 = value;
    }

    /**
     * Gets the value of the wlrg2 property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWLRG2() {
        return wlrg2;
    }

    /**
     * Sets the value of the wlrg2 property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWLRG2(BigDecimal value) {
        this.wlrg2 = value;
    }

    /**
     * Gets the value of the wlrg3 property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWLRG3() {
        return wlrg3;
    }

    /**
     * Sets the value of the wlrg3 property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWLRG3(BigDecimal value) {
        this.wlrg3 = value;
    }

    /**
     * Gets the value of the wocu1 property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWOCU1() {
        return wocu1;
    }

    /**
     * Sets the value of the wocu1 property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWOCU1(BigDecimal value) {
        this.wocu1 = value;
    }

    /**
     * Gets the value of the wocu2 property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWOCU2() {
        return wocu2;
    }

    /**
     * Sets the value of the wocu2 property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWOCU2(BigDecimal value) {
        this.wocu2 = value;
    }

    /**
     * Gets the value of the wocu3 property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWOCU3() {
        return wocu3;
    }

    /**
     * Sets the value of the wocu3 property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWOCU3(BigDecimal value) {
        this.wocu3 = value;
    }

    /**
     * Gets the value of the wcon1 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWCON1() {
        return wcon1;
    }

    /**
     * Sets the value of the wcon1 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWCON1(String value) {
        this.wcon1 = value;
    }

    /**
     * Gets the value of the wcon2 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWCON2() {
        return wcon2;
    }

    /**
     * Sets the value of the wcon2 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWCON2(String value) {
        this.wcon2 = value;
    }

    /**
     * Gets the value of the wcon3 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWCON3() {
        return wcon3;
    }

    /**
     * Sets the value of the wcon3 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWCON3(String value) {
        this.wcon3 = value;
    }

    /**
     * Gets the value of the wregfacb property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWREGFACB() {
        return wregfacb;
    }

    /**
     * Sets the value of the wregfacb property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWREGFACB(BigDecimal value) {
        this.wregfacb = value;
    }

    /**
     * Gets the value of the wregfaco property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWREGFACO() {
        return wregfaco;
    }

    /**
     * Sets the value of the wregfaco property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWREGFACO(BigDecimal value) {
        this.wregfaco = value;
    }

    /**
     * Gets the value of the wreglv property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWREGLV() {
        return wreglv;
    }

    /**
     * Sets the value of the wreglv property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWREGLV(String value) {
        this.wreglv = value;
    }

    /**
     * Gets the value of the wregdep property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWREGDEP() {
        return wregdep;
    }

    /**
     * Sets the value of the wregdep property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWREGDEP(String value) {
        this.wregdep = value;
    }

    /**
     * Gets the value of the wregmem property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWREGMEM() {
        return wregmem;
    }

    /**
     * Sets the value of the wregmem property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWREGMEM(String value) {
        this.wregmem = value;
    }

    /**
     * Gets the value of the wrej1 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWREJ1() {
        return wrej1;
    }

    /**
     * Sets the value of the wrej1 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWREJ1(String value) {
        this.wrej1 = value;
    }

    /**
     * Gets the value of the wrej2 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWREJ2() {
        return wrej2;
    }

    /**
     * Sets the value of the wrej2 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWREJ2(String value) {
        this.wrej2 = value;
    }

    /**
     * Gets the value of the wrej3 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWREJ3() {
        return wrej3;
    }

    /**
     * Sets the value of the wrej3 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWREJ3(String value) {
        this.wrej3 = value;
    }

    /**
     * Gets the value of the wrej4 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWREJ4() {
        return wrej4;
    }

    /**
     * Sets the value of the wrej4 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWREJ4(String value) {
        this.wrej4 = value;
    }

    /**
     * Gets the value of the wrej5 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWREJ5() {
        return wrej5;
    }

    /**
     * Sets the value of the wrej5 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWREJ5(String value) {
        this.wrej5 = value;
    }

    /**
     * Gets the value of the wflg1 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWFLG1() {
        return wflg1;
    }

    /**
     * Sets the value of the wflg1 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWFLG1(String value) {
        this.wflg1 = value;
    }

    /**
     * Gets the value of the wflg2 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWFLG2() {
        return wflg2;
    }

    /**
     * Sets the value of the wflg2 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWFLG2(String value) {
        this.wflg2 = value;
    }

    /**
     * Gets the value of the wflg3 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWFLG3() {
        return wflg3;
    }

    /**
     * Sets the value of the wflg3 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWFLG3(String value) {
        this.wflg3 = value;
    }

    /**
     * Gets the value of the wflg4 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWFLG4() {
        return wflg4;
    }

    /**
     * Sets the value of the wflg4 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWFLG4(String value) {
        this.wflg4 = value;
    }

    /**
     * Gets the value of the wvar1 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWVAR1() {
        return wvar1;
    }

    /**
     * Sets the value of the wvar1 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWVAR1(String value) {
        this.wvar1 = value;
    }

}
