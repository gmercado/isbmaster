package bus.consumoweb.pagoservicios.model;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * <p>Java class for anonymous complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="psr9101" type="{http://aqua.bisa.com/servicios/pagoservicios/ws}psr9101" minOccurs="0"/>
 *         &lt;element name="psr9120" type="{http://aqua.bisa.com/servicios/pagoservicios/ws}psr9120" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="msg" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "psr9101",
        "psr9120"
})
@XmlRootElement(name = "pagoDeudaDeServiciosResponse")
public class PagoDeudaDeServiciosResponse implements Serializable {

    private static final long serialVersionUID = 2186156137327291615L;

    protected Psr9101 psr9101;
    protected Psr9120 psr9120;
    @XmlAttribute(name = "msg")
    protected String msg;
    @XmlAttribute(name = "ticket")
    protected String ticket;
    @XmlAttribute(name = "estado")
    protected String estado;

    /**
     * Gets the value of the psr9101 property.
     *
     * @return possible object is
     * {@link Psr9101 }
     */
    public Psr9101 getPsr9101() {
        return psr9101;
    }

    /**
     * Sets the value of the psr9101 property.
     *
     * @param value allowed object is
     *              {@link Psr9101 }
     */
    public void setPsr9101(Psr9101 value) {
        this.psr9101 = value;
    }

    /**
     * Gets the value of the psr9120 property.
     *
     * @return possible object is
     * {@link Psr9120 }
     */
    public Psr9120 getPsr9120() {
        return psr9120;
    }

    /**
     * Sets the value of the psr9120 property.
     *
     * @param value allowed object is
     *              {@link Psr9120 }
     */
    public void setPsr9120(Psr9120 value) {
        this.psr9120 = value;
    }

    /**
     * Gets the value of the msg property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMsg() {
        return msg;
    }

    /**
     * Sets the value of the msg property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMsg(String value) {
        this.msg = value;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "PagoDeudaDeServiciosResponse{" +
                "msg='" + msg + '\'' +
                ", ticket='" + ticket + '\'' +
                ", estado='" + estado + '\'' +
                '}';
    }
}

