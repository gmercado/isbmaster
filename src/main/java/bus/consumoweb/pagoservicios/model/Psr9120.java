package bus.consumoweb.pagoservicios.model;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>Java class for psr9120 complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType name="psr9120">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="WIMPTOT" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WIMPORTE" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WIMPORTE2" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WCARGO" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WCLSCRG" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WCOBCRG" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WCOTACT" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WCOMVEN" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WORDCTA" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WCODMON" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WCODMON2" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WIMPTE1" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WIMPTE2" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WTPOTE1" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WTPOTE2" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WNOMBRE" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="WSALACT" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WNEGACT" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WSALDIS" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WNEGDIS" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WNUMADI" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WFECHA" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="WNOMBREA" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "psr9120")
public class Psr9120 implements Serializable {

    @XmlAttribute(name = "WIMPTOT")
    protected BigDecimal wimptot;
    @XmlAttribute(name = "WIMPORTE")
    protected BigDecimal wimporte;
    @XmlAttribute(name = "WIMPORTE2")
    protected BigDecimal wimporte2;
    @XmlAttribute(name = "WCARGO")
    protected BigDecimal wcargo;
    @XmlAttribute(name = "WCLSCRG")
    @XmlSchemaType(name = "anySimpleType")
    protected String wclscrg;
    @XmlAttribute(name = "WCOBCRG")
    @XmlSchemaType(name = "anySimpleType")
    protected String wcobcrg;
    @XmlAttribute(name = "WCOTACT")
    protected BigDecimal wcotact;
    @XmlAttribute(name = "WCOMVEN")
    @XmlSchemaType(name = "anySimpleType")
    protected String wcomven;
    @XmlAttribute(name = "WORDCTA")
    protected BigDecimal wordcta;
    @XmlAttribute(name = "WCODMON")
    protected BigDecimal wcodmon;
    @XmlAttribute(name = "WCODMON2")
    protected BigDecimal wcodmon2;
    @XmlAttribute(name = "WIMPTE1")
    protected BigDecimal wimpte1;
    @XmlAttribute(name = "WIMPTE2")
    protected BigDecimal wimpte2;
    @XmlAttribute(name = "WTPOTE1")
    @XmlSchemaType(name = "anySimpleType")
    protected String wtpote1;
    @XmlAttribute(name = "WTPOTE2")
    @XmlSchemaType(name = "anySimpleType")
    protected String wtpote2;
    @XmlAttribute(name = "WNOMBRE")
    @XmlSchemaType(name = "anySimpleType")
    protected String wnombre;
    @XmlAttribute(name = "WSALACT")
    protected BigDecimal wsalact;
    @XmlAttribute(name = "WNEGACT")
    protected BigDecimal wnegact;
    @XmlAttribute(name = "WSALDIS")
    protected BigDecimal wsaldis;
    @XmlAttribute(name = "WNEGDIS")
    protected BigDecimal wnegdis;
    @XmlAttribute(name = "WNUMADI")
    protected BigDecimal wnumadi;
    @XmlAttribute(name = "WFECHA")
    protected BigDecimal wfecha;
    @XmlAttribute(name = "WNOMBREA")
    @XmlSchemaType(name = "anySimpleType")
    protected String wnombrea;

    /**
     * Gets the value of the wimptot property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWIMPTOT() {
        return wimptot;
    }

    /**
     * Sets the value of the wimptot property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWIMPTOT(BigDecimal value) {
        this.wimptot = value;
    }

    /**
     * Gets the value of the wimporte property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWIMPORTE() {
        return wimporte;
    }

    /**
     * Sets the value of the wimporte property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWIMPORTE(BigDecimal value) {
        this.wimporte = value;
    }

    /**
     * Gets the value of the wimporte2 property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWIMPORTE2() {
        return wimporte2;
    }

    /**
     * Sets the value of the wimporte2 property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWIMPORTE2(BigDecimal value) {
        this.wimporte2 = value;
    }

    /**
     * Gets the value of the wcargo property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWCARGO() {
        return wcargo;
    }

    /**
     * Sets the value of the wcargo property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWCARGO(BigDecimal value) {
        this.wcargo = value;
    }

    /**
     * Gets the value of the wclscrg property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWCLSCRG() {
        return wclscrg;
    }

    /**
     * Sets the value of the wclscrg property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWCLSCRG(String value) {
        this.wclscrg = value;
    }

    /**
     * Gets the value of the wcobcrg property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWCOBCRG() {
        return wcobcrg;
    }

    /**
     * Sets the value of the wcobcrg property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWCOBCRG(String value) {
        this.wcobcrg = value;
    }

    /**
     * Gets the value of the wcotact property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWCOTACT() {
        return wcotact;
    }

    /**
     * Sets the value of the wcotact property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWCOTACT(BigDecimal value) {
        this.wcotact = value;
    }

    /**
     * Gets the value of the wcomven property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWCOMVEN() {
        return wcomven;
    }

    /**
     * Sets the value of the wcomven property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWCOMVEN(String value) {
        this.wcomven = value;
    }

    /**
     * Gets the value of the wordcta property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWORDCTA() {
        return wordcta;
    }

    /**
     * Sets the value of the wordcta property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWORDCTA(BigDecimal value) {
        this.wordcta = value;
    }

    /**
     * Gets the value of the wcodmon property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWCODMON() {
        return wcodmon;
    }

    /**
     * Sets the value of the wcodmon property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWCODMON(BigDecimal value) {
        this.wcodmon = value;
    }

    /**
     * Gets the value of the wcodmon2 property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWCODMON2() {
        return wcodmon2;
    }

    /**
     * Sets the value of the wcodmon2 property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWCODMON2(BigDecimal value) {
        this.wcodmon2 = value;
    }

    /**
     * Gets the value of the wimpte1 property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWIMPTE1() {
        return wimpte1;
    }

    /**
     * Sets the value of the wimpte1 property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWIMPTE1(BigDecimal value) {
        this.wimpte1 = value;
    }

    /**
     * Gets the value of the wimpte2 property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWIMPTE2() {
        return wimpte2;
    }

    /**
     * Sets the value of the wimpte2 property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWIMPTE2(BigDecimal value) {
        this.wimpte2 = value;
    }

    /**
     * Gets the value of the wtpote1 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWTPOTE1() {
        return wtpote1;
    }

    /**
     * Sets the value of the wtpote1 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWTPOTE1(String value) {
        this.wtpote1 = value;
    }

    /**
     * Gets the value of the wtpote2 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWTPOTE2() {
        return wtpote2;
    }

    /**
     * Sets the value of the wtpote2 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWTPOTE2(String value) {
        this.wtpote2 = value;
    }

    /**
     * Gets the value of the wnombre property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWNOMBRE() {
        return wnombre;
    }

    /**
     * Sets the value of the wnombre property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWNOMBRE(String value) {
        this.wnombre = value;
    }

    /**
     * Gets the value of the wsalact property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWSALACT() {
        return wsalact;
    }

    /**
     * Sets the value of the wsalact property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWSALACT(BigDecimal value) {
        this.wsalact = value;
    }

    /**
     * Gets the value of the wnegact property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWNEGACT() {
        return wnegact;
    }

    /**
     * Sets the value of the wnegact property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWNEGACT(BigDecimal value) {
        this.wnegact = value;
    }

    /**
     * Gets the value of the wsaldis property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWSALDIS() {
        return wsaldis;
    }

    /**
     * Sets the value of the wsaldis property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWSALDIS(BigDecimal value) {
        this.wsaldis = value;
    }

    /**
     * Gets the value of the wnegdis property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWNEGDIS() {
        return wnegdis;
    }

    /**
     * Sets the value of the wnegdis property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWNEGDIS(BigDecimal value) {
        this.wnegdis = value;
    }

    /**
     * Gets the value of the wnumadi property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWNUMADI() {
        return wnumadi;
    }

    /**
     * Sets the value of the wnumadi property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWNUMADI(BigDecimal value) {
        this.wnumadi = value;
    }

    /**
     * Gets the value of the wfecha property.
     *
     * @return possible object is
     * {@link java.math.BigDecimal }
     */
    public BigDecimal getWFECHA() {
        return wfecha;
    }

    /**
     * Sets the value of the wfecha property.
     *
     * @param value allowed object is
     *              {@link java.math.BigDecimal }
     */
    public void setWFECHA(BigDecimal value) {
        this.wfecha = value;
    }

    /**
     * Gets the value of the wnombrea property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getWNOMBREA() {
        return wnombrea;
    }

    /**
     * Sets the value of the wnombrea property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setWNOMBREA(String value) {
        this.wnombrea = value;
    }

}

