/*
 * Copyright 2013 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.consumoweb.yellowpepper.dao;

import bus.consumoweb.consumer.ConsumidorServiciosWeb;
import bus.consumoweb.consumer.DefaultHandlerRespuesta;
import bus.consumoweb.model.ConsumidorYellowPepper;
import bus.consumoweb.yellowpepper.ConsumidorServiciosWebYellowPepper;
import bus.consumoweb.yellowpepper.RespuestaYellowPepper;
import bus.plumbing.components.TelefonoCelularValidator;
import bus.sms.dao.DetalleMensajeDao;
import bus.sms.dao.EncabezadoMensajeDao;
import bus.sms.entities.EncabezadoMensaje;
import bus.sms.tipos.EstadoDetalleMensaje;
import bus.sms.tipos.EstadoEncabezadoMensaje;
import bus.sms.tipos.TipoMensaje;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Resources;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static com.google.common.base.Charsets.US_ASCII;

/**
 * @author Roger Chura
 * @since 1.0
 */
public class YellowPepperServicioDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(YellowPepperServicioDao.class);
    private static final String PREFIJO_BOLIVIA = "591";

    private final ConsumidorServiciosWeb consumidorServiciosWeb;
    private final ConsumidorServiciosWebYellowPepper consumidorSWYellowPepper;
    private final EncabezadoMensajeDao encabezadoMensajeDao;
    private final DetalleMensajeDao detalleMensajeDao;

    @Inject
    public YellowPepperServicioDao(@ConsumidorYellowPepper ConsumidorServiciosWeb consumidorServiciosWeb,
                                   ConsumidorServiciosWebYellowPepper consumidorSWYellowPepper,
                                   EncabezadoMensajeDao encabezadoMensajeDao, DetalleMensajeDao detalleMensajeDao) {
        this.consumidorServiciosWeb = consumidorServiciosWeb;
        this.consumidorSWYellowPepper = consumidorSWYellowPepper;
        this.encabezadoMensajeDao = encabezadoMensajeDao;
        this.detalleMensajeDao = detalleMensajeDao;
    }

    private RespuestaYellowPepper enviarMensaje(String numeroTelefonoCelular, String mensaje, boolean prioridad)
            throws IOException, SAXException, TimeoutException, ExecutionException {

        if (StringUtils.trimToNull(numeroTelefonoCelular) == null) {
            LOGGER.error("No se tiene el numero de celular para enviar el mensaje.");
            return null;
        }
        if (!TelefonoCelularValidator.isValid(numeroTelefonoCelular)) {
            LOGGER.error("El numero de celular {} no es valido de acuerdo al patron de validacion.", numeroTelefonoCelular);
            return null;
        }
        if (StringUtils.length(StringUtils.trimToEmpty(numeroTelefonoCelular)) == 8) {
            numeroTelefonoCelular = PREFIJO_BOLIVIA + StringUtils.trimToEmpty(numeroTelefonoCelular);
        }
        if (StringUtils.trimToNull(mensaje) == null) {
            LOGGER.error("No se tiene ningun mensaje para enviar al numero de celular {}.", numeroTelefonoCelular);
            return null;
        }

        String fecha = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date());
        String s;
        Map<String, String> interpolar;
        if (prioridad) {
            interpolar = ImmutableMap.<String, String>builder().
                    put("path", "metroline.asmx").
                    put("content-type", "text/xml").
                    put("fono", numeroTelefonoCelular).
                    put("mensaje", mensaje).
                    put("pin", consumidorSWYellowPepper.getPin()).
                    put("sender", consumidorSWYellowPepper.getSender()).
                    put("serial", consumidorSWYellowPepper.getSerial()).
                    put("fecha", fecha).
                    put("prioridad", consumidorSWYellowPepper.getPrioridad()).
                    build();
            s = Resources.toString(Resources.getResource(YellowPepperServicioDao.class, "SoapParaYP-p.xml"), US_ASCII);
        } else {
            interpolar = ImmutableMap.<String, String>builder().
                    put("path", "metroline.asmx").
                    put("content-type", "text/xml").
                    put("fono", numeroTelefonoCelular).
                    put("mensaje", mensaje).
                    put("pin", consumidorSWYellowPepper.getPin()).
                    put("sender", consumidorSWYellowPepper.getSender()).
                    put("serial", consumidorSWYellowPepper.getSerial()).
                    put("fecha", fecha).
                    build();
            s = Resources.toString(Resources.getResource(YellowPepperServicioDao.class, "SoapParaYP.xml"), US_ASCII);
        }
        return (RespuestaYellowPepper) consumidorServiciosWeb.consumir(s, interpolar, new DefaultHandlerRespuesta() {
            @Override
            public String getRespuesta(Object o) {
                return null;
            }
        });
    }


    private RespuestaYellowPepper enviarMensaje(String numeroTelefonoCelular, String mensaje, int prioridad)
            throws IOException, SAXException, TimeoutException, ExecutionException {

        if (StringUtils.trimToNull(numeroTelefonoCelular) == null) {
            LOGGER.error("No se tiene el numero de celular para enviar el mensaje.");
            return null;
        }
        if (!TelefonoCelularValidator.isValid(numeroTelefonoCelular)) {
            LOGGER.error("El numero de celular {} no es valido de acuerdo al patron de validacion.", numeroTelefonoCelular);
            return null;
        }
        if (StringUtils.length(StringUtils.trimToEmpty(numeroTelefonoCelular)) == 8) {
            numeroTelefonoCelular = PREFIJO_BOLIVIA + StringUtils.trimToEmpty(numeroTelefonoCelular);
        }
        if (StringUtils.trimToNull(mensaje) == null) {
            LOGGER.error("No se tiene ningun mensaje para enviar al numero de celular {}.", numeroTelefonoCelular);
            return null;
        }

        String fecha = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date());
        String s;
        Map<String, String> interpolar;
        if (consumidorSWYellowPepper.isPrioridad(prioridad)) {
            interpolar = ImmutableMap.<String, String>builder().
                    put("path", "metroline.asmx").
                    put("content-type", "text/xml").
                    put("fono", numeroTelefonoCelular).
                    put("mensaje", mensaje).
                    put("pin", consumidorSWYellowPepper.getPin()).
                    put("sender", consumidorSWYellowPepper.getSender()).
                    put("serial", consumidorSWYellowPepper.getSerial()).
                    put("fecha", fecha).
                    put("prioridad", prioridad + "").
                    build();
            s = Resources.toString(Resources.getResource(YellowPepperServicioDao.class, "SoapParaYP-p.xml"), US_ASCII);
        } else {
            interpolar = ImmutableMap.<String, String>builder().
                    put("path", "metroline.asmx").
                    put("content-type", "text/xml").
                    put("fono", numeroTelefonoCelular).
                    put("mensaje", mensaje).
                    put("pin", consumidorSWYellowPepper.getPin()).
                    put("sender", consumidorSWYellowPepper.getSender()).
                    put("serial", consumidorSWYellowPepper.getSerial()).
                    put("fecha", fecha).
                    build();
            s = Resources.toString(Resources.getResource(YellowPepperServicioDao.class, "SoapParaYP.xml"), US_ASCII);
        }
        return (RespuestaYellowPepper) consumidorServiciosWeb.consumir(s, interpolar, new DefaultHandlerRespuesta() {
            @Override
            public String getRespuesta(Object o) {
                return null;
            }
        });
    }

    public void sendNotificacionYellowPepperBoA(String celular, String titulo, String mensaje, String cliente, String usuario, String aplicacion) {

        if (StringUtils.trimToNull(celular) == null) {
            LOGGER.error("No se tiene el numero de celular para enviar el mensaje.");
            return;
        }
        if (StringUtils.trimToNull(titulo) == null) {
            LOGGER.error("No se tiene ningun titulo para obtener el encabezado del mensaje a ser enviado.");
            return;
        }
        if (StringUtils.trimToNull(mensaje) == null) {
            LOGGER.error("No se tiene ningun mensaje para enviar al numero de celular {}.", celular);
            return;
        }

        usuario = usuario == null ? "[ISB]" : usuario;

        LOGGER.info("Enviando notifiacion de emision de boleto(s) al celular [{}].", celular);

        //Obtencion del encabezado de mensaje
        EncabezadoMensaje encabezado = encabezadoMensajeDao.getByTituloEstado(titulo, EstadoEncabezadoMensaje.PER);
        if (encabezado != null) {
            if (!encabezado.getMensaje().equalsIgnoreCase(mensaje)) {
                encabezado = null;
            }
        }

        if (encabezado == null) {
            //Registrar nuevo encabezado
            encabezado = new EncabezadoMensaje(titulo, mensaje, EstadoEncabezadoMensaje.PER, null);
            encabezado = encabezadoMensajeDao.registrar(encabezado, usuario);
        }
        RespuestaYellowPepper respuestaYP;
        try {
            respuestaYP = enviarMensaje(celular, mensaje, true);
        } catch (IOException e) {
            respuestaYP = null;
            LOGGER.error("Error HTTP al conectarse a YellowPepper.", e);
        } catch (SAXException e) {
            respuestaYP = null;
            LOGGER.error("Error de programacion al utilizar XML con el servicio web.", e);
        } catch (Exception e) {
            respuestaYP = null;
            LOGGER.error("Error al consumir el servicio a YellowPepper.", e);
        }

        if (respuestaYP != null) {
            LOGGER.info("Respuesta YellowPepper al consumo del servicio web [{}].", respuestaYP.getSendShortMessageResult());
            detalleMensajeDao.registrarNotificacion(encabezado, cliente, celular, usuario, TipoMensaje.NOT, respuestaYP, new Date(), EstadoDetalleMensaje.PRC, aplicacion, "BOA");
        } else {
            detalleMensajeDao.registrarNotificacion(encabezado, cliente, celular, usuario,
                    TipoMensaje.NOT, null, new Date(), EstadoDetalleMensaje.SRE, aplicacion, "BOA");
        }
    }

    public void sendNotificacionYellowPepper(String celular, String titulo, String mensaje, String cliente,
                                             String usuario, String aplicacion, String servicio, int prioridad) {

        if (StringUtils.trimToNull(celular) == null) {
            LOGGER.error("No se tiene el numero de celular para enviar el mensaje.");
            return;
        }
        if (StringUtils.trimToNull(titulo) == null) {
            LOGGER.error("No se tiene ningun titulo para obtener el encabezado del mensaje a ser enviado.");
            return;
        }
        if (StringUtils.trimToNull(mensaje) == null) {
            LOGGER.error("No se tiene ningun mensaje para enviar al numero de celular {}.", celular);
            return;
        }
        aplicacion = StringUtils.trimToEmpty(aplicacion);
        if (!StringUtils.contains(consumidorSWYellowPepper.getSistemasPermitidos(), aplicacion)) {
            LOGGER.warn("El sistema/aplicacion {} no esta contenido en variable de medio ambiente de SMS YP.", aplicacion);
            return;
        }
        usuario = usuario == null ? "[ISB]" : usuario;
        LOGGER.info("Enviando notificacion al celular [{}].", celular);
        //Obtencion del encabezado de mensaje
        EncabezadoMensaje encabezado = new EncabezadoMensaje(titulo, mensaje, EstadoEncabezadoMensaje.PER, null);
        encabezado = encabezadoMensajeDao.registrar(encabezado, usuario);
        RespuestaYellowPepper respuestaYP;
        try {
            respuestaYP = enviarMensaje(celular, mensaje, prioridad);
        } catch (IOException e) {
            respuestaYP = null;
            LOGGER.error("Error HTTP al conectarse a YellowPepper.", e);
        } catch (SAXException e) {
            respuestaYP = null;
            LOGGER.error("Error de programacion al utilizar XML con el servicio web.", e);
        } catch (Exception e) {
            respuestaYP = null;
            LOGGER.error("Error al consumir el servicio a YellowPepper.", e);
        }

        if (respuestaYP != null) {
            LOGGER.info("Respuesta YellowPepper al consumo del servicio web [{}].", respuestaYP.getSendShortMessageResult());
            detalleMensajeDao.registrarNotificacion(encabezado, cliente, celular, usuario, TipoMensaje.NOT, respuestaYP, new Date(), EstadoDetalleMensaje.PRC, aplicacion, servicio);
        } else {
            detalleMensajeDao.registrarNotificacion(encabezado, cliente, celular, usuario,
                    TipoMensaje.NOT, null, new Date(), EstadoDetalleMensaje.SRE, aplicacion, servicio);
        }
    }
}
