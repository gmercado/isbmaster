/*
 * Copyright 2013 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.consumoweb.yellowpepper;

import bus.consumoweb.consumer.ConsumidorServiciosWeb;
import bus.consumoweb.consumer.DefaultHandlerRespuesta;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.StatusLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.*;

/**
 * Consumidor de Solicitud Web para Yellow Pepper
 *
 * @author Roger Chura
 * @since 1.0
 */
public class ConsumidorServiciosWebYellowPepper extends ConsumidorServiciosWeb {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumidorServiciosWebYellowPepper.class);
    private static final String CONTENT_TYPE = "content-type";
    private static final String TEXT_XML = "text/xml";
    private static final String UTF_8 = "UTF-8";

    @Inject
    public ConsumidorServiciosWebYellowPepper(@SQLFueraDeLinea ExecutorService executor,
                                              Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente) {
        super(executor, consumoWebDao, medioAmbiente);
    }

    @Override
    protected String getConsumidorNombre() {
        return "yellowpep";
    }

    @Override
    protected String getHeaderWithIdName() {
        return "X-yellowpep-Id";
    }

    @Override
    protected String newBasicAuthentication() {
        return ""; //NO USA AUTENTICACION
    }

    @Override
    protected String newContentType(Map<String, String> m) {
        if (m.containsKey(CONTENT_TYPE)) {
            return m.get(CONTENT_TYPE);
        }
        return TEXT_XML;
    }

    @Override
    protected String newContentType() {
        return TEXT_XML;
    }

    @Override
    protected HttpHost newHost() {
        final String host = medioAmbiente.getValorDe(YELLOWPEPPER_HOST, YELLOWPEPPER_HOST_DEFAULT);
        final int port = medioAmbiente.getValorIntDe(YELLOWPEPPER_PORT, YELLOWPEPPER_PORT_DEFAULT);
        return new HttpHost(host, port);
    }

    @Override
    protected String newPath(String template, Map<String, String> m) {
        String path = medioAmbiente.getValorDe(YELLOWPEPPER_PATH, YELLOWPEPPER_PATH_DEFAULT);
        return path + m.get("path");
    }

    @Override
    protected HttpHost newProxyHost() {
        return null;
    }

    @Override
    protected Object parseResult(Header[] headers, StatusLine statusLine, DefaultHandlerRespuesta handler, InputStream stream) throws SAXException, IOException {

        byte[] bytes = ByteStreams.toByteArray(stream);
        RespuestaYellowPepper respuesta = new RespuestaYellowPepper();

        if (statusLine.getStatusCode() != 200) {
            LOGGER.info("Error en la respuesta del Web Service. Asumimos que el contenido contiene el problema.");
            respuesta.setSendShortMessageResult(CharStreams.toString(new StringReader(new String(bytes, UTF_8))));
            return respuesta;
        }

        LOGGER.debug("Asumiendo java serializado");
        String xml = CharStreams.toString(new StringReader(new String(bytes, UTF_8)));
        LOGGER.debug("Respuesta Consumo SOAP: " + xml);
        String result;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();
            result = doc.getElementsByTagName("sendShortMessageResult").item(0).getChildNodes().item(0).getNodeValue();
            respuesta.setSendShortMessageResult(result);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            LOGGER.error("Error al parsear el documento XML.", e);
        }
        return respuesta;
    }

    public String getSender() {
        return medioAmbiente.getValorDe(YELLOWPEPPER_SENDER, YELLOWPEPPER_SENDER_DEFAULT);
    }

    public String getPin() {
        return medioAmbiente.getValorDe(YELLOWPEPPER_PIN, YELLOWPEPPER_PIN_DEFAULT);
    }

    public String getSerial() {
        return medioAmbiente.getValorDe(YELLOWPEPPER_SERIAL, YELLOWPEPPER_SERIAL_DEFAULT);
    }

    public String getPrioridad() {
        return medioAmbiente.getValorDe(YELLOWPEPPER_PRIORIDAD, YELLOWPEPPER_PRIORIDAD_DEFAULT);
    }

    @Override
    protected String getSoapAction() {
        return "http://yellowpepper.com/webservices/sendShortMessage";
    }

    @Override
    protected Socket newSocket(SocketAddress socketAddress) throws IOException {
        return new Socket();
    }

    public boolean isPrioridad(int prioridad) {
        String dominio = medioAmbiente.getValorDe(YELLOWPEPPER_PRIORIDAD_DOMINIO, YELLOWPEPPER_PRIORIDAD_DOMINIO_DEFAULT);
        if (StringUtils.contains(dominio, new Integer(prioridad).toString())) {
            return true;
        }
        return false;
    }

    public String getSistemasPermitidos() {
        return medioAmbiente.getValorDe(YELLOWPEPPER_SISTEMAS_PERMITIDOS, YELLOWPEPPER_SISTEMAS_PERMITIDOS_DEFAULT);
    }

}
