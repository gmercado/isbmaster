/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.consumoweb.entities;

import bus.plumbing.utils.FormatosUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Marcelo Morales
 *         Created: 11/24/11 3:52 PM
 */
public class ConsumoWeb implements Serializable {

    private static final long serialVersionUID = -5804256227043437828L;

    private Long id;

    private String consumidor;

    private Date fecha;

    private Integer milis;

    private Integer status;

    private String pregunta;

    private String respuesta;

    private String idResp;

    private Long idRelacionado;

    private long version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConsumidor() {
        return consumidor;
    }

    public void setConsumidor(String consumidor) {
        this.consumidor = consumidor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getMilis() {
        return milis;
    }

    public void setMilis(Integer milis) {
        this.milis = milis;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getIdResp() {
        return idResp;
    }

    public void setIdResp(String idResp) {
        this.idResp = idResp;
    }

    public Long getIdRelacionado() {
        return idRelacionado;
    }

    public void setIdRelacionado(Long idRelacionado) {
        this.idRelacionado = idRelacionado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof ConsumoWeb))
            return false;

        ConsumoWeb that = (ConsumoWeb) o;

        if (id != null ? !id.equals(that.id) : that.id != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getFechaFormato() {
        return FormatosUtils.formatoFechaHora(fecha);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ConsumoWeb");
        sb.append("{id=").append(id);
        sb.append(", consumidor='").append(consumidor).append('\'');
        sb.append(", fecha=").append(fecha);
        sb.append(", milis=").append(milis);
        sb.append(", status=").append(status);
        sb.append(", pregunta='").append(pregunta).append('\'');
        sb.append(", respuesta='").append(respuesta).append('\'');
        sb.append(", idResp=").append(idResp);
        sb.append(", version=").append(version);
        sb.append('}');
        return sb.toString();
    }
}
