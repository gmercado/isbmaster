/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.consumoweb.aqua;

import bus.consumoweb.consumer.ConsumidorServiciosWeb;
import bus.consumoweb.consumer.DefaultHandlerRespuesta;
import bus.consumoweb.dao.ConsumoWebDao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import com.google.inject.Inject;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.StatusLine;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.*;

/**
 * @author Marcelo Morales
 *         Created: 11/24/11 4:46 PM
 */
public class ConsumidorServiciosWebAqua extends ConsumidorServiciosWeb {

    private static final String UTF_8 = "UTF-8";

    @Inject
    public ConsumidorServiciosWebAqua(MedioAmbiente medioAmbiente,
                                      @SQLFueraDeLinea ExecutorService executor,
                                      ConsumoWebDao consumoWebDao) {
        super(executor, consumoWebDao, medioAmbiente);
    }

    @Override
    protected HttpHost newHost() {
        final String host = medioAmbiente.getValorDe(AQUA_HOST, AQUA_HOST_DEFAULT);
        final int port = medioAmbiente.getValorIntDe(AQUA_PORT, AQUA_PORT_DEFAULT);
        return new HttpHost(host, port);
    }

    @Override
    protected String newBasicAuthentication() {
        try {
            final String username = medioAmbiente.getValorDe(AQUA_USERNAME, AQUA_USERNAME_DEFAULT);
            final String password = medioAmbiente.getValorDe(AQUA_PASSWORD, AQUA_PASSWORD_DEFAULT);

            String basic = null;
            if (username != null) {
                basic = username + ":" + password;
                basic = new String(Base64.encodeBase64(basic.getBytes(UTF_8)), UTF_8);
            }
            return basic;
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    protected String newPath(String template, Map<String, String> m) {
        return medioAmbiente.getValorDe(AQUA_PATH, AQUA_PATH_DEFAULT);
    }

    @Override
    protected HttpHost newProxyHost() {
        return null;
    }

    @Override
    protected String getConsumidorNombre() {
        return "aqua";
    }

    @Override
    protected String getHeaderWithIdName() {
        return "X-Aqua-Id";
    }

    protected String newContentType(Map<String, String> m) {
        return "text/xml";
    }

    @Override
    protected String newContentType() {
        return "text/xml";
    }

    @Override
    protected String getSoapAction() {
        return null;
    }

    @Override
    protected Object parseResult(Header[] headers, StatusLine statusLine, DefaultHandlerRespuesta handler, InputStream stream) throws SAXException, IOException {
        final DefaultHandlerRespuesta.ConsumoAProcesar input = new DefaultHandlerRespuesta.ConsumoAProcesar();
        input.headers = headers;
        input.statusLine = statusLine;
        input.is = stream;
        return handler.apply(input);
    }

}
