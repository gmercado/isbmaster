/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.consumoweb.dao;

import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.Date;

/**
 * @author Marcelo Morales
 *         Created: 11/24/11 3:59 PM
 */
public class ConsumoWebDao extends DaoImpl<ConsumoWeb, Long> {

    public static final String TRANSPORTED_PAYLOAD = "cxf.intercepted.payload.fx";
    public static final String TRANSPORTED_PAYLOAD_ID = "cxf.intercepted.payload.fx.PK";

    @Inject
    public ConsumoWebDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Long> countPath(Root<ConsumoWeb> from) {
        return from.get("id");
    }

    @SuppressWarnings({"unchecked"})
    @Override
    protected Iterable<Path<String>> getFullTexts(Root<ConsumoWeb> p) {
        Path<String> pregunta = p.get("pregunta");
        Path<String> respuesta = p.get("respuesta");
        return Lists.newArrayList(pregunta, respuesta);
    }

    public ConsumoWeb persist(String id, String payloadMsg) {
        ConsumoWeb payload = new ConsumoWeb();
        payload.setConsumidor(id != null ? id.substring(1, 11) : "");
        payload.setFecha(new Date());
        payload.setMilis((int) System.currentTimeMillis());
        payload.setStatus(0);
        payload.setPregunta(payloadMsg);
        return persist(payload);
    }
}
