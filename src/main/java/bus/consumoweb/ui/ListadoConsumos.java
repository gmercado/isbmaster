package bus.consumoweb.ui;

import bus.consumoweb.dao.ConsumoWebDao;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.components.Listado;
import bus.database.components.ListadoDataProvider;
import bus.database.components.ListadoPanel;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.DPropertyColumn;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.model.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marcelo Morales
 *         Created: 11/25/11 9:22 AM
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Menu(value = "Consumos de Servicios Web", subtitulo = "Hist\u00f3rico de los consumos web (cliente de servicios web)", subMenu = SubMenu.SERVICIO_WEB)
public class ListadoConsumos extends Listado<ConsumoWeb, Long> {

    private static final long serialVersionUID = -8010405733191234786L;

    @Override
    protected ListadoPanel<ConsumoWeb, Long> newListadoPanel(String id) {
        return new ListadoPanel<ConsumoWeb, Long>(id) {

            @Override
            protected List<IColumn<ConsumoWeb, String>> newColumns(ListadoDataProvider listadoDataProvider) {
                final ArrayList<IColumn<ConsumoWeb, String>> iColumns = new ArrayList<>();
                iColumns.add(new DPropertyColumn<>(Model.of("Id"), "id", "id"));
                iColumns.add(new PropertyColumn<>(Model.of("Consumidor"), "consumidor", "consumidor"));
                iColumns.add(new PropertyColumn<>(Model.of("Fecha"), "fecha", "fechaFormato"));
                iColumns.add(new DPropertyColumn<>(Model.of("Milis"), "milis", "milis"));
                iColumns.add(new DPropertyColumn<>(Model.of("Estado"), "status", "status"));
                iColumns.add(new PropertyColumn<>(Model.of("Pregunta"), "pregunta", "pregunta"));
                iColumns.add(new PropertyColumn<>(Model.of("Respuesta"), "respuesta", "respuesta"));
                iColumns.add(new DPropertyColumn<>(Model.of("Id del servidor"), "idResp", "idResp"));
                return iColumns;
            }

            @Override
            protected Class<? extends Dao<ConsumoWeb, Long>> getProviderClazz() {
                return ConsumoWebDao.class;
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("id", false);
            }
        };
    }
}
