/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.consumoweb.codigobisa;

import bus.consumoweb.consumer.ConsumidorServiciosWeb;
import bus.consumoweb.consumer.DefaultHandlerRespuesta;
import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import com.google.inject.Inject;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.StatusLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.beans.XMLDecoder;
import java.io.*;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.*;
import static org.apache.commons.codec.binary.Base64.encodeBase64;

/**
 * @author Marcelo Morales
 *         Created: 2/6/12 3:40 PM
 */
public class ConsumidorServiciosWebCodigoBisa extends ConsumidorServiciosWeb {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumidorServiciosWebCodigoBisa.class);

    @Inject
    public ConsumidorServiciosWebCodigoBisa(@SQLFueraDeLinea ExecutorService executor,
                                            Dao<ConsumoWeb, Long> consumoWebDao,
                                            MedioAmbiente medioAmbiente) {
        super(executor, consumoWebDao, medioAmbiente);
    }

    @Override
    protected String getHeaderWithIdName() {
        return "codigo-bisa-id";
    }

    @Override
    protected String getConsumidorNombre() {
        return "codigobisa";
    }

    @Override
    protected HttpHost newProxyHost() {
        return null;
    }

    @Override
    protected String newPath(String template, Map<String, String> m) {
        String path = medioAmbiente.getValorDe(CODIGOBISA_PATH, CODIGOBISA_PATH_DEFAULT);
        return path + m.get("path");
    }

    @Override
    protected String newBasicAuthentication() {
        try {
            final String username = medioAmbiente.getValorDe(CODIGOBISA_USERNAME, CODIGOBISA_USERNAME_DEFAULT);
            final String password = medioAmbiente.getValorDe(CODIGOBISA_PASSWORD, CODIGOBISA_PASSWORD_DEFAULT);

            String basic = null;
            if (username != null) {
                basic = username + ":" + password;
                basic = new String(encodeBase64(basic.getBytes("UTF-8")), "UTF-8");
            }
            return basic;
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    protected HttpHost newHost() {
        final String host = medioAmbiente.getValorDe(CODIGOBISA_HOST, CODIGOBISA_HOST_DEFAULT);
        final int port = medioAmbiente.getValorIntDe(CODIGOBISA_PORT, CODIGOBISA_PORT_DEFAULT);
        return new HttpHost(host, port);
    }

    protected String newContentType(Map<String, String> m) {
        if (m.containsKey("content-type")) {
            return m.get("content-type");
        }
        return "text/plain";
    }

    @Override
    protected String newContentType() {
        return "text/plain";
    }

    @Override
    protected Object parseResult(Header[] headers, StatusLine statusLine, DefaultHandlerRespuesta handler, InputStream stream)
            throws SAXException, IOException {

        byte[] bytes = ByteStreams.toByteArray(stream);

        if (headers != null && headers.length > 0 && headers[0] != null) {
            if ("text/plain".equals(headers[0].getValue())) {
                LOGGER.debug("Devolviendo directamente lo respondido por el ws");
                return CharStreams.toString(new StringReader(new String(bytes, "UTF-8")));
            }

            if ("image/png".equals(headers[0].getValue())) {
                LOGGER.debug("Devolviendo un array de bytes");
                return bytes;
            }

            if ("application/xml".equals(headers[0].getValue())) {
                try {
                    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                    return documentBuilder.parse(new ByteArrayInputStream(bytes));
                } catch (ParserConfigurationException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        if (statusLine.getStatusCode() != 200) {
            LOGGER.debug("Asumimos que el contenido contiene el problema");
            return CharStreams.toString(new StringReader(new String(bytes, "UTF-8")));
        }

        LOGGER.debug("Asumiendo java serializado");

        String s = CharStreams.toString(new StringReader(new String(bytes, "UTF-8")));
        /*
         * OJO: a continuaci�n hay un reemplazo de paquetes para no tener que colocar la clase en el mismo lugar
         * Entiendo que es una mala pol�tica, pero es r�pido y no tengo que hacer un handler para el caso particular.
         */
        String replaced = s.replaceAll("com\\.bisa\\.codigobisa\\.servicio", "bus.codigobisa.model");

        LOGGER.debug("Dato obtenido: {}", replaced);

        XMLDecoder xmlDecoder = new XMLDecoder(new ByteArrayInputStream(replaced.getBytes("UTF-8")));
        Object o = xmlDecoder.readObject();
        xmlDecoder.close();
        return o;
    }

    @Override
    protected String getSoapAction() {
        return null;
    }
}
