/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.consumoweb.consumer;

import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.env.api.MedioAmbiente;
import bus.plumbing.utils.Watch;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.http.*;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.DefaultBHttpClientConnection;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.protocol.*;
import org.apache.http.ssl.SSLContexts;
import org.apache.wicket.util.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.net.ssl.SSLContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.*;

import static bus.env.api.Variables.*;
import static bus.plumbing.utils.Watches.CONSUMO_WS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.http.protocol.ExecutionContext.*;

/**
 * @author Marcelo Morales
 *         Created: 11/24/11 2:40 PM
 */
public abstract class ConsumidorServiciosWeb {

    private static final Header[] EMPTY_HEADERS = new Header[0];
    private static final String CONTENT_TYPE = "Content-type";
    private static final int RESPONSE_HTTP = 400;
    private static final int BUFFERSIZE = 8 * 1024;
    private static final String METHOD_GET = "GET";
    private static final String METHOD_POST = "POST";
    protected final Logger LOGGER = LoggerFactory.getLogger(getClass()); // Usamos el logger del hijo, no de esta clase
    protected final MedioAmbiente medioAmbiente;
    private final ExecutorService executor;
    private final Dao<ConsumoWeb, Long> consumoWebDao;

    public ConsumidorServiciosWeb(ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente) {
        this.executor = executor;
        this.consumoWebDao = consumoWebDao;
        this.medioAmbiente = medioAmbiente;
    }

    public final Object consumir(final String template, final Map<String, String> interpolar,
                                 final DefaultHandlerRespuesta handler) throws ExecutionException, TimeoutException {
        return consumir(template, interpolar, handler, medioAmbiente.getValorIntDe(WC_TIMEOUT, WC_TIMEOUT_DEFAULT));
    }

    public final Object consumir(final String template, final Map<String, String> interpolar,
                                 final DefaultHandlerRespuesta handler,
                                 int tiempo) throws ExecutionException, TimeoutException {
        Watch watch = new Watch(CONSUMO_WS);
        watch.start();
        Future<ConsumoWeb> consumoWebFuture = null;
        ResultadoDelConsumo resultadoDelConsumo = null;
        try {
            //Registrar consumo
            consumoWebFuture = executor.submit(() -> registrarConsumoWeb(interpolar));
            //consumir servicio
            resultadoDelConsumo = executor.submit(() -> consumirWS(template, interpolar, handler)).get(tiempo, SECONDS);

            return resultadoDelConsumo.resultadoDelConsumo;
        } catch (InterruptedException e) {
            LOGGER.warn("Se ha interrumpido un consumo de servicios web en " + getClass(), e);
            return null;
        } finally {
            final int milis = (int) watch.stop();
            //Actualizar consumo web
            actualizarConsumoWeb(handler, consumoWebFuture, resultadoDelConsumo, milis);
        }
    }

    private void actualizarConsumoWeb(DefaultHandlerRespuesta handler, final Future<ConsumoWeb> consumoWebFuture, final ResultadoDelConsumo resultadoDelConsumo, int milis) {
        if (consumoWebFuture != null) {
            executor.submit(() -> {
                try {
                    actualizarConsumoWeb(handler, milis, consumoWebFuture, resultadoDelConsumo);
                } catch (InterruptedException e) {
                    LOGGER.error("Interrumpido", e);
                } catch (TimeoutException e) {
                    LOGGER.error("Ay!, la persistencia de consumo web tarda mucho", e);
                } catch (Exception e) {
                    LOGGER.error("Ha ocurrido un error inesperado", e);
                }
            });
        }
    }

    private void actualizarConsumoWeb(DefaultHandlerRespuesta handler, int milis, Future<ConsumoWeb> finalConsumoWebFuture, ResultadoDelConsumo resultadoDelConsumo) throws InterruptedException, ExecutionException, TimeoutException {
        final ConsumoWeb consumoWeb = finalConsumoWebFuture.get(10, TimeUnit.SECONDS);
        final Header[] finalHeaders;
        final Integer finalStatusCode;
        final Object finalO;
        if (resultadoDelConsumo != null) {
            finalHeaders = resultadoDelConsumo.headers;
            finalStatusCode = resultadoDelConsumo.statusLine;
            finalO = resultadoDelConsumo.resultadoDelConsumo;
        } else {
            finalHeaders = EMPTY_HEADERS;
            finalStatusCode = 0;
            finalO = null;
        }
        consumoWeb.setStatus(finalStatusCode);
        consumoWeb.setMilis(milis);
        consumoWeb.setRespuesta(StringUtils.abbreviate(handler.getRespuesta(finalO), 100));
        for (Header finalHeader : finalHeaders) {
            Header header = null;
            try {
                header = finalHeader;
                Long data = Long.parseLong(header.getValue());
                LOGGER.debug("header value " + data + " <- ");
                consumoWeb.setIdResp(header.getValue());
                break;
            } catch (NumberFormatException e) {
                LOGGER.error("No puedo convertir " + header.getValue() + " a long");
            }
        }
        consumoWebDao.merge(consumoWeb);
    }

    private ConsumoWeb registrarConsumoWeb(Map<String, String> interpolar) {
        final ConsumoWeb consumoWeb = new ConsumoWeb();
        consumoWeb.setFecha(new Date());
        consumoWeb.setConsumidor(StringUtils.abbreviate(getConsumidorNombre(), 10));
        consumoWeb.setPregunta(StringUtils.abbreviate(interpolar.toString(), 100));
        consumoWebDao.persist(consumoWeb);
        return consumoWeb;
    }

    protected Object parseResult(Header[] headers, StatusLine statusLine, DefaultHandlerRespuesta handler, InputStream stream) throws SAXException, IOException {
        try {
            final SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            parserFactory.setNamespaceAware(true);
            final SAXParser parser = parserFactory.newSAXParser();
            parser.parse(stream, handler);
            return null;
        } catch (ParserConfigurationException e) {
            throw new IllegalStateException(e);
        }
    }

    protected abstract String getHeaderWithIdName();

    private ResultadoDelConsumo consumirWS(String template, final Map<String, String> interpolar,
                                           final DefaultHandlerRespuesta handler)
            throws IOException {
        DefaultBHttpClientConnection conn = null;
        try {

            conn = new DefaultBHttpClientConnection(BUFFERSIZE);
            final HttpResponse response = consumir(template, interpolar, conn);
            Header[] headers = response.getHeaders(getHeaderWithIdName());

            StatusLine statusLine = response.getStatusLine();
            Integer statusCode = statusLine.getStatusCode();

            InputStream stream = null;
            try {
                final HttpEntity entity = response.getEntity();
                try {
                    stream = entity.getContent();
                } catch (IllegalStateException ignored) {
                    LOGGER.debug("El servicio web no tiene contenido", ignored);
                }
                Object resultadoDelConsumo;
                if (statusCode >= RESPONSE_HTTP) { // es un error http. Generalmente tenemos que mostrarlo
                    String st;
                    if (stream == null) {
                        st = "<sin contenido>";
                    } else {
                        try {
                            st = IOUtils.toString(stream);
                        } catch (IOException e) {
                            st = "<sin contenido>";
                        }
                    }
                    final String s = st;
                    LOGGER.warn("Hemos consumido un servicio que ha retornado un error HTTP:" +
                                    " err={}, est='{}', cont='{}', param={}. Puede ser" +
                                    " cliente sin codbisa, clave movil no existe en aqua o error inesperado",
                            statusCode, statusLine, s, interpolar);

                    /*input.is = new ByteArrayInputStream(s.getBytes());
                    resultadoDelConsumo = handler.apply(input);*/

                    resultadoDelConsumo = parseResult(response.getHeaders(CONTENT_TYPE), statusLine, handler,
                            new ByteArrayInputStream(s.getBytes()));
                } else {
                    /*input.is = stream;
                    resultadoDelConsumo = handler.apply(input);*/
                    resultadoDelConsumo = parseResult(response.getHeaders(CONTENT_TYPE), statusLine, handler, stream);
                }
                ResultadoDelConsumo r = new ResultadoDelConsumo();
                r.resultadoDelConsumo = resultadoDelConsumo;
                r.statusLine = statusCode;
                r.headers = headers;
                return r;

            } catch (SAXException e) {
                LOGGER.error("Error al parser objeto", e);
                return null;
            } finally {
                IOUtils.closeQuietly(stream);
            }
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (IOException e) {
                    LOGGER.error("Error al cerrar", e);
                }
            }
        }
    }

    private HttpResponse consumir(String template, Map<String, String> m, DefaultBHttpClientConnection conn)
            throws IOException {
        final HttpResponse response;
        final HttpProcessor httpProcessor = newProcessor();
        final SocketConfig params = newParams();
        final HttpHost httpHost = newHost();
        final HttpRequestExecutor httpexecutor = new HttpRequestExecutor();
        final HttpCoreContext coreContext = HttpCoreContext.create();
        coreContext.setTargetHost(httpHost);
        coreContext.setAttribute(HTTP_CONNECTION, conn);
        coreContext.setAttribute(HTTP_TARGET_HOST, httpHost);
        final HttpHost proxyHost = newProxyHost();
        if (proxyHost != null) {
            coreContext.setAttribute(HTTP_PROXY_HOST, proxyHost);
        }
        HttpEntity requestBody;
        if (template == null) {
            requestBody = null;
        } else {
            LOGGER.debug("Voy a interpolar {} el la plantilla {}", m, template);
            final StrSubstitutor strs = new StrSubstitutor(m);
            final String cuerpo = strs.replace(template);
            LOGGER.debug("Cuerpo del envio {}", cuerpo);
            requestBody = new StringEntity(cuerpo);
        }

        try {
            long t = System.currentTimeMillis();
            LOGGER.debug("Intentando conectarme al host {}, puerto {}", httpHost.getHostName(), httpHost.getPort());
            //Definir socket
            SocketAddress socketAddress = new InetSocketAddress(httpHost.getHostName(), httpHost.getPort());
            Socket socket = newSocket(socketAddress);
            socket.connect(socketAddress, medioAmbiente.getValorIntDe(WC_TIMEOUT, WC_TIMEOUT_DEFAULT));
            LOGGER.debug("Intentando juntar el socket creado con el cliente de HTTP");
            conn.bind(socket);
            conn.setSocketTimeout(params.getSoTimeout());

            final String path = newPath(template, m);
            final BasicHttpEntityEnclosingRequest request;
            if (requestBody == null) {
                request = new BasicHttpEntityEnclosingRequest(METHOD_GET, path, HttpVersion.HTTP_1_1);
            } else {
                request = new BasicHttpEntityEnclosingRequest(METHOD_POST, path, HttpVersion.HTTP_1_1);
                request.setEntity(requestBody);
            }
            request.addHeader(CONTENT_TYPE, newContentType(m));
            final String soapAction = getSoapAction();
            if (soapAction != null) {
                request.setHeader("SOAPAction", getSoapAction());
            }
            String basic = newBasicAuthentication();
            if (basic != null) {
                request.addHeader("Authorization", "Basic " + basic);
            }
            request.setHeader("Accept", "*/*");
            request.setHeader("Host", httpHost.getHostName());
            if (params.isSoKeepAlive()) {
                request.setHeader("Connection", "keep-alive");
            } else {
                request.setHeader("Connection", "keep-alive");
            }
            LOGGER.debug("Haciendo una peticion a {}", request.getRequestLine().getUri());
            httpexecutor.preProcess(request, httpProcessor, coreContext);

            response = httpexecutor.execute(request, conn, coreContext);

            httpexecutor.postProcess(response, httpProcessor, coreContext);

            LOGGER.debug("Consumo a servicio web en host {} ha tomado {}",
                    httpHost.getHostName(),
                    System.currentTimeMillis() - t);
        } catch (UnknownHostException e) {
            throw new RuntimeException("<UnknownHostException>Ha existido un error de configuracion de red: el host '" +
                    e.getMessage() + "' no se encuentra al intentar conectarme con el host " + httpHost);
        } catch (SocketException e) {
            throw new RuntimeException("<SocketException>Ha existido un error en la comunicacion de red al conectarme con " + httpHost, e);
        } catch (IOException e) {
            throw new RuntimeException("<IOException>Ha ocurrido un error inesperado con " + httpHost, e);
        } catch (HttpException e) {
            throw new RuntimeException("<HttpException>Ha ocurrido un error inesperado con " + httpHost, e);
        }

        LOGGER.debug("Recibiendo respuesta {}", response.getStatusLine());
        return response;
    }

    private HttpProcessor newProcessor() {
        return HttpProcessorBuilder.create().add(new RequestContent())
                .add(new RequestTargetHost())
                .add(new RequestConnControl())
                .add(new RequestUserAgent("isb/1.1"))
                .add(new RequestExpectContinue(true))
                .build();
    }

    protected Socket newSocket(SocketAddress socketAddress) throws IOException {
        try {

            SSLContext sslcontext = SSLContexts.custom()
                    .loadTrustMaterial(null, new TrustSelfSignedStrategy())
                    .build();
            SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslcontext);
            return sslConnectionSocketFactory.createSocket(null);
            /*SSLSocketFactory sslSocketFactory = new SSLSocketFactory(new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    return true;
                }
            });
            return sslSocketFactory.createSocket(null);*/
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            throw new IllegalStateException(e);
        }
    }

    protected abstract String newContentType(Map<String, String> m);

    protected abstract String newContentType();

    protected abstract String getConsumidorNombre();

    protected abstract HttpHost newProxyHost();

    protected abstract String newPath(String template, Map<String, String> m);

    protected abstract String newBasicAuthentication();

    protected abstract HttpHost newHost();

    protected abstract String getSoapAction();

    private SocketConfig newParams() {
        return SocketConfig.custom()
                .setSoTimeout(medioAmbiente.getValorIntDe(WC_SO_TIMEOUT, WC_SO_TIMEOUT_DEFAULT))
                .setSoReuseAddress(false)
                .setSoKeepAlive(false)
                .setSoLinger(-1)
                .setTcpNoDelay(true)
                .build();
    }

    public ExecutorService getExecutor() {
        return executor;
    }

    public Dao<ConsumoWeb, Long> getConsumoWebDao() {
        return consumoWebDao;
    }

    public MedioAmbiente getMedioAmbiente() {
        return medioAmbiente;
    }

    private static class ResultadoDelConsumo {
        Object resultadoDelConsumo;
        Integer statusLine;
        Header[] headers;
    }
}
