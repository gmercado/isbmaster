package bus.consumoweb.consumer;

import org.apache.commons.lang.ArrayUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * @author by rsalvatierra on 27/06/2017.
 */
public class ExtraerDatoHandler extends DefaultHandlerRespuesta {

    private final ArrayList<String>[] strings;

    private final String[] elemento;

    private final Callable<String> respuestaClosure;

    private StringBuilder stringBuilder = null;

    public ExtraerDatoHandler(ArrayList<String>[] strings, Callable<String> respuestaClosure, String... elemento) {
        super(EstiloWS.SIMPLE_XML);
        this.strings = strings;
        this.elemento = elemento;
        this.respuestaClosure = respuestaClosure;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        int elementPos = ArrayUtils.indexOf(elemento, localName);
        if (elementPos != -1) {
            stringBuilder = new StringBuilder();
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (stringBuilder == null) {
            return;
        }
        stringBuilder.append(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        int elementPos = ArrayUtils.indexOf(elemento, localName);
        if (elementPos != -1) {
            strings[elementPos].add(stringBuilder.toString());
            stringBuilder = null;
        }
    }

    @Override
    public String getRespuesta(Object o) {
        try {
            return respuestaClosure.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
