/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.consumoweb.consumer;

import com.google.common.base.Charsets;
import com.google.common.base.Function;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import com.google.common.io.Closeables;
import org.apache.http.Header;
import org.apache.http.StatusLine;
import org.bouncycastle.util.io.TeeInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.soap.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.beans.XMLDecoder;
import java.io.*;

import static org.apache.commons.lang.StringUtils.replace;

/**
 * @author Marcelo Morales
 *         Created: 11/25/11 10:55 AM
 */
public class DefaultHandlerRespuesta extends DefaultHandler implements Function<DefaultHandlerRespuesta.ConsumoAProcesar, Object> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultHandlerRespuesta.class);
    private final EstiloWS estiloWS;
    private final Unmarshaller unmarshaller;
    private final Class<?> declaredType;

    public DefaultHandlerRespuesta() {
        estiloWS = EstiloWS.REST;
        unmarshaller = null;
        declaredType = null;
    }

    public DefaultHandlerRespuesta(EstiloWS estiloWS) {
        this.estiloWS = estiloWS;
        unmarshaller = null;
        declaredType = null;
    }

    public DefaultHandlerRespuesta(JAXBContext jaxbContext, Class<?> declaredType) throws JAXBException {
        this.estiloWS = EstiloWS.JAXB;
        this.unmarshaller = jaxbContext.createUnmarshaller();
        this.declaredType = declaredType;
    }

    public String getRespuesta(Object o) {
        if (o == null) {
            return null;
        }
        if (o instanceof JAXBElement) {
            final Object value = ((JAXBElement) o).getValue();
            if (value == null) {
                return null;
            }
            return value.toString();
        }
        return o.toString();
    }

    @Override
    public Object apply(ConsumoAProcesar input) {
        switch (estiloWS) {
            case SIMPLE_XML:
                try {
                    final SAXParserFactory parserFactory = SAXParserFactory.newInstance();
                    parserFactory.setNamespaceAware(true);
                    final SAXParser parser = parserFactory.newSAXParser();
                    parser.parse(input.is, this);
                    return null;
                } catch (ParserConfigurationException | SAXException | IOException e) {
                    throw new IllegalStateException(e);
                }
            case REST:
                try {
                    byte[] bytes = ByteStreams.toByteArray(input.is);

                    String contenido = CharStreams.toString(new StringReader(new String(bytes, "UTF-8")));

                    if (input.headers != null && input.headers.length > 0 && input.headers[0] != null) {
                        if ("text/plain".equals(input.headers[0].getValue())) {
                            LOGGER.debug("Devolviendo directamente lo respondido por el ws");
                            return contenido;
                        }

                        if ("image/png".equals(input.headers[0].getValue())) {
                            LOGGER.debug("Devolviendo un array de bytes");
                            return bytes;
                        }
                    }

                    if (input.statusLine.getStatusCode() != 200) {
                        LOGGER.debug("Asumimos que el contenido contiene el problema");
                        return contenido;
                    }

                    LOGGER.debug("Asumiendo java serializado");

                    /*
                     * OJO: a continuación hay un reemplazo de paquetes para no tener que colocar la clase en el mismo lugar
                     * Entiendo que es una mala política, pero es rápido y no tengo que hacer un handler para el caso particular.
                     */
                    String replaced = replace(contenido, "com.bisa.codigobisa.servicio", "ebisa.codigobisa.model");

                    LOGGER.debug("Dato obtenido: {}", replaced);

                    XMLDecoder xmlDecoder = new XMLDecoder(new ByteArrayInputStream(replaced.getBytes("UTF-8")));
                    Object o = xmlDecoder.readObject();
                    xmlDecoder.close();
                    return o;
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            case JAXB:
                final ByteArrayOutputStream output = new ByteArrayOutputStream();
                TeeInputStream teeInputStream = null;
                try {
                    teeInputStream = new TeeInputStream(input.is, output);
                    final MessageFactory messageFactory = MessageFactory.newInstance();
                    MimeHeaders mimeHeaders = new MimeHeaders();
                    for (Header header : input.headers) {
                        mimeHeaders.addHeader(header.getName(), header.getValue());
                    }
                    final SOAPMessage message = messageFactory.createMessage(mimeHeaders, teeInputStream);
                    final SOAPBody soapBody = message.getSOAPBody();
                    final Document document = soapBody.extractContentAsDocument();

                    if (LOGGER.isDebugEnabled()) {
                        try {
                            final TransformerFactory transformerFactory = TransformerFactory.newInstance();
                            final Transformer transformer = transformerFactory.newTransformer();
                            final StringWriter writer = new StringWriter();
                            transformer.transform(new DOMSource(document), new StreamResult(writer));
                            writer.close();
                            LOGGER.debug(writer.toString());
                        } catch (TransformerException ignored) {
                            // NOP
                        }
                    }

                    return unmarshaller.unmarshal(document, declaredType);
                } catch (SOAPException e) {
                    LOGGER.warn("Ha fallado SOAP: mensaje es '{}'", new String(output.toByteArray(), Charsets.UTF_8));
                    throw new IllegalStateException(e);
                } catch (IOException | JAXBException e) {
                    throw new IllegalStateException(e);
                } finally {
                    Closeables.closeQuietly(teeInputStream);
                }
        }
        throw new IllegalArgumentException();
    }

    public static class ConsumoAProcesar {
        public InputStream is;
        public Header[] headers;
        public StatusLine statusLine;
    }
}
