package bus.consumoweb.consumer;

/**
 * @author by rsalvatierra on 23/06/2017.
 */
public enum EstiloWS {
    /**
     * Aqua regular
     */
    SIMPLE_XML,

    /**
     * Codigo Bisa
     */
    REST,

    /**
     * Aqua pago de servicios.
     */
    JAXB
}
