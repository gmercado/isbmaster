package bus.consumoweb.consumer;

import bus.consumoweb.entities.ConsumoWeb;
import bus.database.dao.Dao;
import bus.env.api.MedioAmbiente;
import bus.plumbing.utils.Watch;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPBinding;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static bus.plumbing.utils.Watches.CONSUMO_WS;

/*import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.axis2.util.XMLUtils;*/

/**
 * @author rsalvatierra on 04/05/2016.
 */
public abstract class ClienteServiciosWeb {

    public static final String GET = "get";
    public static final String AUX = "ns1";
    protected final Logger LOGGER = LoggerFactory.getLogger(getClass()); // Usamos el logger del hijo, no de esta clase
    protected final MedioAmbiente medioAmbiente;
    private final ExecutorService executor;
    private final Dao<ConsumoWeb, Long> consumoWebDao;
    private int operationType;
    private Class service;
    private Class servicePort;
    private JaxWsProxyFactoryBean factory;

    @Inject
    private OutPayloadInterceptor outPayloadInterceptor;

    @Inject
    private InPayloadInterceptor inPayloadInterceptor;

    public ClienteServiciosWeb(ExecutorService executor, Dao<ConsumoWeb, Long> consumoWebDao, MedioAmbiente medioAmbiente) {
        this.executor = executor;
        this.consumoWebDao = consumoWebDao;
        this.medioAmbiente = medioAmbiente;
    }

    public <T> T setJaxWS(Class<T> service) {
        setProxy();
        factory = new JaxWsProxyFactoryBean();
        //Logging
        factory.getOutInterceptors().add(outPayloadInterceptor);
        factory.getInInterceptors().add(inPayloadInterceptor);
        //Endpoint
        factory.setAddress(getEndPoint());
        //Create port
        return factory.create(service);
    }

    public void addClient(Object servicePort, Boolean MTOM) {
        //By Default
        Client client = ClientProxy.getClient(servicePort);
        HTTPConduit http = (HTTPConduit) client.getConduit();
        BindingProvider bp = (BindingProvider) servicePort;
        if (MTOM) {
            SOAPBinding binding = (SOAPBinding) bp.getBinding();
            binding.setMTOMEnabled(true);
        }
        http.setClient(getHttpClientPolicy());
    }

    public final Object consumir(Object newMessage) {

        Watch watch = new Watch(CONSUMO_WS);
        watch.start();

        LOGGER.debug("Iniciando ejecucion client service");
        /*ServiceClient serviceClient;
        EndpointReference endPointRef;
        Object resultado = null;
        OMElement response = null, request;
        Future<ConsumoWeb> consumoWebFuture = null;
        QName serviceQname, operationQname;
        URL url;
        String nameSpace, serviceName, operationName, portName, endPoint;
        HttpTransportProperties.ProxyProperties proxy;
        Class clazz;
        int responseCode = 0;
        try {
            nameSpace = getNameSpace();
            serviceName = getServiceName();
            operationName = getOperationName();
            portName = getPort();
            endPoint = getEndPoint();
            proxy = getProxy();
            clazz = getResult();
            url = newURL();
            serviceQname = new QName(nameSpace, serviceName);
            operationQname = new QName(nameSpace, operationName);
            endPointRef = new EndpointReference(endPoint);
            request = createMessage(newMessage);
            LOGGER.debug("Parametros {}", request.toStringWithConsume());
            //Registrar consumo
            consumoWebFuture = registrarConsumo(request);
            serviceClient = new ServiceClient(null, url, serviceQname, portName);
            serviceClient.setTargetEPR(endPointRef);
            serviceClient.getOptions().setProperty(HTTPConstants.CHUNKED, false);
            if (proxy != null) {
                LOGGER.debug("Estableciendo conexion Proxy.");
                serviceClient.getOptions().setProperty(HTTPConstants.PROXY, proxy);
            }
            LOGGER.debug("Obteniendo la respuesta de la conexion.");
            response = serviceClient.sendReceive(operationQname, request);
            if (response != null) {
                responseCode = 200;
                resultado = parseResult(response, clazz);
            }
        } catch (AxisFault a) {
            responseCode = 0;
            LOGGER.error("Error en consumo de servicio ", a);
        } catch (Exception e) {
            responseCode = 0;
            LOGGER.error("Error inesperado en consumo de servicio ", e);
        } finally {
            final int milis = (int) watch.stop();
            actualizaConsumo(response, responseCode, milis, null, consumoWebFuture);
        }
        return resultado;*/
        return null;
    }
/*
    public final Object parseResult(OMElement response, Class clazz) throws Exception {
        LOGGER.debug("ParseResult...");
        ByteArrayOutputStream baos = getXmlToBytes((XMLUtils.toDOM(response)).getOwnerDocument());
        InputStream ios = new ByteArrayInputStream(baos.toByteArray());
        JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return jaxbUnmarshaller.unmarshal(ios);
    }*/

    private ByteArrayOutputStream getXmlToBytes(Document docTemp) throws TransformerException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        TransformerFactory.newInstance().newTransformer().transform(new DOMSource(docTemp), new StreamResult(baos));
        return baos;
    }
/*
    private Future<ConsumoWeb> registrarConsumo(OMElement solicitud) {
        final ConsumoWeb consumoWeb = new ConsumoWeb();
        Future<ConsumoWeb> consumoWebFuture;
        consumoWebFuture = getExecutor().submit(() -> {
            consumoWeb.setFecha(new Date());
            consumoWeb.setConsumidor(StringUtils.abbreviate(getId(), 10));
            if (solicitud != null) {
                consumoWeb.setPregunta(solicitud.toStringWithConsume());
            }
            getConsumoWebDao().persist(consumoWeb);
            LOGGER.debug("Registrando el consumo de WS.");
            return consumoWeb;
        });
        return consumoWebFuture;
    }

    private void actualizaConsumo(OMElement respuesta, int codHttp, Integer milis, DatosConsumidor datosConsumidor, Future<ConsumoWeb> future) {
        if (future != null) {
            final Future<ConsumoWeb> finalConsumoWebFuture = future;
            final Integer finalStatusCode = codHttp;
            getExecutor().submit(() -> {
                try {
                    final ConsumoWeb consumoWeb = finalConsumoWebFuture.get(10, TimeUnit.SECONDS);
                    consumoWeb.setStatus(finalStatusCode);
                    consumoWeb.setMilis(milis);
                    if (respuesta != null) {
                        consumoWeb.setRespuesta(respuesta.toStringWithConsume());
                    }
                    if (datosConsumidor != null) {
                        consumoWeb.setIdResp("" + datosConsumidor.getStringReferencia());
                        consumoWeb.setIdRelacionado(datosConsumidor.getIdReferencia());
                    }
                    getConsumoWebDao().merge(consumoWeb);
                    LOGGER.debug("Actualizando el consumo de WS.");
                } catch (InterruptedException e) {
                    LOGGER.error("Error: Actualizacion de consumo interrumpido.", e);
                } catch (TimeoutException e) {
                    LOGGER.error("Error: La persistencia de consumo web tarda mucho.", e);
                } catch (Exception e) {
                    LOGGER.error("Error: Ha ocurrido un error inesperado", e);
                }
            });
        }
    }

    public OMElement createMessage(Object o) {
        XmlRootElement xml = o.getClass().getAnnotation(XmlRootElement.class);
        OMFactory fac = OMAbstractFactory.getOMFactory();
        OMNamespace omNs = fac.createOMNamespace(xml.namespace(), AUX);
        OMElement method = fac.createOMElement(xml.name(), omNs);
        OMElement value;
        String field;
        String get;
        int comienza;
        for (Method m : o.getClass().getDeclaredMethods()) {
            comienza = m.getName().indexOf(GET);
            if (comienza >= 0) {
                field = m.getName().substring(3);
                get = getValor(o, m);
                if (get != null) {
                    value = fac.createOMElement(field, omNs);
                    value.setText(get);
                    method.addChild(value);
                }
            }
        }
        return method;
    }
*/

    public Future<ConsumoWeb> registrarConsumo(String solicitud) {
        final ConsumoWeb consumoWeb = new ConsumoWeb();
        Future<ConsumoWeb> consumoWebFuture;
        consumoWebFuture = getExecutor().submit(() -> {
            consumoWeb.setFecha(new Date());
            consumoWeb.setConsumidor(StringUtils.abbreviate(getId(), 10));
            if (solicitud != null) {
                consumoWeb.setPregunta(solicitud);
            }
            getConsumoWebDao().persist(consumoWeb);
            LOGGER.debug("Registrando el consumo de WS.");
            return consumoWeb;
        });
        return consumoWebFuture;
    }

    public void actualizaConsumo(String respuesta, int codHttp, Integer milis, Future<ConsumoWeb> future) {
        if (future != null) {
            final Future<ConsumoWeb> finalConsumoWebFuture = future;
            final Integer finalStatusCode = codHttp;
            getExecutor().submit(() -> {
                try {
                    final ConsumoWeb consumoWeb = finalConsumoWebFuture.get(10, TimeUnit.SECONDS);
                    consumoWeb.setStatus(finalStatusCode);
                    consumoWeb.setMilis(milis);
                    if (respuesta != null) {
                        consumoWeb.setRespuesta(respuesta);
                    }
                    getConsumoWebDao().merge(consumoWeb);
                    LOGGER.debug("Actualizando el consumo de WS.");
                } catch (InterruptedException e) {
                    LOGGER.error("Error: Actualizacion de consumo interrumpido.", e);
                } catch (TimeoutException e) {
                    LOGGER.error("Error: La persistencia de consumo web tarda mucho.", e);
                } catch (Exception e) {
                    LOGGER.error("Error: Ha ocurrido un error inesperado", e);
                }
            });
        }
    }

    private String getValor(Object o, Method m) {
        String value = null;
        try {
            Object object = m.invoke(o);
            if (object != null) {
                value = String.valueOf(object);
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            LOGGER.error("Error: Ha ocurrido un error inesperado", e);
        }
        return value;
    }

    public ExecutorService getExecutor() {
        return executor;
    }

    public Dao<ConsumoWeb, Long> getConsumoWebDao() {
        return consumoWebDao;
    }

    public MedioAmbiente getMedioAmbiente() {
        return medioAmbiente;
    }

    public int getType() {
        return this.operationType;
    }

    public void setType(int type) {
        this.operationType = type;
    }

    public JaxWsProxyFactoryBean getFactory() {
        return factory;
    }

    public Class getService() {
        return service;
    }

    public void setService(Class service) {
        this.service = service;
    }


    public Class getServicePort() {
        return servicePort;
    }

    public void setServicePort(Class servicePort) {
        this.servicePort = servicePort;
    }

    protected abstract URL newURL();

    protected abstract Class getResult();

    protected abstract String getPort();

    protected abstract String getEndPoint();

    protected abstract String getOperationName();

    protected abstract String getServiceName();

    protected abstract String getId();

    protected abstract String getNameSpace();

    protected abstract void setProxy();

    protected HTTPClientPolicy getHttpClientPolicy() {
        return null;
    }
}
