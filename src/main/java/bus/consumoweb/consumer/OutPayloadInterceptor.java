package bus.consumoweb.consumer;

import bus.consumoweb.dao.ConsumoWebDao;
import bus.consumoweb.entities.ConsumoWeb;
import com.google.inject.Inject;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.LoggingMessage;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicReference;

/**
 * @author by rsalvatierra on 14/02/2017.
 */
public class OutPayloadInterceptor extends LoggingOutInterceptor {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ConsumoWebDao consumoWebDao;
    private final AtomicReference<Message> messageWrapper = new AtomicReference<>();

    @Inject
    public OutPayloadInterceptor(ConsumoWebDao consumoWebDao) {
        super();
        this.consumoWebDao = consumoWebDao;
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        messageWrapper.set(message);
        super.handleMessage(message);
    }

    @Override
    protected String formatLoggingMessage(LoggingMessage loggingMessage) {
        Message message = messageWrapper.get();
        String outSOAP = loggingMessage.getPayload().toString();
        //store payload
        ConsumoWeb payload = consumoWebDao.persist(message.getExchange().getService().getName().getLocalPart(), outSOAP);
        //append the ID to the request as a new parameter
        message.getExchange().put(ConsumoWebDao.TRANSPORTED_PAYLOAD_ID, payload);

        logger.debug(" OUT < ****************************************");
        logger.debug(outSOAP);
        logger.debug(" OUT > ****************************************");

        return super.formatLoggingMessage(loggingMessage);
    }
}
