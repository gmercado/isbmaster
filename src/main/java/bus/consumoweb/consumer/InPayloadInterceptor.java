package bus.consumoweb.consumer;

import bus.consumoweb.dao.ConsumoWebDao;
import bus.consumoweb.entities.ConsumoWeb;
import com.google.inject.Inject;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingMessage;
import org.apache.cxf.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author by rsalvatierra on 14/02/2017.
 */
public class InPayloadInterceptor extends LoggingInInterceptor {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ConsumoWebDao consumoWebDao;
    private final AtomicReference<Message> messageWrapper = new AtomicReference<>();

    @Inject
    public InPayloadInterceptor(ConsumoWebDao consumoWebDao) {
        super();
        this.consumoWebDao = consumoWebDao;
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        messageWrapper.set(message);
        super.handleMessage(message);
    }

    @Override
    protected String formatLoggingMessage(LoggingMessage loggingMessage) {

        String inSOAP = loggingMessage.getPayload().toString();
        Message message = messageWrapper.get();

        logger.debug(" IN < ****************************************");
        logger.debug(inSOAP);
        logger.debug(" IN > ****************************************");

        ConsumoWeb wsPayload = (ConsumoWeb) message.getExchange().get(ConsumoWebDao.TRANSPORTED_PAYLOAD_ID);

        if (wsPayload != null) {
            wsPayload.setStatus(200);
            wsPayload.setRespuesta(loggingMessage.getPayload().toString());

            AtomicLong now = new AtomicLong(System.currentTimeMillis());
            wsPayload.setMilis((int) now.addAndGet(-1 * wsPayload.getMilis()));
            consumoWebDao.merge(wsPayload);
        }
        return super.formatLoggingMessage(loggingMessage);
    }
}
