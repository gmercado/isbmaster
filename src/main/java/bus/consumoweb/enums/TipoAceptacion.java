package bus.consumoweb.enums;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public enum TipoAceptacion {
    T("Si puede"),
    F("No puede");

    private String descripcion;

    private TipoAceptacion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
