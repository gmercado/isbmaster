package bus.consumoweb.enums;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public enum TipoEstadoUsuario {
    H("Habilitado"),
    I("Inhabilitado");

    private String descripcion;

    private TipoEstadoUsuario(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
