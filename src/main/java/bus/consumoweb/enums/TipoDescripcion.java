package bus.consumoweb.enums;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public enum TipoDescripcion {

    DESCRIPCIONCAEDECACTECONO(1),
    DESCRIPCIONTIPCAN(2),
    DESCRIPCIONTIPINT(3),
    DESCRIPCIONTIPOPE(3),
    DESCRIPCIONTIPING(4);

    private int valor;

    private TipoDescripcion(int valor) {
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }
}
