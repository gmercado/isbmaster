package bus.consumoweb.enums;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public enum TipoAlerta {

    CONSULTA_AJENA(1, "ALERTAS POR CONSULTAS AJENAS"),
    CAMBIO_ESTADO(2, "ALERTAS POR CAMBIO EN EL SISTEMA FINANCIERO"),
    NUEVA_DEUDA(3, "ALERTAS POR NUEVA DEUDA EN EL SISTEMA FINANCIERO"),
    VARIACION_SALDO(4, "ALERTAS POR VARIACION DE SALDOS EN EL SISTEMA FINANCIERO"),
    RESUMEN_CONSULTA_AJENA(5, "ALERTA RESUMEN DE CONSULTAS AJENAS"),
    NUEVA_CONSULTA_AJENA(6, "ALERTAS POR CONSULTA AJENA");

    private int id;

    private String valor;

    private TipoAlerta(int id, String valor) {
        this.id = id;
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public String getValor() {
        return valor;
    }
}
