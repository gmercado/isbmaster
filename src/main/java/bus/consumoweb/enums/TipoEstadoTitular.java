package bus.consumoweb.enums;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public enum TipoEstadoTitular {
    P("Procesado completamente"),
    N("No iniciado"),
    E("Eliminado");

    private String descripcion;

    private TipoEstadoTitular(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
