package bus.consumoweb.enums;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public enum CodigoRespuesta {

    OK("000", "Correcto"),
    ERR_CON_INFOCRED("101", "Error en el consumo de INFOCRED"),
    ERR_CON_DATA_DESACTUALIZADA("102", "Encontramos el registro en base pero es desactualizado"),
    RESPUESTA_INFOCRED_DUPLICADO("103", "Infocred retorno mas de un resultado"),
    CONSULTA_NO_INICIADA("104", "No se hizo la consulta crediticia del titular"),
    SIN_RESULTADOS("105", "Infocred no retorno resultados"),
    ERR_BASE_DE_DATOS_INFOCRED("901", "Ocurrio un error en base de datos"),
    ERR_CONEXION("902", "Error al obtener conexion para generar el PDF."),
    ERR_INESPERADO("999", "Error inesperado");

    private String codigo;
    private String descripcion;

    CodigoRespuesta(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static CodigoRespuesta get(String codigo) {
        for (CodigoRespuesta valor : CodigoRespuesta.values()) {
            if (valor.getCodigo().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
