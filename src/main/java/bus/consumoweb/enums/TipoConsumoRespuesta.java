package bus.consumoweb.enums;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public enum TipoConsumoRespuesta {

    CONSUMO("CM"),
    CONSULTA("CN"),
    ERROR("ER");

    private String descripcion;

    TipoConsumoRespuesta(String descripcion) {
        this.descripcion = descripcion;
    }

    public static TipoConsumoRespuesta get(String codigo) {
        for (TipoConsumoRespuesta valor : TipoConsumoRespuesta.values()) {
            if (valor.getDescripcion().equals(codigo)) {
                return valor;
            }
        }
        return null;
    }

    public static TipoConsumoRespuesta getId(String valor) {
        if (valor.equals(TipoConsumoRespuesta.CONSUMO.toString())) {
            return TipoConsumoRespuesta.CONSUMO;
        } else if (valor.equals(TipoConsumoRespuesta.CONSULTA.toString())) {
            return TipoConsumoRespuesta.CONSULTA;
        } else if (valor.equals(TipoConsumoRespuesta.ERROR.toString())) {
            return TipoConsumoRespuesta.ERROR;
        }
        return null;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
