package bus.consumoweb.enums;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public enum TipoDeuda {
    DIRECTA("Saldo Persona Deuda Directa"),
    INDIRECTA("Saldo Persona Deuda Indirecta");

    private String descripcion;

    private TipoDeuda(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
