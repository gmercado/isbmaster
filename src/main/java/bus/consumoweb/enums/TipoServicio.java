package bus.consumoweb.enums;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public enum TipoServicio {

    INFOCRED("Consumo de INFOCRED"),
    SEGIP("Consumo de SEGIP");

    private String descripcion;

    private TipoServicio(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
