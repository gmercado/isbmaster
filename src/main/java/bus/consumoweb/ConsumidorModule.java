/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.consumoweb;

import bus.consumoweb.aqua.ConsumidorServiciosWebAqua;
import bus.consumoweb.codigobisa.ConsumidorServiciosWebCodigoBisa;
import bus.consumoweb.consumer.ConsumidorServiciosWeb;
import bus.consumoweb.dao.ConsumoWebDao;
import bus.consumoweb.entities.ConsumoWeb;
import bus.consumoweb.model.*;
import bus.consumoweb.yellowpepper.ConsumidorServiciosWebYellowPepper;
import bus.database.dao.Dao;
import bus.plumbing.api.AbstractModuleBisa;
import com.bisa.bus.servicios.skyb.boa.api.ProcesosBoa;
import com.bisa.bus.servicios.skyb.boa.api.ServiciosBoa;
import com.bisa.bus.servicios.skyb.boa.consumer.ConsumidorServiciosBoa;
import com.google.inject.TypeLiteral;

/**
 * @author Marcelo Morales
 *         Created: 11/24/11 4:38 PM
 */
public class ConsumidorModule extends AbstractModuleBisa {

    @Override
    protected void configure() {

        bind(ConsumidorServiciosWeb.class).annotatedWith(ConsumidorAqua.class).to(ConsumidorServiciosWebAqua.class);
        bind(ConsumidorServiciosWeb.class).annotatedWith(ConsumidorCodigoBisa.class).to(ConsumidorServiciosWebCodigoBisa.class);
        bind(ConsumidorServiciosWeb.class).annotatedWith(ConsumidorYellowPepper.class).to(ConsumidorServiciosWebYellowPepper.class);
        bind(ConsumidorServiciosWeb.class).annotatedWith(ConsumidorBoa.class).to(ConsumidorServiciosBoa.class);
        bind(Class.class).annotatedWith(ServicioBoa.class).toInstance(ServiciosBoa.class);
        bind(Class.class).annotatedWith(ProcesoBoa.class).toInstance(ProcesosBoa.class);

        bind(new TypeLiteral<Dao<ConsumoWeb, Long>>() {
        }).to(ConsumoWebDao.class);
        bindUI("bus/consumoweb/ui");

    }
}
