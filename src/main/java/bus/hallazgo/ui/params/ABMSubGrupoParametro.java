package bus.hallazgo.ui.params;

import bus.hallazgo.api.BisaWebPagePanel;
import bus.hallazgo.api.ModoPantalla;
import bus.hallazgo.dao.ParametroDao;
import bus.hallazgo.entities.Parametro;
import bus.hallazgo.entities.SubGrupoParametro;
import bus.plumbing.components.AjaxToolButton;
import bus.plumbing.components.AlertFeedbackPanel;
import bus.users.entities.AdmUser;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.validator.StringValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * La clase <code>ABMSubGrupoParametro.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 14/05/2014, 12:55:46
 *
 */
public abstract class ABMSubGrupoParametro extends BisaWebPagePanel<SubGrupoParametro, AdmUser> {
    
    private static final long serialVersionUID = -5978248150995624818L;
    private static final Logger LOGGER = LoggerFactory.getLogger(ABMSubGrupoParametro.class);
    
    private final Parametro parametroPadre;
    private final IModel<SubGrupoParametro> parametro;
    
    private final String titulo;
    private final ModoPantalla modoPantalla;
    
    private IModel<String> mensajeErrorModel = Model.of("");
    private Component mensajeErrorLabel = null;
    
    public ABMSubGrupoParametro(String id, IModel<AdmUser> modelUser, String idPadre, AlertFeedbackPanel alertFeedbackPanel) {
        super(id, modelUser, alertFeedbackPanel);
        this.parametroPadre = getParametroDao().find(idPadre); 
        
        this.parametro = new Model<SubGrupoParametro>();
        SubGrupoParametro nuevo = new SubGrupoParametro();
        nuevo = (SubGrupoParametro) getParametroDao().getSiguienteParametro(nuevo, parametroPadre);
        
        this.parametro.setObject(nuevo);
        this.titulo = "Nuevo Par�metro";
        this.modoPantalla = ModoPantalla.ALTA;

    }

    public ABMSubGrupoParametro(String id, IModel<AdmUser> modelUser, String idPadre, IModel<SubGrupoParametro> modelParam, AlertFeedbackPanel alertFeedbackPanel) {
        super(id, modelUser, alertFeedbackPanel);
        this.parametro = modelParam;
        this.titulo = "Modificar Par�metro";
        this.modoPantalla = ModoPantalla.MOD_BAJA;

        this.parametroPadre = getParametroDao().find(idPadre);
    }

    @Override
    protected IModel<SubGrupoParametro> getTypedModel() {
        return parametro;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        addComponent(new Label("_titulo", titulo));

        mensajeErrorLabel = new Label("mensajeError", mensajeErrorModel).setVisible(false);

        addComponent(mensajeErrorLabel);

        addComponentConBorde(new TextField<String>("id")
                .add(StringValidator.maximumLength(2))
                .setRequired(true)
                .setOutputMarkupId(true)
                .setVisible(true)
                .setEnabled(false));

        addComponentConBorde(new TextField<String>("descripcion")
                .add(StringValidator.maximumLength(100))
                .setRequired(true)
                .setOutputMarkupId(true).setVisible(true), 8);

        addComponentConBorde(new TextField<String>("valor1")
                .add(StringValidator.maximumLength(20)).setLabel(Model.of("Labeles"))
                .setRequired(true)
                .setOutputMarkupId(true)
                .setVisible(StringUtils.isNotBlank(parametroPadre.getEtiqueta1()))
                );

        addComponentConBorde(new TextArea<String>("valor2")
                .add(StringValidator.maximumLength(6144))
                .setRequired(true)
                .setOutputMarkupId(true)
                .setVisible(StringUtils.isNotBlank(parametroPadre.getEtiqueta2())), 12);

        addComponent(new AjaxToolButton("crear"){
            private static final long serialVersionUID = 1L;

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(form);
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                LOGGER.debug("Iniciando creacion");
                Parametro param = parametro.getObject();

                ParametroDao paramDao = getParametroDao();

                // consistencia del registro
                if (param == null){
                    LOGGER.error("Creaci�n no realizada");
                    getSession().warn("No pudimos crear el parametro.");
                    actualizaTarget(target);
                    return;
                }
                
                // enumeramos 
                param = paramDao.getSiguienteParametro(param, parametroPadre);

                // persistimos
                paramDao.persist(param);
                getSession().info("El parametro ["+param+"] fue creado.");

                actualizaTarget(target);
            }
            
        }.setClassAttribute("btn btn-primary")
        .setIconAttribute("icon-play-circle icon-white")
        .setLabel(Model.of("Crear"))
        .setVisible(ModoPantalla.ALTA.equals(modoPantalla))
        );
        
        addComponent(new AjaxToolButton("modificar"){
            private static final long serialVersionUID = 1L; 

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(form);
            }
            
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                LOGGER.debug("Iniciando modificacion");
                Parametro param = parametro.getObject();

                ParametroDao paramDao = getParametroDao();
                
                // registramos 
                paramDao.merge(param);
                getSession().info("El parametro ["+param+"] fue modificado.");

                actualizaTarget(target);
            }
            
        }.setClassAttribute("btn btn-info")
        .setIconAttribute("icon-edit icon-white")
        .setLabel(Model.of("Modificar"))
        .setVisible(!ModoPantalla.ALTA.equals(modoPantalla))
        );
        
        addComponent(new AjaxToolButton("eliminar"){
            private static final long serialVersionUID = 1L; 

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(form);
            }
            
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                LOGGER.debug("Iniciando eliminacion");
                Parametro param = parametro.getObject();

                ParametroDao paramDao = getParametroDao();
                
                // existencia del registro
                if (paramDao.tieneDependencias(param)){
                    LOGGER.debug("No puedo Borrar");
                    mensajeErrorModel.setObject("El parametro ["+param+"] tiene dependencias");
                    mensajeErrorLabel.setVisible(true);
                    target.add(getFormData());
                    return;
                }
                
                // eliminamos 
                paramDao.remove(param.getId());
                getSession().info("El parametro ["+param+"] fue eliminado.");

                actualizaTarget(target);
            }
            
        }.setClassAttribute("btn btn-success")
        .setIconAttribute("icon-remove icon-white")
        .setLabel(Model.of("Borrar"))
        .setVisible(!ModoPantalla.ALTA.equals(modoPantalla))
        );
        
        addComponent(new AjaxToolButton("adicionar"){
            private static final long serialVersionUID = 1L; 

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(form);
            }
            
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                LOGGER.debug("Iniciando eliminacion");
                Parametro param = parametro.getObject();

                PageParameters params = new PageParameters();
                params.add("p_nivel", param.getNivel());
                params.add("p_padre", param.getIdPadre());
                params.add("p_id", param.getId());
                setResponsePage(ListaSubGrupoParametro.class, params);
            }
            
        }.setClassAttribute("btn btn-inverse")
        .setIconAttribute("icon-plus icon-white")
        .setLabel(Model.of("Adicionar " + parametroPadre.getEtiqueta3()))
        .setVisible(!ModoPantalla.ALTA.equals(modoPantalla) && StringUtils.isNotBlank(parametroPadre.getEtiqueta3()))
        );
   }


    
    
}
