package bus.hallazgo.ui.params;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.hallazgo.api.Modulo;
import bus.hallazgo.api.TipoGrupo;
import bus.hallazgo.dao.SubGrupoParametroDao;
import bus.hallazgo.entities.Parametro;
import bus.hallazgo.entities.SubGrupoParametro;
import bus.plumbing.components.AjaxLinkPanel;
import bus.plumbing.components.AjaxLinkPropertyColumn;
import bus.plumbing.components.AjaxToolButton;
import bus.users.api.Metadatas;
import bus.users.entities.AdmUser;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * La clase <code>ListaSubGrupoParametro.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 13/05/2014, 13:13:38
 *
 */
//@AuthorizeInstantiation({RolesBisa.INDO_ADMIN})
//@Menu(value = "Par�metros", subMenu = SubMenu.INDO_ADMINISTRACION)
public class ListaSubGrupoParametro extends Listado<SubGrupoParametro, String> {

    private static final long serialVersionUID = -7190455040593546992L;
    private ListadoPanelBandeja listadoPanelBandeja;

    private WebMarkupContainer markupModal;
    private Panel panelModal;

    private final Parametro paramForFiltro;

    public ListaSubGrupoParametro() {
        super();
        this.paramForFiltro = new Parametro();
        this.paramForFiltro.setTipo(TipoGrupo.G.toString());
        this.paramForFiltro.setModulo(Modulo.ISB);
        this.paramForFiltro.setNivel(0);
    }

    public ListaSubGrupoParametro(PageParameters parameters) {
        super(parameters);
        this.paramForFiltro = new Parametro();
        this.paramForFiltro.setTipo(TipoGrupo.S.toString());
        this.paramForFiltro.setModulo(Modulo.ISB);
        if (existParameter("p_nivel")){
            this.paramForFiltro.setNivel(getPageParameters().get("p_nivel").toInteger());
        }
        if (existParameter("p_padre")){
            this.paramForFiltro.setIdPadre(getPageParameters().get("p_padre").toString());
        }
        if (existParameter("p_id")){
            this.paramForFiltro.setId(getPageParameters().get("p_id").toString());
        }
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        markupModal = new WebMarkupContainer("ContenedorModal");
        markupModal.setOutputMarkupId(true);
        add(markupModal);
        panelModal = new EmptyPanel("modal");
        markupModal.add(panelModal);

    }

    @Override
    protected ListadoPanel<SubGrupoParametro, String> newListadoPanel(String id) {
        listadoPanelBandeja = new ListadoPanelBandeja(id);
        return listadoPanelBandeja;
    }

    public final boolean existParameter(String parameter) {
        if (getPageParameters() == null || getPageParameters().isEmpty()) {
            return false;
        }
        return !(getPageParameters().get(parameter).isNull() || getPageParameters().get(parameter).isEmpty());
    }


    /**
     *
     * La clase <code>ListaSubGrupoParametro.java</code> tiene la funcion de...
     *
     * @author David Cuevas
     * @version 1.0, 13/05/2014, 13:13:46
     *
     */
    private class ListadoPanelBandeja extends ListadoPanel<SubGrupoParametro, String>{
        private static final long serialVersionUID = 3012192182811262487L;

        public ListadoPanelBandeja(String id) {
            super(id);
        }

        @Override
        protected List<IColumn<SubGrupoParametro, String>> newColumns(ListadoDataProvider<SubGrupoParametro, String> dataProvider) {
            ArrayList<IColumn<SubGrupoParametro, String>> iColumns = new ArrayList<IColumn<SubGrupoParametro, String>>();

            iColumns.add(new AjaxLinkPropertyColumn<SubGrupoParametro>(Model.of("Id de Par�metro"), "id", "id") {

                private static final long serialVersionUID = 3496417959060241716L;

                @Override
                protected AjaxLinkPanel<SubGrupoParametro> newAjaxLinkPanel( String componentId, IModel<SubGrupoParametro> rowModel) {
                    final AjaxLinkPanel<SubGrupoParametro> components = super.newAjaxLinkPanel(componentId, rowModel);
                    components.setEscapeModelStrings(false);
                    //components.get("link").add(new AttributeAppender("class", Model.of("btn btn-success")));
                    return components;
                }

                @Override
                protected void onClick(AjaxRequestTarget target, SubGrupoParametro object) {
                    AdmUser admUser  = getSession().getMetaData(Metadatas.USER_META_DATA_KEY);

                    String idPadre = getModel1().getObject().getIdPadre();
                    ABMSubGrupoParametro component = new ABMSubGrupoParametro(panelModal.getId(), new Model<AdmUser>(admUser), idPadre, new Model<SubGrupoParametro>(object), feedbackPanel) {
                        private static final long serialVersionUID = 1L;

                        @Override
                        public void actualizaTarget(AjaxRequestTarget target) {
                            target.add(feedbackPanel);
                            target.add(ListadoPanelBandeja.this.getTable());
                            target.appendJavaScript("$('#PanelModal').modal('hide');");
                        }
                    };

                    panelModal.replaceWith(component);
                    panelModal = component;

                    target.add(markupModal);
                    target.appendJavaScript("$('#PanelModal').modal('show');");
                }
            });

            iColumns.add(new PropertyColumn<SubGrupoParametro, String>(Model.of("Descripci�n del Par�metro"), "descripcion", "descripcion"));

            return iColumns;
        }

        @Override
        protected Class<? extends Dao<SubGrupoParametro, String>> getProviderClazz() {
            return SubGrupoParametroDao.class;
        }

        @Override
        protected EstiloFiltro getEstiloFiltro() {
            return EstiloFiltro.FORMULARIO;
        }

        @Override
        protected SubGrupoParametro newObject1() {
            SubGrupoParametro subGrupoParametro = new SubGrupoParametro();
            subGrupoParametro.setModulo(Modulo.ISB);
            /*
            if (StringUtils.isNotBlank(paramForFiltro.getIdPadre())){
                subGrupoParametro.setIdPadre(paramForFiltro.getId());
            }
            */
            return subGrupoParametro;
        }

        @Override
        protected FiltroPanel<SubGrupoParametro> newFiltroPanel(String id,
                IModel<SubGrupoParametro> model1,
                IModel<SubGrupoParametro> model2) {
            return new ListaSubGrupoParametroFiltroPanel(id, model1, model2, paramForFiltro);
        }

        @Override
        protected boolean isVisibleData() {
            SubGrupoParametro subGrupo = getModel1().getObject();
            return !StringUtils.isEmpty(subGrupo.getIdPadre());
        }

        @Override
        protected void poblarBotones(RepeatingView rv, String idButton, Form<Void> listform) {
            WebMarkupContainer wmk;
            rv.add(wmk = new WebMarkupContainer(rv.newChildId()));

            FormComponent<String> botonCrear = new AjaxToolButton(idButton) {
                private static final long serialVersionUID = -9103344330910834179L;

                protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                    if (StringUtils.isEmpty(getModel1().getObject().getIdPadre())){
                        getSession().warn("Antes debe seleccionar un Grupo.");
                        target.add(feedbackPanel);
                        target.add(ListadoPanelBandeja.this.getTable());
                        return;
                    }


                    AdmUser admUser  = getSession().getMetaData(Metadatas.USER_META_DATA_KEY);

                    String idPadre = getModel1().getObject().getIdPadre();
                    ABMSubGrupoParametro component = new ABMSubGrupoParametro(panelModal.getId(), new Model<AdmUser>(admUser), idPadre, feedbackPanel) {
                        private static final long serialVersionUID = 1L;

                        @Override
                        public void actualizaTarget(AjaxRequestTarget target) {
                            target.add(feedbackPanel);
                            target.add(ListadoPanelBandeja.this.getTable());
                            target.appendJavaScript("$('#PanelModal').modal('hide');");
                        }
                    };

                    panelModal.replaceWith(component);
                    panelModal = component;

                    target.add(markupModal);
                    target.appendJavaScript("$('#PanelModal').modal('show');");
                }

            }.setClassAttribute("btn btn-info")
            .setIconAttribute("icon-file icon-white")
            .setDefaultFormProcessing(false).setLabel(Model.of("Crear nuevo Par�metro"));
            
            //botonCrear.setVisible(StringUtils.isNotEmpty(getModel1().getObject().getIdPadre()));
            
            wmk.add(botonCrear);

        }
        
    }

}
