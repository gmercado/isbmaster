package bus.hallazgo.ui.params;

import bus.database.components.Listado;
import bus.database.components.ListadoDataProvider;
import bus.database.components.ListadoPanel;
import bus.database.dao.Dao;
import bus.hallazgo.dao.GrupoParametroDao;
import bus.hallazgo.entities.GrupoParametro;
import bus.plumbing.components.*;
import bus.users.api.Metadatas;
import bus.users.entities.AdmUser;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * La clase <code>Listado.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 12/05/2014, 14:07:07
 *
 */
//@AuthorizeInstantiation({RolesBisa.INDO_ADMIN})
//@Menu(value = "Grupo de Par�metros", subMenu = SubMenu.INDO_ADMINISTRACION)
public class ListaGrupoParametro extends Listado<GrupoParametro, String> {

    private static final long serialVersionUID = -7190455040593546992L;
    private ListadoPanelBandeja listadoPanelBandeja;

    private WebMarkupContainer markupModal;
    private Panel panelModal;

    @Override
    protected void onInitialize() {
        super.onInitialize();

        markupModal = new WebMarkupContainer("ContenedorModal");
        markupModal.setOutputMarkupId(true);
        add(markupModal);
        panelModal = new EmptyPanel("modal");
        markupModal.add(panelModal);

    }

    @Override
    protected ListadoPanel<GrupoParametro, String> newListadoPanel(String id) {
        listadoPanelBandeja = new ListadoPanelBandeja(id);
        return listadoPanelBandeja;
    }


    /**
     * Clase interna
     *
     * La clase <code>GrupoParametros.java</code> tiene la funcion de...
     *
     * @author David Cuevas
     * @version 1.0, 12/05/2014, 19:03:55
     *
     */
    private class ListadoPanelBandeja extends ListadoPanel<GrupoParametro, String>{
        private static final long serialVersionUID = 3012192182811262487L;

        public ListadoPanelBandeja(String id) {
            super(id);
        }

        @Override
        protected List<IColumn<GrupoParametro, String>> newColumns(ListadoDataProvider<GrupoParametro, String> dataProvider) {
            ArrayList<IColumn<GrupoParametro, String>> iColumns = new ArrayList<IColumn<GrupoParametro, String>>();

            iColumns.add(new AjaxLinkPropertyColumn<GrupoParametro>(Model.of("Id de Grupo"), "id", "id") {

                private static final long serialVersionUID = 3496417959060241716L;

                @Override
                protected AjaxLinkPanel<GrupoParametro> newAjaxLinkPanel( String componentId, IModel<GrupoParametro> rowModel) {
                    final AjaxLinkPanel<GrupoParametro> components = super.newAjaxLinkPanel(componentId, rowModel);
                    components.setEscapeModelStrings(false);
                    //components.get("link").add(new AttributeAppender("class", Model.of("btn btn-success")));
                    return components;
                }

                @Override
                protected IModel<?> createLabelModel(IModel<GrupoParametro> rowModel) {
                    final IModel<String> labelModel = (IModel<String>) super.createLabelModel(rowModel);
                    return new HighlightModel(labelModel, getBuscarModel());
                }

                @Override
                protected void onClick(AjaxRequestTarget target, GrupoParametro object) {
                    AdmUser admUser  = getSession().getMetaData(Metadatas.USER_META_DATA_KEY);

                    ABMGrupoParametro component = new ABMGrupoParametro(panelModal.getId(), new Model<AdmUser>(admUser), new Model<GrupoParametro>(object), feedbackPanel) {
                        private static final long serialVersionUID = 1L;

                        @Override
                        public void actualizaTarget(AjaxRequestTarget target) {
                            target.add(feedbackPanel);
                            target.add(ListadoPanelBandeja.this.getTable());
                            target.appendJavaScript("$('#PanelModal').modal('hide');");
                        }
                    };

                    panelModal.replaceWith(component);
                    panelModal = component;

                    target.add(markupModal);
                    target.appendJavaScript("$('#PanelModal').modal('show');");
                }
            });

            iColumns.add(new AbstractColumn<GrupoParametro, String>(Model.of("Descripci�n del Grupo"), "descripcion") {

                private static final long serialVersionUID = -1292555527962959401L;

                @Override
                public void populateItem(Item<ICellPopulator<GrupoParametro>> item, String componentId, IModel<GrupoParametro> model) {
                    item.add(new Label(componentId,
                            new EnMedioModel(new PropertyModel<String>(model, "descripcion"), getBuscarModel())).
                            add(new HighlightBehaviour(getBuscarModel())));
                }
            });
            
            iColumns.add(new PropertyColumn<GrupoParametro, String>(Model.of("Etiqueta 1"), "etiqueta1", "etiqueta1"));
            iColumns.add(new PropertyColumn<GrupoParametro, String>(Model.of("Etiqueta 2"), "etiqueta2", "etiqueta2"));
            iColumns.add(new PropertyColumn<GrupoParametro, String>(Model.of("Etiqueta 3"), "etiqueta3", "etiqueta3"));
            
            /*  variante para utilizar solo AbstractColumn en lugar de AjaxLinkPropertyColumn
            iColumns.add(new AbstractColumn<Param, String>(Model.of("Id de Grupo"), "id") {
                private static final long serialVersionUID = 3811973271281723659L;

                @Override
                public void populateItem(Item<ICellPopulator<Param>> cellItem, String componentId, final IModel<Param> rowModel) {
                    // LinkPanel<Param> linkPanel = new LinkPanel<Param>(componentId, Model.of("Modificar"), rowModel){
                    LinkPanel<Param> linkPanel = new LinkPanel<Param>(componentId, Model.of(rowModel.getObject().getId()), rowModel){
                        private static final long serialVersionUID = 6716473220738459749L;
                        @Override
                        protected AbstractLink newLink(String linkId, IModel<Param> objectModel) {
                            return new IndicatingAjaxLink<Param>(linkId, objectModel) {
                                private static final long serialVersionUID = -8175988298199837329L;
                                
                                @Override
                                protected void onInitialize() {
                                    super.onInitialize();
                                }

                                @Override
                                public void onClick(AjaxRequestTarget target) {
                                    AdmUser admUser  = getSession().getMetaData(Metadatas.USER_META_DATA_KEY);

                                    GrupoParametrosABM component = new GrupoParametrosABM(panelModal.getId(), new Model<AdmUser>(admUser), rowModel, feedbackPanel) {
                                        private static final long serialVersionUID = 1L;

                                        @Override
                                        public void actualizaTarget(AjaxRequestTarget target) {
                                            target.add(feedbackPanel);
                                            target.add(ListadoPanelBandeja.this.getTable());
                                            target.appendJavaScript("$('#PanelModal').modal('hide');");
                                        }
                                    };
                                    
                                    panelModal.replaceWith(component);
                                    panelModal = component;
                                    
                                    target.add(markupModal);
                                    target.appendJavaScript("$('#PanelModal').modal('show');");
                                }
                            };
                        }
                        
                    };
                    //linkPanel.get("link").add(new AttributeAppender("class", Model.of("btn btn-success")));
                    cellItem.add(linkPanel);
                }
                
            });
            */
            
            return iColumns;
        }

        @Override
        protected Class<? extends Dao<GrupoParametro, String>> getProviderClazz() {
            return GrupoParametroDao.class;
        }
        
        
        @Override
        protected void poblarBotones(RepeatingView rv, String idButton, Form<Void> listform) {
            WebMarkupContainer wmk;
            rv.add(wmk = new WebMarkupContainer(rv.newChildId()));
            
            wmk.add(new AjaxToolButton(idButton) {
                private static final long serialVersionUID = -9103344330910834179L;

                protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                    AdmUser admUser  = getSession().getMetaData(Metadatas.USER_META_DATA_KEY);

                    ABMGrupoParametro component = new ABMGrupoParametro(panelModal.getId(), new Model<AdmUser>(admUser), feedbackPanel) {
                        private static final long serialVersionUID = 1L;

                        @Override
                        public void actualizaTarget(AjaxRequestTarget target) {
                            target.add(feedbackPanel);
                            target.add(ListadoPanelBandeja.this.getTable());
                            target.appendJavaScript("$('#PanelModal').modal('hide');");
                        }
                    };
                    
                    panelModal.replaceWith(component);
                    panelModal = component;
                    
                    target.add(markupModal);
                    target.appendJavaScript("$('#PanelModal').modal('show');");
                }
                
            }.setClassAttribute("btn btn-info")
            .setIconAttribute("icon-file icon-white")
            .setDefaultFormProcessing(false).setLabel(Model.of("Crear nuevo Grupo")));

        }
        
    }

}
