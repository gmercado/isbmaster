package bus.hallazgo.ui.params;

import bus.hallazgo.api.BisaWebPagePanel;
import bus.hallazgo.api.ModoPantalla;
import bus.hallazgo.api.Modulo;
import bus.hallazgo.dao.ParametroDao;
import bus.hallazgo.entities.GrupoParametro;
import bus.hallazgo.entities.Parametro;
import bus.plumbing.components.AjaxToolButton;
import bus.plumbing.components.AlertFeedbackPanel;
import bus.users.entities.AdmUser;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.validation.validator.StringValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * La clase <code>ABMGrupoParametro.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 14/05/2014, 12:55:41
 *
 */
public abstract class ABMGrupoParametro extends BisaWebPagePanel<GrupoParametro, AdmUser>{
    
    private static final long serialVersionUID = -5978248150995624818L;
    private static final Logger LOGGER = LoggerFactory.getLogger(ABMGrupoParametro.class);
    
    private final IModel<GrupoParametro> parametro;
    
    private final String titulo;
    private final ModoPantalla modoPantalla;
    
    private IModel<String> mensajeErrorModel = Model.of("");
    private Component mensajeErrorLabel = null;
    
    public ABMGrupoParametro(String id, IModel<AdmUser> modelUser, AlertFeedbackPanel alertFeedbackPanel) {
        super(id, modelUser, alertFeedbackPanel);
        this.parametro = new Model<GrupoParametro>();
        this.parametro.setObject(new GrupoParametro());
        this.titulo = "Nuevo Grupo";
        this.modoPantalla = ModoPantalla.ALTA;
    }

    public ABMGrupoParametro(String id, IModel<AdmUser> modelUser, IModel<GrupoParametro> modelParam, AlertFeedbackPanel alertFeedbackPanel) {
        super(id, modelUser, alertFeedbackPanel);
        this.parametro = modelParam;
        this.titulo = "Modificar Grupo";
        this.modoPantalla = ModoPantalla.MOD_BAJA;
    }

    @Override
    protected IModel<GrupoParametro> getTypedModel() {
        return parametro;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize(); 
        addComponent(new Label("_titulo", titulo));
        
        mensajeErrorLabel = new Label("mensajeError", mensajeErrorModel).setVisible(false);
        
        addComponent(mensajeErrorLabel);
        
        addComponentConBorde(new TextField<String>("id")
                .add(StringValidator.maximumLength(2))
                .setRequired(true)
                .setOutputMarkupId(true)
                .setVisible(true)
                .setEnabled(ModoPantalla.ALTA.equals(modoPantalla)));

        addComponentConBorde(new TextField<String>("descripcion")
                .add(StringValidator.maximumLength(100))
                .setRequired(true)
                .setOutputMarkupId(true).setVisible(true), 8);
        
        addComponentConBorde(new TextField<String>("etiqueta1")
                .add(StringValidator.maximumLength(20))
                .setRequired(false)
                .setOutputMarkupId(true).setVisible(true));
        
        addComponentConBorde(new TextField<String>("etiqueta2")
                .add(StringValidator.maximumLength(20))
                .setRequired(false)
                .setOutputMarkupId(true).setVisible(true));
        
        addComponentConBorde(new TextField<String>("etiqueta3")
                .add(StringValidator.maximumLength(20))
                .setRequired(false)
                .setOutputMarkupId(true).setVisible(true));
        
        addComponent(new AjaxToolButton("crear"){
            private static final long serialVersionUID = 1L; 

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(form);
            }
            
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                LOGGER.debug("Iniciando creacion");
                Parametro param = parametro.getObject();

                ParametroDao paramDao = getParametroDao();
                
                // consistencia del registro
                if (param == null){
                    LOGGER.error("Creaci�n no realizada");
                    getSession().warn("No pudimos crear el grupo.");
                    actualizaTarget(target);
                    return;
                }

                // existencia del registro
                Parametro encontrado = paramDao.find(param.getId());
                if (encontrado != null){
                    LOGGER.debug("Creaci�n no realizada");
                    mensajeErrorModel.setObject("El parametro ["+encontrado.getId()+"] ya existe con descripci�n ["+encontrado.getDescripcion()+"] ");
                    mensajeErrorLabel.setVisible(true);
                    target.add(getFormData());
                    return;
                }
                
                // Registramos y marcamos el parametro como parte del sistema AQUA2-ISB
                // El nivel del grupo es siempre 0 (CERO)
                param.setModulo(Modulo.ISB);
                param.setNivel(0);
                paramDao.persist(param);
                getSession().info("El parametro ["+param+"] fue creado.");

                actualizaTarget(target);
            }
            
        }.setClassAttribute("btn btn-primary")
        .setIconAttribute("icon-play-circle icon-white")
        .setLabel(Model.of("Crear"))
        .setVisible(ModoPantalla.ALTA.equals(modoPantalla))
        );
        
        addComponent(new AjaxToolButton("modificar"){
            private static final long serialVersionUID = 1L; 

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(form);
            }
            
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                LOGGER.debug("Iniciando modificacion");
                Parametro param = parametro.getObject();

                ParametroDao paramDao = getParametroDao();
                
                // registramos 
                paramDao.merge(param);
                getSession().info("El parametro ["+param+"] fue modificado.");

                actualizaTarget(target);
            }
            
        }.setClassAttribute("btn btn-info")
        .setIconAttribute("icon-edit icon-white")
        .setLabel(Model.of("Modificar"))
        .setVisible(!ModoPantalla.ALTA.equals(modoPantalla))
        );
        
        addComponent(new AjaxToolButton("eliminar"){
            private static final long serialVersionUID = 1L; 

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(form);
            }
            
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                LOGGER.debug("Iniciando eliminacion");
                Parametro param = parametro.getObject();

                ParametroDao paramDao = getParametroDao();
                
                // existencia del registro
                if (paramDao.tieneDependencias(param)){
                    LOGGER.debug("No puedo Borrar");
                    mensajeErrorModel.setObject("El parametro ["+param+"] tiene dependencias");
                    mensajeErrorLabel.setVisible(true);
                    target.add(getFormData());
                    return;
                }
                
                // eliminamos 
                paramDao.remove(param.getId());
                getSession().info("El parametro ["+param+"] fue eliminado.");

                actualizaTarget(target);
            }
            
        }.setClassAttribute("btn btn-success")
        .setIconAttribute("icon-remove icon-white")
        .setLabel(Model.of("Borrar"))
        .setVisible(!ModoPantalla.ALTA.equals(modoPantalla))
        );
        
   }


    
    
}
