package bus.hallazgo.ui.params;

import bus.database.components.FiltroPanel;
import bus.hallazgo.dao.ParametroDao;
import bus.hallazgo.entities.Parametro;
import bus.hallazgo.entities.SubGrupoParametro;
import com.google.inject.Inject;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * La clase <code>ListaSubGrupoParametroFiltroPanel.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 13/05/2014, 13:25:41
 */
public class ListaSubGrupoParametroFiltroPanel extends FiltroPanel<SubGrupoParametro> {
    private static final long serialVersionUID = 7965096479016310416L;

    @Inject
    private ParametroDao parametroDao;

    private final HashMap<String, Parametro> tipos = new HashMap<String, Parametro>();

    protected ListaSubGrupoParametroFiltroPanel(String id,
                                                IModel<SubGrupoParametro> model1,
                                                IModel<SubGrupoParametro> model2,
                                                final Parametro paramForFiltro
    ) {
        super(id, model1, model2);

        add(new DropDownChoice<String>("grupo", new PropertyModel<String>(model1, "idPadre"),
                new AbstractReadOnlyModel<List<? extends String>>() {

                    private static final long serialVersionUID = -2877014085503079783L;

                    @Override
                    public List<? extends String> getObject() {

                        List<Parametro> todos = parametroDao.getTodos(paramForFiltro, null, -1, -1);
                        ArrayList<String> nombres = new ArrayList<String>(todos.size());
                        for (Parametro tt : todos) {
                            tipos.put(tt.getId(), tt);
                            nombres.add(tt.getId());
                        }
                        return nombres;
                    }
                }, new IChoiceRenderer<String>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Object getDisplayValue(String object) {
                Parametro parametro = tipos.get(object);
                return parametro.getId() + " - " + parametro.getDescripcion();
            }

            @Override
            public String getIdValue(String object, int index) {
                return String.valueOf(index);
            }

            @Override
            public String getObject(String id, IModel<? extends List<? extends String>> choices) {
                throw new UnsupportedOperationException("Method not supported");
            }

        }

        ) {
            private static final long serialVersionUID = 1L;

            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }
        }.setNullValid(false).setOutputMarkupId(true));

    }

}
