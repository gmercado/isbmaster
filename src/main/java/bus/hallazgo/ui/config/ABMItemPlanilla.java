package bus.hallazgo.ui.config;

import bus.hallazgo.api.BisaWebPagePanel;
import bus.hallazgo.api.EstadoItemPlanilla;
import bus.hallazgo.api.ModoPantalla;
import bus.hallazgo.dao.ItemPlanillaDao;
import bus.hallazgo.entities.ItemPlanilla;
import bus.hallazgo.entities.SubGrupoParametro;
import bus.plumbing.components.AjaxToolButton;
import bus.plumbing.components.AlertFeedbackPanel;
import bus.users.entities.AdmUser;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 
 * La clase <code>ABMItemPlanilla.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 14/05/2014, 12:55:35
 *
 */
public abstract class ABMItemPlanilla extends BisaWebPagePanel<ItemPlanilla, AdmUser>{
    
    private static final long serialVersionUID = -5978248150995624818L;
    private static final Logger LOGGER = LoggerFactory.getLogger(ABMItemPlanilla.class);
    
    private final IModel<ItemPlanilla> itemPlanilla;
    
    private final String titulo;
    private final ModoPantalla modoPantalla;
    
    private IModel<String> mensajeErrorModel = Model.of("");
    private Component mensajeErrorLabel = null;
    
    private static final Integer NACIONAL=9;
    
    public ABMItemPlanilla(String id, IModel<AdmUser> modelUser, SubGrupoParametro transaccion, AlertFeedbackPanel alertFeedbackPanel) {
        super(id, modelUser, alertFeedbackPanel);
        this.itemPlanilla = new Model<ItemPlanilla>();
        this.itemPlanilla.setObject(new ItemPlanilla());
        this.itemPlanilla.getObject().setTransaccion(transaccion);
        this.itemPlanilla.getObject().setSucursal(NACIONAL);
        this.itemPlanilla.getObject().setAgencia(null);
        this.titulo = "Adici�n de procesos a la Planilla";
        this.modoPantalla = ModoPantalla.ALTA;
    }

    public ABMItemPlanilla(String id, IModel<AdmUser> modelUser, IModel<ItemPlanilla> modelItem, AlertFeedbackPanel alertFeedbackPanel) {
        super(id, modelUser, alertFeedbackPanel);
        this.itemPlanilla = modelItem;
        this.titulo = "Modificar procesos de la Planilla";
        this.modoPantalla = ModoPantalla.MOD_BAJA;
    }

    @Override
    protected IModel<ItemPlanilla> getTypedModel() {
        return itemPlanilla;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        addComponent(new Label("_titulo", titulo));

        mensajeErrorLabel = new Label("mensajeError", mensajeErrorModel).setVisible(false);

        addComponent(mensajeErrorLabel);

        addComponentConBorde(new TextField<Integer>("orden")
                .setRequired(true)
                .setOutputMarkupId(true)
                .setVisible(true), 12);

        addComponentConBorde(new DropDownChoice<SubGrupoParametro>("proceso",
                new ArrayList<SubGrupoParametro>( getSubGrupoParametroDao().getListaProcesos()),
                new ChoiceRenderer<SubGrupoParametro>("descripcionCompleta")
                ).setNullValid(false)
                .setRequired(true)
                .setOutputMarkupId(true)
                .setVisible(true), 12);

        addComponentConBorde(new DropDownChoice<EstadoItemPlanilla>("estado",
                new ArrayList<EstadoItemPlanilla>( Arrays.asList(EstadoItemPlanilla.values()) ),
                new ChoiceRenderer<EstadoItemPlanilla>("descripcion")
                ).setNullValid(false)
                .setRequired(true)
                .setOutputMarkupId(true)
                .setVisible(true), 12);


        addComponent(new AjaxToolButton("crear"){
            private static final long serialVersionUID = 1L;

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(form);
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                LOGGER.debug("Iniciando creacion");
                ItemPlanilla item = itemPlanilla.getObject();

                ItemPlanillaDao itemPlanillaDao = getItemPlanillaDao();

                // consistencia del registro
                if (item == null){
                    LOGGER.error("Creaci�n no realizada");
                    getSession().warn("No pudimos crear el item de la planilla.");
                    actualizaTarget(target);
                    return;
                }

                // existencia del registro
                ItemPlanilla encontrado = itemPlanillaDao.getItemSimilar(item);
                if (encontrado != null){
                    LOGGER.debug("Creaci�n no realizada");
                    mensajeErrorModel.setObject("Ya se tiene un registro similar en estado ["+
                            encontrado.getEstado().getDescripcion()+
                            "] con las mismas caracteristicas.");
                    mensajeErrorLabel.setVisible(true);
                    target.add(getFormData());
                    return;
                }

                // Registramos
                item.setTransaccion(getSubGrupoParametroDao().find(item.getTransaccion().getId()));
                item.setProceso(getSubGrupoParametroDao().find(item.getProceso().getId()));

                itemPlanillaDao.persist(item);

                getSession().info("El proceso ["+item.getProceso()+
                        "] fue registrado para la transacci�n ["+ item.getTransaccion()+
                        "] con id ["+item.getId()+"]");

                actualizaTarget(target);
            }

        }.setClassAttribute("btn btn-primary")
        .setIconAttribute("icon-play-circle icon-white")
        .setLabel(Model.of("Crear"))
        .setVisible(ModoPantalla.ALTA.equals(modoPantalla))
        );

        addComponent(new AjaxToolButton("modificar"){
            private static final long serialVersionUID = 1L;

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(form);
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {

                LOGGER.debug("Iniciando modificacion");
                ItemPlanilla item = itemPlanilla.getObject();
                ItemPlanillaDao itemPlanillaDao = getItemPlanillaDao();

                // existencia del registro
                ItemPlanilla encontrado = itemPlanillaDao.getItemSimilar(item);
                if (encontrado != null && !encontrado.getId().equals(item.getId())){
                    LOGGER.debug("Modificaci�n no realizada");
                    mensajeErrorModel.setObject("Ya se tiene un registro similar en estado ["+encontrado.getEstado().getDescripcion()+
                            "] con las caracteristicas que se desea modificar.");
                    mensajeErrorLabel.setVisible(true);
                    target.add(getFormData());
                    return;
                }

                // registramos
                itemPlanillaDao.merge(item);
                getSession().info("El id ["+item.getId()+"] fue actualizado.");

                actualizaTarget(target);

            }

        }.setClassAttribute("btn btn-info")
        .setIconAttribute("icon-edit icon-white")
        .setLabel(Model.of("Modificar"))
        .setVisible(!ModoPantalla.ALTA.equals(modoPantalla))
        );

        addComponent(new AjaxToolButton("eliminar"){
            private static final long serialVersionUID = 1L;

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(form);
            }

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {

                LOGGER.debug("Iniciando eliminacion");
                ItemPlanilla item = itemPlanilla.getObject();
                ItemPlanillaDao itemPlanillaDao = getItemPlanillaDao();

                // existencia del registro
                // TODO: incorporar validacion sobre dependencias
                /*
                if (paramDao.tieneDependencias(param)){
                    return;
                }*/

                // eliminamos
                itemPlanillaDao.remove(item.getId());
                getSession().info("El registro con id ["+item.getId()+"] fue eliminado para el proceso ["+item.getProceso()+
                        "] de la transacci�n ["+ item.getTransaccion()+ "]");

                actualizaTarget(target);

            }
            
        }.setClassAttribute("btn btn-success")
        .setIconAttribute("icon-remove icon-white")
        .setLabel(Model.of("Borrar"))
        .setVisible(!ModoPantalla.ALTA.equals(modoPantalla))
        );
        
   }

}
