package bus.hallazgo.ui.config;

import bus.database.components.*;
import bus.database.dao.Dao;
import bus.hallazgo.dao.ItemPlanillaDao;
import bus.hallazgo.entities.ItemPlanilla;
import bus.hallazgo.entities.SubGrupoParametro;
import bus.plumbing.components.AjaxLinkPanel;
import bus.plumbing.components.AjaxLinkPropertyColumn;
import bus.plumbing.components.AjaxToolButton;
import bus.users.api.Metadatas;
import bus.users.entities.AdmUser;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * La clase <code>ListaPlanilla.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 14/05/2014, 11:19:35
 *
 */
//@AuthorizeInstantiation({RolesBisa.INDO_ADMIN})
//@Menu(value = "Planilla de Procesos por Transacci�n", subMenu = SubMenu.INDO_ADMINISTRACION)
public class ListaPlanilla extends Listado<ItemPlanilla, Long> {

    private static final long serialVersionUID = -7190455040593546992L;
    private ListadoPanelBandeja listadoPanelBandeja;

    private WebMarkupContainer markupModal;
    private Panel panelModal;

    @Override
    protected void onInitialize() {
        super.onInitialize();

        markupModal = new WebMarkupContainer("ContenedorModal");
        markupModal.setOutputMarkupId(true);
        add(markupModal);
        panelModal = new EmptyPanel("modal");
        markupModal.add(panelModal);

    }

    @Override
    protected ListadoPanel<ItemPlanilla, Long> newListadoPanel(String id) {
        listadoPanelBandeja = new ListadoPanelBandeja(id);
        return listadoPanelBandeja;
    }

    public final boolean existParameter(String parameter) {
        if (getPageParameters() == null || getPageParameters().isEmpty()) {
            return false;
        }
        return !(getPageParameters().get(parameter).isNull() || getPageParameters().get(parameter).isEmpty());
    }



    /**
     *
     * La clase <code>ListaPlanilla.java</code> tiene la funcion de...
     *
     * @author David Cuevas
     * @version 1.0, 14/05/2014, 11:38:20
     *
     */
    private class ListadoPanelBandeja extends ListadoPanel<ItemPlanilla, Long> {
        private static final long serialVersionUID = 3012192182811262487L;

        public ListadoPanelBandeja(String id) {
            super(id);
        }

        @Override
        protected List<IColumn<ItemPlanilla, String>> newColumns(ListadoDataProvider<ItemPlanilla, Long> dataProvider) {
            ArrayList<IColumn<ItemPlanilla, String>> iColumns = new ArrayList<IColumn<ItemPlanilla, String>>();

            iColumns.add(new AjaxLinkPropertyColumn<ItemPlanilla>(Model.of("Id"), "id", "id") {

                private static final long serialVersionUID = 3496417959060241716L;

                @Override
                protected AjaxLinkPanel<ItemPlanilla> newAjaxLinkPanel( String componentId, IModel<ItemPlanilla> rowModel) {
                    final AjaxLinkPanel<ItemPlanilla> components = super.newAjaxLinkPanel(componentId, rowModel);
                    components.setEscapeModelStrings(false);
                    return components;
                }

                @Override
                protected void onClick(AjaxRequestTarget target, ItemPlanilla object) {
                    AdmUser admUser  = getSession().getMetaData(Metadatas.USER_META_DATA_KEY);

                    ABMItemPlanilla component = new ABMItemPlanilla(panelModal.getId(), new Model<AdmUser>(admUser), new Model<ItemPlanilla>(object), feedbackPanel) {
                        private static final long serialVersionUID = 1L;

                        @Override
                        public void actualizaTarget(AjaxRequestTarget target) {
                            target.add(feedbackPanel);
                            target.add(ListadoPanelBandeja.this.getTable());
                            target.appendJavaScript("$('#PanelModal').modal('hide');");
                        }
                    };

                    panelModal.replaceWith(component);
                    panelModal = component;

                    target.add(markupModal);
                    target.appendJavaScript("$('#PanelModal').modal('show');");
                }
            });



            iColumns.add(new PropertyColumn<ItemPlanilla, String>(Model.of("Orden"), "orden", "orden"));
            iColumns.add(new PropertyColumn<ItemPlanilla, String>(Model.of("Proceso"), "proceso.id", "proceso.descripcionCompleta"));
            iColumns.add(new PropertyColumn<ItemPlanilla, String>(Model.of("Estado"), "estado", "estado.descripcion"));

            return iColumns;
        }

        @Override
        protected Class<? extends Dao<ItemPlanilla, Long>> getProviderClazz() {
            return ItemPlanillaDao.class;
        }

        @Override
        protected EstiloFiltro getEstiloFiltro() {
            return EstiloFiltro.FORMULARIO;
        }

        @Override
        protected ItemPlanilla newObject1() {
            ItemPlanilla subGrupoParametro = new ItemPlanilla();
            return subGrupoParametro;
        }

        @Override
        protected FiltroPanel<ItemPlanilla> newFiltroPanel(String id,
                IModel<ItemPlanilla> model1,
                IModel<ItemPlanilla> model2) {
            return new ListaPlanillaFiltroPanel(id, model1, model2);
        }

        @Override
        protected boolean isVisibleData() {
            ItemPlanilla itemPlanilla = getModel1().getObject();
            return itemPlanilla.getTransaccion()!=null;
        }

        @Override
        protected void poblarBotones(RepeatingView rv, String idButton, Form<Void> listform) {
            WebMarkupContainer wmk;
            rv.add(wmk = new WebMarkupContainer(rv.newChildId()));

            FormComponent<String> botonCrear = new AjaxToolButton(idButton) {
                private static final long serialVersionUID = -9103344330910834179L;

                protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                    SubGrupoParametro transaccion = getModel1().getObject().getTransaccion();
                    if (transaccion == null){
                        getSession().warn("Antes debe seleccionar una Transacci�n.");
                        target.add(feedbackPanel);
                        target.add(ListadoPanelBandeja.this.getTable());
                        return;
                    }
                    
                    
                    AdmUser admUser  = getSession().getMetaData(Metadatas.USER_META_DATA_KEY);

                    ABMItemPlanilla component = new ABMItemPlanilla(panelModal.getId(), new Model<AdmUser>(admUser), transaccion, feedbackPanel) {
                        private static final long serialVersionUID = 1L;

                        @Override
                        public void actualizaTarget(AjaxRequestTarget target) {
                            target.add(feedbackPanel);
                            target.add(ListadoPanelBandeja.this.getTable());
                            target.appendJavaScript("$('#PanelModal').modal('hide');");
                        }
                    };
                    
                    panelModal.replaceWith(component);
                    panelModal = component;
                    
                    target.add(markupModal);
                    target.appendJavaScript("$('#PanelModal').modal('show');");
                }
                
            }.setClassAttribute("btn btn-info")
            .setIconAttribute("icon-file icon-white")
            .setDefaultFormProcessing(false).setLabel(Model.of("Crear nuevo Item de la Planilla"));
            
            //botonCrear.setVisible(StringUtils.isNotEmpty(getModel1().getObject().getIdPadre()));
            
            wmk.add(botonCrear);

        }
        
    }

}
