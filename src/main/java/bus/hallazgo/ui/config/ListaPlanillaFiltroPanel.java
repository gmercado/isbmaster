package bus.hallazgo.ui.config;

import bus.database.components.FiltroPanel;
import bus.hallazgo.dao.SubGrupoParametroDao;
import bus.hallazgo.entities.ItemPlanilla;
import bus.hallazgo.entities.SubGrupoParametro;
import com.google.inject.Inject;
import org.apache.wicket.markup.html.form.AbstractSingleSelectChoice;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import java.util.ArrayList;


/**
 * 
 * La clase <code>ListaPlantillaFiltroPanel.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 14/05/2014, 11:39:36
 *
 */
public class ListaPlanillaFiltroPanel extends FiltroPanel<ItemPlanilla> {
    private static final long serialVersionUID = 7965096479016310416L;

    @Inject
    private SubGrupoParametroDao subGrupoParametroDao;
    
    protected ListaPlanillaFiltroPanel(String id,
            IModel<ItemPlanilla> model1,
            IModel<ItemPlanilla> model2
            ) {
        super(id, model1, model2);

        AbstractSingleSelectChoice<SubGrupoParametro> transaccion = new DropDownChoice<SubGrupoParametro>("transaccionW",
                new PropertyModel<SubGrupoParametro>(model1, "transaccion"),
                new ArrayList<SubGrupoParametro>( subGrupoParametroDao.getListaTransacciones()),
                new ChoiceRenderer<SubGrupoParametro>("descripcionCompleta")
                ){

            private static final long serialVersionUID = 1L;

            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }
            
        }.setNullValid(false);

        add(transaccion);
    }

}
