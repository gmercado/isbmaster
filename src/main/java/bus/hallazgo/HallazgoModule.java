package bus.hallazgo;

import bus.plumbing.api.AbstractModuleBisa;

/**
 * 
 * La clase <code>HallazgoModule.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 12/05/2014, 14:02:41
 *
 */
public class HallazgoModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bindUI("bus/hallazgo/ui");
    }
}
