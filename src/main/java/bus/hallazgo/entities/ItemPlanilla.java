package bus.hallazgo.entities;

import bus.hallazgo.api.EstadoItemPlanilla;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 
 * La clase <code>ItemPlanilla.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 14/05/2014, 11:01:23
 *
 */
@Entity
@Table(name="ISBP52")
public class ItemPlanilla implements Serializable {

    private static final long serialVersionUID = 6531360871207936507L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="I52NIDPLA" )
    private Long id;
    
    @Column(name = "I52NSUC")
    private Integer sucursal;
    
    @Column(name = "I52NAGE")
    private Integer agencia;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "I52IND51T")
    private SubGrupoParametro transaccion;
    
    @Column(name = "I52NORD")
    private Integer orden;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "I52IND51P")
    private SubGrupoParametro proceso;
    
    @Column(name = "I52CEST", length = 3)
    @Enumerated(EnumType.STRING)
    private EstadoItemPlanilla estado;

    @Version
    @Column(name = "I52NVER")
    private Integer version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSucursal() {
        return sucursal;
    }

    public void setSucursal(Integer sucursal) {
        this.sucursal = sucursal;
    }

    public Integer getAgencia() {
        return agencia;
    }

    public void setAgencia(Integer agencia) {
        this.agencia = agencia;
    }

    public SubGrupoParametro getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(SubGrupoParametro transaccion) {
        this.transaccion = transaccion;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public SubGrupoParametro getProceso() {
        return proceso;
    }

    public void setProceso(SubGrupoParametro proceso) {
        this.proceso = proceso;
    }

    public EstadoItemPlanilla getEstado() {
        return estado;
    }

    public void setEstado(EstadoItemPlanilla estado) {
        this.estado = estado;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

}
