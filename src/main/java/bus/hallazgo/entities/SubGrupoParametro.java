package bus.hallazgo.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("S")
public class SubGrupoParametro extends Parametro{

    private static final long serialVersionUID = 1L;

    public String getDescripcionCompleta(){
        return this.toString();
    }
    
}
