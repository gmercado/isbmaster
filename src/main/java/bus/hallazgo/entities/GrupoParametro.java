package bus.hallazgo.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("G")
public class GrupoParametro extends Parametro{

    private static final long serialVersionUID = 1L;

}
