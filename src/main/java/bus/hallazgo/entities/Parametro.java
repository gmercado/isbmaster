package bus.hallazgo.entities;

import bus.hallazgo.api.Modulo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 
 * La clase <code>Param.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 12/05/2014, 14:02:18
 *
 */
@Entity
@Table(name="ISBP51")
@DiscriminatorColumn(name = "I51CTIPO", discriminatorType = DiscriminatorType.STRING, length = 1)
public class Parametro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="I51VIDPAR" )
    private String id;
    
    @Column(name = "I51VDESC", length = 100)
    private String descripcion;
    
    @Column(name = "I51VPADRE", length = 50)
    private String idPadre;
    
    @Column(name = "I51CTIPO", length = 1, insertable = false, updatable = false)
    private String tipo;

    @Column(name = "I51NNIVEL")
    private Integer nivel;

    @Column(name = "I51NSEC")
    private Integer secuencia;

    @Enumerated(EnumType.STRING)
    @Column(name = "I51VMOD", length = 10)
    private Modulo modulo;

    @Column(name = "I51VVAL1", length = 20)
    private String valor1;

    @Lob()
    @Column(name = "I51VVAL2", length = 6144)
    private String valor2;

    @Column(name = "I51VETI1", length = 20)
    private String etiqueta1;
    
    @Column(name = "I51VETI2", length = 20)
    private String etiqueta2;
    
    @Column(name = "I51VETI3", length = 20)
    private String etiqueta3;

    @Version
    @Column(name = "I51NVER")
    private Integer version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(String idPadre) {
        this.idPadre = idPadre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public Integer getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Integer secuencia) {
        this.secuencia = secuencia;
    }

    public Modulo getModulo() {
        return modulo;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }

    public String getValor1() {
        return valor1;
    }

    public void setValor1(String valor1) {
        this.valor1 = valor1;
    }

    public String getValor2() {
        return valor2;
    }

    public void setValor2(String valor2) {
        this.valor2 = valor2;
    }

    public String getEtiqueta1() {
        return etiqueta1;
    }

    public void setEtiqueta1(String etiqueta1) {
        this.etiqueta1 = etiqueta1;
    }

    public String getEtiqueta2() {
        return etiqueta2;
    }

    public void setEtiqueta2(String etiqueta2) {
        this.etiqueta2 = etiqueta2;
    }

    public String getEtiqueta3() {
        return etiqueta3;
    }

    public void setEtiqueta3(String etiqueta3) {
        this.etiqueta3 = etiqueta3;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Parametro other = (Parametro) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return getId() + " - " + getDescripcion();
    }
}
