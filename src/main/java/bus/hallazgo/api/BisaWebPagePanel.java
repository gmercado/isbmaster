package bus.hallazgo.api;

import bus.hallazgo.dao.ItemPlanillaDao;
import bus.hallazgo.dao.ParametroDao;
import bus.hallazgo.dao.SubGrupoParametroDao;
import bus.plumbing.components.AlertFeedbackPanel;
import bus.plumbing.components.ControlGroupBorder;
import bus.plumbing.wicket.BisaWebApplication;
import bus.users.entities.AdmUser;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.Application;
import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;


/**
 * 
 * La clase <code>BisaWebPagePanel.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 12/05/2014, 16:03:13
 *
 * @param <S>
 * @param <U>
 */
public abstract class BisaWebPagePanel<S extends Serializable, U extends AdmUser> extends Panel { 

    private static final long serialVersionUID = 953354558498970414L;
    private static final Logger LOGGER = LoggerFactory.getLogger(BisaWebPagePanel.class);
    
    private Form<S> formData;
    public final AlertFeedbackPanel feedbackPanel;

    protected final IModel<U> adminUserModel;

    public BisaWebPagePanel(String id, IModel<U> model, AlertFeedbackPanel feedbackPanel) {
        super(id);
        this.adminUserModel = model;
        this.feedbackPanel = feedbackPanel;
    }

    protected abstract IModel<S> getTypedModel();
    
    @Override
    protected void onInitialize() {
        super.onInitialize();
        final IModel<S> typedModel = getTypedModel();
        setDefaultModel(typedModel);
        add(formData = new Form<S>("_formdata", new CompoundPropertyModel<S>(typedModel)));
    }

    public final void addComponent(Component component){
        formData.add(component);
    }

    public final MarkupContainer addComponentConBorde(String idBorde, boolean mostrarNotificacionesEnLaAlertaPrincipal, final Component component, int tamanioSpan){
        AlertFeedbackPanel feedbackPanelLocal = null;
        if (!mostrarNotificacionesEnLaAlertaPrincipal) {
            feedbackPanelLocal = this.feedbackPanel;
        }
        ControlGroupBorder control = new ControlGroupBorder(idBorde, component, feedbackPanelLocal, new ResourceModel(component.getId()), tamanioSpan){

            private static final long serialVersionUID = 1L;
            
            @Override
            public boolean isVisible() {
                return component.isVisible();
            }
            
        };
        control.add(component);
        formData.add(control);
        return control.getWmc();
    }

    public final MarkupContainer addComponentConBorde(Component component, int tamanioSpan){
        return addComponentConBorde(component.getId()+"_BORDER", false, component, tamanioSpan);
    }
    public final MarkupContainer addComponentConBorde(Component component){
        return addComponentConBorde(component, 4);
    }
    
    public Form<S> getFormData() {
        return formData;
    }

    @SuppressWarnings("unchecked")
    public S getDefaultObject(){
        return (S) this.getDefaultModelObject();
    }

    
    protected ParametroDao getParametroDao(){
        if( StringUtils.contains(Application.get().getClass().getName(), "BisaWebApplication")){
            return BisaWebApplication.get().getInstance(ParametroDao.class);
        } else {
            throw new IllegalStateException("Imposible obtener la instancia de la clase <ParametroDao.class>.");
        }
    }

    protected SubGrupoParametroDao getSubGrupoParametroDao(){
        if( StringUtils.contains(Application.get().getClass().getName(), "BisaWebApplication")){
            return BisaWebApplication.get().getInstance(SubGrupoParametroDao.class);
        } else {
            throw new IllegalStateException("Imposible obtener la instancia de la clase <SubGrupoParametroDao.class>.");
        }
    }
        
    protected ItemPlanillaDao getItemPlanillaDao(){
        if( StringUtils.contains(Application.get().getClass().getName(), "BisaWebApplication")){
            return BisaWebApplication.get().getInstance(ItemPlanillaDao.class);
        } else {
            throw new IllegalStateException("Imposible obtener la instancia de la clase <ItemPlanillaDao.class>.");
        }
    }
        
    
    public abstract void actualizaTarget(AjaxRequestTarget target) ;

}
