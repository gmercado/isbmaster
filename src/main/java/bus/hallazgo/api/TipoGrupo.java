package bus.hallazgo.api;

/**
 * 
 * La clase <code>TipoGrupo.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 13/05/2014, 16:22:08
 *
 */
public enum TipoGrupo {
    G("Grupo"),
    S("Sub Grupo")
    ;
    
    private final String descripcion;
    
    TipoGrupo(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
