package bus.hallazgo.api;

/**
 * 
 * La clase <code>EstadoItemPlanilla.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 14/05/2014, 13:25:30
 *
 */
public enum EstadoItemPlanilla {
    ACT("Activo"),
    INA("Inactivo")
    ;
    
    private final String descripcion;
    
    EstadoItemPlanilla(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
