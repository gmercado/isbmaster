package bus.hallazgo.api;

/**
 * 
 * La clase <code>Modulo.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 12/05/2014, 14:02:31
 *
 */
public enum Modulo {
    ISB("Sistema Aqua2 - Integration Service Bus");
    
    private final String descripcion;
    
    Modulo(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
