package bus.hallazgo.api;

/**
 * 
 * La clase <code>Modo.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 13/05/2014, 09:26:14
 *
 */
public enum ModoPantalla {
    ALTA("Alta"),
    MOD("Modificacion"),
    BAJA("Baja"),
    MOD_BAJA("Modificacion y Baja")
    ;
    
    private final String descripcion;
    
    ModoPantalla(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
