package bus.hallazgo.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.hallazgo.entities.Parametro;
import com.google.inject.Inject;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * La clase <code>ParamDao.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 12/05/2014, 14:02:24
 *
 */
public class ParametroDao extends DaoImpl<Parametro, String> {


    protected ParametroDao(){
        this(null);
    }
    
    @Inject
    protected ParametroDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<String> countPath(Root<Parametro> from) {
        return from.get("id");
    }
    
    @Override
    protected Iterable<Path<String>> getFullTexts(Root<Parametro> p) {
        ArrayList<Path<String>> paths = new ArrayList<Path<String>>();
        paths.add(p.<String>get("id"));
        paths.add(p.<String>get("descripcion"));
        return paths;
    }
    
    public boolean tieneDependencias(final Parametro param){
        if (param == null) {
            return false;
        }
        List<Parametro> lista = doWithTransaction(new WithTransaction<List<Parametro>>() {
            @Override
            public List<Parametro> call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                TypedQuery<Parametro> q = entityManager.createQuery(
                        "select z " + 
                        "  from Parametro z " + 
                        " where z.idPadre = :padre " ,
                        Parametro.class).
                        setParameter("padre", param.getId());
                    return q.getResultList();
            }
        });
        if (lista == null || lista.isEmpty()){
            return false;
        }
        return true;
    }
    
    public Parametro getSiguienteParametro(final Parametro parametro, final Parametro parametroPadre){
        if (parametro == null || parametroPadre == null) {
            return null;
        }
        List<Parametro> lista = doWithTransaction(new WithTransaction<List<Parametro>>() {
            @Override
            public List<Parametro> call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                TypedQuery<Parametro> q = entityManager.createQuery(
                        "select z " + 
                        "  from Parametro z " + 
                        " where z.idPadre = :padre " +
                        " order by z.secuencia desc",
                        Parametro.class)
                        .setParameter("padre", parametroPadre.getId())
                        ;
                    return q.getResultList();
            }
        });
        int secuencia = 1;
        if (lista != null && !lista.isEmpty()){
            secuencia = lista.get(0).getSecuencia()+1;
        }
        parametro.setIdPadre(parametroPadre.getId());
        parametro.setNivel(parametroPadre.getNivel()+1);
        parametro.setModulo(parametroPadre.getModulo());
        parametro.setSecuencia(secuencia);
        parametro.setId(parametroPadre.getId()+"-"+secuencia);
        return parametro;
    }
}
