package bus.hallazgo.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.hallazgo.entities.GrupoParametro;
import bus.hallazgo.entities.Parametro;
import com.google.inject.Inject;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * La clase <code>GrupoParametrosDao.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 13/05/2014, 11:43:13
 *
 */
public class GrupoParametroDao extends DaoImpl<GrupoParametro, String> {

    @Inject
    protected GrupoParametroDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<String> countPath(Root<GrupoParametro> from) {
        return from.get("id");
    }
    
    @Override
    protected Iterable<Path<String>> getFullTexts(Root<GrupoParametro> p) {
        ArrayList<Path<String>> paths = new ArrayList<Path<String>>();
        paths.add(p.<String>get("id"));
        paths.add(p.<String>get("descripcion"));
        return paths;
    }
    
    public boolean tieneDependencias(final GrupoParametro param){
        if (param == null) {
            return false;
        }
        List<Parametro> lista = doWithTransaction(new WithTransaction<List<Parametro>>() {
            @Override
            public List<Parametro> call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                TypedQuery<Parametro> q = entityManager.createQuery(
                        "select z " + 
                        "  from Parametro z " + 
                        " where z.idPadre = :padre " ,
                        Parametro.class).
                        setParameter("padre", param.getId());
                    return q.getResultList();
            }
        });
        if (lista == null || lista.isEmpty()){
            return false;
        }
        return true;
    }
    
    
    
}
