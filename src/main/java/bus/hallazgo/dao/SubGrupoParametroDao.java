package bus.hallazgo.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.hallazgo.api.Modulo;
import bus.hallazgo.entities.SubGrupoParametro;
import com.google.inject.Inject;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * La clase <code>GrupoParametrosDao.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 13/05/2014, 11:43:13
 *
 */
public class SubGrupoParametroDao extends DaoImpl<SubGrupoParametro, String> {

    protected SubGrupoParametroDao(){
        this(null);
    }
    
    @Inject
    protected SubGrupoParametroDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<String> countPath(Root<SubGrupoParametro> from) {
        return from.get("id");
    }
    
    @Override
    protected Iterable<Path<String>> getFullTexts(Root<SubGrupoParametro> p) {
        ArrayList<Path<String>> paths = new ArrayList<Path<String>>();
        paths.add(p.<String>get("id"));
        paths.add(p.<String>get("descripcion"));
        return paths;
    }
    
    public List<SubGrupoParametro> getListaTransacciones(){
        return getListaDe("TR");
    }
    
    public List<SubGrupoParametro> getListaProcesos(){
        return getListaDe("PR");
    }
    
    public List<SubGrupoParametro> getListaConsultas(){
        return getListaDe("CO");
    }
    
    public List<SubGrupoParametro> getListaSectores(){
        return getListaDe("SE");
    }
    
    public List<SubGrupoParametro> getListaCargos(){
        return getListaDe("CA");
    }
    
    public List<SubGrupoParametro> getListaDe(final String tipo){
        List<SubGrupoParametro> lista = doWithTransaction(new WithTransaction<List<SubGrupoParametro>>() {
            @Override
            public List<SubGrupoParametro> call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                TypedQuery<SubGrupoParametro> q = entityManager.createQuery(
                        "select z " + 
                        "  from SubGrupoParametro z " + 
                        " where z.id like :tipo " +
                        "   and z.nivel = 1 " +
                        "   and z.modulo = :modulo" ,
                        SubGrupoParametro.class)
                        .setParameter("modulo", Modulo.ISB)
                        .setParameter("tipo", tipo+"%")
                        ;
                    return q.getResultList();
            }
        });
        return lista;
    }
    
    
}
