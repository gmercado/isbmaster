package bus.hallazgo.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.hallazgo.entities.ItemPlanilla;
import com.google.inject.Inject;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAEntityTransaction;

import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.List;

public class ItemPlanillaDao extends DaoImpl<ItemPlanilla, Long>{

    protected ItemPlanillaDao(){
        this(null);
    }
    
    @Inject
    protected ItemPlanillaDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }
    @Override
    protected Path<Long> countPath(Root<ItemPlanilla> from) {
        return from.get("id");
    }

    public ItemPlanilla getItemSimilar(final ItemPlanilla itemPlanilla){
        List<ItemPlanilla> lista = doWithTransaction(new WithTransaction<List<ItemPlanilla>>() {
            @Override
            public List<ItemPlanilla> call(OpenJPAEntityManager entityManager, OpenJPAEntityTransaction t) {
                TypedQuery<ItemPlanilla> q = entityManager.createQuery(
                        "select z " + 
                        "  from ItemPlanilla z " + 
                        " where z.sucursal = :sucursal " +
                        "   and z.agencia = :agencia " +
                        "   and z.transaccion = :transaccion" +
                        "   and z.proceso = :proceso" ,
                        ItemPlanilla.class)
                        .setParameter("sucursal", itemPlanilla.getSucursal())
                        .setParameter("agencia", itemPlanilla.getAgencia())
                        .setParameter("transaccion", itemPlanilla.getTransaccion())
                        .setParameter("proceso", itemPlanilla.getProceso())
                        ;
                    return q.getResultList();
            }
        });
        if (lista==null || lista.isEmpty()){
            return null;
        }
        return lista.get(0);
    }
    
}
