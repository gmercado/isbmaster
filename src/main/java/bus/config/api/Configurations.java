/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.config.api;

/**
 * @author Marcelo Morales
 * @since 9/14/11
 */
public interface Configurations {

    String BASEPRIN_DRIVER = "database.driver";
    String BASEPRIN_DRIVER_DEFAULT = "com.ibm.as400.access.AS400JDBCDriver";
    String BASEPRIN_URI = "database.url";
    String BASEPRIN_URI_DEFAULT =
            "jdbc:as400://bisades.grupobisa.net/;prompt=false;naming=system;libraries=zbisasql zbnkprd01;query storage limit=-1;";
    String BASEPRIN_TEMPORAL_URI = "database.temporal.url";
    String BASEPRIN_TEMPORAL_URI_DEFAULT =
            "jdbc:as400://bisades.grupobisa.net/;prompt=false;naming=system;libraries=zbisasql ztbnkprd01;query storage limit=-1;";
    String BASEPRIN_USERNAME = "database.username";
    String BASEPRIN_USERNAME_DEFAULT = "EBANK411";
    String BASEPRIN_PASSWORD = "database.password";
    String BASEPRIN_PASSWORD_DEFAULT = "bisaebank";
    boolean BASEPRIN_PASSWORD_CRYPT = true;
    String BASEPRIN_VALIDATION_QUERY = "database.validationQuery";
    String BASEPRIN_VALIDATION_QUERY_DEFAULT = "Select Current_Server from SysIBM/SysDummy1";

    String BASEAUX_DRIVER = "database-aux.driver";
    String BASEAUX_DRIVER_DEFAULT = "com.ibm.db2.jcc.DB2Driver";
    String BASEAUX_URI = "database-aux.url";
    String BASEAUX_URI_DEFAULT = "jdbc:db2://1.10.30.117:50000/DBFIRA1:currentSchema=BKFIR;";
    String BASEAUX_USERNAME = "database-aux.username";
    String BASEAUX_USERNAME_DEFAULT = "db2svc";
    String BASEAUX_PASSWORD = "database-aux.password";
    String BASEAUX_PASSWORD_DEFAULT = "db2final";
    boolean BASEAUX_PASSWORD_CRYPT = true;
    String BASEAUX_VALIDATION_QUERY = "database-aux.validationQuery";
    String BASEAUX_VALIDATION_QUERY_DEFAULT = "select * from BKCONTROL fetch first 1 rows only";

    String BASEAUX2_DRIVER = "database-aux2.driver";
    String BASEAUX2_DRIVER_DEFAULT = "com.ibm.db2.jcc.DB2Driver";
    String BASEAUX2_URI = "database-aux2.url";
    String BASEAUX2_URI_DEFAULT = "jdbc:db2://1.10.30.117:50000/DBFIRA1:currentSchema=BKFIR;";
    String BASEAUX2_USERNAME = "database-aux2.username";
    String BASEAUX2_USERNAME_DEFAULT = "db2svc";
    String BASEAUX2_PASSWORD = "database-aux2.password";
    String BASEAUX2_PASSWORD_DEFAULT = "db2final";
    boolean BASEAUX2_PASSWORD_CRYPT = true;
    String BASEAUX2_VALIDATION_QUERY = "database-aux2.validationQuery";
    String BASEAUX2_VALIDATION_QUERY_DEFAULT = "select * from BKCONTROL fetch first 1 rows only";

    String BASEAUX3_DRIVER = "database-aux3.driver";
    String BASEAUX3_DRIVER_DEFAULT = "com.ibm.db2.jcc.DB2Driver";
    String BASEAUX3_URI = "database-aux3.url";
    String BASEAUX3_URI_DEFAULT = "jdbc:db2://1.10.30.117:50000/DBFIRA1:currentSchema=BKFIR;";
    String BASEAUX3_USERNAME = "database-aux3.username";
    String BASEAUX3_USERNAME_DEFAULT = "db2svc";
    String BASEAUX3_PASSWORD = "database-aux3.password";
    String BASEAUX3_PASSWORD_DEFAULT = "db2final";
    boolean BASEAUX3_PASSWORD_CRYPT = true;
    String BASEAUX3_VALIDATION_QUERY = "database-aux3.validationQuery";
    String BASEAUX3_VALIDATION_QUERY_DEFAULT = "select * from BKCONTROL fetch first 1 rows only";

    String CACHE_MAX = "cache.max.elementos";
    String CACHE_MAX_DEFAULT = "100000";

    String CACHE_AMBIENTE_ACTUAL = "cache.secs.ambiente";
    int CACHE_AMBIENTE_ACTUAL_DEFAULT = 600;

    String CONVERTIR_CCSID = "convertir.ccsid";
    String CONVERTIR_CCSID_DEFAULT = "no";
    /***********************SQL SERVER STADISTICS*************************/
    String BASEAUX_STAD_DRIVER = "database-aux.stadistic.driver";
    String BASEAUX_STAD_DRIVER_DEFAULT = "net.sourceforge.jtds.jdbc.Driver";
    String BASEAUX_STAD_URI = "database-aux.stadistic.url";
    String BASEAUX_STAD_URI_DEFAULT = "jdbc:jtds:sqlserver://xebisadb.grupobisa.net:1207/Stadistic;";
    String BASEAUX_STAD_USERNAME = "database-aux.stadistic.username";
    String BASEAUX_STAD_USERNAME_DEFAULT = "ebisausr";
    String BASEAUX_STAD_PASSWORD = "database-aux.stadistic.password";
    String BASEAUX_STAD_PASSWORD_DEFAULT = "crypt:WLFgPZFoZRZbbv7C6A7pMA==";
    boolean BASEAUX_STAD_PASSWORD_CRYPT = true;
    String BASEAUX_STAD_VALIDATION_QUERY = "database-aux.stadistic.validationQuery";
    String BASEAUX_STAD_VALIDATION_QUERY_DEFAULT = "select getdate()";

    /***********************SQL SERVER CONTRACT*************************/
    String BASEAUX_CONTR_DRIVER = "database-aux.contract.driver";
    String BASEAUX_CONTR_DRIVER_DEFAULT = "net.sourceforge.jtds.jdbc.Driver";
    String BASEAUX_CONTR_URI = "database-aux.contract.url";
    String BASEAUX_CONTR_URI_DEFAULT = "jdbc:jtds:sqlserver://xebisadb.grupobisa.net:1207/EbisaContractDB;";
    String BASEAUX_CONTR_USERNAME = "database-aux.contract.username";
    String BASEAUX_CONTR_USERNAME_DEFAULT = "ebisausr";
    String BASEAUX_CONTR_PASSWORD = "database-aux.contract.password";
    String BASEAUX_CONTR_PASSWORD_DEFAULT = "crypt:WLFgPZFoZRZbbv7C6A7pMA==";
    boolean BASEAUX_CONTR_PASSWORD_CRYPT = true;
    String BASEAUX_CONTR_VALIDATION_QUERY = "database-aux.contract.validationQuery";
    String BASEAUX_CONTR_VALIDATION_QUERY_DEFAULT = "select getdate()";
}
