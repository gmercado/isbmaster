/*
 * Copyright 2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.config.ui;

import bus.config.api.Configurations;
import bus.config.dao.ConfigurationProvider;
import bus.config.dao.CryptUtils;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.configuration.Configuration;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;

import javax.annotation.Nullable;
import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Marcelo Morales
 * @since 9/14/11
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN, RolesBisa.SOLO_LECTURA})
@Menu(value = "Configuraci\u00f3n de aplicaci\u00f3n", subMenu = SubMenu.ADMINISTRACION)
public class VerConfiguracion extends BisaWebPage {

    private static final long serialVersionUID = 3294454907776272561L;

    private final Component componentz;
    private final IModel<String> of1;
    private final IModel<String> of2;

    public VerConfiguracion() {
        super();

        of1 = Model.of("");
        of2 = Model.of("");

        setDefaultModel(of1);

        add(new Label("properties", new AbstractReadOnlyModel<Object>() {

            private static final long serialVersionUID = -5864089962582924422L;

            @Override
            public Object getObject() {
                File fileEncontrado = getInstance(ConfigurationProvider.class).getFileEncontrado();
                if (fileEncontrado == null) {
                    return "ARCHIVO NO ENCONTRADO";
                }
                return fileEncontrado.getAbsolutePath();
            }
        }));

        add(new ListView<String>("archivo-configuracion", new LoadableDetachableModel<List<String>>() {

            private static final long serialVersionUID = 2504996168060593824L;

            @Override
            public List<String> load() {
                Iterable<File> archivosBuscados = getInstance(ConfigurationProvider.class).getArchivosBuscados();
                return Lists.newLinkedList(Iterables.transform(archivosBuscados, new Function<File, String>() {
                    @Override
                    public String apply(@Nullable File input) {
                        if (input == null) {
                            return ";";
                        }
                        return input.getAbsolutePath();
                    }
                }));
            }
        }) {

            private static final long serialVersionUID = -3633549798720625084L;

            @Override
            protected void populateItem(ListItem<String> components) {
                components.add(new Label("properties", components.getModel()));
            }
        });

        Form<String> form;
        add(form = new Form<>("encriptador"));

        add(componentz = new Label("resultado", new AbstractReadOnlyModel<Object>() {

            private static final long serialVersionUID = -1428876267758006548L;

            @Override
            public Object getObject() {
                String x1 = of1.getObject();
                String x2 = of2.getObject();
                CryptUtils cryptUtils = getInstance(CryptUtils.class);
                if (x1.equals(x2)) {
                    return cryptUtils.encssl(x1);
                } else {
                    return "NO CONCUERDAN";
                }
            }
        }).setOutputMarkupId(true));

        form.add(new PasswordTextField("cripto", of1).setOutputMarkupId(true).add(new OnChangeAjaxBehavior() {
            private static final long serialVersionUID = -7475608597392723571L;

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                target.add(componentz);
                target.add(feedbackPanel);
            }
        }));
        form.add(new PasswordTextField("cripto2", of2).setOutputMarkupId(true).add(new OnChangeAjaxBehavior() {
            private static final long serialVersionUID = -7475608597392723571L;

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                target.add(componentz);
                target.add(feedbackPanel);
            }
        }));


        add(new DefaultDataTable<>("configuracion",
                new ArrayList<IColumn<SPar, String>>() {
                    private static final long serialVersionUID = 4877252317731409362L;

                    {
                        add(new PropertyColumn<>(Model.of("llave"), "p"));
                        add(new PropertyColumn<>(Model.of("valor"), "q"));
                    }
                },
                new SortableDataProvider<SPar, String>() {

                    private static final long serialVersionUID = 4453839302280193391L;

                    @Override
                    public Iterator<? extends SPar> iterator(long first, long count) {
                        Configuration configuration = getInstance(Configuration.class);

                        List<SPar> pars = new ArrayList<>((int) count);

                        Field[] declaredFields = Configurations.class.getDeclaredFields();
                        for (Field declaredField : declaredFields) {
                            if (declaredField.getName().endsWith("_DEFAULT")) {
                                continue;
                            }

                            if (declaredField.getName().endsWith("_CRYPT")) {
                                continue;
                            }

                            if (first > 0) {
                                first--;
                                continue;
                            }

                            if (count == 0) {
                                break;
                            }

                            count--;
                            try {

                                boolean mostrar;
                                try {
                                    Configurations.class.getDeclaredField(declaredField.getName() + "_CRYPT").get(null);
                                    mostrar = false;
                                } catch (NoSuchFieldException e) {
                                    mostrar = true;
                                }

                                String nombre = (String) declaredField.get(null);
                                String vpd;
                                try {
                                    Object o = Configurations.class.getDeclaredField(
                                            declaredField.getName() + "_DEFAULT").get(null);
                                    if (o == null) {
                                        vpd = "(sin valor)";
                                    } else {
                                        vpd = o.toString();
                                    }
                                } catch (NoSuchFieldException e) {
                                    vpd = "(sin valor)";
                                }

                                String string;
                                if (mostrar) {
                                    string = configuration.getString(nombre, vpd);
                                } else {
                                    string = "*******";
                                }
                                pars.add(new SPar(nombre, string));
                            } catch (IllegalAccessException e) {
                                LOGGER.error("Hay un error con el field " + declaredField, e);
                            }
                        }
                        return pars.iterator();
                    }

                    @Override
                    public long size() {
                        int cuantos = 0;
                        Field[] declaredFields = Configurations.class.getDeclaredFields();
                        for (Field declaredField : declaredFields) {
                            if (declaredField.getName().endsWith("_DEFAULT")) {
                                continue;
                            }
                            cuantos++;
                        }

                        return cuantos;
                    }

                    @Override
                    public IModel<SPar> model(SPar object) {
                        return Model.of(object);
                    }
                }, 35
        ));

        add(new AjaxFallbackDefaultDataTable<>("propiedades", new ArrayList<IColumn<SPar, String>>() {
            private static final long serialVersionUID = 4877252317731409362L;

            {
                add(new PropertyColumn<>(Model.of("llave"), "p"));
                add(new PropertyColumn<>(Model.of("valor"), "q"));
            }
        }, new SortableDataProvider<SPar, String>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Iterator<? extends SPar> iterator(long first, long count) {
                Properties properties = System.getProperties();
                List<SPar> sPars = properties.keySet().stream().map(o -> new SPar(o.toString(), properties.getProperty(o.toString()))).collect(Collectors.toCollection(LinkedList::new));
                return sPars.subList((int) first, (int) (first + count)).iterator();
            }

            @Override
            public long size() {
                return System.getProperties().size();
            }

            @Override
            public IModel<SPar> model(SPar object) {
                return Model.of(object);
            }
        }, 20));


        add(new AjaxFallbackDefaultDataTable<>("env", new ArrayList<IColumn<SPar, String>>() {
            private static final long serialVersionUID = 4877252317731409362L;

            {
                add(new PropertyColumn<>(Model.of("llave"), "p"));
                add(new PropertyColumn<>(Model.of("valor"), "q"));
            }
        }, new SortableDataProvider<SPar, String>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Iterator<? extends SPar> iterator(long first, long count) {
                Map<String, String> properties = System.getenv();
                List<SPar> sPars = properties.keySet().stream().map(o -> new SPar(o, System.getenv(o))).collect(Collectors.toCollection(LinkedList::new));
                return sPars.subList((int) first, (int) (first + count)).iterator();
            }

            @Override
            public long size() {
                return System.getenv().size();
            }

            @Override
            public IModel<SPar> model(SPar object) {
                return Model.of(object);
            }
        }, 20));
    }

    private static class SPar implements Serializable {

        private static final long serialVersionUID = -7871134813464786719L;

        String p;
        String q;

        public SPar() {
        }

        private SPar(String p, String q) {
            this.p = p;
            this.q = q;
        }
    }
}