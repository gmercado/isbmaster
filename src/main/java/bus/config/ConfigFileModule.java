/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.config;

import bus.config.dao.ConfigurationProvider;
import bus.config.dao.CryptUtils;
import bus.config.dao.NombreAplicacion;
import bus.plumbing.api.AbstractModuleBisa;
import bus.plumbing.api.ICrypt;
import com.google.inject.Scopes;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.util.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Configurador de la aplicación por archivo .properties (por defecto indo.properties).
 * <p>Por defecto busca un archivo llamado "indo.properties", pero puede cambiarse con la propiedad -Dindo.properties.</p>
 * <p>Puede utilizar el viejo crypt: o un más estándar openssl:</p>
 * <p>Adicionalmente puede cambiar la llave de encriptacion con la propiedad java indo.crypt=/archivo/con/la/llave</p>
 * <p>El passphase se puede cambiar con la propiedad java indo.passphrase=/archivo/con/el/passphrase</p>
 * <p>El openssl es <code>openssl enc -aes-128-cbc -a -A -pass file:pathname</code></p>
 *
 * @author Marcelo Morales
 * @since 12/31/10 9:24 AM
 */
public class ConfigFileModule extends AbstractModuleBisa {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigFileModule.class);
    private final String filename;
    private static String DEFAULT_CRYPT = "b21617ca69b03871354aa8586934e493";
    private static String DEFAULT_PASSPHRASE = "taula de canvi";

    /**
     * Ha ocurrido un caso particular, donde en una misma máquina virtual se requiere más de un sistema.
     *
     * @param filename el nombre de un archivo de configuración.
     */
    public ConfigFileModule(String filename) {
        if (filename == null) {
            throw new IllegalArgumentException("necesito el nombre de un archivo");
        }
        String Ofilename = System.getProperty("bus.config.Ofilename");
        LOGGER.info("ConfigFileModule>> {} {}", filename, Ofilename);
        if (Ofilename == null || StringUtils.trimToNull(Ofilename) == null) {
            this.filename = filename;
        } else {
            this.filename = Ofilename;
        }
        LOGGER.info("Target >> {}", this.filename);
    }

    @Override
    protected void configure() {

        bindUI("bus/config/ui");

        String lastElement;
        if (StringUtils.contains(filename, File.separator)) {
            lastElement = StringUtils.substringAfterLast(filename, File.separator);
        } else {
            lastElement = filename;
        }

        String crypt1 = System.getProperty(lastElement + ".crypt");
        if (crypt1 != null) {
            File f = new File(crypt1);
            if (f.exists() && f.canRead()) {
                FileReader fileReader = null;
                try {
                    fileReader = new FileReader(f);
                    DEFAULT_CRYPT = IOUtils.toString(fileReader).trim();
                } catch (IOException e) {
                    LOGGER.error("Ha ocurrido un error al leer el archivo de la llave, se ha a usar la por defecto", e);
                } finally {
                    IOUtils.closeQuietly(fileReader);
                }
            }
        }

        String passphrase1 = System.getProperty(lastElement + ".passphrase");
        if (passphrase1 != null) {
            File f = new File(passphrase1);
            if (f.exists() && f.canRead()) {
                FileReader fileReader = null;
                try {
                    fileReader = new FileReader(f);
                    DEFAULT_PASSPHRASE = IOUtils.toString(fileReader).trim();
                } catch (IOException e) {
                    LOGGER.error("Ha ocurrido un error al leer el archivo de la passphrase, se ha a usar el por defecto", e);
                } finally {
                    IOUtils.closeQuietly(fileReader);
                }
            }
        }

        CryptUtils cryptUtils = new CryptUtils(DEFAULT_CRYPT, DEFAULT_PASSPHRASE);
        bind(CryptUtils.class).toInstance(cryptUtils);
        bind(ICrypt.class).toInstance(cryptUtils);

        bindConstant().annotatedWith(NombreAplicacion.class).to(filename);
        bind(Configuration.class).toProvider(ConfigurationProvider.class).in(Scopes.SINGLETON);
    }

    public static ICrypt getCryptUtils() {
        return new CryptUtils(DEFAULT_CRYPT, DEFAULT_PASSPHRASE);
    }
}
