/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.config.dao;

import bus.plumbing.api.ICrypt;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.RuntimeCryptoException;
import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.generators.OpenSSLPBEParametersGenerator;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Random;

/**
 * @author Marcelo Morales
 * @since 1/3/11 11:14 AM
 */
public class CryptUtils implements Serializable, ICrypt {

    private static final String UTF_8 = "UTF-8";
    private static final String DES_ede = "DESede";
    private static final String US_ASCII = "US-ASCII";
    private static final String PREFIX_LEGADO = "crypt:";
    private static final String PREFIX_OPENSSL = "openssl:";
    private static final Logger LOGGER = LoggerFactory.getLogger(CryptUtils.class);
    private static final Random RANDOM = new SecureRandom();
    private final SecretKey key;
    private final String passphrase;

    public CryptUtils(String crypt, String passphrase) {
        this.passphrase = passphrase;
        try {
            byte[] keyAsBytes = crypt.getBytes(UTF_8);
            KeySpec keySpec = new DESedeKeySpec(keyAsBytes);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES_ede);
            key = keyFactory.generateSecret(keySpec);
        } catch (GeneralSecurityException | UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    private SecretKey getSecretKey() {
        return key;
    }

    public final String dec(String retValue) {
        if (retValue == null) {
            return null;
        }
        if (!retValue.startsWith(PREFIX_LEGADO)) {
            return retValue;
        }
        try {
            Cipher cipher = Cipher.getInstance(DES_ede);
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey());
            byte[] ciphertext = Base64.decodeBase64(retValue.substring(PREFIX_LEGADO.length()).getBytes(UTF_8));
            byte[] clear = cipher.doFinal(ciphertext);
            return new String(clear, UTF_8);
        } catch (UnsupportedEncodingException | GeneralSecurityException e) {
            LOGGER.error("Error al des-encriptar la cadena '" + retValue + "'", e);
        }
        return retValue;
    }

    public final String enc(String value) {
        if (value == null) {
            return null;
        }
        try {
            Cipher cipher = Cipher.getInstance(DES_ede);
            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey());
            byte[] cleartext = value.getBytes(UTF_8);
            byte[] ciphertext = cipher.doFinal(cleartext);
            return PREFIX_LEGADO + new String(Base64.encodeBase64(ciphertext));
        } catch (GeneralSecurityException | UnsupportedEncodingException e) {
            LOGGER.error("Error al encriptar la cadena '" + value + "'", e);
        }
        return value;
    }

    /**
     * Verifica que valga la pena intentar des-encriptar el valor.
     *
     * @param value el valor que posiblemente se requiera desencriptar
     * @return true si pasa las pruebas b�sicas.
     */
    public final boolean esCryptLegado(String value) {
        return value != null // No nulo
                && value.length() > (PREFIX_LEGADO.length() + 10) // Cabe por lo menos un bloque de 64 bits
                && value.startsWith(PREFIX_LEGADO); // empieza con el prefijo
    }

    public final String decssl(String value) {
        if (value == null) {
            return null;
        }

        if (!value.startsWith(PREFIX_OPENSSL)) {
            return value;
        }

        byte[] bytes = new Base64(0, null, false).decode(value.substring(PREFIX_OPENSSL.length()));
        if (bytes.length < 32) {
            LOGGER.error("La cadena {} no parece tener suficiente tamanyo para una encriptacion openssl o no es base64 valido", value);
            return value;
        }

        byte[] prefix = "Salted__".getBytes();
        for (int i = 0; i < prefix.length; i++) {
            if (bytes[i] != prefix[i]) {
                LOGGER.error("La cadena {} no parece tener sal", value);
                return value;
            }
        }
        byte[] salt = new byte[8];
        System.arraycopy(bytes, 8, salt, 0, 8);

        OpenSSLPBEParametersGenerator generator = new OpenSSLPBEParametersGenerator();
        generator.init(PBEParametersGenerator.PKCS5PasswordToBytes(passphrase.toCharArray()), salt);
        CipherParameters cipherParameters = generator.generateDerivedParameters(128, 128);

        PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CBCBlockCipher(new AESFastEngine()));
        cipher.init(false, cipherParameters);

        int outputSize = cipher.getOutputSize(bytes.length - 16);
        byte[] out = new byte[outputSize];
        int cuantos = cipher.processBytes(bytes, 16, bytes.length - 16, out, 0);
        try {
            cuantos += cipher.doFinal(out, cuantos);
            return new String(out, 0, cuantos, US_ASCII).trim();
        } catch (RuntimeCryptoException e) {
            LOGGER.error("error", e);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e); // significa que no hay US-ASCII, imposible!
        } catch (CryptoException e) {
            LOGGER.error("Error en el des-crifrado", e);
        }
        return value;
    }

    public final String encssl(String value) {
        if (value == null) {
            return null;
        }

        byte[] bytes;
        try {
            bytes = value.getBytes(UTF_8);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }

        byte[] salted = "Salted__".getBytes();
        byte[] salt = new byte[8];
        RANDOM.nextBytes(salt);

        OpenSSLPBEParametersGenerator generator = new OpenSSLPBEParametersGenerator();
        generator.init(PBEParametersGenerator.PKCS5PasswordToBytes(passphrase.toCharArray()), salt);
        CipherParameters cipherParameters = generator.generateDerivedParameters(128, 128);

        PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CBCBlockCipher(new AESFastEngine()));
        cipher.init(true, cipherParameters);

        int outputSize = cipher.getOutputSize(bytes.length);
        byte[] out = new byte[outputSize];
        int cuantos = cipher.processBytes(bytes, 0, bytes.length, out, 0);
        try {
            int size = cipher.doFinal(out, cuantos);

            byte[] outfinal = new byte[size + 8 + salt.length];
            System.arraycopy(salted, 0, outfinal, 0, salted.length);
            System.arraycopy(salt, 0, outfinal, salted.length, salt.length);
            System.arraycopy(out, 0, outfinal, salted.length + salt.length, size);

            Base64 base64 = new Base64(0, null, false);
            String s3 = base64.encodeToString(outfinal);

            return PREFIX_OPENSSL + s3;

        } catch (RuntimeCryptoException e) {
            LOGGER.error("error", e);
        } catch (CryptoException e) {
            LOGGER.error("Error en el des-crifrado", e);
        }
        return value;
    }

    public boolean esOpenssl(String value) {
        return value != null // valor
                && value.length() > (PREFIX_OPENSSL.length() + 21 + 10 + 10) // 128 bits + "Salted__" + 8 (sal)
                && value.startsWith(PREFIX_OPENSSL);
    }
}
