package bus.config.dao;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.apache.commons.configuration.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;
import java.util.Arrays;

/**
 * @author Marcelo Morales
 * @since 5/9/12
 */
@Singleton
public class ConfigurationProvider implements Provider<Configuration> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationProvider.class);

    private static final String[] PREFIXES = new String[]{
            "", "/etc/", "" + System.getenv("HOME") + "/etc/"
    };

    private final CryptUtils cryptUtils;

    private String filename;

    private File fileEncontrado;

    @Inject
    public ConfigurationProvider(CryptUtils cryptUtils, @NombreAplicacion String filename) {
        this.cryptUtils = cryptUtils;
        this.filename = filename;
    }

    @Override
    public Configuration get() {
        String lastElement;
        if (StringUtils.contains(filename, '/')) {
            lastElement = StringUtils.substringAfterLast(filename, "/");
        } else {
            lastElement = filename;
        }

        CompositeConfiguration configuration = new CompositeConfiguration();

        String filenameFromConfig = System.getProperty(lastElement);
        if (filenameFromConfig != null) {
            LOGGER.info("Utilizando {} en lugar de {} para configurar la aplicacion porque la propiedad {} aparece",
                    filenameFromConfig, filename, lastElement);
            filename = filenameFromConfig;
        }

        LOGGER.debug("Utilizando la cadena {} para configurar la aplicacion", filenameFromConfig);

        for (File f : getArchivosBuscados()) {
            LOGGER.debug("Intentando configurar con el archivo {}", f);

            if (!f.exists() || !f.isFile() || !f.canRead()) {
                LOGGER.debug("No se pudo configurar con el archivo {} porque no existe, no es un archivo o no se puede leer", f);
                continue;
            }

            LOGGER.info("Se ha encontrado configuracion para la aplicacion en el archivo {}", f);
            fileEncontrado = f;

            try {
                configuration.addConfiguration(new PropertiesConfiguration(f));
                break;
            } catch (ConfigurationException e) {
                LOGGER.warn("Ha ocurrido un error al intentar leer el archivo {}: {}", f.getAbsolutePath(), e.getMessage());
            }
        }

        configuration.addConfiguration(new EnvironmentConfiguration());

        configuration.addConfiguration(new SystemConfiguration());

        return new OverConfiguration(configuration, cryptUtils);
    }

    public Iterable<File> getArchivosBuscados() {
        return Iterables.transform(Arrays.asList(PREFIXES), new Function<String, File>() {

            @Override
            public File apply(@Nullable String input) {
                return new File(input + filename);
            }
        });
    }

    public File getFileEncontrado() {
        return fileEncontrado;
    }
}
