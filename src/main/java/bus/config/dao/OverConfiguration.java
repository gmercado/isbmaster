/*
 * Copyright (c) 2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.config.dao;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.Configuration;

import java.io.Serializable;
import java.util.Iterator;

/**
 * @author Marcelo Morales
 * @since 1/10/11 8:30 AM
 */
public class OverConfiguration extends AbstractConfiguration implements Iterable<String>, Serializable {

    private final Configuration delegate;

    private final CryptUtils cryptUtils;

    public OverConfiguration(Configuration delegate, CryptUtils cryptUtils) {
        this.delegate = delegate;
        this.cryptUtils = cryptUtils;
    }

    @Override
    protected void addPropertyDirect(String key, Object value) {
        delegate.addProperty(key, value);
    }

    @Override
    public boolean isEmpty() {
        return delegate.isEmpty();
    }

    @Override
    public boolean containsKey(String key) {
        return delegate.containsKey(key);
    }

    @Override
    public Object getProperty(String key) {
        Object property = delegate.getProperty(key);
        if (!(property instanceof String)) {
            return property;
        }
        String s = (String) property;
        if (cryptUtils.esCryptLegado(s)) {
            return cryptUtils.dec(s);
        }
        if (cryptUtils.esOpenssl(s)) {
            return cryptUtils.decssl(s);
        }
        return s;
    }

    @Override
    public Iterator<String> getKeys() {
        return delegate.getKeys();
    }

    @Override
    public Iterator<String> iterator() {
        //noinspection unchecked
        return getKeys();
    }
}
