package bus.monitor.transaction;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by josanchez on 27/07/2016.
 */
public class Utiles implements Serializable {

    private static final long serialVersionUID = -1245604926742795042L;

    public Utiles() {
    }

    public String formatString(String var, int largo) {
        var = var==null?"":var;
        int lvar = var.length();
        if (lvar == largo) {
            return var;
        } else if (lvar > largo) {
            return var.substring(0, largo);
        }
        char c = ' ';
        String retorno = "";
        for (int i = 0; i < (largo - lvar); i++) {
            retorno += c;
        }

        return var + retorno;
    }

    public String formatNumber(String var, int largo) {
        var = var==null?"":var;
        int lvar = var.length();
        if (lvar == largo) {
            return var;
        }
        char c = '0';
        String retorno = "";
        for (int i = 0; i < (largo - lvar); i++) {
            retorno += c;
        }

        return retorno + var;
    }

    public String genNunDevice() {
        String numgen = (new StringBuffer()).append(
                String.valueOf(Math.round(10000D * Math.random()))).toString();
        String result = FormatNumDevice(numgen);

        return "EB" + result.substring(0, 8);
    }

    public String genNumControl() {
        String numgen = (new StringBuffer()).append(
                String.valueOf(Math.round(10000D * Math.random()))).toString();
        String result = FormatNumControl(numgen);

        return result.substring(0, 5);
    }

    private static String FormatNumControl(String m) {

        int l = m.length();
        int total;
        String s = "";
        if (l < 5) {
            total = 5 - l;
            for (int i = 0; i < total; i++)
                s = s + random();
            s = m + s;
        }
        if (l == 5)
            s = m;
        return s;
    }

    public static final String formatoCuentaBisa(String cuenta) {
        String parse = "";
        String aux1 = "";
        String aux2 = "";

        aux1 = cuenta.substring(cuenta.length() - 1, cuenta.length());
        aux2 = cuenta.substring(0, cuenta.length() - 1);
        parse = aux2 + "-" + aux1;

        aux1 = parse.substring(parse.length() - 5, parse.length());
        aux2 = parse.substring(0, parse.length() - 5);
        cuenta = aux2 + "-" + aux1;

        aux1 = cuenta.substring(cuenta.length() - 6, cuenta.length());
        aux2 = cuenta.substring(0, cuenta.length() - 6);

        String finalStr = "";
        int largo = aux2.length();
        if (largo < 6) {
            for (int j = 0; j < (6 - largo); j++) {
                finalStr += "0";
            }
        }
        parse = finalStr + aux2 + aux1;
        return parse;
    }

    public static final String formatoCuentaContableBisa(String cuenta) {
        String parse = "";
        String aux1 = "";
        String aux2 = "";

        aux1 = cuenta.substring(cuenta.length() - 4, cuenta.length());
        aux2 = cuenta.substring(0, cuenta.length() - 4);
        parse = aux2 + "-" + aux1;

        aux1 = parse.substring(parse.length() - 6, parse.length());
        aux2 = parse.substring(0, parse.length() - 6);
        cuenta = aux2 + "-" + aux1;

        aux1 = cuenta.substring(cuenta.length() - 9, cuenta.length());
        aux2 = cuenta.substring(0, cuenta.length() - 9);
        parse = aux2 + "-" + aux1;
        cuenta = parse;

        aux1 = cuenta.substring(cuenta.length() - 6, cuenta.length());
        aux2 = cuenta.substring(0, cuenta.length() - 6);

        String finalStr = "";
        int largo = aux2.length();
        if (largo < 3) {
            for (int j = 0; j < (3 - largo); j++) {
                finalStr += "0";
            }
        }
        parse = finalStr + aux2 + aux1;
        return parse;
    }

    private static String FormatNumDevice(String m) {

        int l = m.length();
        int total;
        String s = "";
        if (l < 8) {
            total = 8 - l;
            for (int i = 0; i < total; i++)
                s = s + random();
            s = m + s;
        }
        if (l == 8)
            s = m;
        return s;
    }

    private static String random() {
        SimpleDateFormat simpledateformat = new SimpleDateFormat("SS");
        String result = simpledateformat.format(new Date());
        return result.substring(0, 1);
    }
}

