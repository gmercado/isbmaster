package bus.monitor.transaction.trxout;



import bus.monitor.transaction.Utiles;

import java.io.Serializable;

public class CabeceraOUT implements Serializable {
	
	private static final long serialVersionUID = 7701745754242307879L;

	public static final String LARGO = "130";
	public static final int LARGO_INT = Integer.parseInt(LARGO);
	private Utiles util = new Utiles();
	
	private String WLRGREG;
	private String WTPODTA;
	private String WNUMCTL;
	private String WNUMUSR;
	private String WNUMSEQ;
	private String WPOST;
	private String WFLGAPR;
	private String WTPOAPR;
	private String WTRNPEN;
	private String WFECPRO;
	private String WFECSIS;
	private String WHORA;
	private String WCODSEG;
	private String WFMT1;
	private String WFMT2;
	private String WFMT3;
	private String WLRG1;
	private String WLRG2;
	private String WLRG3;
	private String WOCU1;
	private String WOCU2;
	private String WOCU3;
	private String WCON1;
	private String WCON2;
	private String WCON3;
	private String WREGFACB;
	private String WREGFACO;
	private String WREGLV;
	private String WREGDEP;
	private String WREGMEM;
	private String WREJ1;
	private String WREJ2;
	private String WREJ3;
	private String WREJ4;
	private String WREJ5;
	private String WFLG1;
	private String WFLG2;
	private String WFLG3;
	private String WFLG4;
	private String WVAR1;
	
	public void setWLRGREG(String wlrgreg) {
		WLRGREG = util.formatNumber(wlrgreg, 5);
	}
	public void setWTPODTA(String wtpodta) {
		WTPODTA = util.formatNumber(wtpodta, 2);
	}
	public void setWNUMCTL(String wnumctl) {
		WNUMCTL = util.formatNumber(wnumctl, 5);
	}
	public void setWNUMUSR(String wnumusr) {
		WNUMUSR = util.formatNumber(wnumusr, 4);
	}
	public void setWNUMSEQ(String wnumseq) {
		WNUMSEQ = util.formatNumber(wnumseq, 7);
	}
	public void setWPOST(String wpost) {
		WPOST = util.formatString(wpost, 1);
	}
	public void setWFLGAPR(String wflgapr) {
		WFLGAPR = util.formatString(wflgapr, 1);
	}
	public void setWTPOAPR(String wtpoapr) {
		WTPOAPR = util.formatString(wtpoapr, 1);
	}
	public void setWTRNPEN(String wtrnpen) {
		WTRNPEN = util.formatString(wtrnpen, 1);
	}
	public void setWFECPRO(String wfecpro) {
		WFECPRO = util.formatNumber(wfecpro, 8);
	}
	public void setWFECSIS(String wfecsis) {
		WFECSIS = util.formatNumber(wfecsis, 8);
	}
	public void setWHORA(String whora) {
		WHORA = util.formatNumber(whora, 6);
	}
	public void setWCODSEG(String wcodseg) {
		WCODSEG = util.formatNumber(wcodseg, 11);
	}
	public void setWFMT1(String wfmt1) {
		WFMT1 = util.formatNumber(wfmt1, 3);
	}
	public void setWFMT2(String wfmt2) {
		WFMT2 = util.formatNumber(wfmt2, 3);
	}
	public void setWFMT3(String wfmt3) {
		WFMT3 = util.formatNumber(wfmt3, 3);
	}
	public void setWLRG1(String wlrg1) {
		WLRG1 = util.formatNumber(wlrg1, 5);
	}
	public void setWLRG2(String wlrg2) {
		WLRG2 = util.formatNumber(wlrg2, 5);
	}
	public void setWLRG3(String wlrg3) {
		WLRG3 = util.formatNumber(wlrg3, 5);
	}
	public void setWOCU1(String wocu1) {
		WOCU1 = util.formatNumber(wocu1, 3);
	}
	public void setWOCU2(String wocu2) {
		WOCU2 = util.formatNumber(wocu2, 3);
	}
	public void setWOCU3(String wocu3) {
		WOCU3 = util.formatNumber(wocu3, 3);
	}
	public void setWCON1(String wcon1) {
		WCON1 = util.formatString(wcon1, 1);
	}
	public void setWCON2(String wcon2) {
		WCON2 = util.formatString(wcon2, 1);
	}
	public void setWCON3(String wcon3) {
		WCON3 = util.formatString(wcon3, 1);
	}
	public void setWREGFACB(String wregfacb) {
		WREGFACB = util.formatNumber(wregfacb, 1);
	}
	public void setWREGFACO(String wregfaco) {
		WREGFACO = util.formatNumber(wregfaco, 1);
	}
	public void setWREGLV(String wreglv) {
		WREGLV = util.formatString(wreglv, 1);
	}
	public void setWREGDEP(String wregdep) {
		WREGDEP = util.formatString(wregdep, 1);
	}
	public void setWREGMEM(String wregmem) {
		WREGMEM = util.formatString(wregmem, 1);
	}
	public void setWREJ1(String wrej1) {
		WREJ1 = util.formatString(wrej1, 3);
	}
	public void setWREJ2(String wrej2) {
		WREJ2 = util.formatString(wrej2, 3);
	}
	public void setWREJ3(String wrej3) {
		WREJ3 = util.formatString(wrej3, 3);
	}
	public void setWREJ4(String wrej4) {
		WREJ4 = util.formatString(wrej4, 3);
	}
	public void setWREJ5(String wrej5) {
		WREJ5 = util.formatString(wrej5, 3);
	}
	public void setWFLG1(String wflg1) {
		WFLG1 = util.formatString(wflg1, 1);
	}
	public void setWFLG2(String wflg2) {
		WFLG2 = util.formatString(wflg2, 1);
	}
	public void setWFLG3(String wflg3) {
		WFLG3 = util.formatString(wflg3, 1);
	}
	public void setWFLG4(String wflg4) {
		WFLG4 = util.formatString(wflg4, 1);
	}
	public void setWVAR1(String wvar1) {
		WVAR1 = util.formatString(wvar1, 10);
	}
	public String getWCODSEG() {
		return WCODSEG;
	}
	public String getWCON1() {
		return WCON1;
	}
	public String getWCON2() {
		return WCON2;
	}
	public String getWCON3() {
		return WCON3;
	}
	public String getWFECPRO() {
		return WFECPRO;
	}
	public String getWFECSIS() {
		return WFECSIS;
	}
	public String getWFLG1() {
		return WFLG1;
	}
	public String getWFLG2() {
		return WFLG2;
	}
	public String getWFLG3() {
		return WFLG3;
	}
	public String getWFLG4() {
		return WFLG4;
	}
	public String getWFLGAPR() {
		return WFLGAPR;
	}
	public String getWFMT1() {
		return WFMT1;
	}
	public String getWFMT2() {
		return WFMT2;
	}
	public String getWFMT3() {
		return WFMT3;
	}
	public String getWHORA() {
		return WHORA;
	}
	public String getWLRG1() {
		return WLRG1;
	}
	public String getWLRG2() {
		return WLRG2;
	}
	public String getWLRG3() {
		return WLRG3;
	}
	public String getWLRGREG() {
		return WLRGREG;
	}
	public String getWNUMCTL() {
		return WNUMCTL;
	}
	public String getWNUMSEQ() {
		return WNUMSEQ;
	}
	public String getWNUMUSR() {
		return WNUMUSR;
	}
	public String getWOCU1() {
		return WOCU1;
	}
	public String getWOCU2() {
		return WOCU2;
	}
	public String getWOCU3() {
		return WOCU3;
	}
	public String getWPOST() {
		return WPOST;
	}
	public String getWREGDEP() {
		return WREGDEP;
	}
	public String getWREGFACB() {
		return WREGFACB;
	}
	public String getWREGFACO() {
		return WREGFACO;
	}
	public String getWREGLV() {
		return WREGLV;
	}
	public String getWREGMEM() {
		return WREGMEM;
	}
	public String getWREJ1() {
		return WREJ1;
	}
	public String getWREJ2() {
		return WREJ2;
	}
	public String getWREJ3() {
		return WREJ3;
	}
	public String getWREJ4() {
		return WREJ4;
	}
	public String getWREJ5() {
		return WREJ5;
	}
	public String getWTPOAPR() {
		return WTPOAPR;
	}
	public String getWTPODTA() {
		return WTPODTA;
	}
	public String getWTRNPEN() {
		return WTRNPEN;
	}
	public String getWVAR1() {
		return WVAR1;
	}
	public CabeceraOUT(){
	}
	public CabeceraOUT(String out){
		this.setWLRGREG(out.substring(0, 5));
		this.setWTPODTA(out.substring(5, 7));
		this.setWNUMCTL(out.substring(7, 12));
		this.setWNUMUSR(out.substring(12, 16));
		this.setWNUMSEQ(out.substring(16, 23));
		this.setWPOST(out.substring(23, 24));
		this.setWFLGAPR(out.substring(24, 25));
		this.setWTPOAPR(out.substring(25, 26));
		this.setWTRNPEN(out.substring(26, 27));
		this.setWFECPRO(out.substring(27, 35));
		this.setWFECSIS(out.substring(35, 43));
		this.setWHORA(out.substring(43, 49));
		this.setWCODSEG(out.substring(49, 60));
		this.setWFMT1(out.substring(60, 63));
		this.setWFMT2(out.substring(63, 66));
		this.setWFMT3(out.substring(66, 69));
		this.setWLRG1(out.substring(69, 74));
		this.setWLRG2(out.substring(74, 79));
		this.setWLRG3(out.substring(79, 84));
		this.setWOCU1(out.substring(84, 87));
		this.setWOCU2(out.substring(87, 90));
		this.setWOCU3(out.substring(90, 93));
		this.setWCON1(out.substring(93, 94));
		this.setWCON2(out.substring(94, 95));
		this.setWCON3(out.substring(95, 96));
		this.setWREGFACB(out.substring(96, 97));
		this.setWREGFACO(out.substring(97, 98));
		this.setWREGLV(out.substring(98, 99));
		this.setWREGDEP(out.substring(99, 100));
		this.setWREGMEM(out.substring(100, 101));
		this.setWREJ1(out.substring(101, 104));
		this.setWREJ2(out.substring(104, 107));
		this.setWREJ3(out.substring(107, 110));
		this.setWREJ4(out.substring(110, 113));
		this.setWREJ5(out.substring(113, 116));
		this.setWFLG1(out.substring(116, 117));
		this.setWFLG2(out.substring(117, 118));
		this.setWFLG3(out.substring(118, 119));
		this.setWFLG4(out.substring(119, 120));
		this.setWVAR1(out.substring(120, 130));
	}

	public void setGlobal(){
		this.setWLRGREG(CabeceraOUT.LARGO);
		this.setWTPODTA("");
		this.setWNUMCTL("");
		this.setWNUMUSR("");
		this.setWNUMSEQ("");
		this.setWPOST("");
		this.setWFLGAPR("");
		this.setWTPOAPR("");
		this.setWTRNPEN("");
		this.setWFECPRO("");
		this.setWFECSIS("");
		this.setWHORA("");
		this.setWCODSEG("");
		this.setWFMT1("");
		this.setWFMT2("");
		this.setWFMT3("");
		this.setWLRG1("");
		this.setWLRG2("");
		this.setWLRG3("");
		this.setWOCU1("");
		this.setWOCU2("");
		this.setWOCU3("");
		this.setWCON1("");
		this.setWCON2("");
		this.setWCON3("");
		this.setWREGFACB("");
		this.setWREGFACO("");
		this.setWREGLV("");
		this.setWREGDEP("");
		this.setWREGMEM("");
		this.setWREJ1("");
		this.setWREJ2("");
		this.setWREJ3("");
		this.setWREJ4("");
		this.setWREJ5("");
		this.setWFLG1("");
		this.setWFLG2("");
		this.setWFLG3("");
		this.setWFLG4("");
		this.setWVAR1("");
	}
	public String getCabeceraOUT(){
		return WLRGREG + WTPODTA + WNUMCTL + WNUMUSR + WNUMSEQ + WPOST + 
			WFLGAPR + WTPOAPR + WTRNPEN + WFECPRO + WFECSIS + WHORA + 
			WCODSEG + WFMT1 + WFMT2 + WFMT3 + WLRG1 + WLRG2 + WLRG3 + 
			WOCU1 + WOCU2 + WOCU3 + WCON1 + WCON2 + WCON3 + WREGFACB + 
			WREGFACO + WREGLV + WREGDEP + WREGMEM + WREJ1 + WREJ2 + 
			WREJ3 + WREJ4 + WREJ5 + WFLG1 + WFLG2 + WFLG3 + WFLG4 + WVAR1;
	}
}
