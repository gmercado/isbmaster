package bus.monitor.transaction.trxout;

import java.io.Serializable;

/**
 * Created by josanchez on 27/07/2016.
 */
public class RegOut120 implements Serializable {

    private static final long serialVersionUID = -1630250668810202977L;
    public static final String LARGO = "258"; //Modificado para obtener el ticket.
    public static final int LARGO_INT = Integer.parseInt(RegOut120.LARGO);

    public static final String REPRESENTATION = "120";

    private String WLRGREG;
    private String WIMPTOT;
    private String WIMPORTE;
    private String WIMPORTE2;
    private String WCARGO;
    private String WCLSCRG;
    private String WCOBCRG;
    private String WCOTACT;
    private String WCOMVEN;
    private String WORDCTA;
    private String WCODMON;
    private String WCODMON2;
    private String WIMPTE1;
    private String WIMPTE2;
    private String WTPOTE1;
    private String WTPOTE2;
    private String WNOMBRE;
    private String WSALACT;
    private String WNEGACT;
    private String WSALDIS;
    private String WNEGDIS;
    private String WNUMADI;
    private String WFECHA;
    private String WNOMBREA;

    public RegOut120(String reg) {
        WLRGREG = reg.substring(0,5);
        WIMPTOT = reg.substring(5,20);
        WIMPORTE = reg.substring(20,35);
        WIMPORTE2 = reg.substring(35,50);
        WCARGO = reg.substring(50,65);
        WCLSCRG = reg.substring(65,66);
        WCOBCRG = reg.substring(66,67);
        WCOTACT = reg.substring(67,78);
        WCOMVEN = reg.substring(78,79);
        WORDCTA = reg.substring(79,80);
        WCODMON = reg.substring(80,83);
        WCODMON2 = reg.substring(83,86);
        WIMPTE1 = reg.substring(86,101);
        WIMPTE2 = reg.substring(101,116);
        WTPOTE1 = reg.substring(116,117);
        WTPOTE2 = reg.substring(117,118);
        WNOMBRE = reg.substring(118,158);
        WSALACT = reg.substring(158,173);
        WNEGACT = reg.substring(173,174);
        WSALDIS = reg.substring(174,189);
        WNEGDIS = reg.substring(189,190);
        WNUMADI = reg.substring(190,200);
        WFECHA = reg.substring(200,208);
        WNOMBREA = reg.substring(208,248);

    }

    public String getWCARGO() {
        return WCARGO;
    }

    public String getWCLSCRG() {
        return WCLSCRG;
    }

    public String getWCOBCRG() {
        return WCOBCRG;
    }

    public String getWCODMON() {
        return WCODMON;
    }

    public String getWCODMON2() {
        return WCODMON2;
    }

    public String getWCOMVEN() {
        return WCOMVEN;
    }

    public String getWCOTACT() {
        return WCOTACT;
    }

    public String getWFECHA() {
        return WFECHA;
    }

    public String getWIMPORTE() {
        return WIMPORTE;
    }

    public String getWIMPORTE2() {
        return WIMPORTE2;
    }

    public String getWIMPTE1() {
        return WIMPTE1;
    }

    public String getWIMPTE2() {
        return WIMPTE2;
    }

    public String getWIMPTOT() {
        return WIMPTOT;
    }

    public String getWLRGREG() {
        return WLRGREG;
    }

    public String getWNEGACT() {
        return WNEGACT;
    }

    public String getWNEGDIS() {
        return WNEGDIS;
    }

    public String getWNOMBRE() {
        return WNOMBRE;
    }

    public String getWNOMBREA() {
        return WNOMBREA;
    }

    public String getWNUMADI() {
        return WNUMADI;
    }

    public String getWORDCTA() {
        return WORDCTA;
    }

    public String getWSALACT() {
        return WSALACT;
    }

    public String getWSALDIS() {
        return WSALDIS;
    }

    public String getWTPOTE1() {
        return WTPOTE1;
    }

    public String getWTPOTE2() {
        return WTPOTE2;
    }
    public String getRegOur120(){
        StringBuffer sb = new StringBuffer("");
        sb.append(WLRGREG);
        sb.append(WIMPTOT);
        sb.append(WIMPORTE);
        sb.append(WIMPORTE2);
        sb.append(WCARGO);
        sb.append(WCLSCRG);
        sb.append(WCOBCRG);
        sb.append(WCOTACT);
        sb.append(WCOMVEN);
        sb.append(WORDCTA);
        sb.append(WCODMON);
        sb.append(WCODMON2);
        sb.append(WIMPTE1);
        sb.append(WIMPTE2);
        sb.append(WTPOTE1);
        sb.append(WTPOTE2);
        sb.append(WNOMBRE);
        sb.append(WSALACT);
        sb.append(WNEGACT);
        sb.append(WSALDIS);
        sb.append(WNEGDIS);
        sb.append(WNUMADI);
        sb.append(WFECHA);
        sb.append(WNOMBREA);
        return sb.toString();
    }


}
