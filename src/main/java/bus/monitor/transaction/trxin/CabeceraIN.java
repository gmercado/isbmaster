package bus.monitor.transaction.trxin;

import bus.monitor.transaction.Utiles;

import java.io.Serializable;

/**
 * Created by josanchez on 27/07/2016.
 */
public class CabeceraIN implements Serializable {

    private static final long serialVersionUID = 5373323641469049480L;

    public static final String LARGO = "161";
    public static final int LARGO_INT = Integer.parseInt(LARGO);
    private final String ver = "116";

    private Utiles util = new Utiles();

    private String WLRGREG;
    private String WNUMUSR;
    private String WNUMSEQ;
    private String WNUMCTL;
    private String WTPODTA;
    private String WCODTRN;
    private String WDEVICE;
    private String WUSUARIO;
    private String WCLAVE;
    private String WAPRLCL;
    private String WAPRUSER;
    private String WAPRPWD;
    private String WSUPER;
    private String WVERIF;
    private String WDESATEN;
    private String WREVE;
    private String WCOBCRG;
    private String WDEBCAS;
    private String WCANAL;
    private String WREIMP;
    private String WFECORI;
    private String WHORORI;
    private String WFMT1;
    private String WFMT2;
    private String WFMT3;
    private String WLRG1;
    private String WLRG2;
    private String WLRG3;
    private String WOCU1;
    private String WOCU2;
    private String WOCU3;
    private String WCON1;
    private String WCON2;
    private String WCON3;
    private String WREGLV;
    private String WREGCHE;
    private String WREGDEP;
    private String WVER;
    private String WAPLTP;
    private String WFLG1;
    private String WFLG2;
    private String WFLG3;
    private String WFLG4;
    private String WVAR1;

    public void setWLRGREG(String wlrgreg) {
        WLRGREG = util.formatNumber(wlrgreg, 5);
    }
    public void setWNUMUSR(String wnumusr) {
        WNUMUSR = util.formatNumber(wnumusr, 4);
    }
    public void setWNUMSEQ(String wnumseq) {
        WNUMSEQ = util.formatNumber(wnumseq, 7);
    }
    public void setWNUMCTL(String wnumctl) {
        WNUMCTL = util.formatNumber(wnumctl, 5);
    }
    public void setWTPODTA(String wtpodta) {
        WTPODTA = util.formatNumber(wtpodta, 2);
    }
    public void setWCODTRN(String wcodtrn) {
        WCODTRN = util.formatString(wcodtrn, 4);
    }
    public void setWDEVICE(String wdevice) {
        WDEVICE = util.formatString(wdevice, 10);
    }
    public void setWUSUARIO(String wusuario) {
        WUSUARIO = util.formatString(wusuario, 10);
    }
    public void setWCLAVE(String wclave) {
        WCLAVE = util.formatString(wclave, 10);
    }
    public void setWAPRLCL(String waprlcl) {
        WAPRLCL = util.formatNumber(waprlcl, 4);
    }
    public void setWAPRUSER(String wapruser) {
        WAPRUSER = util.formatString(wapruser, 10);
    }
    public void setWAPRPWD(String waprpwd) {
        WAPRPWD = util.formatString(waprpwd, 10);
    }
    public void setWSUPER(String wsuper) {
        WSUPER = util.formatString(wsuper, 1);
    }
    public void setWVERIF(String wverif) {
        WVERIF = util.formatString(wverif, 1);
    }
    public void setWDESATEN(String wdesaten) {
        WDESATEN = util.formatString(wdesaten, 1);
    }
    public void setWREVE(String wreve) {
        WREVE = util.formatString(wreve, 1);
    }
    public void setWCOBCRG(String wcobcrg) {
        WCOBCRG = util.formatString(wcobcrg, 1);
    }
    public void setWDEBCAS(String wdebcas) {
        WDEBCAS = util.formatString(wdebcas, 1);
    }
    public void setWCANAL(String wcanal) {
        WCANAL = util.formatString(wcanal, 1);
    }
    public void setWREIMP(String wreimp) {
        WREIMP = util.formatString(wreimp, 1);
    }
    public void setWFECORI(String wfecori) {
        WFECORI = util.formatNumber(wfecori, 8);
    }
    public void setWHORORI(String whorori) {
        WHORORI = util.formatNumber(whorori, 6);
    }
    public void setWFMT1(String wfmt1) {
        WFMT1 = util.formatNumber(wfmt1, 3);
    }
    public void setWFMT2(String wfmt2) {
        WFMT2 = util.formatNumber(wfmt2, 3);
    }
    public void setWFMT3(String wfmt3) {
        WFMT3 = util.formatNumber(wfmt3, 3);
    }
    public void setWLRG1(String wlrg1) {
        WLRG1 = util.formatNumber(wlrg1, 5);
    }
    public void setWLRG2(String wlrg2) {
        WLRG2 = util.formatNumber(wlrg2, 5);
    }
    public void setWLRG3(String wlrg3) {
        WLRG3 = util.formatNumber(wlrg3, 5);
    }
    public void setWOCU1(String wocu1) {
        WOCU1 = util.formatNumber(wocu1, 3);
    }
    public void setWOCU2(String wocu2) {
        WOCU2 = util.formatNumber(wocu2, 3);
    }
    public void setWOCU3(String wocu3) {
        WOCU3 = util.formatNumber(wocu3, 3);
    }
    public void setWCON1(String wcon1) {
        WCON1 = util.formatString(wcon1, 1);
    }
    public void setWCON2(String wcon2) {
        WCON2 = util.formatString(wcon2, 1);
    }
    public void setWCON3(String wcon3) {
        WCON3 = util.formatString(wcon3, 1);
    }
    public void setWREGLV(String wreglv) {
        WREGLV = util.formatString(wreglv, 1);
    }
    public void setWREGCHE(String wregche) {
        WREGCHE = util.formatNumber(wregche, 1);
    }
    public void setWREGDEP(String wregdep) {
        WREGDEP = util.formatString(wregdep, 1);
    }
    public void setWVER(String wver) {
        WVER = util.formatNumber(wver, 4);
    }
    public void setWAPLTP(String wapltp) {
        WAPLTP = util.formatString(wapltp, 1);
    }
    public void setWFLG1(String wflg1) {
        WFLG1 = util.formatString(wflg1, 1);
    }
    public void setWFLG2(String wflg2) {
        WFLG2 = util.formatString(wflg2, 1);
    }
    public void setWFLG3(String wflg3) {
        WFLG3 = util.formatString(wflg3, 1);
    }
    public void setWFLG4(String wflg4) {
        WFLG4 = util.formatString(wflg4, 1);
    }
    public void setWVAR1(String wvar1) {
        WVAR1 = util.formatString(wvar1, 10);
    }

    public String getWAPLTP() {
        return WAPLTP;
    }
    public String getWAPRLCL() {
        return WAPRLCL;
    }
    public String getWAPRPWD() {
        return WAPRPWD;
    }
    public String getWAPRUSER() {
        return WAPRUSER;
    }
    public String getWCANAL() {
        return WCANAL;
    }
    public String getWCLAVE() {
        return WCLAVE;
    }
    public String getWCOBCRG() {
        return WCOBCRG;
    }
    public String getWCODTRN() {
        return WCODTRN;
    }
    public String getWCON1() {
        return WCON1;
    }
    public String getWCON2() {
        return WCON2;
    }
    public String getWCON3() {
        return WCON3;
    }
    public String getWDEBCAS() {
        return WDEBCAS;
    }
    public String getWDESATEN() {
        return WDESATEN;
    }
    public String getWDEVICE() {
        return WDEVICE;
    }
    public String getWFECORI() {
        return WFECORI;
    }
    public String getWFLG1() {
        return WFLG1;
    }
    public String getWFLG2() {
        return WFLG2;
    }
    public String getWFLG3() {
        return WFLG3;
    }
    public String getWFLG4() {
        return WFLG4;
    }
    public String getWFMT1() {
        return WFMT1;
    }
    public String getWFMT2() {
        return WFMT2;
    }
    public String getWFMT3() {
        return WFMT3;
    }
    public String getWHORORI() {
        return WHORORI;
    }
    public String getWLRG1() {
        return WLRG1;
    }
    public String getWLRG2() {
        return WLRG2;
    }
    public String getWLRG3() {
        return WLRG3;
    }
    public String getWLRGREG() {
        return WLRGREG;
    }
    public String getWNUMCTL() {
        return WNUMCTL;
    }
    public String getWNUMSEQ() {
        return WNUMSEQ;
    }
    public String getWNUMUSR() {
        return WNUMUSR;
    }
    public String getWOCU1() {
        return WOCU1;
    }
    public String getWOCU2() {
        return WOCU2;
    }
    public String getWOCU3() {
        return WOCU3;
    }
    public String getWREGCHE() {
        return WREGCHE;
    }
    public String getWREGDEP() {
        return WREGDEP;
    }
    public String getWREGLV() {
        return WREGLV;
    }
    public String getWREIMP() {
        return WREIMP;
    }
    public String getWREVE() {
        return WREVE;
    }
    public String getWSUPER() {
        return WSUPER;
    }
    public String getWTPODTA() {
        return WTPODTA;
    }
    public String getWUSUARIO() {
        return WUSUARIO;
    }
    public String getWVAR1() {
        return WVAR1;
    }
    public String getWVER() {
        return WVER;
    }
    public String getWVERIF() {
        return WVERIF;
    }
    public CabeceraIN(){
    }
    public CabeceraIN(String in){
        this.setWLRGREG(in.substring(0, 5));
        this.setWNUMUSR(in.substring(5, 9));
        this.setWNUMSEQ(in.substring(9, 16));
        this.setWNUMCTL(in.substring(16, 21));
        this.setWTPODTA(in.substring(21, 23));
        this.setWCODTRN(in.substring(23, 27));
        this.setWDEVICE(in.substring(27, 37));
        this.setWUSUARIO(in.substring(37, 47));
        this.setWCLAVE(in.substring(47, 57));
        this.setWAPRLCL(in.substring(57, 61));
        this.setWAPRUSER(in.substring(61, 71));
        this.setWAPRPWD(in.substring(71, 81));
        this.setWSUPER(in.substring(81, 82));
        this.setWVERIF(in.substring(82, 83));
        this.setWDESATEN(in.substring(83, 84));
        this.setWREVE(in.substring(84, 85));
        this.setWCOBCRG(in.substring(85, 86));
        this.setWDEBCAS(in.substring(86, 87));
        this.setWCANAL(in.substring(87, 88));
        this.setWREIMP(in.substring(88, 89));
        this.setWFECORI(in.substring(89, 97));
        this.setWHORORI(in.substring(97, 103));
        this.setWFMT1(in.substring(103, 106));
        this.setWFMT2(in.substring(106, 109));
        this.setWFMT3(in.substring(109, 112));
        this.setWLRG1(in.substring(112, 117));
        this.setWLRG2(in.substring(117, 122));
        this.setWLRG3(in.substring(122, 127));
        this.setWOCU1(in.substring(127, 130));
        this.setWOCU2(in.substring(130, 133));
        this.setWOCU3(in.substring(133, 136));
        this.setWCON1(in.substring(136, 137));
        this.setWCON2(in.substring(137, 138));
        this.setWCON3(in.substring(138, 139));
        this.setWREGLV(in.substring(139, 140));
        this.setWREGCHE(in.substring(140, 141));
        this.setWREGDEP(in.substring(141, 142));
        this.setWVER(in.substring(142, 146));
        this.setWAPLTP(in.substring(146, 147));
        this.setWFLG1(in.substring(147, 148));
        this.setWFLG2(in.substring(148, 149));
        this.setWFLG3(in.substring(149, 150));
        this.setWFLG4(in.substring(150, 151));
        this.setWVAR1(in.substring(151, 161));
    }
    public void setGlobal(){
        this.setWLRGREG(CabeceraIN.LARGO);
        this.setWNUMUSR("5101");
        this.setWNUMSEQ("0");
        this.setWNUMCTL(util.genNumControl());
        this.setWTPODTA("1");
        this.setWCODTRN("");
        this.setWDEVICE(util.genNunDevice());
        this.setWUSUARIO("");
        this.setWCLAVE("");
        this.setWAPRLCL("");
        this.setWAPRUSER("EBANKING");
        this.setWAPRPWD("");
        this.setWSUPER("");
        this.setWVERIF("S");
        this.setWDESATEN("");
        this.setWREVE("");
        this.setWCOBCRG("");
        this.setWDEBCAS("");
        this.setWCANAL("5");
        this.setWREIMP("");
        this.setWFECORI("");
        this.setWHORORI("");
        this.setWFMT1("");
        this.setWFMT2("");
        this.setWFMT3("");
        this.setWLRG1("");
        this.setWLRG2("");
        this.setWLRG3("");
        this.setWOCU1("");
        this.setWOCU2("");
        this.setWOCU3("");
        this.setWCON1("");
        this.setWCON2("");
        this.setWCON3("");
        this.setWREGLV("");
        this.setWREGCHE("");
        this.setWREGDEP("");
        this.setWVER(ver);
        this.setWAPLTP("S");
        this.setWFLG1("");
        this.setWFLG2("");
        this.setWFLG3("");
        this.setWFLG4("");
        this.setWVAR1("");
    }
    public String getCabeceraIN(){
        return WLRGREG + WNUMUSR + WNUMSEQ + WNUMCTL + WTPODTA + WCODTRN +
                WDEVICE + WUSUARIO + WCLAVE + WAPRLCL + WAPRUSER + WAPRPWD +
                WSUPER + WVERIF + WDESATEN + WREVE + WCOBCRG + WDEBCAS +
                WCANAL + WREIMP + WFECORI + WHORORI + WFMT1 + WFMT2 + WFMT3 +
                WLRG1 + WLRG2 + WLRG3 + WOCU1 + WOCU2 + WOCU3 + WCON1 + WCON2 +
                WCON3 + WREGLV + WREGCHE + WREGDEP + WVER + WAPLTP + WFLG1 +
                WFLG2 + WFLG3 + WFLG4 + WVAR1;
    }


    @Override
    public String toString() {
        return "CabeceraIN{" +
                "ver='" + ver + '\'' +
                ", util=" + util +
                ", WLRGREG='" + WLRGREG + '\'' +
                ", WNUMUSR='" + WNUMUSR + '\'' +
                ", WNUMSEQ='" + WNUMSEQ + '\'' +
                ", WNUMCTL='" + WNUMCTL + '\'' +
                ", WTPODTA='" + WTPODTA + '\'' +
                ", WCODTRN='" + WCODTRN + '\'' +
                ", WDEVICE='" + WDEVICE + '\'' +
                ", WUSUARIO='" + WUSUARIO + '\'' +
                ", WCLAVE='" + WCLAVE + '\'' +
                ", WAPRLCL='" + WAPRLCL + '\'' +
                ", WAPRUSER='" + WAPRUSER + '\'' +
                ", WAPRPWD='" + WAPRPWD + '\'' +
                ", WSUPER='" + WSUPER + '\'' +
                ", WVERIF='" + WVERIF + '\'' +
                ", WDESATEN='" + WDESATEN + '\'' +
                ", WREVE='" + WREVE + '\'' +
                ", WCOBCRG='" + WCOBCRG + '\'' +
                ", WDEBCAS='" + WDEBCAS + '\'' +
                ", WCANAL='" + WCANAL + '\'' +
                ", WREIMP='" + WREIMP + '\'' +
                ", WFECORI='" + WFECORI + '\'' +
                ", WHORORI='" + WHORORI + '\'' +
                ", WFMT1='" + WFMT1 + '\'' +
                ", WFMT2='" + WFMT2 + '\'' +
                ", WFMT3='" + WFMT3 + '\'' +
                ", WLRG1='" + WLRG1 + '\'' +
                ", WLRG2='" + WLRG2 + '\'' +
                ", WLRG3='" + WLRG3 + '\'' +
                ", WOCU1='" + WOCU1 + '\'' +
                ", WOCU2='" + WOCU2 + '\'' +
                ", WOCU3='" + WOCU3 + '\'' +
                ", WCON1='" + WCON1 + '\'' +
                ", WCON2='" + WCON2 + '\'' +
                ", WCON3='" + WCON3 + '\'' +
                ", WREGLV='" + WREGLV + '\'' +
                ", WREGCHE='" + WREGCHE + '\'' +
                ", WREGDEP='" + WREGDEP + '\'' +
                ", WVER='" + WVER + '\'' +
                ", WAPLTP='" + WAPLTP + '\'' +
                ", WFLG1='" + WFLG1 + '\'' +
                ", WFLG2='" + WFLG2 + '\'' +
                ", WFLG3='" + WFLG3 + '\'' +
                ", WFLG4='" + WFLG4 + '\'' +
                ", WVAR1='" + WVAR1 + '\'' +
                '}';
    }
}
