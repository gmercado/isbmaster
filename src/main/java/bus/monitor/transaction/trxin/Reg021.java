package bus.monitor.transaction.trxin;

import bus.monitor.transaction.Utiles;

import java.io.Serializable;

/**
 * Created by josanchez on 27/07/2016.
 */
public class Reg021 implements Serializable {
    private static final long serialVersionUID = 1978151562936110613L;
    private Utiles util = new Utiles();
    public static final String LARGO = "195";
    public static final String REPRESENTATION = "021";

    private String WLRGREG;
    private String WCLSTRN;
    private String WIMPTOT;
    private String WTPOIMP;
    private String WCUENTA;
    private String WTPOCTA;
    private String WCODMON;
    private String WIMPORTE;
    private String WNUMCOMP;
    private String WCUENTA2;
    private String WTPOCTA2;
    private String WCODMON2;
    private String WIMPORTE2;
    private String WNUMCOMP2;
    private String WIMPORTEA;
    private String WCOTCLI;
    private String WFECHA;
    private String WPLAZA;
    private String WCODADI;
    private String WNOTA;
    private String WTICKET;
    private CabeceraIN in;

    public CabeceraIN getIn() {
        return in;
    }
    public void setIn(CabeceraIN in) {
        this.in = in;
    }
    public void setWLRGREG(String wlrgreg) {
        WLRGREG = util.formatNumber(wlrgreg, 5);
    }
    public void setWCLSTRN(String wclstrn) {
        WCLSTRN = util.formatNumber(wclstrn, 1);
    }
    public void setWIMPTOT(String wimptot) {
        WIMPTOT = util.formatNumber(wimptot, 15);
    }
    public void setWTPOIMP(String wtpoimp) {
        WTPOIMP = util.formatString(wtpoimp, 1);
    }
    public void setWCUENTA(String wcuenta) {
        WCUENTA = util.formatNumber(wcuenta, 10);
    }
    public void setWTPOCTA(String wtpocta) {
        WTPOCTA = util.formatString(wtpocta, 1);
    }
    public void setWCODMON(String wcodmon) {
        WCODMON = util.formatNumber(wcodmon, 3);
    }
    public void setWIMPORTE(String wimporte) {
        WIMPORTE = util.formatNumber(wimporte, 15);
    }
    public void setWNUMCOMP(String wnumcomp) {
        WNUMCOMP = util.formatNumber(wnumcomp, 10);
    }
    public void setWCUENTA2(String wcuenta2) {
        WCUENTA2 = util.formatNumber(wcuenta2, 10);
    }
    public void setWTPOCTA2(String wtpocta2) {
        WTPOCTA2 = util.formatString(wtpocta2, 1);
    }
    public void setWCODMON2(String wcodmon2) {
        WCODMON2 = util.formatNumber(wcodmon2, 3);
    }
    public void setWIMPORTE2(String wimporte2) {
        WIMPORTE2 = util.formatNumber(wimporte2, 15);
    }
    public void setWNUMCOMP2(String wnumcomp2) {
        WNUMCOMP2 = util.formatNumber(wnumcomp2, 10);
    }
    public void setWIMPORTEA(String wimportea) {
        WIMPORTEA = util.formatNumber(wimportea, 15);
    }
    public void setWCOTCLI(String wcotcli) {
        WCOTCLI = util.formatNumber(wcotcli, 11);
    }
    public void setWFECHA(String wfecha) {
        WFECHA = util.formatNumber(wfecha, 8);
    }
    public void setWPLAZA(String wplaza) {
        WPLAZA = util.formatNumber(wplaza, 2);
    }
    public void setWCODADI(String wcodadi) {
        WCODADI = util.formatNumber(wcodadi, 19);
    }
    public void setWNOTA(String wnota) {
        WNOTA = util.formatString(wnota, 30);
    }

    public String getWCLSTRN() {
        return WCLSTRN;
    }
    public String getWCODADI() {
        return WCODADI;
    }
    public String getWCODMON() {
        return WCODMON;
    }
    public String getWCODMON2() {
        return WCODMON2;
    }
    public String getWCOTCLI() {
        return WCOTCLI;
    }
    public String getWCUENTA() {
        return WCUENTA;
    }
    public String getWCUENTA2() {
        return WCUENTA2;
    }
    public String getWFECHA() {
        return WFECHA;
    }
    public String getWIMPORTE() {
        return WIMPORTE;
    }
    public String getWIMPORTE2() {
        return WIMPORTE2;
    }
    public String getWIMPORTEA() {
        return WIMPORTEA;
    }
    public String getWIMPTOT() {
        return WIMPTOT;
    }
    public String getWLRGREG() {
        return WLRGREG;
    }
    public String getWNOTA() {
        return WNOTA;
    }
    public String getWNUMCOMP() {
        return WNUMCOMP;
    }
    public String getWNUMCOMP2() {
        return WNUMCOMP2;
    }
    public String getWPLAZA() {
        return WPLAZA;
    }
    public String getWTPOCTA() {
        return WTPOCTA;
    }
    public String getWTPOCTA2() {
        return WTPOCTA2;
    }
    public String getWTPOIMP() {
        return WTPOIMP;
    }
    public Reg021(){
    }
    public Reg021(String reg) {
        this.setWLRGREG(reg.substring(0,5));
        this.setWCLSTRN(reg.substring(5,6));
        this.setWIMPTOT(reg.substring(6,21));
        this.setWTPOIMP(reg.substring(21,22));
        this.setWCUENTA(reg.substring(22,32));
        this.setWTPOCTA(reg.substring(32,33));
        this.setWCODMON(reg.substring(33,36));
        this.setWIMPORTE(reg.substring(36,51));
        this.setWNUMCOMP(reg.substring(51,61));
        this.setWCUENTA2(reg.substring(61,71));
        this.setWTPOCTA2(reg.substring(71,72));
        this.setWCODMON2(reg.substring(72,75));
        this.setWIMPORTE2(reg.substring(75,90));
        this.setWNUMCOMP2(reg.substring(90,100));
        this.setWIMPORTEA(reg.substring(100,115));
        this.setWCOTCLI(reg.substring(115,126));
        this.setWFECHA(reg.substring(126,134));
        this.setWPLAZA(reg.substring(134,136));
        this.setWCODADI(reg.substring(136,155));
        this.setWNOTA(reg.substring(155,185));
    }
    public void setGlobal(){
        this.setWLRGREG(Reg021.LARGO);
        this.setWCLSTRN("");
        this.setWIMPTOT("");
        this.setWTPOIMP("");
        this.setWCUENTA("");
        this.setWTPOCTA("");
        this.setWCODMON("");
        this.setWIMPORTE("");
        this.setWNUMCOMP("");
        this.setWCUENTA2("");
        this.setWTPOCTA2("");
        this.setWCODMON2("");
        this.setWIMPORTE2("");
        this.setWNUMCOMP2("");
        this.setWIMPORTEA("");
        this.setWCOTCLI("");
        this.setWFECHA("");
        this.setWPLAZA("");
        this.setWCODADI("");
        this.setWNOTA("");

    }
    public String getReg021(){
        return WLRGREG + WCLSTRN + WIMPTOT + WTPOIMP + WCUENTA + WTPOCTA + WCODMON + WIMPORTE + WNUMCOMP + WCUENTA2 + WTPOCTA2 +
                WCODMON2 + WIMPORTE2 + WNUMCOMP2 + WIMPORTEA + WCOTCLI + WFECHA + WPLAZA + WCODADI + WNOTA + WTICKET;
    }

    @Override
    public String toString() {
        return "Reg021{" +
                "WLRGREG='" + WLRGREG + '\'' +
                ", WCLSTRN='" + WCLSTRN + '\'' +
                ", WIMPTOT='" + WIMPTOT + '\'' +
                ", WTPOIMP='" + WTPOIMP + '\'' +
                ", WCUENTA='" + WCUENTA + '\'' +
                ", WTPOCTA='" + WTPOCTA + '\'' +
                ", WCODMON='" + WCODMON + '\'' +
                ", WIMPORTE='" + WIMPORTE + '\'' +
                ", WNUMCOMP='" + WNUMCOMP + '\'' +
                ", WCUENTA2='" + WCUENTA2 + '\'' +
                ", WTPOCTA2='" + WTPOCTA2 + '\'' +
                ", WCODMON2='" + WCODMON2 + '\'' +
                ", WIMPORTE2='" + WIMPORTE2 + '\'' +
                ", WNUMCOMP2='" + WNUMCOMP2 + '\'' +
                ", WIMPORTEA='" + WIMPORTEA + '\'' +
                ", WCOTCLI='" + WCOTCLI + '\'' +
                ", WFECHA='" + WFECHA + '\'' +
                ", WPLAZA='" + WPLAZA + '\'' +
                ", WCODADI='" + WCODADI + '\'' +
                ", WNOTA='" + WNOTA + '\'' +
                ", WTICKET='" + WTICKET + '\'' +
                ", in=" + in +
                '}';
    }
}


