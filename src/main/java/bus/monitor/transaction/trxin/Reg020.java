package bus.monitor.transaction.trxin;

import java.io.Serializable;

/**
 * @author by rsalvatierra on 25/10/2017.
 */
public class Reg020 implements Serializable {
    private String WLRGREG;
    private String WCODBCO;
    private String WCODSUC;
    private String WCTABEN;
    private String WNOMBEN;
    private String WNOMREM;
    private String WTPODOC;
    private String WNRODOC;
    private String WDTAADI;
    private String WTPOCTA;

    public String getWLRGREG() {
        return WLRGREG;
    }

    public void setWLRGREG(String WLRGREG) {
        this.WLRGREG = WLRGREG;
    }

    public String getWCODBCO() {
        return WCODBCO;
    }

    public void setWCODBCO(String WCODBCO) {
        this.WCODBCO = WCODBCO;
    }

    public String getWCODSUC() {
        return WCODSUC;
    }

    public void setWCODSUC(String WCODSUC) {
        this.WCODSUC = WCODSUC;
    }

    public String getWCTABEN() {
        return WCTABEN;
    }

    public void setWCTABEN(String WCTABEN) {
        this.WCTABEN = WCTABEN;
    }

    public String getWNOMBEN() {
        return WNOMBEN;
    }

    public void setWNOMBEN(String WNOMBEN) {
        this.WNOMBEN = WNOMBEN;
    }

    public String getWNOMREM() {
        return WNOMREM;
    }

    public void setWNOMREM(String WNOMREM) {
        this.WNOMREM = WNOMREM;
    }

    public String getWTPODOC() {
        return WTPODOC;
    }

    public void setWTPODOC(String WTPODOC) {
        this.WTPODOC = WTPODOC;
    }

    public String getWNRODOC() {
        return WNRODOC;
    }

    public void setWNRODOC(String WNRODOC) {
        this.WNRODOC = WNRODOC;
    }

    public String getWDTAADI() {
        return WDTAADI;
    }

    public void setWDTAADI(String WDTAADI) {
        this.WDTAADI = WDTAADI;
    }

    public String getWTPOCTA() {
        return WTPOCTA;
    }

    public void setWTPOCTA(String WTPOCTA) {
        this.WTPOCTA = WTPOCTA;
    }
}
