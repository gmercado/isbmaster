/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.monitor.model;

import bus.monitor.entities.TransaccionAudit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.LinkedList;

/**
 * @author Marcelo Morales
 *         Created: 12/28/11 8:57 AM
 */
public final class PSR9101Bean<X extends Serializable, Y extends Serializable, Z extends Serializable> implements Serializable {

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 0), WLRGREG
    private BigDecimal WLRGREG;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), WTPODTA
    private BigDecimal WTPODTA;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 0), WNUMCTL
    private BigDecimal WNUMCTL;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), WNUMUSR
    private BigDecimal WNUMUSR;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), WNUMSEQ
    private BigDecimal WNUMSEQ;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WPOST
    private String WPOST;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WFLGAPR
    private String WFLGAPR;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WTPOAPR
    private String WTPOAPR;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WTRNPEN
    private String WTRNPEN;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(8, 0), WFECPRO
    private BigDecimal WFECPRO;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(8, 0), WFECSIS
    private BigDecimal WFECSIS;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), WHORA
    private BigDecimal WHORA;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(11, 0), WCODSEG
    private BigDecimal WCODSEG;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), WFMT1
    private BigDecimal WFMT1;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), WFMT2
    private BigDecimal WFMT2;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), WFMT3
    private BigDecimal WFMT3;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 0), WLRG1
    private BigDecimal WLRG1;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 0), WLRG2
    private BigDecimal WLRG2;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 0), WLRG3
    private BigDecimal WLRG3;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), WOCU1
    private BigDecimal WOCU1;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), WOCU2
    private BigDecimal WOCU2;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), WOCU3
    private BigDecimal WOCU3;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WCON1
    private String WCON1;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WCON2
    private String WCON2;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WCON3
    private String WCON3;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), WREGFACB
    private BigDecimal WREGFACB;
    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), WREGFACO
    private BigDecimal WREGFACO;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WREGLV
    private String WREGLV;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WREGDEP
    private String WREGDEP;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WREGMEM
    private String WREGMEM;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(3, 37),  WREJ1
    private String WREJ1;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(3, 37),  WREJ2
    private String WREJ2;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(3, 37),  WREJ3
    private String WREJ3;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(3, 37),  WREJ4
    private String WREJ4;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(3, 37),  WREJ5
    private String WREJ5;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WFLG1
    private String WFLG1;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WFLG2
    private String WFLG2;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WFLG3
    private String WFLG3;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WFLG4
    private String WFLG4;
    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(10, 37),  WVAR1
    private String WVAR1;

    private LinkedHashMap<String, Object> entrada;

    private final LinkedList<X> cuerpo1 = new LinkedList<>();
    private final LinkedList<Y> cuerpo2 = new LinkedList<>();
    private final LinkedList<Z> cuerpo3 = new LinkedList<>();

    private TransaccionAudit transaccionAudit;

    public BigDecimal getWLRGREG() {
        return WLRGREG;
    }

    public void setWLRGREG(BigDecimal WLRGREG) {
        this.WLRGREG = WLRGREG;
    }

    public BigDecimal getWTPODTA() {
        return WTPODTA;
    }

    public void setWTPODTA(BigDecimal WTPODTA) {
        this.WTPODTA = WTPODTA;
    }

    public BigDecimal getWNUMCTL() {
        return WNUMCTL;
    }

    public void setWNUMCTL(BigDecimal WNUMCTL) {
        this.WNUMCTL = WNUMCTL;
    }

    public BigDecimal getWNUMUSR() {
        return WNUMUSR;
    }

    public void setWNUMUSR(BigDecimal WNUMUSR) {
        this.WNUMUSR = WNUMUSR;
    }

    public BigDecimal getWNUMSEQ() {
        return WNUMSEQ;
    }

    public void setWNUMSEQ(BigDecimal WNUMSEQ) {
        this.WNUMSEQ = WNUMSEQ;
    }

    public String getWPOST() {
        return WPOST;
    }

    public void setWPOST(String WPOST) {
        this.WPOST = WPOST;
    }

    public String getWFLGAPR() {
        return WFLGAPR;
    }

    public void setWFLGAPR(String WFLGAPR) {
        this.WFLGAPR = WFLGAPR;
    }

    public String getWTPOAPR() {
        return WTPOAPR;
    }

    public void setWTPOAPR(String WTPOAPR) {
        this.WTPOAPR = WTPOAPR;
    }

    public String getWTRNPEN() {
        return WTRNPEN;
    }

    public void setWTRNPEN(String WTRNPEN) {
        this.WTRNPEN = WTRNPEN;
    }

    public BigDecimal getWFECPRO() {
        return WFECPRO;
    }

    public void setWFECPRO(BigDecimal WFECPRO) {
        this.WFECPRO = WFECPRO;
    }

    public BigDecimal getWFECSIS() {
        return WFECSIS;
    }

    public void setWFECSIS(BigDecimal WFECSIS) {
        this.WFECSIS = WFECSIS;
    }

    public BigDecimal getWHORA() {
        return WHORA;
    }

    public void setWHORA(BigDecimal WHORA) {
        this.WHORA = WHORA;
    }

    public BigDecimal getWCODSEG() {
        return WCODSEG;
    }

    public void setWCODSEG(BigDecimal WCODSEG) {
        this.WCODSEG = WCODSEG;
    }

    public BigDecimal getWFMT1() {
        return WFMT1;
    }

    public void setWFMT1(BigDecimal WFMT1) {
        this.WFMT1 = WFMT1;
    }

    public BigDecimal getWFMT2() {
        return WFMT2;
    }

    public void setWFMT2(BigDecimal WFMT2) {
        this.WFMT2 = WFMT2;
    }

    public BigDecimal getWFMT3() {
        return WFMT3;
    }

    public void setWFMT3(BigDecimal WFMT3) {
        this.WFMT3 = WFMT3;
    }

    public BigDecimal getWLRG1() {
        return WLRG1;
    }

    public void setWLRG1(BigDecimal WLRG1) {
        this.WLRG1 = WLRG1;
    }

    public BigDecimal getWLRG2() {
        return WLRG2;
    }

    public void setWLRG2(BigDecimal WLRG2) {
        this.WLRG2 = WLRG2;
    }

    public BigDecimal getWLRG3() {
        return WLRG3;
    }

    public void setWLRG3(BigDecimal WLRG3) {
        this.WLRG3 = WLRG3;
    }

    public BigDecimal getWOCU1() {
        return WOCU1;
    }

    public void setWOCU1(BigDecimal WOCU1) {
        this.WOCU1 = WOCU1;
    }

    public BigDecimal getWOCU2() {
        return WOCU2;
    }

    public void setWOCU2(BigDecimal WOCU2) {
        this.WOCU2 = WOCU2;
    }

    public BigDecimal getWOCU3() {
        return WOCU3;
    }

    public void setWOCU3(BigDecimal WOCU3) {
        this.WOCU3 = WOCU3;
    }

    public String getWCON1() {
        return WCON1;
    }

    public void setWCON1(String WCON1) {
        this.WCON1 = WCON1;
    }

    public String getWCON2() {
        return WCON2;
    }

    public void setWCON2(String WCON2) {
        this.WCON2 = WCON2;
    }

    public String getWCON3() {
        return WCON3;
    }

    public void setWCON3(String WCON3) {
        this.WCON3 = WCON3;
    }

    public BigDecimal getWREGFACB() {
        return WREGFACB;
    }

    public void setWREGFACB(BigDecimal WREGFACB) {
        this.WREGFACB = WREGFACB;
    }

    public BigDecimal getWREGFACO() {
        return WREGFACO;
    }

    public void setWREGFACO(BigDecimal WREGFACO) {
        this.WREGFACO = WREGFACO;
    }

    public String getWREGLV() {
        return WREGLV;
    }

    public void setWREGLV(String WREGLV) {
        this.WREGLV = WREGLV;
    }

    public String getWREGDEP() {
        return WREGDEP;
    }

    public void setWREGDEP(String WREGDEP) {
        this.WREGDEP = WREGDEP;
    }

    public String getWREGMEM() {
        return WREGMEM;
    }

    public void setWREGMEM(String WREGMEM) {
        this.WREGMEM = WREGMEM;
    }

    public String getWREJ1() {
        return WREJ1;
    }

    public void setWREJ1(String WREJ1) {
        this.WREJ1 = WREJ1;
    }

    public String getWREJ2() {
        return WREJ2;
    }

    public void setWREJ2(String WREJ2) {
        this.WREJ2 = WREJ2;
    }

    public String getWREJ3() {
        return WREJ3;
    }

    public void setWREJ3(String WREJ3) {
        this.WREJ3 = WREJ3;
    }

    public String getWREJ4() {
        return WREJ4;
    }

    public void setWREJ4(String WREJ4) {
        this.WREJ4 = WREJ4;
    }

    public String getWREJ5() {
        return WREJ5;
    }

    public void setWREJ5(String WREJ5) {
        this.WREJ5 = WREJ5;
    }

    public String getWFLG1() {
        return WFLG1;
    }

    public void setWFLG1(String WFLG1) {
        this.WFLG1 = WFLG1;
    }

    public String getWFLG2() {
        return WFLG2;
    }

    public void setWFLG2(String WFLG2) {
        this.WFLG2 = WFLG2;
    }

    public String getWFLG3() {
        return WFLG3;
    }

    public void setWFLG3(String WFLG3) {
        this.WFLG3 = WFLG3;
    }

    public String getWFLG4() {
        return WFLG4;
    }

    public void setWFLG4(String WFLG4) {
        this.WFLG4 = WFLG4;
    }

    public String getWVAR1() {
        return WVAR1;
    }

    public void setWVAR1(String WVAR1) {
        this.WVAR1 = WVAR1;
    }

    public LinkedList<X> getCuerpo1() {
        return cuerpo1;
    }

    public LinkedList<Y> getCuerpo2() {
        return cuerpo2;
    }

    public LinkedList<Z> getCuerpo3() {
        return cuerpo3;
    }

    public LinkedHashMap<String, Object> getEntrada() {
        return entrada;
    }

    public void setEntrada(LinkedHashMap<String, Object> entrada) {
        this.entrada = entrada;
    }

    public TransaccionAudit getTransaccionAudit() {
        return transaccionAudit;
    }

    public void setTransaccionAudit(TransaccionAudit transaccionAudit) {
        this.transaccionAudit = transaccionAudit;
    }


}
