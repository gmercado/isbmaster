/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.monitor.model;

import java.io.Serializable;

/**
 * @author Marcelo Morales
 *         Date: 9/14/11
 *         Time: 11:20 AM
 */
public class Cfp755Id implements Serializable {
    private Long cfbk;

    private String cfmsgn;

    public Cfp755Id() {
    }

    public Cfp755Id(Long cfbk, String cfmsgn) {
        this.cfbk = cfbk;
        this.cfmsgn = cfmsgn;
    }

    public Long getCfbk() {
        return cfbk;
    }

    public void setCfbk(Long cfbk) {
        this.cfbk = cfbk;
    }

    public String getCfmsgn() {
        return cfmsgn;
    }

    public void setCfmsgn(String cfmsgn) {
        this.cfmsgn = cfmsgn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cfp755Id)) return false;

        Cfp755Id cfp755Id = (Cfp755Id) o;

        if (cfbk != null ? !cfbk.equals(cfp755Id.cfbk) : cfp755Id.cfbk != null) return false;
        if (cfmsgn != null ? !cfmsgn.equals(cfp755Id.cfmsgn) : cfp755Id.cfmsgn != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cfbk != null ? cfbk.hashCode() : 0;
        result = 31 * result + (cfmsgn != null ? cfmsgn.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Cfp755Id");
        sb.append("{cfbk=").append(cfbk);
        sb.append(", cfmsgn='").append(cfmsgn).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
