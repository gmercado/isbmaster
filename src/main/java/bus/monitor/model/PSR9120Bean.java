/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.monitor.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Marcelo Morales
 *         Created: 12/28/11 9:45 AM
 */
public class PSR9120Bean implements Serializable {

    private static final long serialVersionUID = -3341105542913136435L;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 0), WLRGREG
    private BigDecimal WLRGREG;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(15, 2), WIMPTOT
    private BigDecimal WIMPTOT;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(15, 2), WIMPORTE
    private BigDecimal WIMPORTE;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(15, 2), WIMPORTE2
    private BigDecimal WIMPORTE2;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(15, 2), WCARGO
    private BigDecimal WCARGO;

    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WCLSCRG
    private String WCLSCRG;

    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WCOBCRG
    private String WCOBCRG;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(11, 7), WCOTACT
    private BigDecimal WCOTACT;

    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WCOMVEN
    private String WCOMVEN;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), WORDCTA
    private BigDecimal WORDCTA;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), WCODMON
    private BigDecimal WCODMON;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), WCODMON2
    private BigDecimal WCODMON2;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(15, 2), WIMPTE1
    private BigDecimal WIMPTE1;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(15, 2), WIMPTE2
    private BigDecimal WIMPTE2;

    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WTPOTE1
    private String WTPOTE1;

    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(1, 37),  WTPOTE2
    private String WTPOTE2;

    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(40, 37),  WNOMBRE
    private String WNOMBRE;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(15, 2), WSALACT
    private BigDecimal WSALACT;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), WNEGACT
    private BigDecimal WNEGACT;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(15, 2), WSALDIS
    private BigDecimal WSALDIS;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), WNEGDIS
    private BigDecimal WNEGDIS;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(10, 0), WNUMADI
    private BigDecimal WNUMADI;

    //    addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(8, 0), WFECHA
    private BigDecimal WFECHA;

    //    addFieldDescription(new CharacterFieldDescription(new AS400Text(40, 37),  WNOMBREA
    private String WNOMBREA;

    public BigDecimal getWLRGREG() {
        return WLRGREG;
    }

    public void setWLRGREG(BigDecimal WLRGREG) {
        this.WLRGREG = WLRGREG;
    }

    public BigDecimal getWIMPTOT() {
        return WIMPTOT;
    }

    public void setWIMPTOT(BigDecimal WIMPTOT) {
        this.WIMPTOT = WIMPTOT;
    }

    public BigDecimal getWIMPORTE() {
        return WIMPORTE;
    }

    public void setWIMPORTE(BigDecimal WIMPORTE) {
        this.WIMPORTE = WIMPORTE;
    }

    public BigDecimal getWIMPORTE2() {
        return WIMPORTE2;
    }

    public void setWIMPORTE2(BigDecimal WIMPORTE2) {
        this.WIMPORTE2 = WIMPORTE2;
    }

    public BigDecimal getWCARGO() {
        return WCARGO;
    }

    public void setWCARGO(BigDecimal WCARGO) {
        this.WCARGO = WCARGO;
    }

    public String getWCLSCRG() {
        return WCLSCRG;
    }

    public void setWCLSCRG(String WCLSCRG) {
        this.WCLSCRG = WCLSCRG;
    }

    public String getWCOBCRG() {
        return WCOBCRG;
    }

    public void setWCOBCRG(String WCOBCRG) {
        this.WCOBCRG = WCOBCRG;
    }

    public BigDecimal getWCOTACT() {
        return WCOTACT;
    }

    public void setWCOTACT(BigDecimal WCOTACT) {
        this.WCOTACT = WCOTACT;
    }

    public String getWCOMVEN() {
        return WCOMVEN;
    }

    public void setWCOMVEN(String WCOMVEN) {
        this.WCOMVEN = WCOMVEN;
    }

    public BigDecimal getWORDCTA() {
        return WORDCTA;
    }

    public void setWORDCTA(BigDecimal WORDCTA) {
        this.WORDCTA = WORDCTA;
    }

    public BigDecimal getWCODMON() {
        return WCODMON;
    }

    public void setWCODMON(BigDecimal WCODMON) {
        this.WCODMON = WCODMON;
    }

    public BigDecimal getWCODMON2() {
        return WCODMON2;
    }

    public void setWCODMON2(BigDecimal WCODMON2) {
        this.WCODMON2 = WCODMON2;
    }

    public BigDecimal getWIMPTE1() {
        return WIMPTE1;
    }

    public void setWIMPTE1(BigDecimal WIMPTE1) {
        this.WIMPTE1 = WIMPTE1;
    }

    public BigDecimal getWIMPTE2() {
        return WIMPTE2;
    }

    public void setWIMPTE2(BigDecimal WIMPTE2) {
        this.WIMPTE2 = WIMPTE2;
    }

    public String getWTPOTE1() {
        return WTPOTE1;
    }

    public void setWTPOTE1(String WTPOTE1) {
        this.WTPOTE1 = WTPOTE1;
    }

    public String getWTPOTE2() {
        return WTPOTE2;
    }

    public void setWTPOTE2(String WTPOTE2) {
        this.WTPOTE2 = WTPOTE2;
    }

    public String getWNOMBRE() {
        return WNOMBRE;
    }

    public void setWNOMBRE(String WNOMBRE) {
        this.WNOMBRE = WNOMBRE;
    }

    public BigDecimal getWSALACT() {
        return WSALACT;
    }

    public void setWSALACT(BigDecimal WSALACT) {
        this.WSALACT = WSALACT;
    }

    public BigDecimal getWNEGACT() {
        return WNEGACT;
    }

    public void setWNEGACT(BigDecimal WNEGACT) {
        this.WNEGACT = WNEGACT;
    }

    public BigDecimal getWSALDIS() {
        return WSALDIS;
    }

    public void setWSALDIS(BigDecimal WSALDIS) {
        this.WSALDIS = WSALDIS;
    }

    public BigDecimal getWNEGDIS() {
        return WNEGDIS;
    }

    public void setWNEGDIS(BigDecimal WNEGDIS) {
        this.WNEGDIS = WNEGDIS;
    }

    public BigDecimal getWNUMADI() {
        return WNUMADI;
    }

    public void setWNUMADI(BigDecimal WNUMADI) {
        this.WNUMADI = WNUMADI;
    }

    public BigDecimal getWFECHA() {
        return WFECHA;
    }

    public void setWFECHA(BigDecimal WFECHA) {
        this.WFECHA = WFECHA;
    }

    public String getWNOMBREA() {
        return WNOMBREA;
    }

    public void setWNOMBREA(String WNOMBREA) {
        this.WNOMBREA = WNOMBREA;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IMPORTE=").append(WIMPORTE);
        sb.append(", IMPORTE2=").append(WIMPORTE2);
        sb.append(", CARGO=").append(WCARGO);
        sb.append(", CODMON2=").append(WCODMON2);
        sb.append(", CODMON=").append(WCODMON);     
        sb.append(", ORDCTA=").append(WORDCTA);
        sb.append(", NOMBRE=").append(WNOMBRE);        
        sb.append(", NUMADI=").append(WNUMADI);
        return sb.toString();
    }
}
