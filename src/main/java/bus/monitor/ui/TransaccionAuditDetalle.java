/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.monitor.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.monitor.dao.TransaccionAuditDao;
import bus.monitor.entities.TransaccionAudit;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * @author Marcelo Morales
 *         Date: 9/8/11
 *         Time: 7:38 AM
 */
@Titulo("Detalle de transacci\u00f3n")
public class TransaccionAuditDetalle extends BisaWebPage {

    /**
     * @param parameters necesita el "id" de transaccionaudit
     */
    public TransaccionAuditDetalle(PageParameters parameters) {
        super(parameters);

        IModel<TransaccionAudit> model = new LoadableDetachableModel<TransaccionAudit>() {
            @Override
            protected TransaccionAudit load() {
                return getInstance(TransaccionAuditDao.class).
                        find(getPageParameters().get("id").toLong(), TransaccionAudit.INCLUIR_PETICION);
            }
        };

        setDefaultModel(new CompoundPropertyModel<Object>(model));

        add(new Label("id"));
        add(new Label("transaccion"));
        add(new Label("fecha"));
        add(new Label("wpost"));
        add(new Label("rej1"));
        add(new Label("milisegundos"));
        add(new Label("usuario"));
        add(new Label("caja"));
        add(new Label("secuencia"));
        add(new Label("cajaEntrada"));
        add(new Label("secuenciaEntrada"));
        add(new Label("rej2"));
        add(new Label("device"));
        add(new Label("wreve"));
        add(new Label("formatoEntrada1"));
        add(new Label("formatoEntrada2"));
        add(new Label("formatoEntrada3"));
        add(new Label("formatoSalida1"));
        add(new Label("formatoSalida2"));
        add(new Label("formatoSalida3"));
        add(new Label("numeroDeControl"));
        add(new Label("tipoDeDato"));
        add(new Label("otrosErrores.descripcion"));
    }
}
