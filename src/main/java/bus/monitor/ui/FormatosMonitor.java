/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.monitor.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.monitor.as400.AS400Factory;
import bus.plumbing.api.RolesBisa;
import com.ibm.as400.access.FieldDescription;
import com.ibm.as400.access.RecordFormat;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.list.OddEvenListItem;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler;
import org.apache.wicket.request.resource.ContentDisposition;
import org.apache.wicket.util.resource.AbstractStringResourceStream;
import org.apache.wicket.util.resource.IResourceStream;
import org.apache.wicket.util.time.Duration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Marcelo Morales
 */
@Menu(value = "Formatos del monitor", subMenu = SubMenu.PARAMETROS)
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
public class FormatosMonitor extends BisaWebPage {

    private static final long serialVersionUID = 4436754787044281763L;

    public FormatosMonitor() {

        Model<String> of = Model.of("");
        Form<String> form;
        add(form = new Form<String>("inyectar", of) {

            @Override
            protected void onSubmit() {
                try {
                    AS400Factory as400Factory = getInstance(AS400Factory.class);
                    RecordFormat recordFormat = as400Factory.buscarFormato(getModelObject());
                    if (recordFormat != null) {
                        getSession().info("Ingresado " + recordFormat.getName());
                    }
                } catch (Exception e) {
                    getSession().error(e.getMessage());
                }
            }
        });

        form.add(new RequiredTextField<>("formato", of));

        IModel<List<String>> formatosListModel = new AbstractReadOnlyModel<List<String>>() {
            @Override
            public List<String> getObject() {
                return new ArrayList<>(AS400Factory.getRecordFormats().keySet());
            }
        };

        add(new ListView<String>("lista", formatosListModel) {
            @Override
            protected void populateItem(ListItem<String> stringListItem) {
                stringListItem.add(new Label("formatolink", stringListItem.getModel()).
                        add(new AttributeAppender("href", Model.of("#" + stringListItem.getModelObject()), "")));
            }
        });

        add(new ListView<String>("formato", formatosListModel) {

            @Override
            protected ListItem<String> newItem(int index, IModel<String> itemModel) {
                return new OddEvenListItem<>(index, itemModel);
            }

            @Override
            protected void populateItem(final ListItem<String> stringListItem) {
                final RecordFormatModel recordFormatModel = new RecordFormatModel(stringListItem.getModel());
                stringListItem.add(new ListView<FieldDescription>("field", recordFormatModel) {
                    @Override
                    protected void populateItem(ListItem<FieldDescription> recordFormatListItem) {
                        recordFormatListItem.add(new Label("nombre", new PropertyModel<>(recordFormatListItem.getModel(), "fieldName")));
                        recordFormatListItem.add(new Label("descripcion", new FieldDescriptionModel(recordFormatListItem.getModel())));
                        recordFormatListItem.add(new Label("text", new PropertyModel<>(recordFormatListItem.getModel(), "TEXT")));
                    }
                });

                stringListItem.add(new Link<List<FieldDescription>>("bajar", recordFormatModel) {

                    private static final long serialVersionUID = -4036578807753935411L;

                    @Override
                    public void onClick() {
                        IResourceStream resourceStream = new AbstractStringResourceStream() {

                            private static final long serialVersionUID = 7081519840508945814L;

                            @Override
                            protected String getString() {
                                StringBuilder stringBuilder = new StringBuilder();
                                stringBuilder.
                                        append("import java.math.BigDecimal;\nimport java.io.Serializable;\n\npublic class ").
                                        append(StringUtils.substringAfterLast(stringListItem.getModelObject(), "/")).
                                        append("Bean implements Serializable {\nprivate static final serialVersionUID = 1L;\n\n");

                                for (FieldDescription fieldDescription : getModelObject()) {
                                    String t = fieldDescription.getDataType().getJavaType().getSimpleName();
                                    String n = fieldDescription.getFieldName();
                                    stringBuilder.
                                            append("/**\n").
                                            append(" * ").
                                            append(fieldDescription.getTEXT()).
                                            append("\n */\nprivate ").
                                            append(t).
                                            append(" ").
                                            append(n).
                                            append(";\n\npublic ").
                                            append(t).
                                            append(" get").
                                            append(n).
                                            append("() {\nreturn ").
                                            append(n).
                                            append(";\n}\n\npublic void set").
                                            append(n).
                                            append("(").
                                            append(t).
                                            append(" x){\nthis.").
                                            append(n).
                                            append("=x;\n}\n\n");
                                }

                                stringBuilder.append("}\n");

                                return stringBuilder.toString();
                            }
                        };
                        getRequestCycle().scheduleRequestHandlerAfterCurrent(
                                new ResourceStreamRequestHandler(resourceStream).
                                        setFileName(stringListItem.getModelObject())
                                        .setContentDisposition(ContentDisposition.ATTACHMENT)
                                        .setCacheDuration(Duration.minutes(10)));
                    }
                }.setBody(stringListItem.getModel()));
            }
        });
    }

    private static class RecordFormatModel extends AbstractReadOnlyModel<List<FieldDescription>> {
        private final IModel<String> model;

        public RecordFormatModel(IModel<String> model) {
            this.model = model;
        }

        @Override
        public List<FieldDescription> getObject() {
            return Arrays.asList(AS400Factory.getRecordFormats().get(model.getObject()).getFieldDescriptions());
        }
    }

    private static class FieldDescriptionModel extends AbstractReadOnlyModel<Object> {
        private final IModel<FieldDescription> model;

        public FieldDescriptionModel(IModel<FieldDescription> model) {
            this.model = model;
        }

        @Override
        public Object getObject() {
            FieldDescription object = model.getObject();
            return object.getDataType().getJavaType().getSimpleName() + " " + object.getLength();
        }
    }
}