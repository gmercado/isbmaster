/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.monitor.ui;

import bus.database.components.FiltroPanel;
import bus.monitor.dao.TransaccionAuditDao;
import bus.monitor.entities.TransaccionAudit;
import bus.plumbing.components.Html5DateTextField;
import com.google.inject.Inject;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.validator.RangeValidator;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Marcelo Morales
 *         Date: 7/8/11
 *         Time: 10:29 AM
 */
public class ListadoTransaccionesFiltroPanel extends FiltroPanel<TransaccionAudit> {

    @Inject
    private TransaccionAuditDao dao;


    public ListadoTransaccionesFiltroPanel(String id, IModel<TransaccionAudit> model1, IModel<TransaccionAudit> model2) {
        super(id, model1, model2);

        add(new TextField<>("caja+secuencia", new PropertyModel<>(model1, "cajaSecuencia")).
                add(new RangeValidator<>(new BigDecimal("10000000"), new BigDecimal("99999999999"))));

        add(new Html5DateTextField("fechaDesde", new PropertyModel<>(model1, "fecha")).setOutputMarkupId(true));
        add(new Html5DateTextField("fechaHasta", new PropertyModel<>(model2, "fecha")));

        add(new DropDownChoice<>("transaccion", new PropertyModel<>(model1, "transaccion"),
                new LoadableDetachableModel<List<? extends String>>() {
                    @Override
                    protected List<? extends String> load() {
                        return dao.getTodasTransacciones();
                    }
                }).setNullValid(true).setOutputMarkupId(true));
    }
}
