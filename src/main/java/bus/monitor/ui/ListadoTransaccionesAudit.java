/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.monitor.ui;

import bus.database.components.FiltroPanel;
import bus.database.components.Listado;
import bus.database.components.ListadoDataProvider;
import bus.database.components.ListadoPanel;
import bus.database.dao.Dao;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.monitor.dao.TransaccionAuditDao;
import bus.monitor.entities.TransaccionAudit;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.DPropertyColumn;
import bus.plumbing.components.LinkPropertyColumn;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Marcelo Morales
 *         Date: 7/6/11
 *         Time: 7:07 AM
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN, RolesBisa.SOLO_LECTURA})
@Menu(value = "Transacciones", subtitulo = "Hist\u00f3rico de transacciones monitor en as/400", subMenu = SubMenu.PARAMETROS)
public class ListadoTransaccionesAudit extends Listado<TransaccionAudit, Long> {

    @Override
    protected ListadoPanel<TransaccionAudit, Long> newListadoPanel(String id) {
        return new ListadoPanel<TransaccionAudit, Long>(id) {

            @Override
            protected List<IColumn<TransaccionAudit, String>> newColumns(ListadoDataProvider dataProvider) {
                ArrayList<IColumn<TransaccionAudit, String>> iColumns = new ArrayList<>();
                iColumns.add(new LinkPropertyColumn<TransaccionAudit>(Model.of("Id"), "id", "id") {
                    @Override
                    protected void onClick(TransaccionAudit object) {
                        setResponsePage(TransaccionAuditDetalle.class, new PageParameters().add("id", object.getId()));
                    }
                });
                iColumns.add(new DPropertyColumn<>(Model.of("Caja"), "caja", "caja"));
                iColumns.add(new DPropertyColumn<>(Model.of("Secuencia"), "secuencia", "secuencia"));
                iColumns.add(new PropertyColumn<>(Model.of("Transaccion"), "transaccion", "transaccion"));
                iColumns.add(new PropertyColumn<>(Model.of("Fecha"), "fecha", "fecha"));
                iColumns.add(new PropertyColumn<>(Model.of("Estado"), "otrosErrores.descripcion"));
                iColumns.add(new PropertyColumn<>(Model.of("C\u00f3digo error"), "rej1", "rej1"));
                iColumns.add(new PropertyColumn<>(Model.of("Post"), "wpost", "wpost"));
                iColumns.add(new DPropertyColumn<>(Model.of("Milis"), "milisegundos", "milisegundos"));
                return iColumns;
            }

            @Override
            protected TransaccionAudit newObject1() {
                TransaccionAudit transaccionAudit = new TransaccionAudit();
                transaccionAudit.setFecha(new Date());
                return transaccionAudit;
            }

            @Override
            protected TransaccionAudit newObject2() {
                TransaccionAudit transaccionAudit = new TransaccionAudit();
                transaccionAudit.setFecha(new Date());
                return transaccionAudit;
            }

            @Override
            protected FiltroPanel<TransaccionAudit> newFiltroPanel(String id, IModel<TransaccionAudit> model1, IModel<TransaccionAudit> model2) {
                return new ListadoTransaccionesFiltroPanel(id, model1, model2);
            }

            @Override
            protected Class<? extends Dao<TransaccionAudit, Long>> getProviderClazz() {
                return TransaccionAuditDao.class;
            }

            @Override
            protected SortParam<String> sorteoInicial() {
                return new SortParam<>("fecha", false);
            }
        };
    }

}
