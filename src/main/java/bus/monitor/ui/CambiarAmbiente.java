/*
 *  Copyright 2009 Banco Bisa S.A.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.monitor.ui;

import bus.database.model.AmbienteActual;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.monitor.api.TransaccionMonitorFactoryEstado;
import bus.monitor.api.TransaccionesMonitor;
import bus.plumbing.api.RolesBisa;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RadioChoice;
import org.apache.wicket.model.PropertyModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 *
 * @author Marcelo Morales
 */

@Menu(value = "Consulta de Ambiente", subMenu = SubMenu.ADMINISTRACION)
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
public class CambiarAmbiente extends BisaWebPage {

    protected static final Logger LOGGER = LoggerFactory.getLogger(BisaWebPage.class);


    //private TransaccionesMonitor transaccionMonitor;

    private TransaccionMonitorFactoryEstado estado;


    public CambiarAmbiente() {
        Form<?> cambio;
        // Ambiente Transaccion Monitor

        TransaccionesMonitor transaccionMonitor = getInstance(TransaccionesMonitor.class);
        AmbienteActual ambiente = transaccionMonitor.getAmbienteActual();

        if(ambiente==null){
            estado =  TransaccionMonitorFactoryEstado.INDETERMINADO;
        }
        if(ambiente.isOffline()){
            estado =  TransaccionMonitorFactoryEstado.CERRADO;
        }

        if(ambiente.isAmbienteTemporal()){
            estado =  TransaccionMonitorFactoryEstado.TEMPORAL;
        }else{
            estado =  TransaccionMonitorFactoryEstado.PRODUCCION;
        }

        add(cambio = new Form<Void>("cambio") {

            private static final long serialVersionUID = 23897239847L;

            @Override
            protected void onSubmit() {
                //TODO: codigo en caso de habilitar el ambiente desde aqui.

            }

            @Override
            protected void onBeforeRender() {
                super.onBeforeRender();
            }
        });


        final RadioChoice<TransaccionMonitorFactoryEstado> components = new RadioChoice<TransaccionMonitorFactoryEstado>("ambiente",
                new PropertyModel<TransaccionMonitorFactoryEstado>(this, "estado"),
                Arrays.asList(TransaccionMonitorFactoryEstado.values()),
                new ChoiceRenderer<TransaccionMonitorFactoryEstado>("descripcion"));
        components.setRequired(true);
        components.setEnabled(false);
        cambio.add(components);

    }


    public synchronized TransaccionMonitorFactoryEstado getEstado() {
        return estado;
    }

    public synchronized void setEstado(TransaccionMonitorFactoryEstado estado) {
        this.estado = estado;
    }
}
