/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.monitor.dao;

import bus.monitor.api.IErroresMonitor;
import bus.monitor.entities.ErroresEbisa;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marcelo Morales
 *         Date: 6/19/12
 */
public class CompuestoErroresDao implements IErroresMonitor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompuestoErroresDao.class);

    private final Cfp755Dao cfp755Dao;

    private final ErroresEbisaDao erroresEbisaDao;

    @Inject
    public CompuestoErroresDao(Cfp755Dao cfp755Dao, ErroresEbisaDao erroresEbisaDao) {
        this.cfp755Dao = cfp755Dao;
        this.erroresEbisaDao = erroresEbisaDao;
    }

    @Override
    public String getByMsgN(String msgn) {
        ErroresEbisa erroresEbisa = erroresEbisaDao.find(msgn);
        if (erroresEbisa != null) {
            return erroresEbisa.getDesc();
        }
        return cfp755Dao.getByMsgN(msgn);
    }
}
