/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.monitor.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.monitor.entities.TransaccionAudit;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaQuery;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


/**
 * @author Marcelo Morales
 */
public class TransaccionAuditDao extends DaoImpl<TransaccionAudit, Long> {

    protected TransaccionAuditDao() {
        super(null);
    }

    @Inject
    public TransaccionAuditDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Long> countPath(Root<TransaccionAudit> from) {
        return from.get("id");
    }

    @Override
    protected Predicate[] createQBE(OpenJPACriteriaBuilder cb, Root<TransaccionAudit> from, TransaccionAudit example,
                                    TransaccionAudit example2) {
        List<Predicate> predicates = new LinkedList<>();
        Date fecha = example.getFecha();
        example.setFecha(null);
        Predicate qbe = cb.qbe(from, example);
        predicates.add(qbe);

        if (fecha != null && example2.getFecha() != null) {

            Date inicio = DateUtils.truncate(fecha, Calendar.DAY_OF_MONTH);
            Date inicio2 = DateUtils.truncate(example2.getFecha(), Calendar.DAY_OF_MONTH);
            inicio2 = DateUtils.addMilliseconds(DateUtils.addDays(inicio2, 1), -1);
            predicates.add(cb.between(from.get("fecha"), inicio, inicio2));
        }

        if (example.getCajaSecuencia() != null && example.getCajaSecuencia().compareTo(BigDecimal.ZERO) > 0) {
            // 4, 7
            final String s = example.getCajaSecuencia().toPlainString();
            final String secuenciaStr = StringUtils.substring(s, -7);
            final String cajaStr = StringUtils.substring(s, 0, -7);
            if (!StringUtils.isEmpty(cajaStr) && !StringUtils.isEmpty(secuenciaStr) &&
                    StringUtils.isNumeric(cajaStr) && StringUtils.isNumeric(secuenciaStr)) {
                predicates.add(cb.equal(from.get("caja"), new BigDecimal(cajaStr)));
                predicates.add(cb.equal(from.get("secuencia"), new BigDecimal(secuenciaStr)));
            }
        }

        return predicates.toArray(new Predicate[predicates.size()]);

    }

    public List<? extends String> getTodasTransacciones() {
        return doWithTransaction((WithTransaction<List<? extends String>>) (entityManager, t) -> {
            OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
            OpenJPACriteriaQuery<String> query = cb.createQuery(String.class);

            Root<TransaccionAudit> from = query.from(TransaccionAudit.class);
            query.distinct(true).select(from.get("transaccion"));
            query.orderBy(cb.asc(from.get("transaccion")));

            return entityManager.createQuery(query).getResultList();
        });
    }
}
