/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.monitor.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BaseAuxiliar;
import bus.monitor.api.IErroresEbisa;
import bus.monitor.entities.ErroresEbisa;
import com.google.inject.Inject;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.Arrays;


/**
 * @author Marcelo Morales
 *         Created: 11/8/11 2:43 PM
 */
public class ErroresEbisaDao extends DaoImpl<ErroresEbisa, String> implements IErroresEbisa {

    protected ErroresEbisaDao() {
        super(null);
    }

    @Inject
    public ErroresEbisaDao(@BaseAuxiliar EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<ErroresEbisa> p) {
        //noinspection unchecked
        return Arrays.asList(p.<String>get("code"), p.<String>get("desc"), p.<String>get("model"), p.<String>get("sol"));
    }

    @Override
    protected Expression<Long> selectCount(CriteriaBuilder cb, Root<ErroresEbisa> p) {
        return cb.count(p.get("code"));
    }
}
