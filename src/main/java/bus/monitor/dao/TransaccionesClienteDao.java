package bus.monitor.dao;

import bus.env.api.MedioAmbiente;
import bus.interfaces.as400.entities.Cliente;
import bus.interfaces.as400.entities.Cuenta;
import bus.monitor.api.*;
import bus.monitor.transaction.trxin.Reg020;
import bus.monitor.transaction.trxin.Reg021;
import com.google.inject.Inject;

import java.io.IOException;
import java.math.BigDecimal;

import static bus.env.api.Variables.*;
import static bus.monitor.api.FormatersMonitor.*;
import static bus.monitor.api.ParametersMonitor.*;

/**
 * @author by rsalvatierra on 05/09/2017.
 */
public class TransaccionesClienteDao {
    private final TransaccionesMonitor transaccionesMonitor;
    private final MedioAmbiente medioAmbiente;

    @Inject
    public TransaccionesClienteDao(TransaccionesMonitor transaccionesMonitor, MedioAmbiente medioAmbiente) {
        this.transaccionesMonitor = transaccionesMonitor;
        this.medioAmbiente = medioAmbiente;
    }

    public IRespuestasMonitor consultaCotizacion(final String canal) throws SistemaCerradoException, ImposibleLeerRespuestaException,
            TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {
        WithMonitor<IRespuestasMonitor> withMonitor = new WithMonitor<IRespuestasMonitor>() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {
                // Obtiene la transacción
                String transaccion = medioAmbiente.getValorDe(TRANSACCION_COTIZACION, TRANSACCION_COTIZACION_DEFAULT);
                //Cabecera
                monitor.setDatoCabeceraEnvio(MON_DEVICE, canal);
                monitor.setDatoCabeceraEnvio(MON_TRANSACCION, transaccion);
                monitor.setDatoCabeceraEnvio(MON_USUARIO, getUsuario());
                monitor.setCuerpo1EnvioFormat(PSR9011);
                monitor.setCuerpo1RecepcionFormat(PSR9416);

                monitor.setDatoCuerpo1(MON_CUENTA, BigDecimal.ONE);
                monitor.setDatoCuerpo1(MON_TPOCTA, "I");
            }

            @Override
            public IRespuestasMonitor procesar(IRespuestasMonitor monitor) {
                return monitor;
            }
        };
        return transaccionesMonitor.ejecutarTransaccionMonitor(getVersion(), null, withMonitor);
    }

    public IRespuestasMonitor bloqueaTarjetaDebito(final String canal, final String tarjeta, final String operacion, final String causal) throws SistemaCerradoException, ImposibleLeerRespuestaException,
            TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {
        WithMonitor<IRespuestasMonitor> withMonitor = new WithMonitor<IRespuestasMonitor>() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {
                // Obtiene la transacción
                String transaccion = medioAmbiente.getValorDe(TRANSACCION_BLOQUEO_TARJETA_DEBITO, TRANSACCION_BLOQUEO_TARJETA_DEBITO_DEFAULT);
                //Cabecera
                monitor.setDatoCabeceraEnvio(MON_DEVICE, canal);
                monitor.setDatoCabeceraEnvio(MON_TRANSACCION, transaccion);
                monitor.setDatoCabeceraEnvio(MON_USUARIO, getUsuario());
                monitor.setCuerpo1EnvioFormat(PSR9061);
                //Cuerpo
                monitor.setDatoCuerpo1(MON_NUMERO_TC, new BigDecimal(tarjeta));
                monitor.setDatoCuerpo1(MON_TPOPAG, causal.equalsIgnoreCase("robo") ? "L" : "K");
                monitor.setDatoCuerpo1(MON_TIPO_REQ, operacion);
                monitor.setDatoCuerpo1(MON_CAUSAL, causal);
            }

            @Override
            public IRespuestasMonitor procesar(IRespuestasMonitor monitor) {
                return monitor;
            }
        };
        return transaccionesMonitor.ejecutarTransaccionMonitor(getVersion(), null, withMonitor);
    }

    public IRespuestasMonitor consultaCuenta(final String canal, Cliente cliente) throws SistemaCerradoException, ImposibleLeerRespuestaException,
            TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {
        WithMonitor<IRespuestasMonitor> withMonitor = new WithMonitor<IRespuestasMonitor>() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {
                // Obtiene la transacción
                String transaccion = medioAmbiente.getValorDe(TRANSACCION_CONSULTA_CUENTA, TRANSACCION_CONSULTA_CUENTA_DEFAULT);
                //Cabecera
                monitor.setDatoCabeceraEnvio(MON_DEVICE, canal);
                monitor.setDatoCabeceraEnvio(MON_TRANSACCION, transaccion);
                monitor.setDatoCabeceraEnvio(MON_USUARIO, getUsuario());
                monitor.setCuerpo1EnvioFormat(PSR9011);
                monitor.setCuerpo1RecepcionFormat(PSR9913);
                monitor.setCuerpo2RecepcionFormat(PSR9916);
                //Cuerpo
                monitor.setDatoCuerpo1(MON_CUENTA, new BigDecimal(cliente.getCodCliente()));
                monitor.setDatoCuerpo1(MON_TPOCTA, "I");
                monitor.setDatoCuerpo1(MON_PAG, cliente.getAgencia());
                monitor.setDatoCuerpo1(MON_CNT, new BigDecimal(0));
            }

            @Override
            public IRespuestasMonitor procesar(IRespuestasMonitor monitor) {
                return monitor;
            }
        };
        return transaccionesMonitor.ejecutarTransaccionMonitor(getVersion(), null, withMonitor);
    }

    public IRespuestasMonitor consultaMovimientosCuenta(final String canal, final Cuenta cuenta, final int cantidad) throws SistemaCerradoException, ImposibleLeerRespuestaException,
            TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {
        WithMonitor<IRespuestasMonitor> withMonitor = new WithMonitor<IRespuestasMonitor>() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {
                // Obtiene la transacción
                String transaccion = medioAmbiente.getValorDe(TRANSACCION_CONSULTA_MOV_CUENTA, TRANSACCION_CONSULTA_MOV_CUENTA_DEFAULT);
                //Cabecera
                monitor.setDatoCabeceraEnvio(MON_DEVICE, canal);
                monitor.setDatoCabeceraEnvio(MON_TRANSACCION, transaccion);
                monitor.setDatoCabeceraEnvio(MON_USUARIO, getUsuario());
                monitor.setCuerpo1EnvioFormat(PSR9011);
                monitor.setCuerpo1RecepcionFormat(PSR9112);
                monitor.setCuerpo2RecepcionFormat(PSR9113);
                //Cuerpo
                monitor.setDatoCuerpo1(MON_CUENTA, new BigDecimal(cuenta.getNumeroCuenta()));
                monitor.setDatoCuerpo1(MON_TPOCTA, cuenta.getTipoCta());
                monitor.setDatoCuerpo1(MON_PAG, new BigDecimal(0));
                monitor.setDatoCuerpo1(MON_CNT, new BigDecimal(cantidad));
            }

            @Override
            public IRespuestasMonitor procesar(IRespuestasMonitor monitor) {
                return monitor;
            }
        };
        return transaccionesMonitor.ejecutarTransaccionMonitor(getVersion(), null, withMonitor);
    }

    public IRespuestasMonitor consultaTarjetaCredito(final String canal, BigDecimal cuenta) throws SistemaCerradoException, ImposibleLeerRespuestaException,
            TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {
        WithMonitor<IRespuestasMonitor> withMonitor = new WithMonitor<IRespuestasMonitor>() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {
                // Obtiene la transacción
                String transaccion = medioAmbiente.getValorDe(TRANSACCION_CONSULTA_TARJETA_CREDITO, TRANSACCION_CONSULTA_TARJETA_CREDITO_DEFAULT);
                //Cabecera
                monitor.setDatoCabeceraEnvio(MON_DEVICE, canal);
                monitor.setDatoCabeceraEnvio(MON_TRANSACCION, transaccion);
                monitor.setDatoCabeceraEnvio(MON_USUARIO, getUsuario());
                monitor.setCuerpo1EnvioFormat(PSR9011);
                monitor.setCuerpo1RecepcionFormat(PSR9611);
                //Cuerpo
                monitor.setDatoCuerpo1(MON_CUENTA, cuenta);
            }

            @Override
            public IRespuestasMonitor procesar(IRespuestasMonitor monitor) {
                return monitor;
            }
        };
        return transaccionesMonitor.ejecutarTransaccionMonitor(getVersion(), null, withMonitor);
    }

    public IRespuestasMonitor consultaPrestamo(final String canal, BigDecimal cuenta) throws SistemaCerradoException, ImposibleLeerRespuestaException,
            TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {
        WithMonitor<IRespuestasMonitor> withMonitor = new WithMonitor<IRespuestasMonitor>() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {
                // Obtiene la transacción
                String transaccion = medioAmbiente.getValorDe(TRANSACCION_CONSULTA_PRESTAMO, TRANSACCION_CONSULTA_PRESTAMO_DEFAULT);
                //Cabecera
                monitor.setDatoCabeceraEnvio(MON_DEVICE, canal);
                monitor.setDatoCabeceraEnvio(MON_TRANSACCION, transaccion);
                monitor.setDatoCabeceraEnvio(MON_USUARIO, getUsuario());
                monitor.setCuerpo1EnvioFormat(PSR9011);
                monitor.setCuerpo1RecepcionFormat(PSR9511);
                //Cuerpo
                monitor.setDatoCuerpo1(MON_CUENTA, cuenta);
            }

            @Override
            public IRespuestasMonitor procesar(IRespuestasMonitor monitor) {
                return monitor;
            }
        };
        return transaccionesMonitor.ejecutarTransaccionMonitor(getVersion(), null, withMonitor);
    }

    public IRespuestasMonitor pagoTarjetaCredito(final String canal,
                                                 Reg021 reg021,
                                                 final String transaccion) throws SistemaCerradoException, ImposibleLeerRespuestaException, TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {
        WithMonitor<IRespuestasMonitor> withMonitor = new WithMonitor<IRespuestasMonitor>() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {
                //Cabecera
                monitor.setDatoCabeceraEnvio(MON_DEVICE, canal);
                monitor.setDatoCabeceraEnvio(MON_TRANSACCION, transaccion);
                monitor.setDatoCabeceraEnvio(MON_USUARIO, getUsuario());
                monitor.setDatoCabeceraEnvio(MON_TPODTA, 1);
                monitor.setCuerpo1EnvioFormat(PSR9021);
                monitor.setCuerpo1RecepcionFormat(PSR9120);
                //Cuerpo
                monitor.setDatoCuerpo1(MON_CLSTRN, new BigDecimal(reg021.getWCLSTRN()));
                monitor.setDatoCuerpo1(MON_IMPORTE_TOTAL, new BigDecimal(reg021.getWIMPORTE()));
                monitor.setDatoCuerpo1(MON_TIPO_IMPORTE, reg021.getWTPOIMP());
                monitor.setDatoCuerpo1(MON_CODMONEDA, new BigDecimal(reg021.getWCODMON()));
                monitor.setDatoCuerpo1(MON_CUENTA2, new BigDecimal(reg021.getWCUENTA2()));
                monitor.setDatoCuerpo1(MON_TPOCTA2, reg021.getWTPOCTA2());
                monitor.setDatoCuerpo1(MON_CODMONEDA2, new BigDecimal(reg021.getWCODMON2()));
                monitor.setDatoCuerpo1(MON_CUENTA, new BigDecimal(reg021.getWCODADI()));
                monitor.setDatoCuerpo1(MON_COD_ADICIONAL, BigDecimal.ZERO);
                monitor.setDatoCuerpo1(MON_NOTA, reg021.getWNOTA());
            }

            @Override
            public IRespuestasMonitor procesar(IRespuestasMonitor monitor) {
                return monitor;
            }
        };
        return transaccionesMonitor.ejecutarTransaccionMonitor(getVersion(), null, withMonitor);
    }

    public IRespuestasMonitor transferenciaCuentasBisa(final String canal,
                                                       Reg021 reg021,
                                                       final String transaccion) throws SistemaCerradoException, ImposibleLeerRespuestaException, TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {
        WithMonitor<IRespuestasMonitor> withMonitor = new WithMonitor<IRespuestasMonitor>() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {
                //Cabecera
                monitor.setDatoCabeceraEnvio(MON_DEVICE, canal);
                monitor.setDatoCabeceraEnvio(MON_TRANSACCION, transaccion);
                monitor.setDatoCabeceraEnvio(MON_USUARIO, getUsuario());
                monitor.setDatoCabeceraEnvio(MON_TPODTA, 1);
                monitor.setCuerpo1EnvioFormat(PSR9021);
                monitor.setCuerpo1RecepcionFormat(PSR9120);
                //Cuerpo
                monitor.setDatoCuerpo1(MON_CLSTRN, new BigDecimal(reg021.getWCLSTRN()));
                monitor.setDatoCuerpo1(MON_CUENTA, new BigDecimal(reg021.getWCUENTA()));
                monitor.setDatoCuerpo1(MON_TPOCTA, reg021.getWTPOCTA());
                monitor.setDatoCuerpo1(MON_CODMONEDA, new BigDecimal(reg021.getWCODMON()));

                monitor.setDatoCuerpo1(MON_CUENTA2, new BigDecimal(reg021.getWCUENTA2()));
                monitor.setDatoCuerpo1(MON_TPOCTA2, reg021.getWTPOCTA2());
                monitor.setDatoCuerpo1(MON_CODMONEDA2, new BigDecimal(reg021.getWCODMON2()));
                if (new Integer(reg021.getWCODADI()).equals(new Integer(reg021.getWCODMON()))) {
                    monitor.setDatoCuerpo1(MON_IMPORTE, new BigDecimal(reg021.getWIMPORTE()));
                } else {
                    monitor.setDatoCuerpo1(MON_IMPORTE2, new BigDecimal(reg021.getWIMPORTE()));
                }
                monitor.setDatoCuerpo1(MON_NOTA, reg021.getWNOTA());
            }

            @Override
            public IRespuestasMonitor procesar(IRespuestasMonitor monitor) {
                return monitor;
            }
        };
        return transaccionesMonitor.ejecutarTransaccionMonitor(getVersion(), null, withMonitor);
    }

    public IRespuestasMonitor transferenciaOtrosBancos(final String canal,
                                                       Reg021 reg021,
                                                       Reg020 reg020,
                                                       final String transaccion) throws SistemaCerradoException, ImposibleLeerRespuestaException, TransaccionEfectivaException, TransaccionNoEjecutadaCorrectamenteException, IOException {
        WithMonitor<IRespuestasMonitor> withMonitor = new WithMonitor<IRespuestasMonitor>() {
            @Override
            public void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException {
                //Cabecera
                monitor.setDatoCabeceraEnvio(MON_DEVICE, canal);
                monitor.setDatoCabeceraEnvio(MON_TRANSACCION, transaccion);
                monitor.setDatoCabeceraEnvio(MON_USUARIO, getUsuario());
                monitor.setDatoCabeceraEnvio(MON_TPODTA, 1);
                monitor.setCuerpo1EnvioFormat(PSR9021);
                monitor.setCuerpo2EnvioFormat(PSR9020);
                monitor.setCuerpo1RecepcionFormat(PSR9120);
                //Cuerpo
                monitor.setDatoCuerpo1(MON_CLSTRN, new BigDecimal(reg021.getWCLSTRN()));
                monitor.setDatoCuerpo1(MON_CUENTA, new BigDecimal(reg021.getWCUENTA()));
                monitor.setDatoCuerpo1(MON_TPOCTA, reg021.getWTPOCTA());
                monitor.setDatoCuerpo1(MON_CODMONEDA, new BigDecimal(reg021.getWCODMON()));
                monitor.setDatoCuerpo1(MON_CODMONEDA2, new BigDecimal(reg021.getWCODMON2()));
                if (reg021.getWCODMON().equals(reg021.getWCODMON2())) {
                    monitor.setDatoCuerpo1(MON_IMPORTE, new BigDecimal(reg021.getWIMPORTE()));
                    monitor.setDatoCuerpo1(MON_IMPORTE2, BigDecimal.ZERO);
                } else {
                    monitor.setDatoCuerpo1(MON_IMPORTE, BigDecimal.ZERO);
                    monitor.setDatoCuerpo1(MON_IMPORTE2, new BigDecimal(reg021.getWIMPORTE()));
                }
                monitor.setDatoCuerpo1(MON_NOTA, reg021.getWNOTA());

                monitor.setDatoCuerpo2(MON_DAT_ADICIONAL, reg020.getWDTAADI());
                monitor.setDatoCuerpo2(MON_COD_BANCO, new BigDecimal(reg020.getWCODBCO()));//cod banco
                monitor.setDatoCuerpo2(MON_COD_SUCURSAL, reg020.getWCODSUC());//cod suc
                monitor.setDatoCuerpo2(MON_CUENTA_BEN, reg020.getWCTABEN());//cta destino
                monitor.setDatoCuerpo2(MON_NOMBRE_BEN, reg020.getWNOMBEN());//nombre ben
                monitor.setDatoCuerpo2(MON_TPOCTA, reg020.getWTPOCTA());//tipo cuenta
            }

            @Override
            public IRespuestasMonitor procesar(IRespuestasMonitor monitor) {
                return monitor;
            }
        };
        return transaccionesMonitor.ejecutarTransaccionMonitor(getVersion(), null, withMonitor);
    }

    private int getVersion() {
        return medioAmbiente.getValorIntDe(MONITOR_VERSION, MONITOR_VERSION_DEFAULT);
    }

    private int getUsuario() {
        return medioAmbiente.getValorIntDe(USUARIO_MONITOR, USUARIO_MONITOR_DEFAULT);
    }
}
