package bus.monitor.dao;

import bus.interfaces.ticketing.entities.AgenciaTicket;
import bus.plumbing.utils.FormatosUtils;
import com.bisa.bus.servicios.bisa.dao.ContratosDao;
import com.bisa.bus.servicios.bisa.model.Contratos;
import com.bisa.bus.servicios.bisa.model.TipoContrato;
import com.google.inject.Inject;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.openlogics.gears.jdbc.DataStoreFactory;
import org.openlogics.gears.jdbc.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author by rsalvatierra on 17/01/2018.
 */
public class LogWebDao implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogWebDao.class);

    private static final String COM_SQLSERVER_DRIVER = "net.sourceforge.jtds.jdbc.Driver";
    private static final String JDBC_SQLSERVER = "jdbc:jtds:sqlserver://";

    @Inject
    ContratosDao contratosDao;

    @Inject
    public LogWebDao() {

    }

    static {
        try {
            Class.forName(COM_SQLSERVER_DRIVER);
        } catch (ClassNotFoundException e) {
            LOGGER.error("error al cargar SqlServer", e);
        }
    }

    public void getTintLogWeb(AgenciaTicket agenciaTicket, Date fecha) throws SQLException {
        String cantidadTicket = "";
        org.openlogics.gears.jdbc.ObjectDataStore ds = null;
        try {
            String URI = "x0sql2.grupobisa.net:1207/Stadistic;useLOBs=false";//medioAmbiente.getValorDe(CONEXION_DB2_TICKETING_LOCAL, CONEXION_DB2_TICKETING_LOCAL_DEFAULT);
            String user = "ebisausr";//medioAmbiente.getValorDe(USER_CONEXION_DB2_TICKETING_LOCAL, USER_CONEXION_DB2_TICKETING_LOCAL_DEFAULT);
//            String pass = "crypt:WLFgPZFoZRZbbv7C6A7pMA==";//medioAmbiente.getValorDe(PASS_CONEXION_DB2_TICKETING_LOCAL, PASS_CONEXION_DB2_TICKETING_LOCAL_DEFAULT);
            String pass = "ebisa123";//medioAmbiente.getValorDe(PASS_CONEXION_DB2_TICKETING_LOCAL, PASS_CONEXION_DB2_TICKETING_LOCAL_DEFAULT);
            Class.forName(COM_SQLSERVER_DRIVER);
//            URI = URI.replaceAll("LOCAL", agenciaTicket.getServDB());
            ds = DataStoreFactory.createObjectDataStore(DriverManager.getConnection(JDBC_SQLSERVER + URI, user, pass));
            ds.setAutoClose(false);
            List<Map<String, Object>> map = ds.select(Query.of("select tlw.CustPermID, tlw.AccountFrom, tlw.EndUserPage, tlw.RqDate FROM tint_log_web tlw where ActionData='lvdesem02.htm'"), new MapListHandler());
            String fechaRegistro = null;
            String numeroCuenta = null;
            Contratos contratos = null;
            for (Map<String, Object> stringObjectMap : map) {
                contratos = new Contratos();

                cantidadTicket = (String) ((String) stringObjectMap.get("CustPermID")).trim();
                contratos.setCodigoCliente(cantidadTicket.trim());
                String endUserPage = (String) stringObjectMap.get("EndUserPage");
                //fechaRegistro=FormatosUtils.fechaHoraLargaFormateada((java.util.Date) stringObjectMap.get("RqDate"));
                contratos.setFechaRegistro((java.util.Date) stringObjectMap.get("RqDate"));
                numeroCuenta = (String) stringObjectMap.get("AccountFrom");
                contratos.setNumeroCuenta(numeroCuenta.trim());
                contratos.setTipoContrato(TipoContrato.BOL);

                if (!(endUserPage == null) || !(endUserPage.isEmpty())) {
                    System.out.println("Cantidad:" + cantidadTicket);
                    endUserPage = searchDataContract(endUserPage);
                    contratos.setDetalleContrato(endUserPage);
//                    contratos.setFechaModificacion(LocalDate.now().toDate());
                    contratosDao.persist(contratos);

//                    ds.update(Query.of("insert into contract_log_web values(\'"+fechaRegistro+"\',\'" + cantidadTicket + "\','cli29590','xxxx',\'"+numeroCuenta+"\',\'" + searchDataContract(endUserPage) + "\','BOLE','''',GETDATE(),'','')"));
                }
            }

        } catch (SQLException ex) {
            LOGGER.error("Error al conectarse al DB2 ticketing", ex);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            //ds.tryCommitAndClose();
        }
//        return cantidadTicket;
    }

    public String searchDataContract(String dataUserPage) {
        try {
            if (dataUserPage == null || dataUserPage.isEmpty()) {
                throw new Exception("Documento no Valido");
            }
            String text = "<textarea";
            String wordToFind = "readonly=\"readonly\" cols=\"80\" rows=\"25\">";
            Pattern word = Pattern.compile(wordToFind);
            Matcher match = word.matcher(dataUserPage);
            int strEnd = 0;
            while (match.find()) {
                strEnd = match.end();
            }
            String strContrato = dataUserPage.substring(strEnd, dataUserPage.length() - 1);
            wordToFind = "</textarea>";
            word = Pattern.compile(wordToFind);
            match = word.matcher(strContrato);
            int strStart = 0;
            while (match.find()) {
                strStart = match.start();
            }
            String nuevoStrContrato = strContrato.substring(0, strStart);
            return nuevoStrContrato;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public List<Contratos> getContractEBisa(String numeroCliente, String tipoContrato) throws SQLException {
        String cantidadTicket = "";
        org.openlogics.gears.jdbc.ObjectDataStore ds = null;
        List<Contratos> lista = null;
        try {
            String URI = "x0sql2.grupobisa.net:1207/Stadistic;useLOBs=false";//medioAmbiente.getValorDe(CONEXION_DB2_TICKETING_LOCAL, CONEXION_DB2_TICKETING_LOCAL_DEFAULT);
            String user = "ebisausr";//medioAmbiente.getValorDe(USER_CONEXION_DB2_TICKETING_LOCAL, USER_CONEXION_DB2_TICKETING_LOCAL_DEFAULT);
//            String pass = "crypt:WLFgPZFoZRZbbv7C6A7pMA==";//medioAmbiente.getValorDe(PASS_CONEXION_DB2_TICKETING_LOCAL, PASS_CONEXION_DB2_TICKETING_LOCAL_DEFAULT);
            String pass = "ebisa123";//medioAmbiente.getValorDe(PASS_CONEXION_DB2_TICKETING_LOCAL, PASS_CONEXION_DB2_TICKETING_LOCAL_DEFAULT);
            Class.forName(COM_SQLSERVER_DRIVER);
//            URI = URI.replaceAll("LOCAL", agenciaTicket.getServDB());
            ds = DataStoreFactory.createObjectDataStore(DriverManager.getConnection(JDBC_SQLSERVER + URI, user, pass));
            ds.setAutoClose(false);
            List<Map<String, Object>> map = ds.select(Query.of("select clw.RqUID,clw.CustPermID, clw.AccountFrom, clw.RqDate, clw.FileContract,clw.TypeContract  FROM contract_log_web clw where clw.CustPermID='" + numeroCliente + "' and clw.TypeContract='" + tipoContrato + "' "), new MapListHandler());
            String fechaRegistro = null;
            String numeroCuenta = null;
            lista = new ArrayList<>();
            Contratos contratos = null;
            for (Map<String, Object> stringObjectMap : map) {
                contratos = new Contratos();
                contratos.setCodigoCliente((String) stringObjectMap.get("AccountFrom"));
                contratos.setDetalleContrato((String) stringObjectMap.get("FileContract"));
                fechaRegistro = FormatosUtils.fechaHoraLargaFormateada((java.util.Date) stringObjectMap.get("RqDate"));
//                contratos.setFechaRegistro(fechaRegistro);
                contratos.setNumeroCuenta((String) stringObjectMap.get("AccountFrom"));
                contratos.setTipoContrato(TipoContrato.valueOf((String) stringObjectMap.get("TypeContract")));
                contratos.setId((BigDecimal) stringObjectMap.get("RqUID"));
                lista.add(contratos);
                contratos = null;
            }

        } catch (SQLException ex) {
            LOGGER.error("Error al conectarse al DB2 ticketing", ex);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {

//            ds.tryCommitAndClose();
        }
        return lista;
    }


    public String registTintLogWeb(Contratos contratos) throws SQLException {

        try {
            if (contratos.getDetalleContrato() == null || contratos.getDetalleContrato().isEmpty()) {
                throw new Exception("Informacion Contrato vcia.");
            }
            String strContrato=searchDataContract(contratos.getDetalleContrato());
            contratos.setDetalleContrato(strContrato);
            contratosDao.persist(contratos);
            return "success";
        } catch (SQLException ex) {
            LOGGER.error("Error al conectarse al DB2 ticketing", ex);
            return "failed";
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return "failed";
        } catch (Exception e) {
            return "failed";
        }
//        return cantidadTicket;
    }

}

