/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.monitor.dao;

import bus.database.dao.DaoImplCached;
import bus.database.model.BasePrincipal;
import bus.env.api.MedioAmbiente;
import bus.monitor.entities.Cfp75501;
import bus.monitor.entities.Cfp75502;
import bus.monitor.model.Cfp755;
import bus.monitor.model.Cfp755Id;
import com.google.inject.Inject;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.Serializable;

import static bus.env.api.Variables.CONSULTA_ERRORES_CACHE;
import static bus.env.api.Variables.CONSULTA_ERRORES_CACHE_DEFAULT;

/**
 * @author Marcelo Morales
 *         Date: 9/14/11
 *         Time: 11:13 AM
 */
public class Cfp755Dao extends DaoImplCached<Cfp75501, Serializable> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Cfp755Dao.class);

    private final MedioAmbiente medioAmbiente;

    @Inject
    public Cfp755Dao(@BasePrincipal EntityManagerFactory entityManagerFactory, Ehcache cache,
                     MedioAmbiente medioAmbiente) {
        super(entityManagerFactory, cache);
        this.medioAmbiente = medioAmbiente;
    }

    public String getByMsgN(final String msgn) {

        if (StringUtils.isBlank(msgn)) {
            return "-";
        }

        final String cacheKey = "CFP755." + msgn;
        final Element element = ehcache.get(cacheKey);
        if (element != null && !element.isExpired()) {
            return (String) element.getObjectValue();
        }

        final String mensajeEncontrado = doWithTransaction((entityManager, t) -> {

            final boolean usarcfp75501;
            if (StringUtils.isNumeric(msgn)) {
                final int i = Integer.parseInt(msgn);
                usarcfp75501 = i < 400;
            } else {
                usarcfp75501 = true;
            }

            final Cfp755 cfp755;
            if (usarcfp75501) {
                cfp755 = entityManager.find(Cfp75501.class, new Cfp755Id(1L, msgn));
            } else {
                cfp755 = entityManager.find(Cfp75502.class, new Cfp755Id(1L, msgn));
            }

            if (cfp755 == null) {
                LOGGER.error("No se ha encontrado el error '" + msgn + "' en la CFP755", new Throwable());
                return "Error desconocido";
            }
            return cfp755.getCfmsg();
        });

        Element toPutElement = new Element(cacheKey, mensajeEncontrado);
        Integer ttl = medioAmbiente.getValorIntDe(CONSULTA_ERRORES_CACHE, CONSULTA_ERRORES_CACHE_DEFAULT);
        toPutElement.setTimeToLive(ttl);
        ehcache.put(toPutElement);
        return mensajeEncontrado;
    }
}
