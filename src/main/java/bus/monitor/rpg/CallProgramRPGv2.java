package bus.monitor.rpg;

import static bus.env.api.Variables.RPG_INTENTOS_INVOCACION;
import static bus.env.api.Variables.RPG_INTENTOS_INVOCACION_DEFAULT;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.*;

import bus.database.dao.AmbienteActualDao;
import bus.env.api.MedioAmbiente;
import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionAuditOtrosErrores;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.as400.AS400Factory;
import bus.plumbing.utils.Shutdowner;
import bus.plumbing.utils.Watch;
import bus.plumbing.utils.Watches;
import com.ibm.as400.access.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



import com.google.inject.Inject;

/**
 *
 * La clase <code>CallProgramRPGv2.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 05/08/2013, 17:22:29
 *
 */
public class CallProgramRPGv2{

    private static final Logger LOG = LoggerFactory.getLogger(CallProgramRPGv2.class);
    private static final Logger LOGTRACE = LoggerFactory.getLogger(CallProgramRPGv2.class.getName() + ".trace");

    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(5);

    private final Watch watchEspera400 = new Watch(Watches.MONITOR_400);

    private final MedioAmbiente medioAmbiente;

    private final AmbienteActualDao ambienteActual;

    protected final AS400Factory as400factory;

    @Inject
    public CallProgramRPGv2(
            AS400Factory as400factory, MedioAmbiente medioAmbiente, AmbienteActualDao ambienteActual, Shutdowner shutdowner)
            throws ConnectionPoolException, ImposibleLlamarProgramaRpgException {
        this.medioAmbiente = medioAmbiente;
        this.ambienteActual = ambienteActual;
        this.as400factory = as400factory;
        shutdowner.register(new Runnable() {

            @Override
            public void run() {
                EXECUTOR_SERVICE.shutdown();
            }
        });
    }


    /**
     * Método principal que ejecuta la invocación al programa AS400
     * con los parámetros necesarios de entrada y salida
     *
     * @param parametrosRPG
     *
     * @return
     *
     * @throws ImposibleLeerRespuestaException
     * @throws IOException
     * @throws TransaccionEfectivaException
     * @throws SistemaCerradoException
     * @throws ImposibleLlamarProgramaRpgException
     * @throws ConnectionPoolException
     */
    public ProgramParameter[] ejecutar(ParametrosRPG parametrosRPG) throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException,
            TimeoutException {

        LOGTRACE.debug("0) La llamada al programa se va a ejecutar, verificando si estamos offline.");
        if (ambienteActual.getAmbienteActual().isOffline()) {
            throw new SistemaCerradoException();
        }
        // Ruta completa de la aplicacion RPG
        LOGTRACE.debug("1) Verificando si tenemos la ruta del programa a ser invocado.");
        final String entradaCompletaRPG = parametrosRPG.getFullPathRPG();
        if(entradaCompletaRPG==null){
            throw new ImposibleLlamarProgramaRpgException("No se tiene la ruta de la aplicacion RPG a ser llamada.");
        }

        // Vector de parametros para enviar al programa
        LOGTRACE.debug("2) Verificando si tenemos los parametros para el programa a ser invocado.");
        ProgramParameter[] parametrosPrograma= parametrosRPG.getParameters();
        if(parametrosPrograma==null){
            throw new ImposibleLlamarProgramaRpgException("No se tiene los parámetros para invocar al programa RPG: "+entradaCompletaRPG+".");
        }

        long t = System.currentTimeMillis();

        int intentos = 0;
        final Integer maxIntentos = medioAmbiente.getValorIntDe(RPG_INTENTOS_INVOCACION, RPG_INTENTOS_INVOCACION_DEFAULT);

        LOGTRACE.debug("3) Iniciando ejecucion para llamar al programa de la AS400.");
        LOG.info("Iniciando ejecucion para llamar al programa de la AS400 {}.", entradaCompletaRPG);
        String parametros = parametrosRPG.listarParametros(as400factory);
        LOG.debug("Parametros Inciales: "+ parametros);
        AS400Message[] as400Mensajes=null;

        if(as400factory==null){
            onError(TransaccionAuditOtrosErrores.SEGU);
            throw new ImposibleLlamarProgramaRpgException("No se puede obtener el objeto de AS400Factory para invocar al programa RPG.");
        }

        while (true) {
            AS400 as400 = as400factory.getAS400(AS400.COMMAND);
            try {
                watchEspera400.start();
                //as400factory.getAS400( )
                if(as400==null){
                    onError(TransaccionAuditOtrosErrores.SEGU);
                    throw new ImposibleLlamarProgramaRpgException("No se puede obtener el objeto de AS400 para invocar al programa RPG.");
                }
                LOGTRACE.debug("4) Se ha obtenido el objeto as400 en {} milisegundos.", (System.currentTimeMillis() - t));
                t = System.currentTimeMillis();

                LOGTRACE.debug("5) Preparando la llamada al programa [{}] de la AS400.", entradaCompletaRPG);

                final ProgramCall  pgmAS400 = new ProgramCall(as400);

                pgmAS400.setProgram(entradaCompletaRPG);
                pgmAS400.setParameterList(parametrosPrograma);

                LOG.debug("LLAMADA AL PROGRAMA AS400: Enviando la siguiente cadena (IBM284) <{}> al programa [{}] de la as400 [{}].",
                        new Object[]{parametros, entradaCompletaRPG, as400});

                LOGTRACE.debug("6) Ejecutando la llamada al programa [{}] de la AS400.", entradaCompletaRPG);

                Callable<Boolean> runnable = new Callable<Boolean>() {

                    @Override
                    public Boolean call() throws Exception {
                        return pgmAS400.run();
                    }
                };

                boolean respuesta = EXECUTOR_SERVICE.submit(runnable).get(30, TimeUnit.SECONDS);

                if (!respuesta) {
                    LOGTRACE.debug("7) Error en la llamada al programa [{}] de la AS400.", entradaCompletaRPG);
                    LOG.error("Error en la llamada al programa [{}] de la AS400.", entradaCompletaRPG);

                    as400Mensajes = pgmAS400.getMessageList();
                    int totalMensajes = as400Mensajes.length;
                    for (int i = 0; i < totalMensajes; i++) {
                        LOG.debug("Resultado Mensaje del AS400: {}:{}", as400Mensajes[i].getSeverity(), as400Mensajes[i].getText());
                    }
                    break;
                }

                LOGTRACE.debug("8) Finalizando la llamada al programa [{}] de la AS400.", entradaCompletaRPG);
                LOG.debug("Parametros Finales: "+ parametrosRPG.listarParametros(as400factory));
                if (respuesta) {break;}
                if (intentos > maxIntentos) {break;}

            } catch (ExecutionException ex) {
                if (ex.getCause() instanceof ObjectDoesNotExistException) {
                    onError(TransaccionAuditOtrosErrores.QENE);
                    throw new IllegalStateException(entradaCompletaRPG + " - Objeto AS400 NO existe.", ex);
                }
                if (ex.getCause() instanceof InterruptedException) {
                    onError(TransaccionAuditOtrosErrores.INTE);
                    // Esto puede dar si el hilo de comunicacion con la 400 se tira
                    throw new RuntimeException(
                            "No se ha encontrado respuesta a la escritura en el dataqueue y la espera" +
                                    " interna de la as/400 se ha interrumpido (esto realmente NO DEBERIA DARSE)", ex);
                }
                if (ex.getCause() instanceof AS400SecurityException) {
                    onError(TransaccionAuditOtrosErrores.SEGU);
                    // El securityException NO se pasa.
                    throw new IllegalStateException("Error de seguridad en la as/400: " +
                            AS400Factory.getDescripcionSecurityException((AS400SecurityException) ex.getCause()), ex);
                }
                if (ex.getCause() instanceof ReturnCodeException) {
                    if (intentos >= maxIntentos) {
                        onError(TransaccionAuditOtrosErrores.MCHI);
                        LOG.error("Cantidad maxima de reintentos sobrepasada ({}), lanzando la excepcion original.",
                                intentos);
                        throw new RuntimeException(ex);
                    }
                    LOG.error("Ha ocurrido error '{}' returncode={} al escribir en la cola {} al intento {} reintentando...",
                            new Object[]{ex.getMessage(), ((ReturnCodeException) ex.getCause()).getReturnCode(), entradaCompletaRPG, intentos});

                } else if (ex.getCause() instanceof SocketException || ex.getCause() instanceof SocketTimeoutException) {
                    if (intentos >= maxIntentos) {
                        onError(TransaccionAuditOtrosErrores.MCHI);
                        LOG.error("Cantidad maxima de reintentos sobrepasada ({}), lanzando la excepcion original.",intentos);
                        // XXX: esta excepcion debe ser final!!!!
                        throw (IOException) ex.getCause();
                    }
                    LOG.error("Ha ocurrido error de red '{}' al escribir en la cola {} al intento {} reintentando...",
                            new Object[]{entradaCompletaRPG, ex.getMessage(), intentos});
                } else if (ex.getCause() instanceof IOException) {
                    LOG.error("Error <IOException> al llamar al programa [{}] de AS400.",entradaCompletaRPG);
                    watchEspera400.stop();
                    throw new RuntimeException(ex);
                }
            } catch (InterruptedException e) {
                onError(TransaccionAuditOtrosErrores.INTE);
                // Esto puede dar si el hilo de comunicacion con la 400 se tira
                throw new RuntimeException(
                        "No se ha encontrado respuesta a la escritura en el dataqueue y la espera" +
                                " interna de la as/400 se ha interrumpido (esto realmente NO DEBERIA DARSE)", e);
            } catch (TimeoutException e) {
                onError(TransaccionAuditOtrosErrores.TIME);
                throw e;
            } catch (PropertyVetoException e) {
                LOG.error("Error <PropertyVetoException> al llamar al programa [{}] de AS400.",entradaCompletaRPG);
                onError(TransaccionAuditOtrosErrores.MCHI);
                throw new RuntimeException(e);
            } finally {
                watchEspera400.stop();
                // Desconecta el servicio y retorna el objeto al pool de conexiones
                as400factory.ret(as400, AS400.COMMAND);
            }

        }

        LOGTRACE.debug("9) Terminado del proceso de llamada al programa AS400 {} en {} milisegundos", entradaCompletaRPG, (System.currentTimeMillis() - t));
        LOG.info("Terminado del proceso de llamada al programa AS400 {} en {} milisegundos", entradaCompletaRPG, (System.currentTimeMillis() - t));
        return parametrosPrograma;
    }

    protected void onError(TransaccionAuditOtrosErrores ioe) {
        watchEspera400.stop();

    }

    public AS400Factory getAs400factory(){
        return as400factory;
    }

}


