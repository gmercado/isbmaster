package bus.monitor.rpg;


import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.as400.AS400Factory;
import com.google.inject.Inject;
import com.ibm.as400.access.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by ccalle on 26-01-16.
 * No funciona siempre da timeout
 */
@Deprecated
public class DiaHabilUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(DiaHabilUtil.class);

    private final CallProgramRPGv2 callProgramRPGv2;
    private final MedioAmbiente medioAmbiente;
    private final AS400Factory as400Factory;

    @Inject
    public DiaHabilUtil(AS400Factory as400factory, MedioAmbiente medioAmbiente,
                        CallProgramRPGv2 callProgramRPGv2)throws ConnectionPoolException, ImposibleLlamarProgramaRpgException {
        this.as400Factory = as400factory;
        this.callProgramRPGv2 = callProgramRPGv2;
        this.medioAmbiente = medioAmbiente;
    }

    /**
     *
     * @param fecha yyyyMMDD
     * @return true si es valido
     * @throws ImposibleLeerRespuestaException
     * @throws IOException
     * @throws TransaccionEfectivaException
     * @throws SistemaCerradoException
     * @throws ImposibleLlamarProgramaRpgException
     * @throws ConnectionPoolException
     * @throws TimeoutException
     */
    public boolean ejecutar(final int fecha) throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException, TimeoutException {

        boolean resultado = false;
        ParametrosRPG parametrosRPG = new ParametrosRPG() {

            @Override
            public String getFullPathRPG() {
                String libreriaPrograma = medioAmbiente.getValorDe(Variables.LIBRERIA_SGTE_DIA_HABIL, Variables.LIBRERIA_SGTE_DIA_HABIL_DEFAULT);
                String rutaPrograma = new QSYSObjectPathName(libreriaPrograma,
                        medioAmbiente.getValorDe(Variables.PROGRAMA_SGTE_DIA_HABIL, Variables.PROGRAMA_SGTE_DIA_HABIL_DEFAULT), "PGM").getPath();
                return rutaPrograma;
            }

            @Override
            public ProgramParameter[] loadParameters() {

                /*if(fecha == null){
                    LOGGER.error("Error No se pueden obtener parametros.");
                    return null;
                }*/




//c     *entry        plist
//c                   parm                    spfecA            8 0          AAAAMMDD Fec.Actual
//c                   parm                    rpDias            5 0          + O -
//c                   parm                    spNohabil         1            N=Habil,S=No Habil                  // Vector de parametros
                ProgramParameter[] parametros = new ProgramParameter[3];


                AS400PackedDecimal decParm = new AS400PackedDecimal(8,0);
                AS400PackedDecimal decParm2 = new AS400PackedDecimal(5,0);
                AS400ZonedDecimal as400ZonedDecimal = new AS400ZonedDecimal(8, 0);
                AS400ZonedDecimal as400ZonedDecimal2 = new AS400ZonedDecimal(5, 0);

                ProgramParameter parm0 = new ProgramParameter(as400ZonedDecimal.toBytes(fecha)); //Parametro Ingreso 1

                ProgramParameter parm1 = new ProgramParameter(as400ZonedDecimal.toBytes(1));

                ProgramParameter parm2 = new ProgramParameter(1); //Parametro Salida 1

                parametros[0]=parm0;
                parametros[1]=parm1;
                parametros[2]=parm2;

                return parametros;
            }

        };

        // Invocando al programa RPG de AS400
        try {
            callProgramRPGv2.ejecutar(parametrosRPG);
        } catch (TimeoutException e) {
            LOGGER.warn("Timeout al verificar dia habil "+fecha);
            throw e;
        }

        // Respuesta del programa
        // Evaluando la respuesta del programa
        // +++++++++ Parametro Salida 2



        String valor1 = StringUtils.trimToEmpty(parametrosRPG.obtenerParametro(as400Factory, 1));
        LOGGER.info("respuesta rpg valor1 {}",valor1);
        if(StringUtils.trimToNull(valor1)!=null){
            LOGGER.info("respuesta rpg valor1 {}",valor1);
            if("S".equalsIgnoreCase(valor1)){
                resultado=true;
            }
        }else{
            LOGGER.warn("La respuesta del verificador de dia habil no es S supondremos que no es habil["+fecha+"]");
        }
        return resultado;
    }
}
