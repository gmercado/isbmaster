package bus.monitor.rpg;

import bus.database.dao.AmbienteActualDao;
import bus.env.api.MedioAmbiente;
import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionAuditOtrosErrores;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.as400.AS400Factory;
import bus.plumbing.utils.Watch;
import bus.plumbing.utils.Watches;
import com.ibm.as400.access.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import static bus.env.api.Variables.RPG_INTENTOS_INVOCACION;
import static bus.env.api.Variables.RPG_INTENTOS_INVOCACION_DEFAULT;

/**
 * Clase Abstracta para invocar a un programa de AS400
 * @author Roger Chura
 *  @since 1.0 
 */
public abstract class CallProgramRPG {

    private static final Logger LOG = LoggerFactory.getLogger(CallProgramRPG.class);
    private static final Logger LOGTRACE = LoggerFactory.getLogger(CallProgramRPG.class.getName() + ".trace");
        
    private final Watch watchEspera400 = new Watch(Watches.MONITOR_400);
    
    private final MedioAmbiente medioAmbiente;

    private final AmbienteActualDao ambienteActual;
    
    protected final AS400Factory as400factory; 
    
    public CallProgramRPG( 
            AS400Factory as400factory, MedioAmbiente medioAmbiente, AmbienteActualDao ambienteActual) throws ConnectionPoolException, ImposibleLlamarProgramaRpgException {
        this.medioAmbiente = medioAmbiente;
        this.ambienteActual = ambienteActual; 
        
        this.as400factory = as400factory;
    }
    
    /**
     * Define la ruta completa del programa AS400 a ser invocado
     * @return una cadena que contiene el procedimiento a llamar
     */
    protected abstract String getEntradaCompletaRPG();
    
    /**
     * Define la lista de parametros para el programa de AS400
     * @return la lista de parametros.
     */
    protected abstract ProgramParameter[] getParametros();
    
    /**
     * M�todo principal que ejecuta la invocaci�n al programa AS400
     * con los par�metros necesarios de entrada y salida
     *
     * @throws ImposibleLeerRespuestaException
     * @throws IOException
     * @throws TransaccionEfectivaException
     * @throws SistemaCerradoException
     * @throws ImposibleLlamarProgramaRpgException
     */
    public ProgramParameter[] ejecutar() throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException {

        LOGTRACE.debug("0) La llamada al programa se va a ejecutar, verificando si estamos offline.");
        if (ambienteActual.getAmbienteActual().isOffline()) {
            throw new SistemaCerradoException();
        }
        // Ruta completa de la aplicacion RPG
        LOGTRACE.debug("1) Verificando si tenemos la ruta del programa a ser invocado.");
        final String entradaCompletaRPG = getEntradaCompletaRPG();
        if(entradaCompletaRPG==null){
            throw new ImposibleLlamarProgramaRpgException("No se tiene la ruta de la aplicacion RPG a ser llamada.");
        }

        // Vector de parametros para enviar al programa
        LOGTRACE.debug("2) Verificando si tenemos los parametros para el programa a ser invocado.");
        ProgramParameter[] parametrosPrograma= getParametros();
        if(parametrosPrograma==null){
            throw new ImposibleLlamarProgramaRpgException("No se tiene los par�metros para invocar al programa RPG: "+entradaCompletaRPG+".");
        }
        
        long t = System.currentTimeMillis();
        
        int intentos = 0;
        final Integer maxIntentos = medioAmbiente.getValorIntDe(RPG_INTENTOS_INVOCACION, RPG_INTENTOS_INVOCACION_DEFAULT);

        LOGTRACE.debug("3) Iniciando ejecucion para llamar al programa de la AS400.");
        LOG.info("Iniciando ejecucion para llamar al programa de la AS400 {}.", entradaCompletaRPG);
        String parametros = listarParametros();
        LOG.debug("Parametros Inciales: "+ parametros);        
        AS400Message[] as400Mensajes;
        
        while (true) {
            ProgramCall pgmAS400;
            AS400 as400 = as400factory.getAS400(AS400.COMMAND);
            try {
                watchEspera400.start();
                if (as400==null) {
                    onError(TransaccionAuditOtrosErrores.SEGU);
                    throw new ImposibleLlamarProgramaRpgException("No se puede obtener el objeto de AS400 para invocar al programa RPG.");
                }
                LOGTRACE.debug("4) Se ha obtenido el objeto as400 en {} milisegundos.", (System.currentTimeMillis() - t));
                t = System.currentTimeMillis();
                
                LOGTRACE.debug("5) Preparando la llamada al programa [{}] de la AS400.", entradaCompletaRPG);
                pgmAS400 = new ProgramCall(as400);

                pgmAS400.setProgram(entradaCompletaRPG);
                pgmAS400.setParameterList(parametrosPrograma); 
                
                LOG.info("LLAMADA AL PROGRAMA AS400: Enviando la siguiente cadena (IBM284) <{}> al programa [{}] de la as400 [{}].",
                        new Object[]{parametros, entradaCompletaRPG, as400});
                
                LOGTRACE.debug("6) Ejecutando la llamada al programa [{}] de la AS400.", entradaCompletaRPG);
                
                boolean respuesta = pgmAS400.run(); 

                if (!respuesta) {
                    LOGTRACE.debug("7) Error en la llamada al programa [{}] de la AS400.", entradaCompletaRPG); 
                    LOG.error("Error en la llamada al programa [{}] de la AS400.", entradaCompletaRPG);
                    
                    as400Mensajes = pgmAS400.getMessageList();
                    for (AS400Message as400Mensaje : as400Mensajes) {
                        LOG.info("Resultado Mensaje del AS400: {}:{}", as400Mensaje.getSeverity(), as400Mensaje.getText());
                    }
                    
                    continue;
                 }
                
                LOGTRACE.debug("8) Finalizando la llamada al programa [{}] de la AS400.", entradaCompletaRPG);
                
                LOG.info("Parametros Finales: "+ listarParametros());

                if (respuesta || ++intentos > maxIntentos) {
                    break;
                }

            } catch (ObjectDoesNotExistException ex) {
                onError(TransaccionAuditOtrosErrores.QENE);
                throw new IllegalStateException(entradaCompletaRPG + " NO existe", ex);
            } catch (ErrorCompletingRequestException ex) {
                // Error de protocolo: la red esta loca y habria que intentarlo de nuevo
                if (intentos >= maxIntentos) {
                    onError(TransaccionAuditOtrosErrores.MCHI);
                    LOG.error(
                            "Cantidad maxima de reintentos sobrepasada ({}), lanzando la excepcion original envuelta.",
                            intentos);
                    throw new IllegalStateException(ex);
                }
                LOG.warn("Ha ocurrido error '{}' returncode={} al escribir en la cola {} al intento {} reintentando",
                        new Object[]{ex.getMessage(), ex.getReturnCode(), entradaCompletaRPG, intentos});
            } catch (InterruptedException ex) {
                onError(TransaccionAuditOtrosErrores.INTE);
                // Esto puede dar si el hilo de comunicacion con la 400 se tira
                throw new RuntimeException(
                        "No se ha encontrado respuesta a la escritura en el dataqueue y la espera" +
                                " interna de la as/400 se ha interrumpido (esto realmente NO DEBERIA DARSE)", ex);
            } catch (AS400SecurityException ex) {
                onError(TransaccionAuditOtrosErrores.SEGU);
                // El securityException NO se pasa.
                throw new IllegalStateException("Error de seguridad en la as/400: " +
                        AS400Factory.getDescripcionSecurityException(ex), ex);
            } catch (ConnectionDroppedException ex) {
                if (intentos >= maxIntentos) {
                    onError(TransaccionAuditOtrosErrores.MCHI);
                    LOG.error("Cantidad maxima de reintentos sobrepasada ({}), lanzando la excepcion original.",
                            intentos);
                    throw new RuntimeException(ex);
                }
                LOG.warn(
                        "Ha ocurrido error '{}' returncode={} al escribir en la cola {} al intento {} reintentando...",
                        new Object[]{ex.getMessage(), ex.getReturnCode(), entradaCompletaRPG, intentos});
            } catch (SocketException ex) {
                if (intentos >= maxIntentos) {
                    onError(TransaccionAuditOtrosErrores.MCHI);
                    LOG.error("Cantidad maxima de reintentos sobrepasada ({}), lanzando la excepcion original.",
                            intentos);
                    // XXX: esta excepcion debe ser final!!!!
                    throw ex;
                }
                LOG.warn("Ha ocurrido error de red '{}' al escribir en la cola {} al intento {} reintentando...",
                        new Object[]{entradaCompletaRPG, ex.getMessage(), intentos});
            } catch (SocketTimeoutException ex) {
                if (intentos >= maxIntentos) {
                    onError(TransaccionAuditOtrosErrores.MCHI);
                    LOG.error("Cantidad maxima de reintentos sobrepasada ({}), lanzando la excepcion original.",
                            intentos);
                    throw ex;
                }
                LOG.warn("Ha ocurrido error de red '{}' al escribir en la cola {} al intento {} reintentando...",
                        new Object[]{entradaCompletaRPG, ex.getMessage(), intentos});
            } catch (IOException e) {
                 watchEspera400.stop();
                 throw new RuntimeException(e);
                
            } catch (PropertyVetoException e) {
                onError(TransaccionAuditOtrosErrores.MCHI);
                throw new RuntimeException(e);
                
            } finally {
                watchEspera400.stop();
                // Desconecta el servicio y retorna el objeto al pool de conexiones
                as400factory.ret(as400, AS400.COMMAND);
            }
            
        }
        
        LOGTRACE.debug("9) Terminado del proceso de llamada al programa AS400 {} en {} milisegundos", entradaCompletaRPG, (System.currentTimeMillis() - t));
        LOG.info("Terminado del proceso de llamada al programa AS400 {} en {} milisegundos", entradaCompletaRPG, (System.currentTimeMillis() - t));
        return parametrosPrograma;
    }

    /**
     * Obtiene o retorna un parametro con la posicion indicada en la lista de parametros 
     * @param posicion
     * @return
     */
    protected String obtenerParametro(int posicion){
        ProgramParameter[] parametrosPrograma = getParametros();
        
        if(parametrosPrograma==null){
          LOG.error("No se tienen los parametros para ser buscado en la invocacion al programa RPG.");
          return null;
        }
        if(posicion<0){
            LOG.error("El indice de la posicion para buscar en la lista de parametros es negativa.");
            return null;
        }

        
        AS400Text convertirAS400 = null;
        String valor = "";
        try {
            for(int i=posicion; i<=posicion; i++){
                if(parametrosPrograma[i].getInputData()!=null){
                    convertirAS400 = new AS400Text(parametrosPrograma[i].getInputData().length, as400factory.getCcsid());
                    valor = (String) convertirAS400.toObject(parametrosPrograma[i].getInputData());
                }
                
                if(parametrosPrograma[i].getOutputData()!=null){
                    convertirAS400 = new AS400Text(parametrosPrograma[i].getOutputDataLength(), as400factory.getCcsid());
                    valor = (String) convertirAS400.toObject(parametrosPrograma[i].getOutputData());
                    }
            }            
        } catch (Exception e) {
            LOG.error("Error al obtener el valor del parametro en la posicion {}.",posicion, e);  
            valor = "ERROR_OBTENER_VALOR";
        }

        return valor;
    }
    
    /**
     * Obtiene la lista de parametros y los lista en una cadena String para ser revisados en el LOG 
     * @return
     */
    protected String listarParametros(){
        ProgramParameter[] parametrosPrograma = getParametros();
        if(parametrosPrograma==null){
          LOG.error("No se tiene los parametros para ser mostrados en la invocacion al programa RPG.");
          return null;
        }
        StringBuffer paramEntrada = new StringBuffer();
        StringBuffer paramSalida = new StringBuffer();
        
        AS400Text convertirAS400 = null;
        String valor = "";
        
        for(int i=0; i<parametrosPrograma.length; i++){
            if(parametrosPrograma[i].getInputData()!=null){
                
                convertirAS400 = new AS400Text(parametrosPrograma[i].getInputData().length, as400factory.getCcsid ());
                valor = (String) convertirAS400.toObject(parametrosPrograma[i].getInputData());
                paramEntrada.append("Param Input["+i+"]="+valor+";");
            }
            
            if(parametrosPrograma[i].getOutputData()!=null){
                convertirAS400 = new AS400Text(parametrosPrograma[i].getOutputDataLength(), as400factory.getCcsid ());
                valor = (String) convertirAS400.toObject(parametrosPrograma[i].getOutputData());
                paramSalida.append("Param Output["+i+"]="+valor+";");
                }
        }
        
        return paramEntrada.toString()+paramSalida.toString();
    }
    
    
    /**
     * Obtiene o retorna un parametro con la posicion indicada en la lista de parametros y el tipo de dato
     * @param posicion
     * @return
     */
    protected String obtenerParametro(int posicion, String tipoDato, int digitos){
        ProgramParameter[] parametrosPrograma = getParametros();
        
        if(parametrosPrograma==null){
          LOG.error("No se tienen los parametros para ser buscado en la invocacion al programa RPG.");
          return null;
        }
        if(posicion<0){
            LOG.error("El indice de la posicion para buscar en la lista de parametros es negativa.");
            return null;
        }
        
        AS400Text aS400Text = null;
        AS400Bin2 as400Bin2 = null;
        AS400Bin4 as400Bin4 = null;
        AS400Bin8 as400Bin8 = null;
        AS400PackedDecimal as400PackedDecimal = null;
        
        String valor = "";
        try {
            for(int i=posicion; i<=posicion; i++){
                if("STRING".equalsIgnoreCase(tipoDato)){
                    if(parametrosPrograma[i].getInputData()!=null){
                        aS400Text = new AS400Text(parametrosPrograma[i].getInputData().length, as400factory.getCcsid ());
                        valor = (String) aS400Text.toObject(parametrosPrograma[i].getInputData());
                    }
                    
                    if(parametrosPrograma[i].getOutputData()!=null){
                        aS400Text = new AS400Text(parametrosPrograma[i].getOutputDataLength(), as400factory.getCcsid ());
                        valor = (String) aS400Text.toObject(parametrosPrograma[i].getOutputData());
                        }                    
                }
                if("INT2".equalsIgnoreCase(tipoDato)){
                    as400Bin2 = new AS400Bin2();
                    if(parametrosPrograma[i].getInputData()!=null){
                        short corto = as400Bin2.toShort(parametrosPrograma[i].getInputData());
                        valor = ""+corto;
                    }
                    if(parametrosPrograma[i].getOutputData()!=null){
                        short corto = as400Bin2.toShort(parametrosPrograma[i].getOutputData());
                        valor = ""+corto;
                    }
                }
                if("INT4".equalsIgnoreCase(tipoDato)){
                    as400Bin4 = new AS400Bin4();
                    if(parametrosPrograma[i].getInputData()!=null){
                        int entero = as400Bin4.toInt(parametrosPrograma[i].getInputData());
                        valor = ""+entero;
                    }
                    if(parametrosPrograma[i].getOutputData()!=null){
                        int entero = as400Bin4.toInt(parametrosPrograma[i].getOutputData());
                        valor = ""+entero;
                    }                    
                }
                if("INT8".equalsIgnoreCase(tipoDato)){
                    as400Bin8 = new AS400Bin8();
                    if(parametrosPrograma[i].getInputData()!=null){
                        long largo = as400Bin8.toLong(parametrosPrograma[i].getInputData());
                        valor = ""+largo;
                    }
                    if(parametrosPrograma[i].getOutputData()!=null){
                        long largo = as400Bin8.toLong(parametrosPrograma[i].getOutputData());
                        valor = ""+largo;
                    }                             
                }
                
                if("BIGDEC".equalsIgnoreCase(tipoDato)){
                    as400PackedDecimal = new AS400PackedDecimal(digitos, 0);
                    if(parametrosPrograma[i].getInputData()!=null){
                        BigDecimal decimal = (BigDecimal) as400PackedDecimal.toObject(parametrosPrograma[i].getInputData());
                        valor = ""+decimal.toString();
                    }
                    if(parametrosPrograma[i].getOutputData()!=null){
                        BigDecimal decimal = (BigDecimal) as400PackedDecimal.toObject(parametrosPrograma[i].getOutputData());
                        valor = ""+decimal.toString();
                    }                             
                }                
                
            }            
        } catch (Exception e) {
            LOG.error("Error al obtener el valor del parametro en la posicion {}.",posicion, e);  
            valor = "ERROR_AL_OBTENER_VALOR";
        }

        return valor;
    }
    
    
    protected void onError(TransaccionAuditOtrosErrores ioe) {
        watchEspera400.stop();

    }
    

    /*
    public AmbienteActualBean getAmbienteActual() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        AmbienteActualBean ambienteActualBean = new AmbienteActualBean();
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("Select E20OFFLNE, E20AMTEMP, E20VER from EBP20");


            if (!resultSet.next()) {
                ambienteActualBean.setVersion(0L);
                ambienteActualBean.setOffline(false);
                ambienteActualBean.setTemporal(false);
                return ambienteActualBean;
            }

            ambienteActualBean.setVersion(resultSet.getLong("E20VER"));
            ambienteActualBean.setTemporal(resultSet.getBoolean("E20AMTEMP"));
            ambienteActualBean.setOffline(resultSet.getBoolean("E20OFFLNE"));
            return ambienteActualBean;

        } catch (SQLException e) {
            LOG.error("No tengo EBP20, asumiendo como viene", e);
            ambienteActualBean.setVersion(0L);
            ambienteActualBean.setOffline(false);
            ambienteActualBean.setTemporal(false);
            return ambienteActualBean;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOG.error("Error al cerrar", e);
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOG.error("Error al cerrar", e);
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOG.error("Error al cerrar ds", e);
                }
            }
        }
    }
    */
}

