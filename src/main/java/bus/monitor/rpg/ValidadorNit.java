package bus.monitor.rpg;


import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import bus.monitor.api.ImposibleLeerRespuestaException;
import bus.monitor.api.SistemaCerradoException;
import bus.monitor.api.TransaccionEfectivaException;
import bus.monitor.as400.AS400Factory;
import com.google.inject.Inject;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ConnectionPoolException;
import com.ibm.as400.access.ProgramParameter;
import com.ibm.as400.access.QSYSObjectPathName;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by ccalle on 26-01-16.
 */
public class ValidadorNit {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidadorNit.class);

    private final CallProgramRPGv2 callProgramRPGv2;
    private final MedioAmbiente medioAmbiente;
    private final AS400Factory as400Factory;

    @Inject
    public ValidadorNit(AS400Factory as400factory, MedioAmbiente medioAmbiente,
                        CallProgramRPGv2 callProgramRPGv2)throws ConnectionPoolException, ImposibleLlamarProgramaRpgException {
        this.as400Factory = as400factory;
        this.callProgramRPGv2 = callProgramRPGv2;
        this.medioAmbiente = medioAmbiente;
    }

    /**
     *
     * @param nit
     * @return true si es valido
     * @throws ImposibleLeerRespuestaException
     * @throws IOException
     * @throws TransaccionEfectivaException
     * @throws SistemaCerradoException
     * @throws ImposibleLlamarProgramaRpgException
     * @throws ConnectionPoolException
     * @throws TimeoutException
     */
    public boolean ejecutar(final String nit) throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException, ImposibleLlamarProgramaRpgException, ConnectionPoolException, TimeoutException {

        boolean resultado = false;
        ParametrosRPG parametrosRPG = new ParametrosRPG() {

            @Override
            public String getFullPathRPG() {
                String libreriaPrograma = medioAmbiente.getValorDe(Variables.LIBRERIA_VALIDACION_NIT, Variables.LIBRERIA_VALIDACION_NIT_DEFAULT);
                String rutaPrograma = new QSYSObjectPathName(libreriaPrograma,
                        medioAmbiente.getValorDe(Variables.PROGRAMA_VALIDACION_NIT, Variables.PROGRAMA_VALIDACION_NIT_DEFAULT), "PGM").getPath();
                return rutaPrograma;
            }

            @Override
            public ProgramParameter[] loadParameters() {

                if(nit == null){
                    LOGGER.error("Error No se puede obtener el NIT a ser validado.");
                    return null;
                }

                // NT0004 - Rutina Digito Verificador
                // El programa de AS400 <MSVA06> requiere 5 parametros: 3 de ingreso y 2 de salida
                // 0034.00 c     *entry        plist
                //0035.00 c                   parm                    wpalfa           14
                //0036.00 c                   parm                    wptag             1

                // Vector de parametros
                ProgramParameter[] parametros = new ProgramParameter[2];

                // Conversion de parametros de entrada
                AS400Text paramconveter40 = new AS400Text(14, as400Factory.getCcsid());

                ProgramParameter parm0 = new ProgramParameter(paramconveter40.toBytes(nit)); //Parametro Ingreso 1
                ProgramParameter parm1 = new ProgramParameter(1); //Parametro Salida 4

                parametros[0]=parm0;
                parametros[1]=parm1;

                return parametros;
            }

        };

        // Invocando al programa RPG de AS400
        try {
            callProgramRPGv2.ejecutar(parametrosRPG);
        } catch (TimeoutException e) {
            LOGGER.warn("Timeout al validar NIT "+nit);
            throw e;
        }

        // Respuesta del programa
        // Evaluando la respuesta del programa
        // +++++++++ Parametro Salida 2



        String valor1 = StringUtils.trimToEmpty(parametrosRPG.obtenerParametro(as400Factory, 1));
        if(StringUtils.trimToNull(valor1)!=null){
            if("1".equalsIgnoreCase(valor1)){
                resultado=true;
            }
        }else{
            LOGGER.warn("La respuesta del validador de NIT es nula supondremos que el NIT no es valido, NIT["+nit+"]");
        }
        return resultado;
    }
}
