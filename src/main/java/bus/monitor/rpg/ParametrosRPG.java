package bus.monitor.rpg;

import java.math.BigDecimal;

import bus.monitor.as400.AS400Factory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ibm.as400.access.AS400Bin2;
import com.ibm.as400.access.AS400Bin4;
import com.ibm.as400.access.AS400Bin8;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

/**
 *
 * La clase <code>ParametrosRPG.java</code> tiene la funcion de...
 *
 * @author David Cuevas
 * @version 1.0, 05/08/2013, 18:05:32
 *
 */
public abstract class ParametrosRPG {
    private static final Logger LOGGER = LoggerFactory.getLogger(ParametrosRPG.class);

    private ProgramParameter[] parameters;

    /**
     * La ruta debe incluir la(s) librería(s) utilizadas y el programa a ejecutar.
     *
     * @return Retorna una cadena completa pero que deberia utilizar un <code>new QSYSObjectPathName(libreria,programa,"PGM").getPath()</code>
     */
    public abstract String getFullPathRPG();

    /**
     * Retornamos un arreglo de parámetros <code>ProgramParameter[]</code>
     * que será utilizado por el programa RPG.
     * Cada elemento del arreglo debe contener un objeto <code>Parameter</code> que dimensione el espacio en bytes del tipo de dato del parametro.
     *
     * Ej. <a href="http://pic.dhe.ibm.com/infocenter/iseries/v6r1m0/index.jsp?topic=/rzahh/dtxmp.htm">Documentation IBM</a>
     *
     * @return
     */
    public abstract ProgramParameter[] loadParameters();

    public final ProgramParameter[] getParameters(){
        if (parameters==null){
            parameters = loadParameters();
        }
        return parameters;
    }

    public void detachParameters(){
        parameters = null;
    }

    public void setParameters(ProgramParameter[] parameters){
        this.parameters = parameters;
    }

    /**
     * Obtiene o retorna un parametro con la posicion indicada en la lista de parametros
     * @param posicion
     * @return
     */
    public String obtenerParametro(AS400Factory as400factory, int posicion){
        ProgramParameter[] parametrosPrograma = getParameters();

        if(parametrosPrograma==null){
            LOGGER.error("No se tienen los parametros para ser buscado en la invocacion al programa RPG.");
            return null;
        }
        if(posicion<0){
            LOGGER.error("El indice de la posicion para buscar en la lista de parametros es negativa.");
            return null;
        }


        AS400Text convertirAS400 = null;
        String valor = "";
        try {
            for(int i=posicion; i<=posicion; i++){
                if(parametrosPrograma[i].getInputData()!=null){
                    convertirAS400 = new AS400Text(parametrosPrograma[i].getInputData().length, as400factory.getCcsid());
                    valor = (String) convertirAS400.toObject(parametrosPrograma[i].getInputData());
                }

                if(parametrosPrograma[i].getOutputData()!=null){
                    convertirAS400 = new AS400Text(parametrosPrograma[i].getOutputDataLength(), as400factory.getCcsid());
                    valor = (String) convertirAS400.toObject(parametrosPrograma[i].getOutputData());
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error al obtener el valor del parametro en la posicion {}.",posicion, e);
            valor = "ERROR_OBTENER_VALOR";
        }

        return valor;
    }

    /**
     * Obtiene o retorna un parametro con la posicion indicada en la lista de parametros y el tipo de dato
     * @param posicion
     * @return
     */
    public String obtenerParametro(AS400Factory as400factory, int posicion, String tipoDato, int digitos){
        ProgramParameter[] parametrosPrograma = getParameters();

        if(parametrosPrograma==null){
            LOGGER.error("No se tienen los parametros para ser buscado en la invocacion al programa RPG.");
            return null;
        }
        if(posicion<0){
            LOGGER.error("El indice de la posicion para buscar en la lista de parametros es negativa.");
            return null;
        }

        AS400Text aS400Text = null;
        AS400Bin2 as400Bin2 = null;
        AS400Bin4 as400Bin4 = null;
        AS400Bin8 as400Bin8 = null;
        AS400PackedDecimal as400PackedDecimal = null;

        String valor = "";
        try {
            for(int i=posicion; i<=posicion; i++){
                if("STRING".equalsIgnoreCase(tipoDato)){
                    if(parametrosPrograma[i].getInputData()!=null){
                        aS400Text = new AS400Text(parametrosPrograma[i].getInputData().length, as400factory.getCcsid ());
                        valor = (String) aS400Text.toObject(parametrosPrograma[i].getInputData());
                    }

                    if(parametrosPrograma[i].getOutputData()!=null){
                        aS400Text = new AS400Text(parametrosPrograma[i].getOutputDataLength(), as400factory.getCcsid ());
                        valor = (String) aS400Text.toObject(parametrosPrograma[i].getOutputData());
                    }
                }
                if("INT2".equalsIgnoreCase(tipoDato)){
                    as400Bin2 = new AS400Bin2();
                    if(parametrosPrograma[i].getInputData()!=null){
                        short corto = as400Bin2.toShort(parametrosPrograma[i].getInputData());
                        valor = ""+corto;
                    }
                    if(parametrosPrograma[i].getOutputData()!=null){
                        short corto = as400Bin2.toShort(parametrosPrograma[i].getOutputData());
                        valor = ""+corto;
                    }
                }
                if("INT4".equalsIgnoreCase(tipoDato)){
                    as400Bin4 = new AS400Bin4();
                    if(parametrosPrograma[i].getInputData()!=null){
                        int entero = as400Bin4.toInt(parametrosPrograma[i].getInputData());
                        valor = ""+entero;
                    }
                    if(parametrosPrograma[i].getOutputData()!=null){
                        int entero = as400Bin4.toInt(parametrosPrograma[i].getOutputData());
                        valor = ""+entero;
                    }
                }
                if("INT8".equalsIgnoreCase(tipoDato)){
                    as400Bin8 = new AS400Bin8();
                    if(parametrosPrograma[i].getInputData()!=null){
                        long largo = as400Bin8.toLong(parametrosPrograma[i].getInputData());
                        valor = ""+largo;
                    }
                    if(parametrosPrograma[i].getOutputData()!=null){
                        long largo = as400Bin8.toLong(parametrosPrograma[i].getOutputData());
                        valor = ""+largo;
                    }
                }

                if("BIGDEC".equalsIgnoreCase(tipoDato)){
                    as400PackedDecimal = new AS400PackedDecimal(digitos, 0);
                    if(parametrosPrograma[i].getInputData()!=null){
                        BigDecimal decimal = (BigDecimal) as400PackedDecimal.toObject(parametrosPrograma[i].getInputData());
                        valor = ""+decimal.toString();
                    }
                    if(parametrosPrograma[i].getOutputData()!=null){
                        BigDecimal decimal = (BigDecimal) as400PackedDecimal.toObject(parametrosPrograma[i].getOutputData());
                        valor = ""+decimal.toString();
                    }
                }

            }
        } catch (Exception e) {
            LOGGER.error("Error al obtener el valor del parametro en la posicion {}.",posicion, e);
            valor = "ERROR_AL_OBTENER_VALOR";
        }

        return valor;
    }

    /**
     * Obtiene la lista de parametros y los lista en una cadena String para ser revisados en el LOG
     * @return
     */
    public String listarParametros(AS400Factory as400factory){
        ProgramParameter[] parametrosPrograma = getParameters();
        if(parametrosPrograma==null){
            LOGGER.error("No se tiene los parametros para ser mostrados en la invocacion al programa RPG.");
            return null;
        }
        StringBuffer paramEntrada = new StringBuffer();
        StringBuffer paramSalida = new StringBuffer();

        AS400Text convertirAS400 = null;
        String valor = "";

        for(int i=0; i<parametrosPrograma.length; i++){
            if(parametrosPrograma[i].getInputData()!=null){

                convertirAS400 = new AS400Text(parametrosPrograma[i].getInputData().length, as400factory.getCcsid ());
                valor = (String) convertirAS400.toObject(parametrosPrograma[i].getInputData());
                paramEntrada.append("Param Input["+i+"]="+valor+";");
            }

            if(parametrosPrograma[i].getOutputData()!=null){
                convertirAS400 = new AS400Text(parametrosPrograma[i].getOutputDataLength(), as400factory.getCcsid ());
                valor = (String) convertirAS400.toObject(parametrosPrograma[i].getOutputData());
                paramSalida.append("Param Output["+i+"]="+valor+";");
            }
        }

        return paramEntrada.toString()+paramSalida.toString();
    }




}
