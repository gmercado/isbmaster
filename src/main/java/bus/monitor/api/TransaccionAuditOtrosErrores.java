/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.monitor.api;

/**
 * @author Marcelo Morales
 */
public enum TransaccionAuditOtrosErrores {

    /**
     * Muchos errores.
     */
    MCHI("Muchos intentos"),

    /**
     * Error de seguridad.
     */
    SEGU("Error de seguridad"),

    /**
     * Tiempo de espera agotado (timeout).
     */
    TIME("Tiempo de espera agotado"),

    /**
     * Tiempo de espera agotado y revertida automaticamente.
     */
    TIMR("Tiempo de espera agotado y revertida automaticamente"),

    /**
     * Transaccion realizada pero no se pudo leer el estado.
     */
    TREF("Transaccion realizada pero no se pudo leer el estado"),

    /**
     * Cola de transacciones no existe.
     */
    QENE("Cola de transacciones no existe"),

    /**
     * Cola de transacciones no es un Dataqueue.
     */
    QENQ("Cola de transacciones no es un Dataqueue"),

    /**
     * Comunicacion interrumpida con el sistema principal.
     */
    INTE("Comunicacion interrumpida con el sistema principal"),

    /**
     * OK.
     */
    TROK("Intentada correctamente"),;

    TransaccionAuditOtrosErrores(String descripcion) {
        this.descripcion = descripcion;
    }

    private final String descripcion;

    public String getDescripcion() {
        return descripcion;
    }
}
