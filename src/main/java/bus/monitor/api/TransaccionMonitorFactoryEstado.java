/*
 *  Copyright 2009 Banco Bisa S.A.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package bus.monitor.api;

public enum TransaccionMonitorFactoryEstado {

    PRODUCCION("Ambiente PRODUCCION (posiblemente ZBNKPRD01)"),
    TEMPORAL("Amtiente TEMPORAL (posiblemente ZTBNKPRD01)"),
    CERRADO("Ambiente CERRADO, no se hacen transacciones en el monitor"),
    INDETERMINADO("Error, no se puede determinar el ambiente actual");

    private String descripcion;

    TransaccionMonitorFactoryEstado(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
