/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.monitor.api;

import bus.monitor.model.PSR9101Bean;
import com.ibm.as400.access.Record;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * @author Marcelo Morales
 *         Date: 9/14/11
 *         Time: 10:50 AM
 */
public class TransaccionNoEjecutadaCorrectamenteException extends Exception implements IExcepcionMonitorEn400 {

    private static final long serialVersionUID = -3538705550197990246L;

    private final String byMsgN;

    private PSR9101Bean resultado;

    private final Record cabecera;

    public TransaccionNoEjecutadaCorrectamenteException(String mensaje, Record cabecera) {
        super(mensaje);
        this.cabecera = cabecera;
        byMsgN = "";
    }

    public TransaccionNoEjecutadaCorrectamenteException(PSR9101Bean resultado, Record cabecera) {
        super();
        this.resultado = resultado;
        this.cabecera = cabecera;
        byMsgN = null;
    }

    public TransaccionNoEjecutadaCorrectamenteException(String message, IErroresMonitor dao, PSR9101Bean<?, ?, ?> resultado, Record cabecera) {
        super(message);
        this.cabecera = cabecera;
        byMsgN = dao.getByMsgN(getMessage());
        this.resultado = resultado;
    }

    public TransaccionNoEjecutadaCorrectamenteException(String message, Throwable cause, IErroresMonitor dao, PSR9101Bean<?, ?, ?> resultado, Record cabecera) {
        super(message, cause);
        this.resultado = resultado;
        this.cabecera = cabecera;
        byMsgN = dao.getByMsgN(getMessage());
    }

    public TransaccionNoEjecutadaCorrectamenteException(Throwable cause, PSR9101Bean<?, ?, ?> resultado, Record cabecera) {
        super(cause);
        this.resultado = resultado;
        this.cabecera = cabecera;
        byMsgN = null;
    }

    public PageParameters getParameters() {
        final PageParameters params;
        if (StringUtils.isEmpty(byMsgN)) {
            final String message = getMessage();
            params = new PageParameters().add("desc", message);
        } else {
            params = new PageParameters().add("desc", byMsgN);
        }
        return params;
    }

    public String getByMsgN() {
        return byMsgN;
    }

    public PSR9101Bean getResultado() {
        return resultado;
    }

    public Record getCabeceraEnvio() {
        return cabecera;
    }
}
