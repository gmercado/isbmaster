package bus.monitor.api;

import java.io.IOException;

/**
 * @author Marcelo Morales
 *         Since: 4/22/13
 */
public interface WithMonitorSinRespuesta {

    void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException;
}
