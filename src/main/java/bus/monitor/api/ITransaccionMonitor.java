package bus.monitor.api;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * @author Marcelo Morales
 *         Since: 4/22/13
 */
public interface ITransaccionMonitor {

    void setCuerpo1EnvioFormat(String formato) throws IOException, SistemaCerradoException;

    void setCuerpo2EnvioFormat(String formato) throws IOException, SistemaCerradoException;

    void setCuerpo1RecepcionFormat(String formato) throws IOException, SistemaCerradoException;

    void setCuerpo2RecepcionFormat(String formato) throws IOException, SistemaCerradoException;

    void setDatoCabeceraEnvio(String llave, String valorDe) throws IOException, SistemaCerradoException;

    void setDatoCabeceraEnvio(String llave, int valorIntDe) throws IOException, SistemaCerradoException;

    void setDatoCabeceraEnvio(String llave, BigDecimal valorDe) throws IOException, SistemaCerradoException;

    void setDatoCuerpo1(String llave, BigDecimal valor) throws IOException, SistemaCerradoException;

    void setDatoCuerpo1(String llave, String valor) throws IOException, SistemaCerradoException;

    void setDatoCuerpo1(String llave, int valor);

    void setDatoCuerpo2(String llave, BigDecimal valor) throws IOException, SistemaCerradoException;

    void setDatoCuerpo2(String llave, String valor) throws IOException, SistemaCerradoException;

    void setDatoCuerpo2(String llave, int valor);
}
