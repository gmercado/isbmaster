/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.monitor.api;

import bus.database.model.AmbienteActual;
import bus.env.api.MedioAmbiente;
import bus.monitor.as400.AS400Factory;
import bus.monitor.dao.TransaccionAuditDao;
import bus.monitor.entities.TransaccionAudit;
import bus.monitor.model.PSR9101Bean;
import bus.plumbing.utils.Watch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static bus.plumbing.utils.Watches.MONITOR_MILIS;

/**
 * @author Marcelo Morales
 */
public class TransaccionMonitorAuditable extends TransaccionMonitor {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransaccionMonitorAuditable.class);

    private final ExecutorService executor;

    private final TransaccionAuditDao transaccionAuditDao;

    private TransaccionAudit transaccionAudit;

    private Future<TransaccionAudit> submited;

    private Watch watchMilis = new Watch(MONITOR_MILIS);

    private long tl;

    private long te;

    private long diff;

    private final String grupo;

    public TransaccionMonitorAuditable(AS400Factory as400factory,
                                       AmbienteActual ambienteActual,
                                       ExecutorService executor,
                                       TransaccionAuditDao transaccionAuditDao,
                                       MedioAmbiente medioAmbiente,
                                       @Nullable String grupo) {
        super(as400factory, ambienteActual, medioAmbiente);
        this.executor = executor;
        this.transaccionAuditDao = transaccionAuditDao;

        transaccionAudit = new TransaccionAudit();
        this.grupo = grupo;
    }

    @Override
    protected void doBeforeEjecutar() throws IOException {
        watchMilis.start();
        diff = 0;
    }

    @Override
    protected void onAfterLeer() {
        diff += System.currentTimeMillis() - tl;
    }

    @Override
    protected void onBeforeLeer() {
        tl = System.currentTimeMillis();
    }

    @Override
    protected void onAfterEscribir() {
        diff += System.currentTimeMillis() - te;
    }

    @Override
    protected void onBeforeEscribir() {
        te = System.currentTimeMillis();
    }

    @Override
    protected void doDespuesDatosPorDefecto() throws IOException {
        try {
            transaccionAudit.setTransaccion((String) getCabeceraEnvio().getField("WCODTRN"));
            transaccionAudit.setUsuario((String) getCabeceraEnvio().getField("WUSUARIO"));
            transaccionAudit.setCajaEntrada((BigDecimal) getCabeceraEnvio().getField("WNUMUSR"));
            transaccionAudit.setSecuenciaEntrada((BigDecimal) getCabeceraEnvio().getField("WNUMSEQ"));
            transaccionAudit.setDevice((String) getCabeceraEnvio().getField("WDEVICE"));
            String reve = (String) getCabeceraEnvio().getField("WREVE");
            if (reve != null) {
                if (reve.length() <= 1) {
                    transaccionAudit.setWreve(reve);
                } else {
                    LOGGER.error("Reve: '{}' es muy grande, mayor a 1. Hay algo malo.", reve);
                }
            }
            transaccionAudit.setFormatoEntrada1((BigDecimal) getCabeceraEnvio().getField("WFMT1"));
            transaccionAudit.setFormatoEntrada2((BigDecimal) getCabeceraEnvio().getField("WFMT2"));
            transaccionAudit.setFormatoEntrada3((BigDecimal) getCabeceraEnvio().getField("WFMT3"));
            transaccionAudit.setTipoDeDato((BigDecimal) getCabeceraEnvio().getField("WTPODTA"));
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Ha ocurrido un error inesperado y no se puede guardar el registro de transaccion", e);
        }

        try {
            if (grupo != null) {
                setDatoCabeceraEnvio("WFLG2", grupo);
            }
        } catch (Exception e) {
            LOGGER.error("Se corre una transaccion sin sesion, no se coloca el grupo", e);
        }

    }

    @Override
    protected void onReversionAutomaticaOK(TransaccionAudit ultimaTransaccion) {
        transaccionAudit.setOtrosErrores(TransaccionAuditOtrosErrores.TIMR);
        transaccionAudit.setWreve("a");

        executor.submit(new Runnable() {

            @Override
            public void run() {
                try {
                    transaccionAudit = transaccionAuditDao.merge(transaccionAudit);
                } catch (Exception e) {
                    LOGGER.error(
                            "Ha ocurrido un error inesperado y no se puede actualizar la reversion en el registro " +
                                    transaccionAudit, e);
                    throw new RuntimeException(e); /// XXX
                }
            }
        });
    }

    @Override
    protected void onError(TransaccionAuditOtrosErrores ioe) {
        watchMilis.stop();
        transaccionAudit.setOtrosErrores(ioe);
        transaccionAudit.setMilisegundos(diff);
        transaccionAudit.setFecha(new Date());

        submited = executor.submit(new Callable<TransaccionAudit>() {

            @Override
            public TransaccionAudit call() throws Exception {
                try {
                    return transaccionAudit = transaccionAuditDao.merge(transaccionAudit);
                } catch (Exception e) {
                    LOGGER.error("Ha ocurrido un error inesperado y no se puede guardar el registro de transaccion", e);
                    throw new RuntimeException(e); // XXX
                }
            }
        });
    }

    @Override
    protected void doDespuesTransaccion() {
        watchMilis.stop();
        try {
            transaccionAudit.setOtrosErrores(TransaccionAuditOtrosErrores.TROK);
            transaccionAudit.setWpost((String) getRecordSalidaCabecera().getField("WPOST"));
            transaccionAudit.setCaja((BigDecimal) getRecordSalidaCabecera().getField("WNUMUSR"));
            transaccionAudit.setSecuencia((BigDecimal) getRecordSalidaCabecera().getField("WNUMSEQ"));
            transaccionAudit.setRej1((String) getRecordSalidaCabecera().getField("WREJ1"));
            transaccionAudit.setRej2((String) getRecordSalidaCabecera().getField("WREJ2"));
            transaccionAudit.setFecha(new Date());
            transaccionAudit.setNumeroDeControl((BigDecimal) getRecordSalidaCabecera().getField("WNUMCTL"));
            transaccionAudit.setMilisegundos(diff);
            transaccionAudit.setFormatoSalida1((BigDecimal) getRecordSalidaCabecera().getField("WFMT1"));
            transaccionAudit.setFormatoSalida2((BigDecimal) getRecordSalidaCabecera().getField("WFMT2"));
            transaccionAudit.setFormatoSalida3((BigDecimal) getRecordSalidaCabecera().getField("WFMT3"));

            submited = executor.submit(new Callable<TransaccionAudit>() {

                @Override
                public TransaccionAudit call() throws Exception {
                    try {
                        return transaccionAudit = transaccionAuditDao.merge(transaccionAudit);
                    } catch (Exception e) {
                        LOGGER.error("Ha ocurrido un error inesperado y no se puede guardar el registro de transaccion",
                                e);
                        throw new RuntimeException(e);
                    }
                }
            });
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Ha ocurrido un error inesperado y no se puede guardar el registro de transaccion", e);
        }

    }

    @Override
    public TransaccionAudit getUltimaTransaccion() {
        if (submited == null) {
            LOGGER.error("Alguien esta usando mal el API: buscando la ultima transaccion antes de hacerla",
                    new Throwable());
            return transaccionAudit;
        }
        try {
            return submited.get();
        } catch (InterruptedException e) {
            LOGGER.error("Error al obtener la ultima transaccion", e);
        } catch (ExecutionException e) {
            LOGGER.error("Error al obtener la ultima transaccion", e);
        }
        return null;
    }

    public final <X extends Serializable, Y extends Serializable, Z extends Serializable> PSR9101Bean<X, Y, Z> obtenerObjetoCompleto(X x, Y y, Z z) {
        final PSR9101Bean<X, Y, Z> xyzpsr9101Bean = super.obtenerObjetoCompleto(x, y, z);
        xyzpsr9101Bean.setTransaccionAudit(getUltimaTransaccion());
        return xyzpsr9101Bean;
    }

}
