package bus.monitor.api;


import java.io.IOException;

/**
 * @author Marcelo Morales
 *         Since: 4/22/13
 */
public interface WithMonitor<Q> {

    void preparar(ITransaccionMonitor monitor) throws IOException, SistemaCerradoException;

    Q procesar(IRespuestasMonitor monitor) throws IOException, SistemaCerradoException, TransaccionNoEjecutadaCorrectamenteException;
}
