package bus.monitor.api;

import bus.monitor.model.PSR9101Bean;
import com.ibm.as400.access.Record;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

/**
 * @author Marcelo Morales
 *         Since: 4/22/13
 */
public interface IRespuestasMonitor {

    <X extends Serializable, Y extends Serializable, Z extends Serializable> PSR9101Bean<X, Y, Z> obtenerObjetoCompleto(X x, Y y, Z z);

    <X extends Serializable, Y extends Serializable> PSR9101Bean<X, Y, String> obtenerObjetoCompleto(X x, Y y);

    <X extends Serializable> PSR9101Bean<X, String, String> obtenerObjetoCompleto(X x);

    boolean isTransaccionEjecutadaSatisfactoriamente() throws UnsupportedEncodingException;

    String getDatoRecibidoCabeceraString(String llave);

    BigDecimal getDatoRecibidoCabeceraDecimal(String llave);

    Record getCabeceraEnvio();

    Record getCabeceraRecepcion();

    String getDatoRecibidoCuerpo1String(String param);

    BigDecimal getDatoRecibidoCuerpo1Decimal(String param);

    String getDatoRecibidoCuerpo2String(String param);

    BigDecimal getDatoRecibidoCuerpo2Decimal(String param);

    boolean next1();

    boolean next2();
}
