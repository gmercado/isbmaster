package bus.monitor.api;

/**
 * @author by rsalvatierra on 01/06/2017.
 */
public interface ParametersMonitor {

    String MON_VERSION = "WVER";
    String MON_CANAL = "WCANAL";
    String MON_DEVICE = "WDEVICE";
    String MON_TRANSACCION = "WCODTRN";
    String MON_USUARIO = "WNUMUSR";
    String MON_FLAG = "WFLG";
    String MON_CUENTA = "WCUENTA";
    String MON_TPOCTA = "WTPOCTA";
    String MON_CODMONEDA = "WCODMON";
    String MON_IMPORTE = "WIMPORTE";
    String MON_MONTOTRAN = "WMONTO";

    String MON_SALDO = "WSALACT";
    String MON_SALDO1 = "WSALDO1";
    String MON_SALDO2 = "WSALDO2";
    String MON_SALDO3 = "WSALDO";
    String MON_GLOSA = "WDESC";
    String MON_GLOSA2 = "WTXTADI";
    String MON_DEBCRE = "WDEBCRE";
    String MON_NEGSAL = "WNEGSAL";
    String MON_APRUSUARIO = "WAPRUSER";
    String MON_TPODTA = "WTPODTA";
    String MON_REVE = "WREVE";
    String MON_PAG = "WPAG";
    String MON_CNT = "WCNT";
    String MON_NUMSEQ = "WNUMSEQ";
    String MON_NUMCHQ = "WNUMCHQ";
    String MON_CLSTRN = "WCLSTRN";
    String MON_POST = "WPOST";
    String MON_REJ1 = "WREJ1";
    String MON_FECHA_PROCESO = "WFECPRO";
    String MON_FECHA = "WFECHA";
    String MON_HORA = "WHORA";

    String MON_CUENTA2 = "WCUENTA2";
    String MON_TPOCTA2 = "WTPOCTA2";
    String MON_CODMONEDA2 = "WCODMON2";
    String MON_IMPORTE2 = "WIMPORTE2";

    String MON_IMPORTE_TOTAL = "WIMPTOT";
    String MON_TIPO_IMPORTE = "WTPOIMP";
    String MON_COD_ADICIONAL = "WCODADI";
    String MON_DAT_ADICIONAL = "WDTAADI";
    String MON_COD_BANCO = "WCODBCO";
    String MON_COD_SUCURSAL = "WCODSUC";
    String MON_CUENTA_BEN = "WCTABEN";
    String MON_NOMBRE_BEN = "WNOMBEN";
    String MON_NOTA = "WNOTA";
    String MON_NOMBRE = "WNOMBRE";
    String MON_TPOPROD = "WTPOPROD";
    String MON_ESTADO = "WESTADO";
    String MON_NUMERO_TC = "WNROTC";
    String MON_CAUSAL = "WDESCR";
    String MON_TIPO_REQ = "WTPOREQ";
    String MON_TPOPAG = "WTPOPAG";
    String MON_DESCEST= "WDESCEST";
    String MON_COT_USD_BOB_OF = "WOUSDBS";
    String MON_COT_USD_BOB_COM = "WCUSDBS";
    String MON_COT_USD_BOB_VEN = "WVUSDBS";

    String MON_COT_EUR_BOB_OF = "WOEURBS";
    String MON_COT_EUR_BOB_COM = "WCEURBSE";
    String MON_COT_EUR_BOB_VEN = "WVEURBSE";

    String MON_COT_EUR_USD_OF = "WOEURUSD";
    String MON_COT_EUR_USD_COM = "WCEURUSD";
    String MON_COT_EUR_USD_VEN = "WVEURUSD";

    String MON_COT_UFV_BOB_OF = "WOUFVBSR";
}
