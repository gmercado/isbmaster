package bus.monitor.api;

/**
 * @author by rsalvatierra on 01/06/2017.
 */
public interface FormatersMonitor {
    String PSR9011 = "PSR9011";
    String PSR9015 = "PSR9015";
    String PSR9020 = "PSR9020";
    String PSR9021 = "PSR9021";
    String PSR9061 = "PSR9061";

    String PSR9112 = "PSR9112";
    String PSR9113 = "PSR9113";
    String PSR9114 = "PSR9114";
    String PSR9120 = "PSR9120";

    String PSR9413 = "PSR9413";
    String PSR9416 = "PSR9416";

    String PSR9511 = "PSR9511";
    String PSR9514 = "PSR9514";

    String PSR9611 = "PSR9611";

    String PSR9913 = "PSR9913";
    String PSR9916 = "PSR9916";
}
