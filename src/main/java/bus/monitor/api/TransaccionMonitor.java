/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.monitor.api;

import bus.database.model.AmbienteActual;
import bus.env.api.MedioAmbiente;
import bus.monitor.as400.AS400Factory;
import bus.monitor.entities.TransaccionAudit;
import bus.monitor.model.PSR9101Bean;
import bus.plumbing.utils.Watch;
import com.google.inject.Inject;
import com.ibm.as400.access.*;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static bus.env.api.Variables.*;
import static bus.monitor.as400.FormatosUtils.*;
import static bus.plumbing.utils.Watches.MONITOR_400;

/**
 * @author Marcelo Morales
 */
abstract class TransaccionMonitor implements ITransaccionMonitor, IRespuestasMonitor {

    private static final Logger LOG = LoggerFactory.getLogger(TransaccionMonitor.class);

    private static final Logger LOGTRACE = LoggerFactory.getLogger(TransaccionMonitor.class.getName() + ".trace");

    public static final String CABECERA = "cabecera";
    public static final String CUERPO_1 = "cuerpo1";
    public static final String CUERPO_2 = "cuerpo2";
    public static final String CUERPO_3 = "cuerpo3";

    private final AS400Factory as400factory;

    private final AmbienteActual ambienteActual;

    private final MedioAmbiente medioAmbiente;

    private final Watch watchEspera400 = new Watch(MONITOR_400);

    private static final AtomicInteger ATOMIC_INTEGER = new AtomicInteger(new Random().nextInt(99999));

    private String cuerpo1EnvioTipo;

    private String cuerpo2EnvioTipo;

    private String cuerpo3EnvioTipo;


    private Record cabeceraEnvio;

    private Record cuerpo1Envio;

    private Record cuerpo2Envio;

    private Record cuerpo3Envio;

    private RecordFormat cabeceraRecepcionFormat;

    private Record cabeceraRecepcion;

    private RecordFormat cuerpo1RecepcionFormat;

    private String cuerpo1RecepcionTipo;

    private RecordFormat cuerpo2RecepcionFormat;

    private String cuerpo2RecepcionTipo;

    private RecordFormat cuerpo3RecepcionFormat;

    private String cuerpo3RecepcionTipo;

    private ArrayList<Record> cuerpo1Records;

    private Iterator<Record> cuerpo1Iterator;

    private Record cuerpo1Actual;

    private ArrayList<Record> cuerpo2Records;

    private Iterator<Record> cuerpo2Iterator;

    private Record cuerpo2Actual;

    private ArrayList<Record> cuerpo3Records;

    private Iterator<Record> cuerpo3Iterator;

    private Record cuerpo3Actual;

    private byte[] dataSalida;

    /**
     * true cuando se haya ejecutado como consecuencia de esta ejecucion: sirve para romper la reversa de
     * la reversa de la reversa, etc. cuando la cola da timeout.
     */
    private boolean ejecucionInterna = false;

    @Inject
    public TransaccionMonitor(AS400Factory as400factory, AmbienteActual ambienteActual, MedioAmbiente medioAmbiente) {
        this.as400factory = as400factory;
        this.ambienteActual = ambienteActual;
        this.medioAmbiente = medioAmbiente;
    }

    public final TransaccionMonitor ejecutar() throws ImposibleLeerRespuestaException, IOException,
            TransaccionEfectivaException, SistemaCerradoException {

        LOGTRACE.debug("0) El monitor se va a ejecutar, verificando si estamos offline");

        if (ambienteActual.isOffline()) {
            throw new SistemaCerradoException();
        }

        verificarEstado();

        LOGTRACE.debug("1) Iniciando ejecucion transaccion monitor");
        long t = System.currentTimeMillis();

        doBeforeEjecutar();
        Validate.notNull(as400factory, "Necesitamos AS/400");
        Validate.notNull(cabeceraEnvio, "No ha colocado cabecera de envio");
        Validate.notNull(cabeceraRecepcionFormat, "No ha colocado cabecera de recepcion");
        cabeceraRecepcion = null;
        cuerpo1Records = null;
        cuerpo1Iterator = null;
        cuerpo1Actual = null;
        cuerpo2Records = null;
        cuerpo2Iterator = null;
        cuerpo2Actual = null;
        cuerpo3Records = null;
        cuerpo3Iterator = null;
        cuerpo3Actual = null;
        doDespuesValidar();
        cabeceraEnvio.setField("WLRGREG", new BigDecimal(cabeceraEnvio.getRecordLength()));
        if (cuerpo1Envio != null) {
            cuerpo1Envio.setField("WLRGREG", new BigDecimal(cuerpo1Envio.getRecordLength()));
        }
        if (cuerpo2Envio != null) {
            cuerpo2Envio.setField("WLRGREG", new BigDecimal(cuerpo2Envio.getRecordLength()));
        }
        if (cuerpo3Envio != null) {
            cuerpo3Envio.setField("WLRGREG", new BigDecimal(cuerpo3Envio.getRecordLength()));
        }
        doDespuesCalcularLongitudes();

        LOGTRACE.debug("2) Se han calculado longitudes en {} milisegundos", (System.currentTimeMillis() - t));
        t = System.currentTimeMillis();

        try {

            // Siempre coloca WVERIF="S" en la cabecera
            setDatoCabeceraEnvio("WVERIF", "S");

            String deviceReal = (String) cabeceraEnvio.getField("WDEVICE");
            if (deviceReal == null || "".equals(deviceReal.trim())) {
                String devicePrefix = medioAmbiente.getValorDe(MONITOR_DEVICE_PREFIX, MONITOR_DEVICE_PREFIX_DEFAULT);
                int device = ATOMIC_INTEGER.incrementAndGet() %
                        Integer.valueOf(StringUtils.rightPad("1", 10 - devicePrefix.length(), "0"));
                deviceReal = devicePrefix + StringUtils.leftPad("" + device, 10 - devicePrefix.length(), "0");
                LOG.debug("Usando device aleatorio '{}'", deviceReal);
                cabeceraEnvio.setField("WDEVICE", deviceReal);
            }

            String usuarioReal = (String) cabeceraEnvio.getField("WUSUARIO");
            if (usuarioReal == null || "".equals(usuarioReal.trim())) {
                cabeceraEnvio.setField("WUSUARIO", as400factory.getUsername());
            }

            if (LOG.isDebugEnabled()) {
                BigDecimal bd = (BigDecimal) cabeceraEnvio.getField("WNUMCTL");
                if (bd != null && !bd.equals(BigDecimal.ZERO)) {
                    LOG.debug("Se sobre-escrie el WNUMCTL. Puede darse en segunda transaccion o bien se ha definido WNUMCTL " +
                            "erroneamente en el programa que llama a ejecutar.");
                }
            }
            int keyval = ATOMIC_INTEGER.incrementAndGet() % 100000;
            LOG.debug("Key = {}", keyval);
            final BigDecimal key = new BigDecimal(keyval).abs();

            cabeceraEnvio.setField("WNUMCTL", key);

            if (cuerpo1Envio != null) {
                cabeceraEnvio.setField("WFMT1", new BigDecimal(cuerpo1EnvioTipo));
                cabeceraEnvio.setField("WLRG1", new BigDecimal(cuerpo1Envio.getRecordLength()));
                cabeceraEnvio.setField("WOCU1", BigDecimal.ONE);
            }

            if (cuerpo2Envio != null) {
                cabeceraEnvio.setField("WFMT2", new BigDecimal(cuerpo2EnvioTipo));
                cabeceraEnvio.setField("WLRG2", new BigDecimal(cuerpo2Envio.getRecordLength()));
                cabeceraEnvio.setField("WOCU2", BigDecimal.ONE);
            }

            if (cuerpo3Envio != null) {
                cabeceraEnvio.setField("WFMT3", new BigDecimal(cuerpo3EnvioTipo));
                cabeceraEnvio.setField("WLRG3", new BigDecimal(cuerpo3Envio.getRecordLength()));
                cabeceraEnvio.setField("WOCU3", BigDecimal.ONE);
            }

            doDespuesDatosPorDefecto();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                cabeceraEnvio.getContents(baos);
                if (cuerpo1Envio != null) {
                    cuerpo1Envio.getContents(baos);
                }
                if (cuerpo2Envio != null) {
                    cuerpo2Envio.getContents(baos);
                }
                if (cuerpo3Envio != null) {
                    cuerpo3Envio.getContents(baos);
                }
                baos.close();
            } catch (IOException iOException) {
                throw new RuntimeException(iOException);
            }
            byte[] dataEnvio = baos.toByteArray();
            dataEnvio = mutarDataEnvio(Arrays.copyOf(dataEnvio, dataEnvio.length));

            String ibm284 = new String(dataEnvio, "IBM284");

            LOGTRACE.debug("3) Se ha preparado la transaccion en {} milisegundos", (System.currentTimeMillis() - t));
            t = System.currentTimeMillis();

            final Integer maxIntentos = medioAmbiente.getValorIntDe(MONITOR_INTENTOS_ESCRITURA, MONITOR_INTENTOS_ESCRITURA_DEFAULT);
            long antestransaccion = System.currentTimeMillis();

            final String libreriaColas;
            if (ambienteActual.isAmbienteTemporal()) {
                libreriaColas = medioAmbiente.getValorDe(LIBRERIA_COLAS_MONITOR_TEMPORAL, LIBRERIA_COLAS_MONITOR_TEMPORAL_DEFAULT);
                LOG.debug("Estamos en ambiente TEMPORAL [Libreria:{}].", libreriaColas);
            } else {
                libreriaColas = medioAmbiente.getValorDe(LIBRERIA_COLAS_MONITOR, LIBRERIA_COLAS_MONITOR_DEFAULT);
                LOG.debug("Estamos en ambiente PRODUCCION [Libreria:{}].", libreriaColas);
            }

            int intentos = 0;
            while (true) {
                final String colaEntradaCompleta = new QSYSObjectPathName(libreriaColas,
                        medioAmbiente.getValorDe(LIBRERIA_COLA_IN, LIBRERIA_COLA_IN_DEFAULT), "DTAQ").getPath();
                AS400 as400 = null;
                try {
                    intentos++;
                    as400 = as400factory.getAS400(AS400.DATAQUEUE);
                    LOG.debug("Enviando la siguiente cadena (IBM284) <{}> a la cola {} de la as400 {}",
                            new Object[]{new String(dataEnvio, "IBM284"), colaEntradaCompleta, as400});

                    LOGTRACE.debug("4) Se ha obtenido el as400 en {} milisegundos", (System.currentTimeMillis() - t));
                    t = System.currentTimeMillis();

                    DataQueue dq = new DataQueue(as400, colaEntradaCompleta);

                    onBeforeEscribir();

                    watchEspera400.start();
                    try {
                        dq.write(dataEnvio);
                    } finally {
                        watchEspera400.stop();
                    }

                    LOGTRACE.debug("5) Informacion escrita en DQ en {} milisegundos", (System.currentTimeMillis() - t));
                    t = System.currentTimeMillis();

                    LOG.debug("Informacion escrita correctamente en la AS/400. Compromiso de commit.");
                    break;
                } catch (ObjectDoesNotExistException ex) {
                    onError(TransaccionAuditOtrosErrores.QENE);
                    throw new IllegalStateException(colaEntradaCompleta + " NO existe", ex);
                } catch (IllegalObjectTypeException ex) {
                    onError(TransaccionAuditOtrosErrores.QENQ);
                    throw new IllegalStateException(colaEntradaCompleta + " NO es un Dataqueue", ex);
                } catch (ErrorCompletingRequestException ex) {
                    // Error de protocolo: la red esta loca y habria que intentarlo de nuevo
                    if (intentos >= maxIntentos) {
                        onError(TransaccionAuditOtrosErrores.MCHI);
                        LOG.error(
                                "Cantidad maxima de reintentos sobrepasada ({}), lanzando la excepcion original envuelta.",
                                intentos);
                        throw new IllegalStateException(ex);
                    }
                    LOG.warn("Ha ocurrido error '{}' returncode={} al escribir en la cola {} al intento {} reintentando",
                            new Object[]{ex.getMessage(), ex.getReturnCode(), colaEntradaCompleta, intentos});
                } catch (InterruptedException ex) {
                    onError(TransaccionAuditOtrosErrores.INTE);
                    // Esto puede dar si el hilo de comunicacion con la 400 se tira
                    throw new RuntimeException(
                            "No se ha encontrado respuesta a la escritura en el dataqueue y la espera" +
                                    " interna de la as/400 se ha interrumpido (esto realmente NO DEBERIA DARSE)", ex);
                } catch (AS400SecurityException ex) {
                    onError(TransaccionAuditOtrosErrores.SEGU);
                    // El securityException NO se pasa.
                    throw new IllegalStateException("Error de seguridad en la as/400: " +
                            AS400Factory.getDescripcionSecurityException(ex), ex);
                } catch (ConnectionDroppedException ex) {
                    if (intentos >= maxIntentos) {
                        onError(TransaccionAuditOtrosErrores.MCHI);
                        LOG.error("Cantidad maxima de reintentos sobrepasada ({}), lanzando la excepcion original.",
                                intentos);
                        throw new RuntimeException(ex);
                    }
                    LOG.warn(
                            "Ha ocurrido error '{}' returncode={} al escribir en la cola {} al intento {} reintentando...",
                            new Object[]{ex.getMessage(), ex.getReturnCode(), colaEntradaCompleta, intentos});
                } catch (SocketException ex) {
                    if (intentos >= maxIntentos) {
                        onError(TransaccionAuditOtrosErrores.MCHI);
                        LOG.error("Cantidad maxima de reintentos sobrepasada ({}), lanzando la excepcion original.",
                                intentos);
                        // XXX: esta excepcion debe ser final!!!!
                        throw ex;
                    }
                    LOG.warn("Ha ocurrido error de red '{}' al escribir en la cola {} al intento {} reintentando...",
                            new Object[]{colaEntradaCompleta, ex.getMessage(), intentos});
                } catch (SocketTimeoutException ex) {
                    if (intentos >= maxIntentos) {
                        onError(TransaccionAuditOtrosErrores.MCHI);
                        LOG.error("Cantidad maxima de reintentos sobrepasada ({}), lanzando la excepcion original.",
                                intentos);
                        throw ex;
                    }
                    LOG.warn("Ha ocurrido error de red '{}' al escribir en la cola {} al intento {} reintentando...",
                            new Object[]{colaEntradaCompleta, ex.getMessage(), intentos});
                } catch (ConnectionPoolException e) {
                    if (intentos >= maxIntentos) {
                        onError(TransaccionAuditOtrosErrores.MCHI);
                        LOG.error("Cantidad maxima de reintentos sobrepasada ({}), lanzando la excepcion original.",
                                intentos);
                        throw new RuntimeException(e);
                    }
                    LOG.warn("Ha ocurrido error de red '{}' al escribir en la cola {} al intento {} reintentando...",
                            new Object[]{colaEntradaCompleta, e.getMessage(), intentos});
                } finally {
                    onAfterEscribir();
                    as400factory.ret(as400, AS400.DATAQUEUE);
                }
            }

            LOGTRACE.debug("6) Terminado del proceso de escritura en {} milisegundos", (System.currentTimeMillis() - t));
            t = System.currentTimeMillis();

            final String colaSalidaCompleta = new QSYSObjectPathName(libreriaColas,
                    medioAmbiente.getValorDe(LIBRERIA_COLA_OUT, LIBRERIA_COLA_OUT_DEFAULT), "DTAQ").getPath();

            /*
             * ********************************************************************************************************
             * ATENCION
             * ********************************************************************************************************
             * - esto puede lanzar una excepcion y vamos a intentar esperar unicamente para revertir.
             * - entonces, para no perder el stack completo, envolvemos y volvemos a lanzar la misma envuelta (hack)
             * ********************************************************************************************************
             */
            final Integer timeout = medioAmbiente.getValorIntDe(MONITOR_TIMEOUT, MONITOR_TIMEOUT_DEFAULT);
            try {
                onBeforeLeer();
                t = leerDelDataQueue(t, deviceReal, key, colaSalidaCompleta, timeout);
            } catch (ImposibleLeerRespuestaException imposibleLeerRespuestaException) {
                ///
                final String finalDeviceReal = deviceReal;
                if (ejecucionInterna) {
                    LOG.error("La reversion de la transaccion por timeout ha dado timeout, duh?", imposibleLeerRespuestaException);
                    return this;
                }

                final RecordFormat cef = cabeceraEnvio.getRecordFormat();
                final RecordFormat crf = cabeceraRecepcionFormat;
                final byte[] cabeceraEnvioContents = cabeceraEnvio.getContents();

                BigDecimal wtpodta = (BigDecimal) cabeceraEnvio.getField("WTPODTA");
                if (wtpodta.intValue() != 1) {
                    LOG.info("No se ha podido leer el resultado de una transaccion, pero esta no tiene efecto" +
                                    " (WTPODTA enviado={}, WCODTRN={}), no debe intentar revertirse automaticamente. " +
                                    " Cuerpo1 = {}",
                            new Object[]{wtpodta, cabeceraEnvio.getField("WCODTRN"), cuerpo1Envio});

                    throw new ImposibleLeerRespuestaException(
                            "Tiempo de espera agotado",
                            imposibleLeerRespuestaException,
                            ImposibleLeerRespuestaException.CODIGO_TIMEOUT
                    );
                }

                LOG.info("No se ha logrado leer la respuesta a la transaccion con efecto (WTPODTA enviado={}, WCODTRN={})," +
                                "se intenta revertirla en otro hilo: cuerpo1 = {}",
                        new Object[]{wtpodta, cabeceraEnvio.getField("WCODTRN"), cuerpo1Envio});

                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        onBeforeIntentarRevertirAutomaticamente();
                        try {
                            final Integer timeout = medioAmbiente.getValorIntDe(MONITOR_REVERSION_TIMEOUT, MONITOR_REVERSION_TIMEOUT_DEFAULT);
                            LOG.info("Volviendo a leer la transaccion {} para su reversa", key);
                            leerDelDataQueue(0, finalDeviceReal, key, colaSalidaCompleta, timeout);
                            procesarResultadoDelDataQueue();

                            if (!isTransaccionEjecutadaSatisfactoriamente()) {
                                LOG.info("La transaccion que dio timeout no ha funcionado, no hago naga");
                                return;
                            }

                            TransaccionMonitor transaccionMonitor = new TransaccionMonitor(
                                    as400factory, ambienteActual, medioAmbiente) {
                            };
                            transaccionMonitor.cabeceraEnvio = new Record(cef, cabeceraEnvioContents);
                            transaccionMonitor.ejecucionInterna = true;
                            transaccionMonitor.cabeceraRecepcionFormat = crf;

                            transaccionMonitor.setDatoCabeceraEnvio("WTPODTA", 6);
                            transaccionMonitor.setDatoCabeceraEnvio("WREVE", "S");

                            BigDecimal wnumusr = (BigDecimal) cabeceraRecepcion.getField("WNUMUSR");
                            BigDecimal wnumseq = (BigDecimal) cabeceraRecepcion.getField("WNUMSEQ");

                            transaccionMonitor.setDatoCabeceraEnvio("WNUMUSR", wnumusr);
                            transaccionMonitor.setDatoCabeceraEnvio("WNUMSEQ", wnumseq);

                            transaccionMonitor.ejecutar();

                            if (!transaccionMonitor.isTransaccionEjecutadaSatisfactoriamente()) {
                                LOG.error("NO se ha podido realizar la reversa de la transaccion numusr={}, numseq={}",
                                        wnumusr, wnumseq);
                                return;
                            }

                            onReversionAutomaticaOK(transaccionMonitor.getUltimaTransaccion());

                        } catch (Exception exinterna) {
                            LOG.error("No se ha podido ejecutar la reversion automatica (por timeout)", exinterna);
                        }
                    }
                }, "Reversion por tiempo agotado " + key).start();

                String codigoTimeout;
                if (wtpodta.intValue() != 1) {
                    codigoTimeout = ImposibleLeerRespuestaException.CODIGO_TIMEOUT;
                } else {
                    codigoTimeout = ImposibleLeerRespuestaException.CODIGO_TIMEOUT_TRANS;
                }

                throw new ImposibleLeerRespuestaException(
                        "Tiempo de espera agotado... intentando revertir eventualmente en otro hilo " + key,
                        imposibleLeerRespuestaException,
                        codigoTimeout
                );
            } finally {
                onAfterLeer();
            }

            LOGTRACE.debug("9) Lectura acertada del DQ en {} milisegundos", (System.currentTimeMillis() - t));
            t = System.currentTimeMillis();

            /*
             * ********************************************************************************************************
             * ATENCION
             * ********************************************************************************************************
             * a partir de este momento la transaccion bancaria ES EFECTIVA, y cualquier error generado por esta clase
             * de modo que cualquier excepcion se va a envolver en una TransaccionEfectivaException.
             * ********************************************************************************************************
             */
            if (LOG.isDebugEnabled()) {
                LOG.debug("La transaccion " + cabeceraEnvio.getField("WCODTRN") + " " +
                        cabeceraRecepcion.getField("WNUMUSR") + " " + cabeceraRecepcion.getField("WNUMSEQ") +
                        " ha tomado " + (t - antestransaccion) +
                        " milisegundos y ha devuelto los siguientes " +
                        "datos clave: WPOST=" + cabeceraRecepcion.getField("WPOST") +
                        " WREJ1=" + cabeceraRecepcion.getField("WREJ1") +
                        " WREJ2=" + cabeceraRecepcion.getField("WREJ2") +
                        " WREJ3=" + cabeceraRecepcion.getField("WREJ3") +
                        " WREJ4=" + cabeceraRecepcion.getField("WREJ4") +
                        " WREJ5=" + cabeceraRecepcion.getField("WREJ5"));
            }

            procesarResultadoDelDataQueue();

        } catch (UnsupportedEncodingException ex) {
            throw new IllegalStateException("Java o AS/400 no entienen la codificacion, ESTO REALMENTE NO DEBERIA " +
                    "OCURRIR y seguramente es porque se corre una instalacion de Java no soportada, porque la AS/400" +
                    " tiene alguna condicion bogus o debido a un error GRAVE de programacion.", ex);
        }
        try {
            doDespuesTransaccion();
        } catch (Throwable e) {
            LOG.error("Error al ejecutar doDespuesTransaccion()", e);
        }

        LOGTRACE.debug("A) Terminando la ejecucion luego de {} milisegundos", (System.currentTimeMillis() - t));

        return this;
    }

    private void verificarEstado() throws IOException, SistemaCerradoException {
        if (cabeceraEnvio == null) {
            cabeceraEnvio = as400factory.buscarFormato("PSR9001").getNewRecord();
        }

        if (cabeceraRecepcionFormat == null) {
            cabeceraRecepcionFormat = as400factory.buscarFormato("PSR9101");
        }
    }

    public boolean isTransaccionEjecutadaSatisfactoriamente() throws UnsupportedEncodingException {
        return cabeceraRecepcion != null && "P".equals(cabeceraRecepcion.getField("WPOST"));
    }

    protected void onAfterLeer() {
    }

    protected void onBeforeLeer() {
    }

    protected void onAfterEscribir() {
    }

    protected void onBeforeEscribir() {
    }

    protected void onBeforeIntentarRevertirAutomaticamente() {
    }

    protected void onReversionAutomaticaOK(TransaccionAudit ultimaTransaccion) {
    }

    public final void procesarResultadoDelDataQueue() throws UnsupportedEncodingException, TransaccionEfectivaException {
        Validate.notNull(cabeceraRecepcion);
        Validate.notNull(dataSalida);
        try {
            int posicionHastaProcesado = ((Number) cabeceraRecepcion.getField("WLRGREG")).intValue();
            int[] formato = new int[3];
            formato[0] = ((Number) cabeceraRecepcion.getField("WFMT1")).intValue();
            formato[1] = ((Number) cabeceraRecepcion.getField("WFMT2")).intValue();
            formato[2] = ((Number) cabeceraRecepcion.getField("WFMT3")).intValue();
            int[] longitudes = new int[3];
            longitudes[0] = ((Number) cabeceraRecepcion.getField("WLRG1")).intValue();
            longitudes[1] = ((Number) cabeceraRecepcion.getField("WLRG2")).intValue();
            longitudes[2] = ((Number) cabeceraRecepcion.getField("WLRG3")).intValue();
            int[] cantidades = new int[3];
            cantidades[0] = ((Number) cabeceraRecepcion.getField("WOCU1")).intValue();
            cantidades[1] = ((Number) cabeceraRecepcion.getField("WOCU2")).intValue();
            cantidades[2] = ((Number) cabeceraRecepcion.getField("WOCU3")).intValue();

            if (LOG.isDebugEnabled()) {
                LOG.debug("Formatos 0=" + formato[0] + " 1=" + formato[1] + " 2=" + formato[2]);
                LOG.debug("Longitudes 0=" + longitudes[0] + " 1=" + longitudes[1] + " 2=" + longitudes[2]);
                LOG.debug("Cantidades 0=" + cantidades[0] + " 1=" + cantidades[1] + " 2=" + cantidades[2]);
            }

            for (int i = 0; i < cantidades[0]; i++) {
                byte[] p = ArrayUtils.subarray(dataSalida, posicionHastaProcesado, posicionHastaProcesado +
                        longitudes[0]);
                posicionHastaProcesado += longitudes[0];
                if (cuerpo1RecepcionTipo != null && Integer.parseInt(cuerpo1RecepcionTipo) == formato[0]) {
                    if (cuerpo1Records == null) {
                        cuerpo1Records = new ArrayList<Record>();
                    }
                    cuerpo1Records.add(cuerpo1RecepcionFormat.getNewRecord(p));
                } else if (cuerpo2RecepcionTipo != null && Integer.parseInt(cuerpo2RecepcionTipo) == formato[0]) {
                    if (cuerpo2Records == null) {
                        cuerpo2Records = new ArrayList<Record>();
                    }
                    cuerpo2Records.add(cuerpo2RecepcionFormat.getNewRecord(p));
                } else if (cuerpo3RecepcionTipo != null && Integer.parseInt(cuerpo3RecepcionTipo) == formato[0]) {
                    if (cuerpo3Records == null) {
                        cuerpo3Records = new ArrayList<Record>();
                    }
                    cuerpo3Records.add(cuerpo3RecepcionFormat.getNewRecord(p));
                } else {
                    if (formato[0] != 0) {
                        LOG.error("Hay una falla de configuracion porque no se ha reconocido el formato recibido " +
                                formato[0] + " que no es ni " + cuerpo1RecepcionTipo +
                                " ni " + cuerpo2RecepcionTipo + " ni " + cuerpo3RecepcionTipo, new Throwable());
                    }
                }
            }
            for (int i = 0; i < cantidades[1]; i++) {
                byte[] p = ArrayUtils.subarray(dataSalida, posicionHastaProcesado, posicionHastaProcesado +
                        longitudes[1]);
                posicionHastaProcesado += longitudes[1];
                if (cuerpo1RecepcionTipo != null && Integer.parseInt(cuerpo1RecepcionTipo) == formato[1]) {
                    if (cuerpo1Records == null) {
                        cuerpo1Records = new ArrayList<Record>();
                    }
                    cuerpo1Records.add(cuerpo1RecepcionFormat.getNewRecord(p));
                } else if (cuerpo2RecepcionTipo != null && Integer.parseInt(cuerpo2RecepcionTipo) == formato[1]) {
                    if (cuerpo2Records == null) {
                        cuerpo2Records = new ArrayList<Record>();
                    }
                    cuerpo2Records.add(cuerpo2RecepcionFormat.getNewRecord(p));
                } else if (cuerpo3RecepcionTipo != null && Integer.parseInt(cuerpo3RecepcionTipo) == formato[1]) {
                    if (cuerpo3Records == null) {
                        cuerpo3Records = new ArrayList<Record>();
                    }
                    cuerpo3Records.add(cuerpo3RecepcionFormat.getNewRecord(p));
                } else {
                    LOG.error("Hay una falla de configuracion porque no se ha reconocido el formato recibido " +
                            formato[1] + " que no es ni " + cuerpo1RecepcionTipo +
                            " ni " + cuerpo2RecepcionTipo + " ni " + cuerpo3RecepcionTipo, new Throwable());
                }
            }
            for (int i = 0; i < cantidades[2]; i++) {
                byte[] p = ArrayUtils.subarray(dataSalida, posicionHastaProcesado, posicionHastaProcesado +
                        longitudes[2]);
                posicionHastaProcesado += longitudes[2];
                if (cuerpo1RecepcionTipo != null && Integer.parseInt(cuerpo1RecepcionTipo) == formato[2]) {
                    if (cuerpo1Records == null) {
                        cuerpo1Records = new ArrayList<Record>();
                    }
                    cuerpo1Records.add(cuerpo1RecepcionFormat.getNewRecord(p));
                } else if (cuerpo2RecepcionTipo != null && Integer.parseInt(cuerpo2RecepcionTipo) == formato[2]) {
                    if (cuerpo2Records == null) {
                        cuerpo2Records = new ArrayList<Record>();
                    }
                    cuerpo2Records.add(cuerpo2RecepcionFormat.getNewRecord(p));
                } else if (cuerpo3RecepcionTipo != null && Integer.parseInt(cuerpo3RecepcionTipo) == formato[2]) {
                    if (cuerpo3Records == null) {
                        cuerpo3Records = new ArrayList<Record>();
                    }
                    cuerpo3Records.add(cuerpo3RecepcionFormat.getNewRecord(p));
                } else {
                    LOG.error("Hay una falla de configuracion porque no se ha reconocido el formato recibido " +
                            formato[2] + " que no es ni " + cuerpo1RecepcionTipo +
                            " ni " + cuerpo2RecepcionTipo + " ni " + cuerpo3RecepcionTipo, new Throwable());
                }
            }
            if (LOG.isDebugEnabled()) {
                if (cabeceraEnvio == null) {
                    LOG.debug("Envio: no existe informacion del envio");
                } else {
                    LOG.debug("Envio, Cabecera:");
                    for (FieldDescription fd : cabeceraEnvio.getRecordFormat().getFieldDescriptions()) {
                        LOG.debug(" " + fd.getFieldName() + " : <" + cabeceraEnvio.getField(fd.getFieldName()) + ">");
                    }
                }
                if (cuerpo1Envio != null) {
                    LOG.debug("Envio, Cuerpo 1 ({}):", cuerpo1EnvioTipo);
                    for (FieldDescription fd : cuerpo1Envio.getRecordFormat().getFieldDescriptions()) {
                        LOG.debug(" " + fd.getFieldName() + " : <" + cuerpo1Envio.getField(fd.getFieldName()) + ">");
                    }
                }
                if (cuerpo2Envio != null) {
                    LOG.debug("Envio, Cuerpo 2 ({}):", cuerpo2EnvioTipo);
                    for (FieldDescription fd : cuerpo2Envio.getRecordFormat().getFieldDescriptions()) {
                        LOG.debug(" " + fd.getFieldName() + " : <" + cuerpo2Envio.getField(fd.getFieldName()) + ">");
                    }
                }
                if (cuerpo3Envio != null) {
                    LOG.debug("Envio, Cuerpo 3 ({}):", cuerpo3EnvioTipo);
                    for (FieldDescription fd : cuerpo3Envio.getRecordFormat().getFieldDescriptions()) {
                        LOG.debug(" {} : <{}>.", fd.getFieldName(), cuerpo3Envio.getField(fd.getFieldName()));
                    }
                }
                LOG.debug("Recepcion, Cabecera:");
                for (FieldDescription fd : cabeceraRecepcionFormat.getFieldDescriptions()) {
                    LOG.debug(" " + fd.getFieldName() + " : <" + cabeceraRecepcion.getField(fd.getFieldName()) + ">");
                }
                if (cuerpo1Records != null) {
                    LOG.debug("Existen {} registros de tipo {}.", cuerpo1Records.size(), cuerpo1RecepcionTipo);
                    int j = 1;
                    for (Record record : cuerpo1Records) {
                        LOG.debug("Recepcion, Cuerpo 1. {} de {}.", j, cuerpo1Records.size());
                        j++;
                        for (FieldDescription fd : cuerpo1RecepcionFormat.getFieldDescriptions()) {
                            LOG.debug(" {} : <{}>.", fd.getFieldName(), record.getField(fd.getFieldName()));
                        }
                    }
                }
                if (cuerpo2Records != null) {
                    LOG.debug("Existen {} registros de tipo {}.", cuerpo2Records.size(), cuerpo2RecepcionTipo);
                    int j = 1;
                    for (Record record : cuerpo2Records) {
                        LOG.debug("Recepcion, Cuerpo 2. {} de {}", j, cuerpo2Records.size());
                        j++;
                        for (FieldDescription fd : cuerpo2RecepcionFormat.getFieldDescriptions()) {
                            LOG.debug(" {} : <{}>.", fd.getFieldName(), record.getField(fd.getFieldName()));
                        }
                    }
                }
                if (cuerpo3Records != null) {
                    LOG.debug("Existen {} registros de tipo {}", cuerpo3Records.size(), cuerpo3RecepcionTipo);
                    int j = 1;
                    for (Record record : cuerpo3Records) {
                        LOG.debug("Recepcion, Cuerpo 3. {} de {}", j, cuerpo3Records.size());
                        j++;
                        for (FieldDescription fd : cuerpo3RecepcionFormat.getFieldDescriptions()) {
                            LOG.debug(" " + fd.getFieldName() + " : <" + record.getField(fd.getFieldName()) + ">");
                        }
                    }
                }
            } else if (!isTransaccionEjecutadaSatisfactoriamente() && "107".equals(cabeceraRecepcion.getField("WREJ1"))) {
                LOG.error("Ha ocurrido un error 107: a continuacion se muestra la interaccion a nivel INFO...");
                LOG.info(" Envio, Cabecera:");
                for (FieldDescription fd : cabeceraEnvio.getRecordFormat().getFieldDescriptions()) {
                    LOG.info(" " + fd.getFieldName() + " : <" + cabeceraEnvio.getField(fd.getFieldName()) + ">");
                }
                if (cuerpo1Envio != null) {
                    LOG.info("Envio, Cuerpo 1:");
                    for (FieldDescription fd : cuerpo1Envio.getRecordFormat().getFieldDescriptions()) {
                        LOG.info(" " + fd.getFieldName() + " : <" + cuerpo1Envio.getField(fd.getFieldName()) + ">");
                    }
                }
                if (cuerpo2Envio != null) {
                    LOG.info("Envio, Cuerpo 2:");
                    for (FieldDescription fd : cuerpo2Envio.getRecordFormat().getFieldDescriptions()) {
                        LOG.info(" " + fd.getFieldName() + " : <" + cuerpo2Envio.getField(fd.getFieldName()) + ">");
                    }
                }
                if (cuerpo3Envio != null) {
                    LOG.info("Envio, Cuerpo 3:");
                    for (FieldDescription fd : cuerpo3Envio.getRecordFormat().getFieldDescriptions()) {
                        LOG.info(" " + fd.getFieldName() + " : <" + cuerpo3Envio.getField(fd.getFieldName()) + ">");
                    }
                }
                LOG.info("Recepcion, Cabecera:");
                for (FieldDescription fd : cabeceraRecepcionFormat.getFieldDescriptions()) {
                    LOG.info(" " + fd.getFieldName() + " : <" + cabeceraRecepcion.getField(fd.getFieldName()) + ">");
                }
                if (cuerpo1Records != null) {
                    LOG.info("Existen " + cuerpo1Records.size() + " registros de tipo " + cuerpo1RecepcionTipo);
                    int j = 1;
                    for (Record record : cuerpo1Records) {
                        LOG.info("Recepcion, Cuerpo 1 ({}). {} de {}", new Object[]{cuerpo1RecepcionTipo, j, cuerpo1Records.size()});
                        j++;
                        for (FieldDescription fd : cuerpo1RecepcionFormat.getFieldDescriptions()) {
                            LOG.info(" " + fd.getFieldName() + " : <" + record.getField(fd.getFieldName()) + ">");
                        }
                    }
                }
                if (cuerpo2Records != null) {
                    LOG.info("Existen " + cuerpo2Records.size() + " registros de tipo " + cuerpo2RecepcionTipo);
                    int j = 1;
                    for (Record record : cuerpo2Records) {
                        LOG.info("Recepcion, Cuerpo 2. " + j + " de " + cuerpo2Records.size());
                        j++;
                        for (FieldDescription fd : cuerpo2RecepcionFormat.getFieldDescriptions()) {
                            LOG.info(" " + fd.getFieldName() + " : <" + record.getField(fd.getFieldName()) + ">");
                        }
                    }
                }
                if (cuerpo3Records != null) {
                    LOG.info("Existen " + cuerpo3Records.size() + " registros de tipo " + cuerpo3RecepcionTipo);
                    int j = 1;
                    for (Record record : cuerpo3Records) {
                        LOG.info("Recepcion, Cuerpo 3. " + j + " de " + cuerpo3Records.size());
                        j++;
                        for (FieldDescription fd : cuerpo3RecepcionFormat.getFieldDescriptions()) {
                            LOG.info(" " + fd.getFieldName() + " : <" + record.getField(fd.getFieldName()) + ">");
                        }
                    }
                }
            }

        } catch (RuntimeException e) {
            onError(TransaccionAuditOtrosErrores.TREF);
            throw new TransaccionEfectivaException(e);
        }
    }

    private long leerDelDataQueue(long t, String deviceReal, BigDecimal key, String colaSalidaCompleta, int timeout) throws IOException, ImposibleLeerRespuestaException {
        while (true) {
            AS400 as400 = null;
            try {
                as400 = as400factory.getAS400(AS400.DATAQUEUE);
                KeyedDataQueue dqo = new KeyedDataQueue(as400, colaSalidaCompleta);

                LOGTRACE.debug("7) Obteniendo el as400 {} milisegundos", (System.currentTimeMillis() - t));
                t = System.currentTimeMillis();

                LOG.debug("Intentando lectura de la cola {} con la llave <{}>", colaSalidaCompleta, deviceReal);
                DataQueueEntry dqData = null;

                final Integer maxIntentos = medioAmbiente.getValorIntDe(MONITOR_INTENTOS_LECTURA, MONITOR_INTENTOS_LECTURA_DEFAULT);
                int intentosLectura = 0;
                while (true) {
                    try {
                        intentosLectura++;

                        watchEspera400.start();
                        try {
                            dqData = dqo.read(deviceReal, timeout, "EQ");
                        } finally {
                            watchEspera400.stop();
                        }

                        LOGTRACE.debug("8) Lectura correcta del DQ en {} milisegundos", (System.currentTimeMillis() - t));
                        t = System.currentTimeMillis();

                        break;
                    } catch (ObjectDoesNotExistException ex) {
                        throw new IllegalStateException(colaSalidaCompleta + " NO existe", ex);
                    } catch (IllegalObjectTypeException ex) {
                        throw new IllegalStateException(colaSalidaCompleta + " NO es un keyed dataqueue", ex);
                    } catch (ErrorCompletingRequestException ex) {
                        // Error de protocolo: la red esta loca...
                        if (intentosLectura >= maxIntentos) {
                            onError(TransaccionAuditOtrosErrores.MCHI);
                            throw new ImposibleLeerRespuestaException(
                                    "Se han experimentado continuos errores de protocolo con AS/400", ex,
                                    ImposibleLeerRespuestaException.CODIGO_ERROR_INTERNO
                            );
                        }
                        LOG.warn("Error de protocolo de comunicaciones con la AS/400, re-intentando", ex);
                    } catch (InterruptedException ex) {
                        LOG.error("Error de sincroinzacion AS/400, a la lectura del dataqueue", ex);
                        break;
                    } catch (AS400SecurityException ex) {
                        // SI no ha dado en la lectura, es medio dificil que de en la respuesta, demasiada mala suerte,
                        // simplemente logueamos y seguimos adelante.
                        LOG.error("Error de seguridad AS/400", ex);
                        break;
                    }
                }

                if (dqData == null) {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Envio, Cabecera:");
                        for (FieldDescription fd : cabeceraEnvio.getRecordFormat().getFieldDescriptions()) {
                            LOG.debug(" c : {} : <{}>", fd.getFieldName(), cabeceraEnvio.getField(fd.getFieldName()));
                        }
                        if (cuerpo1Envio != null) {
                            LOG.debug("Envio, Cuerpo 1:");
                            for (FieldDescription f : cuerpo1Envio.getRecordFormat().getFieldDescriptions()) {
                                LOG.debug(" e1: {} : <{}>", f.getFieldName(), cuerpo1Envio.getField(f.getFieldName()));
                            }
                        }
                        if (cuerpo2Envio != null) {
                            LOG.debug("Envio, Cuerpo 2:");
                            for (FieldDescription f : cuerpo2Envio.getRecordFormat().getFieldDescriptions()) {
                                LOG.debug(" e2: {} : <{}>", f.getFieldName(), cuerpo2Envio.getField(f.getFieldName()));
                            }
                        }
                        if (cuerpo3Envio != null) {
                            LOG.debug("Envio, Cuerpo 3:");
                            for (FieldDescription f : cuerpo3Envio.getRecordFormat().getFieldDescriptions()) {
                                LOG.debug(" e3: {} : <{}>", f.getFieldName(), cuerpo3Envio.getField(f.getFieldName()));
                            }
                        }
                    }
                    onError(TransaccionAuditOtrosErrores.TIME);

                    String codigoTimeout;
                    BigDecimal wtpodta = (BigDecimal) cabeceraEnvio.getField("WTPODTA");
                    if (wtpodta.intValue() != 1) {
                        codigoTimeout = ImposibleLeerRespuestaException.CODIGO_TIMEOUT;
                    } else {
                        codigoTimeout = ImposibleLeerRespuestaException.CODIGO_TIMEOUT_TRANS;
                    }

                    throw new ImposibleLeerRespuestaException(
                            "Tiempo de espera DataQueue agotado",
                            codigoTimeout
                    );
                }

                dataSalida = dqData.getData();

                LOG.debug("Outputdata ... " + new String(dataSalida));
                LOG.info("Outputdata IBM284... " + new String(dataSalida, "IBM284"));

                try {
                    dataSalida = mutarDataRecibida(Arrays.copyOf(dataSalida, dataSalida.length));
                } catch (Throwable e) {
                    LOG.error("PELIGRO: El cambio definido por la mutacion a la salida del monitor no se hizo!!", e);
                }
                LOG.debug("Recibiendo la siguiente cadena (IBM284): {}. Las mutaciones estan incluidas",
                        new String(dataSalida, "IBM284"));

                cabeceraRecepcion = cabeceraRecepcionFormat.getNewRecord(dataSalida);
                if (key.equals(cabeceraRecepcion.getField("WNUMCTL"))) {
                    break;
                }
                LOG.error("La llave ({}) no corresponde a la llave que vino en la respuesta ({})." +
                        " Se intentara otra lectura de la misma cola.", key, cabeceraRecepcion.getField("WNUMCTL"));
            } catch (ConnectionPoolException e) {
                throw new RuntimeException(e);
            } finally {
                as400factory.ret(as400, AS400.DATAQUEUE);
            }
        }
        return t;
    }

    protected void onError(TransaccionAuditOtrosErrores ioe) {
    }

    public final void setDatoCabeceraEnvio(String string, int param) throws IOException, SistemaCerradoException {
        verificarEstado();
        Validate.notNull(cabeceraEnvio, "No nos gustan las cabeceras de envio no seleccionadas");
        try {
            cabeceraEnvio.setField(string, new BigDecimal(param));
        } catch (ExtendedIllegalArgumentException e) {
            StringBuilder stringBuilder = construirDatosFormato(cabeceraEnvio.getRecordFormat());
            LOG.error("Error al colocar un dato en la cabecera: {}. A continuacion se muestra la estructura " +
                    "del registro, que posiblemente se haya roto: {}.", e.getMessage(), stringBuilder.toString());
        }
    }

    public final TransaccionMonitor setDatoCabeceraEnvio(String string, short param) throws IOException, SistemaCerradoException {
        verificarEstado();
        Validate.notNull(cabeceraEnvio, "No nos gustan las cabeceras de envio no seleccionadas");
        try {
            cabeceraEnvio.setField(string, new BigDecimal(param));
        } catch (ExtendedIllegalArgumentException e) {
            StringBuilder stringBuilder = construirDatosFormato(cabeceraEnvio.getRecordFormat());
            LOG.error("Error al colocar un dato en la cabecera: {}. A continuacion se muestra la estructura " +
                    "del registro, que posiblemente se haya roto: {}.", e.getMessage(), stringBuilder.toString());
        }
        return this;
    }

    public final void setDatoCabeceraEnvio(String llave, BigDecimal param) throws IOException, SistemaCerradoException {
        verificarEstado();
        Validate.notNull(cabeceraEnvio, "No nos gustan las cabeceras de envio no seleccionadas");
        try {
            cabeceraEnvio.setField(llave, param);
        } catch (ExtendedIllegalArgumentException e) {
            StringBuilder stringBuilder = construirDatosFormato(cabeceraEnvio.getRecordFormat());
            LOG.error("Error al colocar un dato en la cabecera: {}. A continuacion se muestra la estructura " +
                    "del registro, que posiblemente se haya roto: {}.", e.getMessage(), stringBuilder.toString());
        }

    }

    public final void setDatoCabeceraEnvio(String string, String param) throws IOException, SistemaCerradoException {
        verificarEstado();
        Validate.notNull(cabeceraEnvio, "No nos gustan las cabeceras de envio no seleccionadas");
        try {
            cabeceraEnvio.setField(string, param);
        } catch (ExtendedIllegalArgumentException e) {
            StringBuilder stringBuilder = new StringBuilder();
            String[] fieldNames = cabeceraEnvio.getRecordFormat().getFieldNames();
            for (String fieldname : fieldNames) {
                FieldDescription fieldDescription = cabeceraEnvio.getRecordFormat().getFieldDescription(fieldname);
                stringBuilder.append("Nombre: '");
                stringBuilder.append(fieldname);
                stringBuilder.append("' Tipo: ");
                stringBuilder.append(fieldDescription.getDataType().getInstanceType());
                stringBuilder.append(" longtud: ");
                stringBuilder.append(fieldDescription.getLength());
                stringBuilder.append("\n");
            }
            LOG.error("Error al colocar un dato en la cabecera: {}. A continuacion se muestra la estructura " +
                    "del registro, que posiblemente se haya roto: {}.", e.getMessage(), stringBuilder.toString());
        }
    }

//    @Override
//    public final void setDatoCabeceraEnvio(String llave, BigDecimal valorDe) throws IOException, SistemaCerradoException {
//        Validate.notNull(cabeceraEnvio, "No nos gustan las cabeceras de envio no seleccionadas");
//        try {
//            cabeceraEnvio.setField(llave, valorDe);
//        } catch (ExtendedIllegalArgumentException e) {
//            StringBuilder stringBuilder = construirDatosFormato(cabeceraEnvio.getRecordFormat());
//            LOG.error("Error al colocar un dato en la cabecera: {}. A continuacion se muestra la estructura " +
//                    "del registro, que posiblemente se haya roto: {}.", e.getMessage(), stringBuilder.toString());
//        }
//    }


    public final void setDatoCuerpo1(String string, int param) {
        Validate.notNull(cuerpo1Envio, "No nos gusta el cuerpo1 no seleccionado");
        cuerpo1Envio.setField(string, new BigDecimal(param));
    }

    public final TransaccionMonitor setDatoCuerpo1(String string, short param) {
        Validate.notNull(cuerpo1Envio, "No nos gusta el cuerpo1 no seleccionado");
        cuerpo1Envio.setField(string, new BigDecimal(param));
        return this;
    }

    public final void setDatoCuerpo1(String string, BigDecimal param) {
        Validate.notNull(cuerpo1Envio, "No nos gusta el cuerpo1 no seleccionado");
        cuerpo1Envio.setField(string, param);
    }

    public final void setDatoCuerpo1(String string, String param) {
        Validate.notNull(cuerpo1Envio, "No nos gusta el cuerpo1 no seleccionado");
        cuerpo1Envio.setField(string, param);
    }

    public final void setDatoCuerpo2(String string, int param) {
        Validate.notNull(cuerpo2Envio, "No nos gusta el cuerpo2 no seleccionado");
        cuerpo2Envio.setField(string, new BigDecimal(param));
    }

    public final TransaccionMonitor setDatoCuerpo2(String string, short param) {
        Validate.notNull(cuerpo2Envio, "No nos gusta el cuerpo2 no seleccionado");
        cuerpo2Envio.setField(string, new BigDecimal(param));
        return this;
    }

    public final void setDatoCuerpo2(String string, BigDecimal param) {
        Validate.notNull(cuerpo2Envio, "No nos gusta el cuerpo2 no seleccionado");
        cuerpo2Envio.setField(string, param);
    }

    public final void setDatoCuerpo2(String string, String param) {
        Validate.notNull(cuerpo2Envio, "No nos gusta el cuerpo2 no seleccionado");
        cuerpo2Envio.setField(string, param);
    }

    public final TransaccionMonitor setDatoCuerpo3(String string, int param) {
        Validate.notNull(cuerpo3Envio, "No nos gusta el cuerpo3 no seleccionado");
        cuerpo3Envio.setField(string, new BigDecimal(param));
        return this;
    }

    public final TransaccionMonitor setDatoCuerpo3(String string, short param) {
        Validate.notNull(cuerpo3Envio, "No nos gusta el cuerpo3 no seleccionado");
        cuerpo3Envio.setField(string, new BigDecimal(param));
        return this;
    }

    public final TransaccionMonitor setDatoCuerpo3(String string, BigDecimal param) {
        Validate.notNull(cuerpo3Envio, "No nos gusta el cuerpo3 no seleccionado");
        cuerpo3Envio.setField(string, param);
        return this;
    }

    public final TransaccionMonitor setDatoCuerpo3(String string, String param) {
        Validate.notNull(cuerpo3Envio, "No nos gusta el cuerpo3 no seleccionado");
        cuerpo3Envio.setField(string, param);
        return this;
    }

    public final Record getCabeceraEnvio() {
        return cabeceraEnvio;
    }

    public final Record getCuerpo1Envio() {
        return cuerpo1Envio;
    }

    public final void setCuerpo1EnvioFormat(String archivo) throws IOException, SistemaCerradoException {
        LOG.debug("Definiendo formato de cuerpo de envio {} para la transaccion en curso", archivo);
        cuerpo1Envio = as400factory.buscarFormato(archivo).getNewRecord();
        cuerpo1EnvioTipo = StringUtils.substring(archivo, -3);
    }

    public final Record getCuerpo2Envio() {
        return cuerpo2Envio;
    }

    public final void setCuerpo2EnvioFormat(String archivo) throws IOException, SistemaCerradoException {
        LOG.debug("Definiendo formato de cuerpo de envio {} para la transaccion en curso", archivo);
        cuerpo2Envio = as400factory.buscarFormato(archivo).getNewRecord();
        cuerpo2EnvioTipo = StringUtils.substring(archivo, -3);
    }

    public final Record getCuerpo3Envio() {
        return cuerpo3Envio;
    }

    public final TransaccionMonitor setCuerpo3EnvioFormat(String archivo) throws IOException, SistemaCerradoException {
        LOG.debug("Definiendo formato de cuerpo de envio {} para la transaccion en curso", archivo);
        cuerpo3Envio = as400factory.buscarFormato(archivo).getNewRecord();
        cuerpo3EnvioTipo = StringUtils.substring(archivo, -3);
        return this;
    }

    public final RecordFormat getCabeceraRecepcionFormat() {
        return cabeceraRecepcionFormat;
    }

    public final RecordFormat getCuerpo1RecepcionFormat() {
        return cuerpo1RecepcionFormat;
    }

    public final void setCuerpo1RecepcionFormat(String archivo) throws IOException, SistemaCerradoException {
        LOG.debug("Definiendo formato de cuerpo de recepcion {} para la transaccion en curso", archivo);
        this.cuerpo1RecepcionFormat = as400factory.buscarFormato(archivo);
        this.cuerpo1RecepcionTipo = StringUtils.substring(archivo, -3);
    }

    public final RecordFormat getCuerpo2RecepcionFormat() {
        return cuerpo2RecepcionFormat;
    }

    public final void setCuerpo2RecepcionFormat(String archivo) throws IOException, SistemaCerradoException {
        LOG.debug("Definiendo formato de cuerpo de recepcion {} para la transaccion en curso", archivo);
        this.cuerpo2RecepcionFormat = as400factory.buscarFormato(archivo);
        this.cuerpo2RecepcionTipo = StringUtils.substring(archivo, -3);
    }

    public final RecordFormat getCuerpo3RecepcionFormat() {
        return cuerpo3RecepcionFormat;
    }

    public final TransaccionMonitor setCuerpo3RecepcionFormat(String archivo) throws IOException, SistemaCerradoException {
        LOG.debug("Definiendo formato de cuerpo de recepcion {} para la transaccion en curso", archivo);
        this.cuerpo3RecepcionFormat = as400factory.buscarFormato(archivo);
        this.cuerpo3RecepcionTipo = StringUtils.substring(archivo, -3);
        return this;
    }

    public final Record getRecordSalidaCabecera() {
        return cabeceraRecepcion;
    }

    public final String getDatoRecibidoCabeceraString(String param) throws TransaccionIncompletaException {
        return getDatoString(cabeceraRecepcion, param);
    }

    public final BigDecimal getDatoRecibidoCabeceraDecimal(String param) throws TransaccionIncompletaException {
        return getDatoBigDecimal(cabeceraRecepcion, param);
    }

    public final int getDatoRecibidoCabeceraInt(String param) throws TransaccionIncompletaException {
        return getDatoInt(cabeceraRecepcion, param);
    }

    public final boolean next1() {
        if (cuerpo1Records == null) {
            return false;
        }
        if (cuerpo1Iterator == null) {
            cuerpo1Iterator = cuerpo1Records.iterator();
        }
        if (cuerpo1Iterator.hasNext()) {
            cuerpo1Actual = cuerpo1Iterator.next();
            return true;
        }
        return false;
    }

    public final String getDatoRecibidoCuerpo1String(String param) throws TransaccionIncompletaException {
        return getDatoString(cuerpo1Actual, param);
    }

    public final BigDecimal getDatoRecibidoCuerpo1Decimal(String param) throws TransaccionIncompletaException {
        return getDatoBigDecimal(cuerpo1Actual, param);
    }

    public final int getDatoRecibidoCuerpo1Int(String param) throws TransaccionIncompletaException {
        return getDatoInt(cuerpo1Actual, param);
    }

    public final boolean next2() {
        if (cuerpo2Records == null) {
            return false;
        }
        if (cuerpo2Iterator == null) {
            cuerpo2Iterator = cuerpo2Records.iterator();
        }
        if (cuerpo2Iterator.hasNext()) {
            cuerpo2Actual = cuerpo2Iterator.next();
            return true;
        }
        return false;
    }

    public final String getDatoRecibidoCuerpo2String(String param) throws TransaccionIncompletaException {
        return getDatoString(cuerpo2Actual, param);
    }

    public final BigDecimal getDatoRecibidoCuerpo2Decimal(String param) throws TransaccionIncompletaException {
        return getDatoBigDecimal(cuerpo2Actual, param);
    }

    public final int getDatoRecibidoCuerpo2Int(String param) throws TransaccionIncompletaException {
        return getDatoInt(cuerpo2Actual, param);
    }

    public final boolean next3() {
        if (cuerpo3Records == null) {
            return false;
        }
        if (cuerpo3Iterator == null) {
            cuerpo3Iterator = cuerpo3Records.iterator();
        }
        if (cuerpo3Iterator.hasNext()) {
            cuerpo3Actual = cuerpo3Iterator.next();
            return true;
        }
        return false;
    }

    public final String getDatoRecibidoCuerpo3String(String param) throws TransaccionIncompletaException {
        return getDatoString(cuerpo3Actual, param);
    }

    public final BigDecimal getDatoRecibidoCuerpo3Decimal(String param) throws TransaccionIncompletaException {
        return getDatoBigDecimal(cuerpo3Actual, param);
    }

    public final int getDatoRecibidoCuerpo3Int(String param) throws TransaccionIncompletaException {
        return getDatoInt(cuerpo3Actual, param);
    }

    public final List<Record> getCuerpo1Records() {
        if (cuerpo1Records == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(cuerpo1Records);
    }

    public final List<Record> getCuerpo2Records() {
        if (cuerpo2Records == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(cuerpo2Records);
    }

    public final List<Record> getCuerpo3Records() {
        if (cuerpo3Records == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(cuerpo3Records);
    }

    /**
     * Esta llamada puede propagar un IOException, dado que es antes de la pregunta
     *
     * @throws java.io.IOException cuando se requiera detener el procesamiento
     */
    protected void doBeforeEjecutar() throws IOException {
    }

    /**
     * Esta llamada puede propagar un IOException, dado que es antes de la pregunta
     *
     * @throws java.io.IOException cuando se requiera detener el procesamiento
     */
    protected void doDespuesValidar() throws IOException {
    }

    /**
     * Esta llamada puede propagar un IOException, dado que es antes de la pregunta
     *
     * @throws java.io.IOException cuando se requiera detener el procesamiento
     */
    protected void doDespuesCalcularLongitudes() throws IOException {
    }

    /**
     * Esta llamada puede propagar un IOException, dado que es antes de la pregunta
     *
     * @throws java.io.IOException cuando se requiera detener el procesamiento
     */
    protected void doDespuesDatosPorDefecto() throws IOException {
    }

    /**
     * Esta llamada puede propagar un IOException, dado que es antes de la pregunta
     *
     * @param dataEntrada data sin mutar
     * @return la data que debe usar realmente
     */
    protected byte[] mutarDataEnvio(byte[] dataEntrada) {
        return dataEntrada;
    }

    /**
     * Cualquier excepcion que genere se va a loguear e ignorar
     *
     * @param dataSalida data a mutar
     * @return la data que debe usar realmente
     */
    protected byte[] mutarDataRecibida(byte[] dataSalida) {
        return dataSalida;
    }

    /**
     * Cualquier excepcion que genere se va a loguear e ignorar
     */
    protected void doDespuesTransaccion() {
    }

    public final byte[] getDataSalida() {
        return Arrays.copyOf(dataSalida, dataSalida.length);
    }

    public TransaccionAudit getUltimaTransaccion() {
        LOG.error("Alguien esta usando mal el API de monitor", new Throwable());
        return null;
    }

    public Record getCabeceraRecepcion() {
        return cabeceraRecepcion;
    }

    public final Record getCuerpo1Actual() {
        return cuerpo1Actual;
    }

    public final Record getCuerpo2Actual() {
        return cuerpo2Actual;
    }

    public final Record getCuerpo3Actual() {
        return cuerpo3Actual;
    }

    public final void setDataSalida(byte[] dataSalida) {
        this.dataSalida = dataSalida;
    }

    public final void setCuerpo1RecepcionFormat(RecordFormat cuerpo1RecepcionFormat) {
        this.cuerpo1RecepcionFormat = cuerpo1RecepcionFormat;
    }

    public final void setCabeceraRecepcionFormat(RecordFormat cabeceraRecepcionFormat) {
        this.cabeceraRecepcionFormat = cabeceraRecepcionFormat;
    }

    public final void setCabeceraRecepcion(Record cabeceraRecepcion) {
        this.cabeceraRecepcion = cabeceraRecepcion;
    }

    public final void setCuerpo2RecepcionFormat(RecordFormat cuerpo2RecepcionFormat) {
        this.cuerpo2RecepcionFormat = cuerpo2RecepcionFormat;
    }

    public final void setCuerpo3RecepcionFormat(RecordFormat cuerpo3RecepcionFormat) {
        this.cuerpo3RecepcionFormat = cuerpo3RecepcionFormat;
    }

    public final void setCuerpo1RecepcionTipo(String cuerpo1RecepcionTipo) {
        this.cuerpo1RecepcionTipo = cuerpo1RecepcionTipo;
    }

    public final void setCuerpo2RecepcionTipo(String cuerpo2RecepcionTipo) {
        this.cuerpo2RecepcionTipo = cuerpo2RecepcionTipo;
    }

    public final void setCuerpo3RecepcionTipo(String cuerpo3RecepcionTipo) {
        this.cuerpo3RecepcionTipo = cuerpo3RecepcionTipo;
    }

    public final boolean isEjecutado() {
        return dataSalida != null;
    }

    public final PSR9101Bean<?, ?, ?> revertir(PSR9101Bean<?, ?, ?> de, @Nullable Record cabecera)
            throws IOException, SistemaCerradoException, TransaccionEfectivaException, ImposibleLeerRespuestaException {

        verificarEstado();

        if (isEjecutado()) {
            throw new IllegalStateException();
        }

        if (cabecera == null) {

            @SuppressWarnings({"unchecked"})
            final Map<String, Object> entrada = (Map<String, Object>) de.getEntrada().get(CABECERA);
            for (String s : entrada.keySet()) {
                final Object o = entrada.get(s);
                cabeceraEnvio.setField(s, o);
            }

        } else {
            final int numberOfFields = cabecera.getNumberOfFields();

            for (int i = 0; i < numberOfFields; i++) {
                cabeceraEnvio.setField(i, cabecera.getField(i));
            }
        }


        setDatoCabeceraEnvio("WTPODTA", 6);
        setDatoCabeceraEnvio("WREVE", "S");

        setDatoCabeceraEnvio("WNUMUSR", de.getWNUMUSR());
        setDatoCabeceraEnvio("WNUMSEQ", de.getWNUMSEQ());

        ejecutar();

        if (isTransaccionEjecutadaSatisfactoriamente()) {
            LOG.info("Reversion de la transaccion {} {} ejecutada satisfactoriamente", de.getWNUMUSR(), de.getWNUMSEQ());
        } else {
            LOG.error("Reversion automatica para la transaccion {} {} no ha funcionado, debe hacerse a mano",
                    de.getWNUMUSR(), de.getWNUMSEQ());
        }

        return obtenerObjetoCompleto("", "", "");
    }

    @Override
    public <X extends Serializable, Y extends Serializable> PSR9101Bean<X, Y, String> obtenerObjetoCompleto(X x, Y y) {
        return obtenerObjetoCompleto(x, y, "");
    }

    @Override
    public <X extends Serializable> PSR9101Bean<X, String, String> obtenerObjetoCompleto(X x) {
        return obtenerObjetoCompleto(x, "", "");
    }

    public <X extends Serializable, Y extends Serializable, Z extends Serializable> PSR9101Bean<X, Y, Z> obtenerObjetoCompleto(X x, Y y, Z z) {
        try {
            PSR9101Bean<X, Y, Z> completo = new PSR9101Bean<X, Y, Z>();
            completo.setEntrada(new LinkedHashMap<String, Object>());

            final BeanInfo beanInfo;
            final PropertyDescriptor[] propertyDescriptors;
            try {
                beanInfo = Introspector.getBeanInfo(completo.getClass());
                propertyDescriptors = beanInfo.getPropertyDescriptors();
            } catch (IntrospectionException e) {
                throw new IllegalStateException(e);
            }

            if (cabeceraEnvio != null) {
                Map<String, Object> cabecera = new LinkedHashMap<String, Object>();
                completo.getEntrada().put(CABECERA, cabecera);
                for (FieldDescription fd : cabeceraEnvio.getRecordFormat().getFieldDescriptions()) {
                    cabecera.put(fd.getFieldName(), convertIfNeeded(cabeceraEnvio.getField(fd.getFieldName())));
                }
            }

            if (cuerpo1Envio != null) {
                Collection<LinkedHashMap<String, Object>> cuerpo1 = new LinkedList<LinkedHashMap<String, Object>>();
                completo.getEntrada().put(CUERPO_1, cuerpo1);
                LinkedHashMap<String, Object> cuerpoEspecifico = new LinkedHashMap<String, Object>();
                cuerpo1.add(cuerpoEspecifico);
                for (FieldDescription fd : cuerpo1Envio.getRecordFormat().getFieldDescriptions()) {
                    cuerpoEspecifico.put(fd.getFieldName(), convertIfNeeded(cuerpo1Envio.getField(fd.getFieldName())));
                }
            }

            if (cuerpo2Envio != null) {
                Collection<LinkedHashMap<String, Object>> cuerpo2 = new LinkedList<LinkedHashMap<String, Object>>();
                completo.getEntrada().put(CUERPO_2, cuerpo2);
                LinkedHashMap<String, Object> cuerpoEspecifico = new LinkedHashMap<String, Object>();
                cuerpo2.add(cuerpoEspecifico);
                for (FieldDescription fd : cuerpo2Envio.getRecordFormat().getFieldDescriptions()) {
                    cuerpoEspecifico.put(fd.getFieldName(), convertIfNeeded(cuerpo2Envio.getField(fd.getFieldName())));
                }
            }

            if (cuerpo3Envio != null) {
                Collection<LinkedHashMap<String, Object>> cuerpo3 = new LinkedList<LinkedHashMap<String, Object>>();
                completo.getEntrada().put(CUERPO_3, cuerpo3);
                LinkedHashMap<String, Object> cuerpoEspecifico = new LinkedHashMap<String, Object>();
                cuerpo3.add(cuerpoEspecifico);
                for (FieldDescription fd : cuerpo3Envio.getRecordFormat().getFieldDescriptions()) {
                    cuerpoEspecifico.put(fd.getFieldName(), convertIfNeeded(cuerpo3Envio.getField(fd.getFieldName())));
                }
            }

            if (cabeceraRecepcion != null) {
                for (FieldDescription fd : cabeceraRecepcionFormat.getFieldDescriptions()) {
                    for (PropertyDescriptor pd : propertyDescriptors) {
                        if (pd.getName().equals(fd.getFieldName())) {
                            pd.getWriteMethod().invoke(completo, convertIfNeeded(cabeceraRecepcion.getField(fd.getFieldName())));
                        }
                    }
                }
            }

            if (cuerpo1Records != null && x != null) {
                final Class<? extends Serializable> aClass = x.getClass();
                final BeanInfo bi = Introspector.getBeanInfo(aClass);
                final PropertyDescriptor[] pdx = bi.getPropertyDescriptors();
                for (Record record : cuerpo1Records) {
                    @SuppressWarnings({"unchecked"}) X newx = (X) aClass.newInstance();
                    completo.getCuerpo1().add(newx);
                    for (FieldDescription fd : cuerpo1RecepcionFormat.getFieldDescriptions()) {
                        for (PropertyDescriptor pd : pdx) {
                            if (pd.getName().equals(fd.getFieldName())) {
                                pd.getWriteMethod().invoke(newx, convertIfNeeded(record.getField(fd.getFieldName())));
                            }
                        }
                    }
                }
            }

            if (cuerpo2Records != null && y != null) {
                final Class<? extends Serializable> aClass = y.getClass();
                final BeanInfo bi = Introspector.getBeanInfo(aClass);
                final PropertyDescriptor[] pdy = bi.getPropertyDescriptors();
                for (Record record : cuerpo2Records) {
                    @SuppressWarnings({"unchecked"}) Y newy = (Y) aClass.newInstance();
                    completo.getCuerpo2().add(newy);
                    for (FieldDescription fd : cuerpo2RecepcionFormat.getFieldDescriptions()) {
                        for (PropertyDescriptor pd : pdy) {
                            if (pd.getName().equals(fd.getFieldName())) {
                                pd.getWriteMethod().invoke(newy, convertIfNeeded(record.getField(fd.getFieldName())));
                            }
                        }
                    }
                }
            }

            if (cuerpo3Records != null && z != null) {
                final Class<? extends Serializable> aClass = z.getClass();
                final BeanInfo bi = Introspector.getBeanInfo(aClass);
                final PropertyDescriptor[] pdz = bi.getPropertyDescriptors();
                for (Record record : cuerpo3Records) {
                    @SuppressWarnings({"unchecked"}) Z newz = (Z) aClass.newInstance();
                    completo.getCuerpo3().add(newz);
                    for (FieldDescription fd : cuerpo1RecepcionFormat.getFieldDescriptions()) {
                        for (PropertyDescriptor pd : pdz) {
                            if (pd.getName().equals(fd.getFieldName())) {
                                pd.getWriteMethod().invoke(newz, convertIfNeeded(record.getField(fd.getFieldName())));
                            }
                        }
                    }
                }
            }

            return completo;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e); // esto no ocurre
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e); // esto no ocurre
        } catch (IntrospectionException e) {
            throw new RuntimeException(e); // esto no ocurre
        } catch (InstantiationException e) {
            throw new RuntimeException(e); // esto no ocurre
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e); // esto no ocurre
        }
    }

    private Object convertIfNeeded(Object field) {
        if (ambienteActual != null && ambienteActual.convertirCCSID() && field != null && field instanceof String) {
            field = convert((String) field);
        }
        return field;
    }

    private String convert(String x) {
        try {
            CharConverter c1 = new CharConverter(37);
            CharConverter c2 = new CharConverter(284);
            return c2.byteArrayToString(c1.stringToByteArray(x));
        } catch (UnsupportedEncodingException e) {
            LOG.error("Error al convertir, no se convierte", e);
            return x;
        }
    }


    public AmbienteActual getAmbienteActual() {
        return ambienteActual;
    }

}
