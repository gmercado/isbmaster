/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.monitor.api;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Marcelo Morales
 * @since 6/28/11
 */
public class SistemaCerradoException extends Exception implements IExcepcionMonitorAqui {

    private static final long serialVersionUID = -2000327330392901982L;    
    private static final Logger LOGGER = LoggerFactory.getLogger(SistemaCerradoException.class);
    public static final String CODIGO = "MVSCD";

    // MENSAJES SISTEMA CERRADO
    public static final String MENSAJE_SISTEMA_CERRADO_EXTRAORDINARIO =
            "En este momento estamos realizando un mantenimiento extraordinario a nuestros sistemas.";
    public static final String MENSAJE_SISTEMA_CERRADO_1 = "En este momento estamos realizando el mantenimiento diario a nuestros sistemas. " +
            "Normalmente terminamos este mantenimiento a la 1:30 am.";
    public static final String MENSAJE_SISTEMA_CERRADO_2 = "En este momento estamos realizando el mantenimiento diario a nuestros sistemas. " +
            "Normalmente terminamos este mantenimiento a las 4:50 am.";
    public static final String MENSAJE_SISTEMA_CERRADO_SOL = "Disculpa los inconvenientes.";

    // Sistema cerrado al pasar de produccion a temporal
    public static final long HORA_INICIO_CERRADO_1 = Long.valueOf("2350");
    public static final long HORA_FINAL_CERRADO_1 = Long.valueOf("0200");

    // Sistema cerrado al pasar de temporal a produccion
    public static final long HORA_INICIO_CERRADO_2 = Long.valueOf("0400");
    public static final long HORA_FINAL_CERRADO_2 = Long.valueOf("0600");
    
    @Override
    public PageParameters getParameters(IErroresEbisa erroresEbisaDao) {
        PageParameters params = null;
        params = new PageParameters();
        params.add("titulo", "Sistema en Mantenimiento");
        params.add("desc", mensajeSistemaCerrado());
        params.add("sol", SistemaCerradoException.MENSAJE_SISTEMA_CERRADO_SOL);
        return params;
    }
    
    public String mensajeSistemaCerrado() {
        try {
            final SimpleDateFormat sdfHoraNumero = new SimpleDateFormat("HHmm");
            final long horaActual = Long.valueOf(sdfHoraNumero.format(new Date()));
            
            if(horaActual>= SistemaCerradoException.HORA_INICIO_CERRADO_1 || horaActual<= SistemaCerradoException.HORA_FINAL_CERRADO_1) {
                return SistemaCerradoException.MENSAJE_SISTEMA_CERRADO_1;
            }
                
            if (horaActual>= SistemaCerradoException.HORA_INICIO_CERRADO_2 && horaActual<= SistemaCerradoException.HORA_FINAL_CERRADO_2) {
                return SistemaCerradoException.MENSAJE_SISTEMA_CERRADO_2;
            }    
                
            return SistemaCerradoException.MENSAJE_SISTEMA_CERRADO_EXTRAORDINARIO;
        } catch(Throwable e) {
            LOGGER.error("Error formar mensaje de Sistema Cerrado",e);
            return SistemaCerradoException.MENSAJE_SISTEMA_CERRADO_EXTRAORDINARIO;
        }
        
    }
}
