/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.monitor.api;

/**
 * Excepcion que marca que la transaccion SI SE HA hecho y ha ocurrido un error despues de dicha ejecucion.
 *
 * @author Marcelo Morales
 */
public class TransaccionEfectivaException extends Exception {

    private static final long serialVersionUID = 1L;

    public TransaccionEfectivaException(RuntimeException e) {
        super(e);
    }
}
