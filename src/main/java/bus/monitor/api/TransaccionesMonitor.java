package bus.monitor.api;

import bus.database.model.AmbienteActual;
import bus.database.model.SQLFueraDeLinea;
import bus.env.api.MedioAmbiente;
import bus.monitor.as400.AS400Factory;
import bus.monitor.dao.TransaccionAuditDao;
import bus.monitor.model.PSR9101Bean;
import com.google.inject.Inject;
import com.ibm.as400.access.Record;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.concurrent.ExecutorService;

import static bus.env.api.Variables.MONITOR_CANAL;
import static bus.env.api.Variables.MONITOR_CANAL_DEFAULT;
import static bus.monitor.api.ParametersMonitor.MON_CANAL;
import static bus.monitor.api.ParametersMonitor.MON_VERSION;

/**
 * @author Marcelo Morales
 *         Since: 4/22/13
 */
public class TransaccionesMonitor {

    private final AS400Factory as400factory;
    private final AmbienteActual ambienteActual;
    private final ExecutorService executor;
    private final TransaccionAuditDao transaccionAuditDao;
    private final MedioAmbiente medioAmbiente;

    @Inject
    public TransaccionesMonitor(AS400Factory as400factory,
                                AmbienteActual ambienteActual,
                                @SQLFueraDeLinea ExecutorService executor,
                                TransaccionAuditDao transaccionAuditDao,
                                MedioAmbiente medioAmbiente) {
        this.as400factory = as400factory;
        this.ambienteActual = ambienteActual;
        this.executor = executor;
        this.transaccionAuditDao = transaccionAuditDao;
        this.medioAmbiente = medioAmbiente;
    }

    @SuppressWarnings({"unchecked"})
    public <Q> Q ejecutarTransaccionMonitor(int version, @Nullable String grupo, WithMonitor<Q> withMonitor)
            throws SistemaCerradoException, IOException, ImposibleLeerRespuestaException, TransaccionEfectivaException,
            TransaccionNoEjecutadaCorrectamenteException {

        TransaccionMonitorAuditable monitor =
                new TransaccionMonitorAuditable(as400factory, ambienteActual, executor, transaccionAuditDao, medioAmbiente,
                        grupo);
        String canal = medioAmbiente.getValorDe(MONITOR_CANAL, MONITOR_CANAL_DEFAULT);
        monitor.setDatoCabeceraEnvio(MON_VERSION, version);
        monitor.setDatoCabeceraEnvio(MON_CANAL, canal);
        withMonitor.preparar(monitor);
        monitor.ejecutar();
        return withMonitor.procesar(monitor);
    }

    public void revertir(PSR9101Bean<?, ?, ?> x, Record o, String grupo)
            throws SistemaCerradoException, ImposibleLeerRespuestaException, TransaccionEfectivaException, IOException {
        new TransaccionMonitorAuditable(as400factory, ambienteActual, executor, transaccionAuditDao, medioAmbiente,
                grupo).revertir(x, o);
    }

    public AmbienteActual getAmbienteActual() {
        return ambienteActual;
    }
}
