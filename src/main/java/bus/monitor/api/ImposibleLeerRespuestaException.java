/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.monitor.api;

import bus.monitor.entities.ErroresEbisa;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * @author Marcelo Morales
 */
public class ImposibleLeerRespuestaException extends Exception implements IExcepcionMonitorAqui {

    private static final long serialVersionUID = 928739287432L;

    public static final String CODIGO_TIMEOUT = "MVTMO";

    public static final String CODIGO_TIMEOUT_TRANS = "MVTMC";

    public static final String CODIGO_ERROR_INTERNO = "MVILR";

    public static final String CODIGO_RESPUESTA_VACIA = "MVRVA";

    private final String codigo;

    public ImposibleLeerRespuestaException(String message, String codigo) {
        super(message);
        this.codigo = codigo;
    }

    public ImposibleLeerRespuestaException(String message, Throwable cause, String codigo) {
        super(message, cause);
        this.codigo = codigo;
    }

    @Override
    public PageParameters getParameters(IErroresEbisa erroresEbisaDao) {
        final ErroresEbisa ee;
        if (codigo == null) {
            ee = erroresEbisaDao.find(CODIGO_ERROR_INTERNO);
        } else {
            ee = erroresEbisaDao.find(codigo);
        }
        return new PageParameters().
                add("desc", ee.getDesc()).
                add("sol", ee.getSol());

    }

    public String getCodigo() {
        return codigo;
    }
}
