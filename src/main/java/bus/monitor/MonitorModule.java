package bus.monitor;

import bus.monitor.as400.AS400Factory;
import bus.plumbing.api.AbstractModuleBisa;
import com.google.inject.Scopes;

/**
 * @author Marcelo Morales
 *         Created: 5/1/12 6:25 AM
 */
public class MonitorModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(AS400Factory.class).in(Scopes.SINGLETON);
        bindUI("bus/monitor/ui");
    }
}
