/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.monitor.entities;

import bus.monitor.api.TransaccionAuditOtrosErrores;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Marcelo Morales
 */
public class TransaccionAudit implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String INCLUIR_PETICION = "incluir peticion";

    private Long id;

    private String transaccion;

    private String usuario;

    private String wpost;

    private BigDecimal caja;

    private BigDecimal secuencia;

    private BigDecimal cajaEntrada;

    private BigDecimal secuenciaEntrada;

    private BigDecimal cajaSecuencia;

    private String rej1;

    private String rej2;

    private Date fecha;

    private Long milisegundos;

    private String device;

    private String wreve;

    private BigDecimal formatoEntrada1;

    private BigDecimal formatoEntrada2;

    private BigDecimal formatoEntrada3;

    private BigDecimal formatoSalida1;

    private BigDecimal formatoSalida2;

    private BigDecimal formatoSalida3;

    private BigDecimal numeroDeControl;

    private BigDecimal tipoDeDato;

    private TransaccionAuditOtrosErrores otrosErrores;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(String transaccion) {
        this.transaccion = transaccion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getWpost() {
        return wpost;
    }

    public void setWpost(String wpost) {
        this.wpost = wpost;
    }

    public BigDecimal getCaja() {
        return caja;
    }

    public void setCaja(BigDecimal caja) {
        this.caja = caja;
    }

    public BigDecimal getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(BigDecimal secuencia) {
        this.secuencia = secuencia;
    }

    public BigDecimal getCajaEntrada() {
        return cajaEntrada;
    }

    public void setCajaEntrada(BigDecimal cajaEntrada) {
        this.cajaEntrada = cajaEntrada;
    }

    public BigDecimal getSecuenciaEntrada() {
        return secuenciaEntrada;
    }

    public void setSecuenciaEntrada(BigDecimal secuenciaEntrada) {
        this.secuenciaEntrada = secuenciaEntrada;
    }

    public String getRej1() {
        return rej1;
    }

    public void setRej1(String rej1) {
        this.rej1 = rej1;
    }

    public String getRej2() {
        return rej2;
    }

    public void setRej2(String rej2) {
        this.rej2 = rej2;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getMilisegundos() {
        return milisegundos;
    }

    public void setMilisegundos(Long milisegundos) {
        this.milisegundos = milisegundos;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getWreve() {
        return wreve;
    }

    public void setWreve(String wreve) {
        this.wreve = wreve;
    }

    public BigDecimal getFormatoEntrada1() {
        return formatoEntrada1;
    }

    public void setFormatoEntrada1(BigDecimal formatoEntrada1) {
        this.formatoEntrada1 = formatoEntrada1;
    }

    public BigDecimal getFormatoEntrada2() {
        return formatoEntrada2;
    }

    public void setFormatoEntrada2(BigDecimal formatoEntrada2) {
        this.formatoEntrada2 = formatoEntrada2;
    }

    public BigDecimal getFormatoEntrada3() {
        return formatoEntrada3;
    }

    public void setFormatoEntrada3(BigDecimal formatoEntrada3) {
        this.formatoEntrada3 = formatoEntrada3;
    }

    public BigDecimal getFormatoSalida1() {
        return formatoSalida1;
    }

    public void setFormatoSalida1(BigDecimal formatoSalida1) {
        this.formatoSalida1 = formatoSalida1;
    }

    public BigDecimal getFormatoSalida2() {
        return formatoSalida2;
    }

    public void setFormatoSalida2(BigDecimal formatoSalida2) {
        this.formatoSalida2 = formatoSalida2;
    }

    public BigDecimal getFormatoSalida3() {
        return formatoSalida3;
    }

    public void setFormatoSalida3(BigDecimal formatoSalida3) {
        this.formatoSalida3 = formatoSalida3;
    }

    public BigDecimal getNumeroDeControl() {
        return numeroDeControl;
    }

    public void setNumeroDeControl(BigDecimal numeroDeControl) {
        this.numeroDeControl = numeroDeControl;
    }

    public BigDecimal getTipoDeDato() {
        return tipoDeDato;
    }

    public void setTipoDeDato(BigDecimal tipoDeDato) {
        this.tipoDeDato = tipoDeDato;
    }

    public TransaccionAuditOtrosErrores getOtrosErrores() {
        return otrosErrores;
    }

    public void setOtrosErrores(TransaccionAuditOtrosErrores otrosErrores) {
        this.otrosErrores = otrosErrores;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof TransaccionAudit))
            return false;

        TransaccionAudit that = (TransaccionAudit) o;

        if (id != null ? !id.equals(that.id) : that.id != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "TransaccionAudit{" +
                "id=" + id +
                ", transaccion='" + transaccion + '\'' +
                ", wpost=" + wpost +
                ", secuencia=" + secuencia +
                ", caja=" + caja +
                ", cajaEntrada=" + cajaEntrada +
                ", secuenciaEntrada=" + secuenciaEntrada +
                ", rej1='" + rej1 + '\'' +
                ", fecha=" + fecha +
                ", wreve=" + wreve +
                '}';
    }

    public BigDecimal getCajaSecuencia() {
        return cajaSecuencia;
    }

    public void setCajaSecuencia(BigDecimal cajaSecuencia) {
        this.cajaSecuencia = cajaSecuencia;
    }
}
