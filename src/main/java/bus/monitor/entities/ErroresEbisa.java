/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.monitor.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author Marcelo Morales
 *         Created: 11/8/11 2:39 PM
 */
@Entity
@Table(name = "tbl_Error")
public class ErroresEbisa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "Code", length = 5)
    private String code;

    @Column(name = "Description", length = 255, nullable = false)
    private String desc;

    @Column(name = "ModuleID", length = 5, nullable = false)
    private String module;

    @Column(name = "Solution", length = 255, nullable = false)
    private String sol;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getSol() {
        return sol;
    }

    public void setSol(String sol) {
        this.sol = sol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof ErroresEbisa))
            return false;

        ErroresEbisa that = (ErroresEbisa) o;

        if (code != null ? !code.equals(that.code) : that.code != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return code != null ? code.hashCode() : 0;
    }
}
