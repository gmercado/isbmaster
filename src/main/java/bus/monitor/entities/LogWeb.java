package bus.monitor.entities;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "tint_log_web")
public class LogWeb implements Serializable {

    @Id
    @Column(name = "RqUID",  nullable = false)
    private
    String RqUID;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RqDate")
    private
    Date RqDate;

    @Column(name = "StatusDesc", length = 255, nullable = false)
    private
    String StatusDesc;

    @Column(name = "StatusCodeAlt", length = 255, nullable = false)
    private
    String StatusCodeAlt;

    @Column(name = "StatusDesc", length = 255, nullable = false)
    private
    String StatusDescAlt;

    @Column(name = "StatusDescAlt", length = 255, nullable = false)
    private
    String StatusSource;

    @Column(name = "CustPermID", length = 255, nullable = false)
    private
    String CustPermID;

    @Column(name = "CustLOginID", length = 255, nullable = false)
    private
    String CustLOginID;

    @Column(name = "UserName", length = 255, nullable = false)
    private
    String UserName;

    @Column(name = "ActionData", length = 255, nullable = false)
    private
    String ActionData;

    @Column(name = "CurCode", length = 255, nullable = false)
    private
    String CurCode;

    @Column(name = "CurAmt", length = 255, nullable = false)
    private
    String CurAmt;

    @Column(name = "AccountFrom", length = 255, nullable = false)
    private
    String AccountFrom;

    @Column(name = "DataSent", length = 255, nullable = false)
    private
    String DataSent;

    @Column(name = "DataRecv", length = 255, nullable = false)
    private
    String DataRecv;

    @Column(name = "EndUserPage", length = 255, nullable = false)
    private
    String EndUserPage;

    @Column(name = "NumToken", length = 255, nullable = false)
    private
    String NumToken;

    @Column(name = "UserID", length = 255, nullable = false)
    private
    String UserID;


    public String getRqUID() {
        return RqUID;
    }

    public void setRqUID(String rqUID) {
        RqUID = rqUID;
    }

    public Date getRqDate() {
        return RqDate;
    }

    public void setRqDate(Date rqDate) {
        RqDate = rqDate;
    }

    public String getStatusDesc() {
        return StatusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        StatusDesc = statusDesc;
    }

    public String getStatusCodeAlt() {
        return StatusCodeAlt;
    }

    public void setStatusCodeAlt(String statusCodeAlt) {
        StatusCodeAlt = statusCodeAlt;
    }

    public String getStatusDescAlt() {
        return StatusDescAlt;
    }

    public void setStatusDescAlt(String statusDescAlt) {
        StatusDescAlt = statusDescAlt;
    }

    public String getStatusSource() {
        return StatusSource;
    }

    public void setStatusSource(String statusSource) {
        StatusSource = statusSource;
    }

    public String getCustPermID() {
        return CustPermID;
    }

    public void setCustPermID(String custPermID) {
        CustPermID = custPermID;
    }

    public String getCustLOginID() {
        return CustLOginID;
    }

    public void setCustLOginID(String custLOginID) {
        CustLOginID = custLOginID;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getActionData() {
        return ActionData;
    }

    public void setActionData(String actionData) {
        ActionData = actionData;
    }

    public String getCurCode() {
        return CurCode;
    }

    public void setCurCode(String curCode) {
        CurCode = curCode;
    }

    public String getCurAmt() {
        return CurAmt;
    }

    public void setCurAmt(String curAmt) {
        CurAmt = curAmt;
    }

    public String getAccountFrom() {
        return AccountFrom;
    }

    public void setAccountFrom(String accountFrom) {
        AccountFrom = accountFrom;
    }

    public String getDataSent() {
        return DataSent;
    }

    public void setDataSent(String dataSent) {
        DataSent = dataSent;
    }

    public String getDataRecv() {
        return DataRecv;
    }

    public void setDataRecv(String dataRecv) {
        DataRecv = dataRecv;
    }

    public String getEndUserPage() {
        return EndUserPage;
    }

    public void setEndUserPage(String endUserPage) {
        EndUserPage = endUserPage;
    }

    public String getNumToken() {
        return NumToken;
    }

    public void setNumToken(String numToken) {
        NumToken = numToken;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }
}
