/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.monitor.entities;

import bus.monitor.model.Cfp755;

import java.io.Serializable;

/**
 * @author Marcelo Morales
 *         Date: 9/14/11
 *         Time: 11:03 AM
 */
public class Cfp75502 implements Serializable, Cfp755 {

    private static final long serialVersionUID = 3863670590229339193L;

    private Long cfbk;

    private String cfmsgn;

    private String cfmsg;

    public Long getCfbk() {
        return cfbk;
    }

    public void setCfbk(Long cfbk) {
        this.cfbk = cfbk;
    }

    public String getCfmsgn() {
        return cfmsgn;
    }

    public void setCfmsgn(String cfmsgn) {
        this.cfmsgn = cfmsgn;
    }

    public String getCfmsg() {
        return cfmsg;
    }

    public void setCfmsg(String cfmsg) {
        this.cfmsg = cfmsg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cfp75502)) return false;

        Cfp75502 cfp75501 = (Cfp75502) o;

        if (cfbk != null ? !cfbk.equals(cfp75501.cfbk) : cfp75501.cfbk != null) return false;
        if (cfmsgn != null ? !cfmsgn.equals(cfp75501.cfmsgn) : cfp75501.cfmsgn != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cfbk != null ? cfbk.hashCode() : 0;
        result = 31 * result + (cfmsgn != null ? cfmsgn.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Cfp75501");
        sb.append("{cfmsgn='").append(cfmsgn).append('\'');
        sb.append(", cfmsg='").append(cfmsg).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
