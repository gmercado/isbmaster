package bus.monitor.as400;

import com.google.common.collect.Lists;
import org.apache.wicket.markup.html.form.IChoiceRenderer;

import java.util.ArrayList;

public class MonedasUtils {

    private MonedasUtils() {
    }

    /**
     * El c&oacute;digo de uso interno del <b>Banco BISA S.A.</b> para la moneda
     * Bolivianos
     *
     * @since 1.0
     */
    public static final short BISA_BOLIVIANOS = 0;

    /**
     * El c&oacute;digo de uso interno del <b>Banco BISA S.A.</b> para la moneda
     * D&oacute;lares
     *
     * @since 1.0
     */
    public static final short BISA_DOLARES = 2;

    /**
     * El c&oacute;digo de uso interno del <b>Banco BISA S.A.</b> para la moneda
     * UFVs
     *
     * @since 1.0
     */
    public static final short BISA_UFV = 4;

    /**
     * El c&oacute;digo de uso interno del <b>Banco BISA S.A.</b> para la moneda
     * Euros
     *
     * @since 1.0
     */
    public static final short BISA_EUROS = 11;

    /**
     * Devuelve la descripci&oacute;n de la moneda en el formato del
     * c&oacute;digo <b>BISA</b>.
     *
     * @param moneda C&oacute;digo de la moneda en formato <b>BISA</b>
     * @return Descrici&oacute;digo de la moneda en el formato interno del
     *         <b>Banco BISA S.A.</b>
     */
    public static String getDescripcionByBisa(short moneda) {
        switch (moneda) {
            case BISA_BOLIVIANOS:
                return "Bolivianos";
            case BISA_DOLARES:
                return "Dólares";
            case BISA_UFV:
                return "UFVs";
            case BISA_EUROS:
                return "Euros";
            default: {
                throw new IllegalArgumentException("No aceptamos la moneda '" + moneda + "'");
            }
        }
    }

    /**
     * Devuelve la descripci&oacute;n de la moneda en el formato del
     * c&oacute;digo <b>BISA</b>.
     *
     * @param moneda C&oacute;digo de la moneda en formato <b>BISA</b>
     * @return Descrici&oacute;digo de la moneda en el formato interno del
     *         <b>Banco BISA S.A.</b>
     */
    public static String getDescripcionHumanaByBisa(short moneda) {
        switch (moneda) {
            case BISA_BOLIVIANOS:
                return "Bs";
            case BISA_DOLARES:
                return "$us";
            case BISA_UFV:
                return "Ufv";
            case BISA_EUROS:
                return "Eur";
            default:
                throw new IllegalArgumentException("No aceptamos la moneda '" + moneda + "'");
        }
    }

    public static boolean esBolivianos(Short monedaTransaccion) {
        return monedaTransaccion != null && BISA_BOLIVIANOS == monedaTransaccion;
    }
    /*
    public static IChoiceRenderer<Short> choiceRenderer() {
        return new IChoiceRenderer<Short>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Object getDisplayValue(Short object) {
                return getDescripcionByBisa(object);
            }

            @Override
            public String getIdValue(Short object, int index) {
                return Integer.toString(index);
            }
        };
    }*/

    public static ArrayList<Short> todasLasMonedas() {
        return Lists.newArrayList(
                MonedasUtils.BISA_BOLIVIANOS,
                MonedasUtils.BISA_DOLARES,
                MonedasUtils.BISA_EUROS,
                MonedasUtils.BISA_UFV);
    }
}
