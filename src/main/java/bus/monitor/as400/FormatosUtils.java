/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.monitor.as400;

import bus.monitor.api.TransaccionIncompletaException;
import com.ibm.as400.access.ExtendedIllegalArgumentException;
import com.ibm.as400.access.FieldDescription;
import com.ibm.as400.access.Record;
import com.ibm.as400.access.RecordFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author Marcelo Morales
 * @author Ricardo Primintela
 */
public class FormatosUtils {

    private static final Logger LOG = LoggerFactory.getLogger(FormatosUtils.class);

    private FormatosUtils() {
    }

    public static String getDatoString(Record record, String nombre) throws TransaccionIncompletaException {
        if (nombre == null) {
            throw new IllegalArgumentException("Nombre no puede ser null");
        }
        if (record == null) {
            throw new TransaccionIncompletaException();
        }
        try {
            Object dato = record.getField(nombre);
            if (dato == null) {
                return null;
            }
            if (dato instanceof String) {
                return (String) dato;
            }
            LOG.warn("Convirtiendo el dato '" + nombre + "' en String", new Throwable());
            return dato.toString();
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalStateException("Java o AS/400 no entienen la codificacion, ESTO REALMENTE NO DEBERIA " +
                    "OCURRIR y seguramente es porque se corre una instalacion de Java no soportada, porque la AS/400" +
                    " tiene alguna condicion bogus o debido a un error GRAVE de programacion.", ex);
        } catch (ExtendedIllegalArgumentException e) {
            StringBuilder stringBuilder = construirDatosFormato(record.getRecordFormat());
            LOG.error("Ha ocurrido un error al obtener un dato: {}. A continuacion la definicion" +
                    " de datos: {}. Retornando null", e.getMessage(), stringBuilder.toString());
            return null;
        }
    }

    public static BigDecimal getDatoBigDecimal(Record record, String nombre) throws TransaccionIncompletaException {
        if (nombre == null) {
            throw new IllegalArgumentException("Nombre no puede ser null");
        }
        if (record == null) {
            throw new TransaccionIncompletaException();
        }
        try {
            Object dato = record.getField(nombre);
            if (dato instanceof BigDecimal) {
                return (BigDecimal) dato;
            }
            LOG.warn("Intentando convertir el parametro '" + nombre + "' a BigDecimal", new Throwable());
            return new BigDecimal(dato.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalStateException("Java o AS/400 no entienen la codificacion, ESTO REALMENTE NO DEBERIA " +
                    "OCURRIR y seguramente es porque se corre una instalacion de Java no soportada, porque la AS/400" +
                    " tiene alguna condicion bogus o debido a un error GRAVE de programacion.", ex);
        } catch (ExtendedIllegalArgumentException e) {
            StringBuilder stringBuilder = construirDatosFormato(record.getRecordFormat());
            LOG.error("Ha ocurrido un error al obtener un dato: {}. A continuacion la definicion" +
                    " de datos: {}. Retornando null", e.getMessage(), stringBuilder.toString());
            return null;
        }

    }

    public static int getDatoInt(Record record, String nombre) throws TransaccionIncompletaException {
        if (nombre == null) {
            throw new IllegalArgumentException("Nombre no puede ser null");
        }
        if (record == null) {
            throw new TransaccionIncompletaException();
        }
        try {
            Object o = record.getField(nombre);
            if (o instanceof BigDecimal) {
                return ((Number) o).intValue();
            }
            return Integer.parseInt((String) o);
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalStateException("Java o AS/400 no entienen la codificacion, ESTO REALMENTE NO DEBERIA " +
                    "OCURRIR y seguramente es porque se corre una instalacion de Java no soportada, porque la AS/400" +
                    " tiene alguna condicion bogus o debido a un error GRAVE de programacion.", ex);
        } catch (ExtendedIllegalArgumentException e) {
            StringBuilder stringBuilder = construirDatosFormato(record.getRecordFormat());
            LOG.error("Ha ocurrido un error al obtener un dato: {}. A continuacion la definicion" +
                    " de datos: {}. Retornando -1", e.getMessage(), stringBuilder.toString());
            return -1;
        }
    }

    public static BigDecimal getDatoDecimalConSigno(Record record, String field, String signofld) throws TransaccionIncompletaException {
        if (field == null) {
            throw new IllegalArgumentException("field no puede ser null");
        }
        if (record == null) {
            throw new TransaccionIncompletaException();
        }
        if (signofld == null) {
            throw new IllegalArgumentException("signofld no puede ser null");
        }
        try {
            Object dato = record.getField(field);
            Object datoSigno = record.getField(signofld);
            if (dato instanceof BigDecimal) {
                if (datoSigno instanceof BigDecimal) {
                    if (((BigDecimal) datoSigno).compareTo(BigDecimal.ZERO) == 0) {
                        return (BigDecimal) dato;
                    } else {
                        return ((BigDecimal) dato).negate();
                    }
                } else {
                    LOG.error("Error en el parametro de signo {}", signofld);
                }
                return (BigDecimal) dato;
            }
            LOG.warn("Intentando convertir el parametro '{}' a BigDecimal (Cuerpo 1)", field);
            return new BigDecimal(dato.toString());

        } catch (UnsupportedEncodingException ex) {
            throw new IllegalStateException("Java o AS/400 no entienen la codificacion, ESTO REALMENTE NO DEBERIA " +
                    "OCURRIR y seguramente es porque se corre una instalacion de Java no soportada, porque la AS/400" +
                    " tiene alguna condicion bogus o debido a un error GRAVE de programacion.", ex);
        } catch (ExtendedIllegalArgumentException e) {
            StringBuilder stringBuilder = construirDatosFormato(record.getRecordFormat());
            LOG.error("Ha ocurrido un error al obtener un dato: {}. A continuacion la definicion" +
                    " de datos: {}. Retornando null", e.getMessage(), stringBuilder.toString());
            return null;
        }
    }

    public static Date getDatoDate(Record record, String nombre) throws TransaccionIncompletaException {
        if (nombre == null) {
            throw new IllegalArgumentException("Nombre no puede ser null");
        }
        if (record == null) {
            throw new TransaccionIncompletaException();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Object dato = null;
        try {
            dato = record.getField(nombre);
            if (dato instanceof BigDecimal) {
                BigDecimal dato1 = (BigDecimal) dato;
                if (BigDecimal.ZERO.compareTo(dato1) == 0) {
                    return null;
                }
                return sdf.parse(dato1.toPlainString());
            }
            return sdf.parse(((String) dato).trim());
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalStateException("Java o AS/400 no entienen la codificacion, ESTO REALMENTE NO DEBERIA " +
                    "OCURRIR y seguramente es porque se corre una instalacion de Java no soportada, porque la AS/400" +
                    " tiene alguna condicion bogus o debido a un error GRAVE de programacion.", ex);
        } catch (ExtendedIllegalArgumentException e) {
            StringBuilder stringBuilder = construirDatosFormato(record.getRecordFormat());
            LOG.error("Ha ocurrido un error al obtener un dato: {}. A continuacion la definicion" +
                    " de datos: {}. Retornando null", e.getMessage(), stringBuilder.toString());
            return null;
        } catch (ParseException e) {
            LOG.error("No puedo encontrar una fecha en " + dato, e);
            return null;
        }
    }

    public static StringBuilder construirDatosFormato(RecordFormat recordFormat) {
        StringBuilder stringBuilder = new StringBuilder();
        String[] fieldNames = recordFormat.getFieldNames();
        for (String fieldname : fieldNames) {
            FieldDescription fieldDescription = recordFormat.getFieldDescription(fieldname);
            stringBuilder.append("Nombre: '");
            stringBuilder.append(fieldname);
            stringBuilder.append("' Tipo: ");
            stringBuilder.append(fieldDescription.getDataType().getInstanceType());
            stringBuilder.append(" longtud: ");
            stringBuilder.append(fieldDescription.getLength());
            stringBuilder.append("\n");
        }
        return stringBuilder;
    }

    public static NumberFormat numberFormat() {
        DecimalFormat es = new DecimalFormat();

        DecimalFormatSymbols newSymbols = new DecimalFormatSymbols();
        newSymbols.setDecimalSeparator('.');
        newSymbols.setGroupingSeparator(',');
        es.setDecimalFormatSymbols(newSymbols);

        es.setMaximumFractionDigits(2);
        es.setMinimumFractionDigits(2);
        es.setMinimumIntegerDigits(1);
        es.setParseBigDecimal(true);
        return es;
    }

    public static NumberFormat numberFormatConComas() {
        DecimalFormat es = new DecimalFormat();

        DecimalFormatSymbols newSymbols = new DecimalFormatSymbols();
        newSymbols.setDecimalSeparator(',');
        newSymbols.setGroupingSeparator('.');
        es.setDecimalFormatSymbols(newSymbols);

        es.setMaximumFractionDigits(2);
        es.setMinimumFractionDigits(2);
        es.setMinimumIntegerDigits(1);
        es.setParseBigDecimal(true);
        return es;
    }

    public static NumberFormat numberFormat4() {
        DecimalFormat es = new DecimalFormat();

        DecimalFormatSymbols newSymbols = new DecimalFormatSymbols();
        newSymbols.setDecimalSeparator('.');
        newSymbols.setGroupingSeparator(',');
        es.setDecimalFormatSymbols(newSymbols);

        es.setMaximumFractionDigits(4);
        es.setMinimumFractionDigits(3);
        es.setMinimumIntegerDigits(1);
        es.setParseBigDecimal(true);
        return es;
    }

    public static void formatearFecha(StringBuffer appendable, Date fechaAHacer) {
        Calendar c = Calendar.getInstance();
        c.setTime(fechaAHacer);

        //Calendar ahora = Calendar.getInstance();
//      	 GCentellas@grupobisa.com
//           sender-time:	 Sent at 4:00 PM (GMT-04:00). Current time there: 11:56 AM. ?
//           to:	 marcelomorales.name@gmail.com
//           date:	 Tue, Nov 15, 2011 at 4:00 PM
//           subject:	 e-BISA Movil - Observaciones
//
//        if (ahora.get(Calendar.YEAR) == c.get(Calendar.YEAR)) {
//            new SimpleDateFormat("d-MMM", new Locale("es")).
//                    format(fechaAHacer, appendable, new FieldPosition(appendable.length()));
//            return;
//        }

        new SimpleDateFormat("d-MMM-yy", new Locale("es")).
                format(fechaAHacer, appendable, new FieldPosition(appendable.length()));
    }

    public static void formatearMesYanyo(StringBuffer appendable, Date fechaAHacer) {
        Calendar c = Calendar.getInstance();
        c.setTime(fechaAHacer);

        new SimpleDateFormat("MMM-yyyy", new Locale("es")).
                format(fechaAHacer, appendable, new FieldPosition(appendable.length()));
    }


    public static void formatearFechaEnCualquierMomento(StringBuffer appendable, Date fechaAHacer) {
        new SimpleDateFormat("d-MMM-yy", new Locale("es")).
                format(fechaAHacer, appendable, new FieldPosition(appendable.length()));
    }

    public static void formatearFechaEnElPasado(StringBuffer appendable, Date fechaAHacer) {

        if (new Date().before(fechaAHacer)) {
            LOG.error("Estoy formateando una fecha en el futuro cuando uso formatearFechaEnElPasado", new Throwable());
        }

//      	 GCentellas@grupobisa.com
//           sender-time:	 Sent at 4:00 PM (GMT-04:00). Current time there: 11:56 AM. ?
//           to:	 marcelomorales.name@gmail.com
//           date:	 Tue, Nov 15, 2011 at 4:00 PM
//           subject:	 e-BISA Movil - Observaciones
//
//        Calendar c = Calendar.getInstance();
//        c.setTime(fechaAHacer);
//        Calendar ahora = Calendar.getInstance();
//        if (ahora.get(Calendar.YEAR) == c.get(Calendar.YEAR)) {
//            new SimpleDateFormat("d-MMM", new Locale("es")).
//                    format(fechaAHacer, appendable, new FieldPosition(appendable.length()));
//            return;
//        }
//
//        Calendar haceTiempo = Calendar.getInstance();
//        haceTiempo.add(Calendar.MONTH, -11);
//        haceTiempo.set(Calendar.DAY_OF_MONTH, 1);
//        if (c.after(haceTiempo)) {
//            new SimpleDateFormat("d-MMM", new Locale("es")).
//                    format(fechaAHacer, appendable, new FieldPosition(appendable.length()));
//            return;
//        }

        new SimpleDateFormat("d-MMM-yy", new Locale("es")).
                format(fechaAHacer, appendable, new FieldPosition(appendable.length()));
    }

    public static void formatearHora(StringBuffer appendable, Date fecha) {
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        new SimpleDateFormat("HH:mm").format(fecha, appendable, new FieldPosition(appendable.length()));
    }

    public static String formatearNumeroDeCuenta(String cta) {
        final StringBuilder stringBuilder = new StringBuilder();
        appendNumeroCuenta(cta, stringBuilder);
        return stringBuilder.toString();
    }

    public static void appendNumeroCuenta(String nroCta, Appendable stringBuilder) {
        try {
            if (nroCta == null || nroCta.length() < 5) {
                stringBuilder.append(nroCta);
                return;
            }

            if (nroCta.length() == 16) {
                stringBuilder.append(nroCta, 0, 4)
                        .append('-')
                        .append(nroCta, 4, 8)
                        .append('-')
                        .append(nroCta, 8, 12)
                        .append('-')
                        .append(nroCta, 12, 16);
                return;
            }

            int length = nroCta.length();
            stringBuilder.
                    append(nroCta.substring(0, length - 4)).
                    append('-').
                    append(nroCta.substring(length - 4, length - 1)).
                    append('-').
                    append(nroCta.substring(length - 1));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static String getDescripcionCuenta(char wtpocta) {
        final StringBuilder stringBuilder = new StringBuilder();
        appendDescripcionCuenta(wtpocta, stringBuilder, null);
        return stringBuilder.toString();
    }

    public static void appendDescripcionCuenta(char wtpocta, Appendable stringBuilder, Object cuenta) {
        try {
            switch (wtpocta) {
                case 'A':
                case 1:
                    stringBuilder.append("Caja de Ahorro ");
                    break;
                case 'C':
                case 6:
                    stringBuilder.append("Cuenta Corriente ");
                    break;
                case 'T':
                    stringBuilder.append("Tarjeta de Credito ");
                    break;
                case 'P':
                    stringBuilder.append("Prestamo ");
                    break;
                case 'D':
                    stringBuilder.append("Deposito a Plazo Fijo ");
                    break;
                case 'B':
                    stringBuilder.append("Boleta de Garantia ");
                    break;
                case 'E':
                    stringBuilder.append("Tarjeta de Debito ");
                    break;
                case ' ':
                    LOG.debug("Se ha detectado un tipo de cuenta que asumo que es linea <{}>, registro <{}>", wtpocta, cuenta);
                    stringBuilder.append("L�nea de cr�dito ");
                    break;
                default:
                    LOG.warn("Se ha detectado un tipo de cuenta no conocido <{}>, registro <{}>", wtpocta, cuenta);
                    stringBuilder.append("Cuenta");
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static String telefono(String object) {
        if (object == null) {
            return null;
        }
        if (object.length() < 7) {
            return object;
        }
        return "(" + object.substring(0, 3) + ") " + object.substring(3, 6) + "-" + object.substring(6);
    }

    public static String formatSigno(BigDecimal wpsaac, BigDecimal wpnegsac) {
        final NumberFormat numberFormat = numberFormat();
        if (wpnegsac.compareTo(BigDecimal.ZERO) != 0) {
            return numberFormat.format(wpsaac.negate());
        }
        return numberFormat.format(wpsaac);
    }

    public static String formatMayorACero(BigDecimal wpsaac, BigDecimal wpnegsac) {
        final NumberFormat numberFormat = numberFormat();
        if (wpnegsac.compareTo(BigDecimal.ZERO) != 0) {
            return numberFormat.format(new BigDecimal("0.00"));
        }
        return numberFormat.format(wpsaac);
    }
}
