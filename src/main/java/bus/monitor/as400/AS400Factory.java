/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.monitor.as400;

import bus.env.api.MedioAmbiente;
import bus.monitor.api.SistemaCerradoException;
import bus.plumbing.utils.Shutdowner;
import bus.plumbing.utils.Watch;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.ibm.as400.access.*;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyVetoException;
import java.io.*;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import static bus.config.api.Configurations.CONVERTIR_CCSID;
import static bus.env.api.Variables.*;
import static bus.plumbing.utils.Watches.FORMATOS;

/**
 * @author Marcelo Morales
 */
@Singleton
public class AS400Factory {

    public static final Base64 base64 = new Base64();
    private static final Logger LOGGER = LoggerFactory.getLogger(AS400Factory.class);
    private static final int TAMANYO_POOL = 10;
    private static final long ESPERA_ENTRE_INTENTOS = 1000L;
    private static final int INTENTOS = 3;
    private static final Map<String, RecordFormat> RECORD_FORMATS =
            Collections.synchronizedMap(new LinkedHashMap<String, RecordFormat>());
    private static final Watch watchFormatos = new Watch(FORMATOS);
    private final AS400ConnectionPool connectionPool = new AS400ConnectionPool();
    private final MedioAmbiente medioAmbiente;

    @Inject
    public AS400Factory(MedioAmbiente medioAmbiente, Shutdowner shutdowner) {
        this.medioAmbiente = medioAmbiente;

        connectionPool.close();
        /*
         * TODO: NECESITAMOS UNA MEJOR FORMA DE COLOCAR SO-TIMEOUT: EXPLORAR:
         *   1) AUMENTAR EL TIMEOUT A VARIAS HORAS
         *   2) DECORAR EL OBJETO AS/400
         *   3) PROBAR 'UNICAMENTE INACTIVIDAD SEG'UN AS/400.
         *
        SocketProperties socketProperties = new SocketProperties();
        socketProperties.setSoTimeout(SO_TIMEOUT_AS400);
        connectionPool.setSocketProperties(socketProperties);
        connectionPool.setMaxInactivity(SO_TIMEOUT_AS400 * 9 / 10); // 90% del timeout, para evitar timeout
         */
        connectionPool.addConnectionPoolListener(new ConnectionPoolListener() {

            @Override
            public void connectionCreated(ConnectionPoolEvent arg0) {
                AS400 as400 = (AS400) arg0.getSource();
                try {
                    int cccsid = getCcsid();
                    int as400Ccsid = as400.getCcsid();
                    if (cccsid != as400Ccsid) {
                        LOGGER.warn("la parametrizacion as/400 a fallado porque el ccsid {} no es el esperado {} considere configurar {}",
                                new Object[]{as400Ccsid, cccsid, CONVERTIR_CCSID});
                    }
                    as400.setGuiAvailable(false);
                } catch (PropertyVetoException e) {
                    LOGGER.error("Ha ocurrido un error inesperado al parametrizar la 400", e);
                }

            }

            // TODO: Verificar el tiempo entre entrega y devolucion....

            @Override
            public void connectionReleased(ConnectionPoolEvent arg0) {
                // 2012-03-26 17:40:05,575 [883466865@qtp-281035435-0] DEBUG ebisa.plumbing.monitor.as400.AS400Factory - connectionReleased com.ibm.as400.access.ConnectionPoolEvent[source=AS400 (system name: 'bisades.grupobisa.net' user ID: 'AQUA411'):com.ibm.as400.access.AS400@60527849]
                LOGGER.debug("connectionReleased {}", arg0);
            }

            @Override
            public void connectionReturned(ConnectionPoolEvent arg0) {
                // 2012-03-26 17:40:05,577 [883466865@qtp-281035435-0] DEBUG ebisa.plumbing.monitor.as400.AS400Factory - connectionReturned com.ibm.as400.access.ConnectionPoolEvent[source=AS400 (system name: 'bisades.grupobisa.net' user ID: 'AQUA411'):com.ibm.as400.access.AS400@60527849]
                LOGGER.debug("connectionReturned {}", arg0);
            }

            // No necesitamos explotar la informacion de este listener, esto parece ya problema superado...
            @Override
            public void maintenanceThreadRun(ConnectionPoolEvent arg0) {
                LOGGER.debug("Corriendo el thread de mantenimiento {}", arg0);
            }


            @Override
            public void connectionPoolClosed(ConnectionPoolEvent arg0) {
                LOGGER.info("Pool a la as/400 cerrado");
            }

            @Override
            public void connectionExpired(ConnectionPoolEvent arg0) {
                try {
                    LOGGER.warn("El driver de as400 ha reportado una conexion expirada desde el objeto '{}' clase '{}'. {}",
                            new Object[]{arg0.getSource(), arg0.getSource().getClass(), arg0});
                } catch (Exception e) {
                    LOGGER.warn("El driver de as400 ha reportado una conexion expirada y no pude mostrar el origen", e);
                }
            }

        });

        /* OJO:    Tue Jun  7 10:19:16 BOT 2011
         *
         * Lo siguiente parece no tener el efecto que queremos. B�sicamente, la cantidad de conexiones durante
         * el test PoolTest#testPoolDentroAqua() ha mostrado que esta cantidad de conexiones no se respeta:
         *
         * Al principio:
         *  netstat -tn |grep '1.10.3.230.*ESTABL' |wc -l
         *  129
         *
         * Luego:
         *  $ netstat -tn |grep '1.10.3.230.*ESTABL' |wc -l
         *  37
         *
         * De modo que no creo que estemos haciendo esto de forma perfecta. Para otros proyectos, tal vez convenga
         * utilizar. Vamos a probar esto aqui:
         */
        connectionPool.setMaxConnections(TAMANYO_POOL);

        /* OJO:  Wed May 25 17:24:14 BOT 2011
         *
         * Lo siguiente se ha colocado porque es la unica forma en la que PoolTest#testPoolDentroAqua()
         * funciona siempre. La ultima vez que hemos usado un threadUsed(false) hemos tenido un problema
         * de desempenyo en remesas. Pero espero que la cantidad de conexiones (TAMANYO_POOL) hayan sido
         * la razon porque por defecto lo hemos venido colocando en 1. Ahora esta constante tiene un
         * valor bastante grande (5 al momento de hacer este cambio). Adicionalmente el cambio de API ha
         * necesitado que cambiemos algunos programas, como en SIN para incluir el bloque finally.
         */
        connectionPool.setThreadUsed(false);

        connectionPool.setLog(new Log() {

            @Override
            public void log(String s) {
                LOGGER.debug(s);
            }

            @Override
            public void log(String s, Throwable throwable) {
                LOGGER.info(s, throwable);
            }
        });

        /* OJO:    Tue Jun  7 10:24:44 BOT 2011
         *
         * Ahora no nos gusta llenar el tema, prefiero que se haga bajo demanda porque igualito estamos lanzando
         * el ConnectionPoolException cuando se requiere una conexi�n.
         *
         * JT400 har� un hilo AS400 Read Daemon para cada hilo que lo inicie. Este hilo se muere si se hace una
         * des-conexi�n. El hilo se re-utiliza si se re-utiliza el hilo. En el caso de thread-pools y el servicio por
         * http eso es una ventaja, pero es una des-ventaja en los nuevos hilos de "reversi�n" y en el �ltimo
         * cambio a la infraestructura de trabajos as�ncronos, donde se spawnean potencialmente muchos hilos.
         *
         * TODO: queda pendiente el cambio de la interfaz de getAS400()/ret(AS400) sin servicio, que no desconecta.
         */
//        try {
//            connectionPool.fill(system, username, password, AS400.DATAQUEUE, 1);
//        } catch (ConnectionPoolException e) {
//            LOGGER.error("Error al crear el pool de conexiones AS/400: " + e.getMessage(), e);
//        }
        shutdowner.register(new Runnable() {

            @Override
            public void run() {
                try {
                    connectionPool.close();
                } catch (Exception e) {
                    LOGGER.error("Error al cerrar el pool", e);
                }
            }
        });
    }

    public static Map<String, RecordFormat> getRecordFormats() {
        Map<String, RecordFormat> unmodifiableMap = Collections.unmodifiableMap(RECORD_FORMATS);
        LOGGER.debug("Formatos actuales: {}", unmodifiableMap);
        return unmodifiableMap;
    }

    public static String explainMessageList(AS400Message[] messageList) {
        StringBuilder builder = new StringBuilder();
        if (messageList == null) {
            return "(sin mensajes)";
        }
        for (AS400Message aS400Message : messageList) {
            builder.append("date=");
            builder.append(aS400Message.getDate());
            builder.append(" defaultReply=");
            builder.append(aS400Message.getDefaultReply());
            builder.append(" fileName=");
            builder.append(aS400Message.getFileName());
            builder.append(" help=");
            builder.append(aS400Message.getHelp());
            builder.append(" id=");
            builder.append(aS400Message.getID());
            builder.append(" libraryName=");
            builder.append(aS400Message.getLibraryName());
            builder.append(" path=");
            builder.append(aS400Message.getPath());
            builder.append(" severity=");
            builder.append(aS400Message.getSeverity());
            builder.append(" text=");
            builder.append(aS400Message.getText());
            builder.append(" type=");
            builder.append(aS400Message.getType());
            builder.append(" , ");
        }
        return builder.toString();
    }

    public static String getDescripcionSecurityException(AS400SecurityException ex, Object... o) {
        LOGGER.debug("Listado de mensajes de una exepcion: <{}>", explainMessageList(ex.getMessageList()));
        switch (ex.getReturnCode()) {
            case AS400SecurityException.DIRECTORY_ENTRY_ACCESS_DENIED:
                return MessageFormat.format("Acceso denegado al directorio especificado {0}", o);
            case AS400SecurityException.LIBRARY_AUTHORITY_INSUFFICIENT:
                return MessageFormat.format("Acceso denegado a la libreria especificada", o);
            case AS400SecurityException.OBJECT_AUTHORITY_INSUFFICIENT:
                return MessageFormat.format("Acceso denegado al objeto especificado {0}/{1}", o);
            case AS400SecurityException.PASSWORD_ERROR:
                return "Ha ocurrido un error general de contraseña.";
            case AS400SecurityException.PASSWORD_EXPIRED:
                return "La contraseña del usuario ha expirado.";
            case AS400SecurityException.PASSWORD_INCORRECT:
            case AS400SecurityException.PASSWORD_INCORRECT_USERID_DISABLE:
                return "Contraseña incorrecta";
            case AS400SecurityException.USERID_DISABLE:
                return "El usuario ha sido deshabilitado por el sistema";
            case AS400SecurityException.USERID_ERROR:
                return "El nombre de usuario tiene un error no especificado";
            case AS400SecurityException.USERID_UNKNOWN:
                return "El nombre de usuario no existe en AS/400";
            default:
                return "Error general, codigo de error=" + ex.getReturnCode();
        }
    }

    public static RecordFormat getRecordFormat(String x) throws IOException, ClassNotFoundException {
        return (RecordFormat) new ObjectInputStream(new ByteArrayInputStream(base64.decode(x))).readObject();
    }

    public AS400 getAS400(int service) throws ConnectionPoolException {
        String system = medioAmbiente.getValorDe(MONITOR_SISTEMA, MONITOR_SISTEMA_DEFAULT);
        String username = medioAmbiente.getValorDe(MONITOR_USUARIO, MONITOR_USUARIO_DEFAULT);
        String password = medioAmbiente.getValorDe(MONITOR_PASSWORD, MONITOR_PASSWORD_DEFAULT);

        /*
         * Secure connection es mandatorio desde 31-ago-2011 debido a auditor�a del e-bisa.
         */
        //AS400 as400 = connectionPool.getSecureConnection(system, username, password, service);
        // TODO: desde el power 7 (Dec 12 2011), no funciona con secure!!!
        AS400 as400 = connectionPool.getConnection(system, username, password, service);
        LOGGER.debug("Obteniendo objeto {} del pool", as400);
        return as400;
    }

    public void ret(AS400 as400, int servicio) {
        if (as400 != null) {
            /*
             * OJO: Tue Jun  7 10:33:01 BOT 2011
             *
             * Con este "disconnectService", el SO_TIMEOUT ya no es tan importante, dado que no mantendremos hilos
             * leyendo de la AS400. Imagino que el problema queda est� restringido.
             */
            as400.disconnectService(servicio);
            LOGGER.debug("Retornando el objeto {} al pool", as400);
            connectionPool.returnConnectionToPool(as400);
        }
    }

    public String getUsername() {
        return medioAmbiente.getValorDe(MONITOR_USUARIO, MONITOR_USUARIO_DEFAULT);
    }

    public RecordFormat buscarFormato(String archivo)
            throws IOException, SistemaCerradoException {
        String libreria = medioAmbiente.getValorDe(LIBRERIA_FORMATOS_MONITOR, LIBRERIA_FORMATOS_MONITOR_DEFAULT);
        return buscarFormato(libreria, archivo);
    }

    public RecordFormat buscarFormato(String libreria, String archivo)
            throws IOException, SistemaCerradoException {
        RecordFormat rf;

        final String key = libreria + "/" + archivo;
        if (RECORD_FORMATS.containsKey(key)) {
            rf = RECORD_FORMATS.get(key);
            LOGGER.debug("Registro {} encontrado en el cache de formatos", key);
        } else {
            LOGGER.debug("Buscando {} en la base de datos", key);
            int intentos = 0;
            while (true) {
                intentos++;
                AS400 as400 = null;
                watchFormatos.start();
                try {
                    as400 = getAS400(AS400.COMMAND);
                    QSYSObjectPathName name = new QSYSObjectPathName(libreria, archivo, "FILE");
                    AS400FileRecordDescription description = new AS400FileRecordDescription(as400, name.getPath());
                    rf = description.retrieveRecordFormat()[0];
                    RECORD_FORMATS.put(key, rf);
                    LOGGER.debug("Registro {} encontrado y colocado en el cache", key);
                    break;
                } catch (InterruptedException ex) {
                    throw new RuntimeException("No se ha encontrado respuesta a la obtencion de formato " +
                            "(esto realmente NO DEBERIA DARSE)", ex);
                } catch (AS400SecurityException ex) {
                    LOGGER.error("Ha existido un error al obtener un formato de archivo para formatear el monitor," +
                            " a continuacion se muestra una descripcion especifica del problema: " +
                            getDescripcionSecurityException(ex, libreria, archivo), ex);
                    throw new IllegalStateException("La aplicacion no se encuentra en un estado correcto porque las " +
                            "credenciales de conexion a la as/400 no permiten realizar la operacion de examinar un " +
                            "archivo para formatear una peticion o respuesta al monitor.");
                } catch (AS400Exception ex) {
                    LOGGER.error("Error general AS/400 al intentar obtener un formato para el monitor. Mensajes={}",
                            explainMessageList(ex.getAS400MessageList()));
                    throw new IllegalStateException("Error general AS/400 al intentar obtener un archivo para formatear" +
                            " el monitor de transacciones o la lectura de un dataqueue.", ex);
                } catch (IOException e) {
                    if (intentos >= INTENTOS) {
                        LOGGER.error("Se ha sobrepasado el limite de reintentos, lanzando la excepcion original");
                        throw e;
                    }
                    LOGGER.warn("Error de comunicacion AS/400. Reintentando");
                    try {
                        Thread.sleep(ESPERA_ENTRE_INTENTOS);
                    } catch (InterruptedException ex) {
                        // IGNORADO
                    }
                } catch (ConnectionPoolException e) {
                    LOGGER.error("No se ha podido obtener un elemento del pool de as/400, con el retorno " +
                            "(CONFLICTING_POOL_SIZES = 3, UNKNOWN_ERROR = 2, MAX_CONNECTIONS_REACHED = 1) : <<<" +
                            e.getReturnCode() + ">>>", e);

                    if (e.getException() != null) {
                        LOGGER.error("No se ha podido obtener un elemento del pool de as/400, la excepcion original es",
                                e.getException());
                    }
                    throw new SistemaCerradoException();
                } finally {
                    watchFormatos.stop();
                    ret(as400, AS400.COMMAND);
                }
            }
        }

        if (LOGGER.isTraceEnabled()) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            new ObjectOutputStream(out).writeObject(rf);
            LOGGER.trace("A continuacion formato {} " + key + "\n" + new Base64().encodeToString(out.toByteArray()));
        }
        return rf;
    }

    public Integer getCcsid() {
        return AS400Factory.this.medioAmbiente.getValorIntDe(MONITOR_CCSID, MONITOR_CCSID_DEFAULT);
    }
}
