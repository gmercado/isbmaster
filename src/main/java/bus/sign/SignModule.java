package bus.sign;

import bus.plumbing.api.AbstractModuleBisa;
import bus.sign.api.KeyFactory;
import bus.sign.api.Signature;

/**
 * @author by rsalvatierra on 20/12/2016.
 */
public class SignModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(Signature.class).to(KeyFactory.class);
    }
}
