package bus.sign.api;

import bus.sign.utils.SeguridadException;

/**
 * @author by rsalvatierra on 19/12/2016.
 */
public interface Signature {

    String getContenidoProcesado(String content, String aliasLlavePrivada, String claveLlavePrivada, String aliasCertificadoPublico,
                                 String usuarioCertificado) throws SeguridadException;
}
