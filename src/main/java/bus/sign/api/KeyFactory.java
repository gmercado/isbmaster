package bus.sign.api;

import bus.env.api.MedioAmbiente;
import bus.plumbing.utils.Par;
import bus.sign.utils.SeguridadException;
import bus.sign.utils.XMLUtilsBisa;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSEncryptionPart;
import org.apache.ws.security.WSPasswordCallback;
import org.apache.ws.security.WSSecurityEngine;
import org.apache.ws.security.components.crypto.CredentialException;
import org.apache.ws.security.components.crypto.Crypto;
import org.apache.ws.security.components.crypto.Merlin;
import org.apache.ws.security.message.WSSecEncrypt;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.WSSecSignature;
import org.apache.ws.security.message.WSSecTimestamp;
import org.apache.ws.security.message.token.BinarySecurity;
import org.apache.ws.security.message.token.UsernameToken;
import org.apache.ws.security.message.token.X509Security;
import org.apache.ws.security.util.WSSecurityUtil;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.ObjectContainer;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.signature.XMLSignatureException;
import org.apache.xml.security.transforms.TransformationException;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Constants;
import org.apache.xml.security.utils.XMLUtils;
import org.apache.xpath.XPathAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Properties;
import java.util.Vector;

/**
 * @author by rsalvatierra on 19/12/2016.
 */
public class KeyFactory implements Signature {

    private static final Logger LOG = LoggerFactory.getLogger(KeyFactory.class);
    public static final String NMSPACE = "http://www.w3.org/2003/05/soap-envelope";
    public static final String NMSPACE1 = "http://www.w3.org/2005/08/addressing";
    public static final String NMSPACE2 = "http://schemas.xmlsoap.org/ws/2005/02/rm";

    private final MedioAmbiente medioAmbiente;

    private static final String KEYSTORE_FILE_TYPE = "seguridad.certificados.tipoArchivo";
    private static final String KEYSTORE_FILE_TYPE_DEFAULT = "";
    private static final String PRIVATE_KEY_ALIAS = "seguridad.certificados.aliasLlavePrivada";
    private static final String PRIVATE_KEY_ALIAS_DEFAULT = "";
    private static final String PUBLIC_KEYSTORE_FILE = "seguridad.certificados.repositorioPublico";
    private static final String PUBLIC_KEYSTORE_FILE_DEFAULT = "";
    private static final String PUBLIC_KEYSTORE_PASSWORD = "seguridad.certificados.passwordRepositorioPublico";
    private static final String PUBLIC_KEYSTORE_PASSWORD_DEFAULT = "";
    private static final String PRIVATE_KEYSTORE_FILE = "seguridad.certificados.repositorioPrivado";
    private static final String PRIVATE_KEYSTORE_FILE_DEFAULT = "";
    private static final String PRIVATE_KEYSTORE_PASSWORD = "seguridad.certificados.passwordRepositorioPrivado";
    private static final String PRIVATE_KEYSTORE_PASSWORD_DEFAULT = "";
    private static final String PRIVATE_KEYSTORE_KEYNAME_BCB = "seguridad.certificados.keyname.bcb";
    private static final String KEYSTORE_FILE_AUTOFIRMADO = "seguridad.repositorio.certificado.autofirmado";
    private static final String KEYSTORE_FILE_AUTOFIRMADO_DEFAULT = "";
    private static final String KEYSTORE_PASSWORD_AUTOFIRMADO = "seguridad.password.certificado.autofirmado";
    private static final String KEYSTORE_PASSWORD_AUTOFIRMADO_DEFAULT = "";
    private static final String DEBUG_VALIDAR_FIRMA = "debug.seguridad.validar.firma.edv";
    private static final String DEBUG_VALIDAR_FIRMA_DEFAULT = "";

    @Inject
    public KeyFactory(MedioAmbiente medioAmbiente) {
        this.medioAmbiente = medioAmbiente;
    }

    public synchronized Certificate getCertificadoPublico(String alias) {
        String file = medioAmbiente.getValorDe(PUBLIC_KEYSTORE_FILE, PUBLIC_KEYSTORE_FILE_DEFAULT);
        String pass = medioAmbiente.getValorDe(PUBLIC_KEYSTORE_PASSWORD, PUBLIC_KEYSTORE_PASSWORD_DEFAULT);
        FileInputStream fis = null;
        try {
            KeyStore ks = KeyStore.getInstance(medioAmbiente.getValorDe(KEYSTORE_FILE_TYPE, KEYSTORE_FILE_TYPE_DEFAULT));
            fis = new FileInputStream(file);
            ks.load(fis, pass.toCharArray());

            return ks.getCertificate(alias);
        } catch (KeyStoreException e) {
            LOG.error("Error obtener el certificado publico " + alias, e);
        } catch (IOException e) {
            LOG.error("I/O Error!", e);
        } catch (GeneralSecurityException e) {
            LOG.error("Error de seguridad al obtener el certificado publico " + alias, e);
        } catch (Throwable t) {
            LOG.error("Error inesperado >> " + t.getMessage(), t);
        } finally {
            IOUtils.closeQuietly(fis);
        }
        return null;
    }

    public synchronized Certificate getMiCertificadoPublico() {
        return getMiCertificadoPublico(medioAmbiente.getValorDe(PRIVATE_KEY_ALIAS, PRIVATE_KEY_ALIAS_DEFAULT));
    }

    public synchronized Certificate getMiCertificadoPublico(String alias) {
        String file = medioAmbiente.getValorDe(PRIVATE_KEYSTORE_FILE, PRIVATE_KEYSTORE_FILE_DEFAULT);
        String pass = medioAmbiente.getValorDe(PRIVATE_KEYSTORE_PASSWORD, PRIVATE_KEYSTORE_PASSWORD_DEFAULT);
        FileInputStream fis = null;
        try {
            KeyStore ks = KeyStore.getInstance(medioAmbiente.getValorDe(KEYSTORE_FILE_TYPE, KEYSTORE_FILE_TYPE_DEFAULT));
            fis = new FileInputStream(file);
            ks.load(fis, pass.toCharArray());
            return ks.getCertificate(alias);
        } catch (KeyStoreException e) {
            LOG.error("Error obtener el certificado private " + alias, e);
        } catch (IOException e) {
            LOG.error("I/O Error!", e);
        } catch (GeneralSecurityException e) {
            LOG.error("Error de seguridad al obtener el certificado publico " + alias, e);
        } catch (Throwable t) {
            LOG.error("Error inesperado >> " + t.getMessage(), t);
        } finally {
            IOUtils.closeQuietly(fis);
        }
        return null;
    }

    public synchronized PrivateKey getLlavePrivada(String passKey) {
        return getLlavePrivada(medioAmbiente.getValorDe(PRIVATE_KEY_ALIAS, PRIVATE_KEY_ALIAS_DEFAULT), passKey);
    }

    public synchronized PrivateKey getLlavePrivada(String alias, String passKey) {
        String file = medioAmbiente.getValorDe(PRIVATE_KEYSTORE_FILE, PRIVATE_KEYSTORE_FILE_DEFAULT);
        String passRepo = medioAmbiente.getValorDe(PRIVATE_KEYSTORE_PASSWORD, PRIVATE_KEYSTORE_PASSWORD_DEFAULT);
        FileInputStream fis = null;
        try {
            KeyStore ks = KeyStore.getInstance(medioAmbiente.getValorDe(KEYSTORE_FILE_TYPE, KEYSTORE_FILE_TYPE_DEFAULT));
            fis = new FileInputStream(file);
            ks.load(fis, passRepo.toCharArray());
            return (PrivateKey) ks.getKey(alias, StringUtils.trimToEmpty(passKey).toCharArray());
        } catch (KeyStoreException e) {
            LOG.error("Error obtener el certificado publico " + alias, e);
        } catch (IOException e) {
            LOG.error("I/O Error!", e);
        } catch (GeneralSecurityException e) {
            LOG.error("Error de seguridad al obtener el certificado publico " + alias, e);
        } catch (Throwable t) {
            LOG.error("Error inesperado >> " + t.getMessage(), t);
        } finally {
            IOUtils.closeQuietly(fis);
        }
        return null;
    }

    /*
    public synchronized PrivateKey getLlavePrivadaAutofirmado(String passKey) {
    	return getLlavePrivadaAutofirmado(env.valorDe(KEY_ALIAS_AUTOFIRMADO), passKey);
        }
    */
    public synchronized PrivateKey getLlavePrivadaAutofirmado(String alias, String passKey) {
        String file = medioAmbiente.getValorDe(KEYSTORE_FILE_AUTOFIRMADO, KEYSTORE_FILE_AUTOFIRMADO_DEFAULT);
        String passRepo = StringUtils.trimToNull(medioAmbiente.getValorDe(KEYSTORE_PASSWORD_AUTOFIRMADO, KEYSTORE_PASSWORD_AUTOFIRMADO_DEFAULT));
        if (passRepo == null) {
            //Asumimos que la clave privada es la misma para el repositorio
            passRepo = passKey;
        }
        LOG.debug("Keystore Autofirmado " + KEYSTORE_FILE_AUTOFIRMADO + ": {} - " + KEYSTORE_PASSWORD_AUTOFIRMADO + ": {}", file, passRepo);

        FileInputStream fis = null;
        try {
            KeyStore ks = KeyStore.getInstance(medioAmbiente.getValorDe(KEYSTORE_FILE_TYPE, KEYSTORE_FILE_TYPE_DEFAULT));
            fis = new FileInputStream(file);
            ks.load(fis, passRepo.toCharArray());
            return (PrivateKey) ks.getKey(alias, StringUtils.trimToEmpty(passKey).toCharArray());
        } catch (KeyStoreException e) {
            LOG.error("Error obtener el certificado publico " + alias, e);
        } catch (IOException e) {
            LOG.error("I/O Error!", e);
        } catch (GeneralSecurityException e) {
            LOG.error("Error de seguridad al obtener el certificado publico " + alias, e);
        } catch (Throwable t) {
            LOG.error("Error inesperado >> " + t.getMessage(), t);
        } finally {
            IOUtils.closeQuietly(fis);
        }
        return null;
    }

    public String getContenidoProcesado(String content, String aliasLlavePrivada, String claveLlavePrivada, String aliasCertificadoPublico,
                                        String usuarioCertificado) throws SeguridadException {
        StringWriter sw = new StringWriter();
        try {
            LOG.info("content {}, aliasLlavePrivada {}, claveLlavePrivada {}, aliasCertificadoPublico {},usuarioCertificado {}", content, aliasLlavePrivada, claveLlavePrivada, aliasCertificadoPublico, usuarioCertificado);
            long total = System.currentTimeMillis();
            long t;

            InputStream inputStream = IOUtils.toInputStream(content);

            Document parsed = XMLUtilsBisa.createFromInputStream(inputStream);

            Crypto merlinSign = getCryptoSign();
            Crypto merlinEncrypt = getCryptoEncript();

            WSSecHeader secHeader = new WSSecHeader();
            Element securityHeader = secHeader.insertSecurityHeader(parsed);

            // Paso 1: Colocar el Timestamp
            t = System.currentTimeMillis();
            WSSecTimestamp timestamp = new WSSecTimestamp();
            timestamp.setTimeToLive(15);
            Document env = timestamp.build(parsed, secHeader);
            LOG.info("Tiempo en colocar Timestamp {} milis", System.currentTimeMillis() - t);

            // Paso 2: Colocar el BinaryToken
            t = System.currentTimeMillis();
            BinarySecurity bstToken = new X509Security(env);
            ((X509Security) bstToken).setX509Certificate((X509Certificate) getMiCertificadoPublico(aliasLlavePrivada));
            WSSecurityUtil.prependChildElement(securityHeader, bstToken.getElement());
            LOG.info("Tiempo en colocar BinarySecurity {} milis", System.currentTimeMillis() - t);

            // Paso 3: Colocar el UsernameToken
            t = System.currentTimeMillis();
            UsernameToken ut = new UsernameToken(false, env, null);
            ut.addNonce(env);
            ut.addCreated(false, env);
            ut.setName(usuarioCertificado);
            ut.setID("UsernameToken"); // En duro!! En realidad no hay mucho
            // problema
            WSSecurityUtil.prependChildElement(securityHeader, ut.getElement());
            LOG.info("Tiempo en colocar UsernameToken {} milis", System.currentTimeMillis() - t);

            Vector<WSEncryptionPart> partsSign = new Vector<>();
            WSEncryptionPart partSign;
            /*partSign = new WSEncryptionPart("Body", NMSPACE, "Content");
            partsSign.add(partSign);*/
            partSign = new WSEncryptionPart("Action", NMSPACE1, "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart("MessageID", NMSPACE1, "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart("ReplyTo", NMSPACE1, "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart("To", NMSPACE1, "Element");
            partsSign.add(partSign);
            /*partSign = new WSEncryptionPart("RelatesTo", NMSPACE1, "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart("FaultTo", NMSPACE1, "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart("From", NMSPACE1, "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart("Sequence", NMSPACE2, "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart("SequenceAcknowledgement", NMSPACE2, "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart("AckRequested", NMSPACE2, "Element");
            partsSign.add(partSign);
            */
            partSign = new WSEncryptionPart(timestamp.getId(), "Element");
            partsSign.add(partSign);
            partSign = new WSEncryptionPart(ut.getID(), "Element");
            partsSign.add(partSign);

            // PASO 4: Firmar
            t = System.currentTimeMillis();
            WSSecSignature signature = new WSSecSignature();
            signature.setUserInfo(aliasLlavePrivada, claveLlavePrivada);
            signature.setKeyIdentifierType(WSConstants.BST_DIRECT_REFERENCE);
            signature.setParts(partsSign);
            env = signature.build(parsed, merlinSign, secHeader);
            LOG.info("Tiempo en Firmar {} milis", System.currentTimeMillis() - t);

            Vector<WSEncryptionPart> partsEncrypt = new Vector<>();
            WSEncryptionPart partEncrypt;
            partEncrypt = new WSEncryptionPart("Body", NMSPACE, "Content");
            partsEncrypt.add(partEncrypt);
            /*partEncrypt = new WSEncryptionPart("DatosCabecera", "urn:SIN.Recaudacion.Facturacion.WsFacturacion.Repositorio.ContratoServicio", "Element");
            partsEncrypt.add(partEncrypt);*/
            partEncrypt = new WSEncryptionPart(signature.getId(), "Element");
            partsEncrypt.add(partEncrypt);

            // PASO 5 Encriptar
            t = System.currentTimeMillis();
            WSSecEncrypt encrypt = new WSSecEncrypt();
            encrypt.setUserInfo(aliasCertificadoPublico);
            encrypt.setKeyIdentifierType(WSConstants.THUMBPRINT_IDENTIFIER);
            encrypt.setParts(partsEncrypt);
            env = encrypt.build(env, merlinEncrypt, secHeader);
            LOG.info("Tiempo en Encriptar {} milis", System.currentTimeMillis() - t);

            LOG.info("Tiempo de procesamiento total del  xml {} milis", (System.currentTimeMillis() - total));

            TransformerFactory.newInstance().newTransformer().transform(new DOMSource(env), new StreamResult(sw));
        } catch (CredentialException e) {
            String msg = "Problemas de seguridad en los JKS";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (IOException e) {
            String msg = "Algo no se pudo leer o escribir. I/O Error!!";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (TransformerException e) {
            String msg = "No se pudo transformar el xml procesado";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (ParserConfigurationException e) {
            String msg = "No se pudo convertir a Document";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (SAXException e) {
            String msg = "No se pudo convertir a Document";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (Exception e) {
            String msg = "Error inesperado al procesar el documento para el envio al SIN";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        }

        return sw.toString();
    }

    public String getContenidoFirmado(String content, String aliasLlavePrivada, String claveLlavePrivada) throws SeguridadException {
        LOG.debug("Procesando el siguiente contenido\n{}", content);
        StringWriter sw = new StringWriter();
        try {
            long total = System.currentTimeMillis();
            long t;

            t = System.currentTimeMillis();
            // Convertimos el XMl a un arbol de DOM
            InputStream inputStream = IOUtils.toInputStream(content);
            Document parsed = XMLUtilsBisa.createFromInputStream(inputStream);

            // Obtenemos el keystore privado
            Crypto merlinSign = getCryptoSign();

            // Insertamos en header un tag de seguridad
            WSSecHeader secHeader = new WSSecHeader();
            secHeader.insertSecurityHeader(parsed);

            // Configuramos la firma
            WSSecSignature signature = new WSSecSignature();
            signature.setUserInfo(aliasLlavePrivada, claveLlavePrivada);
            signature.setKeyIdentifierType(WSConstants.X509_KEY_IDENTIFIER); //BST_DIRECT_REFERENCE
            //signature.setParts(partsSign);

            // Firmamos con los datos del keystore privado en el header del XML
            Document env = signature.build(parsed, merlinSign, secHeader);

            LOG.debug("Tiempo de procesamiento total del  xml {} milis", (System.currentTimeMillis() - total));

            // transformamos del arbol de DOM a un texto plano XML
            TransformerFactory.newInstance().newTransformer().transform(new DOMSource(env), new StreamResult(sw));
        } catch (CredentialException e) {
            String msg = "Problemas de seguridad en los JKS";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (IOException e) {
            String msg = "Algo no se pudo leer o escribir. I/O Error!!";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (TransformerException e) {
            String msg = "No se pudo transformar el xml procesado";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (ParserConfigurationException e) {
            String msg = "No se pudo convertir a Document";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (SAXException e) {
            String msg = "No se pudo convertir a Document";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (Exception e) {
            String msg = "Error inesperado al procesar el documento para el envio al SIN";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        }

        return sw.toString();
    }

    static {
        org.apache.xml.security.Init.init();
    }

    /*
    public String getContenidoFirmadoSegunACH(Element mensaje1A, String aliasLlavePrivada, String claveLlavePrivada, Long correlativo) throws SeguridadException {
    	String keyName = "BIS";
    	return getContenidoFirmadoSegunACH(mensaje1A, aliasLlavePrivada, claveLlavePrivada, correlativo.toString(), keyName);
    }*/
    /*
    public String getContenidoFirmadoSegunACH(Element mensaje1A, String aliasLlavePrivada, String claveLlavePrivada, String correlativo) throws SeguridadException {

    	String keyName = (String) ObjectUtils.defaultIfNull(StringUtils.trimToNull(env.valorDe(PRIVATE_KEYSTORE_KEYNAME_BCB)),"BIS");
    	return getContenidoFirmadoSegunACH(mensaje1A, aliasLlavePrivada, claveLlavePrivada, correlativo.toString(), keyName);
    }*/

    public String getContenidoFirmadoSegunACH(Element mensaje1A, String aliasLlavePrivada, String claveLlavePrivada, Long correlativo) throws SeguridadException {

        PrivateKey privateKey = getLlavePrivada(aliasLlavePrivada, claveLlavePrivada);

        Element m = mensaje1A;
        Document doc = m.getOwnerDocument();
        String algoritmo = XMLSignature.ALGO_ID_SIGNATURE_RSA;

        if ((privateKey.getAlgorithm()).compareTo("DSA") == 0) {
            algoritmo = XMLSignature.ALGO_ID_SIGNATURE_DSA;
        }
        LOG.debug("Signer:El algoritmo utilizado para la firma: " + algoritmo);

        String targetURI = "";
        String id = "";

        String correlativoYFecha = correlativo == null ? "" : correlativo.toString();
        String tu = correlativoYFecha + " - " + (new Date());
        id = tu;
        targetURI = tu;

        XMLSignature sig = null;
        try {
            sig = new XMLSignature(doc, targetURI, algoritmo);
        } catch (XMLSecurityException e1) {
            LOG.error("Signer:Problemas (1) con XMLSignature con " +
                    "[doc=" + doc + " targetURI=" + targetURI + " algoritmo=" + algoritmo + "]", e1);
            throw new SeguridadException(e1.getMessage());
        }

        doc.appendChild(sig.getElement());

        ObjectContainer obj = new ObjectContainer(doc);
        Node fragment = doc.createDocumentFragment();

        fragment.appendChild(m);

        obj.appendChild(fragment);
        obj.setId(id);
        try {
            sig.appendObject(obj);
        } catch (XMLSignatureException e1) {
            LOG.warn("Signer:datos para el XMLSignature " +
                    "[doc=" + doc + " targetURI=" + targetURI + " algoritmo=" + algoritmo + "]", e1);
            LOG.error("Signer:Problemas (2) Error de XMLSignatureException con " +
                    "[obj=" + obj + "]", e1);
            throw new SeguridadException(e1.getMessage());
        }

        StringWriter strw;
        Transforms transforms = new Transforms(doc);
        try {
            transforms.addTransform(Transforms.TRANSFORM_C14N_OMIT_COMMENTS);
            sig.addDocument("#" + id, transforms, Constants.ALGO_ID_DIGEST_SHA1);
            sig.sign(privateKey);
        } catch (TransformationException e1) {
            LOG.warn("Signer:datos para el XMLSignature " +
                    "[doc=" + doc + " targetURI=" + targetURI + " algoritmo=" + algoritmo + "], ", e1);
            LOG.error("Signer:Problemas (3) Error de TransformationException, " +
                    "tambien estamos tratando de omitir los COMMENTS", e1);
            throw new SeguridadException(e1.getMessage());
        } catch (XMLSignatureException e1) {
            LOG.warn("Signer:datos para el XMLSignature " +
                    "[doc=" + doc + " targetURI=" + targetURI + " algoritmo=" + algoritmo + "]", e1);
            LOG.error("Signer:Problemas (4) Error de XMLSignatureException con " +
                    "[obj=" + obj + "]", e1);
            throw new SeguridadException(e1.getMessage());
        }
        sig.getKeyInfo().addKeyName("BIS");


        strw = new StringWriter();
        try {
            Source source = new DOMSource(doc);
            Result result = new StreamResult(strw);
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            xformer.transform(source, result);
        } catch (javax.xml.transform.TransformerConfigurationException e) {
            LOG.warn("Signer:datos para el XMLSignature " +
                    "[doc=" + doc + " targetURI=" + targetURI + " algoritmo=" + algoritmo + "]", e);
            LOG.error("Signer:Problemas (5) Error de TransformerConfigurationException con " +
                    "[obj=" + obj + "]", e);
            throw new SeguridadException(e.getMessage());
        } catch (javax.xml.transform.TransformerException e) {
            LOG.warn("Signer:datos para el XMLSignature " +
                    "[doc=" + doc + " targetURI=" + targetURI + " algoritmo=" + algoritmo + "]", e);
            LOG.error("Signer:Problemas (6) Error de TransformerException con la " +
                    "transformacion del mensaje, de Doc a String " +
                    "[obj=" + obj + "]", e);
            throw new SeguridadException(e.getMessage());
        }
        String string = strw.toString();
        return string;

    }

    /*
    private String getRepresentacionXML(Node doc)
            throws TransformerFactoryConfigurationError {
        StringWriter strw = new StringWriter();
        try {
            Source source = new DOMSource(doc);
            Result result = new StreamResult(strw);
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.setOutputProperty("encoding", "ISO-8859-1");
            xformer.transform(source, result);
        } catch (javax.xml.transform.TransformerConfigurationException e) {
            LOG.error("Signer:Error en la configuracion del transformador",e);
        } catch (javax.xml.transform.TransformerException e) {
            LOG.error("Signer:Error en la transformacion del mensaje, de Doc a String ",e);
        }
        return strw.toString();
    }
    private String getRepresentacionXML(Document doc)
            throws TransformerFactoryConfigurationError {
        StringWriter strw = new StringWriter();
        try {
            Source source = new DOMSource(doc);
            Result result = new StreamResult(strw);
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.setOutputProperty("encoding", "ISO-8859-1");
            xformer.transform(source, result);
        } catch (javax.xml.transform.TransformerConfigurationException e) {
            LOG.error("Signer:Error en la configuracion del transformador",e);
        } catch (javax.xml.transform.TransformerException e) {
            LOG.error("Signer:Error en la transformacion del mensaje, de Doc a String ",e);
        }
        return strw.toString();
    }
    */

    /*
    public FacturarResponse procesarSeguridadFacturar(String xml, final String claveLlavePrivada) throws SeguridadException {
        FacturarResponse facturarResponse = null;
        try {
            Document respuesta = procesarSeguridad(xml, claveLlavePrivada);

            NodeList elementsByTagNameNS = respuesta.getElementsByTagNameNS(
                    "urn:impuestos-gob-bo:newton:facturacionelectronicaservice:messages:v1",
                    "facturarResponse");
            Element element = (Element) elementsByTagNameNS.item(0);

            JAXBContext context = JAXBContext.newInstance(FacturarResponse.class, ArrayOfFallo.class, Fallo.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            facturarResponse = (FacturarResponse) unmarshaller.unmarshal(element);

            if (facturarResponse.getErrores() == null) {
                LOG.debug(XMLUtilsBisa.documentToPrettyString(respuesta));
                LOG.info("Factura electronica satisfactoriamente solicitada {}", facturarResponse.getETicket());
            } else {
                List<Fallo> fallos = facturarResponse.getErrores().getFallo();
                for (Fallo fallo : fallos) {
                    LOG.error("Error: {}\nDescripcion: {}", fallo.getCodigo(), fallo.getDescripcion());
                }
            }
        } catch (SeguridadException e) {
            throw e;
        } catch (TransformerException e) {
            String msg = "No se pudo transformar el xml procesado";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (JAXBException e) {
            String msg = "Error de JAX-B al intentar transformar el xml de respuesta al objeto requerido";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (Exception e) {
            String msg = "Error inesperado al procesar la factura";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        }
        return facturarResponse;
    }
*/
    /*
    public RecuperarFacturasResponse procesarSeguridadRecuperarFactura(String xml, final String claveLlavePrivada) throws SeguridadException {
        RecuperarFacturasResponse recuperarFacturasResponse = null;
        try {
            Document respuesta = procesarSeguridad(xml, claveLlavePrivada);

            NodeList elementsByTagNameNS = respuesta.getElementsByTagNameNS(
                    "urn:impuestos-gob-bo:newton:facturacionelectronicaservice:messages:v1",
                    "recuperarFacturasResponse");
            Element element = (Element) elementsByTagNameNS.item(0);

            JAXBContext context = JAXBContext.newInstance(RecuperarFacturasResponse.class, ArrayOfFacturaRespuesta.class, ArrayOfFallo.class, FacturaRespuesta.class, Fallo.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            recuperarFacturasResponse = (RecuperarFacturasResponse) unmarshaller.unmarshal(element);

            if (recuperarFacturasResponse.getErrores() == null) {
                LOG.debug(XMLUtilsBisa.documentToPrettyString(respuesta));
                LOG.debug("Factura electronica recuperada satisfactoriamente {}", recuperarFacturasResponse.getETicket());
            } else {
                List<Fallo> fallos = recuperarFacturasResponse.getErrores().getFallo();
                for (Fallo fallo : fallos) {
                    LOG.error("Error: {}\nDescripcion: {}", fallo.getCodigo(), fallo.getDescripcion());
                }
            }
        } catch (SeguridadException e) {
            throw e;
        } catch (TransformerException e) {
            String msg = "No se pudo transformar el xml procesado";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (JAXBException e) {
            String msg = "Error de JAX-B al intentar transformar el xml de respuesta al objeto requerido";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (Exception e) {
            String msg = "Error inesperado al recuperar la factura";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        }
        return recuperarFacturasResponse;
    }
*/
    private Document procesarSeguridad(String xml, final String claveLlavePrivada) throws SeguridadException {
        try {
            LOG.info("Comenzando proceso de verificacion de respuesta de solicitud al SIN");
            Document respuesta = XMLUtilsBisa.createFromString(xml);

            Crypto merlinSign = getCryptoSign();
            Crypto merlinEncrypt = getCryptoEncript();

            WSSecurityEngine secEngine = WSSecurityEngine.getInstance();
            //XXX: Esto devuelve un vector deberiamos hacer algo mas?
            secEngine.processSecurityHeader(respuesta, null, callbacks -> {
                for (int i = 0; i < callbacks.length; i++) {
                    if (callbacks[i] instanceof WSPasswordCallback) {
                        ((WSPasswordCallback) callbacks[i]).setPassword(claveLlavePrivada);
                    }
                }
            }, merlinEncrypt, merlinSign);
            return respuesta;
        } catch (IOException e) {
            String msg = "Algo no se pudo leer o escribir. I/O Error!!";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (ParserConfigurationException e) {
            String msg = "No se pudo convertir a Document";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (SAXException e) {
            String msg = "No se pudo convertir a Document";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (CredentialException e) {
            String msg = "Problemas de seguridad en los JKS";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        } catch (Exception e) {
            String msg = "Error inesperado al procesar la respuesta encriptada del SIN";
            LOG.error(msg, e);
            throw new SeguridadException(msg);
        }
    }

    /**
     * Retorna JKS con llave y certificado privado (del BISA). Para firmar XML
     *
     * @return Crypto
     * @throws IOException
     * @throws CredentialException
     * @throws Exception
     */
    private Crypto getCryptoSign() throws CredentialException, IOException {
        Properties p = new Properties();
        p.setProperty("org.apache.ws.security.crypto.merlin.file", medioAmbiente.getValorDe(PRIVATE_KEYSTORE_FILE, PRIVATE_KEYSTORE_FILE_DEFAULT));
        LOG.info("merlin.file {}", medioAmbiente.getValorDe(PRIVATE_KEYSTORE_FILE, PRIVATE_KEYSTORE_FILE_DEFAULT));
        p.setProperty("org.apache.ws.security.crypto.merlin.keystore.password", medioAmbiente.getValorDe(PRIVATE_KEYSTORE_PASSWORD, PRIVATE_KEYSTORE_PASSWORD_DEFAULT));
        LOG.info("keystore.password {}", medioAmbiente.getValorDe(PRIVATE_KEYSTORE_PASSWORD, PRIVATE_KEYSTORE_PASSWORD_DEFAULT));
        return new Merlin(p);
    }

    /**
     * Retorna JKS con certificado publico (del SIN). Para encriptar el XML
     *
     * @return Crypto
     * @throws IOException
     * @throws CredentialException
     * @throws Exception
     */
    private Crypto getCryptoEncript() throws CredentialException, IOException {
        Properties p = new Properties();
        p.setProperty("org.apache.ws.security.crypto.merlin.file", medioAmbiente.getValorDe(PUBLIC_KEYSTORE_FILE, PUBLIC_KEYSTORE_FILE_DEFAULT));
        LOG.info(" crypto merlin.file{}", medioAmbiente.getValorDe(PUBLIC_KEYSTORE_FILE, PUBLIC_KEYSTORE_FILE_DEFAULT));
        p.setProperty("org.apache.ws.security.crypto.merlin.keystore.password", medioAmbiente.getValorDe(PUBLIC_KEYSTORE_PASSWORD, PUBLIC_KEYSTORE_PASSWORD_DEFAULT));
        LOG.info(" crypto merlin.password{}", medioAmbiente.getValorDe(PUBLIC_KEYSTORE_PASSWORD, PUBLIC_KEYSTORE_PASSWORD_DEFAULT));
        return new Merlin(p);
    }

    /* ===============================================================================================
     * Implementacion para verificar firmas
     *
     * =============================================================================================== */
    public Par<Integer, String> validarFirma(String mensajeXML) {
        LOG.debug("Validando firma el XML [\n" + mensajeXML + "\n]");
        if (medioAmbiente.existe(DEBUG_VALIDAR_FIRMA)) {
            String valor = StringUtils.trimToEmpty(StringUtils.upperCase(medioAmbiente.getValorDe(DEBUG_VALIDAR_FIRMA, DEBUG_VALIDAR_FIRMA_DEFAULT)));
            if ("SI".equals(valor)) {
                LOG.warn("Validamos la firma por encontrarse la variable de medio ambiente [{}] con valor '{}' = 'SI'",
                        DEBUG_VALIDAR_FIRMA, medioAmbiente.getValorDe(DEBUG_VALIDAR_FIRMA, DEBUG_VALIDAR_FIRMA_DEFAULT));
            } else {
                LOG.warn("No validamos la firma por encontrarse la variable de medio ambiente [{}] con valor '{}' <> 'SI'",
                        DEBUG_VALIDAR_FIRMA, medioAmbiente.getValorDe(DEBUG_VALIDAR_FIRMA, DEBUG_VALIDAR_FIRMA_DEFAULT));
                return Par.de(0, "");
            }
        }

        final String end = " en el XML: \n [" + mensajeXML + "]";
        String msg = " en el XML: \n [" + mensajeXML + "]";
        if (StringUtils.isEmpty(StringUtils.trimToEmpty(mensajeXML))) {
            msg = "No encontramos contenido en el mensaje XML para validar la firma";
            LOG.error(msg + end);
            return Par.de(1, msg);
        }

        Document doc = XMLUtilsBisa.xmlToDocument(mensajeXML, "EDV");
        if (doc == null) {
            msg = "No podemos parsear el mensaje como un XML valido para verificar la firma";
            LOG.error(msg + end);
            return Par.de(2, msg);
        }

        Element nscontext = XMLUtils.createDSctx(doc, "ds", Constants.SignatureSpecNS);
        NodeList signatures;
        try {
            signatures = XPathAPI.selectNodeList(doc, "//ds:Signature", nscontext);
        } catch (TransformerException e1) {
            msg = "No podemos encontrar el tag <ds> para validar la firma";
            LOG.error(msg + end, e1);
            return Par.de(3, msg);
        }

        if (signatures.getLength() < 1) {
            msg = "El mensaje no contiene firma alguna";
            LOG.error(msg + end);
            return Par.de(4, msg);
        }

        for (int indexS = 0; indexS < signatures.getLength(); indexS++) {
            Element sigElement = (Element) signatures.item(indexS);
            XMLSignature signature;
            try {
                signature = new XMLSignature(sigElement, "");
            } catch (XMLSignatureException e1) {
                msg = "No podemos construir un 'XMLSignature' a partir del tag ds:Signature XMLSignatureException";
                LOG.error(msg + end, e1);
                return Par.de(5, msg);
            } catch (XMLSecurityException e1) {
                msg = "No podemos construir un 'XMLSignature' a partir del tag ds:Signature XMLSecurityException";
                LOG.error(msg + end, e1);
                return Par.de(6, msg);
            }
            String keyName;
            try {
                keyName = signature.getKeyInfo().itemKeyName(0).getKeyName();
            } catch (XMLSecurityException e1) {
                msg = "No podemos obtener el keyName del tag ds:Signature XMLSecurityException";
                LOG.error(msg + end, e1);
                return Par.de(7, msg);
            }

            Certificate certificate = getCertificadoPublico(keyName);
            if (certificate == null) {
                msg = "No se puede encontrar el certificado para el KeyName [" + keyName + "]";
                LOG.error(msg + end);
                return Par.de(8, msg);
            }

            PublicKey publicKey = certificate.getPublicKey();
            if (publicKey == null) {
                msg = "No se puede encontrar la llave publica del certificado para el KeyName [" + keyName + "]";
                LOG.error(msg + end);
                return Par.de(9, msg);
            }

            try {
                if (!signature.checkSignatureValue(publicKey)) {
                    msg = "No se puede validar la firma para el KeyName [" + keyName + "]";
                    LOG.error(msg + end);
                    return Par.de(10, msg);
                }
            } catch (XMLSignatureException e) {
                msg = "No podemos validar la firma para el KeyName [" + keyName + "] utilizando un XMLSignature";
                LOG.error(msg + end, e);
                return Par.de(11, msg);
            }

            LOG.debug("Firma valida para el Keyname [{}], continuamos verificando por si existen más firmas ", keyName);
        }

        return Par.de(0, "");
    }

}
