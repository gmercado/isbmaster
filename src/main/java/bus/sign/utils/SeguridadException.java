package bus.sign.utils;

/**
 * @author by rsalvatierra on 19/12/2016.
 */
public class SeguridadException extends Exception {

    private static final long serialVersionUID = -7395150998447539318L;

    private String mensaje;

    public SeguridadException(String mensaje) {
        super();
        this.mensaje = mensaje;
    }

    public SeguridadException(String mensaje, Throwable t) {
        super(t.getMessage(), t);
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

}
