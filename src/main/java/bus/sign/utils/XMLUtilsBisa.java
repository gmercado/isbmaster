package bus.sign.utils;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

/**
 * @author by rsalvatierra on 19/12/2016.
 */
public class XMLUtilsBisa implements Serializable {

    private static final long serialVersionUID = -4805872378793517896L;
    private static final Logger LOGGER = LoggerFactory.getLogger(XMLUtilsBisa.class);

    public static Document createFromString(String xml)
            throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(xml)));

    }

    public static Document createFromInputStream(InputStream xml)
            throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(xml);

    }

    public static String documentToPrettyString(Document doc)
            throws TransformerException {
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();

        // Para pretty print
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "2");

        Source source = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        Result result = new StreamResult(writer);
        transformer.transform(source, result);
        return writer.toString();
    }

    public static String getContentOfTag(Document document, String tagname) {
        NodeList nodos = document.getElementsByTagName(tagname);
        if (nodos.getLength() < 1) {
            return "";
        }
        return nodos.item(0).getTextContent();
    }

    /**
     * Proporciona el tag completo y su contenido a partir de un {@link org.w3c.dom.Document Document}
     * <p/>
     * <code> <blockquote> &lt;tagname>contenido&lt;/tagname> </blockquote> </code>
     *
     * @param document
     * @param tagname
     * @return
     */
    public static String getTag(Document document, String tagname) {
        if (document == null) {
            return "";
        }
        NodeList nodos = document.getElementsByTagName(tagname);
        if (nodos.getLength() < 1) {
            return "";
        }
        return nodeToString(nodos.item(0));
    }

    /**
     * Proporciona el tag completo y su contenido a partir de un <code> XML </code>
     * <p/>
     * <code> <blockquote> &lt;tagname>contenido&lt;/tagname> </blockquote> </code>
     *
     * @param xml
     * @param tagname
     * @return
     */
    public static String getTag(String xml, String tagname, String pista) {
        Document docRes = XMLUtilsBisa.xmlToDocument(xml, pista);
        return getTag(docRes, tagname);
    }

    /**
     * Proporciona el tag completo y su contenido a partir de un {@link org.w3c.dom.Node Node}
     * <p/>
     * <code> <blockquote> &lt;tagname>contenido&lt;/tagname> </blockquote> </code>
     *
     * @param nodo
     * @param tagname
     * @return
     */
    public static String getTag(Node nodo, String tagname, String pista) {
        return getTag(XMLUtilsBisa.nodeToString(nodo), tagname, pista);
    }


    /**
     * Convertimos un {@link org.w3c.dom.Node Node} a su representación en <code>XML</code>
     *
     * @param node
     * @return
     */
    public static String nodeToString(Node node) {
        StringWriter sw = new StringWriter();
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException te) {
            return null;
        }
        return sw.toString();
    }

    public static String documentToString(Document documento) {
        StringWriter sw = new StringWriter();
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.transform(new DOMSource(documento), new StreamResult(sw));
        } catch (TransformerException te) {
            return null;
        }
        return sw.toString();
    }


    public static String elementToString(Element elemento) {
        StringWriter strw = new StringWriter();
        try {
            Source source = new DOMSource(elemento);
            Result result = new StreamResult(strw);
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            //xformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            //xformer.setOutputProperty("encoding", "ISO-8859-1");
            xformer.transform(source, result);
        } catch (javax.xml.transform.TransformerConfigurationException e) {
            LOGGER.warn("Error <TransformerConfigurationException> [element=" + elemento + "]", e);
            return null;
        } catch (javax.xml.transform.TransformerException e) {
            LOGGER.warn("Error <TransformerException> [element=" + elemento + "]", e);
            return null;
        }
        return strw.toString();
    }

    public static Document xmlToDocument(String mensajeXML, String marca) {
        InputStream inputStream;
        inputStream = IOUtils.toInputStream(mensajeXML);
        Document parsed = null;
        try {
            parsed = XMLUtilsBisa.createFromInputStream(inputStream);
        } catch (ParserConfigurationException e) {
            LOGGER.warn(marca + "-ParserConfigurationException mensaje a convertir [{}]", mensajeXML);
            LOGGER.error(marca + "-ParserConfigurationException", e);
            return null;
        } catch (SAXException e) {
            LOGGER.warn(marca + "-ParserConfigurationException mensaje a convertir [{}]", mensajeXML);
            LOGGER.error(marca + "-SAXException", e);
            return null;
        } catch (IOException e) {
            LOGGER.warn(marca + "-ParserConfigurationException mensaje a convertir [{}]", mensajeXML);
            LOGGER.error(marca + "-IOException", e);
            return null;
        }
        return parsed;
    }


}
