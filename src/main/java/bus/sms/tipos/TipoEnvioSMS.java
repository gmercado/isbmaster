/*
 * Copyright 2013 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.sms.tipos;

/**
 * Tipos de envio SMS para los Usuarios o Clientes Web.
 * @author Roger Chura
 * @since  1.0
 */ 
public enum TipoEnvioSMS {

	// Nota: Nombre del tipo de envio longitud maxima de 12 caracteres
    CODIGO("Codigo de Confirmacion"),
    CLAVE("Clave Temporal"),
    MENSAJE("Mensaje de Notificacion");
    
    private final String descripcion;
    
    TipoEnvioSMS(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
    
}
