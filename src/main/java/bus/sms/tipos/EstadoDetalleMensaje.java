package bus.sms.tipos;

/**
 * @author by rchura on 02-12-15.
 */
public enum EstadoDetalleMensaje {

    ING("Ingresado"),
    PRC("Procesado"),
    ANU("Anulado"),
    NEN("No enviado"),
    SRE("Sin respuesta"),
    EXC("Excluido");

    private String descripcion;

    EstadoDetalleMensaje(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}

