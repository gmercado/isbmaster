/*
 * Copyright 2013 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.sms.tipos;

/**
 * Estados de Mensajes para los SMS enviados 
 * @author Roger Chura
 * @since  1.0
 */ 
public enum EstadoEncabezadoMensaje {

	// Nota: Nombre del estado del mensaje longitud maxima de 3 caracteres
    PEJ("Por ejecutarse"),
    INA("Inactivo"),
    ACT("Activo"),
    COM("Completado"),
    PER("Permanente"),
    ANU("Anulado")
    ;

    private final String descripcion;

    EstadoEncabezadoMensaje(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
    
}
