package bus.sms.dao;

import bus.consumoweb.yellowpepper.RespuestaYellowPepper;
import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.sms.entities.DetalleMensaje;
import bus.sms.entities.EncabezadoMensaje;
import bus.sms.tipos.EstadoDetalleMensaje;
import bus.sms.tipos.TipoMensaje;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.Arrays;
import java.util.Date;

/**
 * @author by rchura on 02-12-15.
 */
public class DetalleMensajeDao extends DaoImpl<DetalleMensaje, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DetalleMensajeDao.class);

    @Inject
    public DetalleMensajeDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Long> countPath(Root<DetalleMensaje> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<DetalleMensaje> p) {
        Path<String> cliente = p.get("numeroCliente");
        Path<String> celular = p.get("numeroCelular");
        return Arrays.asList(cliente, celular);
    }


    public DetalleMensaje registrar(DetalleMensaje detalleMensaje, String usuario) {
        detalleMensaje.setUsuarioCreador(usuario);
        detalleMensaje.setFechaCreacion(new Date());
        detalleMensaje.setUsuarioModificador(usuario);
        detalleMensaje.setFechaModificacion(new Date());
        return persist(detalleMensaje);
    }

    public DetalleMensaje actualizar(DetalleMensaje detalleMensaje, String usuario) {
        detalleMensaje.setUsuarioModificador(usuario);
        detalleMensaje.setFechaModificacion(new Date());
        return merge(detalleMensaje);
    }


    public DetalleMensaje registrarMensaje(EncabezadoMensaje encabezado, String cliente, String celular, String usuario,
                                           TipoMensaje tipoMensaje, Date fecha,
                                           EstadoDetalleMensaje estadoDetalle, String aplicacion, String servicio) {
        if (encabezado == null) {
            LOGGER.error("Error: No se tiene registro del encabezado del mensaje.");
            return null;
        }

        LOGGER.debug("Iniciando registro de mensaje enviado...");
        // Aqui registrar el objeto de ENVIOS DE MENSAJES, CLAVES <TIP05>
        DetalleMensaje sms = new DetalleMensaje();
        sms.setNumeroCliente(cliente);
        sms.setNumeroCelular(celular);
        sms.setEncabezadoMensaje(encabezado);
        sms.setTipoMensaje(tipoMensaje);
        sms.setAplicacion(aplicacion);
        sms.setServicio(servicio);
        sms.setEstado(estadoDetalle);
        sms.setFechaProceso(fecha);
        try {
            sms = registrar(sms, usuario);
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al crear un registro de tipo <DetalleMensaje>. registrarNotificacion()<Exception>", e);
            return null;
        }
        return sms;
    }


    public DetalleMensaje registrarNotificacion(EncabezadoMensaje encabezado, String cliente, String celular, String usuario,
                                                TipoMensaje tipoMensaje, RespuestaYellowPepper respuestaYellowPepper, Date fecha,
                                                EstadoDetalleMensaje estadoDetalle, String aplicacion, String servicio) {

        if (encabezado == null) {
            LOGGER.error("Error: No se tiene registro del encabezado del mensaje.");
            return null;
        }

        LOGGER.debug("Iniciando registro de mensaje enviado...");
        // Aqui registrar el objeto de ENVIOS DE MENSAJES, CLAVES <TIP05>
        DetalleMensaje sms = registrarMensaje(encabezado, cliente, celular, usuario,
                tipoMensaje, null, null, aplicacion, servicio);
        if (sms == null) {
            LOGGER.error("Error: No se tiene registro del detalle del mensaje.");
            return null;
        }

        if (respuestaYellowPepper != null && respuestaYellowPepper.getSendShortMessageResult() != null) {
            sms.setEstado(EstadoDetalleMensaje.PRC); // Mensaje procesado
            sms.setRespuestaYellowPepper(respuestaYellowPepper.getSendShortMessageResult());
        } else {
            sms.setEstado(estadoDetalle);
        }

        //Fecha del proceso, en base a los estados
        if (EstadoDetalleMensaje.PRC.equals(sms.getEstado()) ||
                EstadoDetalleMensaje.SRE.equals(sms.getEstado())) {
            sms.setFechaProceso(new Date());
        } else {
            sms.setFechaProceso(fecha);
        }

        try {
            sms = actualizar(sms, usuario);
        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error inesperado al crear un registro de tipo <DetalleMensaje>. registrarNotificacion()<Exception>", e);
            return null;
        }
        LOGGER.debug("Finalizando registro de mensaje enviado...");

        return sms;
    }


}
