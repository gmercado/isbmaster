package bus.sms.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.sms.entities.EncabezadoMensaje;
import bus.sms.tipos.EstadoEncabezadoMensaje;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;

/**
 * @author by rchura on 02-12-15.
 */
public class EncabezadoMensajeDao extends DaoImpl<EncabezadoMensaje, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(EncabezadoMensajeDao.class);

    @Inject
    public EncabezadoMensajeDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Long> countPath(Root<EncabezadoMensaje> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<EncabezadoMensaje> p) {
        Path<String> titulo = p.get("titulo");
        return Collections.singletonList(titulo);
    }

    public EncabezadoMensaje registrar(EncabezadoMensaje encabezadoMensaje, String usuario) {
        encabezadoMensaje.setUsuarioCreador(usuario);
        encabezadoMensaje.setFechaCreacion(new Date());
        encabezadoMensaje.setUsuarioModificador(usuario);
        encabezadoMensaje.setFechaModificacion(new Date());
        return persist(encabezadoMensaje);
    }

    public EncabezadoMensaje actualizar(EncabezadoMensaje encabezadoMensaje, String usuario) {
        encabezadoMensaje.setUsuarioModificador(usuario);
        encabezadoMensaje.setFechaModificacion(new Date());
        return merge(encabezadoMensaje);
    }

    public EncabezadoMensaje getByTituloEstado(String titulo, EstadoEncabezadoMensaje estado) {
        LOGGER.debug("Obteniendo registro de Encabezado Mensaje con el Titulo:[{}] y Estado:[{}].", titulo, estado.getDescripcion());
        return doWithTransaction(
                (entityManager, t) -> {
                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
                    CriteriaQuery<EncabezadoMensaje> q = cb.createQuery(EncabezadoMensaje.class);
                    Root<EncabezadoMensaje> p = q.from(EncabezadoMensaje.class);

                    LinkedList<Predicate> predicatesAnd = new LinkedList<>();
                    predicatesAnd.add(cb.equal(p.get("titulo"), titulo));
                    predicatesAnd.add(cb.equal(p.get("estado"), estado));

                    q.where(cb.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()])));
                    TypedQuery<EncabezadoMensaje> query = entityManager.createQuery(q);

                    EncabezadoMensaje encabezado = null;
                    try {
                        encabezado = query.getSingleResult();
                    } catch (NoResultException e) {
                        LOGGER.error("No se encontro registro de Encabezado Mensaje con el Titulo:[{}] y Estado:[{}].", titulo, estado.getDescripcion());
                        encabezado = null;
                    }
                    return encabezado;
                }
        );
    }
}
