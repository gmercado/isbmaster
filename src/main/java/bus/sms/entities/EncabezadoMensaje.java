package bus.sms.entities;

import bus.database.model.EntityBase;
import bus.sms.tipos.EstadoEncabezadoMensaje;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import javax.persistence.*;
import java.util.Date;

/**
 * @author by rchura on 02-12-15.
 */
@Entity
@Table(name = "ISBP61")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S61FECA", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S61USUA")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S61FECM")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S61USUM"))
})
public class EncabezadoMensaje extends EntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "S61ID")
    private Long id;

    @Column(name = "S61TITULO", length = 50)
    private String titulo;

    @Column(name = "S61MENSAJE", length = 150)
    private String mensaje;

    @Column(name = "S61ESTADO", columnDefinition = "CHAR(3)")
    @Enumerated(EnumType.STRING)
    private EstadoEncabezadoMensaje estado;

    @Column(name = "S61FECPRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaProceso;

    public EncabezadoMensaje() {
    }

    public EncabezadoMensaje(String titulo, String mensaje, EstadoEncabezadoMensaje estado, Date fechaProceso) {
        setTitulo(titulo);
        setMensaje(mensaje);
        setEstado(estado);
        setFechaProceso(fechaProceso);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EstadoEncabezadoMensaje getEstado() {
        return estado;
    }

    public void setEstado(EstadoEncabezadoMensaje estado) {
        this.estado = estado;
    }

    public Date getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(Date fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }


    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}

