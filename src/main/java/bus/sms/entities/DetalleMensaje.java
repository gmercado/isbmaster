package bus.sms.entities;

import bus.database.model.EntityBase;
import bus.sms.tipos.EstadoDetalleMensaje;
import bus.sms.tipos.TipoMensaje;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import javax.persistence.*;
import java.text.MessageFormat;
import java.util.Date;

/**
 * Created by rchura on 02-12-15.
 */
@Entity
@Table(name="ISBP62")
@AttributeOverrides({
        @AttributeOverride(name = "fechaCreacion", column = @Column(name = "S62FECA", nullable = false)),
        @AttributeOverride(name = "usuarioCreador", column = @Column(name = "S62USUA")),
        @AttributeOverride(name = "fechaModificacion", column = @Column(name = "S62FECM")),
        @AttributeOverride(name = "usuarioModificador", column = @Column(name = "S62USUM"))
})
public class DetalleMensaje extends EntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="S62ID" )
    private Long id;

    @Column(name = "S62NROCLI", length = 10)
    private String numeroCliente;

    @Column(name = "S62NUMCEL", length = 11)
    private String numeroCelular;

    @Column(name = "S62PARAM0", length = 50)
    private String param0;

    @Column(name = "S62PARAM1", length = 50)
    private String param1;

    @Column(name = "S62PARAM2", length = 50)
    private String param2;

    @Column(name = "S62PARAM3", length = 50)
    private String param3;

    @Column(name = "S62PARAM4", length = 50)
    private String param4;

    @Column(name = "S62PARAM5", length = 50)
    private String param5;

    @Enumerated(EnumType.STRING)
    @Column(name = "S62ESTADO", columnDefinition = "CHAR(3)")
    private EstadoDetalleMensaje estado;

    @Column(name = "S62FECPRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaProceso;

    @Column(name = "S62RESPSTA", length = 30)
    private String respuestaYellowPepper;

    @Enumerated(EnumType.STRING)
    @Column(name = "S62TIPOSMS", columnDefinition = "CHAR(3)")
    private TipoMensaje tipoMensaje;

    @Column(name = "S62APLIC", length = 20)
    private String aplicacion;

    @Column(name = "S62SERV", length = 20)
    private String servicio;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "S62ISBP61")
    private EncabezadoMensaje encabezadoMensaje;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EncabezadoMensaje getEncabezadoMensaje() {
        return encabezadoMensaje;
    }

    public void setEncabezadoMensaje(EncabezadoMensaje encabezadoMensaje) {
        this.encabezadoMensaje = encabezadoMensaje;
    }

    public String getNumeroCliente() {
        return numeroCliente;
    }

    public void setNumeroCliente(String numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    public String getNumeroCelular() {
        return numeroCelular;
    }

    public void setNumeroCelular(String numeroCelular) {
        this.numeroCelular = numeroCelular;
    }

    public String getParam0() {
        return param0;
    }

    public void setParam0(String param0) {
        this.param0 = param0;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }

    public String getParam3() {
        return param3;
    }

    public void setParam3(String param3) {
        this.param3 = param3;
    }

    public String getParam4() {
        return param4;
    }

    public void setParam4(String param4) {
        this.param4 = param4;
    }

    public String getParam5() {
        return param5;
    }

    public void setParam5(String param5) {
        this.param5 = param5;
    }

    public EstadoDetalleMensaje getEstado() {
        return estado;
    }

    public void setEstado(EstadoDetalleMensaje estado) {
        this.estado = estado;
    }

    public Date getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(Date fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public String getRespuestaYellowPepper() {
        return respuestaYellowPepper;
    }

    public void setRespuestaYellowPepper(String respuestaYellowPepper) {
        this.respuestaYellowPepper = respuestaYellowPepper;
    }

    public TipoMensaje getTipoMensaje() {
        return tipoMensaje;
    }

    public void setTipoMensaje(TipoMensaje tipoMensaje) {
        this.tipoMensaje = tipoMensaje;
    }

    public String getAplicacion() {
        return aplicacion;
    }

    public void setAplicacion(String aplicacion) {
        this.aplicacion = aplicacion;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    /**
     * Obtiene el mensaje como tal.
     *
     * Aceptamos los formatos ${1}, ${param1}, y {1}. El cero y el uno representan la misma posicion.
     *
     * @return el mensaje real
     */
    public String getMensajeReal() {
        String mensajeReal = MessageFormat.format(
                getEncabezadoMensaje().getMensaje(), param0, param1, param2, param3, param4, param5);
        return mensajeReal;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

}









