package bus.sms.entities;

import bus.sms.tipos.CanalEnvioSMS;
import bus.sms.tipos.EstadoMensajeSMS;
import bus.sms.tipos.TipoMensaje;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name="ISBP05")
public class Mensaje implements Serializable {

    private static final long serialVersionUID = 7209988526667231717L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="I05NIDBIT" )
    private Long id;
    
    @Column(name="I05TIP92", nullable=false)
    private Long idUsuario;
    
    @Column(name="I05DFECHA", nullable=false )
    private Timestamp fechaRegistro;
        
    @Column(name="I05CTIPO", length=12)
    @Enumerated(EnumType.STRING)
    private TipoMensaje tipoMensaje; 
    
    @Column(name="I05TIP02")
    private Long codigoTramite;

    @Column(name="I05TIP94B", length=3)
    @Enumerated(EnumType.STRING)
    private EstadoMensajeSMS estado;    

    @Column(name="I05VMSG", length=200)
    private String mensajeSms;

    @Column(name="I05CFONO", length=13)
    private String numeroCelular;

    @Column(name="I05VRESPYP", length=25)
    private String respuestaYP;

    @Column(name="I05CCANSMS", length=3)
    @Enumerated(EnumType.STRING)
    private CanalEnvioSMS canalSMS;

    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Timestamp getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public TipoMensaje getTipoMensaje() {
		return tipoMensaje;
	}

	public void setTipoMensaje(TipoMensaje tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	public Long getCodigoTramite() {
		return codigoTramite;
	}

	public void setCodigoTramite(Long codigoTramite) {
		this.codigoTramite = codigoTramite;
	}

	public EstadoMensajeSMS getEstado() {
		return estado;
	}

	public void setEstado(EstadoMensajeSMS estado) {
		this.estado = estado;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Mensajes [canalSMS=");
		builder.append(canalSMS);
		builder.append(", codigoTramite=");
		builder.append(codigoTramite);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", fechaRegistro=");
		builder.append(fechaRegistro);
		builder.append(", id=");
		builder.append(id);
		builder.append(", idUsuario=");
		builder.append(idUsuario);
		builder.append(", mensajeSms=");
		builder.append(mensajeSms);
		builder.append(", numeroCelular=");
		builder.append(numeroCelular);
		builder.append(", respuestaYP=");
		builder.append(respuestaYP);
		builder.append(", tipoMensaje=");
		builder.append(tipoMensaje);
		builder.append("]");
		return builder.toString();
	}

	public String getMensajeSms() {
		return mensajeSms;
	}

	public void setMensajeSms(String mensajeSms) {
		this.mensajeSms = mensajeSms;
	}

	public String getNumeroCelular() {
		return numeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	public String getRespuestaYP() {
		return respuestaYP;
	}

	public void setRespuestaYP(String respuestaYP) {
		this.respuestaYP = respuestaYP;
	}

	public CanalEnvioSMS getCanalSMS() {
		return canalSMS;
	}

	public void setCanalSMS(CanalEnvioSMS canalSMS) {
		this.canalSMS = canalSMS;
	}

	
	
}
