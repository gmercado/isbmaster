/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.sched.quartz;

import bus.plumbing.api.ProgramadorTareas;
import bus.plumbing.utils.Shutdowner;
import bus.sched.api.ProgramadorTareasAdmin;
import bus.sched.dao.TrabajoDao;
import bus.sched.dao.TrabajoHistorialDao;
import bus.sched.entities.Trabajo;
import bus.sched.entities.TrabajoHistorial;
import bus.sched.model.TrabajoTemporalBean;
import com.google.common.base.Function;
import com.google.inject.Inject;
import org.apache.wicket.util.time.Duration;
import org.quartz.*;
import org.quartz.impl.DirectSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.simpl.RAMJobStore;
import org.quartz.simpl.SimpleThreadPool;
import org.quartz.spi.JobStore;
import org.slf4j.Logger;

import javax.annotation.Nullable;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

import static bus.sched.model.EstadoTrabajo.*;
import static com.google.common.collect.Collections2.transform;
import static java.lang.Thread.MIN_PRIORITY;
import static org.apache.commons.lang.StringUtils.abbreviate;
import static org.apache.wicket.util.time.Time.now;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.quartz.TriggerKey.triggerKey;
import static org.quartz.impl.matchers.AndMatcher.and;
import static org.quartz.impl.matchers.GroupMatcher.jobGroupEquals;
import static org.quartz.impl.matchers.NameMatcher.jobNameEquals;
import static org.quartz.impl.matchers.NotMatcher.not;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author Marcelo Morales
 *         Date: Jul 8, 2010
 */
public class ProgramadorTareasImpl implements ProgramadorTareasAdmin {

    private static final Logger LOGGER = getLogger(ProgramadorTareas.class);

    private static final String USUARIO_EJECUTOR_KEY = "__usuario ejecutor___";

    private final Scheduler scheduler;

    private final SimpleThreadPool threadPool;

    private final TrabajoDao trabajoDao;

    private final TrabajoHistorialDao trabajoHistorialDao;

    @Inject
    public ProgramadorTareasImpl(final GuiceJobFactory jobFactory,
                                 Shutdowner shutdowner,
                                 TrabajoDao trabajoDao, TrabajoHistorialDao trabajoHistorialDao) throws SchedulerException {

        this.trabajoDao = trabajoDao;
        this.trabajoHistorialDao = trabajoHistorialDao;

        shutdowner.register(() -> {
            try {
                dispose();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });

        DirectSchedulerFactory factory = DirectSchedulerFactory.getInstance();
        threadPool = new SimpleThreadPool(1, MIN_PRIORITY);
        threadPool.initialize();
        JobStore jobStore = new RAMJobStore();
        factory.createScheduler(threadPool, jobStore);
        scheduler = factory.getScheduler();
        scheduler.setJobFactory(jobFactory);

        scheduler.getListenerManager().addJobListener(new QuartzJobListener(), not(jobGroupEquals(GRUPO_TEMPORAL)));

        scheduler.start();
    }

    @Override
    public void lanzarAhora(Trabajo trabajo, String userlogon) throws SchedulerException {
        JobKey jobKey = JobKey.jobKey(trabajo.getNombre(), trabajo.getGrupo());
        JobDataMap data = new JobDataMap();
        data.put(USUARIO_EJECUTOR_KEY, userlogon);
        scheduler.triggerJob(jobKey, data);
    }

    @Override
    public void activar(Trabajo trabajo) throws SchedulerException {
        scheduler.resumeJob(JobKey.jobKey(trabajo.getNombre(), trabajo.getGrupo()));
        trabajo.setEstado(PROG);
        trabajoDao.merge(trabajo);
    }

    @Override
    public void detener(Trabajo trabajo) throws SchedulerException {
        scheduler.pauseJob(JobKey.jobKey(trabajo.getNombre(), trabajo.getGrupo()));
        trabajo.setEstado(DETE);
        trabajoDao.merge(trabajo);
    }


    public void dispose() throws Exception {
        try {
            LOGGER.info("Deteniendo el scheduler");
            scheduler.shutdown(true);
        } catch (SchedulerException e) {
            LOGGER.error("Ha ocurrido un error al bajar el scheduler", e);
        }
        try {
            LOGGER.info("Deteniendo el thread pool del scheduler");
            threadPool.shutdown(true);
        } catch (RuntimeException e) {
            LOGGER.error("Ha ocurrido un error al bajar el thread pool del scheduler", e);
        }
    }

    @Override
    public void reprogramar(Trabajo trabajo, String cron) throws SchedulerException {
        trabajo.setCron(cron);
        trabajo = trabajoDao.merge(trabajo);

        final TriggerKey triggerKey = triggerKey(trabajo.getNombre() + "-trigger", trabajo.getGrupo());

        CronTrigger trigger = newTrigger().
                forJob(JobKey.jobKey(trabajo.getNombre(), trabajo.getGrupo())).
                withIdentity(triggerKey).
                withSchedule(cronSchedule(trabajo.getCron())).
                build();

        scheduler.standby();
        scheduler.rescheduleJob(triggerKey, trigger);

        if (DETE.equals(trabajo.getEstado())) {
            scheduler.pauseJob(JobKey.jobKey(trabajo.getNombre(), trabajo.getGrupo()));
        }

        scheduler.start();
    }

    @Override
    public void programar(String nombreTrabajo, String grupo, String cronPorDefecto, Class<? extends Job> jc,
                          boolean conHistoria) {
        synchronized (scheduler) {
            Trabajo trabajo = trabajoDao.getByName(nombreTrabajo, grupo, cronPorDefecto);
            final JobKey jobKey = JobKey.jobKey(trabajo.getNombre(), trabajo.getGrupo());
            final JobDetail jobDetail = JobBuilder.newJob(jc).
                    withIdentity(jobKey).
                    storeDurably().
                    build();
            try {
                scheduler.standby();
                scheduler.addJob(jobDetail, true);

                CronTrigger trigger = newTrigger().
                        forJob(jobKey).
                        withIdentity(triggerKey(nombreTrabajo + "-trigger", trabajo.getGrupo())).
                        withSchedule(cronSchedule(trabajo.getCron())).
                        build();

                Date date = scheduler.scheduleJob(trigger);
                LOGGER.info("Programando el trabajo {}, la siguiente ejecucion es {}", jobKey, date);

                if (conHistoria) {
                    ListenerManager listenerManager = scheduler.getListenerManager();
                    String name = nombreTrabajo + "-" + grupo;
                    listenerManager.removeJobListener(name);
                    listenerManager.addJobListener(new HistorialJobListener(name),
                            and(jobNameEquals(nombreTrabajo), jobGroupEquals(grupo)));
                }

                if (DETE.equals(trabajo.getEstado())) {
                    LOGGER.info("El trabajo {} no esta arriba... pausando", jobKey);
                    scheduler.pauseJob(jobKey);
                }
                scheduler.start();
            } catch (SchedulerException e) {
                LOGGER.error("No se ha podido colocar el Job " + jobKey + ". Puede ser peligroso", e);
            }
        }

    }

    private static final String GRUPO_TEMPORAL = "___Grupo temporal___";

    private static final Random RANDOM = new Random();

    @Override
    public void programarTrabajoTemporal(Class<? extends InterruptableJob> jobClass, String name,
                                         Duration esperaInicial, Map<?, ?> dataMap) {
        final String nombreTrabajo = name + "-" + RANDOM.nextInt(Integer.MAX_VALUE - 1);
        final JobDetail jobDetail = JobBuilder.newJob().
                withIdentity(new JobKey(nombreTrabajo, GRUPO_TEMPORAL)).
                ofType(jobClass).
                usingJobData(new JobDataMap(dataMap)).
                withDescription(name).
                build();

        final SimpleTrigger trigger = newTrigger().
                withIdentity(new TriggerKey(name + "-" + RANDOM.nextInt(Integer.MAX_VALUE - 1), GRUPO_TEMPORAL)).
                withSchedule(SimpleScheduleBuilder.simpleSchedule().withRepeatCount(0).withIntervalInSeconds(1)).
                startAt(now().add(esperaInicial).toDate()).
                build();

        try {
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            LOGGER.error("No se ha podido programar el job temporal " + name, e);
        }
    }

    @Override
    public Collection<TrabajoTemporalBean> getTrabajosTemporales() {
        try {
            final Set<JobKey> jobKeys = scheduler.getJobKeys(GroupMatcher.<JobKey>groupEquals(GRUPO_TEMPORAL));
            return transform(jobKeys, new Function<JobKey, TrabajoTemporalBean>() {

                @Override
                public TrabajoTemporalBean apply(@Nullable JobKey input) {
                    try {
                        final List<? extends Trigger> triggersOfJob = scheduler.getTriggersOfJob(input);
                        LOGGER.debug("Triggers of job {}: {}", input, triggersOfJob);
                        final SimpleTrigger simpleTrigger = (SimpleTrigger) triggersOfJob.get(0);
                        return new TrabajoTemporalBean(
                                scheduler.getJobDetail(input),
                                simpleTrigger,
                                scheduler.getTriggerState(simpleTrigger.getKey()));
                    } catch (SchedulerException e) {
                        LOGGER.error("no pude encontar el jobDetail", e);
                        return null;
                    }
                }
            });
        } catch (SchedulerException e) {
            LOGGER.error("No se ha podido encontrar los trabajos temporales", e);
            return Collections.emptyList();
        }
    }

    @Override
    public void cancelarTrabajoTemporal(JobKey jobKey) {

        try {
            scheduler.interrupt(jobKey);
        } catch (UnableToInterruptJobException e) {
            LOGGER.error("Error al interrumpir", e);
        }

        try {
            for (Trigger o : scheduler.getTriggersOfJob(jobKey)) {
                scheduler.unscheduleJob(o.getKey());
            }
        } catch (SchedulerException e) {
            LOGGER.error("Error al interrumpir", e);
        }
    }

    private Trabajo getTrabajo(JobExecutionContext context) {
        final JobDetail jobDetail = context.getJobDetail();
        final JobKey key = jobDetail.getKey();
        return trabajoDao.getByName(key.getName(), key.getGroup(), "");
    }

    private class QuartzJobListener implements JobListener {

        @Override
        public String getName() {
            return "Actualizacion de trabajo";
        }

        @Override
        public void jobToBeExecuted(JobExecutionContext context) {
            try {
                final Trabajo byName = getTrabajo(context);
                if (PROG.equals(byName.getEstado())) {
                    byName.setEstado(EJEC);
                    trabajoDao.merge(byName);
                }
            } catch (RuntimeException e) {
                LOGGER.error("Error el el listener", e);
            }
        }

        @Override
        public void jobExecutionVetoed(JobExecutionContext context) {
            try {
                final Trabajo byName = getTrabajo(context);
                if (EJEC.equals(byName.getEstado())) {
                    byName.setEstado(PROG);
                    trabajoDao.merge(byName);
                }
            } catch (RuntimeException e) {
                LOGGER.error("Error el el listener", e);
            }
        }

        @Override
        public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
            try {
                final Trabajo byName = getTrabajo(context);
                byName.setUltimaEjecucion(new Date());
                if (EJEC.equals(byName.getEstado())) {
                    byName.setEstado(PROG);
                }
                trabajoDao.merge(byName);
            } catch (RuntimeException e) {
                LOGGER.error("Error el el listener", e);
            }
        }
    }

    private class HistorialJobListener implements JobListener {

        private final String name;

        private HistorialJobListener(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public void jobToBeExecuted(JobExecutionContext context) {
        }

        @Override
        public void jobExecutionVetoed(JobExecutionContext context) {
        }

        @Override
        public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
            try {
                TrabajoHistorial historial = new TrabajoHistorial();
                historial.setEjecutado(new Date());
                historial.setProgramado(context.getFireTime());

                if (jobException == null) {
                    historial.setResultado("OK");
                } else {
                    if (jobException.getMessage() == null) {
                        StringWriter out = new StringWriter();
                        jobException.printStackTrace(new PrintWriter(out));
                        historial.setResultado(abbreviate(out.toString(), 100));
                    } else {
                        historial.setResultado(jobException.getMessage());
                    }
                }

                historial.setTrabajo(getTrabajo(context));

                Object o = context.getMergedJobDataMap().get(USUARIO_EJECUTOR_KEY);
                if (o != null) {
                    historial.setEjecutor(o.toString());
                }

                trabajoHistorialDao.persist(historial);
            } catch (RuntimeException e) {
                LOGGER.error("Error el el listener", e);
            }
        }
    }
}
