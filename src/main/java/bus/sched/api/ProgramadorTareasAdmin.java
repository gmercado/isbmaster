package bus.sched.api;

import bus.plumbing.api.ProgramadorTareas;
import bus.sched.entities.Trabajo;
import bus.sched.model.TrabajoTemporalBean;
import org.quartz.JobKey;
import org.quartz.SchedulerException;

import java.util.Collection;

/**
 * @author Marcelo Morales
 *         Since: 2/6/13
 */
public interface ProgramadorTareasAdmin extends ProgramadorTareas {

    void lanzarAhora(Trabajo trabajo, String userlogon) throws SchedulerException;

    void activar(Trabajo trabajo) throws SchedulerException;

    void detener(Trabajo trabajo) throws SchedulerException;

    void reprogramar(Trabajo trabajo, String cron) throws SchedulerException;

    Collection<TrabajoTemporalBean> getTrabajosTemporales();

    void cancelarTrabajoTemporal(JobKey jobKey);
}
