/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.sched.dao;

import bus.database.dao.DaoImpl;
import bus.database.model.BasePrincipal;
import bus.sched.entities.Trabajo;
import bus.sched.model.EstadoTrabajo;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaBuilder;
import org.apache.openjpa.persistence.criteria.OpenJPACriteriaQuery;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

/**
 * @author Marcelo Morales
 *         Created: 2/27/12 5:01 PM
 */
public class TrabajoDao extends DaoImpl<Trabajo, Long> {

    protected TrabajoDao() {
        super(null);
    }

    @Inject
    public TrabajoDao(@BasePrincipal EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    protected Path<Long> countPath(Root<Trabajo> from) {
        return from.get("id");
    }

    @Override
    protected Iterable<Path<String>> getFullTexts(Root<Trabajo> p) {
        Path<String> nombre = p.get("nombre");
        Path<String> grupo = p.get("grupo");
        return Lists.newArrayList(nombre, grupo);
    }

    public Trabajo getByName(final String nombreTrabajo, final String grupoTrabajo, final String ejecucionPorDefecto) {
        return doWithTransaction((entityManager, t) -> {
            final OpenJPACriteriaBuilder cb = entityManager.getCriteriaBuilder();
            final OpenJPACriteriaQuery<Trabajo> query = cb.createQuery(Trabajo.class);
            final Root<Trabajo> from = query.from(Trabajo.class);
            query.where(cb.equal(from.get("nombre"), nombreTrabajo),
                    cb.equal(from.get("grupo"), grupoTrabajo));
            try {
                return entityManager.createQuery(query).getSingleResult();
            } catch (NoResultException e) {
                Trabajo trabajo = new Trabajo();
                trabajo.setNombre(nombreTrabajo);
                trabajo.setGrupo(grupoTrabajo);
                trabajo.setCron(ejecucionPorDefecto);
                trabajo.setEstado(EstadoTrabajo.DETE);
                entityManager.persist(trabajo);
                return trabajo;
            }
        });
    }
}
