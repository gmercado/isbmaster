package bus.sched.ui;

import bus.consumoweb.infocred.utilitarios.DomUtil;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.bus.servicios.asfi.infocred.api.*;
import com.bisa.bus.servicios.asfi.infocred.entities.AlertaCabecera;
import com.bisa.bus.servicios.asfi.infocred.ui.ReSendMailWrapper;
import com.google.common.base.Splitter;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

import static bus.env.api.Variables.INFOCRED_LISTADO_EMAILS;
import static bus.env.api.Variables.INFOCRED_LISTADO_EMAILS_DEFAULT;
import static bus.sched.general.DescargaFtpJob.BIS_CSV;
import static bus.sched.general.DescargaFtpJob.BIS_PIPE;
import static bus.sched.general.DescargaFtpJob.BIS_ZIP_PIPE;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class Importador {

    private MedioAmbiente medioAmbiente;
    private AlertaCabeceraService alertaCabeceraService;
    private InfocredCarteraComaService infocredCarteraComaService;
    private InfocredCarteraPipeService infocredCarteraPipeService;
    private InfocredCarteraZipPipeService infocredCarteraZipPipeService;

    private final ReSendMailWrapper mailWrapper;

    private static final Logger log = LoggerFactory.getLogger(Importador.class);

    @Inject
    public Importador(AlertaCabeceraService alertaCabeceraService, InfocredCarteraPipeService infocredCarteraPipeService,
                      InfocredCarteraComaService infocredCarteraComaService, InfocredCarteraZipPipeService infocredCarteraZipPipeService,
                      ReSendMailWrapper mailWrapper, MedioAmbiente medioAmbiente) {
        this.alertaCabeceraService = alertaCabeceraService;
        this.infocredCarteraPipeService = infocredCarteraPipeService;
        this.infocredCarteraComaService = infocredCarteraComaService;
        this.infocredCarteraZipPipeService = infocredCarteraZipPipeService;
        this.mailWrapper = mailWrapper;
        this.medioAmbiente = medioAmbiente;
    }

    public int importarAlertas(File archivo, String nombreOriginal) {
        log.info("Ingresando a procesar archivos Alerta ...");
        List<String> correos = Splitter.on(DomUtil.SEPARADOR)
                .splitToList(medioAmbiente.getValorDe(INFOCRED_LISTADO_EMAILS, INFOCRED_LISTADO_EMAILS_DEFAULT));
        try {
            AlertaCabecera ac = alertaCabeceraService.guardarAlerta(archivo, nombreOriginal);
            if (ac != null) {
                new MailNotifier(mailWrapper).notifySuccess(nombreOriginal, correos);
                return AdministradorDirectorios.CARGADO;
            }
            new MailNotifier(mailWrapper).notifyError(nombreOriginal, correos,
                    new InfocredException("Verifique que el esquema de la alerta no haya cambiado, comunicate con el encargado"));
            return AdministradorDirectorios.RECHAZADO;
        } catch (InfocredException e) {
            log.error("Error de validación en fichero de alerta : {} ", nombreOriginal);
            new MailNotifier(mailWrapper).notifyError(nombreOriginal, correos, e);
            return AdministradorDirectorios.RECHAZADO;
        } catch (Exception e) {
            log.error("Ocurrio un error inesperado al cargar alertas ", e);
            new MailNotifier(mailWrapper).notifyError(nombreOriginal, correos, e);
            return AdministradorDirectorios.RECHAZADO;
        }
    }

    public int importarCartera(String nombreArchivo, List<String> list, int tipoArchivo) {
        log.info("Ingresando a procesar archivos Cartera ...");
        List<String> correos = Splitter.on(DomUtil.SEPARADOR)
                .splitToList(medioAmbiente.getValorDe(INFOCRED_LISTADO_EMAILS, INFOCRED_LISTADO_EMAILS_DEFAULT));
        // Se verifica si el archivo a importar ya fue procesado con anterioridad
        List<AlertaCabecera> ac = alertaCabeceraService.buscarPorNombreArchivo(nombreArchivo);
        log.debug("El Archivo {}, {}", nombreArchivo, ac.size() > 0 ? "ya fue procesado" : "es nuevo");
        if (ac != null && ac.size() > 0) {
            log.debug("El archivo ya fue procesado.");
            new MailNotifier(mailWrapper).notifyError(nombreArchivo, correos, new InfocredException("El archivo fue procesado anteriormente, " +
                    "si desea procesar este archivo es necesario renombrarlo."));
            return AdministradorDirectorios.RECHAZADO;
        }
        try {
            switch (tipoArchivo) {
                case BIS_CSV:
                    infocredCarteraComaService.guardarCartera(nombreArchivo, list,
                            medioAmbiente.getValorDe(Variables.SEPARADOR_CARTERA_COMA, Variables.SEPARADOR_CARTERA_COMA_DEFAULT));
                    break;
                case BIS_PIPE:
                    infocredCarteraPipeService.guardarCartera(nombreArchivo, list,
                            medioAmbiente.getValorDe(Variables.SEPARADOR_CARTERA_PIPE, Variables.SEPARADOR_CARTERA_PIPE_DEFAULT));
                    break;
                case BIS_ZIP_PIPE:
                    infocredCarteraZipPipeService.guardarCartera(nombreArchivo, list,
                            medioAmbiente.getValorDe(Variables.SEPARADOR_CARTERA_ZIP_PIPE, Variables.SEPARADOR_CARTERA_ZIP_PIPE_DEFAULT));
                    break;
            }
        } catch (InfocredException e) {
            log.error("Error de validación en fichero de cartera : {} ", nombreArchivo);
            new MailNotifier(mailWrapper).notifyError(nombreArchivo, correos, e);
            return AdministradorDirectorios.RECHAZADO;
        } catch (Exception e) {
            log.error("Ocurrio un error inesperado ", e);
            new MailNotifier(mailWrapper).notifyError(nombreArchivo, correos, e);
            return AdministradorDirectorios.RECHAZADO;
        }
        new MailNotifier(mailWrapper).notifySuccess(nombreArchivo, correos);
        return AdministradorDirectorios.CARGADO;
    }

    public static class MailNotifier {

        private ReSendMailWrapper mailWrapper;

        public MailNotifier(ReSendMailWrapper mailWrapper) {
            this.mailWrapper = mailWrapper;
        }

        public void notifySuccess(String nombreArchivo, List<String> destinatarios) {
            mailWrapper.sendMessage("Archivo importado", MailNotifier.getMensajeCorrecto(nombreArchivo), destinatarios);
        }

        public void notifyError(String nombreArchivo, List<String> destinatarios, Exception e) {
            if (e instanceof InfocredException) {
                mailWrapper.sendMessage("Archivo importado", MailNotifier.getMensajeError(nombreArchivo, ((InfocredException) e).getValidationErrors() ), destinatarios);
                return;
            }
            mailWrapper.sendMessage("Archivo importado", MailNotifier.getMensajeError(nombreArchivo, e.getMessage()), destinatarios);
        }

        public void downloadSuccess(String nombreArchivo, List<String> destinatarios) {
            mailWrapper.sendMessage("Archivo descargado (ftp)", MailNotifier.getDescargaFtpCorrecto(nombreArchivo), destinatarios);
        }

        public void downloadError(String nombreArchivo, List<String> destinatarios) {
            mailWrapper.sendMessage("Falla de descarga (ftp)", MailNotifier.getDescargaFtpError(nombreArchivo), destinatarios);
        }

        public static String getMensajeCorrecto(String nombreArchivo) {
            return "Estimados usuarios" +
                    ",\n\nEl archivo " + nombreArchivo + " fue importado exitosamente." +
                    "\n\nAtte.," +
                    "\nBanco BISA S.A.";
        }

        public static String getDescargaFtpCorrecto(String nombreArchivo) {
            return "Estimados usuarios" +
                    ",\n\nEl archivo " + nombreArchivo + " fue descargado exitosamente." +
                    "\n\nAtte.," +
                    "\nBanco BISA S.A.";
        }

        public static String getMensajeError(String nombreArchivo, String mensaje) {
            return "Estimados usuarios" +
                    ",\n\nEl archivo " + nombreArchivo + " fue rechazado." +
                    "\nMensaje:\n" + mensaje +
                    "\n\nPor favor es necesario revisar el archivo y verificar si ingreso en el directorio correcto." +
                    "\n\nAtte.," +
                    "\nBanco BISA S.A.";
        }

        public static String getDescargaFtpError(String nombreArchivo) {
            return "Estimados usuarios" +
                    ",\n\nEl archivo " + nombreArchivo + " No pudo ser descargado de la FTP de Inforcred." +
                    "\n\nPor favor es necesario revisar el archivo original en la FTP y/o proceder con la descarga manual si es necesario." +
                    "\n\nAtte.," +
                    "\nBanco BISA S.A.";
        }
    }
}