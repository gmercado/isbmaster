/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.sched.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Titulo;
import bus.plumbing.api.RolesBisa;
import bus.sched.api.ProgramadorTareasAdmin;
import bus.sched.dao.TrabajoDao;
import bus.sched.entities.Trabajo;
import bus.sched.model.EstadoTrabajo;
import com.google.inject.Inject;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.convert.IConverter;
import org.apache.wicket.util.convert.converter.DateConverter;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static bus.users.api.Metadatas.USER_META_DATA_KEY;

/**
 * @author Marcelo Morales
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Titulo("Detalle de trabajo")
public class TrabajoDetalle extends BisaWebPage {

    private static final long serialVersionUID = 928374923487L;

    private static final Logger LOGGER = LoggerFactory.getLogger(TrabajoDetalle.class);

    private Date ejecucionesDesde;

    private String cron;

    @Inject
    private TrabajoDao trabajoDao;

    @Inject
    private ProgramadorTareasAdmin q;

    public TrabajoDetalle(PageParameters p) {
        super(p);

        setDefaultModel(new CompoundPropertyModel<>(new LoadableDetachableModel<Trabajo>() {
            private static final long serialVersionUID = -4122895340959929189L;

            @Override
            protected Trabajo load() {
                return trabajoDao.find(getPageParameters().get("id").toLong());
            }
        }));

        this.ejecucionesDesde = new Date();
        Form<Trabajo> cancelar = new Form<>("cancelar");
        add(cancelar);

        cancelar.add(new Label("id"));
        cancelar.add(new Label("nombre"));
        cancelar.add(new Label("grupo"));
        cancelar.add(new Label("estado.descripcion"));
        cancelar.add(new Label("cron"));
        cancelar.add(new Label("ultimaEjecucion"));

        cancelar.add(new Button("volver") {

            private static final long serialVersionUID = -5048987968090015381L;

            @Override
            public void onSubmit() {
                setResponsePage(ListadoTrabajos.class);
            }
        });

        cancelar.add(new Button("ejecutar") {

            private static final long serialVersionUID = 9837424L;

            @Override
            public void onSubmit() {
                try {
                    Trabajo trabajo = (Trabajo) TrabajoDetalle.this.getDefaultModelObject();
                    q.lanzarAhora(trabajo, getSession().getMetaData(USER_META_DATA_KEY).getUserlogon());
                    getSession().info("Se ha lanzado una ejecuci\u00F3n del trabajo");
                } catch (Exception ex) {
                    LOGGER.error("Error al schedulear un trabajo", ex);
                    getSession().error("No se pudo iniciar una ejecuci\u00F3n del trabajo");
                }
            }
        });

        Form<?> planDeEjecuciones;
        add(planDeEjecuciones = new Form<Void>("planDeEjecuciones"));
        planDeEjecuciones.add(new TextField<>("cron", new PropertyModel<>(this, "cron")));
        planDeEjecuciones.add(new Button("guardarcron") {
            private static final long serialVersionUID = 1838450323134075018L;

            @Override
            public void onSubmit() {
                try {
                    Trabajo trabajo = (Trabajo) TrabajoDetalle.this.getDefaultModelObject();
                    q.reprogramar(trabajo, cron);
                    getSession().info("Expresi\u00F3n re-programada");
                } catch (Exception e) {
                    LOGGER.error("Ha ocurrido un error inesperado", e);
                    getSession().error("Ha ocurrido un error inesperado, consulte el log");
                }
                setResponsePage(TrabajoDetalle.class, getPageParameters());
            }
        });

        final ModalWindow cronayuda;
        add(cronayuda = new ModalWindow("cronayuda"));
        cronayuda.setContent(new CronHelpPanel(cronayuda.getContentId()));
        cronayuda.setTitle("Expresiones comunes");

        Label image;
        planDeEjecuciones.add(image = new Label("ayuda", "?"));
        image.add(new AjaxEventBehavior("click") {

            private static final long serialVersionUID = 1L;

            @Override
            protected void onEvent(AjaxRequestTarget target) {
                cronayuda.show(target);
            }
        });


        planDeEjecuciones.add(new TextField<Date>("fecha", new PropertyModel<>(this, "ejecucionesDesde")) {
            private static final long serialVersionUID = -2383808108058555744L;

            @Override
            public <Date> IConverter<Date> getConverter(Class<Date> type) {
                //noinspection unchecked
                return (IConverter<Date>) new DateConverter() {

                    private static final long serialVersionUID = -5939087656904723831L;

                    @Override
                    public DateFormat getDateFormat(Locale locale) {
                        return new SimpleDateFormat("dd/MM/yyyy HH:mm", locale);
                    }
                };
            }

        });

        planDeEjecuciones.add(new ListView<Date>("ejecuciones", new AbstractReadOnlyModel<List<Date>>() {

            private static final long serialVersionUID = 1L;

            @Override
            public List<Date> getObject() {
                return getEjecuciones(ejecucionesDesde);
            }
        }) {

            private static final long serialVersionUID = 1L;

            @Override
            protected void populateItem(final ListItem<Date> item) {
                item.add(new Label("ejecucion", new AbstractReadOnlyModel<String>() {

                    private static final long serialVersionUID = 2316597864603201573L;

                    DateFormat df = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.MEDIUM, getLocale());

                    @Override
                    public String getObject() {
                        return df.format(item.getModelObject());
                    }
                }));
            }
        });

        cancelar.add(new Button("activar") {

            private static final long serialVersionUID = 9829874234L;

            @Override
            public void onSubmit() {
                try {
                    Trabajo trabajo = (Trabajo) TrabajoDetalle.this.getDefaultModelObject();
                    q.activar(trabajo);
                    getSession().info("Expresi\u00F3n re-programada");
                } catch (Exception e) {
                    LOGGER.error("Ha ocurrido un error inesperado", e);
                    getSession().error("Ha ocurrido un error inesperado, consulte el log");
                }
                setResponsePage(TrabajoDetalle.class, getPageParameters());
            }

            @Override
            protected void onBeforeRender() {
                super.onBeforeRender();
                Trabajo config = (Trabajo) TrabajoDetalle.this.getDefaultModelObject();
                setEnabled(EstadoTrabajo.DETE.equals(config.getEstado()));
            }
        });

        cancelar.add(new Button("desactivar") {

            private static final long serialVersionUID = 98298742544L;

            @Override
            public void onSubmit() {
                try {
                    Trabajo trabajo = (Trabajo) TrabajoDetalle.this.getDefaultModelObject();
                    q.detener(trabajo);
                    getSession().info("Expresi\u00F3n re-programada");
                } catch (Exception e) {
                    LOGGER.error("Ha ocurrido un error inesperado", e);
                    getSession().error("Ha ocurrido un error inesperado, consulte el log");
                }
                setResponsePage(TrabajoDetalle.class, getPageParameters());
            }

            @Override
            protected void onBeforeRender() {
                super.onBeforeRender();
                Trabajo config = (Trabajo) TrabajoDetalle.this.getDefaultModelObject();
                setEnabled(EstadoTrabajo.PROG.equals(config.getEstado()));
            }
        });
    }

    public List<Date> getEjecuciones(Date date) {
        if (date.before(new Date())) {
            date = new Date();
        }
        String ce = getCron();
        if (ce == null) {
            return Collections.emptyList();
        }
        try {
            CronExpression expression = new CronExpression(ce);
            List<Date> dates = new LinkedList<>();
            for (int i = 0; i < 10; i++) {
                date = expression.getNextValidTimeAfter(date);
                if (date == null) {
                    break;
                }
                dates.add(date);
            }
            return dates;
        } catch (ParseException ex) {
            return Collections.emptyList();
        }
    }

    public synchronized String getCron() {
        if (cron == null) {
            cron = ((Trabajo) getDefaultModelObject()).getCron();
        }
        return cron;
    }

    @SuppressWarnings("unused")
    public void setCron(String cron) { // OJO: se usa con un PropertyModel
        this.cron = cron;
    }
}
