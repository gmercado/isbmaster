/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.sched.ui;

import bus.database.components.EstiloFiltro;
import bus.database.components.FiltroPanel;
import bus.database.components.ListadoDataProvider;
import bus.database.components.ListadoPanel;
import bus.database.dao.Dao;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.CambioCantidadDataTableToolbar;
import bus.plumbing.components.DateTimePropertyColumn;
import bus.plumbing.components.LinkPropertyColumn;
import bus.plumbing.components.SeleccionColumn;
import bus.sched.dao.TrabajoDao;
import bus.sched.dao.TrabajoHistorialDao;
import bus.sched.entities.Trabajo;
import bus.sched.entities.TrabajoHistorial;
import bus.sched.model.EstadoTrabajo;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Marcelo Morales
 */
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
@Menu(value = "Trabajos desatendidos", subMenu = SubMenu.ADMINISTRACION)
public class ListadoTrabajos extends BisaWebPage {

    private static final long serialVersionUID = 1L;

    private final IModel<Trabajo> trabajoSeleccionado;

    private final ListadoPanel<TrabajoHistorial, Long> listadoHistorialPanel;

    public ListadoTrabajos() {
        trabajoSeleccionado = new Model<>(new Trabajo());

        add(listadoHistorialPanel = new ListadoPanel<TrabajoHistorial, Long>("historial de trabajos") {

            @Override
            protected EstiloFiltro getEstiloFiltro() {
                return EstiloFiltro.FORMULARIO;
            }


            @Override
            protected FiltroPanel<TrabajoHistorial> newFiltroPanel(String id, IModel<TrabajoHistorial> model1, IModel<TrabajoHistorial> model2) {
                return new FiltroPanel<TrabajoHistorial>(id, model1, model2) {{
                    setVisible(false);
                }};
            }


            @Override
            protected List<IColumn<TrabajoHistorial, String>> newColumns(ListadoDataProvider<TrabajoHistorial, Long> trabajoHistorialLongListadoDataProvider) {
                LinkedList<IColumn<TrabajoHistorial, String>> iColumns = new LinkedList<>();
                iColumns.add(new DateTimePropertyColumn<>(Model.of("programado"), "programado"));
                iColumns.add(new DateTimePropertyColumn<>(Model.of("ejecutado"), "ejecutado"));
                iColumns.add(new PropertyColumn<>(Model.of("resultado"), "resultado"));
                iColumns.add(new PropertyColumn<>(Model.of("ejecutor"), "ejecutor"));
                return iColumns;
            }

            @Override
            protected Class<? extends Dao<TrabajoHistorial, Long>> getProviderClazz() {
                return TrabajoHistorialDao.class;
            }

            @Override
            protected IModel<TrabajoHistorial> getModel1() {
                return new AbstractReadOnlyModel<TrabajoHistorial>() {

                    @Override
                    public TrabajoHistorial getObject() {
                        TrabajoHistorial th = new TrabajoHistorial();
                        th.setTrabajo(trabajoSeleccionado.getObject());
                        return th;
                    }
                };
            }

            @Override
            protected boolean isVisibleData() {
                return trabajoSeleccionado.getObject().getId() != null;
            }

            @Override
            protected DataTable<TrabajoHistorial, String> newDataTable(String id,
                                                                       List<IColumn<TrabajoHistorial, String>> iColumns,
                                                                       ListadoDataProvider<TrabajoHistorial, Long> trabajoHistorialLongListadoDataProvider) {
                DataTable<TrabajoHistorial, String> dt = super.newDataTable(id, iColumns, trabajoHistorialLongListadoDataProvider);
                dt.addBottomToolbar(new CambioCantidadDataTableToolbar(dt));
                return dt;
            }
        });

        add(new ListadoPanel<Trabajo, Long>("trabajos desatendidos") {

            @Override
            protected List<IColumn<Trabajo, String>> newColumns(ListadoDataProvider listadoDataProvider) {
                List<IColumn<Trabajo, String>> columns = new LinkedList<>();
                columns.add(new LinkPropertyColumn<Trabajo>(Model.of("Nombre"), "nombre") {

                    private static final long serialVersionUID = 92387234L;

                    @Override
                    public String getCssClass() {
                        return null;
                    }

                    @Override
                    protected void onClick(Trabajo object) {
                        setResponsePage(TrabajoDetalle.class, new PageParameters().add("id", object.getId()));
                    }
                });
                columns.add(new PropertyColumn<>(Model.of("Grupo"), "grupo"));
                columns.add(new PropertyColumn<Trabajo, String>(Model.of("Estado"), "estado.descripcion") {

                    @Override
                    public void populateItem(Item<ICellPopulator<Trabajo>> item, String componentId, final IModel<Trabajo> rowModel) {
                        item.add(new Label(componentId, getDataModel(rowModel)).
                                add(new AttributeModifier("class", new AbstractReadOnlyModel<String>() {

                                    @Override
                                    public String getObject() {
                                        EstadoTrabajo estado = rowModel.getObject().getEstado();
                                        switch (estado) {
                                            case DETE:
                                                return "muted";
                                            case EJEC:
                                                return "btn btn-small disabled btn-danger";
                                            case PROG:
                                                return "";
                                        }
                                        return "";
                                    }
                                })));
                    }
                });
                columns.add(new PropertyColumn<>(Model.of("\u00faltima ejecuci\u00f3n"), "ultimaEjecucion"));
                columns.add(new PropertyColumn<>(Model.of("Siguiente ejecuci\u00f3n"), "nextExecution"));
                columns.add(new SeleccionColumn<>(getSeleccionModel(), "sel", listadoHistorialPanel));
                return columns;
            }

            @Override
            protected Class<? extends Dao<Trabajo, Long>> getProviderClazz() {
                return TrabajoDao.class;
            }

            @Override
            protected IModel<Trabajo> getSeleccionModel() {
                return trabajoSeleccionado;
            }
        });

        add(new TrabajosTemporalesPanel("trabajos temporales"));
    }

}
