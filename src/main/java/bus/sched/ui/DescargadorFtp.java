package bus.sched.ui;

import bus.config.dao.CryptUtils;
import bus.consumoweb.infocred.utilitarios.DomUtil;
import bus.env.api.MedioAmbiente;
import bus.env.api.Variables;
import com.bisa.bus.servicios.asfi.infocred.api.AlertaCabeceraService;
import com.bisa.bus.servicios.asfi.infocred.entities.AlertaCabecera;
import com.bisa.bus.servicios.asfi.infocred.ui.ReSendMailWrapper;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

import static bus.env.api.Variables.INFOCRED_LISTADO_EMAILS;
import static bus.env.api.Variables.INFOCRED_LISTADO_EMAILS_DEFAULT;
import static bus.sched.general.DescargaFtpJob.*;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class DescargadorFtp {

    private CryptUtils cryptUtils;
    private MedioAmbiente medioAmbiente;
    private final ReSendMailWrapper mailWrapper;
    private AlertaCabeceraService alertaCabeceraService;

    private static final Logger log = LoggerFactory.getLogger(DescargadorFtp.class);

    @Inject
    public DescargadorFtp(MedioAmbiente medioAmbiente, AlertaCabeceraService alertaCabeceraService,
                          CryptUtils cryptUtils, ReSendMailWrapper mailWrapper) {
        this.cryptUtils = cryptUtils;
        this.mailWrapper = mailWrapper;
        this.medioAmbiente = medioAmbiente;
        this.alertaCabeceraService = alertaCabeceraService;
    }

    public FTPClient getFtpClient() {
        String url = medioAmbiente.getValorDe(Variables.INFOCRED_URL_FTP, Variables.INFOCRED_URL_FTP_DEFAULT);
        int port = medioAmbiente.getValorIntDe(Variables.INFOCRED_PUERTO_FTP, Variables.INFOCRED_PUERTO_FTP_DEFAULT);
        String user = medioAmbiente.getValorDe(Variables.INFOCRED_USUARIO_FTP, Variables.INFOCRED_USUARIO_FTP_DEFAULT);
        String pass = medioAmbiente.getValorDe(Variables.INFOCRED_PASSWORD_FTP, Variables.INFOCRED_PASSWORD_FTP_DEFAULT);
        int type = medioAmbiente.getValorIntDe(Variables.INFOCRED_TIPO_ARCHIVO_FTP, Variables.INFOCRED_TIPO_ARCHIVO_FTP_DEFAULT);

        // verifica si esta encriptado
        if (cryptUtils.esCryptLegado(pass)) {
            pass = cryptUtils.dec(pass);
        } else if (cryptUtils.esOpenssl(pass)) {
            pass = cryptUtils.decssl(pass);
        }
        try {
            FTPClient ftpClient = new FTPClient();
            ftpClient.connect(url, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(type);
            return ftpClient;
        } catch (IOException e) {
            log.error("Error al configurar el cliente FTP, revisar la configuracion", e);
        } catch (Exception e) {
            log.error("Ocurrio un error inesperado al configurar el cliente FTP", e);
        }
        return null;
    }

    public String getNombreArchivo(int tipoArchivo) {
        switch (tipoArchivo) {
            case BIS_CSV:
                return medioAmbiente.getValorDe(Variables.INFOCRED_ARCHIVO_COMA, Variables.INFOCRED_ARCHIVO_COMA_DEFAULT);
            case BIS_PIPE:
                return medioAmbiente.getValorDe(Variables.INFOCRED_ARCHIVO_PIPE, Variables.INFOCRED_ARCHIVO_PIPE_DEFAULT);
            case BIS_ZIP_PIPE:
                return medioAmbiente.getValorDe(Variables.INFOCRED_ARCHIVO_ZIP_PIPE, Variables.INFOCRED_ARCHIVO_ZIP_PIPE_DEFAULT);
            case ALERTA_1:
                return medioAmbiente.getValorDe(Variables.INFOCRED_ARCHIVO_ALERTA_1, Variables.INFOCRED_ARCHIVO_ALERTA_1_DEFAULT);
            case ALERTA_2:
                return medioAmbiente.getValorDe(Variables.INFOCRED_ARCHIVO_ALERTA_2, Variables.INFOCRED_ARCHIVO_ALERTA_2_DEFAULT);
            case ALERTA_3:
                return medioAmbiente.getValorDe(Variables.INFOCRED_ARCHIVO_ALERTA_3, Variables.INFOCRED_ARCHIVO_ALERTA_3_DEFAULT);
            case ALERTA_4:
                return medioAmbiente.getValorDe(Variables.INFOCRED_ARCHIVO_ALERTA_4, Variables.INFOCRED_ARCHIVO_ALERTA_4_DEFAULT);
            case ALERTA_5:
                return medioAmbiente.getValorDe(Variables.INFOCRED_ARCHIVO_ALERTA_5, Variables.INFOCRED_ARCHIVO_ALERTA_5_DEFAULT);
            case ALERTA_6:
                return medioAmbiente.getValorDe(Variables.INFOCRED_ARCHIVO_ALERTA_6, Variables.INFOCRED_ARCHIVO_ALERTA_6_DEFAULT);
            case ALERTA_7:
                return medioAmbiente.getValorDe(Variables.INFOCRED_ARCHIVO_ALERTA_7, Variables.INFOCRED_ARCHIVO_ALERTA_7_DEFAULT);
            default:
                return null;
        }
    }

    public boolean procesarArchivoFTP(FTPClient ftpClient, String directorio, String nombreArchivo) {
        try {
            if (ftpClient != null && !Strings.isNullOrEmpty(nombreArchivo)) {
                for (String nombre : ftpClient.listNames()) {
                    // validamos si el fichero fue descargado anteriormente
                    List<AlertaCabecera> ac = alertaCabeceraService.buscarPorNombreArchivo(nombre);

                    log.debug("El Archivo {}, {}", nombre, ac.size() > 0 ? "ya fue procesado" : "es nuevo");
                    if (ac.size() > 0) {
                        continue;
                    }
                    if (nombre.trim().toLowerCase().contains(nombreArchivo.trim().toLowerCase())) {
                        File outFile = new File(directorio + File.separator + nombre);
                        return descargarArchivo(ftpClient, nombre, outFile);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error al verificar la existencia del archivo {}", e);
        }
        return false;
    }

    public boolean descargarArchivo(FTPClient ftpClient, String nombreArchivo, File output) {
        OutputStream outputStream = null;
        FileOutputStream fileOutputStream = null;
        // Obtenemos el listado de correos de envio
        List<String> correos = Splitter.on(DomUtil.SEPARADOR)
                .splitToList(medioAmbiente.getValorDe(INFOCRED_LISTADO_EMAILS, INFOCRED_LISTADO_EMAILS_DEFAULT));
        try {
            fileOutputStream = new FileOutputStream(output);
            outputStream = new BufferedOutputStream(fileOutputStream);
            boolean fueDescargado = ftpClient.retrieveFile(nombreArchivo, outputStream);
            log.info("El archivo {} del ftp {} ser descargado ", nombreArchivo, fueDescargado ? "Si pudo" : "No pudo");
            if (fueDescargado) {
                new Importador.MailNotifier(mailWrapper).downloadSuccess(nombreArchivo, correos);
            } else {
                new Importador.MailNotifier(mailWrapper).downloadError(nombreArchivo, correos);
            }
            return fueDescargado;
        } catch (Exception e) {
            log.error("Ocurrio un error al tratar de descargar el archivo {} de la FTP ", nombreArchivo, e);
            new Importador.MailNotifier(mailWrapper).downloadError(nombreArchivo, correos);
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException ign) {}
        }
        return false;
    }
}
