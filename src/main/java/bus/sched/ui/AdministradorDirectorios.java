package bus.sched.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class AdministradorDirectorios {

    private static String CARGADOS = "cargados", PENDIENTES = "pendientes", RECHAZADOS = "rechazados";
    public static String ALERTAS = "alertas", CARTERA_COMA = "reporteCartera", CARTERA_PIPE = "carteraPropia", CARTERA_ZIP_PIPE = "carteraCompartida";

    public final static int CARGADO = 1, RECHAZADO = 2;
    private static List<String> directorios = Arrays.asList(ALERTAS, CARTERA_COMA, CARTERA_PIPE, CARTERA_ZIP_PIPE);
    private static List<String> subDirectorios = Arrays.asList(CARGADOS, PENDIENTES, RECHAZADOS);

    private static final Logger log = LoggerFactory.getLogger(AdministradorDirectorios.class);

    public static boolean create(String directorioBase) {
        try {
            for (String dir : directorios) {
                File file = new File(directorioBase + File.separator + dir);
                if (!file.exists()) {
                    file.mkdirs();
                }
                for (String subDir : subDirectorios) {
                    file = new File(directorioBase + File.separator + dir + File.separator + subDir);
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                }
            }
            return true;
        } catch (Exception e) {
            log.error("Ocurrio un error al tratar de crear directorios", e);
        }
        return false;
    }

    @SuppressWarnings({"unchecked"})
    public static List<String> getDirectoriesLectura(String directorioBase) {
        List<String> ans = new ArrayList(directorios.size());
        for (String directorio : directorios)
            ans.add(directorioBase + File.separator + directorio + File.separator + PENDIENTES);

        return ans;
    }

    public static String getDirectorioProcesados(String baseDirectory) {
        return baseDirectory + File.separator + CARGADOS;
    }

    public static String getDirectorioRechazados(String baseDirectory) {
        return baseDirectory + File.separator + RECHAZADOS;
    }
}
