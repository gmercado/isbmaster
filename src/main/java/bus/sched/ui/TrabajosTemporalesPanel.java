/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.sched.ui;

import bus.sched.api.ProgramadorTareasAdmin;
import bus.sched.model.TrabajoTemporalBean;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.list.OddEvenListItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.quartz.SimpleTrigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Marcelo Morales
 *         Created: 3/19/12 9:36 AM
 */
public class TrabajosTemporalesPanel extends Panel {

    private static final long serialVersionUID = -8501303338423794009L;

    private static final Logger LOGGER = LoggerFactory.getLogger(TrabajosTemporalesPanel.class);

    @Inject
    private ProgramadorTareasAdmin programadorTareas;

    public TrabajosTemporalesPanel(String id) {
        super(id);

        IModel<List<TrabajoTemporalBean>> trtempModel =
                new LoadableDetachableModel<List<TrabajoTemporalBean>>() {

                    private static final long serialVersionUID = -2867485614829903167L;

                    @Override
                    protected List<TrabajoTemporalBean> load() {
                        return new LinkedList<>(programadorTareas.getTrabajosTemporales());
                    }
                };

        add(new WebMarkupContainer("sin-trabajos", trtempModel) {

            private static final long serialVersionUID = -2828320527221982333L;

            @Override
            protected void onConfigure() {
                super.onConfigure();
                @SuppressWarnings("unchecked") List<? extends TrabajoTemporalBean> defaultModelObject =
                        (List<? extends TrabajoTemporalBean>) getDefaultModelObject();
                setVisible(defaultModelObject.isEmpty());
            }
        });

        add(new ListView<TrabajoTemporalBean>("trabajo", trtempModel) {

            private static final long serialVersionUID = -2828320527221982333L;

            @Override
            protected ListItem<TrabajoTemporalBean> newItem(int index, IModel<TrabajoTemporalBean> itemModel) {
                return new OddEvenListItem<>(index, itemModel);
            }


            @Override
            protected void populateItem(ListItem<TrabajoTemporalBean> parListItem) {
                final IModel<TrabajoTemporalBean> model = parListItem.getModel();
                parListItem.add(new Label("description", new PropertyModel<>(model, "p.description")));
                parListItem.add(new Label("estado", new LoadableDetachableModel<Object>() {

                    private static final long serialVersionUID = 5979261334256058754L;

                    @Override
                    protected Object load() {
                        final SimpleTrigger q = model.getObject().getQ();
                        if (q.getTimesTriggered() == 0) {
                            return "Programado para empezar a las " +
                                    DateFormat.getTimeInstance(DateFormat.LONG, getSession().getLocale()).
                                            format(q.getStartTime());
                        }
                        return "En ejecuci\u00F3n";
                    }
                }));

                parListItem.add(new Label("data", new LoadableDetachableModel<Object>() {

                    private static final long serialVersionUID = -8124719636908638978L;

                    @Override
                    protected Object load() {
                        return Maps.newHashMap(model.getObject().getP().getJobDataMap()).toString();
                    }
                }));

                parListItem.add(new Link<TrabajoTemporalBean>("cancelar", model) {

                    private static final long serialVersionUID = 9069123890249009510L;

                    @Override
                    public void onClick() {
                        try {
                            programadorTareas.cancelarTrabajoTemporal(getModelObject().getP().getKey());
                            setResponsePage(ListadoTrabajos.class);
                            getSession().info("Trabajo cancelado");
                        } catch (Exception e) {
                            LOGGER.warn("Error al cancelar un trabajo temporal", e);
                            getSession().error("El trabajo no se pudo cancelar porque ya termin\u00F3");
                        }
                    }
                });
            }
        });
    }
}