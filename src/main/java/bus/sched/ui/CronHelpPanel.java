/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.sched.ui;

import org.apache.wicket.markup.html.panel.Panel;

/**
 *
 * @author Marcelo Morales
 */
public class CronHelpPanel extends Panel {

    private static final long serialVersionUID = 29837432L;

    public CronHelpPanel(String id) {
        super(id);
    }
}
