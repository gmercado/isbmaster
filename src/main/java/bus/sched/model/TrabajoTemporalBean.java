/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.sched.model;

import org.quartz.JobDetail;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;

import java.io.Serializable;

/**
 * @author Marcelo Morales
 *         Created: 3/19/12 3:18 PM
 */
public class TrabajoTemporalBean implements Serializable {

    private static final long serialVersionUID = 2247670399556984976L;

    private JobDetail p;
    private SimpleTrigger q;
    private Trigger.TriggerState triggerState;

    public TrabajoTemporalBean() {
    }

    public TrabajoTemporalBean(JobDetail p, SimpleTrigger q, Trigger.TriggerState triggerState) {
        this.p = p;
        this.q = q;
        this.triggerState = triggerState;
    }

    public JobDetail getP() {
        return p;
    }

    public void setP(JobDetail p) {
        this.p = p;
    }

    public SimpleTrigger getQ() {
        return q;
    }

    public void setQ(SimpleTrigger q) {
        this.q = q;
    }

    public Trigger.TriggerState getTriggerState() {
        return triggerState;
    }

    public void setTriggerState(Trigger.TriggerState triggerState) {
        this.triggerState = triggerState;
    }
}
