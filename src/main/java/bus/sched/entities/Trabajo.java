/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.sched.entities;

import bus.sched.model.EstadoTrabajo;
import org.quartz.CronExpression;

import javax.persistence.Version;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

/**
 * @author Marcelo Morales
 */
public class Trabajo implements Serializable {

    private static final long serialVersionUID = 92387239487L;

    private Long id;

    private String nombre;

    private String grupo;

    private String cron;

    private EstadoTrabajo estado;

    private Date ultimaEjecucion;

    @Version
    private long version;

    // OJO: usado en UI.
    @SuppressWarnings("UnusedDeclaration")
    public String getNextExecution() {
        if (cron == null || EstadoTrabajo.DETE.equals(estado)) {
            return null;
        }
        try {
            final Date nextValidTimeAfter = new CronExpression(cron).getNextValidTimeAfter(new Date());
            final DateFormat ds = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, new Locale("es", "US"));
            return ds.format(nextValidTimeAfter);
        } catch (ParseException ex) {
            return null;
        }
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getUltimaEjecucion() {
        return ultimaEjecucion;
    }

    public void setUltimaEjecucion(Date ultimaEjecucion) {
        this.ultimaEjecucion = ultimaEjecucion;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public EstadoTrabajo getEstado() {
        return estado;
    }

    public void setEstado(EstadoTrabajo estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Trabajo))
            return false;

        Trabajo trabajo = (Trabajo) o;

        return !(id != null ? !id.equals(trabajo.id) : trabajo.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }
}
