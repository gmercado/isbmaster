/*
 * Copyright 2010-2012 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bus.sched.entities;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Marcelo Morales
 */
public class TrabajoHistorial implements Serializable {

    private Long id;

    private Date programado;

    private Date ejecutado;

    private String resultado;

    private String ejecutor;

    private Trabajo trabajo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Trabajo getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(Trabajo trabajo) {
        this.trabajo = trabajo;
    }

    public Date getProgramado() {
        return programado;
    }

    public void setProgramado(Date programado) {
        this.programado = programado;
    }

    public Date getEjecutado() {
        return ejecutado;
    }

    public void setEjecutado(Date ejecutado) {
        this.ejecutado = ejecutado;
    }

    public String getEjecutor() {
        return ejecutor;
    }

    public void setEjecutor(String ejecutor) {
        this.ejecutor = ejecutor;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TrabajoHistorial)) return false;

        TrabajoHistorial that = (TrabajoHistorial) o;

        return !(id != null ? !id.equals(that.id) : that.id != null);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
