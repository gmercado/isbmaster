package bus.sched;

import bus.plumbing.api.AbstractModuleBisa;
import bus.plumbing.api.ProgramadorTareas;
import bus.sched.api.ProgramadorTareasAdmin;
import bus.sched.general.DescargaFtpJob;
import bus.sched.general.ImportadorGeneralJob;
import bus.sched.general.SecuenciaCeroJob;
import bus.sched.general.PuestaTemporalProduccionJob;
import bus.sched.quartz.GuiceJobFactory;
import bus.sched.quartz.ProgramadorTareasImpl;
import com.google.inject.Scopes;

/**
 * @author Marcelo Morales
 *         Created: 5/1/12 6:05 AM
 */
public class SchedModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bind(GuiceJobFactory.class).in(Scopes.SINGLETON);
        bind(ProgramadorTareas.class).to(ProgramadorTareasAdmin.class);
        bind(ProgramadorTareasAdmin.class).to(ProgramadorTareasImpl.class).asEagerSingleton();
        bindUI("bus/sched/ui");
        bindSched("Lector descarga Infocred de ftp", "FTP", "0 1 * * * ?", DescargaFtpJob.class);
        bindSched("Lector archivos Infocred de importacion", "General", "0 10 * * * ?", ImportadorGeneralJob.class);
        // Corrige secuencias cero en produccion cada hora en el minuto 1
        bindSched("Secuencia Cero ", "ACH", "0 1 * * * ?", SecuenciaCeroJob.class);
        // Corrige secuencias de Temporal a produccion todos los días a las 7:00 am
        bindSched("Secuencia Temporal a Produccion ", "ACH", "0 0 7 * * ?", PuestaTemporalProduccionJob.class);
    }
}
