package bus.sched.general;

import bus.env.api.Variables;
import bus.sched.ui.AdministradorDirectorios;
import bus.sched.ui.DescargadorFtp;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.commons.net.ftp.FTPClient;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class DescargaFtpJob implements Job {

    private static String PREF = "#fecha";

    public final static int BIS_CSV = 1;
    public final static int BIS_PIPE = 2;
    public final static int BIS_ZIP_PIPE = 3;
    // alertas diarias
    public final static int ALERTA_1 = 4;
    public final static int ALERTA_2 = 5;
    public final static int ALERTA_3 = 6;
    public final static int ALERTA_4 = 7;
    public final static int ALERTA_5 = 8;
    public final static int ALERTA_6 = 9;
    public final static int ALERTA_7 = 10;

    private final DescargadorFtp descargadorFtp;

    private static final Logger log = LoggerFactory.getLogger(DescargaFtpJob.class);

    @Inject
    public DescargaFtpJob(DescargadorFtp descargadorFtp) {
        this.descargadorFtp = descargadorFtp;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("Iniciando proceso de descarga de la FTP de Infocred");

        String remoteFile1[] = Iterables.toArray(Splitter.on("|").trimResults().split(descargadorFtp.getNombreArchivo(BIS_CSV)), String.class);
        String remoteFile2[] = Iterables.toArray(Splitter.on("|").trimResults().split(descargadorFtp.getNombreArchivo(BIS_PIPE)), String.class);
        String remoteFile3[] = Iterables.toArray(Splitter.on("|").trimResults().split(descargadorFtp.getNombreArchivo(BIS_ZIP_PIPE)), String.class);

        String file1 = remoteFile1[0].replace(PREF, new SimpleDateFormat(remoteFile1[1]).format(new Date()));
        String file2 = remoteFile2[0].replace(PREF, new SimpleDateFormat(remoteFile2[1]).format(new Date()));
        String file3 = remoteFile3[0].replace(PREF, new SimpleDateFormat(remoteFile3[1]).format(new Date()));

        // alertas
        String remoteAlerta1 = descargadorFtp.getNombreArchivo(ALERTA_1);
        String remoteAlerta2 = descargadorFtp.getNombreArchivo(ALERTA_2);
        String remoteAlerta3 = descargadorFtp.getNombreArchivo(ALERTA_3);
        String remoteAlerta4 = descargadorFtp.getNombreArchivo(ALERTA_4);
        String remoteAlerta5 = descargadorFtp.getNombreArchivo(ALERTA_5);
        String remoteAlerta6 = descargadorFtp.getNombreArchivo(ALERTA_6);
        String remoteAlerta7 = descargadorFtp.getNombreArchivo(ALERTA_7);

        FTPClient ftpClient = descargadorFtp.getFtpClient();

        try {
            for (String directorio : AdministradorDirectorios.getDirectoriesLectura(Variables.DIRECTORIO_BASE_JOBS_DEFAULT)) {
                if (directorio.contains(AdministradorDirectorios.ALERTAS)) {
                    descargadorFtp.procesarArchivoFTP(ftpClient, directorio, remoteAlerta1);
                    descargadorFtp.procesarArchivoFTP(ftpClient, directorio, remoteAlerta2);
                    descargadorFtp.procesarArchivoFTP(ftpClient, directorio, remoteAlerta3);
                    descargadorFtp.procesarArchivoFTP(ftpClient, directorio, remoteAlerta4);
                    descargadorFtp.procesarArchivoFTP(ftpClient, directorio, remoteAlerta5);
                    descargadorFtp.procesarArchivoFTP(ftpClient, directorio, remoteAlerta6);
                    descargadorFtp.procesarArchivoFTP(ftpClient, directorio, remoteAlerta7);
                }
                if (directorio.contains(AdministradorDirectorios.CARTERA_COMA)) {
                    descargadorFtp.procesarArchivoFTP(ftpClient, directorio, file1);
                }
                if (directorio.contains(AdministradorDirectorios.CARTERA_PIPE)) {
                    descargadorFtp.procesarArchivoFTP(ftpClient, directorio, file2);
                }
                if (directorio.contains(AdministradorDirectorios.CARTERA_ZIP_PIPE)) {
                    descargadorFtp.procesarArchivoFTP(ftpClient, directorio, file3);
                }
            }
        } catch (Exception e) {
            log.error("Ocurrio un error al tratar de descargar el ZIP de Infocred");
        } finally {
            if (ftpClient != null) {
                try {
                    ftpClient.disconnect();
                } catch (IOException ign) {
                }
            }
        }
    }
}