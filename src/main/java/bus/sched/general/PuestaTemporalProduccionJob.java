package bus.sched.general;


import bus.database.model.AmbienteActual;
import bus.env.api.MedioAmbiente;
import com.bisa.bus.servicios.ach.dao.AchpMaestraDao;
import com.bisa.bus.servicios.ach.enums.ParticipanteTipo;
import com.bisa.bus.servicios.ach.utiles.DateUtil;
import com.bisa.bus.servicios.ach.utiles.RespuestaUtil;
import com.bisa.bus.servicios.asfi.infocred.ui.ReSendMailWrapper;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.commons.lang.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.apache.commons.dbutils.handlers.MapListHandler;
import bus.util.Sentencias;
import com.google.inject.Inject;
import org.openlogics.gears.jdbc.DataStoreFactory;
import org.openlogics.gears.jdbc.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.SQLException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;
import java.util.List;

import static bus.env.api.Variables.ACH_USUARIOS_A_NOTIFICAR;
import static bus.env.api.Variables.ACH_USUARIOS_A_NOTIFICAR_DEFAULT;

/**
 * Created by jjusto on 08/12/2017.
 */
public class PuestaTemporalProduccionJob implements Job {
    private static final Logger log = LoggerFactory.getLogger(SecuenciaCeroJob.class);

    private AchpMaestraDao achpMaestradao;
    private AmbienteActual ambienteActual;
    private MedioAmbiente medioAmbiente;
    private final ReSendMailWrapper mailWrapper;
    org.openlogics.gears.jdbc.ObjectDataStore objectDataStore = null;

    @Inject
    public PuestaTemporalProduccionJob(AchpMaestraDao achpMaestradao, ReSendMailWrapper mailWrapper, AmbienteActual ambienteActual, MedioAmbiente medioAmbiente) {
        this.achpMaestradao = achpMaestradao;
        this.mailWrapper = mailWrapper;
        this.ambienteActual = ambienteActual;
        this.medioAmbiente = medioAmbiente;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("ACH - Iniciando proceso para actualizar secuencias  de ambiente Temporal a Producción");
        if (ambienteActual.isOffline() || ambienteActual.isAmbienteTemporal()) {
            log.error("La aplicacion aun no se encuentra en Produccion {}, se debe notificar que la vuelta a produccion se atraso", DateUtil.fechaSistema());
            String mails = medioAmbiente.getValorDe(ACH_USUARIOS_A_NOTIFICAR, ACH_USUARIOS_A_NOTIFICAR_DEFAULT);
            String[] arraysMail =  StringUtils.split(mails, ",;");
            mailWrapper.sendMessage("Retraso de vuelta a produccion",
                    "El sistema ACH aun se encuentra en ambiente temporal, es necesario ejecutar el job manualmente para corregir las secuencias",
                    Arrays.asList(arraysMail));
            return;
        }
        Integer fechaActual = DateUtil.fechaSistema();
        try {
            objectDataStore = DataStoreFactory.createObjectDataStore(achpMaestradao.getConnection());
            objectDataStore.setAutoClose(false);
            // Verificamos si estamos en ambiente de produccion
            log.info("Ingresando a revisar las ordenes generadas en mientras estaba en TEMPORAL.");
            // Se obtiene todas las ordenes generadas en el ambiente TEMPORAL para asignar su secuencia
            objectDataStore.update(Query.of(Sentencias.GUARDA_REGISTROS_AMBIENTE_TMP_PROD ));
            log.info("**** Completado el proceso de paso de ambiente Temporal a Producción sin problemas ****");
        } catch (SQLException e) {
            log.error("Error sql: ", e);
        } catch (Throwable e) {
            log.error("Error inesperado: ", e);
        }

        try{
            // MODIFICACION DE SECUENCIALES DE TEMPORAL A PRODUCCION
            List<Map<String, Object>> ordenes = objectDataStore.select(Query.of(Sentencias.OBTENER_ORDENES_CON_SECUENCIA_AMBIENTE_TEMP), new MapListHandler());
            for (Map<String, Object> orden : ordenes) {
                BigDecimal nroLote = (BigDecimal) orden.get("ACHNROLOT");
                /** Si pertenece a lote se debe buscar cuenta real del debito en LPP01 **/
                if (nroLote.compareTo(BigDecimal.ZERO) > 0) {
                    continue;
//                    TODO: no se tiene aun algo pensado para encontrar secuencias de ordenes pertenecientes a un lote
                }
                String nroCuenta = RespuestaUtil.validaString(orden.get("ACHCNTDEST"));
                if (RespuestaUtil.validaString(orden.get("ACHORIGEN")).equalsIgnoreCase(ParticipanteTipo.BISA.getCodigo())) {
                    nroCuenta = RespuestaUtil.validaString(orden.get("ACHCNTORIG"));
                }
                /** Obtenemos de la tap820 la secuencia **/
                log.info("Fecha Actual: "+fechaActual + " Cuenta: " + nroCuenta + " Transaccion: " + orden.get("ACHCODTRN") + " Num Caja: " + orden.get("ACHNUMCAJA")
                        + " Importe: " + orden.get("ACHIMPORTE") + " Hora caja: " + orden.get("ACHTMECAJA") + " Num Orden: " + orden.get("ACHNUMORD"));
                List<Map<String, Object>> secuencias = objectDataStore.select(Query.of(Sentencias.OBTENER_SECUENCIA_DE_LA_ORDEN, fechaActual, orden.get("ACHCODTRN"), nroCuenta.replaceAll(" ", "").trim(), nroCuenta.replaceAll(" ", "").trim(), orden.get("ACHNUMCAJA"), orden.get("ACHIMPORTE")), new MapListHandler());

                log.info("Tamaño de las secuencias: " + secuencias.size());
                for (Map<String, Object> secuencia : secuencias) {
                    // Si la secuencia es utilizada en otra orden se utilizará la siguiente
                    log.info("SECUENCIAL: " + secuencia.get("XTNUMSEQ") + " NUMUSR: " + orden.get("ACHNUMCAJA") + " IMPORTE: " + new ScalarHandler<Integer>());
                    Integer existe = objectDataStore.select(Query.of(Sentencias.VERIFICAR_SI_SECUENCIA_ESTA_ASIGNADA, fechaActual, secuencia.get("XTNUMSEQ"), orden.get("ACHNUMCAJA")), new ScalarHandler<Integer>());
                    if (existe > 0) {
                        continue;
                    }
                    /** Se actualiza la orden con la secuencia correspondiente **/
                    objectDataStore.update(Query.of(Sentencias.ACTUALIZAR_NRO_SECUENCIA2, secuencia.get("XTNUMSEQ"), orden.get("ACHNUMORD")));
                    log.info("Orden a ser modificada: {}, caja:secuencia {}", orden.get("ACHNUMORD"), orden.get("ACHNUMCAJA") + ":" +  secuencia.get("XTNUMSEQ"));
                    break;
                }
            }

        } catch (SQLException e) {
            log.error("Error sql: ", e);
        } catch (Throwable e) {
            log.error("Error inesperado: ", e);
        } finally {
            if (objectDataStore != null) {
                try {
                    objectDataStore.tryCommitAndClose();
                } catch (SQLException e) {
                    log.error("Error al cerrar la conexion.");
                }
            }
        }
    }
}
