package bus.sched.general;

import com.bisa.bus.servicios.ach.dao.AchpMaestraDao;
import com.bisa.bus.servicios.ach.utiles.DateUtil;
import com.bisa.bus.servicios.ach.utiles.RespuestaUtil;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.apache.commons.dbutils.handlers.MapListHandler;
import bus.util.Sentencias;
import com.google.inject.Inject;
import org.openlogics.gears.jdbc.DataStoreFactory;
import org.openlogics.gears.jdbc.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.SQLException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.List;

/**
 * Created by jjusto on 04/12/2017.
 */
public class SecuenciaCeroJob implements Job {
    private static final Logger log = LoggerFactory.getLogger(SecuenciaCeroJob.class);

    private AchpMaestraDao achpMaestradao;
    org.openlogics.gears.jdbc.ObjectDataStore objectDataStore = null;

    @Inject
    public SecuenciaCeroJob(AchpMaestraDao achpMaestradao) {
        this.achpMaestradao = achpMaestradao;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("ACH - Iniciando proceso para actualizar secuencias 0");

        try {
            Integer fechaActual = DateUtil.fechaSistema();
            BigDecimal fechaJuliana = new BigDecimal(DateUtil.fechaSistemaJulian());
            objectDataStore = DataStoreFactory.createObjectDataStore(achpMaestradao.getConnection());
            objectDataStore.setAutoClose(false);
            List<Map<String, Object>> ordenes = objectDataStore.select(Query.of(Sentencias.OBTENER_ORDENES_CON_SECUENCIA_CERO_POR_FECHA, fechaActual, 0), new MapListHandler());
            for (Map<String, Object> orden : ordenes) {
                BigDecimal nroLote = (BigDecimal) orden.get("ACHNROLOT");
                String nroCuentaDest = (String) orden.get("ACHCNTDEST");
                /** Si pertenece a lote se debe buscar cuenta real del debito en LPP01 **/
                if (nroLote.compareTo(BigDecimal.ZERO) > 0) {
                    continue;
//                    TODO: no se tiene aun algo pensado para encontrar secuencias de ordenes pertenecientes a un lote
                }
                /** Obtenemos de la tap820 la secuencia **/
                log.info("fecha juliana: " + fechaJuliana + " Cnt Destino: " + " Transaccion: " + orden.get("ACHCODTRN") + " Num Caja: " + orden.get("ACHNUMCAJA")
                        + " Importe:" + orden.get("ACHIMPORTE")+ " Hora caja: " + orden.get("ACHTMECAJA") + " Num Orden: " + orden.get("ACHNUMORD"));
                log.info("fecha juliana: " + fechaJuliana + " Cnt Destino:");
                String cnt = RespuestaUtil.validaString(nroCuentaDest);
                List<Map<String, Object>> secuencias = objectDataStore.select(Query.of(Sentencias.OBTENER_SECUENCIA_DE_LA_ORDEN,
                        fechaActual, orden.get("ACHCODTRN"), cnt, cnt, orden.get("ACHNUMCAJA"), orden.get("ACHIMPORTE")), new MapListHandler());
                log.info("Tamaño de las secuencias: " + secuencias.size());
                for (Map<String, Object> secuencia : secuencias) {
                    // Si la secuencia es utilizada en otra orden se utilizará la siguiente
                    log.info("SECUENCIAL: "+secuencia.get("XTNUMSEQ")+" NUMUSR: "+orden.get("ACHNUMCAJA") +" IMPORTE: "+new ScalarHandler<Integer>());
                    Integer existe = objectDataStore.select(Query.of(Sentencias.VERIFICAR_SI_SECUENCIA_ESTA_ASIGNADA, fechaActual, secuencia.get("XTNUMSEQ"), orden.get("ACHNUMCAJA")), new ScalarHandler<Integer>());
                    if (existe > 0) {
                        continue;
                    }
                    /** Se actualiza la orden con la secuencia correspondiente **/
                    log.info("SECCC "+ secuencia.get("XTNUMSEQ")+" NUMORDEN"+ orden.get("ACHNUMORD"));
                    objectDataStore.update(Query.of(Sentencias.ACTUALIZAR_NRO_SECUENCIA, secuencia.get("XTNUMSEQ"), orden.get("ACHNUMORD")));
                    break;
                }
            }
            log.info("**** Completado el proceso de asignacion de secuencias sin problemas ****");
        } catch (SQLException e) {
            log.error("Error sql: ", e);
        } catch (Throwable e) {
            log.error("Error inesperado: ", e);
        } finally {
            if (objectDataStore != null) {
                try {
                    objectDataStore.tryCommitAndClose();
                } catch (SQLException e) {
                    log.error("Error al cerrar la conexion.");
                }
            }
        }
    }
}


