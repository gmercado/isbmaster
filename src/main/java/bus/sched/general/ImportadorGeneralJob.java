package bus.sched.general;

import bus.env.api.Variables;
import bus.sched.ui.AdministradorDirectorios;
import bus.sched.ui.Importador;
import com.google.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static bus.sched.general.DescargaFtpJob.BIS_CSV;
import static bus.sched.general.DescargaFtpJob.BIS_PIPE;
import static bus.sched.general.DescargaFtpJob.BIS_ZIP_PIPE;

/**
 * @Author Gary Mercado
 * Yrag Knup
 */
public class ImportadorGeneralJob implements Job {

    private static final Logger log = LoggerFactory.getLogger(ImportadorGeneralJob.class);

    private final Importador importador;

    @Inject
    public ImportadorGeneralJob(Importador importador) {
        this.importador = importador;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        // Se verifica si los directorios estan creados
        if (!AdministradorDirectorios.create(Variables.DIRECTORIO_BASE_JOBS_DEFAULT)) {
            log.info("No se puede ejecutar los jobs, los directorios no estan creados");
            return;
        }
        for (String directorio : AdministradorDirectorios.getDirectoriesLectura(Variables.DIRECTORIO_BASE_JOBS_DEFAULT)) {
            if (directorio.contains(AdministradorDirectorios.ALERTAS)) {
                procesar(directorio, AdministradorDirectorios.ALERTAS);
            } else if (directorio.contains(AdministradorDirectorios.CARTERA_PIPE)) {
                procesar(directorio, AdministradorDirectorios.CARTERA_PIPE);
            } else if (directorio.contains(AdministradorDirectorios.CARTERA_COMA)) {
                procesar(directorio, AdministradorDirectorios.CARTERA_COMA);
            } else if (directorio.contains(AdministradorDirectorios.CARTERA_ZIP_PIPE)) {
                procesar(directorio, AdministradorDirectorios.CARTERA_ZIP_PIPE);
            }
        }
    }

    public void procesar(String directoryPath, String directorio) {
        DirectoryStream<Path> directoryStream = null;
        try {
            directoryStream = Files.newDirectoryStream(Paths.get(directoryPath));
            String directorioTarget="";
            for (Path filePath : directoryStream) {
                File archivoPendiente = filePath.toFile();
                if (!archivoPendiente.getName().startsWith(".") && !archivoPendiente.isDirectory() && !archivoPendiente.isHidden()) {
                    String destinoBase = filePath.getParent().getParent().toString();
                    int sw = procesarArchivo(archivoPendiente, directorio);
                    directorioTarget = AdministradorDirectorios.getDirectorioRechazados(destinoBase);
                    if (sw == AdministradorDirectorios.CARGADO) {
                        directorioTarget = AdministradorDirectorios.getDirectorioProcesados(destinoBase);
                    }
                }
                String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                File destino = new File(directorioTarget + File.separator + timestamp + "_" + filePath.getFileName().toString());
                Files.move(filePath, destino.toPath(), StandardCopyOption.ATOMIC_MOVE);
            }
        } catch (IOException e) {
            log.error("Error al acceder al directorio " + directoryPath, e);
        } catch (Exception e) {
            log.error("Error inesperado al procesar " + directoryPath, e);
        } finally {
            if (directoryStream != null) {
                try {
                    directoryStream.close();
                } catch (IOException e) {
                    log.error("Error al cerrar el stream", e);
                }
            }
        }
    }

    public int procesarArchivo(File archivoPendiente, String directorio) {
        String nombreOriginal = archivoPendiente.getName();
        log.info("Procesando archivo {} ", nombreOriginal);
        try {
            // Para los casos donde el archivo este comprimido
            if (directorio.equals(AdministradorDirectorios.ALERTAS) && nombreOriginal.endsWith(".zip")) {
                archivoPendiente = unZipIt(archivoPendiente, ".xls");
            } else if (directorio.equals(AdministradorDirectorios.CARTERA_COMA) && nombreOriginal.endsWith(".zip")) {
                archivoPendiente = unZipIt(archivoPendiente, ".csv");
            } else if (directorio.equals(AdministradorDirectorios.CARTERA_PIPE) && nombreOriginal.endsWith(".zip")) {
                archivoPendiente = unZipIt(archivoPendiente, ".txt");
            } else if (directorio.equals(AdministradorDirectorios.CARTERA_ZIP_PIPE) && nombreOriginal.endsWith(".zip")) {
                archivoPendiente = unZipIt(archivoPendiente, ".txt");
            }

            List<String> carteras = new ArrayList<>();
            if (!directorio.equals(AdministradorDirectorios.ALERTAS)) {
                LineIterator iterator = FileUtils.lineIterator(archivoPendiente, "ISO-8859-1");
                try {
                    while (iterator.hasNext())
                        carteras.add(StringUtils.stripAccents(iterator.nextLine()));
                } finally {
                    LineIterator.closeQuietly(iterator);
                }
            }
            if (directorio.equals(AdministradorDirectorios.ALERTAS)) {
                return importador.importarAlertas(archivoPendiente, nombreOriginal);
            }
            if (directorio.equals(AdministradorDirectorios.CARTERA_COMA)) {
                return importador.importarCartera(nombreOriginal, carteras, BIS_CSV);
            }
            if (directorio.equals(AdministradorDirectorios.CARTERA_PIPE)) {
                return importador.importarCartera(nombreOriginal, carteras, BIS_PIPE);
            }
            if (directorio.equals(AdministradorDirectorios.CARTERA_ZIP_PIPE)) {
                return importador.importarCartera(nombreOriginal, carteras, BIS_ZIP_PIPE);
            }

        } catch (Exception e) {
            log.error("Error inesperado al leer el archivo {}, directorio {}", nombreOriginal, directorio, e);
        }
        return AdministradorDirectorios.RECHAZADO;
    }

    public File unZipIt(File fileZip, String suffix) throws IOException {
        log.info("Tratando de descomprimir el archivo {}", fileZip.getName());
        File temp = null;
        InputStream is = null;
        ZipFile zipFile = null;
        OutputStream outputStream = null;
        File tempDir = new File(System.getProperty("java.io.tmpdir"));

        try {
            zipFile = new ZipFile(fileZip);

            Enumeration<?> enu = zipFile.entries();
            while (enu.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) enu.nextElement();

                String name = zipEntry.getName();
                long size = zipEntry.getSize();
                long compressedSize = zipEntry.getCompressedSize();
                log.info("name: %-20s | size: %6d | compressed size: %6d\n", name, size, compressedSize);

                File file = new File(name);
                if (name.endsWith("/")) {
                    file.mkdirs();
                    continue;
                }

                is = zipFile.getInputStream(zipEntry);

                temp = new File(tempDir, FilenameUtils.removeExtension(fileZip.getName()) + suffix);
                outputStream = new FileOutputStream(temp);
                int read = 0;
                byte[] bytes = new byte[1024];
                while ((read = is.read(bytes)) != -1) {
                    outputStream.write(bytes, 0, read);
                }
            }
            return temp;

        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {}
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException ign) {}
            }
            if (zipFile != null) {
                try {
                    zipFile.close();
                } catch (IOException ign) {}
            }
        }
    }
}