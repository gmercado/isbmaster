package bus.logging;

import bus.plumbing.api.AbstractModuleBisa;

/**
 * @author Marcelo Morales
 *         Created: 5/15/12 7:23 PM
 */
public class LoggingModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bindUI("bus/logging/ui");
    }
}
