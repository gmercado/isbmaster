/*
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.logging.ui;

import bus.plumbing.api.RolesBisa;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.authorization.Action;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeAction;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * @author Marcelo Morales
 * @since 2/12/11
 */
public class CambiarNivelPanel extends Panel {

    private static final long serialVersionUID = -808403877140508456L;

    public CambiarNivelPanel(String id, final IModel<Logger> model) {
        super(id, model);
        add(new ChangeLogLink("info", model, Level.INFO));
        add(new ChangeLogLink("debug", model, Level.DEBUG));
    }

    @AuthorizeAction(action = Action.RENDER, roles = {RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
    private static class ChangeLogLink extends AjaxFallbackLink<Void> {
        private static final long serialVersionUID = 271100524610019978L;

        private final IModel<Logger> model;
        private final Level level;

        public ChangeLogLink(String id, IModel<Logger> model, Level level) {
            super(id);
            this.model = model;
            this.level = level;
        }

        @Override
        public void onClick(AjaxRequestTarget target) {
            model.getObject().setLevel(level);
            if (target != null) {
                target.add(findParent(DataTable.class));
            }
        }

        @Override
        protected void onInitialize() {
            super.onInitialize();
            setVisible(model.getObject().getEffectiveLevel().toInt() != level.toInt());
        }
    }
}
