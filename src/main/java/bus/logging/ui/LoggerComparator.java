/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.logging.ui;

import org.apache.log4j.Logger;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;

import java.io.Serializable;
import java.util.Comparator;

/**
 * @author Marcelo Morales
 *         Date: 7/11/11
 *         Time: 2:34 PM
 */
public class LoggerComparator implements Comparator<Logger>, Serializable {

    private static final long serialVersionUID = 5277640584081232271L;
    private final Cmp xmp;
    private final boolean ascending;

    public LoggerComparator(SortParam<String> sp) {
        Cmp y;
        try {
            y = Cmp.valueOf(sp.getProperty());
        } catch (IllegalArgumentException e) {
            y = Cmp.none;
        }
        xmp = y;
        ascending = sp.isAscending();
    }

    @Override
    public int compare(Logger logger, Logger logger1) {
        if (Cmp.name.equals(xmp)) {
            if (ascending) {
                return logger.getName().compareTo(logger1.getName());
            } else {
                return logger1.getName().compareTo(logger.getName());
            }
        }
        return 0;
    }

    private enum Cmp {
        name, none
    }
}
