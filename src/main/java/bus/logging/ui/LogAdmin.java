/*
 *
 * Copyright (c) 2010-2011. Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.logging.ui;

import bus.env.api.MedioAmbiente;
import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.RolesBisa;
import bus.plumbing.components.DropDownPanel;
import bus.plumbing.components.HighlightBehaviour;
import bus.plumbing.components.LinkPropertyColumn;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.UnmodifiableIterator;
import com.google.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.*;
import org.apache.log4j.net.SMTPAppender;
import org.apache.log4j.spi.Filter;
import org.apache.log4j.varia.LevelRangeFilter;
import org.apache.log4j.varia.StringMatchFilter;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackHeadersToolbar;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxNavigationToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.SortOrder;
import org.apache.wicket.extensions.markup.html.repeater.data.table.*;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.OddEvenItem;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.*;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.util.*;

import static bus.env.api.Variables.*;

/**
 * Permite administrar los niveles de log de la aplicaci�n en ejecuci�n.
 *
 * @author Marcelo Morales
 * @since 1/14/11 7:25 PM
 */
@Menu(value = "Administraci\u00f3n de Loggers", subMenu = SubMenu.ADMINISTRACION)
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.ISB_ADMIN})
public class LogAdmin extends BisaWebPage {

    private static final long serialVersionUID = -8784802559345615229L;

    private static final ArrayList<Level> levels = new ArrayList<>(Arrays.asList(Level.DEBUG, Level.INFO));

    private final IModel<Collection<String>> loggersList;

    private final DataTable<Logger, String> dt;

    private final IModel<String> nameModel;

    private final IModel<Level> effectiveLevelModel;

    private final IModel<Boolean> usarNivelReal;

    @Inject
    private MedioAmbiente medioAmbiente;

    /**
     * @param pageParameters puede tener la busqueda por defecto
     */
    public LogAdmin(PageParameters pageParameters) {
        super(pageParameters);

        loggersList = new LoadableDetachableModel<Collection<String>>() {

            private static final long serialVersionUID = -1456171092542641604L;

            @Override
            protected Collection<String> load() {
                @SuppressWarnings({"unchecked"})
                Enumeration<Logger> currentLoggers = Logger.getRootLogger().getLoggerRepository().getCurrentLoggers();
                TreeSet<String> strings = new TreeSet<>();

                while (currentLoggers.hasMoreElements()) {
                    Logger logger = currentLoggers.nextElement();
                    String name = logger.getName();

                    StringBuilder sb = new StringBuilder();
                    for (StringTokenizer tokenizer = new StringTokenizer(name, "."); tokenizer.hasMoreTokens(); ) {
                        String s = tokenizer.nextToken();
                        sb.append(s);
                        strings.add(sb.toString());
                        sb.append(".");
                    }
                }
                return strings;
            }
        };

        if (pageParameters.get("name").isEmpty()) {
            nameModel = new Model<>();
        } else {
            nameModel = new Model<>(pageParameters.get("name").toString());
        }
        effectiveLevelModel = new Model<>();

        Form<Void> form;
        add(form = new Form<>("filtro"));
        form.setOutputMarkupId(true);

        ArrayList<IColumn<Logger, String>> columns = new ArrayList<>();

        columns.add(new PropertyColumn<Logger, String>(Model.of("Nombre"), "name", "name") {

            private static final long serialVersionUID = -3336472935308194613L;

            @Override
            public void populateItem(Item<ICellPopulator<Logger>> item, String id, IModel<Logger> rowModel) {
                item.add(new Label(id, getDataModel(rowModel)).add(new HighlightBehaviour(nameModel)));
            }
        });
        columns.add(new AbstractColumn<Logger, String>(Model.of("Nivel")) {

            private static final long serialVersionUID = 4872084486357463371L;

            @Override
            public Component getHeader(String componentId) {
                return new LevelDropDownPanel(componentId, effectiveLevelModel, levels);
            }

            @Override
            public void populateItem(Item<ICellPopulator<Logger>> cellItem, String componentId, IModel<Logger> rowModel) {
                RepeatingView rv;
                cellItem.add(rv = new RepeatingView(componentId));

                rv.add(new Label(rv.newChildId(), new PropertyModel<>(rowModel, "effectiveLevel")));
                rv.add(new CambiarNivelPanel(rv.newChildId(), rowModel));
            }

            @Override
            public String getCssClass() {
                return "d";
            }

        });

        columns.add(new AbstractColumn<Logger, String>(Model.of("Afectados")) {
            private static final long serialVersionUID = -104459515342665980L;

            @Override
            public void populateItem(Item<ICellPopulator<Logger>> item, String id, final IModel<Logger> rowModel) {
                item.add(new Label(id, new AbstractReadOnlyModel<Integer>() {

                    private static final long serialVersionUID = 2135923159157717051L;

                    @Override
                    public Integer getObject() {
                        int c = 0;
                        for (String nombre : loggersList.getObject()) {
                            if (nombre.startsWith(rowModel.getObject().getName())) {
                                c++;
                            }
                        }
                        return c;
                    }
                }));
            }

            @Override
            public String getCssClass() {
                return "d";
            }
        });

        form.add(dt = new LoggingDataTable("loggers", columns, new SortableDataProvider<Logger, String>() {

            private static final long serialVersionUID = 3715025759090490102L;

            {
                setSort("name", SortOrder.ASCENDING);
            }

            @Override
            public Iterator<? extends Logger> iterator(long first, long count) {
                Collection<Logger> loggers = loggersActuales();
                ArrayList<Logger> ar = new ArrayList<>(loggers);
                Collections.sort(ar, new LoggerComparator(getSort()));
                return ar.subList((int) first, (int) (first + count)).iterator();
            }

            @Override
            public long size() {
                return loggersActuales().size();
            }

            @Override
            public IModel<Logger> model(Logger object) {
                final String name = object.getName();
                return new LoadableDetachableModel<Logger>() {

                    private static final long serialVersionUID = 5533716622479745758L;

                    @Override
                    protected Logger load() {
                        return Logger.getLogger(name);
                    }
                };
            }
        }, 15
        ));

        form.add(new TextField<>("name", nameModel).
                add(new OnChangeAjaxBehavior() {

                    private static final long serialVersionUID = -158157852959951301L;

                    @Override
                    protected void onUpdate(AjaxRequestTarget target) {
                        target.add(dt);
                    }
                }).setOutputMarkupId(true));

        usarNivelReal = Model.of(true);
        form.add(new CheckBox("nivelReal", usarNivelReal).
                add(new OnChangeAjaxBehavior() {

                    private static final long serialVersionUID = -959293205055919561L;

                    @Override
                    protected void onUpdate(AjaxRequestTarget target) {
                        target.add(dt);
                    }
                }).setOutputMarkupId(true));

        form.add(new SeguirElLogPanel("SeguirElLogPanel"));

        List<IColumn<Appender, String>> columnsAppenders = new ArrayList<>();
        columnsAppenders.add(new PropertyColumn<>(Model.of("Nombre"), "name"));
        columnsAppenders.add(new AbstractColumn<Appender, String>(Model.of("Tipo")) {

            @Override
            public void populateItem(Item<ICellPopulator<Appender>> cellItem, String componentId, IModel<Appender> rowModel) {
                Appender appender = rowModel.getObject();
                final String s;
                if (appender instanceof FileAppender) {
                    s = "Archivo " + ((FileAppender) appender).getFile();
                } else if (appender instanceof ConsoleAppender) {
                    s = "Terminal de consola";
                } else if (appender instanceof SMTPAppender) {
                    s = "Correo a " + ((SMTPAppender) appender).getTo();
                } else {
                    s = appender.getClass().toString();
                }
                cellItem.add(new Label(componentId, s));
            }
        });
        columnsAppenders.add(new LinkPropertyColumn<Appender>(Model.of("Operaciones"), "name") {

            @Override
            protected IModel<?> createLabelModel(IModel<Appender> rowModel) {
                return Model.of("eliminar");
            }

            @Override
            protected void onClick(Appender object) {
                Logger.getRootLogger().removeAppender(object);
            }
        });
        form.add(new AjaxFallbackDefaultDataTable<>("appenders", columnsAppenders,
                new SortableDataProvider<Appender, String>() {

                    @SuppressWarnings("unchecked")
                    @Override
                    public Iterator<? extends Appender> iterator(long first, long count) {
                        final UnmodifiableIterator iterator = Iterators.forEnumeration(
                                Logger.getRootLogger().getAllAppenders());
                        Iterators.advance(iterator, (int) first);
                        return Iterators.limit(iterator, (int) count);
                    }

                    @Override
                    public long size() {
                        return Iterators.size(Iterators.forEnumeration(Logger.getRootLogger().getAllAppenders()));
                    }

                    @Override
                    public IModel<Appender> model(Appender object) {
                        final String name = object.getName();
                        return new AbstractReadOnlyModel<Appender>() {

                            @Override
                            public Appender getObject() {
                                return Logger.getRootLogger().getAppender(name);
                            }
                        };
                    }
                }, 10));

// TODO:
//        seguirElLogPanel.setObject(getModelObject());
//        target.add(seguirElLogPanel);

        final Model<String> emailModel = Model.of("");
        final Model<NuevosAppenders> napModel = new Model<>();
        final Model<String> fileModel = Model.of("");
        final Model<Long> megasModel = new Model<>();
        final Model<FiltroAppender> filtroModel = new Model<>();
        final Model<String> expresionModel = new Model<>();
        final Model<Level> nivelModel = new Model<>();

        // TODO: validacion!
        RadioGroup<NuevosAppenders> rna;
        form.add(rna = new RadioGroup<>("radioset", napModel));
        rna.add(new Radio<>("mandar", Model.of(NuevosAppenders.MANDAR_EMAIL), rna));
        rna.add(new TextField<>("email", emailModel));
        rna.add(new Radio<>("guardar", Model.of(NuevosAppenders.CREAR_ARCHIVO), rna));
        rna.add(new TextField<>("file", fileModel));
        rna.add(new TextField<>("megas", megasModel, Long.class));
        rna.add(new Radio<>("salidaestandar", Model.of(NuevosAppenders.CONSOLA), rna));

        RadioGroup<FiltroAppender> rnb;
        form.add(rnb = new RadioGroup<>("radioset2", filtroModel));

        rnb.add(new Radio<>("regular", Model.of(FiltroAppender.EXPRESION), rnb));
        rnb.add(new TextField<>("expresion", expresionModel, String.class));
        rnb.add(new Radio<>("nivel", Model.of(FiltroAppender.NIVEL), rnb));
        rnb.add(new DropDownChoice<>("nivel select", nivelModel,
                Lists.newArrayList(Level.DEBUG, Level.INFO, Level.WARN, Level.ERROR)));

        form.add(new Button("nuevo appender") {

            @Override
            public void onSubmit() {
                Filter f;
                switch (filtroModel.getObject()) {
                    case EXPRESION:
                        f = new StringMatchFilter();
                        ((StringMatchFilter) f).setStringToMatch(expresionModel.getObject());
                        break;
                    case NIVEL:
                        f = new LevelRangeFilter();
                        ((LevelRangeFilter) f).setLevelMin(Level.OFF);
                        ((LevelRangeFilter) f).setLevelMax(nivelModel.getObject());
                        break;
                    default:
                        throw new IllegalStateException();
                }
                if (napModel != null && napModel.getObject() != null) {
                    switch (napModel.getObject()) {
                        case CONSOLA:
                            final ConsoleAppender newAppender = new ConsoleAppender();
                            newAppender.setName(UUID.randomUUID().toString());
                            newAppender.addFilter(f);
                            Logger.getRootLogger().addAppender(newAppender);
                            break;
                        case CREAR_ARCHIVO:
                            RollingFileAppender rollingFileAppender = new RollingFileAppender();
                            rollingFileAppender.setName(UUID.randomUUID().toString());
                            rollingFileAppender.setFile(fileModel.getObject());
                            rollingFileAppender.setAppend(true);
                            rollingFileAppender.setMaxBackupIndex(10);
                            rollingFileAppender.setMaxFileSize("" + megasModel.getObject() + "MB");
                            rollingFileAppender.addFilter(f);
                            Logger.getRootLogger().addAppender(rollingFileAppender);
                            break;
                        case MANDAR_EMAIL:
                            SMTPAppender smtpAppender = new SMTPAppender();
                            smtpAppender.setName(UUID.randomUUID().toString());
                            smtpAppender.setTo(emailModel.getObject());
                            smtpAppender.setFrom(medioAmbiente.getValorDe(MAIL_FROM, MAIL_FROM_DEFAULT));
                            smtpAppender.setSMTPHost(medioAmbiente.getValorDe(MAIL_HOST, MAIL_HOST_DEFAULT));
                            smtpAppender.addFilter(f);
                            Logger.getRootLogger().addAppender(smtpAppender);
                        default:
                            throw new IllegalStateException();
                    }
                }
            }
        });
    }

    private enum NuevosAppenders {
        MANDAR_EMAIL, CREAR_ARCHIVO, CONSOLA
    }

    private enum FiltroAppender {
        EXPRESION, NIVEL
    }

    private Collection<Logger> loggersActuales() {
        Collection<Logger> loggers = Collections2.transform(loggersList.getObject(), new LoggerTransformer());
        ArrayList<Predicate<Logger>> predicates = new ArrayList<>(3);

        if (nameModel.getObject() != null && !nameModel.getObject().isEmpty()) {
            predicates.add(input -> StringUtils.containsIgnoreCase(input.getName(), nameModel.getObject()));
        }

        if (effectiveLevelModel.getObject() != null) {
            if (usarNivelReal.getObject()) {
                predicates.add(input -> input.getLevel() != null &&
                        input.getLevel().toInt() == effectiveLevelModel.getObject().toInt());
            } else {
                predicates.add(input -> input.getEffectiveLevel().toInt() == effectiveLevelModel.getObject().toInt());
            }
        } else {
            if (usarNivelReal.getObject()) {
                predicates.add(input -> input.getLevel() != null);
            }
        }

        if (!predicates.isEmpty()) {
            loggers = Collections2.filter(loggers, Predicates.and(predicates));
        }
        return loggers;
    }

    private class LoggingDataTable extends DataTable<Logger, String> {

        private static final long serialVersionUID = 281271314122254302L;

        public LoggingDataTable(String id, List<IColumn<Logger, String>> columns,
                                ISortableDataProvider<Logger, String> dataProvider, int rowsPerPage) {
            super(id, columns, dataProvider, rowsPerPage);

            setOutputMarkupId(true);
            setVersioned(false);
            addTopToolbar(new AjaxNavigationToolbar(this));
            addTopToolbar(new AjaxFallbackHeadersToolbar<>(this, dataProvider));
            addBottomToolbar(new ConfigureLoggerToolbar(this, nameModel));
        }

        @Override
        protected Item<Logger> newRowItem(String id, int index, IModel<Logger> model) {
            return new OddEvenItem<>(id, index, model);
        }
    }

    private static class LoggerTransformer implements Function<String, Logger> {

        @Override
        public Logger apply(String from) {
            return Logger.getLogger(from);
        }
    }

    private class LevelDropDownPanel extends DropDownPanel<Level> {

        private static final long serialVersionUID = -2644282518016459526L;

        public LevelDropDownPanel(String componentId, IModel<Level> effectiveLevelModel1, ArrayList<Level> levels1) {
            super(componentId, effectiveLevelModel1, levels1);
        }

        @Override
        protected DropDownChoice<Level> newDropDown(String id, IModel<Level> levelIModel, List<Level> choices) {
            DropDownChoice<Level> ddc = super.newDropDown(id, levelIModel, choices);
            ddc.add(new AttributeAppender("class", "span2"));
            ddc.add(new AjaxFormComponentUpdatingBehavior("change") {

                @Override
                protected void onUpdate(AjaxRequestTarget target) {
                    //onUpdate(target);
                    target.add(dt);
                }
            });

//            ddc.add(new AjaxFormValidatingBehavior("onchange") {
//                @Override
//                protected void onSubmit(AjaxRequestTarget target) {
//                    super.onSubmit(target);
//                    target.add(dt);
//                }
//            });

            ddc.setNullValid(true);
            ddc.setOutputMarkupId(true);
            return ddc;
        }
    }
}
