/*
 * Copyright 2010-2011 Banco Bisa S.A.
 *
 * Licensed under the Apache License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bus.logging.ui;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

/**
 * @author Marcelo Morales
 *         Created: 11/3/11 5:24 PM
 */
public class ConfigureLoggerToolbar extends AbstractToolbar {

    private static final long serialVersionUID = 1L;

    public ConfigureLoggerToolbar(final DataTable<?, ?> table, IModel<String> nameModel) {
        super(table);

        WebMarkupContainer td;
        add(td = new WebMarkupContainer("td"));

        td.add(new AttributeModifier("colspan", new Model<>(String.valueOf(table.getColumns().size()))));

        Link<String> link;
        td.add(link = new Link<String>("crear", nameModel) {

            private static final long serialVersionUID = -7543519027758842323L;

            @Override
            public void onClick() {
                String modelObject = getModelObject();
                Logger logger = Logger.getLogger(modelObject);
                logger.setLevel(Level.INFO);
                logger.info("Log utilizado artificialmente desde la interfaz grafica");
                setResponsePage(LogAdmin.class, new PageParameters().add("name", modelObject));
            }
        });

        link.add(new Label("loggerName", nameModel));
    }


    @Override
    public boolean isVisible() {
        return getTable().getRowCount() == 0;
    }
}
