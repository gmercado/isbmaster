/*
 * Copyright 2012 Banco Bisa S.A.
 *
 *    Licensed under the Apache License, Version 1.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-1.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package bus.logging.ui;

import com.google.common.io.Files;
import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.time.Duration;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;

/**
 * @author Marcelo Morales
 * @since 3/20/12
 */
public class SeguirElLogPanel extends Panel {

    public static final long MOSTRAR_TANTOS_BYTES = 8096l;
    private static final long serialVersionUID = 7596703152766986561L;
    private final IModel<String> fileNameModel = Model.of();

    public SeguirElLogPanel(String id) {
        super(id);
        setOutputMarkupId(true);

        final MultiLineLabel mll;
        add(mll = new MultiLineLabel("log", new TailModel()) {

            private static final long serialVersionUID = 5005290610871880372L;

            @Override
            protected void onConfigure() {
                super.onConfigure();
                setVisible(fileNameModel.getObject() != null &&
                        new File(fileNameModel.getObject()).exists() && new File(fileNameModel.getObject()).canRead());
            }
        });
        mll.setOutputMarkupId(true);
        mll.add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(2)));
    }

    public void setObject(String object) {
        fileNameModel.setObject(object);
    }

    private class TailModel extends LoadableDetachableModel<String> {

        private static final long serialVersionUID = 4058642696647504714L;

        @Override
        protected String load() {
            try {
                File f = new File(fileNameModel.getObject());
                long length = f.length();
                if (length < MOSTRAR_TANTOS_BYTES) {
                    return Files.toString(f, Charset.defaultCharset());
                }
                //@see http://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html
                try (RandomAccessFile r = new RandomAccessFile(f, "r")) {
                    r.seek(length - MOSTRAR_TANTOS_BYTES);
                    String line;
                    r.readLine(); // Forget the first one!
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = r.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    return stringBuilder.toString();
                }
            } catch (IOException e) {
                return "No se pudo leer el archivo de log";
            }
        }
    }
}