package bus.ejemplos.ui;

import bus.inicio.ui.BisaWebPage;
import bus.inicio.utils.Menu;
import bus.inicio.utils.SubMenu;
import bus.plumbing.api.ProgramadorTareas;
import bus.plumbing.api.RolesBisa;
import bus.sched.ui.ListadoTrabajos;
import com.google.inject.Inject;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.core.util.lang.PropertyResolver;
import org.apache.wicket.extensions.ajax.markup.html.repeater.data.table.AjaxFallbackDefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.time.Duration;
import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;

import java.awt.*;
import java.util.*;

/**
 * @author Marcelo Morales
 *         Created: 5/15/12 7:26 PM
 */
@Menu(value = "Plantilla de estilos", subMenu = SubMenu.PARAMETROS, subtitulo = "Utiliza esto como ejemplo")
@AuthorizeInstantiation({RolesBisa.ADMIN, RolesBisa.SOLO_LECTURA})
public class PlantillaPage extends BisaWebPage {

    private static final long serialVersionUID = 1L;

    @Inject
    private ProgramadorTareas programadorTareas;

    public PlantillaPage() {

        // INICIO DE COMO USAR TABLAS!

        LinkedList<IColumn<Color, String>> iColumns = new LinkedList<IColumn<Color, String>>();
        iColumns.add(new PropertyColumn<Color, String>(Model.of("Alpha"), "alpha", "alpha"));
        iColumns.add(new PropertyColumn<Color, String>(Model.of("Blue"), "blue", "blue"));
        iColumns.add(new PropertyColumn<Color, String>(Model.of("Green"), "green", "green"));
        iColumns.add(new PropertyColumn<Color, String>(Model.of("Red"), "red", "red"));
        iColumns.add(new AbstractColumn<Color, String>(Model.of("Color")) {

            private static final long serialVersionUID = -1733186901862326860L;

            @Override
            public void populateItem(Item<ICellPopulator<Color>> cellItem, String componentId, IModel<Color> rowModel) {
                Color color = rowModel.getObject();
                cellItem.add(new Label(componentId, "......").
                        add(new AttributeAppender("style",
                                Model.of("background-color: rgba(" + color.getRed() +
                                        "," + color.getGreen() +
                                        "," + color.getBlue() +
                                        "," + color.getAlpha() +
                                        ")"))));
            }
        });

        SortableDataProvider<Color, String> sortableDataProvider = new ColoresDataProvider();

        add(new AjaxFallbackDefaultDataTable<Color, String>("tabla1", iColumns, sortableDataProvider, 5));
        add(new AjaxFallbackDefaultDataTable<Color, String>("tabla2", iColumns, sortableDataProvider, 5));
        add(new AjaxFallbackDefaultDataTable<Color, String>("tabla3", iColumns, sortableDataProvider, 5));
        add(new AjaxFallbackDefaultDataTable<Color, String>("tabla4", iColumns, sortableDataProvider, 5));
        add(new AjaxFallbackDefaultDataTable<Color, String>("tabla5", iColumns, sortableDataProvider, 5));


        // INICIO DE COMO USAR FORMULARIOS
        // TODO: hacer formularios con form-horizontal, form-inline, form-search
        // TODO: varios tipos de controles: class="input-medium search-query", input-small, control-group, help-block, btn, form-actions
        // TODO: botones: colores, dropdowns, split dropdowns,
        // TODO: Colocar alertas
        // TODO: componentes est�ndar: radios, checks, fechas, autocompletados

        // INICIO de TABS y PILLS
        // TODO: ejemplo de tabs con wicket e integrar al bootstrap
        // TODO: inline labels
        // TODO: Badges y contadores


        // TODO: barras de progreso
        // TODO: wells

        // TODO: modales NO DE WICKET
        // TODO: dropdowns
        // TODO: licencias y documentos largos

        // TODO: listados
        // TODO: experimental: toggle buttons

        // TODO: Fontificar XML y XML dentro de XML
        // TODO: experimental: carruseles y acordeones

        IModel<String> m = Model.of("");
        add(new Form<String>("iniciar trabajo temporal", m){

            @Override
            protected void onSubmit() {
                programadorTareas.programarTrabajoTemporal(LoguearJob.class, "loguea", Duration.seconds(30),
                        Collections.singletonMap("Equis X Equis", getModelObject()));
            }
        }.add(new RequiredTextField<String>("lo que loguea", m)).
                add(new BookmarkablePageLink<ListadoTrabajos>("ir a desatendidos", ListadoTrabajos.class)));
    }

    private static final class LoguearJob implements InterruptableJob {

        @Override
        public void interrupt() throws UnableToInterruptJobException {
        }

        @Override
        public void execute(JobExecutionContext context) throws JobExecutionException {
            LOGGER.info(" ---- Logueando " + context.get("Equis X Equis") + " ------ ");
        }
    }

    private static class ColoresDataProvider extends SortableDataProvider<Color, String> {

        private static final long serialVersionUID = -1945309459344646316L;

        private final LinkedList<Color> colors;

        {
            Random random = new Random();
            colors = new LinkedList<Color>();
            int lim = random.nextInt(1000);
            for (int i = 0; i < lim; i++) {
                colors.add(new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255), random.nextInt(255)));
            }
        }

        @Override
        public Iterator<? extends Color> iterator(long first, long count) {
            SortParam sort = getSort();
            if (sort != null) {
                Collections.sort(colors, new Comparator<Color>() {

                    @Override
                    public int compare(Color o1, Color o2) {
                        Integer value1 = ((Number) PropertyResolver.getValue(getSort().getProperty(), o1)).intValue();
                        Integer value2 = ((Number) PropertyResolver.getValue(getSort().getProperty(), o2)).intValue();
                        if (getSort().isAscending()) {
                            return value1.compareTo(value2);
                        } else {
                            return value2.compareTo(value1);
                        }
                    }
                });
            }
            return colors.subList((int)first, (int) (first + count)).iterator();
        }

        @Override
        public long size() {
            return colors.size();
        }

        @Override
        public IModel<Color> model(Color object) {
            return Model.of(object);
        }
    }
}
