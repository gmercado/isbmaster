package bus.ejemplos.sched;

import bus.plumbing.wicket.Version;
import com.google.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marcelo Morales
 *         Created: 5/14/12 7:43 PM
 */
public class MostrarVersionJob implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(MostrarVersionJob.class);

    private final Version version;

    @Inject
    public MostrarVersionJob(Version version) {
        this.version = version;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("------- {} -------", version.getVersionPom());
    }
}
