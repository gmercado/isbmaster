package bus.ejemplos;

import bus.ejemplos.sched.MostrarVersionJob;
import bus.plumbing.api.AbstractModuleBisa;

/**
 * @author Marcelo Morales
 *         Created: 5/14/12 7:43 PM
 */
public class EjemploModule extends AbstractModuleBisa {

    @Override
    protected void configure() {
        bindSched("version", "ejemplo", "0 */2 * * * ?", MostrarVersionJob.class);
        bindSchedConHistoria("version con historia", "ejemplo", "10 */5 * * * ?", MostrarVersionJob.class);
        bindUI("bus/ejemplos/ui");
    }
}
